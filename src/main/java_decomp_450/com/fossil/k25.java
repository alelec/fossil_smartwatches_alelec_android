package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class k25 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ ua5 H;
    @DexIgnore
    public /* final */ RTLImageView I;
    @DexIgnore
    public /* final */ ConstraintLayout J;
    @DexIgnore
    public /* final */ ConstraintLayout K;
    @DexIgnore
    public /* final */ LinearLayout L;
    @DexIgnore
    public /* final */ LinearLayout M;
    @DexIgnore
    public /* final */ ConstraintLayout N;
    @DexIgnore
    public /* final */ RecyclerView O;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat P;
    @DexIgnore
    public /* final */ View Q;
    @DexIgnore
    public /* final */ View R;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public k25(Object obj, View view, int i, View view2, View view3, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, ua5 ua5, RTLImageView rTLImageView, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout5, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, View view4, View view5) {
        super(obj, view, i);
        this.q = view2;
        this.r = view3;
        this.s = flexibleButton;
        this.t = constraintLayout;
        this.u = constraintLayout2;
        this.v = flexibleTextView;
        this.w = flexibleTextView2;
        this.x = flexibleTextView3;
        this.y = flexibleTextView4;
        this.z = flexibleTextView5;
        this.A = flexibleTextView6;
        this.B = flexibleTextView7;
        this.C = flexibleTextView8;
        this.D = flexibleTextView9;
        this.E = flexibleTextView10;
        this.F = flexibleTextView11;
        this.G = flexibleTextView12;
        this.H = ua5;
        a((ViewDataBinding) ua5);
        this.I = rTLImageView;
        this.J = constraintLayout3;
        this.K = constraintLayout4;
        this.L = linearLayout;
        this.M = linearLayout2;
        this.N = constraintLayout5;
        this.O = recyclerView;
        this.P = flexibleSwitchCompat;
        this.Q = view4;
        this.R = view5;
    }
}
