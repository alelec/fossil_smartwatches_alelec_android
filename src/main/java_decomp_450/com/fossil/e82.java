package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e82 extends uf2 implements x62 {
    @DexIgnore
    public e82(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    @DexIgnore
    @Override // com.fossil.x62
    public final ab2 a(ab2 ab2, d72 d72) throws RemoteException {
        Parcel E = E();
        vf2.a(E, ab2);
        vf2.a(E, d72);
        Parcel a = a(2, E);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
