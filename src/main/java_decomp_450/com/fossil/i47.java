package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i47 extends e47 {
    @DexIgnore
    public Long m; // = null;
    @DexIgnore
    public String n;
    @DexIgnore
    public String o;

    @DexIgnore
    public i47(Context context, String str, String str2, int i, Long l, a47 a47) {
        super(context, i, a47);
        this.o = str;
        this.n = str2;
        this.m = l;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public f47 a() {
        return f47.a;
    }

    @DexIgnore
    @Override // com.fossil.e47
    public boolean a(JSONObject jSONObject) {
        a67.a(jSONObject, "pi", this.n);
        a67.a(jSONObject, "rf", this.o);
        Long l = this.m;
        if (l == null) {
            return true;
        }
        jSONObject.put("du", l);
        return true;
    }
}
