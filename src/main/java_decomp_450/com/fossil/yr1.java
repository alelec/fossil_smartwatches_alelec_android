package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class yr1 extends Enum<yr1> {
    @DexIgnore
    public static /* final */ yr1 b;
    @DexIgnore
    public static /* final */ /* synthetic */ yr1[] c;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        yr1 yr1 = new yr1("GOAL_TRACKING", 2, (byte) 2);
        b = yr1;
        c = new yr1[]{new yr1("EVENT_MAPPING", 0, (byte) 0), new yr1("KEY_CODE_MAPPING", 1, (byte) 1), yr1, new yr1("UNDEFINED", 3, (byte) 255)};
    }
    */

    @DexIgnore
    public yr1(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static yr1 valueOf(String str) {
        return (yr1) Enum.valueOf(yr1.class, str);
    }

    @DexIgnore
    public static yr1[] values() {
        return (yr1[]) c.clone();
    }
}
