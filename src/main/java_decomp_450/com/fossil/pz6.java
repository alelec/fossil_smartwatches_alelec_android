package com.fossil;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pz6 extends RecyclerView.q {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b; // = true;
    @DexIgnore
    public int c; // = 1;
    @DexIgnore
    public /* final */ LinearLayoutManager d;

    @DexIgnore
    public pz6(LinearLayoutManager linearLayoutManager) {
        ee7.b(linearLayoutManager, "mLinearLayoutManager");
        this.d = linearLayoutManager;
    }

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(int i, int i2);

    @DexIgnore
    public final void a(RecyclerView recyclerView) {
        int childCount = recyclerView.getChildCount();
        int j = this.d.j();
        int I = this.d.I();
        if (this.b && j > this.a) {
            this.b = false;
            this.a = j;
        }
        if (!this.b && (j - I) - childCount == 0) {
            int i = this.c + 1;
            this.c = i;
            a(i);
            this.b = true;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void onScrollStateChanged(RecyclerView recyclerView, int i) {
        ee7.b(recyclerView, "recyclerView");
        super.onScrollStateChanged(recyclerView, i);
        if (i == 0) {
            a(recyclerView);
            a(this.d.I(), this.d.L());
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.q
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        ee7.b(recyclerView, "recyclerView");
        super.onScrolled(recyclerView, i, i2);
        a(recyclerView);
    }

    @DexIgnore
    public final void a() {
        this.c = 1;
        this.a = 0;
        this.b = true;
    }
}
