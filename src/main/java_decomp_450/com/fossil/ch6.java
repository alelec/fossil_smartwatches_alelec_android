package com.fossil;

import com.portfolio.platform.data.source.WorkoutSessionRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch6 implements Factory<bh6> {
    @DexIgnore
    public /* final */ Provider<WorkoutSessionRepository> a;

    @DexIgnore
    public ch6(Provider<WorkoutSessionRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ch6 a(Provider<WorkoutSessionRepository> provider) {
        return new ch6(provider);
    }

    @DexIgnore
    public static bh6 a(WorkoutSessionRepository workoutSessionRepository) {
        return new bh6(workoutSessionRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public bh6 get() {
        return a(this.a.get());
    }
}
