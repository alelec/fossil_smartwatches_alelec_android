package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.view.menu.ActionMenuItemView;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.h9;
import com.fossil.v1;
import com.fossil.w1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e2 extends k1 implements h9.a {
    @DexIgnore
    public int A;
    @DexIgnore
    public /* final */ SparseBooleanArray B; // = new SparseBooleanArray();
    @DexIgnore
    public e C;
    @DexIgnore
    public a D;
    @DexIgnore
    public c E;
    @DexIgnore
    public b F;
    @DexIgnore
    public /* final */ f G; // = new f();
    @DexIgnore
    public int H;
    @DexIgnore
    public d j;
    @DexIgnore
    public Drawable p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public int t;
    @DexIgnore
    public int u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends u1 {
        @DexIgnore
        public a(Context context, a2 a2Var, View view) {
            super(context, a2Var, view, false, y.actionOverflowMenuStyle);
            if (!((r1) a2Var.getItem()).h()) {
                View view2 = e2.this.j;
                a(view2 == null ? (View) ((k1) e2.this).h : view2);
            }
            a(e2.this.G);
        }

        @DexIgnore
        @Override // com.fossil.u1
        public void e() {
            e2 e2Var = e2.this;
            e2Var.D = null;
            e2Var.H = 0;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ActionMenuItemView.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // androidx.appcompat.view.menu.ActionMenuItemView.b
        public y1 a() {
            a aVar = e2.this.D;
            if (aVar != null) {
                return aVar.c();
            }
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public e a;

        @DexIgnore
        public c(e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void run() {
            if (((k1) e2.this).c != null) {
                ((k1) e2.this).c.a();
            }
            View view = (View) ((k1) e2.this).h;
            if (!(view == null || view.getWindowToken() == null || !this.a.g())) {
                e2.this.C = this.a;
            }
            e2.this.E = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AppCompatImageView implements ActionMenuView.a {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends t2 {
            @DexIgnore
            public a(View view, e2 e2Var) {
                super(view);
            }

            @DexIgnore
            @Override // com.fossil.t2
            public y1 b() {
                e eVar = e2.this.C;
                if (eVar == null) {
                    return null;
                }
                return eVar.c();
            }

            @DexIgnore
            @Override // com.fossil.t2
            public boolean c() {
                e2.this.j();
                return true;
            }

            @DexIgnore
            @Override // com.fossil.t2
            public boolean d() {
                e2 e2Var = e2.this;
                if (e2Var.E != null) {
                    return false;
                }
                e2Var.f();
                return true;
            }
        }

        @DexIgnore
        public d(Context context) {
            super(context, null, y.actionOverflowButtonStyle);
            setClickable(true);
            setFocusable(true);
            setVisibility(0);
            setEnabled(true);
            i3.a(this, getContentDescription());
            setOnTouchListener(new a(this, e2.this));
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean a() {
            return false;
        }

        @DexIgnore
        @Override // androidx.appcompat.widget.ActionMenuView.a
        public boolean b() {
            return false;
        }

        @DexIgnore
        public boolean performClick() {
            if (super.performClick()) {
                return true;
            }
            playSoundEffect(0);
            e2.this.j();
            return true;
        }

        @DexIgnore
        public boolean setFrame(int i, int i2, int i3, int i4) {
            boolean frame = super.setFrame(i, i2, i3, i4);
            Drawable drawable = getDrawable();
            Drawable background = getBackground();
            if (!(drawable == null || background == null)) {
                int width = getWidth();
                int height = getHeight();
                int max = Math.max(width, height) / 2;
                int paddingLeft = (width + (getPaddingLeft() - getPaddingRight())) / 2;
                int paddingTop = (height + (getPaddingTop() - getPaddingBottom())) / 2;
                p7.a(background, paddingLeft - max, paddingTop - max, paddingLeft + max, paddingTop + max);
            }
            return frame;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends u1 {
        @DexIgnore
        public e(Context context, p1 p1Var, View view, boolean z) {
            super(context, p1Var, view, z, y.actionOverflowMenuStyle);
            a(8388613);
            a(e2.this.G);
        }

        @DexIgnore
        @Override // com.fossil.u1
        public void e() {
            if (((k1) e2.this).c != null) {
                ((k1) e2.this).c.close();
            }
            e2.this.C = null;
            super.e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @SuppressLint({"BanParcelableUsage"})
    public static class g implements Parcelable {
        @DexIgnore
        public static /* final */ Parcelable.Creator<g> CREATOR; // = new a();
        @DexIgnore
        public int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Parcelable.Creator<g> {
            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public g createFromParcel(Parcel parcel) {
                return new g(parcel);
            }

            @DexIgnore
            @Override // android.os.Parcelable.Creator
            public g[] newArray(int i) {
                return new g[i];
            }
        }

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public int describeContents() {
            return 0;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.a);
        }

        @DexIgnore
        public g(Parcel parcel) {
            this.a = parcel.readInt();
        }
    }

    @DexIgnore
    public e2(Context context) {
        super(context, e0.abc_action_menu_layout, e0.abc_action_menu_item_layout);
    }

    @DexIgnore
    public boolean h() {
        return this.E != null || i();
    }

    @DexIgnore
    public boolean i() {
        e eVar = this.C;
        return eVar != null && eVar.d();
    }

    @DexIgnore
    public boolean j() {
        p1 p1Var;
        if (!this.r || i() || (p1Var = ((k1) this).c) == null || ((k1) this).h == null || this.E != null || p1Var.j().isEmpty()) {
            return false;
        }
        c cVar = new c(new e(((k1) this).b, ((k1) this).c, this.j, true));
        this.E = cVar;
        ((View) ((k1) this).h).post(cVar);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.k1, com.fossil.v1
    public void a(Context context, p1 p1Var) {
        super.a(context, p1Var);
        Resources resources = context.getResources();
        b1 a2 = b1.a(context);
        if (!this.s) {
            this.r = a2.g();
        }
        if (!this.y) {
            this.t = a2.b();
        }
        if (!this.w) {
            this.v = a2.c();
        }
        int i = this.t;
        if (this.r) {
            if (this.j == null) {
                d dVar = new d(((k1) this).a);
                this.j = dVar;
                if (this.q) {
                    dVar.setImageDrawable(this.p);
                    this.p = null;
                    this.q = false;
                }
                int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
                this.j.measure(makeMeasureSpec, makeMeasureSpec);
            }
            i -= this.j.getMeasuredWidth();
        } else {
            this.j = null;
        }
        this.u = i;
        this.A = (int) (resources.getDisplayMetrics().density * 56.0f);
    }

    @DexIgnore
    @Override // com.fossil.k1
    public w1 b(ViewGroup viewGroup) {
        w1 w1Var = ((k1) this).h;
        w1 b2 = super.b(viewGroup);
        if (w1Var != b2) {
            ((ActionMenuView) b2).setPresenter(this);
        }
        return b2;
    }

    @DexIgnore
    public void c(boolean z2) {
        this.z = z2;
    }

    @DexIgnore
    public void d(boolean z2) {
        this.r = z2;
        this.s = true;
    }

    @DexIgnore
    public Drawable e() {
        d dVar = this.j;
        if (dVar != null) {
            return dVar.getDrawable();
        }
        if (this.q) {
            return this.p;
        }
        return null;
    }

    @DexIgnore
    public boolean f() {
        w1 w1Var;
        c cVar = this.E;
        if (cVar == null || (w1Var = ((k1) this).h) == null) {
            e eVar = this.C;
            if (eVar == null) {
                return false;
            }
            eVar.b();
            return true;
        }
        ((View) w1Var).removeCallbacks(cVar);
        this.E = null;
        return true;
    }

    @DexIgnore
    public boolean g() {
        a aVar = this.D;
        if (aVar == null) {
            return false;
        }
        aVar.b();
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        g gVar = new g();
        gVar.a = this.H;
        return gVar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements v1.a {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public boolean a(p1 p1Var) {
            if (p1Var == ((k1) e2.this).c) {
                return false;
            }
            e2.this.H = ((a2) p1Var).getItem().getItemId();
            v1.a a2 = e2.this.a();
            if (a2 != null) {
                return a2.a(p1Var);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.v1.a
        public void a(p1 p1Var, boolean z) {
            if (p1Var instanceof a2) {
                p1Var.m().a(false);
            }
            v1.a a2 = e2.this.a();
            if (a2 != null) {
                a2.a(p1Var, z);
            }
        }
    }

    @DexIgnore
    public boolean d() {
        return f() | g();
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        int i;
        ArrayList<r1> arrayList;
        int i2;
        int i3;
        int i4;
        e2 e2Var = this;
        p1 p1Var = ((k1) e2Var).c;
        View view = null;
        int i5 = 0;
        if (p1Var != null) {
            arrayList = p1Var.n();
            i = arrayList.size();
        } else {
            arrayList = null;
            i = 0;
        }
        int i6 = e2Var.v;
        int i7 = e2Var.u;
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, 0);
        ViewGroup viewGroup = (ViewGroup) ((k1) e2Var).h;
        boolean z2 = false;
        int i8 = 0;
        int i9 = 0;
        for (int i10 = 0; i10 < i; i10++) {
            r1 r1Var = arrayList.get(i10);
            if (r1Var.k()) {
                i8++;
            } else if (r1Var.j()) {
                i9++;
            } else {
                z2 = true;
            }
            if (e2Var.z && r1Var.isActionViewExpanded()) {
                i6 = 0;
            }
        }
        if (e2Var.r && (z2 || i9 + i8 > i6)) {
            i6--;
        }
        int i11 = i6 - i8;
        SparseBooleanArray sparseBooleanArray = e2Var.B;
        sparseBooleanArray.clear();
        if (e2Var.x) {
            int i12 = e2Var.A;
            i2 = i7 / i12;
            i3 = i12 + ((i7 % i12) / i2);
        } else {
            i3 = 0;
            i2 = 0;
        }
        int i13 = 0;
        int i14 = 0;
        while (i13 < i) {
            r1 r1Var2 = arrayList.get(i13);
            if (r1Var2.k()) {
                View a2 = e2Var.a(r1Var2, view, viewGroup);
                if (e2Var.x) {
                    i2 -= ActionMenuView.a(a2, i3, i2, makeMeasureSpec, i5);
                } else {
                    a2.measure(makeMeasureSpec, makeMeasureSpec);
                }
                int measuredWidth = a2.getMeasuredWidth();
                i7 -= measuredWidth;
                if (i14 == 0) {
                    i14 = measuredWidth;
                }
                int groupId = r1Var2.getGroupId();
                if (groupId != 0) {
                    sparseBooleanArray.put(groupId, true);
                }
                r1Var2.d(true);
                i4 = i;
            } else if (r1Var2.j()) {
                int groupId2 = r1Var2.getGroupId();
                boolean z3 = sparseBooleanArray.get(groupId2);
                boolean z4 = (i11 > 0 || z3) && i7 > 0 && (!e2Var.x || i2 > 0);
                boolean z5 = z4;
                i4 = i;
                if (z4) {
                    View a3 = e2Var.a(r1Var2, null, viewGroup);
                    if (e2Var.x) {
                        int a4 = ActionMenuView.a(a3, i3, i2, makeMeasureSpec, 0);
                        i2 -= a4;
                        if (a4 == 0) {
                            z5 = false;
                        }
                    } else {
                        a3.measure(makeMeasureSpec, makeMeasureSpec);
                    }
                    int measuredWidth2 = a3.getMeasuredWidth();
                    i7 -= measuredWidth2;
                    if (i14 == 0) {
                        i14 = measuredWidth2;
                    }
                    z4 = z5 & (!e2Var.x ? i7 + i14 > 0 : i7 >= 0);
                }
                if (z4 && groupId2 != 0) {
                    sparseBooleanArray.put(groupId2, true);
                } else if (z3) {
                    sparseBooleanArray.put(groupId2, false);
                    for (int i15 = 0; i15 < i13; i15++) {
                        r1 r1Var3 = arrayList.get(i15);
                        if (r1Var3.getGroupId() == groupId2) {
                            if (r1Var3.h()) {
                                i11++;
                            }
                            r1Var3.d(false);
                        }
                    }
                }
                if (z4) {
                    i11--;
                }
                r1Var2.d(z4);
            } else {
                i4 = i;
                r1Var2.d(false);
                i13++;
                i = i4;
                view = null;
                i5 = 0;
                e2Var = this;
            }
            i13++;
            i = i4;
            view = null;
            i5 = 0;
            e2Var = this;
        }
        return true;
    }

    @DexIgnore
    public void a(Configuration configuration) {
        if (!this.w) {
            this.v = b1.a(((k1) this).b).c();
        }
        p1 p1Var = ((k1) this).c;
        if (p1Var != null) {
            p1Var.c(true);
        }
    }

    @DexIgnore
    public void a(Drawable drawable) {
        d dVar = this.j;
        if (dVar != null) {
            dVar.setImageDrawable(drawable);
            return;
        }
        this.q = true;
        this.p = drawable;
    }

    @DexIgnore
    @Override // com.fossil.k1
    public View a(r1 r1Var, View view, ViewGroup viewGroup) {
        View actionView = r1Var.getActionView();
        if (actionView == null || r1Var.f()) {
            actionView = super.a(r1Var, view, viewGroup);
        }
        actionView.setVisibility(r1Var.isActionViewExpanded() ? 8 : 0);
        ActionMenuView actionMenuView = (ActionMenuView) viewGroup;
        ViewGroup.LayoutParams layoutParams = actionView.getLayoutParams();
        if (!actionMenuView.checkLayoutParams(layoutParams)) {
            actionView.setLayoutParams(actionMenuView.generateLayoutParams(layoutParams));
        }
        return actionView;
    }

    @DexIgnore
    @Override // com.fossil.k1
    public void a(r1 r1Var, w1.a aVar) {
        aVar.a(r1Var, 0);
        ActionMenuItemView actionMenuItemView = (ActionMenuItemView) aVar;
        actionMenuItemView.setItemInvoker((ActionMenuView) ((k1) this).h);
        if (this.F == null) {
            this.F = new b();
        }
        actionMenuItemView.setPopupCallback(this.F);
    }

    @DexIgnore
    @Override // com.fossil.k1
    public boolean a(int i, r1 r1Var) {
        return r1Var.h();
    }

    @DexIgnore
    @Override // com.fossil.k1, com.fossil.v1
    public void a(boolean z2) {
        w1 w1Var;
        super.a(z2);
        ((View) ((k1) this).h).requestLayout();
        p1 p1Var = ((k1) this).c;
        boolean z3 = false;
        if (p1Var != null) {
            ArrayList<r1> c2 = p1Var.c();
            int size = c2.size();
            for (int i = 0; i < size; i++) {
                h9 a2 = c2.get(i).a();
                if (a2 != null) {
                    a2.setSubUiVisibilityListener(this);
                }
            }
        }
        p1 p1Var2 = ((k1) this).c;
        ArrayList<r1> j2 = p1Var2 != null ? p1Var2.j() : null;
        if (this.r && j2 != null) {
            int size2 = j2.size();
            if (size2 == 1) {
                z3 = !j2.get(0).isActionViewExpanded();
            } else if (size2 > 0) {
                z3 = true;
            }
        }
        if (z3) {
            if (this.j == null) {
                this.j = new d(((k1) this).a);
            }
            ViewGroup viewGroup = (ViewGroup) this.j.getParent();
            if (viewGroup != ((k1) this).h) {
                if (viewGroup != null) {
                    viewGroup.removeView(this.j);
                }
                ActionMenuView actionMenuView = (ActionMenuView) ((k1) this).h;
                actionMenuView.addView(this.j, actionMenuView.b());
            }
        } else {
            d dVar = this.j;
            if (dVar != null && dVar.getParent() == (w1Var = ((k1) this).h)) {
                ((ViewGroup) w1Var).removeView(this.j);
            }
        }
        ((ActionMenuView) ((k1) this).h).setOverflowReserved(this.r);
    }

    @DexIgnore
    @Override // com.fossil.h9.a
    public void b(boolean z2) {
        if (z2) {
            super.a((a2) null);
            return;
        }
        p1 p1Var = ((k1) this).c;
        if (p1Var != null) {
            p1Var.a(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.k1
    public boolean a(ViewGroup viewGroup, int i) {
        if (viewGroup.getChildAt(i) == this.j) {
            return false;
        }
        return super.a(viewGroup, i);
    }

    @DexIgnore
    @Override // com.fossil.k1, com.fossil.v1
    public boolean a(a2 a2Var) {
        boolean z2 = false;
        if (!a2Var.hasVisibleItems()) {
            return false;
        }
        a2 a2Var2 = a2Var;
        while (a2Var2.t() != ((k1) this).c) {
            a2Var2 = (a2) a2Var2.t();
        }
        View a2 = a(a2Var2.getItem());
        if (a2 == null) {
            return false;
        }
        this.H = a2Var.getItem().getItemId();
        int size = a2Var.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            MenuItem item = a2Var.getItem(i);
            if (item.isVisible() && item.getIcon() != null) {
                z2 = true;
                break;
            }
            i++;
        }
        a aVar = new a(((k1) this).b, a2Var, a2);
        this.D = aVar;
        aVar.a(z2);
        this.D.f();
        super.a(a2Var);
        return true;
    }

    @DexIgnore
    public final View a(MenuItem menuItem) {
        ViewGroup viewGroup = (ViewGroup) ((k1) this).h;
        if (viewGroup == null) {
            return null;
        }
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childAt = viewGroup.getChildAt(i);
            if ((childAt instanceof w1.a) && ((w1.a) childAt).getItemData() == menuItem) {
                return childAt;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.k1, com.fossil.v1
    public void a(p1 p1Var, boolean z2) {
        d();
        super.a(p1Var, z2);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
        int i;
        MenuItem findItem;
        if ((parcelable instanceof g) && (i = ((g) parcelable).a) > 0 && (findItem = ((k1) this).c.findItem(i)) != null) {
            a((a2) findItem.getSubMenu());
        }
    }

    @DexIgnore
    public void a(ActionMenuView actionMenuView) {
        ((k1) this).h = actionMenuView;
        actionMenuView.a(((k1) this).c);
    }
}
