package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bk5 extends Service {
    @DexIgnore
    public static dk5 c;
    @DexIgnore
    public int a;
    @DexIgnore
    public jb5 b; // = jb5.NOT_START;

    @DexIgnore
    public static void a(dk5 dk5) {
        c = dk5;
    }

    @DexIgnore
    public abstract void b();

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public void d() {
        jb5 jb5;
        if (c != null && this.b != (jb5 = jb5.RUNNING)) {
            this.b = jb5;
            c.a(this.a, jb5);
            c.a(this);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return null;
    }

    @DexIgnore
    public void a() {
        jb5 jb5;
        if (c != null && this.b != (jb5 = jb5.FINISHED)) {
            this.b = jb5;
            c.a(this.a, jb5);
            c.b(this);
        }
    }
}
