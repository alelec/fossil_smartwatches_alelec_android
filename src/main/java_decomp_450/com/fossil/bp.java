package com.fossil;

import android.database.Cursor;
import com.fossil.qm;
import com.fossil.zo;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp implements ap {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<zo> b;
    @DexIgnore
    public /* final */ ji c;
    @DexIgnore
    public /* final */ ji d;
    @DexIgnore
    public /* final */ ji e;
    @DexIgnore
    public /* final */ ji f;
    @DexIgnore
    public /* final */ ji g;
    @DexIgnore
    public /* final */ ji h;
    @DexIgnore
    public /* final */ ji i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<zo> {
        @DexIgnore
        public a(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, zo zoVar) {
            String str = zoVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            ajVar.bindLong(2, (long) fp.a(zoVar.b));
            String str2 = zoVar.c;
            if (str2 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, str2);
            }
            String str3 = zoVar.d;
            if (str3 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, str3);
            }
            byte[] a = cm.a(zoVar.e);
            if (a == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindBlob(5, a);
            }
            byte[] a2 = cm.a(zoVar.f);
            if (a2 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindBlob(6, a2);
            }
            ajVar.bindLong(7, zoVar.g);
            ajVar.bindLong(8, zoVar.h);
            ajVar.bindLong(9, zoVar.i);
            ajVar.bindLong(10, (long) zoVar.k);
            ajVar.bindLong(11, (long) fp.a(zoVar.l));
            ajVar.bindLong(12, zoVar.m);
            ajVar.bindLong(13, zoVar.n);
            ajVar.bindLong(14, zoVar.o);
            ajVar.bindLong(15, zoVar.p);
            ajVar.bindLong(16, zoVar.q ? 1 : 0);
            am amVar = zoVar.j;
            if (amVar != null) {
                ajVar.bindLong(17, (long) fp.a(amVar.b()));
                ajVar.bindLong(18, amVar.g() ? 1 : 0);
                ajVar.bindLong(19, amVar.h() ? 1 : 0);
                ajVar.bindLong(20, amVar.f() ? 1 : 0);
                ajVar.bindLong(21, amVar.i() ? 1 : 0);
                ajVar.bindLong(22, amVar.c());
                ajVar.bindLong(23, amVar.d());
                byte[] a3 = fp.a(amVar.a());
                if (a3 == null) {
                    ajVar.bindNull(24);
                } else {
                    ajVar.bindBlob(24, a3);
                }
            } else {
                ajVar.bindNull(17);
                ajVar.bindNull(18);
                ajVar.bindNull(19);
                ajVar.bindNull(20);
                ajVar.bindNull(21);
                ajVar.bindNull(22);
                ajVar.bindNull(23);
                ajVar.bindNull(24);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ji {
        @DexIgnore
        public d(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ji {
        @DexIgnore
        public e(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends ji {
        @DexIgnore
        public f(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends ji {
        @DexIgnore
        public g(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends ji {
        @DexIgnore
        public h(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends ji {
        @DexIgnore
        public i(bp bpVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public bp(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
        this.c = new b(this, ciVar);
        this.d = new c(this, ciVar);
        this.e = new d(this, ciVar);
        this.f = new e(this, ciVar);
        this.g = new f(this, ciVar);
        this.h = new g(this, ciVar);
        this.i = new h(this, ciVar);
        new i(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.ap
    public void a(zo zoVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(zoVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public void b(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.e.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<String> c(String str) {
        fi b2 = fi.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public qm.a d(String str) {
        fi b2 = fi.b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        qm.a aVar = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                aVar = fp.c(a2.getInt(0));
            }
            return aVar;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public int e() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.i.acquire();
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.i.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public int f(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.g.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<cm> g(String str) {
        fi b2 = fi.b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(cm.b(a2.getBlob(0)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public int h(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.f.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public void a(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.c.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public zo e(String str) {
        fi fiVar;
        zo zoVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar2 = new zo(string, string2);
                    zoVar2.b = fp.c(a2.getInt(b12));
                    zoVar2.d = a2.getString(b14);
                    zoVar2.e = cm.b(a2.getBlob(b15));
                    zoVar2.f = cm.b(a2.getBlob(b16));
                    zoVar2.g = a2.getLong(b17);
                    zoVar2.h = a2.getLong(b18);
                    zoVar2.i = a2.getLong(b19);
                    zoVar2.k = a2.getInt(b20);
                    zoVar2.l = fp.a(a2.getInt(b21));
                    zoVar2.m = a2.getLong(b22);
                    zoVar2.n = a2.getLong(b23);
                    zoVar2.o = a2.getLong(b24);
                    zoVar2.p = a2.getLong(b25);
                    zoVar2.q = a2.getInt(b26) != 0;
                    zoVar2.j = amVar;
                    zoVar = zoVar2;
                } else {
                    zoVar = null;
                }
                a2.close();
                fiVar.c();
                return zoVar;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo.b> b(String str) {
        fi b2 = fi.b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "state");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                zo.b bVar = new zo.b();
                bVar.a = a2.getString(b3);
                bVar.b = fp.c(a2.getInt(b4));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<String> d() {
        fi b2 = fi.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo> c() {
        fi fiVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                int i2 = b16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar = new zo(string, string2);
                    zoVar.b = fp.c(a2.getInt(b12));
                    zoVar.d = a2.getString(b14);
                    zoVar.e = cm.b(a2.getBlob(b15));
                    zoVar.f = cm.b(a2.getBlob(i2));
                    i2 = i2;
                    zoVar.g = a2.getLong(b17);
                    b17 = b17;
                    zoVar.h = a2.getLong(b18);
                    b18 = b18;
                    zoVar.i = a2.getLong(b19);
                    zoVar.k = a2.getInt(b20);
                    b20 = b20;
                    zoVar.l = fp.a(a2.getInt(b21));
                    b19 = b19;
                    zoVar.m = a2.getLong(b22);
                    b22 = b22;
                    zoVar.n = a2.getLong(b23);
                    b23 = b23;
                    zoVar.o = a2.getLong(b24);
                    b24 = b24;
                    zoVar.p = a2.getLong(b25);
                    zoVar.q = a2.getInt(b26) != 0;
                    zoVar.j = amVar;
                    arrayList.add(zoVar);
                    b26 = b26;
                    b25 = b25;
                    b15 = b15;
                    b11 = b11;
                    b13 = b13;
                    b3 = b3;
                    b4 = b4;
                    b21 = b21;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public void a(String str, cm cmVar) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.d.acquire();
        byte[] a2 = cm.a(cmVar);
        if (a2 == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindBlob(1, a2);
        }
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo> b() {
        fi fiVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                int i2 = b16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar = new zo(string, string2);
                    zoVar.b = fp.c(a2.getInt(b12));
                    zoVar.d = a2.getString(b14);
                    zoVar.e = cm.b(a2.getBlob(b15));
                    zoVar.f = cm.b(a2.getBlob(i2));
                    i2 = i2;
                    zoVar.g = a2.getLong(b17);
                    b17 = b17;
                    zoVar.h = a2.getLong(b18);
                    b18 = b18;
                    zoVar.i = a2.getLong(b19);
                    zoVar.k = a2.getInt(b20);
                    b20 = b20;
                    zoVar.l = fp.a(a2.getInt(b21));
                    b19 = b19;
                    zoVar.m = a2.getLong(b22);
                    b22 = b22;
                    zoVar.n = a2.getLong(b23);
                    b23 = b23;
                    zoVar.o = a2.getLong(b24);
                    b24 = b24;
                    zoVar.p = a2.getLong(b25);
                    zoVar.q = a2.getInt(b26) != 0;
                    zoVar.j = amVar;
                    arrayList.add(zoVar);
                    b26 = b26;
                    b25 = b25;
                    b15 = b15;
                    b11 = b11;
                    b13 = b13;
                    b3 = b3;
                    b4 = b4;
                    b21 = b21;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public int a(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.h.acquire();
        acquire.bindLong(1, j);
        if (str == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo> a(int i2) {
        fi fiVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 AND schedule_requested_at=-1 ORDER BY period_start_time LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b2.bindLong(1, (long) i2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                int i3 = b16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar = new zo(string, string2);
                    zoVar.b = fp.c(a2.getInt(b12));
                    zoVar.d = a2.getString(b14);
                    zoVar.e = cm.b(a2.getBlob(b15));
                    zoVar.f = cm.b(a2.getBlob(i3));
                    i3 = i3;
                    zoVar.g = a2.getLong(b17);
                    b17 = b17;
                    zoVar.h = a2.getLong(b18);
                    b18 = b18;
                    zoVar.i = a2.getLong(b19);
                    zoVar.k = a2.getInt(b20);
                    b20 = b20;
                    zoVar.l = fp.a(a2.getInt(b21));
                    b19 = b19;
                    zoVar.m = a2.getLong(b22);
                    b22 = b22;
                    zoVar.n = a2.getLong(b23);
                    b23 = b23;
                    zoVar.o = a2.getLong(b24);
                    b24 = b24;
                    zoVar.p = a2.getLong(b25);
                    zoVar.q = a2.getInt(b26) != 0;
                    zoVar.j = amVar;
                    arrayList.add(zoVar);
                    b26 = b26;
                    b25 = b25;
                    b14 = b14;
                    b11 = b11;
                    b13 = b13;
                    b4 = b4;
                    b3 = b3;
                    b21 = b21;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo> a() {
        fi fiVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE state=0 ORDER BY period_start_time", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                int i2 = b16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar = new zo(string, string2);
                    zoVar.b = fp.c(a2.getInt(b12));
                    zoVar.d = a2.getString(b14);
                    zoVar.e = cm.b(a2.getBlob(b15));
                    zoVar.f = cm.b(a2.getBlob(i2));
                    i2 = i2;
                    zoVar.g = a2.getLong(b17);
                    b17 = b17;
                    zoVar.h = a2.getLong(b18);
                    b18 = b18;
                    zoVar.i = a2.getLong(b19);
                    zoVar.k = a2.getInt(b20);
                    b20 = b20;
                    zoVar.l = fp.a(a2.getInt(b21));
                    b19 = b19;
                    zoVar.m = a2.getLong(b22);
                    b22 = b22;
                    zoVar.n = a2.getLong(b23);
                    b23 = b23;
                    zoVar.o = a2.getLong(b24);
                    b24 = b24;
                    zoVar.p = a2.getLong(b25);
                    zoVar.q = a2.getInt(b26) != 0;
                    zoVar.j = amVar;
                    arrayList.add(zoVar);
                    b26 = b26;
                    b25 = b25;
                    b15 = b15;
                    b11 = b11;
                    b13 = b13;
                    b3 = b3;
                    b4 = b4;
                    b21 = b21;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public List<zo> a(long j) {
        fi fiVar;
        fi b2 = fi.b("SELECT `required_network_type`, `requires_charging`, `requires_device_idle`, `requires_battery_not_low`, `requires_storage_not_low`, `trigger_content_update_delay`, `trigger_max_content_delay`, `content_uri_triggers`, `WorkSpec`.`id` AS `id`, `WorkSpec`.`state` AS `state`, `WorkSpec`.`worker_class_name` AS `worker_class_name`, `WorkSpec`.`input_merger_class_name` AS `input_merger_class_name`, `WorkSpec`.`input` AS `input`, `WorkSpec`.`output` AS `output`, `WorkSpec`.`initial_delay` AS `initial_delay`, `WorkSpec`.`interval_duration` AS `interval_duration`, `WorkSpec`.`flex_duration` AS `flex_duration`, `WorkSpec`.`run_attempt_count` AS `run_attempt_count`, `WorkSpec`.`backoff_policy` AS `backoff_policy`, `WorkSpec`.`backoff_delay_duration` AS `backoff_delay_duration`, `WorkSpec`.`period_start_time` AS `period_start_time`, `WorkSpec`.`minimum_retention_duration` AS `minimum_retention_duration`, `WorkSpec`.`schedule_requested_at` AS `schedule_requested_at`, `WorkSpec`.`run_in_foreground` AS `run_in_foreground` FROM workspec WHERE period_start_time >= ? AND state IN (2, 3, 5) ORDER BY period_start_time DESC", 1);
        b2.bindLong(1, j);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "required_network_type");
            int b4 = oi.b(a2, "requires_charging");
            int b5 = oi.b(a2, "requires_device_idle");
            int b6 = oi.b(a2, "requires_battery_not_low");
            int b7 = oi.b(a2, "requires_storage_not_low");
            int b8 = oi.b(a2, "trigger_content_update_delay");
            int b9 = oi.b(a2, "trigger_max_content_delay");
            int b10 = oi.b(a2, "content_uri_triggers");
            int b11 = oi.b(a2, "id");
            int b12 = oi.b(a2, "state");
            int b13 = oi.b(a2, "worker_class_name");
            int b14 = oi.b(a2, "input_merger_class_name");
            int b15 = oi.b(a2, "input");
            int b16 = oi.b(a2, "output");
            fiVar = b2;
            try {
                int b17 = oi.b(a2, "initial_delay");
                int b18 = oi.b(a2, "interval_duration");
                int b19 = oi.b(a2, "flex_duration");
                int b20 = oi.b(a2, "run_attempt_count");
                int b21 = oi.b(a2, "backoff_policy");
                int b22 = oi.b(a2, "backoff_delay_duration");
                int b23 = oi.b(a2, "period_start_time");
                int b24 = oi.b(a2, "minimum_retention_duration");
                int b25 = oi.b(a2, "schedule_requested_at");
                int b26 = oi.b(a2, "run_in_foreground");
                int i2 = b16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b11);
                    String string2 = a2.getString(b13);
                    am amVar = new am();
                    amVar.a(fp.b(a2.getInt(b3)));
                    amVar.b(a2.getInt(b4) != 0);
                    amVar.c(a2.getInt(b5) != 0);
                    amVar.a(a2.getInt(b6) != 0);
                    amVar.d(a2.getInt(b7) != 0);
                    amVar.a(a2.getLong(b8));
                    amVar.b(a2.getLong(b9));
                    amVar.a(fp.a(a2.getBlob(b10)));
                    zo zoVar = new zo(string, string2);
                    zoVar.b = fp.c(a2.getInt(b12));
                    zoVar.d = a2.getString(b14);
                    zoVar.e = cm.b(a2.getBlob(b15));
                    zoVar.f = cm.b(a2.getBlob(i2));
                    i2 = i2;
                    zoVar.g = a2.getLong(b17);
                    zoVar.h = a2.getLong(b18);
                    b18 = b18;
                    zoVar.i = a2.getLong(b19);
                    zoVar.k = a2.getInt(b20);
                    b20 = b20;
                    zoVar.l = fp.a(a2.getInt(b21));
                    b19 = b19;
                    zoVar.m = a2.getLong(b22);
                    b22 = b22;
                    zoVar.n = a2.getLong(b23);
                    b23 = b23;
                    zoVar.o = a2.getLong(b24);
                    b24 = b24;
                    zoVar.p = a2.getLong(b25);
                    zoVar.q = a2.getInt(b26) != 0;
                    zoVar.j = amVar;
                    arrayList.add(zoVar);
                    b26 = b26;
                    b25 = b25;
                    b13 = b13;
                    b4 = b4;
                    b3 = b3;
                    b14 = b14;
                    b17 = b17;
                    b11 = b11;
                    b21 = b21;
                    b5 = b5;
                }
                a2.close();
                fiVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.ap
    public int a(qm.a aVar, String... strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = si.a();
        a2.append("UPDATE workspec SET state=");
        a2.append("?");
        a2.append(" WHERE id IN (");
        si.a(a2, strArr.length);
        a2.append(")");
        aj compileStatement = this.a.compileStatement(a2.toString());
        compileStatement.bindLong(1, (long) fp.a(aVar));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
        }
    }
}
