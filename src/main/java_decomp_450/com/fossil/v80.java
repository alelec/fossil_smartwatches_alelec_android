package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ long e; // = 4294967295L;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<v80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final v80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 8) {
                ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
                return new v80(yz0.b(order.getInt(0)), order.getShort(4), order.getShort(6));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 8"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public v80 createFromParcel(Parcel parcel) {
            return new v80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public v80[] newArray(int i) {
            return new v80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public v80 m80createFromParcel(Parcel parcel) {
            return new v80(parcel, null);
        }
    }

    /*
    static {
        de7 de7 = de7.a;
    }
    */

    @DexIgnore
    public v80(long j, short s, short s2) throws IllegalArgumentException {
        super(o80.TIME);
        this.b = j;
        this.c = s;
        this.d = s2;
        e();
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.b).putShort(this.c).putShort(this.d).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        long j = e;
        long j2 = this.b;
        boolean z = true;
        if (0 <= j2 && j >= j2) {
            short s = this.c;
            if (s >= 0 && 1000 >= s) {
                short s2 = this.d;
                if (Short.MIN_VALUE > s2 || Short.MAX_VALUE < s2) {
                    z = false;
                }
                if (!z) {
                    throw new IllegalArgumentException(yh0.a(yh0.b("timezoneOffsetInMinute("), this.d, ") is out of range ", "[-32768, 32767]."));
                }
                return;
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("millisecond("), this.c, ") is out of range ", "[0, 1000]."));
        }
        StringBuilder b2 = yh0.b("second(");
        b2.append(this.b);
        b2.append(") is out of range ");
        b2.append("[0, ");
        b2.append(e);
        b2.append("].");
        throw new IllegalArgumentException(b2.toString());
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(v80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            v80 v80 = (v80) obj;
            return this.b == v80.b && this.c == v80.c && this.d == v80.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.TimeConfig");
    }

    @DexIgnore
    public final short getMillisecond() {
        return this.c;
    }

    @DexIgnore
    public final long getSecond() {
        return this.b;
    }

    @DexIgnore
    public final short getTimezoneOffsetInMinute() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return ((((Long.valueOf(this.b).hashCode() + (super.hashCode() * 31)) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.c));
        }
        if (parcel != null) {
            parcel.writeInt(yz0.b(this.d));
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("second", this.b);
            jSONObject.put("millisecond", Short.valueOf(this.c));
            jSONObject.put("timezone_offset_in_minute", Short.valueOf(this.d));
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ v80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readLong();
        this.c = (short) parcel.readInt();
        this.d = (short) parcel.readInt();
        e();
    }
}
