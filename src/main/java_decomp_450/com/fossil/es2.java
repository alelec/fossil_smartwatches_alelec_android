package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es2 extends gs2<V> {
    @DexIgnore
    public /* final */ /* synthetic */ ds2 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public es2(ds2 ds2) {
        super(ds2, null);
        this.e = ds2;
    }

    @DexIgnore
    @Override // com.fossil.gs2
    public final V a(int i) {
        return (V) this.e.zzc[i];
    }
}
