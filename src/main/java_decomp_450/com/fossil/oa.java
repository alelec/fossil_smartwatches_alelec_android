package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import com.fossil.ra;
import com.sina.weibo.sdk.api.ImageObject;
import io.flutter.view.AccessibilityBridge;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa {
    @DexIgnore
    public static int d;
    @DexIgnore
    public /* final */ AccessibilityNodeInfo a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ a e; // = new a(1, null);
        @DexIgnore
        public static /* final */ a f; // = new a(2, null);
        @DexIgnore
        public static /* final */ a g; // = new a(16, null);
        @DexIgnore
        public static /* final */ a h; // = new a(4096, null);
        @DexIgnore
        public static /* final */ a i; // = new a(8192, null);
        @DexIgnore
        public static /* final */ a j; // = new a(262144, null);
        @DexIgnore
        public static /* final */ a k; // = new a(524288, null);
        @DexIgnore
        public static /* final */ a l; // = new a(1048576, null);
        @DexIgnore
        public static /* final */ a m; // = new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_UP : null, 16908344, null, null, null);
        @DexIgnore
        public static /* final */ a n; // = new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_DOWN : null, 16908346, null, null, null);
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ Class<? extends ra.a> c;
        @DexIgnore
        public /* final */ ra d;

        /*
        static {
            AccessibilityNodeInfo.AccessibilityAction accessibilityAction = null;
            new a(4, null);
            new a(8, null);
            new a(32, null);
            new a(64, null);
            new a(128, null);
            new a(256, null, ra.b.class);
            new a(512, null, ra.b.class);
            new a(1024, null, ra.c.class);
            new a(2048, null, ra.c.class);
            new a(16384, null);
            new a(32768, null);
            new a(65536, null);
            new a(131072, null, ra.g.class);
            new a(ImageObject.DATA_SIZE, null, ra.h.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_ON_SCREEN : null, AccessibilityBridge.ACTION_SHOW_ON_SCREEN, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_TO_POSITION : null, 16908343, null, null, ra.e.class);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_LEFT : null, 16908345, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SCROLL_RIGHT : null, 16908347, null, null, null);
            new a(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_UP : null, 16908358, null, null, null);
            new a(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_DOWN : null, 16908359, null, null, null);
            new a(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_LEFT : null, 16908360, null, null, null);
            new a(Build.VERSION.SDK_INT >= 29 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_PAGE_RIGHT : null, 16908361, null, null, null);
            new a(Build.VERSION.SDK_INT >= 23 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_CONTEXT_CLICK : null, 16908348, null, null, null);
            new a(Build.VERSION.SDK_INT >= 24 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SET_PROGRESS : null, 16908349, null, null, ra.f.class);
            new a(Build.VERSION.SDK_INT >= 26 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_MOVE_WINDOW : null, 16908354, null, null, ra.d.class);
            new a(Build.VERSION.SDK_INT >= 28 ? AccessibilityNodeInfo.AccessibilityAction.ACTION_SHOW_TOOLTIP : null, 16908356, null, null, null);
            if (Build.VERSION.SDK_INT >= 28) {
                accessibilityAction = AccessibilityNodeInfo.AccessibilityAction.ACTION_HIDE_TOOLTIP;
            }
            new a(accessibilityAction, 16908357, null, null, null);
        }
        */

        @DexIgnore
        public a(int i2, CharSequence charSequence) {
            this(null, i2, charSequence, null, null);
        }

        @DexIgnore
        public int a() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getId();
            }
            return 0;
        }

        @DexIgnore
        public CharSequence b() {
            if (Build.VERSION.SDK_INT >= 21) {
                return ((AccessibilityNodeInfo.AccessibilityAction) this.a).getLabel();
            }
            return null;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            Object obj2 = this.a;
            if (obj2 == null) {
                if (aVar.a != null) {
                    return false;
                }
                return true;
            } else if (!obj2.equals(aVar.a)) {
                return false;
            } else {
                return true;
            }
        }

        @DexIgnore
        public int hashCode() {
            Object obj = this.a;
            if (obj != null) {
                return obj.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public a(Object obj) {
            this(obj, 0, null, null, null);
        }

        @DexIgnore
        public a(int i2, CharSequence charSequence, Class<? extends ra.a> cls) {
            this(null, i2, charSequence, null, cls);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0025  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0028  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(android.view.View r5, android.os.Bundle r6) {
            /*
                r4 = this;
                com.fossil.ra r0 = r4.d
                r1 = 0
                if (r0 == 0) goto L_0x0049
                r0 = 0
                java.lang.Class<? extends com.fossil.ra$a> r2 = r4.c
                if (r2 == 0) goto L_0x0042
                java.lang.Class[] r3 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.reflect.Constructor r2 = r2.getDeclaredConstructor(r3)     // Catch:{ Exception -> 0x0020 }
                java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0020 }
                java.lang.Object r1 = r2.newInstance(r1)     // Catch:{ Exception -> 0x0020 }
                com.fossil.ra$a r1 = (com.fossil.ra.a) r1     // Catch:{ Exception -> 0x0020 }
                r1.a(r6)     // Catch:{ Exception -> 0x001d }
                r0 = r1
                goto L_0x0042
            L_0x001d:
                r6 = move-exception
                r0 = r1
                goto L_0x0021
            L_0x0020:
                r6 = move-exception
            L_0x0021:
                java.lang.Class<? extends com.fossil.ra$a> r1 = r4.c
                if (r1 != 0) goto L_0x0028
                java.lang.String r1 = "null"
                goto L_0x002c
            L_0x0028:
                java.lang.String r1 = r1.getName()
            L_0x002c:
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Failed to execute command with argument class ViewCommandArgument: "
                r2.append(r3)
                r2.append(r1)
                java.lang.String r1 = r2.toString()
                java.lang.String r2 = "A11yActionCompat"
                android.util.Log.e(r2, r1, r6)
            L_0x0042:
                com.fossil.ra r6 = r4.d
                boolean r5 = r6.a(r5, r0)
                return r5
            L_0x0049:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.oa.a.a(android.view.View, android.os.Bundle):boolean");
        }

        @DexIgnore
        public a(Object obj, int i2, CharSequence charSequence, ra raVar, Class<? extends ra.a> cls) {
            this.b = i2;
            this.d = raVar;
            if (Build.VERSION.SDK_INT < 21 || obj != null) {
                this.a = obj;
            } else {
                this.a = new AccessibilityNodeInfo.AccessibilityAction(i2, charSequence);
            }
            this.c = cls;
        }

        @DexIgnore
        public a a(CharSequence charSequence, ra raVar) {
            return new a(null, this.b, charSequence, raVar, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public c(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static c a(int i, int i2, int i3, int i4, boolean z, boolean z2) {
            int i5 = Build.VERSION.SDK_INT;
            if (i5 >= 21) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z, z2));
            }
            if (i5 >= 19) {
                return new c(AccessibilityNodeInfo.CollectionItemInfo.obtain(i, i2, i3, i4, z));
            }
            return new c(null);
        }
    }

    @DexIgnore
    public oa(AccessibilityNodeInfo accessibilityNodeInfo) {
        this.a = accessibilityNodeInfo;
    }

    @DexIgnore
    public static oa B() {
        return a(AccessibilityNodeInfo.obtain());
    }

    @DexIgnore
    public static oa a(AccessibilityNodeInfo accessibilityNodeInfo) {
        return new oa(accessibilityNodeInfo);
    }

    @DexIgnore
    public static String d(int i) {
        if (i == 1) {
            return "ACTION_FOCUS";
        }
        if (i == 2) {
            return "ACTION_CLEAR_FOCUS";
        }
        switch (i) {
            case 4:
                return "ACTION_SELECT";
            case 8:
                return "ACTION_CLEAR_SELECTION";
            case 16:
                return "ACTION_CLICK";
            case 32:
                return "ACTION_LONG_CLICK";
            case 64:
                return "ACTION_ACCESSIBILITY_FOCUS";
            case 128:
                return "ACTION_CLEAR_ACCESSIBILITY_FOCUS";
            case 256:
                return "ACTION_NEXT_AT_MOVEMENT_GRANULARITY";
            case 512:
                return "ACTION_PREVIOUS_AT_MOVEMENT_GRANULARITY";
            case 1024:
                return "ACTION_NEXT_HTML_ELEMENT";
            case 2048:
                return "ACTION_PREVIOUS_HTML_ELEMENT";
            case 4096:
                return "ACTION_SCROLL_FORWARD";
            case 8192:
                return "ACTION_SCROLL_BACKWARD";
            case 16384:
                return "ACTION_COPY";
            case 32768:
                return "ACTION_PASTE";
            case 65536:
                return "ACTION_CUT";
            case 131072:
                return "ACTION_SET_SELECTION";
            case 262144:
                return "ACTION_EXPAND";
            case 524288:
                return "ACTION_COLLAPSE";
            case ImageObject.DATA_SIZE /*{ENCODED_INT: 2097152}*/:
                return "ACTION_SET_TEXT";
            case 16908354:
                return "ACTION_MOVE_WINDOW";
            default:
                switch (i) {
                    case AccessibilityBridge.ACTION_SHOW_ON_SCREEN /*{ENCODED_INT: 16908342}*/:
                        return "ACTION_SHOW_ON_SCREEN";
                    case 16908343:
                        return "ACTION_SCROLL_TO_POSITION";
                    case 16908344:
                        return "ACTION_SCROLL_UP";
                    case 16908345:
                        return "ACTION_SCROLL_LEFT";
                    case 16908346:
                        return "ACTION_SCROLL_DOWN";
                    case 16908347:
                        return "ACTION_SCROLL_RIGHT";
                    case 16908348:
                        return "ACTION_CONTEXT_CLICK";
                    case 16908349:
                        return "ACTION_SET_PROGRESS";
                    default:
                        switch (i) {
                            case 16908356:
                                return "ACTION_SHOW_TOOLTIP";
                            case 16908357:
                                return "ACTION_HIDE_TOOLTIP";
                            case 16908358:
                                return "ACTION_PAGE_UP";
                            case 16908359:
                                return "ACTION_PAGE_DOWN";
                            case 16908360:
                                return "ACTION_PAGE_LEFT";
                            case 16908361:
                                return "ACTION_PAGE_RIGHT";
                            default:
                                return "ACTION_UNKNOWN";
                        }
                }
        }
    }

    @DexIgnore
    public static oa g(View view) {
        return a(AccessibilityNodeInfo.obtain(view));
    }

    @DexIgnore
    public AccessibilityNodeInfo A() {
        return this.a;
    }

    @DexIgnore
    public boolean b(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            return this.a.removeAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
        return false;
    }

    @DexIgnore
    public void c(View view, int i) {
        this.c = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setSource(view, i);
        }
    }

    @DexIgnore
    public int d() {
        return this.a.getChildCount();
    }

    @DexIgnore
    public void e(View view) {
        this.b = -1;
        this.a.setParent(view);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof oa)) {
            return false;
        }
        oa oaVar = (oa) obj;
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            if (oaVar.a != null) {
                return false;
            }
        } else if (!accessibilityNodeInfo.equals(oaVar.a)) {
            return false;
        }
        return this.c == oaVar.c && this.b == oaVar.b;
    }

    @DexIgnore
    public void f(View view) {
        this.c = -1;
        this.a.setSource(view);
    }

    @DexIgnore
    public int h() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.getMovementGranularities();
        }
        return 0;
    }

    @DexIgnore
    public int hashCode() {
        AccessibilityNodeInfo accessibilityNodeInfo = this.a;
        if (accessibilityNodeInfo == null) {
            return 0;
        }
        return accessibilityNodeInfo.hashCode();
    }

    @DexIgnore
    public void i(boolean z) {
        this.a.setFocusable(z);
    }

    @DexIgnore
    public void j(boolean z) {
        this.a.setFocused(z);
    }

    @DexIgnore
    public String k() {
        if (Build.VERSION.SDK_INT >= 18) {
            return this.a.getViewIdResourceName();
        }
        return null;
    }

    @DexIgnore
    public void l(boolean z) {
        this.a.setLongClickable(z);
    }

    @DexIgnore
    public boolean m() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isAccessibilityFocused();
        }
        return false;
    }

    @DexIgnore
    public boolean n() {
        return this.a.isCheckable();
    }

    @DexIgnore
    public boolean o() {
        return this.a.isChecked();
    }

    @DexIgnore
    public boolean p() {
        return this.a.isClickable();
    }

    @DexIgnore
    public void q(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setVisibleToUser(z);
        }
    }

    @DexIgnore
    public boolean r() {
        return this.a.isFocusable();
    }

    @DexIgnore
    public boolean s() {
        return this.a.isFocused();
    }

    @DexIgnore
    public boolean t() {
        return this.a.isLongClickable();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        Rect rect = new Rect();
        a(rect);
        sb.append("; boundsInParent: " + rect);
        b(rect);
        sb.append("; boundsInScreen: " + rect);
        sb.append("; packageName: ");
        sb.append(i());
        sb.append("; className: ");
        sb.append(e());
        sb.append("; text: ");
        sb.append(j());
        sb.append("; contentDescription: ");
        sb.append(f());
        sb.append("; viewId: ");
        sb.append(k());
        sb.append("; checkable: ");
        sb.append(n());
        sb.append("; checked: ");
        sb.append(o());
        sb.append("; focusable: ");
        sb.append(r());
        sb.append("; focused: ");
        sb.append(s());
        sb.append("; selected: ");
        sb.append(w());
        sb.append("; clickable: ");
        sb.append(p());
        sb.append("; longClickable: ");
        sb.append(t());
        sb.append("; enabled: ");
        sb.append(q());
        sb.append("; password: ");
        sb.append(u());
        sb.append("; scrollable: " + v());
        sb.append("; [");
        if (Build.VERSION.SDK_INT >= 21) {
            List<a> b2 = b();
            for (int i = 0; i < b2.size(); i++) {
                a aVar = b2.get(i);
                String d2 = d(aVar.a());
                if (d2.equals("ACTION_UNKNOWN") && aVar.b() != null) {
                    d2 = aVar.b().toString();
                }
                sb.append(d2);
                if (i != b2.size() - 1) {
                    sb.append(", ");
                }
            }
        } else {
            int c2 = c();
            while (c2 != 0) {
                int numberOfTrailingZeros = 1 << Integer.numberOfTrailingZeros(c2);
                c2 &= ~numberOfTrailingZeros;
                sb.append(d(numberOfTrailingZeros));
                if (c2 != 0) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public boolean u() {
        return this.a.isPassword();
    }

    @DexIgnore
    public boolean v() {
        return this.a.isScrollable();
    }

    @DexIgnore
    public boolean w() {
        return this.a.isSelected();
    }

    @DexIgnore
    public boolean x() {
        if (Build.VERSION.SDK_INT >= 26) {
            return this.a.isShowingHintText();
        }
        return b(4);
    }

    @DexIgnore
    public boolean y() {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.isVisibleToUser();
        }
        return false;
    }

    @DexIgnore
    public void z() {
        this.a.recycle();
    }

    @DexIgnore
    public static oa a(oa oaVar) {
        return a(AccessibilityNodeInfo.obtain(oaVar.a));
    }

    @DexIgnore
    public void d(Rect rect) {
        this.a.setBoundsInScreen(rect);
    }

    @DexIgnore
    public void g(CharSequence charSequence) {
        this.a.setText(charSequence);
    }

    @DexIgnore
    public CharSequence i() {
        return this.a.getPackageName();
    }

    @DexIgnore
    public CharSequence j() {
        if (!l()) {
            return this.a.getText();
        }
        List<Integer> a2 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
        List<Integer> a3 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
        List<Integer> a4 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
        List<Integer> a5 = a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        SpannableString spannableString = new SpannableString(TextUtils.substring(this.a.getText(), 0, this.a.getText().length()));
        for (int i = 0; i < a2.size(); i++) {
            spannableString.setSpan(new ma(a5.get(i).intValue(), this, g().getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY")), a2.get(i).intValue(), a3.get(i).intValue(), a4.get(i).intValue());
        }
        return spannableString;
    }

    @DexIgnore
    public final boolean l() {
        return !a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").isEmpty();
    }

    @DexIgnore
    public void n(boolean z) {
        this.a.setScrollable(z);
    }

    @DexIgnore
    public void o(boolean z) {
        this.a.setSelected(z);
    }

    @DexIgnore
    public void p(boolean z) {
        if (Build.VERSION.SDK_INT >= 26) {
            this.a.setShowingHintText(z);
        } else {
            a(4, z);
        }
    }

    @DexIgnore
    public void a(View view) {
        this.a.addChild(view);
    }

    @DexIgnore
    public void b(View view, int i) {
        this.b = i;
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setParent(view, i);
        }
    }

    @DexIgnore
    public void d(boolean z) {
        this.a.setChecked(z);
    }

    @DexIgnore
    public void e(boolean z) {
        this.a.setClickable(z);
    }

    @DexIgnore
    public CharSequence f() {
        return this.a.getContentDescription();
    }

    @DexIgnore
    public Bundle g() {
        if (Build.VERSION.SDK_INT >= 19) {
            return this.a.getExtras();
        }
        return new Bundle();
    }

    @DexIgnore
    public void h(boolean z) {
        this.a.setEnabled(z);
    }

    @DexIgnore
    public void k(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setHeading(z);
        } else {
            a(2, z);
        }
    }

    @DexIgnore
    public void m(boolean z) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a.setScreenReaderFocusable(z);
        } else {
            a(1, z);
        }
    }

    @DexIgnore
    public boolean q() {
        return this.a.isEnabled();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static b a(int i, int i2, boolean z, int i3) {
            int i4 = Build.VERSION.SDK_INT;
            if (i4 >= 21) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z, i3));
            }
            if (i4 >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new b(null);
        }

        @DexIgnore
        public static b a(int i, int i2, boolean z) {
            if (Build.VERSION.SDK_INT >= 19) {
                return new b(AccessibilityNodeInfo.CollectionInfo.obtain(i, i2, z));
            }
            return new b(null);
        }
    }

    @DexIgnore
    public static ClickableSpan[] h(CharSequence charSequence) {
        if (charSequence instanceof Spanned) {
            return (ClickableSpan[]) ((Spanned) charSequence).getSpans(0, charSequence.length(), ClickableSpan.class);
        }
        return null;
    }

    @DexIgnore
    public void a(View view, int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.addChild(view, i);
        }
    }

    @DexIgnore
    public int c() {
        return this.a.getActions();
    }

    @DexIgnore
    public final void d(View view) {
        SparseArray<WeakReference<ClickableSpan>> c2 = c(view);
        if (c2 != null) {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < c2.size(); i++) {
                if (c2.valueAt(i).get() == null) {
                    arrayList.add(Integer.valueOf(i));
                }
            }
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                c2.remove(((Integer) arrayList.get(i2)).intValue());
            }
        }
    }

    @DexIgnore
    public void e(CharSequence charSequence) {
        this.a.setPackageName(charSequence);
    }

    @DexIgnore
    public void f(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setContentInvalid(z);
        }
    }

    @DexIgnore
    public void c(int i) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setMovementGranularities(i);
        }
    }

    @DexIgnore
    public CharSequence e() {
        return this.a.getClassName();
    }

    @DexIgnore
    public void a(int i) {
        this.a.addAction(i);
    }

    @DexIgnore
    public void b(Rect rect) {
        this.a.getBoundsInScreen(rect);
    }

    @DexIgnore
    public void f(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            this.a.setPaneTitle(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.PANE_TITLE_KEY", charSequence);
        }
    }

    @DexIgnore
    public void g(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setDismissable(z);
        }
    }

    @DexIgnore
    public final List<Integer> a(String str) {
        if (Build.VERSION.SDK_INT < 19) {
            return new ArrayList();
        }
        ArrayList<Integer> integerArrayList = this.a.getExtras().getIntegerArrayList(str);
        if (integerArrayList != null) {
            return integerArrayList;
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        this.a.getExtras().putIntegerArrayList(str, arrayList);
        return arrayList;
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> b(View view) {
        SparseArray<WeakReference<ClickableSpan>> c2 = c(view);
        if (c2 != null) {
            return c2;
        }
        SparseArray<WeakReference<ClickableSpan>> sparseArray = new SparseArray<>();
        view.setTag(e6.tag_accessibility_clickable_spans, sparseArray);
        return sparseArray;
    }

    @DexIgnore
    @Deprecated
    public void c(Rect rect) {
        this.a.setBoundsInParent(rect);
    }

    @DexIgnore
    public void c(boolean z) {
        this.a.setCheckable(z);
    }

    @DexIgnore
    public final SparseArray<WeakReference<ClickableSpan>> c(View view) {
        return (SparseArray) view.getTag(e6.tag_accessibility_clickable_spans);
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.a.setContentDescription(charSequence);
    }

    @DexIgnore
    public void c(CharSequence charSequence) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.setError(charSequence);
        }
    }

    @DexIgnore
    public void b(Object obj) {
        AccessibilityNodeInfo.CollectionItemInfo collectionItemInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            AccessibilityNodeInfo accessibilityNodeInfo = this.a;
            if (obj == null) {
                collectionItemInfo = null;
            } else {
                collectionItemInfo = (AccessibilityNodeInfo.CollectionItemInfo) ((c) obj).a;
            }
            accessibilityNodeInfo.setCollectionItemInfo(collectionItemInfo);
        }
    }

    @DexIgnore
    public void d(CharSequence charSequence) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.a.setHintText(charSequence);
        } else if (i >= 19) {
            this.a.getExtras().putCharSequence("androidx.view.accessibility.AccessibilityNodeInfoCompat.HINT_TEXT_KEY", charSequence);
        }
    }

    @DexIgnore
    public void a(a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.a.addAction((AccessibilityNodeInfo.AccessibilityAction) aVar.a);
        }
    }

    @DexIgnore
    public boolean a(int i, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 16) {
            return this.a.performAction(i, bundle);
        }
        return false;
    }

    @DexIgnore
    public List<a> b() {
        List<AccessibilityNodeInfo.AccessibilityAction> actionList = Build.VERSION.SDK_INT >= 21 ? this.a.getActionList() : null;
        if (actionList == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        int size = actionList.size();
        for (int i = 0; i < size; i++) {
            arrayList.add(new a(actionList.get(i)));
        }
        return arrayList;
    }

    @DexIgnore
    @Deprecated
    public void a(Rect rect) {
        this.a.getBoundsInParent(rect);
    }

    @DexIgnore
    public void a(boolean z) {
        if (Build.VERSION.SDK_INT >= 16) {
            this.a.setAccessibilityFocused(z);
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.a.setClassName(charSequence);
    }

    @DexIgnore
    public void a(CharSequence charSequence, View view) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 19 && i < 26) {
            a();
            d(view);
            ClickableSpan[] h = h(charSequence);
            if (h != null && h.length > 0) {
                g().putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ACTION_ID_KEY", e6.accessibility_action_clickable_span);
                SparseArray<WeakReference<ClickableSpan>> b2 = b(view);
                int i2 = 0;
                while (h != null && i2 < h.length) {
                    int a2 = a(h[i2], b2);
                    b2.put(a2, new WeakReference<>(h[i2]));
                    a(h[i2], (Spanned) charSequence, a2);
                    i2++;
                }
            }
        }
    }

    @DexIgnore
    public void b(boolean z) {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.setCanOpenPopup(z);
        }
    }

    @DexIgnore
    public final boolean b(int i) {
        Bundle g = g();
        if (g != null && (g.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & i) == i) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final int a(ClickableSpan clickableSpan, SparseArray<WeakReference<ClickableSpan>> sparseArray) {
        if (sparseArray != null) {
            for (int i = 0; i < sparseArray.size(); i++) {
                if (clickableSpan.equals(sparseArray.valueAt(i).get())) {
                    return sparseArray.keyAt(i);
                }
            }
        }
        int i2 = d;
        d = i2 + 1;
        return i2;
    }

    @DexIgnore
    public final void a() {
        if (Build.VERSION.SDK_INT >= 19) {
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY");
            this.a.getExtras().remove("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY");
        }
    }

    @DexIgnore
    public final void a(ClickableSpan clickableSpan, Spanned spanned, int i) {
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_START_KEY").add(Integer.valueOf(spanned.getSpanStart(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_END_KEY").add(Integer.valueOf(spanned.getSpanEnd(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_FLAGS_KEY").add(Integer.valueOf(spanned.getSpanFlags(clickableSpan)));
        a("androidx.view.accessibility.AccessibilityNodeInfoCompat.SPANS_ID_KEY").add(Integer.valueOf(i));
    }

    @DexIgnore
    public void a(Object obj) {
        AccessibilityNodeInfo.CollectionInfo collectionInfo;
        if (Build.VERSION.SDK_INT >= 19) {
            AccessibilityNodeInfo accessibilityNodeInfo = this.a;
            if (obj == null) {
                collectionInfo = null;
            } else {
                collectionInfo = (AccessibilityNodeInfo.CollectionInfo) ((b) obj).a;
            }
            accessibilityNodeInfo.setCollectionInfo(collectionInfo);
        }
    }

    @DexIgnore
    public final void a(int i, boolean z) {
        Bundle g = g();
        if (g != null) {
            int i2 = g.getInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", 0) & (~i);
            if (!z) {
                i = 0;
            }
            g.putInt("androidx.view.accessibility.AccessibilityNodeInfoCompat.BOOLEAN_PROPERTY_KEY", i | i2);
        }
    }
}
