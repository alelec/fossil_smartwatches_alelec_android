package com.fossil;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.bm4;
import com.fossil.cy6;
import com.fossil.pk6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk6 extends go5 implements bm4.a, cy6.g {
    @DexIgnore
    public static String p; // = "";
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<m65> g;
    @DexIgnore
    public pk6 h;
    @DexIgnore
    public bm4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final mk6 a() {
            return new mk6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements zd<pk6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ mk6 a;

        @DexIgnore
        public b(mk6 mk6) {
            this.a = mk6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pk6.b bVar) {
            FragmentActivity activity;
            ArrayList<Theme> c;
            if (!(bVar == null || (c = bVar.c()) == null)) {
                this.a.d(c);
                mk6 mk6 = this.a;
                String e = bVar.e();
                if (e != null) {
                    String a2 = bVar.a();
                    if (a2 != null) {
                        mk6.a(e, a2, bVar.d());
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            if (bVar.b() && (activity = this.a.getActivity()) != null) {
                HomeActivity.a aVar = HomeActivity.z;
                ee7.a((Object) activity, "it");
                HomeActivity.a.a(aVar, activity, null, 2, null);
                this.a.v();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mk6 a;

        @DexIgnore
        public c(mk6 mk6) {
            this.a = mk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.v();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mk6 a;

        @DexIgnore
        public d(mk6 mk6) {
            this.a = mk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            mk6.a(this.a).a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mk6 a;

        @DexIgnore
        public e(mk6 mk6) {
            this.a = mk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                String valueOf = String.valueOf(System.currentTimeMillis());
                eh5.l.a().a(valueOf);
                UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.y;
                ee7.a((Object) activity, "it");
                aVar.a(activity, valueOf, false);
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ pk6 a(mk6 mk6) {
        pk6 pk6 = mk6.h;
        if (pk6 != null) {
            return pk6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.bm4.a
    public void F(String str) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserDeleteTheme id=" + str);
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.h(childFragmentManager);
        p = str;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void d(ArrayList<Theme> arrayList) {
        ee7.b(arrayList, "listTheme");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "showListTheme size=" + arrayList.size());
        bm4 bm4 = this.i;
        if (bm4 != null) {
            bm4.a(arrayList);
        }
    }

    @DexIgnore
    @Override // com.fossil.bm4.a
    public void j(String str) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserEditTheme id=" + str);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            UserCustomizeThemeActivity.a aVar = UserCustomizeThemeActivity.y;
            ee7.a((Object) activity, "it");
            aVar.a(activity, str, true);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        m65 m65 = (m65) qb.a(LayoutInflater.from(getContext()), 2131558628, null, false, a1());
        this.i = new bm4(new ArrayList(), this);
        PortfolioApp.g0.c().f().a(new ok6()).a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(pk6.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026mesViewModel::class.java)");
            pk6 pk6 = (pk6) a2;
            this.h = pk6;
            if (pk6 != null) {
                pk6.c().a(getViewLifecycleOwner(), new b(this));
                pk6 pk62 = this.h;
                if (pk62 != null) {
                    pk62.d();
                    this.g = new qw6<>(this, m65);
                    ee7.a((Object) m65, "binding");
                    return m65.d();
                }
                ee7.d("mViewModel");
                throw null;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pk6 pk6 = this.h;
        if (pk6 != null) {
            pk6.d();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<m65> qw6 = this.g;
        if (qw6 != null) {
            m65 a2 = qw6.a();
            if (a2 != null) {
                a2.q.setOnClickListener(new c(this));
                a2.t.setOnClickListener(new d(this));
                a2.u.setOnClickListener(new e(this));
                RecyclerView recyclerView = a2.s;
                ee7.a((Object) recyclerView, "binding.rvThemes");
                recyclerView.setAdapter(this.i);
                RecyclerView recyclerView2 = a2.s;
                ee7.a((Object) recyclerView2, "binding.rvThemes");
                recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView3 = a2.s;
                ee7.a((Object) recyclerView3, "binding.rvThemes");
                recyclerView3.setNestedScrollingEnabled(false);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.bm4.a
    public void u(String str) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ThemesFragment", "onUserChooseTheme id=" + str);
        pk6 pk6 = this.h;
        if (pk6 != null) {
            pk6.b(str);
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void a(String str, String str2, List<Style> list) {
        ee7.b(str, "userSelectedThemeId");
        ee7.b(str2, "currentThemeId");
        qw6<m65> qw6 = this.g;
        if (qw6 != null) {
            m65 a2 = qw6.a();
            if (a2 != null && list != null) {
                bm4 bm4 = this.i;
                if (bm4 != null) {
                    bm4.a(list, str);
                }
                String a3 = eh5.l.a().a(Explore.COLUMN_BACKGROUND, list);
                String a4 = eh5.l.a().a("primaryText", list);
                Typeface b2 = eh5.l.a().b("headline2", list);
                if (a3 != null) {
                    a2.r.setBackgroundColor(Color.parseColor(a3));
                }
                if (a4 != null) {
                    a2.v.setTextColor(Color.parseColor(a4));
                    a2.q.setColorFilter(Color.parseColor(a4));
                }
                if (b2 != null) {
                    FlexibleTextView flexibleTextView = a2.v;
                    ee7.a((Object) flexibleTextView, "it.tvTitle");
                    flexibleTextView.setTypeface(b2);
                }
                String a5 = eh5.l.a().a("onPrimaryButton", list);
                Typeface b3 = eh5.l.a().b("textButtonPrimary", list);
                String a6 = eh5.l.a().a("primaryButton", list);
                if (a5 != null) {
                    a2.t.setTextColor(Color.parseColor(a5));
                    a2.u.setTextColor(Color.parseColor(a5));
                }
                if (b3 != null) {
                    FlexibleButton flexibleButton = a2.t;
                    ee7.a((Object) flexibleButton, "it.tvApply");
                    flexibleButton.setTypeface(b3);
                    FlexibleButton flexibleButton2 = a2.u;
                    ee7.a((Object) flexibleButton2, "it.tvCreateNewTheme");
                    flexibleButton2.setTypeface(b3);
                }
                if (a6 != null) {
                    a2.t.setBackgroundColor(Color.parseColor(a6));
                    a2.u.setBackgroundColor(Color.parseColor(a6));
                    return;
                }
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        FLogger.INSTANCE.getLocal().d("ThemesFragment", "onDialogFragmentResult");
        if (str.hashCode() == -1478349291 && str.equals("DELETE_THEME") && i2 == 2131363307) {
            pk6 pk6 = this.h;
            if (pk6 != null) {
                pk6.a(p);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }
}
