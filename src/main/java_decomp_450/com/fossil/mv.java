package com.fossil;

import com.facebook.share.internal.VideoUploader;
import java.io.IOException;
import java.net.URI;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpOptions;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpTrace;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public class mv implements pv {
    @DexIgnore
    public /* final */ HttpClient a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends HttpEntityEnclosingRequestBase {
        @DexIgnore
        public a(String str) {
            setURI(URI.create(str));
        }

        @DexIgnore
        public String getMethod() {
            return "PATCH";
        }
    }

    @DexIgnore
    public mv(HttpClient httpClient) {
        this.a = httpClient;
    }

    @DexIgnore
    public static void a(HttpUriRequest httpUriRequest, Map<String, String> map) {
        for (String str : map.keySet()) {
            httpUriRequest.setHeader(str, map.get(str));
        }
    }

    @DexIgnore
    public static HttpUriRequest b(yu<?> yuVar, Map<String, String> map) throws lu {
        switch (yuVar.getMethod()) {
            case -1:
                byte[] postBody = yuVar.getPostBody();
                if (postBody == null) {
                    return new HttpGet(yuVar.getUrl());
                }
                HttpPost httpPost = new HttpPost(yuVar.getUrl());
                httpPost.addHeader("Content-Type", yuVar.getPostBodyContentType());
                httpPost.setEntity(new ByteArrayEntity(postBody));
                return httpPost;
            case 0:
                return new HttpGet(yuVar.getUrl());
            case 1:
                HttpPost httpPost2 = new HttpPost(yuVar.getUrl());
                httpPost2.addHeader("Content-Type", yuVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPost2, yuVar);
                return httpPost2;
            case 2:
                HttpPut httpPut = new HttpPut(yuVar.getUrl());
                httpPut.addHeader("Content-Type", yuVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) httpPut, yuVar);
                return httpPut;
            case 3:
                return new HttpDelete(yuVar.getUrl());
            case 4:
                return new HttpHead(yuVar.getUrl());
            case 5:
                return new HttpOptions(yuVar.getUrl());
            case 6:
                return new HttpTrace(yuVar.getUrl());
            case 7:
                a aVar = new a(yuVar.getUrl());
                aVar.addHeader("Content-Type", yuVar.getBodyContentType());
                a((HttpEntityEnclosingRequestBase) aVar, yuVar);
                return aVar;
            default:
                throw new IllegalStateException("Unknown request method.");
        }
    }

    @DexIgnore
    public void a(HttpUriRequest httpUriRequest) throws IOException {
    }

    @DexIgnore
    @Override // com.fossil.pv
    public HttpResponse a(yu<?> yuVar, Map<String, String> map) throws IOException, lu {
        HttpUriRequest b = b(yuVar, map);
        a(b, map);
        a(b, yuVar.getHeaders());
        a(b);
        HttpParams params = b.getParams();
        int timeoutMs = yuVar.getTimeoutMs();
        HttpConnectionParams.setConnectionTimeout(params, VideoUploader.RETRY_DELAY_UNIT_MS);
        HttpConnectionParams.setSoTimeout(params, timeoutMs);
        return this.a.execute(b);
    }

    @DexIgnore
    public static void a(HttpEntityEnclosingRequestBase httpEntityEnclosingRequestBase, yu<?> yuVar) throws lu {
        byte[] body = yuVar.getBody();
        if (body != null) {
            httpEntityEnclosingRequestBase.setEntity(new ByteArrayEntity(body));
        }
    }
}
