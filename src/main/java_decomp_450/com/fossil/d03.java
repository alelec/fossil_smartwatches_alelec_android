package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d03 implements tr2<c03> {
    @DexIgnore
    public static d03 b; // = new d03();
    @DexIgnore
    public /* final */ tr2<c03> a;

    @DexIgnore
    public d03(tr2<c03> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static long A() {
        return ((c03) b.zza()).u();
    }

    @DexIgnore
    public static long B() {
        return ((c03) b.zza()).w();
    }

    @DexIgnore
    public static long C() {
        return ((c03) b.zza()).b();
    }

    @DexIgnore
    public static long D() {
        return ((c03) b.zza()).g();
    }

    @DexIgnore
    public static long E() {
        return ((c03) b.zza()).m();
    }

    @DexIgnore
    public static long F() {
        return ((c03) b.zza()).p();
    }

    @DexIgnore
    public static long G() {
        return ((c03) b.zza()).j();
    }

    @DexIgnore
    public static long a() {
        return ((c03) b.zza()).l();
    }

    @DexIgnore
    public static long b() {
        return ((c03) b.zza()).t();
    }

    @DexIgnore
    public static long c() {
        return ((c03) b.zza()).v();
    }

    @DexIgnore
    public static long d() {
        return ((c03) b.zza()).r();
    }

    @DexIgnore
    public static long e() {
        return ((c03) b.zza()).s();
    }

    @DexIgnore
    public static long f() {
        return ((c03) b.zza()).n();
    }

    @DexIgnore
    public static String g() {
        return ((c03) b.zza()).q();
    }

    @DexIgnore
    public static long h() {
        return ((c03) b.zza()).k();
    }

    @DexIgnore
    public static long i() {
        return ((c03) b.zza()).zza();
    }

    @DexIgnore
    public static long j() {
        return ((c03) b.zza()).zzb();
    }

    @DexIgnore
    public static String k() {
        return ((c03) b.zza()).zzc();
    }

    @DexIgnore
    public static String l() {
        return ((c03) b.zza()).zzd();
    }

    @DexIgnore
    public static long m() {
        return ((c03) b.zza()).zze();
    }

    @DexIgnore
    public static long n() {
        return ((c03) b.zza()).zzf();
    }

    @DexIgnore
    public static long o() {
        return ((c03) b.zza()).zzg();
    }

    @DexIgnore
    public static long p() {
        return ((c03) b.zza()).zzh();
    }

    @DexIgnore
    public static long q() {
        return ((c03) b.zza()).d();
    }

    @DexIgnore
    public static long r() {
        return ((c03) b.zza()).a();
    }

    @DexIgnore
    public static long s() {
        return ((c03) b.zza()).h();
    }

    @DexIgnore
    public static long t() {
        return ((c03) b.zza()).zzl();
    }

    @DexIgnore
    public static long u() {
        return ((c03) b.zza()).zzm();
    }

    @DexIgnore
    public static long v() {
        return ((c03) b.zza()).f();
    }

    @DexIgnore
    public static long w() {
        return ((c03) b.zza()).o();
    }

    @DexIgnore
    public static long x() {
        return ((c03) b.zza()).i();
    }

    @DexIgnore
    public static long y() {
        return ((c03) b.zza()).c();
    }

    @DexIgnore
    public static long z() {
        return ((c03) b.zza()).e();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ c03 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public d03() {
        this(sr2.a(new f03()));
    }
}
