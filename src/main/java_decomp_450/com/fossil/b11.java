package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b11 extends fe7 implements gd7<sz0, Boolean> {
    @DexIgnore
    public static /* final */ b11 a; // = new b11();

    @DexIgnore
    public b11() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public Boolean invoke(sz0 sz0) {
        sz0 sz02 = sz0;
        ay0 ay0 = sz02.c;
        return Boolean.valueOf(ay0 == ay0.s || ay0 == ay0.f || ay0 == ay0.n || sz02.d.c.a == z51.GATT_NULL);
    }
}
