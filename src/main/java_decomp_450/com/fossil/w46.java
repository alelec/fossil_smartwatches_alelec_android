package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w46 extends r46 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ s46 k;
    @DexIgnore
    public /* final */ WatchAppRepository l;
    @DexIgnore
    public /* final */ ch5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ w46 this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w46$a$a")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.w46$a$a  reason: collision with other inner class name */
        public static final class C0227a extends zb7 implements kd7<yi7, fb7<? super List<WatchApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0227a(a aVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0227a aVar = new C0227a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<WatchApp>> fb7) {
                return ((C0227a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return ea7.d((Collection) this.this$0.this$0.l.queryWatchAppByName(this.this$0.$query));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(w46 w46, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = w46;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, this.$query, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                list = new ArrayList();
                if (this.$query.length() > 0) {
                    ti7 a2 = this.this$0.c();
                    C0227a aVar = new C0227a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = list;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                }
                this.this$0.k.b(this.this$0.a(list));
                this.this$0.h = this.$query;
                return i97.a;
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list = (List) obj;
            this.this$0.k.b(this.this$0.a(list));
            this.this$0.h = this.$query;
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1", f = "WatchAppSearchPresenter.kt", l = {40, 43}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ w46 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allSearchedWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends WatchApp>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    WatchAppRepository e = this.this$0.this$0.l;
                    List<String> C = this.this$0.this$0.m.C();
                    ee7.a((Object) C, "sharedPreferencesManager.watchAppSearchedIdsRecent");
                    return e.getWatchAppByIds(C);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w46$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.w46$b$b  reason: collision with other inner class name */
        public static final class C0228b extends zb7 implements kd7<yi7, fb7<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0228b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0228b bVar = new C0228b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends WatchApp>> fb7) {
                return ((C0228b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.l.getAllWatchAppRaw();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(w46 w46, fb7 fb7) {
            super(2, fb7);
            this.this$0 = w46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                C0228b bVar = new C0228b(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, bVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                List list = (List) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.j.clear();
                this.this$0.j.addAll((List) obj);
                this.this$0.i();
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list2 = (List) obj;
            this.this$0.i.clear();
            this.this$0.i.addAll(list2);
            ti7 a4 = this.this$0.c();
            a aVar = new a(this, null);
            this.L$0 = yi7;
            this.L$1 = list2;
            this.label = 2;
            obj = vh7.a(a4, aVar, this);
            if (obj == a2) {
                return a2;
            }
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.i();
            return i97.a;
        }
    }

    @DexIgnore
    public w46(s46 s46, WatchAppRepository watchAppRepository, ch5 ch5) {
        ee7.b(s46, "mView");
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        this.k = s46;
        this.l = watchAppRepository;
        this.m = ch5;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.r46
    public void h() {
        this.h = "";
        this.k.n();
        i();
    }

    @DexIgnore
    public final void i() {
        if (this.j.isEmpty()) {
            this.k.b(a(ea7.d((Collection) this.i)));
        } else {
            this.k.c(a(ea7.d((Collection) this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            s46 s46 = this.k;
            String str = this.h;
            if (str != null) {
                s46.b(str);
                String str2 = this.h;
                if (str2 != null) {
                    a(str2);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public void a(String str, String str2, String str3) {
        ee7.b(str, "watchAppTop");
        ee7.b(str2, "watchAppMiddle");
        ee7.b(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.r46
    public void a(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ik7 unused = xh7.b(e(), null, null, new a(this, str, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.r46
    public void a(WatchApp watchApp) {
        ee7.b(watchApp, "selectedWatchApp");
        List<String> C = this.m.C();
        ee7.a((Object) C, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!C.contains(watchApp.getWatchappId())) {
            C.add(0, watchApp.getWatchappId());
            if (C.size() > 5) {
                C = C.subList(0, 5);
            }
            this.m.d(C);
        }
        this.k.a(watchApp);
    }

    @DexIgnore
    public final List<r87<WatchApp, String>> a(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp watchApp : list) {
            if (!ee7.a((Object) watchApp.getWatchappId(), (Object) "empty")) {
                String watchappId = watchApp.getWatchappId();
                if (ee7.a((Object) watchappId, (Object) this.e)) {
                    arrayList.add(new r87(watchApp, ViewHierarchy.DIMENSION_TOP_KEY));
                } else if (ee7.a((Object) watchappId, (Object) this.f)) {
                    arrayList.add(new r87(watchApp, "middle"));
                } else if (ee7.a((Object) watchappId, (Object) this.g)) {
                    arrayList.add(new r87(watchApp, "bottom"));
                } else {
                    arrayList.add(new r87(watchApp, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Bundle a(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }
}
