package com.fossil;

import com.fossil.gw;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o00 {
    @DexIgnore
    public /* final */ q00 a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Map<Class<?>, C0147a<?>> a; // = new HashMap();

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.o00$a$a")
        /* renamed from: com.fossil.o00$a$a  reason: collision with other inner class name */
        public static class C0147a<Model> {
            @DexIgnore
            public /* final */ List<m00<Model, ?>> a;

            @DexIgnore
            public C0147a(List<m00<Model, ?>> list) {
                this.a = list;
            }
        }

        @DexIgnore
        public void a() {
            this.a.clear();
        }

        @DexIgnore
        public <Model> void a(Class<Model> cls, List<m00<Model, ?>> list) {
            if (this.a.put(cls, new C0147a<>(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @DexIgnore
        public <Model> List<m00<Model, ?>> a(Class<Model> cls) {
            C0147a<?> aVar = this.a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.a;
        }
    }

    @DexIgnore
    public o00(b9<List<Throwable>> b9Var) {
        this(new q00(b9Var));
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, n00<? extends Model, ? extends Data> n00) {
        this.a.a(cls, cls2, n00);
        this.b.a();
    }

    @DexIgnore
    public final synchronized <A> List<m00<A, ?>> b(Class<A> cls) {
        List<m00<A, ?>> a2;
        a2 = this.b.a(cls);
        if (a2 == null) {
            a2 = Collections.unmodifiableList(this.a.a(cls));
            this.b.a(cls, a2);
        }
        return a2;
    }

    @DexIgnore
    public o00(q00 q00) {
        this.b = new a();
        this.a = q00;
    }

    @DexIgnore
    public <A> List<m00<A, ?>> a(A a2) {
        List<m00<A, ?>> b2 = b((Class) b((Object) a2));
        if (!b2.isEmpty()) {
            int size = b2.size();
            List<m00<A, ?>> emptyList = Collections.emptyList();
            boolean z = true;
            for (int i = 0; i < size; i++) {
                m00<A, ?> m00 = b2.get(i);
                if (m00.a(a2)) {
                    if (z) {
                        emptyList = new ArrayList<>(size - i);
                        z = false;
                    }
                    emptyList.add(m00);
                }
            }
            if (!emptyList.isEmpty()) {
                return emptyList;
            }
            throw new gw.c(a2, b2);
        }
        throw new gw.c(a2);
    }

    @DexIgnore
    public static <A> Class<A> b(A a2) {
        return (Class<A>) a2.getClass();
    }

    @DexIgnore
    public synchronized List<Class<?>> a(Class<?> cls) {
        return this.a.b(cls);
    }
}
