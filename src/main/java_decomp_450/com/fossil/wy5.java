package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wy5 {
    @DexIgnore
    public /* final */ uy5 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ ArrayList<bt5> c;

    @DexIgnore
    public wy5(uy5 uy5, int i, ArrayList<bt5> arrayList) {
        ee7.b(uy5, "mView");
        this.a = uy5;
        this.b = i;
        this.c = arrayList;
    }

    @DexIgnore
    public final ArrayList<bt5> a() {
        ArrayList<bt5> arrayList = this.c;
        if (arrayList != null) {
            return arrayList;
        }
        return new ArrayList<>();
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final uy5 c() {
        return this.a;
    }
}
