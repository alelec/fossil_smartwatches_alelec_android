package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class tc4 implements ho3 {
    @DexIgnore
    public /* final */ uc4 a;
    @DexIgnore
    public /* final */ Intent b;

    @DexIgnore
    public tc4(uc4 uc4, Intent intent) {
        this.a = uc4;
        this.b = intent;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3 no3) {
        this.a.a(this.b, no3);
    }
}
