package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import com.google.firebase.iid.FirebaseInstanceId;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fb4 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ Map<String, Long> c; // = new n4();

    @DexIgnore
    public fb4(Context context) {
        this.b = context;
        this.a = context.getSharedPreferences("com.google.android.gms.appid", 0);
        a("com.google.android.gms.appid-no-backup");
    }

    @DexIgnore
    public final void a(String str) {
        File file = new File(v6.c(this.b), str);
        if (!file.exists()) {
            try {
                if (file.createNewFile() && !b()) {
                    Log.i("FirebaseInstanceId", "App restored, clearing state");
                    a();
                    FirebaseInstanceId.p().m();
                }
            } catch (IOException e) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(e.getMessage());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Error creating file in no backup dir: ".concat(valueOf) : new String("Error creating file in no backup dir: "));
                }
            }
        }
    }

    @DexIgnore
    public synchronized boolean b() {
        return this.a.getAll().isEmpty();
    }

    @DexIgnore
    public synchronized void c(String str) {
        b(String.valueOf(str).concat("|T|"));
    }

    @DexIgnore
    public final long d(String str) {
        String string = this.a.getString(a(str, "cre"), null);
        if (string == null) {
            return 0;
        }
        try {
            return Long.parseLong(string);
        } catch (NumberFormatException unused) {
            return 0;
        }
    }

    @DexIgnore
    public synchronized long e(String str) {
        long f;
        f = f(str);
        this.c.put(str, Long.valueOf(f));
        return f;
    }

    @DexIgnore
    public final long f(String str) {
        long currentTimeMillis = System.currentTimeMillis();
        if (this.a.contains(a(str, "cre"))) {
            return d(str);
        }
        SharedPreferences.Editor edit = this.a.edit();
        edit.putString(a(str, "cre"), String.valueOf(currentTimeMillis));
        edit.commit();
        return currentTimeMillis;
    }

    @DexIgnore
    public final void b(String str) {
        SharedPreferences.Editor edit = this.a.edit();
        for (String str2 : this.a.getAll().keySet()) {
            if (str2.startsWith(str)) {
                edit.remove(str2);
            }
        }
        edit.commit();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ long d; // = TimeUnit.DAYS.toMillis(7);
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public a(String str, String str2, long j) {
            this.a = str;
            this.b = str2;
            this.c = j;
        }

        @DexIgnore
        public static String a(String str, String str2, long j) {
            try {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("token", str);
                jSONObject.put("appVersion", str2);
                jSONObject.put("timestamp", j);
                return jSONObject.toString();
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 24);
                sb.append("Failed to encode token: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                return null;
            }
        }

        @DexIgnore
        public static a b(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            if (!str.startsWith("{")) {
                return new a(str, null, 0);
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                return new a(jSONObject.getString("token"), jSONObject.getString("appVersion"), jSONObject.getLong("timestamp"));
            } catch (JSONException e) {
                String valueOf = String.valueOf(e);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
                sb.append("Failed to parse token: ");
                sb.append(valueOf);
                Log.w("FirebaseInstanceId", sb.toString());
                return null;
            }
        }

        @DexIgnore
        public boolean a(String str) {
            return System.currentTimeMillis() > this.c + d || !str.equals(this.b);
        }
    }

    @DexIgnore
    public synchronized a b(String str, String str2, String str3) {
        return a.b(this.a.getString(a(str, str2, str3), null));
    }

    @DexIgnore
    public final String a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 4 + String.valueOf(str2).length() + String.valueOf(str3).length());
        sb.append(str);
        sb.append("|T|");
        sb.append(str2);
        sb.append("|");
        sb.append(str3);
        return sb.toString();
    }

    @DexIgnore
    public static String a(String str, String str2) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 3 + String.valueOf(str2).length());
        sb.append(str);
        sb.append("|S|");
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public synchronized void a() {
        this.c.clear();
        this.a.edit().clear().commit();
    }

    @DexIgnore
    public synchronized void a(String str, String str2, String str3, String str4, String str5) {
        String a2 = a.a(str4, str5, System.currentTimeMillis());
        if (a2 != null) {
            SharedPreferences.Editor edit = this.a.edit();
            edit.putString(a(str, str2, str3), a2);
            edit.commit();
        }
    }
}
