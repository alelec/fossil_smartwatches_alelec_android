package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;
import java.util.Comparator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg6 {
    @DexIgnore
    public static /* final */ tg6 a; // = new tg6();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartTime().getMillis()), Long.valueOf(t2.getStartTime().getMillis()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getTrackedAt().getMillis()), Long.valueOf(t2.getTrackedAt().getMillis()));
        }
    }

    @DexIgnore
    public final String a(long j, int i, boolean z) {
        boolean z2;
        String str;
        int hourOfDay = new DateTime(j, DateTimeZone.forOffsetMillis(i * 1000)).getHourOfDay();
        if (hourOfDay < 12) {
            if (hourOfDay == 0) {
                hourOfDay = 12;
            }
            z2 = true;
        } else {
            if (hourOfDay != 12) {
                hourOfDay -= 12;
            }
            z2 = false;
        }
        String a2 = ig5.a(PortfolioApp.g0.c(), z2 ? 2131887514 : 2131887516);
        if (i >= 0) {
            str = '+' + re5.a(((float) i) / 3600.0f, 1);
        } else {
            str = re5.a(((float) i) / 3600.0f, 1);
        }
        if (z) {
            we7 we7 = we7.a;
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131887517);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026ce, R.string.s_time_zone)");
            we7 we72 = we7.a;
            ee7.a((Object) a2, "amPmRes");
            String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(hourOfDay)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            String format2 = String.format(a3, Arrays.copyOf(new Object[]{format, str}, 2));
            ee7.a((Object) format2, "java.lang.String.format(format, *args)");
            return format2;
        } else if (hourOfDay % 6 != 0) {
            return "";
        } else {
            we7 we73 = we7.a;
            ee7.a((Object) a2, "amPmRes");
            String format3 = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(hourOfDay)}, 1));
            ee7.a((Object) format3, "java.lang.String.format(format, *args)");
            FLogger.INSTANCE.getLocal().d("SupportedFunction", "temp=" + format3);
            int hashCode = format3.hashCode();
            if (hashCode != 1771) {
                if (hashCode != 1786) {
                    if (hashCode != 48736) {
                        if (hashCode != 48751 || !format3.equals("12p")) {
                            return format3;
                        }
                        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886623);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Label__12p)");
                        return a4;
                    } else if (!format3.equals("12a")) {
                        return format3;
                    } else {
                        String a5 = ig5.a(PortfolioApp.g0.c(), 2131886621);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026in_StepsToday_Label__12a)");
                        return a5;
                    }
                } else if (!format3.equals("6p")) {
                    return format3;
                } else {
                    String a6 = ig5.a(PortfolioApp.g0.c(), 2131886625);
                    ee7.a((Object) a6, "LanguageHelper.getString\u2026ain_StepsToday_Label__6p)");
                    return a6;
                }
            } else if (!format3.equals("6a")) {
                return format3;
            } else {
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886624);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026ain_StepsToday_Label__6a)");
                return a7;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x02a0  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x03e7  */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x03e8  */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x0488  */
    /* JADX WARNING: Removed duplicated region for block: B:204:0x04aa  */
    /* JADX WARNING: Removed duplicated region for block: B:224:0x013e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x0250 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x02a3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x0377 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0447 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x024d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.fossil.r87<java.util.ArrayList<com.portfolio.platform.ui.view.chart.base.BarChart.a>, java.util.ArrayList<java.lang.String>> a(java.util.Date r40, java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySample> r41, int r42) {
        /*
            r39 = this;
            r1 = r39
            r0 = r40
            r2 = r42
            monitor-enter(r39)
            java.lang.String r3 = "date"
            com.fossil.ee7.b(r0, r3)     // Catch:{ all -> 0x0585 }
            if (r41 == 0) goto L_0x0013
            java.util.List r4 = com.fossil.ea7.d(r41)     // Catch:{ all -> 0x0585 }
            goto L_0x0014
        L_0x0013:
            r4 = 0
        L_0x0014:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0585 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()     // Catch:{ all -> 0x0585 }
            java.lang.String r6 = "SupportedFunction"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0585 }
            r7.<init>()     // Catch:{ all -> 0x0585 }
            java.lang.String r8 = "transferActivitySamplesToDetailChart - date="
            r7.append(r8)     // Catch:{ all -> 0x0585 }
            r7.append(r0)     // Catch:{ all -> 0x0585 }
            java.lang.String r8 = ", copyOfActivitySample="
            r7.append(r8)     // Catch:{ all -> 0x0585 }
            if (r4 == 0) goto L_0x0039
            int r8 = r4.size()     // Catch:{ all -> 0x0585 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0585 }
            goto L_0x003a
        L_0x0039:
            r8 = 0
        L_0x003a:
            r7.append(r8)     // Catch:{ all -> 0x0585 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0585 }
            r5.d(r6, r7)     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r5.<init>()     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r6.<init>()     // Catch:{ all -> 0x0585 }
            java.util.HashSet r7 = new java.util.HashSet     // Catch:{ all -> 0x0585 }
            r7.<init>()     // Catch:{ all -> 0x0585 }
            java.util.Calendar r8 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x0585 }
            r9 = 11
            int r8 = r8.get(r9)     // Catch:{ all -> 0x0585 }
            java.lang.Boolean r9 = com.fossil.zd5.w(r40)     // Catch:{ all -> 0x0585 }
            r11 = 1
            if (r4 == 0) goto L_0x052f
            boolean r12 = r4.isEmpty()     // Catch:{ all -> 0x0585 }
            r12 = r12 ^ r11
            if (r12 != r11) goto L_0x052f
            int r12 = r4.size()     // Catch:{ all -> 0x0585 }
            if (r12 <= r11) goto L_0x0079
            com.fossil.tg6$a r12 = new com.fossil.tg6$a     // Catch:{ all -> 0x0585 }
            r12.<init>()     // Catch:{ all -> 0x0585 }
            com.fossil.aa7.a(r4, r12)     // Catch:{ all -> 0x0585 }
        L_0x0079:
            java.util.Iterator r12 = r4.iterator()     // Catch:{ all -> 0x0585 }
        L_0x007d:
            boolean r13 = r12.hasNext()     // Catch:{ all -> 0x0585 }
            if (r13 == 0) goto L_0x0095
            java.lang.Object r13 = r12.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r13 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r13     // Catch:{ all -> 0x0585 }
            int r13 = r13.getTimeZoneOffsetInSecond()     // Catch:{ all -> 0x0585 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0585 }
            r7.add(r13)     // Catch:{ all -> 0x0585 }
            goto L_0x007d
        L_0x0095:
            java.lang.Object r12 = com.fossil.ea7.d(r4)     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r12 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r12     // Catch:{ all -> 0x0585 }
            int r12 = r12.getTimeZoneOffsetInSecond()     // Catch:{ all -> 0x0585 }
            java.lang.Object r13 = com.fossil.ea7.f(r4)     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r13 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r13     // Catch:{ all -> 0x0585 }
            int r13 = r13.getTimeZoneOffsetInSecond()     // Catch:{ all -> 0x0585 }
            java.util.TimeZone r14 = com.fossil.zd5.a(r12)     // Catch:{ all -> 0x0585 }
            java.util.Date r14 = com.fossil.zd5.d(r0, r14)     // Catch:{ all -> 0x0585 }
            java.util.TimeZone r13 = com.fossil.zd5.a(r13)     // Catch:{ all -> 0x0585 }
            java.util.Date r0 = com.fossil.zd5.c(r0, r13)     // Catch:{ all -> 0x0585 }
            java.lang.String r13 = "startDate"
            com.fossil.ee7.a(r14, r13)     // Catch:{ all -> 0x0585 }
            long r13 = r14.getTime()     // Catch:{ all -> 0x0585 }
            java.lang.String r15 = "endDate"
            com.fossil.ee7.a(r0, r15)     // Catch:{ all -> 0x0585 }
            long r15 = r0.getTime()     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r0.<init>()     // Catch:{ all -> 0x0585 }
            r0.addAll(r4)     // Catch:{ all -> 0x0585 }
            java.util.TimeZone r4 = java.util.TimeZone.getDefault()     // Catch:{ all -> 0x0585 }
            java.lang.String r3 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r4, r3)     // Catch:{ all -> 0x0585 }
            java.lang.String r3 = r4.getID()     // Catch:{ all -> 0x0585 }
            int r3 = com.fossil.zd5.a(r3, r11)     // Catch:{ all -> 0x0585 }
            int r4 = r7.size()     // Catch:{ all -> 0x0585 }
            r18 = 0
            if (r4 > r11) goto L_0x0211
            if (r12 == r3) goto L_0x00f0
            r3 = 1
            goto L_0x00f1
        L_0x00f0:
            r3 = 0
        L_0x00f1:
            java.lang.String r3 = r1.a(r13, r12, r3)     // Catch:{ all -> 0x0585 }
            r6.add(r3)     // Catch:{ all -> 0x0585 }
            r3 = 0
        L_0x00f9:
            int r4 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r4 > 0) goto L_0x0562
            r20 = 3600000(0x36ee80, double:1.7786363E-317)
            long r10 = r13 + r20
            r7 = 0
            java.lang.String r4 = r1.a(r10, r12, r7)     // Catch:{ all -> 0x0585 }
            r6.add(r4)     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r7.<init>()     // Catch:{ all -> 0x0585 }
            java.util.Iterator r4 = r0.iterator()     // Catch:{ all -> 0x0585 }
        L_0x0113:
            boolean r20 = r4.hasNext()     // Catch:{ all -> 0x0585 }
            if (r20 == 0) goto L_0x0141
            r20 = r12
            java.lang.Object r12 = r4.next()     // Catch:{ all -> 0x0585 }
            r21 = r12
            com.portfolio.platform.data.model.room.fitness.ActivitySample r21 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r21     // Catch:{ all -> 0x0585 }
            org.joda.time.DateTime r21 = r21.getStartTime()     // Catch:{ all -> 0x0585 }
            long r21 = r21.getMillis()     // Catch:{ all -> 0x0585 }
            int r23 = (r13 > r21 ? 1 : (r13 == r21 ? 0 : -1))
            if (r23 <= 0) goto L_0x0130
            goto L_0x0137
        L_0x0130:
            int r23 = (r10 > r21 ? 1 : (r10 == r21 ? 0 : -1))
            if (r23 <= 0) goto L_0x0137
            r21 = 1
            goto L_0x0139
        L_0x0137:
            r21 = 0
        L_0x0139:
            if (r21 == 0) goto L_0x013e
            r7.add(r12)     // Catch:{ all -> 0x0585 }
        L_0x013e:
            r12 = r20
            goto L_0x0113
        L_0x0141:
            r20 = r12
            r0.removeAll(r7)     // Catch:{ all -> 0x0585 }
            if (r2 == 0) goto L_0x01a0
            r4 = 1
            if (r2 == r4) goto L_0x0180
            r12 = 2
            if (r2 == r12) goto L_0x0167
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r12 = r18
        L_0x0154:
            boolean r14 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r14 == 0) goto L_0x019d
            java.lang.Object r14 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r14 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r14     // Catch:{ all -> 0x0585 }
            double r21 = r14.getSteps()     // Catch:{ all -> 0x0585 }
            double r12 = r12 + r21
            goto L_0x0154
        L_0x0167:
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r12 = r18
        L_0x016d:
            boolean r14 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r14 == 0) goto L_0x019d
            java.lang.Object r14 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r14 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r14     // Catch:{ all -> 0x0585 }
            double r21 = r14.getCalories()     // Catch:{ all -> 0x0585 }
            double r12 = r12 + r21
            goto L_0x016d
        L_0x0180:
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r12 = r18
        L_0x0186:
            boolean r14 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r14 == 0) goto L_0x019d
            java.lang.Object r14 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r14 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r14     // Catch:{ all -> 0x0585 }
            int r14 = r14.getActiveTime()     // Catch:{ all -> 0x0585 }
            r21 = r5
            double r4 = (double) r14     // Catch:{ all -> 0x0585 }
            double r12 = r12 + r4
            r5 = r21
            goto L_0x0186
        L_0x019d:
            r21 = r5
            goto L_0x01bb
        L_0x01a0:
            r21 = r5
            java.util.Iterator r4 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r12 = r18
        L_0x01a8:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x0585 }
            if (r5 == 0) goto L_0x01bb
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r5 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r5     // Catch:{ all -> 0x0585 }
            double r22 = r5.getSteps()     // Catch:{ all -> 0x0585 }
            double r12 = r12 + r22
            goto L_0x01a8
        L_0x01bb:
            com.portfolio.platform.ui.view.chart.base.BarChart$b r5 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x0585 }
            r23 = 0
            r24 = 0
            r25 = 0
            int r4 = (int) r12     // Catch:{ all -> 0x0585 }
            r27 = 0
            r28 = 23
            r29 = 0
            r22 = r5
            r26 = r4
            r22.<init>(r23, r24, r25, r26, r27, r28, r29)     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$a r7 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x0585 }
            r31 = 0
            r4 = 1
            java.util.ArrayList[] r12 = new java.util.ArrayList[r4]     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r13 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r4]     // Catch:{ all -> 0x0585 }
            r14 = 0
            r13[r14] = r5     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r5 = com.fossil.w97.a(r13)     // Catch:{ all -> 0x0585 }
            r12[r14] = r5     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r32 = com.fossil.w97.a(r12)     // Catch:{ all -> 0x0585 }
            r33 = 0
            java.lang.String r5 = "isToday"
            com.fossil.ee7.a(r9, r5)     // Catch:{ all -> 0x0585 }
            boolean r5 = r9.booleanValue()     // Catch:{ all -> 0x0585 }
            if (r5 == 0) goto L_0x01f9
            if (r8 != r3) goto L_0x01f9
            r35 = 1
            goto L_0x01fb
        L_0x01f9:
            r35 = 0
        L_0x01fb:
            r36 = 5
            r37 = 0
            r30 = r7
            r30.<init>(r31, r32, r33, r35, r36, r37)     // Catch:{ all -> 0x0585 }
            r5 = r21
            r5.add(r7)     // Catch:{ all -> 0x0585 }
            int r3 = r3 + 1
            r13 = r10
            r12 = r20
            r11 = 1
            goto L_0x00f9
        L_0x0211:
            r20 = r12
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r10.<init>()     // Catch:{ all -> 0x0585 }
        L_0x0218:
            r11 = 900000(0xdbba0, double:4.44659E-318)
            int r21 = (r13 > r15 ? 1 : (r13 == r15 ? 0 : -1))
            if (r21 > 0) goto L_0x04be
            long r21 = r13 + r11
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r4.<init>()     // Catch:{ all -> 0x0585 }
            java.util.Iterator r24 = r0.iterator()     // Catch:{ all -> 0x0585 }
        L_0x022a:
            boolean r25 = r24.hasNext()     // Catch:{ all -> 0x0585 }
            if (r25 == 0) goto L_0x0254
            java.lang.Object r11 = r24.next()     // Catch:{ all -> 0x0585 }
            r12 = r11
            com.portfolio.platform.data.model.room.fitness.ActivitySample r12 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r12     // Catch:{ all -> 0x0585 }
            org.joda.time.DateTime r12 = r12.getStartTime()     // Catch:{ all -> 0x0585 }
            long r27 = r12.getMillis()     // Catch:{ all -> 0x0585 }
            int r12 = (r13 > r27 ? 1 : (r13 == r27 ? 0 : -1))
            if (r12 <= 0) goto L_0x0244
            goto L_0x024a
        L_0x0244:
            int r12 = (r21 > r27 ? 1 : (r21 == r27 ? 0 : -1))
            if (r12 <= 0) goto L_0x024a
            r12 = 1
            goto L_0x024b
        L_0x024a:
            r12 = 0
        L_0x024b:
            if (r12 == 0) goto L_0x0250
            r4.add(r11)     // Catch:{ all -> 0x0585 }
        L_0x0250:
            r11 = 900000(0xdbba0, double:4.44659E-318)
            goto L_0x022a
        L_0x0254:
            r0.removeAll(r4)     // Catch:{ all -> 0x0585 }
            java.util.Iterator r11 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r12 = r20
        L_0x025d:
            boolean r20 = r11.hasNext()     // Catch:{ all -> 0x0585 }
            if (r20 == 0) goto L_0x04b8
            java.lang.Object r20 = r11.next()     // Catch:{ all -> 0x0585 }
            r24 = r0
            r0 = r20
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x0585 }
            r20 = r7
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0585 }
            r7.<init>()     // Catch:{ all -> 0x0585 }
            java.util.Iterator r27 = r4.iterator()     // Catch:{ all -> 0x0585 }
        L_0x0278:
            boolean r28 = r27.hasNext()     // Catch:{ all -> 0x0585 }
            if (r28 == 0) goto L_0x02aa
            r28 = r4
            java.lang.Object r4 = r27.next()     // Catch:{ all -> 0x0585 }
            r29 = r4
            com.portfolio.platform.data.model.room.fitness.ActivitySample r29 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r29     // Catch:{ all -> 0x0585 }
            r30 = r11
            int r11 = r29.getTimeZoneOffsetInSecond()     // Catch:{ all -> 0x0585 }
            if (r0 != 0) goto L_0x0293
            r31 = r15
            goto L_0x029d
        L_0x0293:
            r31 = r15
            int r15 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r11 != r15) goto L_0x029d
            r11 = 1
            goto L_0x029e
        L_0x029d:
            r11 = 0
        L_0x029e:
            if (r11 == 0) goto L_0x02a3
            r7.add(r4)     // Catch:{ all -> 0x0585 }
        L_0x02a3:
            r4 = r28
            r11 = r30
            r15 = r31
            goto L_0x0278
        L_0x02aa:
            r28 = r4
            r30 = r11
            r31 = r15
            org.joda.time.DateTime r4 = new org.joda.time.DateTime     // Catch:{ all -> 0x0585 }
            int r11 = r0.intValue()     // Catch:{ all -> 0x0585 }
            int r11 = r11 * 1000
            org.joda.time.DateTimeZone r11 = org.joda.time.DateTimeZone.forOffsetMillis(r11)     // Catch:{ all -> 0x0585 }
            r4.<init>(r13, r11)     // Catch:{ all -> 0x0585 }
            int r4 = r4.getHourOfDay()     // Catch:{ all -> 0x0585 }
            boolean r11 = r7.isEmpty()     // Catch:{ all -> 0x0585 }
            r15 = 1
            r11 = r11 ^ r15
            if (r11 == 0) goto L_0x0406
            if (r2 == 0) goto L_0x0327
            if (r2 == r15) goto L_0x0304
            r11 = 2
            if (r2 == r11) goto L_0x02eb
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r33 = r18
        L_0x02d8:
            boolean r16 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r16 == 0) goto L_0x0324
            java.lang.Object r16 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r16 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r16     // Catch:{ all -> 0x0585 }
            double r35 = r16.getSteps()     // Catch:{ all -> 0x0585 }
            double r33 = r33 + r35
            goto L_0x02d8
        L_0x02eb:
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r33 = r18
        L_0x02f1:
            boolean r16 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r16 == 0) goto L_0x0324
            java.lang.Object r16 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r16 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r16     // Catch:{ all -> 0x0585 }
            double r35 = r16.getCalories()     // Catch:{ all -> 0x0585 }
            double r33 = r33 + r35
            goto L_0x02f1
        L_0x0304:
            r11 = 2
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r33 = r18
        L_0x030b:
            boolean r16 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r16 == 0) goto L_0x0324
            java.lang.Object r16 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r16 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r16     // Catch:{ all -> 0x0585 }
            int r11 = r16.getActiveTime()     // Catch:{ all -> 0x0585 }
            r35 = r13
            double r13 = (double) r11     // Catch:{ all -> 0x0585 }
            double r33 = r33 + r13
            r13 = r35
            r11 = 2
            goto L_0x030b
        L_0x0324:
            r35 = r13
            goto L_0x0342
        L_0x0327:
            r35 = r13
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x0585 }
            r33 = r18
        L_0x032f:
            boolean r11 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r11 == 0) goto L_0x0342
            java.lang.Object r11 = r7.next()     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.data.model.room.fitness.ActivitySample r11 = (com.portfolio.platform.data.model.room.fitness.ActivitySample) r11     // Catch:{ all -> 0x0585 }
            double r13 = r11.getSteps()     // Catch:{ all -> 0x0585 }
            double r33 = r33 + r13
            goto L_0x032f
        L_0x0342:
            java.util.Iterator r7 = r10.iterator()     // Catch:{ all -> 0x0585 }
        L_0x0346:
            boolean r11 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r11 == 0) goto L_0x0378
            java.lang.Object r11 = r7.next()     // Catch:{ all -> 0x0585 }
            r13 = r11
            com.fossil.v87 r13 = (com.fossil.v87) r13     // Catch:{ all -> 0x0585 }
            java.lang.Object r14 = r13.getFirst()     // Catch:{ all -> 0x0585 }
            java.lang.Number r14 = (java.lang.Number) r14     // Catch:{ all -> 0x0585 }
            int r14 = r14.intValue()     // Catch:{ all -> 0x0585 }
            if (r14 != r4) goto L_0x0374
            java.lang.Object r13 = r13.getSecond()     // Catch:{ all -> 0x0585 }
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ all -> 0x0585 }
            int r13 = r13.intValue()     // Catch:{ all -> 0x0585 }
            if (r0 != 0) goto L_0x036c
            goto L_0x0374
        L_0x036c:
            int r14 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r13 != r14) goto L_0x0374
            r13 = 1
            goto L_0x0375
        L_0x0374:
            r13 = 0
        L_0x0375:
            if (r13 == 0) goto L_0x0346
            goto L_0x0379
        L_0x0378:
            r11 = 0
        L_0x0379:
            com.fossil.v87 r11 = (com.fossil.v87) r11     // Catch:{ all -> 0x0585 }
            if (r11 == 0) goto L_0x03b3
            r10.remove(r11)     // Catch:{ all -> 0x0585 }
            com.fossil.v87 r7 = new com.fossil.v87     // Catch:{ all -> 0x0585 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r13 = new com.fossil.r87     // Catch:{ all -> 0x0585 }
            java.lang.Object r14 = r11.getThird()     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r14 = (com.fossil.r87) r14     // Catch:{ all -> 0x0585 }
            java.lang.Object r14 = r14.getFirst()     // Catch:{ all -> 0x0585 }
            java.lang.Number r14 = (java.lang.Number) r14     // Catch:{ all -> 0x0585 }
            double r37 = r14.doubleValue()     // Catch:{ all -> 0x0585 }
            double r37 = r37 + r33
            java.lang.Double r14 = java.lang.Double.valueOf(r37)     // Catch:{ all -> 0x0585 }
            java.lang.Object r11 = r11.getThird()     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r11 = (com.fossil.r87) r11     // Catch:{ all -> 0x0585 }
            java.lang.Object r11 = r11.getSecond()     // Catch:{ all -> 0x0585 }
            r13.<init>(r14, r11)     // Catch:{ all -> 0x0585 }
            r7.<init>(r4, r0, r13)     // Catch:{ all -> 0x0585 }
            r10.add(r7)     // Catch:{ all -> 0x0585 }
            goto L_0x0485
        L_0x03b3:
            com.fossil.v87 r7 = new com.fossil.v87     // Catch:{ all -> 0x0585 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r13 = new com.fossil.r87     // Catch:{ all -> 0x0585 }
            java.lang.Double r14 = java.lang.Double.valueOf(r33)     // Catch:{ all -> 0x0585 }
            java.lang.String r15 = "isToday"
            com.fossil.ee7.a(r9, r15)     // Catch:{ all -> 0x0585 }
            boolean r15 = r9.booleanValue()     // Catch:{ all -> 0x0585 }
            if (r15 == 0) goto L_0x03d7
            if (r0 != 0) goto L_0x03cd
            goto L_0x03d7
        L_0x03cd:
            int r15 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r15 != r3) goto L_0x03d7
            if (r8 != r4) goto L_0x03d7
            r4 = 1
            goto L_0x03d8
        L_0x03d7:
            r4 = 0
        L_0x03d8:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0585 }
            r13.<init>(r14, r4)     // Catch:{ all -> 0x0585 }
            r7.<init>(r11, r0, r13)     // Catch:{ all -> 0x0585 }
            r10.add(r7)     // Catch:{ all -> 0x0585 }
            if (r0 != 0) goto L_0x03e8
            goto L_0x03ee
        L_0x03e8:
            int r4 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r12 == r4) goto L_0x03ff
        L_0x03ee:
            java.lang.String r4 = "timeZoneOffset"
            com.fossil.ee7.a(r0, r4)     // Catch:{ all -> 0x0585 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x0585 }
            r4 = 1
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0585 }
            r12 = r0
            goto L_0x0486
        L_0x03ff:
            r0 = 0
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0585 }
            goto L_0x0486
        L_0x0406:
            r35 = r13
            if (r0 != 0) goto L_0x040c
            goto L_0x0485
        L_0x040c:
            int r7 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r12 != r7) goto L_0x0485
            java.util.Iterator r7 = r10.iterator()     // Catch:{ all -> 0x0585 }
        L_0x0416:
            boolean r13 = r7.hasNext()     // Catch:{ all -> 0x0585 }
            if (r13 == 0) goto L_0x0448
            java.lang.Object r13 = r7.next()     // Catch:{ all -> 0x0585 }
            r14 = r13
            com.fossil.v87 r14 = (com.fossil.v87) r14     // Catch:{ all -> 0x0585 }
            java.lang.Object r15 = r14.getFirst()     // Catch:{ all -> 0x0585 }
            java.lang.Number r15 = (java.lang.Number) r15     // Catch:{ all -> 0x0585 }
            int r15 = r15.intValue()     // Catch:{ all -> 0x0585 }
            if (r15 != r4) goto L_0x0444
            java.lang.Object r14 = r14.getSecond()     // Catch:{ all -> 0x0585 }
            java.lang.Number r14 = (java.lang.Number) r14     // Catch:{ all -> 0x0585 }
            int r14 = r14.intValue()     // Catch:{ all -> 0x0585 }
            if (r0 != 0) goto L_0x043c
            goto L_0x0444
        L_0x043c:
            int r15 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r14 != r15) goto L_0x0444
            r14 = 1
            goto L_0x0445
        L_0x0444:
            r14 = 0
        L_0x0445:
            if (r14 == 0) goto L_0x0416
            goto L_0x0449
        L_0x0448:
            r13 = 0
        L_0x0449:
            com.fossil.v87 r13 = (com.fossil.v87) r13     // Catch:{ all -> 0x0585 }
            if (r13 != 0) goto L_0x0485
            com.fossil.v87 r7 = new com.fossil.v87     // Catch:{ all -> 0x0585 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r14 = new com.fossil.r87     // Catch:{ all -> 0x0585 }
            java.lang.Double r15 = java.lang.Double.valueOf(r18)     // Catch:{ all -> 0x0585 }
            java.lang.String r11 = "isToday"
            com.fossil.ee7.a(r9, r11)     // Catch:{ all -> 0x0585 }
            boolean r11 = r9.booleanValue()     // Catch:{ all -> 0x0585 }
            if (r11 == 0) goto L_0x0471
            if (r0 != 0) goto L_0x0467
            goto L_0x0471
        L_0x0467:
            int r11 = r0.intValue()     // Catch:{ all -> 0x0585 }
            if (r11 != r3) goto L_0x0471
            if (r8 != r4) goto L_0x0471
            r4 = 1
            goto L_0x0472
        L_0x0471:
            r4 = 0
        L_0x0472:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0585 }
            r14.<init>(r15, r4)     // Catch:{ all -> 0x0585 }
            r7.<init>(r13, r0, r14)     // Catch:{ all -> 0x0585 }
            r10.add(r7)     // Catch:{ all -> 0x0585 }
            r0 = 0
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0585 }
            goto L_0x0486
        L_0x0485:
            r7 = 0
        L_0x0486:
            if (r7 == 0) goto L_0x04aa
            boolean r0 = r7.booleanValue()     // Catch:{ all -> 0x0585 }
            if (r0 != 0) goto L_0x0497
            boolean r0 = r6.isEmpty()     // Catch:{ all -> 0x0585 }
            if (r0 == 0) goto L_0x0495
            goto L_0x0497
        L_0x0495:
            r0 = 0
            goto L_0x0498
        L_0x0497:
            r0 = 1
        L_0x0498:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x0585 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0585 }
            r13 = r35
            java.lang.String r0 = r1.a(r13, r12, r0)     // Catch:{ all -> 0x0585 }
            r6.add(r0)     // Catch:{ all -> 0x0585 }
            goto L_0x04ac
        L_0x04aa:
            r13 = r35
        L_0x04ac:
            r7 = r20
            r0 = r24
            r4 = r28
            r11 = r30
            r15 = r31
            goto L_0x025d
        L_0x04b8:
            r20 = r12
            r13 = r21
            goto L_0x0218
        L_0x04be:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ all -> 0x0585 }
            r2 = 2131886621(0x7f12021d, float:1.9407826E38)
            java.lang.String r0 = com.fossil.ig5.a(r0, r2)     // Catch:{ all -> 0x0585 }
            r6.add(r0)     // Catch:{ all -> 0x0585 }
            java.util.Iterator r0 = r10.iterator()     // Catch:{ all -> 0x0585 }
        L_0x04d2:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x0585 }
            if (r2 == 0) goto L_0x0562
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x0585 }
            com.fossil.v87 r2 = (com.fossil.v87) r2     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$a r3 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x0585 }
            r8 = 0
            r4 = 1
            java.util.ArrayList[] r7 = new java.util.ArrayList[r4]     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r9 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r4]     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b r18 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x0585 }
            r11 = 0
            r12 = 0
            r13 = 0
            java.lang.Object r10 = r2.getThird()     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r10 = (com.fossil.r87) r10     // Catch:{ all -> 0x0585 }
            java.lang.Object r10 = r10.getFirst()     // Catch:{ all -> 0x0585 }
            java.lang.Number r10 = (java.lang.Number) r10     // Catch:{ all -> 0x0585 }
            double r14 = r10.doubleValue()     // Catch:{ all -> 0x0585 }
            int r14 = (int) r14     // Catch:{ all -> 0x0585 }
            r15 = 0
            r16 = 23
            r17 = 0
            r10 = r18
            r10.<init>(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ all -> 0x0585 }
            r10 = 0
            r9[r10] = r18     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r9 = com.fossil.w97.a(r9)     // Catch:{ all -> 0x0585 }
            r7[r10] = r9     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r9 = com.fossil.w97.a(r7)     // Catch:{ all -> 0x0585 }
            r10 = 0
            java.lang.Object r2 = r2.getThird()     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r2 = (com.fossil.r87) r2     // Catch:{ all -> 0x0585 }
            java.lang.Object r2 = r2.getSecond()     // Catch:{ all -> 0x0585 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x0585 }
            boolean r12 = r2.booleanValue()     // Catch:{ all -> 0x0585 }
            r13 = 5
            r14 = 0
            r7 = r3
            r7.<init>(r8, r9, r10, r12, r13, r14)     // Catch:{ all -> 0x0585 }
            r5.add(r3)     // Catch:{ all -> 0x0585 }
            goto L_0x04d2
        L_0x052f:
            com.portfolio.platform.ui.view.chart.base.BarChart$a r0 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x0585 }
            r16 = 0
            r2 = 1
            java.util.ArrayList[] r3 = new java.util.ArrayList[r2]     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r2 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r2]     // Catch:{ all -> 0x0585 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b r4 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x0585 }
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 23
            r14 = 0
            r7 = r4
            r7.<init>(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x0585 }
            r7 = 0
            r2[r7] = r4     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r2 = com.fossil.w97.a(r2)     // Catch:{ all -> 0x0585 }
            r3[r7] = r2     // Catch:{ all -> 0x0585 }
            java.util.ArrayList r17 = com.fossil.w97.a(r3)     // Catch:{ all -> 0x0585 }
            r18 = 0
            r20 = 0
            r21 = 13
            r22 = 0
            r15 = r0
            r15.<init>(r16, r17, r18, r20, r21, r22)     // Catch:{ all -> 0x0585 }
            r5.add(r0)     // Catch:{ all -> 0x0585 }
        L_0x0562:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0585 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0585 }
            java.lang.String r2 = "SupportedFunction"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0585 }
            r3.<init>()     // Catch:{ all -> 0x0585 }
            java.lang.String r4 = "transferActivitySamplesToDetailChart - detailChartData="
            r3.append(r4)     // Catch:{ all -> 0x0585 }
            r3.append(r5)     // Catch:{ all -> 0x0585 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0585 }
            r0.d(r2, r3)     // Catch:{ all -> 0x0585 }
            com.fossil.r87 r0 = new com.fossil.r87     // Catch:{ all -> 0x0585 }
            r0.<init>(r5, r6)     // Catch:{ all -> 0x0585 }
            monitor-exit(r39)
            return r0
        L_0x0585:
            r0 = move-exception
            monitor-exit(r39)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tg6.a(java.util.Date, java.util.List, int):com.fossil.r87");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0312  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0313  */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x03b4  */
    /* JADX WARNING: Removed duplicated region for block: B:178:0x0131 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x01e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x03d3 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:188:0x0238 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:190:0x029b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0372 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01e1  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0235  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x029c A[LOOP:7: B:82:0x0268->B:95:0x029c, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.fossil.r87<java.util.ArrayList<com.portfolio.platform.ui.view.chart.base.BarChart.a>, java.util.ArrayList<java.lang.String>> a(java.util.Date r36, java.util.List<com.portfolio.platform.data.model.goaltracking.GoalTrackingData> r37) {
        /*
            r35 = this;
            r1 = r35
            r0 = r36
            r2 = r37
            monitor-enter(r35)
            java.lang.String r3 = "date"
            com.fossil.ee7.b(r0, r3)     // Catch:{ all -> 0x04b1 }
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x04b1 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ all -> 0x04b1 }
            java.lang.String r4 = "SupportedFunction"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x04b1 }
            r5.<init>()     // Catch:{ all -> 0x04b1 }
            java.lang.String r6 = "transferGoalTrackingSamplesToDetailChart - date="
            r5.append(r6)     // Catch:{ all -> 0x04b1 }
            r5.append(r0)     // Catch:{ all -> 0x04b1 }
            java.lang.String r6 = ", goalTrackingSamples="
            r5.append(r6)     // Catch:{ all -> 0x04b1 }
            if (r2 == 0) goto L_0x0031
            int r7 = r37.size()     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x04b1 }
            goto L_0x0032
        L_0x0031:
            r7 = 0
        L_0x0032:
            r5.append(r7)     // Catch:{ all -> 0x04b1 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x04b1 }
            r3.d(r4, r5)     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r3.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r4.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.HashSet r5 = new java.util.HashSet     // Catch:{ all -> 0x04b1 }
            r5.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.Calendar r7 = java.util.Calendar.getInstance()     // Catch:{ all -> 0x04b1 }
            r8 = 11
            int r7 = r7.get(r8)     // Catch:{ all -> 0x04b1 }
            java.lang.Boolean r8 = com.fossil.zd5.w(r36)     // Catch:{ all -> 0x04b1 }
            r9 = 0
            r10 = 1
            if (r2 == 0) goto L_0x045d
            boolean r11 = r37.isEmpty()     // Catch:{ all -> 0x04b1 }
            r11 = r11 ^ r10
            if (r11 != r10) goto L_0x045d
            int r11 = r37.size()     // Catch:{ all -> 0x04b1 }
            if (r11 <= r10) goto L_0x0072
            com.fossil.tg6$b r11 = new com.fossil.tg6$b     // Catch:{ all -> 0x04b1 }
            r11.<init>()     // Catch:{ all -> 0x04b1 }
            com.fossil.aa7.a(r2, r11)     // Catch:{ all -> 0x04b1 }
        L_0x0072:
            java.util.Iterator r11 = r37.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x0076:
            boolean r12 = r11.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r12 == 0) goto L_0x008e
            java.lang.Object r12 = r11.next()     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r12 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r12     // Catch:{ all -> 0x04b1 }
            int r12 = r12.getTimezoneOffsetInSecond()     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x04b1 }
            r5.add(r12)     // Catch:{ all -> 0x04b1 }
            goto L_0x0076
        L_0x008e:
            java.lang.Object r11 = com.fossil.ea7.d(r37)     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r11 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r11     // Catch:{ all -> 0x04b1 }
            int r11 = r11.getTimezoneOffsetInSecond()     // Catch:{ all -> 0x04b1 }
            java.lang.Object r12 = com.fossil.ea7.f(r37)     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r12 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r12     // Catch:{ all -> 0x04b1 }
            int r12 = r12.getTimezoneOffsetInSecond()     // Catch:{ all -> 0x04b1 }
            java.util.TimeZone r13 = com.fossil.zd5.a(r11)     // Catch:{ all -> 0x04b1 }
            java.util.Date r13 = com.fossil.zd5.d(r0, r13)     // Catch:{ all -> 0x04b1 }
            java.util.TimeZone r12 = com.fossil.zd5.a(r12)     // Catch:{ all -> 0x04b1 }
            java.util.Date r0 = com.fossil.zd5.c(r0, r12)     // Catch:{ all -> 0x04b1 }
            java.lang.String r12 = "startDate"
            com.fossil.ee7.a(r13, r12)     // Catch:{ all -> 0x04b1 }
            long r12 = r13.getTime()     // Catch:{ all -> 0x04b1 }
            java.lang.String r14 = "endDate"
            com.fossil.ee7.a(r0, r14)     // Catch:{ all -> 0x04b1 }
            long r14 = r0.getTime()     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r0.<init>()     // Catch:{ all -> 0x04b1 }
            r0.addAll(r2)     // Catch:{ all -> 0x04b1 }
            java.util.TimeZone r2 = java.util.TimeZone.getDefault()     // Catch:{ all -> 0x04b1 }
            java.lang.String r6 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r2, r6)     // Catch:{ all -> 0x04b1 }
            java.lang.String r2 = r2.getID()     // Catch:{ all -> 0x04b1 }
            int r2 = com.fossil.zd5.a(r2, r10)     // Catch:{ all -> 0x04b1 }
            int r6 = r5.size()     // Catch:{ all -> 0x04b1 }
            if (r6 > r10) goto L_0x0190
            if (r11 == r2) goto L_0x00e7
            r2 = 1
            goto L_0x00e8
        L_0x00e7:
            r2 = 0
        L_0x00e8:
            java.lang.String r2 = r1.a(r12, r11, r2)     // Catch:{ all -> 0x04b1 }
            r4.add(r2)     // Catch:{ all -> 0x04b1 }
            r2 = 0
        L_0x00f0:
            int r5 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r5 > 0) goto L_0x018d
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r5 = r5 + r12
            java.lang.String r10 = r1.a(r5, r11, r9)     // Catch:{ all -> 0x04b1 }
            r4.add(r10)     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r10.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x0108:
            boolean r17 = r16.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r17 == 0) goto L_0x0133
            java.lang.Object r9 = r16.next()     // Catch:{ all -> 0x04b1 }
            r18 = r9
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r18 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r18     // Catch:{ all -> 0x04b1 }
            org.joda.time.DateTime r18 = r18.getTrackedAt()     // Catch:{ all -> 0x04b1 }
            long r18 = r18.getMillis()     // Catch:{ all -> 0x04b1 }
            int r20 = (r12 > r18 ? 1 : (r12 == r18 ? 0 : -1))
            if (r20 <= 0) goto L_0x0123
            goto L_0x012a
        L_0x0123:
            int r20 = (r5 > r18 ? 1 : (r5 == r18 ? 0 : -1))
            if (r20 <= 0) goto L_0x012a
            r18 = 1
            goto L_0x012c
        L_0x012a:
            r18 = 0
        L_0x012c:
            if (r18 == 0) goto L_0x0131
            r10.add(r9)     // Catch:{ all -> 0x04b1 }
        L_0x0131:
            r9 = 0
            goto L_0x0108
        L_0x0133:
            r0.removeAll(r10)     // Catch:{ all -> 0x04b1 }
            int r23 = r10.size()     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b r9 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x04b1 }
            r20 = 0
            r21 = 0
            r22 = 0
            r24 = 0
            r25 = 23
            r26 = 0
            r19 = r9
            r19.<init>(r20, r21, r22, r23, r24, r25, r26)     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$a r10 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x04b1 }
            r28 = 0
            r12 = 1
            java.util.ArrayList[] r13 = new java.util.ArrayList[r12]     // Catch:{ all -> 0x04b1 }
            r36 = r5
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r5 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r12]     // Catch:{ all -> 0x04b1 }
            r6 = 0
            r5[r6] = r9     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r5 = com.fossil.w97.a(r5)     // Catch:{ all -> 0x04b1 }
            r13[r6] = r5     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r29 = com.fossil.w97.a(r13)     // Catch:{ all -> 0x04b1 }
            r30 = 0
            java.lang.String r5 = "isToday"
            com.fossil.ee7.a(r8, r5)     // Catch:{ all -> 0x04b1 }
            boolean r5 = r8.booleanValue()     // Catch:{ all -> 0x04b1 }
            if (r5 == 0) goto L_0x0177
            if (r7 != r2) goto L_0x0177
            r32 = 1
            goto L_0x0179
        L_0x0177:
            r32 = 0
        L_0x0179:
            r33 = 5
            r34 = 0
            r27 = r10
            r27.<init>(r28, r29, r30, r32, r33, r34)     // Catch:{ all -> 0x04b1 }
            r3.add(r10)     // Catch:{ all -> 0x04b1 }
            int r2 = r2 + 1
            r12 = r36
            r9 = 0
            r10 = 1
            goto L_0x00f0
        L_0x018d:
            r2 = r3
            goto L_0x048e
        L_0x0190:
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r2.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.TimeZone r6 = java.util.TimeZone.getDefault()     // Catch:{ all -> 0x04b1 }
            java.lang.String r9 = "TimeZone.getDefault()"
            com.fossil.ee7.a(r6, r9)     // Catch:{ all -> 0x04b1 }
            java.lang.String r6 = r6.getID()     // Catch:{ all -> 0x04b1 }
            r9 = 1
            int r6 = com.fossil.zd5.a(r6, r9)     // Catch:{ all -> 0x04b1 }
        L_0x01a7:
            r9 = 900000(0xdbba0, double:4.44659E-318)
            int r18 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r18 > 0) goto L_0x03e5
            long r18 = r12 + r9
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r9.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r10 = r0.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x01b9:
            boolean r20 = r10.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r20 == 0) goto L_0x01e7
            r20 = r11
            java.lang.Object r11 = r10.next()     // Catch:{ all -> 0x04b1 }
            r21 = r11
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r21 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r21     // Catch:{ all -> 0x04b1 }
            org.joda.time.DateTime r21 = r21.getTrackedAt()     // Catch:{ all -> 0x04b1 }
            long r21 = r21.getMillis()     // Catch:{ all -> 0x04b1 }
            int r23 = (r12 > r21 ? 1 : (r12 == r21 ? 0 : -1))
            if (r23 <= 0) goto L_0x01d6
            goto L_0x01dd
        L_0x01d6:
            int r23 = (r18 > r21 ? 1 : (r18 == r21 ? 0 : -1))
            if (r23 <= 0) goto L_0x01dd
            r21 = 1
            goto L_0x01df
        L_0x01dd:
            r21 = 0
        L_0x01df:
            if (r21 == 0) goto L_0x01e4
            r9.add(r11)     // Catch:{ all -> 0x04b1 }
        L_0x01e4:
            r11 = r20
            goto L_0x01b9
        L_0x01e7:
            r20 = r11
            r0.removeAll(r9)     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r10 = r5.iterator()     // Catch:{ all -> 0x04b1 }
            r11 = r20
        L_0x01f2:
            boolean r20 = r10.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r20 == 0) goto L_0x03e1
            java.lang.Object r20 = r10.next()     // Catch:{ all -> 0x04b1 }
            r21 = r0
            r0 = r20
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ all -> 0x04b1 }
            r20 = r5
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x04b1 }
            r5.<init>()     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r22 = r9.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x020d:
            boolean r23 = r22.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r23 == 0) goto L_0x023f
            r23 = r9
            java.lang.Object r9 = r22.next()     // Catch:{ all -> 0x04b1 }
            r24 = r9
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r24 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingData) r24     // Catch:{ all -> 0x04b1 }
            r25 = r10
            int r10 = r24.getTimezoneOffsetInSecond()     // Catch:{ all -> 0x04b1 }
            if (r0 != 0) goto L_0x0228
            r26 = r14
            goto L_0x0232
        L_0x0228:
            r26 = r14
            int r14 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r10 != r14) goto L_0x0232
            r10 = 1
            goto L_0x0233
        L_0x0232:
            r10 = 0
        L_0x0233:
            if (r10 == 0) goto L_0x0238
            r5.add(r9)     // Catch:{ all -> 0x04b1 }
        L_0x0238:
            r9 = r23
            r10 = r25
            r14 = r26
            goto L_0x020d
        L_0x023f:
            r23 = r9
            r25 = r10
            r26 = r14
            org.joda.time.DateTime r9 = new org.joda.time.DateTime     // Catch:{ all -> 0x04b1 }
            int r10 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            int r10 = r10 * 1000
            org.joda.time.DateTimeZone r10 = org.joda.time.DateTimeZone.forOffsetMillis(r10)     // Catch:{ all -> 0x04b1 }
            r9.<init>(r12, r10)     // Catch:{ all -> 0x04b1 }
            int r9 = r9.getHourOfDay()     // Catch:{ all -> 0x04b1 }
            boolean r10 = r5.isEmpty()     // Catch:{ all -> 0x04b1 }
            r14 = 1
            r10 = r10 ^ r14
            if (r10 == 0) goto L_0x0331
            int r5 = r5.size()     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r10 = r2.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x0268:
            boolean r14 = r10.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r14 == 0) goto L_0x029f
            java.lang.Object r14 = r10.next()     // Catch:{ all -> 0x04b1 }
            r15 = r14
            com.fossil.v87 r15 = (com.fossil.v87) r15     // Catch:{ all -> 0x04b1 }
            java.lang.Object r22 = r15.getFirst()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r22 = (java.lang.Number) r22     // Catch:{ all -> 0x04b1 }
            r24 = r10
            int r10 = r22.intValue()     // Catch:{ all -> 0x04b1 }
            if (r10 != r9) goto L_0x0298
            java.lang.Object r10 = r15.getSecond()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r10 = (java.lang.Number) r10     // Catch:{ all -> 0x04b1 }
            int r10 = r10.intValue()     // Catch:{ all -> 0x04b1 }
            if (r0 != 0) goto L_0x0290
            goto L_0x0298
        L_0x0290:
            int r15 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r10 != r15) goto L_0x0298
            r10 = 1
            goto L_0x0299
        L_0x0298:
            r10 = 0
        L_0x0299:
            if (r10 == 0) goto L_0x029c
            goto L_0x02a0
        L_0x029c:
            r10 = r24
            goto L_0x0268
        L_0x029f:
            r14 = 0
        L_0x02a0:
            com.fossil.v87 r14 = (com.fossil.v87) r14     // Catch:{ all -> 0x04b1 }
            if (r14 == 0) goto L_0x02dc
            r2.remove(r14)     // Catch:{ all -> 0x04b1 }
            com.fossil.v87 r10 = new com.fossil.v87     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r15 = new com.fossil.r87     // Catch:{ all -> 0x04b1 }
            java.lang.Object r22 = r14.getThird()     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r22 = (com.fossil.r87) r22     // Catch:{ all -> 0x04b1 }
            java.lang.Object r22 = r22.getFirst()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r22 = (java.lang.Number) r22     // Catch:{ all -> 0x04b1 }
            int r22 = r22.intValue()     // Catch:{ all -> 0x04b1 }
            int r22 = r22 + r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r22)     // Catch:{ all -> 0x04b1 }
            java.lang.Object r14 = r14.getThird()     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r14 = (com.fossil.r87) r14     // Catch:{ all -> 0x04b1 }
            java.lang.Object r14 = r14.getSecond()     // Catch:{ all -> 0x04b1 }
            r15.<init>(r5, r14)     // Catch:{ all -> 0x04b1 }
            r10.<init>(r9, r0, r15)     // Catch:{ all -> 0x04b1 }
            r2.add(r10)     // Catch:{ all -> 0x04b1 }
            r22 = r3
            goto L_0x03b1
        L_0x02dc:
            com.fossil.v87 r10 = new com.fossil.v87     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r15 = new com.fossil.r87     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x04b1 }
            r22 = r3
            java.lang.String r3 = "isToday"
            com.fossil.ee7.a(r8, r3)     // Catch:{ all -> 0x04b1 }
            boolean r3 = r8.booleanValue()     // Catch:{ all -> 0x04b1 }
            if (r3 == 0) goto L_0x0302
            if (r0 != 0) goto L_0x02f8
            goto L_0x0302
        L_0x02f8:
            int r3 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r3 != r6) goto L_0x0302
            if (r7 != r9) goto L_0x0302
            r3 = 1
            goto L_0x0303
        L_0x0302:
            r3 = 0
        L_0x0303:
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x04b1 }
            r15.<init>(r5, r3)     // Catch:{ all -> 0x04b1 }
            r10.<init>(r14, r0, r15)     // Catch:{ all -> 0x04b1 }
            r2.add(r10)     // Catch:{ all -> 0x04b1 }
            if (r0 != 0) goto L_0x0313
            goto L_0x0319
        L_0x0313:
            int r3 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r11 == r3) goto L_0x032a
        L_0x0319:
            java.lang.String r3 = "timeZoneOffset"
            com.fossil.ee7.a(r0, r3)     // Catch:{ all -> 0x04b1 }
            int r0 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            r3 = 1
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x04b1 }
            r11 = r0
            goto L_0x03b2
        L_0x032a:
            r0 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x04b1 }
            goto L_0x03b2
        L_0x0331:
            r22 = r3
            if (r0 != 0) goto L_0x0337
            goto L_0x03b1
        L_0x0337:
            int r3 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r11 != r3) goto L_0x03b1
            java.util.Iterator r3 = r2.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x0341:
            boolean r5 = r3.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r5 == 0) goto L_0x0373
            java.lang.Object r5 = r3.next()     // Catch:{ all -> 0x04b1 }
            r10 = r5
            com.fossil.v87 r10 = (com.fossil.v87) r10     // Catch:{ all -> 0x04b1 }
            java.lang.Object r14 = r10.getFirst()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r14 = (java.lang.Number) r14     // Catch:{ all -> 0x04b1 }
            int r14 = r14.intValue()     // Catch:{ all -> 0x04b1 }
            if (r14 != r9) goto L_0x036f
            java.lang.Object r10 = r10.getSecond()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r10 = (java.lang.Number) r10     // Catch:{ all -> 0x04b1 }
            int r10 = r10.intValue()     // Catch:{ all -> 0x04b1 }
            if (r0 != 0) goto L_0x0367
            goto L_0x036f
        L_0x0367:
            int r14 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r10 != r14) goto L_0x036f
            r10 = 1
            goto L_0x0370
        L_0x036f:
            r10 = 0
        L_0x0370:
            if (r10 == 0) goto L_0x0341
            goto L_0x0374
        L_0x0373:
            r5 = 0
        L_0x0374:
            com.fossil.v87 r5 = (com.fossil.v87) r5     // Catch:{ all -> 0x04b1 }
            if (r5 != 0) goto L_0x03b1
            com.fossil.v87 r3 = new com.fossil.v87     // Catch:{ all -> 0x04b1 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r10 = new com.fossil.r87     // Catch:{ all -> 0x04b1 }
            r14 = 0
            java.lang.Integer r15 = java.lang.Integer.valueOf(r14)     // Catch:{ all -> 0x04b1 }
            java.lang.String r14 = "isToday"
            com.fossil.ee7.a(r8, r14)     // Catch:{ all -> 0x04b1 }
            boolean r14 = r8.booleanValue()     // Catch:{ all -> 0x04b1 }
            if (r14 == 0) goto L_0x039d
            if (r0 != 0) goto L_0x0393
            goto L_0x039d
        L_0x0393:
            int r14 = r0.intValue()     // Catch:{ all -> 0x04b1 }
            if (r14 != r6) goto L_0x039d
            if (r7 != r9) goto L_0x039d
            r9 = 1
            goto L_0x039e
        L_0x039d:
            r9 = 0
        L_0x039e:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ all -> 0x04b1 }
            r10.<init>(r15, r9)     // Catch:{ all -> 0x04b1 }
            r3.<init>(r5, r0, r10)     // Catch:{ all -> 0x04b1 }
            r2.add(r3)     // Catch:{ all -> 0x04b1 }
            r0 = 0
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x04b1 }
            goto L_0x03b2
        L_0x03b1:
            r5 = 0
        L_0x03b2:
            if (r5 == 0) goto L_0x03d3
            boolean r0 = r5.booleanValue()     // Catch:{ all -> 0x04b1 }
            if (r0 != 0) goto L_0x03c3
            boolean r0 = r4.isEmpty()     // Catch:{ all -> 0x04b1 }
            if (r0 == 0) goto L_0x03c1
            goto L_0x03c3
        L_0x03c1:
            r0 = 0
            goto L_0x03c4
        L_0x03c3:
            r0 = 1
        L_0x03c4:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ all -> 0x04b1 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x04b1 }
            java.lang.String r0 = r1.a(r12, r11, r0)     // Catch:{ all -> 0x04b1 }
            r4.add(r0)     // Catch:{ all -> 0x04b1 }
        L_0x03d3:
            r5 = r20
            r0 = r21
            r3 = r22
            r9 = r23
            r10 = r25
            r14 = r26
            goto L_0x01f2
        L_0x03e1:
            r12 = r18
            goto L_0x01a7
        L_0x03e5:
            r22 = r3
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ all -> 0x04b1 }
            r3 = 2131886621(0x7f12021d, float:1.9407826E38)
            java.lang.String r0 = com.fossil.ig5.a(r0, r3)     // Catch:{ all -> 0x04b1 }
            r4.add(r0)     // Catch:{ all -> 0x04b1 }
            java.util.Iterator r0 = r2.iterator()     // Catch:{ all -> 0x04b1 }
        L_0x03fb:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x04b1 }
            if (r2 == 0) goto L_0x045a
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x04b1 }
            com.fossil.v87 r2 = (com.fossil.v87) r2     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$a r3 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x04b1 }
            r6 = 0
            r5 = 1
            java.util.ArrayList[] r7 = new java.util.ArrayList[r5]     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r8 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r5]     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b r5 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x04b1 }
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.Object r9 = r2.getThird()     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r9 = (com.fossil.r87) r9     // Catch:{ all -> 0x04b1 }
            java.lang.Object r9 = r9.getFirst()     // Catch:{ all -> 0x04b1 }
            java.lang.Number r9 = (java.lang.Number) r9     // Catch:{ all -> 0x04b1 }
            int r13 = r9.intValue()     // Catch:{ all -> 0x04b1 }
            r14 = 0
            r15 = 23
            r16 = 0
            r9 = r5
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ all -> 0x04b1 }
            r9 = 0
            r8[r9] = r5     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r5 = com.fossil.w97.a(r8)     // Catch:{ all -> 0x04b1 }
            r7[r9] = r5     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r7 = com.fossil.w97.a(r7)     // Catch:{ all -> 0x04b1 }
            r8 = 0
            java.lang.Object r2 = r2.getThird()     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r2 = (com.fossil.r87) r2     // Catch:{ all -> 0x04b1 }
            java.lang.Object r2 = r2.getSecond()     // Catch:{ all -> 0x04b1 }
            java.lang.Boolean r2 = (java.lang.Boolean) r2     // Catch:{ all -> 0x04b1 }
            boolean r10 = r2.booleanValue()     // Catch:{ all -> 0x04b1 }
            r11 = 5
            r12 = 0
            r5 = r3
            r5.<init>(r6, r7, r8, r10, r11, r12)     // Catch:{ all -> 0x04b1 }
            r2 = r22
            r2.add(r3)     // Catch:{ all -> 0x04b1 }
            r22 = r2
            goto L_0x03fb
        L_0x045a:
            r2 = r22
            goto L_0x048e
        L_0x045d:
            r2 = r3
            com.portfolio.platform.ui.view.chart.base.BarChart$a r0 = new com.portfolio.platform.ui.view.chart.base.BarChart$a     // Catch:{ all -> 0x04b1 }
            r6 = 0
            r3 = 1
            java.util.ArrayList[] r5 = new java.util.ArrayList[r3]     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b[] r3 = new com.portfolio.platform.ui.view.chart.base.BarChart.b[r3]     // Catch:{ all -> 0x04b1 }
            com.portfolio.platform.ui.view.chart.base.BarChart$b r15 = new com.portfolio.platform.ui.view.chart.base.BarChart$b     // Catch:{ all -> 0x04b1 }
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 23
            r14 = 0
            r7 = r15
            r7.<init>(r8, r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x04b1 }
            r7 = 0
            r3[r7] = r15     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r3 = com.fossil.w97.a(r3)     // Catch:{ all -> 0x04b1 }
            r5[r7] = r3     // Catch:{ all -> 0x04b1 }
            java.util.ArrayList r7 = com.fossil.w97.a(r5)     // Catch:{ all -> 0x04b1 }
            r8 = 0
            r10 = 0
            r11 = 13
            r12 = 0
            r5 = r0
            r5.<init>(r6, r7, r8, r10, r11, r12)     // Catch:{ all -> 0x04b1 }
            r2.add(r0)     // Catch:{ all -> 0x04b1 }
        L_0x048e:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x04b1 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x04b1 }
            java.lang.String r3 = "SupportedFunction"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x04b1 }
            r5.<init>()     // Catch:{ all -> 0x04b1 }
            java.lang.String r6 = "transferGoalTrackingSamplesToDetailChart - detailChartData="
            r5.append(r6)     // Catch:{ all -> 0x04b1 }
            r5.append(r2)     // Catch:{ all -> 0x04b1 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x04b1 }
            r0.d(r3, r5)     // Catch:{ all -> 0x04b1 }
            com.fossil.r87 r0 = new com.fossil.r87     // Catch:{ all -> 0x04b1 }
            r0.<init>(r2, r4)     // Catch:{ all -> 0x04b1 }
            monitor-exit(r35)
            return r0
        L_0x04b1:
            r0 = move-exception
            monitor-exit(r35)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tg6.a(java.util.Date, java.util.List):com.fossil.r87");
    }
}
