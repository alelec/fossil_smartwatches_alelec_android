package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class li4 implements sg4 {
    @DexIgnore
    public int a() {
        return 10;
    }

    @DexIgnore
    @Override // com.fossil.sg4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Negative size is not allowed. Input: " + i + 'x' + i2);
        } else {
            int a = a();
            if (map != null && map.containsKey(og4.MARGIN)) {
                a = Integer.parseInt(map.get(og4.MARGIN).toString());
            }
            return a(a(str), i, i2, a);
        }
    }

    @DexIgnore
    public abstract boolean[] a(String str);

    @DexIgnore
    public static dh4 a(boolean[] zArr, int i, int i2, int i3) {
        int length = zArr.length;
        int i4 = i3 + length;
        int max = Math.max(i, i4);
        int max2 = Math.max(1, i2);
        int i5 = max / i4;
        int i6 = (max - (length * i5)) / 2;
        dh4 dh4 = new dh4(max, max2);
        int i7 = 0;
        while (i7 < length) {
            if (zArr[i7]) {
                dh4.a(i6, 0, i5, max2);
            }
            i7++;
            i6 += i5;
        }
        return dh4;
    }

    @DexIgnore
    public static int a(boolean[] zArr, int i, int[] iArr, boolean z) {
        int i2 = 0;
        for (int i3 : iArr) {
            int i4 = 0;
            while (i4 < i3) {
                zArr[i] = z;
                i4++;
                i++;
            }
            i2 += i3;
            z = !z;
        }
        return i2;
    }
}
