package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q90 extends k60 implements Parcelable {
    @DexIgnore
    public /* final */ s90 a;

    @DexIgnore
    public q90(s90 s90) {
        this.a = s90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(new JSONObject(), r51.d, this.a);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final s90 getActionType() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a.ordinal());
    }

    @DexIgnore
    public q90(Parcel parcel) {
        this(s90.values()[parcel.readInt()]);
    }
}
