package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r04 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements t04<List<String>> {
        @DexIgnore
        public /* final */ List<String> a; // = uy3.a();

        @DexIgnore
        @Override // com.fossil.t04
        public boolean a(String str) {
            this.a.add(str);
            return true;
        }

        @DexIgnore
        @Override // com.fossil.t04
        public List<String> getResult() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends i04<File> {
        @DexIgnore
        public String toString() {
            return "Files.fileTreeTraverser()";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends m04 {
        @DexIgnore
        public /* final */ File a;

        @DexIgnore
        public /* synthetic */ c(File file, a aVar) {
            this(file);
        }

        @DexIgnore
        public String toString() {
            return "Files.asByteSource(" + this.a + ")";
        }

        @DexIgnore
        public c(File file) {
            jw3.a(file);
            this.a = file;
        }

        @DexIgnore
        @Override // com.fossil.m04
        public FileInputStream a() throws IOException {
            return new FileInputStream(this.a);
        }
    }

    /*
    static {
        new b();
    }
    */

    @DexIgnore
    public static m04 a(File file) {
        return new c(file, null);
    }

    @DexIgnore
    public static List<String> b(File file, Charset charset) throws IOException {
        return (List) a(file, charset, new a());
    }

    @DexIgnore
    public static n04 a(File file, Charset charset) {
        return a(file).a(charset);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T a(File file, Charset charset, t04<T> t04) throws IOException {
        return (T) a(file, charset).a(t04);
    }
}
