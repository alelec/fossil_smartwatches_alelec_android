package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import androidx.collection.SimpleArrayMap;
import com.facebook.internal.FetchedAppSettings;
import com.fossil.v94;
import java.io.IOException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class db4 {
    @DexIgnore
    public static int h;
    @DexIgnore
    public static PendingIntent i;
    @DexIgnore
    public /* final */ SimpleArrayMap<String, oo3<Bundle>> a; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ ta4 c;
    @DexIgnore
    public /* final */ ScheduledExecutorService d;
    @DexIgnore
    public Messenger e;
    @DexIgnore
    public Messenger f;
    @DexIgnore
    public v94 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends qg2 {
        @DexIgnore
        public a(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public void handleMessage(Message message) {
            db4.this.a(message);
        }
    }

    @DexIgnore
    public db4(Context context, ta4 ta4) {
        this.b = context;
        this.c = ta4;
        this.e = new Messenger(new a(Looper.getMainLooper()));
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(1);
        scheduledThreadPoolExecutor.setKeepAliveTime(60, TimeUnit.SECONDS);
        scheduledThreadPoolExecutor.allowCoreThreadTimeOut(true);
        this.d = scheduledThreadPoolExecutor;
    }

    @DexIgnore
    public static boolean d(Bundle bundle) {
        return bundle != null && bundle.containsKey("google.messenger");
    }

    @DexIgnore
    public static final /* synthetic */ no3 e(Bundle bundle) throws Exception {
        if (d(bundle)) {
            return qo3.a((Object) null);
        }
        return qo3.a(bundle);
    }

    @DexIgnore
    public final void a(Message message) {
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new v94.b());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof v94) {
                        this.g = (v94) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f = (Messenger) parcelableExtra;
                    }
                }
                a((Intent) message.obj);
                return;
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }

    @DexIgnore
    public void b(Intent intent) {
        String stringExtra = intent.getStringExtra("error");
        if (stringExtra == null) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 49);
            sb.append("Unexpected response, no error or registration id ");
            sb.append(valueOf);
            Log.w("FirebaseInstanceId", sb.toString());
            return;
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(stringExtra);
            Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Received InstanceID error ".concat(valueOf2) : new String("Received InstanceID error "));
        }
        if (stringExtra.startsWith("|")) {
            String[] split = stringExtra.split(FetchedAppSettings.DialogFeatureConfig.DIALOG_CONFIG_DIALOG_NAME_FEATURE_NAME_SEPARATOR);
            if (split.length <= 2 || !"ID".equals(split[1])) {
                String valueOf3 = String.valueOf(stringExtra);
                Log.w("FirebaseInstanceId", valueOf3.length() != 0 ? "Unexpected structured response ".concat(valueOf3) : new String("Unexpected structured response "));
                return;
            }
            String str = split[2];
            String str2 = split[3];
            if (str2.startsWith(":")) {
                str2 = str2.substring(1);
            }
            a(str, intent.putExtra("error", str2).getExtras());
            return;
        }
        synchronized (this.a) {
            for (int i2 = 0; i2 < this.a.size(); i2++) {
                a(this.a.c(i2), intent.getExtras());
            }
        }
    }

    @DexIgnore
    public final no3<Bundle> c(Bundle bundle) {
        if (!this.c.e()) {
            return qo3.a((Exception) new IOException("MISSING_INSTANCEID_SERVICE"));
        }
        return b(bundle).b(u94.a(), new za4(this, bundle));
    }

    @DexIgnore
    public static synchronized void a(Context context, Intent intent) {
        synchronized (db4.class) {
            if (i == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                i = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", i);
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, String str) {
        Intent intent = new Intent();
        intent.setPackage("com.google.android.gms");
        if (this.c.d() == 2) {
            intent.setAction("com.google.iid.TOKEN_REQUEST");
        } else {
            intent.setAction("com.google.android.c2dm.intent.REGISTER");
        }
        intent.putExtras(bundle);
        a(this.b, intent);
        a(intent, str);
    }

    @DexIgnore
    public final no3<Bundle> b(Bundle bundle) {
        String a2 = a();
        oo3<Bundle> oo3 = new oo3<>();
        synchronized (this.a) {
            this.a.put(a2, oo3);
        }
        a(bundle, a2);
        oo3.a().a(u94.a(), new bb4(this, a2, this.d.schedule(new ab4(oo3), 30, TimeUnit.SECONDS)));
        return oo3.a();
    }

    @DexIgnore
    public void a(Intent intent, String str) {
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 5);
        sb.append("|ID|");
        sb.append(str);
        sb.append("|");
        intent.putExtra("kid", sb.toString());
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(intent.getExtras());
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 8);
            sb2.append("Sending ");
            sb2.append(valueOf);
            Log.d("FirebaseInstanceId", sb2.toString());
        }
        intent.putExtra("google.messenger", this.e);
        if (!(this.f == null && this.g == null)) {
            Message obtain = Message.obtain();
            obtain.obj = intent;
            try {
                if (this.f != null) {
                    this.f.send(obtain);
                    return;
                } else {
                    this.g.a(obtain);
                    return;
                }
            } catch (RemoteException unused) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    Log.d("FirebaseInstanceId", "Messenger failed, fallback to startService");
                }
            }
        }
        if (this.c.d() == 2) {
            this.b.sendBroadcast(intent);
        } else {
            this.b.startService(intent);
        }
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        synchronized (this.a) {
            oo3<Bundle> remove = this.a.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                Log.w("FirebaseInstanceId", valueOf.length() != 0 ? "Missing callback for ".concat(valueOf) : new String("Missing callback for "));
                return;
            }
            remove.a(bundle);
        }
    }

    @DexIgnore
    public final void a(Intent intent) {
        String action = intent.getAction();
        if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
            String stringExtra = intent.getStringExtra("registration_id");
            if (stringExtra == null) {
                stringExtra = intent.getStringExtra("unregistered");
            }
            if (stringExtra == null) {
                b(intent);
                return;
            }
            Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
            if (matcher.matches()) {
                String group = matcher.group(1);
                String group2 = matcher.group(2);
                Bundle extras = intent.getExtras();
                extras.putString("registration_id", group2);
                a(group, extras);
            } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(stringExtra);
                Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Unexpected response string: ".concat(valueOf) : new String("Unexpected response string: "));
            }
        } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf2 = String.valueOf(action);
            Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Unexpected response action: ".concat(valueOf2) : new String("Unexpected response action: "));
        }
    }

    @DexIgnore
    public no3<Bundle> a(Bundle bundle) {
        if (this.c.c() >= 12000000) {
            return ma4.a(this.b).b(1, bundle).a(u94.a(), ya4.a);
        }
        return c(bundle);
    }

    @DexIgnore
    public static synchronized String a() {
        String num;
        synchronized (db4.class) {
            int i2 = h;
            h = i2 + 1;
            num = Integer.toString(i2);
        }
        return num;
    }

    @DexIgnore
    public final /* synthetic */ void a(String str, ScheduledFuture scheduledFuture, no3 no3) {
        synchronized (this.a) {
            this.a.remove(str);
        }
        scheduledFuture.cancel(false);
    }

    @DexIgnore
    public static final /* synthetic */ void a(oo3 oo3) {
        if (oo3.b((Exception) new IOException("TIMEOUT"))) {
            Log.w("FirebaseInstanceId", "No response");
        }
    }

    @DexIgnore
    public final /* synthetic */ no3 a(Bundle bundle, no3 no3) throws Exception {
        if (no3.e() && d((Bundle) no3.b())) {
            return b(bundle).a(u94.a(), cb4.a);
        }
        return no3;
    }

    @DexIgnore
    public static final /* synthetic */ Bundle a(no3 no3) throws Exception {
        if (no3.e()) {
            return (Bundle) no3.b();
        }
        if (Log.isLoggable("FirebaseInstanceId", 3)) {
            String valueOf = String.valueOf(no3.a());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
            sb.append("Error making request: ");
            sb.append(valueOf);
            Log.d("FirebaseInstanceId", sb.toString());
        }
        throw new IOException("SERVICE_NOT_AVAILABLE");
    }
}
