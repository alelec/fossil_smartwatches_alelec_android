package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "coil.RealImageLoader$loadData$2", f = "RealImageLoader.kt", l = {369, 385, 511}, m = "invokeSuspend")
public final class oq extends zb7 implements kd7<yi7, fb7<? super nr>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Object $data;
    @DexIgnore
    public /* final */ /* synthetic */ pr $fetcher;
    @DexIgnore
    public /* final */ /* synthetic */ Object $mappedData;
    @DexIgnore
    public /* final */ /* synthetic */ it $request;
    @DexIgnore
    public /* final */ /* synthetic */ qt $scale;
    @DexIgnore
    public /* final */ /* synthetic */ rt $size;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$11;
    @DexIgnore
    public Object L$12;
    @DexIgnore
    public Object L$13;
    @DexIgnore
    public Object L$14;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ nq this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oq(nq nqVar, it itVar, rt rtVar, qt qtVar, pr prVar, Object obj, Object obj2, fb7 fb7) {
        super(2, fb7);
        this.this$0 = nqVar;
        this.$request = itVar;
        this.$size = rtVar;
        this.$scale = qtVar;
        this.$fetcher = prVar;
        this.$mappedData = obj;
        this.$data = obj2;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        oq oqVar = new oq(this.this$0, this.$request, this.$size, this.$scale, this.$fetcher, this.$mappedData, this.$data, fb7);
        oqVar.p$ = (yi7) obj;
        return oqVar;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super nr> fb7) {
        return ((oq) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x016a  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01e3  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0256  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r19) {
        /*
            r18 = this;
            r7 = r18
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r7.label
            r8 = 3
            r9 = 2
            r2 = 1
            if (r1 == 0) goto L_0x008f
            if (r1 == r2) goto L_0x007f
            if (r1 == r9) goto L_0x0064
            if (r1 != r8) goto L_0x005c
            java.lang.Object r1 = r7.L$14
            android.graphics.Bitmap r1 = (android.graphics.Bitmap) r1
            java.lang.Object r1 = r7.L$13
            com.fossil.yt r1 = (com.fossil.yt) r1
            java.lang.Object r1 = r7.L$11
            java.util.Iterator r1 = (java.util.Iterator) r1
            java.lang.Object r2 = r7.L$10
            android.graphics.Bitmap r2 = (android.graphics.Bitmap) r2
            java.lang.Object r3 = r7.L$9
            android.graphics.Bitmap r3 = (android.graphics.Bitmap) r3
            java.lang.Object r3 = r7.L$8
            java.lang.Iterable r3 = (java.lang.Iterable) r3
            java.lang.Object r4 = r7.L$7
            com.fossil.yi7 r4 = (com.fossil.yi7) r4
            java.lang.Object r5 = r7.L$6
            com.fossil.oq r5 = (com.fossil.oq) r5
            java.lang.Object r6 = r7.L$5
            com.fossil.rt r6 = (com.fossil.rt) r6
            java.lang.Object r9 = r7.L$4
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r7.L$3
            com.fossil.nq r10 = (com.fossil.nq) r10
            java.lang.Object r11 = r7.L$2
            com.fossil.nr r11 = (com.fossil.nr) r11
            java.lang.Object r12 = r7.L$1
            com.fossil.ir r12 = (com.fossil.ir) r12
            java.lang.Object r13 = r7.L$0
            com.fossil.yi7 r13 = (com.fossil.yi7) r13
            com.fossil.t87.a(r19)
            r15 = r7
            r14 = r13
            r7 = r0
            r13 = r12
            r0 = 3
            r12 = r11
            r11 = r10
            r10 = r9
            r9 = r6
            r6 = r5
            r5 = r19
            goto L_0x0223
        L_0x005c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0064:
            java.lang.Object r1 = r7.L$3
            com.fossil.fr r1 = (com.fossil.fr) r1
            java.lang.Object r1 = r7.L$2
            com.fossil.or r1 = (com.fossil.or) r1
            java.lang.Object r2 = r7.L$1
            com.fossil.ir r2 = (com.fossil.ir) r2
            java.lang.Object r3 = r7.L$0
            com.fossil.yi7 r3 = (com.fossil.yi7) r3
            com.fossil.t87.a(r19)     // Catch:{ Exception -> 0x007c }
            r12 = r1
            r1 = r19
            goto L_0x012a
        L_0x007c:
            r0 = move-exception
            goto L_0x0144
        L_0x007f:
            java.lang.Object r1 = r7.L$1
            com.fossil.ir r1 = (com.fossil.ir) r1
            java.lang.Object r2 = r7.L$0
            com.fossil.yi7 r2 = (com.fossil.yi7) r2
            com.fossil.t87.a(r19)
            r11 = r1
            r10 = r2
            r1 = r19
            goto L_0x00cd
        L_0x008f:
            com.fossil.t87.a(r19)
            com.fossil.yi7 r10 = r7.p$
            com.fossil.nq r1 = r7.this$0
            com.fossil.rs r1 = r1.d
            com.fossil.it r3 = r7.$request
            com.fossil.rt r4 = r7.$size
            com.fossil.qt r5 = r7.$scale
            com.fossil.nq r6 = r7.this$0
            com.fossil.ys r6 = r6.f
            boolean r6 = r6.a()
            com.fossil.ir r11 = r1.a(r3, r4, r5, r6)
            com.fossil.pr r1 = r7.$fetcher
            com.fossil.nq r3 = r7.this$0
            com.fossil.rq r3 = r3.p
            java.lang.Object r4 = r7.$mappedData
            com.fossil.rt r5 = r7.$size
            r7.L$0 = r10
            r7.L$1 = r11
            r7.label = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r11
            r6 = r18
            java.lang.Object r1 = r1.a(r2, r3, r4, r5, r6)
            if (r1 != r0) goto L_0x00cd
            return r0
        L_0x00cd:
            r12 = r1
            com.fossil.or r12 = (com.fossil.or) r12
            boolean r1 = r12 instanceof com.fossil.vr
            if (r1 == 0) goto L_0x014e
            com.fossil.zi7.a(r10)     // Catch:{ Exception -> 0x0142 }
            com.fossil.it r1 = r7.$request     // Catch:{ Exception -> 0x0142 }
            boolean r1 = com.fossil.iu.a(r1)     // Catch:{ Exception -> 0x0142 }
            if (r1 == 0) goto L_0x00e2
            com.fossil.hr r1 = com.fossil.hr.c     // Catch:{ Exception -> 0x0142 }
            goto L_0x0105
        L_0x00e2:
            com.fossil.it r1 = r7.$request     // Catch:{ Exception -> 0x0142 }
            com.fossil.fr r1 = r1.f()     // Catch:{ Exception -> 0x0142 }
            if (r1 == 0) goto L_0x00eb
            goto L_0x0105
        L_0x00eb:
            com.fossil.nq r1 = r7.this$0     // Catch:{ Exception -> 0x0142 }
            com.fossil.jq r1 = r1.g     // Catch:{ Exception -> 0x0142 }
            java.lang.Object r2 = r7.$data     // Catch:{ Exception -> 0x0142 }
            r3 = r12
            com.fossil.vr r3 = (com.fossil.vr) r3     // Catch:{ Exception -> 0x0142 }
            com.fossil.ar7 r3 = r3.c()     // Catch:{ Exception -> 0x0142 }
            r4 = r12
            com.fossil.vr r4 = (com.fossil.vr) r4     // Catch:{ Exception -> 0x0142 }
            java.lang.String r4 = r4.b()     // Catch:{ Exception -> 0x0142 }
            com.fossil.fr r1 = r1.a(r2, r3, r4)     // Catch:{ Exception -> 0x0142 }
        L_0x0105:
            com.fossil.nq r2 = r7.this$0     // Catch:{ Exception -> 0x0142 }
            com.fossil.rq r2 = r2.p     // Catch:{ Exception -> 0x0142 }
            r3 = r12
            com.fossil.vr r3 = (com.fossil.vr) r3     // Catch:{ Exception -> 0x0142 }
            com.fossil.ar7 r3 = r3.c()     // Catch:{ Exception -> 0x0142 }
            com.fossil.rt r4 = r7.$size     // Catch:{ Exception -> 0x0142 }
            r7.L$0 = r10     // Catch:{ Exception -> 0x0142 }
            r7.L$1 = r11     // Catch:{ Exception -> 0x0142 }
            r7.L$2 = r12     // Catch:{ Exception -> 0x0142 }
            r7.L$3 = r1     // Catch:{ Exception -> 0x0142 }
            r7.label = r9     // Catch:{ Exception -> 0x0142 }
            r5 = r11
            r6 = r18
            java.lang.Object r1 = r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0142 }
            if (r1 != r0) goto L_0x0128
            return r0
        L_0x0128:
            r3 = r10
            r2 = r11
        L_0x012a:
            com.fossil.cr r1 = (com.fossil.cr) r1     // Catch:{ Exception -> 0x0142 }
            com.fossil.nr r4 = new com.fossil.nr
            android.graphics.drawable.Drawable r5 = r1.a()
            boolean r1 = r1.b()
            com.fossil.vr r12 = (com.fossil.vr) r12
            com.fossil.br r6 = r12.a()
            r4.<init>(r5, r1, r6)
            r11 = r2
            r10 = r3
            goto L_0x0155
        L_0x0142:
            r0 = move-exception
            r1 = r12
        L_0x0144:
            com.fossil.vr r1 = (com.fossil.vr) r1
            com.fossil.ar7 r1 = r1.c()
            com.fossil.iu.a(r1)
            throw r0
        L_0x014e:
            boolean r1 = r12 instanceof com.fossil.nr
            if (r1 == 0) goto L_0x0265
            r4 = r12
            com.fossil.nr r4 = (com.fossil.nr) r4
        L_0x0155:
            com.fossil.zi7.a(r10)
            com.fossil.nq r1 = r7.this$0
            com.fossil.it r2 = r7.$request
            java.util.List r2 = r2.v()
            com.fossil.rt r3 = r7.$size
            boolean r5 = r2.isEmpty()
            if (r5 == 0) goto L_0x016a
            goto L_0x024e
        L_0x016a:
            android.graphics.drawable.Drawable r5 = r4.d()
            boolean r5 = r5 instanceof android.graphics.drawable.BitmapDrawable
            if (r5 == 0) goto L_0x017d
            android.graphics.drawable.Drawable r5 = r4.d()
            android.graphics.drawable.BitmapDrawable r5 = (android.graphics.drawable.BitmapDrawable) r5
            android.graphics.Bitmap r5 = r5.getBitmap()
            goto L_0x01cd
        L_0x017d:
            r5 = 4
            com.fossil.cu r6 = com.fossil.cu.c
            boolean r6 = r6.a()
            if (r6 == 0) goto L_0x01bd
            com.fossil.cu r6 = com.fossil.cu.c
            int r6 = r6.b()
            if (r6 > r5) goto L_0x01bd
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = "Converting drawable of type "
            r6.append(r9)
            android.graphics.drawable.Drawable r9 = r4.d()
            java.lang.Class r9 = r9.getClass()
            java.lang.String r9 = r9.getCanonicalName()
            r6.append(r9)
            r9 = 32
            r6.append(r9)
            java.lang.String r9 = "to apply transformations: "
            r6.append(r9)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            java.lang.String r9 = "RealImageLoader"
            android.util.Log.println(r5, r9, r6)
        L_0x01bd:
            com.fossil.gr r5 = r1.e
            android.graphics.drawable.Drawable r6 = r4.d()
            android.graphics.Bitmap$Config r9 = r11.d()
            android.graphics.Bitmap r5 = r5.a(r6, r3, r9)
        L_0x01cd:
            java.util.Iterator r6 = r2.iterator()
            r9 = r3
            r12 = r4
            r15 = r7
            r4 = r10
            r14 = r4
            r13 = r11
            r11 = r1
            r3 = r2
            r10 = r3
            r2 = r5
            r1 = r6
            r6 = r15
        L_0x01dd:
            boolean r16 = r1.hasNext()
            if (r16 == 0) goto L_0x022d
            java.lang.Object r8 = r1.next()
            r7 = r8
            com.fossil.yt r7 = (com.fossil.yt) r7
            r17 = r0
            com.fossil.rq r0 = r11.p
            r19 = r0
            java.lang.String r0 = "bitmap"
            com.fossil.ee7.a(r5, r0)
            r15.L$0 = r14
            r15.L$1 = r13
            r15.L$2 = r12
            r15.L$3 = r11
            r15.L$4 = r10
            r15.L$5 = r9
            r15.L$6 = r6
            r15.L$7 = r4
            r15.L$8 = r3
            r15.L$9 = r5
            r15.L$10 = r2
            r15.L$11 = r1
            r15.L$12 = r8
            r15.L$13 = r7
            r15.L$14 = r5
            r0 = 3
            r15.label = r0
            r8 = r19
            java.lang.Object r5 = r7.a(r8, r5, r9, r6)
            r7 = r17
            if (r5 != r7) goto L_0x0223
            return r7
        L_0x0223:
            android.graphics.Bitmap r5 = (android.graphics.Bitmap) r5
            com.fossil.zi7.a(r4)
            r8 = 3
            r0 = r7
            r7 = r18
            goto L_0x01dd
        L_0x022d:
            java.lang.String r0 = "transformedBitmap"
            com.fossil.ee7.a(r5, r0)
            android.content.Context r0 = r11.i
            android.content.res.Resources r0 = r0.getResources()
            java.lang.String r1 = "context.resources"
            com.fossil.ee7.a(r0, r1)
            android.graphics.drawable.BitmapDrawable r13 = new android.graphics.drawable.BitmapDrawable
            r13.<init>(r0, r5)
            r14 = 0
            r15 = 0
            r16 = 6
            r17 = 0
            com.fossil.nr r4 = com.fossil.nr.a(r12, r13, r14, r15, r16, r17)
        L_0x024e:
            android.graphics.drawable.Drawable r0 = r4.d()
            boolean r1 = r0 instanceof android.graphics.drawable.BitmapDrawable
            if (r1 != 0) goto L_0x0257
            r0 = 0
        L_0x0257:
            android.graphics.drawable.BitmapDrawable r0 = (android.graphics.drawable.BitmapDrawable) r0
            if (r0 == 0) goto L_0x0264
            android.graphics.Bitmap r0 = r0.getBitmap()
            if (r0 == 0) goto L_0x0264
            r0.prepareToDraw()
        L_0x0264:
            return r4
        L_0x0265:
            com.fossil.p87 r0 = new com.fossil.p87
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.oq.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
