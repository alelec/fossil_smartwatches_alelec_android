package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.fossil.tl5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.EmptyFirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.LabelRepository;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl5 extends fl4<h, j, i> {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ c t; // = new c(null);
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public Device f;
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public boolean h;
    @DexIgnore
    public k i;
    @DexIgnore
    public /* final */ g j; // = new g();
    @DexIgnore
    public /* final */ DeviceRepository k;
    @DexIgnore
    public /* final */ tl5 l;
    @DexIgnore
    public /* final */ ch5 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ aw6 o;
    @DexIgnore
    public /* final */ ad5 p;
    @DexIgnore
    public /* final */ LabelRepository q;
    @DexIgnore
    public /* final */ FileRepository r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends j {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public a(String str) {
            ee7.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends j {
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public b(String str, boolean z) {
            ee7.b(str, "serial");
            this.a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public final String a() {
            return wl5.s;
        }

        @DexIgnore
        public /* synthetic */ c(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(int i, String str, String str2) {
            super(i, str, str2);
            ee7.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str, String str2) {
            super(-1, str, str2);
            ee7.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends j {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public f(String str) {
            ee7.b(str, "serial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class g implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
            ServiceActionResult serviceActionResult = ServiceActionResult.values()[intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), ServiceActionResult.UNALLOWED_ACTION.ordinal())];
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = wl5.t.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + wl5.this.j() + ", isSuccess=" + intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1));
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, wl5.this.h(), true) && communicateMode == CommunicateMode.LINK && wl5.this.j()) {
                switch (xl5.a[serviceActionResult.ordinal()]) {
                    case 1:
                        wl5 wl5 = wl5.this;
                        Bundle extras = intent.getExtras();
                        if (extras != null) {
                            String string = extras.getString("device_model", "");
                            ee7.a((Object) string, "intent.extras!!.getStrin\u2026nstants.DEVICE_MODEL, \"\")");
                            wl5.b(string);
                            wl5 wl52 = wl5.this;
                            if (stringExtra != null) {
                                wl52.a(new a(stringExtra));
                                wl5.this.m();
                                return;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    case 2:
                        wl5 wl53 = wl5.this;
                        if (stringExtra != null) {
                            wl53.a(new f(stringExtra));
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 3:
                        wl5 wl54 = wl5.this;
                        if (stringExtra != null) {
                            wl54.a(new m(stringExtra, true));
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 4:
                        wl5 wl55 = wl5.this;
                        if (stringExtra != null) {
                            wl55.a(new m(stringExtra, false));
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 5:
                        wl5 wl56 = wl5.this;
                        if (stringExtra != null) {
                            wl56.a(new b(stringExtra, true));
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 6:
                        wl5 wl57 = wl5.this;
                        if (stringExtra != null) {
                            wl57.a(new b(stringExtra, false));
                            return;
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 7:
                        FLogger.INSTANCE.getLocal().d(wl5.t.a(), "onReceive(), ASK_FOR_LINK_SERVER");
                        Bundle extras2 = intent.getExtras();
                        if (extras2 != null) {
                            MisfitDeviceProfile misfitDeviceProfile = (MisfitDeviceProfile) extras2.getParcelable("device");
                            if (misfitDeviceProfile != null) {
                                wl5.this.d(misfitDeviceProfile.getDeviceSerial());
                                wl5.this.c(misfitDeviceProfile.getAddress());
                                wl5.this.a(new Device(misfitDeviceProfile.getDeviceSerial(), misfitDeviceProfile.getAddress(), misfitDeviceProfile.getDeviceModel(), misfitDeviceProfile.getFirmwareVersion(), misfitDeviceProfile.getBatteryLevel(), 50, false, 64, null));
                                Device e = wl5.this.e();
                                if (e != null) {
                                    e.appendAdditionalInfo(misfitDeviceProfile);
                                }
                                wl5.this.d();
                                return;
                            }
                            wl5 wl58 = wl5.this;
                            String h = wl58.h();
                            if (h != null) {
                                wl58.a(new d(FailureCode.UNKNOWN_ERROR, h, "No device profile"));
                                PortfolioApp c = PortfolioApp.g0.c();
                                String h2 = wl5.this.h();
                                if (h2 != null) {
                                    c.a(h2);
                                    return;
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 8:
                        FLogger.INSTANCE.getLocal().d(wl5.t.a(), "onReceive(), ASK_FOR_LABEL_FILE");
                        wl5 wl59 = wl5.this;
                        ee7.a((Object) stringExtra, "serial");
                        wl59.a(stringExtra);
                        return;
                    case 9:
                        wl5.this.a(false);
                        PortfolioApp c2 = PortfolioApp.g0.c();
                        if (stringExtra != null) {
                            c2.c(stringExtra, wl5.this.g());
                            wl5 wl510 = wl5.this;
                            Device e2 = wl510.e();
                            if (e2 != null) {
                                wl510.a(new l(e2));
                                return;
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    case 10:
                        wl5.this.a(false);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = wl5.t.a();
                        local2.d(a3, "Pair device failed due to " + intExtra + ", remove this device on button service");
                        try {
                            PortfolioApp c3 = PortfolioApp.g0.c();
                            String h3 = wl5.this.h();
                            if (h3 != null) {
                                c3.m(h3);
                                PortfolioApp c4 = PortfolioApp.g0.c();
                                String h4 = wl5.this.h();
                                if (h4 != null) {
                                    c4.l(h4);
                                    if (intExtra > 1000) {
                                        wl5 wl511 = wl5.this;
                                        String h5 = wl511.h();
                                        if (h5 != null) {
                                            wl511.a(new d(intExtra, h5, ""));
                                            return;
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    } else {
                                        wl5 wl512 = wl5.this;
                                        String h6 = wl512.h();
                                        if (h6 != null) {
                                            wl512.a(new k(intExtra, h6, ""));
                                            wl5 wl513 = wl5.this;
                                            k i = wl513.i();
                                            if (i != null) {
                                                wl513.a((fl4.a) i);
                                                wl5.this.a((k) null);
                                                return;
                                            }
                                            ee7.a();
                                            throw null;
                                        }
                                        ee7.a();
                                        throw null;
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } catch (Exception e3) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String a4 = wl5.t.a();
                            local3.d(a4, "Pair device failed, remove this device on button service exception=" + e3.getMessage());
                        }
                    default:
                        return;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public h(String str, String str2) {
            ee7.b(str, "device");
            ee7.b(str2, "macAddress");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public i(int i, String str, String str2) {
            ee7.b(str, "deviceId");
            this.a = i;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class j implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends i {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(int i, String str, String str2) {
            super(i, str, str2);
            ee7.b(str, "deviceId");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l extends j {
        @DexIgnore
        public /* final */ Device a;

        @DexIgnore
        public l(Device device) {
            ee7.b(device, "device");
            this.a = device;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends j {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public m(String str, boolean z) {
            ee7.b(str, "serial");
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements fl4.e<fs6, ds6> {
        @DexIgnore
        public /* final */ /* synthetic */ wl5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public n(wl5 wl5) {
            this.a = wl5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fs6 fs6) {
            ee7.b(fs6, "responseValue");
            FLogger.INSTANCE.getLocal().d(wl5.t.a(), " get device setting success");
            this.a.l();
        }

        @DexIgnore
        public void a(ds6 ds6) {
            ee7.b(ds6, "errorValue");
            FLogger.INSTANCE.getLocal().d(wl5.t.a(), " get device setting fail!!");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String h = this.a.h();
            if (h != null) {
                String a2 = wl5.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Get device setting of ");
                String h2 = this.a.h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(", server error=");
                    sb.append(ds6.a());
                    sb.append(", error = ");
                    sb.append(ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.LINK_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.NETWORK_ERROR));
                    remote.e(component, session, h, a2, sb.toString());
                    wl5 wl5 = this.a;
                    int a3 = ds6.a();
                    String h3 = this.a.h();
                    if (h3 != null) {
                        wl5.a(new k(a3, h3, ""));
                        PortfolioApp c = PortfolioApp.g0.c();
                        String h4 = this.a.h();
                        if (h4 != null) {
                            c.a(h4, PairingResponse.CREATOR.buildPairingLinkServerResponse(false, ds6.a()));
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$downloadLabelFile$1", f = "LinkDeviceUseCase.kt", l = {284, 293}, m = "invokeSuspend")
    public static final class o extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(wl5 wl5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = wl5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            o oVar = new o(this.this$0, this.$serial, fb7);
            oVar.p$ = (yi7) obj;
            return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((o) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00e4  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00e6  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
                r13 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r13.label
                r2 = 2
                r3 = 1
                java.lang.String r4 = "0.0"
                if (r1 == 0) goto L_0x0035
                if (r1 == r3) goto L_0x002d
                if (r1 != r2) goto L_0x0025
                java.lang.Object r0 = r13.L$3
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r0 = r13.L$2
                com.portfolio.platform.data.source.local.label.Label r0 = (com.portfolio.platform.data.source.local.label.Label) r0
                java.lang.Object r1 = r13.L$1
                com.fossil.zi5 r1 = (com.fossil.zi5) r1
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x00dc
            L_0x0025:
                java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r14.<init>(r0)
                throw r14
            L_0x002d:
                java.lang.Object r1 = r13.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r14)
                goto L_0x004d
            L_0x0035:
                com.fossil.t87.a(r14)
                com.fossil.yi7 r1 = r13.p$
                com.fossil.wl5 r14 = r13.this$0
                com.portfolio.platform.data.source.LabelRepository r14 = r14.q
                java.lang.String r5 = r13.$serial
                r13.L$0 = r1
                r13.label = r3
                java.lang.Object r14 = r14.downloadLabel(r5, r13)
                if (r14 != r0) goto L_0x004d
                return r0
            L_0x004d:
                com.fossil.zi5 r14 = (com.fossil.zi5) r14
                boolean r3 = r14 instanceof com.fossil.bj5
                java.lang.String r5 = "downloadLabel for serial = "
                if (r3 == 0) goto L_0x011d
                com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
                com.fossil.wl5$c r6 = com.fossil.wl5.t
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                r7.append(r5)
                java.lang.String r5 = r13.$serial
                r7.append(r5)
                java.lang.String r5 = " Success"
                r7.append(r5)
                java.lang.String r5 = r7.toString()
                r3.d(r6, r5)
                r3 = r14
                com.fossil.bj5 r3 = (com.fossil.bj5) r3
                java.lang.Object r3 = r3.a()
                com.portfolio.platform.data.source.local.label.Label r3 = (com.portfolio.platform.data.source.local.label.Label) r3
                if (r3 == 0) goto L_0x0109
                com.portfolio.platform.data.source.local.label.Data r5 = r3.getData()
                java.lang.String r7 = r5.getUrl()
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                com.fossil.wl5$c r6 = com.fossil.wl5.t
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "downloadLabel labelUrl = "
                r8.append(r9)
                r8.append(r7)
                java.lang.String r8 = r8.toString()
                r5.d(r6, r8)
                boolean r5 = com.fossil.mh7.a(r7)
                if (r5 != 0) goto L_0x0109
                com.fossil.wl5 r5 = r13.this$0
                com.portfolio.platform.data.source.FileRepository r5 = r5.r
                com.misfit.frameworks.buttonservice.model.FileType r6 = com.misfit.frameworks.buttonservice.model.FileType.LABEL
                r5.deletedFilesByType(r6)
                com.fossil.wl5 r5 = r13.this$0
                com.portfolio.platform.data.source.FileRepository r6 = r5.r
                com.misfit.frameworks.buttonservice.model.FileType r8 = com.misfit.frameworks.buttonservice.model.FileType.LABEL
                r9 = 0
                r11 = 4
                r12 = 0
                r13.L$0 = r1
                r13.L$1 = r14
                r13.L$2 = r3
                r13.L$3 = r7
                r13.label = r2
                r10 = r13
                java.lang.Object r14 = com.portfolio.platform.data.source.FileRepository.downloadFromUrl$default(r6, r7, r8, r9, r10, r11, r12)
                if (r14 != r0) goto L_0x00db
                return r0
            L_0x00db:
                r0 = r3
            L_0x00dc:
                java.lang.Boolean r14 = (java.lang.Boolean) r14
                boolean r14 = r14.booleanValue()
                if (r14 == 0) goto L_0x00e6
                r14 = 0
                goto L_0x00e8
            L_0x00e6:
                r14 = 1938(0x792, float:2.716E-42)
            L_0x00e8:
                com.portfolio.platform.data.source.local.label.Metadata r0 = r0.getMetadata()
                if (r0 == 0) goto L_0x00f5
                java.lang.String r0 = r0.getVersion()
                if (r0 == 0) goto L_0x00f5
                r4 = r0
            L_0x00f5:
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                java.lang.String r1 = r13.$serial
                com.misfit.frameworks.buttonservice.model.pairing.PairingResponse$CREATOR r2 = com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR
                com.misfit.frameworks.buttonservice.model.pairing.LabelResponse r14 = r2.buildLabelResponse(r4, r14)
                r0.a(r1, r14)
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            L_0x0109:
                com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r14 = r14.c()
                java.lang.String r0 = r13.$serial
                com.misfit.frameworks.buttonservice.model.pairing.PairingResponse$CREATOR r1 = com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR
                r2 = 10000(0x2710, float:1.4013E-41)
                com.misfit.frameworks.buttonservice.model.pairing.LabelResponse r1 = r1.buildLabelResponse(r4, r2)
                r14.a(r0, r1)
                goto L_0x015d
            L_0x011d:
                boolean r0 = r14 instanceof com.fossil.yi5
                if (r0 == 0) goto L_0x015d
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.wl5$c r1 = com.fossil.wl5.t
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                r2.append(r5)
                java.lang.String r3 = r13.$serial
                r2.append(r3)
                java.lang.String r3 = " Fail"
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                r0.d(r1, r2)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                java.lang.String r1 = r13.$serial
                com.misfit.frameworks.buttonservice.model.pairing.PairingResponse$CREATOR r2 = com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR
                com.fossil.yi5 r14 = (com.fossil.yi5) r14
                int r14 = r14.a()
                com.misfit.frameworks.buttonservice.model.pairing.LabelResponse r14 = r2.buildLabelResponse(r4, r14)
                r0.a(r1, r14)
            L_0x015d:
                com.fossil.i97 r14 = com.fossil.i97.a
                return r14
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wl5.o.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $deviceModel;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ wl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(Device device, fb7 fb7, wl5 wl5) {
            super(2, fb7);
            this.$deviceModel = device;
            this.this$0 = wl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            p pVar = new p(this.$deviceModel, fb7, this.this$0);
            pVar.p$ = (yi7) obj;
            return pVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((p) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0176  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x01c2  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0217  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r18) {
            /*
                r17 = this;
                r0 = r17
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 4
                r4 = 3
                r5 = 2
                java.lang.String r6 = "Steal a device with serial "
                r7 = 1
                if (r2 == 0) goto L_0x0079
                if (r2 == r7) goto L_0x006f
                if (r2 == r5) goto L_0x005e
                if (r2 == r4) goto L_0x003f
                if (r2 != r3) goto L_0x0037
                java.lang.Object r1 = r0.L$5
                com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
                java.lang.Object r1 = r0.L$4
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r1 = r0.L$3
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r0.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r0.L$1
                com.fossil.zi5 r1 = (com.fossil.zi5) r1
                java.lang.Object r1 = r0.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r18)
                r2 = r18
                goto L_0x0213
            L_0x0037:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x003f:
                java.lang.Object r2 = r0.L$4
                java.lang.String r2 = (java.lang.String) r2
                java.lang.Object r4 = r0.L$3
                com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
                java.lang.Object r5 = r0.L$2
                com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
                java.lang.Object r6 = r0.L$1
                com.fossil.zi5 r6 = (com.fossil.zi5) r6
                java.lang.Object r7 = r0.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r18)
                r8 = r7
                r7 = r6
                r6 = r5
                r5 = r4
                r4 = r18
                goto L_0x01bc
            L_0x005e:
                java.lang.Object r2 = r0.L$1
                com.fossil.zi5 r2 = (com.fossil.zi5) r2
                java.lang.Object r5 = r0.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r18)
                r6 = r2
                r7 = r5
                r5 = r18
                goto L_0x0171
            L_0x006f:
                java.lang.Object r2 = r0.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r18)
                r8 = r18
                goto L_0x00e7
            L_0x0079:
                com.fossil.t87.a(r18)
                com.fossil.yi7 r2 = r0.p$
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r9 = r8.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r10 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API
                com.misfit.frameworks.buttonservice.log.FLogger$Session r11 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR
                com.portfolio.platform.data.model.Device r8 = r0.$deviceModel
                java.lang.String r12 = r8.getDeviceId()
                com.fossil.wl5$c r8 = com.fossil.wl5.t
                java.lang.String r13 = r8.a()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                r8.append(r6)
                com.portfolio.platform.data.model.Device r14 = r0.$deviceModel
                java.lang.String r14 = r14.getDeviceId()
                r8.append(r14)
                java.lang.String r14 = r8.toString()
                r9.i(r10, r11, r12, r13, r14)
                com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r8 = r8.c()
                com.misfit.frameworks.buttonservice.communite.CommunicateMode r9 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.LINK
                com.portfolio.platform.data.model.Device r10 = r0.$deviceModel
                java.lang.String r10 = r10.getDeviceId()
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "Force link a device with serial "
                r11.append(r12)
                com.portfolio.platform.data.model.Device r12 = r0.$deviceModel
                java.lang.String r12 = r12.getDeviceId()
                r11.append(r12)
                java.lang.String r11 = r11.toString()
                r8.a(r9, r10, r11)
                com.fossil.wl5 r8 = r0.this$0
                com.portfolio.platform.data.source.DeviceRepository r8 = r8.k
                com.portfolio.platform.data.model.Device r9 = r0.$deviceModel
                r0.L$0 = r2
                r0.label = r7
                java.lang.Object r8 = r8.forceLinkDevice(r9, r0)
                if (r8 != r1) goto L_0x00e7
                return r1
            L_0x00e7:
                com.fossil.zi5 r8 = (com.fossil.zi5) r8
                boolean r9 = r8 instanceof com.fossil.bj5
                r10 = 0
                if (r9 == 0) goto L_0x0243
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r11 = r9.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r12 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API
                com.misfit.frameworks.buttonservice.log.FLogger$Session r13 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR
                com.portfolio.platform.data.model.Device r9 = r0.$deviceModel
                java.lang.String r14 = r9.getDeviceId()
                com.fossil.wl5$c r9 = com.fossil.wl5.t
                java.lang.String r15 = r9.a()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                r9.append(r6)
                com.portfolio.platform.data.model.Device r6 = r0.$deviceModel
                java.lang.String r6 = r6.getDeviceId()
                r9.append(r6)
                java.lang.String r6 = " success"
                r9.append(r6)
                java.lang.String r16 = r9.toString()
                r11.i(r12, r13, r14, r15, r16)
                com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                com.fossil.wl5$c r9 = com.fossil.wl5.t
                java.lang.String r9 = r9.a()
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "forceLinkDevice onSuccess device="
                r11.append(r12)
                com.portfolio.platform.data.model.Device r12 = r0.$deviceModel
                java.lang.String r12 = r12.getDeviceId()
                r11.append(r12)
                java.lang.String r11 = r11.toString()
                r6.d(r9, r11)
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                com.portfolio.platform.data.model.Device r9 = r0.$deviceModel
                java.lang.String r9 = r9.getDeviceId()
                com.misfit.frameworks.buttonservice.model.pairing.PairingResponse$CREATOR r11 = com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR
                com.misfit.frameworks.buttonservice.model.pairing.PairingLinkServerResponse r7 = r11.buildPairingLinkServerResponse(r7, r10)
                r6.a(r9, r7)
                com.fossil.wl5 r6 = r0.this$0
                com.portfolio.platform.data.source.UserRepository r6 = r6.n
                r0.L$0 = r2
                r0.L$1 = r8
                r0.label = r5
                java.lang.Object r5 = r6.getCurrentUser(r0)
                if (r5 != r1) goto L_0x016f
                return r1
            L_0x016f:
                r7 = r2
                r6 = r8
            L_0x0171:
                r2 = r5
                com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
                if (r2 == 0) goto L_0x02ea
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r8 = r2.getUserId()
                r5.append(r8)
                r8 = 58
                r5.append(r8)
                com.portfolio.platform.data.model.Device r8 = r0.$deviceModel
                java.lang.String r8 = r8.getDeviceId()
                r5.append(r8)
                java.lang.String r5 = r5.toString()
                com.fossil.wl5 r8 = r0.this$0
                com.fossil.aw6 r8 = r8.o
                com.fossil.aw6$b r9 = new com.fossil.aw6$b
                com.fossil.pb5 r10 = new com.fossil.pb5
                r10.<init>()
                r9.<init>(r5, r10)
                r0.L$0 = r7
                r0.L$1 = r6
                r0.L$2 = r2
                r0.L$3 = r2
                r0.L$4 = r5
                r0.label = r4
                java.lang.Object r4 = com.fossil.gl4.a(r8, r9, r0)
                if (r4 != r1) goto L_0x01b7
                return r1
            L_0x01b7:
                r8 = r7
                r7 = r6
                r6 = r2
                r2 = r5
                r5 = r6
            L_0x01bc:
                com.fossil.fl4$c r4 = (com.fossil.fl4.c) r4
                boolean r9 = r4 instanceof com.fossil.aw6.d
                if (r9 == 0) goto L_0x0217
                com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
                com.fossil.wl5$c r10 = com.fossil.wl5.t
                java.lang.String r10 = r10.a()
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                java.lang.String r12 = "Push secret key "
                r11.append(r12)
                r12 = r4
                com.fossil.aw6$d r12 = (com.fossil.aw6.d) r12
                java.lang.String r13 = r12.a()
                r11.append(r13)
                java.lang.String r13 = " to server after pair device success"
                r11.append(r13)
                java.lang.String r11 = r11.toString()
                r9.d(r10, r11)
                com.fossil.wl5 r9 = r0.this$0
                com.portfolio.platform.data.source.DeviceRepository r9 = r9.k
                com.portfolio.platform.data.model.Device r10 = r0.$deviceModel
                java.lang.String r10 = r10.getDeviceId()
                java.lang.String r11 = r12.a()
                r0.L$0 = r8
                r0.L$1 = r7
                r0.L$2 = r6
                r0.L$3 = r5
                r0.L$4 = r2
                r0.L$5 = r4
                r0.label = r3
                java.lang.Object r2 = r9.updateDeviceSecretKey(r10, r11, r0)
                if (r2 != r1) goto L_0x0213
                return r1
            L_0x0213:
                com.fossil.zi5 r2 = (com.fossil.zi5) r2
                goto L_0x02ea
            L_0x0217:
                boolean r1 = r4 instanceof com.fossil.aw6.c
                if (r1 == 0) goto L_0x02ea
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.wl5$c r2 = com.fossil.wl5.t
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r5 = "Push secret key fail due to "
                r3.append(r5)
                com.fossil.aw6$c r4 = (com.fossil.aw6.c) r4
                int r4 = r4.a()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                goto L_0x02ea
            L_0x0243:
                boolean r1 = r8 instanceof com.fossil.yi5
                if (r1 == 0) goto L_0x02ea
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r11 = r1.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r12 = com.misfit.frameworks.buttonservice.log.FLogger.Component.API
                com.misfit.frameworks.buttonservice.log.FLogger$Session r13 = com.misfit.frameworks.buttonservice.log.FLogger.Session.PAIR
                com.portfolio.platform.data.model.Device r1 = r0.$deviceModel
                java.lang.String r14 = r1.getDeviceId()
                com.fossil.wl5$c r1 = com.fossil.wl5.t
                java.lang.String r15 = r1.a()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r1.append(r6)
                com.portfolio.platform.data.model.Device r2 = r0.$deviceModel
                java.lang.String r2 = r2.getDeviceId()
                r1.append(r2)
                java.lang.String r2 = ", server error="
                r1.append(r2)
                com.fossil.yi5 r8 = (com.fossil.yi5) r8
                int r2 = r8.a()
                r1.append(r2)
                java.lang.String r2 = ", error = "
                r1.append(r2)
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder r2 = com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.INSTANCE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder$Step r3 = com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Step.LINK_DEVICE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder$Component r4 = com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.Component.APP
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder$AppError r5 = com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder.AppError.NETWORK_ERROR
                java.lang.String r2 = r2.build(r3, r4, r5)
                r1.append(r2)
                java.lang.String r16 = r1.toString()
                r11.e(r12, r13, r14, r15, r16)
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.fossil.wl5$c r2 = com.fossil.wl5.t
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "forceLinkDevice onFail errorCode="
                r3.append(r4)
                int r4 = r8.a()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r1.d(r2, r3)
                com.fossil.wl5 r1 = r0.this$0
                com.fossil.wl5$k r2 = new com.fossil.wl5$k
                int r3 = r8.a()
                com.portfolio.platform.data.model.Device r4 = r0.$deviceModel
                java.lang.String r4 = r4.getDeviceId()
                java.lang.String r5 = ""
                r2.<init>(r3, r4, r5)
                r1.a(r2)
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                com.portfolio.platform.data.model.Device r2 = r0.$deviceModel
                java.lang.String r2 = r2.getDeviceId()
                com.misfit.frameworks.buttonservice.model.pairing.PairingResponse$CREATOR r3 = com.misfit.frameworks.buttonservice.model.pairing.PairingResponse.CREATOR
                int r4 = r8.a()
                com.misfit.frameworks.buttonservice.model.pairing.PairingLinkServerResponse r3 = r3.buildPairingLinkServerResponse(r10, r4)
                r1.a(r2, r3)
            L_0x02ea:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wl5.p.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase", f = "LinkDeviceUseCase.kt", l = {173}, m = "run")
    public static final class q extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ wl5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(wl5 wl5, fb7 fb7) {
            super(fb7);
            this.this$0 = wl5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((h) null, (fb7<Object>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements fl4.e<tl5.d, tl5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ wl5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.LinkDeviceUseCase$sendLatestFWResponseToDevice$1$onSuccess$1", f = "LinkDeviceUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ tl5.d $responseValue;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ r this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(r rVar, tl5.d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = rVar;
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$responseValue, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    String a = this.$responseValue.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = wl5.t.a();
                    local.d(a2, "checkFirmware - downloadFw SUCCESS, latestFwVersion=" + a);
                    FirmwareData a3 = pm5.g.a(this.this$0.a.m, this.this$0.a.f());
                    if (a3 == null) {
                        a3 = new EmptyFirmwareData();
                    }
                    PortfolioApp c = PortfolioApp.g0.c();
                    String h = this.this$0.a.h();
                    if (h != null) {
                        c.a(h, PairingResponse.CREATOR.buildPairingUpdateFWResponse(a3));
                        return i97.a;
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public r(wl5 wl5) {
            this.a = wl5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tl5.d dVar) {
            ee7.b(dVar, "responseValue");
            ik7 unused = xh7.b(this.a.b(), null, null, new a(this, dVar, null), 3, null);
        }

        @DexIgnore
        public void a(tl5.c cVar) {
            ee7.b(cVar, "errorValue");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.API;
            FLogger.Session session = FLogger.Session.PAIR;
            String h = this.a.h();
            if (h != null) {
                String a2 = wl5.t.a();
                StringBuilder sb = new StringBuilder();
                sb.append(" downloadFw FAILED!!!, latestFwVersion=");
                String h2 = this.a.h();
                if (h2 != null) {
                    sb.append(h2);
                    sb.append(" but device is DianaEV1!!!");
                    remote.e(component, session, h, a2, sb.toString());
                    FLogger.INSTANCE.getLocal().e(wl5.t.a(), "checkFirmware - downloadFw FAILED!!!");
                    wl5 wl5 = this.a;
                    String h3 = wl5.h();
                    if (h3 != null) {
                        wl5.a(new e(h3, ""));
                        PortfolioApp c = PortfolioApp.g0.c();
                        String h4 = this.a.h();
                        if (h4 != null) {
                            c.a(h4);
                            this.a.a(false);
                            return;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    /*
    static {
        String simpleName = wl5.class.getSimpleName();
        ee7.a((Object) simpleName, "LinkDeviceUseCase::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public wl5(DeviceRepository deviceRepository, tl5 tl5, ch5 ch5, UserRepository userRepository, aw6 aw6, ad5 ad5, LabelRepository labelRepository, FileRepository fileRepository) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(tl5, "mDownloadFwByDeviceModel");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(aw6, "mDecryptValueKeyStoreUseCase");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(labelRepository, "mLabelRepository");
        ee7.b(fileRepository, "mFileRepository");
        this.k = deviceRepository;
        this.l = tl5;
        this.m = ch5;
        this.n = userRepository;
        this.o = aw6;
        this.p = ad5;
        this.q = labelRepository;
        this.r = fileRepository;
    }

    @DexIgnore
    public final String h() {
        return this.d;
    }

    @DexIgnore
    public final k i() {
        return this.i;
    }

    @DexIgnore
    public final boolean j() {
        return this.h;
    }

    @DexIgnore
    public final void k() {
        nj5.d.a(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    public final synchronized void l() {
        Device device = this.f;
        if (device != null) {
            ik7 unused = xh7.b(b(), null, null, new p(device, null, this), 3, null);
        }
    }

    @DexIgnore
    public final void m() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = s;
        local.d(str, "sendLatestFWResponseToDevice(), deviceModel=" + this.g + ", isSkipOTA=" + this.m.a0());
        if (PortfolioApp.g0.c().G() || !this.m.a0()) {
            this.l.a(new tl5.b(this.g), new r(this));
            return;
        }
        PortfolioApp c2 = PortfolioApp.g0.c();
        String str2 = this.d;
        if (str2 != null) {
            c2.a(str2, PairingResponse.CREATOR.buildPairingUpdateFWResponse(new SkipFirmwareData()));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void n() {
        nj5.d.b(this.j, CommunicateMode.LINK);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(h hVar, fb7 fb7) {
        return a(hVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "<set-?>");
        this.g = str;
    }

    @DexIgnore
    public final void c(String str) {
        this.e = str;
    }

    @DexIgnore
    public final void d(String str) {
        this.d = str;
    }

    @DexIgnore
    public final Device e() {
        return this.f;
    }

    @DexIgnore
    public final String f() {
        return this.g;
    }

    @DexIgnore
    public final String g() {
        return this.e;
    }

    @DexIgnore
    public final void a(Device device) {
        this.f = device;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return s;
    }

    @DexIgnore
    public final synchronized void d() {
        ad5 ad5 = this.p;
        String str = this.d;
        if (str != null) {
            fl4<es6, fs6, ds6> a2 = ad5.a(str);
            String str2 = this.d;
            if (str2 != null) {
                a2.a(new es6(str2), new n(this));
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        this.h = z;
    }

    @DexIgnore
    public final void a(k kVar) {
        this.i = kVar;
    }

    @DexIgnore
    public final void a(ShineDevice shineDevice, fl4.e<? super j, ? super i> eVar) {
        ee7.b(shineDevice, "closestDevice");
        ee7.b(eVar, "caseCallback");
        FLogger.INSTANCE.getLocal().d(s, "onRetrieveLinkAction");
        this.h = true;
        this.d = shineDevice.getSerial();
        this.e = shineDevice.getMacAddress();
        a(eVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.wl5.h r7, com.fossil.fb7<java.lang.Object> r8) {
        /*
            r6 = this;
            boolean r0 = r8 instanceof com.fossil.wl5.q
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.wl5$q r0 = (com.fossil.wl5.q) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.wl5$q r0 = new com.fossil.wl5$q
            r0.<init>(r6, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            java.lang.Object r7 = r0.L$1
            com.fossil.wl5$h r7 = (com.fossil.wl5.h) r7
            java.lang.Object r7 = r0.L$0
            com.fossil.wl5 r7 = (com.fossil.wl5) r7
            com.fossil.t87.a(r8)     // Catch:{ Exception -> 0x0032 }
            goto L_0x00b5
        L_0x0032:
            r7 = move-exception
            goto L_0x008f
        L_0x0034:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r8 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r8)
            throw r7
        L_0x003c:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.fossil.wl5.s
            java.lang.String r4 = "running UseCase"
            r8.d(r2, r4)
            java.lang.String r8 = ""
            if (r7 != 0) goto L_0x0058
            com.fossil.wl5$i r7 = new com.fossil.wl5$i
            r0 = 600(0x258, float:8.41E-43)
            r7.<init>(r0, r8, r8)
            return r7
        L_0x0058:
            r6.h = r3
            java.lang.String r2 = r7.a()
            r6.d = r2
            r6.e = r8
            r7.b()
            java.lang.String r8 = r7.b()
            r6.e = r8
            com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r8 = r8.c()
            java.lang.String r2 = r6.d
            r4 = 0
            if (r2 == 0) goto L_0x008b
            java.lang.String r5 = r6.e
            if (r5 == 0) goto L_0x0087
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r7 = r8.a(r2, r5, r0)
            if (r7 != r1) goto L_0x00b5
            return r1
        L_0x0087:
            com.fossil.ee7.a()
            throw r4
        L_0x008b:
            com.fossil.ee7.a()
            throw r4
        L_0x008f:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r0 = com.fossil.wl5.s
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Error inside "
            r1.append(r2)
            java.lang.String r2 = com.fossil.wl5.s
            r1.append(r2)
            java.lang.String r2 = ".connectDevice - e="
            r1.append(r2)
            r1.append(r7)
            java.lang.String r7 = r1.toString()
            r8.e(r0, r7)
        L_0x00b5:
            java.lang.Object r7 = new java.lang.Object
            r7.<init>()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.wl5.a(com.fossil.wl5$h, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final synchronized void a(long j2) {
        FLogger.INSTANCE.getLocal().d(s, "requestAuthorizeDevice()");
        PortfolioApp c2 = PortfolioApp.g0.c();
        String str = this.d;
        if (str != null) {
            c2.a(str, new PairingAuthorizeResponse(j2));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final synchronized void a(String str) {
        ee7.b(str, "serial");
        this.h = true;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "downloadLabelFile, serial = " + str);
        ik7 unused = xh7.b(b(), null, null, new o(this, str, null), 3, null);
    }
}
