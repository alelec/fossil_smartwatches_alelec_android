package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km4 {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource", f = "FlagRemoteDataSource.kt", l = {21}, m = "fetchFlags")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ km4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(km4 km4, fb7 fb7) {
            super(fb7);
            this.this$0 = km4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.app_setting.flag.data.FlagRemoteDataSource$fetchFlags$response$1", f = "FlagRemoteDataSource.kt", l = {21}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $agent;
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ km4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(km4 km4, String str, String str2, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = km4;
            this.$activeSerial = str;
            this.$agent = str2;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$activeSerial, this.$agent, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                String str = this.$activeSerial;
                String str2 = this.$agent;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.featureFlag(str, str2, ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public km4(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r12, java.lang.String r13, java.lang.String[] r14, com.fossil.fb7<? super com.fossil.zi5<com.fossil.ie4>> r15) {
        /*
            r11 = this;
            boolean r0 = r15 instanceof com.fossil.km4.a
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.km4$a r0 = (com.fossil.km4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.km4$a r0 = new com.fossil.km4$a
            r0.<init>(r11, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0049
            if (r2 != r3) goto L_0x0041
            java.lang.Object r12 = r0.L$5
            com.fossil.ie4 r12 = (com.fossil.ie4) r12
            java.lang.Object r12 = r0.L$4
            com.fossil.de4 r12 = (com.fossil.de4) r12
            java.lang.Object r12 = r0.L$3
            java.lang.String[] r12 = (java.lang.String[]) r12
            java.lang.Object r12 = r0.L$2
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r12 = r0.L$1
            java.lang.String r12 = (java.lang.String) r12
            java.lang.Object r12 = r0.L$0
            com.fossil.km4 r12 = (com.fossil.km4) r12
            com.fossil.t87.a(r15)
            goto L_0x0087
        L_0x0041:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0049:
            com.fossil.t87.a(r15)
            com.fossil.de4 r15 = new com.fossil.de4
            r15.<init>()
            int r2 = r14.length
            r4 = 0
        L_0x0053:
            if (r4 >= r2) goto L_0x005d
            r5 = r14[r4]
            r15.a(r5)
            int r4 = r4 + 1
            goto L_0x0053
        L_0x005d:
            com.fossil.ie4 r2 = new com.fossil.ie4
            r2.<init>()
            java.lang.String r4 = "flags"
            r2.a(r4, r15)
            com.fossil.km4$b r4 = new com.fossil.km4$b
            r10 = 0
            r5 = r4
            r6 = r11
            r7 = r12
            r8 = r13
            r9 = r2
            r5.<init>(r6, r7, r8, r9, r10)
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.L$3 = r14
            r0.L$4 = r15
            r0.L$5 = r2
            r0.label = r3
            java.lang.Object r15 = com.fossil.aj5.a(r4, r0)
            if (r15 != r1) goto L_0x0087
            return r1
        L_0x0087:
            com.fossil.zi5 r15 = (com.fossil.zi5) r15
            boolean r12 = r15 instanceof com.fossil.bj5
            if (r12 == 0) goto L_0x009d
            com.fossil.bj5 r12 = new com.fossil.bj5
            com.fossil.bj5 r15 = (com.fossil.bj5) r15
            java.lang.Object r13 = r15.a()
            boolean r14 = r15.b()
            r12.<init>(r13, r14)
            goto L_0x00ba
        L_0x009d:
            boolean r12 = r15 instanceof com.fossil.yi5
            if (r12 == 0) goto L_0x00bb
            com.fossil.yi5 r12 = new com.fossil.yi5
            com.fossil.yi5 r15 = (com.fossil.yi5) r15
            int r1 = r15.a()
            com.portfolio.platform.data.model.ServerError r2 = r15.c()
            java.lang.Throwable r3 = r15.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r12
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x00ba:
            return r12
        L_0x00bb:
            com.fossil.p87 r12 = new com.fossil.p87
            r12.<init>()
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.km4.a(java.lang.String, java.lang.String, java.lang.String[], com.fossil.fb7):java.lang.Object");
    }
}
