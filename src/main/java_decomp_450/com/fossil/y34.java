package com.fossil;

import android.content.Context;
import android.util.Log;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y34 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ l14 b;
    @DexIgnore
    public /* final */ e44 c;
    @DexIgnore
    public /* final */ long d; // = System.currentTimeMillis();
    @DexIgnore
    public z34 e;
    @DexIgnore
    public z34 f;
    @DexIgnore
    public w34 g;
    @DexIgnore
    public /* final */ j44 h;
    @DexIgnore
    public /* final */ k34 i;
    @DexIgnore
    public /* final */ d34 j;
    @DexIgnore
    public ExecutorService k;
    @DexIgnore
    public u34 l;
    @DexIgnore
    public y24 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<no3<Void>> {
        @DexIgnore
        public /* final */ /* synthetic */ t74 a;

        @DexIgnore
        public a(t74 t74) {
            this.a = t74;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public no3<Void> call() throws Exception {
            return y34.this.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ t74 a;

        @DexIgnore
        public b(t74 t74) {
            this.a = t74;
        }

        @DexIgnore
        public void run() {
            no3 unused = y34.this.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Callable<Boolean> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Boolean call() throws Exception {
            try {
                boolean d = y34.this.e.d();
                z24 a2 = z24.a();
                a2.a("Initialization marker file removed: " + d);
                return Boolean.valueOf(d);
            } catch (Exception e) {
                z24.a().b("Problem encountered deleting Crashlytics initialization marker.", e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Boolean> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Boolean call() throws Exception {
            return Boolean.valueOf(y34.this.g.c());
        }
    }

    @DexIgnore
    public y34(l14 l14, j44 j44, y24 y24, e44 e44, k34 k34, d34 d34, ExecutorService executorService) {
        this.b = l14;
        this.c = e44;
        this.a = l14.b();
        this.h = j44;
        this.m = y24;
        this.i = k34;
        this.j = d34;
        this.k = executorService;
        this.l = new u34(executorService);
    }

    @DexIgnore
    public static String e() {
        return "17.1.1";
    }

    @DexIgnore
    public final void c(t74 t74) {
        Future<?> submit = this.k.submit(new b(t74));
        z24.a().a("Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.");
        try {
            submit.get(4, TimeUnit.SECONDS);
        } catch (InterruptedException e2) {
            z24.a().b("Crashlytics was interrupted during initialization.", e2);
        } catch (ExecutionException e3) {
            z24.a().b("Problem encountered during Crashlytics initialization.", e3);
        } catch (TimeoutException e4) {
            z24.a().b("Crashlytics timed out during initialization.", e4);
        }
    }

    @DexIgnore
    public boolean d(t74 t74) {
        String e2 = t34.e(this.a);
        z24 a2 = z24.a();
        a2.a("Mapping file ID is: " + e2);
        if (a(e2, t34.a(this.a, "com.crashlytics.RequireBuildId", true))) {
            String b2 = this.b.d().b();
            try {
                z24 a3 = z24.a();
                a3.c("Initializing Crashlytics " + e());
                w64 w64 = new w64(this.a);
                this.f = new z34("crash_marker", w64);
                this.e = new z34("initialization_marker", w64);
                m64 m64 = new m64();
                n34 a4 = n34.a(this.a, this.h, b2, e2);
                o84 o84 = new o84(this.a);
                z24 a5 = z24.a();
                a5.a("Installer package name is: " + a4.c);
                this.g = new w34(this.a, this.l, m64, this.h, this.c, w64, this.f, a4, null, null, this.m, o84, this.j, t74);
                boolean b3 = b();
                a();
                this.g.a(Thread.getDefaultUncaughtExceptionHandler(), t74);
                if (!b3 || !t34.b(this.a)) {
                    z24.a().a("Exception handling initialization successful");
                    return true;
                }
                z24.a().a("Crashlytics did not finish previous background initialization. Initializing synchronously.");
                c(t74);
                return false;
            } catch (Exception e3) {
                z24.a().b("Crashlytics was not started due to an exception during initialization", e3);
                this.g = null;
                return false;
            }
        } else {
            throw new IllegalStateException("The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
        }
    }

    @DexIgnore
    public no3<Void> b(t74 t74) {
        return v44.a(this.k, new a(t74));
    }

    @DexIgnore
    public final no3<Void> a(t74 t74) {
        d();
        this.g.a();
        try {
            this.i.a(x34.a(this));
            b84 b2 = t74.b();
            if (!b2.a().a) {
                z24.a().a("Collection of crash reports disabled in Crashlytics settings.");
                return qo3.a((Exception) new RuntimeException("Collection of crash reports disabled in Crashlytics settings."));
            }
            if (!this.g.b(b2.b().a)) {
                z24.a().a("Could not finalize previous sessions.");
            }
            no3<Void> a2 = this.g.a(1.0f, t74.a());
            c();
            return a2;
        } catch (Exception e2) {
            z24.a().b("Crashlytics encountered a problem during asynchronous initialization.", e2);
            return qo3.a(e2);
        } finally {
            c();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.g.d(str);
    }

    @DexIgnore
    public boolean b() {
        return this.e.c();
    }

    @DexIgnore
    public void c() {
        this.l.b(new c());
    }

    @DexIgnore
    public void a(String str) {
        this.g.a(System.currentTimeMillis() - this.d, str);
    }

    @DexIgnore
    public void a(String str, String str2) {
        this.g.b(str, str2);
    }

    @DexIgnore
    public final void a() {
        try {
            Boolean.TRUE.equals((Boolean) v44.a(this.l.b(new d())));
        } catch (Exception unused) {
        }
    }

    @DexIgnore
    public static boolean a(String str, boolean z) {
        if (!z) {
            z24.a().a("Configured not to require a build ID.");
            return true;
        } else if (!t34.b(str)) {
            return true;
        } else {
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".     |  | ");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".   \\ |  | /");
            Log.e("FirebaseCrashlytics", ".    \\    /");
            Log.e("FirebaseCrashlytics", ".     \\  /");
            Log.e("FirebaseCrashlytics", ".      \\/");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", "The Crashlytics build ID is missing. This occurs when Crashlytics tooling is absent from your app's build configuration. Please review Crashlytics onboarding instructions and ensure you have a valid Crashlytics account.");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            Log.e("FirebaseCrashlytics", ".      /\\");
            Log.e("FirebaseCrashlytics", ".     /  \\");
            Log.e("FirebaseCrashlytics", ".    /    \\");
            Log.e("FirebaseCrashlytics", ".   / |  | \\");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", ".     |  |");
            Log.e("FirebaseCrashlytics", CodelessMatcher.CURRENT_CLASS_NAME);
            return false;
        }
    }

    @DexIgnore
    public void d() {
        this.l.a();
        this.e.a();
        z24.a().a("Initialization marker file created.");
    }
}
