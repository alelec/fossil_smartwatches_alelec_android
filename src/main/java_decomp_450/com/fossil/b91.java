package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b91 extends e71 {
    @DexIgnore
    public byte[] L; // = new byte[0];
    @DexIgnore
    public byte M; // = -1;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public qk1 P; // = qk1.FTD;

    @DexIgnore
    public b91(g51 g51, short s, qa1 qa1, ri1 ri1, int i) {
        super(g51, s, qa1, ri1, i);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (!this.N) {
            this.N = true;
            ((uh1) this).E = ((v81) this).v.c != ay0.a;
            byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) 8).array();
            ee7.a((Object) array, "ByteBuffer.allocate(1)\n \u2026                 .array()");
            b(array);
        } else {
            ((uh1) this).E = true;
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract void c(byte[] bArr);

    @DexIgnore
    @Override // com.fossil.uh1
    public final boolean c(rk1 rk1) {
        return rk1.a == this.P;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final void e(rk1 rk1) {
        if (!this.N) {
            super.e(rk1);
            return;
        }
        byte[] bArr = rk1.b;
        ((uh1) this).E = true;
        ((v81) this).g.add(new zq0(0, rk1.a, bArr, new JSONObject(), 1));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final void f(rk1 rk1) {
        byte[] bArr;
        if (((v81) this).s) {
            bArr = ir0.b.a(((v81) this).y.u, this.P, rk1.b);
        } else {
            bArr = rk1.b;
        }
        boolean z = false;
        int b = yz0.b(yz0.b((byte) (bArr[0] & 63)));
        int b2 = yz0.b(yz0.b((byte) (((byte) (this.M + 1)) & 63)));
        if (b == b2) {
            a(((v81) this).p);
            this.M = (byte) b2;
            this.L = yz0.a(this.L, s97.a(bArr, 1, bArr.length));
            if (((byte) (bArr[0] & Byte.MIN_VALUE)) != ((byte) 0)) {
                z = true;
            }
            this.O = z;
            if (z) {
                c(this.L);
                s();
                return;
            }
            return;
        }
        ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.g, null, null, 27);
        ((uh1) this).E = true;
    }

    @DexIgnore
    public void s() {
    }
}
