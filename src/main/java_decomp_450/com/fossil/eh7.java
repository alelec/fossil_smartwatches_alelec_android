package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class eh7 {
    @DexIgnore
    public static final <T> void a(Appendable appendable, T t, gd7<? super T, ? extends CharSequence> gd7) {
        ee7.b(appendable, "$this$appendElement");
        if (gd7 != null) {
            appendable.append((CharSequence) gd7.invoke(t));
            return;
        }
        if (t != null ? t instanceof CharSequence : true) {
            appendable.append(t);
        } else if (t instanceof Character) {
            appendable.append(t.charValue());
        } else {
            appendable.append(String.valueOf(t));
        }
    }
}
