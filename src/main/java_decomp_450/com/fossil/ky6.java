package com.fossil;

import android.content.Context;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ky6 {
    @DexIgnore
    public static int a(Context context, float f) {
        float applyDimension = TypedValue.applyDimension(1, f, context.getResources().getDisplayMetrics());
        int i = (int) (((double) applyDimension) + 0.5d);
        if (i != 0 || applyDimension <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return i;
        }
        return 1;
    }
}
