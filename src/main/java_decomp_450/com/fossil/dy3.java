package com.fossil;

import com.google.j2objc.annotations.Weak;
import java.io.Serializable;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class dy3<K, V> extends iy3<Map.Entry<K, V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ by3<K, V> map;

        @DexIgnore
        public a(by3<K, V> by3) {
            this.map = by3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.entrySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends dy3<K, V> {
        @DexIgnore
        @Weak
        public /* final */ transient by3<K, V> b;
        @DexIgnore
        public /* final */ transient Map.Entry<K, V>[] c;

        @DexIgnore
        public b(by3<K, V> by3, Map.Entry<K, V>[] entryArr) {
            this.b = by3;
            this.c = entryArr;
        }

        @DexIgnore
        @Override // com.fossil.iy3
        public zx3<Map.Entry<K, V>> createAsList() {
            return new nz3(this, this.c);
        }

        @DexIgnore
        @Override // com.fossil.dy3
        public by3<K, V> map() {
            return this.b;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
        public j04<Map.Entry<K, V>> iterator() {
            return qy3.a((Object[]) this.c);
        }
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        V v = map().get(entry.getKey());
        if (v == null || !v.equals(entry.getValue())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public int hashCode() {
        return map().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public boolean isHashCodeFast() {
        return map().isHashCodeFast();
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return map().isPartialView();
    }

    @DexIgnore
    public abstract by3<K, V> map();

    @DexIgnore
    public int size() {
        return map().size();
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.iy3
    public Object writeReplace() {
        return new a(map());
    }
}
