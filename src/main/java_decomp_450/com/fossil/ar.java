package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ar implements fr {
    @DexIgnore
    public /* final */ Paint a; // = new Paint(3);
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends InputStream {
        @DexIgnore
        public /* final */ InputStream a;

        @DexIgnore
        public a(InputStream inputStream) {
            ee7.b(inputStream, "delegate");
            this.a = inputStream;
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int available() {
            return Integer.MAX_VALUE;
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() {
            this.a.close();
        }

        @DexIgnore
        public void mark(int i) {
            this.a.mark(i);
        }

        @DexIgnore
        public boolean markSupported() {
            return this.a.markSupported();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read() {
            return this.a.read();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public void reset() {
            this.a.reset();
        }

        @DexIgnore
        @Override // java.io.InputStream
        public long skip(long j) {
            return this.a.skip(j);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr) {
            ee7.b(bArr, "b");
            return this.a.read(bArr);
        }

        @DexIgnore
        @Override // java.io.InputStream
        public int read(byte[] bArr, int i, int i2) {
            ee7.b(bArr, "b");
            return this.a.read(bArr, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends dr7 {
        @DexIgnore
        public Exception b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sr7 sr7) {
            super(sr7);
            ee7.b(sr7, "delegate");
        }

        @DexIgnore
        public final Exception b() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.sr7, com.fossil.dr7
        public long b(yq7 yq7, long j) {
            ee7.b(yq7, "sink");
            try {
                return super.b(yq7, j);
            } catch (Exception e) {
                this.b = e;
                throw e;
            }
        }
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    public ar(Context context) {
        ee7.b(context, "context");
        this.b = context;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [java.lang.Throwable, android.graphics.Rect] */
    /* JADX WARN: Type inference failed for: r1v9 */
    /* JADX WARN: Type inference failed for: r1v30 */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0202, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0203, code lost:
        com.fossil.hc7.a(r3, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x0207, code lost:
        throw r0;
     */
    @DexIgnore
    @Override // com.fossil.fr
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.rq r26, com.fossil.ar7 r27, com.fossil.rt r28, com.fossil.ir r29, com.fossil.fb7<? super com.fossil.cr> r30) {
        /*
            r25 = this;
            r7 = r25
            r0 = r26
            r1 = r28
            android.graphics.BitmapFactory$Options r8 = new android.graphics.BitmapFactory$Options
            r8.<init>()
            com.fossil.ar$c r2 = new com.fossil.ar$c
            r3 = r27
            r2.<init>(r3)
            com.fossil.ar7 r3 = com.fossil.ir7.a(r2)
            r9 = 1
            r8.inJustDecodeBounds = r9
            com.fossil.ar7 r4 = r3.peek()
            java.io.InputStream r4 = r4.u()
            r5 = 0
            android.graphics.BitmapFactory.decodeStream(r4, r5, r8)
            java.lang.Exception r4 = r2.b()
            if (r4 != 0) goto L_0x0208
            r10 = 0
            r8.inJustDecodeBounds = r10
            com.fossil.ub r4 = new com.fossil.ub
            com.fossil.ar$a r6 = new com.fossil.ar$a
            com.fossil.ar7 r11 = r3.peek()
            java.io.InputStream r11 = r11.u()
            r6.<init>(r11)
            r4.<init>(r6)
            boolean r6 = r4.d()
            int r11 = r4.c()
            if (r11 <= 0) goto L_0x004c
            r4 = 1
            goto L_0x004d
        L_0x004c:
            r4 = 0
        L_0x004d:
            r12 = 90
            if (r11 == r12) goto L_0x0058
            r12 = 270(0x10e, float:3.78E-43)
            if (r11 != r12) goto L_0x0056
            goto L_0x0058
        L_0x0056:
            r12 = 0
            goto L_0x0059
        L_0x0058:
            r12 = 1
        L_0x0059:
            if (r12 == 0) goto L_0x005e
            int r13 = r8.outHeight
            goto L_0x0060
        L_0x005e:
            int r13 = r8.outWidth
        L_0x0060:
            if (r12 == 0) goto L_0x0065
            int r12 = r8.outWidth
            goto L_0x0067
        L_0x0065:
            int r12 = r8.outHeight
        L_0x0067:
            if (r6 != 0) goto L_0x0071
            if (r4 == 0) goto L_0x006c
            goto L_0x0071
        L_0x006c:
            android.graphics.Bitmap$Config r4 = r29.d()
            goto L_0x0079
        L_0x0071:
            android.graphics.Bitmap$Config r4 = r29.d()
            android.graphics.Bitmap$Config r4 = com.fossil.iu.c(r4)
        L_0x0079:
            boolean r14 = r29.b()
            java.lang.String r15 = r8.outMimeType
            boolean r14 = r7.a(r14, r4, r15)
            if (r14 == 0) goto L_0x0087
            android.graphics.Bitmap$Config r4 = android.graphics.Bitmap.Config.RGB_565
        L_0x0087:
            r8.inPreferredConfig = r4
            int r4 = android.os.Build.VERSION.SDK_INT
            r14 = 26
            if (r4 < r14) goto L_0x009b
            android.graphics.ColorSpace r4 = r29.c()
            if (r4 == 0) goto L_0x009b
            android.graphics.ColorSpace r4 = r29.c()
            r8.inPreferredColorSpace = r4
        L_0x009b:
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 < r14) goto L_0x00a8
            android.graphics.Bitmap$Config r4 = r8.inPreferredConfig
            android.graphics.Bitmap$Config r14 = android.graphics.Bitmap.Config.HARDWARE
            if (r4 == r14) goto L_0x00a6
            goto L_0x00a8
        L_0x00a6:
            r4 = 0
            goto L_0x00a9
        L_0x00a8:
            r4 = 1
        L_0x00a9:
            r8.inMutable = r4
            r8.inScaled = r10
            int r14 = r8.outWidth
            java.lang.String r15 = "inPreferredConfig"
            if (r14 <= 0) goto L_0x0193
            int r10 = r8.outHeight
            if (r10 > 0) goto L_0x00b9
            goto L_0x0193
        L_0x00b9:
            boolean r5 = r1 instanceof com.fossil.ot
            if (r5 != 0) goto L_0x00d1
            r8.inSampleSize = r9
            if (r4 == 0) goto L_0x00cc
            android.graphics.Bitmap$Config r1 = r8.inPreferredConfig
            com.fossil.ee7.a(r1, r15)
            android.graphics.Bitmap r1 = r0.b(r14, r10, r1)
            r8.inBitmap = r1
        L_0x00cc:
            r16 = r6
        L_0x00ce:
            r1 = 0
            goto L_0x019b
        L_0x00d1:
            int r5 = android.os.Build.VERSION.SDK_INT
            r9 = 19
            if (r5 < r9) goto L_0x0168
            com.fossil.ot r1 = (com.fossil.ot) r1
            int r4 = r1.a()
            int r1 = r1.b()
            com.fossil.qt r5 = r29.h()
            int r5 = com.fossil.er.a(r13, r12, r4, r1, r5)
            r8.inSampleSize = r5
            double r9 = (double) r13
            double r13 = (double) r5
            double r16 = r9 / r13
            double r9 = (double) r12
            double r12 = (double) r5
            double r18 = r9 / r12
            double r4 = (double) r4
            double r9 = (double) r1
            com.fossil.qt r24 = r29.h()
            r20 = r4
            r22 = r9
            double r4 = com.fossil.er.a(r16, r18, r20, r22, r24)
            boolean r1 = r29.a()
            r9 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            if (r1 == 0) goto L_0x010d
            double r4 = com.fossil.qf7.b(r4, r9)
        L_0x010d:
            int r1 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r1 == 0) goto L_0x0113
            r1 = 1
            goto L_0x0114
        L_0x0113:
            r1 = 0
        L_0x0114:
            r8.inScaled = r1
            if (r1 == 0) goto L_0x0137
            r1 = 1
            double r9 = (double) r1
            r1 = 2147483647(0x7fffffff, float:NaN)
            int r12 = (r4 > r9 ? 1 : (r4 == r9 ? 0 : -1))
            if (r12 <= 0) goto L_0x012c
            double r9 = (double) r1
            double r9 = r9 / r4
            int r9 = com.fossil.af7.a(r9)
            r8.inDensity = r9
            r8.inTargetDensity = r1
            goto L_0x0137
        L_0x012c:
            r8.inDensity = r1
            double r9 = (double) r1
            double r9 = r9 * r4
            int r1 = com.fossil.af7.a(r9)
            r8.inTargetDensity = r1
        L_0x0137:
            boolean r1 = r8.inMutable
            if (r1 == 0) goto L_0x00cc
            int r1 = r8.outWidth
            double r9 = (double) r1
            int r1 = r8.inSampleSize
            double r12 = (double) r1
            double r9 = r9 / r12
            int r12 = r8.outHeight
            double r12 = (double) r12
            r16 = r6
            double r6 = (double) r1
            double r12 = r12 / r6
            double r9 = r9 * r4
            r6 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            double r9 = r9 + r6
            double r9 = java.lang.Math.ceil(r9)
            int r1 = (int) r9
            double r4 = r4 * r12
            double r4 = r4 + r6
            double r4 = java.lang.Math.ceil(r4)
            int r4 = (int) r4
            android.graphics.Bitmap$Config r5 = r8.inPreferredConfig
            com.fossil.ee7.a(r5, r15)
            android.graphics.Bitmap r1 = r0.b(r1, r4, r5)
            r8.inBitmap = r1
            goto L_0x00ce
        L_0x0168:
            r16 = r6
            if (r4 == 0) goto L_0x0177
            android.graphics.Bitmap$Config r4 = r8.inPreferredConfig
            com.fossil.ee7.a(r4, r15)
            android.graphics.Bitmap r4 = r0.b(r14, r10, r4)
            r8.inBitmap = r4
        L_0x0177:
            android.graphics.Bitmap r4 = r8.inBitmap
            if (r4 == 0) goto L_0x017d
            r1 = 1
            goto L_0x018f
        L_0x017d:
            com.fossil.ot r1 = (com.fossil.ot) r1
            int r4 = r1.d()
            int r1 = r1.c()
            com.fossil.qt r5 = r29.h()
            int r1 = com.fossil.er.a(r13, r12, r4, r1, r5)
        L_0x018f:
            r8.inSampleSize = r1
            goto L_0x00ce
        L_0x0193:
            r16 = r6
            r1 = 1
            r8.inSampleSize = r1
            r1 = 0
            r8.inBitmap = r1
        L_0x019b:
            java.io.InputStream r4 = r3.u()     // Catch:{ all -> 0x01fd }
            android.graphics.Bitmap r4 = android.graphics.BitmapFactory.decodeStream(r4, r1, r8)     // Catch:{ all -> 0x01fd }
            com.fossil.hc7.a(r3, r1)
            java.lang.Exception r1 = r2.b()
            if (r1 == 0) goto L_0x01b2
            if (r4 == 0) goto L_0x01b1
            r0.a(r4)
        L_0x01b1:
            throw r1
        L_0x01b2:
            if (r4 == 0) goto L_0x01ef
            android.graphics.Bitmap$Config r5 = r8.inPreferredConfig
            com.fossil.ee7.a(r5, r15)
            r1 = r25
            r2 = r26
            r3 = r4
            r4 = r5
            r5 = r16
            r6 = r11
            android.graphics.Bitmap r0 = r1.a(r2, r3, r4, r5, r6)
            r1 = 0
            r0.setDensity(r1)
            r2 = r25
            android.content.Context r3 = r2.b
            android.content.res.Resources r3 = r3.getResources()
            java.lang.String r4 = "context.resources"
            com.fossil.ee7.a(r3, r4)
            android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable
            r4.<init>(r3, r0)
            int r0 = r8.inSampleSize
            r3 = 1
            if (r0 > r3) goto L_0x01e8
            boolean r0 = r8.inScaled
            if (r0 == 0) goto L_0x01e6
            goto L_0x01e8
        L_0x01e6:
            r9 = 0
            goto L_0x01e9
        L_0x01e8:
            r9 = 1
        L_0x01e9:
            com.fossil.cr r0 = new com.fossil.cr
            r0.<init>(r4, r9)
            return r0
        L_0x01ef:
            r2 = r25
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "BitmapFactory returned a null Bitmap. Often this means BitmapFactory could not decode the image data read from the input source (e.g. network or disk) as it's not encoded as a valid image format."
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x01fd:
            r0 = move-exception
            r2 = r25
            r1 = r0
            throw r1     // Catch:{ all -> 0x0202 }
        L_0x0202:
            r0 = move-exception
            r4 = r0
            com.fossil.hc7.a(r3, r1)
            throw r4
        L_0x0208:
            r2 = r7
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ar.a(com.fossil.rq, com.fossil.ar7, com.fossil.rt, com.fossil.ir, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.fr
    public boolean a(ar7 ar7, String str) {
        ee7.b(ar7, "source");
        return true;
    }

    @DexIgnore
    public final boolean a(boolean z, Bitmap.Config config, String str) {
        return z && (Build.VERSION.SDK_INT < 26 || config == Bitmap.Config.ARGB_8888) && ee7.a(str, "image/jpeg");
    }

    @DexIgnore
    public final Bitmap a(rq rqVar, Bitmap bitmap, Bitmap.Config config, boolean z, int i) {
        Bitmap bitmap2;
        boolean z2 = i > 0;
        if (!z && !z2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        float width = ((float) bitmap.getWidth()) / 2.0f;
        float height = ((float) bitmap.getHeight()) / 2.0f;
        if (z) {
            matrix.postScale(-1.0f, 1.0f, width, height);
        }
        if (z2) {
            matrix.postRotate((float) i, width, height);
        }
        RectF rectF = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix.mapRect(rectF);
        if (!(rectF.left == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && rectF.top == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
            matrix.postTranslate(-rectF.left, -rectF.top);
        }
        if (i == 90 || i == 270) {
            bitmap2 = rqVar.a(bitmap.getHeight(), bitmap.getWidth(), config);
        } else {
            bitmap2 = rqVar.a(bitmap.getWidth(), bitmap.getHeight(), config);
        }
        new Canvas(bitmap2).drawBitmap(bitmap, matrix, this.a);
        rqVar.a(bitmap);
        return bitmap2;
    }
}
