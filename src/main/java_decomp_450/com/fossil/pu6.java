package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.format.DateUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.fl4;
import com.fossil.fm5;
import com.fossil.hm5;
import com.fossil.mm5;
import com.fossil.yl5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pu6 extends el4 {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ b z; // = new b(null);
    @DexIgnore
    public MutableLiveData<c> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Handler f; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public MutableLiveData<String> g;
    @DexIgnore
    public /* final */ LiveData<Device> h;
    @DexIgnore
    public d i;
    @DexIgnore
    public c j;
    @DexIgnore
    public String k;
    @DexIgnore
    public /* final */ Runnable l;
    @DexIgnore
    public zd<Device> m;
    @DexIgnore
    public /* final */ MutableLiveData<a> n;
    @DexIgnore
    public /* final */ i o;
    @DexIgnore
    public /* final */ DeviceRepository p;
    @DexIgnore
    public /* final */ fm5 q;
    @DexIgnore
    public /* final */ mm5 r;
    @DexIgnore
    public /* final */ ad5 s;
    @DexIgnore
    public /* final */ ch5 t;
    @DexIgnore
    public /* final */ hm5 u;
    @DexIgnore
    public /* final */ yl5 v;
    @DexIgnore
    public /* final */ ro4 w;
    @DexIgnore
    public /* final */ PortfolioApp x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public mn4 c;

        @DexIgnore
        public a(boolean z, boolean z2, mn4 mn4) {
            this.a = z;
            this.b = z2;
            this.c = mn4;
        }

        @DexIgnore
        public final mn4 a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return this.a == aVar.a && this.b == aVar.b && ee7.a(this.c, aVar.c);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r1v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r1v6, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            boolean z = this.a;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = i2 * 31;
            boolean z2 = this.b;
            if (z2 == 0) {
                i = z2;
            }
            int i5 = (i4 + i) * 31;
            mn4 mn4 = this.c;
            return i5 + (mn4 != null ? mn4.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "CheckingChallengeWrapper(needToLeave=" + this.a + ", isSwitch=" + this.b + ", challenge=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final String a() {
            return pu6.y;
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public d b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public r87<Integer, String> e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public String k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;

        @DexIgnore
        public c() {
            this(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        }

        @DexIgnore
        public c(boolean z, d dVar, String str, Integer num, r87<Integer, String> r87, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.a = z;
            this.b = dVar;
            this.c = str;
            this.d = num;
            this.e = r87;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final String c() {
            return this.g;
        }

        @DexIgnore
        public final boolean d() {
            return this.f;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public final r87<Integer, String> f() {
            return this.e;
        }

        @DexIgnore
        public final boolean g() {
            return this.l;
        }

        @DexIgnore
        public final String h() {
            return this.k;
        }

        @DexIgnore
        public final String i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.m;
        }

        @DexIgnore
        public final d k() {
            return this.b;
        }

        @DexIgnore
        public final String l() {
            return this.c;
        }

        @DexIgnore
        public final Integer m() {
            return this.d;
        }

        @DexIgnore
        public String toString() {
            String a2 = new Gson().a(this);
            ee7.a((Object) a2, "Gson().toJson(this)");
            return a2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(boolean z, d dVar, String str, Integer num, r87 r87, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, zd7 zd7) {
            this((i2 & 1) != 0 ? false : z, (i2 & 2) != 0 ? null : dVar, (i2 & 4) != 0 ? null : str, (i2 & 8) != 0 ? null : num, (i2 & 16) != 0 ? null : r87, (i2 & 32) != 0 ? false : z2, (i2 & 64) != 0 ? null : str2, (i2 & 128) != 0 ? null : str3, (i2 & 256) != 0 ? null : str4, (i2 & 512) != 0 ? null : str5, (i2 & 1024) == 0 ? str6 : null, (i2 & 2048) != 0 ? false : z3, (i2 & 4096) == 0 ? z4 : false);
        }

        @DexIgnore
        public final void a(d dVar) {
            this.b = dVar;
        }

        @DexIgnore
        public final void a(String str) {
            this.c = str;
        }

        @DexIgnore
        public static /* synthetic */ void a(c cVar, boolean z, d dVar, String str, Integer num, r87 r87, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4, int i2, Object obj) {
            boolean z5 = false;
            boolean z6 = (i2 & 1) != 0 ? false : z;
            String str7 = null;
            d dVar2 = (i2 & 2) != 0 ? null : dVar;
            String str8 = (i2 & 4) != 0 ? null : str;
            Integer num2 = (i2 & 8) != 0 ? null : num;
            r87 r872 = (i2 & 16) != 0 ? null : r87;
            boolean z7 = (i2 & 32) != 0 ? false : z2;
            String str9 = (i2 & 64) != 0 ? null : str2;
            String str10 = (i2 & 128) != 0 ? null : str3;
            String str11 = (i2 & 256) != 0 ? null : str4;
            String str12 = (i2 & 512) != 0 ? null : str5;
            if ((i2 & 1024) == 0) {
                str7 = str6;
            }
            boolean z8 = (i2 & 2048) != 0 ? false : z3;
            if ((i2 & 4096) == 0) {
                z5 = z4;
            }
            cVar.a(z6, dVar2, str8, num2, r872, z7, str9, str10, str11, str12, str7, z8, z5);
        }

        @DexIgnore
        public final synchronized void a(boolean z, d dVar, String str, Integer num, r87<Integer, String> r87, boolean z2, String str2, String str3, String str4, String str5, String str6, boolean z3, boolean z4) {
            this.a = z;
            this.b = dVar;
            this.c = str;
            this.d = num;
            this.e = r87;
            this.f = z2;
            this.g = str2;
            this.h = str3;
            this.i = str4;
            this.j = str5;
            this.k = str6;
            this.l = z3;
            this.m = z4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public Device a;
        @DexIgnore
        public String b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public Boolean e;

        @DexIgnore
        public d(Device device, String str, boolean z, boolean z2, Boolean bool) {
            ee7.b(device, "deviceModel");
            ee7.b(str, "deviceName");
            this.a = device;
            this.b = str;
            this.c = z;
            this.d = z2;
            this.e = bool;
        }

        @DexIgnore
        public final Device a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public final boolean e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof d)) {
                return false;
            }
            d dVar = (d) obj;
            return ee7.a(this.a, dVar.a) && ee7.a(this.b, dVar.b) && this.c == dVar.c && this.d == dVar.d && ee7.a(this.e, dVar.e);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r2v4, resolved type: boolean */
        /* JADX DEBUG: Multi-variable search result rejected for r3v1, resolved type: int */
        /* JADX DEBUG: Multi-variable search result rejected for r3v2, resolved type: int */
        /* JADX WARN: Multi-variable type inference failed */
        public int hashCode() {
            Device device = this.a;
            int i = 0;
            int hashCode = (device != null ? device.hashCode() : 0) * 31;
            String str = this.b;
            int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
            boolean z = this.c;
            int i2 = 1;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode2 + i3) * 31;
            boolean z2 = this.d;
            if (z2 == 0) {
                i2 = z2;
            }
            int i6 = (i5 + i2) * 31;
            Boolean bool = this.e;
            if (bool != null) {
                i = bool.hashCode();
            }
            return i6 + i;
        }

        @DexIgnore
        public String toString() {
            return "WatchSettingWrapper(deviceModel=" + this.a + ", deviceName=" + this.b + ", isConnected=" + this.c + ", isActive=" + this.d + ", shouldShowVibrationUI=" + this.e + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Device device, String str, boolean z, boolean z2, Boolean bool, int i, zd7 zd7) {
            this(device, str, z, z2, (i & 16) != 0 ? null : bool);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1", f = "WatchSettingViewModel.kt", l = {MFNetworkReturnCode.BAD_REQUEST}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $currentActiveSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pu6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkToReconnectOrSwitchActiveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.w.a(new String[]{"running", "waiting"}, vt4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(pu6 pu6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pu6;
            this.$currentActiveSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$currentActiveSerial, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Boolean a3 = this.this$0.t.a();
                ee7.a((Object) a3, "mSharedPreferencesManager.bcStatus()");
                if (a3.booleanValue()) {
                    ti7 b = qj7.b();
                    a aVar = new a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(b, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                } else {
                    if (!mh7.a((CharSequence) this.$currentActiveSerial)) {
                        this.this$0.b(this.$currentActiveSerial, 0);
                    } else {
                        pu6 pu6 = this.this$0;
                        Object a4 = pu6.g.a();
                        if (a4 != null) {
                            ee7.a(a4, "mSerialLiveData.value!!");
                            pu6.a((String) a4, 1);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                    return i97.a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mn4 mn4 = (mn4) obj;
            if (mn4 != null) {
                el4.a(this.this$0, false, false, 2, null);
                this.this$0.n.a(new a(true, true, mn4));
            } else {
                this.this$0.t.a(pb7.a(false));
                if (!mh7.a((CharSequence) this.$currentActiveSerial)) {
                    this.this$0.b(this.$currentActiveSerial, 0);
                } else {
                    pu6 pu62 = this.this$0;
                    Object a5 = pu62.g.a();
                    if (a5 != null) {
                        ee7.a(a5, "mSerialLiveData.value!!");
                        pu62.a((String) a5, 1);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1", f = "WatchSettingViewModel.kt", l = {545}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSwitch;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pu6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.watchsetting.WatchSettingViewModel$checkingBeforeRemoveDevice$1$challenge$1", f = "WatchSettingViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super mn4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super mn4> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.w.a(new String[]{"running", "waiting"}, vt4.a.a());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(pu6 pu6, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pu6;
            this.$isSwitch = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$isSwitch, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Boolean a3 = this.this$0.t.a();
                ee7.a((Object) a3, "mSharedPreferencesManager.bcStatus()");
                if (a3.booleanValue()) {
                    String c = PortfolioApp.g0.c().c();
                    Device device = (Device) this.this$0.h.a();
                    if (!ee7.a((Object) c, (Object) (device != null ? device.getDeviceId() : null))) {
                        this.this$0.n.a(new a(false, this.$isSwitch, null));
                    } else {
                        ti7 b = qj7.b();
                        a aVar = new a(this, null);
                        this.L$0 = yi7;
                        this.L$1 = c;
                        this.label = 1;
                        obj = vh7.a(b, aVar, this);
                        if (obj == a2) {
                            return a2;
                        }
                    }
                } else {
                    this.this$0.n.a(new a(false, this.$isSwitch, null));
                }
                el4.a(this.this$0, false, false, 2, null);
                return i97.a;
            } else if (i == 1) {
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            mn4 mn4 = (mn4) obj;
            if (mn4 == null) {
                this.this$0.t.a(pb7.a(false));
                this.this$0.n.a(new a(false, this.$isSwitch, null));
            } else {
                this.this$0.n.a(new a(true, this.$isSwitch, mn4));
            }
            el4.a(this.this$0, false, false, 2, null);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements fl4.e<mm5.d, mm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public g(pu6 pu6, String str) {
            this.a = pu6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(mm5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "removeDevice success serial=" + this.a.g + ".value");
            el4.a(this.a, false, true, 1, null);
            c.a(this.a.j, true, null, null, null, null, false, null, null, null, null, null, false, false, 8190, null);
            this.a.f();
        }

        @DexIgnore
        public void a(mm5.c cVar) {
            Integer num;
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "remove device " + this.a.g + ".value fail due to " + cVar.b());
            el4.a(this.a, false, true, 1, null);
            String b2 = cVar.b();
            switch (b2.hashCode()) {
                case -1767173543:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_STOP_WORKOUT_FAIL")) {
                        return;
                    }
                    break;
                case -1697024179:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_LACK_PERMISSION")) {
                        if (cVar.c() != null) {
                            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                            pu6 pu6 = this.a;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                            if (array != null) {
                                ib5[] ib5Arr = (ib5[]) array;
                                pu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                            } else {
                                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                            }
                        }
                        this.a.f();
                        return;
                    }
                    return;
                case -643272338:
                    if (b2.equals("UNLINK_FAIL_ON_SERVER")) {
                        if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                            num = 600;
                        } else {
                            num = cVar.c().get(0);
                        }
                        ee7.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                        int intValue = num.intValue();
                        c j = this.a.j;
                        Integer valueOf = Integer.valueOf(intValue);
                        String a3 = cVar.a();
                        if (a3 == null) {
                            a3 = "";
                        }
                        c.a(j, false, null, null, null, new r87(valueOf, a3), false, null, null, null, null, null, false, false, 8175, null);
                        this.a.f();
                        return;
                    }
                    return;
                case 1447890910:
                    if (!b2.equals("UNLINK_FAIL_DUE_TO_SYNC_FAIL")) {
                        return;
                    }
                    break;
                case 1768665169:
                    if (b2.equals("UNLINK_FAIL_DUE_TO_PENDING_WORKOUT")) {
                        c.a(this.a.j, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                        this.a.f();
                        return;
                    }
                    return;
                default:
                    return;
            }
            c.a(this.a.j, false, null, null, null, null, false, null, null, null, null, this.b, false, false, 7167, null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements fl4.e<hm5.d, hm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public h(pu6 pu6, String str) {
            this.a = pu6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(hm5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "removeDevice(), switch to newDevice=" + this.b + " success");
            el4.a(this.a, false, true, 1, null);
            this.a.i(this.b);
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(hm5.c cVar) {
            Integer num;
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "removeDevice switch to " + this.b + " fail due to " + cVar.b());
            el4.a(this.a, false, true, 1, null);
            int b2 = cVar.b();
            if (b2 == 113) {
                if (cVar.c() != null) {
                    List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(new ArrayList(cVar.c()));
                    ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    pu6 pu6 = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                    if (array != null) {
                        ib5[] ib5Arr = (ib5[]) array;
                        pu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                this.a.f();
            } else if (b2 == 114) {
                if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                    num = 600;
                } else {
                    num = cVar.c().get(0);
                }
                ee7.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
                int intValue = num.intValue();
                c j = this.a.j;
                Integer valueOf = Integer.valueOf(intValue);
                String a3 = cVar.a();
                if (a3 == null) {
                    a3 = "";
                }
                c.a(j, false, null, null, null, new r87(valueOf, a3), false, null, null, null, null, null, false, false, 8175, null);
                this.a.f();
            } else if (b2 == 117) {
                c.a(this.a.j, false, null, null, null, null, false, null, null, this.b, null, null, false, false, 7935, null);
                this.a.f();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(pu6 pu6) {
            this.a = pu6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Device device;
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "onConnectionStateChangeReceiver, serial=" + stringExtra);
            if (ee7.a((Object) stringExtra, (Object) ((String) this.a.g.a())) && mh7.b(stringExtra, this.a.x.c(), true)) {
                FLogger.INSTANCE.getLocal().d(pu6.z.a(), "onConnectionStateChanged");
                LiveData d = this.a.h;
                if (!(d == null || (device = (Device) d.a()) == null)) {
                    this.a.a(device);
                }
                el4.a(this.a, false, true, 1, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;

        @DexIgnore
        public j(pu6 pu6) {
            this.a = pu6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<Device> apply(String str) {
            DeviceRepository e = this.a.p;
            ee7.a((Object) str, "serial");
            return e.getDeviceBySerialAsLiveData(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;

        @DexIgnore
        public k(pu6 pu6) {
            this.a = pu6;
        }

        @DexIgnore
        public final void run() {
            this.a.q();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;

        @DexIgnore
        public l(pu6 pu6) {
            this.a = pu6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Device device) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "on device changed " + device);
            this.a.a(device);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Device $device$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Device $this_run;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pu6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ m this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(m mVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = mVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.p.getDeviceNameBySerial(this.this$0.$this_run.getDeviceId());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(Device device, fb7 fb7, pu6 pu6, Device device2) {
            super(2, fb7);
            this.$this_run = device;
            this.this$0 = pu6;
            this.$device$inlined = device2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            m mVar = new m(this.$this_run, fb7, this.this$0, this.$device$inlined);
            mVar.p$ = (yi7) obj;
            return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((m) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            yi7 yi7;
            String str;
            boolean z;
            boolean z2;
            Object a2 = nb7.a();
            int i = this.label;
            boolean z3 = false;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = pu6.z.a();
                local.d(a3, "onDeviceChanged - device: " + this.$device$inlined);
                str = PortfolioApp.g0.c().c();
                boolean z4 = !FossilDeviceSerialPatternUtil.isSamSlimDevice(this.$this_run.getDeviceId()) && !FossilDeviceSerialPatternUtil.isDianaDevice(this.$this_run.getDeviceId());
                if (this.$this_run.getVibrationStrength() == null) {
                    this.$this_run.setVibrationStrength(pb7.a(50));
                    i97 i97 = i97.a;
                }
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = str;
                this.Z$0 = z4;
                this.label = 1;
                obj2 = vh7.a(b, aVar, this);
                if (obj2 == a2) {
                    return a2;
                }
                z = z4;
            } else if (i == 1) {
                z = this.Z$0;
                str = (String) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
                obj2 = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str2 = (String) obj2;
            if (this.$this_run.getBatteryLevel() > 100) {
                this.$this_run.setBatteryLevel(100);
            }
            if (ee7.a((Object) this.$this_run.getDeviceId(), (Object) str)) {
                try {
                    IButtonConnectivity b2 = PortfolioApp.g0.b();
                    if (b2 != null && b2.getGattState(this.$this_run.getDeviceId()) == 2) {
                        z3 = true;
                    }
                    z2 = z3;
                } catch (Exception unused) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String a4 = pu6.z.a();
                    local2.e(a4, "exception when get gatt state of " + yi7);
                    z2 = false;
                }
                this.this$0.i = new d(this.$device$inlined, str2, z2, true, pb7.a(z));
            } else {
                this.this$0.i = new d(this.$device$inlined, str2, false, false, pb7.a(z));
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a5 = pu6.z.a();
            local3.d(a5, "onDeviceChanged, mDeviceWrapper=" + this.this$0.i);
            c j = this.this$0.j;
            d f = this.this$0.i;
            if (f != null) {
                c.a(j, false, f, null, null, null, false, null, null, null, null, null, false, false, 8189, null);
                this.this$0.f();
                this.this$0.f.removeCallbacksAndMessages(null);
                this.this$0.q();
                return i97.a;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements fl4.e<hm5.d, hm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public n(pu6 pu6, String str) {
            this.a = pu6;
            this.b = str;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(hm5.d dVar) {
            ee7.b(dVar, "responseValue");
            el4.a(this.a, false, true, 1, null);
            this.a.i(this.b);
            this.a.a(dVar.a());
            this.a.m();
        }

        @DexIgnore
        public void a(hm5.c cVar) {
            Integer num;
            ee7.b(cVar, "errorValue");
            if (cVar.c() == null || !(!cVar.c().isEmpty())) {
                num = 600;
            } else {
                num = cVar.c().get(0);
            }
            ee7.a((Object) num, "if (errorValue.subErrorC\u2026                        }");
            int intValue = num.intValue();
            el4.a(this.a, false, true, 1, null);
            c j = this.a.j;
            Integer valueOf = Integer.valueOf(intValue);
            String a2 = cVar.a();
            if (a2 == null) {
                a2 = "";
            }
            c.a(j, false, null, null, null, new r87(valueOf, a2), false, null, null, null, null, null, false, false, 8175, null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements fl4.e<yl5.e, yl5.d> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public o(pu6 pu6) {
            this.a = pu6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(yl5.e eVar) {
            ee7.b(eVar, "responseValue");
            el4.a(this.a, false, true, 1, null);
        }

        @DexIgnore
        public void a(yl5.d dVar) {
            ee7.b(dVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "reconnectActiveDevice fail!! errorValue=" + dVar.a());
            el4.a(this.a, false, true, 1, null);
            int c = dVar.c();
            if (c == 1101 || c == 1112 || c == 1113) {
                List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(dVar.b());
                ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
                pu6 pu6 = this.a;
                Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                if (array != null) {
                    ib5[] ib5Arr = (ib5[]) array;
                    pu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                    this.a.f();
                    return;
                }
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
            c.a(this.a.j, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
            this.a.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements fl4.e<lm5, jm5> {
        @DexIgnore
        public /* final */ /* synthetic */ pu6 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public p(pu6 pu6, String str, int i) {
            this.a = pu6;
            this.b = str;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(lm5 lm5) {
            ee7.b(lm5, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "syncDevice success - serial=" + this.b);
            int i = this.c;
            if (i == 0) {
                pu6 pu6 = this.a;
                Object a3 = pu6.g.a();
                if (a3 != null) {
                    ee7.a(a3, "mSerialLiveData.value!!");
                    pu6.a((String) a3, 1);
                    return;
                }
                ee7.a();
                throw null;
            } else if (i == 1) {
                pu6 pu62 = this.a;
                Object a4 = pu62.g.a();
                if (a4 != null) {
                    pu62.a((String) a4);
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public void a(jm5 jm5) {
            ee7.b(jm5, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "syncDevice fail - serial=" + this.b + " - errorCode=" + jm5.a());
            int i = qu6.a[jm5.a().ordinal()];
            if (i == 1) {
                FLogger.INSTANCE.getLocal().d(pu6.z.a(), "Sync of this device is in progress, keep waiting for result");
            } else if (i == 2) {
                if (jm5.b() != null) {
                    List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(new ArrayList(jm5.b()));
                    ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026rrorValue.subErrorCodes))");
                    pu6 pu6 = this.a;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                    if (array != null) {
                        ib5[] ib5Arr = (ib5[]) array;
                        pu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                        this.a.f();
                    } else {
                        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                el4.a(this.a, false, true, 1, null);
            } else if (i == 3) {
                int i2 = this.c;
                if (i2 == 0) {
                    c.a(this.a.j, false, null, null, null, null, false, this.b, null, null, null, null, false, false, 8127, null);
                    this.a.f();
                } else if (i2 == 1) {
                    c.a(this.a.j, false, null, null, null, null, false, null, null, null, this.b, null, false, false, 7679, null);
                    this.a.f();
                }
                el4.a(this.a, false, true, 1, null);
            } else if (i == 4) {
                int i3 = this.c;
                if (i3 == 0) {
                    c j = this.a.j;
                    Object a3 = this.a.g.a();
                    if (a3 != null) {
                        c.a(j, false, null, null, null, null, false, null, (String) a3, null, null, null, false, false, 8063, null);
                        this.a.f();
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i3 == 1) {
                    c j2 = this.a.j;
                    Object a4 = this.a.g.a();
                    if (a4 != null) {
                        c.a(j2, false, null, null, null, null, false, null, null, null, null, (String) a4, false, false, 7167, null);
                        this.a.f();
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                el4.a(this.a, false, true, 1, null);
            } else if (i != 5) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a5 = pu6.z.a();
                local2.d(a5, "errorValue.lastErrorCode=" + jm5.a());
                c.a(this.a.j, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                this.a.f();
                el4.a(this.a, false, true, 1, null);
            } else {
                FLogger.INSTANCE.getLocal().d(pu6.z.a(), "User deny stopping workout");
                String build = ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, ErrorCodeBuilder.Component.APP, ErrorCodeBuilder.AppError.USER_CANCELLED);
                int i4 = this.c;
                if (i4 == 0) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.SWITCH_DEVICE;
                    Object a6 = this.a.g.a();
                    if (a6 != null) {
                        ee7.a(a6, "mSerialLiveData.value!!");
                        remote.e(component, session, (String) a6, pu6.z.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i4 == 1) {
                    FLogger.INSTANCE.getRemote().e(FLogger.Component.APP, FLogger.Session.REMOVE_DEVICE, this.b, pu6.z.a(), build, ErrorCodeBuilder.Step.SYNC_CURRENT_DEVICE, "User deny stopping workout");
                }
                el4.a(this.a, false, true, 1, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements fl4.e<fm5.d, fm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ Device a;
        @DexIgnore
        public /* final */ /* synthetic */ pu6 b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public q(Device device, String str, pu6 pu6, int i) {
            this.a = device;
            this.b = pu6;
            this.c = i;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fm5.d dVar) {
            Device a2;
            ee7.b(dVar, "responseValue");
            Integer vibrationStrength = this.a.getVibrationStrength();
            el4.a(this.b, false, true, 1, null);
            c.a(this.b.j, false, null, null, Integer.valueOf(this.c), null, false, null, null, null, null, null, false, false, 8183, null);
            this.b.f();
            d f = this.b.i;
            if (!(f == null || (a2 = f.a()) == null)) {
                a2.setVibrationStrength(vibrationStrength);
            }
            FLogger.INSTANCE.getLocal().d(pu6.z.a(), "updateVibrationLevel success");
        }

        @DexIgnore
        public void a(fm5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "updateVibrationLevel fail!! errorValue=" + cVar.a());
            el4.a(this.b, false, true, 1, null);
            int c2 = cVar.c();
            if (c2 != 1101) {
                if (c2 == 8888) {
                    c.a(this.b.j, false, null, null, null, null, false, null, null, null, null, null, true, false, 6143, null);
                    this.b.f();
                    return;
                } else if (!(c2 == 1112 || c2 == 1113)) {
                    c.a(this.b.j, false, null, null, null, null, true, null, null, null, null, null, false, false, 8159, null);
                    this.b.f();
                    return;
                }
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(cVar.b());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            pu6 pu6 = this.b;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                pu6.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                this.b.f();
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    /*
    static {
        String simpleName = pu6.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchSettingViewModel::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public pu6(DeviceRepository deviceRepository, fm5 fm5, mm5 mm5, ad5 ad5, ch5 ch5, hm5 hm5, yl5 yl5, ro4 ro4, PortfolioApp portfolioApp) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(fm5, "mSetVibrationStrengthUseCase");
        ee7.b(mm5, "mUnlinkDeviceUseCase");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(hm5, "mSwitchActiveDeviceUseCase");
        ee7.b(yl5, "mReconnectDeviceUseCase");
        ee7.b(ro4, "challengeRepository");
        ee7.b(portfolioApp, "mApp");
        this.p = deviceRepository;
        this.q = fm5;
        this.r = mm5;
        this.s = ad5;
        this.t = ch5;
        this.u = hm5;
        this.v = yl5;
        this.w = ro4;
        this.x = portfolioApp;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>();
        this.g = mutableLiveData;
        LiveData<Device> b2 = ge.b(mutableLiveData, new j(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026lAsLiveData(serial)\n    }");
        this.h = b2;
        this.j = new c(false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.l = new k(this);
        this.m = new l(this);
        this.n = new MutableLiveData<>();
        this.o = new i(this);
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(y, "reconnectActiveDevice");
        el4.a(this, true, false, 2, null);
        yl5 yl5 = this.v;
        String a2 = this.g.a();
        if (a2 != null) {
            ee7.a((Object) a2, "mSerialLiveData.value!!");
            yl5.a(new yl5.c(a2), new o(this));
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void n() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "removeDevice serial=" + this.g + ".value");
        el4.a(this, true, false, 2, null);
        String c2 = this.x.c();
        if (!ee7.a((Object) this.g.a(), (Object) c2) || mh7.a((CharSequence) c2)) {
            a(this.g.a());
        } else {
            b(c2, 1);
        }
    }

    @DexIgnore
    public final void o() {
        c.a(this.j, false, null, null, null, null, false, null, null, null, null, null, false, false, 8191, null);
        this.q.g();
        this.v.e();
        this.u.h();
        nj5.d.a(CommunicateMode.FORCE_CONNECT, CommunicateMode.SET_VIBRATION_STRENGTH, CommunicateMode.SWITCH_DEVICE);
        PortfolioApp portfolioApp = this.x;
        i iVar = this.o;
        portfolioApp.registerReceiver(iVar, new IntentFilter(this.x.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<Device> liveData = this.h;
        if (liveData != null) {
            liveData.a(this.m);
        }
    }

    @DexIgnore
    public final void p() {
        try {
            this.u.i();
            LiveData<Device> liveData = this.h;
            if (liveData != null) {
                liveData.b(this.m);
            }
            this.v.f();
            this.q.h();
            this.x.unregisterReceiver(this.o);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = y;
            local.e(str, "stop with " + e2);
        }
        this.f.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void q() {
        CharSequence charSequence;
        String a2 = this.g.a();
        if (a2 != null) {
            if (TextUtils.equals(PortfolioApp.g0.c().c(), a2)) {
                PortfolioApp portfolioApp = this.x;
                ee7.a((Object) a2, "it");
                if (portfolioApp.g(a2)) {
                    charSequence = this.x.getString(2131886733);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = y;
                    local.d(str, "updateSyncTime " + charSequence);
                    c.a(this.j, false, null, charSequence.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
                    this.k = charSequence.toString();
                    f();
                    this.f.postDelayed(this.l, 60000);
                }
            }
            long e2 = this.t.e(a2);
            if (((int) e2) == 0) {
                charSequence = "";
            } else if (System.currentTimeMillis() - e2 < 60000) {
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131887136);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                charSequence = String.format(a3, Arrays.copyOf(new Object[]{1}, 1));
                ee7.a((Object) charSequence, "java.lang.String.format(format, *args)");
            } else {
                charSequence = DateUtils.getRelativeTimeSpanString(e2, System.currentTimeMillis(), TimeUnit.SECONDS.toMillis(1));
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = y;
            local2.d(str2, "updateSyncTime " + charSequence);
            c.a(this.j, false, null, charSequence.toString(), null, null, false, null, null, null, null, null, false, false, 8187, null);
            this.k = charSequence.toString();
            f();
            this.f.postDelayed(this.l, 60000);
        }
    }

    @DexIgnore
    public final boolean r() {
        String a2 = this.g.a();
        if (a2 == null) {
            return true;
        }
        if (FossilDeviceSerialPatternUtil.isSamSlimDevice(a2) || FossilDeviceSerialPatternUtil.isDianaDevice(a2)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void c(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        this.x.a(str, true);
    }

    @DexIgnore
    public final void d(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        this.x.a(str, true);
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d(y, "checkToReconnectOrSwitchActiveDevice");
        String c2 = this.x.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "activeSerial=" + c2 + ", mSerialLiveData=" + this.g.a());
        if (ee7.a((Object) this.g.a(), (Object) c2)) {
            m();
        } else if (!xw6.b(PortfolioApp.g0.c())) {
            c.a(this.j, false, null, null, null, new r87(601, ""), false, null, null, null, null, null, false, false, 8175, null);
            f();
        } else {
            el4.a(this, true, false, 2, null);
            ik7 unused = xh7.b(ie.a(this), null, null, new e(this, c2, null), 3, null);
        }
    }

    @DexIgnore
    public final void f(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        a(str, 2);
    }

    @DexIgnore
    public final LiveData<a> g() {
        return this.n;
    }

    @DexIgnore
    public final String h() {
        String b2;
        d dVar = this.i;
        return (dVar == null || (b2 = dVar.b()) == null) ? "" : b2;
    }

    @DexIgnore
    public final d i() {
        return this.i;
    }

    @DexIgnore
    public final String j() {
        return this.k;
    }

    @DexIgnore
    public final MutableLiveData<c> k() {
        return this.e;
    }

    @DexIgnore
    public final String l() {
        return this.g.a();
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        a(str);
    }

    @DexIgnore
    public final void g(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        this.x.a(str, false);
    }

    @DexIgnore
    public final void h(String str) {
        ee7.b(str, "serial");
        el4.a(this, true, false, 2, null);
        this.x.a(str, false);
    }

    @DexIgnore
    public final void i(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "setWatchSerial, serial=" + str);
        this.g.b(str);
    }

    @DexIgnore
    public final void f() {
        this.j.a(i());
        this.j.a(j());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, ".emitUIState(), mUiModelWrapper=" + this.j);
        this.e.a(this.j);
    }

    @DexIgnore
    public final void b(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "syncDevice - serial=" + str + ", userAction=" + i2);
        this.s.b(str).a(new km5(!be5.o.a(FossilDeviceSerialPatternUtil.getDeviceBySerial(str)) ? 10 : 15, str, false), new p(this, str, i2));
    }

    @DexIgnore
    public final void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "removeDevice " + str);
        this.r.a(str != null ? new mm5.b(str) : null, new g(this, str));
    }

    @DexIgnore
    public final void a(int i2) {
        d dVar;
        Device a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.d(str, "updateVibrationLevel " + i2 + " of " + this.g.a());
        String a3 = this.g.a();
        if (a3 != null && (dVar = this.i) != null && (a2 = dVar.a()) != null) {
            Integer vibrationStrength = a2.getVibrationStrength();
            if ((vibrationStrength == null || vibrationStrength.intValue() != i2) && !FossilDeviceSerialPatternUtil.isSamSlimDevice(a3) && !FossilDeviceSerialPatternUtil.isDianaDevice(a3)) {
                el4.a(this, true, false, 2, null);
                fm5 fm5 = this.q;
                ee7.a((Object) a3, "it");
                fm5.a(new fm5.b(a3, i2), new q(a2, a3, this, i2));
            }
        }
    }

    @DexIgnore
    public final void e(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "force switch to device " + str);
        el4.a(this, true, false, 2, null);
        this.u.a(new hm5.b(str, 4), new n(this, str));
    }

    @DexIgnore
    public final void a(Device device) {
        if (device != null) {
            ik7 unused = xh7.b(ie.a(this), null, null, new m(device, null, this, device), 3, null);
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, z2, null), 3, null);
    }

    @DexIgnore
    public final void a(String str, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = y;
        local.d(str2, "switch to device " + str + " mode " + i2);
        this.u.a(new hm5.b(str, i2), new h(this, str));
    }
}
