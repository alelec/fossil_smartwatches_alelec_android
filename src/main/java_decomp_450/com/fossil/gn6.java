package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gn6 implements Factory<fn6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public gn6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static gn6 a(Provider<ThemeRepository> provider) {
        return new gn6(provider);
    }

    @DexIgnore
    public static fn6 a(ThemeRepository themeRepository) {
        return new fn6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fn6 get() {
        return a(this.a.get());
    }
}
