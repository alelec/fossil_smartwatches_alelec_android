package com.fossil;

import com.fossil.fw3;
import com.fossil.wy3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vy3 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public wy3.p d;
    @DexIgnore
    public wy3.p e;
    @DexIgnore
    public aw3<Object> f;

    @DexIgnore
    @CanIgnoreReturnValue
    public vy3 a(aw3<Object> aw3) {
        jw3.b(this.f == null, "key equivalence was already set to %s", this.f);
        jw3.a(aw3);
        this.f = aw3;
        this.a = true;
        return this;
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public vy3 b(int i) {
        boolean z = true;
        jw3.b(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        jw3.a(z);
        this.b = i;
        return this;
    }

    @DexIgnore
    public aw3<Object> c() {
        return (aw3) fw3.a(this.f, d().defaultEquivalence());
    }

    @DexIgnore
    public wy3.p d() {
        return (wy3.p) fw3.a(this.d, wy3.p.STRONG);
    }

    @DexIgnore
    public wy3.p e() {
        return (wy3.p) fw3.a(this.e, wy3.p.STRONG);
    }

    @DexIgnore
    public <K, V> ConcurrentMap<K, V> f() {
        if (!this.a) {
            return new ConcurrentHashMap(b(), 0.75f, a());
        }
        return wy3.create(this);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public vy3 g() {
        a(wy3.p.WEAK);
        return this;
    }

    @DexIgnore
    public String toString() {
        fw3.b a2 = fw3.a(this);
        int i = this.b;
        if (i != -1) {
            a2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            a2.a("concurrencyLevel", i2);
        }
        wy3.p pVar = this.d;
        if (pVar != null) {
            a2.a("keyStrength", zv3.a(pVar.toString()));
        }
        wy3.p pVar2 = this.e;
        if (pVar2 != null) {
            a2.a("valueStrength", zv3.a(pVar2.toString()));
        }
        if (this.f != null) {
            a2.b("keyEquivalence");
        }
        return a2.toString();
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public vy3 a(int i) {
        boolean z = true;
        jw3.b(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        jw3.a(z);
        this.c = i;
        return this;
    }

    @DexIgnore
    public int b() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    @DexIgnore
    public vy3 b(wy3.p pVar) {
        jw3.b(this.e == null, "Value strength was already set to %s", this.e);
        jw3.a(pVar);
        this.e = pVar;
        if (pVar != wy3.p.STRONG) {
            this.a = true;
        }
        return this;
    }

    @DexIgnore
    public int a() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    @DexIgnore
    public vy3 a(wy3.p pVar) {
        jw3.b(this.d == null, "Key strength was already set to %s", this.d);
        jw3.a(pVar);
        this.d = pVar;
        if (pVar != wy3.p.STRONG) {
            this.a = true;
        }
        return this;
    }
}
