package com.fossil;

import java.io.Serializable;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ex3<T> extends jz3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ zx3<Comparator<? super T>> comparators;

    @DexIgnore
    public ex3(Comparator<? super T> comparator, Comparator<? super T> comparator2) {
        this.comparators = zx3.of(comparator, comparator2);
    }

    @DexIgnore
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(T t, T t2) {
        int size = this.comparators.size();
        for (int i = 0; i < size; i++) {
            int compare = this.comparators.get(i).compare(t, t2);
            if (compare != 0) {
                return compare;
            }
        }
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ex3) {
            return this.comparators.equals(((ex3) obj).comparators);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.comparators.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.compound(" + this.comparators + ")";
    }

    @DexIgnore
    public ex3(Iterable<? extends Comparator<? super T>> iterable) {
        this.comparators = zx3.copyOf(iterable);
    }
}
