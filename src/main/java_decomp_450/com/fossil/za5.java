package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum za5 {
    EMAIL(Constants.EMAIL, false),
    FACEBOOK(Constants.FACEBOOK, true),
    GOOGLE("google", true),
    APPLE("apple", true),
    WECHAT("wechat", true),
    WEIBO("weibo", true);
    
    @DexIgnore
    public /* final */ boolean isSSO;
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public za5(String str, boolean z) {
        this.value = str;
        this.isSSO = z;
    }

    @DexIgnore
    public static za5 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            za5[] values = values();
            for (za5 za5 : values) {
                if (str.equalsIgnoreCase(za5.value)) {
                    return za5;
                }
            }
        }
        return EMAIL;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }

    @DexIgnore
    public boolean isSSO() {
        return this.isSSO;
    }
}
