package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.fossil.tp3;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zp3 extends Service implements sp3 {
    @DexIgnore
    public ComponentName a;
    @DexIgnore
    public c b;
    @DexIgnore
    public IBinder c;
    @DexIgnore
    public Intent d;
    @DexIgnore
    public Looper e;
    @DexIgnore
    public /* final */ Object f; // = new Object();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public dq3 h; // = new dq3(new a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends tp3.b {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.tp3.b
        public final void a(tp3.a aVar) {
            zp3.this.a(aVar);
        }

        @DexIgnore
        @Override // com.fossil.tp3.b
        public final void b(tp3.a aVar, int i, int i2) {
            zp3.this.b(aVar, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.tp3.b
        public final void c(tp3.a aVar, int i, int i2) {
            zp3.this.c(aVar, i, i2);
        }

        @DexIgnore
        @Override // com.fossil.tp3.b
        public final void a(tp3.a aVar, int i, int i2) {
            zp3.this.a(aVar, i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b(zp3 zp3) {
        }

        @DexIgnore
        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        }

        @DexIgnore
        public final void onServiceDisconnected(ComponentName componentName) {
        }
    }

    @DexIgnore
    public Looper a() {
        if (this.e == null) {
            HandlerThread handlerThread = new HandlerThread("WearableListenerService");
            handlerThread.start();
            this.e = handlerThread.getLooper();
        }
        return this.e;
    }

    @DexIgnore
    public void a(qp3 qp3) {
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public void a(rp3 rp3) {
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public void a(rp3 rp3, int i, int i2) {
    }

    @DexIgnore
    public void a(tp3.a aVar) {
    }

    @DexIgnore
    public void a(tp3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public void a(up3 up3) {
    }

    @DexIgnore
    public void a(wq3 wq3) {
    }

    @DexIgnore
    public void a(xp3 xp3) {
    }

    @DexIgnore
    public void a(xq3 xq3) {
    }

    @DexIgnore
    public void a(yp3 yp3) {
    }

    @DexIgnore
    public void a(List<yp3> list) {
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public void b(rp3 rp3, int i, int i2) {
    }

    @DexIgnore
    public void b(tp3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public void b(yp3 yp3) {
    }

    @DexIgnore
    @Override // com.fossil.sp3
    public void c(rp3 rp3, int i, int i2) {
    }

    @DexIgnore
    public void c(tp3.a aVar, int i, int i2) {
    }

    @DexIgnore
    public final IBinder onBind(Intent intent) {
        if ("com.google.android.gms.wearable.BIND_LISTENER".equals(intent.getAction())) {
            return this.c;
        }
        return null;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.a = new ComponentName(this, zp3.class.getName());
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.a);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 10);
            sb.append("onCreate: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        this.b = new c(a());
        Intent intent = new Intent("com.google.android.gms.wearable.BIND_LISTENER");
        this.d = intent;
        intent.setComponent(this.a);
        this.c = new d();
    }

    @DexIgnore
    public void onDestroy() {
        if (Log.isLoggable("WearableLS", 3)) {
            String valueOf = String.valueOf(this.a);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 11);
            sb.append("onDestroy: ");
            sb.append(valueOf);
            Log.d("WearableLS", sb.toString());
        }
        synchronized (this.f) {
            this.g = true;
            if (this.b != null) {
                this.b.a();
            } else {
                String valueOf2 = String.valueOf(this.a);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 111);
                sb2.append("onDestroy: mServiceHandler not set, did you override onCreate() but forget to call super.onCreate()? component=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
        super.onDestroy();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends Handler {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ b b; // = new b();

        @DexIgnore
        public c(Looper looper) {
            super(looper);
        }

        @DexIgnore
        public final void a() {
            getLooper().quit();
            a("quit");
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void b() {
            if (!this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(zp3.this.a);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
                    sb.append("bindService: ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                zp3.this.bindService(zp3.this.d, this.b, 1);
                this.a = true;
            }
        }

        @DexIgnore
        public final void dispatchMessage(Message message) {
            b();
            try {
                super.dispatchMessage(message);
            } finally {
                if (!hasMessages(0)) {
                    a("dispatch");
                }
            }
        }

        @DexIgnore
        @SuppressLint({"UntrackedBindService"})
        public final synchronized void a(String str) {
            if (this.a) {
                if (Log.isLoggable("WearableLS", 2)) {
                    String valueOf = String.valueOf(zp3.this.a);
                    StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 17 + String.valueOf(valueOf).length());
                    sb.append("unbindService: ");
                    sb.append(str);
                    sb.append(", ");
                    sb.append(valueOf);
                    Log.v("WearableLS", sb.toString());
                }
                try {
                    zp3.this.unbindService(this.b);
                } catch (RuntimeException e) {
                    Log.e("WearableLS", "Exception when unbinding from local service", e);
                }
                this.a = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends mq3 {
        @DexIgnore
        public volatile int a;

        @DexIgnore
        public d() {
            this.a = -1;
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(DataHolder dataHolder) {
            zq3 zq3 = new zq3(this, dataHolder);
            try {
                String valueOf = String.valueOf(dataHolder);
                int count = dataHolder.getCount();
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append(valueOf);
                sb.append(", rows=");
                sb.append(count);
                if (a(zq3, "onDataItemChanged", sb.toString())) {
                }
            } finally {
                dataHolder.close();
            }
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void b(pq3 pq3) {
            a(new cr3(this, pq3), "onPeerDisconnected", pq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void b(List<pq3> list) {
            a(new dr3(this, list), "onConnectedNodes", list);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(nq3 nq3) {
            a(new ar3(this, nq3), "onMessageReceived", nq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(pq3 pq3) {
            a(new br3(this, pq3), "onPeerConnected", pq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(aq3 aq3) {
            a(new er3(this, aq3), "onConnectedCapabilityChanged", aq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(uq3 uq3) {
            a(new fr3(this, uq3), "onNotificationReceived", uq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(sq3 sq3) {
            a(new gr3(this, sq3), "onEntityUpdate", sq3);
        }

        @DexIgnore
        @Override // com.fossil.lq3
        public final void a(eq3 eq3) {
            a(new hr3(this, eq3), "onChannelEvent", eq3);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0074 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0075  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(java.lang.Runnable r5, java.lang.String r6, java.lang.Object r7) {
            /*
                r4 = this;
                r0 = 3
                java.lang.String r1 = "WearableLS"
                boolean r1 = android.util.Log.isLoggable(r1, r0)
                r2 = 0
                r3 = 1
                if (r1 == 0) goto L_0x0029
                java.lang.Object[] r0 = new java.lang.Object[r0]
                r0[r2] = r6
                com.fossil.zp3 r6 = com.fossil.zp3.this
                android.content.ComponentName r6 = r6.a
                java.lang.String r6 = r6.toString()
                r0[r3] = r6
                r6 = 2
                r0[r6] = r7
                java.lang.String r6 = "%s: %s %s"
                java.lang.String r6 = java.lang.String.format(r6, r0)
                java.lang.String r7 = "WearableLS"
                android.util.Log.d(r7, r6)
            L_0x0029:
                int r6 = android.os.Binder.getCallingUid()
                int r7 = r4.a
                if (r6 != r7) goto L_0x0033
            L_0x0031:
                r6 = 1
                goto L_0x0072
            L_0x0033:
                com.fossil.zp3 r7 = com.fossil.zp3.this
                com.fossil.rq3 r7 = com.fossil.rq3.a(r7)
                java.lang.String r0 = "com.google.android.wearable.app.cn"
                boolean r7 = r7.a(r0)
                if (r7 == 0) goto L_0x004e
                com.fossil.zp3 r7 = com.fossil.zp3.this
                java.lang.String r0 = "com.google.android.wearable.app.cn"
                boolean r7 = com.fossil.y92.a(r7, r6, r0)
                if (r7 == 0) goto L_0x004e
                r4.a = r6
                goto L_0x0031
            L_0x004e:
                com.fossil.zp3 r7 = com.fossil.zp3.this
                boolean r7 = com.fossil.y92.a(r7, r6)
                if (r7 == 0) goto L_0x0059
                r4.a = r6
                goto L_0x0031
            L_0x0059:
                r7 = 57
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>(r7)
                java.lang.String r7 = "Caller is not GooglePlayServices; caller UID: "
                r0.append(r7)
                r0.append(r6)
                java.lang.String r6 = r0.toString()
                java.lang.String r7 = "WearableLS"
                android.util.Log.e(r7, r6)
                r6 = 0
            L_0x0072:
                if (r6 != 0) goto L_0x0075
                return r2
            L_0x0075:
                com.fossil.zp3 r6 = com.fossil.zp3.this
                java.lang.Object r6 = r6.f
                monitor-enter(r6)
                com.fossil.zp3 r7 = com.fossil.zp3.this     // Catch:{ all -> 0x0091 }
                boolean r7 = r7.g     // Catch:{ all -> 0x0091 }
                if (r7 == 0) goto L_0x0086
                monitor-exit(r6)     // Catch:{ all -> 0x0091 }
                return r2
            L_0x0086:
                com.fossil.zp3 r7 = com.fossil.zp3.this     // Catch:{ all -> 0x0091 }
                com.fossil.zp3$c r7 = r7.b     // Catch:{ all -> 0x0091 }
                r7.post(r5)     // Catch:{ all -> 0x0091 }
                monitor-exit(r6)     // Catch:{ all -> 0x0091 }
                return r3
            L_0x0091:
                r5 = move-exception
                monitor-exit(r6)     // Catch:{ all -> 0x0091 }
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.zp3.d.a(java.lang.Runnable, java.lang.String, java.lang.Object):boolean");
        }
    }
}
