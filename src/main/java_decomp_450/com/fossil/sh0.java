package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sh0 extends uh1 {
    @DexIgnore
    public /* final */ byte[] G;
    @DexIgnore
    public byte[] H;
    @DexIgnore
    public /* final */ qk1 I;
    @DexIgnore
    public /* final */ qk1 J;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ sh0(ri1 ri1, x81 x81, qa1 qa1, int i, int i2) {
        super(qa1, ri1, (i2 & 8) != 0 ? 3 : i);
        this.G = x81.a();
        byte[] array = ByteBuffer.allocate(2).put(x81.c.a()).put(x81.d.a).array();
        ee7.a((Object) array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        this.H = array;
        qk1 qk1 = qk1.AUTHENTICATION;
        this.I = qk1;
        this.J = qk1;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final long a(rk1 rk1) {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final mw0 a(byte b) {
        return mc1.f.a(b);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 l() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] n() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final qk1 o() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public final byte[] q() {
        return this.H;
    }
}
