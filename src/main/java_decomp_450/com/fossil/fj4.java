package com.fossil;

import com.fossil.cj4;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;

public final class fj4 {
    public static final int[] a = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /*
        static {
            /*
                com.fossil.bj4[] r0 = com.fossil.bj4.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.fj4.a.a = r0
                com.fossil.bj4 r1 = com.fossil.bj4.NUMERIC     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.fj4.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.bj4 r1 = com.fossil.bj4.ALPHANUMERIC     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.fj4.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.bj4 r1 = com.fossil.bj4.BYTE     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.fj4.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.bj4 r1 = com.fossil.bj4.KANJI     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.fj4.a.<clinit>():void");
        }
        */
    }

    public static int a(ej4 ej4) {
        return gj4.a(ej4) + gj4.b(ej4) + gj4.c(ej4) + gj4.d(ej4);
    }

    public static void b(CharSequence charSequence, ch4 ch4) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - '0';
            int i2 = i + 2;
            if (i2 < length) {
                ch4.a((charAt * 100) + ((charSequence.charAt(i + 1) - '0') * 10) + (charSequence.charAt(i2) - '0'), 10);
                i += 3;
            } else {
                i++;
                if (i < length) {
                    ch4.a((charAt * 10) + (charSequence.charAt(i) - '0'), 7);
                    i = i2;
                } else {
                    ch4.a(charAt, 4);
                }
            }
        }
    }

    public static ij4 a(String str, aj4 aj4, Map<og4, ?> map) throws tg4 {
        String str2;
        cj4 cj4;
        eh4 characterSetECIByName;
        if (map == null || !map.containsKey(og4.CHARACTER_SET)) {
            str2 = "ISO-8859-1";
        } else {
            str2 = map.get(og4.CHARACTER_SET).toString();
        }
        bj4 a2 = a(str, str2);
        ch4 ch4 = new ch4();
        if (a2 == bj4.BYTE && !"ISO-8859-1".equals(str2) && (characterSetECIByName = eh4.getCharacterSetECIByName(str2)) != null) {
            a(characterSetECIByName, ch4);
        }
        a(a2, ch4);
        ch4 ch42 = new ch4();
        a(str, a2, ch42, str2);
        if (map == null || !map.containsKey(og4.QR_VERSION)) {
            cj4 = a(aj4, a2, ch4, ch42);
        } else {
            cj4 = cj4.a(Integer.parseInt(map.get(og4.QR_VERSION).toString()));
            if (!a(a(a2, ch4, ch42, cj4), cj4, aj4)) {
                throw new tg4("Data too big for requested version");
            }
        }
        ch4 ch43 = new ch4();
        ch43.a(ch4);
        a(a2 == bj4.BYTE ? ch42.f() : str.length(), cj4, a2, ch43);
        ch43.a(ch42);
        cj4.b a3 = cj4.a(aj4);
        int b = cj4.b() - a3.d();
        a(b, ch43);
        ch4 a4 = a(ch43, cj4.b(), b, a3.c());
        ij4 ij4 = new ij4();
        ij4.a(aj4);
        ij4.a(a2);
        ij4.a(cj4);
        int a5 = cj4.a();
        ej4 ej4 = new ej4(a5, a5);
        int a6 = a(a4, aj4, cj4, ej4);
        ij4.a(a6);
        hj4.a(a4, aj4, cj4, a6, ej4);
        ij4.a(ej4);
        return ij4;
    }

    public static cj4 a(aj4 aj4, bj4 bj4, ch4 ch4, ch4 ch42) throws tg4 {
        return a(a(bj4, ch4, ch42, a(a(bj4, ch4, ch42, cj4.a(1)), aj4)), aj4);
    }

    public static int a(bj4 bj4, ch4 ch4, ch4 ch42, cj4 cj4) {
        return ch4.d() + bj4.getCharacterCountBits(cj4) + ch42.d();
    }

    public static int a(int i) {
        int[] iArr = a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    public static bj4 a(String str, String str2) {
        if ("Shift_JIS".equals(str2) && a(str)) {
            return bj4.KANJI;
        }
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                z2 = true;
            } else if (a(charAt) == -1) {
                return bj4.BYTE;
            } else {
                z = true;
            }
        }
        if (z) {
            return bj4.ALPHANUMERIC;
        }
        if (z2) {
            return bj4.NUMERIC;
        }
        return bj4.BYTE;
    }

    public static boolean a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                byte b = bytes[i] & 255;
                if ((b < 129 || b > 159) && (b < 224 || b > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    public static int a(ch4 ch4, aj4 aj4, cj4 cj4, ej4 ej4) throws tg4 {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        for (int i3 = 0; i3 < 8; i3++) {
            hj4.a(ch4, aj4, cj4, i3, ej4);
            int a2 = a(ej4);
            if (a2 < i) {
                i2 = i3;
                i = a2;
            }
        }
        return i2;
    }

    public static cj4 a(int i, aj4 aj4) throws tg4 {
        for (int i2 = 1; i2 <= 40; i2++) {
            cj4 a2 = cj4.a(i2);
            if (a(i, a2, aj4)) {
                return a2;
            }
        }
        throw new tg4("Data too big");
    }

    public static boolean a(int i, cj4 cj4, aj4 aj4) {
        return cj4.b() - cj4.a(aj4).d() >= (i + 7) / 8;
    }

    public static void a(int i, ch4 ch4) throws tg4 {
        int i2 = i << 3;
        if (ch4.d() <= i2) {
            for (int i3 = 0; i3 < 4 && ch4.d() < i2; i3++) {
                ch4.a(false);
            }
            int d = ch4.d() & 7;
            if (d > 0) {
                while (d < 8) {
                    ch4.a(false);
                    d++;
                }
            }
            int f = i - ch4.f();
            for (int i4 = 0; i4 < f; i4++) {
                ch4.a((i4 & 1) == 0 ? 236 : 17, 8);
            }
            if (ch4.d() != i2) {
                throw new tg4("Bits size does not equal capacity");
            }
            return;
        }
        throw new tg4("data bits cannot fit in the QR Code" + ch4.d() + " > " + i2);
    }

    public static void a(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) throws tg4 {
        if (i4 < i3) {
            int i5 = i % i3;
            int i6 = i3 - i5;
            int i7 = i / i3;
            int i8 = i7 + 1;
            int i9 = i2 / i3;
            int i10 = i9 + 1;
            int i11 = i7 - i9;
            int i12 = i8 - i10;
            if (i11 != i12) {
                throw new tg4("EC bytes mismatch");
            } else if (i3 != i6 + i5) {
                throw new tg4("RS blocks mismatch");
            } else if (i != ((i9 + i11) * i6) + ((i10 + i12) * i5)) {
                throw new tg4("Total bytes mismatch");
            } else if (i4 < i6) {
                iArr[0] = i9;
                iArr2[0] = i11;
            } else {
                iArr[0] = i10;
                iArr2[0] = i12;
            }
        } else {
            throw new tg4("Block ID too large");
        }
    }

    public static ch4 a(ch4 ch4, int i, int i2, int i3) throws tg4 {
        if (ch4.f() == i2) {
            ArrayList<dj4> arrayList = new ArrayList(i3);
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < i3; i7++) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                a(i, i2, i3, i7, iArr, iArr2);
                int i8 = iArr[0];
                byte[] bArr = new byte[i8];
                ch4.a(i4 << 3, bArr, 0, i8);
                byte[] a2 = a(bArr, iArr2[0]);
                arrayList.add(new dj4(bArr, a2));
                i5 = Math.max(i5, i8);
                i6 = Math.max(i6, a2.length);
                i4 += iArr[0];
            }
            if (i2 == i4) {
                ch4 ch42 = new ch4();
                for (int i9 = 0; i9 < i5; i9++) {
                    for (dj4 dj4 : arrayList) {
                        byte[] a3 = dj4.a();
                        if (i9 < a3.length) {
                            ch42.a(a3[i9], 8);
                        }
                    }
                }
                for (int i10 = 0; i10 < i6; i10++) {
                    for (dj4 dj42 : arrayList) {
                        byte[] b = dj42.b();
                        if (i10 < b.length) {
                            ch42.a(b[i10], 8);
                        }
                    }
                }
                if (i == ch42.f()) {
                    return ch42;
                }
                throw new tg4("Interleaving error: " + i + " and " + ch42.f() + " differ.");
            }
            throw new tg4("Data bytes does not match offset");
        }
        throw new tg4("Number of bits and data bytes does not match");
    }

    public static byte[] a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        new hh4(fh4.k).a(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) iArr[length + i3];
        }
        return bArr2;
    }

    public static void a(bj4 bj4, ch4 ch4) {
        ch4.a(bj4.getBits(), 4);
    }

    public static void a(int i, cj4 cj4, bj4 bj4, ch4 ch4) throws tg4 {
        int characterCountBits = bj4.getCharacterCountBits(cj4);
        int i2 = 1 << characterCountBits;
        if (i < i2) {
            ch4.a(i, characterCountBits);
            return;
        }
        throw new tg4(i + " is bigger than " + (i2 - 1));
    }

    public static void a(String str, bj4 bj4, ch4 ch4, String str2) throws tg4 {
        int i = a.a[bj4.ordinal()];
        if (i == 1) {
            b(str, ch4);
        } else if (i == 2) {
            a((CharSequence) str, ch4);
        } else if (i == 3) {
            a(str, ch4, str2);
        } else if (i == 4) {
            a(str, ch4);
        } else {
            throw new tg4("Invalid mode: " + bj4);
        }
    }

    public static void a(CharSequence charSequence, ch4 ch4) throws tg4 {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int a2 = a(charSequence.charAt(i));
            if (a2 != -1) {
                int i2 = i + 1;
                if (i2 < length) {
                    int a3 = a(charSequence.charAt(i2));
                    if (a3 != -1) {
                        ch4.a((a2 * 45) + a3, 11);
                        i += 2;
                    } else {
                        throw new tg4();
                    }
                } else {
                    ch4.a(a2, 6);
                    i = i2;
                }
            } else {
                throw new tg4();
            }
        }
    }

    public static void a(String str, ch4 ch4, String str2) throws tg4 {
        try {
            for (byte b : str.getBytes(str2)) {
                ch4.a(b, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new tg4(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[LOOP:0: B:4:0x0008->B:17:0x0035, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r6, com.fossil.ch4 r7) throws com.fossil.tg4 {
        /*
            java.lang.String r0 = "Shift_JIS"
            byte[] r6 = r6.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x004d }
            int r0 = r6.length
            r1 = 0
        L_0x0008:
            if (r1 >= r0) goto L_0x004c
            byte r2 = r6[r1]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r1 + 1
            byte r3 = r6[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 8
            r2 = r2 | r3
            r3 = 33088(0x8140, float:4.6366E-41)
            r4 = -1
            if (r2 < r3) goto L_0x0024
            r5 = 40956(0x9ffc, float:5.7392E-41)
            if (r2 > r5) goto L_0x0024
        L_0x0022:
            int r2 = r2 - r3
            goto L_0x0033
        L_0x0024:
            r3 = 57408(0xe040, float:8.0446E-41)
            if (r2 < r3) goto L_0x0032
            r3 = 60351(0xebbf, float:8.457E-41)
            if (r2 > r3) goto L_0x0032
            r3 = 49472(0xc140, float:6.9325E-41)
            goto L_0x0022
        L_0x0032:
            r2 = -1
        L_0x0033:
            if (r2 == r4) goto L_0x0044
            int r3 = r2 >> 8
            int r3 = r3 * 192
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r3 + r2
            r2 = 13
            r7.a(r3, r2)
            int r1 = r1 + 2
            goto L_0x0008
        L_0x0044:
            com.fossil.tg4 r6 = new com.fossil.tg4
            java.lang.String r7 = "Invalid byte sequence"
            r6.<init>(r7)
            throw r6
        L_0x004c:
            return
        L_0x004d:
            r6 = move-exception
            com.fossil.tg4 r7 = new com.fossil.tg4
            r7.<init>(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.fj4.a(java.lang.String, com.fossil.ch4):void");
    }

    public static void a(eh4 eh4, ch4 ch4) {
        ch4.a(bj4.ECI.getBits(), 4);
        ch4.a(eh4.getValue(), 8);
    }
}
