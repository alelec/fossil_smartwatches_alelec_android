package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.o14;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p14 implements o14 {
    @DexIgnore
    public static volatile o14 c;
    @DexIgnore
    public /* final */ eb3 a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new ConcurrentHashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements o14.a {
        @DexIgnore
        public a(p14 p14, String str) {
        }
    }

    @DexIgnore
    public p14(eb3 eb3) {
        a72.a(eb3);
        this.a = eb3;
    }

    @DexIgnore
    public static o14 a(l14 l14, Context context, i94 i94) {
        a72.a(l14);
        a72.a(context);
        a72.a(i94);
        a72.a(context.getApplicationContext());
        if (c == null) {
            synchronized (p14.class) {
                if (c == null) {
                    Bundle bundle = new Bundle(1);
                    if (l14.h()) {
                        i94.a(j14.class, w14.a, x14.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", l14.g());
                    }
                    c = new p14(sn2.a(context, (String) null, (String) null, (String) null, bundle).a());
                }
            }
        }
        return c;
    }

    @DexIgnore
    @Override // com.fossil.o14
    public void a(String str, String str2, Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        if (q14.a(str) && q14.a(str2, bundle) && q14.a(str, str2, bundle)) {
            q14.b(str, str2, bundle);
            this.a.a(str, str2, bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.o14
    public void a(String str, String str2, Object obj) {
        if (q14.a(str) && q14.a(str, str2)) {
            this.a.a(str, str2, obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.o14
    public o14.a a(String str, o14.b bVar) {
        Object obj;
        a72.a(bVar);
        if (!q14.a(str) || a(str)) {
            return null;
        }
        eb3 eb3 = this.a;
        if ("fiam".equals(str)) {
            obj = new t14(eb3, bVar);
        } else if (CrashDumperPlugin.NAME.equals(str) || "clx".equals(str)) {
            obj = new v14(eb3, bVar);
        } else {
            obj = null;
        }
        if (obj == null) {
            return null;
        }
        this.b.put(str, obj);
        return new a(this, str);
    }

    @DexIgnore
    public final boolean a(String str) {
        return !str.isEmpty() && this.b.containsKey(str) && this.b.get(str) != null;
    }

    @DexIgnore
    public static final /* synthetic */ void a(f94 f94) {
        boolean z = ((j14) f94.a()).a;
        synchronized (p14.class) {
            ((p14) c).a.a(z);
        }
    }
}
