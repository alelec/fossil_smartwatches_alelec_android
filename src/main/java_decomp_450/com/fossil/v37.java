package com.fossil;

import com.facebook.appevents.UserDataStore;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v37 {
    @DexIgnore
    public long a; // = 0;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public int d; // = 0;
    @DexIgnore
    public String e; // = "";

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("tm", this.a);
            jSONObject.put(UserDataStore.STATE, this.b);
            if (this.c != null) {
                jSONObject.put("dm", this.c);
            }
            jSONObject.put("pt", this.d);
            if (this.e != null) {
                jSONObject.put("rip", this.e);
            }
            jSONObject.put("ts", System.currentTimeMillis() / 1000);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    @DexIgnore
    public void a(int i) {
        this.d = i;
    }

    @DexIgnore
    public void a(long j) {
        this.a = j;
    }

    @DexIgnore
    public void a(String str) {
        this.c = str;
    }

    @DexIgnore
    public void b(int i) {
        this.b = i;
    }

    @DexIgnore
    public void b(String str) {
        this.e = str;
    }
}
