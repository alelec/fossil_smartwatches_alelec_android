package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Collections;
import java.util.IdentityHashMap;
import java.util.Set;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yh {
    @DexIgnore
    public /* final */ Set<LiveData> a; // = Collections.newSetFromMap(new IdentityHashMap());
    @DexIgnore
    public /* final */ ci b;

    @DexIgnore
    public yh(ci ciVar) {
        this.b = ciVar;
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return new gi(this.b, this, z, callable, strArr);
    }

    @DexIgnore
    public void b(LiveData liveData) {
        this.a.remove(liveData);
    }

    @DexIgnore
    public void a(LiveData liveData) {
        this.a.add(liveData);
    }
}
