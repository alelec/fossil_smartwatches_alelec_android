package com.fossil;

import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.internal.ServerProtocol;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ti3 extends kh3 {
    @DexIgnore
    public pj3 c;
    @DexIgnore
    public oi3 d;
    @DexIgnore
    public /* final */ Set<ri3> e; // = new CopyOnWriteArraySet();
    @DexIgnore
    public boolean f;
    @DexIgnore
    public /* final */ AtomicReference<String> g; // = new AtomicReference<>();
    @DexIgnore
    public /* final */ pm3 h;
    @DexIgnore
    public boolean i; // = true;

    @DexIgnore
    public ti3(oh3 oh3) {
        super(oh3);
        this.h = new pm3(oh3);
    }

    @DexIgnore
    public final void A() {
        if (f().getApplicationContext() instanceof Application) {
            ((Application) f().getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    @DexIgnore
    public final Boolean B() {
        AtomicReference atomicReference = new AtomicReference();
        return (Boolean) c().a(atomicReference, 15000, "boolean test flag value", new ui3(this, atomicReference));
    }

    @DexIgnore
    public final String C() {
        AtomicReference atomicReference = new AtomicReference();
        return (String) c().a(atomicReference, 15000, "String test flag value", new ej3(this, atomicReference));
    }

    @DexIgnore
    public final Long D() {
        AtomicReference atomicReference = new AtomicReference();
        return (Long) c().a(atomicReference, 15000, "long test flag value", new fj3(this, atomicReference));
    }

    @DexIgnore
    public final Integer E() {
        AtomicReference atomicReference = new AtomicReference();
        return (Integer) c().a(atomicReference, 15000, "int test flag value", new ij3(this, atomicReference));
    }

    @DexIgnore
    public final Double F() {
        AtomicReference atomicReference = new AtomicReference();
        return (Double) c().a(atomicReference, 15000, "double test flag value", new hj3(this, atomicReference));
    }

    @DexIgnore
    public final String G() {
        a();
        return this.g.get();
    }

    @DexIgnore
    public final void H() {
        g();
        a();
        w();
        if (((ii3) this).a.l()) {
            if (l().a(wb3.h0)) {
                ym3 l = l();
                l.b();
                Boolean e2 = l.e("google_analytics_deferred_deep_link_enabled");
                if (e2 != null && e2.booleanValue()) {
                    e().A().a("Deferred Deep Link feature enabled.");
                    c().a(new vi3(this));
                }
            }
            q().D();
            this.i = false;
            String y = k().y();
            if (!TextUtils.isEmpty(y)) {
                h().n();
                if (!y.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", y);
                    a("auto", "_ou", bundle);
                }
            }
        }
    }

    @DexIgnore
    public final String I() {
        wj3 A = ((ii3) this).a.D().A();
        if (A != null) {
            return A.a;
        }
        return null;
    }

    @DexIgnore
    public final String J() {
        wj3 A = ((ii3) this).a.D().A();
        if (A != null) {
            return A.b;
        }
        return null;
    }

    @DexIgnore
    public final String K() {
        if (((ii3) this).a.z() != null) {
            return ((ii3) this).a.z();
        }
        try {
            return xj3.a(f(), "google_app_id");
        } catch (IllegalStateException e2) {
            ((ii3) this).a.e().t().a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    @DexIgnore
    public final void L() {
        g();
        String a = k().s.a();
        if (a != null) {
            if ("unset".equals(a)) {
                a("app", "_npa", (Object) null, zzm().b());
            } else {
                a("app", "_npa", Long.valueOf(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE.equals(a) ? 1 : 0), zzm().b());
            }
        }
        if (!((ii3) this).a.g() || !this.i) {
            e().A().a("Updating Scion state (FE)");
            q().B();
            return;
        }
        e().A().a("Recording app launch after enabling measurement for the first time (FE)");
        H();
        if (t13.a() && l().a(wb3.w0)) {
            t().d.a();
        }
        if (h13.a() && l().a(wb3.B0)) {
            if (!(((ii3) this).a.s().a.p().k.a() > 0)) {
                bh3 s = ((ii3) this).a.s();
                s.a.i();
                s.a(s.a.f().getPackageName());
            }
        }
        if (l().a(wb3.R0)) {
            c().a(new nj3(this));
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        w();
        a();
        c().a(new kj3(this, z));
    }

    @DexIgnore
    public final void b(boolean z) {
        g();
        a();
        w();
        e().A().a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        k().b(z);
        L();
    }

    @DexIgnore
    public final void c(String str, String str2, Bundle bundle) {
        a();
        b((String) null, str, str2, bundle);
    }

    @DexIgnore
    public final void d(Bundle bundle) {
        g();
        w();
        a72.a(bundle);
        a72.b(bundle.getString("name"));
        if (!((ii3) this).a.g()) {
            e().B().a("Conditional property not cleared since app measurement is disabled");
            return;
        }
        try {
            q().a(new wm3(bundle.getString("app_id"), bundle.getString("origin"), new em3(bundle.getString("name"), 0, null, null), bundle.getLong("creation_timestamp"), bundle.getBoolean("active"), bundle.getString("trigger_event_name"), null, bundle.getLong("trigger_timeout"), null, bundle.getLong("time_to_live"), j().a(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), bundle.getLong("creation_timestamp"), true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    @Override // com.fossil.kh3
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final void c(Bundle bundle) {
        g();
        w();
        a72.a(bundle);
        a72.b(bundle.getString("name"));
        a72.b(bundle.getString("origin"));
        a72.a(bundle.get("value"));
        if (!((ii3) this).a.g()) {
            e().B().a("Conditional property not set since app measurement is disabled");
            return;
        }
        em3 em3 = new em3(bundle.getString("name"), bundle.getLong("triggered_timestamp"), bundle.get("value"), bundle.getString("origin"));
        try {
            ub3 a = j().a(bundle.getString("app_id"), bundle.getString("triggered_event_name"), bundle.getBundle("triggered_event_params"), bundle.getString("origin"), 0, true, false);
            q().a(new wm3(bundle.getString("app_id"), bundle.getString("origin"), em3, bundle.getLong("creation_timestamp"), false, bundle.getString("trigger_event_name"), j().a(bundle.getString("app_id"), bundle.getString("timed_out_event_name"), bundle.getBundle("timed_out_event_params"), bundle.getString("origin"), 0, true, false), bundle.getLong("trigger_timeout"), a, bundle.getLong("time_to_live"), j().a(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), 0, true, false)));
        } catch (IllegalArgumentException unused) {
        }
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, zzm().b());
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Bundle bundle) {
        a();
        g();
        a(str, str2, j, bundle, true, this.d == null || jm3.i(str2), false, null);
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        a();
        g();
        a(str, str2, zzm().b(), bundle);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:129:0x031e  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0357  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x03bc  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x03d0  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x03ea  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0404  */
    /* JADX WARNING: Removed duplicated region for block: B:161:0x0444  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04f7  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0506  */
    /* JADX WARNING: Removed duplicated region for block: B:195:0x0586  */
    /* JADX WARNING: Removed duplicated region for block: B:209:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r28, java.lang.String r29, long r30, android.os.Bundle r32, boolean r33, boolean r34, boolean r35, java.lang.String r36) {
        /*
            r27 = this;
            r7 = r27
            r8 = r28
            r15 = r29
            r13 = r30
            r12 = r32
            com.fossil.a72.b(r28)
            com.fossil.a72.a(r32)
            r27.g()
            r27.w()
            com.fossil.oh3 r0 = r7.a
            boolean r0 = r0.g()
            if (r0 != 0) goto L_0x002c
            com.fossil.jg3 r0 = r27.e()
            com.fossil.mg3 r0 = r0.A()
            java.lang.String r1 = "Event not sent since app measurement is disabled"
            r0.a(r1)
            return
        L_0x002c:
            com.fossil.ym3 r0 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.c0
            boolean r0 = r0.a(r1)
            if (r0 == 0) goto L_0x0056
            com.fossil.cg3 r0 = r27.p()
            java.util.List r0 = r0.G()
            if (r0 == 0) goto L_0x0056
            boolean r0 = r0.contains(r15)
            if (r0 != 0) goto L_0x0056
            com.fossil.jg3 r0 = r27.e()
            com.fossil.mg3 r0 = r0.A()
            java.lang.String r1 = "Dropping non-safelisted event. event name, origin"
            r0.a(r1, r15, r8)
            return
        L_0x0056:
            boolean r0 = r7.f
            r11 = 0
            r10 = 0
            r9 = 1
            if (r0 != 0) goto L_0x00b0
            r7.f = r9
            com.fossil.oh3 r0 = r7.a     // Catch:{ ClassNotFoundException -> 0x00a3 }
            boolean r0 = r0.C()     // Catch:{ ClassNotFoundException -> 0x00a3 }
            if (r0 != 0) goto L_0x0076
            java.lang.String r0 = "com.google.android.gms.tagmanager.TagManagerService"
            android.content.Context r1 = r27.f()     // Catch:{ ClassNotFoundException -> 0x00a3 }
            java.lang.ClassLoader r1 = r1.getClassLoader()     // Catch:{ ClassNotFoundException -> 0x00a3 }
            java.lang.Class r0 = java.lang.Class.forName(r0, r9, r1)     // Catch:{ ClassNotFoundException -> 0x00a3 }
            goto L_0x007c
        L_0x0076:
            java.lang.String r0 = "com.google.android.gms.tagmanager.TagManagerService"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00a3 }
        L_0x007c:
            java.lang.String r1 = "initialize"
            java.lang.Class[] r2 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x0094 }
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            r2[r10] = r3     // Catch:{ Exception -> 0x0094 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0094 }
            java.lang.Object[] r1 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x0094 }
            android.content.Context r2 = r27.f()     // Catch:{ Exception -> 0x0094 }
            r1[r10] = r2     // Catch:{ Exception -> 0x0094 }
            r0.invoke(r11, r1)     // Catch:{ Exception -> 0x0094 }
            goto L_0x00b0
        L_0x0094:
            r0 = move-exception
            com.fossil.jg3 r1 = r27.e()
            com.fossil.mg3 r1 = r1.w()
            java.lang.String r2 = "Failed to invoke Tag Manager's initialize() method"
            r1.a(r2, r0)
            goto L_0x00b0
        L_0x00a3:
            com.fossil.jg3 r0 = r27.e()
            com.fossil.mg3 r0 = r0.z()
            java.lang.String r1 = "Tag Manager is not found and thus will not be used"
            r0.a(r1)
        L_0x00b0:
            com.fossil.ym3 r0 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.i0
            boolean r0 = r0.a(r1)
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = "_cmp"
            boolean r0 = r0.equals(r15)
            if (r0 == 0) goto L_0x00e1
            java.lang.String r0 = "gclid"
            boolean r1 = r12.containsKey(r0)
            if (r1 == 0) goto L_0x00e1
            java.lang.String r4 = r12.getString(r0)
            com.fossil.n92 r0 = r27.zzm()
            long r5 = r0.b()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_lgclid"
            r1 = r27
            r1.a(r2, r3, r4, r5)
        L_0x00e1:
            boolean r0 = com.fossil.g23.a()
            if (r0 == 0) goto L_0x010f
            com.fossil.ym3 r0 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.N0
            boolean r0 = r0.a(r1)
            if (r0 == 0) goto L_0x010f
            r27.b()
            if (r33 == 0) goto L_0x010f
            boolean r0 = com.fossil.jm3.j(r29)
            if (r0 == 0) goto L_0x010f
            com.fossil.jm3 r0 = r27.j()
            com.fossil.wg3 r1 = r27.k()
            com.fossil.xg3 r1 = r1.C
            android.os.Bundle r1 = r1.a()
            r0.a(r12, r1)
        L_0x010f:
            r0 = 40
            if (r35 == 0) goto L_0x0173
            r27.b()
            java.lang.String r1 = "_iap"
            boolean r1 = r1.equals(r15)
            if (r1 != 0) goto L_0x0173
            com.fossil.oh3 r1 = r7.a
            com.fossil.jm3 r1 = r1.v()
            java.lang.String r2 = "event"
            boolean r3 = r1.a(r2, r15)
            r4 = 2
            if (r3 != 0) goto L_0x012e
            goto L_0x0141
        L_0x012e:
            java.lang.String[] r3 = com.fossil.ni3.a
            boolean r3 = r1.a(r2, r3, r15)
            if (r3 != 0) goto L_0x0139
            r4 = 13
            goto L_0x0141
        L_0x0139:
            boolean r1 = r1.a(r2, r0, r15)
            if (r1 != 0) goto L_0x0140
            goto L_0x0141
        L_0x0140:
            r4 = 0
        L_0x0141:
            if (r4 == 0) goto L_0x0173
            com.fossil.jg3 r1 = r27.e()
            com.fossil.mg3 r1 = r1.v()
            com.fossil.hg3 r2 = r27.i()
            java.lang.String r2 = r2.a(r15)
            java.lang.String r3 = "Invalid public event name. Event will not be logged (FE)"
            r1.a(r3, r2)
            com.fossil.oh3 r1 = r7.a
            r1.v()
            java.lang.String r0 = com.fossil.jm3.a(r15, r0, r9)
            if (r15 == 0) goto L_0x0167
            int r10 = r29.length()
        L_0x0167:
            com.fossil.oh3 r1 = r7.a
            com.fossil.jm3 r1 = r1.v()
            java.lang.String r2 = "_ev"
            r1.a(r4, r2, r0, r10)
            return
        L_0x0173:
            r27.b()
            com.fossil.zj3 r1 = r27.r()
            com.fossil.wj3 r1 = r1.a(r10)
            java.lang.String r2 = "_sc"
            if (r1 == 0) goto L_0x018a
            boolean r3 = r12.containsKey(r2)
            if (r3 != 0) goto L_0x018a
            r1.d = r9
        L_0x018a:
            if (r33 == 0) goto L_0x0190
            if (r35 == 0) goto L_0x0190
            r3 = 1
            goto L_0x0191
        L_0x0190:
            r3 = 0
        L_0x0191:
            com.fossil.zj3.a(r1, r12, r3)
            java.lang.String r3 = "am"
            boolean r16 = r3.equals(r8)
            boolean r3 = com.fossil.jm3.i(r29)
            if (r33 == 0) goto L_0x01d3
            com.fossil.oi3 r4 = r7.d
            if (r4 == 0) goto L_0x01d3
            if (r3 != 0) goto L_0x01d3
            if (r16 != 0) goto L_0x01d3
            com.fossil.jg3 r0 = r27.e()
            com.fossil.mg3 r0 = r0.A()
            com.fossil.hg3 r1 = r27.i()
            java.lang.String r1 = r1.a(r15)
            com.fossil.hg3 r2 = r27.i()
            java.lang.String r2 = r2.a(r12)
            java.lang.String r3 = "Passing event to registered event handler (FE)"
            r0.a(r3, r1, r2)
            com.fossil.oi3 r1 = r7.d
            r2 = r28
            r3 = r29
            r4 = r32
            r5 = r30
            r1.a(r2, r3, r4, r5)
            return
        L_0x01d3:
            com.fossil.oh3 r3 = r7.a
            boolean r3 = r3.l()
            if (r3 != 0) goto L_0x01dc
            return
        L_0x01dc:
            com.fossil.jm3 r3 = r27.j()
            int r3 = r3.a(r15)
            if (r3 == 0) goto L_0x0220
            com.fossil.jg3 r1 = r27.e()
            com.fossil.mg3 r1 = r1.v()
            com.fossil.hg3 r2 = r27.i()
            java.lang.String r2 = r2.a(r15)
            java.lang.String r4 = "Invalid event name. Event will not be logged (FE)"
            r1.a(r4, r2)
            r27.j()
            java.lang.String r0 = com.fossil.jm3.a(r15, r0, r9)
            if (r15 == 0) goto L_0x0208
            int r10 = r29.length()
        L_0x0208:
            com.fossil.oh3 r1 = r7.a
            com.fossil.jm3 r1 = r1.v()
            java.lang.String r2 = "_ev"
            r28 = r1
            r29 = r36
            r30 = r3
            r31 = r2
            r32 = r0
            r33 = r10
            r28.a(r29, r30, r31, r32, r33)
            return
        L_0x0220:
            java.lang.String r0 = "_sn"
            java.lang.String r5 = "_o"
            java.lang.String r3 = "_si"
            java.lang.String[] r4 = new java.lang.String[]{r5, r0, r2, r3}
            java.util.List r17 = com.fossil.o92.a(r4)
            com.fossil.jm3 r4 = r27.j()
            r6 = 1
            r9 = r4
            r4 = 0
            r10 = r36
            r19 = r11
            r11 = r29
            r12 = r32
            r13 = r17
            r14 = r35
            r15 = r6
            android.os.Bundle r15 = r9.a(r10, r11, r12, r13, r14, r15)
            if (r15 == 0) goto L_0x026f
            boolean r6 = r15.containsKey(r2)
            if (r6 == 0) goto L_0x026f
            boolean r6 = r15.containsKey(r3)
            if (r6 != 0) goto L_0x0255
            goto L_0x026f
        L_0x0255:
            java.lang.String r0 = r15.getString(r0)
            java.lang.String r2 = r15.getString(r2)
            long r9 = r15.getLong(r3)
            java.lang.Long r3 = java.lang.Long.valueOf(r9)
            com.fossil.wj3 r11 = new com.fossil.wj3
            long r9 = r3.longValue()
            r11.<init>(r0, r2, r9)
            goto L_0x0271
        L_0x026f:
            r11 = r19
        L_0x0271:
            if (r11 != 0) goto L_0x0275
            r0 = r1
            goto L_0x0276
        L_0x0275:
            r0 = r11
        L_0x0276:
            com.fossil.ym3 r1 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.U
            boolean r1 = r1.a(r2)
            java.lang.String r14 = "_ae"
            r9 = 0
            if (r1 == 0) goto L_0x02b1
            r27.b()
            com.fossil.zj3 r1 = r27.r()
            com.fossil.wj3 r1 = r1.a(r4)
            if (r1 == 0) goto L_0x02b1
            r13 = r29
            boolean r1 = r14.equals(r13)
            if (r1 == 0) goto L_0x02b3
            com.fossil.il3 r1 = r27.t()
            com.fossil.pl3 r1 = r1.e
            long r1 = r1.b()
            int r3 = (r1 > r9 ? 1 : (r1 == r9 ? 0 : -1))
            if (r3 <= 0) goto L_0x02b3
            com.fossil.jm3 r3 = r27.j()
            r3.a(r15, r1)
            goto L_0x02b3
        L_0x02b1:
            r13 = r29
        L_0x02b3:
            boolean r1 = com.fossil.w03.a()
            if (r1 == 0) goto L_0x0335
            com.fossil.ym3 r1 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.v0
            boolean r1 = r1.a(r2)
            if (r1 == 0) goto L_0x0335
            java.lang.String r1 = "auto"
            boolean r1 = r1.equals(r8)
            java.lang.String r2 = "_ffr"
            if (r1 != 0) goto L_0x0318
            java.lang.String r1 = "_ssr"
            boolean r1 = r1.equals(r13)
            if (r1 == 0) goto L_0x0318
            com.fossil.jm3 r1 = r27.j()
            java.lang.String r2 = r15.getString(r2)
            boolean r3 = com.fossil.x92.a(r2)
            if (r3 == 0) goto L_0x02e8
            r11 = r19
            goto L_0x02ec
        L_0x02e8:
            java.lang.String r11 = r2.trim()
        L_0x02ec:
            com.fossil.wg3 r2 = r1.k()
            com.fossil.ch3 r2 = r2.z
            java.lang.String r2 = r2.a()
            boolean r2 = com.fossil.jm3.c(r11, r2)
            if (r2 == 0) goto L_0x030b
            com.fossil.jg3 r1 = r1.e()
            com.fossil.mg3 r1 = r1.A()
            java.lang.String r2 = "Not logging duplicate session_start_with_rollout event"
            r1.a(r2)
            r1 = 0
            goto L_0x0315
        L_0x030b:
            com.fossil.wg3 r1 = r1.k()
            com.fossil.ch3 r1 = r1.z
            r1.a(r11)
            r1 = 1
        L_0x0315:
            if (r1 != 0) goto L_0x0335
            return
        L_0x0318:
            boolean r1 = r14.equals(r13)
            if (r1 == 0) goto L_0x0335
            com.fossil.jm3 r1 = r27.j()
            com.fossil.wg3 r1 = r1.k()
            com.fossil.ch3 r1 = r1.z
            java.lang.String r1 = r1.a()
            boolean r3 = android.text.TextUtils.isEmpty(r1)
            if (r3 != 0) goto L_0x0335
            r15.putString(r2, r1)
        L_0x0335:
            java.util.ArrayList r12 = new java.util.ArrayList
            r12.<init>()
            r12.add(r15)
            com.fossil.jm3 r1 = r27.j()
            java.security.SecureRandom r1 = r1.t()
            long r2 = r1.nextLong()
            com.fossil.wg3 r1 = r27.k()
            com.fossil.ah3 r1 = r1.u
            long r19 = r1.a()
            int r1 = (r19 > r9 ? 1 : (r19 == r9 ? 0 : -1))
            if (r1 <= 0) goto L_0x03bc
            com.fossil.wg3 r1 = r27.k()
            r9 = r30
            boolean r1 = r1.a(r9)
            if (r1 == 0) goto L_0x03be
            com.fossil.wg3 r1 = r27.k()
            com.fossil.yg3 r1 = r1.w
            boolean r1 = r1.a()
            if (r1 == 0) goto L_0x03be
            com.fossil.jg3 r1 = r27.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.String r6 = "Current session is expired, remove the session number, ID, and engagement time"
            r1.a(r6)
            r6 = 0
            com.fossil.n92 r1 = r27.zzm()
            long r19 = r1.b()
            java.lang.String r11 = "auto"
            java.lang.String r21 = "_sid"
            r1 = r27
            r22 = r2
            r2 = r11
            r3 = r21
            r11 = 0
            r4 = r6
            r8 = r5
            r5 = r19
            r1.a(r2, r3, r4, r5)
            r4 = 0
            com.fossil.n92 r1 = r27.zzm()
            long r5 = r1.b()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_sno"
            r1 = r27
            r1.a(r2, r3, r4, r5)
            com.fossil.n92 r1 = r27.zzm()
            long r5 = r1.b()
            java.lang.String r2 = "auto"
            java.lang.String r3 = "_se"
            r1 = r27
            r1.a(r2, r3, r4, r5)
            goto L_0x03c2
        L_0x03bc:
            r9 = r30
        L_0x03be:
            r22 = r2
            r8 = r5
            r11 = 0
        L_0x03c2:
            java.lang.String r1 = "extend_session"
            r2 = 0
            long r1 = r15.getLong(r1, r2)
            r3 = 1
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x03ea
            com.fossil.jg3 r1 = r27.e()
            com.fossil.mg3 r1 = r1.B()
            java.lang.String r2 = "EXTEND_SESSION param attached: initiate a new session or extend the current active session"
            r1.a(r2)
            com.fossil.oh3 r1 = r7.a
            com.fossil.il3 r1 = r1.r()
            com.fossil.rl3 r1 = r1.d
            r5 = 1
            r1.a(r9, r5)
            goto L_0x03eb
        L_0x03ea:
            r5 = 1
        L_0x03eb:
            java.util.Set r1 = r15.keySet()
            int r2 = r15.size()
            java.lang.String[] r2 = new java.lang.String[r2]
            java.lang.Object[] r1 = r1.toArray(r2)
            java.lang.String[] r1 = (java.lang.String[]) r1
            java.util.Arrays.sort(r1)
            boolean r2 = com.fossil.p03.a()
            if (r2 == 0) goto L_0x043d
            com.fossil.ym3 r2 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.H0
            boolean r2 = r2.a(r3)
            if (r2 == 0) goto L_0x043d
            com.fossil.ym3 r2 = r27.l()
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.G0
            boolean r2 = r2.a(r3)
            if (r2 == 0) goto L_0x043d
            int r0 = r1.length
            r2 = 0
        L_0x041e:
            if (r2 >= r0) goto L_0x0435
            r3 = r1[r2]
            r27.j()
            java.lang.Object r4 = r15.get(r3)
            android.os.Bundle[] r4 = com.fossil.jm3.b(r4)
            if (r4 == 0) goto L_0x0432
            r15.putParcelableArray(r3, r4)
        L_0x0432:
            int r2 = r2 + 1
            goto L_0x041e
        L_0x0435:
            r33 = r8
            r2 = r13
            r0 = r14
            r20 = 1
            goto L_0x04ff
        L_0x043d:
            int r2 = r1.length
            r3 = 0
            r4 = 0
        L_0x0440:
            java.lang.String r6 = "_eid"
            if (r3 >= r2) goto L_0x04ec
            r5 = r1[r3]
            java.lang.Object r19 = r15.get(r5)
            r27.j()
            r32 = r1
            android.os.Bundle[] r1 = com.fossil.jm3.b(r19)
            if (r1 == 0) goto L_0x04ca
            int r11 = r1.length
            r15.putInt(r5, r11)
            r19 = r2
            r11 = 0
        L_0x045c:
            int r2 = r1.length
            if (r11 >= r2) goto L_0x04bc
            r2 = r1[r11]
            r20 = r15
            r15 = 1
            com.fossil.zj3.a(r0, r2, r15)
            com.fossil.jm3 r18 = r27.j()
            r21 = 0
            java.lang.String r24 = "_ep"
            r9 = r18
            r10 = r36
            r25 = r11
            r11 = r24
            r26 = r12
            r12 = r2
            r2 = r13
            r13 = r17
            r18 = r0
            r0 = r14
            r14 = r35
            r33 = r8
            r8 = r20
            r20 = 1
            r15 = r21
            android.os.Bundle r9 = r9.a(r10, r11, r12, r13, r14, r15)
            java.lang.String r10 = "_en"
            r9.putString(r10, r2)
            r10 = r22
            r9.putLong(r6, r10)
            java.lang.String r12 = "_gn"
            r9.putString(r12, r5)
            int r12 = r1.length
            java.lang.String r13 = "_ll"
            r9.putInt(r13, r12)
            java.lang.String r12 = "_i"
            r13 = r25
            r9.putInt(r12, r13)
            r12 = r26
            r12.add(r9)
            int r9 = r13 + 1
            r14 = r0
            r13 = r2
            r15 = r8
            r0 = r18
            r8 = r33
            r11 = r9
            r9 = r30
            goto L_0x045c
        L_0x04bc:
            r18 = r0
            r33 = r8
            r2 = r13
            r0 = r14
            r8 = r15
            r10 = r22
            r20 = 1
            int r1 = r1.length
            int r4 = r4 + r1
            goto L_0x04d7
        L_0x04ca:
            r18 = r0
            r19 = r2
            r33 = r8
            r2 = r13
            r0 = r14
            r8 = r15
            r10 = r22
            r20 = 1
        L_0x04d7:
            int r3 = r3 + 1
            r1 = r32
            r14 = r0
            r13 = r2
            r15 = r8
            r22 = r10
            r0 = r18
            r2 = r19
            r5 = 1
            r11 = 0
            r9 = r30
            r8 = r33
            goto L_0x0440
        L_0x04ec:
            r33 = r8
            r2 = r13
            r0 = r14
            r8 = r15
            r10 = r22
            r20 = 1
            if (r4 == 0) goto L_0x04ff
            r8.putLong(r6, r10)
            java.lang.String r1 = "_epc"
            r8.putInt(r1, r4)
        L_0x04ff:
            r10 = 0
        L_0x0500:
            int r1 = r12.size()
            if (r10 >= r1) goto L_0x0576
            java.lang.Object r1 = r12.get(r10)
            android.os.Bundle r1 = (android.os.Bundle) r1
            if (r10 == 0) goto L_0x0510
            r3 = 1
            goto L_0x0511
        L_0x0510:
            r3 = 0
        L_0x0511:
            if (r3 == 0) goto L_0x051a
            java.lang.String r3 = "_ep"
            r8 = r28
            r9 = r33
            goto L_0x051f
        L_0x051a:
            r8 = r28
            r9 = r33
            r3 = r2
        L_0x051f:
            r1.putString(r9, r8)
            if (r34 == 0) goto L_0x052c
            com.fossil.jm3 r4 = r27.j()
            android.os.Bundle r1 = r4.a(r1)
        L_0x052c:
            r11 = r1
            com.fossil.ub3 r13 = new com.fossil.ub3
            com.fossil.tb3 r4 = new com.fossil.tb3
            r4.<init>(r11)
            r1 = r13
            r14 = r2
            r2 = r3
            r3 = r4
            r4 = r28
            r15 = 1
            r5 = r30
            r1.<init>(r2, r3, r4, r5)
            com.fossil.ek3 r1 = r27.q()
            r5 = r36
            r1.a(r13, r5)
            if (r16 != 0) goto L_0x056e
            java.util.Set<com.fossil.ri3> r1 = r7.e
            java.util.Iterator r13 = r1.iterator()
        L_0x0551:
            boolean r1 = r13.hasNext()
            if (r1 == 0) goto L_0x056e
            java.lang.Object r1 = r13.next()
            com.fossil.ri3 r1 = (com.fossil.ri3) r1
            android.os.Bundle r4 = new android.os.Bundle
            r4.<init>(r11)
            r2 = r28
            r3 = r29
            r5 = r30
            r1.a(r2, r3, r4, r5)
            r5 = r36
            goto L_0x0551
        L_0x056e:
            int r10 = r10 + 1
            r33 = r9
            r2 = r14
            r20 = 1
            goto L_0x0500
        L_0x0576:
            r14 = r2
            r15 = 1
            r27.b()
            com.fossil.zj3 r1 = r27.r()
            r2 = 0
            com.fossil.wj3 r1 = r1.a(r2)
            if (r1 == 0) goto L_0x059b
            boolean r0 = r0.equals(r14)
            if (r0 == 0) goto L_0x059b
            com.fossil.il3 r0 = r27.t()
            com.fossil.n92 r1 = r27.zzm()
            long r1 = r1.c()
            r0.a(r15, r15, r1)
        L_0x059b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ti3.a(java.lang.String, java.lang.String, long, android.os.Bundle, boolean, boolean, boolean, java.lang.String):void");
    }

    @DexIgnore
    public final void b(String str, String str2, long j, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        c().a(new wi3(this, str, str2, j, jm3.b(bundle), z, z2, z3, str3));
    }

    @DexIgnore
    public final void b(ri3 ri3) {
        a();
        w();
        a72.a(ri3);
        if (!this.e.remove(ri3)) {
            e().w().a("OnEventListener had not been registered");
        }
    }

    @DexIgnore
    public final void b(Bundle bundle) {
        a72.a(bundle);
        a72.b(bundle.getString("app_id"));
        m();
        throw null;
    }

    @DexIgnore
    public final void b(Bundle bundle, long j) {
        a72.a(bundle);
        ji3.a(bundle, "app_id", String.class, null);
        ji3.a(bundle, "origin", String.class, null);
        ji3.a(bundle, "name", String.class, null);
        ji3.a(bundle, "value", Object.class, null);
        ji3.a(bundle, "trigger_event_name", String.class, null);
        ji3.a(bundle, "trigger_timeout", Long.class, 0L);
        ji3.a(bundle, "timed_out_event_name", String.class, null);
        ji3.a(bundle, "timed_out_event_params", Bundle.class, null);
        ji3.a(bundle, "triggered_event_name", String.class, null);
        ji3.a(bundle, "triggered_event_params", Bundle.class, null);
        ji3.a(bundle, "time_to_live", Long.class, 0L);
        ji3.a(bundle, "expired_event_name", String.class, null);
        ji3.a(bundle, "expired_event_params", Bundle.class, null);
        a72.b(bundle.getString("name"));
        a72.b(bundle.getString("origin"));
        a72.a(bundle.get("value"));
        bundle.putLong("creation_timestamp", j);
        String string = bundle.getString("name");
        Object obj = bundle.get("value");
        if (j().b(string) != 0) {
            e().t().a("Invalid conditional user property name", i().c(string));
        } else if (j().b(string, obj) != 0) {
            e().t().a("Invalid conditional user property value", i().c(string), obj);
        } else {
            Object c2 = j().c(string, obj);
            if (c2 == null) {
                e().t().a("Unable to normalize conditional user property value", i().c(string), obj);
                return;
            }
            ji3.a(bundle, c2);
            long j2 = bundle.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle.getString("trigger_event_name")) || (j2 <= 15552000000L && j2 >= 1)) {
                long j3 = bundle.getLong("time_to_live");
                if (j3 > 15552000000L || j3 < 1) {
                    e().t().a("Invalid conditional user property time to live", i().c(string), Long.valueOf(j3));
                } else {
                    c().a(new cj3(this, bundle));
                }
            } else {
                e().t().a("Invalid conditional user property timeout", i().c(string), Long.valueOf(j2));
            }
        }
    }

    @DexIgnore
    public final void b(String str, String str2, String str3, Bundle bundle) {
        long b = zzm().b();
        a72.b(str2);
        Bundle bundle2 = new Bundle();
        if (str != null) {
            bundle2.putString("app_id", str);
        }
        bundle2.putString("name", str2);
        bundle2.putLong("creation_timestamp", b);
        if (str3 != null) {
            bundle2.putString("expired_event_name", str3);
            bundle2.putBundle("expired_event_params", bundle);
        }
        c().a(new bj3(this, bundle2));
    }

    @DexIgnore
    public final ArrayList<Bundle> b(String str, String str2, String str3) {
        if (c().s()) {
            e().t().a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (xm3.a()) {
            e().t().a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            ((ii3) this).a.c().a(atomicReference, 5000, "get conditional user properties", new dj3(this, atomicReference, str, str2, str3));
            List list = (List) atomicReference.get();
            if (list != null) {
                return jm3.b((List<wm3>) list);
            }
            e().t().a("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    @DexIgnore
    public final Map<String, Object> b(String str, String str2, String str3, boolean z) {
        if (c().s()) {
            e().t().a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (xm3.a()) {
            e().t().a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            ((ii3) this).a.c().a(atomicReference, 5000, "get user properties", new gj3(this, atomicReference, str, str2, str3, z));
            List<em3> list = (List) atomicReference.get();
            if (list == null) {
                e().t().a("Timed out waiting for handle get user properties, includeInternal", Boolean.valueOf(z));
                return Collections.emptyMap();
            }
            n4 n4Var = new n4(list.size());
            for (em3 em3 : list) {
                n4Var.put(em3.b, em3.zza());
            }
            return n4Var;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        a();
        String str3 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        if (l().a(wb3.D0)) {
            if (jm3.c(str2, "screen_view")) {
                r().a(bundle2, j);
                return;
            }
        }
        b(str3, str2, j, bundle2, z2, !z2 || this.d == null || jm3.i(str2), !z, null);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z) {
        a(str, str2, obj, true, zzm().b());
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z, long j) {
        if (str == null) {
            str = "app";
        }
        int i2 = 6;
        int i3 = 0;
        if (z) {
            i2 = j().b(str2);
        } else {
            jm3 j2 = j();
            if (j2.a("user property", str2)) {
                if (!j2.a("user property", pi3.a, str2)) {
                    i2 = 15;
                } else if (j2.a("user property", 24, str2)) {
                    i2 = 0;
                }
            }
        }
        if (i2 != 0) {
            j();
            String a = jm3.a(str2, 24, true);
            if (str2 != null) {
                i3 = str2.length();
            }
            ((ii3) this).a.v().a(i2, "_ev", a, i3);
        } else if (obj != null) {
            int b = j().b(str2, obj);
            if (b != 0) {
                j();
                String a2 = jm3.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i3 = String.valueOf(obj).length();
                }
                ((ii3) this).a.v().a(b, "_ev", a2, i3);
                return;
            }
            Object c2 = j().c(str2, obj);
            if (c2 != null) {
                a(str, str2, j, c2);
            }
        } else {
            a(str, str2, j, (Object) null);
        }
    }

    @DexIgnore
    public final void a(String str, String str2, long j, Object obj) {
        c().a(new yi3(this, str, str2, obj, j));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            com.fossil.a72.b(r9)
            com.fossil.a72.b(r10)
            r8.g()
            r8.a()
            r8.w()
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L_0x0063
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x0053
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0053
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.String r10 = r0.toLowerCase(r10)
            java.lang.String r11 = "false"
            boolean r10 = r11.equals(r10)
            r2 = 1
            if (r10 == 0) goto L_0x0038
            r4 = r2
            goto L_0x003a
        L_0x0038:
            r4 = 0
        L_0x003a:
            java.lang.Long r10 = java.lang.Long.valueOf(r4)
            com.fossil.wg3 r0 = r8.k()
            com.fossil.ch3 r0 = r0.s
            long r4 = r10.longValue()
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x004e
            java.lang.String r11 = "true"
        L_0x004e:
            r0.a(r11)
            r6 = r10
            goto L_0x0061
        L_0x0053:
            if (r11 != 0) goto L_0x0063
            com.fossil.wg3 r10 = r8.k()
            com.fossil.ch3 r10 = r10.s
            java.lang.String r0 = "unset"
            r10.a(r0)
            r6 = r11
        L_0x0061:
            r3 = r1
            goto L_0x0065
        L_0x0063:
            r3 = r10
            r6 = r11
        L_0x0065:
            com.fossil.oh3 r10 = r8.a
            boolean r10 = r10.g()
            if (r10 != 0) goto L_0x007b
            com.fossil.jg3 r9 = r8.e()
            com.fossil.mg3 r9 = r9.B()
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.a(r10)
            return
        L_0x007b:
            com.fossil.oh3 r10 = r8.a
            boolean r10 = r10.l()
            if (r10 != 0) goto L_0x0084
            return
        L_0x0084:
            com.fossil.em3 r10 = new com.fossil.em3
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            com.fossil.ek3 r9 = r8.q()
            r9.a(r10)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ti3.a(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    @DexIgnore
    public final void a(String str) {
        this.g.set(str);
    }

    @DexIgnore
    public final void a(oi3 oi3) {
        oi3 oi32;
        g();
        a();
        w();
        if (!(oi3 == null || oi3 == (oi32 = this.d))) {
            a72.b(oi32 == null, "EventInterceptor already set.");
        }
        this.d = oi3;
    }

    @DexIgnore
    public final void a(ri3 ri3) {
        a();
        w();
        a72.a(ri3);
        if (!this.e.add(ri3)) {
            e().w().a("OnEventListener already registered");
        }
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        a(bundle, zzm().b());
    }

    @DexIgnore
    public final void a(Bundle bundle, long j) {
        a72.a(bundle);
        a();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            e().w().a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        b(bundle2, j);
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, Bundle bundle) {
        a72.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final ArrayList<Bundle> a(String str, String str2) {
        a();
        return b((String) null, str, str2);
    }

    @DexIgnore
    public final ArrayList<Bundle> a(String str, String str2, String str3) {
        a72.b(str);
        m();
        throw null;
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        a();
        return b((String) null, str, str2, z);
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        a72.b(str);
        m();
        throw null;
    }
}
