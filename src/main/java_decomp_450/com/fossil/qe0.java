package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe0 extends k60 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ pe0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qe0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public qe0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                int readInt = parcel.readInt();
                Parcelable readParcelable = parcel.readParcelable(pe0.class.getClassLoader());
                if (readParcelable != null) {
                    ee7.a((Object) readParcelable, "parcel.readParcelable<Fi\u2026class.java.classLoader)!!");
                    return new qe0(readString, readInt, (pe0) readParcelable);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qe0[] newArray(int i) {
            return new qe0[i];
        }
    }

    @DexIgnore
    public qe0(String str, int i, pe0 pe0) {
        this.a = str;
        this.b = i;
        this.c = pe0;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.G, this.a), r51.G4, Integer.valueOf(this.b)), this.c.a());
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qe0)) {
            return false;
        }
        qe0 qe0 = (qe0) obj;
        return ee7.a(this.a, qe0.a) && this.b == qe0.b && ee7.a(this.c, qe0.c);
    }

    @DexIgnore
    public final pe0 getFitnessData() {
        return this.c;
    }

    @DexIgnore
    public final String getName() {
        return this.a;
    }

    @DexIgnore
    public final int getRank() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.b) * 31;
        pe0 pe0 = this.c;
        if (pe0 != null) {
            i = pe0.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("Player(name=");
        b2.append(this.a);
        b2.append(", rank=");
        b2.append(this.b);
        b2.append(", fitnessData=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeInt(this.b);
        parcel.writeParcelable(this.c, i);
    }
}
