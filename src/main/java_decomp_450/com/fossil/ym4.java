package com.fossil;

import android.animation.TimeInterpolator;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.view.animation.AccelerateDecelerateInterpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ym4 extends Handler {
    @DexIgnore
    public /* final */ WeakReference<en4> a;
    @DexIgnore
    public long b;
    @DexIgnore
    public TimeInterpolator c; // = new AccelerateDecelerateInterpolator();
    @DexIgnore
    public long d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ym4(Context context, en4 en4) {
        super(context.getMainLooper());
        ee7.b(context, "context");
        ee7.b(en4, "iContinuousAnimator");
        this.a = new WeakReference<>(en4);
    }

    @DexIgnore
    public final boolean a(en4 en4) {
        float min = Math.min(((float) (System.currentTimeMillis() - this.b)) / en4.getAnimationDuration(), 1.0f);
        en4.setCurrentValue(en4.getFrom() + ((en4.getTo() - en4.getFrom()) * this.c.getInterpolation(min)));
        return min >= ((float) 1);
    }

    @DexIgnore
    public final void b(Message message, en4 en4) {
        Object obj = message.obj;
        if (obj != null) {
            en4.setFrom(((float[]) obj)[0]);
            Object obj2 = message.obj;
            if (obj2 != null) {
                en4.setTo(((float[]) obj2)[1]);
                this.b = System.currentTimeMillis();
                en4.setAnimationState(an4.ANIMATING);
                sendEmptyMessageDelayed(zm4.TICK.ordinal(), ((long) en4.getFrameDelay()) - (SystemClock.uptimeMillis() - this.d));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.FloatArray");
        }
        throw new x87("null cannot be cast to non-null type kotlin.FloatArray");
    }

    @DexIgnore
    public void handleMessage(Message message) {
        ee7.b(message, "msg");
        en4 en4 = this.a.get();
        if (en4 != null) {
            zm4 zm4 = zm4.values()[message.what];
            zm4 zm42 = zm4.TICK;
            if (zm4 == zm42) {
                removeMessages(zm42.ordinal());
            }
            this.d = SystemClock.uptimeMillis();
            int i = xm4.c[en4.getAnimationState().ordinal()];
            if (i == 1) {
                int i2 = xm4.a[zm4.ordinal()];
                if (i2 == 1) {
                    a(message, en4);
                } else if (i2 == 2) {
                    b(message, en4);
                } else if (i2 == 3) {
                    removeMessages(zm4.TICK.ordinal());
                }
            } else if (i == 2) {
                int i3 = xm4.b[zm4.ordinal()];
                if (i3 == 1) {
                    a(message, en4);
                } else if (i3 == 2) {
                    this.b = System.currentTimeMillis();
                    en4.setFrom(en4.getCurrentValue());
                    Object obj = message.obj;
                    if (obj != null) {
                        en4.setTo(((float[]) obj)[1]);
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type kotlin.FloatArray");
                } else if (i3 == 3) {
                    if (a(en4)) {
                        en4.setAnimationState(an4.IDLE);
                        en4.setCurrentValue(en4.getTo());
                    }
                    sendEmptyMessageDelayed(zm4.TICK.ordinal(), ((long) en4.getFrameDelay()) - (SystemClock.uptimeMillis() - this.d));
                    en4.a();
                }
            }
        }
    }

    @DexIgnore
    public final void a(Message message, en4 en4) {
        en4.setFrom(en4.getTo());
        Object obj = message.obj;
        if (obj != null) {
            en4.setTo(((float[]) obj)[0]);
            en4.setCurrentValue(en4.getTo());
            en4.setAnimationState(an4.IDLE);
            en4.a();
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.FloatArray");
    }
}
