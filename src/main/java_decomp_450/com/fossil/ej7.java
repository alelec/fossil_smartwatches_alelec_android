package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ej7 {
    @DexIgnore
    public static final String a(fb7<?> fb7) {
        Object obj;
        if (fb7 instanceof lj7) {
            return fb7.toString();
        }
        try {
            s87.a aVar = s87.Companion;
            obj = s87.m60constructorimpl(fb7 + '@' + b(fb7));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            obj = s87.m60constructorimpl(t87.a(th));
        }
        if (s87.m63exceptionOrNullimpl(obj) != null) {
            obj = fb7.getClass().getName() + '@' + b(fb7);
        }
        return (String) obj;
    }

    @DexIgnore
    public static final String b(Object obj) {
        return Integer.toHexString(System.identityHashCode(obj));
    }

    @DexIgnore
    public static final String a(Object obj) {
        return obj.getClass().getSimpleName();
    }
}
