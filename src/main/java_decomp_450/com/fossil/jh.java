package com.fossil;

import androidx.renderscript.RenderScript;

public class jh extends ih {
    public int d;
    public c e;
    public b f;
    public boolean g;
    public int h;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|(2:1|2)|3|(2:5|6)|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(32:0|(2:1|2)|3|5|6|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Can't wrap try/catch for region: R(33:0|1|2|3|5|6|7|9|10|11|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|36) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0043 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x004d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0058 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0063 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0079 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0085 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0091 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x009d */
        /*
        static {
            /*
                com.fossil.jh$b[] r0 = com.fossil.jh.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.jh.a.b = r0
                r1 = 1
                com.fossil.jh$b r2 = com.fossil.jh.b.PIXEL_LA     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.jh.a.b     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.jh$b r3 = com.fossil.jh.b.PIXEL_RGB     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                r2 = 3
                int[] r3 = com.fossil.jh.a.b     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.jh$b r4 = com.fossil.jh.b.PIXEL_RGBA     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                com.fossil.jh$c[] r3 = com.fossil.jh.c.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.fossil.jh.a.a = r3
                com.fossil.jh$c r4 = com.fossil.jh.c.FLOAT_32     // Catch:{ NoSuchFieldError -> 0x0039 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0039 }
                r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0043 }
                com.fossil.jh$c r3 = com.fossil.jh.c.FLOAT_64     // Catch:{ NoSuchFieldError -> 0x0043 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0043 }
                r1[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0043 }
            L_0x0043:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x004d }
                com.fossil.jh$c r1 = com.fossil.jh.c.SIGNED_8     // Catch:{ NoSuchFieldError -> 0x004d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004d }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004d }
            L_0x004d:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0058 }
                com.fossil.jh$c r1 = com.fossil.jh.c.SIGNED_16     // Catch:{ NoSuchFieldError -> 0x0058 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0058 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0058 }
            L_0x0058:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0063 }
                com.fossil.jh$c r1 = com.fossil.jh.c.SIGNED_32     // Catch:{ NoSuchFieldError -> 0x0063 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0063 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0063 }
            L_0x0063:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x006e }
                com.fossil.jh$c r1 = com.fossil.jh.c.SIGNED_64     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0079 }
                com.fossil.jh$c r1 = com.fossil.jh.c.UNSIGNED_8     // Catch:{ NoSuchFieldError -> 0x0079 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0079 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0079 }
            L_0x0079:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0085 }
                com.fossil.jh$c r1 = com.fossil.jh.c.UNSIGNED_16     // Catch:{ NoSuchFieldError -> 0x0085 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0085 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0085 }
            L_0x0085:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x0091 }
                com.fossil.jh$c r1 = com.fossil.jh.c.UNSIGNED_32     // Catch:{ NoSuchFieldError -> 0x0091 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0091 }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0091 }
            L_0x0091:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x009d }
                com.fossil.jh$c r1 = com.fossil.jh.c.UNSIGNED_64     // Catch:{ NoSuchFieldError -> 0x009d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009d }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009d }
            L_0x009d:
                int[] r0 = com.fossil.jh.a.a     // Catch:{ NoSuchFieldError -> 0x00a9 }
                com.fossil.jh$c r1 = com.fossil.jh.c.BOOLEAN     // Catch:{ NoSuchFieldError -> 0x00a9 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a9 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00a9 }
            L_0x00a9:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.jh.a.<clinit>():void");
        }
        */
    }

    public enum b {
        USER(0),
        PIXEL_L(7),
        PIXEL_A(8),
        PIXEL_LA(9),
        PIXEL_RGB(10),
        PIXEL_RGBA(11),
        PIXEL_DEPTH(12),
        PIXEL_YUV(13);
        
        public int mID;

        public b(int i) {
            this.mID = i;
        }
    }

    public jh(long j, RenderScript renderScript, c cVar, b bVar, boolean z, int i) {
        super(j, renderScript);
        if (cVar == c.UNSIGNED_5_6_5 || cVar == c.UNSIGNED_4_4_4_4 || cVar == c.UNSIGNED_5_5_5_1) {
            this.d = cVar.mSize;
        } else if (i == 3) {
            this.d = cVar.mSize * 4;
        } else {
            this.d = cVar.mSize * i;
        }
        this.e = cVar;
        this.f = bVar;
        this.g = z;
        this.h = i;
    }

    public static jh a(RenderScript renderScript, c cVar) {
        b bVar = b.USER;
        return new jh(renderScript.a((long) cVar.mID, bVar.mID, false, 1), renderScript, cVar, bVar, false, 1);
    }

    public static jh c(RenderScript renderScript) {
        if (renderScript.n == null) {
            renderScript.n = a(renderScript, c.UNSIGNED_8, b.PIXEL_A);
        }
        return renderScript.n;
    }

    public static jh d(RenderScript renderScript) {
        if (renderScript.p == null) {
            renderScript.p = a(renderScript, c.UNSIGNED_4_4_4_4, b.PIXEL_RGBA);
        }
        return renderScript.p;
    }

    public static jh f(RenderScript renderScript) {
        if (renderScript.o == null) {
            renderScript.o = a(renderScript, c.UNSIGNED_5_6_5, b.PIXEL_RGB);
        }
        return renderScript.o;
    }

    public static jh g(RenderScript renderScript) {
        if (renderScript.m == null) {
            renderScript.m = a(renderScript, c.UNSIGNED_8);
        }
        return renderScript.m;
    }

    public static jh h(RenderScript renderScript) {
        if (renderScript.r == null) {
            renderScript.r = a(renderScript, c.UNSIGNED_8, 4);
        }
        return renderScript.r;
    }

    public long b(RenderScript renderScript) {
        return renderScript.b((long) this.e.mID, this.f.mID, this.g, this.h);
    }

    public int e() {
        return this.d;
    }

    public static jh e(RenderScript renderScript) {
        if (renderScript.q == null) {
            renderScript.q = a(renderScript, c.UNSIGNED_8, b.PIXEL_RGBA);
        }
        return renderScript.q;
    }

    public enum c {
        NONE(0, 0),
        FLOAT_32(2, 4),
        FLOAT_64(3, 8),
        SIGNED_8(4, 1),
        SIGNED_16(5, 2),
        SIGNED_32(6, 4),
        SIGNED_64(7, 8),
        UNSIGNED_8(8, 1),
        UNSIGNED_16(9, 2),
        UNSIGNED_32(10, 4),
        UNSIGNED_64(11, 8),
        BOOLEAN(12, 1),
        UNSIGNED_5_6_5(13, 2),
        UNSIGNED_5_5_5_1(14, 2),
        UNSIGNED_4_4_4_4(15, 2),
        MATRIX_4X4(16, 64),
        MATRIX_3X3(17, 36),
        MATRIX_2X2(18, 16),
        RS_ELEMENT(1000),
        RS_TYPE(1001),
        RS_ALLOCATION(1002),
        RS_SAMPLER(1003),
        RS_SCRIPT(1004);
        
        public int mID;
        public int mSize;

        public c(int i, int i2) {
            this.mID = i;
            this.mSize = i2;
        }

        public c(int i) {
            this.mID = i;
            this.mSize = 4;
            if (RenderScript.G == 8) {
                this.mSize = 32;
            }
        }
    }

    public static jh a(RenderScript renderScript, c cVar, int i) {
        if (i < 2 || i > 4) {
            throw new mh("Vector size out of range 2-4.");
        }
        switch (a.a[cVar.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                b bVar = b.USER;
                return new jh(renderScript.a((long) cVar.mID, bVar.mID, false, i), renderScript, cVar, bVar, false, i);
            default:
                throw new mh("Cannot create vector of non-primitive type.");
        }
    }

    public static jh a(RenderScript renderScript, c cVar, b bVar) {
        if (bVar != b.PIXEL_L && bVar != b.PIXEL_A && bVar != b.PIXEL_LA && bVar != b.PIXEL_RGB && bVar != b.PIXEL_RGBA && bVar != b.PIXEL_DEPTH && bVar != b.PIXEL_YUV) {
            throw new mh("Unsupported DataKind");
        } else if (cVar != c.UNSIGNED_8 && cVar != c.UNSIGNED_16 && cVar != c.UNSIGNED_5_6_5 && cVar != c.UNSIGNED_4_4_4_4 && cVar != c.UNSIGNED_5_5_5_1) {
            throw new mh("Unsupported DataType");
        } else if (cVar == c.UNSIGNED_5_6_5 && bVar != b.PIXEL_RGB) {
            throw new mh("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_5_5_5_1 && bVar != b.PIXEL_RGBA) {
            throw new mh("Bad kind and type combo");
        } else if (cVar == c.UNSIGNED_4_4_4_4 && bVar != b.PIXEL_RGBA) {
            throw new mh("Bad kind and type combo");
        } else if (cVar != c.UNSIGNED_16 || bVar == b.PIXEL_DEPTH) {
            int i = a.b[bVar.ordinal()];
            int i2 = i != 1 ? i != 2 ? i != 3 ? 1 : 4 : 3 : 2;
            return new jh(renderScript.a((long) cVar.mID, bVar.mID, true, i2), renderScript, cVar, bVar, true, i2);
        } else {
            throw new mh("Bad kind and type combo");
        }
    }

    public boolean a(jh jhVar) {
        c cVar;
        if (equals(jhVar)) {
            return true;
        }
        if (this.d == jhVar.d && (cVar = this.e) != c.NONE && cVar == jhVar.e && this.h == jhVar.h) {
            return true;
        }
        return false;
    }
}
