package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk4 implements Factory<ff5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> c;

    @DexIgnore
    public yk4(wj4 wj4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static yk4 a(wj4 wj4, Provider<DeviceRepository> provider, Provider<PortfolioApp> provider2) {
        return new yk4(wj4, provider, provider2);
    }

    @DexIgnore
    public static ff5 a(wj4 wj4, DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        ff5 a2 = wj4.a(deviceRepository, portfolioApp);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ff5 get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
