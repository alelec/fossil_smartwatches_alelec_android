package com.fossil;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class it1 implements iu1 {
    @DexIgnore
    public static /* final */ String c; // = cu1.a("hts/frbslgiggolai.o/0clgbthfra=snpoo", "tp:/ieaeogn.ogepscmvc/o/ac?omtjo_rt3");
    @DexIgnore
    public static /* final */ String d; // = cu1.a("hts/frbslgigp.ogepscmv/ieo/eaybtho", "tp:/ieaeogn-agolai.o/1frlglgc/aclg");
    @DexIgnore
    public static /* final */ String e; // = cu1.a("AzSCki82AwsLzKd5O8zo", "IayckHiZRO1EFl1aGoK");
    @DexIgnore
    public static /* final */ Set<bt1> f; // = Collections.unmodifiableSet(new HashSet(Arrays.asList(bt1.a("proto"), bt1.a("json"))));
    @DexIgnore
    public static /* final */ it1 g; // = new it1(d, e);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    /*
    static {
        new it1(c, null);
    }
    */

    @DexIgnore
    public it1(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    public static it1 a(byte[] bArr) {
        String str = new String(bArr, Charset.forName("UTF-8"));
        if (str.startsWith("1$")) {
            String[] split = str.substring(2).split(Pattern.quote("\\"), 2);
            if (split.length == 2) {
                String str2 = split[0];
                if (!str2.isEmpty()) {
                    String str3 = split[1];
                    if (str3.isEmpty()) {
                        str3 = null;
                    }
                    return new it1(str2, str3);
                }
                throw new IllegalArgumentException("Missing endpoint in CCTDestination extras");
            }
            throw new IllegalArgumentException("Extra is not a valid encoded LegacyFlgDestination");
        }
        throw new IllegalArgumentException("Version marker missing from extras");
    }

    @DexIgnore
    public byte[] b() {
        if (this.b == null && this.a == null) {
            return null;
        }
        Object[] objArr = new Object[4];
        objArr[0] = "1$";
        objArr[1] = this.a;
        objArr[2] = "\\";
        String str = this.b;
        if (str == null) {
            str = "";
        }
        objArr[3] = str;
        return String.format("%s%s%s%s", objArr).getBytes(Charset.forName("UTF-8"));
    }

    @DexIgnore
    public String c() {
        return this.b;
    }

    @DexIgnore
    public String d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.hu1
    public byte[] getExtras() {
        return b();
    }

    @DexIgnore
    @Override // com.fossil.hu1
    public String getName() {
        return "cct";
    }

    @DexIgnore
    @Override // com.fossil.iu1
    public Set<bt1> a() {
        return f;
    }
}
