package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class ov4 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSettingRepository.Anon8 a;
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSetting b;
    @DexIgnore
    private /* final */ /* synthetic */ MicroAppSettingDataSource.PushMicroAppSettingToServerCallback c;

    @DexIgnore
    public /* synthetic */ ov4(MicroAppSettingRepository.Anon8 anon8, MicroAppSetting microAppSetting, MicroAppSettingDataSource.PushMicroAppSettingToServerCallback pushMicroAppSettingToServerCallback) {
        this.a = anon8;
        this.b = microAppSetting;
        this.c = pushMicroAppSettingToServerCallback;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b, this.c);
    }
}
