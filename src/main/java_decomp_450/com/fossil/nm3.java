package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nm3> CREATOR; // = new mm3();
    @DexIgnore
    public /* final */ String A;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;
    @DexIgnore
    public /* final */ long j;
    @DexIgnore
    public /* final */ String p;
    @DexIgnore
    public /* final */ long q;
    @DexIgnore
    public /* final */ long r;
    @DexIgnore
    public /* final */ int s;
    @DexIgnore
    public /* final */ boolean t;
    @DexIgnore
    public /* final */ boolean u;
    @DexIgnore
    public /* final */ boolean v;
    @DexIgnore
    public /* final */ String w;
    @DexIgnore
    public /* final */ Boolean x;
    @DexIgnore
    public /* final */ long y;
    @DexIgnore
    public /* final */ List<String> z;

    @DexIgnore
    public nm3(String str, String str2, String str3, long j2, String str4, long j3, long j4, String str5, boolean z2, boolean z3, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        a72.b(str);
        this.a = str;
        this.b = TextUtils.isEmpty(str2) ? null : str2;
        this.c = str3;
        this.j = j2;
        this.d = str4;
        this.e = j3;
        this.f = j4;
        this.g = str5;
        this.h = z2;
        this.i = z3;
        this.p = str6;
        this.q = j5;
        this.r = j6;
        this.s = i2;
        this.t = z4;
        this.u = z5;
        this.v = z6;
        this.w = str7;
        this.x = bool;
        this.y = j7;
        this.z = list;
        this.A = str8;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a, false);
        k72.a(parcel, 3, this.b, false);
        k72.a(parcel, 4, this.c, false);
        k72.a(parcel, 5, this.d, false);
        k72.a(parcel, 6, this.e);
        k72.a(parcel, 7, this.f);
        k72.a(parcel, 8, this.g, false);
        k72.a(parcel, 9, this.h);
        k72.a(parcel, 10, this.i);
        k72.a(parcel, 11, this.j);
        k72.a(parcel, 12, this.p, false);
        k72.a(parcel, 13, this.q);
        k72.a(parcel, 14, this.r);
        k72.a(parcel, 15, this.s);
        k72.a(parcel, 16, this.t);
        k72.a(parcel, 17, this.u);
        k72.a(parcel, 18, this.v);
        k72.a(parcel, 19, this.w, false);
        k72.a(parcel, 21, this.x, false);
        k72.a(parcel, 22, this.y);
        k72.b(parcel, 23, this.z, false);
        k72.a(parcel, 24, this.A, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public nm3(String str, String str2, String str3, String str4, long j2, long j3, String str5, boolean z2, boolean z3, long j4, String str6, long j5, long j6, int i2, boolean z4, boolean z5, boolean z6, String str7, Boolean bool, long j7, List<String> list, String str8) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.j = j4;
        this.d = str4;
        this.e = j2;
        this.f = j3;
        this.g = str5;
        this.h = z2;
        this.i = z3;
        this.p = str6;
        this.q = j5;
        this.r = j6;
        this.s = i2;
        this.t = z4;
        this.u = z5;
        this.v = z6;
        this.w = str7;
        this.x = bool;
        this.y = j7;
        this.z = list;
        this.A = str8;
    }
}
