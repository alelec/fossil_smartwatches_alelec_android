package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i92 implements Parcelable.Creator<WakeLockEvent> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        String str = null;
        ArrayList<String> arrayList = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    j = j72.s(parcel, a);
                    break;
                case 3:
                case 7:
                case 9:
                default:
                    j72.v(parcel, a);
                    break;
                case 4:
                    str = j72.e(parcel, a);
                    break;
                case 5:
                    i3 = j72.q(parcel, a);
                    break;
                case 6:
                    arrayList = j72.g(parcel, a);
                    break;
                case 8:
                    j2 = j72.s(parcel, a);
                    break;
                case 10:
                    str3 = j72.e(parcel, a);
                    break;
                case 11:
                    i2 = j72.q(parcel, a);
                    break;
                case 12:
                    str2 = j72.e(parcel, a);
                    break;
                case 13:
                    str4 = j72.e(parcel, a);
                    break;
                case 14:
                    i4 = j72.q(parcel, a);
                    break;
                case 15:
                    f = j72.n(parcel, a);
                    break;
                case 16:
                    j3 = j72.s(parcel, a);
                    break;
                case 17:
                    str5 = j72.e(parcel, a);
                    break;
                case 18:
                    z = j72.i(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new WakeLockEvent(i, j, i2, str, i3, arrayList, str2, j2, i4, str3, str4, f, j3, str5, z);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ WakeLockEvent[] newArray(int i) {
        return new WakeLockEvent[i];
    }
}
