package com.fossil;

import androidx.loader.app.LoaderManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx5 implements Factory<ex5> {
    @DexIgnore
    public static ex5 a(LoaderManager loaderManager, zw5 zw5, int i, rl4 rl4, qy5 qy5, dy5 dy5, cz5 cz5, cm5 cm5) {
        return new ex5(loaderManager, zw5, i, rl4, qy5, dy5, cz5, cm5);
    }
}
