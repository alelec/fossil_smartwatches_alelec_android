package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c23 implements d23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.collection.firebase_global_collection_flag_enabled", true);

    @DexIgnore
    @Override // com.fossil.d23
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
