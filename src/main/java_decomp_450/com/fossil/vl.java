package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vl {
    @DexIgnore
    public /* final */ ViewPager2 a;
    @DexIgnore
    public /* final */ xl b;
    @DexIgnore
    public /* final */ RecyclerView c;

    @DexIgnore
    public vl(ViewPager2 viewPager2, xl xlVar, RecyclerView recyclerView) {
        this.a = viewPager2;
        this.b = xlVar;
        this.c = recyclerView;
    }

    @DexIgnore
    public boolean a() {
        return this.b.d();
    }
}
