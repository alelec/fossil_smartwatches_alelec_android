package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uv1 implements Factory<dw1> {
    @DexIgnore
    public /* final */ Provider<by1> a;

    @DexIgnore
    public uv1(Provider<by1> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static uv1 a(Provider<by1> provider) {
        return new uv1(provider);
    }

    @DexIgnore
    public static dw1 a(by1 by1) {
        dw1 a2 = tv1.a(by1);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public dw1 get() {
        return a(this.a.get());
    }
}
