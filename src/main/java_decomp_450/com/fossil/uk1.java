package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Incorrect class signature, class is equals to this class: <T:Ljava/lang/Object;>Lcom/fossil/uk1<TT;>; */
public abstract class uk1<T> {
    @DexIgnore
    public /* final */ r60 a;
    @DexIgnore
    public /* final */ ng1 b;
    @DexIgnore
    public /* final */ pb1 c;

    @DexIgnore
    public uk1(pb1 pb1, r60 r60) {
        this.a = r60;
        this.c = pb1;
        this.b = ng1.CRC32;
    }

    @DexIgnore
    public ng1 a() {
        return this.b;
    }

    @DexIgnore
    public abstract T a(byte[] bArr) throws f41;

    @DexIgnore
    public boolean b(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        if (this.c != pb1.z.a(order.getShort(0)) || new r60(bArr[2], bArr[3]).getMajor() != this.a.getMajor() || yz0.b(order.getInt(8)) != yz0.b((bArr.length - 12) - 4)) {
            return false;
        }
        if (yz0.b(order.getInt(bArr.length - 4)) != ik1.a.a(s97.a(bArr, 12, bArr.length - 4), a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public uk1(pb1 pb1) {
        this.a = new r60((byte) 1, (byte) 0);
        this.c = pb1;
        this.b = ng1.CRC32;
    }
}
