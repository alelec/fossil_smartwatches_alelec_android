package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga7 implements List, Serializable, RandomAccess, ye7 {
    @DexIgnore
    public static /* final */ ga7 INSTANCE; // = new ga7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7390468764508069838L;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ void add(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void add(int i, Void r2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean add(Void r2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public boolean addAll(int i, Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Void) {
            return contains((Void) obj);
        }
        return false;
    }

    @DexIgnore
    public boolean contains(Void r2) {
        ee7.b(r2, "element");
        return false;
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean containsAll(Collection collection) {
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        return collection.isEmpty();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof List) && ((List) obj).isEmpty();
    }

    @DexIgnore
    public int getSize() {
        return 0;
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final /* bridge */ int indexOf(Object obj) {
        if (obj instanceof Void) {
            return indexOf((Void) obj);
        }
        return -1;
    }

    @DexIgnore
    public int indexOf(Void r2) {
        ee7.b(r2, "element");
        return -1;
    }

    @DexIgnore
    public boolean isEmpty() {
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator iterator() {
        return fa7.a;
    }

    @DexIgnore
    public final /* bridge */ int lastIndexOf(Object obj) {
        if (obj instanceof Void) {
            return lastIndexOf((Void) obj);
        }
        return -1;
    }

    @DexIgnore
    public int lastIndexOf(Void r2) {
        ee7.b(r2, "element");
        return -1;
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator listIterator() {
        return fa7.a;
    }

    @DexIgnore
    @Override // java.util.List
    public Void remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.List
    public /* synthetic */ Object set(int i, Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void set(int i, Void r2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }

    @DexIgnore
    @Override // java.util.List
    public List subList(int i, int i2) {
        if (i == 0 && i2 == 0) {
            return this;
        }
        throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2);
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return "[]";
    }

    @DexIgnore
    @Override // java.util.List
    public Void get(int i) {
        throw new IndexOutOfBoundsException("Empty list doesn't contain element at index " + i + '.');
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator listIterator(int i) {
        if (i == 0) {
            return fa7.a;
        }
        throw new IndexOutOfBoundsException("Index: " + i);
    }
}
