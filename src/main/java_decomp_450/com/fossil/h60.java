package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum h60 implements ke0 {
    BLUETOOTH_OFF(0, 255),
    LOCATION_PERMISSION_NOT_GRANTED(247, 255),
    LOCATION_SERVICE_NOT_ENABLED(248, 255),
    ALREADY_STARTED(249, 1),
    REGISTRATION_FAILED(250, 2),
    SYSTEM_INTERNAL_ERROR(251, 3),
    UNSUPPORTED(252, 4),
    OUT_OF_HARDWARE_RESOURCE(253, 5),
    SCANNING_TOO_FREQUENTLY(254, 6),
    UNKNOWN_ERROR(255, 255);
    
    @DexIgnore
    public static /* final */ a e; // = new a(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final h60 a(int i) {
            h60 h60;
            h60[] values = h60.values();
            int length = values.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    h60 = null;
                    break;
                }
                h60 = values[i2];
                if (h60.c == i) {
                    break;
                }
                i2++;
            }
            return h60 != null ? h60 : h60.UNKNOWN_ERROR;
        }
    }

    @DexIgnore
    public h60(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    @Override // com.fossil.ke0
    public int getCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ke0
    public String getLogName() {
        return this.a;
    }
}
