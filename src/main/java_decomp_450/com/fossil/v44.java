package com.fossil;

import android.os.Looper;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v44 {
    @DexIgnore
    public static /* final */ FilenameFilter a; // = new a();
    @DexIgnore
    public static /* final */ ExecutorService b; // = h44.a("awaitEvenIfOnMainThread task continuation executor");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements fo3<T, Void> {
        @DexIgnore
        public /* final */ /* synthetic */ oo3 a;

        @DexIgnore
        public b(oo3 oo3) {
            this.a = oo3;
        }

        @DexIgnore
        @Override // com.fossil.fo3
        public Void then(no3<T> no3) throws Exception {
            if (no3.e()) {
                this.a.b((Object) no3.b());
                return null;
            }
            this.a.b(no3.a());
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Callable a;
        @DexIgnore
        public /* final */ /* synthetic */ oo3 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements fo3<T, Void> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.fo3
            public Void then(no3<T> no3) throws Exception {
                if (no3.e()) {
                    c.this.b.a((Object) no3.b());
                    return null;
                }
                c.this.b.a(no3.a());
                return null;
            }
        }

        @DexIgnore
        public c(Callable callable, oo3 oo3) {
            this.a = callable;
            this.b = oo3;
        }

        @DexIgnore
        public void run() {
            try {
                ((no3) this.a.call()).a(new a());
            } catch (Exception e) {
                this.b.a(e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements fo3<T, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ CountDownLatch a;

        @DexIgnore
        public d(CountDownLatch countDownLatch) {
            this.a = countDownLatch;
        }

        @DexIgnore
        @Override // com.fossil.fo3
        public Object then(no3<T> no3) throws Exception {
            this.a.countDown();
            return null;
        }
    }

    @DexIgnore
    public static int a(File file, File file2, int i, Comparator<File> comparator) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        File[] listFiles2 = file2.listFiles(a);
        if (listFiles == null) {
            listFiles = new File[0];
        }
        if (listFiles2 == null) {
            listFiles2 = new File[0];
        }
        arrayList.addAll(Arrays.asList(listFiles));
        arrayList.addAll(Arrays.asList(listFiles2));
        return a(arrayList, i, comparator);
    }

    @DexIgnore
    public static int a(File file, int i, Comparator<File> comparator) {
        return a(file, a, i, comparator);
    }

    @DexIgnore
    public static int a(File file, FilenameFilter filenameFilter, int i, Comparator<File> comparator) {
        File[] listFiles = file.listFiles(filenameFilter);
        if (listFiles == null) {
            return 0;
        }
        return a(Arrays.asList(listFiles), i, comparator);
    }

    @DexIgnore
    public static int a(List<File> list, int i, Comparator<File> comparator) {
        int size = list.size();
        Collections.sort(list, comparator);
        for (File file : list) {
            if (size <= i) {
                return size;
            }
            a(file);
            size--;
        }
        return size;
    }

    @DexIgnore
    public static <T> no3<T> a(no3<T> no3, no3<T> no32) {
        oo3 oo3 = new oo3();
        b bVar = new b(oo3);
        no3.a(bVar);
        no32.a(bVar);
        return oo3.a();
    }

    @DexIgnore
    public static <T> no3<T> a(Executor executor, Callable<no3<T>> callable) {
        oo3 oo3 = new oo3();
        executor.execute(new c(callable, oo3));
        return oo3.a();
    }

    @DexIgnore
    public static <T> T a(no3<T> no3) throws InterruptedException, TimeoutException {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        no3.a(b, new d(countDownLatch));
        if (Looper.getMainLooper() == Looper.myLooper()) {
            countDownLatch.await(4, TimeUnit.SECONDS);
        } else {
            countDownLatch.await();
        }
        if (no3.d()) {
            return no3.b();
        }
        throw new TimeoutException();
    }

    @DexIgnore
    public static void a(File file) {
        if (file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                a(file2);
            }
        }
        file.delete();
    }
}
