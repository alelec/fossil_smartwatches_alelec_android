package com.fossil;

import com.portfolio.platform.data.model.InstalledApp;
import java.nio.charset.Charset;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ed5 implements yw {
    @DexIgnore
    public /* final */ InstalledApp b;

    @DexIgnore
    public ed5(InstalledApp installedApp) {
        ee7.b(installedApp, "installedApp");
        this.b = installedApp;
    }

    @DexIgnore
    public final InstalledApp a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        ee7.b(messageDigest, "messageDigest");
        String identifier = this.b.getIdentifier();
        ee7.a((Object) identifier, "installedApp.identifier");
        Charset charset = yw.a;
        ee7.a((Object) charset, "Key.CHARSET");
        if (identifier != null) {
            byte[] bytes = identifier.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }
}
