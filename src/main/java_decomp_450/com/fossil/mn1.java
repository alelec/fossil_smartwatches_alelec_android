package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn1 extends sj1 {
    @DexIgnore
    public /* final */ m90 A;

    @DexIgnore
    public mn1(m90 m90, ri1 ri1) {
        super(qa1.B, ri1);
        this.A = m90;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.s0, this.A.a());
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        qk1 qk1 = qk1.ASYNC;
        m90 m90 = this.A;
        ByteBuffer order = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(4).o\u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(cy0.b.a);
        order.put(ru0.MUSIC_EVENT.a);
        order.put(m90.getAction().a());
        order.put(m90.getActionStatus().a());
        byte[] array = order.array();
        ee7.a((Object) array, "musicEventData.array()");
        return new ze1(qk1, array, ((v81) this).y.w);
    }
}
