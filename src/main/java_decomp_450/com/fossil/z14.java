package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z14 implements d24 {
    @DexIgnore
    @Override // com.fossil.d24
    public <T> Set<T> c(Class<T> cls) {
        return b(cls).get();
    }

    @DexIgnore
    @Override // com.fossil.d24
    public <T> T get(Class<T> cls) {
        ob4<T> a = a(cls);
        if (a == null) {
            return null;
        }
        return a.get();
    }
}
