package com.fossil;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xp1 extends HandlerThread {
    @DexIgnore
    public /* final */ Handler a;
    @DexIgnore
    public /* final */ g71 b;
    @DexIgnore
    public /* final */ gg1 c;
    @DexIgnore
    public /* final */ i97 d; // = i97.a;
    @DexIgnore
    public /* final */ j91 e;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public xp1(String str, int i, long j, String str2, String str3, dh0 dh0, int i2, bi0 bi0, j91 j91, boolean z) {
        super(xp1.class.getSimpleName(), 10);
        this.e = j91;
        this.f = z;
        super.start();
        Handler handler = new Handler(getLooper());
        this.a = handler;
        g71 g71 = new g71(str, i, j, handler, str2, str3);
        this.b = g71;
        gg1 gg1 = new gg1(g71, dh0, bi0, this.a, this.f);
        this.c = gg1;
        if (i2 > 0) {
            long j2 = ((long) i2) * ((long) 1000);
            d91 d91 = gg1.d;
            if (d91 != null) {
                d91.a = true;
                gg1.j.removeCallbacks(d91);
            }
            if (j2 > 0) {
                gg1.f = j2;
            }
            d91 d912 = new d91(gg1);
            gg1.d = d912;
            if (d912 != null) {
                gg1.j.post(d912);
            }
        }
    }

    @DexIgnore
    public boolean a(wr1 wr1) {
        boolean b2 = b(wr1);
        if (b() > 0) {
            a(b());
        }
        return b2;
    }

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public final boolean b(wr1 wr1) {
        try {
            wr1.b = a();
            wr1.l = yj0.e.a();
            m60 a2 = yj0.e.a(wr1.getMacAddress());
            if (a2 != null) {
                wr1.k = a2;
            }
            wr1.j = yj0.e.b(wr1.getMacAddress());
            String b2 = yn0.h.b(wr1.a(0));
            if (b2 != null) {
                return this.b.a(b2);
            }
            return false;
        } catch (OutOfMemoryError unused) {
            return false;
        }
    }

    @DexIgnore
    public final long c() {
        return this.b.e();
    }

    @DexIgnore
    @SuppressLint({"ApplySharedPref"})
    /* renamed from: d */
    public final long a() {
        Long l;
        long longValue;
        SharedPreferences.Editor edit;
        SharedPreferences.Editor putLong;
        synchronized (this.d) {
            SharedPreferences a2 = yz0.a(this.e);
            Long valueOf = a2 != null ? Long.valueOf(a2.getLong("log_line_number", Long.MAX_VALUE)) : null;
            if (valueOf != null) {
                if (valueOf.longValue() != Long.MAX_VALUE) {
                    l = Long.valueOf(valueOf.longValue() + 1);
                    if (!(a2 == null || (edit = a2.edit()) == null || (putLong = edit.putLong("log_line_number", l.longValue())) == null)) {
                        putLong.commit();
                    }
                    longValue = l.longValue();
                }
            }
            l = 0L;
            putLong.commit();
            longValue = l.longValue();
        }
        return longValue;
    }

    @DexIgnore
    public final void start() {
        super.start();
    }

    @DexIgnore
    public static /* synthetic */ void a(xp1 xp1, long j, int i, Object obj) {
        if (obj == null) {
            if ((i & 1) != 0) {
                j = 0;
            }
            xp1.a(j);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startUploadLog");
    }

    @DexIgnore
    public final void a(long j) {
        gg1 gg1 = this.c;
        db1 db1 = gg1.e;
        if (db1 != null) {
            db1.a = true;
            gg1.j.removeCallbacks(db1);
        }
        db1 db12 = new db1(new ya1(gg1));
        gg1.e = db12;
        if (db12 != null) {
            gg1.j.postDelayed(db12, j);
        }
    }
}
