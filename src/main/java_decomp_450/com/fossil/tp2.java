package com.fossil;

import com.fossil.bw2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp2 extends bw2<tp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ tp2 zzj;
    @DexIgnore
    public static volatile wx2<tp2> zzk;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public String zzd; // = "";
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public long zzf;
    @DexIgnore
    public float zzg;
    @DexIgnore
    public double zzh;
    @DexIgnore
    public jw2<tp2> zzi; // = bw2.o();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<tp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(tp2.zzj);
        }

        @DexIgnore
        public final a a(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).a(str);
            return this;
        }

        @DexIgnore
        public final a b(String str) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).b(str);
            return this;
        }

        @DexIgnore
        public final a o() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).B();
            return this;
        }

        @DexIgnore
        public final a p() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).C();
            return this;
        }

        @DexIgnore
        public final int q() {
            return ((tp2) ((bw2.a) this).b).z();
        }

        @DexIgnore
        public final a zza() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).A();
            return this;
        }

        @DexIgnore
        public final a zze() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).E();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }

        @DexIgnore
        public final a a(long j) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).a(j);
            return this;
        }

        @DexIgnore
        public final a a(double d) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).a(d);
            return this;
        }

        @DexIgnore
        public final a a(a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).a((tp2) ((bw2) aVar.g()));
            return this;
        }

        @DexIgnore
        public final a a(Iterable<? extends tp2> iterable) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((tp2) ((bw2.a) this).b).a(iterable);
            return this;
        }
    }

    /*
    static {
        tp2 tp2 = new tp2();
        zzj = tp2;
        bw2.a(tp2.class, tp2);
    }
    */

    @DexIgnore
    public static a F() {
        return (a) zzj.e();
    }

    @DexIgnore
    public final void A() {
        this.zzc &= -3;
        this.zze = zzj.zze;
    }

    @DexIgnore
    public final void B() {
        this.zzc &= -5;
        this.zzf = 0;
    }

    @DexIgnore
    public final void C() {
        this.zzc &= -17;
        this.zzh = 0.0d;
    }

    @DexIgnore
    public final void D() {
        jw2<tp2> jw2 = this.zzi;
        if (!jw2.zza()) {
            this.zzi = bw2.a(jw2);
        }
    }

    @DexIgnore
    public final void E() {
        this.zzi = bw2.o();
    }

    @DexIgnore
    public final void a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    @DexIgnore
    public final void b(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    @DexIgnore
    public final String p() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String r() {
        return this.zze;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final long t() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean u() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final float v() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean w() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final double x() {
        return this.zzh;
    }

    @DexIgnore
    public final List<tp2> y() {
        return this.zzi;
    }

    @DexIgnore
    public final int z() {
        return this.zzi.size();
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 4;
        this.zzf = j;
    }

    @DexIgnore
    public final void a(double d) {
        this.zzc |= 16;
        this.zzh = d;
    }

    @DexIgnore
    public final void a(tp2 tp2) {
        tp2.getClass();
        D();
        this.zzi.add(tp2);
    }

    @DexIgnore
    public final void a(Iterable<? extends tp2> iterable) {
        D();
        ju2.a(iterable, this.zzi);
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new tp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001\u1008\u0000\u0002\u1008\u0001\u0003\u1002\u0002\u0004\u1001\u0003\u0005\u1000\u0004\u0006\u001b", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi", tp2.class});
            case 4:
                return zzj;
            case 5:
                wx2<tp2> wx2 = zzk;
                if (wx2 == null) {
                    synchronized (tp2.class) {
                        wx2 = zzk;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzj);
                            zzk = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
