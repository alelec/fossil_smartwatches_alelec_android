package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.DeclarationFile;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppVariant;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gj5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public List<DeclarationFile> d;
    @DexIgnore
    public long e;
    @DexIgnore
    public long f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;

    @DexIgnore
    public void a(List<DeclarationFile> list) {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        this.d.addAll(list);
    }

    @DexIgnore
    public void b(String str) {
        this.c = str;
    }

    @DexIgnore
    public void c(String str) {
        this.b = str;
    }

    @DexIgnore
    public void d(String str) {
        this.i = str;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public void b(long j) {
        this.f = j;
    }

    @DexIgnore
    public List<DeclarationFile> c() {
        if (this.d == null) {
            this.d = new ArrayList();
        }
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.h;
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public void a(long j) {
        this.e = j;
    }

    @DexIgnore
    public void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void a(String str) {
        this.a = str;
    }

    @DexIgnore
    public void a(int i2) {
        this.g = i2;
    }

    @DexIgnore
    public MicroAppVariant a() {
        MicroAppVariant microAppVariant = new MicroAppVariant();
        microAppVariant.setAppId(this.a);
        microAppVariant.setName(this.b);
        microAppVariant.setDescription(this.c);
        microAppVariant.setCreateAt(this.e);
        microAppVariant.setUpdateAt(this.f);
        microAppVariant.setMajorNumber(this.g);
        microAppVariant.setMinorNumber(this.h);
        microAppVariant.setSerialNumbers(this.i);
        return microAppVariant;
    }
}
