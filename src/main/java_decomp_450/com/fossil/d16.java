package com.fossil;

import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d16 extends z06 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ String h; // = ig5.a(PortfolioApp.g0.c(), 2131886348);
    @DexIgnore
    public /* final */ String i; // = ig5.a(PortfolioApp.g0.c(), 2131886349);
    @DexIgnore
    public /* final */ ArrayList<String> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ a16 k;
    @DexIgnore
    public /* final */ ch5 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ oe7 $isAddressSaved$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ oe7 $isChanged$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.h().a(this.this$0.this$0.j);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d16$b$b")
        /* renamed from: com.fossil.d16$b$b  reason: collision with other inner class name */
        public static final class C0034b extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0034b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0034b bVar = new C0034b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((C0034b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository i2 = this.this$0.this$0.i();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = i2.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(MFUser mFUser, fb7 fb7, b bVar) {
                super(2, fb7);
                this.$user = mFUser;
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.$user, fb7, this.this$0);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository i2 = this.this$0.this$0.i();
                    MFUser mFUser = this.$user;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = i2.updateUser(mFUser, true, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class d extends zb7 implements kd7<yi7, fb7<? super zi5<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(MFUser mFUser, fb7 fb7, b bVar) {
                super(2, fb7);
                this.$user = mFUser;
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.$user, fb7, this.this$0);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super zi5<? extends MFUser>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository i2 = this.this$0.this$0.i();
                    MFUser mFUser = this.$user;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = i2.updateUser(mFUser, false, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, fb7 fb7, d16 d16, String str2, oe7 oe7, oe7 oe72) {
            super(2, fb7);
            this.$it = str;
            this.this$0 = d16;
            this.$address$inlined = str2;
            this.$isChanged$inlined = oe7;
            this.$isAddressSaved$inlined = oe72;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$it, fb7, this.this$0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:115:0x02d8  */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x02e4  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0187  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x01ce  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x0230  */
        /* JADX WARNING: Removed duplicated region for block: B:94:0x0273  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                java.lang.String r2 = " failed"
                java.lang.String r3 = " successfully"
                r4 = 4
                r5 = 3
                r6 = 2
                r7 = 0
                java.lang.String r8 = " - work = "
                r9 = 1
                r10 = 0
                if (r1 == 0) goto L_0x0058
                if (r1 == r9) goto L_0x0050
                if (r1 == r6) goto L_0x0046
                if (r1 == r5) goto L_0x0035
                if (r1 != r4) goto L_0x002d
                java.lang.Object r0 = r14.L$2
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r1 = r14.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x0226
            L_0x002d:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0035:
                java.lang.Object r1 = r14.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r5 = r14.L$1
                com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
                java.lang.Object r6 = r14.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r15)
                goto L_0x017f
            L_0x0046:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
            L_0x004d:
                r6 = r1
                goto L_0x00d4
            L_0x0050:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x00be
            L_0x0058:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r1 = r14.p$
                java.lang.String r15 = r14.$it
                int r15 = r15.length()
                if (r15 != 0) goto L_0x0067
                r15 = 1
                goto L_0x0068
            L_0x0067:
                r15 = 0
            L_0x0068:
                if (r15 != 0) goto L_0x00be
                com.fossil.d16 r15 = r14.this$0
                java.util.ArrayList r15 = r15.j
                java.lang.String r11 = r14.$it
                boolean r15 = r15.contains(r11)
                if (r15 != 0) goto L_0x00be
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r11 = com.fossil.d16.n
                java.lang.StringBuilder r12 = new java.lang.StringBuilder
                r12.<init>()
                java.lang.String r13 = "add "
                r12.append(r13)
                java.lang.String r13 = r14.$address$inlined
                r12.append(r13)
                java.lang.String r13 = " to recent search list"
                r12.append(r13)
                java.lang.String r12 = r12.toString()
                r15.d(r11, r12)
                com.fossil.d16 r15 = r14.this$0
                java.util.ArrayList r15 = r15.j
                java.lang.String r11 = r14.$it
                r15.add(r7, r11)
                com.fossil.d16 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.c()
                com.fossil.d16$b$a r11 = new com.fossil.d16$b$a
                r11.<init>(r14, r10)
                r14.L$0 = r1
                r14.label = r9
                java.lang.Object r15 = com.fossil.vh7.a(r15, r11, r14)
                if (r15 != r0) goto L_0x00be
                return r0
            L_0x00be:
                com.fossil.d16 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.c()
                com.fossil.d16$b$b r11 = new com.fossil.d16$b$b
                r11.<init>(r14, r10)
                r14.L$0 = r1
                r14.label = r6
                java.lang.Object r15 = com.fossil.vh7.a(r15, r11, r14)
                if (r15 != r0) goto L_0x004d
                return r0
            L_0x00d4:
                com.portfolio.platform.data.model.MFUser r15 = (com.portfolio.platform.data.model.MFUser) r15
                if (r15 == 0) goto L_0x02ed
                com.fossil.d16 r1 = r14.this$0
                java.lang.String r1 = r1.g
                if (r1 != 0) goto L_0x00e2
                goto L_0x0153
            L_0x00e2:
                int r11 = r1.hashCode()
                r12 = 2255103(0x2268ff, float:3.160072E-39)
                if (r11 == r12) goto L_0x0122
                r12 = 76517104(0x48f8ef0, float:3.3750406E-36)
                if (r11 == r12) goto L_0x00f1
                goto L_0x0153
            L_0x00f1:
                java.lang.String r11 = "Other"
                boolean r1 = r1.equals(r11)
                if (r1 == 0) goto L_0x0153
                com.fossil.oe7 r1 = r14.$isChanged$inlined
                com.portfolio.platform.data.model.MFUser$Address r11 = r15.getAddresses()
                if (r11 == 0) goto L_0x011e
                java.lang.String r11 = r11.getWork()
                java.lang.String r12 = r14.$address$inlined
                boolean r11 = android.text.TextUtils.equals(r11, r12)
                r11 = r11 ^ r9
                r1.element = r11
                com.portfolio.platform.data.model.MFUser$Address r1 = r15.getAddresses()
                if (r1 == 0) goto L_0x011a
                java.lang.String r11 = r14.$address$inlined
                r1.setWork(r11)
                goto L_0x0153
            L_0x011a:
                com.fossil.ee7.a()
                throw r10
            L_0x011e:
                com.fossil.ee7.a()
                throw r10
            L_0x0122:
                java.lang.String r11 = "Home"
                boolean r1 = r1.equals(r11)
                if (r1 == 0) goto L_0x0153
                com.fossil.oe7 r1 = r14.$isChanged$inlined
                com.portfolio.platform.data.model.MFUser$Address r11 = r15.getAddresses()
                if (r11 == 0) goto L_0x014f
                java.lang.String r11 = r11.getHome()
                java.lang.String r12 = r14.$address$inlined
                boolean r11 = android.text.TextUtils.equals(r11, r12)
                r11 = r11 ^ r9
                r1.element = r11
                com.portfolio.platform.data.model.MFUser$Address r1 = r15.getAddresses()
                if (r1 == 0) goto L_0x014b
                java.lang.String r11 = r14.$address$inlined
                r1.setHome(r11)
                goto L_0x0153
            L_0x014b:
                com.fossil.ee7.a()
                throw r10
            L_0x014f:
                com.fossil.ee7.a()
                throw r10
            L_0x0153:
                com.fossil.oe7 r1 = r14.$isChanged$inlined
                boolean r1 = r1.element
                if (r1 == 0) goto L_0x02d2
                com.fossil.d16 r1 = r14.this$0
                com.fossil.a16 r1 = r1.j()
                r1.b()
                com.fossil.d16 r1 = r14.this$0
                com.fossil.ti7 r1 = r1.c()
                com.fossil.d16$b$c r11 = new com.fossil.d16$b$c
                r11.<init>(r15, r10, r14)
                r14.L$0 = r6
                r14.L$1 = r15
                r14.L$2 = r15
                r14.label = r5
                java.lang.Object r1 = com.fossil.vh7.a(r1, r11, r14)
                if (r1 != r0) goto L_0x017c
                return r0
            L_0x017c:
                r5 = r15
                r15 = r1
                r1 = r5
            L_0x017f:
                com.fossil.zi5 r15 = (com.fossil.zi5) r15
                boolean r11 = r15 instanceof com.fossil.bj5
                java.lang.String r12 = "update UserRepository to remote DB success - home = "
                if (r11 == 0) goto L_0x01ce
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r0 = com.fossil.d16.n
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                r2.append(r12)
                com.portfolio.platform.data.model.MFUser$Address r4 = r1.getAddresses()
                if (r4 == 0) goto L_0x01ca
                java.lang.String r4 = r4.getHome()
                r2.append(r4)
                r2.append(r8)
                com.portfolio.platform.data.model.MFUser$Address r1 = r1.getAddresses()
                if (r1 == 0) goto L_0x01c6
                java.lang.String r1 = r1.getWork()
                r2.append(r1)
                r2.append(r3)
                java.lang.String r1 = r2.toString()
                r15.d(r0, r1)
                com.fossil.oe7 r15 = r14.$isAddressSaved$inlined
                r15.element = r9
                goto L_0x02c9
            L_0x01c6:
                com.fossil.ee7.a()
                throw r10
            L_0x01ca:
                com.fossil.ee7.a()
                throw r10
            L_0x01ce:
                boolean r15 = r15 instanceof com.fossil.yi5
                if (r15 == 0) goto L_0x02c9
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r11 = com.fossil.d16.n
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                r13.append(r12)
                com.portfolio.platform.data.model.MFUser$Address r12 = r1.getAddresses()
                if (r12 == 0) goto L_0x02c5
                java.lang.String r12 = r12.getHome()
                r13.append(r12)
                r13.append(r8)
                com.portfolio.platform.data.model.MFUser$Address r12 = r1.getAddresses()
                if (r12 == 0) goto L_0x02c1
                java.lang.String r12 = r12.getWork()
                r13.append(r12)
                r13.append(r2)
                java.lang.String r12 = r13.toString()
                r15.d(r11, r12)
                com.fossil.d16 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.c()
                com.fossil.d16$b$d r11 = new com.fossil.d16$b$d
                r11.<init>(r1, r10, r14)
                r14.L$0 = r6
                r14.L$1 = r5
                r14.L$2 = r1
                r14.label = r4
                java.lang.Object r15 = com.fossil.vh7.a(r15, r11, r14)
                if (r15 != r0) goto L_0x0225
                return r0
            L_0x0225:
                r0 = r1
            L_0x0226:
                com.fossil.zi5 r15 = (com.fossil.zi5) r15
                com.fossil.oe7 r1 = r14.$isAddressSaved$inlined
                boolean r4 = r15 instanceof com.fossil.bj5
                java.lang.String r5 = "update UserRepository to local DB success - home = "
                if (r4 == 0) goto L_0x0273
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r2 = com.fossil.d16.n
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                r4.append(r5)
                com.portfolio.platform.data.model.MFUser$Address r5 = r0.getAddresses()
                if (r5 == 0) goto L_0x026f
                java.lang.String r5 = r5.getHome()
                r4.append(r5)
                r4.append(r8)
                com.portfolio.platform.data.model.MFUser$Address r0 = r0.getAddresses()
                if (r0 == 0) goto L_0x026b
                java.lang.String r0 = r0.getWork()
                r4.append(r0)
                r4.append(r3)
                java.lang.String r0 = r4.toString()
                r15.d(r2, r0)
                r7 = 1
                goto L_0x02b0
            L_0x026b:
                com.fossil.ee7.a()
                throw r10
            L_0x026f:
                com.fossil.ee7.a()
                throw r10
            L_0x0273:
                boolean r15 = r15 instanceof com.fossil.yi5
                if (r15 == 0) goto L_0x02bb
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                java.lang.String r3 = com.fossil.d16.n
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                r4.append(r5)
                com.portfolio.platform.data.model.MFUser$Address r5 = r0.getAddresses()
                if (r5 == 0) goto L_0x02b7
                java.lang.String r5 = r5.getHome()
                r4.append(r5)
                r4.append(r8)
                com.portfolio.platform.data.model.MFUser$Address r0 = r0.getAddresses()
                if (r0 == 0) goto L_0x02b3
                java.lang.String r0 = r0.getWork()
                r4.append(r0)
                r4.append(r2)
                java.lang.String r0 = r4.toString()
                r15.d(r3, r0)
            L_0x02b0:
                r1.element = r7
                goto L_0x02c9
            L_0x02b3:
                com.fossil.ee7.a()
                throw r10
            L_0x02b7:
                com.fossil.ee7.a()
                throw r10
            L_0x02bb:
                com.fossil.p87 r15 = new com.fossil.p87
                r15.<init>()
                throw r15
            L_0x02c1:
                com.fossil.ee7.a()
                throw r10
            L_0x02c5:
                com.fossil.ee7.a()
                throw r10
            L_0x02c9:
                com.fossil.d16 r15 = r14.this$0
                com.fossil.a16 r15 = r15.j()
                r15.a()
            L_0x02d2:
                com.fossil.oe7 r15 = r14.$isAddressSaved$inlined
                boolean r15 = r15.element
                if (r15 == 0) goto L_0x02e4
                com.fossil.d16 r15 = r14.this$0
                com.fossil.a16 r15 = r15.j()
                java.lang.String r0 = r14.$address$inlined
                r15.y(r0)
                goto L_0x02ed
            L_0x02e4:
                com.fossil.d16 r15 = r14.this$0
                com.fossil.a16 r15 = r15.j()
                r15.y(r10)
            L_0x02ed:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d16.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1$recentSearchedAddress$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<String>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.h().f();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(d16 d16, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d16;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ee7.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.e = Places.createClient(PortfolioApp.g0.c());
            this.this$0.j().a(this.this$0.e);
            if (ee7.a((Object) this.this$0.g, (Object) "Home")) {
                a16 j = this.this$0.j();
                String d = this.this$0.h;
                ee7.a((Object) d, "mHomeTitle");
                j.setTitle(d);
            } else {
                a16 j2 = this.this$0.j();
                String g = this.this$0.i;
                ee7.a((Object) g, "mWorkTitle");
                j2.setTitle(g);
            }
            this.this$0.j().p(this.this$0.f);
            this.this$0.j().e(this.this$0.j);
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = d16.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public d16(a16 a16, ch5 ch5, UserRepository userRepository) {
        ee7.b(a16, "mView");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
        this.k = a16;
        this.l = ch5;
        this.m = userRepository;
    }

    @DexIgnore
    public final ch5 h() {
        return this.l;
    }

    @DexIgnore
    public final UserRepository i() {
        return this.m;
    }

    @DexIgnore
    public final a16 j() {
        return this.k;
    }

    @DexIgnore
    public void k() {
        this.k.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.e = null;
    }

    @DexIgnore
    public void a(String str, String str2) {
        ee7.b(str, "addressType");
        ee7.b(str2, "defaultPlace");
        this.f = str2;
        this.g = str;
    }

    @DexIgnore
    @Override // com.fossil.z06
    public void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "onUserExit with address " + str);
        oe7 oe7 = new oe7();
        oe7.element = false;
        oe7 oe72 = new oe7();
        oe72.element = false;
        if (str == null || xh7.b(e(), null, null, new b(str, null, this, str, oe7, oe72), 3, null) == null) {
            this.k.y(null);
            i97 i97 = i97.a;
        }
    }
}
