package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gk2 extends ak2<oi2> {
    @DexIgnore
    public static /* final */ vj2 E; // = vj2.FIT_CONFIG;
    @DexIgnore
    public static /* final */ v02.g<gk2> F; // = new v02.g<>();
    @DexIgnore
    public static /* final */ v02<v02.d.C0203d> G; // = new v02<>("Fitness.CONFIG_API", new ik2(), F);

    /*
    static {
        new v02("Fitness.CONFIG_CLIENT", new kk2(), F);
    }
    */

    @DexIgnore
    public gk2(Context context, Looper looper, j62 j62, a12.b bVar, a12.c cVar) {
        super(context, looper, E, bVar, cVar, j62);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitConfigApi");
        if (queryLocalInterface instanceof oi2) {
            return (oi2) queryLocalInterface;
        }
        return new ni2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.fitness.internal.IGoogleFitConfigApi";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public final int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.fitness.ConfigApi";
    }
}
