package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ck7 implements dk7 {
    @DexIgnore
    public /* final */ uk7 a;

    @DexIgnore
    public ck7(uk7 uk7) {
        this.a = uk7;
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public uk7 a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public boolean isActive() {
        return false;
    }

    @DexIgnore
    public String toString() {
        return dj7.c() ? a().a("New") : super.toString();
    }
}
