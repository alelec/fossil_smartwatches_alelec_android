package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zf0 {
    ON,
    OFF,
    TOGGLE;
    
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final zf0 a(String str) {
            zf0[] values = zf0.values();
            for (zf0 zf0 : values) {
                String a = yz0.a(zf0);
                String lowerCase = str.toLowerCase(b21.x.e());
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                if (ee7.a((Object) a, (Object) lowerCase)) {
                    return zf0;
                }
            }
            return null;
        }
    }
}
