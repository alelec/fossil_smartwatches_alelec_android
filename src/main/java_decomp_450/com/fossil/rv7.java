package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv7 implements tu7<mo7, Float> {
    @DexIgnore
    public static /* final */ rv7 a; // = new rv7();

    @DexIgnore
    public Float a(mo7 mo7) throws IOException {
        return Float.valueOf(mo7.string());
    }
}
