package com.fossil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw2 extends sw2 {
    @DexIgnore
    public static /* final */ Class<?> c; // = Collections.unmodifiableList(Collections.emptyList()).getClass();

    @DexIgnore
    public uw2() {
        super();
    }

    @DexIgnore
    public static <E> List<E> b(Object obj, long j) {
        return (List) bz2.f(obj, j);
    }

    @DexIgnore
    @Override // com.fossil.sw2
    public final void a(Object obj, long j) {
        Object obj2;
        List list = (List) bz2.f(obj, j);
        if (list instanceof tw2) {
            obj2 = ((tw2) list).zze();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof vx2) || !(list instanceof jw2)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                jw2 jw2 = (jw2) list;
                if (jw2.zza()) {
                    jw2.zzb();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        bz2.a(obj, j, obj2);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v15, resolved type: com.fossil.qw2 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v17, resolved type: com.fossil.qw2 */
    /* JADX DEBUG: Multi-variable search result rejected for r1v18, resolved type: com.fossil.qw2 */
    /* JADX WARN: Multi-variable type inference failed */
    public static <L> List<L> a(Object obj, long j, int i) {
        List<L> list;
        List<L> list2;
        ArrayList arrayList;
        List<L> b = b(obj, j);
        if (b.isEmpty()) {
            if (b instanceof tw2) {
                list2 = new qw2(i);
            } else if (!(b instanceof vx2) || !(b instanceof jw2)) {
                list2 = new ArrayList<>(i);
            } else {
                list2 = ((jw2) b).zza(i);
            }
            bz2.a(obj, j, list2);
            return list2;
        }
        if (c.isAssignableFrom(b.getClass())) {
            ArrayList arrayList2 = new ArrayList(b.size() + i);
            arrayList2.addAll(b);
            bz2.a(obj, j, arrayList2);
            arrayList = arrayList2;
        } else if (b instanceof vy2) {
            qw2 qw2 = new qw2(b.size() + i);
            qw2.addAll((vy2) b);
            bz2.a(obj, j, qw2);
            arrayList = qw2;
        } else if (!(b instanceof vx2) || !(b instanceof jw2)) {
            return b;
        } else {
            jw2 jw2 = (jw2) b;
            if (jw2.zza()) {
                return b;
            }
            jw2 zza = jw2.zza(b.size() + i);
            bz2.a(obj, j, zza);
            return zza;
        }
        return list;
    }

    @DexIgnore
    @Override // com.fossil.sw2
    public final <E> void a(Object obj, Object obj2, long j) {
        List b = b(obj2, j);
        List a = a(obj, j, b.size());
        int size = a.size();
        int size2 = b.size();
        if (size > 0 && size2 > 0) {
            a.addAll(b);
        }
        if (size > 0) {
            b = a;
        }
        bz2.a(obj, j, b);
    }
}
