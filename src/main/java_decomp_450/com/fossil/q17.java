package com.fossil;

import io.flutter.plugin.common.MethodCall;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q17 {
    @DexIgnore
    public static Integer a(MethodCall methodCall) {
        return (Integer) methodCall.argument("logLevel");
    }

    @DexIgnore
    public static boolean a(int i) {
        return i >= 1;
    }

    @DexIgnore
    public static boolean b(int i) {
        return i >= 2;
    }
}
