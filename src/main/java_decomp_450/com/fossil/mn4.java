package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn4 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("challengeType")
    public String b;
    @DexIgnore
    @te4("name")
    public String c;
    @DexIgnore
    @te4("description")
    public String d;
    @DexIgnore
    @te4("owner")
    public eo4 e;
    @DexIgnore
    @te4("numberOfPlayers")
    public Integer f;
    @DexIgnore
    @te4(SampleRaw.COLUMN_START_TIME)
    public Date g;
    @DexIgnore
    @te4(SampleRaw.COLUMN_END_TIME)
    public Date h;
    @DexIgnore
    @te4("target")
    public Integer i;
    @DexIgnore
    @te4("duration")
    public Integer j;
    @DexIgnore
    @te4(ShareConstants.WEB_DIALOG_PARAM_PRIVACY)
    public String p;
    @DexIgnore
    @te4("version")
    public String q;
    @DexIgnore
    @te4("status")
    public String r;
    @DexIgnore
    @te4("syncStatusDataBase64")
    public String s;
    @DexIgnore
    @te4("createdAt")
    public Date t;
    @DexIgnore
    @te4("updatedAt")
    public Date u;
    @DexIgnore
    public String v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<mn4> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public mn4 createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new mn4(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public mn4[] newArray(int i) {
            return new mn4[i];
        }
    }

    @DexIgnore
    public mn4(String str, String str2, String str3, String str4, eo4 eo4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, String str8, Date date3, Date date4, String str9) {
        ee7.b(str, "id");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = eo4;
        this.f = num;
        this.g = date;
        this.h = date2;
        this.i = num2;
        this.j = num3;
        this.p = str5;
        this.q = str6;
        this.r = str7;
        this.s = str8;
        this.t = date3;
        this.u = date4;
        this.v = str9;
    }

    @DexIgnore
    public final void a(Integer num) {
        this.f = num;
    }

    @DexIgnore
    public final Date b() {
        return this.t;
    }

    @DexIgnore
    public final String c() {
        return this.d;
    }

    @DexIgnore
    public final Integer d() {
        return this.j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final Date e() {
        return this.h;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof mn4)) {
            return false;
        }
        mn4 mn4 = (mn4) obj;
        return ee7.a(this.a, mn4.a) && ee7.a(this.b, mn4.b) && ee7.a(this.c, mn4.c) && ee7.a(this.d, mn4.d) && ee7.a(this.e, mn4.e) && ee7.a(this.f, mn4.f) && ee7.a(this.g, mn4.g) && ee7.a(this.h, mn4.h) && ee7.a(this.i, mn4.i) && ee7.a(this.j, mn4.j) && ee7.a(this.p, mn4.p) && ee7.a(this.q, mn4.q) && ee7.a(this.r, mn4.r) && ee7.a(this.s, mn4.s) && ee7.a(this.t, mn4.t) && ee7.a(this.u, mn4.u) && ee7.a(this.v, mn4.v);
    }

    @DexIgnore
    public final String f() {
        return this.a;
    }

    @DexIgnore
    public final String g() {
        return this.c;
    }

    @DexIgnore
    public final Integer h() {
        return this.f;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        eo4 eo4 = this.e;
        int hashCode5 = (hashCode4 + (eo4 != null ? eo4.hashCode() : 0)) * 31;
        Integer num = this.f;
        int hashCode6 = (hashCode5 + (num != null ? num.hashCode() : 0)) * 31;
        Date date = this.g;
        int hashCode7 = (hashCode6 + (date != null ? date.hashCode() : 0)) * 31;
        Date date2 = this.h;
        int hashCode8 = (hashCode7 + (date2 != null ? date2.hashCode() : 0)) * 31;
        Integer num2 = this.i;
        int hashCode9 = (hashCode8 + (num2 != null ? num2.hashCode() : 0)) * 31;
        Integer num3 = this.j;
        int hashCode10 = (hashCode9 + (num3 != null ? num3.hashCode() : 0)) * 31;
        String str5 = this.p;
        int hashCode11 = (hashCode10 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.q;
        int hashCode12 = (hashCode11 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.r;
        int hashCode13 = (hashCode12 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.s;
        int hashCode14 = (hashCode13 + (str8 != null ? str8.hashCode() : 0)) * 31;
        Date date3 = this.t;
        int hashCode15 = (hashCode14 + (date3 != null ? date3.hashCode() : 0)) * 31;
        Date date4 = this.u;
        int hashCode16 = (hashCode15 + (date4 != null ? date4.hashCode() : 0)) * 31;
        String str9 = this.v;
        if (str9 != null) {
            i2 = str9.hashCode();
        }
        return hashCode16 + i2;
    }

    @DexIgnore
    public final eo4 i() {
        return this.e;
    }

    @DexIgnore
    public final String j() {
        return this.p;
    }

    @DexIgnore
    public final Date m() {
        return this.g;
    }

    @DexIgnore
    public final String p() {
        return this.r;
    }

    @DexIgnore
    public final String s() {
        return this.s;
    }

    @DexIgnore
    public final Integer t() {
        return this.i;
    }

    @DexIgnore
    public String toString() {
        return "Challenge(id=" + this.a + ", type=" + this.b + ", name=" + this.c + ", des=" + this.d + ", owner=" + this.e + ", numberOfPlayers=" + this.f + ", startTime=" + this.g + ", endTime=" + this.h + ", target=" + this.i + ", duration=" + this.j + ", privacy=" + this.p + ", version=" + this.q + ", status=" + this.r + ", syncData=" + this.s + ", createdAt=" + this.t + ", updatedAt=" + this.u + ", category=" + this.v + ")";
    }

    @DexIgnore
    public final String u() {
        return this.b;
    }

    @DexIgnore
    public final Date v() {
        return this.u;
    }

    @DexIgnore
    public final String w() {
        return this.q;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeString(this.d);
        parcel.writeParcelable(this.e, i2);
        parcel.writeValue(this.f);
        Date date = this.g;
        if (date != null) {
            parcel.writeLong(date.getTime());
        }
        Date date2 = this.h;
        if (date2 != null) {
            parcel.writeLong(date2.getTime());
        }
        parcel.writeValue(this.i);
        parcel.writeValue(this.j);
        parcel.writeString(this.p);
        parcel.writeString(this.q);
        parcel.writeString(this.r);
        parcel.writeString(this.s);
        Date date3 = this.t;
        if (date3 != null) {
            parcel.writeLong(date3.getTime());
        }
        Date date4 = this.u;
        if (date4 != null) {
            parcel.writeLong(date4.getTime());
        }
        parcel.writeString(this.v);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ mn4(String str, String str2, String str3, String str4, eo4 eo4, Integer num, Date date, Date date2, Integer num2, Integer num3, String str5, String str6, String str7, String str8, Date date3, Date date4, String str9, int i2, zd7 zd7) {
        this(str, str2, str3, str4, eo4, num, date, date2, num2, num3, str5, str6, str7, str8, date3, date4, (i2 & 65536) != 0 ? "joined_challenge" : str9);
    }

    @DexIgnore
    public final void a(String str) {
        this.r = str;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public mn4(android.os.Parcel r23) {
        /*
            r22 = this;
            r0 = r23
            java.lang.String r1 = "parcel"
            com.fossil.ee7.b(r0, r1)
            java.lang.String r3 = r23.readString()
            r1 = 0
            if (r3 == 0) goto L_0x00ad
            java.lang.String r2 = "parcel.readString()!!"
            com.fossil.ee7.a(r3, r2)
            java.lang.String r4 = r23.readString()
            java.lang.String r5 = r23.readString()
            java.lang.String r6 = r23.readString()
            java.lang.Class<com.fossil.eo4> r2 = com.fossil.eo4.class
            java.lang.ClassLoader r2 = r2.getClassLoader()
            android.os.Parcelable r2 = r0.readParcelable(r2)
            r7 = r2
            com.fossil.eo4 r7 = (com.fossil.eo4) r7
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r8 = r2 instanceof java.lang.Integer
            if (r8 != 0) goto L_0x003b
            r2 = r1
        L_0x003b:
            r8 = r2
            java.lang.Integer r8 = (java.lang.Integer) r8
            java.util.Date r9 = new java.util.Date
            long r10 = r23.readLong()
            r9.<init>(r10)
            java.util.Date r10 = new java.util.Date
            long r11 = r23.readLong()
            r10.<init>(r11)
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r11 = r2 instanceof java.lang.Integer
            if (r11 != 0) goto L_0x005f
            r2 = r1
        L_0x005f:
            r11 = r2
            java.lang.Integer r11 = (java.lang.Integer) r11
            java.lang.Class r2 = java.lang.Integer.TYPE
            java.lang.ClassLoader r2 = r2.getClassLoader()
            java.lang.Object r2 = r0.readValue(r2)
            boolean r12 = r2 instanceof java.lang.Integer
            if (r12 != 0) goto L_0x0071
            goto L_0x0072
        L_0x0071:
            r1 = r2
        L_0x0072:
            r12 = r1
            java.lang.Integer r12 = (java.lang.Integer) r12
            java.lang.String r13 = r23.readString()
            java.lang.String r14 = r23.readString()
            java.lang.String r15 = r23.readString()
            java.lang.String r16 = r23.readString()
            java.util.Date r1 = new java.util.Date
            r17 = r1
            r20 = r12
            r21 = r13
            long r12 = r23.readLong()
            r1.<init>(r12)
            java.util.Date r1 = new java.util.Date
            r18 = r1
            long r12 = r23.readLong()
            r1.<init>(r12)
            java.lang.String r19 = r23.readString()
            r2 = r22
            r12 = r20
            r13 = r21
            r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x00ad:
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mn4.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public final String a() {
        return this.v;
    }
}
