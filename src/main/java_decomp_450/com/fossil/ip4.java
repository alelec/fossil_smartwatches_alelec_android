package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.kp4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ip4 extends go5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<wy4> g;
    @DexIgnore
    public kp4 h;
    @DexIgnore
    public sn4 i;
    @DexIgnore
    public qn4 j;
    @DexIgnore
    public List<dn4> p; // = new ArrayList();
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ip4.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final ip4 a(sn4 sn4, qn4 qn4) {
            ip4 ip4 = new ip4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_template_extra", sn4);
            bundle.putParcelable("challenge_draft_extra", qn4);
            ip4.setArguments(bundle);
            return ip4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public b(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.onBackPressed();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public c(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ip4.d(this.a).t();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public d(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public final void onClick(View view) {
            ip4.d(this.a).s();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public e(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public f(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kp4 d = ip4.d(this.a);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                d.b(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public g(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            kp4 d = ip4.d(this.a);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                d.a(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public h(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ip4.d(this.a).a(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public i(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ip4.d(this.a).b(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public j(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ip4.d(this.a).c(charSequence);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<kp4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public k(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(kp4.c cVar) {
            this.a.b(cVar.a());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<v87<? extends Boolean, ? extends ServerError, ? extends qn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public l(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<Boolean, ? extends ServerError, qn4> v87) {
            if (v87.getFirst().booleanValue()) {
                qn4 third = v87.getThird();
                if (third == null) {
                    FragmentActivity activity = this.a.getActivity();
                    if (activity != null) {
                        activity.finish();
                        return;
                    }
                    return;
                }
                ip4 ip4 = this.a;
                ip4.b(ip4.a(third));
                return;
            }
            ServerError serverError = (ServerError) v87.getSecond();
            bx6 bx6 = bx6.c;
            String str = null;
            Integer code = serverError != null ? serverError.getCode() : null;
            if (serverError != null) {
                str = serverError.getMessage();
            }
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(code, str, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public m(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ee7.a((Object) bool, "it");
            if (bool.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public n(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = wy4.B;
                ee7.a((Object) flexibleTextInputLayout, "inputDes");
                flexibleTextInputLayout.setErrorEnabled(!bool.booleanValue());
                if (!bool.booleanValue()) {
                    FlexibleTextInputLayout flexibleTextInputLayout2 = wy4.B;
                    ee7.a((Object) flexibleTextInputLayout2, "inputDes");
                    FlexibleTextInputLayout flexibleTextInputLayout3 = wy4.B;
                    ee7.a((Object) flexibleTextInputLayout3, "inputDes");
                    flexibleTextInputLayout2.setError(ig5.a(flexibleTextInputLayout3.getContext(), 2131886219));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements zd<qn4> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public o(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0072  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0074  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0077  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x009f  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0139  */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onChanged(com.fossil.qn4 r10) {
            /*
                r9 = this;
                com.fossil.ip4 r0 = r9.a
                com.fossil.qw6 r0 = com.fossil.ip4.b(r0)
                java.lang.Object r0 = r0.a()
                com.fossil.wy4 r0 = (com.fossil.wy4) r0
                if (r0 == 0) goto L_0x01aa
                java.lang.String r1 = r10.f()
                int r2 = r1.hashCode()
                r3 = -314497661(0xffffffffed412583, float:-3.7359972E27)
                r4 = 2131886274(0x7f1200c2, float:1.9407122E38)
                java.lang.String r5 = "edtPrivacy"
                if (r2 == r3) goto L_0x003c
                r3 = -62011551(0xfffffffffc4dc761, float:-4.273859E36)
                if (r2 == r3) goto L_0x0026
                goto L_0x0055
            L_0x0026:
                java.lang.String r2 = "public_with_friend"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0055
                com.portfolio.platform.view.FlexibleTextInputEditText r1 = r0.y
                com.fossil.ee7.a(r1, r5)
                android.content.Context r1 = r1.getContext()
                java.lang.String r1 = com.fossil.ig5.a(r1, r4)
                goto L_0x0062
            L_0x003c:
                java.lang.String r2 = "private"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0055
                com.portfolio.platform.view.FlexibleTextInputEditText r1 = r0.y
                com.fossil.ee7.a(r1, r5)
                android.content.Context r1 = r1.getContext()
                r2 = 2131886275(0x7f1200c3, float:1.9407124E38)
                java.lang.String r1 = com.fossil.ig5.a(r1, r2)
                goto L_0x0062
            L_0x0055:
                com.portfolio.platform.view.FlexibleTextInputEditText r1 = r0.y
                com.fossil.ee7.a(r1, r5)
                android.content.Context r1 = r1.getContext()
                java.lang.String r1 = com.fossil.ig5.a(r1, r4)
            L_0x0062:
                com.portfolio.platform.view.FlexibleTextInputEditText r2 = r0.y
                r2.setText(r1)
                java.lang.String r1 = r10.e()
                int r1 = r1.length()
                r2 = 0
                if (r1 <= 0) goto L_0x0074
                r1 = 1
                goto L_0x0075
            L_0x0074:
                r1 = 0
            L_0x0075:
                if (r1 == 0) goto L_0x0080
                com.portfolio.platform.view.FlexibleTextInputEditText r1 = r0.x
                java.lang.String r3 = r10.e()
                r1.setText(r3)
            L_0x0080:
                com.portfolio.platform.view.FlexibleTextInputEditText r1 = r0.u
                java.lang.String r3 = r10.a()
                r1.setText(r3)
                java.lang.String r1 = r10.j()
                java.lang.String r3 = "activity_best_result"
                boolean r1 = com.fossil.ee7.a(r1, r3)
                java.lang.String r3 = "llStepGoal"
                r4 = 4
                java.lang.String r5 = "llDuration"
                java.lang.String r6 = "tvDurationStep"
                r7 = 2131099764(0x7f060074, float:1.781189E38)
                if (r1 == 0) goto L_0x0139
                com.portfolio.platform.view.FlexibleTextView r1 = r0.K
                com.fossil.ee7.a(r1, r6)
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                r8 = 2131886267(0x7f1200bb, float:1.9407108E38)
                java.lang.String r6 = com.fossil.ig5.a(r6, r8)
                r1.setText(r6)
                android.widget.LinearLayout r1 = r0.G
                com.fossil.ee7.a(r1, r5)
                r1.setVisibility(r2)
                android.widget.LinearLayout r1 = r0.H
                com.fossil.ee7.a(r1, r3)
                r1.setVisibility(r4)
                int r10 = r10.b()
                int r10 = r10 / 60
                int r1 = r10 / 60
                int r10 = r10 % 60
                com.portfolio.platform.view.FlexibleEditText r3 = r0.v
                java.lang.String r1 = java.lang.String.valueOf(r1)
                r3.setText(r1)
                com.portfolio.platform.view.FlexibleEditText r1 = r0.w
                java.lang.String r10 = java.lang.String.valueOf(r10)
                r1.setText(r10)
                com.fossil.ip4 r10 = r9.a
                com.fossil.qn4 r10 = r10.j
                if (r10 == 0) goto L_0x01aa
                com.portfolio.platform.view.FlexibleEditText r10 = r0.v
                java.lang.String r1 = "edtHour"
                com.fossil.ee7.a(r10, r1)
                r10.setEnabled(r2)
                com.portfolio.platform.view.FlexibleEditText r10 = r0.w
                java.lang.String r1 = "edtMin"
                com.fossil.ee7.a(r10, r1)
                r10.setEnabled(r2)
                com.portfolio.platform.view.FlexibleEditText r10 = r0.v
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                int r1 = com.fossil.v6.a(r1, r7)
                r10.setTextColor(r1)
                com.portfolio.platform.view.FlexibleEditText r10 = r0.w
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                int r1 = com.fossil.v6.a(r1, r7)
                r10.setTextColor(r1)
                com.portfolio.platform.view.FlexibleTextInputEditText r10 = r0.y
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                int r1 = com.fossil.v6.a(r1, r7)
                r10.setTextColor(r1)
                com.portfolio.platform.view.FlexibleTextView r10 = r0.K
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                int r0 = com.fossil.v6.a(r0, r7)
                r10.setTextColor(r0)
                goto L_0x01aa
            L_0x0139:
                com.portfolio.platform.view.FlexibleTextView r1 = r0.K
                com.fossil.ee7.a(r1, r6)
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                r8 = 2131886281(0x7f1200c9, float:1.9407136E38)
                java.lang.String r6 = com.fossil.ig5.a(r6, r8)
                r1.setText(r6)
                android.widget.LinearLayout r1 = r0.G
                com.fossil.ee7.a(r1, r5)
                r1.setVisibility(r4)
                android.widget.LinearLayout r1 = r0.H
                com.fossil.ee7.a(r1, r3)
                r1.setVisibility(r2)
                com.portfolio.platform.view.FlexibleEditText r1 = r0.z
                int r10 = r10.i()
                java.lang.String r10 = java.lang.String.valueOf(r10)
                r1.setText(r10)
                com.fossil.ip4 r10 = r9.a
                com.fossil.qn4 r10 = r10.j
                if (r10 == 0) goto L_0x01aa
                com.portfolio.platform.view.FlexibleEditText r10 = r0.z
                java.lang.String r1 = "edtStep"
                com.fossil.ee7.a(r10, r1)
                r10.setEnabled(r2)
                com.portfolio.platform.view.FlexibleEditText r10 = r0.z
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                int r1 = com.fossil.v6.a(r1, r7)
                r10.setTextColor(r1)
                com.portfolio.platform.view.FlexibleTextInputEditText r10 = r0.y
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                int r1 = com.fossil.v6.a(r1, r7)
                r10.setTextColor(r1)
                com.portfolio.platform.view.FlexibleTextView r10 = r0.K
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                int r0 = com.fossil.v6.a(r0, r7)
                r10.setTextColor(r0)
            L_0x01aa:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ip4.o.onChanged(com.fossil.qn4):void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements zd<List<dn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public p(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<dn4> list) {
            T t;
            String str;
            FlexibleTextInputEditText flexibleTextInputEditText;
            this.a.p.clear();
            List c = this.a.p;
            ee7.a((Object) list, "models");
            c.addAll(list);
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (t.c()) {
                    break;
                }
            }
            T t2 = t;
            if (t2 != null) {
                Object a2 = t2.a();
                if (ee7.a(a2, (Object) "private")) {
                    str = ig5.a(PortfolioApp.g0.c(), 2131886275);
                } else if (ee7.a(a2, (Object) "public_with_friend")) {
                    str = ig5.a(PortfolioApp.g0.c(), 2131886274);
                } else {
                    str = ig5.a(PortfolioApp.g0.c(), 2131886274);
                }
                wy4 wy4 = (wy4) ip4.b(this.a).a();
                if (wy4 != null && (flexibleTextInputEditText = wy4.y) != null) {
                    flexibleTextInputEditText.setText(str);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public q(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null && (flexibleEditText = wy4.v) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public r(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null && (flexibleEditText = wy4.w) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s<T> implements zd<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public s(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Integer num) {
            FlexibleEditText flexibleEditText;
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null && (flexibleEditText = wy4.z) != null) {
                flexibleEditText.setText(String.valueOf(num.intValue()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public t(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null) {
                FlexibleButton flexibleButton = wy4.r;
                ee7.a((Object) flexibleButton, "btnNext");
                ee7.a((Object) bool, "it");
                flexibleButton.setEnabled(bool.booleanValue());
                FlexibleButton flexibleButton2 = wy4.q;
                ee7.a((Object) flexibleButton2, "btnDone");
                flexibleButton2.setEnabled(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public u(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            int i;
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null) {
                FlexibleTextView flexibleTextView = wy4.A;
                ee7.a((Object) flexibleTextView, "ftvInputErrorDurationStep");
                ee7.a((Object) bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = wy4.A;
                    ee7.a((Object) flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = wy4.A;
                    ee7.a((Object) flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(ig5.a(flexibleTextView3.getContext(), 2131886264));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public v(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            int i;
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 != null) {
                FlexibleTextView flexibleTextView = wy4.A;
                ee7.a((Object) flexibleTextView, "ftvInputErrorDurationStep");
                ee7.a((Object) bool, "isValid");
                if (bool.booleanValue()) {
                    i = 8;
                } else {
                    FlexibleTextView flexibleTextView2 = wy4.A;
                    ee7.a((Object) flexibleTextView2, "ftvInputErrorDurationStep");
                    FlexibleTextView flexibleTextView3 = wy4.A;
                    ee7.a((Object) flexibleTextView3, "ftvInputErrorDurationStep");
                    flexibleTextView2.setText(ig5.a(flexibleTextView3.getContext(), 2131886271));
                    i = 0;
                }
                flexibleTextView.setVisibility(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w<T> implements zd<kp4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        public w(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(kp4.b bVar) {
            wy4 wy4 = (wy4) ip4.b(this.a).a();
            if (wy4 == null) {
                return;
            }
            if (bVar.b()) {
                FlexibleTextInputLayout flexibleTextInputLayout = wy4.C;
                ee7.a((Object) flexibleTextInputLayout, "inputName");
                flexibleTextInputLayout.setErrorEnabled(false);
                return;
            }
            FlexibleTextInputLayout flexibleTextInputLayout2 = wy4.C;
            ee7.a((Object) flexibleTextInputLayout2, "inputName");
            flexibleTextInputLayout2.setErrorEnabled(true);
            if (bVar.a() < 1) {
                FlexibleTextInputLayout flexibleTextInputLayout3 = wy4.C;
                ee7.a((Object) flexibleTextInputLayout3, "inputName");
                FlexibleTextView flexibleTextView = wy4.A;
                ee7.a((Object) flexibleTextView, "ftvInputErrorDurationStep");
                flexibleTextInputLayout3.setError(ig5.a(flexibleTextView.getContext(), 2131886243));
            } else if (bVar.a() > 32) {
                FlexibleTextInputLayout flexibleTextInputLayout4 = wy4.C;
                ee7.a((Object) flexibleTextInputLayout4, "inputName");
                FlexibleTextView flexibleTextView2 = wy4.A;
                ee7.a((Object) flexibleTextView2, "ftvInputErrorDurationStep");
                flexibleTextInputLayout4.setError(ig5.a(flexibleTextView2.getContext(), 2131886254));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x implements er5 {
        @DexIgnore
        public /* final */ /* synthetic */ ip4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public x(ip4 ip4) {
            this.a = ip4;
        }

        @DexIgnore
        @Override // com.fossil.er5
        public void a(dn4 dn4) {
            ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ip4.d(this.a).a(dn4);
        }
    }

    /*
    static {
        String simpleName = ip4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCCreateChallengeInputFr\u2026nt::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 b(ip4 ip4) {
        qw6<wy4> qw6 = ip4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ kp4 d(ip4 ip4) {
        kp4 kp4 = ip4.h;
        if (kp4 != null) {
            return kp4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<wy4> qw6 = this.g;
        if (qw6 != null) {
            wy4 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                FlexibleButton flexibleButton = a2.r;
                ee7.a((Object) flexibleButton, "btnNext");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.q;
                ee7.a((Object) flexibleButton2, "btnDone");
                flexibleButton2.setVisibility(8);
                FlexibleTextInputLayout flexibleTextInputLayout = a2.D;
                ee7.a((Object) flexibleTextInputLayout, "inputPrivacy");
                flexibleTextInputLayout.setEnabled(true);
                return;
            }
            FlexibleButton flexibleButton3 = a2.r;
            ee7.a((Object) flexibleButton3, "btnNext");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.q;
            ee7.a((Object) flexibleButton4, "btnDone");
            flexibleButton4.setVisibility(0);
            FlexibleTextInputLayout flexibleTextInputLayout2 = a2.D;
            ee7.a((Object) flexibleTextInputLayout2, "inputPrivacy");
            flexibleTextInputLayout2.setEnabled(false);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        String str;
        sn4 sn4 = this.i;
        if (sn4 == null || (str = sn4.g()) == null) {
            qn4 qn4 = this.j;
            str = qn4 != null ? qn4.j() : null;
        }
        String str2 = ee7.a(str, "activity_best_result") ? "bc_abr_input" : "bc_arg_input";
        qd5 c2 = qd5.f.c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a(str2, activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final void g1() {
        qw6<wy4> qw6 = this.g;
        if (qw6 != null) {
            wy4 a2 = qw6.a();
            if (a2 != null) {
                a2.E.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                a2.y.setOnClickListener(new e(this));
                a2.x.addTextChangedListener(new f(this));
                a2.u.addTextChangedListener(new g(this));
                a2.v.addTextChangedListener(new h(this));
                a2.w.addTextChangedListener(new i(this));
                a2.z.addTextChangedListener(new j(this));
                ImageView imageView = a2.F;
                ee7.a((Object) imageView, "ivThumbnail");
                Context context = imageView.getContext();
                sn4 sn4 = this.i;
                imageView.setImageDrawable(v6.c(context, sn4 != null ? sn4.f() : 2131231010));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        kp4 kp4 = this.h;
        if (kp4 != null) {
            kp4.f().a(getViewLifecycleOwner(), new o(this));
            kp4 kp42 = this.h;
            if (kp42 != null) {
                kp42.o().a(getViewLifecycleOwner(), new p(this));
                kp4 kp43 = this.h;
                if (kp43 != null) {
                    kp43.h().a(getViewLifecycleOwner(), new q(this));
                    kp4 kp44 = this.h;
                    if (kp44 != null) {
                        kp44.k().a(getViewLifecycleOwner(), new r(this));
                        kp4 kp45 = this.h;
                        if (kp45 != null) {
                            kp45.p().a(getViewLifecycleOwner(), new s(this));
                            kp4 kp46 = this.h;
                            if (kp46 != null) {
                                kp46.n().a(getViewLifecycleOwner(), new t(this));
                                kp4 kp47 = this.h;
                                if (kp47 != null) {
                                    kp47.r().a(getViewLifecycleOwner(), new u(this));
                                    kp4 kp48 = this.h;
                                    if (kp48 != null) {
                                        kp48.q().a(getViewLifecycleOwner(), new v(this));
                                        kp4 kp49 = this.h;
                                        if (kp49 != null) {
                                            kp49.l().a(getViewLifecycleOwner(), new w(this));
                                            kp4 kp410 = this.h;
                                            if (kp410 != null) {
                                                kp410.m().a(getViewLifecycleOwner(), new k(this));
                                                kp4 kp411 = this.h;
                                                if (kp411 != null) {
                                                    kp411.g().a(getViewLifecycleOwner(), new l(this));
                                                    kp4 kp412 = this.h;
                                                    if (kp412 != null) {
                                                        kp412.i().a(getViewLifecycleOwner(), new m(this));
                                                        kp4 kp413 = this.h;
                                                        if (kp413 != null) {
                                                            kp413.e().a(getViewLifecycleOwner(), new n(this));
                                                        } else {
                                                            ee7.d("viewModel");
                                                            throw null;
                                                        }
                                                    } else {
                                                        ee7.d("viewModel");
                                                        throw null;
                                                    }
                                                } else {
                                                    ee7.d("viewModel");
                                                    throw null;
                                                }
                                            } else {
                                                ee7.d("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            ee7.d("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        ee7.d("viewModel");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("viewModel");
                                    throw null;
                                }
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void i1() {
        bn4 b2 = bn4.z.b();
        String a2 = ig5.a(requireContext(), 2131886213);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026nge_List__PrivacySetting)");
        b2.setTitle(a2);
        b2.x(this.p);
        b2.a(new x(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, bn4.z.a());
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == 11 && i3 == -1) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(i3);
            }
            FragmentActivity activity2 = getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.setResult(i3, intent);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().g().a(this);
        rj4 rj4 = this.f;
        qn4 qn4 = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(kp4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026putViewModel::class.java)");
            this.h = (kp4) a2;
            Bundle arguments = getArguments();
            this.i = arguments != null ? (sn4) arguments.getParcelable("challenge_template_extra") : null;
            Bundle arguments2 = getArguments();
            if (arguments2 != null) {
                qn4 = (qn4) arguments2.getParcelable("challenge_draft_extra");
            }
            this.j = qn4;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        wy4 wy4 = (wy4) qb.a(layoutInflater, 2131558524, viewGroup, false, a1());
        this.g = new qw6<>(this, wy4);
        ee7.a((Object) wy4, "binding");
        return wy4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        f1();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qn4 qn4 = this.j;
        if (qn4 != null) {
            kp4 kp4 = this.h;
            if (kp4 == null) {
                ee7.d("viewModel");
                throw null;
            } else if (qn4 != null) {
                kp4.b(qn4);
                T(false);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            T(true);
            kp4 kp42 = this.h;
            if (kp42 != null) {
                kp42.a(this.i);
            } else {
                ee7.d("viewModel");
                throw null;
            }
        }
        g1();
        h1();
    }

    @DexIgnore
    public final void b(qn4 qn4) {
        BCInviteFriendActivity.y.b(this, qn4);
    }

    @DexIgnore
    public final Intent a(qn4 qn4) {
        Intent intent = new Intent();
        intent.putExtra("challenge_name_extra", qn4.e());
        intent.putExtra("challenge_des_extra", qn4.a());
        intent.putExtra("challenge_target_extra", qn4.i());
        intent.putExtra("challenge_duration_extra", qn4.b());
        return intent;
    }

    @DexIgnore
    public final void b(Intent intent) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }
}
