package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.stetho.dumpapp.Framer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w80 extends n80 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b(null);
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public enum a {
        OFF((byte) 0),
        LOW(Framer.STDERR_FRAME_PREFIX),
        MEDIUM((byte) 75),
        HIGH((byte) 100);
        
        @DexIgnore
        public static /* final */ C0229a c; // = new C0229a(null);
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.w80$a$a")
        /* renamed from: com.fossil.w80$a$a  reason: collision with other inner class name */
        public static final class C0229a {
            @DexIgnore
            public /* synthetic */ C0229a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(byte b) {
                boolean z = false;
                if (true == (b <= 25)) {
                    return a.OFF;
                }
                if (true == (b <= 50)) {
                    return a.LOW;
                }
                if (b <= 75) {
                    z = true;
                }
                if (true == z) {
                    return a.MEDIUM;
                }
                return a.HIGH;
            }
        }

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<w80> {
        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
        }

        @DexIgnore
        public final w80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new w80(a.c.a(bArr[0]));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", require: 1"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public w80 createFromParcel(Parcel parcel) {
            return new w80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public w80[] newArray(int i) {
            return new w80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public w80 m83createFromParcel(Parcel parcel) {
            return new w80(parcel, null);
        }
    }

    @DexIgnore
    public w80(a aVar) {
        super(o80.VIBE_STRENGTH);
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(this.b.a()).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(w80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((w80) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
    }

    @DexIgnore
    public final a getVibeStrengthLevel() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return this.b.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public String d() {
        return yz0.a(this.b);
    }

    @DexIgnore
    public /* synthetic */ w80(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            this.b = a.valueOf(readString);
            return;
        }
        ee7.a();
        throw null;
    }
}
