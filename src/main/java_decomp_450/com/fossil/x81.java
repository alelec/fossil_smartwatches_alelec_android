package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum x81 {
    SEND_PHONE_RANDOM_NUMBER(c51.SET, a71.SEND_PHONE_RANDOM_NUMBER, null, null, 12),
    SEND_BOTH_SIDES_RANDOM_NUMBERS(c51.SET, a71.SEND_BOTH_RANDOM_NUMBER, null, null, 12),
    EXCHANGE_PUBLIC_KEYS(c51.SET, a71.EXCHANGE_PUBLIC_KEY, null, null, 12),
    PROCESS_USER_AUTHORIZATION(c51.GET, a71.PROCESS_USER_AUTHORIZATION, null, null, 12),
    STOP_PROCESS(c51.SET, a71.STOP_PROCESS, null, null, 12);
    
    @DexIgnore
    public /* final */ c51 a;
    @DexIgnore
    public /* final */ a71 b;
    @DexIgnore
    public /* final */ c51 c;
    @DexIgnore
    public /* final */ a71 d;

    @DexIgnore
    public /* synthetic */ x81(c51 c51, a71 a71, c51 c512, a71 a712, int i) {
        c512 = (i & 4) != 0 ? c51.RESPONSE : c512;
        a712 = (i & 8) != 0 ? a71 : a712;
        this.a = c51;
        this.b = a71;
        this.c = c512;
        this.d = a712;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] array = ByteBuffer.allocate(2).put(this.a.a).put(this.b.a).array();
        ee7.a((Object) array, "ByteBuffer.allocate(2)\n \u2026\n                .array()");
        return array;
    }
}
