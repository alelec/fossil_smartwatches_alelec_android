package com.fossil;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp5 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public List<r87<MicroApp, String>> a;
    @DexIgnore
    public List<r87<MicroApp, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public e d;
    @DexIgnore
    public d e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gp5 gp5, View view) {
            super(view);
            ee7.b(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends a {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gp5 gp5, View view) {
            super(gp5, view);
            ee7.b(view, "itemView");
            View findViewById = view.findViewById(2131363317);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(MicroApp microApp);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends a {
        @DexIgnore
        public /* final */ CustomizeWidget a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public MicroApp d;
        @DexIgnore
        public /* final */ ConstraintLayout e;
        @DexIgnore
        public /* final */ /* synthetic */ gp5 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                MicroApp a2 = this.a.a();
                if (a2 != null) {
                    e a3 = this.a.f.d;
                    if (a3 != null) {
                        a3.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(gp5.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(gp5.f, "itemClick(), MicroApp tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(gp5 gp5, View view) {
            super(gp5, view);
            ee7.b(view, "itemView");
            this.f = gp5;
            View findViewById = view.findViewById(2131363460);
            ee7.a((Object) findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363304);
            ee7.a((Object) findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            this.c = (FlexibleTextView) view.findViewById(2131363201);
            View findViewById3 = view.findViewById(2131362962);
            ee7.a((Object) findViewById3, "itemView.findViewById(R.id.root_background)");
            this.e = (ConstraintLayout) findViewById3;
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                this.e.setBackgroundColor(Color.parseColor(b2));
            }
            view.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final MicroApp a() {
            return this.d;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.a;
        }

        @DexIgnore
        public final void a(MicroApp microApp) {
            this.d = microApp;
        }
    }

    /*
    static {
        new b(null);
        String name = gp5.class.getName();
        ee7.a((Object) name, "SearchMicroAppAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    public final void b(List<r87<MicroApp, String>> list) {
        d dVar;
        this.a = list;
        if (!(list == null || !list.isEmpty() || (dVar = this.e) == null)) {
            dVar.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<r87<MicroApp, String>> list = this.a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return (this.a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final void a(List<r87<MicroApp, String>> list) {
        ee7.b(list, "value");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558669, viewGroup, false);
            ee7.a((Object) inflate, "view");
            return new c(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558668, viewGroup, false);
        ee7.a((Object) inflate2, "view");
        return new f(this, inflate2);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        if (!(aVar instanceof c)) {
            f fVar = (f) aVar;
            List<r87<MicroApp, String>> list = this.a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty(this.b.get(i2).getSecond())) {
                        FlexibleTextView b2 = fVar.b();
                        ee7.a((Object) b2, "resultSearchViewHolder.tvAssignedTo");
                        b2.setVisibility(0);
                        FlexibleTextView b3 = fVar.b();
                        ee7.a((Object) b3, "resultSearchViewHolder.tvAssignedTo");
                        b3.setText(ig5.a(PortfolioApp.g0.c(), 2131886510));
                    } else {
                        FlexibleTextView b4 = fVar.b();
                        ee7.a((Object) b4, "resultSearchViewHolder.tvAssignedTo");
                        b4.setVisibility(8);
                    }
                    fVar.d().c(this.b.get(i2).getFirst().getId());
                    fVar.c().setText(ig5.a(PortfolioApp.g0.c(), this.b.get(i2).getFirst().getNameKey(), this.b.get(i2).getFirst().getName()));
                    fVar.a(this.b.get(i2).getFirst());
                    return;
                }
                fVar.a(null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty(list.get(i).getSecond())) {
                    FlexibleTextView b5 = fVar.b();
                    ee7.a((Object) b5, "resultSearchViewHolder.tvAssignedTo");
                    b5.setVisibility(0);
                    FlexibleTextView b6 = fVar.b();
                    ee7.a((Object) b6, "resultSearchViewHolder.tvAssignedTo");
                    b6.setText(ig5.a(PortfolioApp.g0.c(), 2131886510));
                } else {
                    FlexibleTextView b7 = fVar.b();
                    ee7.a((Object) b7, "resultSearchViewHolder.tvAssignedTo");
                    b7.setVisibility(8);
                }
                fVar.d().c(list.get(i).getFirst().getId());
                String a2 = ig5.a(PortfolioApp.g0.c(), list.get(i).getFirst().getNameKey(), list.get(i).getFirst().getName());
                FlexibleTextView c2 = fVar.c();
                ee7.a((Object) a2, "name");
                c2.setText(a(a2, this.c));
                fVar.a(list.get(i).getFirst());
            } else {
                fVar.a(null);
            }
        } else if (this.b.isEmpty()) {
            ((c) aVar).a().setVisibility(4);
        } else {
            ((c) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    public final SpannableString a(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                ah7 ah7 = new ah7(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    hg7<yg7> findAll$default = ah7.findAll$default(ah7, lowerCase, 0, 2, null);
                    SpannableString spannableString = new SpannableString(str);
                    for (yg7 yg7 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), yg7.a().c().intValue(), yg7.a().c().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    public final void a(e eVar) {
        ee7.b(eVar, "listener");
        this.d = eVar;
    }

    @DexIgnore
    public final void a(d dVar) {
        ee7.b(dVar, "listener");
        this.e = dVar;
    }
}
