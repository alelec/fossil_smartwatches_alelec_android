package com.fossil;

import android.os.Bundle;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m32 {
    @DexIgnore
    <A extends v02.b, T extends r12<? extends i12, A>> T a(T t);

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(i02 i02, v02<?> v02, boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t);

    @DexIgnore
    void b();

    @DexIgnore
    void b(Bundle bundle);

    @DexIgnore
    void c();
}
