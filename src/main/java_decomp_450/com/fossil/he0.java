package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he0<V> extends ie0<V> {
    @DexIgnore
    public he0<V> f(gd7<? super i97, i97> gd7) {
        if (a()) {
            try {
                gd7.invoke(i97.a);
            } catch (Exception e) {
                wl0.h.a(e);
            }
        } else {
            ((q60) this).f.add(gd7);
        }
        return this;
    }

    @DexIgnore
    public he0<V> g(gd7<? super Float, i97> gd7) {
        if (!a()) {
            ((q60) this).g.add(gd7);
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ie0
    /* renamed from: d */
    public he0<V> b(gd7<? super je0, i97> gd7) {
        ie0<V> d = super.b(gd7);
        if (d != null) {
            return (he0) d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    @Override // com.fossil.ie0
    public he0<V> e(gd7<? super V, i97> gd7) {
        ie0<V> e = super.c((gd7) gd7);
        if (e != null) {
            return (he0) e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }

    @DexIgnore
    @Override // com.fossil.ie0, com.fossil.ie0, com.fossil.q60
    public he0<V> a(gd7<? super je0, i97> gd7) {
        ie0<V> a = super.a(gd7);
        if (a != null) {
            return (he0) a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.task.ProgressTask<V>");
    }
}
