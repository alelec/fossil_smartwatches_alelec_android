package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class la5 extends ka5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362374, 1);
        E.put(2131362373, 2);
        E.put(2131362751, 3);
        E.put(2131362051, 4);
        E.put(2131363277, 5);
        E.put(2131363278, 6);
        E.put(2131363301, 7);
        E.put(2131363303, 8);
        E.put(2131362369, 9);
        E.put(2131362401, 10);
        E.put(2131362434, 11);
    }
    */

    @DexIgnore
    public la5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public la5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[1], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[11], (View) objArr[3], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[8]);
        this.C = -1;
        ((ka5) this).r.setTag(null);
        a(view);
        f();
    }
}
