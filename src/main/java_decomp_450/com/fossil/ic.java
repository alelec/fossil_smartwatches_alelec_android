package com.fossil;

import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic extends he {
    @DexIgnore
    public static /* final */ ViewModelProvider.Factory g; // = new a();
    @DexIgnore
    public /* final */ HashMap<String, Fragment> a; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, ic> b; // = new HashMap<>();
    @DexIgnore
    public /* final */ HashMap<String, ViewModelStore> c; // = new HashMap<>();
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ViewModelProvider.Factory {
        @DexIgnore
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends he> T create(Class<T> cls) {
            return new ic(true);
        }
    }

    @DexIgnore
    public ic(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public static ic a(ViewModelStore viewModelStore) {
        return (ic) new ViewModelProvider(viewModelStore, g).a(ic.class);
    }

    @DexIgnore
    public boolean b() {
        return this.e;
    }

    @DexIgnore
    public ic c(Fragment fragment) {
        ic icVar = this.b.get(fragment.mWho);
        if (icVar != null) {
            return icVar;
        }
        ic icVar2 = new ic(this.d);
        this.b.put(fragment.mWho, icVar2);
        return icVar2;
    }

    @DexIgnore
    public ViewModelStore d(Fragment fragment) {
        ViewModelStore viewModelStore = this.c.get(fragment.mWho);
        if (viewModelStore != null) {
            return viewModelStore;
        }
        ViewModelStore viewModelStore2 = new ViewModelStore();
        this.c.put(fragment.mWho, viewModelStore2);
        return viewModelStore2;
    }

    @DexIgnore
    public boolean e(Fragment fragment) {
        return this.a.remove(fragment.mWho) != null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || ic.class != obj.getClass()) {
            return false;
        }
        ic icVar = (ic) obj;
        if (!this.a.equals(icVar.a) || !this.b.equals(icVar.b) || !this.c.equals(icVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean f(Fragment fragment) {
        if (!this.a.containsKey(fragment.mWho)) {
            return true;
        }
        if (this.d) {
            return this.e;
        }
        return !this.f;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "onCleared called for " + this);
        }
        this.e = true;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it = this.a.values().iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it2 = this.b.keySet().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it3 = this.c.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }

    @DexIgnore
    public void b(Fragment fragment) {
        if (FragmentManager.d(3)) {
            Log.d("FragmentManager", "Clearing non-config state for " + fragment);
        }
        ic icVar = this.b.get(fragment.mWho);
        if (icVar != null) {
            icVar.onCleared();
            this.b.remove(fragment.mWho);
        }
        ViewModelStore viewModelStore = this.c.get(fragment.mWho);
        if (viewModelStore != null) {
            viewModelStore.a();
            this.c.remove(fragment.mWho);
        }
    }

    @DexIgnore
    public boolean a(Fragment fragment) {
        if (this.a.containsKey(fragment.mWho)) {
            return false;
        }
        this.a.put(fragment.mWho, fragment);
        return true;
    }

    @DexIgnore
    public Fragment a(String str) {
        return this.a.get(str);
    }

    @DexIgnore
    public Collection<Fragment> a() {
        return this.a.values();
    }
}
