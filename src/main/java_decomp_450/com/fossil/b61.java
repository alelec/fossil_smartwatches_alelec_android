package com.fossil;

import com.fossil.p80;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class b61 extends ce7 implements gd7<byte[], p80> {
    @DexIgnore
    public b61(p80.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final uf7 getOwner() {
        return te7.a(p80.a.class);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DisplayUnitConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public p80 invoke(byte[] bArr) {
        return ((p80.a) ((vd7) this).receiver).a(bArr);
    }
}
