package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik4 implements Factory<FirmwareFileRepository> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public ik4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static ik4 a(wj4 wj4) {
        return new ik4(wj4);
    }

    @DexIgnore
    public static FirmwareFileRepository b(wj4 wj4) {
        FirmwareFileRepository h = wj4.h();
        c87.a(h, "Cannot return null from a non-@Nullable @Provides method");
        return h;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public FirmwareFileRepository get() {
        return b(this.a);
    }
}
