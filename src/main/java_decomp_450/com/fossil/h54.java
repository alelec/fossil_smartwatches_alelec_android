package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h54 extends v54.d.a.b {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    @Override // com.fossil.v54.d.a.b
    public String a() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof v54.d.a.b) {
            return this.a.equals(((v54.d.a.b) obj).a());
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode() ^ 1000003;
    }

    @DexIgnore
    public String toString() {
        return "Organization{clsId=" + this.a + "}";
    }
}
