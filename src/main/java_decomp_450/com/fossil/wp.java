package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wp implements vp {
    @DexIgnore
    public /* final */ lp a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Executor c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Executor {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            wp.this.b(runnable);
        }
    }

    @DexIgnore
    public wp(Executor executor) {
        this.a = new lp(executor);
    }

    @DexIgnore
    @Override // com.fossil.vp
    public Executor a() {
        return this.c;
    }

    @DexIgnore
    public void b(Runnable runnable) {
        this.b.post(runnable);
    }

    @DexIgnore
    @Override // com.fossil.vp
    public void a(Runnable runnable) {
        this.a.execute(runnable);
    }

    @DexIgnore
    @Override // com.fossil.vp
    public lp b() {
        return this.a;
    }
}
