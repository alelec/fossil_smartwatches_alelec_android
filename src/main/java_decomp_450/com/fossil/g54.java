package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g54 extends v54.d.a {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ v54.d.a.b d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.a.AbstractC0205a {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public v54.d.a.b d;
        @DexIgnore
        public String e;

        @DexIgnore
        @Override // com.fossil.v54.d.a.AbstractC0205a
        public v54.d.a.AbstractC0205a a(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.a.AbstractC0205a
        public v54.d.a.AbstractC0205a b(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null identifier");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.a.AbstractC0205a
        public v54.d.a.AbstractC0205a c(String str) {
            this.e = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.a.AbstractC0205a
        public v54.d.a.AbstractC0205a d(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null version");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.a.AbstractC0205a
        public v54.d.a a() {
            String str = "";
            if (this.a == null) {
                str = str + " identifier";
            }
            if (this.b == null) {
                str = str + " version";
            }
            if (str.isEmpty()) {
                return new g54(this.a, this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.a
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.a
    public String b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.a
    public String c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.a
    public v54.d.a.b d() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.a
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        v54.d.a.b bVar;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.a)) {
            return false;
        }
        v54.d.a aVar = (v54.d.a) obj;
        if (this.a.equals(aVar.b()) && this.b.equals(aVar.e()) && ((str = this.c) != null ? str.equals(aVar.a()) : aVar.a() == null) && ((bVar = this.d) != null ? bVar.equals(aVar.d()) : aVar.d() == null)) {
            String str2 = this.e;
            if (str2 == null) {
                if (aVar.c() == null) {
                    return true;
                }
            } else if (str2.equals(aVar.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003;
        String str = this.c;
        int i = 0;
        int hashCode2 = (hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003;
        v54.d.a.b bVar = this.d;
        int hashCode3 = (hashCode2 ^ (bVar == null ? 0 : bVar.hashCode())) * 1000003;
        String str2 = this.e;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode3 ^ i;
    }

    @DexIgnore
    public String toString() {
        return "Application{identifier=" + this.a + ", version=" + this.b + ", displayVersion=" + this.c + ", organization=" + this.d + ", installationUuid=" + this.e + "}";
    }

    @DexIgnore
    public g54(String str, String str2, String str3, v54.d.a.b bVar, String str4) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = bVar;
        this.e = str4;
    }
}
