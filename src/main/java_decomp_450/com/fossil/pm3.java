package com.fossil;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm3 {
    @DexIgnore
    public /* final */ oh3 a;

    @DexIgnore
    public pm3(oh3 oh3) {
        this.a = oh3;
    }

    @DexIgnore
    public final void a() {
        this.a.c().g();
        if (d()) {
            if (c()) {
                this.a.p().A.a(null);
                Bundle bundle = new Bundle();
                bundle.putString("source", "(not set)");
                bundle.putString("medium", "(not set)");
                bundle.putString("_cis", "intent");
                bundle.putLong("_cc", 1);
                this.a.u().a("auto", "_cmpx", bundle);
            } else {
                String a2 = this.a.p().A.a();
                if (TextUtils.isEmpty(a2)) {
                    this.a.e().u().a("Cache still valid but referrer not found");
                } else {
                    long a3 = ((this.a.p().B.a() / 3600000) - 1) * 3600000;
                    Uri parse = Uri.parse(a2);
                    Bundle bundle2 = new Bundle();
                    Pair pair = new Pair(parse.getPath(), bundle2);
                    for (String str : parse.getQueryParameterNames()) {
                        bundle2.putString(str, parse.getQueryParameter(str));
                    }
                    ((Bundle) pair.second).putLong("_cc", a3);
                    this.a.u().a((String) pair.first, "_cmp", (Bundle) pair.second);
                }
                this.a.p().A.a(null);
            }
            this.a.p().B.a(0);
        }
    }

    @DexIgnore
    public final void b() {
        if (d() && c()) {
            this.a.p().A.a(null);
        }
    }

    @DexIgnore
    public final boolean c() {
        if (d() && this.a.zzm().b() - this.a.p().B.a() > this.a.o().a((String) null, wb3.S0)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean d() {
        return this.a.p().B.a() > 0;
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        String str2;
        this.a.c().g();
        if (!this.a.g()) {
            if (bundle == null || bundle.isEmpty()) {
                str2 = null;
            } else {
                if (str == null || str.isEmpty()) {
                    str = "auto";
                }
                Uri.Builder builder = new Uri.Builder();
                builder.path(str);
                for (String str3 : bundle.keySet()) {
                    builder.appendQueryParameter(str3, bundle.getString(str3));
                }
                str2 = builder.build().toString();
            }
            if (!TextUtils.isEmpty(str2)) {
                this.a.p().A.a(str2);
                this.a.p().B.a(this.a.zzm().b());
            }
        }
    }
}
