package com.fossil;

import com.fossil.dz3;
import com.fossil.ez3;
import com.fossil.iy3;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rz3<E> extends hy3<E> {
    @DexIgnore
    public static /* final */ rz3<Object> EMPTY; // = new rz3<>(zx3.of());
    @DexIgnore
    public /* final */ transient ez3.e<E>[] c;
    @DexIgnore
    public /* final */ transient ez3.e<E>[] d;
    @DexIgnore
    public /* final */ transient int e;
    @DexIgnore
    public /* final */ transient int f;
    @DexIgnore
    @LazyInit
    public transient iy3<E> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends iy3.b<E> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean contains(Object obj) {
            return rz3.this.contains(obj);
        }

        @DexIgnore
        @Override // com.fossil.iy3.b
        public E get(int i) {
            return (E) rz3.this.c[i].getElement();
        }

        @DexIgnore
        @Override // com.fossil.vx3
        public boolean isPartialView() {
            return true;
        }

        @DexIgnore
        public int size() {
            return rz3.this.c.length;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<E> extends ez3.e<E> {
        @DexIgnore
        public /* final */ ez3.e<E> nextInBucket;

        @DexIgnore
        public c(E e, int i, ez3.e<E> eVar) {
            super(e, i);
            this.nextInBucket = eVar;
        }

        @DexIgnore
        @Override // com.fossil.ez3.e
        public ez3.e<E> nextInBucket() {
            return this.nextInBucket;
        }
    }

    @DexIgnore
    public rz3(Collection<? extends dz3.a<? extends E>> collection) {
        ez3.e<E> eVar;
        int size = collection.size();
        ez3.e<E>[] eVarArr = new ez3.e[size];
        if (size == 0) {
            this.c = eVarArr;
            this.d = null;
            this.e = 0;
            this.f = 0;
            this.g = iy3.of();
            return;
        }
        int a2 = sx3.a(size, 1.0d);
        int i = a2 - 1;
        ez3.e<E>[] eVarArr2 = new ez3.e[a2];
        long j = 0;
        Iterator<? extends dz3.a<? extends E>> it = collection.iterator();
        int i2 = 0;
        int i3 = 0;
        while (it.hasNext()) {
            dz3.a aVar = (dz3.a) it.next();
            Object element = aVar.getElement();
            jw3.a(element);
            int count = aVar.getCount();
            int hashCode = element.hashCode();
            int a3 = sx3.a(hashCode) & i;
            ez3.e<E> eVar2 = eVarArr2[a3];
            if (eVar2 == null) {
                eVar = (aVar instanceof ez3.e) && !(aVar instanceof c) ? (ez3.e) aVar : new ez3.e<>(element, count);
            } else {
                eVar = new c<>(element, count, eVar2);
            }
            i2 += hashCode ^ count;
            eVarArr[i3] = eVar;
            eVarArr2[a3] = eVar;
            j += (long) count;
            i3++;
        }
        this.c = eVarArr;
        this.d = eVarArr2;
        this.e = x04.a(j);
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.dz3
    public int count(Object obj) {
        ez3.e<E>[] eVarArr = this.d;
        if (!(obj == null || eVarArr == null)) {
            for (ez3.e<E> eVar = eVarArr[sx3.a(obj) & (eVarArr.length - 1)]; eVar != null; eVar = eVar.nextInBucket()) {
                if (gw3.a(obj, eVar.getElement())) {
                    return eVar.getCount();
                }
            }
        }
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.hy3
    public dz3.a<E> getEntry(int i) {
        return this.c[i];
    }

    @DexIgnore
    @Override // com.fossil.hy3
    public int hashCode() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.dz3
    public iy3<E> elementSet() {
        iy3<E> iy3 = this.g;
        if (iy3 != null) {
            return iy3;
        }
        b bVar = new b();
        this.g = bVar;
        return bVar;
    }
}
