package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb6 extends go5 implements mb6, RecyclerViewCalendar.b {
    @DexIgnore
    public xz6 f;
    @DexIgnore
    public qw6<q15> g;
    @DexIgnore
    public lb6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ nb6 a;

        @DexIgnore
        public b(nb6 nb6, q15 q15) {
            this.a = nb6;
        }

        @DexIgnore
        public final void onClick(View view) {
            nb6.a(this.a).a().a((Integer) 2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ nb6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(nb6 nb6) {
            this.a = nb6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.c
        public void a(Calendar calendar) {
            ee7.b(calendar, "calendar");
            lb6 b = this.a.h;
            if (b != null) {
                Date time = calendar.getTime();
                ee7.a((Object) time, "calendar.time");
                b.a(time);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ xz6 a(nb6 nb6) {
        xz6 xz6 = nb6.f;
        if (xz6 != null) {
            return xz6;
        }
        ee7.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "GoalTrackingOverviewMonthFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        q15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        qw6<q15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.b("hybridGoalTrackingTab");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        q15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onCreateView");
        q15 q15 = (q15) qb.a(layoutInflater, 2131558560, viewGroup, false, a1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            he a3 = je.a(activity).a(xz6.class);
            ee7.a((Object) a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = (xz6) a3;
            q15.s.setOnClickListener(new b(this, q15));
        }
        RecyclerViewCalendar recyclerViewCalendar = q15.q;
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        q15.q.setOnCalendarMonthChanged(new c(this));
        q15.q.setOnCalendarItemClickListener(this);
        this.g = new qw6<>(this, q15);
        f1();
        qw6<q15> qw6 = this.g;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onResume");
        f1();
        lb6 lb6 = this.h;
        if (lb6 != null) {
            lb6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onStop");
        lb6 lb6 = this.h;
        if (lb6 != null) {
            lb6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.mb6
    public void b(boolean z) {
        q15 a2;
        qw6<q15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                RecyclerViewCalendar recyclerViewCalendar = a2.q;
                ee7.a((Object) recyclerViewCalendar, "binding.calendarMonth");
                recyclerViewCalendar.setVisibility(4);
                ConstraintLayout constraintLayout = a2.r;
                ee7.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            RecyclerViewCalendar recyclerViewCalendar2 = a2.q;
            ee7.a((Object) recyclerViewCalendar2, "binding.calendarMonth");
            recyclerViewCalendar2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.r;
            ee7.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.recyclerview.RecyclerViewCalendar.b
    public void a(int i2, Calendar calendar) {
        ee7.b(calendar, "calendar");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i2 + ", calendar=" + calendar);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.A;
            Date time = calendar.getTime();
            ee7.a((Object) time, "it.time");
            ee7.a((Object) activity, Constants.ACTIVITY);
            aVar.a(time, activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.mb6
    public void a(TreeMap<Long, Float> treeMap) {
        q15 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ee7.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        qw6<q15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    @Override // com.fossil.mb6
    public void a(Date date, Date date2) {
        q15 a2;
        ee7.b(date, "selectDate");
        ee7.b(date2, GoalPhase.COLUMN_START_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        qw6<q15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            ee7.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            ee7.a((Object) instance2, "startCalendar");
            instance2.setTime(zd5.q(date2));
            ee7.a((Object) instance3, "endCalendar");
            instance3.setTime(zd5.l(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(lb6 lb6) {
        ee7.b(lb6, "presenter");
        this.h = lb6;
    }
}
