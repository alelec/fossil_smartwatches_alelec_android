package com.fossil;

import com.fossil.fo7;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp7 implements ip7 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ fp7 b;
    @DexIgnore
    public /* final */ ar7 c;
    @DexIgnore
    public /* final */ zq7 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements sr7 {
        @DexIgnore
        public /* final */ er7 a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public long c;

        @DexIgnore
        public b() {
            this.a = new er7(rp7.this.c.d());
            this.c = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            rp7 rp7 = rp7.this;
            int i = rp7.e;
            if (i != 6) {
                if (i == 5) {
                    rp7.a(this.a);
                    rp7 rp72 = rp7.this;
                    rp72.e = 6;
                    fp7 fp7 = rp72.b;
                    if (fp7 != null) {
                        fp7.a(!z, rp72, this.c, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + rp7.this.e);
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            try {
                long b2 = rp7.this.c.b(yq7, j);
                if (b2 > 0) {
                    this.c += b2;
                }
                return b2;
            } catch (IOException e) {
                a(false, e);
                throw e;
            }
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public tr7 d() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements qr7 {
        @DexIgnore
        public /* final */ er7 a; // = new er7(rp7.this.d.d());
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public void a(yq7 yq7, long j) throws IOException {
            if (this.b) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                rp7.this.d.c(j);
                rp7.this.d.a("\r\n");
                rp7.this.d.a(yq7, j);
                rp7.this.d.a("\r\n");
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
        public synchronized void close() throws IOException {
            if (!this.b) {
                this.b = true;
                rp7.this.d.a("0\r\n\r\n");
                rp7.this.a(this.a);
                rp7.this.e = 3;
            }
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public tr7 d() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.qr7, java.io.Flushable
        public synchronized void flush() throws IOException {
            if (!this.b) {
                rp7.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public /* final */ go7 e;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public boolean g; // = true;

        @DexIgnore
        public d(go7 go7) {
            super();
            this.e = go7;
        }

        @DexIgnore
        public final void a() throws IOException {
            if (this.f != -1) {
                rp7.this.c.p();
            }
            try {
                this.f = rp7.this.c.t();
                String trim = rp7.this.c.p().trim();
                if (this.f < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f + trim + "\"");
                } else if (this.f == 0) {
                    this.g = false;
                    kp7.a(rp7.this.a.l(), this.e, rp7.this.f());
                    a(true, null);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        @DexIgnore
        @Override // com.fossil.rp7.b, com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (((b) this).b) {
                throw new IllegalStateException("closed");
            } else if (!this.g) {
                return -1;
            } else {
                long j2 = this.f;
                if (j2 == 0 || j2 == -1) {
                    a();
                    if (!this.g) {
                        return -1;
                    }
                }
                long b = super.b(yq7, Math.min(j, this.f));
                if (b != -1) {
                    this.f -= b;
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!((b) this).b) {
                if (this.g && !ro7.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                ((b) this).b = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements qr7 {
        @DexIgnore
        public /* final */ er7 a; // = new er7(rp7.this.d.d());
        @DexIgnore
        public boolean b;
        @DexIgnore
        public long c;

        @DexIgnore
        public e(long j) {
            this.c = j;
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public void a(yq7 yq7, long j) throws IOException {
            if (!this.b) {
                ro7.a(yq7.x(), 0, j);
                if (j <= this.c) {
                    rp7.this.d.a(yq7, j);
                    this.c -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.c + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.b) {
                this.b = true;
                if (this.c <= 0) {
                    rp7.this.a(this.a);
                    rp7.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public tr7 d() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.qr7, java.io.Flushable
        public void flush() throws IOException {
            if (!this.b) {
                rp7.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long e;

        @DexIgnore
        public f(rp7 rp7, long j) throws IOException {
            super();
            this.e = j;
            if (j == 0) {
                a(true, null);
            }
        }

        @DexIgnore
        @Override // com.fossil.rp7.b, com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!((b) this).b) {
                long j2 = this.e;
                if (j2 == 0) {
                    return -1;
                }
                long b = super.b(yq7, Math.min(j2, j));
                if (b != -1) {
                    long j3 = this.e - b;
                    this.e = j3;
                    if (j3 == 0) {
                        a(true, null);
                    }
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!((b) this).b) {
                if (this.e != 0 && !ro7.a(this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, null);
                }
                ((b) this).b = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public g(rp7 rp7) {
            super();
        }

        @DexIgnore
        @Override // com.fossil.rp7.b, com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (((b) this).b) {
                throw new IllegalStateException("closed");
            } else if (this.e) {
                return -1;
            } else {
                long b = super.b(yq7, j);
                if (b != -1) {
                    return b;
                }
                this.e = true;
                a(true, null);
                return -1;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!((b) this).b) {
                if (!this.e) {
                    a(false, null);
                }
                ((b) this).b = true;
            }
        }
    }

    @DexIgnore
    public rp7(OkHttpClient okHttpClient, fp7 fp7, ar7 ar7, zq7 zq7) {
        this.a = okHttpClient;
        this.b = fp7;
        this.c = ar7;
        this.d = zq7;
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public qr7 a(lo7 lo7, long j) {
        if ("chunked".equalsIgnoreCase(lo7.a("Transfer-Encoding"))) {
            return c();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void b() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public qr7 c() {
        if (this.e == 1) {
            this.e = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void cancel() {
        bp7 c2 = this.b.c();
        if (c2 != null) {
            c2.b();
        }
    }

    @DexIgnore
    public sr7 d() throws IOException {
        if (this.e == 4) {
            fp7 fp7 = this.b;
            if (fp7 != null) {
                this.e = 5;
                fp7.e();
                return new g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String e() throws IOException {
        String b2 = this.c.b(this.f);
        this.f -= (long) b2.length();
        return b2;
    }

    @DexIgnore
    public fo7 f() throws IOException {
        fo7.a aVar = new fo7.a();
        while (true) {
            String e2 = e();
            if (e2.length() == 0) {
                return aVar.a();
            }
            po7.a.a(aVar, e2);
        }
    }

    @DexIgnore
    public sr7 b(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new f(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void a(lo7 lo7) throws IOException {
        a(lo7.c(), op7.a(lo7, this.b.c().f().b().type()));
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public mo7 a(Response response) throws IOException {
        fp7 fp7 = this.b;
        fp7.f.e(fp7.e);
        String b2 = response.b("Content-Type");
        if (!kp7.b(response)) {
            return new np7(b2, 0, ir7.a(b(0)));
        }
        if ("chunked".equalsIgnoreCase(response.b("Transfer-Encoding"))) {
            return new np7(b2, -1, ir7.a(a(response.x().g())));
        }
        long a2 = kp7.a(response);
        if (a2 != -1) {
            return new np7(b2, a2, ir7.a(b(a2)));
        }
        return new np7(b2, -1, ir7.a(d()));
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public void a(fo7 fo7, String str) throws IOException {
        if (this.e == 0) {
            this.d.a(str).a("\r\n");
            int b2 = fo7.b();
            for (int i = 0; i < b2; i++) {
                this.d.a(fo7.a(i)).a(": ").a(fo7.b(i)).a("\r\n");
            }
            this.d.a("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    @Override // com.fossil.ip7
    public Response.a a(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                qp7 a2 = qp7.a(e());
                Response.a aVar = new Response.a();
                aVar.a(a2.a);
                aVar.a(a2.b);
                aVar.a(a2.c);
                aVar.a(f());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    public qr7 a(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public sr7 a(go7 go7) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new d(go7);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(er7 er7) {
        tr7 g2 = er7.g();
        er7.a(tr7.d);
        g2.a();
        g2.b();
    }
}
