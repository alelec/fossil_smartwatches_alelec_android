package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ok5 implements Factory<mk5> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<DNDSettingsDatabase> b;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> d;

    @DexIgnore
    public ok5(Provider<ch5> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static ok5 a(Provider<ch5> provider, Provider<DNDSettingsDatabase> provider2, Provider<QuickResponseRepository> provider3, Provider<NotificationSettingsDatabase> provider4) {
        return new ok5(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static mk5 a(ch5 ch5, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new mk5(ch5, dNDSettingsDatabase, quickResponseRepository, notificationSettingsDatabase);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public mk5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
