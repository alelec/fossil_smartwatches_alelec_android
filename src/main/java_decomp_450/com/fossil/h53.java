package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h53 extends i72 implements i12 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<h53> CREATOR; // = new s53();
    @DexIgnore
    public /* final */ Status a;
    @DexIgnore
    public /* final */ i53 b;

    @DexIgnore
    public h53(Status status) {
        this(status, null);
    }

    @DexIgnore
    public h53(Status status, i53 i53) {
        this.a = status;
        this.b = i53;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public final Status a() {
        return this.a;
    }

    @DexIgnore
    public final i53 e() {
        return this.b;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) a(), i, false);
        k72.a(parcel, 2, (Parcelable) e(), i, false);
        k72.a(parcel, a2);
    }
}
