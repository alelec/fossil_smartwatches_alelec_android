package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface en2 extends IInterface {
    @DexIgnore
    int a() throws RemoteException;

    @DexIgnore
    void b(ab2 ab2) throws RemoteException;

    @DexIgnore
    void b(String str) throws RemoteException;

    @DexIgnore
    boolean b(en2 en2) throws RemoteException;

    @DexIgnore
    void f() throws RemoteException;

    @DexIgnore
    String getId() throws RemoteException;

    @DexIgnore
    LatLng getPosition() throws RemoteException;

    @DexIgnore
    void r() throws RemoteException;

    @DexIgnore
    void remove() throws RemoteException;

    @DexIgnore
    void setAlpha(float f) throws RemoteException;

    @DexIgnore
    void setAnchor(float f, float f2) throws RemoteException;

    @DexIgnore
    void setDraggable(boolean z) throws RemoteException;

    @DexIgnore
    void setFlat(boolean z) throws RemoteException;

    @DexIgnore
    void setInfoWindowAnchor(float f, float f2) throws RemoteException;

    @DexIgnore
    void setPosition(LatLng latLng) throws RemoteException;

    @DexIgnore
    void setRotation(float f) throws RemoteException;

    @DexIgnore
    void setTitle(String str) throws RemoteException;

    @DexIgnore
    void setVisible(boolean z) throws RemoteException;

    @DexIgnore
    void setZIndex(float f) throws RemoteException;

    @DexIgnore
    boolean v() throws RemoteException;
}
