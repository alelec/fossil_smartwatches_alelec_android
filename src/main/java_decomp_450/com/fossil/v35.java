package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v35 extends u35 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i I; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray J;
    @DexIgnore
    public long H;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        J = sparseIntArray;
        sparseIntArray.put(2131362636, 1);
        J.put(2131362517, 2);
        J.put(2131362664, 3);
        J.put(2131362345, 4);
        J.put(2131362979, 5);
        J.put(2131362099, 6);
        J.put(2131362496, 7);
        J.put(2131362781, 8);
        J.put(2131362457, 9);
        J.put(2131363390, 10);
        J.put(2131362780, 11);
        J.put(2131362456, 12);
        J.put(2131363391, 13);
        J.put(2131362774, 14);
        J.put(2131362403, 15);
        J.put(2131363392, 16);
    }
    */

    @DexIgnore
    public v35(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 17, I, J));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.H = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.H != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.H = 1;
        }
        g();
    }

    @DexIgnore
    public v35(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[6], (FlexibleTextView) objArr[4], (ImageView) objArr[15], (ImageView) objArr[12], (ImageView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[2], (ImageView) objArr[1], (ImageView) objArr[3], (LinearLayout) objArr[14], (LinearLayout) objArr[11], (LinearLayout) objArr[8], (ConstraintLayout) objArr[0], (RecyclerView) objArr[5], (View) objArr[10], (View) objArr[13], (View) objArr[16]);
        this.H = -1;
        ((u35) this).C.setTag(null);
        a(view);
        f();
    }
}
