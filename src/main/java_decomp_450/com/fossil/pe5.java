package com.fossil;

import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pe5 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = w97.a((Object[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue(), MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue()});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = w97.a((Object[]) new String[]{MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue(), MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue()});
    @DexIgnore
    public static /* final */ pe5 c; // = new pe5();

    @DexIgnore
    public final String a(String str) {
        ee7.b(str, "microAppId");
        if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886527);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
            return a2;
        } else if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886363);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        } else if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID.getValue())) {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131887119);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026naProfile_List__SetGoals)");
            return a4;
        } else if (!ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_RING_PHONE.getValue())) {
            return "";
        } else {
            String a5 = ig5.a(PortfolioApp.g0.c(), 2131886417);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026ingtone_Title__RingPhone)");
            return a5;
        }
    }

    @DexIgnore
    public final List<String> b(String str) {
        ee7.b(str, "microAppId");
        if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
            int i = Build.VERSION.SDK_INT;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MicroAppHelper", "android.os.Build.VERSION.SDK_INT=" + i);
            if (i >= 29) {
                return w97.a((Object[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
            }
            return w97.a((Object[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
        } else if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return w97.a((Object[]) new String[]{InAppPermission.NOTIFICATION_ACCESS});
        } else {
            return new ArrayList();
        }
    }

    @DexIgnore
    public final boolean c(String str) {
        ee7.b(str, "microAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        ee7.b(str, "microAppId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean e(String str) {
        ee7.b(str, "microAppId");
        List<String> b2 = b(str);
        String[] a2 = px6.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MicroAppHelper", "isPermissionGrantedForMicroApp " + str + " granted=" + a2 + " required=" + b2);
        Iterator<T> it = b2.iterator();
        while (it.hasNext()) {
            if (!t97.a((Object[]) a2, (Object) it.next())) {
                return false;
            }
        }
        return true;
    }
}
