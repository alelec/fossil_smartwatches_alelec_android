package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vo1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wm1(vo1 vo1) {
        super(1);
        this.a = vo1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        vo1 vo1 = this.a;
        vu0 vu0 = (vu0) v81;
        long j = vu0.M;
        long j2 = vu0.N;
        long j3 = vu0.O;
        if (0 == j2) {
            vo1.D = vo1.E;
            vo1.a(eu0.a(((zk0) vo1).v, null, is0.SUCCESS, null, 5));
        } else if (j != vo1.E) {
            vo1.a(eu0.a(((zk0) vo1).v, null, is0.FLOW_BROKEN, null, 5));
        } else {
            vo1.F = j + j2;
            long a2 = ik1.a.a(vo1.H.e, (int) j, (int) j2, ng1.CRC32);
            le0 le0 = le0.DEBUG;
            if (j3 == a2) {
                long j4 = vo1.F;
                long j5 = vo1.G;
                if (j4 == j5) {
                    vo1.D = j4;
                    vo1.a(eu0.a(((zk0) vo1).v, null, is0.SUCCESS, null, 5));
                } else {
                    vo1.F = Math.min(((j4 - vo1.E) / ((long) 2)) + j4, j5);
                    vo1.E = j4;
                    zk0.a(vo1, qa1.o, null, 2, null);
                }
            } else {
                vo1.F = (vo1.F + vo1.E) / ((long) 2);
                zk0.a(vo1, qa1.o, null, 2, null);
            }
        }
        return i97.a;
    }
}
