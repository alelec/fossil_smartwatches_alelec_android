package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jn5 extends fl4<b, fl4.d, fl4.a> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public /* final */ SummariesRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;
    @DexIgnore
    public /* final */ ActivitiesRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            ee7.b(date, "date");
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries", f = "FetchSummaries.kt", l = {29, 57, 58, 61}, m = "run")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(jn5 jn5, fb7 fb7) {
            super(fb7);
            this.this$0 = jn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<? super i97>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = jn5.class.getSimpleName();
        ee7.a((Object) simpleName, "FetchSummaries::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public jn5(SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, ActivitiesRepository activitiesRepository) {
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(activitiesRepository, "mActivitiesRepository");
        this.d = summariesRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
        this.g = activitiesRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<? super i97>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return h;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01d0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01d1  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0202 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.jn5.b r17, com.fossil.fb7<? super com.fossil.i97> r18) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            boolean r3 = r2 instanceof com.fossil.jn5.c
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.jn5$c r3 = (com.fossil.jn5.c) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.jn5$c r3 = new com.fossil.jn5$c
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 4
            r7 = 3
            r8 = 2
            r9 = 1
            if (r5 == 0) goto L_0x00cf
            if (r5 == r9) goto L_0x00bb
            if (r5 == r8) goto L_0x0092
            if (r5 == r7) goto L_0x0069
            if (r5 != r6) goto L_0x0061
            java.lang.Object r1 = r3.L$9
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$8
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r3.L$7
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$6
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$5
            java.util.Calendar r1 = (java.util.Calendar) r1
            java.lang.Object r1 = r3.L$4
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$3
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r3.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r1 = r3.L$1
            com.fossil.jn5$b r1 = (com.fossil.jn5.b) r1
            java.lang.Object r1 = r3.L$0
            com.fossil.jn5 r1 = (com.fossil.jn5) r1
            com.fossil.t87.a(r2)
            goto L_0x0203
        L_0x0061:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0069:
            java.lang.Object r1 = r3.L$8
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r5 = r3.L$7
            java.util.Date r5 = (java.util.Date) r5
            java.lang.Object r7 = r3.L$6
            java.util.Date r7 = (java.util.Date) r7
            java.lang.Object r8 = r3.L$5
            java.util.Calendar r8 = (java.util.Calendar) r8
            java.lang.Object r9 = r3.L$4
            java.util.Date r9 = (java.util.Date) r9
            java.lang.Object r10 = r3.L$3
            com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
            java.lang.Object r11 = r3.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r12 = r3.L$1
            com.fossil.jn5$b r12 = (com.fossil.jn5.b) r12
            java.lang.Object r13 = r3.L$0
            com.fossil.jn5 r13 = (com.fossil.jn5) r13
            com.fossil.t87.a(r2)
            goto L_0x01d5
        L_0x0092:
            java.lang.Object r1 = r3.L$7
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r5 = r3.L$6
            java.util.Date r5 = (java.util.Date) r5
            java.lang.Object r8 = r3.L$5
            java.util.Calendar r8 = (java.util.Calendar) r8
            java.lang.Object r9 = r3.L$4
            java.util.Date r9 = (java.util.Date) r9
            java.lang.Object r10 = r3.L$3
            com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
            java.lang.Object r11 = r3.L$2
            java.util.Date r11 = (java.util.Date) r11
            java.lang.Object r12 = r3.L$1
            com.fossil.jn5$b r12 = (com.fossil.jn5.b) r12
            java.lang.Object r13 = r3.L$0
            com.fossil.jn5 r13 = (com.fossil.jn5) r13
            com.fossil.t87.a(r2)
            r7 = r5
            r5 = r1
            r1 = r11
            r11 = r9
            goto L_0x01b1
        L_0x00bb:
            java.lang.Object r1 = r3.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r5 = r3.L$1
            com.fossil.jn5$b r5 = (com.fossil.jn5.b) r5
            java.lang.Object r10 = r3.L$0
            com.fossil.jn5 r10 = (com.fossil.jn5) r10
            com.fossil.t87.a(r2)
            r15 = r2
            r2 = r1
            r1 = r5
            r5 = r15
            goto L_0x010d
        L_0x00cf:
            com.fossil.t87.a(r2)
            if (r1 != 0) goto L_0x00d7
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x00d7:
            java.util.Date r2 = r17.a()
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r10 = com.fossil.jn5.h
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "executeUseCase - date="
            r11.append(r12)
            java.lang.String r12 = com.fossil.qy6.a(r2)
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            r5.d(r10, r11)
            com.portfolio.platform.data.source.UserRepository r5 = r0.f
            r3.L$0 = r0
            r3.L$1 = r1
            r3.L$2 = r2
            r3.label = r9
            java.lang.Object r5 = r5.getCurrentUser(r3)
            if (r5 != r4) goto L_0x010c
            return r4
        L_0x010c:
            r10 = r0
        L_0x010d:
            com.portfolio.platform.data.model.MFUser r5 = (com.portfolio.platform.data.model.MFUser) r5
            if (r5 == 0) goto L_0x020b
            java.lang.String r11 = r5.getCreatedAt()
            boolean r11 = android.text.TextUtils.isEmpty(r11)
            if (r11 == 0) goto L_0x011d
            goto L_0x020b
        L_0x011d:
            java.lang.String r11 = r5.getCreatedAt()
            if (r11 == 0) goto L_0x0206
            java.util.Date r11 = com.fossil.zd5.d(r11)
            java.util.Calendar r12 = java.util.Calendar.getInstance()
            java.lang.String r13 = "calendar"
            com.fossil.ee7.a(r12, r13)
            r12.setTime(r2)
            r13 = 5
            r12.set(r13, r9)
            long r13 = r12.getTimeInMillis()
            java.lang.String r9 = "createdDate"
            com.fossil.ee7.a(r11, r9)
            long r6 = r11.getTime()
            boolean r6 = com.fossil.zd5.a(r13, r6)
            if (r6 == 0) goto L_0x014c
            r6 = r11
            goto L_0x0167
        L_0x014c:
            java.util.Calendar r6 = com.fossil.zd5.e(r12)
            java.lang.String r7 = "DateHelper.getStartOfMonth(calendar)"
            com.fossil.ee7.a(r6, r7)
            java.util.Date r6 = r6.getTime()
            java.lang.String r7 = "DateHelper.getStartOfMonth(calendar).time"
            com.fossil.ee7.a(r6, r7)
            boolean r7 = com.fossil.zd5.b(r11, r6)
            if (r7 == 0) goto L_0x0167
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0167:
            java.lang.Boolean r7 = com.fossil.zd5.v(r6)
            java.lang.String r13 = "DateHelper.isThisMonth(startDate)"
            com.fossil.ee7.a(r7, r13)
            boolean r7 = r7.booleanValue()
            if (r7 == 0) goto L_0x017c
            java.util.Date r7 = new java.util.Date
            r7.<init>()
            goto L_0x018e
        L_0x017c:
            java.util.Calendar r7 = com.fossil.zd5.m(r6)
            java.lang.String r13 = "DateHelper.getEndOfMonth(startDate)"
            com.fossil.ee7.a(r7, r13)
            java.util.Date r7 = r7.getTime()
            java.lang.String r13 = "DateHelper.getEndOfMonth(startDate).time"
            com.fossil.ee7.a(r7, r13)
        L_0x018e:
            com.portfolio.platform.data.source.ActivitiesRepository r13 = r10.g
            r3.L$0 = r10
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r5
            r3.L$4 = r11
            r3.L$5 = r12
            r3.L$6 = r7
            r3.L$7 = r6
            r3.label = r8
            java.lang.Object r8 = r13.getPendingActivities(r6, r7, r3)
            if (r8 != r4) goto L_0x01a9
            return r4
        L_0x01a9:
            r13 = r10
            r10 = r5
            r5 = r6
            r15 = r12
            r12 = r1
            r1 = r2
            r2 = r8
            r8 = r15
        L_0x01b1:
            java.util.List r2 = (java.util.List) r2
            com.portfolio.platform.data.source.FitnessDataRepository r6 = r13.e
            r3.L$0 = r13
            r3.L$1 = r12
            r3.L$2 = r1
            r3.L$3 = r10
            r3.L$4 = r11
            r3.L$5 = r8
            r3.L$6 = r7
            r3.L$7 = r5
            r3.L$8 = r2
            r9 = 3
            r3.label = r9
            java.lang.Object r6 = r6.getFitnessData(r5, r7, r3)
            if (r6 != r4) goto L_0x01d1
            return r4
        L_0x01d1:
            r9 = r11
            r11 = r1
            r1 = r2
            r2 = r6
        L_0x01d5:
            java.util.List r2 = (java.util.List) r2
            boolean r6 = r1.isEmpty()
            if (r6 == 0) goto L_0x0203
            boolean r6 = r2.isEmpty()
            if (r6 == 0) goto L_0x0203
            com.portfolio.platform.data.source.SummariesRepository r6 = r13.d
            r3.L$0 = r13
            r3.L$1 = r12
            r3.L$2 = r11
            r3.L$3 = r10
            r3.L$4 = r9
            r3.L$5 = r8
            r3.L$6 = r7
            r3.L$7 = r5
            r3.L$8 = r1
            r3.L$9 = r2
            r1 = 4
            r3.label = r1
            java.lang.Object r1 = r6.loadSummaries(r5, r7, r3)
            if (r1 != r4) goto L_0x0203
            return r4
        L_0x0203:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0206:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x020b:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.jn5.h
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "executeUseCase - FAILED!!! with user="
            r3.append(r4)
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jn5.a(com.fossil.jn5$b, com.fossil.fb7):java.lang.Object");
    }
}
