package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rw2 {
    @DexIgnore
    public volatile jx2 a;
    @DexIgnore
    public volatile tu2 b;

    /*
    static {
        nv2.a();
    }
    */

    @DexIgnore
    public final jx2 a(jx2 jx2) {
        jx2 jx22 = this.a;
        this.b = null;
        this.a = jx2;
        return jx22;
    }

    @DexIgnore
    public final jx2 b(jx2 jx2) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    try {
                        this.a = jx2;
                        this.b = tu2.zza;
                    } catch (iw2 unused) {
                        this.a = jx2;
                        this.b = tu2.zza;
                    }
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof rw2)) {
            return false;
        }
        rw2 rw2 = (rw2) obj;
        jx2 jx2 = this.a;
        jx2 jx22 = rw2.a;
        if (jx2 == null && jx22 == null) {
            return b().equals(rw2.b());
        }
        if (jx2 != null && jx22 != null) {
            return jx2.equals(jx22);
        }
        if (jx2 != null) {
            return jx2.equals(rw2.b(jx2.d()));
        }
        return b(jx22.d()).equals(jx22);
    }

    @DexIgnore
    public int hashCode() {
        return 1;
    }

    @DexIgnore
    public final int a() {
        if (this.b != null) {
            return this.b.zza();
        }
        if (this.a != null) {
            return this.a.k();
        }
        return 0;
    }

    @DexIgnore
    public final tu2 b() {
        if (this.b != null) {
            return this.b;
        }
        synchronized (this) {
            if (this.b != null) {
                return this.b;
            }
            if (this.a == null) {
                this.b = tu2.zza;
            } else {
                this.b = this.a.h();
            }
            return this.b;
        }
    }
}
