package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zg6 extends dy6 {
    @DexIgnore
    public static /* final */ String E;
    @DexIgnore
    public static /* final */ a F; // = new a(null);
    @DexIgnore
    public Long A;
    @DexIgnore
    public Long B;
    @DexIgnore
    public qw6<k75> C;
    @DexIgnore
    public HashMap D;
    @DexIgnore
    public Integer j;
    @DexIgnore
    public Integer p;
    @DexIgnore
    public Integer q;
    @DexIgnore
    public Float r;
    @DexIgnore
    public Integer s;
    @DexIgnore
    public Integer t;
    @DexIgnore
    public Integer u;
    @DexIgnore
    public Float v;
    @DexIgnore
    public b w;
    @DexIgnore
    public vb5 x;
    @DexIgnore
    public String y;
    @DexIgnore
    public Long z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return zg6.E;
        }

        @DexIgnore
        public final zg6 b() {
            return new zg6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(vb5 vb5, int i, Integer num, float f, Float f2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ zg6 a;

        @DexIgnore
        public c(zg6 zg6) {
            this.a = zg6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = zg6.F.a();
            local.d(a2, "second was changed from " + i + " to " + i2);
            this.a.u = Integer.valueOf(i2);
            this.a.c1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zg6 a;

        @DexIgnore
        public d(zg6 zg6) {
            this.a = zg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            Integer c;
            b d = this.a.w;
            if (!(d == null || (c = this.a.q) == null)) {
                int intValue = c.intValue();
                vb5 a2 = this.a.x;
                if (a2 != null) {
                    Integer f = this.a.u;
                    Float b = this.a.r;
                    if (b != null) {
                        d.a(a2, intValue, f, b.floatValue(), this.a.v);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.a.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ zg6 a;

        @DexIgnore
        public e(zg6 zg6) {
            this.a = zg6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ zg6 a;

        @DexIgnore
        public f(zg6 zg6) {
            this.a = zg6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = zg6.F.a();
            local.d(a2, "first was changed from " + i + " to " + i2);
            this.a.q = Integer.valueOf(i2);
            this.a.c1();
        }
    }

    /*
    static {
        String simpleName = zg6.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutEditPickerFragment::class.java.simpleName");
        E = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.D;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final k75 b1() {
        qw6<k75> qw6 = this.C;
        if (qw6 != null) {
            return qw6.a();
        }
        return null;
    }

    @DexIgnore
    public final void c1() {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        Long l = this.z;
        if (l != null) {
            long longValue = l.longValue();
            Calendar instance = Calendar.getInstance();
            Integer num = this.q;
            if (num != null) {
                int intValue = num.intValue();
                int i = instance.get(11);
                Integer num2 = this.u;
                if (num2 != null) {
                    int intValue2 = num2.intValue();
                    int i2 = instance.get(12);
                    ee7.a((Object) instance, "calendar");
                    boolean a2 = a(intValue, i, intValue2, i2, longValue, instance.getTimeInMillis());
                    String str = a2 ? "flexible_button_primary" : "flexible_button_disabled";
                    k75 b1 = b1();
                    if (!(b1 == null || (flexibleButton2 = b1.s) == null)) {
                        flexibleButton2.setEnabled(a2);
                    }
                    k75 b12 = b1();
                    if (b12 != null && (flexibleButton = b12.s) != null) {
                        flexibleButton.a(str);
                        return;
                    }
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, 2131951897);
    }

    @DexIgnore
    @Override // com.fossil.ns3, com.fossil.m0, com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        FLogger.INSTANCE.getLocal().d(E, "onCreateDialog");
        View inflate = View.inflate(getContext(), 2131558641, null);
        ee7.a((Object) inflate, "view");
        inflate.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        ms3 ms3 = new ms3(requireContext(), getTheme());
        ms3.setContentView(inflate);
        ms3.setCanceledOnTouchOutside(true);
        return ms3;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ViewDataBinding a2 = qb.a(layoutInflater, 2131558641, viewGroup, false);
        ee7.a((Object) a2, "DataBindingUtil.inflate(\u2026picker, container, false)");
        k75 k75 = (k75) a2;
        this.C = new qw6<>(this, k75);
        return k75.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        FlexibleButton flexibleButton;
        FlexibleButton flexibleButton2;
        NumberPicker numberPicker;
        NumberPicker numberPicker2;
        k75 b1;
        NumberPicker numberPicker3;
        NumberPicker numberPicker4;
        NumberPicker numberPicker5;
        NumberPicker numberPicker6;
        NumberPicker numberPicker7;
        NumberPicker numberPicker8;
        NumberPicker numberPicker9;
        NumberPicker numberPicker10;
        NumberPicker numberPicker11;
        FlexibleTextView flexibleTextView;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        k75 b12 = b1();
        if (!(b12 == null || (flexibleTextView = b12.t) == null)) {
            flexibleTextView.setText(this.y);
        }
        Integer num = this.j;
        if (num != null) {
            int intValue = num.intValue();
            k75 b13 = b1();
            if (!(b13 == null || (numberPicker11 = b13.u) == null)) {
                numberPicker11.setMaxValue(intValue);
            }
        }
        Integer num2 = this.p;
        if (num2 != null) {
            int intValue2 = num2.intValue();
            k75 b14 = b1();
            if (!(b14 == null || (numberPicker10 = b14.u) == null)) {
                numberPicker10.setMinValue(intValue2);
            }
        }
        Integer num3 = this.q;
        if (num3 != null) {
            int intValue3 = num3.intValue();
            k75 b15 = b1();
            if (!(b15 == null || (numberPicker9 = b15.u) == null)) {
                numberPicker9.setValue(intValue3);
            }
        }
        Float f2 = this.r;
        if (f2 != null) {
            float floatValue = f2.floatValue();
            ArrayList arrayList = new ArrayList();
            Integer num4 = this.p;
            if (num4 != null) {
                int intValue4 = num4.intValue();
                Integer num5 = this.j;
                if (num5 != null) {
                    int intValue5 = num5.intValue();
                    if (intValue4 <= intValue5) {
                        while (true) {
                            float f3 = ((float) intValue4) * floatValue;
                            if (f3 % ((float) 1) == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                arrayList.add(String.valueOf((int) f3));
                            } else {
                                arrayList.add(String.valueOf(re5.c(f3, 1)));
                            }
                            if (intValue4 == intValue5) {
                                break;
                            }
                            intValue4++;
                        }
                    }
                    k75 b16 = b1();
                    if (!(b16 == null || (numberPicker8 = b16.u) == null)) {
                        Object[] array = arrayList.toArray(new String[0]);
                        if (array != null) {
                            numberPicker8.setDisplayedValues((String[]) array);
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        Integer num6 = this.s;
        if (num6 != null) {
            int intValue6 = num6.intValue();
            k75 b17 = b1();
            if (!(b17 == null || (numberPicker7 = b17.v) == null)) {
                numberPicker7.setMaxValue(intValue6);
            }
        }
        Integer num7 = this.t;
        if (num7 != null) {
            int intValue7 = num7.intValue();
            k75 b18 = b1();
            if (!(b18 == null || (numberPicker6 = b18.v) == null)) {
                numberPicker6.setMinValue(intValue7);
            }
        }
        Integer num8 = this.u;
        if (num8 != null) {
            int intValue8 = num8.intValue();
            k75 b19 = b1();
            if (!(b19 == null || (numberPicker5 = b19.v) == null)) {
                numberPicker5.setValue(intValue8);
            }
        }
        Float f4 = this.v;
        if (f4 != null) {
            float floatValue2 = f4.floatValue();
            ArrayList arrayList2 = new ArrayList();
            Integer num9 = this.t;
            if (num9 != null) {
                int intValue9 = num9.intValue();
                Integer num10 = this.s;
                if (num10 != null) {
                    int intValue10 = num10.intValue();
                    if (intValue9 <= intValue10) {
                        while (true) {
                            float f5 = ((float) intValue9) * floatValue2;
                            if (f5 % ((float) 1) == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                arrayList2.add(String.valueOf((int) f5));
                            } else {
                                arrayList2.add(String.valueOf(re5.c(f5, 1)));
                            }
                            if (intValue9 == intValue10) {
                                break;
                            }
                            intValue9++;
                        }
                    }
                    k75 b110 = b1();
                    if (!(b110 == null || (numberPicker4 = b110.v) == null)) {
                        Object[] array2 = arrayList2.toArray(new String[0]);
                        if (array2 != null) {
                            numberPicker4.setDisplayedValues((String[]) array2);
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        if (!(this.u != null || (b1 = b1()) == null || (numberPicker3 = b1.v) == null)) {
            numberPicker3.setVisibility(8);
        }
        k75 b111 = b1();
        if (!(b111 == null || (numberPicker2 = b111.u) == null)) {
            numberPicker2.setOnValueChangedListener(new f(this));
        }
        k75 b112 = b1();
        if (!(b112 == null || (numberPicker = b112.v) == null)) {
            numberPicker.setOnValueChangedListener(new c(this));
        }
        k75 b113 = b1();
        if (!(b113 == null || (flexibleButton2 = b113.s) == null)) {
            flexibleButton2.setOnClickListener(new d(this));
        }
        k75 b114 = b1();
        if (b114 != null && (flexibleButton = b114.r) != null) {
            flexibleButton.setOnClickListener(new e(this));
        }
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "title");
        this.y = str;
    }

    @DexIgnore
    public final void c(Long l) {
        this.A = l;
    }

    @DexIgnore
    public final boolean a(int i, int i2, int i3, int i4, long j2, long j3) {
        vb5 vb5;
        if (!DateUtils.isToday(j2) || (vb5 = this.x) == null) {
            return true;
        }
        int i5 = ah6.a[vb5.ordinal()];
        if (i5 == 1) {
            Long l = this.B;
            if (l == null) {
                return true;
            }
            long longValue = l.longValue();
            int i6 = ((i2 * 3600) + (i4 * 60)) * 1000;
            long j4 = ((long) (((i * 3600) + (i3 * 60)) * 1000)) + longValue;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = E;
            local.d(str, "checkIfValidStartTime duration " + longValue + " editedTime " + j4 + " curTime " + i6);
            if (j4 <= ((long) i6)) {
                return true;
            }
            return false;
        } else if (i5 != 2) {
            return true;
        } else {
            if (i == 0 && i3 == 0) {
                return false;
            }
            Long l2 = this.A;
            if (l2 == null) {
                return true;
            }
            long longValue2 = l2.longValue();
            long j5 = ((long) (((i * 3600) + (i3 * 60)) * 1000)) + longValue2;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = E;
            local2.d(str2, "checkIfValidStartTime startTime " + longValue2 + " durationEdited " + j5 + " now " + j3);
            if (j5 <= j3) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public final void b(wb5 wb5) {
        Float f2 = null;
        this.u = wb5 != null ? Integer.valueOf(wb5.d()) : null;
        this.t = wb5 != null ? Integer.valueOf(wb5.b()) : null;
        this.s = wb5 != null ? Integer.valueOf(wb5.a()) : null;
        if (wb5 != null) {
            f2 = Float.valueOf(wb5.c());
        }
        this.v = f2;
    }

    @DexIgnore
    public final void b(Long l) {
        if (l != null) {
            this.B = Long.valueOf(l.longValue());
        }
    }

    @DexIgnore
    public final void a(vb5 vb5) {
        ee7.b(vb5, "mode");
        this.x = vb5;
    }

    @DexIgnore
    public final void a(wb5 wb5) {
        Float f2 = null;
        this.q = wb5 != null ? Integer.valueOf(wb5.d()) : null;
        this.p = wb5 != null ? Integer.valueOf(wb5.b()) : null;
        this.j = wb5 != null ? Integer.valueOf(wb5.a()) : null;
        if (wb5 != null) {
            f2 = Float.valueOf(wb5.c());
        }
        this.r = f2;
    }

    @DexIgnore
    public final void a(Long l) {
        this.z = l;
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.w = bVar;
    }
}
