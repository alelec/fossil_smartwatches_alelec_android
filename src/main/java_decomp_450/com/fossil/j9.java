package com.fossil;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j9 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ View.OnLongClickListener f; // = new a();
    @DexIgnore
    public /* final */ View.OnTouchListener g; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnLongClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            return j9.this.a(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnTouchListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return j9.this.a(view, motionEvent);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        boolean a(View view, j9 j9Var);
    }

    @DexIgnore
    public j9(View view, c cVar) {
        this.a = view;
        this.b = cVar;
    }

    @DexIgnore
    public void a() {
        this.a.setOnLongClickListener(this.f);
        this.a.setOnTouchListener(this.g);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (r2 != 3) goto L_0x004d;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.View r7, android.view.MotionEvent r8) {
        /*
            r6 = this;
            float r0 = r8.getX()
            int r0 = (int) r0
            float r1 = r8.getY()
            int r1 = (int) r1
            int r2 = r8.getAction()
            r3 = 0
            if (r2 == 0) goto L_0x0049
            r4 = 1
            if (r2 == r4) goto L_0x0046
            r5 = 2
            if (r2 == r5) goto L_0x001b
            r7 = 3
            if (r2 == r7) goto L_0x0046
            goto L_0x004d
        L_0x001b:
            r2 = 8194(0x2002, float:1.1482E-41)
            boolean r2 = com.fossil.q9.a(r8, r2)
            if (r2 == 0) goto L_0x004d
            int r8 = r8.getButtonState()
            r8 = r8 & r4
            if (r8 != 0) goto L_0x002b
            goto L_0x004d
        L_0x002b:
            boolean r8 = r6.e
            if (r8 == 0) goto L_0x0030
            goto L_0x004d
        L_0x0030:
            int r8 = r6.c
            if (r8 != r0) goto L_0x0039
            int r8 = r6.d
            if (r8 != r1) goto L_0x0039
            goto L_0x004d
        L_0x0039:
            r6.c = r0
            r6.d = r1
            com.fossil.j9$c r8 = r6.b
            boolean r7 = r8.a(r7, r6)
            r6.e = r7
            return r7
        L_0x0046:
            r6.e = r3
            goto L_0x004d
        L_0x0049:
            r6.c = r0
            r6.d = r1
        L_0x004d:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.j9.a(android.view.View, android.view.MotionEvent):boolean");
    }

    @DexIgnore
    public boolean a(View view) {
        return this.b.a(view, this);
    }

    @DexIgnore
    public void a(Point point) {
        point.set(this.c, this.d);
    }
}
