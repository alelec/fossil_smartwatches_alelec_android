package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class j63 extends zl2 implements i63 {
    @DexIgnore
    public j63() {
        super("com.google.android.gms.location.ILocationListener");
    }

    @DexIgnore
    public static i63 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationListener");
        return queryLocalInterface instanceof i63 ? (i63) queryLocalInterface : new k63(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.zl2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        onLocationChanged((Location) jm2.a(parcel, Location.CREATOR));
        return true;
    }
}
