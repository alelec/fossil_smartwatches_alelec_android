package com.fossil;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a30 implements cx<InputStream, t20> {
    @DexIgnore
    public /* final */ List<ImageHeaderParser> a;
    @DexIgnore
    public /* final */ cx<ByteBuffer, t20> b;
    @DexIgnore
    public /* final */ az c;

    @DexIgnore
    public a30(List<ImageHeaderParser> list, cx<ByteBuffer, t20> cxVar, az azVar) {
        this.a = list;
        this.b = cxVar;
        this.c = azVar;
    }

    @DexIgnore
    public boolean a(InputStream inputStream, ax axVar) throws IOException {
        return !((Boolean) axVar.a(z20.b)).booleanValue() && xw.b(this.a, inputStream, this.c) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public uy<t20> a(InputStream inputStream, int i, int i2, ax axVar) throws IOException {
        byte[] a2 = a(inputStream);
        if (a2 == null) {
            return null;
        }
        return this.b.a(ByteBuffer.wrap(a2), i, i2, axVar);
    }

    @DexIgnore
    public static byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (!Log.isLoggable("StreamGifDecoder", 5)) {
                return null;
            }
            Log.w("StreamGifDecoder", "Error reading data from stream", e);
            return null;
        }
    }
}
