package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import com.facebook.applinks.AppLinkData;
import com.fossil.ay1;
import com.fossil.ku1;
import com.fossil.pu1;
import com.j256.ormlite.field.FieldType;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rx1 implements sw1, ay1 {
    @DexIgnore
    public static /* final */ bt1 e; // = bt1.a("proto");
    @DexIgnore
    public /* final */ xx1 a;
    @DexIgnore
    public /* final */ by1 b;
    @DexIgnore
    public /* final */ by1 c;
    @DexIgnore
    public /* final */ tw1 d;

    @DexIgnore
    public interface b<T, U> {
        @DexIgnore
        U apply(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            this.a = str;
            this.b = str2;
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T a();
    }

    @DexIgnore
    public rx1(by1 by1, by1 by12, tw1 tw1, xx1 xx1) {
        this.a = xx1;
        this.b = by1;
        this.c = by12;
        this.d = tw1;
    }

    @DexIgnore
    public static /* synthetic */ SQLiteDatabase b(Throwable th) {
        throw new zx1("Timed out while trying to open db.", th);
    }

    @DexIgnore
    public static String c(Iterable<yw1> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<yw1> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().b());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    @DexIgnore
    public static /* synthetic */ byte[] d(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (cursor.moveToNext()) {
            byte[] blob = cursor.getBlob(0);
            arrayList.add(blob);
            i += blob.length;
        }
        byte[] bArr = new byte[i];
        int i2 = 0;
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            byte[] bArr2 = (byte[]) arrayList.get(i3);
            System.arraycopy(bArr2, 0, bArr, i2, bArr2.length);
            i2 += bArr2.length;
        }
        return bArr;
    }

    @DexIgnore
    public SQLiteDatabase a() {
        xx1 xx1 = this.a;
        xx1.getClass();
        return (SQLiteDatabase) a(ix1.a(xx1), lx1.a());
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public int cleanUp() {
        return ((Integer) a(cx1.a(this.b.a() - this.d.b()))).intValue();
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @DexIgnore
    public final byte[] e(long j) {
        return (byte[]) a(a().query("event_payloads", new String[]{"bytes"}, "event_id = ?", new String[]{String.valueOf(j)}, null, null, "sequence_num"), ex1.a());
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public Iterable<pu1> i() {
        return (Iterable) a(bx1.a());
    }

    @DexIgnore
    public final Long b(SQLiteDatabase sQLiteDatabase, pu1 pu1) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(pu1.a(), String.valueOf(hy1.a(pu1.c()))));
        if (pu1.b() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(pu1.b(), 0));
        }
        return (Long) a(sQLiteDatabase.query("transport_contexts", new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX}, sb.toString(), (String[]) arrayList.toArray(new String[0]), null, null, null), nx1.a());
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public yw1 a(pu1 pu1, ku1 ku1) {
        kv1.a("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", pu1.c(), ku1.f(), pu1.a());
        long longValue = ((Long) a(mx1.a(this, pu1, ku1))).longValue();
        if (longValue < 1) {
            return null;
        }
        return yw1.a(longValue, pu1, ku1);
    }

    @DexIgnore
    public final boolean e() {
        return b() * c() >= this.d.e();
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public boolean c(pu1 pu1) {
        return ((Boolean) a(qx1.a(this, pu1))).booleanValue();
    }

    @DexIgnore
    public static /* synthetic */ List c(SQLiteDatabase sQLiteDatabase) {
        return (List) a(sQLiteDatabase.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), jx1.a());
    }

    @DexIgnore
    public static /* synthetic */ Long a(rx1 rx1, pu1 pu1, ku1 ku1, SQLiteDatabase sQLiteDatabase) {
        if (rx1.e()) {
            return -1L;
        }
        long a2 = rx1.a(sQLiteDatabase, pu1);
        int d2 = rx1.d.d();
        byte[] a3 = ku1.c().a();
        boolean z = a3.length <= d2;
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", Long.valueOf(a2));
        contentValues.put("transport_name", ku1.f());
        contentValues.put("timestamp_ms", Long.valueOf(ku1.d()));
        contentValues.put("uptime_ms", Long.valueOf(ku1.g()));
        contentValues.put("payload_encoding", ku1.c().b().a());
        contentValues.put("code", ku1.b());
        contentValues.put("num_attempts", (Integer) 0);
        contentValues.put("inline", Boolean.valueOf(z));
        contentValues.put("payload", z ? a3 : new byte[0]);
        long insert = sQLiteDatabase.insert("events", null, contentValues);
        if (!z) {
            int ceil = (int) Math.ceil(((double) a3.length) / ((double) d2));
            for (int i = 1; i <= ceil; i++) {
                byte[] copyOfRange = Arrays.copyOfRange(a3, (i - 1) * d2, Math.min(i * d2, a3.length));
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put(LogBuilder.KEY_EVENT_ID, Long.valueOf(insert));
                contentValues2.put("sequence_num", Integer.valueOf(i));
                contentValues2.put("bytes", copyOfRange);
                sQLiteDatabase.insert("event_payloads", null, contentValues2);
            }
        }
        for (Map.Entry<String, String> entry : ku1.e().entrySet()) {
            ContentValues contentValues3 = new ContentValues();
            contentValues3.put(LogBuilder.KEY_EVENT_ID, Long.valueOf(insert));
            contentValues3.put("name", entry.getKey());
            contentValues3.put("value", entry.getValue());
            sQLiteDatabase.insert("event_metadata", null, contentValues3);
        }
        return Long.valueOf(insert);
    }

    @DexIgnore
    public static /* synthetic */ List c(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            pu1.a d2 = pu1.d();
            d2.a(cursor.getString(1));
            d2.a(hy1.a(cursor.getInt(2)));
            d2.a(b(cursor.getString(3)));
            arrayList.add(d2.a());
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ Long b(Cursor cursor) {
        if (!cursor.moveToNext()) {
            return null;
        }
        return Long.valueOf(cursor.getLong(0));
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public void b(Iterable<yw1> iterable) {
        if (iterable.iterator().hasNext()) {
            a(ox1.a("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in " + c(iterable)));
        }
    }

    @DexIgnore
    public final List<yw1> c(SQLiteDatabase sQLiteDatabase, pu1 pu1) {
        ArrayList arrayList = new ArrayList();
        Long b2 = b(sQLiteDatabase, pu1);
        if (b2 == null) {
            return arrayList;
        }
        a(sQLiteDatabase.query("events", new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", "payload", "code", "inline"}, "context_id = ?", new String[]{b2.toString()}, null, null, null, String.valueOf(this.d.c())), dx1.a(this, arrayList, pu1));
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public long b(pu1 pu1) {
        return ((Long) a(a().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{pu1.a(), String.valueOf(hy1.a(pu1.c()))}), px1.a())).longValue();
    }

    @DexIgnore
    public static /* synthetic */ List b(rx1 rx1, pu1 pu1, SQLiteDatabase sQLiteDatabase) {
        List<yw1> c2 = rx1.c(sQLiteDatabase, pu1);
        rx1.a(c2, rx1.a(sQLiteDatabase, c2));
        return c2;
    }

    @DexIgnore
    public static bt1 c(String str) {
        if (str == null) {
            return e;
        }
        return bt1.a(str);
    }

    @DexIgnore
    public static byte[] b(String str) {
        if (str == null) {
            return null;
        }
        return Base64.decode(str, 0);
    }

    @DexIgnore
    public final long c() {
        return a().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }

    @DexIgnore
    public final long b() {
        return a().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }

    @DexIgnore
    public final long a(SQLiteDatabase sQLiteDatabase, pu1 pu1) {
        Long b2 = b(sQLiteDatabase, pu1);
        if (b2 != null) {
            return b2.longValue();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", pu1.a());
        contentValues.put("priority", Integer.valueOf(hy1.a(pu1.c())));
        contentValues.put("next_request_ms", (Integer) 0);
        if (pu1.b() != null) {
            contentValues.put(AppLinkData.ARGUMENTS_EXTRAS_KEY, Base64.encodeToString(pu1.b(), 0));
        }
        return sQLiteDatabase.insert("transport_contexts", null, contentValues);
    }

    @DexIgnore
    public static /* synthetic */ Object a(String str, SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.compileStatement(str).execute();
        sQLiteDatabase.compileStatement("DELETE FROM events WHERE num_attempts >= 10").execute();
        return null;
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public void a(Iterable<yw1> iterable) {
        if (iterable.iterator().hasNext()) {
            a().compileStatement("DELETE FROM events WHERE _id in " + c(iterable)).execute();
        }
    }

    @DexIgnore
    public static /* synthetic */ Long a(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return 0L;
    }

    @DexIgnore
    public static /* synthetic */ Boolean a(rx1 rx1, pu1 pu1, SQLiteDatabase sQLiteDatabase) {
        Long b2 = rx1.b(sQLiteDatabase, pu1);
        if (b2 == null) {
            return false;
        }
        return (Boolean) a(rx1.a().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{b2.toString()}), kx1.a());
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public void a(pu1 pu1, long j) {
        a(zw1.a(j, pu1));
    }

    @DexIgnore
    public static /* synthetic */ Object a(long j, pu1 pu1, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("next_request_ms", Long.valueOf(j));
        if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{pu1.a(), String.valueOf(hy1.a(pu1.c()))}) < 1) {
            contentValues.put("backend_name", pu1.a());
            contentValues.put("priority", Integer.valueOf(hy1.a(pu1.c())));
            sQLiteDatabase.insert("transport_contexts", null, contentValues);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.sw1
    public Iterable<yw1> a(pu1 pu1) {
        return (Iterable) a(ax1.a(this, pu1));
    }

    @DexIgnore
    public static /* synthetic */ Object a(rx1 rx1, List list, pu1 pu1, Cursor cursor) {
        while (cursor.moveToNext()) {
            boolean z = false;
            long j = cursor.getLong(0);
            if (cursor.getInt(7) != 0) {
                z = true;
            }
            ku1.a i = ku1.i();
            i.a(cursor.getString(1));
            i.a(cursor.getLong(2));
            i.b(cursor.getLong(3));
            if (z) {
                i.a(new ju1(c(cursor.getString(4)), cursor.getBlob(5)));
            } else {
                i.a(new ju1(c(cursor.getString(4)), rx1.e(j)));
            }
            if (!cursor.isNull(6)) {
                i.a(Integer.valueOf(cursor.getInt(6)));
            }
            list.add(yw1.a(j, pu1, i.a()));
        }
        return null;
    }

    @DexIgnore
    public final Map<Long, Set<c>> a(SQLiteDatabase sQLiteDatabase, List<yw1> list) {
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).b());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        a(sQLiteDatabase.query("event_metadata", new String[]{LogBuilder.KEY_EVENT_ID, "name", "value"}, sb.toString(), null, null, null, null), fx1.a(hashMap));
        return hashMap;
    }

    @DexIgnore
    public static /* synthetic */ Object a(Map map, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            Set set = (Set) map.get(Long.valueOf(j));
            if (set == null) {
                set = new HashSet();
                map.put(Long.valueOf(j), set);
            }
            set.add(new c(cursor.getString(1), cursor.getString(2)));
        }
        return null;
    }

    @DexIgnore
    public final List<yw1> a(List<yw1> list, Map<Long, Set<c>> map) {
        ListIterator<yw1> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            yw1 next = listIterator.next();
            if (map.containsKey(Long.valueOf(next.b()))) {
                ku1.a h = next.a().h();
                for (c cVar : map.get(Long.valueOf(next.b()))) {
                    h.a(cVar.a, cVar.b);
                }
                listIterator.set(yw1.a(next.b(), next.c(), h.a()));
            }
        }
        return list;
    }

    @DexIgnore
    public final <T> T a(d<T> dVar, b<Throwable, T> bVar) {
        long a2 = this.c.a();
        while (true) {
            try {
                return dVar.a();
            } catch (SQLiteDatabaseLockedException e2) {
                if (this.c.a() >= ((long) this.d.a()) + a2) {
                    return bVar.apply(e2);
                }
                SystemClock.sleep(50);
            }
        }
    }

    @DexIgnore
    public final void a(SQLiteDatabase sQLiteDatabase) {
        a(gx1.a(sQLiteDatabase), hx1.a());
    }

    @DexIgnore
    public static /* synthetic */ Object a(Throwable th) {
        throw new zx1("Timed out while trying to acquire the lock.", th);
    }

    @DexIgnore
    @Override // com.fossil.ay1
    public <T> T a(ay1.a<T> aVar) {
        SQLiteDatabase a2 = a();
        a(a2);
        try {
            T a3 = aVar.a();
            a2.setTransactionSuccessful();
            return a3;
        } finally {
            a2.endTransaction();
        }
    }

    @DexIgnore
    public final <T> T a(b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase a2 = a();
        a2.beginTransaction();
        try {
            T apply = bVar.apply(a2);
            a2.setTransactionSuccessful();
            return apply;
        } finally {
            a2.endTransaction();
        }
    }

    @DexIgnore
    public static <T> T a(Cursor cursor, b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }
}
