package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface c73 extends IInterface {
    @DexIgnore
    boolean B() throws RemoteException;

    @DexIgnore
    boolean D() throws RemoteException;

    @DexIgnore
    boolean i() throws RemoteException;

    @DexIgnore
    boolean k() throws RemoteException;

    @DexIgnore
    boolean l() throws RemoteException;

    @DexIgnore
    boolean o() throws RemoteException;

    @DexIgnore
    boolean p() throws RemoteException;

    @DexIgnore
    void setCompassEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMapToolbarEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMyLocationButtonEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setRotateGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setScrollGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setTiltGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setZoomControlsEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setZoomGesturesEnabled(boolean z) throws RemoteException;

    @DexIgnore
    boolean t() throws RemoteException;
}
