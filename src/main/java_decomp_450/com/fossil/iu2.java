package com.fossil;

import com.fossil.iu2;
import com.fossil.ju2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iu2<MessageType extends ju2<MessageType, BuilderType>, BuilderType extends iu2<MessageType, BuilderType>> implements mx2 {
    @DexIgnore
    public abstract BuilderType a(MessageType messagetype);

    @DexIgnore
    public abstract BuilderType a(byte[] bArr, int i, int i2) throws iw2;

    @DexIgnore
    public abstract BuilderType a(byte[] bArr, int i, int i2, nv2 nv2) throws iw2;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.iu2<MessageType extends com.fossil.ju2<MessageType, BuilderType>, BuilderType extends com.fossil.iu2<MessageType, BuilderType>> */
    /* JADX DEBUG: Multi-variable search result rejected for r2v2, resolved type: com.fossil.ju2 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.mx2
    public final /* synthetic */ mx2 a(jx2 jx2) {
        if (d().getClass().isInstance(jx2)) {
            return a((ju2) jx2);
        }
        throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
    }

    @DexIgnore
    @Override // com.fossil.mx2
    public final /* synthetic */ mx2 a(byte[] bArr, nv2 nv2) throws iw2 {
        a(bArr, 0, bArr.length, nv2);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.mx2
    public final /* synthetic */ mx2 a(byte[] bArr) throws iw2 {
        a(bArr, 0, bArr.length);
        return this;
    }
}
