package com.fossil;

import android.content.Context;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv1 implements Factory<fv1> {
    @DexIgnore
    public /* final */ Provider<Context> a;
    @DexIgnore
    public /* final */ Provider<by1> b;
    @DexIgnore
    public /* final */ Provider<by1> c;

    @DexIgnore
    public gv1(Provider<Context> provider, Provider<by1> provider2, Provider<by1> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static gv1 a(Provider<Context> provider, Provider<by1> provider2, Provider<by1> provider3) {
        return new gv1(provider, provider2, provider3);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fv1 get() {
        return new fv1(this.a.get(), this.b.get(), this.c.get());
    }
}
