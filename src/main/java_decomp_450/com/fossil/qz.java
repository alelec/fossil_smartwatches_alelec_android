package com.fossil;

import com.fossil.nz;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qz implements nz.a {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        File a();
    }

    @DexIgnore
    public qz(a aVar, long j) {
        this.a = j;
        this.b = aVar;
    }

    @DexIgnore
    @Override // com.fossil.nz.a
    public nz build() {
        File a2 = this.b.a();
        if (a2 == null) {
            return null;
        }
        if (a2.mkdirs() || (a2.exists() && a2.isDirectory())) {
            return rz.a(a2, this.a);
        }
        return null;
    }
}
