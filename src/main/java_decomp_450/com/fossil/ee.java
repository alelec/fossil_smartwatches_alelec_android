package com.fossil;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.SavedStateHandleController;
import androidx.lifecycle.ViewModelProvider;
import androidx.savedstate.SavedStateRegistry;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee extends ViewModelProvider.b {
    @DexIgnore
    public static /* final */ Class<?>[] f; // = {Application.class, de.class};
    @DexIgnore
    public static /* final */ Class<?>[] g; // = {de.class};
    @DexIgnore
    public /* final */ Application a;
    @DexIgnore
    public /* final */ ViewModelProvider.a b;
    @DexIgnore
    public /* final */ Bundle c;
    @DexIgnore
    public /* final */ Lifecycle d;
    @DexIgnore
    public /* final */ SavedStateRegistry e;

    @DexIgnore
    @SuppressLint({"LambdaLast"})
    public ee(Application application, ui uiVar, Bundle bundle) {
        this.e = uiVar.getSavedStateRegistry();
        this.d = uiVar.getLifecycle();
        this.c = bundle;
        this.a = application;
        this.b = ViewModelProvider.a.a(application);
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.b
    public <T extends he> T a(String str, Class<T> cls) {
        Constructor constructor;
        T t;
        T t2;
        boolean isAssignableFrom = yc.class.isAssignableFrom(cls);
        if (isAssignableFrom) {
            constructor = a(cls, f);
        } else {
            constructor = a(cls, g);
        }
        if (constructor == null) {
            return (T) this.b.create(cls);
        }
        SavedStateHandleController a2 = SavedStateHandleController.a(this.e, this.d, str, this.c);
        if (isAssignableFrom) {
            try {
                t2 = (T) ((he) constructor.newInstance(this.a, a2.a()));
            } catch (IllegalAccessException e2) {
                throw new RuntimeException("Failed to access " + cls, e2);
            } catch (InstantiationException e3) {
                throw new RuntimeException("A " + cls + " cannot be instantiated.", e3);
            } catch (InvocationTargetException e4) {
                throw new RuntimeException("An exception happened in constructor of " + cls, e4.getCause());
            }
        } else {
            t2 = (T) ((he) constructor.newInstance(a2.a()));
        }
        t.setTagIfAbsent("androidx.lifecycle.savedstate.vm.tag", a2);
        return t;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.b, androidx.lifecycle.ViewModelProvider.Factory
    public <T extends he> T create(Class<T> cls) {
        String canonicalName = cls.getCanonicalName();
        if (canonicalName != null) {
            return (T) a(canonicalName, cls);
        }
        throw new IllegalArgumentException("Local and anonymous classes can not be ViewModels");
    }

    @DexIgnore
    public static <T> Constructor<T> a(Class<T> cls, Class<?>[] clsArr) {
        for (Constructor<?> constructor : cls.getConstructors()) {
            Constructor<T> constructor2 = (Constructor<T>) constructor;
            if (Arrays.equals(clsArr, constructor2.getParameterTypes())) {
                return constructor2;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // androidx.lifecycle.ViewModelProvider.d
    public void a(he heVar) {
        SavedStateHandleController.a(heVar, this.e, this.d);
    }
}
