package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cd<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public /* final */ Runnable f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LiveData<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // androidx.lifecycle.LiveData
        public void d() {
            cd cdVar = cd.this;
            cdVar.a.execute(cdVar.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r0v7, resolved type: java.lang.Object */
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: androidx.lifecycle.LiveData<T> */
        /* JADX DEBUG: Multi-variable search result rejected for r0v12, resolved type: java.lang.Object */
        /* JADX WARN: Multi-variable type inference failed */
        public void run() {
            do {
                boolean z = false;
                if (cd.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    boolean z2 = false;
                    while (cd.this.c.compareAndSet(true, false)) {
                        try {
                            obj = cd.this.a();
                            z2 = true;
                        } catch (Throwable th) {
                            cd.this.d.set(false);
                            throw th;
                        }
                    }
                    if (z2) {
                        cd.this.b.a(obj);
                    }
                    cd.this.d.set(false);
                    z = z2;
                }
                if (!z) {
                    return;
                }
            } while (cd.this.c.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            boolean c = cd.this.b.c();
            if (cd.this.c.compareAndSet(false, true) && c) {
                cd cdVar = cd.this;
                cdVar.a.execute(cdVar.e);
            }
        }
    }

    @DexIgnore
    public cd(Executor executor) {
        this.a = executor;
        this.b = new a();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        o3.c().b(this.f);
    }
}
