package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.jx;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gw {
    @DexIgnore
    public /* final */ o00 a;
    @DexIgnore
    public /* final */ e40 b;
    @DexIgnore
    public /* final */ i40 c;
    @DexIgnore
    public /* final */ j40 d;
    @DexIgnore
    public /* final */ kx e;
    @DexIgnore
    public /* final */ g30 f;
    @DexIgnore
    public /* final */ f40 g;
    @DexIgnore
    public /* final */ h40 h; // = new h40();
    @DexIgnore
    public /* final */ g40 i; // = new g40();
    @DexIgnore
    public /* final */ b9<List<Throwable>> j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RuntimeException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a {
        @DexIgnore
        public b() {
            super("Failed to find image header parser.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends a {
        @DexIgnore
        public c(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        @DexIgnore
        public <M> c(M m, List<m00<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + ((Object) m));
        }

        @DexIgnore
        public c(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends a {
        @DexIgnore
        public d(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a {
        @DexIgnore
        public e(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    @DexIgnore
    public gw() {
        b9<List<Throwable>> b2 = w50.b();
        this.j = b2;
        this.a = new o00(b2);
        this.b = new e40();
        this.c = new i40();
        this.d = new j40();
        this.e = new kx();
        this.f = new g30();
        this.g = new f40();
        a(Arrays.asList("Gif", "Bitmap", "BitmapDrawable"));
    }

    @DexIgnore
    public <Data> gw a(Class<Data> cls, vw<Data> vwVar) {
        this.b.a(cls, vwVar);
        return this;
    }

    @DexIgnore
    public <Data, TResource, Transcode> sy<Data, TResource, Transcode> b(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        sy<Data, TResource, Transcode> a2 = this.i.a(cls, cls2, cls3);
        if (this.i.a(a2)) {
            return null;
        }
        if (a2 == null) {
            List<hy<Data, TResource, Transcode>> a3 = a(cls, cls2, cls3);
            if (a3.isEmpty()) {
                a2 = null;
            } else {
                a2 = new sy<>(cls, cls2, cls3, a3, this.j);
            }
            this.i.a(cls, cls2, cls3, a2);
        }
        return a2;
    }

    @DexIgnore
    public <Model, TResource, Transcode> List<Class<?>> c(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2, cls3);
        if (a2 == null) {
            a2 = new ArrayList<>();
            for (Class<?> cls4 : this.a.a((Class<?>) cls)) {
                for (Class<?> cls5 : this.c.b(cls4, cls2)) {
                    if (!this.f.b(cls5, cls3).isEmpty() && !a2.contains(cls5)) {
                        a2.add(cls5);
                    }
                }
            }
            this.h.a(cls, cls2, cls3, Collections.unmodifiableList(a2));
        }
        return a2;
    }

    @DexIgnore
    public <Data, TResource> gw a(Class<Data> cls, Class<TResource> cls2, cx<Data, TResource> cxVar) {
        a("legacy_append", cls, cls2, cxVar);
        return this;
    }

    @DexIgnore
    public <Data, TResource> gw a(String str, Class<Data> cls, Class<TResource> cls2, cx<Data, TResource> cxVar) {
        this.c.a(str, cxVar, cls, cls2);
        return this;
    }

    @DexIgnore
    public final gw a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.a(arrayList);
        return this;
    }

    @DexIgnore
    public boolean b(uy<?> uyVar) {
        return this.d.a(uyVar.d()) != null;
    }

    @DexIgnore
    public <TResource> gw a(Class<TResource> cls, dx<TResource> dxVar) {
        this.d.a(cls, dxVar);
        return this;
    }

    @DexIgnore
    public <X> jx<X> b(X x) {
        return this.e.a((Object) x);
    }

    @DexIgnore
    public gw a(jx.a<?> aVar) {
        this.e.a(aVar);
        return this;
    }

    @DexIgnore
    public <TResource, Transcode> gw a(Class<TResource> cls, Class<Transcode> cls2, f30<TResource, Transcode> f30) {
        this.f.a(cls, cls2, f30);
        return this;
    }

    @DexIgnore
    public gw a(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    @DexIgnore
    public <Model, Data> gw a(Class<Model> cls, Class<Data> cls2, n00<Model, Data> n00) {
        this.a.a(cls, cls2, n00);
        return this;
    }

    @DexIgnore
    public final <Data, TResource, Transcode> List<hy<Data, TResource, Transcode>> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.c.b(cls, cls2)) {
            for (Class cls5 : this.f.b(cls4, cls3)) {
                arrayList.add(new hy(cls, cls4, cls5, this.c.a(cls, cls4), this.f.a(cls4, cls5), this.j));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public <X> vw<X> c(X x) throws e {
        vw<X> a2 = this.b.a(x.getClass());
        if (a2 != null) {
            return a2;
        }
        throw new e(x.getClass());
    }

    @DexIgnore
    public <X> dx<X> a(uy<X> uyVar) throws d {
        dx<X> a2 = this.d.a(uyVar.d());
        if (a2 != null) {
            return a2;
        }
        throw new d(uyVar.d());
    }

    @DexIgnore
    public <Model> List<m00<Model, ?>> a(Model model) {
        return this.a.a((Object) model);
    }

    @DexIgnore
    public List<ImageHeaderParser> a() {
        List<ImageHeaderParser> a2 = this.g.a();
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new b();
    }
}
