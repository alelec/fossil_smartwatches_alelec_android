package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h73 extends wm2 implements g73 {
    @DexIgnore
    public h73() {
        super("com.google.android.gms.maps.internal.IOnInfoWindowClickListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        f(fn2.a(parcel.readStrongBinder()));
        parcel2.writeNoException();
        return true;
    }
}
