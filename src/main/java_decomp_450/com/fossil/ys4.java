package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public xt4 b; // = new xt4(1);
    @DexIgnore
    public gt4 c;
    @DexIgnore
    public ft4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        ee7.a((Object) ys4.class.getSimpleName(), "AllFriendListAdapter::class.java.simpleName");
    }
    */

    @DexIgnore
    public ys4(gr5 gr5) {
        ee7.b(gr5, "listener");
        this.c = new gt4(2, gr5);
        this.d = new ft4(3);
    }

    @DexIgnore
    public final void a(List<? extends Object> list) {
        ee7.b(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void c() {
        this.a.clear();
        notifyDataSetChanged();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.a(this.a, i)) {
            return this.b.a();
        }
        if (this.c.a(this.a, i)) {
            return this.c.a();
        }
        if (this.d.a(this.a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + this.a.get(i));
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == 1) {
            this.b.a(this.a, i, viewHolder);
        } else if (itemViewType == 2) {
            this.c.a(this.a, i, viewHolder);
        } else if (itemViewType == 3) {
            this.d.a(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.a(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.a(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.a(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }

    @DexIgnore
    public final Object a(int i) {
        return this.a.get(i);
    }
}
