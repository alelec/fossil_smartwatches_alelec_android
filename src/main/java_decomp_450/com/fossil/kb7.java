package com.fossil;

import com.fossil.s87;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kb7<T> implements fb7<T>, sb7 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater<kb7<?>, Object> b; // = AtomicReferenceFieldUpdater.newUpdater(kb7.class, Object.class, Constants.RESULT);
    @DexIgnore
    public /* final */ fb7<T> a;
    @DexIgnore
    public volatile Object result;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.fb7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public kb7(fb7<? super T> fb7, Object obj) {
        ee7.b(fb7, "delegate");
        this.a = fb7;
        this.result = obj;
    }

    @DexIgnore
    public final Object a() {
        Object obj = this.result;
        lb7 lb7 = lb7.UNDECIDED;
        if (obj == lb7) {
            if (b.compareAndSet(this, lb7, nb7.a())) {
                return nb7.a();
            }
            obj = this.result;
        }
        if (obj == lb7.RESUMED) {
            return nb7.a();
        }
        if (!(obj instanceof s87.b)) {
            return obj;
        }
        throw ((s87.b) obj).exception;
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public sb7 getCallerFrame() {
        fb7<T> fb7 = this.a;
        if (!(fb7 instanceof sb7)) {
            fb7 = null;
        }
        return (sb7) fb7;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public void resumeWith(Object obj) {
        while (true) {
            Object obj2 = this.result;
            lb7 lb7 = lb7.UNDECIDED;
            if (obj2 == lb7) {
                if (b.compareAndSet(this, lb7, obj)) {
                    return;
                }
            } else if (obj2 != nb7.a()) {
                throw new IllegalStateException("Already resumed");
            } else if (b.compareAndSet(this, nb7.a(), lb7.RESUMED)) {
                this.a.resumeWith(obj);
                return;
            }
        }
    }

    @DexIgnore
    public String toString() {
        return "SafeContinuation for " + this.a;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public kb7(fb7<? super T> fb7) {
        this(fb7, lb7.UNDECIDED);
        ee7.b(fb7, "delegate");
    }
}
