package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx0 extends fe7 implements gd7<zk0, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ wm0[] a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kx0(wm0[] wm0Arr) {
        super(1);
        this.a = wm0Arr;
    }

    @DexIgnore
    public final boolean a(zk0 zk0) {
        return !t97.a(this.a, zk0.y);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ Boolean invoke(zk0 zk0) {
        return Boolean.valueOf(a(zk0));
    }
}
