package com.fossil;

import java.util.concurrent.CountDownLatch;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class z94 implements ho3 {
    @DexIgnore
    public /* final */ CountDownLatch a;

    @DexIgnore
    public z94(CountDownLatch countDownLatch) {
        this.a = countDownLatch;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3 no3) {
        this.a.countDown();
    }
}
