package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ns {
    @DexIgnore
    public static final a a = a.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ /* synthetic */ a a; // = new a();

        @DexIgnore
        public final ns a(ds dsVar, int i) {
            ee7.b(dsVar, "referenceCounter");
            if (i > 0) {
                return new qs(dsVar, i);
            }
            return fs.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ boolean b;
        @DexIgnore
        public /* final */ int c;

        @DexIgnore
        public b(Bitmap bitmap, boolean z, int i) {
            ee7.b(bitmap, "bitmap");
            this.a = bitmap;
            this.b = z;
            this.c = i;
        }

        @DexIgnore
        public final Bitmap a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.c;
        }

        @DexIgnore
        public final boolean c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && this.b == bVar.b && this.c == bVar.c;
        }

        @DexIgnore
        public int hashCode() {
            Bitmap bitmap = this.a;
            int hashCode = (bitmap != null ? bitmap.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            return ((hashCode + i) * 31) + this.c;
        }

        @DexIgnore
        public String toString() {
            return "Value(bitmap=" + this.a + ", isSampled=" + this.b + ", size=" + this.c + ")";
        }
    }

    @DexIgnore
    b a(String str);

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(String str, Bitmap bitmap, boolean z);
}
