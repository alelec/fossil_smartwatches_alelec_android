package com.fossil;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ff7 extends ef7 {
    @DexIgnore
    public /* final */ a c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends ThreadLocal<Random> {
        @DexIgnore
        @Override // java.lang.ThreadLocal
        public Random initialValue() {
            return new Random();
        }
    }

    @DexIgnore
    @Override // com.fossil.ef7
    public Random c() {
        Object obj = this.c.get();
        ee7.a(obj, "implStorage.get()");
        return (Random) obj;
    }
}
