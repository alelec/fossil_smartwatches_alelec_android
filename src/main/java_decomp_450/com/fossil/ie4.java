package com.fossil;

import com.google.gson.JsonElement;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ie4 extends JsonElement {
    @DexIgnore
    public /* final */ cf4<String, JsonElement> a; // = new cf4<>();

    @DexIgnore
    public void a(String str, JsonElement jsonElement) {
        if (jsonElement == null) {
            jsonElement = he4.a;
        }
        this.a.put(str, jsonElement);
    }

    @DexIgnore
    public de4 b(String str) {
        return (de4) this.a.get(str);
    }

    @DexIgnore
    public ie4 c(String str) {
        return (ie4) this.a.get(str);
    }

    @DexIgnore
    public boolean d(String str) {
        return this.a.containsKey(str);
    }

    @DexIgnore
    public Set<Map.Entry<String, JsonElement>> entrySet() {
        return this.a.entrySet();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof ie4) && ((ie4) obj).a.equals(this.a));
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    public void a(String str, String str2) {
        a(str, a((Object) str2));
    }

    @DexIgnore
    public void a(String str, Number number) {
        a(str, a(number));
    }

    @DexIgnore
    public void a(String str, Boolean bool) {
        a(str, a(bool));
    }

    @DexIgnore
    public final JsonElement a(Object obj) {
        return obj == null ? he4.a : new le4(obj);
    }

    @DexIgnore
    public JsonElement a(String str) {
        return this.a.get(str);
    }
}
