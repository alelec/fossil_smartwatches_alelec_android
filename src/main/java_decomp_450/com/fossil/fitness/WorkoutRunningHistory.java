package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class WorkoutRunningHistory implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutRunningHistory> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mDuration;
    @DexIgnore
    public /* final */ int mStart;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<WorkoutRunningHistory> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutRunningHistory createFromParcel(Parcel parcel) {
            return new WorkoutRunningHistory(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WorkoutRunningHistory[] newArray(int i) {
            return new WorkoutRunningHistory[i];
        }
    }

    @DexIgnore
    public WorkoutRunningHistory(int i, int i2) {
        this.mStart = i;
        this.mDuration = i2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof WorkoutRunningHistory)) {
            return false;
        }
        WorkoutRunningHistory workoutRunningHistory = (WorkoutRunningHistory) obj;
        if (this.mStart == workoutRunningHistory.mStart && this.mDuration == workoutRunningHistory.mDuration) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public int getStart() {
        return this.mStart;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.mStart) * 31) + this.mDuration;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutRunningHistory{mStart=" + this.mStart + ",mDuration=" + this.mDuration + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStart);
        parcel.writeInt(this.mDuration);
    }

    @DexIgnore
    public WorkoutRunningHistory(Parcel parcel) {
        this.mStart = parcel.readInt();
        this.mDuration = parcel.readInt();
    }
}
