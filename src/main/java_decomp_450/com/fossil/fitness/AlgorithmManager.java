package com.fossil.fitness;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class AlgorithmManager {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends AlgorithmManager {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public static native AlgorithmManager defaultManager();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native void native_clearCache(long j);

        @DexIgnore
        private native DataCaching native_getDataCaching(long j);

        @DexIgnore
        private native boolean native_getDebugEnabled(long j);

        @DexIgnore
        private native UserProfile native_getUserProfile(long j);

        @DexIgnore
        private native void native_setDataCaching(long j, DataCaching dataCaching);

        @DexIgnore
        private native void native_setDebugEnabled(long j, boolean z);

        @DexIgnore
        private native void native_setUserProfile(long j, UserProfile userProfile);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public void clearCache() {
            native_clearCache(this.nativeRef);
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public DataCaching getDataCaching() {
            return native_getDataCaching(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public boolean getDebugEnabled() {
            return native_getDebugEnabled(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public UserProfile getUserProfile() {
            return native_getUserProfile(this.nativeRef);
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public void setDataCaching(DataCaching dataCaching) {
            native_setDataCaching(this.nativeRef, dataCaching);
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public void setDebugEnabled(boolean z) {
            native_setDebugEnabled(this.nativeRef, z);
        }

        @DexIgnore
        @Override // com.fossil.fitness.AlgorithmManager
        public void setUserProfile(UserProfile userProfile) {
            native_setUserProfile(this.nativeRef, userProfile);
        }
    }

    @DexIgnore
    public static AlgorithmManager defaultManager() {
        return CppProxy.defaultManager();
    }

    @DexIgnore
    public abstract void clearCache();

    @DexIgnore
    public abstract DataCaching getDataCaching();

    @DexIgnore
    public abstract boolean getDebugEnabled();

    @DexIgnore
    public abstract UserProfile getUserProfile();

    @DexIgnore
    public abstract void setDataCaching(DataCaching dataCaching);

    @DexIgnore
    public abstract void setDebugEnabled(boolean z);

    @DexIgnore
    public abstract void setUserProfile(UserProfile userProfile);
}
