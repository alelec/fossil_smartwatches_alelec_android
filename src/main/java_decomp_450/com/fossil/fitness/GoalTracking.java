package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTracking implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<GoalTracking> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ byte mGoalId;
    @DexIgnore
    public /* final */ int mTimestamp;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<GoalTracking> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public GoalTracking createFromParcel(Parcel parcel) {
            return new GoalTracking(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public GoalTracking[] newArray(int i) {
            return new GoalTracking[i];
        }
    }

    @DexIgnore
    public GoalTracking(int i, int i2, byte b) {
        this.mTimestamp = i;
        this.mTimezoneOffsetInSecond = i2;
        this.mGoalId = b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof GoalTracking)) {
            return false;
        }
        GoalTracking goalTracking = (GoalTracking) obj;
        if (this.mTimestamp == goalTracking.mTimestamp && this.mTimezoneOffsetInSecond == goalTracking.mTimezoneOffsetInSecond && this.mGoalId == goalTracking.mGoalId) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public byte getGoalId() {
        return this.mGoalId;
    }

    @DexIgnore
    public int getTimestamp() {
        return this.mTimestamp;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mTimestamp) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mGoalId;
    }

    @DexIgnore
    public String toString() {
        return "GoalTracking{mTimestamp=" + this.mTimestamp + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mGoalId=" + ((int) this.mGoalId) + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mTimestamp);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeByte(this.mGoalId);
    }

    @DexIgnore
    public GoalTracking(Parcel parcel) {
        this.mTimestamp = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mGoalId = parcel.readByte();
    }
}
