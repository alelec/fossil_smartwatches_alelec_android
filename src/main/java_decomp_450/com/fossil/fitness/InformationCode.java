package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum InformationCode {
    UNKNOWN(0),
    SOFTWARE_RESET(1),
    HARDWARE_RESET(2),
    TIME_CHANGED(3),
    FIRST_MINUTE(4),
    OUT_OF_BATTERY(5);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public InformationCode(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
