package com.fossil;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ut7 {
    @DexIgnore
    public static volatile int a;
    @DexIgnore
    public static /* final */ fu7 b; // = new fu7();
    @DexIgnore
    public static /* final */ cu7 c; // = new cu7();
    @DexIgnore
    public static /* final */ String[] d; // = {"1.6", "1.7"};
    @DexIgnore
    public static String e; // = "org/slf4j/impl/StaticLoggerBinder.class";

    /*
    static {
        gu7.b("slf4j.detectLoggerNameMismatch");
    }
    */

    @DexIgnore
    public static final void a() {
        Set<URL> set = null;
        try {
            if (!f()) {
                set = c();
                c(set);
            }
            hu7.c();
            a = 3;
            b(set);
            d();
            h();
            b.a();
        } catch (NoClassDefFoundError e2) {
            if (b(e2.getMessage())) {
                a = 4;
                gu7.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                gu7.a("Defaulting to no-operation (NOP) logger implementation");
                gu7.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            a(e2);
            throw e2;
        } catch (NoSuchMethodError e3) {
            String message = e3.getMessage();
            if (message != null && message.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                a = 2;
                gu7.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                gu7.a("Your binding is version 1.5.5 or earlier.");
                gu7.a("Upgrade your binding to version 1.6.x.");
            }
            throw e3;
        } catch (Exception e4) {
            a(e4);
            throw new IllegalStateException("Unexpected initialization failure", e4);
        }
    }

    @DexIgnore
    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        return str.contains("org/slf4j/impl/StaticLoggerBinder") || str.contains("org.slf4j.impl.StaticLoggerBinder");
    }

    @DexIgnore
    public static Set<URL> c() {
        Enumeration<URL> enumeration;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            ClassLoader classLoader = ut7.class.getClassLoader();
            if (classLoader == null) {
                enumeration = ClassLoader.getSystemResources(e);
            } else {
                enumeration = classLoader.getResources(e);
            }
            while (enumeration.hasMoreElements()) {
                linkedHashSet.add(enumeration.nextElement());
            }
        } catch (IOException e2) {
            gu7.a("Error getting resources from path", e2);
        }
        return linkedHashSet;
    }

    @DexIgnore
    public static void d() {
        synchronized (b) {
            b.d();
            for (eu7 eu7 : b.c()) {
                eu7.a(a(eu7.c()));
            }
        }
    }

    @DexIgnore
    public static st7 e() {
        if (a == 0) {
            synchronized (ut7.class) {
                if (a == 0) {
                    a = 1;
                    g();
                }
            }
        }
        int i = a;
        if (i == 1) {
            return b;
        }
        if (i == 2) {
            throw new IllegalStateException("org.slf4j.LoggerFactory in failed state. Original exception was thrown EARLIER. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
        } else if (i == 3) {
            return hu7.c().a();
        } else {
            if (i == 4) {
                return c;
            }
            throw new IllegalStateException("Unreachable code");
        }
    }

    @DexIgnore
    public static boolean f() {
        String c2 = gu7.c("java.vendor.url");
        if (c2 == null) {
            return false;
        }
        return c2.toLowerCase().contains("android");
    }

    @DexIgnore
    public static final void g() {
        a();
        if (a == 3) {
            i();
        }
    }

    @DexIgnore
    public static void h() {
        LinkedBlockingQueue<zt7> b2 = b.b();
        int size = b2.size();
        ArrayList<zt7> arrayList = new ArrayList(128);
        int i = 0;
        while (b2.drainTo(arrayList, 128) != 0) {
            for (zt7 zt7 : arrayList) {
                a(zt7);
                int i2 = i + 1;
                if (i == 0) {
                    a(zt7, size);
                }
                i = i2;
            }
            arrayList.clear();
        }
    }

    @DexIgnore
    public static final void i() {
        try {
            String str = hu7.c;
            boolean z = false;
            for (String str2 : d) {
                if (str.startsWith(str2)) {
                    z = true;
                }
            }
            if (!z) {
                gu7.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                gu7.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError unused) {
        } catch (Throwable th) {
            gu7.a("Unexpected problem occured during version sanity check", th);
        }
    }

    @DexIgnore
    public static void b() {
        gu7.a("The following set of substitute loggers may have been accessed");
        gu7.a("during the initialization phase. Logging calls during this");
        gu7.a("phase were not honored. However, subsequent logging calls to these");
        gu7.a("loggers will work as normally expected.");
        gu7.a("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    @DexIgnore
    public static void b(Set<URL> set) {
        if (set != null && a(set)) {
            gu7.a("Actual binding is of type [" + hu7.c().b() + "]");
        }
    }

    @DexIgnore
    public static void c(Set<URL> set) {
        if (a(set)) {
            gu7.a("Class path contains multiple SLF4J bindings.");
            Iterator<URL> it = set.iterator();
            while (it.hasNext()) {
                gu7.a("Found binding in [" + it.next() + "]");
            }
            gu7.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    @DexIgnore
    public static void a(Throwable th) {
        a = 2;
        gu7.a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    @DexIgnore
    public static void a(zt7 zt7, int i) {
        if (zt7.a().d()) {
            a(i);
        } else if (!zt7.a().e()) {
            b();
        }
    }

    @DexIgnore
    public static void a(zt7 zt7) {
        if (zt7 != null) {
            eu7 a2 = zt7.a();
            String c2 = a2.c();
            if (a2.f()) {
                throw new IllegalStateException("Delegate logger cannot be null at this state.");
            } else if (!a2.e()) {
                if (a2.d()) {
                    a2.a(zt7);
                } else {
                    gu7.a(c2);
                }
            }
        }
    }

    @DexIgnore
    public static void a(int i) {
        gu7.a("A number (" + i + ") of logging calls during the initialization phase have been intercepted and are");
        gu7.a("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        gu7.a("See also http://www.slf4j.org/codes.html#replay");
    }

    @DexIgnore
    public static boolean a(Set<URL> set) {
        return set.size() > 1;
    }

    @DexIgnore
    public static tt7 a(String str) {
        return e().a(str);
    }
}
