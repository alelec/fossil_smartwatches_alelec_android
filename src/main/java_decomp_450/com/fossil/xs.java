package com.fossil;

import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs extends RuntimeException {
    @DexIgnore
    public /* final */ Response response;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xs(Response response2) {
        super("HTTP " + response2.e() + ": " + response2.n());
        ee7.b(response2, "response");
        this.response = response2;
    }

    @DexIgnore
    public final Response getResponse() {
        return this.response;
    }
}
