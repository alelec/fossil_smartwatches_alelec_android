package com.fossil;

import android.text.TextUtils;
import com.fossil.lo7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj5 implements Interceptor {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public Auth b;
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return hj5.f;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.retrofit.AuthenticationInterceptor$intercept$1", f = "AuthenticationInterceptor.kt", l = {}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $currentUser;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(MFUser mFUser, fb7 fb7) {
            super(2, fb7);
            this.$currentUser = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.$currentUser, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                UserDatabase a = pg5.i.a();
                if (!(a == null || (userDao = a.userDao()) == null)) {
                    userDao.updateUser(this.$currentUser);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = hj5.class.getSimpleName();
        ee7.a((Object) simpleName, "AuthenticationInterceptor::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public hj5(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, ch5 ch5) {
        ee7.b(portfolioApp, "mApplication");
        ee7.b(authApiGuestService, "mAuthApiGuestService");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = authApiGuestService;
        this.e = ch5;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        MFUser.Auth auth;
        UserDao userDao;
        ee7.b(chain, "chain");
        lo7.a f2 = chain.c().f();
        UserDatabase a2 = pg5.i.a();
        MFUser currentUser = (a2 == null || (userDao = a2.userDao()) == null) ? null : userDao.getCurrentUser();
        String accessToken = (currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
        if (!TextUtils.isEmpty(accessToken) && currentUser != null) {
            long currentTimeMillis = (System.currentTimeMillis() - this.e.d()) / ((long) 1000);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "loginDurationInSeconds = " + currentTimeMillis + " tokenExpiresIn " + currentUser.getAuth().getAccessTokenExpiresIn());
            if (currentTimeMillis >= ((long) currentUser.getAuth().getAccessTokenExpiresIn())) {
                int i = 0;
                Auth a3 = a(currentUser, this.a.getAndIncrement() == 0);
                if (this.a.decrementAndGet() == 0) {
                    this.b = null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = f;
                local2.d(str2, "intercept decrementAndGet mCount=" + this.a);
                if ((a3 != null ? a3.getAccessToken() : null) != null) {
                    MFUser.Auth auth2 = currentUser.getAuth();
                    String accessToken2 = a3.getAccessToken();
                    if (accessToken2 != null) {
                        auth2.setAccessToken(accessToken2);
                        MFUser.Auth auth3 = currentUser.getAuth();
                        String refreshToken = a3.getRefreshToken();
                        if (refreshToken != null) {
                            auth3.setRefreshToken(refreshToken);
                            MFUser.Auth auth4 = currentUser.getAuth();
                            String y = zd5.y(a3.getAccessTokenExpiresAt());
                            ee7.a((Object) y, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                            auth4.setAccessTokenExpiresAt(y);
                            MFUser.Auth auth5 = currentUser.getAuth();
                            Integer accessTokenExpiresIn = a3.getAccessTokenExpiresIn();
                            if (accessTokenExpiresIn != null) {
                                i = accessTokenExpiresIn.intValue();
                            }
                            auth5.setAccessTokenExpiresIn(i);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = f;
                            local3.d(str3, "accessToken = " + a3.getAccessToken() + ", refreshToken = " + a3.getRefreshToken());
                            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(currentUser, null), 3, null);
                            this.e.y(a3.getAccessToken());
                            this.e.a(System.currentTimeMillis());
                            f2.a("Authorization", "Bearer " + a3.getAccessToken());
                            f2.a();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Bearer ");
                    if (accessToken != null) {
                        sb.append(accessToken);
                        f2.a("Authorization", sb.toString());
                        f2.a();
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bearer ");
                if (accessToken != null) {
                    sb2.append(accessToken);
                    f2.a("Authorization", sb2.toString());
                    f2.a();
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        f2.a("Content-Type", Constants.CONTENT_TYPE);
        f2.a("User-Agent", ab5.b.a());
        f2.a("locale", this.c.l());
        try {
            Response a4 = chain.a(f2.a());
            ee7.a((Object) a4, "chain.proceed(requestBuilder.build())");
            return a4;
        } catch (Exception e2) {
            String str4 = "";
            if (e2 instanceof ServerErrorException) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = f;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("exception=");
                ServerErrorException serverErrorException = (ServerErrorException) e2;
                sb3.append(serverErrorException.getServerError());
                local4.d(str5, sb3.toString());
                serverError = serverErrorException.getServerError();
            } else if (e2 instanceof UnknownHostException) {
                serverError = new ServerError(601, str4);
            } else if (e2 instanceof SocketTimeoutException) {
                serverError = new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, str4);
            } else {
                serverError = new ServerError(600, str4);
            }
            Response.a aVar = new Response.a();
            aVar.a(f2.a());
            aVar.a(jo7.HTTP_1_1);
            Integer code = serverError.getCode();
            ee7.a((Object) code, "serverError.code");
            aVar.a(code.intValue());
            String message = serverError.getMessage();
            if (message != null) {
                str4 = message;
            }
            aVar.a(str4);
            aVar.a(mo7.create(ho7.b(com.zendesk.sdk.network.Constants.APPLICATION_JSON), new Gson().a(serverError)));
            aVar.a("Content-Type", Constants.CONTENT_TYPE);
            aVar.a("User-Agent", ab5.b.a());
            aVar.a("Locale", this.c.l());
            Response a5 = aVar.a();
            ee7.a((Object) a5, "Response.Builder()\n     \u2026                 .build()");
            return a5;
        }
    }

    @DexIgnore
    public final synchronized Auth a(MFUser mFUser, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = f;
        local.d(str, "getAuth needProcess=" + z + " mCount=" + this.a);
        if (z) {
            ie4 ie4 = new ie4();
            ie4.a(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
            try {
                fv7<Auth> a2 = this.d.tokenRefresh(ie4).a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = lj5.g.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Refresh Token error=");
                sb.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                sb.append(" body=");
                sb.append(a2 != null ? a2.c() : null);
                local2.d(a3, sb.toString());
                this.b = a2.a();
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str2 = f;
                local3.d(str2, "getAuth needProcess=" + z + " mCount=" + this.a + " exception=" + e2.getMessage());
                e2.printStackTrace();
                return null;
            }
        }
        return this.b;
    }
}
