package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.GraphRequest;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.jt1;
import com.fossil.ku1;
import com.fossil.tt1;
import com.fossil.ut1;
import com.fossil.vt1;
import com.fossil.xt1;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.zendesk.sdk.network.Constants;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bu1 implements jv1 {
    @DexIgnore
    public /* final */ r84 a;
    @DexIgnore
    public /* final */ ConnectivityManager b;
    @DexIgnore
    public /* final */ URL c; // = a(it1.c);
    @DexIgnore
    public /* final */ by1 d;
    @DexIgnore
    public /* final */ by1 e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ URL a;
        @DexIgnore
        public /* final */ st1 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public a(URL url, st1 st1, String str) {
            this.a = url;
            this.b = st1;
            this.c = str;
        }

        @DexIgnore
        public a a(URL url) {
            return new a(url, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ URL b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public b(int i, URL url, long j) {
            this.a = i;
            this.b = url;
            this.c = j;
        }
    }

    @DexIgnore
    public bu1(Context context, by1 by1, by1 by12) {
        d94 d94 = new d94();
        d94.a(kt1.a);
        d94.a(true);
        this.a = d94.a();
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.d = by12;
        this.e = by1;
        this.f = 40000;
    }

    @DexIgnore
    @Override // com.fossil.jv1
    public ku1 a(ku1 ku1) {
        int i;
        int i2;
        NetworkInfo activeNetworkInfo = this.b.getActiveNetworkInfo();
        ku1.a h = ku1.h();
        h.a("sdk-version", Build.VERSION.SDK_INT);
        h.a(DeviceRequestsHelper.DEVICE_INFO_MODEL, Build.MODEL);
        h.a("hardware", Build.HARDWARE);
        h.a("device", Build.DEVICE);
        h.a("product", Build.PRODUCT);
        h.a("os-uild", Build.ID);
        h.a("manufacturer", Build.MANUFACTURER);
        h.a("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        h.a("tz-offset", (long) (TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        if (activeNetworkInfo == null) {
            i = xt1.c.zzs.zza();
        } else {
            i = activeNetworkInfo.getType();
        }
        h.a("net-type", i);
        if (activeNetworkInfo == null) {
            i2 = xt1.b.zza.zza();
        } else {
            i2 = activeNetworkInfo.getSubtype();
            if (i2 == -1) {
                i2 = xt1.b.zzu.zza();
            } else if (xt1.b.zza(i2) == null) {
                i2 = 0;
            }
        }
        h.a("mobile-subtype", i2);
        return h.a();
    }

    @DexIgnore
    @Override // com.fossil.jv1
    public dv1 a(cv1 cv1) {
        ut1.a aVar;
        HashMap hashMap = new HashMap();
        for (ku1 ku1 : cv1.a()) {
            String f2 = ku1.f();
            if (!hashMap.containsKey(f2)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(ku1);
                hashMap.put(f2, arrayList);
            } else {
                ((List) hashMap.get(f2)).add(ku1);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry entry : hashMap.entrySet()) {
            ku1 ku12 = (ku1) ((List) entry.getValue()).get(0);
            vt1.a h = vt1.h();
            h.a(yt1.zza);
            h.a(this.e.a());
            h.b(this.d.a());
            tt1.a c2 = tt1.c();
            c2.a(tt1.b.zzb);
            jt1.a i = jt1.i();
            i.a(Integer.valueOf(ku12.b("sdk-version")));
            i.e(ku12.a(DeviceRequestsHelper.DEVICE_INFO_MODEL));
            i.c(ku12.a("hardware"));
            i.a(ku12.a("device"));
            i.g(ku12.a("product"));
            i.f(ku12.a("os-uild"));
            i.d(ku12.a("manufacturer"));
            i.b(ku12.a("fingerprint"));
            c2.a(i.a());
            h.a(c2.a());
            try {
                h.a(Integer.parseInt((String) entry.getKey()));
            } catch (NumberFormatException unused) {
                h.b((String) entry.getKey());
            }
            ArrayList arrayList3 = new ArrayList();
            for (ku1 ku13 : (List) entry.getValue()) {
                ju1 c3 = ku13.c();
                bt1 b2 = c3.b();
                if (b2.equals(bt1.a("proto"))) {
                    aVar = ut1.a(c3.a());
                } else if (b2.equals(bt1.a("json"))) {
                    aVar = ut1.a(new String(c3.a(), Charset.forName("UTF-8")));
                } else {
                    kv1.b("CctTransportBackend", "Received event of unsupported encoding %s. Skipping...", b2);
                }
                aVar.a(ku13.d());
                aVar.b(ku13.g());
                aVar.c(ku13.c("tz-offset"));
                xt1.a c4 = xt1.c();
                c4.a(xt1.c.zza(ku13.b("net-type")));
                c4.a(xt1.b.zza(ku13.b("mobile-subtype")));
                aVar.a(c4.a());
                if (ku13.b() != null) {
                    aVar.a(ku13.b());
                }
                arrayList3.add(aVar.a());
            }
            h.a(arrayList3);
            arrayList2.add(h.a());
        }
        st1 a2 = st1.a(arrayList2);
        String str = null;
        URL url = this.c;
        if (cv1.b() != null) {
            try {
                it1 a3 = it1.a(cv1.b());
                if (a3.c() != null) {
                    str = a3.c();
                }
                if (a3.d() != null) {
                    url = a(a3.d());
                }
            } catch (IllegalArgumentException unused2) {
                return dv1.c();
            }
        }
        try {
            b bVar = (b) mv1.a(5, new a(url, a2, str), zt1.a(this), au1.a());
            if (bVar.a == 200) {
                return dv1.a(bVar.c);
            }
            int i2 = bVar.a;
            if (i2 < 500) {
                if (i2 != 404) {
                    return dv1.c();
                }
            }
            return dv1.d();
        } catch (IOException e2) {
            kv1.a("CctTransportBackend", "Could not make request to the backend", (Throwable) e2);
            return dv1.d();
        }
    }

    @DexIgnore
    public static URL a(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e2) {
            throw new IllegalArgumentException("Invalid url: " + str, e2);
        }
    }

    @DexIgnore
    public final b a(a aVar) throws IOException {
        kv1.a("CctTransportBackend", "Making request to: %s", aVar.a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) aVar.a.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.f);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("User-Agent", String.format("datatransport/%s android/", "2.3.0"));
        httpURLConnection.setRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.setRequestProperty("Content-Type", Constants.APPLICATION_JSON);
        httpURLConnection.setRequestProperty("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        String str = aVar.c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        try {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            try {
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(outputStream);
                try {
                    this.a.a(aVar.b, new BufferedWriter(new OutputStreamWriter(gZIPOutputStream)));
                    gZIPOutputStream.close();
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    int responseCode = httpURLConnection.getResponseCode();
                    kv1.a("CctTransportBackend", "Status Code: " + responseCode);
                    kv1.a("CctTransportBackend", "Content-Type: " + httpURLConnection.getHeaderField("Content-Type"));
                    kv1.a("CctTransportBackend", "Content-Encoding: " + httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER));
                    if (responseCode == 302 || responseCode == 301 || responseCode == 307) {
                        return new b(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0);
                    }
                    if (responseCode != 200) {
                        return new b(responseCode, null, 0);
                    }
                    InputStream inputStream = httpURLConnection.getInputStream();
                    try {
                        InputStream gZIPInputStream = DecompressionHelper.GZIP_ENCODING.equals(httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER)) ? new GZIPInputStream(inputStream) : inputStream;
                        try {
                            b bVar = new b(responseCode, null, wt1.a(new BufferedReader(new InputStreamReader(gZIPInputStream))).a());
                            if (gZIPInputStream != null) {
                                gZIPInputStream.close();
                            }
                            if (inputStream != null) {
                                inputStream.close();
                            }
                            return bVar;
                        } catch (Throwable unused) {
                        }
                    } catch (Throwable unused2) {
                    }
                } catch (Throwable unused3) {
                }
                throw th;
                throw th;
                throw th;
                throw th;
            } catch (Throwable unused4) {
            }
        } catch (ConnectException | UnknownHostException e2) {
            kv1.a("CctTransportBackend", "Couldn't open connection, returning with 500", e2);
            return new b(500, null, 0);
        } catch (t84 | IOException e3) {
            kv1.a("CctTransportBackend", "Couldn't encode request, returning with 400", e3);
            return new b(MFNetworkReturnCode.BAD_REQUEST, null, 0);
        }
    }

    @DexIgnore
    public static /* synthetic */ a a(a aVar, b bVar) {
        URL url = bVar.b;
        if (url == null) {
            return null;
        }
        kv1.a("CctTransportBackend", "Following redirect to: %s", url);
        return aVar.a(bVar.b);
    }
}
