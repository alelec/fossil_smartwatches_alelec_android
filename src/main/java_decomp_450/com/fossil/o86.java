package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o86 implements Factory<n86> {
    @DexIgnore
    public static n86 a(l86 l86, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        return new n86(l86, summariesRepository, fitnessDataRepository, userRepository, pj4);
    }
}
