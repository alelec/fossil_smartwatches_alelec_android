package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qi0 {
    ALARM((byte) 0),
    REMINDER((byte) 1);
    
    @DexIgnore
    public static /* final */ ns1 e; // = new ns1(null);
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public qi0(byte b) {
        this.a = b;
    }
}
