package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import java.lang.ref.WeakReference;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qf<T> extends AbstractList<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ c<T> c;
    @DexIgnore
    public /* final */ f d;
    @DexIgnore
    public /* final */ sf<T> e;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public T g; // = null;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public boolean j; // = false;
    @DexIgnore
    public int p; // = Integer.MAX_VALUE;
    @DexIgnore
    public int q; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public /* final */ AtomicBoolean r; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ArrayList<WeakReference<e>> s; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public a(boolean z, boolean z2, boolean z3) {
            this.a = z;
            this.b = z2;
            this.c = z3;
        }

        @DexIgnore
        public void run() {
            if (this.a) {
                qf.this.c.a();
            }
            if (this.b) {
                qf.this.i = true;
            }
            if (this.c) {
                qf.this.j = true;
            }
            qf.this.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ boolean b;

        @DexIgnore
        public b(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public void run() {
            qf.this.a(this.a, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<T> {
        @DexIgnore
        public abstract void a();

        @DexIgnore
        public abstract void a(T t);

        @DexIgnore
        public abstract void b(T t);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<Key, Value> {
        @DexIgnore
        public /* final */ lf<Key, Value> a;
        @DexIgnore
        public /* final */ f b;
        @DexIgnore
        public Executor c;
        @DexIgnore
        public Executor d;
        @DexIgnore
        public c e;
        @DexIgnore
        public Key f;

        @DexIgnore
        public d(lf<Key, Value> lfVar, f fVar) {
            if (lfVar == null) {
                throw new IllegalArgumentException("DataSource may not be null");
            } else if (fVar != null) {
                this.a = lfVar;
                this.b = fVar;
            } else {
                throw new IllegalArgumentException("Config may not be null");
            }
        }

        @DexIgnore
        public d<Key, Value> a(Executor executor) {
            this.d = executor;
            return this;
        }

        @DexIgnore
        public d<Key, Value> b(Executor executor) {
            this.c = executor;
            return this;
        }

        @DexIgnore
        public d<Key, Value> a(c cVar) {
            this.e = cVar;
            return this;
        }

        @DexIgnore
        public d<Key, Value> a(Key key) {
            this.f = key;
            return this;
        }

        @DexIgnore
        public qf<Value> a() {
            Executor executor = this.c;
            if (executor != null) {
                Executor executor2 = this.d;
                if (executor2 != null) {
                    return qf.a(this.a, executor, executor2, this.e, this.b, this.f);
                }
                throw new IllegalArgumentException("BackgroundThreadExecutor required");
            }
            throw new IllegalArgumentException("MainThreadExecutor required");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e {
        @DexIgnore
        public abstract void a(int i, int i2);

        @DexIgnore
        public abstract void b(int i, int i2);

        @DexIgnore
        public abstract void c(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ boolean c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ int e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public int a; // = -1;
            @DexIgnore
            public int b; // = -1;
            @DexIgnore
            public int c; // = -1;
            @DexIgnore
            public boolean d; // = true;
            @DexIgnore
            public int e; // = Integer.MAX_VALUE;

            @DexIgnore
            public a a(boolean z) {
                this.d = z;
                return this;
            }

            @DexIgnore
            public a b(int i) {
                if (i >= 1) {
                    this.a = i;
                    return this;
                }
                throw new IllegalArgumentException("Page size must be a positive number");
            }

            @DexIgnore
            public a c(int i) {
                this.b = i;
                return this;
            }

            @DexIgnore
            public a a(int i) {
                this.c = i;
                return this;
            }

            @DexIgnore
            public f a() {
                if (this.b < 0) {
                    this.b = this.a;
                }
                if (this.c < 0) {
                    this.c = this.a * 3;
                }
                if (this.d || this.b != 0) {
                    int i = this.e;
                    if (i == Integer.MAX_VALUE || i >= this.a + (this.b * 2)) {
                        return new f(this.a, this.b, this.d, this.c, this.e);
                    }
                    throw new IllegalArgumentException("Maximum size must be at least pageSize + 2*prefetchDist, pageSize=" + this.a + ", prefetchDist=" + this.b + ", maxSize=" + this.e);
                }
                throw new IllegalArgumentException("Placeholders and prefetch are the only ways to trigger loading of more data in the PagedList, so either placeholders must be enabled, or prefetch distance must be > 0.");
            }
        }

        @DexIgnore
        public f(int i, int i2, boolean z, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = z;
            this.e = i3;
            this.d = i4;
        }
    }

    @DexIgnore
    public qf(sf<T> sfVar, Executor executor, Executor executor2, c<T> cVar, f fVar) {
        this.e = sfVar;
        this.a = executor;
        this.b = executor2;
        this.c = cVar;
        this.d = fVar;
        this.h = (fVar.b * 2) + fVar.a;
    }

    @DexIgnore
    public static <K, T> qf<T> a(lf<K, T> lfVar, Executor executor, Executor executor2, c<T> cVar, f fVar, K k) {
        int i2;
        if (lfVar.isContiguous() || !fVar.c) {
            if (!lfVar.isContiguous()) {
                lfVar = ((uf) lfVar).wrapAsContiguousWithoutPlaceholders();
                if (k != null) {
                    i2 = k.intValue();
                    return new kf((jf) lfVar, executor, executor2, cVar, fVar, k, i2);
                }
            }
            i2 = -1;
            return new kf((jf) lfVar, executor, executor2, cVar, fVar, k, i2);
        }
        return new wf((uf) lfVar, executor, executor2, cVar, fVar, k != null ? k.intValue() : 0);
    }

    @DexIgnore
    public abstract void a(qf<T> qfVar, e eVar);

    @DexIgnore
    public void c(int i2) {
        if (i2 < 0 || i2 >= size()) {
            throw new IndexOutOfBoundsException("Index: " + i2 + ", Size: " + size());
        }
        this.f = f() + i2;
        d(i2);
        this.p = Math.min(this.p, i2);
        this.q = Math.max(this.q, i2);
        a(true);
    }

    @DexIgnore
    public abstract lf<?, T> d();

    @DexIgnore
    public abstract void d(int i2);

    @DexIgnore
    public void d(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.s.size() - 1; size >= 0; size--) {
                e eVar = this.s.get(size).get();
                if (eVar != null) {
                    eVar.a(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public abstract Object e();

    @DexIgnore
    public void e(int i2) {
        this.f += i2;
        this.p += i2;
        this.q += i2;
    }

    @DexIgnore
    public int f() {
        return this.e.j();
    }

    @DexIgnore
    public abstract boolean g();

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public T get(int i2) {
        T t = this.e.get(i2);
        if (t != null) {
            this.g = t;
        }
        return t;
    }

    @DexIgnore
    public boolean h() {
        return this.r.get();
    }

    @DexIgnore
    public boolean i() {
        return h();
    }

    @DexIgnore
    public List<T> j() {
        if (i()) {
            return this;
        }
        return new vf(this);
    }

    @DexIgnore
    public int size() {
        return this.e.size();
    }

    @DexIgnore
    public void f(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.s.size() - 1; size >= 0; size--) {
                e eVar = this.s.get(size).get();
                if (eVar != null) {
                    eVar.c(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void e(int i2, int i3) {
        if (i3 != 0) {
            for (int size = this.s.size() - 1; size >= 0; size--) {
                e eVar = this.s.get(size).get();
                if (eVar != null) {
                    eVar.b(i2, i3);
                }
            }
        }
    }

    @DexIgnore
    public void c() {
        this.r.set(true);
    }

    @DexIgnore
    public void a(boolean z, boolean z2, boolean z3) {
        if (this.c != null) {
            if (this.p == Integer.MAX_VALUE) {
                this.p = this.e.size();
            }
            if (this.q == Integer.MIN_VALUE) {
                this.q = 0;
            }
            if (z || z2 || z3) {
                this.a.execute(new a(z, z2, z3));
                return;
            }
            return;
        }
        throw new IllegalStateException("Can't defer BoundaryCallback, no instance");
    }

    @DexIgnore
    public void a(boolean z) {
        boolean z2 = true;
        boolean z3 = this.i && this.p <= this.d.b;
        if (!this.j || this.q < (size() - 1) - this.d.b) {
            z2 = false;
        }
        if (z3 || z2) {
            if (z3) {
                this.i = false;
            }
            if (z2) {
                this.j = false;
            }
            if (z) {
                this.a.execute(new b(z3, z2));
            } else {
                a(z3, z2);
            }
        }
    }

    @DexIgnore
    public void a(boolean z, boolean z2) {
        if (z) {
            this.c.b(this.e.c());
        }
        if (z2) {
            this.c.a(this.e.d());
        }
    }

    @DexIgnore
    public void a(List<T> list, e eVar) {
        if (!(list == null || list == this)) {
            if (!list.isEmpty()) {
                a((qf) ((qf) list), eVar);
            } else if (!this.e.isEmpty()) {
                eVar.b(0, this.e.size());
            }
        }
        for (int size = this.s.size() - 1; size >= 0; size--) {
            if (this.s.get(size).get() == null) {
                this.s.remove(size);
            }
        }
        this.s.add(new WeakReference<>(eVar));
    }

    @DexIgnore
    public void a(e eVar) {
        for (int size = this.s.size() - 1; size >= 0; size--) {
            e eVar2 = this.s.get(size).get();
            if (eVar2 == null || eVar2 == eVar) {
                this.s.remove(size);
            }
        }
    }
}
