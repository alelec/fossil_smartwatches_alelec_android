package com.fossil;

import android.annotation.SuppressLint;
import com.fossil.uz;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tz extends r50<yw, uy<?>> implements uz {
    @DexIgnore
    public uz.a d;

    @DexIgnore
    public tz(long j) {
        super(j);
    }

    @DexIgnore
    @Override // com.fossil.uz
    public /* bridge */ /* synthetic */ uy a(yw ywVar, uy uyVar) {
        return (uy) super.b((Object) ywVar, (Object) uyVar);
    }

    @DexIgnore
    /* renamed from: b */
    public void a(yw ywVar, uy<?> uyVar) {
        uz.a aVar = this.d;
        if (aVar != null && uyVar != null) {
            aVar.a(uyVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.uz
    public /* bridge */ /* synthetic */ uy a(yw ywVar) {
        return (uy) super.c(ywVar);
    }

    @DexIgnore
    @Override // com.fossil.uz
    public void a(uz.a aVar) {
        this.d = aVar;
    }

    @DexIgnore
    /* renamed from: a */
    public int b(uy<?> uyVar) {
        if (uyVar == null) {
            return super.b(null);
        }
        return uyVar.c();
    }

    @DexIgnore
    @Override // com.fossil.uz
    @SuppressLint({"InlinedApi"})
    public void a(int i) {
        if (i >= 40) {
            a();
        } else if (i >= 20 || i == 15) {
            a(c() / 2);
        }
    }
}
