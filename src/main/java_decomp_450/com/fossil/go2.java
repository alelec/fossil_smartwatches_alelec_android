package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ o43 e;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public go2(sn2 sn2, o43 o43) {
        super(sn2);
        this.f = sn2;
        this.e = o43;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.f.h.getCachedAppInstanceId(this.e);
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void b() {
        this.e.a(null);
    }
}
