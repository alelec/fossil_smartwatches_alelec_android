package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ws3 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ws3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ ht3 a;
    @DexIgnore
    public /* final */ ht3 b;
    @DexIgnore
    public /* final */ ht3 c;
    @DexIgnore
    public /* final */ c d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<ws3> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ws3 createFromParcel(Parcel parcel) {
            return new ws3((ht3) parcel.readParcelable(ht3.class.getClassLoader()), (ht3) parcel.readParcelable(ht3.class.getClassLoader()), (ht3) parcel.readParcelable(ht3.class.getClassLoader()), (c) parcel.readParcelable(c.class.getClassLoader()), null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ws3[] newArray(int i) {
            return new ws3[i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ long e; // = nt3.a(ht3.a(1900, 0).g);
        @DexIgnore
        public static /* final */ long f; // = nt3.a(ht3.a(2100, 11).g);
        @DexIgnore
        public long a; // = e;
        @DexIgnore
        public long b; // = f;
        @DexIgnore
        public Long c;
        @DexIgnore
        public c d; // = bt3.a(Long.MIN_VALUE);

        @DexIgnore
        public b(ws3 ws3) {
            this.a = ws3.a.g;
            this.b = ws3.b.g;
            this.c = Long.valueOf(ws3.c.g);
            this.d = ws3.d;
        }

        @DexIgnore
        public b a(long j) {
            this.c = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public ws3 a() {
            if (this.c == null) {
                long d1 = et3.d1();
                if (this.a > d1 || d1 > this.b) {
                    d1 = this.a;
                }
                this.c = Long.valueOf(d1);
            }
            Bundle bundle = new Bundle();
            bundle.putParcelable("DEEP_COPY_VALIDATOR_KEY", this.d);
            return new ws3(ht3.a(this.a), ht3.a(this.b), ht3.a(this.c.longValue()), (c) bundle.getParcelable("DEEP_COPY_VALIDATOR_KEY"), null);
        }
    }

    @DexIgnore
    public interface c extends Parcelable {
        @DexIgnore
        boolean e(long j);
    }

    @DexIgnore
    public /* synthetic */ ws3(ht3 ht3, ht3 ht32, ht3 ht33, c cVar, a aVar) {
        this(ht3, ht32, ht33, cVar);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public ht3 e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ws3)) {
            return false;
        }
        ws3 ws3 = (ws3) obj;
        if (!this.a.equals(ws3.a) || !this.b.equals(ws3.b) || !this.c.equals(ws3.c) || !this.d.equals(ws3.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int f() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, this.b, this.c, this.d});
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.a, 0);
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
    }

    @DexIgnore
    public ws3(ht3 ht3, ht3 ht32, ht3 ht33, c cVar) {
        this.a = ht3;
        this.b = ht32;
        this.c = ht33;
        this.d = cVar;
        if (ht3.compareTo(ht33) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        } else if (ht33.compareTo(ht32) <= 0) {
            this.f = ht3.b(ht32) + 1;
            this.e = (ht32.d - ht3.d) + 1;
        } else {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
    }

    @DexIgnore
    public c a() {
        return this.d;
    }

    @DexIgnore
    public ht3 b() {
        return this.b;
    }

    @DexIgnore
    public int c() {
        return this.f;
    }

    @DexIgnore
    public ht3 d() {
        return this.c;
    }
}
