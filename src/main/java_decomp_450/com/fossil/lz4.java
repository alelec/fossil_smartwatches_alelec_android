package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lz4 extends kz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y;
    @DexIgnore
    public /* final */ NestedScrollView v;
    @DexIgnore
    public long w;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        y = sparseIntArray;
        sparseIntArray.put(2131362034, 1);
        y.put(2131363409, 2);
        y.put(2131362667, 3);
        y.put(2131363410, 4);
        y.put(2131362245, 5);
    }
    */

    @DexIgnore
    public lz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 6, x, y));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public lz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (FlexibleButton) objArr[5], (ImageView) objArr[3], (View) objArr[2], (View) objArr[4]);
        this.w = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.v = nestedScrollView;
        nestedScrollView.setTag(null);
        a(view);
        f();
    }
}
