package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.te5;
import com.portfolio.platform.data.NetworkState;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue5 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements te5.a {
        @DexIgnore
        public /* final */ /* synthetic */ MutableLiveData a;

        @DexIgnore
        public a(MutableLiveData mutableLiveData) {
            this.a = mutableLiveData;
        }

        @DexIgnore
        @Override // com.fossil.te5.a
        public final void a(te5.g gVar) {
            ee7.b(gVar, "report");
            if (gVar.b()) {
                this.a.a(NetworkState.Companion.getLOADING());
            } else if (gVar.a()) {
                this.a.a(NetworkState.Companion.error(ue5.b(gVar)));
            } else {
                this.a.a(NetworkState.Companion.getLOADED());
            }
        }
    }

    @DexIgnore
    public static final String b(te5.g gVar) {
        te5.d[] values = te5.d.values();
        ArrayList arrayList = new ArrayList();
        for (te5.d dVar : values) {
            Throwable a2 = gVar.a(dVar);
            String message = a2 != null ? a2.getMessage() : null;
            if (message != null) {
                arrayList.add(message);
            }
        }
        return arrayList.isEmpty() ^ true ? (String) ea7.d((List) arrayList) : "";
    }

    @DexIgnore
    public static final LiveData<NetworkState> a(te5 te5) {
        ee7.b(te5, "$this$createStatusLiveData");
        MutableLiveData mutableLiveData = new MutableLiveData();
        te5.a(new a(mutableLiveData));
        return mutableLiveData;
    }
}
