package com.fossil;

import dagger.internal.Factory;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x77<K, V, V2> implements Factory<Map<K, V2>> {
    @DexIgnore
    public /* final */ Map<K, Provider<V>> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<K, V, V2> {
        @DexIgnore
        public /* final */ LinkedHashMap<K, Provider<V>> a;

        @DexIgnore
        public a(int i) {
            this.a = y77.b(i);
        }

        @DexIgnore
        public a<K, V, V2> a(K k, Provider<V> provider) {
            LinkedHashMap<K, Provider<V>> linkedHashMap = this.a;
            c87.a(k, "key");
            c87.a(provider, "provider");
            linkedHashMap.put(k, provider);
            return this;
        }
    }

    @DexIgnore
    public x77(Map<K, Provider<V>> map) {
        this.a = Collections.unmodifiableMap(map);
    }

    @DexIgnore
    public final Map<K, Provider<V>> a() {
        return this.a;
    }
}
