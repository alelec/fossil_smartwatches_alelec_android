package com.fossil;

import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class q26 extends cl4 {
    @DexIgnore
    public abstract void a(a06 a06);

    @DexIgnore
    public abstract void a(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void b(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    public abstract void c(WatchFaceWrapper watchFaceWrapper);

    @DexIgnore
    public abstract void h();
}
