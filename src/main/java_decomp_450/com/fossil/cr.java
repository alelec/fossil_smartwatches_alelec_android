package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr {
    @DexIgnore
    public /* final */ Drawable a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public cr(Drawable drawable, boolean z) {
        ee7.b(drawable, ResourceManager.DRAWABLE);
        this.a = drawable;
        this.b = z;
    }

    @DexIgnore
    public final Drawable a() {
        return this.a;
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof cr)) {
            return false;
        }
        cr crVar = (cr) obj;
        return ee7.a(this.a, crVar.a) && this.b == crVar.b;
    }

    @DexIgnore
    public int hashCode() {
        Drawable drawable = this.a;
        int hashCode = (drawable != null ? drawable.hashCode() : 0) * 31;
        boolean z = this.b;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return "DecodeResult(drawable=" + this.a + ", isSampled=" + this.b + ")";
    }
}
