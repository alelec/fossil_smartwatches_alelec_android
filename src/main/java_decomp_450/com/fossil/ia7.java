package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ia7 implements Set, Serializable, ye7 {
    @DexIgnore
    public static /* final */ ia7 INSTANCE; // = new ia7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 3406603774387020532L;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean add(Void r2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean addAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof Void) {
            return contains((Void) obj);
        }
        return false;
    }

    @DexIgnore
    public boolean contains(Void r2) {
        ee7.b(r2, "element");
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean containsAll(Collection collection) {
        ee7.b(collection, MessengerShareContentUtility.ELEMENTS);
        return collection.isEmpty();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Set) && ((Set) obj).isEmpty();
    }

    @DexIgnore
    public int getSize() {
        return 0;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public boolean isEmpty() {
        return true;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.lang.Iterable
    public Iterator iterator() {
        return fa7.a;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean removeAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public boolean retainAll(Collection collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }

    @DexIgnore
    public Object[] toArray() {
        return yd7.a(this);
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set
    public <T> T[] toArray(T[] tArr) {
        return (T[]) yd7.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return "[]";
    }
}
