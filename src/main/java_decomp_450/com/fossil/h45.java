package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h45 extends g45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362517, 1);
        E.put(2131362402, 2);
        E.put(2131362683, 3);
        E.put(2131363390, 4);
        E.put(2131362406, 5);
        E.put(2131362687, 6);
        E.put(2131363391, 7);
        E.put(2131362462, 8);
        E.put(2131362706, 9);
        E.put(2131363392, 10);
        E.put(2131362656, 11);
    }
    */

    @DexIgnore
    public h45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public h45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[1], (RTLImageView) objArr[11], (RTLImageView) objArr[3], (RTLImageView) objArr[6], (RTLImageView) objArr[9], (View) objArr[4], (View) objArr[7], (View) objArr[10]);
        this.C = -1;
        ((g45) this).q.setTag(null);
        a(view);
        f();
    }
}
