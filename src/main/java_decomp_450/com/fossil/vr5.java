package com.fossil;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.cy6;
import com.fossil.os5;
import com.fossil.ov3;
import com.fossil.zz5;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr5 extends ho5 implements d06, View.OnClickListener, zz5.d, cy6.g, ro5 {
    @DexIgnore
    public static /* final */ a w; // = new a(null);
    @DexIgnore
    public c06 g;
    @DexIgnore
    public ConstraintLayout h;
    @DexIgnore
    public ViewPager2 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public zz5 p;
    @DexIgnore
    public qw6<s25> q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public HashMap v;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final vr5 a() {
            return new vr5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ vr5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(vr5 vr5) {
            this.a = vr5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g b;
            Drawable b2;
            TabLayout tabLayout2;
            TabLayout.g b3;
            Drawable b4;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "onPageScrolled " + i + " mCurrentPosition " + this.a.f1());
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.s)) {
                int parseColor = Color.parseColor(this.a.s);
                s25 s25 = (s25) vr5.b(this.a).a();
                if (!(s25 == null || (tabLayout2 = s25.A) == null || (b3 = tabLayout2.b(i)) == null || (b4 = b3.b()) == null)) {
                    b4.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.r) && this.a.f1() != i) {
                int parseColor2 = Color.parseColor(this.a.r);
                s25 s252 = (s25) vr5.b(this.a).a();
                if (!(s252 == null || (tabLayout = s252.A) == null || (b = tabLayout.b(this.a.f1())) == null || (b2 = b.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.n(i);
            this.a.g1().a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements os5.b {
        @DexIgnore
        public /* final */ /* synthetic */ vr5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(vr5 vr5, String str) {
            this.a = vr5;
            this.b = str;
        }

        @DexIgnore
        @Override // com.fossil.os5.b
        public void a(String str) {
            ee7.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.g1().a(str, this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.os5.b
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ vr5 a;

        @DexIgnore
        public d(vr5 vr5) {
            this.a = vr5;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDianaCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.r);
                if (i == 0) {
                    gVar.b(2131230941);
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.b(2131230817);
                    Drawable b2 = gVar.b();
                    if (b2 != null) {
                        b2.setTint(parseColor);
                    }
                } else {
                    gVar.b(2131230963);
                    Drawable b3 = gVar.b();
                    if (b3 != null) {
                        b3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.s) && i == this.a.f1()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeDianaCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.f1());
                    Drawable b4 = gVar.b();
                    if (b4 != null) {
                        b4.setTint(Color.parseColor(this.a.s));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ qw6 b(vr5 vr5) {
        qw6<s25> qw6 = vr5.q;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.v;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void d(int i2) {
        if (isActive()) {
            zz5 zz5 = this.p;
            if (zz5 == null) {
                ee7.d("mAdapterDiana");
                throw null;
            } else if (zz5.getItemCount() > i2) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeDianaCustomizeFragment";
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void e(int i2) {
        this.u = true;
        zz5 zz5 = this.p;
        if (zz5 == null) {
            ee7.d("mAdapterDiana");
            throw null;
        } else if (zz5.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final int f1() {
        return this.j;
    }

    @DexIgnore
    public final c06 g1() {
        c06 c06 = this.g;
        if (c06 != null) {
            return c06;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d06
    public int getItemCount() {
        ViewPager2 viewPager2 = this.i;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        ee7.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public final void h1() {
        qw6<s25> qw6 = this.q;
        if (qw6 != null) {
            s25 a2 = qw6.a();
            TabLayout tabLayout = a2 != null ? a2.A : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    new ov3(tabLayout, viewPager2, new d(this)).a();
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void k() {
        String string = getString(2131886761);
        ee7.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        W(string);
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void l() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            ee7.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.g0.c().c(), false, false, 12, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void m() {
        a();
    }

    @DexIgnore
    public final void n(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        ee7.b(view, "v");
        if (view.getId() == 2131362472 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
            ee7.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, null);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onCreateView");
        s25 s25 = (s25) qb.a(layoutInflater, 2131558574, viewGroup, false, a1());
        ee7.a((Object) s25, "binding");
        a(s25);
        qw6<s25> qw6 = new qw6<>(this, s25);
        this.q = qw6;
        if (qw6 != null) {
            s25 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        c06 c06 = this.g;
        if (c06 != null) {
            if (c06 != null) {
                c06.g();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        c06 c06 = this.g;
        if (c06 != null) {
            if (c06 != null) {
                c06.f();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onViewCreated");
        h1();
        V("customize_view");
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        if (z) {
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        jf5 c12 = c1();
        if (c12 != null) {
            c12.a("");
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void w() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showCreateNewSuccessfully mCurrentPosition " + this.j);
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void x() {
        c06 c06 = this.g;
        if (c06 != null) {
            c06.j();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void y() {
        FLogger.INSTANCE.getLocal().d("HomeDianaCustomizeFragment", "onAddPresetClick");
        c06 c06 = this.g;
        if (c06 != null) {
            c06.h();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void b(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        ee7.b(str, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetComplicationClick preset=");
        sb.append(dz5 != null ? dz5.a() : null);
        sb.append(" complicationPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (dz5 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.B;
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            String d2 = dz5.d();
            ArrayList<a9<View, String>> arrayList = new ArrayList<>(list);
            c06 c06 = this.g;
            if (c06 != null) {
                aVar.a(requireActivity, d2, arrayList, list2, c06.i(), 1, str, ViewHierarchy.DIMENSION_TOP_KEY);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(s25 s25) {
        this.r = eh5.l.a().b("nonBrandSwitchDisabledGray");
        this.s = eh5.l.a().b("primaryColor");
        this.t = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        this.p = new zz5(new ArrayList(), null, this);
        ConstraintLayout constraintLayout = s25.q;
        ee7.a((Object) constraintLayout, "binding.clNoDevice");
        this.h = constraintLayout;
        s25.w.setImageResource(2131230943);
        s25.u.setOnClickListener(this);
        ViewPager2 viewPager2 = s25.z;
        ee7.a((Object) viewPager2, "binding.rvPreset");
        this.i = viewPager2;
        if (viewPager2 != null) {
            if (viewPager2.getChildAt(0) != null) {
                ViewPager2 viewPager22 = this.i;
                if (viewPager22 != null) {
                    View childAt = viewPager22.getChildAt(0);
                    if (childAt != null) {
                        ((RecyclerView) childAt).setOverScrollMode(2);
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager23 = this.i;
            if (viewPager23 != null) {
                zz5 zz5 = this.p;
                if (zz5 != null) {
                    viewPager23.setAdapter(zz5);
                    if (!TextUtils.isEmpty(this.t)) {
                        TabLayout tabLayout = s25.A;
                        ee7.a((Object) tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.t)));
                    }
                    ViewPager2 viewPager24 = this.i;
                    if (viewPager24 != null) {
                        viewPager24.a(new b(this));
                        ViewPager2 viewPager25 = this.i;
                        if (viewPager25 != null) {
                            viewPager25.setCurrentItem(this.j);
                            s25.v.setOnClickListener(this);
                            return;
                        }
                        ee7.d("rvCustomize");
                        throw null;
                    }
                    ee7.d("rvCustomize");
                    throw null;
                }
                ee7.d("mAdapterDiana");
                throw null;
            }
            ee7.d("rvCustomize");
            throw null;
        }
        ee7.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void b(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onEditThemeClick preset=");
        sb.append(dz5 != null ? dz5.a() : null);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        c06 c06 = this.g;
        if (c06 != null) {
            c06.a(dz5, list, list2);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void d(boolean z, boolean z2) {
        if (z) {
            qw6<s25> qw6 = this.q;
            if (qw6 != null) {
                s25 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.C;
                    ee7.a((Object) flexibleTextView, "tvTapIconToCustomize");
                    flexibleTextView.setText(ig5.a(getContext(), 2131886551));
                    FlexibleTextView flexibleTextView2 = a2.C;
                    ee7.a((Object) flexibleTextView2, "tvTapIconToCustomize");
                    flexibleTextView2.setVisibility(0);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        qw6<s25> qw62 = this.q;
        if (qw62 != null) {
            s25 a3 = qw62.a();
            if (a3 == null) {
                return;
            }
            if (!z2) {
                FlexibleTextView flexibleTextView3 = a3.C;
                ee7.a((Object) flexibleTextView3, "tvTapIconToCustomize");
                flexibleTextView3.setText(ig5.a(getContext(), 2131886538));
                FlexibleTextView flexibleTextView4 = a3.C;
                ee7.a((Object) flexibleTextView4, "tvTapIconToCustomize");
                flexibleTextView4.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView5 = a3.C;
            ee7.a((Object) flexibleTextView5, "tvTapIconToCustomize");
            flexibleTextView5.setVisibility(4);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showDeleteSuccessfully - position=" + i2);
        zz5 zz5 = this.p;
        if (zz5 == null) {
            ee7.d("mAdapterDiana");
            throw null;
        } else if (zz5.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                ee7.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void b(boolean z, String str, String str2, String str3) {
        ee7.b(str, "currentPresetName");
        ee7.b(str2, "nextPresetName");
        ee7.b(str3, "nextPresetId");
        isActive();
        String string = requireActivity().getString(2131886541);
        ee7.a((Object) string, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
        if (z) {
            String string2 = requireActivity().getString(2131886542);
            ee7.a((Object) string2, "requireActivity().getStr\u2026ingAPresetIsPermanentAnd)");
            we7 we7 = we7.a;
            string = String.format(string2, Arrays.copyOf(new Object[]{mh7.d(str2)}, 1));
            ee7.a((Object) string, "java.lang.String.format(format, *args)");
        }
        Bundle bundle = new Bundle();
        bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
        cy6.f fVar = new cy6.f(2131558484);
        we7 we72 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886543);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{str}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        fVar.a(2131363342, format);
        fVar.a(2131363255, string);
        fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886540));
        fVar.a(2131363229, ig5.a(PortfolioApp.g0.c(), 2131886539));
        fVar.a(2131363307);
        fVar.a(2131363229);
        fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void a(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        if (dz5 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.B;
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            String d2 = dz5.d();
            ArrayList<a9<View, String>> arrayList = new ArrayList<>(list);
            c06 c06 = this.g;
            if (c06 != null) {
                aVar.a(requireActivity, d2, arrayList, list2, c06.i(), 0, ViewHierarchy.DIMENSION_TOP_KEY, ViewHierarchy.DIMENSION_TOP_KEY);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void a(dz5 dz5, List<? extends a9<View, String>> list, List<? extends a9<CustomizeWidget, String>> list2, String str, int i2) {
        ee7.b(list, "views");
        ee7.b(list2, "customizeWidgetViews");
        ee7.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(dz5 != null ? dz5.a() : null);
        sb.append(" watchAppPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeDianaCustomizeFragment", sb.toString());
        if (dz5 != null) {
            DianaCustomizeEditActivity.a aVar = DianaCustomizeEditActivity.B;
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            String d2 = dz5.d();
            ArrayList<a9<View, String>> arrayList = new ArrayList<>(list);
            c06 c06 = this.g;
            if (c06 != null) {
                aVar.a(requireActivity, d2, arrayList, list2, c06.i(), 2, ViewHierarchy.DIMENSION_TOP_KEY, str);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zz5.d
    public void a(String str, String str2) {
        ee7.b(str, "presetName");
        ee7.b(str2, "presetId");
        if (getChildFragmentManager().b("RenamePresetDialogFragment") == null) {
            os5 a2 = os5.g.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        String str2;
        ee7.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363307) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                ee7.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
            } else {
                str2 = "";
            }
            c06 c06 = this.g;
            if (c06 != null) {
                c06.a(str2);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void a(List<dz5> list, DianaComplicationRingStyle dianaComplicationRingStyle) {
        ee7.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDianaCustomizeFragment", "showPresets - data=" + list.size());
        zz5 zz5 = this.p;
        if (zz5 != null) {
            zz5.a(list, dianaComplicationRingStyle);
            if (!this.u && this.j == 0) {
                this.u = false;
            }
            if (!this.u) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.j);
                } else {
                    ee7.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            ee7.d("mAdapterDiana");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.d06
    public void a(boolean z) {
        if (z) {
            ConstraintLayout constraintLayout = this.h;
            if (constraintLayout != null) {
                constraintLayout.setVisibility(0);
            } else {
                ee7.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout2 = this.h;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(8);
            } else {
                ee7.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(c06 c06) {
        ee7.b(c06, "presenter");
        this.g = c06;
    }
}
