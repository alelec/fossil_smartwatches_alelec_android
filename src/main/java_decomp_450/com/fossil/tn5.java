package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn5 extends fl4<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            ee7.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(String str, String str2) {
            ee7.b(str, Constants.EMAIL);
            ee7.b(str2, "password");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public d(MFUser.Auth auth) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.LoginEmailUseCase", f = "LoginEmailUseCase.kt", l = {31}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(tn5 tn5, fb7 fb7) {
            super(fb7);
            this.this$0 = tn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = tn5.class.getSimpleName();
        ee7.a((Object) simpleName, "LoginEmailUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public tn5(UserRepository userRepository) {
        ee7.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.tn5.c r9, com.fossil.fb7<java.lang.Object> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.tn5.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.tn5$e r0 = (com.fossil.tn5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.tn5$e r0 = new com.fossil.tn5$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 600(0x258, float:8.41E-43)
            java.lang.String r5 = ""
            if (r2 == 0) goto L_0x0045
            if (r2 != r3) goto L_0x003d
            java.lang.Object r9 = r0.L$3
            com.portfolio.platform.data.Access r9 = (com.portfolio.platform.data.Access) r9
            java.lang.Object r9 = r0.L$2
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$1
            com.fossil.tn5$c r9 = (com.fossil.tn5.c) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.tn5 r9 = (com.fossil.tn5) r9
            com.fossil.t87.a(r10)
            goto L_0x0099
        L_0x003d:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0045:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.tn5.e
            java.lang.String r6 = "running UseCase"
            r10.d(r2, r6)
            if (r9 != 0) goto L_0x005d
            com.fossil.tn5$b r9 = new com.fossil.tn5$b
            r9.<init>(r4, r5)
            return r9
        L_0x005d:
            java.lang.String r10 = r9.a()
            if (r10 == 0) goto L_0x00f0
            java.lang.String r10 = r10.toLowerCase()
            java.lang.String r2 = "(this as java.lang.String).toLowerCase()"
            com.fossil.ee7.a(r10, r2)
            com.fossil.ng5 r2 = com.fossil.ng5.a()
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            com.portfolio.platform.data.Access r2 = r2.a(r6)
            if (r2 != 0) goto L_0x0082
            com.fossil.tn5$b r9 = new com.fossil.tn5$b
            r9.<init>(r4, r5)
            return r9
        L_0x0082:
            com.portfolio.platform.data.source.UserRepository r6 = r8.d
            java.lang.String r7 = r9.b()
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r10 = r6.loginEmail(r10, r7, r0)
            if (r10 != r1) goto L_0x0099
            return r1
        L_0x0099:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x00ad
            com.fossil.tn5$d r9 = new com.fossil.tn5$d
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            com.portfolio.platform.data.model.MFUser$Auth r10 = (com.portfolio.platform.data.model.MFUser.Auth) r10
            r9.<init>(r10)
            goto L_0x00ef
        L_0x00ad:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00ea
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r0 = com.fossil.tn5.e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Inside .run failed with http code="
            r1.append(r2)
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.d(r0, r1)
            com.fossil.tn5$b r9 = new com.fossil.tn5$b
            int r0 = r10.a()
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x00e6
            java.lang.String r10 = r10.getMessage()
            if (r10 == 0) goto L_0x00e6
            r5 = r10
        L_0x00e6:
            r9.<init>(r0, r5)
            goto L_0x00ef
        L_0x00ea:
            com.fossil.tn5$b r9 = new com.fossil.tn5$b
            r9.<init>(r4, r5)
        L_0x00ef:
            return r9
        L_0x00f0:
            com.fossil.x87 r9 = new com.fossil.x87
            java.lang.String r10 = "null cannot be cast to non-null type java.lang.String"
            r9.<init>(r10)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tn5.a(com.fossil.tn5$c, com.fossil.fb7):java.lang.Object");
    }
}
