package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e91 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ i71 CREATOR; // = new i71(null);
    @DexIgnore
    public /* final */ di0 a;
    @DexIgnore
    public /* final */ ei1 b;
    @DexIgnore
    public /* final */ ak0 c;
    @DexIgnore
    public /* final */ xl0 d;
    @DexIgnore
    public /* final */ short e;

    @DexIgnore
    public e91(di0 di0, ei1 ei1, ak0 ak0, xl0 xl0, short s) {
        this.a = di0;
        this.b = ei1;
        this.c = ak0;
        this.d = xl0;
        this.e = s;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.V0, yz0.a(this.a)), r51.X0, yz0.a(this.b)), r51.U0, yz0.a(this.c)), r51.Y0, yz0.a(this.d)), r51.P3, Short.valueOf(this.e));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(e91.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            e91 e91 = (e91) obj;
            return this.a == e91.a && this.b == e91.b && this.c == e91.c && this.d == e91.d && this.e == e91.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.animation.HandAnimation");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return ((this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31)) * 31) + this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
