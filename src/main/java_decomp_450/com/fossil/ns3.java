package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ns3 extends m0 {
    @DexIgnore
    public boolean a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends BottomSheetBehavior.e {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, float f) {
        }

        @DexIgnore
        @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.e
        public void a(View view, int i) {
            if (i == 5) {
                ns3.this.Z0();
            }
        }
    }

    @DexIgnore
    public final boolean T(boolean z) {
        Dialog dialog = getDialog();
        if (!(dialog instanceof ms3)) {
            return false;
        }
        ms3 ms3 = (ms3) dialog;
        BottomSheetBehavior<FrameLayout> c = ms3.c();
        if (!c.g() || !ms3.d()) {
            return false;
        }
        a(c, z);
        return true;
    }

    @DexIgnore
    public final void Z0() {
        if (this.a) {
            super.dismissAllowingStateLoss();
        } else {
            super.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void dismiss() {
        if (!T(false)) {
            super.dismiss();
        }
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void dismissAllowingStateLoss() {
        if (!T(true)) {
            super.dismissAllowingStateLoss();
        }
    }

    @DexIgnore
    @Override // com.fossil.m0, com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        return new ms3(getContext(), getTheme());
    }

    @DexIgnore
    public final void a(BottomSheetBehavior<?> bottomSheetBehavior, boolean z) {
        this.a = z;
        if (bottomSheetBehavior.e() == 5) {
            Z0();
            return;
        }
        if (getDialog() instanceof ms3) {
            ((ms3) getDialog()).e();
        }
        bottomSheetBehavior.a(new b());
        bottomSheetBehavior.e(5);
    }
}
