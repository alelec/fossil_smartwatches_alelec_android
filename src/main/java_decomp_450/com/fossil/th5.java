package com.fossil;

import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class th5 implements MembersInjector<sh5> {
    @DexIgnore
    public static void a(sh5 sh5, ch5 ch5) {
        sh5.a = ch5;
    }

    @DexIgnore
    public static void a(sh5 sh5, zv6 zv6) {
        sh5.b = zv6;
    }

    @DexIgnore
    public static void a(sh5 sh5, pd5 pd5) {
        sh5.c = pd5;
    }
}
