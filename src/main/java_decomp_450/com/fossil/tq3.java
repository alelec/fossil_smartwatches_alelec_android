package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq3 implements Parcelable.Creator<sq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        byte b2 = 0;
        String str = null;
        byte b3 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                b2 = j72.k(parcel, a);
            } else if (a2 == 3) {
                b3 = j72.k(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                str = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new sq3(b2, b3, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sq3[] newArray(int i) {
        return new sq3[i];
    }
}
