package com.fossil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.fossil.cy6;
import com.fossil.px6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Comparator;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class el5 extends go5 implements cy6.g {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public BlockingQueue<ib5> f;
    @DexIgnore
    public HashMap g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<ib5> {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        /* renamed from: a */
        public final int compare(ib5 ib5, ib5 ib52) {
            return ib5.ordinal() - ib52.ordinal();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = el5.class.getSimpleName();
        ee7.a((Object) simpleName, "BasePermissionFragment::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.g;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void a(ib5 ib5) {
        BlockingQueue<ib5> blockingQueue = this.f;
        if (blockingQueue == null) {
            ee7.d("mPermissionQueue");
            throw null;
        } else if (!blockingQueue.contains(ib5)) {
            BlockingQueue<ib5> blockingQueue2 = this.f;
            if (blockingQueue2 != null) {
                blockingQueue2.offer(ib5);
            } else {
                ee7.d("mPermissionQueue");
                throw null;
            }
        }
    }

    @DexIgnore
    public final boolean f1() {
        boolean a2 = px6.a.a(PortfolioApp.g0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenBackgroundLocationPermission() - isBackgroundLocationPermissionGranted = " + a2);
        if (!a2) {
            r1();
        }
        return !a2;
    }

    @DexIgnore
    public final boolean g1() {
        boolean c = px6.a.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenBluetoothPermission() - isBluetoothEnabled = " + c);
        if (!c) {
            s1();
        }
        return !c;
    }

    @DexIgnore
    public final boolean h1() {
        boolean b2 = px6.a.b(PortfolioApp.g0.c());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenLocationPermission() - isLocationPermissionGranted = " + b2);
        if (!b2) {
            t1();
        }
        return !b2;
    }

    @DexIgnore
    public final boolean i1() {
        boolean d = px6.a.d();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "forceOpenLocationService() - isLocationOpen = " + d);
        if (!d) {
            u1();
        }
        return !d;
    }

    @DexIgnore
    public final void j1() {
        startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
    }

    @DexIgnore
    public final void k1() {
        startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
    }

    @DexIgnore
    public final void l1() {
        Intent intent = new Intent();
        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY, PortfolioApp.g0.c().getPackageName(), null));
        startActivity(intent);
    }

    @DexIgnore
    public abstract void m1();

    @DexIgnore
    public abstract void n1();

    @DexIgnore
    public abstract void o1();

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f = new PriorityBlockingQueue(5, b.a);
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.g6.b, com.fossil.go5, androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        ee7.b(strArr, "permissions");
        ee7.b(iArr, "grantResults");
        if (i != 0) {
            super.onRequestPermissionsResult(i, strArr, iArr);
            return;
        }
        boolean z = true;
        if (!(!(iArr.length == 0)) || iArr[0] != 0) {
            z = false;
        }
        if (z) {
            a(new ib5[0]);
        } else {
            o1();
        }
    }

    @DexIgnore
    public abstract void p1();

    @DexIgnore
    public abstract void q1();

    @DexIgnore
    public final void r1() {
        BlockingQueue<ib5> blockingQueue = this.f;
        if (blockingQueue != null) {
            ib5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != ib5.BACKGROUND_LOCATION_PERMISSION_OFF) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.N(childFragmentManager);
                return;
            }
            bx6 bx62 = bx6.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager2, "childFragmentManager");
            bx62.L(childFragmentManager2);
            return;
        }
        ee7.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void s1() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.g(childFragmentManager);
        }
    }

    @DexIgnore
    public final void t1() {
        BlockingQueue<ib5> blockingQueue = this.f;
        if (blockingQueue != null) {
            ib5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationPermission() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != ib5.LOCATION_PERMISSION_FEATURE_OFF) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.N(childFragmentManager);
                return;
            }
            bx6 bx62 = bx6.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager2, "childFragmentManager");
            bx62.M(childFragmentManager2);
            return;
        }
        ee7.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void u1() {
        BlockingQueue<ib5> blockingQueue = this.f;
        if (blockingQueue != null) {
            ib5 peek = blockingQueue.peek();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "requestLocationService() - permissionErrorCode = " + peek);
            if (!isActive()) {
                return;
            }
            if (peek == null || peek != ib5.LOCATION_SERVICE_FEATURE_OFF) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.C(childFragmentManager);
                return;
            }
            bx6 bx62 = bx6.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager2, "childFragmentManager");
            bx62.B(childFragmentManager2);
            return;
        }
        ee7.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    public final void a(ib5... ib5Arr) {
        boolean z;
        ee7.b(ib5Arr, "permissionCodes");
        for (ib5 ib5 : ib5Arr) {
            a(ib5);
        }
        BlockingQueue<ib5> blockingQueue = this.f;
        if (blockingQueue != null) {
            ib5 peek = blockingQueue.peek();
            FLogger.INSTANCE.getLocal().d(h, "processPermissionPopups() - permissionErrorCode = " + peek);
            if (peek == null) {
                q1();
                return;
            }
            switch (fl5.a[peek.ordinal()]) {
                case 1:
                    z = g1();
                    break;
                case 2:
                case 3:
                    z = h1();
                    break;
                case 4:
                case 5:
                    z = i1();
                    break;
                case 6:
                    z = f1();
                    break;
                default:
                    throw new p87();
            }
            if (!z) {
                BlockingQueue<ib5> blockingQueue2 = this.f;
                if (blockingQueue2 != null) {
                    blockingQueue2.remove(peek);
                    a(new ib5[0]);
                    return;
                }
                ee7.d("mPermissionQueue");
                throw null;
            }
            return;
        }
        ee7.d("mPermissionQueue");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i, Intent intent) {
        ee7.b(str, "tag");
        if (ee7.a((Object) str, (Object) "REQUEST_LOCATION_SERVICE_PERMISSION")) {
            if (i == 2131362413) {
                p1();
                l1();
            } else if (i != 2131363307) {
                o1();
            } else {
                px6.a aVar = px6.a;
                FragmentActivity requireActivity = requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                aVar.b(requireActivity, 0);
            }
        } else if (ee7.a((Object) str, (Object) bx6.c.a())) {
            if (i == 2131362413) {
                p1();
                l1();
            } else if (i != 2131363307) {
                o1();
            } else {
                px6.a aVar2 = px6.a;
                FragmentActivity requireActivity2 = requireActivity();
                ee7.a((Object) requireActivity2, "requireActivity()");
                aVar2.a(requireActivity2, 1);
            }
        } else if (ee7.a((Object) str, (Object) "REQUEST_OPEN_LOCATION_SERVICE")) {
            if (i != 2131362413) {
                n1();
                return;
            }
            p1();
            k1();
        } else if (!ee7.a((Object) str, (Object) "BLUETOOTH_OFF")) {
        } else {
            if (i != 2131362413) {
                m1();
                return;
            }
            p1();
            j1();
        }
    }
}
