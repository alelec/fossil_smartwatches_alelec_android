package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class al implements cl {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public al(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof al) && ((al) obj).a.equals(this.a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
