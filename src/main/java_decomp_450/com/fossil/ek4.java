package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiUserService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ek4 implements Factory<AuthApiUserService> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<hj5> b;
    @DexIgnore
    public /* final */ Provider<lj5> c;

    @DexIgnore
    public ek4(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static ek4 a(wj4 wj4, Provider<hj5> provider, Provider<lj5> provider2) {
        return new ek4(wj4, provider, provider2);
    }

    @DexIgnore
    public static AuthApiUserService a(wj4 wj4, hj5 hj5, lj5 lj5) {
        AuthApiUserService b2 = wj4.b(hj5, lj5);
        c87.a(b2, "Cannot return null from a non-@Nullable @Provides method");
        return b2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public AuthApiUserService get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
