package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.Looper;
import com.fossil.v02;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b53 extends z02<v02.d.C0203d> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ll2 {
        @DexIgnore
        public /* final */ oo3<Void> a;

        @DexIgnore
        public a(oo3<Void> oo3) {
            this.a = oo3;
        }

        @DexIgnore
        @Override // com.fossil.kl2
        public final void a(hl2 hl2) {
            g22.a(hl2.a(), this.a);
        }
    }

    @DexIgnore
    public b53(Context context) {
        super(context, e53.c, (v02.d) null, new o12());
    }

    @DexIgnore
    public final kl2 a(oo3<Boolean> oo3) {
        return new b63(this, oo3);
    }

    @DexIgnore
    public no3<Void> a(c53 c53) {
        return g22.a(a(z12.a(c53, c53.class.getSimpleName())));
    }

    @DexIgnore
    public no3<Void> a(LocationRequest locationRequest, c53 c53, Looper looper) {
        bm2 a2 = bm2.a(locationRequest);
        y12 a3 = z12.a(c53, im2.a(looper), c53.class.getSimpleName());
        return a(new z53(this, a3, a2, a3), new a63(this, a3.b()));
    }

    @DexIgnore
    public no3<Location> i() {
        return a((f22) new y53(this));
    }
}
