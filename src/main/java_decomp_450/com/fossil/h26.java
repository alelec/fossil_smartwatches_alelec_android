package com.fossil;

import com.portfolio.platform.data.model.diana.Complication;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface h26 extends dl4<g26> {
    @DexIgnore
    void a(Complication complication);

    @DexIgnore
    void b(String str);

    @DexIgnore
    void b(List<r87<Complication, String>> list);

    @DexIgnore
    void c(List<r87<Complication, String>> list);

    @DexIgnore
    void n();
}
