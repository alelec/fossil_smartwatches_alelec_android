package com.fossil;

import android.content.Context;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol4 {
    @DexIgnore
    public static /* final */ a A;
    @DexIgnore
    public static /* final */ Properties a;
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ String w;
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ String z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ol4.b;
        }

        @DexIgnore
        public final String b() {
            return ol4.x;
        }

        @DexIgnore
        public final String c() {
            return ol4.w;
        }

        @DexIgnore
        public final String d() {
            return ol4.c;
        }

        @DexIgnore
        public final String e() {
            return ol4.r;
        }

        @DexIgnore
        public final String f() {
            return ol4.v;
        }

        @DexIgnore
        public final String g() {
            return ol4.u;
        }

        @DexIgnore
        public final String h() {
            return ol4.s;
        }

        @DexIgnore
        public final String i() {
            return ol4.j;
        }

        @DexIgnore
        public final String j() {
            return ol4.k;
        }

        @DexIgnore
        public final String k() {
            return ol4.l;
        }

        @DexIgnore
        public final String l() {
            return ol4.f;
        }

        @DexIgnore
        public final String m() {
            return ol4.g;
        }

        @DexIgnore
        public final String n() {
            return ol4.h;
        }

        @DexIgnore
        public final String o() {
            return ol4.m;
        }

        @DexIgnore
        public final String p() {
            return ol4.i;
        }

        @DexIgnore
        public final String q() {
            return ol4.o;
        }

        @DexIgnore
        public final String r() {
            return ol4.n;
        }

        @DexIgnore
        public final String s() {
            return ol4.t;
        }

        @DexIgnore
        public final String t() {
            return ol4.p;
        }

        @DexIgnore
        public final String u() {
            return ol4.d;
        }

        @DexIgnore
        public final String v() {
            return ol4.e;
        }

        @DexIgnore
        public final String w() {
            return ol4.z;
        }

        @DexIgnore
        public final String x() {
            return ol4.y;
        }

        @DexIgnore
        public final String y() {
            return ol4.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final Properties a(Context context) {
            ee7.b(context, "context");
            Properties properties = new Properties();
            try {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(context.getAssets().open("production.properties"), "UTF-8"));
                properties.load(bufferedReader);
                bufferedReader.close();
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("XXX", "Exception when load properties=" + e);
            }
            return properties;
        }
    }

    /*
    static {
        a aVar = new a(null);
        A = aVar;
        Properties a2 = aVar.a(PortfolioApp.g0.c());
        a = a2;
        String property = a2.getProperty("APP_CODE");
        ee7.a((Object) property, "config.getProperty(\"APP_CODE\")");
        b = property;
        String property2 = a.getProperty("BRAND_ID");
        ee7.a((Object) property2, "config.getProperty(\"BRAND_ID\")");
        c = property2;
        ee7.a((Object) a.getProperty("UA_REDIRECT"), "config.getProperty(\"UA_REDIRECT\")");
        String property3 = a.getProperty("WEIBO_REDIRECT_URL");
        ee7.a((Object) property3, "config.getProperty(\"WEIBO_REDIRECT_URL\")");
        d = property3;
        String property4 = a.getProperty("WEIBO_SCOPE");
        ee7.a((Object) property4, "config.getProperty(\"WEIBO_SCOPE\")");
        e = property4;
        String property5 = a.getProperty("MISFIT_API_BASE_URL_STAGING");
        ee7.a((Object) property5, "config.getProperty(\"MISFIT_API_BASE_URL_STAGING\")");
        f = property5;
        String property6 = a.getProperty("MISFIT_API_BASE_URL_STAGING_V2");
        ee7.a((Object) property6, "config.getProperty(\"MISF\u2026API_BASE_URL_STAGING_V2\")");
        g = property6;
        String property7 = a.getProperty("MISFIT_API_BASE_URL_STAGING_V2DOT1");
        ee7.a((Object) property7, "config.getProperty(\"MISF\u2026BASE_URL_STAGING_V2DOT1\")");
        h = property7;
        String property8 = a.getProperty("MISFIT_WEB_BASE_URL_STAGING");
        ee7.a((Object) property8, "config.getProperty(\"MISFIT_WEB_BASE_URL_STAGING\")");
        i = property8;
        String property9 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION");
        ee7.a((Object) property9, "config.getProperty(\"MISF\u2026API_BASE_URL_PRODUCTION\")");
        j = property9;
        String property10 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2");
        ee7.a((Object) property10, "config.getProperty(\"MISF\u2026_BASE_URL_PRODUCTION_V2\")");
        k = property10;
        String property11 = a.getProperty("MISFIT_API_BASE_URL_PRODUCTION_V2DOT1");
        ee7.a((Object) property11, "config.getProperty(\"MISF\u2026E_URL_PRODUCTION_V2DOT1\")");
        l = property11;
        String property12 = a.getProperty("MISFIT_WEB_BASE_URL_PRODUCTION");
        ee7.a((Object) property12, "config.getProperty(\"MISF\u2026WEB_BASE_URL_PRODUCTION\")");
        m = property12;
        ee7.a((Object) a.getProperty("EMAIL_MAGIC_STAGING_STAGING"), "config.getProperty(\"EMAIL_MAGIC_STAGING_STAGING\")");
        ee7.a((Object) a.getProperty("EMAIL_MAGIC_STAGING_PRODUCTION"), "config.getProperty(\"EMAI\u2026AGIC_STAGING_PRODUCTION\")");
        ee7.a((Object) a.getProperty("EMAIL_MAGIC_PRODUCTION_STAGING"), "config.getProperty(\"EMAI\u2026AGIC_PRODUCTION_STAGING\")");
        ee7.a((Object) a.getProperty("EMAIL_MAGIC_PRODUCTION_PRODUCTION"), "config.getProperty(\"EMAI\u2026C_PRODUCTION_PRODUCTION\")");
        String property13 = a.getProperty("SDK_ENDPOINT_STAGING");
        ee7.a((Object) property13, "config.getProperty(\"SDK_ENDPOINT_STAGING\")");
        n = property13;
        String property14 = a.getProperty("SDK_ENDPOINT_PRODUCTION");
        ee7.a((Object) property14, "config.getProperty(\"SDK_ENDPOINT_PRODUCTION\")");
        o = property14;
        String property15 = a.getProperty("LIST_MICRO_APP_NOT_SUPPORTED");
        ee7.a((Object) property15, "config.getProperty(\"LIST_MICRO_APP_NOT_SUPPORTED\")");
        p = property15;
        String property16 = a.getProperty("ZENDESK_URL");
        ee7.a((Object) property16, "config.getProperty(\"ZENDESK_URL\")");
        q = property16;
        String property17 = a.getProperty("CLOUD_LOG_BASE_URL");
        ee7.a((Object) property17, "config.getProperty(\"CLOUD_LOG_BASE_URL\")");
        r = property17;
        String property18 = a.getProperty("LOG_BRAND_NAME");
        ee7.a((Object) property18, "config.getProperty(\"LOG_BRAND_NAME\")");
        s = property18;
        String property19 = a.getProperty("SDK_V2_LOG_END_POINT");
        ee7.a((Object) property19, "config.getProperty(\"SDK_V2_LOG_END_POINT\")");
        t = property19;
        String property20 = a.getProperty("GOOGLE_PROXY_BASE_URL_STAGING");
        ee7.a((Object) property20, "config.getProperty(\"GOOG\u2026_PROXY_BASE_URL_STAGING\")");
        u = property20;
        String property21 = a.getProperty("GOOGLE_PROXY_BASE_URL_PRODUCTION");
        ee7.a((Object) property21, "config.getProperty(\"GOOG\u2026OXY_BASE_URL_PRODUCTION\")");
        v = property21;
        String property22 = a.getProperty("APPLE_AUTHORIZATION_STAGING_URL");
        ee7.a((Object) property22, "config.getProperty(\"APPL\u2026THORIZATION_STAGING_URL\")");
        w = property22;
        String property23 = a.getProperty("APPLE_AUTHORIZATION_PRODUCTION_URL");
        ee7.a((Object) property23, "config.getProperty(\"APPL\u2026RIZATION_PRODUCTION_URL\")");
        x = property23;
        String property24 = a.getProperty("WHAT_NEW_STAGING_URL");
        String str = "";
        if (property24 == null) {
            property24 = str;
        }
        y = property24;
        String property25 = a.getProperty("WHAT_NEW_PRODUCTION_URL");
        if (property25 != null) {
            str = property25;
        }
        z = str;
    }
    */
}
