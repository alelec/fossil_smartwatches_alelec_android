package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.text.SimpleDateFormat;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<pn4> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<io4> {
    }

    @DexIgnore
    public final String a(Date date) {
        if (date == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = zd5.m.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.format(date);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "fromOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final io4 b(String str) {
        try {
            return (io4) new Gson().a(str, new b().getType());
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final Date c(String str) {
        if (str == null) {
            return null;
        }
        try {
            SimpleDateFormat simpleDateFormat = zd5.m.get();
            if (simpleDateFormat != null) {
                return simpleDateFormat.parse(str);
            }
            ee7.a();
            throw null;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("ChallengeConverter", "toOffsetDateTime - e=" + e);
            return null;
        }
    }

    @DexIgnore
    public final String a(pn4 pn4) {
        try {
            return new Gson().a(pn4);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final pn4 a(String str) {
        try {
            return (pn4) new Gson().a(str, new a().getType());
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final String a(io4 io4) {
        try {
            return new Gson().a(io4);
        } catch (Exception unused) {
            return null;
        }
    }
}
