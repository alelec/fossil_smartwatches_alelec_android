package com.fossil;

import com.fossil.sv2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qv2<T extends sv2<T>> {
    @DexIgnore
    public static /* final */ qv2 d; // = new qv2(true);
    @DexIgnore
    public /* final */ dy2<T, Object> a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    public qv2() {
        this.a = dy2.c(16);
    }

    @DexIgnore
    public static <T extends sv2<T>> qv2<T> g() {
        return d;
    }

    @DexIgnore
    public final void a() {
        if (!this.b) {
            this.a.a();
            this.b = true;
        }
    }

    @DexIgnore
    public final boolean b() {
        return this.b;
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> c() {
        if (this.c) {
            return new ow2(this.a.entrySet().iterator());
        }
        return this.a.entrySet().iterator();
    }

    @DexIgnore
    public final /* synthetic */ Object clone() throws CloneNotSupportedException {
        qv2 qv2 = new qv2();
        for (int i = 0; i < this.a.c(); i++) {
            Map.Entry<T, Object> a2 = this.a.a(i);
            qv2.a(a2.getKey(), a2.getValue());
        }
        for (Map.Entry<T, Object> entry : this.a.d()) {
            qv2.a(entry.getKey(), entry.getValue());
        }
        qv2.c = this.c;
        return qv2;
    }

    @DexIgnore
    public final Iterator<Map.Entry<T, Object>> d() {
        if (this.c) {
            return new ow2(this.a.e().iterator());
        }
        return this.a.e().iterator();
    }

    @DexIgnore
    public final boolean e() {
        for (int i = 0; i < this.a.c(); i++) {
            if (!b(this.a.a(i))) {
                return false;
            }
        }
        for (Map.Entry<T, Object> entry : this.a.d()) {
            if (!b(entry)) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof qv2)) {
            return false;
        }
        return this.a.equals(((qv2) obj).a);
    }

    @DexIgnore
    public final int f() {
        int i = 0;
        for (int i2 = 0; i2 < this.a.c(); i2++) {
            i += c(this.a.a(i2));
        }
        for (Map.Entry<T, Object> entry : this.a.d()) {
            i += c(entry);
        }
        return i;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public static <T extends sv2<T>> boolean b(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        if (key.zzc() == pz2.MESSAGE) {
            if (key.zzd()) {
                for (jx2 jx2 : (List) entry.getValue()) {
                    if (!jx2.j()) {
                        return false;
                    }
                }
            } else {
                Object value = entry.getValue();
                if (value instanceof jx2) {
                    if (!((jx2) value).j()) {
                        return false;
                    }
                } else if (value instanceof nw2) {
                    return true;
                } else {
                    throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
                }
            }
        }
        return true;
    }

    @DexIgnore
    public qv2(boolean z) {
        this(dy2.c(0));
        a();
    }

    @DexIgnore
    public static int c(Map.Entry<T, Object> entry) {
        T key = entry.getKey();
        Object value = entry.getValue();
        if (key.zzc() != pz2.MESSAGE || key.zzd() || key.zze()) {
            return b((sv2<?>) key, value);
        }
        if (value instanceof nw2) {
            return iv2.b(entry.getKey().zza(), (nw2) value);
        }
        return iv2.b(entry.getKey().zza(), (jx2) value);
    }

    @DexIgnore
    public final Object a(T t) {
        Object obj = this.a.get(t);
        if (!(obj instanceof nw2)) {
            return obj;
        }
        nw2 nw2 = (nw2) obj;
        nw2.c();
        throw null;
    }

    @DexIgnore
    public qv2(dy2<T, Object> dy2) {
        this.a = dy2;
        a();
    }

    @DexIgnore
    public final void a(T t, Object obj) {
        if (!t.zzd()) {
            a(t.zzb(), obj);
        } else if (obj instanceof List) {
            ArrayList arrayList = new ArrayList();
            arrayList.addAll((List) obj);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj2 = arrayList.get(i);
                i++;
                a(t.zzb(), obj2);
            }
            obj = arrayList;
        } else {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
        if (obj instanceof nw2) {
            this.c = true;
        }
        this.a.put(t, obj);
    }

    @DexIgnore
    public static int b(iz2 iz2, Object obj) {
        switch (tv2.b[iz2.ordinal()]) {
            case 1:
                return iv2.b(((Double) obj).doubleValue());
            case 2:
                return iv2.b(((Float) obj).floatValue());
            case 3:
                return iv2.d(((Long) obj).longValue());
            case 4:
                return iv2.e(((Long) obj).longValue());
            case 5:
                return iv2.f(((Integer) obj).intValue());
            case 6:
                return iv2.g(((Long) obj).longValue());
            case 7:
                return iv2.i(((Integer) obj).intValue());
            case 8:
                return iv2.b(((Boolean) obj).booleanValue());
            case 9:
                return iv2.c((jx2) obj);
            case 10:
                if (obj instanceof nw2) {
                    return iv2.a((nw2) obj);
                }
                return iv2.b((jx2) obj);
            case 11:
                if (obj instanceof tu2) {
                    return iv2.b((tu2) obj);
                }
                return iv2.b((String) obj);
            case 12:
                if (obj instanceof tu2) {
                    return iv2.b((tu2) obj);
                }
                return iv2.b((byte[]) obj);
            case 13:
                return iv2.g(((Integer) obj).intValue());
            case 14:
                return iv2.j(((Integer) obj).intValue());
            case 15:
                return iv2.h(((Long) obj).longValue());
            case 16:
                return iv2.h(((Integer) obj).intValue());
            case 17:
                return iv2.f(((Long) obj).longValue());
            case 18:
                if (obj instanceof dw2) {
                    return iv2.k(((dw2) obj).zza());
                }
                return iv2.k(((Integer) obj).intValue());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0025, code lost:
        if ((r3 instanceof com.fossil.dw2) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        if ((r3 instanceof byte[]) == false) goto L_0x0014;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if ((r3 instanceof com.fossil.nw2) == false) goto L_0x0014;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(com.fossil.iz2 r2, java.lang.Object r3) {
        /*
            com.fossil.ew2.a(r3)
            int[] r0 = com.fossil.tv2.a
            com.fossil.pz2 r2 = r2.zza()
            int r2 = r2.ordinal()
            r2 = r0[r2]
            r0 = 1
            r1 = 0
            switch(r2) {
                case 1: goto L_0x0040;
                case 2: goto L_0x003d;
                case 3: goto L_0x003a;
                case 4: goto L_0x0037;
                case 5: goto L_0x0034;
                case 6: goto L_0x0031;
                case 7: goto L_0x0028;
                case 8: goto L_0x001f;
                case 9: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            r0 = 0
            goto L_0x0042
        L_0x0016:
            boolean r2 = r3 instanceof com.fossil.jx2
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.fossil.nw2
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x001f:
            boolean r2 = r3 instanceof java.lang.Integer
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof com.fossil.dw2
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0028:
            boolean r2 = r3 instanceof com.fossil.tu2
            if (r2 != 0) goto L_0x0042
            boolean r2 = r3 instanceof byte[]
            if (r2 == 0) goto L_0x0014
            goto L_0x0042
        L_0x0031:
            boolean r0 = r3 instanceof java.lang.String
            goto L_0x0042
        L_0x0034:
            boolean r0 = r3 instanceof java.lang.Boolean
            goto L_0x0042
        L_0x0037:
            boolean r0 = r3 instanceof java.lang.Double
            goto L_0x0042
        L_0x003a:
            boolean r0 = r3 instanceof java.lang.Float
            goto L_0x0042
        L_0x003d:
            boolean r0 = r3 instanceof java.lang.Long
            goto L_0x0042
        L_0x0040:
            boolean r0 = r3 instanceof java.lang.Integer
        L_0x0042:
            if (r0 == 0) goto L_0x0045
            return
        L_0x0045:
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Wrong object type used with protocol message reflection."
            r2.<init>(r3)
            throw r2
            switch-data {1->0x0040, 2->0x003d, 3->0x003a, 4->0x0037, 5->0x0034, 6->0x0031, 7->0x0028, 8->0x001f, 9->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qv2.a(com.fossil.iz2, java.lang.Object):void");
    }

    @DexIgnore
    public final void a(qv2<T> qv2) {
        for (int i = 0; i < qv2.a.c(); i++) {
            a((Map.Entry) qv2.a.a(i));
        }
        for (Map.Entry<T, Object> entry : qv2.a.d()) {
            a((Map.Entry) entry);
        }
    }

    @DexIgnore
    public static Object a(Object obj) {
        if (obj instanceof sx2) {
            return ((sx2) obj).zza();
        }
        if (!(obj instanceof byte[])) {
            return obj;
        }
        byte[] bArr = (byte[]) obj;
        byte[] bArr2 = new byte[bArr.length];
        System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
        return bArr2;
    }

    @DexIgnore
    public static int b(sv2<?> sv2, Object obj) {
        iz2 zzb = sv2.zzb();
        int zza = sv2.zza();
        if (!sv2.zzd()) {
            return a(zzb, zza, obj);
        }
        int i = 0;
        if (sv2.zze()) {
            for (Object obj2 : (List) obj) {
                i += b(zzb, obj2);
            }
            return iv2.e(zza) + i + iv2.l(i);
        }
        for (Object obj3 : (List) obj) {
            i += a(zzb, zza, obj3);
        }
        return i;
    }

    @DexIgnore
    public final void a(Map.Entry<T, Object> entry) {
        jx2 jx2;
        T key = entry.getKey();
        Object value = entry.getValue();
        if (value instanceof nw2) {
            nw2 nw2 = (nw2) value;
            nw2.c();
            throw null;
        } else if (key.zzd()) {
            Object a2 = a((sv2) key);
            if (a2 == null) {
                a2 = new ArrayList();
            }
            for (Object obj : (List) value) {
                ((List) a2).add(a(obj));
            }
            this.a.put(key, a2);
        } else if (key.zzc() == pz2.MESSAGE) {
            Object a3 = a((sv2) key);
            if (a3 == null) {
                this.a.put(key, a(value));
                return;
            }
            if (a3 instanceof sx2) {
                jx2 = key.a((sx2) a3, (sx2) value);
            } else {
                jx2 = key.a(((jx2) a3).f(), (jx2) value).g();
            }
            this.a.put(key, jx2);
        } else {
            this.a.put(key, a(value));
        }
    }

    @DexIgnore
    public static void a(iv2 iv2, iz2 iz2, int i, Object obj) throws IOException {
        if (iz2 == iz2.GROUP) {
            jx2 jx2 = (jx2) obj;
            ew2.a(jx2);
            iv2.a(i, 3);
            jx2.a(iv2);
            iv2.a(i, 4);
            return;
        }
        iv2.a(i, iz2.zzb());
        switch (tv2.b[iz2.ordinal()]) {
            case 1:
                iv2.a(((Double) obj).doubleValue());
                return;
            case 2:
                iv2.a(((Float) obj).floatValue());
                return;
            case 3:
                iv2.a(((Long) obj).longValue());
                return;
            case 4:
                iv2.a(((Long) obj).longValue());
                return;
            case 5:
                iv2.a(((Integer) obj).intValue());
                return;
            case 6:
                iv2.c(((Long) obj).longValue());
                return;
            case 7:
                iv2.d(((Integer) obj).intValue());
                return;
            case 8:
                iv2.a(((Boolean) obj).booleanValue());
                return;
            case 9:
                ((jx2) obj).a(iv2);
                return;
            case 10:
                iv2.a((jx2) obj);
                return;
            case 11:
                if (obj instanceof tu2) {
                    iv2.a((tu2) obj);
                    return;
                } else {
                    iv2.a((String) obj);
                    return;
                }
            case 12:
                if (obj instanceof tu2) {
                    iv2.a((tu2) obj);
                    return;
                }
                byte[] bArr = (byte[]) obj;
                iv2.b(bArr, 0, bArr.length);
                return;
            case 13:
                iv2.b(((Integer) obj).intValue());
                return;
            case 14:
                iv2.d(((Integer) obj).intValue());
                return;
            case 15:
                iv2.c(((Long) obj).longValue());
                return;
            case 16:
                iv2.c(((Integer) obj).intValue());
                return;
            case 17:
                iv2.b(((Long) obj).longValue());
                return;
            case 18:
                if (obj instanceof dw2) {
                    iv2.a(((dw2) obj).zza());
                    return;
                } else {
                    iv2.a(((Integer) obj).intValue());
                    return;
                }
            default:
                return;
        }
    }

    @DexIgnore
    public static int a(iz2 iz2, int i, Object obj) {
        int e = iv2.e(i);
        if (iz2 == iz2.GROUP) {
            ew2.a((jx2) obj);
            e <<= 1;
        }
        return e + b(iz2, obj);
    }
}
