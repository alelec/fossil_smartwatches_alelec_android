package com.fossil;

import com.fossil.wf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xf7<R> extends ag7<R>, wf7<R> {

    @DexIgnore
    public interface a<R> extends wf7.a<R>, gd7<R, i97> {
    }

    @DexIgnore
    a<R> getSetter();
}
