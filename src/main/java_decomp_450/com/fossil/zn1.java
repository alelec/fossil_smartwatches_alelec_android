package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zn1 {
    b((byte) 0),
    KEYBOARD((byte) 1);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public zn1(byte b2) {
        this.a = b2;
    }
}
