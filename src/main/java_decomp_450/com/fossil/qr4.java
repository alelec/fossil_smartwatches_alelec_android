package com.fossil;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.vr4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr4 extends go5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public qw6<ay4> f;
    @DexIgnore
    public tr4 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public vr4 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qr4.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final qr4 a(String str) {
            qr4 qr4 = new qr4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_history_id_extra", str);
            qr4.setArguments(bundle);
            return qr4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public b(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        public final void onClick(View view) {
            qr4.e(this.a).g();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public c(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            qr4.e(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextView.OnEditorActionListener {
        @DexIgnore
        public /* final */ /* synthetic */ ay4 a;
        @DexIgnore
        public /* final */ /* synthetic */ qr4 b;

        @DexIgnore
        public d(ay4 ay4, qr4 qr4) {
            this.a = ay4;
            this.b = qr4;
        }

        @DexIgnore
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            ee7.b(textView, "v");
            if (i != 3) {
                return false;
            }
            qr4 qr4 = this.b;
            FlexibleEditText flexibleEditText = this.a.s;
            ee7.a((Object) flexibleEditText, "etSearch");
            qr4.e(flexibleEditText);
            qr4.e(this.b).h();
            qr4.e(this.b).b(textView.getText().toString());
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public e(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity;
            if (!(this.a.j == null || !this.a.p || (activity = this.a.getActivity()) == null)) {
                activity.setResult(-1);
            }
            FragmentActivity activity2 = this.a.getActivity();
            if (activity2 != null) {
                activity2.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements vr4.b {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public f(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        @Override // com.fossil.vr4.b
        public void a(un4 un4, int i) {
            ee7.b(un4, "friend");
            qr4.e(this.a).a(un4, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ g a; // = new g();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                um4.a.a(PortfolioApp.g0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public h(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ee7.a((Object) bool, "it");
            if (bool.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<ko4<List<un4>>> {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public i(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(ko4<List<un4>> ko4) {
            FlexibleTextView flexibleTextView;
            ProgressBar progressBar;
            ay4 ay4 = (ay4) qr4.c(this.a).a();
            if (!(ay4 == null || (progressBar = ay4.x) == null)) {
                progressBar.setVisibility(8);
            }
            ay4 ay42 = (ay4) qr4.c(this.a).a();
            if (!(ay42 == null || (flexibleTextView = ay42.u) == null)) {
                flexibleTextView.setVisibility(8);
            }
            List<un4> c = ko4.c();
            if (c == null) {
                ServerError a2 = ko4.a();
                ay4 ay43 = (ay4) qr4.c(this.a).a();
                if (ay43 != null) {
                    FlexibleTextView flexibleTextView2 = ay43.t;
                    ee7.a((Object) flexibleTextView2, "ftvNotFound");
                    flexibleTextView2.setText(String.valueOf(a2));
                }
            } else if (c.isEmpty()) {
                ay4 ay44 = (ay4) qr4.c(this.a).a();
                if (ay44 != null) {
                    if (this.a.j == null) {
                        FlexibleEditText flexibleEditText = ay44.s;
                        ee7.a((Object) flexibleEditText, "etSearch");
                        String valueOf = String.valueOf(flexibleEditText.getText());
                        we7 we7 = we7.a;
                        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886282);
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                        String format = String.format(a3, Arrays.copyOf(new Object[]{valueOf}, 1));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        FlexibleTextView flexibleTextView3 = ay44.t;
                        ee7.a((Object) flexibleTextView3, "ftvNotFound");
                        flexibleTextView3.setText(format);
                    } else {
                        FlexibleTextView flexibleTextView4 = ay44.t;
                        ee7.a((Object) flexibleTextView4, "ftvNotFound");
                        flexibleTextView4.setText("");
                    }
                    FlexibleTextView flexibleTextView5 = ay44.t;
                    ee7.a((Object) flexibleTextView5, "ftvNotFound");
                    flexibleTextView5.setVisibility(0);
                }
            } else {
                vr4 b = this.a.i;
                if (b != null) {
                    b.a(c);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<r87<? extends ko4<Boolean>, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public j(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<ko4<Boolean>, Integer> r87) {
            String str;
            Boolean c = r87.getFirst().c();
            int intValue = r87.getSecond().intValue();
            if (c != null) {
                qr4 qr4 = this.a;
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886328);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                qr4.Y(a2);
                vr4 b = this.a.i;
                if (b != null) {
                    b.a(intValue);
                    return;
                }
                return;
            }
            ServerError a3 = r87.getFirst().a();
            Integer num = null;
            Integer code = a3 != null ? a3.getCode() : null;
            if (code != null && code.intValue() == 404001) {
                this.a.p = true;
                vr4 b2 = this.a.i;
                if (b2 != null) {
                    b2.a(intValue);
                }
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886295);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
                bx6.a(childFragmentManager, a4, a5);
            } else if (code != null && code.intValue() == 409001) {
                qr4 qr42 = this.a;
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886328);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026Label__FriendRequestSent)");
                qr42.Y(a6);
                vr4 b3 = this.a.i;
                if (b3 != null) {
                    b3.a(intValue);
                }
            } else {
                xe5 xe5 = xe5.b;
                ServerError a7 = r87.getFirst().a();
                if (a7 != null) {
                    num = a7.getCode();
                }
                ServerError a8 = r87.getFirst().a();
                if (a8 == null || (str = a8.getMessage()) == null) {
                    str = "";
                }
                String b4 = xe5.b(num, str);
                bx6 bx62 = bx6.c;
                FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager2, "childFragmentManager");
                bx62.a(code, b4, childFragmentManager2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<wt4> {
        @DexIgnore
        public /* final */ /* synthetic */ qr4 a;

        @DexIgnore
        public k(qr4 qr4) {
            this.a = qr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(wt4 wt4) {
            ay4 ay4;
            if (wt4 != null) {
                int i = rr4.a[wt4.ordinal()];
                if (i == 1) {
                    ay4 ay42 = (ay4) qr4.c(this.a).a();
                    if (ay42 != null) {
                        ay42.s.setText("");
                        ProgressBar progressBar = ay42.x;
                        ee7.a((Object) progressBar, "prg");
                        progressBar.setVisibility(8);
                        FlexibleTextView flexibleTextView = ay42.u;
                        ee7.a((Object) flexibleTextView, "ftvSearch");
                        flexibleTextView.setVisibility(8);
                    }
                    vr4 b = this.a.i;
                    if (b != null) {
                        b.c();
                    }
                } else if (i == 2) {
                    ay4 ay43 = (ay4) qr4.c(this.a).a();
                    if (ay43 != null) {
                        FlexibleTextView flexibleTextView2 = ay43.t;
                        ee7.a((Object) flexibleTextView2, "ftvNotFound");
                        flexibleTextView2.setVisibility(8);
                        FlexibleEditText flexibleEditText = ay43.s;
                        ee7.a((Object) flexibleEditText, "etSearch");
                        if (TextUtils.isEmpty(String.valueOf(flexibleEditText.getText()))) {
                            RTLImageView rTLImageView = ay43.q;
                            ee7.a((Object) rTLImageView, "btnSearchClear");
                            rTLImageView.setVisibility(8);
                            return;
                        }
                        RTLImageView rTLImageView2 = ay43.q;
                        ee7.a((Object) rTLImageView2, "btnSearchClear");
                        rTLImageView2.setVisibility(0);
                    }
                } else if (i == 3 && (ay4 = (ay4) qr4.c(this.a).a()) != null) {
                    FlexibleTextView flexibleTextView3 = ay4.t;
                    ee7.a((Object) flexibleTextView3, "ftvNotFound");
                    flexibleTextView3.setVisibility(8);
                    ProgressBar progressBar2 = ay4.x;
                    ee7.a((Object) progressBar2, "prg");
                    progressBar2.setVisibility(0);
                    FlexibleTextView flexibleTextView4 = ay4.u;
                    ee7.a((Object) flexibleTextView4, "ftvSearch");
                    flexibleTextView4.setVisibility(0);
                    vr4 b2 = this.a.i;
                    if (b2 != null) {
                        b2.c();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<r87<? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ l a; // = new l();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<String, String> r87) {
            um4.a.a(r87.getFirst(), r87.getSecond(), PortfolioApp.g0.c());
        }
    }

    /*
    static {
        String simpleName = qr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCFindFriendsFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 c(qr4 qr4) {
        qw6<ay4> qw6 = qr4.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ tr4 e(qr4 qr4) {
        tr4 tr4 = qr4.g;
        if (tr4 != null) {
            return tr4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void Y(String str) {
        LayoutInflater layoutInflater = getLayoutInflater();
        ee7.a((Object) layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558827, activity != null ? (ViewGroup) activity.findViewById(2131362169) : null);
        View findViewById = inflate.findViewById(2131362360);
        ee7.a((Object) findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<ay4> qw6 = this.f;
        if (qw6 != null) {
            ay4 a2 = qw6.a();
            if (a2 != null) {
                if (this.j != null) {
                    ConstraintLayout constraintLayout = a2.r;
                    ee7.a((Object) constraintLayout, "cSearch");
                    constraintLayout.setVisibility(8);
                    View view = a2.w;
                    ee7.a((Object) view, "line");
                    view.setVisibility(0);
                }
                RTLImageView rTLImageView = a2.q;
                ee7.a((Object) rTLImageView, "btnSearchClear");
                rTLImageView.setVisibility(8);
                a2.q.setOnClickListener(new b(this));
                a2.s.setOnFocusChangeListener(g.a);
                a2.s.addTextChangedListener(new c(this));
                a2.s.setOnEditorActionListener(new d(a2, this));
                a2.v.setOnClickListener(new e(this));
                vr4 vr4 = new vr4();
                this.i = vr4;
                vr4.a(new f(this));
                RecyclerView recyclerView = a2.z;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                recyclerView.setAdapter(this.i);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        tr4 tr4 = this.g;
        if (tr4 != null) {
            tr4.c().a(getViewLifecycleOwner(), new h(this));
            tr4 tr42 = this.g;
            if (tr42 != null) {
                tr42.d().a(getViewLifecycleOwner(), new i(this));
                tr4 tr43 = this.g;
                if (tr43 != null) {
                    tr43.f().a(getViewLifecycleOwner(), new j(this));
                    tr4 tr44 = this.g;
                    if (tr44 != null) {
                        tr44.e().a(getViewLifecycleOwner(), new k(this));
                        tr4 tr45 = this.g;
                        if (tr45 != null) {
                            tr45.b().a(getViewLifecycleOwner(), l.a);
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.j = arguments != null ? arguments.getString("challenge_history_id_extra") : null;
        PortfolioApp.g0.c().f().c().a(this);
        rj4 rj4 = this.h;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(tr4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ndsViewModel::class.java)");
            this.g = (tr4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ay4 ay4 = (ay4) qb.a(layoutInflater, 2131558511, viewGroup, false, a1());
        this.f = new qw6<>(this, ay4);
        ee7.a((Object) ay4, "binding");
        return ay4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qd5 c2 = qd5.f.c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a("bc_friend_add", activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        String str = this.j;
        if (str != null) {
            tr4 tr4 = this.g;
            if (tr4 == null) {
                ee7.d("viewModel");
                throw null;
            } else if (str != null) {
                tr4.a(str);
            } else {
                ee7.a();
                throw null;
            }
        }
        f1();
        g1();
    }

    @DexIgnore
    public final void e(View view) {
        Object systemService = view.getContext().getSystemService("input_method");
        if (systemService != null) {
            ((InputMethodManager) systemService).hideSoftInputFromWindow(view.getWindowToken(), 0);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.view.inputmethod.InputMethodManager");
    }
}
