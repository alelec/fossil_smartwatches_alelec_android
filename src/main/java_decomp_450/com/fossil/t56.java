package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t56 {
    @DexIgnore
    public /* final */ q56 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public t56(q56 q56, String str, String str2) {
        ee7.b(q56, "mView");
        ee7.b(str, "mPresetId");
        ee7.b(str2, "mMicroAppPos");
        this.a = q56;
    }

    @DexIgnore
    public final q56 a() {
        return this.a;
    }
}
