package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iu4 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public iu4() {
        be4 be4 = new be4();
        be4.a(Date.class, new GsonConverterShortDate());
        Gson a2 = be4.a();
        ee7.a((Object) a2, "GsonBuilder().registerTy\u2026rterShortDate()).create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(ActivityStatistic.ActivityDailyBest activityDailyBest) {
        if (activityDailyBest == null) {
            return null;
        }
        try {
            return this.a.a(activityDailyBest);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(i97.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ActivityStatistic.CaloriesBestDay b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (ActivityStatistic.CaloriesBestDay) this.a.a(str, ActivityStatistic.CaloriesBestDay.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(i97.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final ActivityStatistic.ActivityDailyBest a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (ActivityStatistic.ActivityDailyBest) this.a.a(str, ActivityStatistic.ActivityDailyBest.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toActivityDailyBest - e=");
            e.printStackTrace();
            sb.append(i97.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public final String a(ActivityStatistic.CaloriesBestDay caloriesBestDay) {
        if (caloriesBestDay == null) {
            return null;
        }
        try {
            return this.a.a(caloriesBestDay);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("fromCaloriesBestDay - e=");
            e.printStackTrace();
            sb.append(i97.a);
            local.d("ActivityStatisticConverter", sb.toString());
            return null;
        }
    }
}
