package com.fossil;

import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import android.support.v4.media.session.MediaSessionCompat;
import android.util.Log;
import com.fossil.we;
import com.fossil.xe;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ye {
    @DexIgnore
    public static Field a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends xe.a {
        @DexIgnore
        public a(Context context, c cVar) {
            super(context, cVar);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((c) ((we.b) this).a).a(str, new b(result), bundle);
        }
    }

    @DexIgnore
    public interface c extends xe.b {
        @DexIgnore
        void a(String str, b bVar, Bundle bundle);
    }

    /*
    static {
        try {
            Field declaredField = MediaBrowserService.Result.class.getDeclaredField("mFlags");
            a = declaredField;
            declaredField.setAccessible(true);
        } catch (NoSuchFieldException e) {
            Log.w("MBSCompatApi26", e);
        }
    }
    */

    @DexIgnore
    public static Object a(Context context, c cVar) {
        return new a(context, cVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public MediaBrowserService.Result a;

        @DexIgnore
        public b(MediaBrowserService.Result result) {
            this.a = result;
        }

        @DexIgnore
        public void a(List<Parcel> list, int i) {
            try {
                ye.a.setInt(this.a, i);
            } catch (IllegalAccessException e) {
                Log.w("MBSCompatApi26", e);
            }
            this.a.sendResult(a(list));
        }

        @DexIgnore
        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }
    }
}
