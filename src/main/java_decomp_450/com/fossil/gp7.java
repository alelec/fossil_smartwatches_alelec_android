package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.facebook.stetho.server.http.HttpHeaders;
import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.fo7;
import com.fossil.lo7;
import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.RequestBody;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp7 implements Interceptor {
    @DexIgnore
    public /* final */ yn7 a;

    @DexIgnore
    public gp7(yn7 yn7) {
        this.a = yn7;
    }

    @DexIgnore
    public final String a(List<xn7> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (i > 0) {
                sb.append("; ");
            }
            xn7 xn7 = list.get(i);
            sb.append(xn7.a());
            sb.append('=');
            sb.append(xn7.b());
        }
        return sb.toString();
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        lo7 c = chain.c();
        lo7.a f = c.f();
        RequestBody a2 = c.a();
        if (a2 != null) {
            ho7 b = a2.b();
            if (b != null) {
                f.b("Content-Type", b.toString());
            }
            long a3 = a2.a();
            if (a3 != -1) {
                f.b(HttpHeaders.CONTENT_LENGTH, Long.toString(a3));
                f.a("Transfer-Encoding");
            } else {
                f.b("Transfer-Encoding", "chunked");
                f.a(HttpHeaders.CONTENT_LENGTH);
            }
        }
        boolean z = false;
        if (c.a("Host") == null) {
            f.b("Host", ro7.a(c.g(), false));
        }
        if (c.a(WebSocketHandler.HEADER_CONNECTION) == null) {
            f.b(WebSocketHandler.HEADER_CONNECTION, "Keep-Alive");
        }
        if (c.a("Accept-Encoding") == null && c.a("Range") == null) {
            z = true;
            f.b("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        }
        List<xn7> a4 = this.a.a(c.g());
        if (!a4.isEmpty()) {
            f.b("Cookie", a(a4));
        }
        if (c.a("User-Agent") == null) {
            f.b("User-Agent", so7.a());
        }
        Response a5 = chain.a(f.a());
        kp7.a(this.a, c.g(), a5.k());
        Response.a q = a5.q();
        q.a(c);
        if (z && DecompressionHelper.GZIP_ENCODING.equalsIgnoreCase(a5.b(GraphRequest.CONTENT_ENCODING_HEADER)) && kp7.b(a5)) {
            fr7 fr7 = new fr7(a5.a().source());
            fo7.a a6 = a5.k().a();
            a6.c(GraphRequest.CONTENT_ENCODING_HEADER);
            a6.c(HttpHeaders.CONTENT_LENGTH);
            q.a(a6.a());
            q.a(new np7(a5.b("Content-Type"), -1, ir7.a(fr7)));
        }
        return q.a();
    }
}
