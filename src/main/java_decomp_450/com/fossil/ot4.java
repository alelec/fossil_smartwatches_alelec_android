package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot4 {
    @DexIgnore
    public static /* final */ ot4 a; // = new ot4();

    @DexIgnore
    public final List<sn4> a() {
        return w97.d(new sn4(PortfolioApp.g0.c().getString(2131886268), PortfolioApp.g0.c().getString(2131886266), 2131231010, "activity_best_result", 0, 86400, "public_with_friend"), new sn4(PortfolioApp.g0.c().getString(2131886280), PortfolioApp.g0.c().getString(2131886279), 2131231013, "activity_reach_goal", 15000, 259200, "public_with_friend"));
    }

    @DexIgnore
    public final List<Date> b() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("BCGenerator", "laterTime - phoneDate: " + new Date() + " - exactDate: " + vt4.a.a());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(vt4.a.a());
        Date time = instance.getTime();
        ee7.a((Object) time, "currentDate");
        arrayList.add(time);
        for (int i = 1; i <= 6; i++) {
            instance.add(6, 1);
            Date time2 = instance.getTime();
            ee7.a((Object) time2, "calendar.time");
            arrayList.add(time2);
        }
        return arrayList;
    }

    @DexIgnore
    public final List<dn4> c() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886315);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886317);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        return w97.d(new dn4(a2, tt4.ABOUT, false, 4, null), new dn4(a3, tt4.LEADER_BOARD, false, 4, null));
    }

    @DexIgnore
    public final List<dn4> d() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886315);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026enu_List__AboutChallenge)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886317);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026ist__ViewFullLeaderboard)");
        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886316);
        ee7.a((Object) a4, "LanguageHelper.getString\u2026enu_List__LeaveChallenge)");
        return w97.d(new dn4(a2, tt4.ABOUT, false, 4, null), new dn4(a3, tt4.LEADER_BOARD, false, 4, null), new dn4(a4, tt4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<dn4> e() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886200);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return w97.d(new dn4(a2, tt4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<dn4> f() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886199);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026ailable_Allow_List__Edit)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886198);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026e_Allow_List__AddFriends)");
        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886200);
        ee7.a((Object) a4, "LanguageHelper.getString\u2026low_List__LeaveChallenge)");
        return w97.d(new dn4(a2, tt4.EDIT, false, 4, null), new dn4(a3, tt4.ADD_FRIENDS, false, 4, null), new dn4(a4, tt4.LEAVE, false, 4, null));
    }

    @DexIgnore
    public final List<dn4> g() {
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886269);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026cySettings_List__Friends)");
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131886270);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026cySettings_List__Private)");
        return w97.d(new dn4(a2, "public_with_friend", true), new dn4(a3, "private", false));
    }

    @DexIgnore
    public final List<dn4> h() {
        ArrayList arrayList = new ArrayList();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("BCGenerator", "soonTime - phoneDate: " + new Date() + " - exactDate: " + vt4.a.a());
        Calendar instance = Calendar.getInstance();
        Date a2 = vt4.a.a();
        ee7.a((Object) instance, "calendar");
        instance.setTime(a2);
        instance.set(12, (((instance.get(12) + 5) / 15) + 1) * 15);
        Date time = instance.getTime();
        ee7.a((Object) time, "calendar.time");
        arrayList.add(time);
        instance.add(12, 15);
        Date time2 = instance.getTime();
        ee7.a((Object) time2, "calendar.time");
        arrayList.add(time2);
        instance.add(12, 15);
        Date time3 = instance.getTime();
        ee7.a((Object) time3, "calendar.time");
        arrayList.add(time3);
        return nt4.a(arrayList, a2);
    }
}
