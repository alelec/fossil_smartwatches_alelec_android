package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.stats.WakeLockEvent;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h92 {
    @DexIgnore
    public static h92 a; // = new h92();
    @DexIgnore
    public static Boolean b;

    @DexIgnore
    public static h92 a() {
        return a;
    }

    @DexIgnore
    public static boolean b() {
        if (b == null) {
            b = false;
        }
        return b.booleanValue();
    }

    @DexIgnore
    public void a(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list) {
        a(context, str, i, str2, str3, str4, i2, list, 0);
    }

    @DexIgnore
    public void a(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list, long j) {
        if (b()) {
            if (TextUtils.isEmpty(str)) {
                String valueOf = String.valueOf(str);
                Log.e("WakeLockTracker", valueOf.length() != 0 ? "missing wakeLock key. ".concat(valueOf) : new String("missing wakeLock key. "));
            } else if (7 == i || 8 == i || 10 == i || 11 == i) {
                a(context, new WakeLockEvent(System.currentTimeMillis(), i, str2, i2, g92.a(list), str, SystemClock.elapsedRealtime(), ea2.a(context), str3, g92.a(context.getPackageName()), ea2.b(context), j, str4, false));
            }
        }
    }

    @DexIgnore
    public static void a(Context context, WakeLockEvent wakeLockEvent) {
        try {
            context.startService(new Intent().setComponent(f92.a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", wakeLockEvent));
        } catch (Exception e) {
            Log.wtf("WakeLockTracker", e);
        }
    }
}
