package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx2 implements gx2 {
    @DexIgnore
    @Override // com.fossil.gx2
    public final Object a(Object obj) {
        return dx2.zza().zzb();
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final Object b(Object obj) {
        ((dx2) obj).zzc();
        return obj;
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final Map<?, ?> zza(Object obj) {
        return (dx2) obj;
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final ex2<?, ?> zzb(Object obj) {
        bx2 bx2 = (bx2) obj;
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final Map<?, ?> zzc(Object obj) {
        return (dx2) obj;
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final boolean zzd(Object obj) {
        return !((dx2) obj).zzd();
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final Object zza(Object obj, Object obj2) {
        dx2 dx2 = (dx2) obj;
        dx2 dx22 = (dx2) obj2;
        if (!dx22.isEmpty()) {
            if (!dx2.zzd()) {
                dx2 = dx2.zzb();
            }
            dx2.zza(dx22);
        }
        return dx2;
    }

    @DexIgnore
    @Override // com.fossil.gx2
    public final int zza(int i, Object obj, Object obj2) {
        dx2 dx2 = (dx2) obj;
        bx2 bx2 = (bx2) obj2;
        if (dx2.isEmpty()) {
            return 0;
        }
        Iterator it = dx2.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
}
