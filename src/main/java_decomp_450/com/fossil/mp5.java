package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp5 extends rf<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public ob5 k;
    @DexIgnore
    public /* final */ PortfolioApp l;
    @DexIgnore
    public /* final */ rp5 m;
    @DexIgnore
    public /* final */ FragmentManager n;
    @DexIgnore
    public /* final */ go5 o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            ee7.b(str, "mDayOfWeek");
            ee7.b(str2, "mDayOfMonth");
            ee7.b(str3, "mDailyValue");
            ee7.b(str4, "mDailyUnit");
            ee7.b(str5, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ b(java.util.Date r10, boolean r11, boolean r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.lang.String r17, int r18, com.fossil.zd7 r19) {
            /*
                r9 = this;
                r0 = r18
                r1 = r0 & 1
                if (r1 == 0) goto L_0x0008
                r1 = 0
                goto L_0x0009
            L_0x0008:
                r1 = r10
            L_0x0009:
                r2 = r0 & 2
                r3 = 0
                if (r2 == 0) goto L_0x0010
                r2 = 0
                goto L_0x0011
            L_0x0010:
                r2 = r11
            L_0x0011:
                r4 = r0 & 4
                if (r4 == 0) goto L_0x0016
                goto L_0x0017
            L_0x0016:
                r3 = r12
            L_0x0017:
                r4 = r0 & 8
                java.lang.String r5 = ""
                if (r4 == 0) goto L_0x001f
                r4 = r5
                goto L_0x0020
            L_0x001f:
                r4 = r13
            L_0x0020:
                r6 = r0 & 16
                if (r6 == 0) goto L_0x0026
                r6 = r5
                goto L_0x0027
            L_0x0026:
                r6 = r14
            L_0x0027:
                r7 = r0 & 32
                if (r7 == 0) goto L_0x002d
                r7 = r5
                goto L_0x002e
            L_0x002d:
                r7 = r15
            L_0x002e:
                r8 = r0 & 64
                if (r8 == 0) goto L_0x0034
                r8 = r5
                goto L_0x0036
            L_0x0034:
                r8 = r16
            L_0x0036:
                r0 = r0 & 128(0x80, float:1.794E-43)
                if (r0 == 0) goto L_0x003b
                goto L_0x003d
            L_0x003b:
                r5 = r17
            L_0x003d:
                r10 = r9
                r11 = r1
                r12 = r2
                r13 = r3
                r14 = r4
                r15 = r6
                r16 = r7
                r17 = r8
                r18 = r5
                r10.<init>(r11, r12, r13, r14, r15, r16, r17, r18)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mp5.b.<init>(java.util.Date, boolean, boolean, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, com.fossil.zd7):void");
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final void c(String str) {
            ee7.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void d(String str) {
            ee7.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ y75 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ mp5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.d.m.a(a2);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(mp5 mp5, y75 y75, View view) {
            super(view);
            ee7.b(y75, "binding");
            ee7.b(view, "root");
            this.d = mp5;
            this.b = y75;
            this.c = view;
            y75.d().setOnClickListener(new a(this));
            this.b.r.setTextColor(mp5.f);
            this.b.v.setTextColor(mp5.f);
        }

        @DexIgnore
        public void a(ActivitySummary activitySummary) {
            b a2 = this.d.a(activitySummary);
            this.a = a2.d();
            FlexibleTextView flexibleTextView = this.b.u;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.f());
            FlexibleTextView flexibleTextView2 = this.b.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.e());
            FlexibleTextView flexibleTextView3 = this.b.s;
            ee7.a((Object) flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(a2.c());
            FlexibleTextView flexibleTextView4 = this.b.r;
            ee7.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(a2.b());
            FlexibleTextView flexibleTextView5 = this.b.v;
            ee7.a((Object) flexibleTextView5, "binding.ftvEst");
            flexibleTextView5.setText(a2.a());
            if (a2.g()) {
                this.b.r.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099942));
                FlexibleTextView flexibleTextView6 = this.b.r;
                ee7.a((Object) flexibleTextView6, "binding.ftvDailyUnit");
                flexibleTextView6.setAllCaps(true);
            } else {
                this.b.r.setTextColor(v6.a(PortfolioApp.g0.c(), 2131099944));
                FlexibleTextView flexibleTextView7 = this.b.r;
                ee7.a((Object) flexibleTextView7, "binding.ftvDailyUnit");
                flexibleTextView7.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            ee7.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.g());
            FlexibleTextView flexibleTextView8 = this.b.u;
            ee7.a((Object) flexibleTextView8, "binding.ftvDayOfWeek");
            flexibleTextView8.setSelected(a2.h());
            FlexibleTextView flexibleTextView9 = this.b.t;
            ee7.a((Object) flexibleTextView9, "binding.ftvDayOfMonth");
            flexibleTextView9.setSelected(a2.h());
            if (a2.h()) {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (a2.g()) {
                this.b.q.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            ee7.b(str, "mWeekly");
            ee7.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ a85 g;
        @DexIgnore
        public /* final */ /* synthetic */ mp5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.e != null && this.a.f != null) {
                    rp5 e = this.a.h.m;
                    Date b = this.a.e;
                    if (b != null) {
                        Date a2 = this.a.f;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public e(com.fossil.mp5 r4, com.fossil.a85 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.ee7.b(r5, r0)
                r3.h = r4
                com.fossil.y75 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.ee7.a(r0, r1)
                android.view.View r1 = r5.d()
                java.lang.String r2 = "binding.root"
                com.fossil.ee7.a(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r4 = r5.q
                com.fossil.mp5$e$a r5 = new com.fossil.mp5$e$a
                r5.<init>(r3)
                r4.setOnClickListener(r5)
                return
            L_0x0029:
                com.fossil.ee7.a()
                r4 = 0
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.mp5.e.<init>(com.fossil.mp5, com.fossil.a85):void");
        }

        @DexIgnore
        @Override // com.fossil.mp5.c
        public void a(ActivitySummary activitySummary) {
            d b = this.h.b(activitySummary);
            this.f = b.a();
            this.e = b.b();
            FlexibleTextView flexibleTextView = this.g.s;
            ee7.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(activitySummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ mp5 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(mp5 mp5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = mp5;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            ee7.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.o.getId() + ", isAdded=" + this.a.o.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.n.b(this.a.o.d1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                nc b3 = this.a.n.b();
                b3.a(view.getId(), this.a.o, this.a.o.d1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                nc b4 = this.a.n.b();
                b4.d(b2);
                b4.d();
                nc b5 = this.a.n.b();
                b5.a(view.getId(), this.a.o, this.a.o.d1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActivitiesAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.o.getId() + ", isAdded2=" + this.a.o.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ee7.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mp5(kp5 kp5, PortfolioApp portfolioApp, rp5 rp5, FragmentManager fragmentManager, go5 go5) {
        super(kp5);
        ee7.b(kp5, "activityDifference");
        ee7.b(portfolioApp, "mApp");
        ee7.b(rp5, "mOnItemClick");
        ee7.b(fragmentManager, "mFragmentManager");
        ee7.b(go5, "mFragment");
        this.l = portfolioApp;
        this.m = rp5;
        this.n = fragmentManager;
        this.o = go5;
        String b2 = eh5.l.a().b("primaryText");
        String str = "#FFFFFF";
        this.d = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("nonBrandSurface");
        this.e = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("nonBrandNonReachGoal");
        this.f = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("dianaStepsTab");
        this.g = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("secondaryText");
        this.h = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(b7 == null ? str : b7);
        String b8 = eh5.l.a().b("onDianaStepsTab");
        this.j = Color.parseColor(b8 != null ? b8 : str);
        this.k = ob5.METRIC;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.o.getId() == 0) {
            return 1010101;
        }
        return (long) this.o.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) getItem(i2);
        if (activitySummary == null) {
            return 1;
        }
        this.c.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
        Calendar calendar = this.c;
        ee7.a((Object) calendar, "mCalendar");
        Boolean w = zd5.w(calendar.getTime());
        ee7.a((Object) w, "DateHelper.isToday(mCalendar.time)");
        if (w.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        ee7.b(viewHolder, "holder");
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            ee7.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            ee7.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local.d("DashboardActivitiesAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            ee7.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((ActivitySummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((ActivitySummary) getItem(i2));
        } else {
            ((e) viewHolder).a((ActivitySummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        ee7.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            y75 a2 = y75.a(from, viewGroup, false);
            ee7.a((Object) a2, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View d2 = a2.d();
            ee7.a((Object) d2, "itemActivityDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            y75 a3 = y75.a(from, viewGroup, false);
            ee7.a((Object) a3, "ItemActivityDayBinding.i\u2026tInflater, parent, false)");
            View d3 = a3.d();
            ee7.a((Object) d3, "itemActivityDayBinding.root");
            return new c(this, a3, d3);
        } else {
            a85 a4 = a85.a(from, viewGroup, false);
            ee7.a((Object) a4, "ItemActivityWeekBinding.\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(qf<ActivitySummary> qfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
        sb.append(" on thread ");
        Thread currentThread = Thread.currentThread();
        ee7.a((Object) currentThread, "Thread.currentThread()");
        sb.append(currentThread.getName());
        local.d("DashboardActivitiesAdapter", sb.toString());
        super.b(qfVar);
    }

    @DexIgnore
    public final b a(ActivitySummary activitySummary) {
        String str;
        b bVar = new b(null, false, false, null, null, null, null, null, 255, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            boolean z = true;
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i2 = instance.get(7);
            ee7.a((Object) instance, "calendar");
            Boolean w = zd5.w(instance.getTime());
            ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
            if (w.booleanValue()) {
                String a2 = ig5.a(this.l, 2131886640);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026n_StepsToday_Text__Today)");
                bVar.e(a2);
            } else {
                bVar.e(xe5.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.d(String.valueOf(instance.get(5)));
            if (activitySummary.getSteps() > ((double) 0)) {
                double steps = activitySummary.getSteps();
                bVar.c(af5.a.b(Integer.valueOf((int) steps)));
                String a3 = ig5.a(this.l, 2131886643);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026_StepsToday_Title__Steps)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    bVar.b(lowerCase);
                    if (this.k == ob5.IMPERIAL) {
                        StringBuilder sb = new StringBuilder();
                        we7 we7 = we7.a;
                        String a4 = ig5.a(this.l, 2131886633);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format = String.format(a4, Arrays.copyOf(new Object[]{af5.a.a(Float.valueOf((float) activitySummary.getDistance()), this.k)}, 1));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        sb.append(format);
                        sb.append(" ");
                        sb.append(PortfolioApp.g0.c().getString(2131886798));
                        str = sb.toString();
                    } else {
                        StringBuilder sb2 = new StringBuilder();
                        we7 we72 = we7.a;
                        String a5 = ig5.a(this.l, 2131886633);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026oday_Text__EstNumberUnit)");
                        String format2 = String.format(a5, Arrays.copyOf(new Object[]{af5.a.a(Float.valueOf((float) activitySummary.getDistance()), this.k)}, 1));
                        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                        sb2.append(format2);
                        sb2.append(" ");
                        sb2.append(PortfolioApp.g0.c().getString(2131886797));
                        str = sb2.toString();
                    }
                    bVar.a(str);
                    if (activitySummary.getStepGoal() > 0) {
                        if (steps < ((double) activitySummary.getStepGoal())) {
                            z = false;
                        }
                        bVar.b(z);
                    } else {
                        bVar.b(false);
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String a6 = ig5.a(this.l, 2131886671);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                bVar.b(a6);
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(ActivitySummary activitySummary) {
        String str;
        d dVar = new d(null, null, null, null, 15, null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            ee7.a((Object) instance, "calendar");
            Boolean w = zd5.w(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String c2 = zd5.c(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String c3 = zd5.c(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            ee7.a((Object) w, "isToday");
            if (w.booleanValue()) {
                str = ig5.a(this.l, 2131886644);
                ee7.a((Object) str, "LanguageHelper.getString\u2026epsToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = c3 + ' ' + i5 + " - " + c3 + ' ' + i2;
            } else if (i7 == i4) {
                str = c3 + ' ' + i5 + " - " + c2 + ' ' + i2;
            } else {
                str = c3 + ' ' + i5 + ", " + i7 + " - " + c2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            we7 we7 = we7.a;
            String a2 = ig5.a(this.l, 2131886636);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
            Object[] objArr = new Object[1];
            af5 af5 = af5.a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            objArr[0] = af5.b(totalValuesOfWeek != null ? Integer.valueOf((int) totalValuesOfWeek.getTotalStepsOfWeek()) : null);
            String format = String.format(a2, Arrays.copyOf(objArr, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }

    @DexIgnore
    public final void a(ob5 ob5, int i2, int i3) {
        ee7.b(ob5, "distanceUnit");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActivitiesAdapter", "updateUserUnits - old=" + this.k + ", new=" + ob5);
        this.k = ob5;
        if (getItemCount() > 0 && i3 >= 0 && i2 >= 0) {
            int max = Math.max(0, i2 - 5);
            notifyItemRangeChanged(max, Math.min(getItemCount(), i3 + 5) - max);
        }
    }
}
