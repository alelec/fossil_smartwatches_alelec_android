package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gf5 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public Map<String, String> c; // = new HashMap();
    @DexIgnore
    public qd5 d;

    @DexIgnore
    public gf5(qd5 qd5, String str) {
        ee7.b(qd5, "analyticsHelper");
        ee7.b(str, "eventName");
        this.d = qd5;
        this.a = str;
    }

    @DexIgnore
    public final gf5 a(String str, String str2) {
        ee7.b(str, "paramName");
        ee7.b(str2, "paramValue");
        Map<String, String> map = this.c;
        if (map != null) {
            map.put(str, str2);
            return this;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        String str;
        Map<String, String> map = this.c;
        if (map == null) {
            ee7.a();
            throw null;
        } else if (!map.isEmpty() || (str = this.b) == null) {
            qd5 qd5 = this.d;
            if (qd5 != null) {
                qd5.a(this.a, this.c);
            } else {
                ee7.a();
                throw null;
            }
        } else if (str != null) {
            qd5 qd52 = this.d;
            if (qd52 != null) {
                String str2 = this.a;
                if (str != null) {
                    qd52.a(str2, str);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            qd5 qd53 = this.d;
            if (qd53 != null) {
                qd53.a(this.a);
            } else {
                ee7.a();
                throw null;
            }
        }
    }
}
