package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk5 implements MembersInjector<tk5> {
    @DexIgnore
    public static void a(tk5 tk5, PortfolioApp portfolioApp) {
        tk5.a = portfolioApp;
    }

    @DexIgnore
    public static void a(tk5 tk5, ApiServiceV2 apiServiceV2) {
        tk5.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(tk5 tk5, LocationSource locationSource) {
        tk5.c = locationSource;
    }

    @DexIgnore
    public static void a(tk5 tk5, UserRepository userRepository) {
        tk5.d = userRepository;
    }

    @DexIgnore
    public static void a(tk5 tk5, ch5 ch5) {
        tk5.e = ch5;
    }

    @DexIgnore
    public static void a(tk5 tk5, DianaPresetRepository dianaPresetRepository) {
        tk5.f = dianaPresetRepository;
    }
}
