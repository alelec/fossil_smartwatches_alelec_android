package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.model.microapp.weather.WeatherSettings;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ch5 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences b;
    @DexIgnore
    public /* final */ SharedPreferences c;
    @DexIgnore
    public /* final */ SharedPreferences d;
    @DexIgnore
    public /* final */ SharedPreferences e;
    @DexIgnore
    public /* final */ SharedPreferences f;
    @DexIgnore
    public /* final */ SharedPreferences g;
    @DexIgnore
    public /* final */ SharedPreferences h;
    @DexIgnore
    public /* final */ SharedPreferences i;
    @DexIgnore
    public /* final */ SharedPreferences j;
    @DexIgnore
    public /* final */ SharedPreferences.Editor k;
    @DexIgnore
    public /* final */ SharedPreferences l;
    @DexIgnore
    public /* final */ SharedPreferences m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends TypeToken<List<String>> {
        @DexIgnore
        public a(ch5 ch5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends TypeToken<List<String>> {
        @DexIgnore
        public b(ch5 ch5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends TypeToken<List<String>> {
        @DexIgnore
        public c(ch5 ch5) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends TypeToken<List<String>> {
        @DexIgnore
        public d(ch5 ch5) {
        }
    }

    @DexIgnore
    public ch5(Context context) {
        this.a = context.getSharedPreferences("PREFERENCE_DEVICE", 0);
        this.b = context.getSharedPreferences("PREFERENCE_AUTH", 0);
        this.c = context.getSharedPreferences("PREFERENCE_ONBOARDING", 0);
        this.d = context.getSharedPreferences("PREFERENCE_MIGRATE", 0);
        this.e = context.getSharedPreferences("PREFERENCE_BUDDY_CHALLENGE", 0);
        this.f = context.getSharedPreferences("PREFERENCE_CONNECTED_APPS", 0);
        this.m = context.getSharedPreferences("project_fossil_wearables_fossil", 0);
        this.g = context.getSharedPreferences("PREFERENCE_APPS", 0);
        this.h = context.getSharedPreferences("PREFERENCE_USER", 0);
        this.i = context.getSharedPreferences("PREFERENCE_FIREBASE", 0);
        this.j = context.getSharedPreferences("PREFERENCE_QUICK_RESPONSE", 0);
        SharedPreferences sharedPreferences = context.getSharedPreferences("PREFERENCE_PERMISSION", 0);
        this.l = sharedPreferences;
        this.k = sharedPreferences.edit();
    }

    @DexIgnore
    public String A() {
        return a(this.h, "social_profile_id", "");
    }

    @DexIgnore
    public String B() {
        return a(this.b, "USER_ACCESS_TOKEN", "");
    }

    @DexIgnore
    public List<String> C() {
        String a2 = a(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new c(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getWatchAppSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public String D() {
        return a(this.g, "KEY_WIDGETS_DATE_CHANGED", "");
    }

    @DexIgnore
    public boolean E() {
        return a(this.e, "KEY_SHOW_SUGGESTION_FIND_FRIEND", false);
    }

    @DexIgnore
    public boolean F() {
        return a(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", false);
    }

    @DexIgnore
    public boolean G() {
        return a(this.a, "AutoSyncEnable", true);
    }

    @DexIgnore
    public boolean H() {
        return a(this.a, "consider_bundle_firmware_as_latest", false);
    }

    @DexIgnore
    public boolean I() {
        return a(this.h, "KEY_DND_MODE_HYBRID_ENABLE", false);
    }

    @DexIgnore
    public boolean J() {
        return a(this.h, "KEY_DND_SCHEDULED_ENABLE", false);
    }

    @DexIgnore
    public boolean K() {
        return this.a.getBoolean("KEY_IS_DEVICE_LOCATE_ENABLE", false);
    }

    @DexIgnore
    public boolean L() {
        return this.c.getBoolean("KEY_DEVICE_LOCATE_NEED_WARNING", true);
    }

    @DexIgnore
    public boolean M() {
        return a(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", false);
    }

    @DexIgnore
    public boolean N() {
        return a(this.a, "HWLogSyncEnable", true);
    }

    @DexIgnore
    public boolean O() {
        return a(this.a, "KEY_DISPLAY_DEVICE_INFO", false);
    }

    @DexIgnore
    public Boolean P() {
        return Boolean.valueOf(a(this.h, "is_need_sync_with_watch", false));
    }

    @DexIgnore
    public boolean Q() {
        return a(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", true);
    }

    @DexIgnore
    public boolean R() {
        return a(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", false);
    }

    @DexIgnore
    public boolean S() {
        return a(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", true);
    }

    @DexIgnore
    public Boolean T() {
        return Boolean.valueOf(a(this.j, "KEY_QUICK_RESPONSE", false));
    }

    @DexIgnore
    public boolean U() {
        return a(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", false);
    }

    @DexIgnore
    public boolean V() {
        return a(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", false);
    }

    @DexIgnore
    public boolean W() {
        return a(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", false);
    }

    @DexIgnore
    public boolean X() {
        return a(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", false);
    }

    @DexIgnore
    public boolean Y() {
        return a(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", false);
    }

    @DexIgnore
    public boolean Z() {
        return a(this.a, "showAllDevices", false);
    }

    @DexIgnore
    public final int a(SharedPreferences sharedPreferences, String str, int i2) {
        return sharedPreferences.getInt(str, i2);
    }

    @DexIgnore
    public boolean a0() {
        return a(this.a, "skipOta", false);
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, int i2) {
        sharedPreferences.edit().putInt(str, i2).apply();
    }

    @DexIgnore
    public boolean b0() {
        return a(this.a, "KEY_SYNC_SESSION_RUNNING", false);
    }

    @DexIgnore
    public void c(boolean z) {
        b(this.a, "consider_bundle_firmware_as_latest", z);
    }

    @DexIgnore
    public void c0() {
        this.a.edit().clear().apply();
        this.b.edit().clear().apply();
        this.c.edit().clear().apply();
        this.e.edit().clear().apply();
        this.h.edit().clear().apply();
        this.i.edit().clear().apply();
        d0();
    }

    @DexIgnore
    public long d(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastSyncTime" + str, 0L);
    }

    @DexIgnore
    public void d0() {
        boolean Q = Q();
        this.h.edit().clear().apply();
        p(Q);
    }

    @DexIgnore
    public String e() {
        return a(this.a, "active_device_address", "");
    }

    @DexIgnore
    public void e0() {
        b(this.e, "KEY_SHOW_SUGGESTION_FIND_FRIEND", true);
    }

    @DexIgnore
    public void f(boolean z) {
        b(this.a, "KEY_IS_DEVICE_LOCATE_ENABLE", z);
    }

    @DexIgnore
    public boolean f0() {
        return a(this.g, "KEY_FORCE_REGISTER_FIREBASE_TOKEN", false);
    }

    @DexIgnore
    public String g() {
        return a(this.i, "KEY_AUTHORISED_FIREBASE_TOKEN", "");
    }

    @DexIgnore
    public int h(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "PREFS_VIBRATION_STRENGTH" + str, 25);
    }

    @DexIgnore
    public void i(boolean z) {
        b(this.a, "HWLogSyncEnable", z);
    }

    @DexIgnore
    public String j() {
        return a(this.i, "KEY_FIREBASE_TOKEN", "");
    }

    @DexIgnore
    public boolean k(String str) {
        return a(this.a, "FrontLightEnable", false);
    }

    @DexIgnore
    public void l(boolean z) {
        b(this.a, "KEY_NEED_TO_SHOW_LEGACY_POPUP", z);
    }

    @DexIgnore
    public int m() {
        return a(this.g, "KEY_USING_SWITCH_URL_SETTING", 0);
    }

    @DexIgnore
    public void n(String str) {
        SharedPreferences sharedPreferences = this.a;
        a(sharedPreferences, "tempDeviceFirmwareInfo_" + str);
    }

    @DexIgnore
    public void o(String str) {
        b(this.a, "active_device_address", str);
    }

    @DexIgnore
    public void p(String str) {
        b(this.a, "installation_id_android", str);
    }

    @DexIgnore
    public long q() {
        long a2 = a(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", -1L);
        if (a2 != -1) {
            return a2;
        }
        long currentTimeMillis = System.currentTimeMillis();
        c(currentTimeMillis);
        return currentTimeMillis;
    }

    @DexIgnore
    public int r() {
        return a(this.a, "lastNotificationID", 0);
    }

    @DexIgnore
    public void s(String str) {
        b(this.i, "KEY_AUTHORISED_FIREBASE_TOKEN", str);
    }

    @DexIgnore
    public void t(String str) {
        b(this.i, "KEY_FIREBASE_TOKEN", str);
    }

    @DexIgnore
    public boolean u() {
        return this.m.getBoolean("KEY_ENABLE_FIND_MY_DEVICE", false);
    }

    @DexIgnore
    public void v(boolean z) {
        b(this.a, "showAllDevices", z);
    }

    @DexIgnore
    public void w(boolean z) {
        b(this.a, "skipOta", z);
    }

    @DexIgnore
    public void x(String str) {
        b(this.a, "realTimeStepTimeStamp_", str);
    }

    @DexIgnore
    public String y() {
        return a(this.a, "realTimeStepTimeStamp_", "");
    }

    @DexIgnore
    public int z() {
        return a(this.a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public final long a(SharedPreferences sharedPreferences, String str, long j2) {
        return sharedPreferences.getLong(str, j2);
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, long j2) {
        sharedPreferences.edit().putLong(str, j2).apply();
    }

    @DexIgnore
    public void c(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public void d(int i2) {
        b(this.a, "lastNotificationID", i2);
    }

    @DexIgnore
    public long e(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastSyncTimeSuccess" + str, 0L);
    }

    @DexIgnore
    public List<String> f() {
        String a2 = a(this.h, "KEY_ADDRESS_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new b(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getAddressSearchedRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void g(boolean z) {
        b(this.a, "FrontLightEnable", z);
    }

    @DexIgnore
    public void h(boolean z) {
        b(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", z);
    }

    @DexIgnore
    public boolean i(String str) {
        return a(this.a, "FULL_SYNC_DEVICE", "").startsWith(str);
    }

    @DexIgnore
    public void j(boolean z) {
        b(this.h, "KEY_IS_GOAL_TRACKING_SETTING_COMPLETED", z);
    }

    @DexIgnore
    public boolean k() {
        return a(this.f, "KEY_GOOGLE_FIT_USER_ENABLE", false);
    }

    @DexIgnore
    public boolean l(String str) {
        SharedPreferences sharedPreferences = this.g;
        return a(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, false);
    }

    @DexIgnore
    public void m(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedToUpdateBLEWhenUpgradeLegacy - isNeedToUpdate=" + z);
        b(this.g, "KEY_NEED_TO_UPDATE_BLE_WHEN_UPGRADE_LEGACY", z);
    }

    @DexIgnore
    public void n(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsRegisteredContactObserver - isSet=" + z);
        b(this.g, "KEY_IS_REGISTER_CONTACT_OBSERVER", z);
    }

    @DexIgnore
    public String o() {
        String a2 = a(this.a, "KEY_WEATHER_SETTINGS", "");
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        return new Gson().a(new WeatherSettings(), WeatherSettings.class);
    }

    @DexIgnore
    public String p() {
        return this.d.getString("KEY_USER_LAST_APP_VERSION_NAME_Q", "");
    }

    @DexIgnore
    public void r(String str) {
        b(this.d, "KEY_USER_LAST_APP_VERSION_NAME_Q", str);
    }

    @DexIgnore
    public void s(boolean z) {
        b(this.h, "KEY_REMINDERS_INACTIVITY_NUDGE_ENABLE", z);
    }

    @DexIgnore
    public List<String> t() {
        String a2 = a(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new d(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getMicroAppSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public void u(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setKeyWeatherSettings - weatherSettings=" + str);
        b(this.a, "KEY_WEATHER_SETTINGS", str);
    }

    @DexIgnore
    public String v() {
        return a(this.m, "KEY_LOW_BATTERY_DEVICE", "");
    }

    @DexIgnore
    public void w(String str) {
        b(this.a, "KEY_LOW_BATTERY_DEVICE", str);
    }

    @DexIgnore
    public int x() {
        return a(this.m, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", 25);
    }

    @DexIgnore
    public void y(String str) {
        b(this.b, "USER_ACCESS_TOKEN", str);
    }

    @DexIgnore
    public void z(String str) {
        b(this.g, "KEY_WIDGETS_DATE_CHANGED", str);
    }

    @DexIgnore
    public final void a(SharedPreferences sharedPreferences, String str) {
        sharedPreferences.edit().remove(str).apply();
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, String str2) {
        sharedPreferences.edit().putString(str, str2).apply();
    }

    @DexIgnore
    public long c(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "lastReminderSyncTimeSuccess" + str, 0L);
    }

    @DexIgnore
    public long d() {
        return a(this.b, "ACCESS_TOKEN_TIME", 0L);
    }

    @DexIgnore
    public void e(int i2) {
        b(this.a, "KEY_DEBUG_REPLACE_BATTERY_LEVEL", i2);
    }

    @DexIgnore
    public String g(String str) {
        SharedPreferences sharedPreferences = this.a;
        return a(sharedPreferences, "tempDeviceFirmwareInfo_" + str, "");
    }

    @DexIgnore
    public List<String> h() {
        String a2 = a(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", "");
        if (TextUtils.isEmpty(a2)) {
            return new ArrayList();
        }
        try {
            return (List) new Gson().a(a2, new a(this).getType());
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SharedPreferencesManager", "getComplicationSearchedIdsRecent - parse json failed, json=" + a2 + ", ex=" + e2);
            return new ArrayList();
        }
    }

    @DexIgnore
    public Boolean j(String str) {
        return Boolean.valueOf(this.l.getBoolean(str, true));
    }

    @DexIgnore
    public void k(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setIsNeedRemoveRedundantContact - isNeed=" + z);
        b(this.g, "KEY_NEED_REMOVE_REDUNDANT_CONTACT", z);
    }

    @DexIgnore
    public boolean l() {
        return a(this.h, "KEY_COMMUTE_TIME_TUTORIAL", false);
    }

    @DexIgnore
    public void p(boolean z) {
        b(this.h, "KEY_SHOWING_RATING_PROMPT_CURRENT_VERSION", z);
    }

    @DexIgnore
    public void r(boolean z) {
        b(this.h, "KEY_REMINDERS_BEDTIME_REMINDER_ENABLE", z);
    }

    @DexIgnore
    public String s() {
        return a(this.g, "KEY_LOCALE_VERSION", "");
    }

    @DexIgnore
    public void v(String str) {
        b(this.g, "KEY_LOCALE_VERSION", str);
    }

    @DexIgnore
    public String w() {
        return this.m.getString("active_device_address", "");
    }

    @DexIgnore
    public void x(boolean z) {
        b(this.g, "KEY_FORCE_REGISTER_FIREBASE_TOKEN", z);
    }

    @DexIgnore
    public final String a(SharedPreferences sharedPreferences, String str, String str2) {
        return sharedPreferences.getString(str, str2);
    }

    @DexIgnore
    public final void b(kb5 kb5, String str, String str2) {
        if (kb5 instanceof ya5) {
            this.g.edit().putString(str, str2).apply();
        } else if (kb5 instanceof pb5) {
            this.h.edit().putString(str, str2).apply();
        }
    }

    @DexIgnore
    public void c(long j2) {
        b(this.h, "KEY_LAST_NO_CRASH_EVENT_TIMESTAMP", j2);
    }

    @DexIgnore
    public void d(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_WATCH_APP_SEARCHED_RECENT", new Gson().a(list));
        }
    }

    @DexIgnore
    public void e(boolean z) {
        b(this.h, "KEY_DND_SCHEDULED_ENABLE", z);
    }

    @DexIgnore
    public int i() {
        return a(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", 0);
    }

    @DexIgnore
    public Boolean m(String str) {
        SharedPreferences sharedPreferences = this.g;
        return Boolean.valueOf(a(sharedPreferences, "is_challenge_id_" + str, true));
    }

    @DexIgnore
    public String n() {
        return a(this.a, "KEY_WEARING_POSITION", "unspecified");
    }

    @DexIgnore
    public void q(boolean z) {
        b(this.h, "KEY_NEW_SOLUTION_FOR_SMS_ENABLE", z);
    }

    @DexIgnore
    public void u(boolean z) {
        b(this.h, "KEY_REMINDERS_SLEEP_SUMMARY_ENABLE", z);
    }

    @DexIgnore
    public final String a(kb5 kb5, String str, String str2) {
        if (kb5 instanceof ya5) {
            return this.g.getString(str, str2);
        }
        return kb5 instanceof pb5 ? this.h.getString(str, str2) : "";
    }

    @DexIgnore
    public void c(int i2) {
        b(this.h, "KEY_CURRENT_SYNC_SUCCESSULLY_SESSIONS", i2);
    }

    @DexIgnore
    public void q(String str) {
        b(this.h, "social_profile_id", str);
    }

    @DexIgnore
    public void c(Boolean bool) {
        b(this.a, "KEY_SYNC_SESSION_RUNNING", bool.booleanValue());
    }

    @DexIgnore
    public void d(boolean z) {
        b(this.h, "KEY_DND_MODE_HYBRID_ENABLE", z);
    }

    @DexIgnore
    public void o(boolean z) {
        b(this.h, "KEY_COMMUTE_TIME_TUTORIAL", z);
    }

    @DexIgnore
    public void c(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_MICRO_APP_SEARCHED_RECENT", new Gson().a(list));
        }
    }

    @DexIgnore
    public void d(Boolean bool) {
        b(this.j, "KEY_QUICK_RESPONSE", bool.booleanValue());
    }

    @DexIgnore
    public final void b(SharedPreferences sharedPreferences, String str, boolean z) {
        sharedPreferences.edit().putBoolean(str, z).apply();
    }

    @DexIgnore
    public final boolean a(SharedPreferences sharedPreferences, String str, boolean z) {
        return sharedPreferences.getBoolean(str, z);
    }

    @DexIgnore
    public void b(boolean z) {
        b(this.a, "AutoSyncEnable", z);
    }

    @DexIgnore
    public Long c() {
        return Long.valueOf(a(this.g, "set_sync_first_time", 0L));
    }

    @DexIgnore
    public Firmware a(String str) {
        SharedPreferences sharedPreferences = this.a;
        try {
            return (Firmware) new Gson().a(a(sharedPreferences, "bundle_latest_firmware_data" + str, ""), Firmware.class);
        } catch (oe4 e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e2);
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.e("SharedPreferencesManager", ".getBundleLatestFirmwareModel() failed, ex=" + e3);
        }
        return null;
    }

    @DexIgnore
    public HeartRateMode b(String str) {
        return HeartRateMode.Companion.fromValue(a(this.a, "HeartRateMode", HeartRateMode.NONE.getValue()));
    }

    @DexIgnore
    public String f(String str) {
        SharedPreferences sharedPreferences = this.g;
        return a(sharedPreferences, "localization_" + str, "");
    }

    @DexIgnore
    public void t(boolean z) {
        b(this.h, "KEY_REMINDERS_KEEP_GOING_ENABLE", z);
    }

    @DexIgnore
    public void b(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastSyncTime" + str, j2);
    }

    @DexIgnore
    public void b(long j2) {
        b(this.a, "lastHWSyncTime", j2);
    }

    @DexIgnore
    public String b(String str, kb5 kb5) {
        return a(kb5, "encryptedDataInKeyStore" + str, "");
    }

    @DexIgnore
    public void a(Firmware firmware, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "bundle_latest_firmware_data" + str, new Gson().a(firmware));
    }

    @DexIgnore
    public void b(String str, kb5 kb5, String str2) {
        b(kb5, "encryptedDataInKeyStore" + str, str2);
    }

    @DexIgnore
    public void a(HeartRateMode heartRateMode) {
        b(this.a, "HeartRateMode", heartRateMode.getValue());
    }

    @DexIgnore
    public void b(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_COMPLICATION_SEARCHED_RECENT", new Gson().a(list));
        }
    }

    @DexIgnore
    public void a(long j2, String str) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "lastReminderSyncTimeSuccess" + str, j2);
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "calibrationState_" + str + str2, z);
    }

    @DexIgnore
    public void b(String str, String str2) {
        SharedPreferences sharedPreferences = this.g;
        b(sharedPreferences, "localization_" + str2, str);
    }

    @DexIgnore
    public void a(String str, long j2, boolean z) {
        String str2;
        SharedPreferences sharedPreferences = this.a;
        if (z) {
            str2 = str + j2;
        } else {
            str2 = "";
        }
        b(sharedPreferences, "FULL_SYNC_DEVICE", str2);
    }

    @DexIgnore
    public void b(String str, Boolean bool) {
        SharedPreferences sharedPreferences = this.g;
        b(sharedPreferences, "is_challenge_id_" + str, bool.booleanValue());
    }

    @DexIgnore
    public void a(long j2) {
        b(this.b, "ACCESS_TOKEN_TIME", j2);
    }

    @DexIgnore
    public void b(Boolean bool) {
        b(this.g, SharePreferencesUtils.BC_STATUS, bool.booleanValue());
    }

    @DexIgnore
    public void a(String str, String str2) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "tempDeviceFirmwareInfo_" + str, str2);
    }

    @DexIgnore
    public int b() {
        return a(this.g, "set_sync_data_retry_time", 0);
    }

    @DexIgnore
    public void a(String str, boolean z) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "KEY_PAIR_COMPLETE_DEVICE" + str, z);
    }

    @DexIgnore
    public void b(int i2) {
        b(this.g, "set_sync_data_retry_time", i2);
    }

    @DexIgnore
    public void a(boolean z, String str) {
        SharedPreferences sharedPreferences = this.g;
        b(sharedPreferences, "KEY_MIGRATION_COMPLETE" + str, z);
    }

    @DexIgnore
    public String a(String str, kb5 kb5) {
        return a(kb5, "cipherIv" + str, "");
    }

    @DexIgnore
    public void a(String str, kb5 kb5, String str2) {
        b(kb5, "cipherIv" + str, str2);
    }

    @DexIgnore
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SharedPreferencesManager", "setAppDataVersion - appDataVersion=" + i2);
        b(this.g, "KEY_USER_LAST_DATA_VERSION", i2);
    }

    @DexIgnore
    public void a(List<String> list) {
        if (!list.isEmpty()) {
            b(this.h, "KEY_ADDRESS_SEARCHED_RECENT", new Gson().a(list));
        }
    }

    @DexIgnore
    public void a(String str, long j2) {
        SharedPreferences sharedPreferences = this.a;
        b(sharedPreferences, "DELAY_OTA_BY_USER" + str, j2);
    }

    @DexIgnore
    public void a(boolean z) {
        b(this.h, "KEY_ALL_APPS_TOGGLE_ENABLE", z);
    }

    @DexIgnore
    public void a(Boolean bool) {
        b(this.h, "is_need_sync_with_watch", bool.booleanValue());
    }

    @DexIgnore
    public Boolean a() {
        return Boolean.valueOf(a(this.g, SharePreferencesUtils.BC_STATUS, true));
    }

    @DexIgnore
    public void a(String str, Boolean bool) {
        this.k.putBoolean(str, bool.booleanValue());
        this.k.commit();
    }

    @DexIgnore
    public void a(Long l2) {
        b(this.g, "set_sync_first_time", l2.longValue());
    }
}
