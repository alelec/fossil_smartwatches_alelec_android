package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ds7 extends js7 {
    @DexIgnore
    public fs7 option;

    @DexIgnore
    public ds7(String str) {
        super(str);
    }

    @DexIgnore
    public fs7 getOption() {
        return this.option;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ds7(com.fossil.fs7 r3) {
        /*
            r2 = this;
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            java.lang.String r1 = "Missing argument for option: "
            r0.append(r1)
            java.lang.String r1 = r3.getKey()
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r2.<init>(r0)
            r2.option = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ds7.<init>(com.fossil.fs7):void");
    }
}
