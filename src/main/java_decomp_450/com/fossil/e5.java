package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.i5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e5 extends m5 {
    @DexIgnore
    public int m0; // = 0;
    @DexIgnore
    public ArrayList<p5> n0; // = new ArrayList<>(4);
    @DexIgnore
    public boolean o0; // = true;

    @DexIgnore
    @Override // com.fossil.i5
    public void G() {
        super.G();
        this.n0.clear();
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void H() {
        p5 p5Var;
        float f;
        p5 p5Var2;
        int i = this.m0;
        float f2 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                p5Var = ((i5) this).u.d();
            } else if (i == 2) {
                p5Var = ((i5) this).t.d();
            } else if (i == 3) {
                p5Var = ((i5) this).v.d();
            } else {
                return;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            p5Var = ((i5) this).s.d();
        }
        int size = this.n0.size();
        p5 p5Var3 = null;
        int i2 = 0;
        while (i2 < size) {
            p5 p5Var4 = this.n0.get(i2);
            if (((r5) p5Var4).b == 1) {
                int i3 = this.m0;
                if (i3 == 0 || i3 == 2) {
                    f = p5Var4.g;
                    if (f < f2) {
                        p5Var2 = p5Var4.f;
                    } else {
                        i2++;
                    }
                } else {
                    f = p5Var4.g;
                    if (f > f2) {
                        p5Var2 = p5Var4.f;
                    } else {
                        i2++;
                    }
                }
                p5Var3 = p5Var2;
                f2 = f;
                i2++;
            } else {
                return;
            }
        }
        if (y4.j() != null) {
            y4.j().y++;
        }
        p5Var.f = p5Var3;
        p5Var.g = f2;
        p5Var.a();
        int i4 = this.m0;
        if (i4 == 0) {
            ((i5) this).u.d().a(p5Var3, f2);
        } else if (i4 == 1) {
            ((i5) this).s.d().a(p5Var3, f2);
        } else if (i4 == 2) {
            ((i5) this).v.d().a(p5Var3, f2);
        } else if (i4 == 3) {
            ((i5) this).t.d().a(p5Var3, f2);
        }
    }

    @DexIgnore
    public boolean L() {
        return this.o0;
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void a(int i) {
        p5 p5Var;
        p5 p5Var2;
        i5 i5Var = ((i5) this).D;
        if (i5Var != null && ((j5) i5Var).u(2)) {
            int i2 = this.m0;
            if (i2 == 0) {
                p5Var = ((i5) this).s.d();
            } else if (i2 == 1) {
                p5Var = ((i5) this).u.d();
            } else if (i2 == 2) {
                p5Var = ((i5) this).t.d();
            } else if (i2 == 3) {
                p5Var = ((i5) this).v.d();
            } else {
                return;
            }
            p5Var.b(5);
            int i3 = this.m0;
            if (i3 == 0 || i3 == 1) {
                ((i5) this).t.d().a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ((i5) this).v.d().a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                ((i5) this).s.d().a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                ((i5) this).u.d().a((p5) null, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.n0.clear();
            for (int i4 = 0; i4 < ((m5) this).l0; i4++) {
                i5 i5Var2 = ((m5) this).k0[i4];
                if (this.o0 || i5Var2.b()) {
                    int i5 = this.m0;
                    if (i5 == 0) {
                        p5Var2 = i5Var2.s.d();
                    } else if (i5 == 1) {
                        p5Var2 = i5Var2.u.d();
                    } else if (i5 == 2) {
                        p5Var2 = i5Var2.t.d();
                    } else if (i5 != 3) {
                        p5Var2 = null;
                    } else {
                        p5Var2 = i5Var2.v.d();
                    }
                    if (p5Var2 != null) {
                        this.n0.add(p5Var2);
                        p5Var2.a(p5Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.i5
    public boolean b() {
        return true;
    }

    @DexIgnore
    public void c(boolean z) {
        this.o0 = z;
    }

    @DexIgnore
    public void u(int i) {
        this.m0 = i;
    }

    @DexIgnore
    @Override // com.fossil.i5
    public void a(y4 y4Var) {
        h5[] h5VarArr;
        boolean z;
        int i;
        int i2;
        h5[] h5VarArr2 = ((i5) this).A;
        h5VarArr2[0] = ((i5) this).s;
        h5VarArr2[2] = ((i5) this).t;
        h5VarArr2[1] = ((i5) this).u;
        h5VarArr2[3] = ((i5) this).v;
        int i3 = 0;
        while (true) {
            h5VarArr = ((i5) this).A;
            if (i3 >= h5VarArr.length) {
                break;
            }
            h5VarArr[i3].i = y4Var.a(h5VarArr[i3]);
            i3++;
        }
        int i4 = this.m0;
        if (i4 >= 0 && i4 < 4) {
            h5 h5Var = h5VarArr[i4];
            int i5 = 0;
            while (true) {
                if (i5 >= ((m5) this).l0) {
                    z = false;
                    break;
                }
                i5 i5Var = ((m5) this).k0[i5];
                if ((this.o0 || i5Var.b()) && ((((i = this.m0) == 0 || i == 1) && i5Var.k() == i5.b.MATCH_CONSTRAINT) || (((i2 = this.m0) == 2 || i2 == 3) && i5Var.r() == i5.b.MATCH_CONSTRAINT))) {
                    z = true;
                } else {
                    i5++;
                }
            }
            int i6 = this.m0;
            if (i6 == 0 || i6 == 1 ? l().k() == i5.b.WRAP_CONTENT : l().r() == i5.b.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < ((m5) this).l0; i7++) {
                i5 i5Var2 = ((m5) this).k0[i7];
                if (this.o0 || i5Var2.b()) {
                    c5 a = y4Var.a(i5Var2.A[this.m0]);
                    h5[] h5VarArr3 = i5Var2.A;
                    int i8 = this.m0;
                    h5VarArr3[i8].i = a;
                    if (i8 == 0 || i8 == 2) {
                        y4Var.b(h5Var.i, a, z);
                    } else {
                        y4Var.a(h5Var.i, a, z);
                    }
                }
            }
            int i9 = this.m0;
            if (i9 == 0) {
                y4Var.a(((i5) this).u.i, ((i5) this).s.i, 0, 6);
                if (!z) {
                    y4Var.a(((i5) this).s.i, ((i5) this).D.u.i, 0, 5);
                }
            } else if (i9 == 1) {
                y4Var.a(((i5) this).s.i, ((i5) this).u.i, 0, 6);
                if (!z) {
                    y4Var.a(((i5) this).s.i, ((i5) this).D.s.i, 0, 5);
                }
            } else if (i9 == 2) {
                y4Var.a(((i5) this).v.i, ((i5) this).t.i, 0, 6);
                if (!z) {
                    y4Var.a(((i5) this).t.i, ((i5) this).D.v.i, 0, 5);
                }
            } else if (i9 == 3) {
                y4Var.a(((i5) this).t.i, ((i5) this).v.i, 0, 6);
                if (!z) {
                    y4Var.a(((i5) this).t.i, ((i5) this).D.t.i, 0, 5);
                }
            }
        }
    }
}
