package com.fossil;

import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e46 extends he {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public Gson a; // = new Gson();
    @DexIgnore
    public CommuteTimeWatchAppSetting b;
    @DexIgnore
    public MFUser c;
    @DexIgnore
    public MutableLiveData<b> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UserRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public List<AddressWrapper> a;

        @DexIgnore
        public b(List<AddressWrapper> list) {
            this.a = list;
        }

        @DexIgnore
        public final List<AddressWrapper> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && ee7.a(this.a, ((b) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            List<AddressWrapper> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(addresses=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ e46 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeWatchAppSettingsViewModel$start$1$1", f = "CommuteTimeWatchAppSettingsViewModel.kt", l = {63}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository b = this.this$0.this$0.e;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(e46 e46, fb7 fb7) {
            super(2, fb7);
            this.this$0 = e46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            e46 e46;
            Object a2 = nb7.a();
            int i = this.label;
            List<AddressWrapper> list = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                e46 e462 = this.this$0;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = e462;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                e46 = e462;
            } else if (i == 1) {
                e46 = (e46) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            e46.c = (MFUser) obj;
            e46 e463 = this.this$0;
            CommuteTimeWatchAppSetting a3 = e463.b;
            if (a3 != null) {
                list = a3.getAddresses();
            }
            e463.a(list);
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = e46.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeWatchAppSetti\u2026el::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public e46(ch5 ch5, UserRepository userRepository) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
        this.e = userRepository;
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.d;
    }

    @DexIgnore
    public final boolean d() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (commuteTimeWatchAppSetting == null || commuteTimeWatchAppSetting.getAddresses().size() >= 10) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void e() {
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting b() {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(new ArrayList());
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
        if (commuteTimeWatchAppSetting2 != null) {
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting2.getAddresses();
            ArrayList arrayList = new ArrayList();
            for (T t : addresses) {
                if (!TextUtils.isEmpty(t.getAddress())) {
                    arrayList.add(t);
                }
            }
            commuteTimeWatchAppSetting.setAddresses(ea7.d((Collection) arrayList));
        }
        return commuteTimeWatchAppSetting;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting a() {
        return this.b;
    }

    @DexIgnore
    public final void a(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.a.a(str, CommuteTimeWatchAppSetting.class);
            List<AddressWrapper> addresses = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = addresses.iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                T next = it.next();
                if (next.getType() == AddressWrapper.AddressType.HOME) {
                    z = true;
                }
                if (z) {
                    arrayList.add(next);
                }
            }
            List<AddressWrapper> addresses2 = commuteTimeWatchAppSetting.getAddresses();
            ArrayList arrayList2 = new ArrayList();
            for (T t : addresses2) {
                if (t.getType() == AddressWrapper.AddressType.WORK) {
                    arrayList2.add(t);
                }
            }
            if (arrayList.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(0, new AddressWrapper(AddressWrapper.AddressType.HOME.getValue(), AddressWrapper.AddressType.HOME));
            }
            if (arrayList2.isEmpty()) {
                commuteTimeWatchAppSetting.getAddresses().add(1, new AddressWrapper(AddressWrapper.AddressType.WORK.getValue(), AddressWrapper.AddressType.WORK));
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local.d(str2, "exception when parse commute time setting " + e2);
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting(null, 1, null);
        }
        this.b = commuteTimeWatchAppSetting;
        if (commuteTimeWatchAppSetting == null) {
            this.b = new CommuteTimeWatchAppSetting(null, 1, null);
        }
    }

    @DexIgnore
    public final void b(AddressWrapper addressWrapper) {
        if (addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (addressWrapper.getType() == AddressWrapper.AddressType.OTHER) {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
                if (commuteTimeWatchAppSetting != null) {
                    commuteTimeWatchAppSetting.getAddresses().remove(addressWrapper);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                if (commuteTimeWatchAppSetting2 != null) {
                    commuteTimeWatchAppSetting2.getAddresses();
                    if (ee7.a((Object) addressWrapper.getId(), (Object) addressWrapper.getId())) {
                        addressWrapper.setAddress("");
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
            if (commuteTimeWatchAppSetting3 != null) {
                list = commuteTimeWatchAppSetting3.getAddresses();
            }
            a(list);
        }
    }

    @DexIgnore
    public final void a(AddressWrapper addressWrapper) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = this.b;
        if (commuteTimeWatchAppSetting != null && addressWrapper != null) {
            List<AddressWrapper> list = null;
            if (commuteTimeWatchAppSetting != null) {
                int i = 0;
                int i2 = -1;
                for (T t : commuteTimeWatchAppSetting.getAddresses()) {
                    int i3 = i + 1;
                    if (i >= 0) {
                        if (ee7.a((Object) addressWrapper.getId(), (Object) t.getId())) {
                            i2 = i;
                        }
                        i = i3;
                    } else {
                        w97.c();
                        throw null;
                    }
                }
                if (i2 != -1) {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = this.b;
                    if (commuteTimeWatchAppSetting2 != null) {
                        commuteTimeWatchAppSetting2.getAddresses().remove(i2);
                        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting3 = this.b;
                        if (commuteTimeWatchAppSetting3 != null) {
                            commuteTimeWatchAppSetting3.getAddresses().add(i2, addressWrapper);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting4 = this.b;
                    if (commuteTimeWatchAppSetting4 != null) {
                        commuteTimeWatchAppSetting4.getAddresses().add(addressWrapper);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting5 = this.b;
                if (commuteTimeWatchAppSetting5 != null) {
                    list = commuteTimeWatchAppSetting5.getAddresses();
                }
                a(list);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<AddressWrapper> list) {
        this.d.a(new b(list));
    }
}
