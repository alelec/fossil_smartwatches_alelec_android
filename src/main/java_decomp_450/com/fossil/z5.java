package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z5 {
    @DexIgnore
    public static /* final */ int[] ColorStateListItem; // = {16843173, 16843551, 2130968707};
    @DexIgnore
    public static /* final */ int ColorStateListItem_alpha; // = 2;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_alpha; // = 1;
    @DexIgnore
    public static /* final */ int ColorStateListItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout; // = {2130969321, 2130969701};
    @DexIgnore
    public static /* final */ int[] CoordinatorLayout_Layout; // = {16842931, 2130969330, 2130969331, 2130969333, 2130969377, 2130969387, 2130969388};
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_android_layout_gravity; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchor; // = 1;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_anchorGravity; // = 2;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_behavior; // = 3;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_dodgeInsetEdges; // = 4;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_insetEdge; // = 5;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_Layout_layout_keyline; // = 6;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_keylines; // = 0;
    @DexIgnore
    public static /* final */ int CoordinatorLayout_statusBarBackground; // = 1;
    @DexIgnore
    public static /* final */ int[] FontFamily; // = {2130969198, 2130969199, 2130969200, 2130969201, 2130969202, 2130969203};
    @DexIgnore
    public static /* final */ int[] FontFamilyFont; // = {16844082, 16844083, 16844095, 16844143, 16844144, 2130969196, 2130969204, 2130969205, 2130969206, 2130969842};
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_font; // = 0;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontStyle; // = 2;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontVariationSettings; // = 4;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_fontWeight; // = 1;
    @DexIgnore
    public static /* final */ int FontFamilyFont_android_ttcIndex; // = 3;
    @DexIgnore
    public static /* final */ int FontFamilyFont_font; // = 5;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontStyle; // = 6;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontVariationSettings; // = 7;
    @DexIgnore
    public static /* final */ int FontFamilyFont_fontWeight; // = 8;
    @DexIgnore
    public static /* final */ int FontFamilyFont_ttcIndex; // = 9;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderAuthority; // = 0;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderCerts; // = 1;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchStrategy; // = 2;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderFetchTimeout; // = 3;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderPackage; // = 4;
    @DexIgnore
    public static /* final */ int FontFamily_fontProviderQuery; // = 5;
    @DexIgnore
    public static /* final */ int[] GradientColor; // = {16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051};
    @DexIgnore
    public static /* final */ int[] GradientColorItem; // = {16843173, 16844052};
    @DexIgnore
    public static /* final */ int GradientColorItem_android_color; // = 0;
    @DexIgnore
    public static /* final */ int GradientColorItem_android_offset; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerColor; // = 7;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerX; // = 3;
    @DexIgnore
    public static /* final */ int GradientColor_android_centerY; // = 4;
    @DexIgnore
    public static /* final */ int GradientColor_android_endColor; // = 1;
    @DexIgnore
    public static /* final */ int GradientColor_android_endX; // = 10;
    @DexIgnore
    public static /* final */ int GradientColor_android_endY; // = 11;
    @DexIgnore
    public static /* final */ int GradientColor_android_gradientRadius; // = 5;
    @DexIgnore
    public static /* final */ int GradientColor_android_startColor; // = 0;
    @DexIgnore
    public static /* final */ int GradientColor_android_startX; // = 8;
    @DexIgnore
    public static /* final */ int GradientColor_android_startY; // = 9;
    @DexIgnore
    public static /* final */ int GradientColor_android_tileMode; // = 6;
    @DexIgnore
    public static /* final */ int GradientColor_android_type; // = 2;
}
