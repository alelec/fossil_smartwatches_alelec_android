package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o91 implements Parcelable.Creator<ib1> {
    @DexIgnore
    public /* synthetic */ o91(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ib1 createFromParcel(Parcel parcel) {
        return new ib1(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ib1[] newArray(int i) {
        return new ib1[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public ib1 m45createFromParcel(Parcel parcel) {
        return new ib1(parcel, null);
    }
}
