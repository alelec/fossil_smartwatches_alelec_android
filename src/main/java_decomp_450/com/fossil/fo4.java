package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo4 {
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("socialId")
    public String b;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @te4("points")
    public Integer e;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String f;
    @DexIgnore
    @te4("createdAt")
    public String g;
    @DexIgnore
    @te4("updatedAt")
    public String h;

    @DexIgnore
    public fo4(String str, String str2, String str3, String str4, Integer num, String str5, String str6, String str7) {
        ee7.b(str, "id");
        ee7.b(str2, "socialId");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = num;
        this.f = str5;
        this.g = str6;
        this.h = str7;
    }

    @DexIgnore
    public final String a() {
        return this.g;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final Integer e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof fo4)) {
            return false;
        }
        fo4 fo4 = (fo4) obj;
        return ee7.a(this.a, fo4.a) && ee7.a(this.b, fo4.b) && ee7.a(this.c, fo4.c) && ee7.a(this.d, fo4.d) && ee7.a(this.e, fo4.e) && ee7.a(this.f, fo4.f) && ee7.a(this.g, fo4.g) && ee7.a(this.h, fo4.h);
    }

    @DexIgnore
    public final String f() {
        return this.f;
    }

    @DexIgnore
    public final String g() {
        return this.b;
    }

    @DexIgnore
    public final String h() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Integer num = this.e;
        int hashCode5 = (hashCode4 + (num != null ? num.hashCode() : 0)) * 31;
        String str5 = this.f;
        int hashCode6 = (hashCode5 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.g;
        int hashCode7 = (hashCode6 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.h;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public String toString() {
        return "Profile(id=" + this.a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", points=" + this.e + ", profilePicture=" + this.f + ", createdAt=" + this.g + ", updatedAt=" + this.h + ")";
    }
}
