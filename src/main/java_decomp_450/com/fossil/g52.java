package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g52 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ h52 a;

    @DexIgnore
    public g52(h52 h52) {
        this.a = h52;
    }

    @DexIgnore
    public final void run() {
        this.a.r.lock();
        try {
            this.a.h();
        } finally {
            this.a.r.unlock();
        }
    }
}
