package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mz4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    public mz4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, ImageView imageView, ImageView imageView2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, View view2, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = flexibleButton;
        this.t = flexibleButton2;
        this.u = flexibleButton3;
        this.v = imageView;
        this.w = imageView2;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = view2;
        this.A = view3;
    }
}
