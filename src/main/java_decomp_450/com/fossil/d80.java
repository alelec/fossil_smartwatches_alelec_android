package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d80 extends n80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<d80> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final d80 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 1) {
                return new d80(yz0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).get(0)));
            }
            throw new IllegalArgumentException(yh0.a(yh0.b("Invalid data size: "), bArr.length, ", ", "require: 1"));
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public d80 createFromParcel(Parcel parcel) {
            return new d80(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public d80[] newArray(int i) {
            return new d80[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public d80 m11createFromParcel(Parcel parcel) {
            return new d80(parcel, null);
        }
    }

    @DexIgnore
    public d80(short s) {
        super(o80.CURRENT_HEART_RATE);
        this.b = s;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) this.b).array();
        ee7.a((Object) array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(d80.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((d80) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.CurrentHeartRateConfig");
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public int hashCode() {
        return (super.hashCode() * 31) + this.b;
    }

    @DexIgnore
    @Override // com.fossil.n80
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte((byte) this.b);
        }
    }

    @DexIgnore
    @Override // com.fossil.n80
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.w3, Short.valueOf(this.b));
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ d80(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = yz0.b(parcel.readByte());
    }
}
