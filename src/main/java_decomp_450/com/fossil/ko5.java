package com.fossil;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.gx6;
import com.fossil.ov3;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.home.HomeActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko5 extends go5 implements yo6 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public xo6 f;
    @DexIgnore
    public wl4 g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ String j; // = eh5.l.a().b("disabledButton");
    @DexIgnore
    public /* final */ String p; // = eh5.l.a().b("primaryColor");
    @DexIgnore
    public qw6<c15> q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ko5 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            ko5 ko5 = new ko5();
            ko5.setArguments(bundle);
            return ko5;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ov3.b {
        @DexIgnore
        public /* final */ /* synthetic */ ko5 a;

        @DexIgnore
        public b(ko5 ko5) {
            this.a = ko5;
        }

        @DexIgnore
        @Override // com.fossil.ov3.b
        public final void a(TabLayout.g gVar, int i) {
            ee7.b(gVar, "tab");
            if (!TextUtils.isEmpty(this.a.j) && !TextUtils.isEmpty(this.a.p)) {
                int parseColor = Color.parseColor(this.a.j);
                int parseColor2 = Color.parseColor(this.a.p);
                gVar.b(2131230963);
                if (i == this.a.i) {
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor2);
                        return;
                    }
                    return;
                }
                Drawable b2 = gVar.b();
                if (b2 != null) {
                    b2.setTint(parseColor);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ c15 a;
        @DexIgnore
        public /* final */ /* synthetic */ ko5 b;

        @DexIgnore
        public c(c15 c15, ko5 ko5) {
            this.a = c15;
            this.b = ko5;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void a(int i, float f, int i2) {
            Drawable b2;
            Drawable b3;
            super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.b.p)) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("ExploreWatchFragment", "set icon color " + this.b.p);
                int parseColor = Color.parseColor(this.b.p);
                TabLayout.g b4 = this.a.s.b(i);
                if (!(b4 == null || (b3 = b4.b()) == null)) {
                    b3.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.b.j) && this.b.i != i) {
                int parseColor2 = Color.parseColor(this.b.j);
                TabLayout.g b5 = this.a.s.b(this.b.i);
                if (!(b5 == null || (b2 = b5.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.b.i = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ko5 a;

        @DexIgnore
        public d(ko5 ko5) {
            this.a = ko5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ko5.c(this.a).h();
        }
    }

    @DexIgnore
    public static final /* synthetic */ xo6 c(ko5 ko5) {
        xo6 xo6 = ko5.f;
        if (xo6 != null) {
            return xo6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ExploreWatchFragment";
    }

    @DexIgnore
    @Override // com.fossil.yo6
    public void e() {
        DashBar dashBar;
        qw6<c15> qw6 = this.q;
        if (qw6 != null) {
            c15 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.t) != null) {
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.a(dashBar, this.h, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<c15> qw6 = new qw6<>(this, (c15) qb.a(layoutInflater, 2131558553, viewGroup, false, a1()));
        this.q = qw6;
        if (qw6 != null) {
            c15 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        wl4 wl4 = this.g;
        if (wl4 != null) {
            wl4.c();
            xo6 xo6 = this.f;
            if (xo6 != null) {
                xo6.g();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        xo6 xo6 = this.f;
        if (xo6 != null) {
            xo6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        this.g = new wl4();
        qw6<c15> qw6 = this.q;
        if (qw6 != null) {
            c15 a2 = qw6.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.u;
                ee7.a((Object) viewPager2, "binding.vpExplore");
                wl4 wl4 = this.g;
                if (wl4 != null) {
                    viewPager2.setAdapter(wl4);
                    if (a2.u.getChildAt(0) != null) {
                        View childAt = a2.u.getChildAt(0);
                        if (childAt != null) {
                            ((RecyclerView) childAt).setOverScrollMode(2);
                        } else {
                            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                        }
                    }
                    TabLayout tabLayout = a2.s;
                    ViewPager2 viewPager22 = a2.u;
                    if (viewPager22 != null) {
                        new ov3(tabLayout, viewPager22, new b(this)).a();
                        a2.u.a(new c(a2, this));
                        a2.q.setOnClickListener(new d(this));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            }
            Bundle arguments = getArguments();
            if (arguments != null) {
                boolean z = arguments.getBoolean("IS_ONBOARDING_FLOW");
                this.h = z;
                xo6 xo6 = this.f;
                if (xo6 != null) {
                    xo6.a(z);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yo6
    public void q0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) activity, "it");
            HomeActivity.a.a(aVar, activity, null, 2, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.yo6
    public void u(List<? extends Explore> list) {
        ee7.b(list, "data");
        wl4 wl4 = this.g;
        if (wl4 != null) {
            wl4.a(list);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(xo6 xo6) {
        ee7.b(xo6, "presenter");
        this.f = xo6;
    }
}
