package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj7 implements dk7 {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public uj7(boolean z) {
        this.a = z;
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public uk7 a() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public boolean isActive() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Empty{");
        sb.append(isActive() ? "Active" : "New");
        sb.append('}');
        return sb.toString();
    }
}
