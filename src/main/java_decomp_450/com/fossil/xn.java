package com.fossil;

import android.content.Context;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xn extends vn<qn> {
    @DexIgnore
    public static /* final */ String e; // = im.a("NetworkMeteredCtrlr");

    @DexIgnore
    public xn(Context context, vp vpVar) {
        super(ho.a(context, vpVar).c());
    }

    @DexIgnore
    @Override // com.fossil.vn
    public boolean a(zo zoVar) {
        return zoVar.j.b() == jm.METERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(qn qnVar) {
        if (Build.VERSION.SDK_INT < 26) {
            im.a().a(e, "Metered network constraint is not supported before API 26, only checking for connected state.", new Throwable[0]);
            return !qnVar.a();
        } else if (!qnVar.a() || !qnVar.b()) {
            return true;
        } else {
            return false;
        }
    }
}
