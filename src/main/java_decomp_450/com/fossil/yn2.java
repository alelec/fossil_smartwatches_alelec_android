package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yn2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public yn2(sn2 sn2, Activity activity, String str, String str2) {
        super(sn2);
        this.h = sn2;
        this.e = activity;
        this.f = str;
        this.g = str2;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.h.h.setCurrentScreen(cb2.a(this.e), this.f, this.g, ((sn2.a) this).a);
    }
}
