package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pt6 {
    @DexIgnore
    public /* final */ cl5 a;
    @DexIgnore
    public /* final */ jt6 b;

    @DexIgnore
    public pt6(cl5 cl5, jt6 jt6) {
        ee7.b(cl5, "mContext");
        ee7.b(jt6, "mView");
        this.a = cl5;
        this.b = jt6;
    }

    @DexIgnore
    public final jt6 a() {
        return this.b;
    }
}
