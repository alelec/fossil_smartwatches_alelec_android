package com.fossil;

import com.fossil.t80;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class qo1 extends ce7 implements gd7<byte[], t80> {
    @DexIgnore
    public qo1(t80.b bVar) {
        super(1, bVar);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final uf7 getOwner() {
        return te7.a(t80.b.class);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/InactiveNudgeConfig;";
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public t80 invoke(byte[] bArr) {
        return ((t80.b) ((vd7) this).receiver).a(bArr);
    }
}
