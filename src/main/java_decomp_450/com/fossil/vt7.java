package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vt7 extends Serializable {
    @DexIgnore
    public static final String ANY_MARKER = "*";
    @DexIgnore
    public static final String ANY_NON_NULL_MARKER = "+";
}
