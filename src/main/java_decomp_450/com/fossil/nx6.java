package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.fl4;
import com.fossil.nw5;
import com.fossil.rd5;
import com.fossil.vu5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nx6 {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ nx6 b; // = new nx6();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2", f = "NotificationAppHelper.kt", l = {150, 151}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super List<AppNotificationFilter>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ vu5 $getAllContactGroup;
        @DexIgnore
        public /* final */ /* synthetic */ nw5 $getApps;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationSettingsDatabase $notificationSettingsDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ ch5 $sharedPrefs;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nx6$a$a")
        @tb7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$contactMessageDefer$1", f = "NotificationAppHelper.kt", l = {99}, m = "invokeSuspend")
        /* renamed from: com.fossil.nx6$a$a  reason: collision with other inner class name */
        public static final class C0143a extends zb7 implements kd7<yi7, fb7<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $callSettingsType;
            @DexIgnore
            public /* final */ /* synthetic */ qe7 $messageSettingsType;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0143a(a aVar, qe7 qe7, qe7 qe72, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
                this.$callSettingsType = qe7;
                this.$messageSettingsType = qe72;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0143a aVar = new C0143a(this.this$0, this.$callSettingsType, this.$messageSettingsType, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<AppNotificationFilter>> fb7) {
                return ((C0143a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    vu5 vu5 = this.this$0.$getAllContactGroup;
                    this.L$0 = yi7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    obj = gl4.a(vu5, null, this);
                    if (obj == a) {
                        return a;
                    }
                    list = arrayList;
                } else if (i == 1) {
                    list = (List) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                fl4.c cVar = (fl4.c) obj;
                if (cVar instanceof vu5.d) {
                    List d = ea7.d((Collection) ((vu5.d) cVar).a());
                    int i2 = this.$callSettingsType.element;
                    if (i2 == 0) {
                        DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                        list.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                    } else if (i2 == 1) {
                        int size = d.size();
                        for (int i3 = 0; i3 < size; i3++) {
                            ContactGroup contactGroup = (ContactGroup) d.get(i3);
                            DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                            List<Contact> contacts = contactGroup.getContacts();
                            ee7.a((Object) contacts, "item.contacts");
                            if (!contacts.isEmpty()) {
                                Contact contact = contactGroup.getContacts().get(0);
                                ee7.a((Object) contact, "item.contacts[0]");
                                appNotificationFilter.setSender(contact.getDisplayName());
                                list.add(appNotificationFilter);
                            }
                        }
                    }
                    int i4 = this.$messageSettingsType.element;
                    if (i4 == 0) {
                        DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                        list.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                    } else if (i4 == 1) {
                        int size2 = d.size();
                        for (int i5 = 0; i5 < size2; i5++) {
                            ContactGroup contactGroup2 = (ContactGroup) d.get(i5);
                            DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            FNotification fNotification = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                            List<Contact> contacts2 = contactGroup2.getContacts();
                            ee7.a((Object) contacts2, "item.contacts");
                            if (!contacts2.isEmpty()) {
                                AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification);
                                Contact contact2 = contactGroup2.getContacts().get(0);
                                ee7.a((Object) contact2, "item.contacts[0]");
                                appNotificationFilter2.setSender(contact2.getDisplayName());
                                list.add(appNotificationFilter2);
                            }
                        }
                    }
                }
                return list;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1", f = "NotificationAppHelper.kt", l = {143}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<AppNotificationFilter>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationAllFilterList;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(a aVar, List list, boolean z, fb7 fb7) {
                super(2, fb7);
                this.this$0 = aVar;
                this.$notificationAllFilterList = list;
                this.$isAllAppToggleEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<AppNotificationFilter>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ArrayList arrayList = new ArrayList();
                    nw5 nw5 = this.this$0.$getApps;
                    this.L$0 = yi7;
                    this.L$1 = arrayList;
                    this.label = 1;
                    obj = gl4.a(nw5, null, this);
                    if (obj == a) {
                        return a;
                    }
                    list = arrayList;
                } else if (i == 1) {
                    list = (List) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                fl4.c cVar = (fl4.c) obj;
                if (cVar instanceof nw5.a) {
                    this.$notificationAllFilterList.addAll(mx6.a(((nw5.a) cVar).a(), this.$isAllAppToggleEnabled));
                }
                return list;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ch5 ch5, NotificationSettingsDatabase notificationSettingsDatabase, vu5 vu5, nw5 nw5, fb7 fb7) {
            super(2, fb7);
            this.$sharedPrefs = ch5;
            this.$notificationSettingsDatabase = notificationSettingsDatabase;
            this.$getAllContactGroup = vu5;
            this.$getApps = nw5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.$sharedPrefs, this.$notificationSettingsDatabase, this.$getAllContactGroup, this.$getApps, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super List<AppNotificationFilter>> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            List list;
            Object obj2;
            List list2;
            yi7 yi7;
            qe7 qe7;
            qe7 qe72;
            boolean z;
            hj7 hj7;
            NotificationSettingsModel notificationSettingsModel;
            NotificationSettingsModel notificationSettingsModel2;
            hj7 hj72;
            Object obj3;
            List list3;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi72 = this.p$;
                qe7 qe73 = new qe7();
                qe73.element = 0;
                qe7 qe74 = new qe7();
                qe74.element = 0;
                list = new ArrayList();
                boolean F = this.$sharedPrefs.F();
                NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
                NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = this.$notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = nx6.a;
                local.d(a2, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + F);
                if (notificationSettingsWithIsCallNoLiveData != null) {
                    qe73.element = notificationSettingsWithIsCallNoLiveData.getSettingsType();
                } else {
                    this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
                }
                if (notificationSettingsWithIsCallNoLiveData2 != null) {
                    qe74.element = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
                } else {
                    this.$notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
                }
                hj7 a3 = xh7.a(yi72, null, null, new C0143a(this, qe73, qe74, null), 3, null);
                hj7 = xh7.a(yi72, null, null, new b(this, list, F, null), 3, null);
                this.L$0 = yi72;
                this.L$1 = qe73;
                this.L$2 = qe74;
                this.L$3 = list;
                this.Z$0 = F;
                this.L$4 = notificationSettingsWithIsCallNoLiveData;
                this.L$5 = notificationSettingsWithIsCallNoLiveData2;
                this.L$6 = a3;
                this.L$7 = hj7;
                this.L$8 = list;
                this.label = 1;
                obj3 = a3.c(this);
                if (obj3 == a) {
                    return a;
                }
                yi7 = yi72;
                qe7 = qe73;
                qe72 = qe74;
                z = F;
                notificationSettingsModel = notificationSettingsWithIsCallNoLiveData;
                notificationSettingsModel2 = notificationSettingsWithIsCallNoLiveData2;
                hj72 = a3;
                list3 = list;
            } else if (i == 1) {
                list3 = (List) this.L$8;
                boolean z2 = this.Z$0;
                t87.a(obj);
                yi7 = (yi7) this.L$0;
                qe7 = (qe7) this.L$1;
                qe72 = (qe7) this.L$2;
                z = z2;
                notificationSettingsModel = (NotificationSettingsModel) this.L$4;
                notificationSettingsModel2 = (NotificationSettingsModel) this.L$5;
                hj72 = (hj7) this.L$6;
                list = (List) this.L$3;
                hj7 = (hj7) this.L$7;
                obj3 = obj;
            } else if (i == 2) {
                list2 = (List) this.L$8;
                hj7 hj73 = (hj7) this.L$7;
                hj7 hj74 = (hj7) this.L$6;
                NotificationSettingsModel notificationSettingsModel3 = (NotificationSettingsModel) this.L$5;
                NotificationSettingsModel notificationSettingsModel4 = (NotificationSettingsModel) this.L$4;
                qe7 qe75 = (qe7) this.L$2;
                qe7 qe76 = (qe7) this.L$1;
                yi7 yi73 = (yi7) this.L$0;
                t87.a(obj);
                list = (List) this.L$3;
                obj2 = obj;
                list2.addAll((Collection) obj2);
                return list;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list3.addAll((Collection) obj3);
            this.L$0 = yi7;
            this.L$1 = qe7;
            this.L$2 = qe72;
            this.L$3 = list;
            this.Z$0 = z;
            this.L$4 = notificationSettingsModel;
            this.L$5 = notificationSettingsModel2;
            this.L$6 = hj72;
            this.L$7 = hj7;
            this.L$8 = list;
            this.label = 2;
            obj2 = hj7.c(this);
            if (obj2 == a) {
                return a;
            }
            list2 = list;
            list2.addAll((Collection) obj2);
            return list;
        }
    }

    /*
    static {
        String simpleName = nx6.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationAppHelper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final List<AppFilter> a(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? ah5.p.a().a().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter a(String str, MFDeviceFamily mFDeviceFamily) {
        ee7.b(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return ah5.p.a().a().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final boolean a(String str) {
        ee7.b(str, "type");
        return a(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final Object a(nw5 nw5, vu5 vu5, NotificationSettingsDatabase notificationSettingsDatabase, ch5 ch5, fb7<? super List<AppNotificationFilter>> fb7) {
        return vh7.a(qj7.a(), new a(ch5, notificationSettingsDatabase, vu5, nw5, null), fb7);
    }

    @DexIgnore
    public final AppNotificationFilterSettings a(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        String str;
        ArrayList arrayList = new ArrayList();
        if (sparseArray != null) {
            SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
            ee7.a((Object) clone, "it.clone()");
            short s = -1;
            int size = clone.size();
            for (int i = 0; i < size; i++) {
                List<BaseFeatureModel> valueAt = clone.valueAt(i);
                if (valueAt != null) {
                    for (BaseFeatureModel baseFeatureModel : valueAt) {
                        if (baseFeatureModel.isEnabled()) {
                            short c = (short) ze5.c(baseFeatureModel.getHour());
                            int i2 = 10000;
                            if (baseFeatureModel instanceof ContactGroup) {
                                ContactGroup contactGroup = (ContactGroup) baseFeatureModel;
                                if (contactGroup.getContacts() != null) {
                                    for (Contact contact : contactGroup.getContacts()) {
                                        ee7.a((Object) contact, "contact");
                                        if (contact.isUseCall()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
                                            }
                                            aa0 aa0 = new aa0(c, c, s, i2);
                                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                            AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
                                            appNotificationFilter.setSender(contact.getDisplayName());
                                            appNotificationFilter.setHandMovingConfig(aa0);
                                            appNotificationFilter.setVibePattern(ca0.CALL);
                                            arrayList.add(appNotificationFilter);
                                        }
                                        if (contact.isUseSms()) {
                                            if (z) {
                                                s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
                                            }
                                            aa0 aa02 = new aa0(c, c, s, i2);
                                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                            AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
                                            appNotificationFilter2.setSender(contact.getDisplayName());
                                            appNotificationFilter2.setHandMovingConfig(aa02);
                                            appNotificationFilter2.setVibePattern(ca0.TEXT);
                                            arrayList.add(appNotificationFilter2);
                                        }
                                        i2 = 10000;
                                    }
                                }
                            } else if (baseFeatureModel instanceof AppFilter) {
                                AppFilter appFilter = (AppFilter) baseFeatureModel;
                                String type = appFilter.getType();
                                if (!(type == null || mh7.a(type))) {
                                    if (appFilter.getName() == null) {
                                        str = "";
                                    } else {
                                        str = appFilter.getName();
                                    }
                                    if (z) {
                                        s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
                                    }
                                    aa0 aa03 = new aa0(c, c, s, 10000);
                                    ee7.a((Object) str, "appName");
                                    String type2 = appFilter.getType();
                                    ee7.a((Object) type2, "item.type");
                                    AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(str, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
                                    appNotificationFilter3.setHandMovingConfig(aa03);
                                    appNotificationFilter3.setVibePattern(ca0.DEFAULT_OTHER_APPS);
                                    arrayList.add(appNotificationFilter3);
                                }
                            }
                        }
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, "buildAppNotificationFilterSettings size = " + arrayList.size());
        return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final boolean a(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku specialSku;
        ee7.b(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            specialSku = companion.fromType(sku);
        } else {
            specialSku = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return specialSku == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(Context context, NotificationSettingsDatabase notificationSettingsDatabase, ch5 ch5) {
        int i;
        ee7.b(context, "context");
        ee7.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        ee7.b(ch5, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean F = ch5.F();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        int i2 = 0;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + F);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> a2 = a(i, i2);
        List<at5> a3 = a(context);
        arrayList.addAll(a2);
        arrayList.addAll(mx6.a(a3, F));
        return arrayList;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(int i, int i2) {
        ArrayList arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = ah5.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (i == 0) {
            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
            arrayList.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
        } else if (i == 1) {
            int size = allContactGroups.size();
            for (int i3 = 0; i3 < size; i3++) {
                ContactGroup contactGroup = allContactGroups.get(i3);
                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                ee7.a((Object) contactGroup, "item");
                List<Contact> contacts = contactGroup.getContacts();
                ee7.a((Object) contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                    Contact contact = contactGroup.getContacts().get(0);
                    ee7.a((Object) contact, "item.contacts[0]");
                    appNotificationFilter.setSender(contact.getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        if (i2 == 0) {
            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
            arrayList.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
        } else if (i2 == 1) {
            int size2 = allContactGroups.size();
            for (int i4 = 0; i4 < size2; i4++) {
                ContactGroup contactGroup2 = allContactGroups.get(i4);
                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                ee7.a((Object) contactGroup2, "item");
                List<Contact> contacts2 = contactGroup2.getContacts();
                ee7.a((Object) contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                    Contact contact2 = contactGroup2.getContacts().get(0);
                    ee7.a((Object) contact2, "item.contacts[0]");
                    appNotificationFilter2.setSender(contact2.getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<at5> a(Context context) {
        List<AppFilter> allAppFilters = ah5.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<rd5.b> it = rd5.g.f().iterator();
        while (it.hasNext()) {
            rd5.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !mh7.b(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator<AppFilter> it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter next2 = it2.next();
                    ee7.a((Object) next2, "appFilter");
                    if (ee7.a((Object) next2.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(next2.getDbRowId());
                        installedApp.setCurrentHandGroup(next2.getHour());
                        break;
                    }
                }
                at5 at5 = new at5();
                at5.setInstalledApp(installedApp);
                at5.setUri(next.c());
                at5.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(at5);
            }
        }
        return linkedList;
    }
}
