package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl1 {
    @DexIgnore
    public /* synthetic */ gl1(zd7 zd7) {
    }

    @DexIgnore
    public final byte[] a(g70[] g70Arr, short s, r60 r60) {
        Object[] array = t97.d(g70Arr).toArray(new g70[0]);
        if (array != null) {
            g70[] g70Arr2 = (g70[]) array;
            if (g70Arr2.length == 0) {
                return new byte[0];
            }
            return zr0.d.a(s, r60, g70Arr2);
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
