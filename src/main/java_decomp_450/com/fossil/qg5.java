package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qg5 {
    @DexIgnore
    public static /* final */ ti7 h; // = qj7.b();
    @DexIgnore
    public int a;
    @DexIgnore
    public kn7 b; // = mn7.a(false, 1, null);
    @DexIgnore
    public ApiServiceV2 c;
    @DexIgnore
    public /* final */ LinkedBlockingDeque<b> d; // = new LinkedBlockingDeque<>();
    @DexIgnore
    public /* final */ List<String> e; // = new ArrayList();
    @DexIgnore
    public /* final */ ki7 f;
    @DexIgnore
    public /* final */ yi7 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ LocalFile b;
        @DexIgnore
        public /* final */ c c;

        @DexIgnore
        public b(LocalFile localFile, c cVar) {
            ee7.b(localFile, "localFile");
            this.b = localFile;
            this.c = cVar;
        }

        @DexIgnore
        public final c a() {
            return this.c;
        }

        @DexIgnore
        public final LocalFile b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof b)) {
                return false;
            }
            return ee7.a((Object) this.b.getRemoteUrl(), (Object) ((b) obj).b.getRemoteUrl());
        }

        @DexIgnore
        public int hashCode() {
            int hashCode = this.b.hashCode() * 31;
            c cVar = this.c;
            return hashCode + (cVar != null ? cVar.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return "DownloadFileJobInfo(localFile=" + this.b + ", callback=" + this.c + ")";
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void onComplete(boolean z, LocalFile localFile);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1", f = "FileDownloadManager.kt", l = {97}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ b $job;
        @DexIgnore
        public /* final */ /* synthetic */ LocalFile $localFile;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qg5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.manager.FileDownloadManager$downloadFile$1$repoResponse$1", f = "FileDownloadManager.kt", l = {97}, m = "invokeSuspend")
        public static final class a extends zb7 implements gd7<fb7<? super fv7<mo7>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(1, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(fb7<?> fb7) {
                ee7.b(fb7, "completion");
                return new a(this.this$0, fb7);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public final Object invoke(fb7<? super fv7<mo7>> fb7) {
                return ((a) create(fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    ApiServiceV2 b = this.this$0.this$0.b();
                    String remoteUrl = this.this$0.$localFile.getRemoteUrl();
                    this.label = 1;
                    obj = b.downloadFile(remoteUrl, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(qg5 qg5, LocalFile localFile, b bVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = qg5;
            this.$localFile = localFile;
            this.$job = bVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$localFile, this.$job, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01f7  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0268  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x0352  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = -1
                r3 = 0
                r4 = 0
                java.lang.String r5 = "FileDownloadManager"
                r6 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 != r6) goto L_0x0027
                java.lang.Object r0 = r12.L$3
                java.lang.String r0 = (java.lang.String) r0
                java.lang.Object r1 = r12.L$2
                java.io.File r1 = (java.io.File) r1
                java.lang.Object r7 = r12.L$1
                java.lang.String r7 = (java.lang.String) r7
                int r7 = r12.I$0
                java.lang.Object r8 = r12.L$0
                com.fossil.yi7 r8 = (com.fossil.yi7) r8
                com.fossil.t87.a(r13)
                goto L_0x0133
            L_0x0027:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x002f:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "Downloading "
                r7.append(r8)
                com.portfolio.platform.data.model.LocalFile r8 = r12.$localFile
                java.lang.String r8 = r8.getRemoteUrl()
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                r1.d(r5, r7)
                com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r1 = r1.c()
                android.content.Context r1 = r1.getApplicationContext()
                com.portfolio.platform.data.model.LocalFile r7 = r12.$localFile
                com.misfit.frameworks.buttonservice.model.FileType r7 = r7.getType()
                java.lang.String r1 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r1, r7)
                java.io.File r7 = new java.io.File
                r7.<init>(r1)
                boolean r8 = r7.exists()
                if (r8 != 0) goto L_0x0076
                r7.mkdirs()
            L_0x0076:
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                r8.append(r1)
                java.lang.String r9 = java.io.File.separator
                r8.append(r9)
                com.portfolio.platform.data.model.LocalFile r9 = r12.$localFile
                java.lang.String r9 = r9.getFileName()
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                java.io.File r9 = new java.io.File
                r9.<init>(r8)
                boolean r9 = r9.exists()
                if (r9 == 0) goto L_0x0118
                com.portfolio.platform.data.model.LocalFile r9 = r12.$localFile
                r9.setLocalUri(r8)
                com.portfolio.platform.data.model.LocalFile r9 = r12.$localFile
                java.lang.String r9 = r9.getChecksum()
                boolean r9 = android.text.TextUtils.isEmpty(r9)
                if (r9 != 0) goto L_0x00c0
                com.portfolio.platform.cloudimage.ChecksumUtil r9 = com.portfolio.platform.cloudimage.ChecksumUtil.INSTANCE
                com.portfolio.platform.data.model.LocalFile r10 = r12.$localFile
                java.lang.String r10 = r10.getLocalUri()
                com.portfolio.platform.data.model.LocalFile r11 = r12.$localFile
                java.lang.String r11 = r11.getChecksum()
                boolean r9 = r9.verifyDownloadFile(r10, r11)
                if (r9 == 0) goto L_0x0118
            L_0x00c0:
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "File  "
                r0.append(r1)
                com.portfolio.platform.data.model.LocalFile r1 = r12.$localFile
                java.lang.String r1 = r1.getRemoteUrl()
                r0.append(r1)
                java.lang.String r1 = " exists, downloaded successful"
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r13.d(r5, r0)
                com.portfolio.platform.data.model.LocalFile r13 = r12.$localFile
                r13.setPinType(r4)
                com.fossil.qg5$b r13 = r12.$job
                com.fossil.qg5$c r13 = r13.a()
                if (r13 == 0) goto L_0x00f7
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                r13.onComplete(r6, r0)
            L_0x00f7:
                com.fossil.qg5 r13 = r12.this$0
                java.util.List r13 = r13.e
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getRemoteUrl()
                r13.remove(r0)
                com.fossil.qg5 r13 = r12.this$0
                int r0 = r13.a
                int r0 = r0 + r2
                r13.a = r0
                com.fossil.qg5 r13 = r12.this$0
                r13.a()
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            L_0x0118:
                com.fossil.qg5$d$a r9 = new com.fossil.qg5$d$a
                r9.<init>(r12, r3)
                r12.L$0 = r13
                r12.I$0 = r4
                r12.L$1 = r1
                r12.L$2 = r7
                r12.L$3 = r8
                r12.label = r6
                java.lang.Object r13 = com.fossil.aj5.a(r9, r12)
                if (r13 != r0) goto L_0x0130
                return r0
            L_0x0130:
                r1 = r7
                r0 = r8
                r7 = 0
            L_0x0133:
                com.fossil.zi5 r13 = (com.fossil.zi5) r13
                boolean r8 = r13 instanceof com.fossil.bj5
                if (r8 == 0) goto L_0x02ed
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "isSuccessful of "
                r9.append(r10)
                com.portfolio.platform.data.model.LocalFile r10 = r12.$localFile
                java.lang.String r10 = r10.getRemoteUrl()
                r9.append(r10)
                java.lang.String r10 = " onResponse: response = "
                r9.append(r10)
                r9.append(r13)
                java.lang.String r9 = r9.toString()
                r8.d(r5, r9)
                r8 = 100000(0x186a0, float:1.4013E-40)
                byte[] r8 = new byte[r8]
                com.fossil.bj5 r13 = (com.fossil.bj5) r13
                java.lang.Object r13 = r13.a()
                com.fossil.mo7 r13 = (com.fossil.mo7) r13
                if (r13 == 0) goto L_0x0175
                java.io.InputStream r13 = r13.byteStream()
                goto L_0x0176
            L_0x0175:
                r13 = r3
            L_0x0176:
                com.portfolio.platform.data.model.LocalFile r9 = r12.$localFile
                r9.setPinType(r4)
                boolean r9 = r1.exists()     // Catch:{ Exception -> 0x01bd }
                if (r9 != 0) goto L_0x0184
                r1.mkdirs()     // Catch:{ Exception -> 0x01bd }
            L_0x0184:
                java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x01bd }
                r1.<init>(r0)     // Catch:{ Exception -> 0x01bd }
                boolean r9 = r1.exists()     // Catch:{ Exception -> 0x01bd }
                if (r9 != 0) goto L_0x0192
                r1.createNewFile()     // Catch:{ Exception -> 0x01bd }
            L_0x0192:
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x01bd }
                r1.<init>(r0, r4)     // Catch:{ Exception -> 0x01bd }
                if (r13 == 0) goto L_0x01ab
            L_0x0199:
                int r3 = r13.read(r8)     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
                if (r3 != r2) goto L_0x01a0
                goto L_0x01ab
            L_0x01a0:
                r1.write(r8, r4, r3)     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
                goto L_0x0199
            L_0x01a4:
                r0 = move-exception
                r3 = r1
                goto L_0x02e6
            L_0x01a8:
                r0 = move-exception
                r3 = r1
                goto L_0x01be
            L_0x01ab:
                r1.flush()     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
                com.portfolio.platform.data.model.LocalFile r3 = r12.$localFile     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
                r3.setLocalUri(r0)     // Catch:{ Exception -> 0x01a8, all -> 0x01a4 }
                com.fossil.rs7.a(r13)
                com.fossil.rs7.a(r1)
                goto L_0x01eb
            L_0x01ba:
                r0 = move-exception
                goto L_0x02e6
            L_0x01bd:
                r0 = move-exception
            L_0x01be:
                com.portfolio.platform.data.model.LocalFile r1 = r12.$localFile     // Catch:{ all -> 0x01ba }
                r1.setPinType(r6)     // Catch:{ all -> 0x01ba }
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x01ba }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x01ba }
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x01ba }
                r8.<init>()     // Catch:{ all -> 0x01ba }
                java.lang.String r9 = "exception "
                r8.append(r9)     // Catch:{ all -> 0x01ba }
                r0.printStackTrace()     // Catch:{ all -> 0x01ba }
                com.fossil.i97 r9 = com.fossil.i97.a     // Catch:{ all -> 0x01ba }
                r8.append(r9)     // Catch:{ all -> 0x01ba }
                java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x01ba }
                r1.d(r5, r8)     // Catch:{ all -> 0x01ba }
                r0.printStackTrace()     // Catch:{ all -> 0x01ba }
                com.fossil.rs7.a(r13)
                com.fossil.rs7.a(r3)
            L_0x01eb:
                com.portfolio.platform.data.model.LocalFile r13 = r12.$localFile
                java.lang.String r13 = r13.getChecksum()
                boolean r13 = android.text.TextUtils.isEmpty(r13)
                if (r13 != 0) goto L_0x0268
                com.portfolio.platform.cloudimage.ChecksumUtil r13 = com.portfolio.platform.cloudimage.ChecksumUtil.INSTANCE
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getLocalUri()
                com.portfolio.platform.data.model.LocalFile r1 = r12.$localFile
                java.lang.String r1 = r1.getChecksum()
                boolean r13 = r13.verifyDownloadFile(r0, r1)
                java.lang.String r0 = "Callback of "
                if (r13 == 0) goto L_0x023e
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r1.append(r0)
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getRemoteUrl()
                r1.append(r0)
                java.lang.String r0 = ": true"
                r1.append(r0)
                java.lang.String r0 = r1.toString()
                r13.d(r5, r0)
                com.fossil.qg5$b r13 = r12.$job
                com.fossil.qg5$c r13 = r13.a()
                if (r13 == 0) goto L_0x0275
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                r13.onComplete(r6, r0)
                goto L_0x0275
            L_0x023e:
                com.portfolio.platform.data.model.LocalFile r13 = r12.$localFile
                r13.setPinType(r6)
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                r1.append(r0)
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getRemoteUrl()
                r1.append(r0)
                java.lang.String r0 = ": false"
                r1.append(r0)
                java.lang.String r0 = r1.toString()
                r13.e(r5, r0)
                r7 = 1
                goto L_0x0275
            L_0x0268:
                com.fossil.qg5$b r13 = r12.$job
                com.fossil.qg5$c r13 = r13.a()
                if (r13 == 0) goto L_0x0275
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                r13.onComplete(r6, r0)
            L_0x0275:
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                com.portfolio.platform.data.model.LocalFile r1 = r12.$localFile
                java.lang.String r1 = r1.getRemoteUrl()
                r0.append(r1)
                java.lang.String r1 = " downloaded, having "
                r0.append(r1)
                com.fossil.qg5 r1 = r12.this$0
                int r1 = r1.a
                r0.append(r1)
                java.lang.String r1 = " download running"
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r13.d(r5, r0)
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r1 = "size of mQueue: "
                r0.append(r1)
                com.fossil.qg5 r1 = r12.this$0
                java.util.concurrent.LinkedBlockingDeque r1 = r1.d
                int r1 = r1.size()
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r13.d(r5, r0)
                com.fossil.qg5 r13 = r12.this$0
                java.util.List r13 = r13.e
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getRemoteUrl()
                r13.remove(r0)
                com.fossil.qg5 r13 = r12.this$0
                int r0 = r13.a
                int r0 = r0 + r2
                r13.a = r0
                com.fossil.qg5 r13 = r12.this$0
                r13.a()
                goto L_0x0350
            L_0x02e6:
                com.fossil.rs7.a(r13)
                com.fossil.rs7.a(r3)
                throw r0
            L_0x02ed:
                boolean r0 = r13 instanceof com.fossil.yi5
                if (r0 == 0) goto L_0x0350
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r7 = "onFailure of "
                r1.append(r7)
                com.portfolio.platform.data.model.LocalFile r7 = r12.$localFile
                java.lang.String r7 = r7.getRemoteUrl()
                r1.append(r7)
                java.lang.String r7 = ": Failure code="
                r1.append(r7)
                com.fossil.yi5 r13 = (com.fossil.yi5) r13
                int r7 = r13.a()
                r1.append(r7)
                java.lang.String r7 = " message="
                r1.append(r7)
                com.portfolio.platform.data.model.ServerError r13 = r13.c()
                if (r13 == 0) goto L_0x0327
                java.lang.String r3 = r13.getMessage()
            L_0x0327:
                r1.append(r3)
                java.lang.String r13 = r1.toString()
                r0.e(r5, r13)
                com.fossil.qg5 r13 = r12.this$0
                java.util.List r13 = r13.e
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                java.lang.String r0 = r0.getRemoteUrl()
                r13.remove(r0)
                com.fossil.qg5 r13 = r12.this$0
                int r0 = r13.a
                int r0 = r0 + r2
                r13.a = r0
                com.fossil.qg5 r13 = r12.this$0
                r13.a()
                r7 = 1
            L_0x0350:
                if (r7 == 0) goto L_0x0379
                com.fossil.qg5$b r13 = r12.$job
                int r13 = r13.c()
                if (r13 >= r6) goto L_0x036c
                com.fossil.qg5$b r13 = r12.$job
                int r0 = r13.c()
                int r0 = r0 + r6
                r13.a(r0)
                com.fossil.qg5 r13 = r12.this$0
                com.fossil.qg5$b r0 = r12.$job
                r13.b(r0)
                goto L_0x0379
            L_0x036c:
                com.fossil.qg5$b r13 = r12.$job
                com.fossil.qg5$c r13 = r13.a()
                if (r13 == 0) goto L_0x0379
                com.portfolio.platform.data.model.LocalFile r0 = r12.$localFile
                r13.onComplete(r4, r0)
            L_0x0379:
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qg5.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.manager.FileDownloadManager$enqueue$1", f = "FileDownloadManager.kt", l = {205}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qg5 qg5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = qg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kn7 kn7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kn7 c = this.this$0.b;
                this.L$0 = yi7;
                this.L$1 = c;
                this.label = 1;
                if (c.a(null, this) == a) {
                    return a;
                }
                kn7 = c;
            } else if (i == 1) {
                kn7 = (kn7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            try {
                this.this$0.a();
                i97 i97 = i97.a;
                kn7.a(null);
                return i97.a;
            } catch (Throwable th) {
                kn7.a(null);
                throw th;
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public qg5() {
        ki7 a2 = dl7.a(null, 1, null);
        this.f = a2;
        this.g = zi7.a(a2.plus(h));
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final ApiServiceV2 b() {
        ApiServiceV2 apiServiceV2 = this.c;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        ee7.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final synchronized void a() {
        if (this.d.size() > 0 && this.a < 3) {
            b pop = this.d.pop();
            ee7.a((Object) pop, "jobToRun");
            a(pop);
        }
    }

    @DexIgnore
    public final synchronized void b(b bVar) {
        ee7.b(bVar, "job");
        if (!this.e.contains(bVar.b().getRemoteUrl())) {
            if (!this.d.contains(bVar)) {
                this.d.add(bVar);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FileDownloadManager", bVar.b().getRemoteUrl() + " added to mQueue at times: " + bVar.c() + ", having " + this.a + " download running");
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new e(this, null), 3, null);
                return;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("FileDownloadManager", bVar.b().getRemoteUrl() + " is already added");
    }

    @DexIgnore
    public final void a(b bVar) {
        LocalFile b2 = bVar.b();
        this.a++;
        this.e.add(b2.getRemoteUrl());
        ik7 unused = xh7.b(this.g, null, null, new d(this, b2, bVar, null), 3, null);
    }
}
