package com.fossil;

import androidx.lifecycle.Lifecycle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr extends Lifecycle {
    @DexIgnore
    public static /* final */ wr a; // = new wr();

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public Lifecycle.State a() {
        return Lifecycle.State.RESUMED;
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void a(rd rdVar) {
        ee7.b(rdVar, "observer");
    }

    @DexIgnore
    @Override // androidx.lifecycle.Lifecycle
    public void b(rd rdVar) {
        ee7.b(rdVar, "observer");
    }
}
