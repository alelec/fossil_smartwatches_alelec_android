package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm6 extends he {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b(null, null, null, null, 15, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;

        @DexIgnore
        public b() {
            this(null, null, null, null, 15, null);
        }

        @DexIgnore
        public b(Integer num, Integer num2, Integer num3, Integer num4) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.d;
        }

        @DexIgnore
        public final Integer c() {
            return this.a;
        }

        @DexIgnore
        public final Integer d() {
            return this.c;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Integer num, Integer num2, Integer num3, Integer num4, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4);
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            bVar.a(num, num2, num3, num4);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2, Integer num3, Integer num4) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeHeartRateChartViewModel$saveColor$1", f = "CustomizeHeartRateChartViewModel.kt", l = {55, 56, 76}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $colorMax;
        @DexIgnore
        public /* final */ /* synthetic */ String $colorMin;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gm6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gm6 gm6, String str, String str2, String str3, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gm6;
            this.$id = str;
            this.$colorMax = str2;
            this.$colorMin = str3;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$id, this.$colorMax, this.$colorMin, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0091  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00a9  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00e3  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x014a A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x0153  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r10.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x004f
                if (r1 == r4) goto L_0x003f
                if (r1 == r3) goto L_0x002f
                if (r1 != r2) goto L_0x0027
                java.lang.Object r0 = r10.L$3
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r10.L$2
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r10.L$1
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r10.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r11)
                goto L_0x014b
            L_0x0027:
                java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r11.<init>(r0)
                throw r11
            L_0x002f:
                java.lang.Object r1 = r10.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r3 = r10.L$1
                com.fossil.se7 r3 = (com.fossil.se7) r3
                java.lang.Object r4 = r10.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r11)
                goto L_0x008f
            L_0x003f:
                java.lang.Object r1 = r10.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r4 = r10.L$1
                com.fossil.se7 r4 = (com.fossil.se7) r4
                java.lang.Object r6 = r10.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r11)
                goto L_0x0073
            L_0x004f:
                com.fossil.t87.a(r11)
                com.fossil.yi7 r11 = r10.p$
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                com.fossil.gm6 r6 = r10.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r6.c
                java.lang.String r7 = r10.$id
                r10.L$0 = r11
                r10.L$1 = r1
                r10.L$2 = r1
                r10.label = r4
                java.lang.Object r4 = r6.getThemeById(r7, r10)
                if (r4 != r0) goto L_0x0070
                return r0
            L_0x0070:
                r6 = r11
                r11 = r4
                r4 = r1
            L_0x0073:
                com.portfolio.platform.data.model.Theme r11 = (com.portfolio.platform.data.model.Theme) r11
                if (r11 == 0) goto L_0x0078
                goto L_0x0095
            L_0x0078:
                com.fossil.gm6 r11 = r10.this$0
                com.portfolio.platform.data.source.ThemeRepository r11 = r11.c
                r10.L$0 = r6
                r10.L$1 = r4
                r10.L$2 = r1
                r10.label = r3
                java.lang.Object r11 = r11.getCurrentTheme(r10)
                if (r11 != r0) goto L_0x008d
                return r0
            L_0x008d:
                r3 = r4
                r4 = r6
            L_0x008f:
                if (r11 == 0) goto L_0x0153
                com.portfolio.platform.data.model.Theme r11 = (com.portfolio.platform.data.model.Theme) r11
                r6 = r4
                r4 = r3
            L_0x0095:
                r1.element = r11
                com.fossil.se7 r11 = new com.fossil.se7
                r11.<init>()
                r11.element = r5
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                r1.element = r5
                java.lang.String r3 = r10.$colorMax
                if (r3 == 0) goto L_0x00df
                T r3 = r4.element
                com.portfolio.platform.data.model.Theme r3 = (com.portfolio.platform.data.model.Theme) r3
                java.util.ArrayList r3 = r3.getStyles()
                java.util.Iterator r3 = r3.iterator()
            L_0x00b5:
                boolean r5 = r3.hasNext()
                if (r5 == 0) goto L_0x00df
                java.lang.Object r5 = r3.next()
                com.portfolio.platform.data.model.Style r5 = (com.portfolio.platform.data.model.Style) r5
                java.lang.String r7 = r5.getKey()
                java.lang.String r8 = "maxHeartRate"
                boolean r7 = com.fossil.ee7.a(r7, r8)
                if (r7 == 0) goto L_0x00b5
                java.lang.String r7 = r10.$colorMax
                r5.setValue(r7)
                java.lang.String r5 = r10.$colorMax
                int r5 = android.graphics.Color.parseColor(r5)
                java.lang.Integer r5 = com.fossil.pb7.a(r5)
                r11.element = r5
                goto L_0x00b5
            L_0x00df:
                java.lang.String r3 = r10.$colorMin
                if (r3 == 0) goto L_0x0119
                T r3 = r4.element
                com.portfolio.platform.data.model.Theme r3 = (com.portfolio.platform.data.model.Theme) r3
                java.util.ArrayList r3 = r3.getStyles()
                java.util.Iterator r3 = r3.iterator()
            L_0x00ef:
                boolean r5 = r3.hasNext()
                if (r5 == 0) goto L_0x0119
                java.lang.Object r5 = r3.next()
                com.portfolio.platform.data.model.Style r5 = (com.portfolio.platform.data.model.Style) r5
                java.lang.String r7 = r5.getKey()
                java.lang.String r8 = "lowestHeartRate"
                boolean r7 = com.fossil.ee7.a(r7, r8)
                if (r7 == 0) goto L_0x00ef
                java.lang.String r7 = r10.$colorMin
                r5.setValue(r7)
                java.lang.String r5 = r10.$colorMin
                int r5 = android.graphics.Color.parseColor(r5)
                java.lang.Integer r5 = com.fossil.pb7.a(r5)
                r1.element = r5
                goto L_0x00ef
            L_0x0119:
                com.fossil.gm6 r3 = r10.this$0
                com.fossil.gm6$b r3 = r3.b
                T r5 = r11.element
                r7 = r5
                java.lang.Integer r7 = (java.lang.Integer) r7
                java.lang.Integer r5 = (java.lang.Integer) r5
                T r8 = r1.element
                r9 = r8
                java.lang.Integer r9 = (java.lang.Integer) r9
                java.lang.Integer r8 = (java.lang.Integer) r8
                r3.a(r7, r5, r9, r8)
                com.fossil.gm6 r3 = r10.this$0
                com.portfolio.platform.data.source.ThemeRepository r3 = r3.c
                T r5 = r4.element
                com.portfolio.platform.data.model.Theme r5 = (com.portfolio.platform.data.model.Theme) r5
                r10.L$0 = r6
                r10.L$1 = r4
                r10.L$2 = r11
                r10.L$3 = r1
                r10.label = r2
                java.lang.Object r11 = r3.upsertUserTheme(r5, r10)
                if (r11 != r0) goto L_0x014b
                return r0
            L_0x014b:
                com.fossil.gm6 r11 = r10.this$0
                r11.a()
                com.fossil.i97 r11 = com.fossil.i97.a
                return r11
            L_0x0153:
                com.fossil.ee7.a()
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.gm6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = gm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeHeartRateChartV\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public gm6(ThemeRepository themeRepository) {
        ee7.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        int i2;
        String a2 = dm6.r.a();
        if (a2 != null) {
            i = Color.parseColor(a2);
        } else {
            i = Color.parseColor(e);
        }
        String b2 = dm6.r.b();
        if (b2 != null) {
            i2 = Color.parseColor(b2);
        } else {
            i2 = Color.parseColor(e);
        }
        this.b.a(Integer.valueOf(i), Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i2));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        if (i == 601) {
            b.a(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 12, null);
            a();
        } else if (i == 602) {
            b.a(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 3, null);
            a();
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = d;
        local.d(str4, "saveColor colorMax=" + str2 + " colorMin=" + str3);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, str, str2, str3, null), 3, null);
    }
}
