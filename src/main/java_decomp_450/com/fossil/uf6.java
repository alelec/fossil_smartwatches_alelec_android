package com.fossil;

import android.os.Bundle;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.te5;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uf6 extends pf6 implements te5.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<r87<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ kn7 j; // = mn7.a(false, 1, null);
    @DexIgnore
    public List<GoalTrackingSummary> k; // = new ArrayList();
    @DexIgnore
    public GoalTrackingSummary l;
    @DexIgnore
    public List<GoalTrackingData> m;
    @DexIgnore
    public LiveData<qx6<List<GoalTrackingSummary>>> n;
    @DexIgnore
    public Listing<GoalTrackingData> o;
    @DexIgnore
    public /* final */ qf6 p;
    @DexIgnore
    public /* final */ GoalTrackingRepository q;
    @DexIgnore
    public /* final */ pj4 r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {Action.Selfie.TAKE_BURST}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$addRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {203}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ GoalTrackingData $goalTrackingData;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, GoalTrackingData goalTrackingData, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$goalTrackingData = goalTrackingData;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$goalTrackingData, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    GoalTrackingRepository f = this.this$0.this$0.q;
                    List<GoalTrackingData> d = w97.d(this.$goalTrackingData);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (f.insertFromDevice(d, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uf6 uf6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uf6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$date, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                String uuid = UUID.randomUUID().toString();
                ee7.a((Object) uuid, "UUID.randomUUID().toString()");
                Date date = this.$date;
                TimeZone timeZone = TimeZone.getDefault();
                ee7.a((Object) timeZone, "TimeZone.getDefault()");
                DateTime a3 = zd5.a(date, timeZone.getRawOffset() / 1000);
                ee7.a((Object) a3, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                ee7.a((Object) timeZone2, "TimeZone.getDefault()");
                GoalTrackingData goalTrackingData = new GoalTrackingData(uuid, a3, timeZone2.getRawOffset() / 1000, this.$date, new Date().getTime(), new Date().getTime());
                ti7 b = this.this$0.c();
                a aVar = new a(this, goalTrackingData, null);
                this.L$0 = yi7;
                this.L$1 = goalTrackingData;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                GoalTrackingData goalTrackingData2 = (GoalTrackingData) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingData $raw;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$deleteRecord$1$1", f = "GoalTrackingDetailPresenter.kt", l = {211}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    GoalTrackingRepository f = this.this$0.this$0.q;
                    GoalTrackingData goalTrackingData = this.this$0.$raw;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (f.deleteGoalTracking(goalTrackingData, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uf6 uf6, GoalTrackingData goalTrackingData, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uf6;
            this.$raw = goalTrackingData;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$raw, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$observeGoalTrackingDataPaging$1", f = "GoalTrackingDetailPresenter.kt", l = {85}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qf<GoalTrackingData>> {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uf6$d$a$a")
            /* renamed from: com.fossil.uf6$d$a$a  reason: collision with other inner class name */
            public static final class C0194a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ qf $dataList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uf6$d$a$a$a")
                /* renamed from: com.fossil.uf6$d$a$a$a  reason: collision with other inner class name */
                public static final class C0195a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public Object L$1;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0194a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0195a(fb7 fb7, C0194a aVar) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0195a aVar = new C0195a(fb7, this.this$0);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0195a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        uf6 uf6;
                        Object a = nb7.a();
                        int i = this.label;
                        if (i == 0) {
                            t87.a(obj);
                            yi7 yi7 = this.p$;
                            uf6 uf62 = this.this$0.this$0.a.this$0;
                            GoalTrackingRepository f = uf62.q;
                            Date date = this.this$0.this$0.a.$date;
                            this.L$0 = yi7;
                            this.L$1 = uf62;
                            this.label = 1;
                            obj = f.getGoalTrackingDataInDate(date, this);
                            if (obj == a) {
                                return a;
                            }
                            uf6 = uf62;
                        } else if (i == 1) {
                            uf6 = (uf6) this.L$1;
                            yi7 yi72 = (yi7) this.L$0;
                            t87.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        uf6.m = (List) obj;
                        return i97.a;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0194a(qf qfVar, fb7 fb7, a aVar) {
                    super(2, fb7);
                    this.$dataList = qfVar;
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0194a aVar = new C0194a(this.$dataList, fb7, this.this$0);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0194a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    kn7 kn7;
                    yi7 yi7;
                    kn7 kn72;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi72 = this.p$;
                        kn72 = this.this$0.a.this$0.j;
                        this.L$0 = yi72;
                        this.L$1 = kn72;
                        this.label = 1;
                        if (kn72.a(null, this) == a) {
                            return a;
                        }
                        yi7 = yi72;
                    } else if (i == 1) {
                        kn72 = (kn7) this.L$1;
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        kn7 = (kn7) this.L$1;
                        yi7 yi73 = (yi7) this.L$0;
                        try {
                            t87.a(obj);
                            this.this$0.a.this$0.p.c(this.$dataList);
                            if (this.this$0.a.this$0.h && this.this$0.a.this$0.i) {
                                ik7 unused = this.this$0.a.this$0.m();
                            }
                            i97 i97 = i97.a;
                            kn7.a(null);
                            return i97.a;
                        } catch (Throwable th) {
                            th = th;
                        }
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    try {
                        ti7 b = qj7.b();
                        C0195a aVar = new C0195a(null, this);
                        this.L$0 = yi7;
                        this.L$1 = kn72;
                        this.label = 2;
                        if (vh7.a(b, aVar, this) == a) {
                            return a;
                        }
                        kn7 = kn72;
                        this.this$0.a.this$0.p.c(this.$dataList);
                        ik7 unused2 = this.this$0.a.this$0.m();
                        i97 i972 = i97.a;
                        kn7.a(null);
                        return i97.a;
                    } catch (Throwable th2) {
                        th = th2;
                        kn7 = kn72;
                        kn7.a(null);
                        throw th;
                    }
                }
            }

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<GoalTrackingData> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getGoalTrackingDataPaging observer size=");
                sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
                local.d("GoalTrackingDetailPresenter", sb.toString());
                if (qfVar != null) {
                    this.a.this$0.i = true;
                    ik7 unused = xh7.b(this.a.this$0.e(), null, null, new C0194a(qfVar, null, this), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(uf6 uf6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uf6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$date, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            uf6 uf6;
            LiveData pagedList;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                uf6 uf62 = this.this$0;
                GoalTrackingRepository f = uf62.q;
                Date date = this.$date;
                pj4 c = this.this$0.r;
                uf6 uf63 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = uf62;
                this.label = 1;
                obj = f.getGoalTrackingDataPaging(date, c, uf63, this);
                if (obj == a2) {
                    return a2;
                }
                uf6 = uf62;
            } else if (i == 1) {
                uf6 = (uf6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            uf6.o = (Listing) obj;
            Listing e = this.this$0.o;
            if (!(e == null || (pagedList = e.getPagedList()) == null)) {
                qf6 o = this.this$0.p;
                if (o != null) {
                    pagedList.a((rf6) o, new a(this));
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1", f = "GoalTrackingDetailPresenter.kt", l = {142, 260, 167}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$setDate$1$1", f = "GoalTrackingDetailPresenter.kt", l = {142}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Date> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.g(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends zb7 implements kd7<yi7, fb7<? super GoalTrackingSummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(fb7 fb7, e eVar) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(fb7, this.this$0);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super GoalTrackingSummary> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    uf6 uf6 = this.this$0.this$0;
                    return uf6.a(uf6.f, this.this$0.this$0.k);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(uf6 uf6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uf6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$date, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:32:0x019e A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01af  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r1 = r16
                java.lang.Object r0 = com.fossil.nb7.a()
                int r2 = r1.label
                r3 = 3
                r4 = 2
                r5 = 1
                r6 = 0
                if (r2 == 0) goto L_0x0064
                if (r2 == r5) goto L_0x0056
                if (r2 == r4) goto L_0x003b
                if (r2 != r3) goto L_0x0033
                java.lang.Object r0 = r1.L$4
                r2 = r0
                com.fossil.kn7 r2 = (com.fossil.kn7) r2
                java.lang.Object r0 = r1.L$3
                com.fossil.r87 r0 = (com.fossil.r87) r0
                java.lang.Object r0 = r1.L$2
                android.util.Pair r0 = (android.util.Pair) r0
                java.lang.Object r0 = r1.L$1
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                java.lang.Object r0 = r1.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r17)     // Catch:{ all -> 0x0030 }
                r3 = r17
                goto L_0x019f
            L_0x0030:
                r0 = move-exception
                goto L_0x01e9
            L_0x0033:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r2)
                throw r0
            L_0x003b:
                java.lang.Object r2 = r1.L$4
                com.fossil.kn7 r2 = (com.fossil.kn7) r2
                java.lang.Object r4 = r1.L$3
                com.fossil.r87 r4 = (com.fossil.r87) r4
                java.lang.Object r7 = r1.L$2
                android.util.Pair r7 = (android.util.Pair) r7
                java.lang.Object r8 = r1.L$1
                java.lang.Boolean r8 = (java.lang.Boolean) r8
                boolean r9 = r1.Z$0
                java.lang.Object r10 = r1.L$0
                com.fossil.yi7 r10 = (com.fossil.yi7) r10
                com.fossil.t87.a(r17)
                goto L_0x017f
            L_0x0056:
                java.lang.Object r2 = r1.L$1
                com.fossil.uf6 r2 = (com.fossil.uf6) r2
                java.lang.Object r7 = r1.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r17)
                r8 = r17
                goto L_0x0089
            L_0x0064:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r7 = r1.p$
                com.fossil.uf6 r2 = r1.this$0
                java.util.Date r2 = r2.e
                if (r2 != 0) goto L_0x008e
                com.fossil.uf6 r2 = r1.this$0
                com.fossil.ti7 r8 = r2.b()
                com.fossil.uf6$e$a r9 = new com.fossil.uf6$e$a
                r9.<init>(r6)
                r1.L$0 = r7
                r1.L$1 = r2
                r1.label = r5
                java.lang.Object r8 = com.fossil.vh7.a(r8, r9, r1)
                if (r8 != r0) goto L_0x0089
                return r0
            L_0x0089:
                java.util.Date r8 = (java.util.Date) r8
                r2.e = r8
            L_0x008e:
                r10 = r7
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "setDate - date="
                r7.append(r8)
                java.util.Date r8 = r1.$date
                r7.append(r8)
                java.lang.String r8 = ", createdAt="
                r7.append(r8)
                com.fossil.uf6 r8 = r1.this$0
                java.util.Date r8 = r8.e
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                java.lang.String r8 = "GoalTrackingDetailPresenter"
                r2.d(r8, r7)
                com.fossil.uf6 r2 = r1.this$0
                java.util.Date r7 = r1.$date
                r2.f = r7
                com.fossil.uf6 r2 = r1.this$0
                java.util.Date r2 = r2.e
                java.util.Date r7 = r1.$date
                boolean r9 = com.fossil.zd5.c(r2, r7)
                java.util.Date r2 = new java.util.Date
                r2.<init>()
                java.util.Date r7 = r1.$date
                boolean r2 = com.fossil.zd5.c(r2, r7)
                r2 = r2 ^ r5
                java.util.Date r7 = r1.$date
                java.lang.Boolean r7 = com.fossil.zd5.w(r7)
                com.fossil.uf6 r11 = r1.this$0
                com.fossil.qf6 r11 = r11.p
                java.util.Date r12 = r1.$date
                java.lang.String r13 = "isToday"
                com.fossil.ee7.a(r7, r13)
                boolean r13 = r7.booleanValue()
                r11.a(r12, r9, r13, r2)
                java.util.Date r2 = r1.$date
                com.fossil.uf6 r11 = r1.this$0
                java.util.Date r11 = r11.e
                android.util.Pair r2 = com.fossil.zd5.a(r2, r11)
                java.lang.String r11 = "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)"
                com.fossil.ee7.a(r2, r11)
                com.fossil.uf6 r11 = r1.this$0
                androidx.lifecycle.MutableLiveData r11 = r11.g
                java.lang.Object r11 = r11.a()
                com.fossil.r87 r11 = (com.fossil.r87) r11
                com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "setDate - rangeDateValue="
                r13.append(r14)
                r13.append(r11)
                java.lang.String r14 = ", newRange="
                r13.append(r14)
                com.fossil.r87 r14 = new com.fossil.r87
                java.lang.Object r15 = r2.first
                java.lang.Object r5 = r2.second
                r14.<init>(r15, r5)
                r13.append(r14)
                java.lang.String r5 = r13.toString()
                r12.d(r8, r5)
                if (r11 == 0) goto L_0x01ed
                java.lang.Object r5 = r11.getFirst()
                java.util.Date r5 = (java.util.Date) r5
                java.lang.Object r8 = r2.first
                java.util.Date r8 = (java.util.Date) r8
                boolean r5 = com.fossil.zd5.d(r5, r8)
                if (r5 == 0) goto L_0x01ed
                java.lang.Object r5 = r11.getSecond()
                java.util.Date r5 = (java.util.Date) r5
                java.lang.Object r8 = r2.second
                java.util.Date r8 = (java.util.Date) r8
                boolean r5 = com.fossil.zd5.d(r5, r8)
                if (r5 != 0) goto L_0x0160
                goto L_0x01ed
            L_0x0160:
                com.fossil.uf6 r5 = r1.this$0
                com.fossil.kn7 r5 = r5.j
                r1.L$0 = r10
                r1.Z$0 = r9
                r1.L$1 = r7
                r1.L$2 = r2
                r1.L$3 = r11
                r1.L$4 = r5
                r1.label = r4
                java.lang.Object r4 = r5.a(r6, r1)
                if (r4 != r0) goto L_0x017b
                return r0
            L_0x017b:
                r8 = r7
                r4 = r11
                r7 = r2
                r2 = r5
            L_0x017f:
                com.fossil.uf6 r5 = r1.this$0
                com.fossil.ti7 r5 = r5.b()
                com.fossil.uf6$e$b r11 = new com.fossil.uf6$e$b
                r11.<init>(r6, r1)
                r1.L$0 = r10
                r1.Z$0 = r9
                r1.L$1 = r8
                r1.L$2 = r7
                r1.L$3 = r4
                r1.L$4 = r2
                r1.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r5, r11, r1)
                if (r3 != r0) goto L_0x019f
                return r0
            L_0x019f:
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r3 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) r3
                com.fossil.uf6 r0 = r1.this$0
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r0 = r0.l
                boolean r0 = com.fossil.ee7.a(r0, r3)
                r4 = 1
                r0 = r0 ^ r4
                if (r0 == 0) goto L_0x01b4
                com.fossil.uf6 r0 = r1.this$0
                r0.l = r3
            L_0x01b4:
                com.fossil.uf6 r0 = r1.this$0
                com.fossil.qf6 r0 = r0.p
                com.fossil.uf6 r3 = r1.this$0
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r3 = r3.l
                r0.a(r3)
                com.fossil.uf6 r0 = r1.this$0
                com.fossil.uf6 r3 = r1.this$0
                java.util.Date r3 = r3.f
                r0.d(r3)
                com.fossil.uf6 r0 = r1.this$0
                boolean r0 = r0.h
                if (r0 == 0) goto L_0x01e3
                com.fossil.uf6 r0 = r1.this$0
                boolean r0 = r0.i
                if (r0 == 0) goto L_0x01e3
                com.fossil.uf6 r0 = r1.this$0
                com.fossil.ik7 unused = r0.m()
            L_0x01e3:
                com.fossil.i97 r0 = com.fossil.i97.a
                r2.a(r6)
                goto L_0x0213
            L_0x01e9:
                r2.a(r6)
                throw r0
            L_0x01ed:
                com.fossil.uf6 r0 = r1.this$0
                r3 = 0
                r0.h = r3
                com.fossil.uf6 r0 = r1.this$0
                r0.i = r3
                com.fossil.uf6 r0 = r1.this$0
                java.util.Date r3 = r0.f
                r0.d(r3)
                com.fossil.uf6 r0 = r1.this$0
                androidx.lifecycle.MutableLiveData r0 = r0.g
                com.fossil.r87 r3 = new com.fossil.r87
                java.lang.Object r4 = r2.first
                java.lang.Object r2 = r2.second
                r3.<init>(r4, r2)
                r0.a(r3)
            L_0x0213:
                com.fossil.i97 r0 = com.fossil.i97.a
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uf6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1", f = "GoalTrackingDetailPresenter.kt", l = {260, 227, 230}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$showDetailChart$1$1$maxValue$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> c;
                ArrayList<BarChart.b> arrayList;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Iterator it = this.$data.iterator();
                    int i = 0;
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        obj2 = it.next();
                        if (it.hasNext()) {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                            ee7.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 += pb7.a(it2.next().e()).intValue();
                            }
                            Integer a = pb7.a(i2);
                            do {
                                Object next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                                ee7.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 += pb7.a(it3.next().e()).intValue();
                                }
                                Integer a2 = pb7.a(i3);
                                if (a.compareTo((Object) a2) < 0) {
                                    obj2 = next;
                                    a = a2;
                                }
                            } while (it.hasNext());
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += pb7.a(it4.next().e()).intValue();
                    }
                    return pb7.a(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends zb7 implements kd7<yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(fb7 fb7, f fVar) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(fb7, this.this$0);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return tg6.a.a(this.this$0.this$0.f, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(uf6 uf6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uf6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00cf A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00d0  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x00fc  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0101  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r11.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x004f
                if (r1 == r4) goto L_0x0042
                if (r1 == r3) goto L_0x0032
                if (r1 != r2) goto L_0x002a
                java.lang.Object r0 = r11.L$3
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r11.L$2
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r2 = r11.L$1
                com.fossil.kn7 r2 = (com.fossil.kn7) r2
                java.lang.Object r3 = r11.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r12)     // Catch:{ all -> 0x0027 }
                goto L_0x00d5
            L_0x0027:
                r12 = move-exception
                goto L_0x011c
            L_0x002a:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L_0x0032:
                java.lang.Object r1 = r11.L$1
                com.fossil.kn7 r1 = (com.fossil.kn7) r1
                java.lang.Object r3 = r11.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r12)     // Catch:{ all -> 0x003e }
                goto L_0x0082
            L_0x003e:
                r12 = move-exception
                r2 = r1
                goto L_0x011c
            L_0x0042:
                java.lang.Object r1 = r11.L$1
                com.fossil.kn7 r1 = (com.fossil.kn7) r1
                java.lang.Object r4 = r11.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r12)
                r12 = r4
                goto L_0x0067
            L_0x004f:
                com.fossil.t87.a(r12)
                com.fossil.yi7 r12 = r11.p$
                com.fossil.uf6 r1 = r11.this$0
                com.fossil.kn7 r1 = r1.j
                r11.L$0 = r12
                r11.L$1 = r1
                r11.label = r4
                java.lang.Object r4 = r1.a(r5, r11)
                if (r4 != r0) goto L_0x0067
                return r0
            L_0x0067:
                com.fossil.uf6 r4 = r11.this$0
                com.fossil.ti7 r4 = r4.b()
                com.fossil.uf6$f$b r6 = new com.fossil.uf6$f$b
                r6.<init>(r5, r11)
                r11.L$0 = r12
                r11.L$1 = r1
                r11.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r4, r6, r11)
                if (r3 != r0) goto L_0x007f
                return r0
            L_0x007f:
                r10 = r3
                r3 = r12
                r12 = r10
            L_0x0082:
                com.fossil.r87 r12 = (com.fossil.r87) r12
                java.lang.Object r4 = r12.getFirst()
                java.util.ArrayList r4 = (java.util.ArrayList) r4
                com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
                java.lang.String r7 = "GoalTrackingDetailPresenter"
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "showDetailChart - date="
                r8.append(r9)
                com.fossil.uf6 r9 = r11.this$0
                java.util.Date r9 = r9.f
                r8.append(r9)
                java.lang.String r9 = ", data="
                r8.append(r9)
                r8.append(r4)
                java.lang.String r8 = r8.toString()
                r6.d(r7, r8)
                com.fossil.uf6 r6 = r11.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.uf6$f$a r7 = new com.fossil.uf6$f$a
                r7.<init>(r4, r5)
                r11.L$0 = r3
                r11.L$1 = r1
                r11.L$2 = r12
                r11.L$3 = r4
                r11.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r6, r7, r11)
                if (r2 != r0) goto L_0x00d0
                return r0
            L_0x00d0:
                r0 = r4
                r10 = r1
                r1 = r12
                r12 = r2
                r2 = r10
            L_0x00d5:
                java.lang.Integer r12 = (java.lang.Integer) r12
                com.fossil.uf6 r3 = r11.this$0
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r3 = r3.l
                if (r3 == 0) goto L_0x00f0
                int r3 = r3.getGoalTarget()
                int r3 = r3 / 16
                java.lang.Integer r3 = com.fossil.pb7.a(r3)
                if (r3 == 0) goto L_0x00f0
                int r3 = r3.intValue()
                goto L_0x00f2
            L_0x00f0:
                r3 = 8
            L_0x00f2:
                com.fossil.uf6 r4 = r11.this$0
                com.fossil.qf6 r4 = r4.p
                com.portfolio.platform.ui.view.chart.base.BarChart$c r6 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                if (r12 == 0) goto L_0x0101
                int r12 = r12.intValue()
                goto L_0x0102
            L_0x0101:
                r12 = 0
            L_0x0102:
                int r7 = r3 / 16
                int r12 = java.lang.Math.max(r12, r7)
                r6.<init>(r12, r3, r0)
                java.lang.Object r12 = r1.getSecond()
                java.util.ArrayList r12 = (java.util.ArrayList) r12
                r4.a(r6, r12)
                com.fossil.i97 r12 = com.fossil.i97.a
                r2.a(r5)
                com.fossil.i97 r12 = com.fossil.i97.a
                return r12
            L_0x011c:
                r2.a(r5)
                throw r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uf6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<qx6<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1", f = "GoalTrackingDetailPresenter.kt", l = {118}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.uf6$g$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$subscribeGoalTrackingSummaryData$1$1$summary$1", f = "GoalTrackingDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.uf6$g$a$a  reason: collision with other inner class name */
            public static final class C0196a extends zb7 implements kd7<yi7, fb7<? super GoalTrackingSummary>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0196a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0196a aVar = new C0196a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super GoalTrackingSummary> fb7) {
                    return ((C0196a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        uf6 uf6 = this.this$0.this$0.a;
                        return uf6.a(uf6.f, this.this$0.this$0.a.k);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = this.this$0.a.b();
                    C0196a aVar = new C0196a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) obj;
                if (this.this$0.a.l == null || (!ee7.a(this.this$0.a.l, goalTrackingSummary))) {
                    this.this$0.a.l = goalTrackingSummary;
                    this.this$0.a.p.a(this.this$0.a.l);
                    if (this.this$0.a.h && this.this$0.a.i) {
                        ik7 unused = this.this$0.a.m();
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore
        public g(uf6 uf6) {
            this.a = uf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<GoalTrackingSummary>> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingDetailPresenter", "start - mGoalTrackingSummary -- goalTrackingSummary=" + qx6);
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                this.a.k = qx6 != null ? (List) qx6.c() : null;
                this.a.h = true;
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ uf6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailPresenter$summaryTransformations$1$1", f = "GoalTrackingDetailPresenter.kt", l = {60, 60}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<GoalTrackingSummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<GoalTrackingSummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    GoalTrackingRepository f = this.this$0.a.q;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = f.getSummaries(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(uf6 uf6) {
            this.a = uf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<GoalTrackingSummary>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public uf6(qf6 qf6, GoalTrackingRepository goalTrackingRepository, pj4 pj4) {
        ee7.b(qf6, "mView");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(pj4, "mAppExecutors");
        this.p = qf6;
        this.q = goalTrackingRepository;
        this.r = pj4;
        LiveData<qx6<List<GoalTrackingSummary>>> b2 = ge.b(this.g, new h(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026d, true))\n        }\n    }");
        this.n = b2;
    }

    @DexIgnore
    public final void d(Date date) {
        h();
        ik7 unused = xh7.b(e(), null, null, new d(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        n();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "stop");
        LiveData<qx6<List<GoalTrackingSummary>>> liveData = this.n;
        qf6 qf6 = this.p;
        if (qf6 != null) {
            liveData.a((rf6) qf6);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void h() {
        LiveData<qf<GoalTrackingData>> pagedList;
        try {
            Listing<GoalTrackingData> listing = this.o;
            if (listing != null && (pagedList = listing.getPagedList()) != null) {
                qf6 qf6 = this.p;
                if (qf6 != null) {
                    pagedList.a((rf6) qf6);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("GoalTrackingDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void i() {
        vc7<i97> retry;
        FLogger.INSTANCE.getLocal().d("GoalTrackingDetailPresenter", "retry all failed request");
        Listing<GoalTrackingData> listing = this.o;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void j() {
        Date o2 = zd5.o(this.f);
        ee7.a((Object) o2, "DateHelper.getNextDate(mDate)");
        c(o2);
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void k() {
        Date p2 = zd5.p(this.f);
        ee7.a((Object) p2, "DateHelper.getPrevDate(mDate)");
        c(p2);
    }

    @DexIgnore
    public void l() {
        this.p.a(this);
    }

    @DexIgnore
    public final ik7 m() {
        return xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final void n() {
        LiveData<qx6<List<GoalTrackingSummary>>> liveData = this.n;
        qf6 qf6 = this.p;
        if (qf6 != null) {
            liveData.a((rf6) qf6, new g(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void c(Date date) {
        ee7.b(date, "date");
        ik7 unused = xh7.b(e(), null, null, new e(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void b(Date date) {
        ee7.b(date, "date");
        d(date);
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void a(Bundle bundle) {
        ee7.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void a(Date date) {
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "addRecord date=" + date);
        ik7 unused = xh7.b(e(), null, null, new b(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pf6
    public void a(GoalTrackingData goalTrackingData) {
        ee7.b(goalTrackingData, OrmLiteConfigUtil.RAW_DIR_NAME);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingDetailPresenter", "deleteRecord GoalTrackingData=" + goalTrackingData);
        ik7 unused = xh7.b(e(), null, null, new c(this, goalTrackingData, null), 3, null);
    }

    @DexIgnore
    public final GoalTrackingSummary a(Date date, List<GoalTrackingSummary> list) {
        T t = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (zd5.d(next.getDate(), date)) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    @Override // com.fossil.te5.a
    public void a(te5.g gVar) {
        ee7.b(gVar, "report");
        if (gVar.a()) {
            this.p.d();
        }
    }
}
