package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl1 extends v81 {
    @DexIgnore
    public gd7<? super xp0, i97> A;
    @DexIgnore
    public kd7<? super byte[], ? super qk1, i97> B;
    @DexIgnore
    public long C;

    @DexIgnore
    public pl1(ri1 ri1) {
        super(qa1.k, ri1, 0, 4);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(pm1 pm1) {
        if (pm1.a == dd1.DISCONNECTED) {
            so1.b.a(((v81) this).y.u);
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void b(rk1 rk1) {
        gd7<? super xp0, i97> gd7;
        Object obj;
        byte[] bArr = rk1.b;
        qk1 qk1 = rk1.a;
        kd7<? super byte[], ? super qk1, i97> kd7 = this.B;
        if (kd7 != null) {
            kd7.invoke(bArr, qk1);
        }
        if (qk1 == qk1.ASYNC) {
            xp0 a = so1.b.a(((v81) this).y.u, bArr);
            if (a == null || a.c) {
                wl0 wl0 = wl0.h;
                ci1 ci1 = ci1.e;
                String str = ((v81) this).y.u;
                String str2 = ((v81) this).c;
                String str3 = ((v81) this).d;
                JSONObject a2 = yz0.a(new JSONObject(), r51.Q0, yz0.a(bArr, (String) null, 1));
                r51 r51 = r51.j1;
                if (a == null || (obj = a.a()) == null) {
                    obj = JSONObject.NULL;
                }
                wl0.a(new wr1("streaming", ci1, str, str2, str3, true, null, null, null, yz0.a(a2, r51, obj), 448));
            }
            if (a != null && (gd7 = this.A) != null) {
                gd7.invoke(a);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.v81
    public eo0 d() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void f() {
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.C = j;
    }
}
