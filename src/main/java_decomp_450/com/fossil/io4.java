package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io4 {
    @DexIgnore
    @te4("id")
    public String a;
    @DexIgnore
    @te4("socialId")
    public String b;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_FIRST_NAME)
    public String c;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_LAST_NAME)
    public String d;
    @DexIgnore
    @te4(Constants.PROFILE_KEY_PROFILE_PIC)
    public String e;

    @DexIgnore
    public final String a() {
        return this.e;
    }

    @DexIgnore
    public final String b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof io4)) {
            return false;
        }
        io4 io4 = (io4) obj;
        return ee7.a(this.a, io4.a) && ee7.a(this.b, io4.b) && ee7.a(this.c, io4.c) && ee7.a(this.d, io4.d) && ee7.a(this.e, io4.e);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.d;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.e;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return hashCode4 + i;
    }

    @DexIgnore
    public String toString() {
        return "ProfileData(id=" + this.a + ", socialId=" + this.b + ", firstName=" + this.c + ", lastName=" + this.d + ", avatar=" + this.e + ")";
    }
}
