package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class k14 implements ob4 {
    @DexIgnore
    public /* final */ l14 a;
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public k14(l14 l14, Context context) {
        this.a = l14;
        this.b = context;
    }

    @DexIgnore
    public static ob4 a(l14 l14, Context context) {
        return new k14(l14, context);
    }

    @DexIgnore
    @Override // com.fossil.ob4
    public Object get() {
        return l14.a(this.a, this.b);
    }
}
