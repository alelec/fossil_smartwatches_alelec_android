package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class cr7 implements qr7 {
    @DexIgnore
    public /* final */ qr7 a;

    @DexIgnore
    public cr7(qr7 qr7) {
        ee7.b(qr7, "delegate");
        this.a = qr7;
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public void a(yq7 yq7, long j) throws IOException {
        ee7.b(yq7, "source");
        this.a.a(yq7, j);
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    @Override // com.fossil.qr7
    public tr7 d() {
        return this.a.d();
    }

    @DexIgnore
    @Override // com.fossil.qr7, java.io.Flushable
    public void flush() throws IOException {
        this.a.flush();
    }

    @DexIgnore
    public String toString() {
        return getClass().getSimpleName() + '(' + this.a + ')';
    }
}
