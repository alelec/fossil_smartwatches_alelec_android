package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class po4 {
    @DexIgnore
    public /* final */ nn4 a;

    @DexIgnore
    public po4(nn4 nn4) {
        ee7.b(nn4, "dao");
        this.a = nn4;
    }

    @DexIgnore
    public final long a(mn4 mn4) {
        ee7.b(mn4, "challenge");
        return this.a.a(mn4);
    }

    @DexIgnore
    public final LiveData<mn4> b(String str) {
        ee7.b(str, "id");
        return this.a.g(str);
    }

    @DexIgnore
    public final Long[] c(List<yn4> list) {
        ee7.b(list, "histories");
        return this.a.b(list);
    }

    @DexIgnore
    public final int d(String str) {
        ee7.b(str, "id");
        return this.a.a(str);
    }

    @DexIgnore
    public final hn4 e(String str) {
        ee7.b(str, "id");
        return this.a.b(str);
    }

    @DexIgnore
    public final LiveData<List<yn4>> f() {
        return this.a.c();
    }

    @DexIgnore
    public final yn4 g(String str) {
        ee7.b(str, "id");
        return this.a.c(str);
    }

    @DexIgnore
    public final Long[] a(List<mn4> list) {
        ee7.b(list, "challenges");
        return this.a.a(list);
    }

    @DexIgnore
    public final void b() {
        this.a.h();
    }

    @DexIgnore
    public final int c(String str) {
        ee7.b(str, "id");
        return this.a.f(str);
    }

    @DexIgnore
    public final List<in4> d() {
        return this.a.d();
    }

    @DexIgnore
    public final List<jn4> e() {
        return this.a.a();
    }

    @DexIgnore
    public final LiveData<yn4> f(String str) {
        ee7.b(str, "challengeId");
        return this.a.d(str);
    }

    @DexIgnore
    public final mn4 a(String str) {
        ee7.b(str, "id");
        return this.a.e(str);
    }

    @DexIgnore
    public final LiveData<mn4> b(String[] strArr, Date date) {
        ee7.b(strArr, "status");
        ee7.b(date, "date");
        return this.a.a(strArr, date);
    }

    @DexIgnore
    public final void c() {
        this.a.f();
    }

    @DexIgnore
    public final Long[] d(List<jn4> list) {
        ee7.b(list, "players");
        return this.a.c(list);
    }

    @DexIgnore
    public final long a(in4 in4) {
        ee7.b(in4, "fitnessData");
        return this.a.a(in4);
    }

    @DexIgnore
    public final Long[] b(List<hn4> list) {
        ee7.b(list, "displayPlayer");
        return this.a.d(list);
    }

    @DexIgnore
    public final mn4 a(String[] strArr, Date date) {
        ee7.b(strArr, "status");
        ee7.b(date, "date");
        return this.a.b(strArr, date);
    }

    @DexIgnore
    public final void a(String[] strArr) {
        ee7.b(strArr, "status");
        this.a.a(strArr);
    }

    @DexIgnore
    public final int a(yn4 yn4) {
        ee7.b(yn4, "historyChallenge");
        return this.a.a(yn4);
    }

    @DexIgnore
    public final void a() {
        this.a.b();
        this.a.h();
        this.a.i();
        this.a.g();
        this.a.e();
    }
}
