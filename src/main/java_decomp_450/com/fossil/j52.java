package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j52 implements a42 {
    @DexIgnore
    public /* final */ /* synthetic */ h52 a;

    @DexIgnore
    public j52(h52 h52) {
        this.a = h52;
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(Bundle bundle) {
        this.a.r.lock();
        try {
            this.a.a(bundle);
            i02 unused = this.a.j = i02.e;
            this.a.h();
        } finally {
            this.a.r.unlock();
        }
    }

    @DexIgnore
    public /* synthetic */ j52(h52 h52, g52 g52) {
        this(h52);
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(i02 i02) {
        this.a.r.lock();
        try {
            i02 unused = this.a.j = i02;
            this.a.h();
        } finally {
            this.a.r.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.a42
    public final void a(int i, boolean z) {
        this.a.r.lock();
        try {
            if (!this.a.q && this.a.p != null) {
                if (this.a.p.x()) {
                    boolean unused = this.a.q = true;
                    this.a.e.a(i);
                    this.a.r.unlock();
                    return;
                }
            }
            boolean unused2 = this.a.q = false;
            this.a.a(i, z);
        } finally {
            this.a.r.unlock();
        }
    }
}
