package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p62 {
    @DexIgnore
    public static int a; // = 129;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static p62 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ Uri f; // = new Uri.Builder().scheme("content").authority("com.google.android.gms.chimera").build();
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ComponentName c; // = null;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ boolean e;

        @DexIgnore
        public a(String str, String str2, int i, boolean z) {
            a72.b(str);
            this.a = str;
            a72.b(str2);
            this.b = str2;
            this.d = i;
            this.e = z;
        }

        @DexIgnore
        public final ComponentName a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final int c() {
            return this.d;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return y62.a(this.a, aVar.a) && y62.a(this.b, aVar.b) && y62.a(this.c, aVar.c) && this.d == aVar.d && this.e == aVar.e;
        }

        @DexIgnore
        public final int hashCode() {
            return y62.a(this.a, this.b, this.c, Integer.valueOf(this.d), Boolean.valueOf(this.e));
        }

        @DexIgnore
        public final String toString() {
            String str = this.a;
            if (str != null) {
                return str;
            }
            a72.a(this.c);
            return this.c.flattenToString();
        }

        @DexIgnore
        public final Intent a(Context context) {
            if (this.a == null) {
                return new Intent().setComponent(this.c);
            }
            Intent b2 = this.e ? b(context) : null;
            if (b2 == null) {
                return new Intent(this.a).setPackage(this.b);
            }
            return b2;
        }

        @DexIgnore
        public final Intent b(Context context) {
            Bundle bundle;
            Bundle bundle2 = new Bundle();
            bundle2.putString("serviceActionBundleKey", this.a);
            Intent intent = null;
            try {
                bundle = context.getContentResolver().call(f, "serviceIntentCall", (String) null, bundle2);
            } catch (IllegalArgumentException e2) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                sb.append("Dynamic intent resolution failed: ");
                sb.append(valueOf);
                Log.w("ConnectionStatusConfig", sb.toString());
                bundle = null;
            }
            if (bundle != null) {
                intent = (Intent) bundle.getParcelable("serviceResponseIntentKey");
            }
            if (intent == null) {
                String valueOf2 = String.valueOf(this.a);
                Log.w("ConnectionStatusConfig", valueOf2.length() != 0 ? "Dynamic lookup for intent failed for action: ".concat(valueOf2) : new String("Dynamic lookup for intent failed for action: "));
            }
            return intent;
        }
    }

    @DexIgnore
    public static int a() {
        return a;
    }

    @DexIgnore
    public abstract boolean a(a aVar, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public abstract void b(a aVar, ServiceConnection serviceConnection, String str);

    @DexIgnore
    public static p62 a(Context context) {
        synchronized (b) {
            if (c == null) {
                c = new q82(context.getApplicationContext());
            }
        }
        return c;
    }

    @DexIgnore
    public final void a(String str, String str2, int i, ServiceConnection serviceConnection, String str3, boolean z) {
        b(new a(str, str2, i, z), serviceConnection, str3);
    }
}
