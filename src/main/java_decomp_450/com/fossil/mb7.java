package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mb7 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xb7 {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ gd7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fb7 fb7, fb7 fb72, gd7 gd7) {
            super(fb72);
            this.$completion = fb7;
            this.$this_createCoroutineUnintercepted$inlined = gd7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                t87.a(obj);
                gd7 gd7 = this.$this_createCoroutineUnintercepted$inlined;
                if (gd7 != null) {
                    xe7.a(gd7, 1);
                    return gd7.invoke(this);
                }
                throw new x87("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                t87.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends rb7 {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ ib7 $context;
        @DexIgnore
        public /* final */ /* synthetic */ gd7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fb7 fb7, ib7 ib7, fb7 fb72, ib7 ib72, gd7 gd7) {
            super(fb72, ib72);
            this.$completion = fb7;
            this.$context = ib7;
            this.$this_createCoroutineUnintercepted$inlined = gd7;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                t87.a(obj);
                gd7 gd7 = this.$this_createCoroutineUnintercepted$inlined;
                if (gd7 != null) {
                    xe7.a(gd7, 1);
                    return gd7.invoke(this);
                }
                throw new x87("null cannot be cast to non-null type (kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                t87.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xb7 {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ kd7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(fb7 fb7, fb7 fb72, kd7 kd7, Object obj) {
            super(fb72);
            this.$completion = fb7;
            this.$this_createCoroutineUnintercepted$inlined = kd7;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                t87.a(obj);
                kd7 kd7 = this.$this_createCoroutineUnintercepted$inlined;
                if (kd7 != null) {
                    xe7.a(kd7, 2);
                    return kd7.invoke(this.$receiver$inlined, this);
                }
                throw new x87("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                t87.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends rb7 {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $completion;
        @DexIgnore
        public /* final */ /* synthetic */ ib7 $context;
        @DexIgnore
        public /* final */ /* synthetic */ Object $receiver$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ kd7 $this_createCoroutineUnintercepted$inlined;
        @DexIgnore
        public int label;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(fb7 fb7, ib7 ib7, fb7 fb72, ib7 ib72, kd7 kd7, Object obj) {
            super(fb72, ib72);
            this.$completion = fb7;
            this.$context = ib7;
            this.$this_createCoroutineUnintercepted$inlined = kd7;
            this.$receiver$inlined = obj;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public Object invokeSuspend(Object obj) {
            int i = this.label;
            if (i == 0) {
                this.label = 1;
                t87.a(obj);
                kd7 kd7 = this.$this_createCoroutineUnintercepted$inlined;
                if (kd7 != null) {
                    xe7.a(kd7, 2);
                    return kd7.invoke(this.$receiver$inlined, this);
                }
                throw new x87("null cannot be cast to non-null type (R, kotlin.coroutines.Continuation<T>) -> kotlin.Any?");
            } else if (i == 1) {
                this.label = 2;
                t87.a(obj);
                return obj;
            } else {
                throw new IllegalStateException("This coroutine had already completed".toString());
            }
        }
    }

    @DexIgnore
    public static final <T> fb7<i97> a(gd7<? super fb7<? super T>, ? extends Object> gd7, fb7<? super T> fb7) {
        ee7.b(gd7, "$this$createCoroutineUnintercepted");
        ee7.b(fb7, "completion");
        vb7.a(fb7);
        if (gd7 instanceof ob7) {
            return ((ob7) gd7).create(fb7);
        }
        ib7 context = fb7.getContext();
        if (context == jb7.INSTANCE) {
            if (fb7 != null) {
                return new a(fb7, fb7, gd7);
            }
            throw new x87("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (fb7 != null) {
            return new b(fb7, context, fb7, context, gd7);
        } else {
            throw new x87("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    public static final <R, T> fb7<i97> a(kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7, R r, fb7<? super T> fb7) {
        ee7.b(kd7, "$this$createCoroutineUnintercepted");
        ee7.b(fb7, "completion");
        vb7.a(fb7);
        if (kd7 instanceof ob7) {
            return ((ob7) kd7).create(r, fb7);
        }
        ib7 context = fb7.getContext();
        if (context == jb7.INSTANCE) {
            if (fb7 != null) {
                return new c(fb7, fb7, kd7, r);
            }
            throw new x87("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        } else if (fb7 != null) {
            return new d(fb7, context, fb7, context, kd7, r);
        } else {
            throw new x87("null cannot be cast to non-null type kotlin.coroutines.Continuation<kotlin.Any?>");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.fb7<? super T> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.rb7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.fossil.rb7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v6, resolved type: com.fossil.rb7 */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> fb7<T> a(fb7<? super T> fb7) {
        fb7<T> fb72;
        ee7.b(fb7, "$this$intercepted");
        rb7 rb7 = !(fb7 instanceof rb7) ? null : fb7;
        return (rb7 == null || (fb72 = (fb7<T>) rb7.intercepted()) == null) ? fb7 : fb72;
    }
}
