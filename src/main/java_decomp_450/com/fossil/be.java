package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import com.fossil.ce;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class be implements LifecycleOwner {
    @DexIgnore
    public static /* final */ be i; // = new be();
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public boolean c; // = true;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public Handler e;
    @DexIgnore
    public /* final */ LifecycleRegistry f; // = new LifecycleRegistry(this);
    @DexIgnore
    public Runnable g; // = new a();
    @DexIgnore
    public ce.a h; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            be.this.e();
            be.this.f();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ce.a {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.ce.a
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ce.a
        public void onResume() {
            be.this.b();
        }

        @DexIgnore
        @Override // com.fossil.ce.a
        public void onStart() {
            be.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends jd {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            ce.a(activity).d(be.this.h);
        }

        @DexIgnore
        @Override // com.fossil.jd
        public void onActivityPaused(Activity activity) {
            be.this.a();
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            be.this.d();
        }
    }

    @DexIgnore
    public static void b(Context context) {
        i.a(context);
    }

    @DexIgnore
    public static LifecycleOwner g() {
        return i;
    }

    @DexIgnore
    public void a() {
        int i2 = this.b - 1;
        this.b = i2;
        if (i2 == 0) {
            this.e.postDelayed(this.g, 700);
        }
    }

    @DexIgnore
    public void c() {
        int i2 = this.a + 1;
        this.a = i2;
        if (i2 == 1 && this.d) {
            this.f.a(Lifecycle.a.ON_START);
            this.d = false;
        }
    }

    @DexIgnore
    public void d() {
        this.a--;
        f();
    }

    @DexIgnore
    public void e() {
        if (this.b == 0) {
            this.c = true;
            this.f.a(Lifecycle.a.ON_PAUSE);
        }
    }

    @DexIgnore
    public void f() {
        if (this.a == 0 && this.c) {
            this.f.a(Lifecycle.a.ON_STOP);
            this.d = true;
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LifecycleOwner
    public Lifecycle getLifecycle() {
        return this.f;
    }

    @DexIgnore
    public void b() {
        int i2 = this.b + 1;
        this.b = i2;
        if (i2 != 1) {
            return;
        }
        if (this.c) {
            this.f.a(Lifecycle.a.ON_RESUME);
            this.c = false;
            return;
        }
        this.e.removeCallbacks(this.g);
    }

    @DexIgnore
    public void a(Context context) {
        this.e = new Handler();
        this.f.a(Lifecycle.a.ON_CREATE);
        ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new c());
    }
}
