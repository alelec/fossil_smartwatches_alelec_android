package com.fossil;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class li7 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater b; // = AtomicIntegerFieldUpdater.newUpdater(li7.class, "_handled");
    @DexIgnore
    public volatile int _handled;
    @DexIgnore
    public /* final */ Throwable a;

    @DexIgnore
    public li7(Throwable th, boolean z) {
        this.a = th;
        this._handled = z ? 1 : 0;
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean a() {
        return this._handled;
    }

    @DexIgnore
    public final boolean b() {
        return b.compareAndSet(this, 0, 1);
    }

    @DexIgnore
    public String toString() {
        return ej7.a(this) + '[' + this.a + ']';
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ li7(Throwable th, boolean z, int i, zd7 zd7) {
        this(th, (i & 2) != 0 ? false : z);
    }
}
