package com.fossil;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.j256.ormlite.field.FieldType;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l37 implements SharedPreferences {
    @DexIgnore
    public /* final */ ContentResolver a;
    @DexIgnore
    public /* final */ String[] b; // = {FieldType.FOREIGN_ID_FIELD_SUFFIX, "key", "type", "value"};
    @DexIgnore
    public /* final */ HashMap<String, Object> c; // = new HashMap<>();
    @DexIgnore
    public a d; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements SharedPreferences.Editor {
        @DexIgnore
        public Map<String, Object> a; // = new HashMap();
        @DexIgnore
        public Set<String> b; // = new HashSet();
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public ContentResolver d;

        @DexIgnore
        public a(ContentResolver contentResolver) {
            this.d = contentResolver;
        }

        @DexIgnore
        public void apply() {
        }

        @DexIgnore
        public SharedPreferences.Editor clear() {
            this.c = true;
            return this;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0097  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0099  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x00ae  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x003f A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean commit() {
            /*
                r10 = this;
                android.content.ContentValues r0 = new android.content.ContentValues
                r0.<init>()
                boolean r1 = r10.c
                r2 = 0
                if (r1 == 0) goto L_0x0014
                android.content.ContentResolver r1 = r10.d
                android.net.Uri r3 = com.fossil.t27.a
                r4 = 0
                r1.delete(r3, r4, r4)
                r10.c = r2
            L_0x0014:
                java.util.Set<java.lang.String> r1 = r10.b
                java.util.Iterator r1 = r1.iterator()
            L_0x001a:
                boolean r3 = r1.hasNext()
                java.lang.String r4 = "key = ?"
                r5 = 1
                if (r3 == 0) goto L_0x0035
                java.lang.Object r3 = r1.next()
                java.lang.String r3 = (java.lang.String) r3
                android.content.ContentResolver r6 = r10.d
                android.net.Uri r7 = com.fossil.t27.a
                java.lang.String[] r5 = new java.lang.String[r5]
                r5[r2] = r3
                r6.delete(r7, r4, r5)
                goto L_0x001a
            L_0x0035:
                java.util.Map<java.lang.String, java.lang.Object> r1 = r10.a
                java.util.Set r1 = r1.entrySet()
                java.util.Iterator r1 = r1.iterator()
            L_0x003f:
                boolean r3 = r1.hasNext()
                if (r3 == 0) goto L_0x00c1
                java.lang.Object r3 = r1.next()
                java.util.Map$Entry r3 = (java.util.Map.Entry) r3
                java.lang.Object r6 = r3.getValue()
                java.lang.String r7 = "MicroMsg.SDK.PluginProvider.Resolver"
                if (r6 != 0) goto L_0x005a
                java.lang.String r8 = "unresolve failed, null value"
            L_0x0055:
                com.fossil.p27.a(r7, r8)
                r7 = 0
                goto L_0x0095
            L_0x005a:
                boolean r8 = r6 instanceof java.lang.Integer
                if (r8 == 0) goto L_0x0060
                r7 = 1
                goto L_0x0095
            L_0x0060:
                boolean r8 = r6 instanceof java.lang.Long
                if (r8 == 0) goto L_0x0066
                r7 = 2
                goto L_0x0095
            L_0x0066:
                boolean r8 = r6 instanceof java.lang.String
                if (r8 == 0) goto L_0x006c
                r7 = 3
                goto L_0x0095
            L_0x006c:
                boolean r8 = r6 instanceof java.lang.Boolean
                if (r8 == 0) goto L_0x0072
                r7 = 4
                goto L_0x0095
            L_0x0072:
                boolean r8 = r6 instanceof java.lang.Float
                if (r8 == 0) goto L_0x0078
                r7 = 5
                goto L_0x0095
            L_0x0078:
                boolean r8 = r6 instanceof java.lang.Double
                if (r8 == 0) goto L_0x007e
                r7 = 6
                goto L_0x0095
            L_0x007e:
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "unresolve failed, unknown type="
                r8.<init>(r9)
                java.lang.Class r9 = r6.getClass()
                java.lang.String r9 = r9.toString()
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                goto L_0x0055
            L_0x0095:
                if (r7 != 0) goto L_0x0099
                r6 = 0
                goto L_0x00ac
            L_0x0099:
                java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
                java.lang.String r8 = "type"
                r0.put(r8, r7)
                java.lang.String r6 = r6.toString()
                java.lang.String r7 = "value"
                r0.put(r7, r6)
                r6 = 1
            L_0x00ac:
                if (r6 == 0) goto L_0x003f
                android.content.ContentResolver r6 = r10.d
                android.net.Uri r7 = com.fossil.t27.a
                java.lang.String[] r8 = new java.lang.String[r5]
                java.lang.Object r3 = r3.getKey()
                java.lang.String r3 = (java.lang.String) r3
                r8[r2] = r3
                r6.update(r7, r0, r4, r8)
                goto L_0x003f
            L_0x00c1:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.l37.a.commit():boolean");
        }

        @DexIgnore
        public SharedPreferences.Editor putBoolean(String str, boolean z) {
            this.a.put(str, Boolean.valueOf(z));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putFloat(String str, float f) {
            this.a.put(str, Float.valueOf(f));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putInt(String str, int i) {
            this.a.put(str, Integer.valueOf(i));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putLong(String str, long j) {
            this.a.put(str, Long.valueOf(j));
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        public SharedPreferences.Editor putString(String str, String str2) {
            this.a.put(str, str2);
            this.b.remove(str);
            return this;
        }

        @DexIgnore
        @Override // android.content.SharedPreferences.Editor
        public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
            return null;
        }

        @DexIgnore
        public SharedPreferences.Editor remove(String str) {
            this.b.add(str);
            return this;
        }
    }

    @DexIgnore
    public l37(Context context) {
        this.a = context.getContentResolver();
    }

    @DexIgnore
    public final Object a(String str) {
        try {
            Cursor query = this.a.query(t27.a, this.b, "key = ?", new String[]{str}, null);
            if (query == null) {
                return null;
            }
            Object a2 = query.moveToFirst() ? s27.a(query.getInt(query.getColumnIndex("type")), query.getString(query.getColumnIndex("value"))) : null;
            query.close();
            return a2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public boolean contains(String str) {
        return a(str) != null;
    }

    @DexIgnore
    public SharedPreferences.Editor edit() {
        if (this.d == null) {
            this.d = new a(this.a);
        }
        return this.d;
    }

    @DexIgnore
    @Override // android.content.SharedPreferences
    public Map<String, ?> getAll() {
        try {
            Cursor query = this.a.query(t27.a, this.b, null, null, null);
            if (query == null) {
                return null;
            }
            int columnIndex = query.getColumnIndex("key");
            int columnIndex2 = query.getColumnIndex("type");
            int columnIndex3 = query.getColumnIndex("value");
            while (query.moveToNext()) {
                this.c.put(query.getString(columnIndex), s27.a(query.getInt(columnIndex2), query.getString(columnIndex3)));
            }
            query.close();
            return this.c;
        } catch (Exception e) {
            e.printStackTrace();
            return this.c;
        }
    }

    @DexIgnore
    public boolean getBoolean(String str, boolean z) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Boolean)) ? z : ((Boolean) a2).booleanValue();
    }

    @DexIgnore
    public float getFloat(String str, float f) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Float)) ? f : ((Float) a2).floatValue();
    }

    @DexIgnore
    public int getInt(String str, int i) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Integer)) ? i : ((Integer) a2).intValue();
    }

    @DexIgnore
    public long getLong(String str, long j) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof Long)) ? j : ((Long) a2).longValue();
    }

    @DexIgnore
    public String getString(String str, String str2) {
        Object a2 = a(str);
        return (a2 == null || !(a2 instanceof String)) ? str2 : (String) a2;
    }

    @DexIgnore
    @Override // android.content.SharedPreferences
    public Set<String> getStringSet(String str, Set<String> set) {
        return null;
    }

    @DexIgnore
    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    @DexIgnore
    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }
}
