package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ja0 extends na0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ja0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public ja0 createFromParcel(Parcel parcel) {
            return new ja0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ja0[] newArray(int i) {
            return new ja0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public ja0 m29createFromParcel(Parcel parcel) {
            return new ja0(parcel, null);
        }
    }

    @DexIgnore
    public ja0() {
        super(pa0.MUSIC, null, null, 6);
    }

    @DexIgnore
    public ja0(zg0 zg0) {
        super(pa0.MUSIC, zg0, null, 4);
    }

    @DexIgnore
    public /* synthetic */ ja0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
