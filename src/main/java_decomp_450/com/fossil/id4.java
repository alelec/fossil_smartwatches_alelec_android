package com.fossil;

import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;
import java.util.ArrayDeque;
import java.util.Iterator;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class id4 {
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ ArrayDeque<String> d; // = new ArrayDeque<>();
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public boolean f; // = false;

    @DexIgnore
    public id4(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        this.a = sharedPreferences;
        this.b = str;
        this.c = str2;
        this.e = executor;
    }

    @DexIgnore
    public static id4 a(SharedPreferences sharedPreferences, String str, String str2, Executor executor) {
        id4 id4 = new id4(sharedPreferences, str, str2, executor);
        id4.b();
        return id4;
    }

    @DexIgnore
    public final void b() {
        synchronized (this.d) {
            this.d.clear();
            String string = this.a.getString(this.b, "");
            if (!TextUtils.isEmpty(string)) {
                if (string.contains(this.c)) {
                    String[] split = string.split(this.c, -1);
                    if (split.length == 0) {
                        Log.e("FirebaseMessaging", "Corrupted queue. Please check the queue contents and item separator provided");
                    }
                    for (String str : split) {
                        if (!TextUtils.isEmpty(str)) {
                            this.d.add(str);
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final String c() {
        String peek;
        synchronized (this.d) {
            peek = this.d.peek();
        }
        return peek;
    }

    @DexIgnore
    public final String d() {
        StringBuilder sb = new StringBuilder();
        Iterator<String> it = this.d.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            sb.append(this.c);
        }
        return sb.toString();
    }

    @DexIgnore
    /* renamed from: e */
    public final void a() {
        synchronized (this.d) {
            this.a.edit().putString(this.b, d()).commit();
        }
    }

    @DexIgnore
    public final void f() {
        this.e.execute(new hd4(this));
    }

    @DexIgnore
    public final boolean a(boolean z) {
        if (z && !this.f) {
            f();
        }
        return z;
    }

    @DexIgnore
    public final boolean a(Object obj) {
        boolean remove;
        synchronized (this.d) {
            remove = this.d.remove(obj);
            a(remove);
        }
        return remove;
    }
}
