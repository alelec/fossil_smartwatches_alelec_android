package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class do4 implements co4 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<ao4> b;
    @DexIgnore
    public /* final */ bo4 c; // = new bo4();
    @DexIgnore
    public /* final */ ji d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<ao4> {
        @DexIgnore
        public a(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, ao4 ao4) {
            if (ao4.e() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, ao4.e());
            }
            if (ao4.i() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, ao4.i());
            }
            if (ao4.a() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, ao4.a());
            }
            String a2 = do4.this.c.a(ao4.d());
            if (a2 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a2);
            }
            String a3 = do4.this.c.a(ao4.b());
            if (a3 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a3);
            }
            String a4 = do4.this.c.a(ao4.f());
            if (a4 == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, a4);
            }
            ajVar.bindLong(7, ao4.c() ? 1 : 0);
            ajVar.bindLong(8, (long) ao4.g());
            String a5 = do4.this.c.a(ao4.h());
            if (a5 == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindString(9, a5);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notification` (`id`,`titleKey`,`bodyKey`,`createdAt`,`challengeData`,`profileData`,`confirmChallenge`,`rank`,`reachGoalAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(do4 do4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM notification WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(do4 do4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM notification";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<List<ao4>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public d(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<ao4> call() throws Exception {
            Cursor a2 = pi.a(do4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "titleKey");
                int b4 = oi.b(a2, "bodyKey");
                int b5 = oi.b(a2, "createdAt");
                int b6 = oi.b(a2, "challengeData");
                int b7 = oi.b(a2, "profileData");
                int b8 = oi.b(a2, "confirmChallenge");
                int b9 = oi.b(a2, "rank");
                int b10 = oi.b(a2, "reachGoalAt");
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    arrayList.add(new ao4(a2.getString(b2), a2.getString(b3), a2.getString(b4), do4.this.c.c(a2.getString(b5)), do4.this.c.a(a2.getString(b6)), do4.this.c.b(a2.getString(b7)), a2.getInt(b8) != 0, a2.getInt(b9), do4.this.c.c(a2.getString(b10))));
                }
                return arrayList;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore
    public do4(ci ciVar) {
        this.a = ciVar;
        this.b = new a(ciVar);
        new b(this, ciVar);
        this.d = new c(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.co4
    public Long[] a(List<ao4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.co4
    public LiveData<List<ao4>> b() {
        return this.a.getInvalidationTracker().a(new String[]{"notification"}, false, (Callable) new d(fi.b("SELECT * FROM notification ORDER BY createdAt DESC", 0)));
    }

    @DexIgnore
    @Override // com.fossil.co4
    public void a() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.d.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }
}
