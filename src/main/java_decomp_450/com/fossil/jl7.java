package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jl7<U, T extends U> extends jm7<T> implements Runnable {
    @DexIgnore
    public /* final */ long e;

    @DexIgnore
    public jl7(long j, fb7<? super U> fb7) {
        super(fb7.getContext(), fb7);
        this.e = j;
    }

    @DexIgnore
    @Override // com.fossil.rh7, com.fossil.pk7
    public String k() {
        return super.k() + "(timeMillis=" + this.e + ')';
    }

    @DexIgnore
    public void run() {
        a((Throwable) kl7.a(this.e, this));
    }
}
