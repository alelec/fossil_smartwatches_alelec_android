package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public interface a53 {
    @DexIgnore
    c12<Status> a(a12 a12, d53 d53);

    @DexIgnore
    c12<Status> a(a12 a12, LocationRequest locationRequest, d53 d53);
}
