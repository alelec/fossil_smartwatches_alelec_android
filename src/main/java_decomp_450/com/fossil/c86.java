package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c86 implements Factory<b86> {
    @DexIgnore
    public static b86 a(z76 z76, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new b86(z76, userRepository, summariesRepository, portfolioApp);
    }
}
