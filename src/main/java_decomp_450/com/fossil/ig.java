package com.fossil;

import com.fossil.ng;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;
    @DexIgnore
    public /* final */ ng.d<T> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> {
        @DexIgnore
        public static /* final */ Object d; // = new Object();
        @DexIgnore
        public static Executor e;
        @DexIgnore
        public Executor a;
        @DexIgnore
        public Executor b;
        @DexIgnore
        public /* final */ ng.d<T> c;

        @DexIgnore
        public a(ng.d<T> dVar) {
            this.c = dVar;
        }

        @DexIgnore
        public ig<T> a() {
            if (this.b == null) {
                synchronized (d) {
                    if (e == null) {
                        e = Executors.newFixedThreadPool(2);
                    }
                }
                this.b = e;
            }
            return new ig<>(this.a, this.b, this.c);
        }
    }

    @DexIgnore
    public ig(Executor executor, Executor executor2, ng.d<T> dVar) {
        this.a = executor;
        this.b = executor2;
        this.c = dVar;
    }

    @DexIgnore
    public Executor a() {
        return this.b;
    }

    @DexIgnore
    public ng.d<T> b() {
        return this.c;
    }

    @DexIgnore
    public Executor c() {
        return this.a;
    }
}
