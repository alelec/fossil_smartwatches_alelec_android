package com.fossil;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f00 implements yw {
    @DexIgnore
    public /* final */ g00 b;
    @DexIgnore
    public /* final */ URL c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public URL f;
    @DexIgnore
    public volatile byte[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public f00(URL url) {
        this(url, g00.a);
    }

    @DexIgnore
    public String a() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        u50.a(url);
        return url.toString();
    }

    @DexIgnore
    public final byte[] b() {
        if (this.g == null) {
            this.g = a().getBytes(yw.a);
        }
        return this.g;
    }

    @DexIgnore
    public Map<String, String> c() {
        return this.b.a();
    }

    @DexIgnore
    public final String d() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                u50.a(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    @DexIgnore
    public final URL e() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(d());
        }
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (!(obj instanceof f00)) {
            return false;
        }
        f00 f00 = (f00) obj;
        if (!a().equals(f00.a()) || !this.b.equals(f00.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public URL f() throws MalformedURLException {
        return e();
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        if (this.h == 0) {
            int hashCode = a().hashCode();
            this.h = hashCode;
            this.h = (hashCode * 31) + this.b.hashCode();
        }
        return this.h;
    }

    @DexIgnore
    public String toString() {
        return a();
    }

    @DexIgnore
    public f00(String str) {
        this(str, g00.a);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b());
    }

    @DexIgnore
    public f00(URL url, g00 g00) {
        u50.a(url);
        this.c = url;
        this.d = null;
        u50.a(g00);
        this.b = g00;
    }

    @DexIgnore
    public f00(String str, g00 g00) {
        this.c = null;
        u50.a(str);
        this.d = str;
        u50.a(g00);
        this.b = g00;
    }
}
