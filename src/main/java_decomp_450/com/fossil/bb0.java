package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bb0 extends k60 implements Parcelable {
    @DexIgnore
    public /* final */ cb0 a;
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public bb0(cb0 cb0, byte b2) {
        this.a = cb0;
        this.b = b2;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.M2, yz0.a(this.a)), r51.x1, Short.valueOf(yz0.b(this.b)));
    }

    @DexIgnore
    public final byte b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            bb0 bb0 = (bb0) obj;
            return this.a == bb0.a && this.b == bb0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.DeviceEvent");
    }

    @DexIgnore
    public final cb0 getDeviceEventId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public bb0(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0017
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.ee7.a(r0, r1)
            com.fossil.cb0 r0 = com.fossil.cb0.valueOf(r0)
            byte r3 = r3.readByte()
            r2.<init>(r0, r3)
            return
        L_0x0017:
            com.fossil.ee7.a()
            r3 = 0
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bb0.<init>(android.os.Parcel):void");
    }
}
