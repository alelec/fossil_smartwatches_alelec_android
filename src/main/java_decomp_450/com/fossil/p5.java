package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.h5;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p5 extends r5 {
    @DexIgnore
    public h5 c;
    @DexIgnore
    public p5 d;
    @DexIgnore
    public float e;
    @DexIgnore
    public p5 f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h; // = 0;
    @DexIgnore
    public p5 i;
    @DexIgnore
    public q5 j; // = null;
    @DexIgnore
    public int k; // = 1;
    @DexIgnore
    public q5 l; // = null;
    @DexIgnore
    public int m; // = 1;

    @DexIgnore
    public p5(h5 h5Var) {
        this.c = h5Var;
    }

    @DexIgnore
    public String a(int i2) {
        return i2 == 1 ? "DIRECT" : i2 == 2 ? "CENTER" : i2 == 3 ? "MATCH" : i2 == 4 ? "CHAIN" : i2 == 5 ? "BARRIER" : "UNCONNECTED";
    }

    @DexIgnore
    public void a(p5 p5Var, float f2) {
        if (((r5) this).b == 0 || !(this.f == p5Var || this.g == f2)) {
            this.f = p5Var;
            this.g = f2;
            if (((r5) this).b == 1) {
                b();
            }
            a();
        }
    }

    @DexIgnore
    public void b(int i2) {
        this.h = i2;
    }

    @DexIgnore
    @Override // com.fossil.r5
    public void d() {
        super.d();
        this.d = null;
        this.e = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.j = null;
        this.k = 1;
        this.l = null;
        this.m = 1;
        this.f = null;
        this.g = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.i = null;
        this.h = 0;
    }

    @DexIgnore
    @Override // com.fossil.r5
    public void e() {
        p5 p5Var;
        p5 p5Var2;
        p5 p5Var3;
        p5 p5Var4;
        p5 p5Var5;
        p5 p5Var6;
        float f2;
        float f3;
        float f4;
        float f5;
        p5 p5Var7;
        boolean z = true;
        if (((r5) this).b != 1 && this.h != 4) {
            q5 q5Var = this.j;
            if (q5Var != null) {
                if (((r5) q5Var).b == 1) {
                    this.e = ((float) this.k) * q5Var.c;
                } else {
                    return;
                }
            }
            q5 q5Var2 = this.l;
            if (q5Var2 != null) {
                if (((r5) q5Var2).b == 1) {
                    float f6 = q5Var2.c;
                } else {
                    return;
                }
            }
            if (this.h == 1 && ((p5Var7 = this.d) == null || ((r5) p5Var7).b == 1)) {
                p5 p5Var8 = this.d;
                if (p5Var8 == null) {
                    this.f = this;
                    this.g = this.e;
                } else {
                    this.f = p5Var8.f;
                    this.g = p5Var8.g + this.e;
                }
                a();
            } else if (this.h == 2 && (p5Var4 = this.d) != null && ((r5) p5Var4).b == 1 && (p5Var5 = this.i) != null && (p5Var6 = p5Var5.d) != null && ((r5) p5Var6).b == 1) {
                if (y4.j() != null) {
                    y4.j().v++;
                }
                this.f = this.d.f;
                p5 p5Var9 = this.i;
                p5Var9.f = p5Var9.d.f;
                h5.d dVar = this.c.c;
                int i2 = 0;
                if (!(dVar == h5.d.RIGHT || dVar == h5.d.BOTTOM)) {
                    z = false;
                }
                if (z) {
                    f3 = this.d.g;
                    f2 = this.i.d.g;
                } else {
                    f3 = this.i.d.g;
                    f2 = this.d.g;
                }
                float f7 = f3 - f2;
                h5 h5Var = this.c;
                h5.d dVar2 = h5Var.c;
                if (dVar2 == h5.d.LEFT || dVar2 == h5.d.RIGHT) {
                    f5 = f7 - ((float) this.c.b.t());
                    f4 = this.c.b.V;
                } else {
                    f5 = f7 - ((float) h5Var.b.j());
                    f4 = this.c.b.W;
                }
                int b = this.c.b();
                int b2 = this.i.c.b();
                if (this.c.g() == this.i.c.g()) {
                    f4 = 0.5f;
                    b2 = 0;
                } else {
                    i2 = b;
                }
                float f8 = (float) i2;
                float f9 = (float) b2;
                float f10 = (f5 - f8) - f9;
                if (z) {
                    p5 p5Var10 = this.i;
                    p5Var10.g = p5Var10.d.g + f9 + (f10 * f4);
                    this.g = (this.d.g - f8) - (f10 * (1.0f - f4));
                } else {
                    this.g = this.d.g + f8 + (f10 * f4);
                    p5 p5Var11 = this.i;
                    p5Var11.g = (p5Var11.d.g - f9) - (f10 * (1.0f - f4));
                }
                a();
                this.i.a();
            } else if (this.h == 3 && (p5Var = this.d) != null && ((r5) p5Var).b == 1 && (p5Var2 = this.i) != null && (p5Var3 = p5Var2.d) != null && ((r5) p5Var3).b == 1) {
                if (y4.j() != null) {
                    y4.j().w++;
                }
                p5 p5Var12 = this.d;
                this.f = p5Var12.f;
                p5 p5Var13 = this.i;
                p5 p5Var14 = p5Var13.d;
                p5Var13.f = p5Var14.f;
                this.g = p5Var12.g + this.e;
                p5Var13.g = p5Var14.g + p5Var13.e;
                a();
                this.i.a();
            } else if (this.h == 5) {
                this.c.b.H();
            }
        }
    }

    @DexIgnore
    public float f() {
        return this.g;
    }

    @DexIgnore
    public void g() {
        h5 g2 = this.c.g();
        if (g2 != null) {
            if (g2.g() == this.c) {
                this.h = 4;
                g2.d().h = 4;
            }
            int b = this.c.b();
            h5.d dVar = this.c.c;
            if (dVar == h5.d.RIGHT || dVar == h5.d.BOTTOM) {
                b = -b;
            }
            a(g2.d(), b);
        }
    }

    @DexIgnore
    public String toString() {
        if (((r5) this).b != 1) {
            return "{ " + this.c + " UNRESOLVED} type: " + a(this.h);
        } else if (this.f == this) {
            return "[" + this.c + ", RESOLVED: " + this.g + "]  type: " + a(this.h);
        } else {
            return "[" + this.c + ", RESOLVED: " + this.f + ":" + this.g + "] type: " + a(this.h);
        }
    }

    @DexIgnore
    public void b(p5 p5Var, float f2) {
        this.i = p5Var;
    }

    @DexIgnore
    public void b(p5 p5Var, int i2, q5 q5Var) {
        this.i = p5Var;
        this.l = q5Var;
        this.m = i2;
    }

    @DexIgnore
    public void a(int i2, p5 p5Var, int i3) {
        this.h = i2;
        this.d = p5Var;
        this.e = (float) i3;
        p5Var.a(this);
    }

    @DexIgnore
    public void a(p5 p5Var, int i2) {
        this.d = p5Var;
        this.e = (float) i2;
        p5Var.a(this);
    }

    @DexIgnore
    public void a(p5 p5Var, int i2, q5 q5Var) {
        this.d = p5Var;
        p5Var.a(this);
        this.j = q5Var;
        this.k = i2;
        q5Var.a(this);
    }

    @DexIgnore
    public void a(y4 y4Var) {
        c5 e2 = this.c.e();
        p5 p5Var = this.f;
        if (p5Var == null) {
            y4Var.a(e2, (int) (this.g + 0.5f));
        } else {
            y4Var.a(e2, y4Var.a(p5Var.c), (int) (this.g + 0.5f), 6);
        }
    }
}
