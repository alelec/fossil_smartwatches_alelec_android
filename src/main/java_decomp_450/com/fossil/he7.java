package com.fossil;

import com.fossil.bg7;
import com.fossil.yf7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class he7 extends je7 implements yf7 {
    @DexIgnore
    public he7() {
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public sf7 computeReflected() {
        te7.a(this);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.bg7
    public Object getDelegate(Object obj) {
        return ((yf7) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.gd7
    public Object invoke(Object obj) {
        return get(obj);
    }

    @DexIgnore
    public he7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.bg7
    public bg7.a getGetter() {
        return ((yf7) getReflected()).getGetter();
    }

    @DexIgnore
    @Override // com.fossil.yf7
    public yf7.a getSetter() {
        return ((yf7) getReflected()).getSetter();
    }
}
