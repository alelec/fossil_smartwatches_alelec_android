package com.fossil;

import android.graphics.RectF;
import com.fossil.nr5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mr5 {
    @DexIgnore
    public /* final */ RectF a; // = new RectF();
    @DexIgnore
    public /* final */ RectF b; // = new RectF();
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public float k; // = 1.0f;
    @DexIgnore
    public float l; // = 1.0f;

    @DexIgnore
    public static boolean a(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f6 && f3 > f5 && f3 < f7;
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.i = (float) i2;
        this.j = (float) i3;
    }

    @DexIgnore
    public float b() {
        return Math.min(this.f, this.j / this.l);
    }

    @DexIgnore
    public float c() {
        return Math.min(this.e, this.i / this.k);
    }

    @DexIgnore
    public float d() {
        return Math.max(this.d, this.h / this.l);
    }

    @DexIgnore
    public float e() {
        return Math.max(this.c, this.g / this.k);
    }

    @DexIgnore
    public RectF f() {
        this.b.set(this.a);
        return this.b;
    }

    @DexIgnore
    public float g() {
        return this.l;
    }

    @DexIgnore
    public float h() {
        return this.k;
    }

    @DexIgnore
    public boolean i() {
        return this.a.width() >= 100.0f && this.a.height() >= 100.0f;
    }

    @DexIgnore
    public static boolean c(float f2, float f3, float f4, float f5, float f6, float f7) {
        return Math.abs(f2 - f4) <= f7 && f3 > f5 && f3 < f6;
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.g = (float) i2;
        this.h = (float) i3;
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5) {
        this.e = f2;
        this.f = f3;
        this.k = f4;
        this.l = f5;
    }

    @DexIgnore
    public static boolean b(float f2, float f3, float f4, float f5, float f6, float f7) {
        return f2 > f4 && f2 < f5 && Math.abs(f3 - f6) <= f7;
    }

    @DexIgnore
    public void a(lr5 lr5) {
        this.c = (float) lr5.C;
        this.d = (float) lr5.D;
        this.g = (float) lr5.E;
        this.h = (float) lr5.F;
        this.i = (float) lr5.G;
        this.j = (float) lr5.H;
    }

    @DexIgnore
    public void a(RectF rectF) {
        this.a.set(rectF);
    }

    @DexIgnore
    public nr5 a(float f2, float f3, float f4, CropImageView.c cVar) {
        nr5.b bVar;
        if (cVar == CropImageView.c.OVAL) {
            bVar = a(f2, f3);
        } else {
            bVar = a(f2, f3, f4);
        }
        if (bVar != null) {
            return new nr5(bVar, this, f2, f3);
        }
        return null;
    }

    @DexIgnore
    public final nr5.b a(float f2, float f3, float f4) {
        RectF rectF = this.a;
        if (a(f2, f3, rectF.left, rectF.top, f4)) {
            return nr5.b.TOP_LEFT;
        }
        RectF rectF2 = this.a;
        if (a(f2, f3, rectF2.right, rectF2.top, f4)) {
            return nr5.b.TOP_RIGHT;
        }
        RectF rectF3 = this.a;
        if (a(f2, f3, rectF3.left, rectF3.bottom, f4)) {
            return nr5.b.BOTTOM_LEFT;
        }
        RectF rectF4 = this.a;
        if (a(f2, f3, rectF4.right, rectF4.bottom, f4)) {
            return nr5.b.BOTTOM_RIGHT;
        }
        RectF rectF5 = this.a;
        if (a(f2, f3, rectF5.left, rectF5.top, rectF5.right, rectF5.bottom) && a()) {
            return nr5.b.CENTER;
        }
        RectF rectF6 = this.a;
        if (b(f2, f3, rectF6.left, rectF6.right, rectF6.top, f4)) {
            return nr5.b.TOP;
        }
        RectF rectF7 = this.a;
        if (b(f2, f3, rectF7.left, rectF7.right, rectF7.bottom, f4)) {
            return nr5.b.BOTTOM;
        }
        RectF rectF8 = this.a;
        if (c(f2, f3, rectF8.left, rectF8.top, rectF8.bottom, f4)) {
            return nr5.b.LEFT;
        }
        RectF rectF9 = this.a;
        if (c(f2, f3, rectF9.right, rectF9.top, rectF9.bottom, f4)) {
            return nr5.b.RIGHT;
        }
        RectF rectF10 = this.a;
        if (!a(f2, f3, rectF10.left, rectF10.top, rectF10.right, rectF10.bottom) || a()) {
            return null;
        }
        return nr5.b.CENTER;
    }

    @DexIgnore
    public final nr5.b a(float f2, float f3) {
        float width = this.a.width() / 6.0f;
        RectF rectF = this.a;
        float f4 = rectF.left;
        float f5 = f4 + width;
        float f6 = f4 + (width * 5.0f);
        float height = rectF.height() / 6.0f;
        float f7 = this.a.top;
        float f8 = f7 + height;
        float f9 = f7 + (height * 5.0f);
        if (f2 < f5) {
            if (f3 < f8) {
                return nr5.b.TOP_LEFT;
            }
            if (f3 < f9) {
                return nr5.b.LEFT;
            }
            return nr5.b.BOTTOM_LEFT;
        } else if (f2 < f6) {
            if (f3 < f8) {
                return nr5.b.TOP;
            }
            if (f3 < f9) {
                return nr5.b.CENTER;
            }
            return nr5.b.BOTTOM;
        } else if (f3 < f8) {
            return nr5.b.TOP_RIGHT;
        } else {
            if (f3 < f9) {
                return nr5.b.RIGHT;
            }
            return nr5.b.BOTTOM_RIGHT;
        }
    }

    @DexIgnore
    public static boolean a(float f2, float f3, float f4, float f5, float f6) {
        return Math.abs(f2 - f4) <= f6 && Math.abs(f3 - f5) <= f6;
    }

    @DexIgnore
    public final boolean a() {
        return !i();
    }
}
