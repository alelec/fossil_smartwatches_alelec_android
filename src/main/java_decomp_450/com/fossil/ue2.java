package com.fossil;

import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ue2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ Intent b;
    @DexIgnore
    public /* final */ /* synthetic */ te2 c;

    @DexIgnore
    public ue2(te2 te2, Intent intent, Intent intent2) {
        this.c = te2;
        this.a = intent;
        this.b = intent2;
    }

    @DexIgnore
    public final void run() {
        this.c.handleIntent(this.a);
        this.c.a(this.b);
    }
}
