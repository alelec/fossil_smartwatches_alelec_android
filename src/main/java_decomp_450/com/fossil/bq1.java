package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq1 extends pe1 {
    @DexIgnore
    public static /* final */ bo1 CREATOR; // = new bo1(null);
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public /* synthetic */ bq1(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        return yz0.a(super.a(), r51.S3, Byte.valueOf(this.b));
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b);
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(bq1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((bq1) obj).b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.StartRepeatInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }
}
