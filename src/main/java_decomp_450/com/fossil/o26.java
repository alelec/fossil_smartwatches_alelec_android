package com.fossil;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o26 extends dy6 {
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<gx4> p;
    @DexIgnore
    public b q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final o26 a() {
            return new o26();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void I0();

        @DexIgnore
        void Q();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ o26 b;

        @DexIgnore
        public c(Dialog dialog, gx4 gx4, o26 o26) {
            this.a = dialog;
            this.b = o26;
        }

        @DexIgnore
        public final void onClick(View view) {
            o26 o26 = this.b;
            Dialog dialog = this.a;
            ee7.a((Object) dialog, "dialog");
            o26.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ o26 b;

        @DexIgnore
        public d(Dialog dialog, gx4 gx4, o26 o26) {
            this.a = dialog;
            this.b = o26;
        }

        @DexIgnore
        public final void onClick(View view) {
            b a2 = this.b.q;
            if (a2 != null) {
                a2.Q();
            }
            o26 o26 = this.b;
            Dialog dialog = this.a;
            ee7.a((Object) dialog, "dialog");
            o26.onDismiss(dialog);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ Dialog a;
        @DexIgnore
        public /* final */ /* synthetic */ o26 b;

        @DexIgnore
        public e(Dialog dialog, gx4 gx4, o26 o26) {
            this.a = dialog;
            this.b = o26;
        }

        @DexIgnore
        public final void onClick(View view) {
            b a2 = this.b.q;
            if (a2 != null) {
                a2.I0();
            }
            o26 o26 = this.b;
            Dialog dialog = this.a;
            ee7.a((Object) dialog, "dialog");
            o26.onDismiss(dialog);
        }
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Dialog dialog;
        ee7.b(layoutInflater, "inflater");
        gx4 gx4 = (gx4) qb.a(layoutInflater, 2131558501, viewGroup, false, this.j);
        qw6<gx4> qw6 = new qw6<>(this, gx4);
        this.p = qw6;
        gx4 a2 = qw6.a();
        if (!(a2 == null || (dialog = getDialog()) == null)) {
            a2.q.setOnClickListener(new c(dialog, a2, this));
            a2.s.setOnClickListener(new d(dialog, a2, this));
            a2.r.setOnClickListener(new e(dialog, a2, this));
        }
        ee7.a((Object) gx4, "binding");
        return gx4.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    public final void a(b bVar) {
        this.q = bVar;
    }
}
