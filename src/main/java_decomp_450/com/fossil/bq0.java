package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq0 {
    @DexIgnore
    public /* synthetic */ bq0(zd7 zd7) {
    }

    @DexIgnore
    public final xr0 a(int i) {
        xr0 xr0;
        xr0[] values = xr0.values();
        int length = values.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                xr0 = null;
                break;
            }
            xr0 = values[i2];
            if (xr0.a == i) {
                break;
            }
            i2++;
        }
        return xr0 != null ? xr0 : xr0.k;
    }
}
