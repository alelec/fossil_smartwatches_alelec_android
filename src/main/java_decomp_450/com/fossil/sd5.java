package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sd5 implements MembersInjector<rd5> {
    @DexIgnore
    public static void a(rd5 rd5, ch5 ch5) {
        rd5.a = ch5;
    }

    @DexIgnore
    public static void a(rd5 rd5, DeviceRepository deviceRepository) {
        rd5.b = deviceRepository;
    }

    @DexIgnore
    public static void a(rd5 rd5, UserRepository userRepository) {
        rd5.c = userRepository;
    }

    @DexIgnore
    public static void a(rd5 rd5, aw6 aw6) {
        rd5.d = aw6;
    }
}
