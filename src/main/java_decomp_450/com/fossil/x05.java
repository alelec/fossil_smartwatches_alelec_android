package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import com.portfolio.platform.view.WindowInsetsFrameLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x05 extends w05 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131362149, 1);
        A.put(2131362059, 2);
        A.put(2131363342, 3);
        A.put(2131363229, 4);
        A.put(2131363311, 5);
        A.put(2131362991, 6);
        A.put(2131362020, 7);
    }
    */

    @DexIgnore
    public x05(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public x05(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (CropImageView) objArr[7], (ConstraintLayout) objArr[0], (ConstraintLayout) objArr[2], (WindowInsetsFrameLayout) objArr[1], (RecyclerView) objArr[6], (TextView) objArr[4], (TextView) objArr[5], (TextView) objArr[3]);
        this.y = -1;
        ((w05) this).r.setTag(null);
        a(view);
        f();
    }
}
