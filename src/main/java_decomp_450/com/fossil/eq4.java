package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.searchFriend.BCFindFriendsActivity;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eq4 extends go5 {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public static /* final */ a u; // = new a(null);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public qw6<e35> g;
    @DexIgnore
    public gq4 h;
    @DexIgnore
    public String i;
    @DexIgnore
    public long j;
    @DexIgnore
    public int p; // = -1;
    @DexIgnore
    public String q;
    @DexIgnore
    public iq4 r;
    @DexIgnore
    public HashMap s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return eq4.t;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final eq4 a(String str, long j, int i, String str2) {
            eq4 eq4 = new eq4();
            Bundle bundle = new Bundle();
            bundle.putString("challenge_id_extra", str);
            bundle.putLong("challenge_start_time_extra", j);
            bundle.putInt("challenge_target_extra", i);
            bundle.putString("challenge_status_extra", str2);
            eq4.setArguments(bundle);
            return eq4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public b(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ e35 a;
        @DexIgnore
        public /* final */ /* synthetic */ eq4 b;

        @DexIgnore
        public c(e35 e35, eq4 eq4) {
            this.a = e35;
            this.b = eq4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.q;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            String b2 = this.b.i;
            if (b2 != null) {
                eq4.d(this.b).a(b2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public d(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        public final void onClick(View view) {
            BCFindFriendsActivity.a aVar = BCFindFriendsActivity.y;
            eq4 eq4 = this.a;
            aVar.a(eq4, eq4.i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<List<? extends Object>> {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public e(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Object> list) {
            iq4 c = eq4.c(this.a);
            ee7.a((Object) list, "it");
            c.a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public f(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            e35 e35 = (e35) eq4.a(this.a).a();
            if (e35 != null) {
                SwipeRefreshLayout swipeRefreshLayout = e35.w;
                ee7.a((Object) swipeRefreshLayout, "swipe");
                ee7.a((Object) bool, "it");
                swipeRefreshLayout.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public g(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            if (r87.getFirst().booleanValue()) {
                e35 e35 = (e35) eq4.a(this.a).a();
                if (e35 != null) {
                    FlexibleTextView flexibleTextView = e35.q;
                    ee7.a((Object) flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                return;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, ig5.a(activity, 2131886227), 1).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ eq4 a;

        @DexIgnore
        public h(eq4 eq4) {
            this.a = eq4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            e35 e35;
            ImageView imageView;
            if (ee7.a((Object) "completed", (Object) str) && (e35 = (e35) eq4.a(this.a).a()) != null && (imageView = e35.s) != null) {
                imageView.setVisibility(0);
            }
        }
    }

    /*
    static {
        String simpleName = eq4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCLeaderBoardFragment::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(eq4 eq4) {
        qw6<e35> qw6 = eq4.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ iq4 c(eq4 eq4) {
        iq4 iq4 = eq4.r;
        if (iq4 != null) {
            return iq4;
        }
        ee7.d("leaderBoardAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ gq4 d(eq4 eq4) {
        gq4 gq4 = eq4.h;
        if (gq4 != null) {
            return gq4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.s;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        this.r = new iq4(this.j, this.p);
        qw6<e35> qw6 = this.g;
        if (qw6 != null) {
            e35 a2 = qw6.a();
            if (a2 != null) {
                RecyclerView recyclerView = a2.v;
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                iq4 iq4 = this.r;
                if (iq4 != null) {
                    recyclerView.setAdapter(iq4);
                    a2.t.setOnClickListener(new b(this));
                    a2.w.setOnRefreshListener(new c(a2, this));
                    a2.s.setOnClickListener(new d(this));
                    return;
                }
                ee7.d("leaderBoardAdapter");
                throw null;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        gq4 gq4 = this.h;
        if (gq4 != null) {
            gq4.d().a(getViewLifecycleOwner(), new e(this));
            gq4 gq42 = this.h;
            if (gq42 != null) {
                gq42.c().a(getViewLifecycleOwner(), new f(this));
                gq4 gq43 = this.h;
                if (gq43 != null) {
                    gq43.b().a(getViewLifecycleOwner(), new g(this));
                    gq4 gq44 = this.h;
                    if (gq44 != null) {
                        gq44.a().a(getViewLifecycleOwner(), new h(this));
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        super.onActivityResult(i2, i3, intent);
        if (i2 == 16 && i3 == -1 && (str = this.i) != null) {
            gq4 gq4 = this.h;
            if (gq4 == null) {
                ee7.d("viewModel");
                throw null;
            } else if (str != null) {
                gq4.a(str);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().l().a(this);
        rj4 rj4 = this.f;
        String str = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(gq4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
            this.h = (gq4) a2;
            Bundle arguments = getArguments();
            this.i = arguments != null ? arguments.getString("challenge_id_extra") : null;
            Bundle arguments2 = getArguments();
            this.j = arguments2 != null ? arguments2.getLong("challenge_start_time_extra") : new Date().getTime();
            Bundle arguments3 = getArguments();
            this.p = arguments3 != null ? arguments3.getInt("challenge_target_extra") : -1;
            Bundle arguments4 = getArguments();
            if (arguments4 != null) {
                str = arguments4.getString("challenge_status_extra");
            }
            this.q = str;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        e35 e35 = (e35) qb.a(layoutInflater, 2131558580, viewGroup, false, a1());
        this.g = new qw6<>(this, e35);
        ee7.a((Object) e35, "binding");
        return e35.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qd5 c2 = qd5.f.c();
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c2.a("bc_full_leader_board", activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        gq4 gq4 = this.h;
        if (gq4 != null) {
            gq4.a(this.i, this.q);
            f1();
            g1();
            return;
        }
        ee7.d("viewModel");
        throw null;
    }
}
