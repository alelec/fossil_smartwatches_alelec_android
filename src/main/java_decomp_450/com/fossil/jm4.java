package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm4 {
    @DexIgnore
    public /* final */ hm4 a;

    @DexIgnore
    public jm4(hm4 hm4) {
        ee7.b(hm4, "dao");
        this.a = hm4;
    }

    @DexIgnore
    public final Long[] a(List<nm4> list) {
        ee7.b(list, "flag");
        return this.a.a(list);
    }

    @DexIgnore
    public final nm4 a(String str) {
        ee7.b(str, "id");
        return this.a.a(str);
    }
}
