package com.fossil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class us2<K, V> extends bs2<K, V> implements Serializable {
    @DexIgnore
    public /* final */ transient ss2<K, ? extends ps2<V>> a;

    @DexIgnore
    public us2(ss2<K, ? extends ps2<V>> ss2, int i) {
        this.a = ss2;
    }

    @DexIgnore
    @Override // com.fossil.yr2
    public /* bridge */ /* synthetic */ boolean equals(@NullableDecl Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.yr2
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.yr2
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    @Override // com.fossil.yr2
    public final boolean zza(@NullableDecl Object obj) {
        return obj != null && super.zza(obj);
    }

    @DexIgnore
    @Override // com.fossil.yr2
    public final Map<K, Collection<V>> zzb() {
        throw new AssertionError("should never be called");
    }

    @DexIgnore
    @Override // com.fossil.ft2, com.fossil.yr2
    public final /* synthetic */ Map zza() {
        return this.a;
    }
}
