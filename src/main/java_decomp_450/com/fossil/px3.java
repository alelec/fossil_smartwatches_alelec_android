package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class px3 {
    @DexIgnore
    public abstract Object delegate();

    @DexIgnore
    public String toString() {
        return delegate().toString();
    }
}
