package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vn0 implements Parcelable.Creator<rp0> {
    @DexIgnore
    public /* synthetic */ vn0(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public rp0 createFromParcel(Parcel parcel) {
        return new rp0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public rp0[] newArray(int i) {
        return new rp0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public rp0 m81createFromParcel(Parcel parcel) {
        return new rp0(parcel, null);
    }
}
