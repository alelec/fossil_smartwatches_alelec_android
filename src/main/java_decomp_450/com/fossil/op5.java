package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class op5 extends rf<GoalTrackingSummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar c; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ PortfolioApp k;
    @DexIgnore
    public /* final */ rp5 l;
    @DexIgnore
    public /* final */ FragmentManager m;
    @DexIgnore
    public /* final */ go5 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4) {
            ee7.b(str, "mDayOfWeek");
            ee7.b(str2, "mDayOfMonth");
            ee7.b(str3, "mDailyValue");
            ee7.b(str4, "mDailyUnit");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final Date c() {
            return this.a;
        }

        @DexIgnore
        public final String d() {
            return this.e;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.c;
        }

        @DexIgnore
        public final boolean g() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? false : z, (i & 4) == 0 ? z2 : false, (i & 8) != 0 ? "" : str, (i & 16) != 0 ? "" : str2, (i & 32) != 0 ? "" : str3, (i & 64) != 0 ? "" : str4);
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.f;
        }

        @DexIgnore
        public final void c(String str) {
            ee7.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void d(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.g;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.g = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ i95 b;
        @DexIgnore
        public /* final */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ op5 d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.d.l.a(a2);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(op5 op5, i95 i95, View view) {
            super(view);
            ee7.b(i95, "binding");
            ee7.b(view, "root");
            this.d = op5;
            this.b = i95;
            this.c = view;
            i95.d().setOnClickListener(new a(this));
            this.b.r.setTextColor(op5.f);
        }

        @DexIgnore
        public void a(GoalTrackingSummary goalTrackingSummary) {
            b a2 = this.d.a(goalTrackingSummary);
            this.a = a2.c();
            FlexibleTextView flexibleTextView = this.b.u;
            ee7.a((Object) flexibleTextView, "binding.ftvDayOfWeek");
            flexibleTextView.setText(a2.e());
            FlexibleTextView flexibleTextView2 = this.b.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvDayOfMonth");
            flexibleTextView2.setText(a2.d());
            FlexibleTextView flexibleTextView3 = this.b.s;
            ee7.a((Object) flexibleTextView3, "binding.ftvDailyValue");
            flexibleTextView3.setText(a2.b());
            FlexibleTextView flexibleTextView4 = this.b.r;
            ee7.a((Object) flexibleTextView4, "binding.ftvDailyUnit");
            flexibleTextView4.setText(a2.a());
            ConstraintLayout constraintLayout = this.b.q;
            ee7.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.f());
            FlexibleTextView flexibleTextView5 = this.b.u;
            ee7.a((Object) flexibleTextView5, "binding.ftvDayOfWeek");
            flexibleTextView5.setSelected(a2.g());
            FlexibleTextView flexibleTextView6 = this.b.t;
            ee7.a((Object) flexibleTextView6, "binding.ftvDayOfMonth");
            flexibleTextView6.setSelected(a2.g());
            if (a2.g()) {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.g);
                this.b.t.setBackgroundColor(this.d.g);
                this.b.u.setTextColor(this.d.j);
                this.b.t.setTextColor(this.d.j);
            } else if (a2.f()) {
                this.b.q.setBackgroundColor(this.d.e);
                this.b.u.setBackgroundColor(this.d.e);
                this.b.t.setBackgroundColor(this.d.e);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            } else {
                this.b.q.setBackgroundColor(this.d.i);
                this.b.u.setBackgroundColor(this.d.i);
                this.b.t.setBackgroundColor(this.d.i);
                this.b.u.setTextColor(this.d.h);
                this.b.t.setTextColor(this.d.d);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            ee7.b(str, "mWeekly");
            ee7.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            ee7.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            ee7.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date e;
        @DexIgnore
        public Date f;
        @DexIgnore
        public /* final */ k95 g;
        @DexIgnore
        public /* final */ /* synthetic */ op5 h;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.e != null && this.a.f != null) {
                    rp5 e = this.a.h.l;
                    Date b = this.a.e;
                    if (b != null) {
                        Date a2 = this.a.f;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public e(com.fossil.op5 r4, com.fossil.k95 r5) {
            /*
                r3 = this;
                java.lang.String r0 = "binding"
                com.fossil.ee7.b(r5, r0)
                r3.h = r4
                com.fossil.i95 r0 = r5.r
                if (r0 == 0) goto L_0x0029
                java.lang.String r1 = "binding.dailyItem!!"
                com.fossil.ee7.a(r0, r1)
                android.view.View r1 = r5.d()
                java.lang.String r2 = "binding.root"
                com.fossil.ee7.a(r1, r2)
                r3.<init>(r4, r0, r1)
                r3.g = r5
                androidx.constraintlayout.widget.ConstraintLayout r4 = r5.q
                com.fossil.op5$e$a r5 = new com.fossil.op5$e$a
                r5.<init>(r3)
                r4.setOnClickListener(r5)
                return
            L_0x0029:
                com.fossil.ee7.a()
                r4 = 0
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.op5.e.<init>(com.fossil.op5, com.fossil.k95):void");
        }

        @DexIgnore
        @Override // com.fossil.op5.c
        public void a(GoalTrackingSummary goalTrackingSummary) {
            d b = this.h.b(goalTrackingSummary);
            this.f = b.a();
            this.e = b.b();
            FlexibleTextView flexibleTextView = this.g.s;
            ee7.a((Object) flexibleTextView, "binding.ftvWeekly");
            flexibleTextView.setText(b.c());
            FlexibleTextView flexibleTextView2 = this.g.t;
            ee7.a((Object) flexibleTextView2, "binding.ftvWeeklyValue");
            flexibleTextView2.setText(b.d());
            super.a(goalTrackingSummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ op5 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(op5 op5, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = op5;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            ee7.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.n.getId() + ", isAdded=" + this.a.n.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.m.b(this.a.n.d1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                nc b3 = this.a.m.b();
                b3.a(view.getId(), this.a.n, this.a.n.d1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                nc b4 = this.a.m.b();
                b4.d(b2);
                b4.d();
                nc b5 = this.a.m.b();
                b5.a(view.getId(), this.a.n, this.a.n.d1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardGoalTrackingAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.n.getId() + ", isAdded2=" + this.a.n.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            ee7.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ /* synthetic */ FrameLayout a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(FrameLayout frameLayout, View view) {
            super(view);
            this.a = frameLayout;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public op5(qp5 qp5, PortfolioApp portfolioApp, rp5 rp5, FragmentManager fragmentManager, go5 go5) {
        super(qp5);
        ee7.b(qp5, "difference");
        ee7.b(portfolioApp, "mApp");
        ee7.b(rp5, "mOnItemClick");
        ee7.b(fragmentManager, "mFragmentManager");
        ee7.b(go5, "mFragment");
        this.k = portfolioApp;
        this.l = rp5;
        this.m = fragmentManager;
        this.n = go5;
        String b2 = eh5.l.a().b("primaryText");
        String str = "#FFFFFF";
        this.d = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("nonBrandSurface");
        this.e = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("nonBrandNonReachGoal");
        this.f = Color.parseColor(b4 == null ? str : b4);
        String b5 = eh5.l.a().b("hybridGoalTrackingTab");
        this.g = Color.parseColor(b5 == null ? str : b5);
        String b6 = eh5.l.a().b("secondaryText");
        this.h = Color.parseColor(b6 == null ? str : b6);
        String b7 = eh5.l.a().b("nonBrandActivityDetailBackground");
        this.i = Color.parseColor(b7 == null ? str : b7);
        String b8 = eh5.l.a().b("onHybridGoalTrackingTab");
        this.j = Color.parseColor(b8 != null ? b8 : str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return super.getItemId(i2);
        }
        if (this.n.getId() == 0) {
            return 1010101;
        }
        return (long) this.n.getId();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) getItem(i2);
        if (goalTrackingSummary == null) {
            return 1;
        }
        Calendar calendar = this.c;
        ee7.a((Object) calendar, "mCalendar");
        calendar.setTime(goalTrackingSummary.getDate());
        Calendar calendar2 = this.c;
        ee7.a((Object) calendar2, "mCalendar");
        Boolean w = zd5.w(calendar2.getTime());
        ee7.a((Object) w, "DateHelper.isToday(mCalendar.time)");
        if (w.booleanValue() || this.c.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        ee7.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardGoalTrackingAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            ee7.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            ee7.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardGoalTrackingAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            ee7.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((GoalTrackingSummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((GoalTrackingSummary) getItem(i2));
        } else {
            ((e) viewHolder).a((GoalTrackingSummary) getItem(i2));
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        ee7.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            i95 a2 = i95.a(from, viewGroup, false);
            ee7.a((Object) a2, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View d2 = a2.d();
            ee7.a((Object) d2, "itemGoalTrackingDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            i95 a3 = i95.a(from, viewGroup, false);
            ee7.a((Object) a3, "ItemGoalTrackingDayBindi\u2026tInflater, parent, false)");
            View d3 = a3.d();
            ee7.a((Object) d3, "itemGoalTrackingDayBinding.root");
            return new c(this, a3, d3);
        } else {
            k95 a4 = k95.a(from, viewGroup, false);
            ee7.a((Object) a4, "ItemGoalTrackingWeekBind\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(qf<GoalTrackingSummary> qfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
        local.d("DashboardGoalTrackingAdapter", sb.toString());
        super.b(qfVar);
    }

    @DexIgnore
    public final b a(GoalTrackingSummary goalTrackingSummary) {
        b bVar = new b(null, false, false, null, null, null, null, 127, null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            int i2 = instance.get(7);
            bVar.a(instance.getTime());
            bVar.c(String.valueOf(instance.get(5)));
            Boolean w = zd5.w(instance.getTime());
            ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
            if (w.booleanValue()) {
                String a2 = ig5.a(this.k, 2131886707);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026rackingToday_Text__Today)");
                bVar.d(a2);
            } else {
                bVar.d(xe5.b.b(i2));
            }
            bVar.b(String.valueOf(goalTrackingSummary.getTotalTracked()));
            bVar.a("/ " + goalTrackingSummary.getGoalTarget() + " " + ig5.a(this.k, 2131886706));
            boolean z = false;
            if (goalTrackingSummary.getGoalTarget() > 0) {
                if (goalTrackingSummary.getTotalTracked() >= goalTrackingSummary.getGoalTarget()) {
                    z = true;
                }
                bVar.b(z);
            } else {
                bVar.b(false);
            }
            if (goalTrackingSummary.getTotalTracked() <= 0) {
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    public final d b(GoalTrackingSummary goalTrackingSummary) {
        String str;
        d dVar = new d(null, null, null, null, 15, null);
        if (goalTrackingSummary != null) {
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "calendar");
            instance.setTime(goalTrackingSummary.getDate());
            Boolean w = zd5.w(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String c2 = zd5.c(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String c3 = zd5.c(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            ee7.a((Object) w, "isToday");
            if (w.booleanValue()) {
                str = ig5.a(this.k, 2131886709);
                ee7.a((Object) str, "LanguageHelper.getString\u2026ingToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = c3 + ' ' + i5 + " - " + c3 + ' ' + i2;
            } else if (i7 == i4) {
                str = c3 + ' ' + i5 + " - " + c2 + ' ' + i2;
            } else {
                str = c3 + ' ' + i5 + ", " + i7 + " - " + c2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            if (goalTrackingSummary.getTotalTargetOfWeek() > 0) {
                int a2 = af7.a((((float) goalTrackingSummary.getTotalValueOfWeek()) / ((float) goalTrackingSummary.getTotalTargetOfWeek())) * ((float) 100));
                StringBuilder sb = new StringBuilder();
                sb.append(a2);
                sb.append('%');
                dVar.b(sb.toString());
            }
        }
        return dVar;
    }
}
