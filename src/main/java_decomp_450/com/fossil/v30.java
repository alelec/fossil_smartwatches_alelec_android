package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v30 {
    @DexIgnore
    public /* final */ Set<n40> a; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ List<n40> b; // = new ArrayList();
    @DexIgnore
    public boolean c;

    @DexIgnore
    public boolean a(n40 n40) {
        boolean z = true;
        if (n40 == null) {
            return true;
        }
        boolean remove = this.a.remove(n40);
        if (!this.b.remove(n40) && !remove) {
            z = false;
        }
        if (z) {
            n40.clear();
        }
        return z;
    }

    @DexIgnore
    public void b(n40 n40) {
        this.a.add(n40);
        if (!this.c) {
            n40.c();
            return;
        }
        n40.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.b.add(n40);
    }

    @DexIgnore
    public void c() {
        this.c = true;
        for (n40 n40 : v50.a(this.a)) {
            if (n40.isRunning()) {
                n40.d();
                this.b.add(n40);
            }
        }
    }

    @DexIgnore
    public void d() {
        for (n40 n40 : v50.a(this.a)) {
            if (!n40.f() && !n40.e()) {
                n40.clear();
                if (!this.c) {
                    n40.c();
                } else {
                    this.b.add(n40);
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        this.c = false;
        for (n40 n40 : v50.a(this.a)) {
            if (!n40.f() && !n40.isRunning()) {
                n40.c();
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "{numRequests=" + this.a.size() + ", isPaused=" + this.c + "}";
    }

    @DexIgnore
    public void a() {
        for (n40 n40 : v50.a(this.a)) {
            a(n40);
        }
        this.b.clear();
    }

    @DexIgnore
    public void b() {
        this.c = true;
        for (n40 n40 : v50.a(this.a)) {
            if (n40.isRunning() || n40.f()) {
                n40.clear();
                this.b.add(n40);
            }
        }
    }
}
