package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zu {
    @DexIgnore
    public /* final */ AtomicInteger a;
    @DexIgnore
    public /* final */ Set<yu<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<yu<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<yu<?>> d;
    @DexIgnore
    public /* final */ mu e;
    @DexIgnore
    public /* final */ su f;
    @DexIgnore
    public /* final */ bv g;
    @DexIgnore
    public /* final */ tu[] h;
    @DexIgnore
    public nu i;
    @DexIgnore
    public /* final */ List<a> j;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(yu<T> yuVar);
    }

    @DexIgnore
    public zu(mu muVar, su suVar, int i2, bv bvVar) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = muVar;
        this.f = suVar;
        this.h = new tu[i2];
        this.g = bvVar;
    }

    @DexIgnore
    public int a() {
        return this.a.incrementAndGet();
    }

    @DexIgnore
    public void b() {
        c();
        nu nuVar = new nu(this.c, this.d, this.e, this.g);
        this.i = nuVar;
        nuVar.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            tu tuVar = new tu(this.d, this.f, this.e, this.g);
            this.h[i2] = tuVar;
            tuVar.start();
        }
    }

    @DexIgnore
    public void c() {
        nu nuVar = this.i;
        if (nuVar != null) {
            nuVar.b();
        }
        tu[] tuVarArr = this.h;
        for (tu tuVar : tuVarArr) {
            if (tuVar != null) {
                tuVar.b();
            }
        }
    }

    @DexIgnore
    public <T> yu<T> a(yu<T> yuVar) {
        yuVar.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(yuVar);
        }
        yuVar.setSequence(a());
        yuVar.addMarker("add-to-queue");
        if (!yuVar.shouldCache()) {
            this.d.add(yuVar);
            return yuVar;
        }
        this.c.add(yuVar);
        return yuVar;
    }

    @DexIgnore
    public <T> void b(yu<T> yuVar) {
        synchronized (this.b) {
            this.b.remove(yuVar);
        }
        synchronized (this.j) {
            for (a aVar : this.j) {
                aVar.a(yuVar);
            }
        }
    }

    @DexIgnore
    public zu(mu muVar, su suVar, int i2) {
        this(muVar, suVar, i2, new qu(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public zu(mu muVar, su suVar) {
        this(muVar, suVar, 4);
    }
}
