package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o82 implements Parcelable.Creator<m62> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ m62 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        IBinder iBinder = null;
        Scope[] scopeArr = null;
        Bundle bundle = null;
        Account account = null;
        k02[] k02Arr = null;
        k02[] k02Arr2 = null;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        boolean z = false;
        int i4 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 1:
                    i = j72.q(parcel, a);
                    break;
                case 2:
                    i2 = j72.q(parcel, a);
                    break;
                case 3:
                    i3 = j72.q(parcel, a);
                    break;
                case 4:
                    str = j72.e(parcel, a);
                    break;
                case 5:
                    iBinder = j72.p(parcel, a);
                    break;
                case 6:
                    scopeArr = (Scope[]) j72.b(parcel, a, Scope.CREATOR);
                    break;
                case 7:
                    bundle = j72.a(parcel, a);
                    break;
                case 8:
                    account = (Account) j72.a(parcel, a, Account.CREATOR);
                    break;
                case 9:
                default:
                    j72.v(parcel, a);
                    break;
                case 10:
                    k02Arr = (k02[]) j72.b(parcel, a, k02.CREATOR);
                    break;
                case 11:
                    k02Arr2 = (k02[]) j72.b(parcel, a, k02.CREATOR);
                    break;
                case 12:
                    z = j72.i(parcel, a);
                    break;
                case 13:
                    i4 = j72.q(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new m62(i, i2, i3, str, iBinder, scopeArr, bundle, account, k02Arr, k02Arr2, z, i4);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ m62[] newArray(int i) {
        return new m62[i];
    }
}
