package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kz3<E> extends Iterator<E> {
    @DexIgnore
    @Override // java.util.Iterator
    @CanIgnoreReturnValue
    E next();

    @DexIgnore
    E peek();
}
