package com.fossil;

import android.location.Location;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol2 extends el2 implements nl2 {
    @DexIgnore
    public ol2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.internal.IGoogleLocationManagerService");
    }

    @DexIgnore
    @Override // com.fossil.nl2
    public final void a(dm2 dm2) throws RemoteException {
        Parcel E = E();
        jm2.a(E, dm2);
        b(59, E);
    }

    @DexIgnore
    @Override // com.fossil.nl2
    public final void a(f53 f53, pl2 pl2, String str) throws RemoteException {
        Parcel E = E();
        jm2.a(E, f53);
        jm2.a(E, pl2);
        E.writeString(str);
        b(63, E);
    }

    @DexIgnore
    @Override // com.fossil.nl2
    public final void a(om2 om2) throws RemoteException {
        Parcel E = E();
        jm2.a(E, om2);
        b(75, E);
    }

    @DexIgnore
    @Override // com.fossil.nl2
    public final void e(boolean z) throws RemoteException {
        Parcel E = E();
        jm2.a(E, z);
        b(12, E);
    }

    @DexIgnore
    @Override // com.fossil.nl2
    public final Location zza(String str) throws RemoteException {
        Parcel E = E();
        E.writeString(str);
        Parcel a = a(21, E);
        Location location = (Location) jm2.a(a, Location.CREATOR);
        a.recycle();
        return location;
    }
}
