package com.fossil;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f04 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> extends c implements Collection<E> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        @Override // java.util.Collection
        public boolean add(E e) {
            boolean add;
            synchronized (((c) this).mutex) {
                add = delegate().add(e);
            }
            return add;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean addAll(Collection<? extends E> collection) {
            boolean addAll;
            synchronized (((c) this).mutex) {
                addAll = delegate().addAll(collection);
            }
            return addAll;
        }

        @DexIgnore
        public void clear() {
            synchronized (((c) this).mutex) {
                delegate().clear();
            }
        }

        @DexIgnore
        public boolean contains(Object obj) {
            boolean contains;
            synchronized (((c) this).mutex) {
                contains = delegate().contains(obj);
            }
            return contains;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            boolean containsAll;
            synchronized (((c) this).mutex) {
                containsAll = delegate().containsAll(collection);
            }
            return containsAll;
        }

        @DexIgnore
        public boolean isEmpty() {
            boolean isEmpty;
            synchronized (((c) this).mutex) {
                isEmpty = delegate().isEmpty();
            }
            return isEmpty;
        }

        @DexIgnore
        @Override // java.util.Collection, java.lang.Iterable
        public Iterator<E> iterator() {
            return delegate().iterator();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            boolean remove;
            synchronized (((c) this).mutex) {
                remove = delegate().remove(obj);
            }
            return remove;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            boolean removeAll;
            synchronized (((c) this).mutex) {
                removeAll = delegate().removeAll(collection);
            }
            return removeAll;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            boolean retainAll;
            synchronized (((c) this).mutex) {
                retainAll = delegate().retainAll(collection);
            }
            return retainAll;
        }

        @DexIgnore
        public int size() {
            int size;
            synchronized (((c) this).mutex) {
                size = delegate().size();
            }
            return size;
        }

        @DexIgnore
        public Object[] toArray() {
            Object[] array;
            synchronized (((c) this).mutex) {
                array = delegate().toArray();
            }
            return array;
        }

        @DexIgnore
        public b(Collection<E> collection, Object obj) {
            super(collection, obj);
        }

        @DexIgnore
        @Override // com.fossil.f04.c
        public Collection<E> delegate() {
            return (Collection) super.delegate();
        }

        @DexIgnore
        @Override // java.util.Collection
        public <T> T[] toArray(T[] tArr) {
            T[] tArr2;
            synchronized (((c) this).mutex) {
                tArr2 = (T[]) delegate().toArray(tArr);
            }
            return tArr2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object delegate;
        @DexIgnore
        public /* final */ Object mutex;

        @DexIgnore
        public c(Object obj, Object obj2) {
            jw3.a(obj);
            this.delegate = obj;
            this.mutex = obj2 == null ? this : obj2;
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            synchronized (this.mutex) {
                objectOutputStream.defaultWriteObject();
            }
        }

        @DexIgnore
        public Object delegate() {
            return this.delegate;
        }

        @DexIgnore
        public String toString() {
            String obj;
            synchronized (this.mutex) {
                obj = this.delegate.toString();
            }
            return obj;
        }
    }

    @DexIgnore
    public static <E> Queue<E> a(Queue<E> queue, Object obj) {
        return queue instanceof d ? queue : new d(queue, obj);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<E> extends b<E> implements Queue<E> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;

        @DexIgnore
        public d(Queue<E> queue, Object obj) {
            super(queue, obj);
        }

        @DexIgnore
        @Override // java.util.Queue
        public E element() {
            E element;
            synchronized (((c) this).mutex) {
                element = delegate().element();
            }
            return element;
        }

        @DexIgnore
        @Override // java.util.Queue
        public boolean offer(E e) {
            boolean offer;
            synchronized (((c) this).mutex) {
                offer = delegate().offer(e);
            }
            return offer;
        }

        @DexIgnore
        @Override // java.util.Queue
        public E peek() {
            E peek;
            synchronized (((c) this).mutex) {
                peek = delegate().peek();
            }
            return peek;
        }

        @DexIgnore
        @Override // java.util.Queue
        public E poll() {
            E poll;
            synchronized (((c) this).mutex) {
                poll = delegate().poll();
            }
            return poll;
        }

        @DexIgnore
        @Override // java.util.Queue
        public E remove() {
            E remove;
            synchronized (((c) this).mutex) {
                remove = delegate().remove();
            }
            return remove;
        }

        @DexIgnore
        @Override // com.fossil.f04.b, com.fossil.f04.b, com.fossil.f04.c
        public Queue<E> delegate() {
            return (Queue) super.delegate();
        }
    }
}
