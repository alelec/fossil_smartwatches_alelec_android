package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j97 {
    @DexIgnore
    public static final String a(long j) {
        return a(j, 10);
    }

    @DexIgnore
    public static final String a(long j, int i) {
        if (j >= 0) {
            qg7.a(i);
            String l = Long.toString(j, i);
            ee7.a((Object) l, "java.lang.Long.toString(this, checkRadix(radix))");
            return l;
        }
        long j2 = (long) i;
        long j3 = ((j >>> 1) / j2) << 1;
        long j4 = j - (j3 * j2);
        if (j4 >= j2) {
            j4 -= j2;
            j3++;
        }
        StringBuilder sb = new StringBuilder();
        qg7.a(i);
        String l2 = Long.toString(j3, i);
        ee7.a((Object) l2, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l2);
        qg7.a(i);
        String l3 = Long.toString(j4, i);
        ee7.a((Object) l3, "java.lang.Long.toString(this, checkRadix(radix))");
        sb.append(l3);
        return sb.toString();
    }
}
