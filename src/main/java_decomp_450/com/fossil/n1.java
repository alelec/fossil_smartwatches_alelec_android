package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.view.menu.ExpandedMenuView;
import com.fossil.v1;
import com.fossil.w1;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n1 implements v1, AdapterView.OnItemClickListener {
    @DexIgnore
    public Context a;
    @DexIgnore
    public LayoutInflater b;
    @DexIgnore
    public p1 c;
    @DexIgnore
    public ExpandedMenuView d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public v1.a h;
    @DexIgnore
    public a i;
    @DexIgnore
    public int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BaseAdapter {
        @DexIgnore
        public int a; // = -1;

        @DexIgnore
        public a() {
            a();
        }

        @DexIgnore
        public void a() {
            r1 f = n1.this.c.f();
            if (f != null) {
                ArrayList<r1> j = n1.this.c.j();
                int size = j.size();
                for (int i = 0; i < size; i++) {
                    if (j.get(i) == f) {
                        this.a = i;
                        return;
                    }
                }
            }
            this.a = -1;
        }

        @DexIgnore
        public int getCount() {
            int size = n1.this.c.j().size() - n1.this.e;
            return this.a < 0 ? size : size - 1;
        }

        @DexIgnore
        public long getItemId(int i) {
            return (long) i;
        }

        @DexIgnore
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                n1 n1Var = n1.this;
                view = n1Var.b.inflate(n1Var.g, viewGroup, false);
            }
            ((w1.a) view).a(getItem(i), 0);
            return view;
        }

        @DexIgnore
        public void notifyDataSetChanged() {
            a();
            super.notifyDataSetChanged();
        }

        @DexIgnore
        public r1 getItem(int i) {
            ArrayList<r1> j = n1.this.c.j();
            int i2 = i + n1.this.e;
            int i3 = this.a;
            if (i3 >= 0 && i2 >= i3) {
                i2++;
            }
            return j.get(i2);
        }
    }

    @DexIgnore
    public n1(Context context, int i2) {
        this(i2, 0);
        this.a = context;
        this.b = LayoutInflater.from(context);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Context context, p1 p1Var) {
        if (this.f != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, this.f);
            this.a = contextThemeWrapper;
            this.b = LayoutInflater.from(contextThemeWrapper);
        } else if (this.a != null) {
            this.a = context;
            if (this.b == null) {
                this.b = LayoutInflater.from(context);
            }
        }
        this.c = p1Var;
        a aVar = this.i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    public void b(Bundle bundle) {
        SparseArray<Parcelable> sparseArray = new SparseArray<>();
        ExpandedMenuView expandedMenuView = this.d;
        if (expandedMenuView != null) {
            expandedMenuView.saveHierarchyState(sparseArray);
        }
        bundle.putSparseParcelableArray("android:menu:list", sparseArray);
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean b(p1 p1Var, r1 r1Var) {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public Parcelable c() {
        if (this.d == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        b(bundle);
        return bundle;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public int getId() {
        return this.j;
    }

    @DexIgnore
    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        this.c.a(this.i.getItem(i2), this, 0);
    }

    @DexIgnore
    public n1(int i2, int i3) {
        this.g = i2;
        this.f = i3;
    }

    @DexIgnore
    public w1 a(ViewGroup viewGroup) {
        if (this.d == null) {
            this.d = (ExpandedMenuView) this.b.inflate(e0.abc_expanded_menu_layout, viewGroup, false);
            if (this.i == null) {
                this.i = new a();
            }
            this.d.setAdapter((ListAdapter) this.i);
            this.d.setOnItemClickListener(this);
        }
        return this.d;
    }

    @DexIgnore
    public ListAdapter a() {
        if (this.i == null) {
            this.i = new a();
        }
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(boolean z) {
        a aVar = this.i;
        if (aVar != null) {
            aVar.notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(v1.a aVar) {
        this.h = aVar;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public boolean a(a2 a2Var) {
        if (!a2Var.hasVisibleItems()) {
            return false;
        }
        new q1(a2Var).a((IBinder) null);
        v1.a aVar = this.h;
        if (aVar == null) {
            return true;
        }
        aVar.a(a2Var);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(p1 p1Var, boolean z) {
        v1.a aVar = this.h;
        if (aVar != null) {
            aVar.a(p1Var, z);
        }
    }

    @DexIgnore
    public void a(Bundle bundle) {
        SparseArray<Parcelable> sparseParcelableArray = bundle.getSparseParcelableArray("android:menu:list");
        if (sparseParcelableArray != null) {
            this.d.restoreHierarchyState(sparseParcelableArray);
        }
    }

    @DexIgnore
    @Override // com.fossil.v1
    public void a(Parcelable parcelable) {
        a((Bundle) parcelable);
    }
}
