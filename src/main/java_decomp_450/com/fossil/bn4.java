package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.cn4;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn4 extends dy6 implements View.OnClickListener {
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public List<dn4> j; // = w97.a();
    @DexIgnore
    public cn4 p;
    @DexIgnore
    public /* final */ pb q; // = new pm4(this);
    @DexIgnore
    public qw6<gw4> r;
    @DexIgnore
    public er5 s;
    @DexIgnore
    public dn4 t;
    @DexIgnore
    public String u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bn4.y;
        }

        @DexIgnore
        public final bn4 b() {
            return new bn4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ bn4 a;

        @DexIgnore
        public b(bn4 bn4) {
            this.a = bn4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ bn4 a;

        @DexIgnore
        public c(bn4 bn4) {
            this.a = bn4;
        }

        @DexIgnore
        public final void onClick(View view) {
            er5 b;
            dn4 a2 = this.a.t;
            if (!(a2 == null || (b = this.a.s) == null)) {
                b.a(a2);
            }
            this.a.dismiss();
        }
    }

    /*
    static {
        String name = bn4.class.getName();
        ee7.a((Object) name, "BottomDialog::class.java.name");
        y = name;
    }
    */

    @DexIgnore
    public final void U(boolean z2) {
        this.v = z2;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void b1() {
        this.p = new cn4(this.j, this);
        qw6<gw4> qw6 = this.r;
        if (qw6 != null) {
            gw4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.s;
                ee7.a((Object) flexibleTextView, "ftvTitle");
                flexibleTextView.setText(this.u);
                RecyclerView recyclerView = a2.u;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                ee7.a((Object) recyclerView, "this");
                cn4 cn4 = this.p;
                if (cn4 != null) {
                    recyclerView.setAdapter(cn4);
                    recyclerView.setHasFixedSize(true);
                    a2.t.setOnClickListener(new b(this));
                    if (this.v) {
                        FlexibleButton flexibleButton = a2.q;
                        ee7.a((Object) flexibleButton, "btnOk");
                        flexibleButton.setVisibility(0);
                        RTLImageView rTLImageView = a2.t;
                        ee7.a((Object) rTLImageView, "ivClose");
                        rTLImageView.setVisibility(8);
                    } else {
                        FlexibleButton flexibleButton2 = a2.q;
                        ee7.a((Object) flexibleButton2, "btnOk");
                        flexibleButton2.setVisibility(8);
                        RTLImageView rTLImageView2 = a2.t;
                        ee7.a((Object) rTLImageView2, "ivClose");
                        rTLImageView2.setVisibility(0);
                    }
                    a2.q.setOnClickListener(new c(this));
                    return;
                }
                ee7.d("bottomDialogAdapter");
                throw null;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        b1();
    }

    @DexIgnore
    public void onClick(View view) {
        Object tag = view != null ? view.getTag() : null;
        if (tag != null) {
            cn4.a aVar = (cn4.a) tag;
            dn4 dn4 = this.j.get(aVar.getAdapterPosition());
            this.t = dn4;
            if (!this.w) {
                Iterator<dn4> it = this.j.iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i = -1;
                        break;
                    } else if (it.next().c()) {
                        break;
                    } else {
                        i++;
                    }
                }
                int adapterPosition = aVar.getAdapterPosition();
                if (!(i == -1 || i == adapterPosition)) {
                    this.j.get(i).a(false);
                    this.j.get(adapterPosition).a(true);
                    cn4 cn4 = this.p;
                    if (cn4 != null) {
                        cn4.notifyDataSetChanged();
                    } else {
                        ee7.d("bottomDialogAdapter");
                        throw null;
                    }
                }
            }
            if (!this.v) {
                er5 er5 = this.s;
                if (er5 != null) {
                    er5.a(dn4);
                }
                dismiss();
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.customview.BottomDialogAdapter.PrivacyHolder");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        gw4 gw4 = (gw4) qb.a(layoutInflater, 2131558443, viewGroup, false, this.q);
        this.r = new qw6<>(this, gw4);
        ee7.a((Object) gw4, "binding");
        return gw4.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "title");
        this.u = str;
    }

    @DexIgnore
    public final bn4 x(List<dn4> list) {
        ee7.b(list, "newData");
        Iterator<dn4> it = list.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (it.next().c()) {
                break;
            } else {
                i++;
            }
        }
        if (i != -1) {
            this.t = list.get(i);
        }
        this.j = list;
        return this;
    }

    @DexIgnore
    public final void a(er5 er5) {
        ee7.b(er5, "listener");
        this.s = er5;
    }
}
