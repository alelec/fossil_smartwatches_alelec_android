package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e76 implements Factory<d76> {
    @DexIgnore
    public static d76 a(c76 c76, PortfolioApp portfolioApp, DeviceRepository deviceRepository, ad5 ad5, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, ch5 ch5, HeartRateSampleRepository heartRateSampleRepository, ro4 ro4) {
        return new d76(c76, portfolioApp, deviceRepository, ad5, summariesRepository, goalTrackingRepository, sleepSummariesRepository, ch5, heartRateSampleRepository, ro4);
    }
}
