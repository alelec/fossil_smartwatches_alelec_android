package com.fossil;

import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class my implements yw {
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ Class<?> e;
    @DexIgnore
    public /* final */ Class<?> f;
    @DexIgnore
    public /* final */ yw g;
    @DexIgnore
    public /* final */ Map<Class<?>, ex<?>> h;
    @DexIgnore
    public /* final */ ax i;
    @DexIgnore
    public int j;

    @DexIgnore
    public my(Object obj, yw ywVar, int i2, int i3, Map<Class<?>, ex<?>> map, Class<?> cls, Class<?> cls2, ax axVar) {
        u50.a(obj);
        this.b = obj;
        u50.a(ywVar, "Signature must not be null");
        this.g = ywVar;
        this.c = i2;
        this.d = i3;
        u50.a(map);
        this.h = map;
        u50.a(cls, "Resource class must not be null");
        this.e = cls;
        u50.a(cls2, "Transcode class must not be null");
        this.f = cls2;
        u50.a(axVar);
        this.i = axVar;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (!(obj instanceof my)) {
            return false;
        }
        my myVar = (my) obj;
        if (!this.b.equals(myVar.b) || !this.g.equals(myVar.g) || this.d != myVar.d || this.c != myVar.c || !this.h.equals(myVar.h) || !this.e.equals(myVar.e) || !this.f.equals(myVar.f) || !this.i.equals(myVar.i)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        if (this.j == 0) {
            int hashCode = this.b.hashCode();
            this.j = hashCode;
            int hashCode2 = (hashCode * 31) + this.g.hashCode();
            this.j = hashCode2;
            int i2 = (hashCode2 * 31) + this.c;
            this.j = i2;
            int i3 = (i2 * 31) + this.d;
            this.j = i3;
            int hashCode3 = (i3 * 31) + this.h.hashCode();
            this.j = hashCode3;
            int hashCode4 = (hashCode3 * 31) + this.e.hashCode();
            this.j = hashCode4;
            int hashCode5 = (hashCode4 * 31) + this.f.hashCode();
            this.j = hashCode5;
            this.j = (hashCode5 * 31) + this.i.hashCode();
        }
        return this.j;
    }

    @DexIgnore
    public String toString() {
        return "EngineKey{model=" + this.b + ", width=" + this.c + ", height=" + this.d + ", resourceClass=" + this.e + ", transcodeClass=" + this.f + ", signature=" + this.g + ", hashCode=" + this.j + ", transformations=" + this.h + ", options=" + this.i + '}';
    }
}
