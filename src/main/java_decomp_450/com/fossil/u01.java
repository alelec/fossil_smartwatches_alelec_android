package com.fossil;

import java.util.ArrayList;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u01 {
    @DexIgnore
    public /* final */ xc1<zk0> a; // = new xc1<>(cs0.a);
    @DexIgnore
    public /* final */ xc1<zk0> b; // = new xc1<>(yt0.a);
    @DexIgnore
    public Hashtable<zk0, jh1> c; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public wj0 e;

    @DexIgnore
    public u01(wj0 wj0) {
        this.e = wj0;
    }

    @DexIgnore
    public final void a(wj0 wj0) {
        synchronized (this.d) {
            this.e = wj0;
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final zk0[] b() {
        zk0[] zk0Arr;
        synchronized (this.d) {
            zk0[] array = this.a.toArray(new zk0[0]);
            if (array != null) {
                zk0Arr = array;
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return zk0Arr;
    }

    @DexIgnore
    public final zk0[] c() {
        zk0[] zk0Arr;
        synchronized (this.d) {
            zk0[] array = this.b.toArray(new zk0[0]);
            if (array != null) {
                zk0Arr = array;
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return zk0Arr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0079, code lost:
        if (r18.e.a(r2) != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a0, code lost:
        if (r18.e.a(r2) != false) goto L_0x00a2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0008 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void d() {
        /*
            r18 = this;
            r0 = r18
            com.fossil.xc1<com.fossil.zk0> r1 = r0.a
            java.util.Iterator r1 = r1.iterator()
        L_0x0008:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0197
            java.lang.Object r2 = r1.next()
            com.fossil.zk0 r2 = (com.fossil.zk0) r2
            boolean r3 = r2.t
            if (r3 == 0) goto L_0x0023
            com.fossil.xc1<com.fossil.zk0> r3 = r0.a
            r3.remove(r2)
            java.util.Hashtable<com.fossil.zk0, com.fossil.jh1> r3 = r0.c
            r3.remove(r2)
            goto L_0x0008
        L_0x0023:
            java.lang.String r3 = "phase"
            com.fossil.ee7.a(r2, r3)
            java.util.ArrayList r3 = r2.f()
            com.fossil.ul0 r4 = com.fossil.ul0.TRANSFER_DATA
            boolean r3 = r3.contains(r4)
            r4 = 0
            r6 = 1
            if (r3 == 0) goto L_0x007c
            com.fossil.xc1<com.fossil.zk0> r3 = r0.b
            java.util.Iterator r3 = r3.iterator()
            r7 = 0
        L_0x003d:
            boolean r8 = r3.hasNext()
            if (r8 == 0) goto L_0x0071
            java.lang.Object r8 = r3.next()
            com.fossil.zk0 r8 = (com.fossil.zk0) r8
            java.util.ArrayList r9 = r8.f()
            com.fossil.ul0 r10 = com.fossil.ul0.TRANSFER_DATA
            boolean r9 = r9.contains(r10)
            if (r9 == 0) goto L_0x0069
            com.fossil.a61 r9 = r8.e()
            com.fossil.a61 r10 = r2.e()
            int r9 = r9.compareTo(r10)
            if (r9 >= 0) goto L_0x0069
            com.fossil.is0 r7 = com.fossil.is0.INTERRUPTED
            r8.a(r7)
            r7 = 1
        L_0x0069:
            boolean r8 = r8.a(r2)
            if (r8 != 0) goto L_0x003d
            r7 = 1
            goto L_0x003d
        L_0x0071:
            if (r7 != 0) goto L_0x00a3
            com.fossil.wj0 r3 = r0.e
            boolean r3 = r3.a(r2)
            if (r3 == 0) goto L_0x00a3
            goto L_0x00a2
        L_0x007c:
            com.fossil.xc1<com.fossil.zk0> r3 = r0.b
            java.util.Iterator r3 = r3.iterator()
        L_0x0082:
            boolean r7 = r3.hasNext()
            if (r7 == 0) goto L_0x0097
            java.lang.Object r7 = r3.next()
            r8 = r7
            com.fossil.zk0 r8 = (com.fossil.zk0) r8
            boolean r8 = r8.a(r2)
            r8 = r8 ^ r6
            if (r8 == 0) goto L_0x0082
            goto L_0x0098
        L_0x0097:
            r7 = 0
        L_0x0098:
            if (r7 != 0) goto L_0x00a3
            com.fossil.wj0 r3 = r0.e
            boolean r3 = r3.a(r2)
            if (r3 == 0) goto L_0x00a3
        L_0x00a2:
            r4 = 1
        L_0x00a3:
            if (r4 == 0) goto L_0x0008
            com.fossil.xc1<com.fossil.zk0> r3 = r0.a
            r3.remove(r2)
            com.fossil.xc1<com.fossil.zk0> r3 = r0.b
            r3.b(r2)
            com.fossil.sv0 r3 = new com.fossil.sv0
            r3.<init>(r0)
            r2.a(r3)
            java.util.Hashtable<com.fossil.zk0, com.fossil.jh1> r3 = r0.c
            java.lang.Object r2 = r3.get(r2)
            com.fossil.jh1 r2 = (com.fossil.jh1) r2
            if (r2 == 0) goto L_0x0008
            com.fossil.zk0 r3 = r2.a
            com.fossil.db1 r4 = r3.q
            r3.a(r4)
            com.fossil.zk0 r3 = r2.a
            boolean r4 = r3.t
            if (r4 == 0) goto L_0x00d0
            goto L_0x0008
        L_0x00d0:
            com.fossil.ri1 r4 = r3.w
            com.fossil.le0 r4 = com.fossil.le0.DEBUG
            java.lang.String r4 = r3.a
            java.lang.String r4 = r3.z
            org.json.JSONObject r3 = r3.i()
            r4 = 2
            r3.toString(r4)
            com.fossil.zk0 r3 = r2.a
            boolean r4 = r3.A
            if (r4 == 0) goto L_0x0115
            com.fossil.wl0 r4 = com.fossil.wl0.h
            com.fossil.wr1 r15 = new com.fossil.wr1
            com.fossil.wm0 r3 = r3.y
            java.lang.String r7 = com.fossil.yz0.a(r3)
            com.fossil.ci1 r8 = com.fossil.ci1.b
            com.fossil.zk0 r3 = r2.a
            com.fossil.ri1 r6 = r3.w
            java.lang.String r9 = r6.u
            com.fossil.wm0 r3 = r3.y
            java.lang.String r10 = com.fossil.yz0.a(r3)
            com.fossil.zk0 r3 = r2.a
            java.lang.String r11 = r3.z
            org.json.JSONObject r16 = r3.i()
            r12 = 1
            r13 = 0
            r14 = 0
            r3 = 0
            r17 = 448(0x1c0, float:6.28E-43)
            r6 = r15
            r5 = r15
            r15 = r3
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
            r4.a(r5)
        L_0x0115:
            com.fossil.zk0 r3 = r2.a
            com.fossil.eu0 r4 = r3.v
            com.fossil.is0 r5 = com.fossil.is0.SUCCESS
            r6 = 5
            r7 = 0
            com.fossil.eu0 r4 = com.fossil.eu0.a(r4, r7, r5, r7, r6)
            r3.v = r4
            com.fossil.zk0 r3 = r2.a
            com.fossil.ri1 r4 = r3.w
            com.fossil.jb1 r3 = r3.c
            java.util.concurrent.CopyOnWriteArraySet<com.fossil.jb1> r5 = r4.h
            boolean r5 = r5.contains(r3)
            if (r5 != 0) goto L_0x0136
            java.util.concurrent.CopyOnWriteArraySet<com.fossil.jb1> r4 = r4.h
            r4.add(r3)
        L_0x0136:
            com.fossil.zk0 r3 = r2.a
            java.util.concurrent.CopyOnWriteArrayList<com.fossil.gd7<com.fossil.zk0, com.fossil.i97>> r3 = r3.d
            java.util.Iterator r3 = r3.iterator()
        L_0x013e:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0150
            java.lang.Object r4 = r3.next()
            com.fossil.gd7 r4 = (com.fossil.gd7) r4
            com.fossil.zk0 r5 = r2.a
            r4.invoke(r5)
            goto L_0x013e
        L_0x0150:
            com.fossil.zk0 r3 = r2.a
            long r4 = java.lang.System.currentTimeMillis()
            r3.l = r4
            com.fossil.zk0 r6 = r2.a
            r6.j()
            boolean r2 = r6.c()
            if (r2 == 0) goto L_0x0192
            com.fossil.um1 r2 = new com.fossil.um1
            com.fossil.ri1 r8 = r6.w
            com.fossil.en0 r9 = r6.x
            com.fossil.f31 r10 = com.fossil.f31.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY
            com.fossil.mp0 r3 = com.fossil.mp0.b
            java.lang.String r4 = r8.u
            com.fossil.qn0 r3 = r3.a(r4)
            byte[] r11 = r3.a()
            java.lang.String r12 = r6.z
            r7 = r2
            r7.<init>(r8, r9, r10, r11, r12)
            com.fossil.td1 r8 = new com.fossil.td1
            r8.<init>(r6)
            com.fossil.nf1 r9 = new com.fossil.nf1
            r9.<init>(r6)
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 56
            r14 = 0
            com.fossil.zk0.a(r6, r7, r8, r9, r10, r11, r12, r13, r14)
            goto L_0x0008
        L_0x0192:
            r6.h()
            goto L_0x0008
        L_0x0197:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u01.d():void");
    }

    @DexIgnore
    public final ArrayList<zk0> a() {
        ArrayList<zk0> arrayList;
        synchronized (this.d) {
            arrayList = new ArrayList<>();
            arrayList.addAll(this.a);
            arrayList.addAll(this.b);
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(is0 is0, wm0[] wm0Arr) {
        a(is0, new cz0(wm0Arr));
    }

    @DexIgnore
    public final void a(is0 is0, gd7<? super zk0, Boolean> gd7) {
        for (zk0 zk0 : this.b) {
            ee7.a((Object) zk0, "phase");
            if (gd7.invoke(zk0).booleanValue()) {
                zk0.a(is0);
                this.b.remove(zk0);
            }
        }
    }

    @DexIgnore
    public final void a(is0 is0, wm0[] wm0Arr) {
        kx0 kx0 = new kx0(wm0Arr);
        a(is0, kx0);
        for (zk0 zk0 : this.a) {
            ee7.a((Object) zk0, "phase");
            if (kx0.invoke(zk0).booleanValue()) {
                zk0.a(is0);
                this.a.remove(zk0);
            }
        }
    }
}
