package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha {
    @DexIgnore
    public WeakReference<View> a;
    @DexIgnore
    public Runnable b; // = null;
    @DexIgnore
    public Runnable c; // = null;
    @DexIgnore
    public int d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ ia a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public a(ha haVar, ia iaVar, View view) {
            this.a = iaVar;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.a.a(this.b);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.a.b(this.b);
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            this.a.c(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ka a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;

        @DexIgnore
        public b(ha haVar, ka kaVar, View view) {
            this.a = kaVar;
            this.b = view;
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            this.a.a(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements ia {
        @DexIgnore
        public ha a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c(ha haVar) {
            this.a = haVar;
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void a(View view) {
            Object tag = view.getTag(2113929216);
            ia iaVar = tag instanceof ia ? (ia) tag : null;
            if (iaVar != null) {
                iaVar.a(view);
            }
        }

        @DexIgnore
        @Override // com.fossil.ia
        @SuppressLint({"WrongConstant"})
        public void b(View view) {
            int i = this.a.d;
            ia iaVar = null;
            if (i > -1) {
                view.setLayerType(i, null);
                this.a.d = -1;
            }
            if (Build.VERSION.SDK_INT >= 16 || !this.b) {
                ha haVar = this.a;
                Runnable runnable = haVar.c;
                if (runnable != null) {
                    haVar.c = null;
                    runnable.run();
                }
                Object tag = view.getTag(2113929216);
                if (tag instanceof ia) {
                    iaVar = (ia) tag;
                }
                if (iaVar != null) {
                    iaVar.b(view);
                }
                this.b = true;
            }
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void c(View view) {
            this.b = false;
            ia iaVar = null;
            if (this.a.d > -1) {
                view.setLayerType(2, null);
            }
            ha haVar = this.a;
            Runnable runnable = haVar.b;
            if (runnable != null) {
                haVar.b = null;
                runnable.run();
            }
            Object tag = view.getTag(2113929216);
            if (tag instanceof ia) {
                iaVar = (ia) tag;
            }
            if (iaVar != null) {
                iaVar.c(view);
            }
        }
    }

    @DexIgnore
    public ha(View view) {
        this.a = new WeakReference<>(view);
    }

    @DexIgnore
    public ha a(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setDuration(j);
        }
        return this;
    }

    @DexIgnore
    public ha b(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().translationY(f);
        }
        return this;
    }

    @DexIgnore
    public void c() {
        View view = this.a.get();
        if (view != null) {
            view.animate().start();
        }
    }

    @DexIgnore
    public ha a(float f) {
        View view = this.a.get();
        if (view != null) {
            view.animate().alpha(f);
        }
        return this;
    }

    @DexIgnore
    public long b() {
        View view = this.a.get();
        if (view != null) {
            return view.animate().getDuration();
        }
        return 0;
    }

    @DexIgnore
    public ha a(Interpolator interpolator) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setInterpolator(interpolator);
        }
        return this;
    }

    @DexIgnore
    public ha b(long j) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setStartDelay(j);
        }
        return this;
    }

    @DexIgnore
    public void a() {
        View view = this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    @DexIgnore
    public ha a(ia iaVar) {
        View view = this.a.get();
        if (view != null) {
            if (Build.VERSION.SDK_INT >= 16) {
                a(view, iaVar);
            } else {
                view.setTag(2113929216, iaVar);
                a(view, new c(this));
            }
        }
        return this;
    }

    @DexIgnore
    public final void a(View view, ia iaVar) {
        if (iaVar != null) {
            view.animate().setListener(new a(this, iaVar, view));
        } else {
            view.animate().setListener(null);
        }
    }

    @DexIgnore
    public ha a(ka kaVar) {
        View view = this.a.get();
        if (view != null && Build.VERSION.SDK_INT >= 19) {
            b bVar = null;
            if (kaVar != null) {
                bVar = new b(this, kaVar, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
