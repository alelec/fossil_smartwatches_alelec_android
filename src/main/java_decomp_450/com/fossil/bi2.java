package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi2 extends ak2<yi2> {
    @DexIgnore
    public static /* final */ vj2 E; // = vj2.FIT_SESSIONS;
    @DexIgnore
    public static /* final */ v02.g<bi2> F; // = new v02.g<>();
    @DexIgnore
    public static /* final */ v02<v02.d.C0203d> G; // = new v02<>("Fitness.SESSIONS_API", new ci2(), F);
    @DexIgnore
    public static /* final */ v02<v02.d.b> H; // = new v02<>("Fitness.SESSIONS_CLIENT", new ei2(), F);

    @DexIgnore
    public bi2(Context context, Looper looper, j62 j62, a12.b bVar, a12.c cVar) {
        super(context, looper, E, bVar, cVar, j62);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitSessionsApi");
        if (queryLocalInterface instanceof yi2) {
            return (yi2) queryLocalInterface;
        }
        return new xi2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String i() {
        return "com.google.android.gms.fitness.internal.IGoogleFitSessionsApi";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public final int k() {
        return q02.a;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public final String p() {
        return "com.google.android.gms.fitness.SessionsApi";
    }
}
