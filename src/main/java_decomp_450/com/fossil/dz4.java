package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.buddy_challenge.customview.CircleProgressView;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dz4 extends cz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i D; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray E;
    @DexIgnore
    public long C;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        E = sparseIntArray;
        sparseIntArray.put(2131362962, 1);
        E.put(2131362498, 2);
        E.put(2131362397, 3);
        E.put(2131362022, 4);
        E.put(2131362364, 5);
        E.put(2131362356, 6);
        E.put(2131362361, 7);
        E.put(2131362475, 8);
        E.put(2131362479, 9);
        E.put(2131362355, 10);
        E.put(2131362400, 11);
    }
    */

    @DexIgnore
    public dz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 12, D, E));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.C = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.C != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.C = 1;
        }
        g();
    }

    @DexIgnore
    public dz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (CircleProgressView) objArr[4], (TimerTextView) objArr[10], (FlexibleTextView) objArr[6], (TimerTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[2], (ConstraintLayout) objArr[1], (InterceptSwipe) objArr[0]);
        this.C = -1;
        ((cz4) this).B.setTag(null);
        a(view);
        f();
    }
}
