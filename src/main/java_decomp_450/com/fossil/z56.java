package com.fossil;

import com.portfolio.platform.data.model.Category;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z56 extends cl4 {
    @DexIgnore
    public abstract void a(j56 j56);

    @DexIgnore
    public abstract void a(Category category);

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, String str2);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public abstract void i();
}
