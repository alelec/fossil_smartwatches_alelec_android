package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xj4 implements Factory<pd5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<UserRepository> c;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> d;

    @DexIgnore
    public xj4(wj4 wj4, Provider<ch5> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    @DexIgnore
    public static xj4 a(wj4 wj4, Provider<ch5> provider, Provider<UserRepository> provider2, Provider<AlarmsRepository> provider3) {
        return new xj4(wj4, provider, provider2, provider3);
    }

    @DexIgnore
    public static pd5 a(wj4 wj4, ch5 ch5, UserRepository userRepository, AlarmsRepository alarmsRepository) {
        pd5 a2 = wj4.a(ch5, userRepository, alarmsRepository);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pd5 get() {
        return a(this.a, this.b.get(), this.c.get(), this.d.get());
    }
}
