package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l4 extends Drawable {
    @DexIgnore
    public float a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ Rect d;
    @DexIgnore
    public float e;
    @DexIgnore
    public boolean f; // = false;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public ColorStateList h;
    @DexIgnore
    public PorterDuffColorFilter i;
    @DexIgnore
    public ColorStateList j;
    @DexIgnore
    public PorterDuff.Mode k; // = PorterDuff.Mode.SRC_IN;

    @DexIgnore
    public l4(ColorStateList colorStateList, float f2) {
        this.a = f2;
        this.b = new Paint(5);
        a(colorStateList);
        this.c = new RectF();
        this.d = new Rect();
    }

    @DexIgnore
    public final void a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.h = colorStateList;
        this.b.setColor(colorStateList.getColorForState(getState(), this.h.getDefaultColor()));
    }

    @DexIgnore
    public float b() {
        return this.e;
    }

    @DexIgnore
    public float c() {
        return this.a;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        boolean z;
        Paint paint = this.b;
        if (this.i == null || paint.getColorFilter() != null) {
            z = false;
        } else {
            paint.setColorFilter(this.i);
            z = true;
        }
        RectF rectF = this.c;
        float f2 = this.a;
        canvas.drawRoundRect(rectF, f2, f2, paint);
        if (z) {
            paint.setColorFilter(null);
        }
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        outline.setRoundRect(this.d, this.a);
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2 = this.j;
        return (colorStateList2 != null && colorStateList2.isStateful()) || ((colorStateList = this.h) != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        a(rect);
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        PorterDuff.Mode mode;
        ColorStateList colorStateList = this.h;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        boolean z = colorForState != this.b.getColor();
        if (z) {
            this.b.setColor(colorForState);
        }
        ColorStateList colorStateList2 = this.j;
        if (colorStateList2 == null || (mode = this.k) == null) {
            return z;
        }
        this.i = a(colorStateList2, mode);
        return true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.b.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        this.j = colorStateList;
        this.i = a(colorStateList, this.k);
        invalidateSelf();
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        this.k = mode;
        this.i = a(this.j, mode);
        invalidateSelf();
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        a(colorStateList);
        invalidateSelf();
    }

    @DexIgnore
    public void a(float f2, boolean z, boolean z2) {
        if (f2 != this.e || this.f != z || this.g != z2) {
            this.e = f2;
            this.f = z;
            this.g = z2;
            a((Rect) null);
            invalidateSelf();
        }
    }

    @DexIgnore
    public final void a(Rect rect) {
        if (rect == null) {
            rect = getBounds();
        }
        this.c.set((float) rect.left, (float) rect.top, (float) rect.right, (float) rect.bottom);
        this.d.set(rect);
        if (this.f) {
            float b2 = m4.b(this.e, this.a, this.g);
            this.d.inset((int) Math.ceil((double) m4.a(this.e, this.a, this.g)), (int) Math.ceil((double) b2));
            this.c.set(this.d);
        }
    }

    @DexIgnore
    public void a(float f2) {
        if (f2 != this.a) {
            this.a = f2;
            a((Rect) null);
            invalidateSelf();
        }
    }

    @DexIgnore
    public ColorStateList a() {
        return this.h;
    }

    @DexIgnore
    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(getState(), 0), mode);
    }
}
