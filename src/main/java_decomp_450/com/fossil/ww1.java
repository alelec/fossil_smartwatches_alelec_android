package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww1 implements Factory<Integer> {
    @DexIgnore
    public static /* final */ ww1 a; // = new ww1();

    @DexIgnore
    public static ww1 a() {
        return a;
    }

    @DexIgnore
    public static int b() {
        return uw1.b();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Integer get() {
        return Integer.valueOf(b());
    }
}
