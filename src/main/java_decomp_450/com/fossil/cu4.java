package com.fossil;

import android.os.SystemClock;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cu4 implements View.OnClickListener {
    @DexIgnore
    public long a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ gd7<View, i97> c;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.gd7<? super android.view.View, com.fossil.i97> */
    /* JADX WARN: Multi-variable type inference failed */
    public cu4(int i, gd7<? super View, i97> gd7) {
        ee7.b(gd7, "onSafeClick");
        this.b = i;
        this.c = gd7;
    }

    @DexIgnore
    public void onClick(View view) {
        if (SystemClock.elapsedRealtime() - this.a >= ((long) this.b)) {
            this.a = SystemClock.elapsedRealtime();
            this.c.invoke(view);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ cu4(int i, gd7 gd7, int i2, zd7 zd7) {
        this((i2 & 1) != 0 ? 1000 : i, gd7);
    }
}
