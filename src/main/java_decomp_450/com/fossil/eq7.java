package com.fossil;

import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eq7 {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ int[] b; // = new int[10];

    @DexIgnore
    public void a() {
        this.a = 0;
        Arrays.fill(this.b, 0);
    }

    @DexIgnore
    public int b() {
        if ((this.a & 2) != 0) {
            return this.b[1];
        }
        return -1;
    }

    @DexIgnore
    public int c(int i) {
        return (this.a & 32) != 0 ? this.b[5] : i;
    }

    @DexIgnore
    public boolean d(int i) {
        return ((1 << i) & this.a) != 0;
    }

    @DexIgnore
    public int b(int i) {
        return (this.a & 16) != 0 ? this.b[4] : i;
    }

    @DexIgnore
    public int c() {
        if ((this.a & 128) != 0) {
            return this.b[7];
        }
        return 65535;
    }

    @DexIgnore
    public int d() {
        return Integer.bitCount(this.a);
    }

    @DexIgnore
    public eq7 a(int i, int i2) {
        if (i >= 0) {
            int[] iArr = this.b;
            if (i < iArr.length) {
                this.a = (1 << i) | this.a;
                iArr[i] = i2;
            }
        }
        return this;
    }

    @DexIgnore
    public int a(int i) {
        return this.b[i];
    }

    @DexIgnore
    public void a(eq7 eq7) {
        for (int i = 0; i < 10; i++) {
            if (eq7.d(i)) {
                a(i, eq7.a(i));
            }
        }
    }
}
