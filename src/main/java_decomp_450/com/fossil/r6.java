package com.fossil;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.h;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r6 extends Service {
    @DexIgnore
    public void a(int i, String str) {
        String[] packagesForUid = getPackageManager().getPackagesForUid(i);
        int length = packagesForUid.length;
        int i2 = 0;
        while (i2 < length) {
            if (!packagesForUid[i2].equals(str)) {
                i2++;
            } else {
                return;
            }
        }
        throw new SecurityException("NotificationSideChannelService: Uid " + i + " is not authorized for package " + str);
    }

    @DexIgnore
    public abstract void a(String str);

    @DexIgnore
    public abstract void a(String str, int i, String str2);

    @DexIgnore
    public abstract void a(String str, int i, String str2, Notification notification);

    @DexIgnore
    public IBinder onBind(Intent intent) {
        if (!intent.getAction().equals("android.support.BIND_NOTIFICATION_SIDE_CHANNEL") || Build.VERSION.SDK_INT > 19) {
            return null;
        }
        return new a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends h.a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.h
        public void a(String str, int i, String str2, Notification notification) throws RemoteException {
            r6.this.a(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                r6.this.a(str, i, str2, notification);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        @Override // com.fossil.h
        public void a(String str, int i, String str2) throws RemoteException {
            r6.this.a(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                r6.this.a(str, i, str2);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }

        @DexIgnore
        @Override // com.fossil.h
        public void a(String str) {
            r6.this.a(Binder.getCallingUid(), str);
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                r6.this.a(str);
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
    }
}
