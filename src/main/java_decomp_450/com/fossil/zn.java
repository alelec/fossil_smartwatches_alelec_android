package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zn extends vn<qn> {
    @DexIgnore
    public zn(Context context, vp vpVar) {
        super(ho.a(context, vpVar).c());
    }

    @DexIgnore
    @Override // com.fossil.vn
    public boolean a(zo zoVar) {
        return zoVar.j.b() == jm.UNMETERED;
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(qn qnVar) {
        return !qnVar.a() || qnVar.b();
    }
}
