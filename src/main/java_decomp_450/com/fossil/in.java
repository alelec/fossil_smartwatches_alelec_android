package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.work.impl.WorkDatabase;
import androidx.work.impl.background.systemalarm.SystemAlarmService;
import com.fossil.ln;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class in implements vm {
    @DexIgnore
    public static /* final */ String d; // = im.a("CommandHandler");
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ Map<String, vm> b; // = new HashMap();
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public in(Context context) {
        this.a = context;
    }

    @DexIgnore
    public static Intent a(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_DELAY_MET");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_SCHEDULE_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public static Intent c(Context context, String str) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_STOP_WORK");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        return intent;
    }

    @DexIgnore
    public final void d(Intent intent, int i, ln lnVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        im.a().a(d, String.format("Handling schedule work for %s", string), new Throwable[0]);
        WorkDatabase f = lnVar.e().f();
        f.beginTransaction();
        try {
            zo e = f.f().e(string);
            if (e == null) {
                im a2 = im.a();
                String str = d;
                a2.e(str, "Skipping scheduling " + string + " because it's no longer in the DB", new Throwable[0]);
            } else if (e.b.isFinished()) {
                im a3 = im.a();
                String str2 = d;
                a3.e(str2, "Skipping scheduling " + string + "because it is finished.", new Throwable[0]);
                f.endTransaction();
            } else {
                long a4 = e.a();
                if (!e.b()) {
                    im.a().a(d, String.format("Setting up Alarms for %s at %s", string, Long.valueOf(a4)), new Throwable[0]);
                    hn.a(this.a, lnVar.e(), string, a4);
                } else {
                    im.a().a(d, String.format("Opportunistically setting an alarm for %s at %s", string, Long.valueOf(a4)), new Throwable[0]);
                    hn.a(this.a, lnVar.e(), string, a4);
                    lnVar.a(new ln.b(lnVar, a(this.a), i));
                }
                f.setTransactionSuccessful();
                f.endTransaction();
            }
        } finally {
            f.endTransaction();
        }
    }

    @DexIgnore
    public void e(Intent intent, int i, ln lnVar) {
        String action = intent.getAction();
        if ("ACTION_CONSTRAINTS_CHANGED".equals(action)) {
            a(intent, i, lnVar);
        } else if ("ACTION_RESCHEDULE".equals(action)) {
            c(intent, i, lnVar);
        } else if (!a(intent.getExtras(), "KEY_WORKSPEC_ID")) {
            im.a().b(d, String.format("Invalid request for %s, requires %s.", action, "KEY_WORKSPEC_ID"), new Throwable[0]);
        } else if ("ACTION_SCHEDULE_WORK".equals(action)) {
            d(intent, i, lnVar);
        } else if ("ACTION_DELAY_MET".equals(action)) {
            b(intent, i, lnVar);
        } else if ("ACTION_STOP_WORK".equals(action)) {
            a(intent, lnVar);
        } else if ("ACTION_EXECUTION_COMPLETED".equals(action)) {
            a(intent, i);
        } else {
            im.a().e(d, String.format("Ignoring intent %s", intent), new Throwable[0]);
        }
    }

    @DexIgnore
    public static Intent a(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_CONSTRAINTS_CHANGED");
        return intent;
    }

    @DexIgnore
    public static Intent b(Context context) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_RESCHEDULE");
        return intent;
    }

    @DexIgnore
    public final void c(Intent intent, int i, ln lnVar) {
        im.a().a(d, String.format("Handling reschedule %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        lnVar.e().i();
    }

    @DexIgnore
    public static Intent a(Context context, String str, boolean z) {
        Intent intent = new Intent(context, SystemAlarmService.class);
        intent.setAction("ACTION_EXECUTION_COMPLETED");
        intent.putExtra("KEY_WORKSPEC_ID", str);
        intent.putExtra("KEY_NEEDS_RESCHEDULE", z);
        return intent;
    }

    @DexIgnore
    public final void b(Intent intent, int i, ln lnVar) {
        Bundle extras = intent.getExtras();
        synchronized (this.c) {
            String string = extras.getString("KEY_WORKSPEC_ID");
            im.a().a(d, String.format("Handing delay met for %s", string), new Throwable[0]);
            if (!this.b.containsKey(string)) {
                kn knVar = new kn(this.a, i, string, lnVar);
                this.b.put(string, knVar);
                knVar.b();
            } else {
                im.a().a(d, String.format("WorkSpec %s is already being handled for ACTION_DELAY_MET", string), new Throwable[0]);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.vm
    public void a(String str, boolean z) {
        synchronized (this.c) {
            vm remove = this.b.remove(str);
            if (remove != null) {
                remove.a(str, z);
            }
        }
    }

    @DexIgnore
    public boolean a() {
        boolean z;
        synchronized (this.c) {
            z = !this.b.isEmpty();
        }
        return z;
    }

    @DexIgnore
    public final void a(Intent intent, ln lnVar) {
        String string = intent.getExtras().getString("KEY_WORKSPEC_ID");
        im.a().a(d, String.format("Handing stopWork work for %s", string), new Throwable[0]);
        lnVar.e().c(string);
        hn.a(this.a, lnVar.e(), string);
        lnVar.a(string, false);
    }

    @DexIgnore
    public final void a(Intent intent, int i, ln lnVar) {
        im.a().a(d, String.format("Handling constraints changed %s", intent), new Throwable[0]);
        new jn(this.a, i, lnVar).a();
    }

    @DexIgnore
    public final void a(Intent intent, int i) {
        Bundle extras = intent.getExtras();
        String string = extras.getString("KEY_WORKSPEC_ID");
        boolean z = extras.getBoolean("KEY_NEEDS_RESCHEDULE");
        im.a().a(d, String.format("Handling onExecutionCompleted %s, %s", intent, Integer.valueOf(i)), new Throwable[0]);
        a(string, z);
    }

    @DexIgnore
    public static boolean a(Bundle bundle, String... strArr) {
        if (bundle == null || bundle.isEmpty()) {
            return false;
        }
        for (String str : strArr) {
            if (bundle.get(str) == null) {
                return false;
            }
        }
        return true;
    }
}
