package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br1 extends jr1 {
    @DexIgnore
    public /* final */ n90 S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ br1(ri1 ri1, en0 en0, n90 n90, short s, String str, int i) {
        super(ri1, en0, wm0.z, true, (i & 8) != 0 ? gq0.b.b(ri1.u, pb1.MUSIC_CONTROL) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 160);
        this.S = n90;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.u, this.S.a());
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public byte[] n() {
        bs0 bs0 = bs0.d;
        short s = ((v61) this).D;
        r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.MUSIC_CONTROL.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        return bs0.a(s, r60, this.S);
    }
}
