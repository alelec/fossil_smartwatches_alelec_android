package com.fossil;

import com.fossil.iy3;
import com.fossil.vx3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import com.google.errorprone.annotations.concurrent.LazyInit;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ny3<E> extends oy3<E> implements NavigableSet<E>, c04<E> {
    @DexIgnore
    public /* final */ transient Comparator<? super E> comparator;
    @DexIgnore
    @LazyInit
    public transient ny3<E> descendingSet;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<? super E> comparator;
        @DexIgnore
        public /* final */ Object[] elements;

        @DexIgnore
        public b(Comparator<? super E> comparator2, Object[] objArr) {
            this.comparator = comparator2;
            this.elements = objArr;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.ny3$a */
        /* JADX WARN: Multi-variable type inference failed */
        public Object readResolve() {
            a aVar = new a(this.comparator);
            aVar.a(this.elements);
            return aVar.a();
        }
    }

    @DexIgnore
    public ny3(Comparator<? super E> comparator2) {
        this.comparator = comparator2;
    }

    @DexIgnore
    public static <E> ny3<E> construct(Comparator<? super E> comparator2, int i, E... eArr) {
        if (i == 0) {
            return emptySet(comparator2);
        }
        iz3.b(eArr, i);
        Arrays.sort(eArr, 0, i, comparator2);
        int i2 = 1;
        for (int i3 = 1; i3 < i; i3++) {
            E e = eArr[i3];
            if (comparator2.compare(e, eArr[i2 - 1]) != 0) {
                eArr[i2] = e;
                i2++;
            }
        }
        Arrays.fill(eArr, i2, i, (Object) null);
        return new tz3(zx3.asImmutableList(eArr, i2), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> copyOf(E[] eArr) {
        return construct(jz3.natural(), eArr.length, (Object[]) eArr.clone());
    }

    @DexIgnore
    public static <E> ny3<E> copyOfSorted(SortedSet<E> sortedSet) {
        Comparator a2 = d04.a(sortedSet);
        zx3 copyOf = zx3.copyOf((Collection) sortedSet);
        if (copyOf.isEmpty()) {
            return emptySet(a2);
        }
        return new tz3(copyOf, a2);
    }

    @DexIgnore
    public static <E> tz3<E> emptySet(Comparator<? super E> comparator2) {
        return jz3.natural().equals(comparator2) ? (tz3<E>) tz3.NATURAL_EMPTY_SET : new tz3<>(zx3.of(), comparator2);
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> naturalOrder() {
        return new a<>(jz3.natural());
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public static <E> ny3<E> of() {
        return tz3.NATURAL_EMPTY_SET;
    }

    @DexIgnore
    public static <E> a<E> orderedBy(Comparator<E> comparator2) {
        return new a<>(comparator2);
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws InvalidObjectException {
        throw new InvalidObjectException("Use SerializedForm");
    }

    @DexIgnore
    public static <E extends Comparable<?>> a<E> reverseOrder() {
        return new a<>(jz3.natural().reverse());
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E ceiling(E e) {
        return (E) py3.a(tailSet((Object) e, true), (Object) null);
    }

    @DexIgnore
    @Override // java.util.SortedSet, com.fossil.c04
    public Comparator<? super E> comparator() {
        return this.comparator;
    }

    @DexIgnore
    public ny3<E> createDescendingSet() {
        return new gx3(this);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public abstract j04<E> descendingIterator();

    @DexIgnore
    @Override // java.util.SortedSet
    public E first() {
        return iterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E floor(E e) {
        return (E) qy3.b(headSet((Object) e, true).descendingIterator(), (Object) null);
    }

    @DexIgnore
    public abstract ny3<E> headSetImpl(E e, boolean z);

    @DexIgnore
    @Override // java.util.NavigableSet
    public E higher(E e) {
        return (E) py3.a(tailSet((Object) e, false), (Object) null);
    }

    @DexIgnore
    public abstract int indexOf(Object obj);

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.util.NavigableSet, java.lang.Iterable
    public abstract j04<E> iterator();

    @DexIgnore
    @Override // java.util.SortedSet
    public E last() {
        return descendingIterator().next();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public E lower(E e) {
        return (E) qy3.b(headSet((Object) e, false).descendingIterator(), (Object) null);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollFirst() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    @CanIgnoreReturnValue
    @Deprecated
    public final E pollLast() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public abstract ny3<E> subSetImpl(E e, boolean z, E e2, boolean z2);

    @DexIgnore
    public abstract ny3<E> tailSetImpl(E e, boolean z);

    @DexIgnore
    public int unsafeCompare(Object obj, Object obj2) {
        return unsafeCompare(this.comparator, obj, obj2);
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.iy3
    public Object writeReplace() {
        return new b(this.comparator, toArray());
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public static <E> ny3<E> copyOf(Iterable<? extends E> iterable) {
        return copyOf(jz3.natural(), iterable);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e) {
        return new tz3(zx3.of(e), jz3.natural());
    }

    @DexIgnore
    public static int unsafeCompare(Comparator<?> comparator2, Object obj, Object obj2) {
        return comparator2.compare(obj, obj2);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public ny3<E> descendingSet() {
        ny3<E> ny3 = this.descendingSet;
        if (ny3 != null) {
            return ny3;
        }
        ny3<E> createDescendingSet = createDescendingSet();
        this.descendingSet = createDescendingSet;
        createDescendingSet.descendingSet = this;
        return createDescendingSet;
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e, E e2) {
        return construct(jz3.natural(), 2, e, e2);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public ny3<E> headSet(E e) {
        return headSet((Object) e, false);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public ny3<E> subSet(E e, E e2) {
        return subSet((Object) e, true, (Object) e2, false);
    }

    @DexIgnore
    @Override // java.util.SortedSet, java.util.NavigableSet
    public ny3<E> tailSet(E e) {
        return tailSet((Object) e, true);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<E> extends iy3.a<E> {
        @DexIgnore
        public /* final */ Comparator<? super E> c;

        @DexIgnore
        public a(Comparator<? super E> comparator) {
            jw3.a(comparator);
            this.c = comparator;
        }

        @DexIgnore
        @Override // com.fossil.iy3.a, com.fossil.iy3.a, com.fossil.vx3.a, com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(E e) {
            super.a((Object) e);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.iy3.a, com.fossil.vx3.a, com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(E... eArr) {
            super.a((Object[]) eArr);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.iy3.a, com.fossil.vx3.b
        @CanIgnoreReturnValue
        public a<E> a(Iterator<? extends E> it) {
            super.a((Iterator) it);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.iy3.a
        public ny3<E> a() {
            ny3<E> construct = ny3.construct(this.c, ((vx3.a) this).b, ((vx3.a) this).a);
            ((vx3.a) this).b = construct.size();
            return construct;
        }
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public static <E> ny3<E> copyOf(Collection<? extends E> collection) {
        return copyOf((Comparator) jz3.natural(), (Collection) collection);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e, E e2, E e3) {
        return construct(jz3.natural(), 3, e, e2, e3);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public ny3<E> headSet(E e, boolean z) {
        jw3.a(e);
        return headSetImpl(e, z);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public ny3<E> subSet(E e, boolean z, E e2, boolean z2) {
        jw3.a(e);
        jw3.a(e2);
        jw3.a(this.comparator.compare(e, e2) <= 0);
        return subSetImpl(e, z, e2, z2);
    }

    @DexIgnore
    @Override // java.util.NavigableSet
    public ny3<E> tailSet(E e, boolean z) {
        jw3.a(e);
        return tailSetImpl(e, z);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e, E e2, E e3, E e4) {
        return construct(jz3.natural(), 4, e, e2, e3, e4);
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public static <E> ny3<E> copyOf(Iterator<? extends E> it) {
        return copyOf(jz3.natural(), it);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e, E e2, E e3, E e4, E e5) {
        return construct(jz3.natural(), 5, e, e2, e3, e4, e5);
    }

    @DexIgnore
    public static <E extends Comparable<? super E>> ny3<E> of(E e, E e2, E e3, E e4, E e5, E e6, E... eArr) {
        int length = eArr.length + 6;
        Comparable[] comparableArr = new Comparable[length];
        comparableArr[0] = e;
        comparableArr[1] = e2;
        comparableArr[2] = e3;
        comparableArr[3] = e4;
        comparableArr[4] = e5;
        comparableArr[5] = e6;
        System.arraycopy(eArr, 0, comparableArr, 6, eArr.length);
        return construct(jz3.natural(), length, comparableArr);
    }

    @DexIgnore
    public static <E> ny3<E> copyOf(Comparator<? super E> comparator2, Iterator<? extends E> it) {
        a aVar = new a(comparator2);
        aVar.a((Iterator) it);
        return aVar.a();
    }

    @DexIgnore
    public static <E> ny3<E> copyOf(Comparator<? super E> comparator2, Iterable<? extends E> iterable) {
        jw3.a(comparator2);
        if (d04.a(comparator2, iterable) && (iterable instanceof ny3)) {
            ny3<E> ny3 = (ny3) iterable;
            if (!ny3.isPartialView()) {
                return ny3;
            }
        }
        Object[] c = py3.c(iterable);
        return construct(comparator2, c.length, c);
    }

    @DexIgnore
    public static <E> ny3<E> copyOf(Comparator<? super E> comparator2, Collection<? extends E> collection) {
        return copyOf((Comparator) comparator2, (Iterable) collection);
    }
}
