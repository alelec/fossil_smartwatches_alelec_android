package com.fossil;

import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.ArrayList;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qf6 extends dl4<pf6> {
    @DexIgnore
    void a(do5 do5, ArrayList<String> arrayList);

    @DexIgnore
    void a(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    void a(Date date, boolean z, boolean z2, boolean z3);

    @DexIgnore
    void c(qf<GoalTrackingData> qfVar);

    @DexIgnore
    void d();
}
