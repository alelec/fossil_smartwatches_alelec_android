package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq0 {
    @DexIgnore
    public static /* final */ HashMap<r87<String, pb1>, Byte> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ gq0 b; // = new gq0();

    @DexIgnore
    public final short a(String str, pb1 pb1) {
        return pb1.a;
    }

    @DexIgnore
    public final short b(String str, pb1 pb1) {
        Byte b2 = a.get(new r87(str, pb1));
        byte byteValue = b2 != null ? b2.byteValue() : 0;
        switch (ko0.b[pb1.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return pb1.a;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
                return pb1.a((byte) 0);
            case 15:
            case 16:
                return pb1.a((byte) 254);
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
                a.put(new r87<>(str, pb1), Byte.valueOf((byte) ((yz0.b(byteValue) + 1) % yz0.b((byte) -1))));
                return pb1.a(byteValue);
            default:
                throw new p87();
        }
    }
}
