package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum vv2 {
    DOUBLE(0, xv2.SCALAR, kw2.DOUBLE),
    FLOAT(1, xv2.SCALAR, kw2.FLOAT),
    INT64(2, xv2.SCALAR, kw2.LONG),
    UINT64(3, xv2.SCALAR, kw2.LONG),
    INT32(4, xv2.SCALAR, kw2.INT),
    FIXED64(5, xv2.SCALAR, kw2.LONG),
    FIXED32(6, xv2.SCALAR, kw2.INT),
    BOOL(7, xv2.SCALAR, kw2.BOOLEAN),
    STRING(8, xv2.SCALAR, kw2.STRING),
    MESSAGE(9, xv2.SCALAR, kw2.MESSAGE),
    BYTES(10, xv2.SCALAR, kw2.BYTE_STRING),
    UINT32(11, xv2.SCALAR, kw2.INT),
    ENUM(12, xv2.SCALAR, kw2.ENUM),
    SFIXED32(13, xv2.SCALAR, kw2.INT),
    SFIXED64(14, xv2.SCALAR, kw2.LONG),
    SINT32(15, xv2.SCALAR, kw2.INT),
    SINT64(16, xv2.SCALAR, kw2.LONG),
    GROUP(17, xv2.SCALAR, kw2.MESSAGE),
    DOUBLE_LIST(18, xv2.VECTOR, kw2.DOUBLE),
    FLOAT_LIST(19, xv2.VECTOR, kw2.FLOAT),
    INT64_LIST(20, xv2.VECTOR, kw2.LONG),
    UINT64_LIST(21, xv2.VECTOR, kw2.LONG),
    INT32_LIST(22, xv2.VECTOR, kw2.INT),
    FIXED64_LIST(23, xv2.VECTOR, kw2.LONG),
    FIXED32_LIST(24, xv2.VECTOR, kw2.INT),
    BOOL_LIST(25, xv2.VECTOR, kw2.BOOLEAN),
    STRING_LIST(26, xv2.VECTOR, kw2.STRING),
    MESSAGE_LIST(27, xv2.VECTOR, kw2.MESSAGE),
    BYTES_LIST(28, xv2.VECTOR, kw2.BYTE_STRING),
    UINT32_LIST(29, xv2.VECTOR, kw2.INT),
    ENUM_LIST(30, xv2.VECTOR, kw2.ENUM),
    SFIXED32_LIST(31, xv2.VECTOR, kw2.INT),
    SFIXED64_LIST(32, xv2.VECTOR, kw2.LONG),
    SINT32_LIST(33, xv2.VECTOR, kw2.INT),
    SINT64_LIST(34, xv2.VECTOR, kw2.LONG),
    DOUBLE_LIST_PACKED(35, xv2.PACKED_VECTOR, kw2.DOUBLE),
    FLOAT_LIST_PACKED(36, xv2.PACKED_VECTOR, kw2.FLOAT),
    INT64_LIST_PACKED(37, xv2.PACKED_VECTOR, kw2.LONG),
    UINT64_LIST_PACKED(38, xv2.PACKED_VECTOR, kw2.LONG),
    INT32_LIST_PACKED(39, xv2.PACKED_VECTOR, kw2.INT),
    FIXED64_LIST_PACKED(40, xv2.PACKED_VECTOR, kw2.LONG),
    FIXED32_LIST_PACKED(41, xv2.PACKED_VECTOR, kw2.INT),
    BOOL_LIST_PACKED(42, xv2.PACKED_VECTOR, kw2.BOOLEAN),
    UINT32_LIST_PACKED(43, xv2.PACKED_VECTOR, kw2.INT),
    ENUM_LIST_PACKED(44, xv2.PACKED_VECTOR, kw2.ENUM),
    SFIXED32_LIST_PACKED(45, xv2.PACKED_VECTOR, kw2.INT),
    SFIXED64_LIST_PACKED(46, xv2.PACKED_VECTOR, kw2.LONG),
    SINT32_LIST_PACKED(47, xv2.PACKED_VECTOR, kw2.INT),
    SINT64_LIST_PACKED(48, xv2.PACKED_VECTOR, kw2.LONG),
    GROUP_LIST(49, xv2.VECTOR, kw2.MESSAGE),
    MAP(50, xv2.MAP, kw2.VOID);
    
    @DexIgnore
    public static /* final */ vv2[] c0;
    @DexIgnore
    public /* final */ kw2 zzaz;
    @DexIgnore
    public /* final */ int zzba;
    @DexIgnore
    public /* final */ xv2 zzbb;
    @DexIgnore
    public /* final */ Class<?> zzbc;
    @DexIgnore
    public /* final */ boolean zzbd;

    /*
    static {
        vv2[] values = values();
        c0 = new vv2[values.length];
        for (vv2 vv2 : values) {
            c0[vv2.zzba] = vv2;
        }
    }
    */

    @DexIgnore
    public vv2(int i, xv2 xv2, kw2 kw2) {
        int i2;
        this.zzba = i;
        this.zzbb = xv2;
        this.zzaz = kw2;
        int i3 = uv2.a[xv2.ordinal()];
        boolean z = true;
        if (i3 == 1) {
            this.zzbc = kw2.zza();
        } else if (i3 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = kw2.zza();
        }
        this.zzbd = (xv2 != xv2.SCALAR || (i2 = uv2.b[kw2.ordinal()]) == 1 || i2 == 2 || i2 == 3) ? false : z;
    }

    @DexIgnore
    public final int zza() {
        return this.zzba;
    }
}
