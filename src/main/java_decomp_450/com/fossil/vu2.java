package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu2 implements Comparator<tu2> {
    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // java.util.Comparator
    public final /* synthetic */ int compare(tu2 tu2, tu2 tu22) {
        tu2 tu23 = tu2;
        tu2 tu24 = tu22;
        yu2 yu2 = (yu2) tu23.iterator();
        yu2 yu22 = (yu2) tu24.iterator();
        while (yu2.hasNext() && yu22.hasNext()) {
            int compare = Integer.compare(tu2.a(yu2.zza()), tu2.a(yu22.zza()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(tu23.zza(), tu24.zza());
    }
}
