package com.fossil;

import android.annotation.SuppressLint;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import com.fossil.zs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"MissingPermission"})
public final class bt implements zs {
    @DexIgnore
    public static /* final */ NetworkRequest e; // = new NetworkRequest.Builder().addCapability(12).build();
    @DexIgnore
    public /* final */ b b; // = new b(this);
    @DexIgnore
    public /* final */ ConnectivityManager c;
    @DexIgnore
    public /* final */ zs.b d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public /* final */ /* synthetic */ bt a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(bt btVar) {
            this.a = btVar;
        }

        @DexIgnore
        public void onAvailable(Network network) {
            ee7.b(network, "network");
            this.a.a(network, true);
        }

        @DexIgnore
        public void onLost(Network network) {
            ee7.b(network, "network");
            this.a.a(network, false);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public bt(ConnectivityManager connectivityManager, zs.b bVar) {
        ee7.b(connectivityManager, "connectivityManager");
        ee7.b(bVar, "listener");
        this.c = connectivityManager;
        this.d = bVar;
    }

    @DexIgnore
    @Override // com.fossil.zs
    public void start() {
        this.c.registerNetworkCallback(e, this.b);
    }

    @DexIgnore
    @Override // com.fossil.zs
    public void stop() {
        this.c.unregisterNetworkCallback(this.b);
    }

    @DexIgnore
    @Override // com.fossil.zs
    public boolean a() {
        Network[] allNetworks = this.c.getAllNetworks();
        ee7.a((Object) allNetworks, "connectivityManager.allNetworks");
        for (Network network : allNetworks) {
            ee7.a((Object) network, "it");
            if (a(network)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void a(Network network, boolean z) {
        boolean z2;
        Network[] allNetworks = this.c.getAllNetworks();
        ee7.a((Object) allNetworks, "connectivityManager.allNetworks");
        int length = allNetworks.length;
        boolean z3 = false;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Network network2 = allNetworks[i];
            if (ee7.a(network2, network)) {
                z2 = z;
            } else {
                ee7.a((Object) network2, "it");
                z2 = a(network2);
            }
            if (z2) {
                z3 = true;
                break;
            }
            i++;
        }
        this.d.a(z3);
    }

    @DexIgnore
    public final boolean a(Network network) {
        NetworkCapabilities networkCapabilities = this.c.getNetworkCapabilities(network);
        if (networkCapabilities == null || !networkCapabilities.hasCapability(12)) {
            return false;
        }
        return true;
    }
}
