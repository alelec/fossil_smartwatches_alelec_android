package com.fossil;

import com.fossil.av;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tv extends uv<JSONObject> {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public tv(int i, String str, JSONObject jSONObject, av.b<JSONObject> bVar, av.a aVar) {
        super(i, str, jSONObject == null ? null : jSONObject.toString(), bVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.uv, com.fossil.yu
    public av<JSONObject> parseNetworkResponse(vu vuVar) {
        try {
            return av.a(new JSONObject(new String(vuVar.b, nv.a(vuVar.c, uv.PROTOCOL_CHARSET))), nv.a(vuVar));
        } catch (UnsupportedEncodingException e) {
            return av.a(new xu(e));
        } catch (JSONException e2) {
            return av.a(new xu(e2));
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public tv(String str, JSONObject jSONObject, av.b<JSONObject> bVar, av.a aVar) {
        this(jSONObject == null ? 0 : 1, str, jSONObject, bVar, aVar);
    }
}
