package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayGoalChart;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cb6 extends go5 implements bb6 {
    @DexIgnore
    public xz6 f;
    @DexIgnore
    public qw6<o15> g;
    @DexIgnore
    public ab6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ cb6 a;

        @DexIgnore
        public b(cb6 cb6, o15 o15) {
            this.a = cb6;
        }

        @DexIgnore
        public final void onClick(View view) {
            cb6.a(this.a).a().a((Integer) 2);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ xz6 a(cb6 cb6) {
        xz6 xz6 = cb6.f;
        if (xz6 != null) {
            return xz6;
        }
        ee7.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void b(boolean z) {
        o15 a2;
        qw6<o15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                OverviewDayGoalChart overviewDayGoalChart = a2.r;
                ee7.a((Object) overviewDayGoalChart, "binding.dayChart");
                overviewDayGoalChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                ee7.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewDayGoalChart overviewDayGoalChart2 = a2.r;
            ee7.a((Object) overviewDayGoalChart2, "binding.dayChart");
            overviewDayGoalChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            ee7.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "GoalTrackingOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        o15 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        qw6<o15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            overviewDayGoalChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        o15 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onCreateView");
        o15 o15 = (o15) qb.a(layoutInflater, 2131558559, viewGroup, false, a1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            he a3 = je.a(activity).a(xz6.class);
            ee7.a((Object) a3, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = (xz6) a3;
            o15.s.setOnClickListener(new b(this, o15));
        }
        this.g = new qw6<>(this, o15);
        f1();
        qw6<o15> qw6 = this.g;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onResume");
        f1();
        ab6 ab6 = this.h;
        if (ab6 != null) {
            ab6.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onStop");
        ab6 ab6 = this.h;
        if (ab6 != null) {
            ab6.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.bb6
    public void a(do5 do5, ArrayList<String> arrayList) {
        o15 a2;
        OverviewDayGoalChart overviewDayGoalChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayFragment", "showDayDetailChart - baseModel=" + do5);
        qw6<o15> qw6 = this.g;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayGoalChart = a2.r) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayGoalChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayGoalChart.a(do5);
        }
    }

    @DexIgnore
    public void a(ab6 ab6) {
        ee7.b(ab6, "presenter");
        this.h = ab6;
    }
}
