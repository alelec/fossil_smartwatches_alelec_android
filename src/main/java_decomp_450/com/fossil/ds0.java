package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds0 extends s81 {
    @DexIgnore
    public /* final */ jg0[] T;

    @DexIgnore
    public ds0(ri1 ri1, en0 en0, jg0[] jg0Arr) {
        super(ri1, en0, wm0.f0, true, gq0.b.b(ri1.u, pb1.MICRO_APP), new byte[0], LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, null, 192);
        this.T = jg0Arr;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.Y3, yz0.a(this.T));
    }

    @DexIgnore
    @Override // com.fossil.jr1, com.fossil.s81
    public byte[] n() {
        r60 microAppVersion = ((zk0) this).x.a().getMicroAppVersion();
        r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.MICRO_APP.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        ee7.a((Object) r60, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
        tc1 tc1 = new tc1(this.T, microAppVersion);
        byte[] a = yz0.a(yz0.a(yz0.a(new byte[]{tc1.b.getMajor(), tc1.b.getMinor(), ck1.b.a()}, tc1.a(tc1.e)), tc1.a(tc1.c)), tc1.a(tc1.d));
        int a2 = (int) ik1.a.a(a, ng1.CRC32);
        ByteBuffer order = ByteBuffer.allocate(a.length + 4).order(ByteOrder.LITTLE_ENDIAN);
        order.put(a);
        order.putInt(a2);
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return qs1.d.a(((v61) this).D, r60, array);
    }
}
