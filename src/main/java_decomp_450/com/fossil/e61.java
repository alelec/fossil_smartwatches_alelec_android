package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e61 {
    @DexIgnore
    public static /* final */ e61 a; // = new e61();

    /*
    static {
        new x91(24, 40, 0, 600);
        new x91(9, 12, 0, 72);
        new x91(80, 108, 4, 600);
    }
    */

    @DexIgnore
    public final boolean a(h41 h41, x91 x91) {
        int i = x91.a;
        int i2 = x91.b;
        int i3 = h41.a;
        return i <= i3 && i2 >= i3 && h41.b == x91.c && h41.c >= x91.d;
    }
}
