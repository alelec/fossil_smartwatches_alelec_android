package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f46 implements Factory<e46> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;

    @DexIgnore
    public f46(Provider<ch5> provider, Provider<UserRepository> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static f46 a(Provider<ch5> provider, Provider<UserRepository> provider2) {
        return new f46(provider, provider2);
    }

    @DexIgnore
    public static e46 a(ch5 ch5, UserRepository userRepository) {
        return new e46(ch5, userRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public e46 get() {
        return a(this.a.get(), this.b.get());
    }
}
