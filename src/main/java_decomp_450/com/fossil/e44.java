package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e44 {
    @DexIgnore
    public Object a; // = new Object();
    @DexIgnore
    public oo3<Void> b; // = new oo3<>();
    @DexIgnore
    public /* final */ SharedPreferences c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public /* final */ l14 f;
    @DexIgnore
    public oo3<Void> g; // = new oo3<>();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0075 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public e44(com.fossil.l14 r5) {
        /*
            r4 = this;
            r4.<init>()
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r4.a = r0
            com.fossil.oo3 r0 = new com.fossil.oo3
            r0.<init>()
            r4.b = r0
            com.fossil.oo3 r0 = new com.fossil.oo3
            r0.<init>()
            r4.g = r0
            r4.f = r5
            android.content.Context r5 = r5.b()
            if (r5 == 0) goto L_0x0086
            android.content.SharedPreferences r0 = com.fossil.t34.h(r5)
            r4.c = r0
            r1 = 0
            java.lang.String r2 = "firebase_crashlytics_collection_enabled"
            boolean r0 = r0.contains(r2)
            r2 = 1
            if (r0 == 0) goto L_0x003b
            android.content.SharedPreferences r5 = r4.c
            java.lang.String r0 = "firebase_crashlytics_collection_enabled"
            boolean r5 = r5.getBoolean(r0, r2)
        L_0x0038:
            r2 = r5
            r1 = 1
            goto L_0x006e
        L_0x003b:
            android.content.pm.PackageManager r0 = r5.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0064 }
            if (r0 == 0) goto L_0x006e
            java.lang.String r5 = r5.getPackageName()     // Catch:{ NameNotFoundException -> 0x0064 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r5 = r0.getApplicationInfo(r5, r3)     // Catch:{ NameNotFoundException -> 0x0064 }
            if (r5 == 0) goto L_0x006e
            android.os.Bundle r0 = r5.metaData     // Catch:{ NameNotFoundException -> 0x0064 }
            if (r0 == 0) goto L_0x006e
            android.os.Bundle r0 = r5.metaData     // Catch:{ NameNotFoundException -> 0x0064 }
            java.lang.String r3 = "firebase_crashlytics_collection_enabled"
            boolean r0 = r0.containsKey(r3)     // Catch:{ NameNotFoundException -> 0x0064 }
            if (r0 == 0) goto L_0x006e
            android.os.Bundle r5 = r5.metaData     // Catch:{ NameNotFoundException -> 0x0064 }
            java.lang.String r0 = "firebase_crashlytics_collection_enabled"
            boolean r5 = r5.getBoolean(r0)     // Catch:{ NameNotFoundException -> 0x0064 }
            goto L_0x0038
        L_0x0064:
            r5 = move-exception
            com.fossil.z24 r0 = com.fossil.z24.a()
            java.lang.String r3 = "Unable to get PackageManager. Falling through"
            r0.a(r3, r5)
        L_0x006e:
            r4.e = r2
            r4.d = r1
            java.lang.Object r5 = r4.a
            monitor-enter(r5)
            boolean r0 = r4.a()     // Catch:{ all -> 0x0083 }
            if (r0 == 0) goto L_0x0081
            com.fossil.oo3<java.lang.Void> r0 = r4.b     // Catch:{ all -> 0x0083 }
            r1 = 0
            r0.b(r1)     // Catch:{ all -> 0x0083 }
        L_0x0081:
            monitor-exit(r5)     // Catch:{ all -> 0x0083 }
            return
        L_0x0083:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x0083 }
            throw r0
        L_0x0086:
            java.lang.RuntimeException r5 = new java.lang.RuntimeException
            java.lang.String r0 = "null context"
            r5.<init>(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.e44.<init>(com.fossil.l14):void");
    }

    @DexIgnore
    public boolean a() {
        if (this.d) {
            return this.e;
        }
        return this.f.g();
    }

    @DexIgnore
    public no3<Void> b() {
        no3<Void> a2;
        synchronized (this.a) {
            a2 = this.b.a();
        }
        return a2;
    }

    @DexIgnore
    public no3<Void> c() {
        return v44.a(this.g.a(), b());
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            this.g.b((Void) null);
            return;
        }
        throw new IllegalStateException("An invalid data collection token was used.");
    }
}
