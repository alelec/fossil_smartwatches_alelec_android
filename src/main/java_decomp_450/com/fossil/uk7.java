package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk7 extends zl7 implements dk7 {
    @DexIgnore
    @Override // com.fossil.dk7
    public uk7 a() {
        return this;
    }

    @DexIgnore
    public final String a(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("List{");
        sb.append(str);
        sb.append("}[");
        Object b = b();
        if (b != null) {
            boolean z = true;
            for (bm7 bm7 = (bm7) b; !ee7.a(bm7, this); bm7 = bm7.c()) {
                if (bm7 instanceof ok7) {
                    ok7 ok7 = (ok7) bm7;
                    if (z) {
                        z = false;
                    } else {
                        sb.append(", ");
                    }
                    sb.append(ok7);
                }
            }
            sb.append("]");
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "StringBuilder().apply(builderAction).toString()");
            return sb2;
        }
        throw new x87("null cannot be cast to non-null type kotlinx.coroutines.internal.Node /* = kotlinx.coroutines.internal.LockFreeLinkedListNode */");
    }

    @DexIgnore
    @Override // com.fossil.dk7
    public boolean isActive() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public String toString() {
        return dj7.c() ? a("Active") : super.toString();
    }
}
