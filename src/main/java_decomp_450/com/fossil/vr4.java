package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.oy6;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr4 extends RecyclerView.g<a> {
    @DexIgnore
    public List<un4> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ oy6.b c;
    @DexIgnore
    public /* final */ st4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ g95 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ vr4 c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vr4$a$a")
        /* renamed from: com.fossil.vr4$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0221a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;
            @DexIgnore
            public /* final */ /* synthetic */ un4 b;

            @DexIgnore
            public View$OnClickListenerC0221a(a aVar, un4 un4, oy6.b bVar, st4 st4) {
                this.a = aVar;
                this.b = un4;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2;
                if (this.a.getAdapterPosition() != -1 && (a2 = this.a.c.b) != null) {
                    a2.a(this.b, this.a.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vr4 vr4, g95 g95, View view) {
            super(view);
            ee7.b(g95, "binding");
            ee7.b(view, "root");
            this.c = vr4;
            this.a = g95;
            this.b = view;
        }

        @DexIgnore
        public void a(un4 un4, oy6.b bVar, st4 st4) {
            ee7.b(un4, "item");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            g95 g95 = this.a;
            String b2 = fu4.a.b(un4.b(), un4.e(), un4.i());
            FlexibleTextView flexibleTextView = g95.u;
            ee7.a((Object) flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleTextView flexibleTextView2 = g95.v;
            ee7.a((Object) flexibleTextView2, "tvSocial");
            flexibleTextView2.setText(un4.i());
            ImageView imageView = g95.s;
            ee7.a((Object) imageView, "ivAvatar");
            rt4.a(imageView, un4.h(), b2, bVar, st4);
            g95.r.setOnClickListener(new View$OnClickListenerC0221a(this, un4, bVar, st4));
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(un4 un4, int i);
    }

    @DexIgnore
    public vr4() {
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.c = b2;
        this.d = st4.d.a();
    }

    @DexIgnore
    public final void c() {
        if (this.a.size() != 0) {
            this.a.clear();
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        aVar.a(this.a.get(i), this.c, this.d);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        g95 a2 = g95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemFriendSearchBinding.\u2026tInflater, parent, false)");
        View d2 = a2.d();
        ee7.a((Object) d2, "itemFriendSearchBinding.root");
        return new a(this, a2, d2);
    }

    @DexIgnore
    public final void a(List<un4> list) {
        ee7.b(list, "friendList");
        if (!ee7.a(this.a, list)) {
            this.a.clear();
            this.a.addAll(list);
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public final void a(int i) {
        if (i != -1) {
            this.a.remove(i);
            notifyItemRemoved(i);
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
