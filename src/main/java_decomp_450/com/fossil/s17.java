package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s17 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ List<Object> b;

    @DexIgnore
    public s17(String str, List<Object> list) {
        this.a = str;
        this.b = list == null ? new ArrayList<>() : list;
    }

    @DexIgnore
    public static Object b(Object obj) {
        if (obj == null) {
            return null;
        }
        if (t17.c) {
            Log.d("Sqflite", "arg " + obj.getClass().getCanonicalName() + " " + a(obj));
        }
        if (obj instanceof List) {
            List list = (List) obj;
            byte[] bArr = new byte[list.size()];
            for (int i = 0; i < list.size(); i++) {
                bArr[i] = (byte) ((Integer) list.get(i)).intValue();
            }
            obj = bArr;
        }
        if (t17.c) {
            Log.d("Sqflite", "arg " + obj.getClass().getCanonicalName() + " " + a(obj));
        }
        return obj;
    }

    @DexIgnore
    public final String[] a(List<Object> list) {
        return (String[]) c(list).toArray(new String[0]);
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public Object[] d() {
        return b(this.b);
    }

    @DexIgnore
    public s17 e() {
        if (this.b.size() == 0) {
            return this;
        }
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList();
        int length = this.a.length();
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = this.a.charAt(i3);
            if (charAt == '?') {
                int i4 = i3 + 1;
                if (i4 < length && Character.isDigit(this.a.charAt(i4))) {
                    return this;
                }
                i++;
                if (i2 >= this.b.size()) {
                    return this;
                }
                int i5 = i2 + 1;
                Object obj = this.b.get(i2);
                if ((obj instanceof Integer) || (obj instanceof Long)) {
                    sb.append(obj.toString());
                    i2 = i5;
                } else {
                    arrayList.add(obj);
                    i2 = i5;
                }
            }
            sb.append(charAt);
        }
        if (i != this.b.size()) {
            return this;
        }
        return new s17(sb.toString(), arrayList);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof s17)) {
            return false;
        }
        s17 s17 = (s17) obj;
        String str = this.a;
        if (str != null) {
            if (!str.equals(s17.a)) {
                return false;
            }
        } else if (s17.a != null) {
            return false;
        }
        if (this.b.size() != s17.b.size()) {
            return false;
        }
        for (int i = 0; i < this.b.size(); i++) {
            if (!(this.b.get(i) instanceof byte[]) || !(s17.b.get(i) instanceof byte[])) {
                if (!this.b.get(i).equals(s17.b.get(i))) {
                    return false;
                }
            } else if (!Arrays.equals((byte[]) this.b.get(i), (byte[]) s17.b.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        List<Object> list = this.b;
        if (list == null || list.isEmpty()) {
            str = "";
        } else {
            str = " " + c(this.b);
        }
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public static String a(Object obj) {
        if (obj == null) {
            return null;
        }
        if (obj instanceof byte[]) {
            ArrayList arrayList = new ArrayList();
            for (byte b2 : (byte[]) obj) {
                arrayList.add(Integer.valueOf(b2));
            }
            return arrayList.toString();
        } else if (obj instanceof Map) {
            return a((Map<Object, Object>) ((Map) obj)).toString();
        } else {
            return obj.toString();
        }
    }

    @DexIgnore
    public final List<String> c(List<Object> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (Object obj : list) {
                arrayList.add(a(obj));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Object[] b(List<Object> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (Object obj : list) {
                arrayList.add(b(obj));
            }
        }
        return arrayList.toArray(new Object[0]);
    }

    @DexIgnore
    public static Map<String, Object> a(Map<Object, Object> map) {
        Object obj;
        HashMap hashMap = new HashMap();
        for (Map.Entry<Object, Object> entry : map.entrySet()) {
            Object value = entry.getValue();
            if (value instanceof Map) {
                obj = a((Map<Object, Object>) ((Map) value));
            } else {
                obj = a(value);
            }
            hashMap.put(a(entry.getKey()), obj);
        }
        return hashMap;
    }

    @DexIgnore
    public List<Object> b() {
        return this.b;
    }

    @DexIgnore
    public String[] a() {
        return a(this.b);
    }
}
