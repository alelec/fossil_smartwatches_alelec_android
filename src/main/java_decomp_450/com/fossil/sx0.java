package com.fossil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.SharedPreferences;
import com.facebook.places.PlaceManager;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sx0 {
    @DexIgnore
    public static /* final */ Hashtable<String, km1> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ Hashtable<String, String> b; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ sx0 c;

    /*
    static {
        String string;
        sx0 sx0 = new sx0();
        c = sx0;
        SharedPreferences a2 = yz0.a(j91.a);
        if (a2 != null && (string = a2.getString("com.fossil.blesdk.device.cache", null)) != null) {
            ee7.a((Object) string, "getSharedPreferences(Sha\u2026ENCE_KEY, null) ?: return");
            try {
                JSONArray jSONArray = new JSONArray(yn0.h.a(string));
                int length = jSONArray.length();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string2 = jSONObject.getString(PlaceManager.PARAM_MAC_ADDRESS);
                    String string3 = jSONObject.getString(Constants.SERIAL_NUMBER);
                    ee7.a((Object) string3, "serialNumber");
                    ee7.a((Object) string2, "macAddress");
                    sx0.a(string3, string2);
                }
            } catch (Exception e) {
                wl0.h.a(e);
            }
        }
    }
    */

    @DexIgnore
    public final km1 a(BluetoothDevice bluetoothDevice, String str) {
        km1 km1;
        km1 km12;
        t11 t11 = t11.a;
        bluetoothDevice.getAddress();
        synchronized (a) {
            km1 km13 = a.get(bluetoothDevice.getAddress());
            if (km13 == null) {
                km1 = new km1(bluetoothDevice, str, null);
                a.put(km1.t.getMacAddress(), km1);
                if (m60.t.a(str)) {
                    c.a(str, km1.t.getMacAddress());
                }
            } else {
                if (m60.t.a(km13.t.getSerialNumber()) || !m60.t.a(str)) {
                    km12 = km13;
                } else {
                    m60 a2 = m60.a(km13.t, null, null, str, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 262139);
                    km13.t = a2;
                    a.put(a2.getMacAddress(), km13);
                    km12 = km13;
                    c.a(str, km13.t.getMacAddress());
                }
                km1 = km12;
            }
        }
        return km1;
    }

    @DexIgnore
    public final String b(String str) {
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                String key = entry.getKey();
                if (mh7.b(entry.getValue(), str, true)) {
                    return key;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public final String a(String str) {
        String str2;
        synchronized (b) {
            str2 = b.get(str);
        }
        return str2;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        if (m60.t.a(str) && BluetoothAdapter.checkBluetoothAddress(str2)) {
            synchronized (b) {
                b.put(str, str2);
                i97 i97 = i97.a;
            }
        }
    }

    @DexIgnore
    public final void a() {
        JSONArray jSONArray = new JSONArray();
        synchronized (b) {
            for (Map.Entry<String, String> entry : b.entrySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(Constants.SERIAL_NUMBER, entry.getKey());
                    jSONObject.put(PlaceManager.PARAM_MAC_ADDRESS, entry.getValue());
                } catch (Exception e) {
                    wl0.h.a(e);
                }
                jSONArray.put(jSONObject);
            }
            i97 i97 = i97.a;
        }
        String jSONArray2 = jSONArray.toString();
        ee7.a((Object) jSONArray2, "array.toString()");
        String b2 = yn0.h.b(jSONArray2);
        SharedPreferences a2 = yz0.a(j91.a);
        if (a2 != null) {
            a2.edit().putString("com.fossil.blesdk.device.cache", b2).apply();
        }
    }
}
