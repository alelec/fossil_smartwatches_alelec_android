package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr1 extends fe7 implements gd7<je0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ fh0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vr1(fh0 fh0) {
        super(1);
        this.a = fh0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(je0 je0) {
        je0 je02 = je0;
        xm0 xm0 = xm0.i;
        xm0.d.remove(this.a.a);
        o60 o60 = (o60) je02;
        if (o60.getErrorCode() == p60.REQUEST_UNSUPPORTED) {
            xm0.i.d(this.a.a);
            this.a.a.a(je02);
        } else if (o60.getErrorCode() == p60.REQUEST_FAILED && o60.b().b == is0.HID_INPUT_DEVICE_DISABLED) {
            xm0 xm02 = xm0.i;
            xm0.c.add(this.a.a);
        } else {
            xm0.i.a(this.a.a, qr0.b.a("HID_EXPONENT_BACK_OFF_TAG"));
        }
        return i97.a;
    }
}
