package com.fossil;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rj5 implements SensorEventListener {
    @DexIgnore
    public /* final */ d a; // = new d();
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public SensorManager c;
    @DexIgnore
    public Sensor d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public long a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public b c;
    }

    @DexIgnore
    public rj5(a aVar) {
        this.b = aVar;
    }

    @DexIgnore
    public boolean a(SensorManager sensorManager) {
        if (this.d != null) {
            return true;
        }
        Sensor defaultSensor = sensorManager.getDefaultSensor(1);
        this.d = defaultSensor;
        if (defaultSensor != null) {
            this.c = sensorManager;
            sensorManager.registerListener(this, defaultSensor, 0);
        }
        if (this.d != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    @DexIgnore
    public void onSensorChanged(SensorEvent sensorEvent) {
        boolean a2 = a(sensorEvent);
        this.a.a(sensorEvent.timestamp, a2);
        if (this.a.b()) {
            this.a.a();
            this.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public b a;

        @DexIgnore
        public b a() {
            b bVar = this.a;
            if (bVar == null) {
                return new b();
            }
            this.a = bVar.c;
            return bVar;
        }

        @DexIgnore
        public void a(b bVar) {
            bVar.c = this.a;
            this.a = bVar;
        }
    }

    @DexIgnore
    public void a() {
        Sensor sensor = this.d;
        if (sensor != null) {
            this.c.unregisterListener(this, sensor);
            this.c = null;
            this.d = null;
        }
    }

    @DexIgnore
    public final boolean a(SensorEvent sensorEvent) {
        float[] fArr = sensorEvent.values;
        float f = fArr[0];
        float f2 = fArr[1];
        float f3 = fArr[2];
        if (Math.sqrt((double) ((f * f) + (f2 * f2) + (f3 * f3))) > 13.0d) {
            return true;
        }
        return false;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ c a; // = new c();
        @DexIgnore
        public b b;
        @DexIgnore
        public b c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public void a(long j, boolean z) {
            a(j - 500000000);
            b a2 = this.a.a();
            a2.a = j;
            a2.b = z;
            a2.c = null;
            b bVar = this.c;
            if (bVar != null) {
                bVar.c = a2;
            }
            this.c = a2;
            if (this.b == null) {
                this.b = a2;
            }
            this.d++;
            if (z) {
                this.e++;
            }
        }

        @DexIgnore
        public boolean b() {
            b bVar;
            b bVar2 = this.c;
            if (!(bVar2 == null || (bVar = this.b) == null || bVar2.a - bVar.a < 250000000)) {
                int i = this.e;
                int i2 = this.d;
                if (i >= (i2 >> 1) + (i2 >> 2)) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public void a() {
            while (true) {
                b bVar = this.b;
                if (bVar != null) {
                    this.b = bVar.c;
                    this.a.a(bVar);
                } else {
                    this.c = null;
                    this.d = 0;
                    this.e = 0;
                    return;
                }
            }
        }

        @DexIgnore
        public void a(long j) {
            b bVar;
            while (this.d >= 4 && (bVar = this.b) != null && j - bVar.a > 0) {
                if (bVar.b) {
                    this.e--;
                }
                this.d--;
                b bVar2 = bVar.c;
                this.b = bVar2;
                if (bVar2 == null) {
                    this.c = null;
                }
                this.a.a(bVar);
            }
        }
    }
}
