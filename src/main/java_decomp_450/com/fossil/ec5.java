package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ec5 extends fc5 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public ec5(boolean z, String str) {
        this.a = z;
        this.b = str;
    }

    @DexIgnore
    public String a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.a;
    }
}
