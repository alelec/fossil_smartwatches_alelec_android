package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ky4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ RecyclerView B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ RTLImageView u;
    @DexIgnore
    public /* final */ ImageView v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ LinearLayout z;

    @DexIgnore
    public ky4(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, ImageView imageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ImageView imageView2, ConstraintLayout constraintLayout, View view2, ImageView imageView3, LinearLayout linearLayout, ConstraintLayout constraintLayout2, RecyclerView recyclerView, View view3) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = imageView;
        this.s = flexibleTextView;
        this.t = flexibleTextView2;
        this.u = rTLImageView;
        this.v = imageView2;
        this.w = constraintLayout;
        this.x = view2;
        this.y = imageView3;
        this.z = linearLayout;
        this.A = constraintLayout2;
        this.B = recyclerView;
        this.C = view3;
    }
}
