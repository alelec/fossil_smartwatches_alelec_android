package com.fossil;

import com.fossil.x77;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b87<K, V> extends x77<K, V, Provider<V>> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends x77.a<K, V, Provider<V>> {
        @DexIgnore
        @Override // com.fossil.x77.a
        public b<K, V> a(K k, Provider<V> provider) {
            super.a((Object) k, (Provider) provider);
            return this;
        }

        @DexIgnore
        public b(int i) {
            super(i);
        }

        @DexIgnore
        public b87<K, V> a() {
            return new b87<>(((x77.a) this).a);
        }
    }

    @DexIgnore
    public static <K, V> b<K, V> a(int i) {
        return new b<>(i);
    }

    @DexIgnore
    public b87(Map<K, Provider<V>> map) {
        super(map);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Map<K, Provider<V>> get() {
        return a();
    }
}
