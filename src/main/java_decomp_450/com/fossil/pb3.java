package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ki3 a;
    @DexIgnore
    public /* final */ /* synthetic */ mb3 b;

    @DexIgnore
    public pb3(mb3 mb3, ki3 ki3) {
        this.b = mb3;
        this.a = ki3;
    }

    @DexIgnore
    public final void run() {
        this.a.b();
        if (xm3.a()) {
            this.a.c().a(this);
            return;
        }
        boolean b2 = this.b.b();
        long unused = this.b.c = 0;
        if (b2) {
            this.b.a();
        }
    }
}
