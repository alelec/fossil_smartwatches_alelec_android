package com.fossil;

import com.fossil.ql4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.AuthApiUserService;
import java.net.SocketTimeoutException;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en5 extends ql4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ AuthApiUserService d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return en5.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            ee7.b(str, "oldPass");
            ee7.b(str2, "newPass");
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ql4.c {
    }

    /*
    static {
        String simpleName = en5.class.getSimpleName();
        ee7.a((Object) simpleName, "ChangePasswordUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public en5(AuthApiUserService authApiUserService) {
        ee7.b(authApiUserService, "mAuthApiUserService");
        this.d = authApiUserService;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends jj5<ie4> {
        @DexIgnore
        public /* final */ /* synthetic */ en5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(en5 en5) {
            this.a = en5;
        }

        @DexIgnore
        @Override // com.fossil.jj5
        public void a(Call<ie4> call, fv7<ie4> fv7) {
            ee7.b(call, "call");
            ee7.b(fv7, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = en5.f.a();
            local.d(a2, "changePassword onSuccessResponse response=" + fv7);
            this.a.a().onSuccess(new d());
        }

        @DexIgnore
        @Override // com.fossil.jj5
        public void a(Call<ie4> call, ServerError serverError) {
            ee7.b(call, "call");
            ee7.b(serverError, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = en5.f.a();
            local.d(a2, "changePassword onErrorResponse response=" + serverError.getUserMessage());
            ql4.d a3 = this.a.a();
            Integer code = serverError.getCode();
            ee7.a((Object) code, "response.code");
            a3.a(new c(code.intValue(), serverError.getUserMessage()));
        }

        @DexIgnore
        @Override // com.fossil.jj5
        public void a(Call<ie4> call, Throwable th) {
            ee7.b(call, "call");
            ee7.b(th, "t");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = en5.f.a();
            StringBuilder sb = new StringBuilder();
            sb.append("changePassword onFail throwable=");
            th.printStackTrace();
            sb.append(i97.a);
            local.d(a2, sb.toString());
            if (th instanceof SocketTimeoutException) {
                this.a.a().a(new c(MFNetworkReturnCode.CLIENT_TIMEOUT, null));
            } else {
                this.a.a().a(new c(601, null));
            }
        }
    }

    @DexIgnore
    public void a(b bVar) {
        ee7.b(bVar, "requestValues");
        if (!xw6.b(PortfolioApp.g0.c())) {
            a().a(new c(601, ""));
            return;
        }
        ie4 ie4 = new ie4();
        try {
            ie4.a("oldPassword", bVar.b());
            ie4.a("newPassword", bVar.a());
        } catch (Exception unused) {
        }
        this.d.changePassword(ie4).a(new e(this));
    }
}
