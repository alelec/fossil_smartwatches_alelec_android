package com.fossil;

import java.io.Serializable;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr2 {
    @DexIgnore
    public static <T> tr2<T> a(tr2<T> tr2) {
        if ((tr2 instanceof ur2) || (tr2 instanceof vr2)) {
            return tr2;
        }
        if (tr2 instanceof Serializable) {
            return new vr2(tr2);
        }
        return new ur2(tr2);
    }

    @DexIgnore
    public static <T> tr2<T> a(@NullableDecl T t) {
        return new xr2(t);
    }
}
