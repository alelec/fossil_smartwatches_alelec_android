package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z25 extends y25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i E; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray F;
    @DexIgnore
    public long D;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        F = sparseIntArray;
        sparseIntArray.put(2131362517, 1);
        F.put(2131362382, 2);
        F.put(2131362378, 3);
        F.put(2131362617, 4);
        F.put(2131362236, 5);
        F.put(2131363266, 6);
        F.put(2131363265, 7);
        F.put(2131362000, 8);
        F.put(2131362511, 9);
        F.put(2131361988, 10);
        F.put(2131362338, 11);
        F.put(2131361963, 12);
    }
    */

    @DexIgnore
    public z25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 13, E, F));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.D = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.D != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.D = 1;
        }
        g();
    }

    @DexIgnore
    public z25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[12], (FlexibleCheckBox) objArr[10], (FlexibleCheckBox) objArr[8], (FlexibleTextInputEditText) objArr[5], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[1], (FlexibleTextInputLayout) objArr[4], (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6]);
        this.D = -1;
        ((y25) this).A.setTag(null);
        a(view);
        f();
    }
}
