package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pn0 extends fe7 implements gd7<BluetoothDevice, String> {
    @DexIgnore
    public static /* final */ pn0 a; // = new pn0();

    @DexIgnore
    public pn0() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public String invoke(BluetoothDevice bluetoothDevice) {
        String address = bluetoothDevice.getAddress();
        ee7.a((Object) address, "it.address");
        return address;
    }
}
