package com.fossil;

import com.fossil.y12;
import com.google.android.gms.location.LocationAvailability;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul2 implements y12.b<c53> {
    @DexIgnore
    public /* final */ /* synthetic */ LocationAvailability a;

    @DexIgnore
    public ul2(sl2 sl2, LocationAvailability locationAvailability) {
        this.a = locationAvailability;
    }

    @DexIgnore
    @Override // com.fossil.y12.b
    public final void a() {
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.y12.b
    public final /* synthetic */ void a(c53 c53) {
        c53.onLocationAvailability(this.a);
    }
}
