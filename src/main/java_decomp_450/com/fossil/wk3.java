package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 d;
    @DexIgnore
    public /* final */ /* synthetic */ r43 e;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 f;

    @DexIgnore
    public wk3(ek3 ek3, String str, String str2, boolean z, nm3 nm3, r43 r43) {
        this.f = ek3;
        this.a = str;
        this.b = str2;
        this.c = z;
        this.d = nm3;
        this.e = r43;
    }

    @DexIgnore
    public final void run() {
        Bundle bundle = new Bundle();
        try {
            bg3 d2 = this.f.d;
            if (d2 == null) {
                this.f.e().t().a("Failed to get user properties; not connected to service", this.a, this.b);
                return;
            }
            Bundle a2 = jm3.a(d2.a(this.a, this.b, this.c, this.d));
            this.f.J();
            this.f.j().a(this.e, a2);
        } catch (RemoteException e2) {
            this.f.e().t().a("Failed to get user properties; remote exception", this.a, e2);
        } finally {
            this.f.j().a(this.e, bundle);
        }
    }
}
