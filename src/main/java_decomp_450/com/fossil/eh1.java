package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eh1 extends fe7 implements vc7<i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public eh1(zk1 zk1) {
        super(0);
        this.a = zk1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.vc7
    public i97 invoke() {
        bi1 c = this.a.n();
        if (c != null) {
            a81 a81 = new a81(c.c());
            bi1 a2 = this.a.a(a81.a, a81.b);
            if (a2 == null && (a2 = this.a.n()) == null) {
                ee7.a();
                throw null;
            }
            this.a.b(a2);
            return i97.a;
        }
        ee7.a();
        throw null;
    }
}
