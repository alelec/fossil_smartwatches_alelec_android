package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz6 extends tm {
    @DexIgnore
    public /* final */ Map<Class<? extends ListenableWorker>, Provider<a07<? extends ListenableWorker>>> b;

    @DexIgnore
    public zz6(Map<Class<? extends ListenableWorker>, Provider<a07<? extends ListenableWorker>>> map) {
        ee7.b(map, "workerFactoryMap");
        this.b = map;
    }

    @DexIgnore
    @Override // com.fossil.tm
    public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
        T t;
        Provider provider;
        ee7.b(context, "appContext");
        ee7.b(str, "workerClassName");
        ee7.b(workerParameters, "workerParameters");
        Iterator<T> it = this.b.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (Class.forName(str).isAssignableFrom((Class) t.getKey())) {
                break;
            }
        }
        T t2 = t;
        if (t2 != null && (provider = (Provider) t2.getValue()) != null) {
            return ((a07) provider.get()).a(context, workerParameters);
        }
        throw new IllegalArgumentException("could not find worker: " + str);
    }
}
