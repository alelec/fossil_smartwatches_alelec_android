package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f70 extends v60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<f70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final f70 a(byte[] bArr) {
            return new f70(new String(s97.a(bArr, 0, bArr.length - 1), sg7.a));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public f70[] newArray(int i) {
            return new f70[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public f70 createFromParcel(Parcel parcel) {
            parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                return new f70(readString);
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public f70(String str) {
        super(go0.c);
        this.b = str;
    }

    @DexIgnore
    @Override // com.fossil.v60, com.fossil.k60
    public JSONObject a() {
        return yz0.a(super.a(), r51.g, this.b);
    }

    @DexIgnore
    @Override // com.fossil.v60
    public byte[] c() {
        String str = this.b;
        Charset c = b21.x.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return s97.a(bytes, (byte) 0);
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.v60
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(f70.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.b, ((f70) obj).b) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.TitleEntry");
    }

    @DexIgnore
    public final String getTitle() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.v60
    public int hashCode() {
        return this.b.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.v60
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b);
        }
    }
}
