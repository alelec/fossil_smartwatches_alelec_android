package com.fossil;

import com.fossil.o40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u40 implements o40, n40 {
    @DexIgnore
    public /* final */ o40 a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public volatile n40 c;
    @DexIgnore
    public volatile n40 d;
    @DexIgnore
    public o40.a e;
    @DexIgnore
    public o40.a f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public u40(Object obj, o40 o40) {
        o40.a aVar = o40.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.b = obj;
        this.a = o40;
    }

    @DexIgnore
    public void a(n40 n40, n40 n402) {
        this.c = n40;
        this.d = n402;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public o40 b() {
        o40 b2;
        synchronized (this.b) {
            b2 = this.a != null ? this.a.b() : this;
        }
        return b2;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean c(n40 n40) {
        boolean z;
        synchronized (this.b) {
            z = h() && n40.equals(this.c) && !a();
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void clear() {
        synchronized (this.b) {
            this.g = false;
            this.e = o40.a.CLEARED;
            this.f = o40.a.CLEARED;
            this.d.clear();
            this.c.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean d(n40 n40) {
        boolean z;
        synchronized (this.b) {
            z = i() && (n40.equals(this.c) || this.e != o40.a.SUCCESS);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public void e(n40 n40) {
        synchronized (this.b) {
            if (n40.equals(this.d)) {
                this.f = o40.a.SUCCESS;
                return;
            }
            this.e = o40.a.SUCCESS;
            if (this.a != null) {
                this.a.e(this);
            }
            if (!this.f.isComplete()) {
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean f(n40 n40) {
        boolean z;
        synchronized (this.b) {
            z = g() && n40.equals(this.c) && this.e != o40.a.PAUSED;
        }
        return z;
    }

    @DexIgnore
    public final boolean g() {
        o40 o40 = this.a;
        return o40 == null || o40.f(this);
    }

    @DexIgnore
    public final boolean h() {
        o40 o40 = this.a;
        return o40 == null || o40.c(this);
    }

    @DexIgnore
    public final boolean i() {
        o40 o40 = this.a;
        return o40 == null || o40.d(this);
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean isRunning() {
        boolean z;
        synchronized (this.b) {
            z = this.e == o40.a.RUNNING;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.n40, com.fossil.o40
    public boolean a() {
        boolean z;
        synchronized (this.b) {
            if (!this.d.a()) {
                if (!this.c.a()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d A[ORIG_RETURN, RETURN, SYNTHETIC] */
    @Override // com.fossil.n40
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(com.fossil.n40 r4) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof com.fossil.u40
            r1 = 0
            if (r0 == 0) goto L_0x002e
            com.fossil.u40 r4 = (com.fossil.u40) r4
            com.fossil.n40 r0 = r3.c
            if (r0 != 0) goto L_0x0010
            com.fossil.n40 r0 = r4.c
            if (r0 != 0) goto L_0x002e
            goto L_0x001a
        L_0x0010:
            com.fossil.n40 r0 = r3.c
            com.fossil.n40 r2 = r4.c
            boolean r0 = r0.b(r2)
            if (r0 == 0) goto L_0x002e
        L_0x001a:
            com.fossil.n40 r0 = r3.d
            if (r0 != 0) goto L_0x0023
            com.fossil.n40 r4 = r4.d
            if (r4 != 0) goto L_0x002e
            goto L_0x002d
        L_0x0023:
            com.fossil.n40 r0 = r3.d
            com.fossil.n40 r4 = r4.d
            boolean r4 = r0.b(r4)
            if (r4 == 0) goto L_0x002e
        L_0x002d:
            r1 = 1
        L_0x002e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u40.b(com.fossil.n40):boolean");
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void c() {
        synchronized (this.b) {
            this.g = true;
            try {
                if (!(this.e == o40.a.SUCCESS || this.f == o40.a.RUNNING)) {
                    this.f = o40.a.RUNNING;
                    this.d.c();
                }
                if (this.g && this.e != o40.a.RUNNING) {
                    this.e = o40.a.RUNNING;
                    this.c.c();
                }
            } finally {
                this.g = false;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void d() {
        synchronized (this.b) {
            if (!this.f.isComplete()) {
                this.f = o40.a.PAUSED;
                this.d.d();
            }
            if (!this.e.isComplete()) {
                this.e = o40.a.PAUSED;
                this.c.d();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean f() {
        boolean z;
        synchronized (this.b) {
            z = this.e == o40.a.SUCCESS;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public void a(n40 n40) {
        synchronized (this.b) {
            if (!n40.equals(this.c)) {
                this.f = o40.a.FAILED;
                return;
            }
            this.e = o40.a.FAILED;
            if (this.a != null) {
                this.a.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean e() {
        boolean z;
        synchronized (this.b) {
            z = this.e == o40.a.CLEARED;
        }
        return z;
    }
}
