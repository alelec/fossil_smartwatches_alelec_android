package com.fossil;

import android.location.Location;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.fitness.WorkoutSessionManager;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ Location[] e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jc0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jc0 createFromParcel(Parcel parcel) {
            return new jc0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jc0[] newArray(int i) {
            return new jc0[i];
        }
    }

    @DexIgnore
    public jc0(byte b, int i, long j) {
        super(cb0.WORKOUT_ROUTE_IMAGE, b, i);
        this.d = j;
        ArrayList<GpsDataPoint> gpsRoute = WorkoutSessionManager.getGpsRoute(j);
        ee7.a((Object) gpsRoute, "WorkoutSessionManager.getGpsRoute(sessionId)");
        ArrayList arrayList = new ArrayList(x97.a(gpsRoute, 10));
        for (T t : gpsRoute) {
            ee7.a((Object) t, "gpsDataPoint");
            Location location = new Location("gpsDataPoint");
            location.setLongitude(t.getLongitude());
            location.setLatitude(t.getLatitude());
            location.setTime((long) (t.getTimestamp() * 1000));
            location.setAltitude(t.getAltitude());
            location.setAccuracy(t.getHorizontalAccuracy());
            if (Build.VERSION.SDK_INT >= 26) {
                location.setVerticalAccuracyMeters(t.getVerticalAccuracy());
            }
            location.setSpeed((float) t.getSpeed());
            location.setBearing((float) t.getHeading());
            arrayList.add(location);
        }
        Object[] array = arrayList.toArray(new Location[0]);
        if (array != null) {
            this.e = (Location[]) array;
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        return yz0.a(super.a(), r51.G5, Long.valueOf(this.d));
    }

    @DexIgnore
    public final Location[] d() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(jc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((jc0) obj).d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return Long.valueOf(this.d).hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.e, i);
        }
    }

    @DexIgnore
    public /* synthetic */ jc0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = parcel.readLong();
        Location[] locationArr = (Location[]) parcel.createTypedArray(Location.CREATOR);
        this.e = locationArr == null ? new Location[0] : locationArr;
    }
}
