package com.fossil;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl extends RecyclerView.ViewHolder {
    @DexIgnore
    public rl(FrameLayout frameLayout) {
        super(frameLayout);
    }

    @DexIgnore
    public static rl a(ViewGroup viewGroup) {
        FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        frameLayout.setId(da.b());
        frameLayout.setSaveEnabled(false);
        return new rl(frameLayout);
    }

    @DexIgnore
    public FrameLayout a() {
        return (FrameLayout) ((RecyclerView.ViewHolder) this).itemView;
    }
}
