package com.fossil;

import android.content.Intent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mh6 extends cl4 {
    @DexIgnore
    public abstract void a(Intent intent);

    @DexIgnore
    public abstract void a(boolean z);

    @DexIgnore
    public abstract FossilDeviceSerialPatternUtil.DEVICE h();

    @DexIgnore
    public abstract void i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();
}
