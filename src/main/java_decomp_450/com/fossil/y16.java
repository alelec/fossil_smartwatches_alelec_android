package com.fossil;

import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y16 extends dl4<x16> {
    @DexIgnore
    void L(String str);

    @DexIgnore
    void m(List<SecondTimezoneSetting> list);
}
