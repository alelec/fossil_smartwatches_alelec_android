package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.cy6;
import com.fossil.oi6;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class li6 extends go5 implements cy6.g {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public oi6 f;
    @DexIgnore
    public qw6<g55> g;
    @DexIgnore
    public kd5 h;
    @DexIgnore
    public jo5 i;
    @DexIgnore
    public yz6 j;
    @DexIgnore
    public rj4 p;
    @DexIgnore
    public sq6 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final li6 a() {
            return new li6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public b(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(fb5.FEMALE);
            li6.d(this.a).a(fb5.FEMALE);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public c(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(fb5.OTHER);
            li6.d(this.a).a(fb5.OTHER);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public d(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            li6.d(this.a).e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<oi6.b> {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public e(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(oi6.b bVar) {
            FlexibleTextInputLayout flexibleTextInputLayout;
            FlexibleTextInputLayout flexibleTextInputLayout2;
            FlexibleTextInputLayout flexibleTextInputLayout3;
            g55 g55;
            FlexibleTextInputEditText flexibleTextInputEditText;
            MFUser i = bVar.i();
            if (i != null) {
                this.a.b(i);
            }
            Boolean j = bVar.j();
            if (j != null) {
                this.a.T(j.booleanValue());
            }
            Uri d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            if (bVar.e()) {
                this.a.j();
            } else {
                this.a.h();
            }
            if (bVar.h() != null) {
                this.a.v();
            }
            r87<Integer, String> g = bVar.g();
            if (g != null) {
                this.a.a(g.getFirst().intValue(), g.getSecond());
            }
            if (bVar.f()) {
                this.a.C();
            }
            Bundle c = bVar.c();
            if (c != null) {
                this.a.a(c);
            }
            String a2 = bVar.a();
            if (!(a2 == null || (g55 = (g55) li6.c(this.a).a()) == null || (flexibleTextInputEditText = g55.L) == null)) {
                flexibleTextInputEditText.setText(a2);
            }
            if (bVar.b() == null) {
                g55 g552 = (g55) li6.c(this.a).a();
                if (g552 != null && (flexibleTextInputLayout3 = g552.C) != null) {
                    flexibleTextInputLayout3.setErrorEnabled(false);
                    return;
                }
                return;
            }
            g55 g553 = (g55) li6.c(this.a).a();
            if (!(g553 == null || (flexibleTextInputLayout2 = g553.C) == null)) {
                flexibleTextInputLayout2.setErrorEnabled(true);
            }
            g55 g554 = (g55) li6.c(this.a).a();
            if (g554 != null && (flexibleTextInputLayout = g554.C) != null) {
                flexibleTextInputLayout.setError(bVar.b());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<Date> {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public f(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Date date) {
            if (date != null) {
                li6.d(this.a).b(date);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public g(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            oi6 d = li6.d(this.a);
            g55 g55 = (g55) li6.c(this.a).a();
            String valueOf = String.valueOf((g55 == null || (flexibleTextInputEditText = g55.s) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                d.a(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            FlexibleTextInputEditText flexibleTextInputEditText;
            oi6 d = li6.d(this.a);
            g55 g55 = (g55) li6.c(this.a).a();
            String valueOf = String.valueOf((g55 == null || (flexibleTextInputEditText = g55.t) == null) ? null : flexibleTextInputEditText.getEditableText());
            if (valueOf != null) {
                d.b(nh7.d((CharSequence) valueOf).toString());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public j(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public k(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements uz6 {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;
        @DexIgnore
        public /* final */ /* synthetic */ g55 b;

        @DexIgnore
        public l(li6 li6, g55 g55) {
            this.a = li6;
            this.b = g55;
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(int i) {
            if (this.b.I.getUnit() == ob5.METRIC) {
                li6.d(this.a).a(i);
                return;
            }
            li6.d(this.a).a(af7.a(xd5.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(boolean z) {
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void b(int i) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements uz6 {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;
        @DexIgnore
        public /* final */ /* synthetic */ g55 b;

        @DexIgnore
        public m(li6 li6, g55 g55) {
            this.a = li6;
            this.b = g55;
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(int i) {
            if (this.b.J.getUnit() == ob5.METRIC) {
                li6.d(this.a).b(af7.a((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            li6.d(this.a).b(af7.a(xd5.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(boolean z) {
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void b(int i) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ li6 a;

        @DexIgnore
        public n(li6 li6) {
            this.a = li6;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.b(fb5.MALE);
            li6.d(this.a).a(fb5.MALE);
        }
    }

    /*
    static {
        String simpleName = li6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileEditFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 c(li6 li6) {
        qw6<g55> qw6 = li6.g;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ oi6 d(li6 li6) {
        oi6 oi6 = li6.f;
        if (oi6 != null) {
            return oi6;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @iu7(1222)
    public final void doCameraTask() {
        FragmentActivity activity;
        Intent b2;
        if (xg5.a(xg5.b, (Context) getActivity(), xg5.a.EDIT_AVATAR, false, false, false, (Integer) null, 60, (Object) null) && (activity = getActivity()) != null && (b2 = je5.b(activity)) != null) {
            startActivityForResult(b2, 1234);
        }
    }

    @DexIgnore
    public final void C() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.G(childFragmentManager);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004c, code lost:
        if ((com.fossil.nh7.d(r5).length() > 0) != false) goto L_0x0050;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void T(boolean r5) {
        /*
            r4 = this;
            com.fossil.qw6<com.fossil.g55> r0 = r4.g
            if (r0 == 0) goto L_0x006c
            java.lang.Object r0 = r0.a()
            com.fossil.g55 r0 = (com.fossil.g55) r0
            if (r0 == 0) goto L_0x006b
            r1 = 1
            r2 = 0
            if (r5 == 0) goto L_0x004f
            com.portfolio.platform.view.FlexibleTextInputEditText r5 = r0.s
            java.lang.String r3 = "it.etFirstName"
            com.fossil.ee7.a(r5, r3)
            android.text.Editable r5 = r5.getEditableText()
            java.lang.String r3 = "it.etFirstName.editableText"
            com.fossil.ee7.a(r5, r3)
            java.lang.CharSequence r5 = com.fossil.nh7.d(r5)
            int r5 = r5.length()
            if (r5 <= 0) goto L_0x002c
            r5 = 1
            goto L_0x002d
        L_0x002c:
            r5 = 0
        L_0x002d:
            if (r5 == 0) goto L_0x004f
            com.portfolio.platform.view.FlexibleTextInputEditText r5 = r0.t
            java.lang.String r3 = "it.etLastName"
            com.fossil.ee7.a(r5, r3)
            android.text.Editable r5 = r5.getEditableText()
            java.lang.String r3 = "it.etLastName.editableText"
            com.fossil.ee7.a(r5, r3)
            java.lang.CharSequence r5 = com.fossil.nh7.d(r5)
            int r5 = r5.length()
            if (r5 <= 0) goto L_0x004b
            r5 = 1
            goto L_0x004c
        L_0x004b:
            r5 = 0
        L_0x004c:
            if (r5 == 0) goto L_0x004f
            goto L_0x0050
        L_0x004f:
            r1 = 0
        L_0x0050:
            com.portfolio.platform.view.ProgressButton r5 = r0.K
            java.lang.String r2 = "it.save"
            com.fossil.ee7.a(r5, r2)
            r5.setEnabled(r1)
            if (r1 == 0) goto L_0x0064
            com.portfolio.platform.view.ProgressButton r5 = r0.K
            java.lang.String r0 = "flexible_button_primary"
            r5.a(r0)
            goto L_0x006b
        L_0x0064:
            com.portfolio.platform.view.ProgressButton r5 = r0.K
            java.lang.String r0 = "flexible_button_disabled"
            r5.a(r0)
        L_0x006b:
            return
        L_0x006c:
            java.lang.String r5 = "mBinding"
            com.fossil.ee7.d(r5)
            r5 = 0
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.li6.T(boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                ee7.a((Object) requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    oi6 oi6 = this.f;
                    if (oi6 != null) {
                        if (oi6.c()) {
                            bx6 bx6 = bx6.c;
                            FragmentManager childFragmentManager = getChildFragmentManager();
                            ee7.a((Object) childFragmentManager, "childFragmentManager");
                            bx6.V(childFragmentManager);
                        } else {
                            FragmentActivity activity = getActivity();
                            if (activity != null) {
                                activity.finish();
                            }
                        }
                        return true;
                    }
                    ee7.d("mViewModel");
                    throw null;
                }
            }
        }
        return true;
    }

    @DexIgnore
    public final void f1() {
        oi6 oi6 = this.f;
        if (oi6 != null) {
            oi6.f();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void j() {
        ProgressButton progressButton;
        qw6<g55> qw6 = this.g;
        if (qw6 != null) {
            g55 a2 = qw6.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.c();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            oi6 oi6 = this.f;
            if (oi6 != null) {
                oi6.a(intent);
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        g55 g55 = (g55) qb.a(LayoutInflater.from(getContext()), 2131558609, null, false, a1());
        jo5 jo5 = (jo5) getChildFragmentManager().b(jo5.t.a());
        this.i = jo5;
        if (jo5 == null) {
            this.i = jo5.t.b();
        }
        tj4 f2 = PortfolioApp.g0.c().f();
        jo5 jo52 = this.i;
        if (jo52 != null) {
            f2.a(new qq6(jo52)).a(this);
            rj4 rj4 = this.p;
            if (rj4 != null) {
                he a2 = je.a(this, rj4).a(oi6.class);
                ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ditViewModel::class.java)");
                this.f = (oi6) a2;
                he a3 = je.a(requireActivity()).a(yz6.class);
                ee7.a((Object) a3, "ViewModelProviders.of(re\u2026DayViewModel::class.java)");
                yz6 yz6 = (yz6) a3;
                this.j = yz6;
                if (yz6 != null) {
                    yz6.a().a(getViewLifecycleOwner(), new f(this));
                    g55.F.setOnClickListener(new g(this));
                    g55.s.addTextChangedListener(new h(this));
                    g55.t.addTextChangedListener(new i(this));
                    g55.K.setOnClickListener(new j(this));
                    FossilCircleImageView fossilCircleImageView = g55.q;
                    ee7.a((Object) fossilCircleImageView, "binding.avatar");
                    bf5.a(fossilCircleImageView, new k(this));
                    g55.I.setValuePickerListener(new l(this, g55));
                    g55.J.setValuePickerListener(new m(this, g55));
                    g55.v.setOnClickListener(new n(this));
                    g55.u.setOnClickListener(new b(this));
                    g55.w.setOnClickListener(new c(this));
                    g55.L.setOnClickListener(new d(this));
                    oi6 oi6 = this.f;
                    if (oi6 != null) {
                        oi6.a().a(getViewLifecycleOwner(), new e(this));
                        this.g = new qw6<>(this, g55);
                        ee7.a((Object) g55, "binding");
                        return g55.d();
                    }
                    ee7.d("mViewModel");
                    throw null;
                }
                ee7.d("mUserBirthDayViewModel");
                throw null;
            }
            ee7.d("viewModelFactory");
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        oi6 oi6 = this.f;
        if (oi6 != null) {
            oi6.d();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        V("edit_profile_view");
        kd5 a2 = hd5.a(this);
        ee7.a((Object) a2, "GlideApp.with(this)");
        this.h = a2;
    }

    @DexIgnore
    public final void v() {
        if (getActivity() != null) {
            FragmentActivity requireActivity = requireActivity();
            ee7.a((Object) requireActivity, "requireActivity()");
            if (!requireActivity.isFinishing()) {
                FragmentActivity requireActivity2 = requireActivity();
                ee7.a((Object) requireActivity2, "requireActivity()");
                if (!requireActivity2.isDestroyed()) {
                    requireActivity().finish();
                }
            }
        }
    }

    @DexIgnore
    @SuppressLint({"SimpleDateFormat"})
    public final void b(MFUser mFUser) {
        FlexibleTextInputEditText flexibleTextInputEditText;
        FlexibleTextView flexibleTextView;
        FlexibleTextInputEditText flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3;
        FossilCircleImageView fossilCircleImageView;
        FossilCircleImageView fossilCircleImageView2;
        FossilCircleImageView fossilCircleImageView3;
        FossilCircleImageView fossilCircleImageView4;
        String profilePicture = mFUser.getProfilePicture();
        String str = mFUser.getFirstName() + " " + mFUser.getLastName();
        if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
            kd5 kd5 = this.h;
            if (kd5 != null) {
                jd5<Drawable> a2 = kd5.a((Object) new gd5("", str)).a(new r40().a((ex<Bitmap>) new vd5()));
                qw6<g55> qw6 = this.g;
                if (qw6 != null) {
                    g55 a3 = qw6.a();
                    FossilCircleImageView fossilCircleImageView5 = a3 != null ? a3.q : null;
                    if (fossilCircleImageView5 != null) {
                        a2.a((ImageView) fossilCircleImageView5);
                        qw6<g55> qw62 = this.g;
                        if (qw62 != null) {
                            g55 a4 = qw62.a();
                            if (!(a4 == null || (fossilCircleImageView3 = a4.q) == null)) {
                                fossilCircleImageView3.setBorderColor(v6.a(PortfolioApp.g0.c(), 2131099830));
                            }
                            qw6<g55> qw63 = this.g;
                            if (qw63 != null) {
                                g55 a5 = qw63.a();
                                if (!(a5 == null || (fossilCircleImageView2 = a5.q) == null)) {
                                    fossilCircleImageView2.setBorderWidth(3);
                                }
                                qw6<g55> qw64 = this.g;
                                if (qw64 != null) {
                                    g55 a6 = qw64.a();
                                    if (!(a6 == null || (fossilCircleImageView = a6.q) == null)) {
                                        fossilCircleImageView.setBackground(v6.c(PortfolioApp.g0.c(), 2131231270));
                                    }
                                } else {
                                    ee7.d("mBinding");
                                    throw null;
                                }
                            } else {
                                ee7.d("mBinding");
                                throw null;
                            }
                        } else {
                            ee7.d("mBinding");
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mBinding");
                    throw null;
                }
            } else {
                ee7.d("mGlideRequests");
                throw null;
            }
        } else {
            qw6<g55> qw65 = this.g;
            if (qw65 != null) {
                g55 a7 = qw65.a();
                if (!(a7 == null || (fossilCircleImageView4 = a7.q) == null)) {
                    kd5 kd52 = this.h;
                    if (kd52 != null) {
                        fossilCircleImageView4.a(kd52, profilePicture, str);
                    } else {
                        ee7.d("mGlideRequests");
                        throw null;
                    }
                }
            } else {
                ee7.d("mBinding");
                throw null;
            }
        }
        qw6<g55> qw66 = this.g;
        if (qw66 != null) {
            g55 a8 = qw66.a();
            if (!(a8 == null || (flexibleTextInputEditText3 = a8.s) == null)) {
                flexibleTextInputEditText3.setText(mFUser.getFirstName());
            }
            qw6<g55> qw67 = this.g;
            if (qw67 != null) {
                g55 a9 = qw67.a();
                if (!(a9 == null || (flexibleTextInputEditText2 = a9.t) == null)) {
                    flexibleTextInputEditText2.setText(mFUser.getLastName());
                }
                qw6<g55> qw68 = this.g;
                if (qw68 != null) {
                    g55 a10 = qw68.a();
                    if (!(a10 == null || (flexibleTextView = a10.M) == null)) {
                        flexibleTextView.setText(mFUser.getEmail());
                    }
                    ud5 ud5 = ud5.a;
                    fb5 a11 = fb5.Companion.a(mFUser.getGender());
                    String birthday = mFUser.getBirthday();
                    if (birthday != null) {
                        r87<Integer, Integer> a12 = ud5.a(a11, mFUser.getAge(birthday));
                        if (mFUser.getHeightInCentimeters() == 0) {
                            mFUser.setHeightInCentimeters(a12.getFirst().intValue());
                        }
                        if (mFUser.getHeightInCentimeters() > 0) {
                            int heightInCentimeters = mFUser.getHeightInCentimeters();
                            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                            String height = unitGroup != null ? unitGroup.getHeight() : null;
                            if (height != null) {
                                ob5 fromString = ob5.fromString(height);
                                ee7.a((Object) fromString, "Unit.fromString(user.unitGroup?.height!!)");
                                a(heightInCentimeters, fromString);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        if (mFUser.getWeightInGrams() == 0) {
                            mFUser.setWeightInGrams(a12.getSecond().intValue() * 1000);
                        }
                        if (mFUser.getWeightInGrams() > 0) {
                            int weightInGrams = mFUser.getWeightInGrams();
                            MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                            String weight = unitGroup2 != null ? unitGroup2.getWeight() : null;
                            if (weight != null) {
                                ob5 fromString2 = ob5.fromString(weight);
                                ee7.a((Object) fromString2, "Unit.fromString(user.unitGroup?.weight!!)");
                                b(weightInGrams, fromString2);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        String birthday2 = mFUser.getBirthday();
                        if (!TextUtils.isEmpty(birthday2)) {
                            try {
                                Date e2 = zd5.e(birthday2);
                                qw6<g55> qw69 = this.g;
                                if (qw69 != null) {
                                    g55 a13 = qw69.a();
                                    if (!(a13 == null || (flexibleTextInputEditText = a13.L) == null)) {
                                        flexibleTextInputEditText.setText(zd5.d(e2));
                                    }
                                } else {
                                    ee7.d("mBinding");
                                    throw null;
                                }
                            } catch (Exception e3) {
                                e3.printStackTrace();
                            }
                        }
                        b(fb5.Companion.a(mFUser.getGender()));
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h() {
        ProgressButton progressButton;
        qw6<g55> qw6 = this.g;
        if (qw6 != null) {
            g55 a2 = qw6.a();
            if (a2 != null && (progressButton = a2.K) != null) {
                progressButton.b();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        jo5 jo5 = this.i;
        if (jo5 != null) {
            jo5.setArguments(bundle);
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            jo5.show(childFragmentManager, jo5.t.a());
        }
    }

    @DexIgnore
    public final void a(int i2, ob5 ob5) {
        int i3 = mi6.a[ob5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = s;
            local.d(str, "updateData height=" + i2 + " metric");
            qw6<g55> qw6 = this.g;
            if (qw6 != null) {
                g55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    ee7.a((Object) flexibleTextView, "it.ftvHeightUnit");
                    flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131887147));
                    a2.I.setUnit(ob5.METRIC);
                    a2.I.setFormatter(new ProfileFormatter(-1));
                    a2.I.a(100, 251, i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            qw6<g55> qw62 = this.g;
            if (qw62 != null) {
                g55 a3 = qw62.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.z;
                    ee7.a((Object) flexibleTextView2, "it.ftvHeightUnit");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131887148));
                    r87<Integer, Integer> b2 = xd5.b((float) i2);
                    a3.I.setUnit(ob5.IMPERIAL);
                    a3.I.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.I;
                    Integer first = b2.getFirst();
                    ee7.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = xd5.a(first.intValue());
                    Integer second = b2.getSecond();
                    ee7.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(Uri uri) {
        if (isActive()) {
            kd5 kd5 = this.h;
            if (kd5 != null) {
                jd5<Drawable> a2 = kd5.a(uri).a(iy.a).a(new r40().a((ex<Bitmap>) new vd5()));
                qw6<g55> qw6 = this.g;
                if (qw6 != null) {
                    g55 a3 = qw6.a();
                    FossilCircleImageView fossilCircleImageView = a3 != null ? a3.q : null;
                    if (fossilCircleImageView != null) {
                        a2.a((ImageView) fossilCircleImageView);
                        T(true);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.d("mBinding");
                throw null;
            }
            ee7.d("mGlideRequests");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        ee7.b(intent, "data");
        if (str.hashCode() != -1375614559 || !str.equals("UNSAVED_CHANGE")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).a(str, i2, intent);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        } else if (i2 == 2131363229) {
            v();
        } else if (i2 == 2131363307) {
            f1();
        }
    }

    @DexIgnore
    public final void b(int i2, ob5 ob5) {
        int i3 = mi6.b[ob5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = s;
            local.d(str, "updateData weight=" + i2 + " metric");
            qw6<g55> qw6 = this.g;
            if (qw6 != null) {
                g55 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B;
                    ee7.a((Object) flexibleTextView, "it.ftvWeightUnit");
                    flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131886901));
                    a2.J.setUnit(ob5.METRIC);
                    a2.J.setFormatter(new ProfileFormatter(4));
                    a2.J.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            qw6<g55> qw62 = this.g;
            if (qw62 != null) {
                g55 a3 = qw62.a();
                if (a3 != null) {
                    FlexibleTextView flexibleTextView2 = a3.B;
                    ee7.a((Object) flexibleTextView2, "it.ftvWeightUnit");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131886902));
                    float f2 = xd5.f((float) i2);
                    a3.J.setUnit(ob5.IMPERIAL);
                    a3.J.setFormatter(new ProfileFormatter(4));
                    a3.J.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2, String str) {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    public final void b(fb5 fb5) {
        qw6<g55> qw6 = this.g;
        if (qw6 != null) {
            g55 a2 = qw6.a();
            if (a2 != null) {
                a2.u.a("flexible_button_secondary");
                a2.v.a("flexible_button_secondary");
                a2.w.a("flexible_button_secondary");
                int i2 = mi6.c[fb5.ordinal()];
                if (i2 == 1) {
                    qw6<g55> qw62 = this.g;
                    if (qw62 != null) {
                        g55 a3 = qw62.a();
                        if (a3 != null && a3.u != null) {
                            a2.u.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                } else if (i2 != 2) {
                    qw6<g55> qw63 = this.g;
                    if (qw63 != null) {
                        g55 a4 = qw63.a();
                        if (a4 != null && a4.w != null) {
                            a2.w.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                } else {
                    qw6<g55> qw64 = this.g;
                    if (qw64 != null) {
                        g55 a5 = qw64.a();
                        if (a5 != null && a5.v != null) {
                            a2.v.a("flexible_button_primary");
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                }
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }
}
