package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jr1 extends v61 {
    @DexIgnore
    public byte[] E;
    @DexIgnore
    public long F;
    @DexIgnore
    public float G;
    @DexIgnore
    public long H;
    @DexIgnore
    public long I;
    @DexIgnore
    public boolean J;
    @DexIgnore
    public int K;
    @DexIgnore
    public qk1 L;
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public long O;
    @DexIgnore
    public long P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ float R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jr1(ri1 ri1, en0 en0, wm0 wm0, boolean z, short s, float f, String str, boolean z2, int i) {
        super(ri1, en0, wm0, s, (i & 64) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, (i & 128) != 0 ? true : z2);
        float f2 = (i & 32) != 0 ? 0.001f : f;
        this.Q = z;
        this.R = f2;
        this.E = new byte[0];
        this.J = true;
        this.L = qk1.UNKNOWN;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        if (!(!(this.E.length == 0))) {
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
        } else if (this.Q) {
            zk0.a(this, qa1.l, null, 2, null);
        } else {
            zk0.a(this, qa1.n, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61
    public JSONObject i() {
        return yz0.a(yz0.a(yz0.a(super.i(), r51.H, Integer.valueOf(this.E.length)), r51.I, Long.valueOf(ik1.a.a(this.E, ng1.CRC32))), r51.v0, Boolean.valueOf(this.Q));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void j() {
        try {
            this.E = n();
            this.F = 0;
            this.H = 0;
            this.I = 0;
            this.J = true;
            this.K = 0;
            this.L = qk1.UNKNOWN;
        } catch (f41 e) {
            wl0.h.a(e);
            a(eu0.a(((zk0) this).v, null, is0.UNSUPPORTED_FORMAT, null, 5));
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(yz0.a(super.k(), r51.m4, Long.valueOf(Math.max(this.N - this.M, 0L))), r51.n4, Long.valueOf(Math.max(this.P - this.O, 0L)));
    }

    @DexIgnore
    public final nr1 m() {
        if (this.N == 0) {
            this.N = System.currentTimeMillis();
        }
        if (this.O == 0) {
            this.O = System.currentTimeMillis();
        }
        long o = o();
        long j = this.I;
        long j2 = o - j;
        short s = ((v61) this).D;
        byte[] a = s97.a(this.E, (int) j, (int) o());
        ri1 ri1 = ((zk0) this).w;
        nr1 nr1 = new nr1(s, new qz0(a, Math.min(ri1.j, ri1.p), this.L), ((zk0) this).w);
        uf1 uf1 = new uf1(this, j2);
        if (!((v81) nr1).t) {
            ((v81) nr1).o.add(uf1);
        }
        nr1.b(new qh1(this));
        nr1.a(new oj1(this));
        nr1 nr12 = nr1;
        ((v81) nr12).s = c();
        return nr12;
    }

    @DexIgnore
    public abstract byte[] n();

    @DexIgnore
    public final long o() {
        return yz0.b(this.E.length);
    }

    @DexIgnore
    public nw0 p() {
        return new nw0(((v61) this).D, qa1.m, ((zk0) this).w, 0, 8);
    }

    @DexIgnore
    public void q() {
        a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        if (r0 < 3) goto L_0x0008;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void r() {
        /*
            r7 = this;
            boolean r0 = r7.J
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x000a
            r7.J = r1
        L_0x0008:
            r1 = 1
            goto L_0x002d
        L_0x000a:
            long r3 = r7.I
            long r5 = r7.H
            long r3 = r3 - r5
            float r0 = (float) r3
            r3 = 1065353216(0x3f800000, float:1.0)
            float r0 = r0 * r3
            long r3 = r7.o()
            float r3 = (float) r3
            float r0 = r0 / r3
            r3 = 1008981770(0x3c23d70a, float:0.01)
            int r0 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0024
            r7.K = r1
            goto L_0x0008
        L_0x0024:
            int r0 = r7.K
            int r0 = r0 + r2
            r7.K = r0
            r3 = 3
            if (r0 >= r3) goto L_0x002d
            goto L_0x0008
        L_0x002d:
            r0 = 0
            if (r1 == 0) goto L_0x0037
            com.fossil.qa1 r1 = com.fossil.qa1.l
            r2 = 2
            com.fossil.zk0.a(r7, r1, r0, r2, r0)
            goto L_0x0043
        L_0x0037:
            com.fossil.eu0 r1 = r7.v
            com.fossil.is0 r2 = com.fossil.is0.DATA_TRANSFER_RETRY_REACH_THRESHOLD
            r3 = 5
            com.fossil.eu0 r0 = com.fossil.eu0.a(r1, r0, r2, r0, r3)
            r7.a(r0)
        L_0x0043:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jr1.r():void");
    }

    @DexIgnore
    public final void a(vo1 vo1) {
        a(vo1.D);
        long j = this.I;
        this.F = j;
        float o = (((float) j) * 1.0f) / ((float) o());
        if (Math.abs(o - this.G) > this.R || o == 1.0f) {
            this.G = o;
            a(o);
        }
        zk0.a(this, qa1.l, null, 2, null);
    }

    @DexIgnore
    public static final /* synthetic */ void a(jr1 jr1, long j, long j2) {
        long o = jr1.o();
        if (1 <= j && o >= j) {
            long a = ik1.a.a(jr1.E, ng1.CRC32);
            jr1.a(j);
            if (j2 != a) {
                ri1 ri1 = ((zk0) jr1).w;
                en0 en0 = ((zk0) jr1).x;
                String str = ri1.u;
                short s = ((v61) jr1).D;
                byte[] bArr = jr1.E;
                zk0.a(jr1, new vo1(ri1, en0, new bi1(str, bi1.CREATOR.b(s), bi1.CREATOR.a(s), bArr, yz0.b(bArr.length), ik1.a.a(jr1.E, ng1.CRC32), 0, false), ((zk0) jr1).z), new kn1(jr1), new kp1(jr1), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
            } else if (j == jr1.o()) {
                zk0.a(jr1, qa1.m, null, 2, null);
            } else {
                jr1.r();
            }
        } else {
            jr1.a(0L);
            jr1.r();
        }
    }

    @DexIgnore
    public final void a(long j) {
        this.H = this.I;
        this.I = j;
        this.F = j;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public v81 a(qa1 qa1) {
        int i = ma1.a[qa1.ordinal()];
        if (i == 1) {
            nn0 nn0 = new nn0(((v61) this).D, ((zk0) this).w, 0, 4);
            nn0.b(new gc1(this));
            return nn0;
        } else if (i == 2) {
            if (this.M == 0) {
                this.M = System.currentTimeMillis();
            }
            at0 at0 = new at0(this.I, o() - this.I, o(), ((v61) this).D, ((zk0) this).w, 0, 32);
            at0.b(new ae1(this));
            return at0;
        } else if (i == 3) {
            nw0 p = p();
            p.b(new ll1(this));
            return p;
        } else if (i != 4) {
            return null;
        } else {
            return m();
        }
    }
}
