package com.fossil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ss2<K, V> implements Serializable, Map<K, V> {
    @DexIgnore
    public transient ws2<Map.Entry<K, V>> a;
    @DexIgnore
    public transient ws2<K> b;
    @DexIgnore
    public transient ps2<V> c;

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean containsKey(@NullableDecl Object obj) {
        return get(obj) != null;
    }

    @DexIgnore
    public boolean containsValue(@NullableDecl Object obj) {
        return ((ps2) values()).contains(obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public /* synthetic */ Set entrySet() {
        ws2<Map.Entry<K, V>> ws2 = this.a;
        if (ws2 != null) {
            return ws2;
        }
        ws2<Map.Entry<K, V>> zza = zza();
        this.a = zza;
        return zza;
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Map
    public abstract V get(@NullableDecl Object obj);

    @DexIgnore
    @Override // java.util.Map
    public final V getOrDefault(@NullableDecl Object obj, @NullableDecl V v) {
        V v2 = get(obj);
        return v2 != null ? v2 : v;
    }

    @DexIgnore
    public int hashCode() {
        return ut2.a((ws2) entrySet());
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    @Override // java.util.Map
    public /* synthetic */ Set keySet() {
        ws2<K> ws2 = this.b;
        if (ws2 != null) {
            return ws2;
        }
        ws2<K> zzb = zzb();
        this.b = zzb;
        return zzb;
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.Map
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        int size = size();
        if (size >= 0) {
            StringBuilder sb = new StringBuilder((int) Math.min(((long) size) << 3, 1073741824L));
            sb.append('{');
            boolean z = true;
            for (Map.Entry<K, V> entry : entrySet()) {
                if (!z) {
                    sb.append(", ");
                }
                z = false;
                sb.append((Object) entry.getKey());
                sb.append('=');
                sb.append((Object) entry.getValue());
            }
            sb.append('}');
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder("size".length() + 40);
        sb2.append("size");
        sb2.append(" cannot be negative but was: ");
        sb2.append(size);
        throw new IllegalArgumentException(sb2.toString());
    }

    @DexIgnore
    @Override // java.util.Map
    public /* synthetic */ Collection values() {
        ps2<V> ps2 = this.c;
        if (ps2 != null) {
            return ps2;
        }
        ps2<V> zzc = zzc();
        this.c = zzc;
        return zzc;
    }

    @DexIgnore
    public abstract ws2<Map.Entry<K, V>> zza();

    @DexIgnore
    public abstract ws2<K> zzb();

    @DexIgnore
    public abstract ps2<V> zzc();
}
