package com.fossil;

import com.fossil.ix;
import com.fossil.m00;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u00<Model> implements m00<Model, Model> {
    @DexIgnore
    public static /* final */ u00<?> a; // = new u00<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Model> implements n00<Model, Model> {
        @DexIgnore
        public static /* final */ a<?> a; // = new a<>();

        @DexIgnore
        public static <T> a<T> a() {
            return (a<T>) a;
        }

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Model, Model> a(q00 q00) {
            return u00.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model> implements ix<Model> {
        @DexIgnore
        public /* final */ Model a;

        @DexIgnore
        public b(Model model) {
            this.a = model;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super Model> aVar) {
            aVar.a((Object) this.a);
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<Model> getDataClass() {
            return (Class<Model>) this.a.getClass();
        }
    }

    @DexIgnore
    public static <T> u00<T> a() {
        return (u00<T>) a;
    }

    @DexIgnore
    @Override // com.fossil.m00
    public boolean a(Model model) {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m00
    public m00.a<Model> a(Model model, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(model), new b(model));
    }
}
