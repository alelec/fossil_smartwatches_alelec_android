package com.fossil;

import android.location.Location;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl2 extends j63 {
    @DexIgnore
    public /* final */ y12<d53> a;

    @DexIgnore
    public wl2(y12<d53> y12) {
        this.a = y12;
    }

    @DexIgnore
    public final synchronized void E() {
        this.a.a();
    }

    @DexIgnore
    @Override // com.fossil.i63
    public final synchronized void onLocationChanged(Location location) {
        this.a.a(new xl2(this, location));
    }
}
