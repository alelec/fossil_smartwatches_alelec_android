package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jw4 extends iw4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        w = sparseIntArray;
        sparseIntArray.put(2131362458, 1);
        w.put(2131362647, 2);
        w.put(2131363388, 3);
    }
    */

    @DexIgnore
    public jw4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 4, v, w));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public jw4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[1], (RTLImageView) objArr[2], (View) objArr[3]);
        this.u = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.t = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
