package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zt3 extends yt3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends dv3 {
        @DexIgnore
        public a(hv3 hv3) {
            super(hv3);
        }

        @DexIgnore
        @Override // com.fossil.dv3
        public boolean isStateful() {
            return true;
        }
    }

    @DexIgnore
    public zt3(FloatingActionButton floatingActionButton, wu3 wu3) {
        super(floatingActionButton, wu3);
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void a(ColorStateList colorStateList, PorterDuff.Mode mode, ColorStateList colorStateList2, int i) {
        Drawable drawable;
        dv3 a2 = a();
        ((yt3) this).b = a2;
        a2.setTintList(colorStateList);
        if (mode != null) {
            ((yt3) this).b.setTintMode(mode);
        }
        ((yt3) this).b.a(((yt3) this).y.getContext());
        if (i > 0) {
            ((yt3) this).d = a(i, colorStateList);
            xt3 xt3 = ((yt3) this).d;
            e9.a(xt3);
            dv3 dv3 = ((yt3) this).b;
            e9.a(dv3);
            drawable = new LayerDrawable(new Drawable[]{xt3, dv3});
        } else {
            ((yt3) this).d = null;
            drawable = ((yt3) this).b;
        }
        RippleDrawable rippleDrawable = new RippleDrawable(uu3.b(colorStateList2), drawable, null);
        ((yt3) this).c = rippleDrawable;
        ((yt3) this).e = rippleDrawable;
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void b(ColorStateList colorStateList) {
        Drawable drawable = ((yt3) this).c;
        if (drawable instanceof RippleDrawable) {
            ((RippleDrawable) drawable).setColor(uu3.b(colorStateList));
        } else {
            super.b(colorStateList);
        }
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public float e() {
        return ((yt3) this).y.getElevation();
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void o() {
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void q() {
        B();
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public boolean v() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public boolean w() {
        return ((yt3) this).z.a() || !y();
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void z() {
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void a(float f, float f2, float f3) {
        if (Build.VERSION.SDK_INT == 21) {
            ((yt3) this).y.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(yt3.G, a(f, f3));
            stateListAnimator.addState(yt3.H, a(f, f2));
            stateListAnimator.addState(yt3.I, a(f, f2));
            stateListAnimator.addState(yt3.J, a(f, f2));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(((yt3) this).y, "elevation", f).setDuration(0L));
            int i = Build.VERSION.SDK_INT;
            if (i >= 22 && i <= 24) {
                FloatingActionButton floatingActionButton = ((yt3) this).y;
                arrayList.add(ObjectAnimator.ofFloat(floatingActionButton, View.TRANSLATION_Z, floatingActionButton.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(((yt3) this).y, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(yt3.F);
            stateListAnimator.addState(yt3.K, animatorSet);
            stateListAnimator.addState(yt3.L, a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES));
            ((yt3) this).y.setStateListAnimator(stateListAnimator);
        }
        if (w()) {
            B();
        }
    }

    @DexIgnore
    public final Animator a(float f, float f2) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(((yt3) this).y, "elevation", f).setDuration(0L)).with(ObjectAnimator.ofFloat(((yt3) this).y, View.TRANSLATION_Z, f2).setDuration(100L));
        animatorSet.setInterpolator(yt3.F);
        return animatorSet;
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (((yt3) this).y.isEnabled()) {
            ((yt3) this).y.setElevation(((yt3) this).h);
            if (((yt3) this).y.isPressed()) {
                ((yt3) this).y.setTranslationZ(((yt3) this).j);
            } else if (((yt3) this).y.isFocused() || ((yt3) this).y.isHovered()) {
                ((yt3) this).y.setTranslationZ(((yt3) this).i);
            } else {
                ((yt3) this).y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        } else {
            ((yt3) this).y.setElevation(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            ((yt3) this).y.setTranslationZ(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }

    @DexIgnore
    public xt3 a(int i, ColorStateList colorStateList) {
        Context context = ((yt3) this).y.getContext();
        hv3 hv3 = ((yt3) this).a;
        e9.a(hv3);
        xt3 xt3 = new xt3(hv3);
        xt3.a(v6.a(context, kr3.design_fab_stroke_top_outer_color), v6.a(context, kr3.design_fab_stroke_top_inner_color), v6.a(context, kr3.design_fab_stroke_end_inner_color), v6.a(context, kr3.design_fab_stroke_end_outer_color));
        xt3.a((float) i);
        xt3.a(colorStateList);
        return xt3;
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public dv3 a() {
        hv3 hv3 = ((yt3) this).a;
        e9.a(hv3);
        return new a(hv3);
    }

    @DexIgnore
    @Override // com.fossil.yt3
    public void a(Rect rect) {
        if (((yt3) this).z.a()) {
            super.a(rect);
        } else if (!y()) {
            int sizeDimension = (((yt3) this).k - ((yt3) this).y.getSizeDimension()) / 2;
            rect.set(sizeDimension, sizeDimension, sizeDimension, sizeDimension);
        } else {
            rect.set(0, 0, 0, 0);
        }
    }
}
