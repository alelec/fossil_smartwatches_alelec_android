package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hw6 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public ActivitiesRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.MigrateDataActivitiesAndSleeps", f = "MigrateDataActivitiesAndSleeps.kt", l = {46}, m = VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE)
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ hw6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(hw6 hw6, fb7 fb7) {
            super(fb7);
            this.this$0 = hw6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = hw6.class.getSimpleName();
        ee7.a((Object) simpleName, "MigrateDataActivitiesAnd\u2026ps::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public hw6() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0044  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x010d A[LOOP:2: B:35:0x0107->B:37:0x010d, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r12) {
        /*
            r11 = this;
            boolean r0 = r12 instanceof com.fossil.hw6.b
            if (r0 == 0) goto L_0x0013
            r0 = r12
            com.fossil.hw6$b r0 = (com.fossil.hw6.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.hw6$b r0 = new com.fossil.hw6$b
            r0.<init>(r11, r12)
        L_0x0018:
            java.lang.Object r12 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "pin"
            r4 = 1
            if (r2 == 0) goto L_0x0044
            if (r2 != r4) goto L_0x003c
            java.lang.Object r1 = r0.L$3
            java.util.ArrayList r1 = (java.util.ArrayList) r1
            java.lang.Object r1 = r0.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r1 = r0.L$1
            com.fossil.gi5 r1 = (com.fossil.gi5) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.hw6 r0 = (com.fossil.hw6) r0
            com.fossil.t87.a(r12)
            goto L_0x00b3
        L_0x003c:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r0)
            throw r12
        L_0x0044:
            com.fossil.t87.a(r12)
            com.fossil.jx6 r12 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            java.lang.String r12 = r12.f(r2)
            boolean r12 = android.text.TextUtils.isEmpty(r12)
            if (r12 != 0) goto L_0x0126
            com.fossil.ah5$a r12 = com.fossil.ah5.p
            com.fossil.ah5 r12 = r12.a()
            com.fossil.gi5 r12 = r12.i()
            java.lang.String r2 = "FitnessSample"
            java.util.List r2 = r12.b(r2)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r6 = r2.iterator()
        L_0x0074:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x009d
            java.lang.Object r7 = r6.next()
            com.portfolio.platform.data.model.PinObject r7 = (com.portfolio.platform.data.model.PinObject) r7
            com.google.gson.Gson r8 = new com.google.gson.Gson
            r8.<init>()
            com.fossil.ee7.a(r7, r3)
            java.lang.String r9 = r7.getJsonData()
            java.lang.Class<com.portfolio.platform.data.model.room.fitness.SampleRaw> r10 = com.portfolio.platform.data.model.room.fitness.SampleRaw.class
            java.lang.Object r8 = r8.a(r9, r10)
            com.portfolio.platform.data.model.room.fitness.SampleRaw r8 = (com.portfolio.platform.data.model.room.fitness.SampleRaw) r8
            if (r8 == 0) goto L_0x0099
            r5.add(r8)
        L_0x0099:
            r12.a(r7)
            goto L_0x0074
        L_0x009d:
            com.portfolio.platform.data.source.ActivitiesRepository r6 = r11.a
            if (r6 == 0) goto L_0x011f
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r2
            r0.L$3 = r5
            r0.label = r4
            java.lang.Object r0 = r6.updateActivityPinType(r5, r4, r0)
            if (r0 != r1) goto L_0x00b2
            return r1
        L_0x00b2:
            r1 = r12
        L_0x00b3:
            java.lang.String r12 = "MFSleepSessionUpload"
            java.util.List r12 = r1.b(r12)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r12 = r12.iterator()
        L_0x00c2:
            boolean r2 = r12.hasNext()
            if (r2 == 0) goto L_0x00f9
            java.lang.Object r2 = r12.next()
            com.portfolio.platform.data.model.PinObject r2 = (com.portfolio.platform.data.model.PinObject) r2
            com.fossil.be4 r5 = new com.fossil.be4
            r5.<init>()
            java.lang.Class<org.joda.time.DateTime> r6 = org.joda.time.DateTime.class
            com.portfolio.platform.helper.GsonConvertDateTime r7 = new com.portfolio.platform.helper.GsonConvertDateTime
            r7.<init>()
            r5.a(r6, r7)
            com.google.gson.Gson r5 = r5.a()
            com.fossil.ee7.a(r2, r3)
            java.lang.String r6 = r2.getJsonData()
            java.lang.Class<com.fossil.wearables.fsl.sleep.MFSleepSession> r7 = com.fossil.wearables.fsl.sleep.MFSleepSession.class
            java.lang.Object r5 = r5.a(r6, r7)
            com.fossil.wearables.fsl.sleep.MFSleepSession r5 = (com.fossil.wearables.fsl.sleep.MFSleepSession) r5
            if (r5 == 0) goto L_0x00f5
            r0.add(r5)
        L_0x00f5:
            r1.a(r2)
            goto L_0x00c2
        L_0x00f9:
            com.fossil.ah5$a r12 = com.fossil.ah5.p
            com.fossil.ah5 r12 = r12.a()
            com.fossil.wearables.fsl.sleep.MFSleepSessionProvider r12 = r12.l()
            java.util.Iterator r0 = r0.iterator()
        L_0x0107:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0133
            java.lang.Object r1 = r0.next()
            com.fossil.wearables.fsl.sleep.MFSleepSession r1 = (com.fossil.wearables.fsl.sleep.MFSleepSession) r1
            java.lang.String r2 = "sleepSession"
            com.fossil.ee7.a(r1, r2)
            r1.setPinType(r4)
            r12.updateSleepSessionPinType(r1, r4)
            goto L_0x0107
        L_0x011f:
            java.lang.String r12 = "mActivityRepository"
            com.fossil.ee7.d(r12)
            r12 = 0
            throw r12
        L_0x0126:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r0 = com.fossil.hw6.b
            java.lang.String r1 = "There is no user here. No need to migrate"
            r12.d(r0, r1)
        L_0x0133:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.hw6.a(com.fossil.fb7):java.lang.Object");
    }
}
