package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pc7 extends oc7 {
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00a8, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00a9, code lost:
        com.fossil.hc7.a(r0, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00ac, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final byte[] a(java.io.File r10) {
        /*
            java.lang.String r0 = "$this$readBytes"
            com.fossil.ee7.b(r10, r0)
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r10)
            long r1 = r10.length()     // Catch:{ all -> 0x00a6 }
            r3 = 2147483647(0x7fffffff, float:NaN)
            long r3 = (long) r3
            java.lang.String r5 = "File "
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 > 0) goto L_0x0084
            int r2 = (int) r1
            byte[] r1 = new byte[r2]
            r3 = 0
            r4 = r2
            r6 = 0
        L_0x001e:
            if (r4 <= 0) goto L_0x002a
            int r7 = r0.read(r1, r6, r4)
            if (r7 >= 0) goto L_0x0027
            goto L_0x002a
        L_0x0027:
            int r4 = r4 - r7
            int r6 = r6 + r7
            goto L_0x001e
        L_0x002a:
            java.lang.String r7 = "java.util.Arrays.copyOf(this, newSize)"
            r8 = 0
            if (r4 <= 0) goto L_0x0037
            byte[] r1 = java.util.Arrays.copyOf(r1, r6)
            com.fossil.ee7.a(r1, r7)
            goto L_0x0066
        L_0x0037:
            int r4 = r0.read()
            r6 = -1
            if (r4 != r6) goto L_0x003f
            goto L_0x0066
        L_0x003f:
            com.fossil.jc7 r6 = new com.fossil.jc7
            r9 = 8193(0x2001, float:1.1481E-41)
            r6.<init>(r9)
            r6.write(r4)
            r4 = 2
            com.fossil.gc7.a(r0, r6, r3, r4, r8)
            int r4 = r6.size()
            int r4 = r4 + r2
            if (r4 < 0) goto L_0x006a
            byte[] r10 = r6.getBuffer()
            byte[] r1 = java.util.Arrays.copyOf(r1, r4)
            com.fossil.ee7.a(r1, r7)
            int r4 = r6.size()
            com.fossil.s97.a(r10, r1, r2, r3, r4)
        L_0x0066:
            com.fossil.hc7.a(r0, r8)
            return r1
        L_0x006a:
            java.lang.OutOfMemoryError r1 = new java.lang.OutOfMemoryError
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r5)
            r2.append(r10)
            java.lang.String r10 = " is too big to fit in memory."
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r1.<init>(r10)
            throw r1
        L_0x0084:
            java.lang.OutOfMemoryError r3 = new java.lang.OutOfMemoryError
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r5)
            r4.append(r10)
            java.lang.String r10 = " is too big ("
            r4.append(r10)
            r4.append(r1)
            java.lang.String r10 = " bytes) to fit in memory."
            r4.append(r10)
            java.lang.String r10 = r4.toString()
            r3.<init>(r10)
            throw r3
        L_0x00a6:
            r10 = move-exception
            throw r10     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r1 = move-exception
            com.fossil.hc7.a(r0, r10)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pc7.a(java.io.File):byte[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001f, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001b, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001c, code lost:
        com.fossil.hc7.a(r0, r1);
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(java.io.File r1, byte[] r2) {
        /*
            java.lang.String r0 = "$this$writeBytes"
            com.fossil.ee7.b(r1, r0)
            java.lang.String r0 = "array"
            com.fossil.ee7.b(r2, r0)
            java.io.FileOutputStream r0 = new java.io.FileOutputStream
            r0.<init>(r1)
            r0.write(r2)     // Catch:{ all -> 0x0019 }
            com.fossil.i97 r1 = com.fossil.i97.a     // Catch:{ all -> 0x0019 }
            r1 = 0
            com.fossil.hc7.a(r0, r1)
            return
        L_0x0019:
            r1 = move-exception
            throw r1     // Catch:{ all -> 0x001b }
        L_0x001b:
            r2 = move-exception
            com.fossil.hc7.a(r0, r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pc7.a(java.io.File, byte[]):void");
    }
}
