package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e37 extends v27 {
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;

    @DexIgnore
    public e37() {
    }

    @DexIgnore
    public e37(Bundle bundle) {
        a(bundle);
    }

    @DexIgnore
    @Override // com.fossil.v27
    public int a() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.v27
    public void a(Bundle bundle) {
        super.a(bundle);
        this.c = bundle.getString("_wxapi_sendauth_resp_token");
        this.d = bundle.getString("_wxapi_sendauth_resp_state");
        bundle.getString("_wxapi_sendauth_resp_url");
        bundle.getString("_wxapi_sendauth_resp_lang");
        bundle.getString("_wxapi_sendauth_resp_country");
    }
}
