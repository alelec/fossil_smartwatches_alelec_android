package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v81 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public String d;
    @DexIgnore
    public long e;
    @DexIgnore
    public wr1 f;
    @DexIgnore
    public /* final */ ArrayList<zq0> g;
    @DexIgnore
    public long h;
    @DexIgnore
    public dp0 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public eo0 k;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<v81, i97>> l;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<v81, i97>> m;
    @DexIgnore
    public CopyOnWriteArrayList<gd7<v81, i97>> n;
    @DexIgnore
    public CopyOnWriteArrayList<kd7<v81, Float, i97>> o;
    @DexIgnore
    public /* final */ vc7<i97> p;
    @DexIgnore
    public /* final */ b51 q;
    @DexIgnore
    public /* final */ y61 r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public sz0 v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public /* final */ qa1 x;
    @DexIgnore
    public /* final */ ri1 y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore
    public v81(qa1 qa1, ri1 ri1, int i2) {
        this.x = qa1;
        this.y = ri1;
        this.z = i2;
        this.a = yz0.a(qa1);
        this.b = yh0.a("UUID.randomUUID().toString()");
        this.c = "";
        this.d = "";
        this.e = System.currentTimeMillis();
        System.currentTimeMillis();
        this.f = new wr1(yz0.a(this.x), ci1.d, this.y.u, this.c, this.d, false, null, null, null, yz0.a(new JSONObject(), r51.e2, this.b), 448);
        this.g = new ArrayList<>();
        this.h = 6500;
        Looper myLooper = Looper.myLooper();
        myLooper = myLooper == null ? Looper.getMainLooper() : myLooper;
        if (myLooper != null) {
            this.j = new Handler(myLooper);
            this.l = new CopyOnWriteArrayList<>();
            this.m = new CopyOnWriteArrayList<>();
            this.n = new CopyOnWriteArrayList<>();
            this.o = new CopyOnWriteArrayList<>();
            this.p = new e31(this);
            this.q = new b51(this);
            this.r = new y61(this);
            this.v = new sz0(this.x, this.b, ay0.b, null, null, 24);
            this.w = true;
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public long a(rk1 rk1) {
        return 0;
    }

    @DexIgnore
    public void a(eo0 eo0) {
    }

    @DexIgnore
    public void a(pm1 pm1) {
        if (pm1.a == dd1.CONNECTED) {
            return;
        }
        if (pm1.b == xr0.j.a) {
            a(ay0.n);
        } else {
            a(ay0.f);
        }
    }

    @DexIgnore
    public final void b() {
        JSONObject jSONObject;
        byte[] bArr;
        if (this.t || this.u) {
            a();
            return;
        }
        eo0 d2 = d();
        this.k = d2;
        if (d2 != null) {
            wr1 wr1 = this.f;
            if (wr1 == null || (jSONObject = wr1.m) == null) {
                jSONObject = new JSONObject();
            }
            boolean z2 = false;
            JSONObject a2 = yz0.a(jSONObject, d2.a(false));
            JSONObject jSONObject2 = new JSONObject();
            boolean z3 = this instanceof nr1;
            if (z3) {
                bArr = ((nr1) this).G.d;
            } else {
                bArr = d2 instanceof ze1 ? ((ze1) d2).l : new byte[0];
            }
            if (bArr.length == 0) {
                z2 = true;
            }
            if (!z2) {
                int length = bArr.length;
                yz0.a(yz0.a(yz0.a(jSONObject2, r51.Q0, (length > 500 || (z3 && ee7.a(this.c, yz0.a(wm0.y)))) ? "" : yz0.a(bArr, (String) null, 1)), r51.R0, Integer.valueOf(length)), r51.S0, Long.valueOf(ik1.a.a(bArr, ng1.CRC32)));
            }
            JSONObject a3 = yz0.a(a2, jSONObject2);
            wr1 wr12 = this.f;
            if (wr12 != null) {
                wr12.m = a3;
            }
            ri1 ri1 = this.y;
            eo0 eo0 = this.k;
            if (eo0 != null) {
                ri1.a(eo0);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            f();
        }
    }

    @DexIgnore
    public void b(rk1 rk1) {
    }

    @DexIgnore
    public final Handler c() {
        return this.j;
    }

    @DexIgnore
    public abstract eo0 d();

    @DexIgnore
    public long e() {
        return this.h;
    }

    @DexIgnore
    public abstract void f();

    @DexIgnore
    public JSONObject g() {
        return new JSONObject();
    }

    @DexIgnore
    public JSONObject h() {
        return new JSONObject();
    }

    @DexIgnore
    public final void i() {
        dp0 dp0 = this.i;
        if (dp0 != null) {
            this.j.removeCallbacks(dp0);
        }
        dp0 dp02 = this.i;
        if (dp02 != null) {
            dp02.a = true;
        }
        this.i = null;
    }

    @DexIgnore
    public final void j() {
        wr1 wr1 = this.f;
        if (wr1 != null) {
            yz0.a(wr1.m, r51.s1, Double.valueOf(yz0.a(System.currentTimeMillis())));
            if (this.w) {
                wl0.h.a(wr1);
            }
            this.f = null;
        }
    }

    @DexIgnore
    public void c(eo0 eo0) {
        JSONObject jSONObject;
        this.v = sz0.a(this.v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        wr1 wr1 = this.f;
        if (wr1 != null) {
            wr1.i = true;
        }
        wr1 wr12 = this.f;
        if (!(wr12 == null || (jSONObject = wr12.m) == null)) {
            yz0.a(jSONObject, r51.j, yz0.a(ay0.a));
        }
        if (this.v.c == ay0.a) {
            a(eo0);
        }
    }

    @DexIgnore
    public void a(long j2) {
        this.h = j2;
    }

    @DexIgnore
    public final void a(float f2) {
        Iterator<T> it = this.o.iterator();
        while (it.hasNext()) {
            it.next().invoke(this, Float.valueOf(f2));
        }
    }

    @DexIgnore
    public final void a(ti1 ti1) {
        if (ti1 instanceof pm1) {
            a((pm1) ti1);
        } else if (ti1 instanceof rk1) {
            b((rk1) ti1);
        }
    }

    @DexIgnore
    public final void a() {
        if (!this.t) {
            this.t = true;
            this.u = false;
            i();
            this.y.f.remove(this.q);
            this.y.g.remove(this.r);
            j();
            if (this.w) {
                wl0 wl0 = wl0.h;
                Object[] array = this.g.toArray(new zq0[0]);
                if (array != null) {
                    zq0[] zq0Arr = (zq0[]) array;
                    JSONObject a2 = yz0.a(yz0.a(new JSONObject(), r51.j, c(this.v)), r51.e2, this.b);
                    sz0 sz0 = this.v;
                    if (sz0.c == ay0.a) {
                        yz0.a(a2, h(), false, 2);
                    } else {
                        yz0.a(a2, r51.N0, sz0.a());
                    }
                    if (!(zq0Arr.length == 0)) {
                        JSONArray jSONArray = new JSONArray();
                        for (zq0 zq0 : zq0Arr) {
                            jSONArray.put(zq0.a());
                        }
                        yz0.a(a2, r51.C0, jSONArray);
                    }
                    wl0.a(new wr1(yz0.a(this.x), ci1.e, this.y.u, this.c, this.d, ay0.a == this.v.c, null, null, null, a2, 448));
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            ay0 ay0 = ay0.a;
            sz0 sz02 = this.v;
            if (ay0 == sz02.c) {
                le0 le0 = le0.DEBUG;
                yz0.a(sz02.a(), h()).toString(2);
                Iterator<T> it = this.l.iterator();
                while (it.hasNext()) {
                    it.next().invoke(this);
                }
            } else {
                le0 le02 = le0.ERROR;
                yz0.a(sz02.a(), h()).toString(2);
                Iterator<T> it2 = this.m.iterator();
                while (it2.hasNext()) {
                    it2.next().invoke(this);
                }
            }
            Iterator<T> it3 = this.n.iterator();
            while (it3.hasNext()) {
                it3.next().invoke(this);
            }
        }
    }

    @DexIgnore
    public String c(sz0 sz0) {
        String logName;
        switch (m11.c[sz0.c.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                mw0 mw0 = sz0.e;
                return (mw0 == null || (logName = mw0.getLogName()) == null) ? yz0.a(ay0.t) : logName;
            case 8:
            case 9:
                return yz0.a(ay0.q);
            default:
                return b(sz0);
        }
    }

    @DexIgnore
    public void b(eo0 eo0) {
        JSONObject jSONObject;
        JSONObject a2;
        this.v = sz0.a(this.v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        wr1 wr1 = this.f;
        if (wr1 != null) {
            wr1.i = false;
        }
        wr1 wr12 = this.f;
        if (!(wr12 == null || (jSONObject = wr12.m) == null || (a2 = yz0.a(jSONObject, r51.j, b(this.v))) == null)) {
            yz0.a(a2, r51.N0, eo0.d.a());
        }
        j();
        a();
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ v81(qa1 qa1, ri1 ri1, int i2, int i3) {
        this(qa1, ri1, (i3 & 4) != 0 ? 3 : i2);
    }

    @DexIgnore
    public final v81 b(gd7<? super v81, i97> gd7) {
        if (!this.t) {
            this.l.add(gd7);
        } else if (this.v.c == ay0.a) {
            gd7.invoke(this);
        }
        return this;
    }

    @DexIgnore
    public String b(sz0 sz0) {
        ay0 ay0;
        if (m11.b[sz0.d.b.ordinal()] == 1) {
            return yz0.a(xr0.m.a(sz0.d.c.b));
        }
        switch (m11.a[sz0.c.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                ay0 = sz0.c;
                break;
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                ay0 = ay0.d;
                break;
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
                ay0 = ay0.t;
                break;
            default:
                ay0 = ay0.t;
                break;
        }
        return yz0.a(ay0);
    }

    @DexIgnore
    public final void a(zq0 zq0) {
        this.g.add(zq0);
    }

    @DexIgnore
    public final void a(sz0 sz0) {
        this.v = sz0;
        a();
    }

    @DexIgnore
    public final void a(vc7<i97> vc7) {
        i();
        if (e() > 0) {
            this.i = new dp0(this, vc7);
            le0 le0 = le0.DEBUG;
            e();
            dp0 dp0 = this.i;
            if (dp0 != null) {
                this.j.postDelayed(dp0, e());
            }
        }
    }

    @DexIgnore
    public final void a(ay0 ay0) {
        oi0 oi0;
        if (!this.t && !this.u) {
            le0 le0 = le0.DEBUG;
            this.u = true;
            this.v = sz0.a(this.v, null, null, ay0, null, null, 27);
            eo0 eo0 = this.k;
            if (eo0 == null || eo0.c) {
                a();
                return;
            }
            int i2 = m11.d[ay0.ordinal()];
            if (i2 == 1) {
                oi0 = oi0.e;
            } else if (i2 == 2) {
                oi0 = oi0.m;
            } else if (i2 != 3) {
                oi0 = oi0.l;
            } else {
                oi0 = oi0.f;
            }
            this.y.a(this.k, oi0);
        }
    }

    @DexIgnore
    public final v81 a(gd7<? super v81, i97> gd7) {
        if (!this.t) {
            this.m.add(gd7);
        } else if (this.v.c != ay0.a) {
            gd7.invoke(this);
        }
        return this;
    }
}
