package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fv4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<SleepDistribution> {
    }

    @DexIgnore
    public final String a(SleepDistribution sleepDistribution) {
        if (sleepDistribution == null) {
            return null;
        }
        try {
            return wc5.a(sleepDistribution);
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final SleepDistribution a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return (SleepDistribution) new Gson().a(str, new a().getType());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("SleepDistributionConverter", String.valueOf(i97.a));
            return null;
        }
    }
}
