package com.fossil;

import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq5 {
    @DexIgnore
    public /* final */ gq5 a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ArrayList<Alarm> c;
    @DexIgnore
    public /* final */ Alarm d;

    @DexIgnore
    public iq5(gq5 gq5, String str, ArrayList<Alarm> arrayList, Alarm alarm) {
        ee7.b(gq5, "mView");
        ee7.b(str, "mDeviceId");
        ee7.b(arrayList, "mAlarms");
        this.a = gq5;
        this.b = str;
        this.c = arrayList;
        this.d = alarm;
    }

    @DexIgnore
    public final Alarm a() {
        return this.d;
    }

    @DexIgnore
    public final ArrayList<Alarm> b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final gq5 d() {
        return this.a;
    }
}
