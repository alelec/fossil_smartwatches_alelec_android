package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a65 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ RTLImageView H;
    @DexIgnore
    public /* final */ RTLImageView I;
    @DexIgnore
    public /* final */ ImageView J;
    @DexIgnore
    public /* final */ RTLImageView K;
    @DexIgnore
    public /* final */ View L;
    @DexIgnore
    public /* final */ FlexibleProgressBar M;
    @DexIgnore
    public /* final */ ConstraintLayout N;
    @DexIgnore
    public /* final */ ViewPager2 O;
    @DexIgnore
    public /* final */ ViewPager2 P;
    @DexIgnore
    public /* final */ FlexibleTextView Q;
    @DexIgnore
    public /* final */ View R;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ TabLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public a65(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, LinearLayout linearLayout, ConstraintLayout constraintLayout5, TabLayout tabLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, RTLImageView rTLImageView, RTLImageView rTLImageView2, ImageView imageView, RTLImageView rTLImageView3, View view2, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout6, ViewPager2 viewPager2, ViewPager2 viewPager22, FlexibleTextView flexibleTextView11, View view3) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = constraintLayout3;
        this.t = constraintLayout4;
        this.u = linearLayout;
        this.v = constraintLayout5;
        this.w = tabLayout;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = flexibleTextView5;
        this.C = flexibleTextView6;
        this.D = flexibleTextView7;
        this.E = flexibleTextView8;
        this.F = flexibleTextView9;
        this.G = flexibleTextView10;
        this.H = rTLImageView;
        this.I = rTLImageView2;
        this.J = imageView;
        this.K = rTLImageView3;
        this.L = view2;
        this.M = flexibleProgressBar;
        this.N = constraintLayout6;
        this.O = viewPager2;
        this.P = viewPager22;
        this.Q = flexibleTextView11;
        this.R = view3;
    }
}
