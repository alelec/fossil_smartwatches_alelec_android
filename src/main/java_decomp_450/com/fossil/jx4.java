package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jx4 extends ix4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362656, 1);
        P.put(2131362660, 2);
        P.put(2131362286, 3);
        P.put(2131362518, 4);
        P.put(2131362278, 5);
        P.put(2131362454, 6);
        P.put(2131362713, 7);
        P.put(2131362485, 8);
        P.put(2131363109, 9);
        P.put(2131362052, 10);
        P.put(2131362184, 11);
        P.put(2131362182, 12);
        P.put(2131362186, 13);
        P.put(2131362187, 14);
        P.put(2131362185, 15);
        P.put(2131362181, 16);
        P.put(2131362183, 17);
        P.put(2131363388, 18);
        P.put(2131362862, 19);
        P.put(2131362864, 20);
        P.put(2131362863, 21);
        P.put(2131362263, 22);
    }
    */

    @DexIgnore
    public jx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 23, O, P));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.N != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.N = 1;
        }
        g();
    }

    @DexIgnore
    public jx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[10], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[17], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[14], (FlexibleButton) objArr[22], (FlexibleEditText) objArr[5], (FlexibleEditText) objArr[3], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[4], (ImageView) objArr[1], (RTLImageView) objArr[2], (ImageView) objArr[7], (NumberPicker) objArr[19], (NumberPicker) objArr[21], (NumberPicker) objArr[20], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[9], (View) objArr[18]);
        this.N = -1;
        ((ix4) this).K.setTag(null);
        a(view);
        f();
    }
}
