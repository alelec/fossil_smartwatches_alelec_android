package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ga1 extends bn0 {
    @DexIgnore
    public int E;

    @DexIgnore
    public ga1(ri1 ri1, en0 en0) {
        super(ri1, en0, wm0.e, new qh0(ri1));
    }

    @DexIgnore
    @Override // com.fossil.bn0
    public void b(v81 v81) {
        if (v81 instanceof qh0) {
            this.E = ((qh0) v81).A;
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return Integer.valueOf(this.E);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(super.k(), r51.b, Integer.valueOf(this.E));
    }
}
