package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lq5 implements Factory<gq5> {
    @DexIgnore
    public static gq5 a(iq5 iq5) {
        gq5 d = iq5.d();
        c87.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
