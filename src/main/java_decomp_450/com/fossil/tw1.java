package com.fossil;

import com.fossil.qw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tw1 {
    @DexIgnore
    public static /* final */ tw1 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(int i);

        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract tw1 a();

        @DexIgnore
        public abstract a b(int i);

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract a c(int i);
    }

    /*
    static {
        a f = f();
        f.b(10485760L);
        f.b(200);
        f.a(10000);
        f.a(604800000L);
        f.c(81920);
        a = f.a();
    }
    */

    @DexIgnore
    public static a f() {
        return new qw1.b();
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract int d();

    @DexIgnore
    public abstract long e();
}
