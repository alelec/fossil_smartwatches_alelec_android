package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm5 implements Factory<am5> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<UserSettingDao> c;

    @DexIgnore
    public bm5(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static bm5 a(Provider<PortfolioApp> provider, Provider<UserRepository> provider2, Provider<UserSettingDao> provider3) {
        return new bm5(provider, provider2, provider3);
    }

    @DexIgnore
    public static am5 a(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        return new am5(portfolioApp, userRepository, userSettingDao);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public am5 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
