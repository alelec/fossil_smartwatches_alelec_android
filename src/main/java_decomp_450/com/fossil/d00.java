package com.fossil;

import android.util.Base64;
import com.fossil.ix;
import com.fossil.m00;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d00<Model, Data> implements m00<Model, Data> {
    @DexIgnore
    public /* final */ a<Data> a;

    @DexIgnore
    public interface a<Data> {
        @DexIgnore
        Data a(String str) throws IllegalArgumentException;

        @DexIgnore
        void a(Data data) throws IOException;

        @DexIgnore
        Class<Data> getDataClass();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<Model> implements n00<Model, InputStream> {
        @DexIgnore
        public /* final */ a<InputStream> a; // = new a(this);

        @DexIgnore
        @Override // com.fossil.n00
        public m00<Model, InputStream> a(q00 q00) {
            return new d00(this.a);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements a<InputStream> {
            @DexIgnore
            public a(c cVar) {
            }

            @DexIgnore
            @Override // com.fossil.d00.a
            public Class<InputStream> getDataClass() {
                return InputStream.class;
            }

            @DexIgnore
            @Override // com.fossil.d00.a
            public InputStream a(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            @DexIgnore
            public void a(InputStream inputStream) throws IOException {
                inputStream.close();
            }
        }
    }

    @DexIgnore
    public d00(a<Data> aVar) {
        this.a = aVar;
    }

    @DexIgnore
    @Override // com.fossil.m00
    public m00.a<Data> a(Model model, int i, int i2, ax axVar) {
        return new m00.a<>(new k50(model), new b(model.toString(), this.a));
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<Data> implements ix<Data> {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ a<Data> b;
        @DexIgnore
        public Data c;

        @DexIgnore
        public b(String str, a<Data> aVar) {
            this.a = str;
            this.b = aVar;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super Data> aVar) {
            try {
                Data a2 = this.b.a(this.a);
                this.c = a2;
                aVar.a((Object) a2);
            } catch (IllegalArgumentException e) {
                aVar.a((Exception) e);
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return sw.LOCAL;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<Data> getDataClass() {
            return this.b.getDataClass();
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
            try {
                this.b.a((Object) this.c);
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m00
    public boolean a(Model model) {
        return model.toString().startsWith("data:image");
    }
}
