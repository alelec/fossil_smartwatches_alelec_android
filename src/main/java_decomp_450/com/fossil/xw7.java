package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw7 {
    @DexIgnore
    public final File a(Context context, String str, String str2, boolean z) {
        ee7.b(context, "context");
        ee7.b(str, "id");
        ee7.b(str2, "displayName");
        String str3 = z ? "_origin" : "";
        return new File(context.getCacheDir(), str + str3 + '_' + str2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0048, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        com.fossil.hc7.a(r3, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004c, code lost:
        throw r4;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.io.File a(android.content.Context r2, java.lang.String r3, java.lang.String r4, int r5, boolean r6) {
        /*
            r1 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = "assetId"
            com.fossil.ee7.b(r3, r0)
            java.lang.String r0 = "extName"
            com.fossil.ee7.b(r4, r0)
            java.io.File r4 = r1.a(r2, r3, r4, r6)
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x001a
            return r4
        L_0x001a:
            android.content.ContentResolver r2 = r2.getContentResolver()
            r0 = 1
            if (r5 != r0) goto L_0x0028
            android.net.Uri r5 = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            android.net.Uri r3 = android.net.Uri.withAppendedPath(r5, r3)
            goto L_0x002e
        L_0x0028:
            android.net.Uri r5 = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI
            android.net.Uri r3 = android.net.Uri.withAppendedPath(r5, r3)
        L_0x002e:
            if (r6 == 0) goto L_0x0034
            android.net.Uri r3 = android.provider.MediaStore.setRequireOriginal(r3)
        L_0x0034:
            java.io.InputStream r2 = r2.openInputStream(r3)
            java.io.FileOutputStream r3 = new java.io.FileOutputStream
            r3.<init>(r4)
            r5 = 0
            if (r2 == 0) goto L_0x004d
            r6 = 0
            r0 = 2
            com.fossil.gc7.a(r2, r3, r6, r0, r5)     // Catch:{ all -> 0x0046 }
            goto L_0x004d
        L_0x0046:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x0048 }
        L_0x0048:
            r4 = move-exception
            com.fossil.hc7.a(r3, r2)
            throw r4
        L_0x004d:
            com.fossil.hc7.a(r3, r5)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xw7.a(android.content.Context, java.lang.String, java.lang.String, int, boolean):java.io.File");
    }

    @DexIgnore
    public final void a(Context context, zw7 zw7, byte[] bArr, boolean z) {
        ee7.b(context, "context");
        ee7.b(zw7, "asset");
        ee7.b(bArr, "byteArray");
        File a = a(context, zw7.e(), zw7.b(), z);
        if (a.exists()) {
            mx7.a(zw7.e() + " , isOrigin: " + z + ", cache file exists, ignore save");
            return;
        }
        File parentFile = a.getParentFile();
        if (parentFile == null || !parentFile.exists()) {
            a.mkdirs();
        }
        pc7.a(a, bArr);
        mx7.a(zw7.e() + " , isOrigin: " + z + ", cached");
    }
}
