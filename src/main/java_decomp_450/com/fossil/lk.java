package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lk implements mk {
    @DexIgnore
    public /* final */ ViewGroupOverlay a;

    @DexIgnore
    public lk(ViewGroup viewGroup) {
        this.a = viewGroup.getOverlay();
    }

    @DexIgnore
    @Override // com.fossil.rk
    public void a(Drawable drawable) {
        this.a.add(drawable);
    }

    @DexIgnore
    @Override // com.fossil.rk
    public void b(Drawable drawable) {
        this.a.remove(drawable);
    }

    @DexIgnore
    @Override // com.fossil.mk
    public void a(View view) {
        this.a.add(view);
    }

    @DexIgnore
    @Override // com.fossil.mk
    public void b(View view) {
        this.a.remove(view);
    }
}
