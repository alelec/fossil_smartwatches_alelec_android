package com.fossil;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutScreenshotWrapper;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.workout.WorkoutTetherScreenShotManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kf6 extends ff6 implements te5.a {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g; // = new Date();
    @DexIgnore
    public MutableLiveData<r87<Date, Date>> h; // = new MutableLiveData<>();
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public List<ActivitySummary> k; // = new ArrayList();
    @DexIgnore
    public List<ActivitySample> l; // = new ArrayList();
    @DexIgnore
    public ActivitySummary m;
    @DexIgnore
    public List<ActivitySample> n;
    @DexIgnore
    public /* final */ WorkoutTetherScreenShotManager o;
    @DexIgnore
    public ob5 p;
    @DexIgnore
    public LiveData<qx6<List<ActivitySummary>>> q;
    @DexIgnore
    public LiveData<qx6<List<ActivitySample>>> r;
    @DexIgnore
    public Listing<WorkoutSession> s;
    @DexIgnore
    public /* final */ gf6 t;
    @DexIgnore
    public /* final */ SummariesRepository u;
    @DexIgnore
    public /* final */ ActivitiesRepository v;
    @DexIgnore
    public /* final */ UserRepository w;
    @DexIgnore
    public /* final */ WorkoutSessionRepository x;
    @DexIgnore
    public /* final */ pj4 y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<ActivitySample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(ActivitySample activitySample) {
            return Boolean.valueOf(invoke(activitySample));
        }

        @DexIgnore
        public final boolean invoke(ActivitySample activitySample) {
            ee7.b(activitySample, "it");
            return zd5.d(activitySample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$observeWorkoutSessionData$1", f = "CaloriesDetailPresenter.kt", l = {99}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qf<WorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kf6$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$observeWorkoutSessionData$1$1$1", f = "CaloriesDetailPresenter.kt", l = {110}, m = "invokeSuspend")
            /* renamed from: com.fossil.kf6$c$a$a  reason: collision with other inner class name */
            public static final class C0093a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ qf $pageList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kf6$c$a$a$a")
                /* renamed from: com.fossil.kf6$c$a$a$a  reason: collision with other inner class name */
                public static final class C0094a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0093a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0094a(fb7 fb7, C0093a aVar) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0094a aVar = new C0094a(fb7, this.this$0);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                        return ((C0094a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            Iterator it = this.this$0.$pageList.iterator();
                            while (it.hasNext()) {
                                WorkoutSession workoutSession = (WorkoutSession) it.next();
                                List<WorkoutGpsPoint> workoutGpsPoints = workoutSession.getWorkoutGpsPoints();
                                boolean z = false;
                                boolean z2 = workoutGpsPoints != null && (workoutGpsPoints.isEmpty() ^ true);
                                boolean z3 = workoutSession.getMode() != ub5.INDOOR;
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                local.d("CaloriesDetailPresenter", workoutSession.getId() + "-isHasGpsDataPoint " + z2 + " isOutdoorMode " + z3);
                                if (z2 && z3) {
                                    String screenShotUri = workoutSession.getScreenShotUri();
                                    if (screenShotUri != null) {
                                        if (screenShotUri.length() > 0) {
                                            z = true;
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    local2.d("CaloriesDetailPresenter", workoutSession.getId() + "-isScreenshotUriExist " + z);
                                    if (!z) {
                                        String a = this.this$0.this$0.a.this$0.o.a(workoutSession.getId());
                                        if (ee5.a.a(a)) {
                                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                            local3.d("CaloriesDetailPresenter", workoutSession.getId() + "-Found in internal cache with path " + a);
                                            workoutSession.setScreenShotUri(a);
                                        } else {
                                            WorkoutTetherScreenShotManager r = this.this$0.this$0.a.this$0.o;
                                            ee7.a((Object) workoutSession, "workoutSession");
                                            WorkoutScreenshotWrapper a2 = r.a(workoutSession);
                                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                            local4.d("CaloriesDetailPresenter", workoutSession.getId() + "-Enqueue to generate screenshot " + a2);
                                            this.this$0.this$0.a.this$0.o.a(a2);
                                        }
                                    }
                                }
                            }
                            return i97.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0093a(a aVar, qf qfVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$pageList = qfVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0093a aVar = new C0093a(this.this$0, this.$pageList, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0093a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        FragmentActivity activity = ((hf6) this.this$0.a.this$0.t).getActivity();
                        if (activity != null) {
                            WorkoutTetherScreenShotManager r = this.this$0.a.this$0.o;
                            ee7.a((Object) activity, "it");
                            r.a(activity);
                            ti7 a2 = qj7.a();
                            C0094a aVar = new C0094a(null, this);
                            this.L$0 = yi7;
                            this.L$1 = activity;
                            this.label = 1;
                            if (vh7.a(a2, aVar, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        FragmentActivity fragmentActivity = (FragmentActivity) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    gf6 q = this.this$0.a.this$0.t;
                    ob5 i2 = this.this$0.a.this$0.p;
                    qf<WorkoutSession> qfVar = this.$pageList;
                    ee7.a((Object) qfVar, "pageList");
                    q.a(true, i2, qfVar);
                    return i97.a;
                }
            }

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<WorkoutSession> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("CaloriesDetailPresenter", "getWorkoutSessionsPaging observed size = " + qfVar.size());
                if (be5.o.g(PortfolioApp.g0.c().c())) {
                    ee7.a((Object) qfVar, "pageList");
                    if (ea7.d((Collection) qfVar).isEmpty()) {
                        this.a.this$0.t.a(false, this.a.this$0.p, qfVar);
                        return;
                    }
                }
                ik7 unused = xh7.b(this.a.this$0.e(), null, null, new C0093a(this, qfVar, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kf6 kf6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kf6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$date, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kf6 kf6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kf6 kf62 = this.this$0;
                WorkoutSessionRepository s = kf62.x;
                Date date = this.$date;
                WorkoutSessionRepository s2 = this.this$0.x;
                pj4 a3 = this.this$0.y;
                kf6 kf63 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = kf62;
                this.label = 1;
                obj = s.getWorkoutSessionsPaging(date, s2, a3, kf63, this);
                if (obj == a2) {
                    return a2;
                }
                kf6 = kf62;
            } else if (i == 1) {
                kf6 = (kf6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            kf6.s = (Listing) obj;
            Listing j = this.this$0.s;
            if (j != null) {
                LiveData pagedList = j.getPagedList();
                gf6 q = this.this$0.t;
                if (q != null) {
                    pagedList.a((hf6) q, new a(this));
                    return i97.a;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ kf6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$sampleTransformations$1$1", f = "CaloriesDetailPresenter.kt", l = {79, 79}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySample>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySample>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    ActivitiesRepository c = this.this$0.a.v;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = c.getActivityList(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(kf6 kf6) {
            this.a = kf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySample>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$setDate$1", f = "CaloriesDetailPresenter.kt", l = {209, 232, 233}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$setDate$1$1", f = "CaloriesDetailPresenter.kt", l = {209}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Date> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.g(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$setDate$1$samples$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<ActivitySample>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<ActivitySample>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    kf6 kf6 = this.this$0.this$0;
                    return kf6.a(kf6.g, this.this$0.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$setDate$1$summary$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ActivitySummary> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    kf6 kf6 = this.this$0.this$0;
                    return kf6.b(kf6.g, this.this$0.this$0.k);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(kf6 kf6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kf6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$date, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x019d A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x019e  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x01af  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01c1  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r0 = r16
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 3
                r4 = 2
                r5 = 0
                r6 = 1
                if (r2 == 0) goto L_0x0060
                if (r2 == r6) goto L_0x0052
                if (r2 == r4) goto L_0x0037
                if (r2 != r3) goto L_0x002f
                java.lang.Object r1 = r0.L$4
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r1 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r1
                java.lang.Object r2 = r0.L$3
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r2 = r0.L$2
                android.util.Pair r2 = (android.util.Pair) r2
                java.lang.Object r2 = r0.L$1
                java.lang.Boolean r2 = (java.lang.Boolean) r2
                java.lang.Object r2 = r0.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r17)
                r2 = r17
                goto L_0x019f
            L_0x002f:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0037:
                java.lang.Object r2 = r0.L$3
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r4 = r0.L$2
                android.util.Pair r4 = (android.util.Pair) r4
                java.lang.Object r7 = r0.L$1
                java.lang.Boolean r7 = (java.lang.Boolean) r7
                boolean r8 = r0.Z$0
                java.lang.Object r9 = r0.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r17)
                r10 = r7
                r7 = r4
                r4 = r17
                goto L_0x017c
            L_0x0052:
                java.lang.Object r2 = r0.L$1
                com.fossil.kf6 r2 = (com.fossil.kf6) r2
                java.lang.Object r7 = r0.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r17)
                r8 = r17
                goto L_0x0085
            L_0x0060:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r7 = r0.p$
                com.fossil.kf6 r2 = r0.this$0
                java.util.Date r2 = r2.f
                if (r2 != 0) goto L_0x008a
                com.fossil.kf6 r2 = r0.this$0
                com.fossil.ti7 r8 = r2.b()
                com.fossil.kf6$e$a r9 = new com.fossil.kf6$e$a
                r9.<init>(r5)
                r0.L$0 = r7
                r0.L$1 = r2
                r0.label = r6
                java.lang.Object r8 = com.fossil.vh7.a(r8, r9, r0)
                if (r8 != r1) goto L_0x0085
                return r1
            L_0x0085:
                java.util.Date r8 = (java.util.Date) r8
                r2.f = r8
            L_0x008a:
                r9 = r7
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "setDate - date="
                r7.append(r8)
                java.util.Date r8 = r0.$date
                r7.append(r8)
                java.lang.String r8 = ", createdAt="
                r7.append(r8)
                com.fossil.kf6 r8 = r0.this$0
                java.util.Date r8 = r8.f
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                java.lang.String r8 = "CaloriesDetailPresenter"
                r2.d(r8, r7)
                com.fossil.kf6 r2 = r0.this$0
                java.util.Date r7 = r0.$date
                r2.g = r7
                com.fossil.kf6 r2 = r0.this$0
                java.util.Date r2 = r2.f
                java.util.Date r7 = r0.$date
                boolean r2 = com.fossil.zd5.c(r2, r7)
                java.util.Date r7 = new java.util.Date
                r7.<init>()
                java.util.Date r10 = r0.$date
                boolean r7 = com.fossil.zd5.c(r7, r10)
                r7 = r7 ^ r6
                java.util.Date r10 = r0.$date
                java.lang.Boolean r10 = com.fossil.zd5.w(r10)
                com.fossil.kf6 r11 = r0.this$0
                com.fossil.gf6 r11 = r11.t
                java.util.Date r12 = r0.$date
                java.lang.String r13 = "isToday"
                com.fossil.ee7.a(r10, r13)
                boolean r13 = r10.booleanValue()
                r11.a(r12, r2, r13, r7)
                java.util.Date r7 = r0.$date
                com.fossil.kf6 r11 = r0.this$0
                java.util.Date r11 = r11.f
                android.util.Pair r7 = com.fossil.zd5.a(r7, r11)
                java.lang.String r11 = "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)"
                com.fossil.ee7.a(r7, r11)
                com.fossil.kf6 r11 = r0.this$0
                androidx.lifecycle.MutableLiveData r11 = r11.h
                java.lang.Object r11 = r11.a()
                com.fossil.r87 r11 = (com.fossil.r87) r11
                com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "setDate - rangeDateValue="
                r13.append(r14)
                r13.append(r11)
                java.lang.String r14 = ", newRange="
                r13.append(r14)
                com.fossil.r87 r14 = new com.fossil.r87
                java.lang.Object r15 = r7.first
                java.lang.Object r6 = r7.second
                r14.<init>(r15, r6)
                r13.append(r14)
                java.lang.String r6 = r13.toString()
                r12.d(r8, r6)
                if (r11 == 0) goto L_0x01fa
                java.lang.Object r6 = r11.getFirst()
                java.util.Date r6 = (java.util.Date) r6
                java.lang.Object r8 = r7.first
                java.util.Date r8 = (java.util.Date) r8
                boolean r6 = com.fossil.zd5.d(r6, r8)
                if (r6 == 0) goto L_0x01fa
                java.lang.Object r6 = r11.getSecond()
                java.util.Date r6 = (java.util.Date) r6
                java.lang.Object r8 = r7.second
                java.util.Date r8 = (java.util.Date) r8
                boolean r6 = com.fossil.zd5.d(r6, r8)
                if (r6 != 0) goto L_0x015c
                goto L_0x01fa
            L_0x015c:
                com.fossil.kf6 r6 = r0.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.kf6$e$c r8 = new com.fossil.kf6$e$c
                r8.<init>(r0, r5)
                r0.L$0 = r9
                r0.Z$0 = r2
                r0.L$1 = r10
                r0.L$2 = r7
                r0.L$3 = r11
                r0.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r6, r8, r0)
                if (r4 != r1) goto L_0x017a
                return r1
            L_0x017a:
                r8 = r2
                r2 = r11
            L_0x017c:
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r4 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r4
                com.fossil.kf6 r6 = r0.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.kf6$e$b r11 = new com.fossil.kf6$e$b
                r11.<init>(r0, r5)
                r0.L$0 = r9
                r0.Z$0 = r8
                r0.L$1 = r10
                r0.L$2 = r7
                r0.L$3 = r2
                r0.L$4 = r4
                r0.label = r3
                java.lang.Object r2 = com.fossil.vh7.a(r6, r11, r0)
                if (r2 != r1) goto L_0x019e
                return r1
            L_0x019e:
                r1 = r4
            L_0x019f:
                java.util.List r2 = (java.util.List) r2
                com.fossil.kf6 r3 = r0.this$0
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r3 = r3.m
                boolean r3 = com.fossil.ee7.a(r3, r1)
                r4 = 1
                r3 = r3 ^ r4
                if (r3 == 0) goto L_0x01b4
                com.fossil.kf6 r3 = r0.this$0
                r3.m = r1
            L_0x01b4:
                com.fossil.kf6 r1 = r0.this$0
                java.util.List r1 = r1.n
                boolean r1 = com.fossil.ee7.a(r1, r2)
                r1 = r1 ^ r4
                if (r1 == 0) goto L_0x01c6
                com.fossil.kf6 r1 = r0.this$0
                r1.n = r2
            L_0x01c6:
                com.fossil.kf6 r1 = r0.this$0
                com.fossil.gf6 r1 = r1.t
                com.fossil.kf6 r2 = r0.this$0
                com.fossil.ob5 r2 = r2.p
                com.fossil.kf6 r3 = r0.this$0
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r3 = r3.m
                r1.a(r2, r3)
                com.fossil.kf6 r1 = r0.this$0
                java.util.Date r2 = r1.g
                r1.c(r2)
                com.fossil.kf6 r1 = r0.this$0
                boolean r1 = r1.i
                if (r1 == 0) goto L_0x0217
                com.fossil.kf6 r1 = r0.this$0
                boolean r1 = r1.j
                if (r1 == 0) goto L_0x0217
                com.fossil.kf6 r1 = r0.this$0
                com.fossil.ik7 unused = r1.o()
                goto L_0x0217
            L_0x01fa:
                com.fossil.kf6 r1 = r0.this$0
                r2 = 0
                r1.i = r2
                com.fossil.kf6 r1 = r0.this$0
                r1.j = r2
                com.fossil.kf6 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.h
                com.fossil.r87 r2 = new com.fossil.r87
                java.lang.Object r3 = r7.first
                java.lang.Object r4 = r7.second
                r2.<init>(r3, r4)
                r1.a(r2)
            L_0x0217:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.kf6.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1", f = "CaloriesDetailPresenter.kt", l = {272, 274}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$maxValue$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> c;
                ArrayList<BarChart.b> arrayList;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Iterator it = this.$data.iterator();
                    int i = 0;
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        obj2 = it.next();
                        if (it.hasNext()) {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                            ee7.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 += pb7.a(it2.next().e()).intValue();
                            }
                            Integer a = pb7.a(i2);
                            do {
                                Object next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                                ee7.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 += pb7.a(it3.next().e()).intValue();
                                }
                                Integer a2 = pb7.a(i3);
                                if (a.compareTo((Object) a2) < 0) {
                                    obj2 = next;
                                    a = a2;
                                }
                            } while (it.hasNext());
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += pb7.a(it4.next().e()).intValue();
                    }
                    return pb7.a(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$showDetailChart$1$pair$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return tg6.a.a(this.this$0.this$0.g, this.this$0.this$0.n, 2);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(kf6 kf6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kf6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x008f  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r4) goto L_0x0027
                if (r1 != r3) goto L_0x001f
                java.lang.Object r0 = r8.L$2
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r8.L$1
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r2 = r8.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r9)
                goto L_0x0070
            L_0x001f:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x0027:
                java.lang.Object r1 = r8.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r9)
                goto L_0x004a
            L_0x002f:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r1 = r8.p$
                com.fossil.kf6 r9 = r8.this$0
                com.fossil.ti7 r9 = r9.b()
                com.fossil.kf6$f$b r5 = new com.fossil.kf6$f$b
                r5.<init>(r8, r2)
                r8.L$0 = r1
                r8.label = r4
                java.lang.Object r9 = com.fossil.vh7.a(r9, r5, r8)
                if (r9 != r0) goto L_0x004a
                return r0
            L_0x004a:
                com.fossil.r87 r9 = (com.fossil.r87) r9
                java.lang.Object r4 = r9.getFirst()
                java.util.ArrayList r4 = (java.util.ArrayList) r4
                com.fossil.kf6 r5 = r8.this$0
                com.fossil.ti7 r5 = r5.b()
                com.fossil.kf6$f$a r6 = new com.fossil.kf6$f$a
                r6.<init>(r4, r2)
                r8.L$0 = r1
                r8.L$1 = r9
                r8.L$2 = r4
                r8.label = r3
                java.lang.Object r1 = com.fossil.vh7.a(r5, r6, r8)
                if (r1 != r0) goto L_0x006c
                return r0
            L_0x006c:
                r0 = r4
                r7 = r1
                r1 = r9
                r9 = r7
            L_0x0070:
                java.lang.Integer r9 = (java.lang.Integer) r9
                com.fossil.ge5$a r2 = com.fossil.ge5.c
                com.fossil.kf6 r3 = r8.this$0
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r3 = r3.m
                com.fossil.gb5 r4 = com.fossil.gb5.CALORIES
                int r2 = r2.a(r3, r4)
                com.fossil.kf6 r3 = r8.this$0
                com.fossil.gf6 r3 = r3.t
                com.portfolio.platform.ui.view.chart.base.BarChart$c r4 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                if (r9 == 0) goto L_0x008f
                int r9 = r9.intValue()
                goto L_0x0090
            L_0x008f:
                r9 = 0
            L_0x0090:
                int r5 = r2 / 16
                int r9 = java.lang.Math.max(r9, r5)
                r4.<init>(r9, r2, r0)
                java.lang.Object r9 = r1.getSecond()
                java.util.ArrayList r9 = (java.util.ArrayList) r9
                r3.a(r4, r9)
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.kf6.f.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1", f = "CaloriesDetailPresenter.kt", l = {149}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kf6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$1", f = "CaloriesDetailPresenter.kt", l = {150}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ob5>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ob5> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                String str;
                MFUser.UnitGroup unitGroup;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository p = this.this$0.this$0.w;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = p.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFUser mFUser = (MFUser) obj;
                if (mFUser == null || (unitGroup = mFUser.getUnitGroup()) == null || (str = unitGroup.getDistance()) == null) {
                    str = ob5.METRIC.getValue();
                }
                return ob5.fromString(str);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b<T> implements zd<qx6<? extends List<ActivitySummary>>> {
            @DexIgnore
            public /* final */ /* synthetic */ g a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1", f = "CaloriesDetailPresenter.kt", l = {163}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kf6$g$b$a$a")
                @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$2$1$summary$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.kf6$g$b$a$a  reason: collision with other inner class name */
                public static final class C0095a extends zb7 implements kd7<yi7, fb7<? super ActivitySummary>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0095a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0095a aVar = new C0095a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super ActivitySummary> fb7) {
                        return ((C0095a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            kf6 kf6 = this.this$0.this$0.a.this$0;
                            return kf6.b(kf6.g, this.this$0.this$0.a.this$0.k);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ti7 b = this.this$0.a.this$0.b();
                        C0095a aVar = new C0095a(this, null);
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = vh7.a(b, aVar, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ActivitySummary activitySummary = (ActivitySummary) obj;
                    if (this.this$0.a.this$0.m == null || (!ee7.a(this.this$0.a.this$0.m, activitySummary))) {
                        this.this$0.a.this$0.m = activitySummary;
                        this.this$0.a.this$0.t.a(this.this$0.a.this$0.p, this.this$0.a.this$0.m);
                        if (this.this$0.a.this$0.i && this.this$0.a.this$0.j) {
                            ik7 unused = this.this$0.a.this$0.o();
                        }
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public b(g gVar) {
                this.a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<? extends List<ActivitySummary>> qx6) {
                lb5 a2 = qx6.a();
                List list = (List) qx6.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - summaryTransformations -- activitySummaries=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("CaloriesDetailPresenter", sb.toString());
                if (a2 == lb5.NETWORK_LOADING || a2 == lb5.SUCCESS) {
                    this.a.this$0.k = list;
                    this.a.this$0.i = true;
                    ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(this, null), 3, null);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c<T> implements zd<qx6<? extends List<ActivitySample>>> {
            @DexIgnore
            public /* final */ /* synthetic */ g a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1", f = "CaloriesDetailPresenter.kt", l = {185}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ c this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kf6$g$c$a$a")
                @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$start$1$3$1$samples$1", f = "CaloriesDetailPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.kf6$g$c$a$a  reason: collision with other inner class name */
                public static final class C0096a extends zb7 implements kd7<yi7, fb7<? super List<ActivitySample>>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0096a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0096a aVar = new C0096a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super List<ActivitySample>> fb7) {
                        return ((C0096a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            kf6 kf6 = this.this$0.this$0.a.this$0;
                            return kf6.a(kf6.g, this.this$0.this$0.a.this$0.l);
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(c cVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = cVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ti7 b = this.this$0.a.this$0.b();
                        C0096a aVar = new C0096a(this, null);
                        this.L$0 = yi7;
                        this.label = 1;
                        obj = vh7.a(b, aVar, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    List list = (List) obj;
                    if (this.this$0.a.this$0.n == null || (!ee7.a(this.this$0.a.this$0.n, list))) {
                        this.this$0.a.this$0.n = list;
                        if (this.this$0.a.this$0.i && this.this$0.a.this$0.j) {
                            ik7 unused = this.this$0.a.this$0.o();
                        }
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            public c(g gVar) {
                this.a = gVar;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qx6<? extends List<ActivitySample>> qx6) {
                lb5 a2 = qx6.a();
                List list = (List) qx6.b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("start - sampleTransformations -- activitySamples=");
                sb.append(list != null ? Integer.valueOf(list.size()) : null);
                sb.append(", status=");
                sb.append(a2);
                local.d("CaloriesDetailPresenter", sb.toString());
                if (a2 == lb5.NETWORK_LOADING || a2 == lb5.SUCCESS) {
                    this.a.this$0.l = list;
                    this.a.this$0.j = true;
                    ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(this, null), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(kf6 kf6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kf6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            kf6 kf6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                kf6 kf62 = this.this$0;
                ti7 b2 = kf62.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = kf62;
                this.label = 1;
                obj = vh7.a(b2, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                kf6 = kf62;
            } else if (i == 1) {
                kf6 = (kf6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ee7.a(obj, "withContext(commonPool) \u2026TRIC.value)\n            }");
            kf6.p = (ob5) obj;
            LiveData u = this.this$0.q;
            gf6 q = this.this$0.t;
            if (q != null) {
                u.a((hf6) q, new b(this));
                this.this$0.r.a((LifecycleOwner) this.this$0.t, new c(this));
                return i97.a;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ kf6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailPresenter$summaryTransformations$1$1", f = "CaloriesDetailPresenter.kt", l = {75, 75}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SummariesRepository m = this.this$0.a.u;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = m.getSummaries(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(kf6 kf6) {
            this.a = kf6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySummary>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public kf6(gf6 gf6, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FileRepository fileRepository, pj4 pj4, PortfolioApp portfolioApp) {
        ee7.b(gf6, "mView");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(activitiesRepository, "mActivitiesRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        ee7.b(fileRepository, "mFileRepository");
        ee7.b(pj4, "appExecutors");
        ee7.b(portfolioApp, "mApp");
        this.t = gf6;
        this.u = summariesRepository;
        this.v = activitiesRepository;
        this.w = userRepository;
        this.x = workoutSessionRepository;
        this.y = pj4;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        this.o = new WorkoutTetherScreenShotManager(this.x, fileRepository);
        this.p = ob5.METRIC;
        LiveData<qx6<List<ActivitySummary>>> b2 = ge.b(this.h, new h(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.q = b2;
        LiveData<qx6<List<ActivitySample>>> b3 = ge.b(this.h, new d(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.r = b3;
    }

    @DexIgnore
    @Override // com.fossil.te5.a
    public void a(te5.g gVar) {
        ee7.b(gVar, "report");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("CaloriesDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        ik7 unused = xh7.b(e(), null, null, new g(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("CaloriesDetailPresenter", "stop");
        LiveData<qx6<List<ActivitySummary>>> liveData = this.q;
        gf6 gf6 = this.t;
        if (gf6 != null) {
            liveData.a((hf6) gf6);
            this.r.a((LifecycleOwner) this.t);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public ob5 i() {
        return this.p;
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void k() {
        LiveData<qf<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.s;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                gf6 gf6 = this.t;
                if (gf6 != null) {
                    pagedList.a((hf6) gf6);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.calories.CaloriesDetailFragment");
                }
            }
            this.x.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("CaloriesDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void l() {
        Date o2 = zd5.o(this.g);
        ee7.a((Object) o2, "DateHelper.getNextDate(mDate)");
        b(o2);
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void m() {
        Date p2 = zd5.p(this.g);
        ee7.a((Object) p2, "DateHelper.getPrevDate(mDate)");
        b(p2);
    }

    @DexIgnore
    public void n() {
        this.t.a(this);
    }

    @DexIgnore
    public final ik7 o() {
        return xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public WorkoutTetherScreenShotManager j() {
        return this.o;
    }

    @DexIgnore
    public final void c(Date date) {
        k();
        ik7 unused = xh7.b(e(), null, null, new c(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void b(Date date) {
        ee7.b(date, "date");
        ik7 unused = xh7.b(e(), null, null, new e(this, date, null), 3, null);
    }

    @DexIgnore
    public final ActivitySummary b(Date date, List<ActivitySummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (zd5.d(next.getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return t2;
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void a(Date date) {
        ee7.b(date, "date");
        c(date);
    }

    @DexIgnore
    @Override // com.fossil.ff6
    public void a(Bundle bundle) {
        ee7.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.g.getTime());
    }

    @DexIgnore
    public final List<ActivitySample> a(Date date, List<ActivitySample> list) {
        hg7 b2;
        hg7 a2;
        if (list == null || (b2 = ea7.b((Iterable) list)) == null || (a2 = og7.a(b2, new b(date))) == null) {
            return null;
        }
        return og7.g(a2);
    }
}
