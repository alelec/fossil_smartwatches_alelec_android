package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t73 extends wm2 implements s73 {
    @DexIgnore
    public t73() {
        super("com.google.android.gms.maps.internal.IOnMarkerDragListener");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            c(fn2.a(parcel.readStrongBinder()));
        } else if (i == 2) {
            e(fn2.a(parcel.readStrongBinder()));
        } else if (i != 3) {
            return false;
        } else {
            d(fn2.a(parcel.readStrongBinder()));
        }
        parcel2.writeNoException();
        return true;
    }
}
