package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.gx6;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ProfileFormatter;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.view.ruler.RulerValuePicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo5 extends go5 implements qp6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public pp6 f;
    @DexIgnore
    public qw6<k45> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return mo5.i;
        }

        @DexIgnore
        public final mo5 b() {
            return new mo5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 a;

        @DexIgnore
        public c(mo5 mo5) {
            this.a = mo5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
            CharSequence e = gVar.e();
            ob5 ob5 = ob5.METRIC;
            if (ee7.a((Object) e, (Object) PortfolioApp.g0.c().getString(2131886900))) {
                ob5 = ob5.IMPERIAL;
            }
            mo5.a(this.a).a(ob5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TabLayout.d {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 a;

        @DexIgnore
        public e(mo5 mo5) {
            this.a = mo5;
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void a(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void b(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
            CharSequence e = gVar.e();
            ob5 ob5 = ob5.METRIC;
            if (ee7.a((Object) e, (Object) PortfolioApp.g0.c().getString(2131886902))) {
                ob5 = ob5.IMPERIAL;
            }
            mo5.a(this.a).b(ob5);
        }

        @DexIgnore
        @Override // com.google.android.material.tabs.TabLayout.c
        public void c(TabLayout.g gVar) {
            ee7.b(gVar, "tab");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 a;

        @DexIgnore
        public f(mo5 mo5) {
            this.a = mo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            mo5.a(this.a).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 a;

        @DexIgnore
        public g(mo5 mo5) {
            this.a = mo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            mo5.a(this.a).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ mo5 a;

        @DexIgnore
        public h(mo5 mo5) {
            this.a = mo5;
        }

        @DexIgnore
        public final void onClick(View view) {
            mo5.a(this.a).a(false);
        }
    }

    /*
    static {
        String simpleName = mo5.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "OnboardingHeightWeightFr\u2026::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ pp6 a(mo5 mo5) {
        pp6 pp6 = mo5.f;
        if (pp6 != null) {
            return pp6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void b(int i2, ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        int i3 = no5.a[ob5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "updateData weight=" + i2 + " metric");
            qw6<k45> qw6 = this.g;
            if (qw6 != null) {
                k45 a2 = qw6.a();
                if (a2 != null) {
                    TabLayout.g b2 = a2.A.b(0);
                    if (b2 != null) {
                        b2.h();
                    }
                    a2.x.setUnit(ob5.METRIC);
                    a2.x.setFormatter(new ProfileFormatter(4));
                    a2.x.a(350, Action.DisplayMode.ACTIVITY, Math.round((((float) i2) / 1000.0f) * ((float) 10)));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local2.d(str2, "updateData weight=" + i2 + " imperial");
            qw6<k45> qw62 = this.g;
            if (qw62 != null) {
                k45 a3 = qw62.a();
                if (a3 != null) {
                    TabLayout.g b3 = a3.A.b(1);
                    if (b3 != null) {
                        b3.h();
                    }
                    float f2 = xd5.f((float) i2);
                    a3.x.setUnit(ob5.IMPERIAL);
                    a3.x.setFormatter(new ProfileFormatter(4));
                    a3.x.a(780, 4401, Math.round(f2 * ((float) 10)));
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void b0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
            ee7.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, true, false, 4, null);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return i;
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void e() {
        DashBar dashBar;
        qw6<k45> qw6 = this.g;
        if (qw6 != null) {
            k45 a2 = qw6.a();
            if (a2 != null && (dashBar = a2.u) != null) {
                gx6.a aVar = gx6.a;
                ee7.a((Object) dashBar, "this");
                aVar.a(dashBar, 500);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void f() {
        if (isActive()) {
            a();
        }
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void g() {
        if (isActive()) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886907);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026edTerms_Text__PleaseWait)");
            W(a2);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<k45> qw6 = new qw6<>(this, (k45) qb.a(layoutInflater, 2131558597, viewGroup, false, a1()));
        this.g = qw6;
        if (qw6 != null) {
            k45 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        pp6 pp6 = this.f;
        if (pp6 != null) {
            pp6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        pp6 pp6 = this.f;
        if (pp6 != null) {
            pp6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<k45> qw6 = this.g;
        if (qw6 != null) {
            k45 a2 = qw6.a();
            if (a2 != null) {
                a2.w.setValuePickerListener(new b(a2, this));
                a2.z.a((TabLayout.d) new c(this));
                a2.x.setValuePickerListener(new d(a2, this));
                a2.A.a((TabLayout.d) new e(this));
                a2.t.setOnClickListener(new f(this));
                a2.C.setOnClickListener(new g(this));
                a2.q.setOnClickListener(new h(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(pp6 pp6) {
        ee7.b(pp6, "presenter");
        this.f = pp6;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements uz6 {
        @DexIgnore
        public /* final */ /* synthetic */ k45 a;
        @DexIgnore
        public /* final */ /* synthetic */ mo5 b;

        @DexIgnore
        public b(k45 k45, mo5 mo5) {
            this.a = k45;
            this.b = mo5;
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(int i) {
            if (this.a.w.getUnit() == ob5.METRIC) {
                mo5.a(this.b).a(i);
                return;
            }
            mo5.a(this.b).a(Math.round(xd5.a((float) (i / 12), ((float) i) % 12.0f)));
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(boolean z) {
            TabLayout tabLayout = this.a.z;
            ee7.a((Object) tabLayout, "it.tlHeightUnit");
            qy6.a(tabLayout, !z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements uz6 {
        @DexIgnore
        public /* final */ /* synthetic */ k45 a;
        @DexIgnore
        public /* final */ /* synthetic */ mo5 b;

        @DexIgnore
        public d(k45 k45, mo5 mo5) {
            this.a = k45;
            this.b = mo5;
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(int i) {
            if (this.a.x.getUnit() == ob5.METRIC) {
                mo5.a(this.b).b(Math.round((((float) i) / 10.0f) * 1000.0f));
                return;
            }
            mo5.a(this.b).b(Math.round(xd5.k(((float) i) / 10.0f)));
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void b(int i) {
        }

        @DexIgnore
        @Override // com.fossil.uz6
        public void a(boolean z) {
            TabLayout tabLayout = this.a.A;
            ee7.a((Object) tabLayout, "it.tlWeightUnit");
            qy6.a(tabLayout, !z);
        }
    }

    @DexIgnore
    @Override // com.fossil.qp6
    public void a(int i2, ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        int i3 = no5.b[ob5.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.d(str, "updateData height=" + i2 + " metric");
            qw6<k45> qw6 = this.g;
            if (qw6 != null) {
                k45 a2 = qw6.a();
                if (a2 != null) {
                    TabLayout.g b2 = a2.z.b(0);
                    if (b2 != null) {
                        b2.h();
                    }
                    a2.w.setUnit(ob5.METRIC);
                    a2.w.setFormatter(new ProfileFormatter(-1));
                    a2.w.a(100, 251, i2);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local2.d(str2, "updateData height=" + i2 + " imperial");
            qw6<k45> qw62 = this.g;
            if (qw62 != null) {
                k45 a3 = qw62.a();
                if (a3 != null) {
                    TabLayout.g b3 = a3.z.b(1);
                    if (b3 != null) {
                        b3.h();
                    }
                    r87<Integer, Integer> b4 = xd5.b((float) i2);
                    a3.w.setUnit(ob5.IMPERIAL);
                    a3.w.setFormatter(new ProfileFormatter(3));
                    RulerValuePicker rulerValuePicker = a3.w;
                    Integer first = b4.getFirst();
                    ee7.a((Object) first, "currentHeightInFeetAndInches.first");
                    int a4 = xd5.a(first.intValue());
                    Integer second = b4.getSecond();
                    ee7.a((Object) second, "currentHeightInFeetAndInches.second");
                    rulerValuePicker.a(40, 99, a4 + second.intValue());
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
