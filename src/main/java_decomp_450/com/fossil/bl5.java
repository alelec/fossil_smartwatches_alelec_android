package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.transition.Transition;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bl5 extends Transition {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:widgetControl:backgroundDrawableColorgjhkh", "android:widgetControl:bottomTextColor", "android:widgetControl:iconTintColor"};
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(CustomizeWidget customizeWidget, Bundle bundle) {
            ee7.b(customizeWidget, "view");
            ee7.b(bundle, "extra");
            bundle.putInt("android:widgetControl:backgroundDrawableColorgjhkh", customizeWidget.getBackgroundDrawableColor());
            bundle.putInt("android:widgetControl:bottomTextColor", customizeWidget.getBottomTextColor());
            bundle.putInt("android:widgetControl:iconTintColor", customizeWidget.getIconTintColor());
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ArgbEvaluator a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ FlexibleTextView d;

        @DexIgnore
        public b(ArgbEvaluator argbEvaluator, int i, int i2, FlexibleTextView flexibleTextView) {
            this.a = argbEvaluator;
            this.b = i;
            this.c = i2;
            this.d = flexibleTextView;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            ArgbEvaluator argbEvaluator = this.a;
            ee7.a((Object) valueAnimator, "animation");
            Object evaluate = argbEvaluator.evaluate(valueAnimator.getAnimatedFraction(), Integer.valueOf(this.b), Integer.valueOf(this.c));
            if (evaluate != null) {
                this.d.setTextColor(((Integer) evaluate).intValue());
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ ArgbEvaluator a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ ImageView d;

        @DexIgnore
        public c(ArgbEvaluator argbEvaluator, int i, int i2, ImageView imageView) {
            this.a = argbEvaluator;
            this.b = i;
            this.c = i2;
            this.d = imageView;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            ArgbEvaluator argbEvaluator = this.a;
            ee7.a((Object) valueAnimator, "animation");
            Object evaluate = argbEvaluator.evaluate(valueAnimator.getAnimatedFraction(), Integer.valueOf(this.b), Integer.valueOf(this.c));
            if (evaluate != null) {
                int intValue = ((Integer) evaluate).intValue();
                ImageView imageView = this.d;
                ee7.a((Object) imageView, "ivTop");
                imageView.setImageTintList(ColorStateList.valueOf(intValue));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore
    public final void a(CustomizeWidget customizeWidget, TransitionValues transitionValues) {
        Bundle bundle = (Bundle) transitionValues.view.getTag(2131363127);
        if (bundle != null) {
            Map map = transitionValues.values;
            ee7.a((Object) map, "transitionValues.values");
            map.put("android:widgetControl:backgroundDrawableColorgjhkh", Integer.valueOf(bundle.getInt("android:widgetControl:backgroundDrawableColorgjhkh", customizeWidget.getBackgroundDrawableColor())));
            Map map2 = transitionValues.values;
            ee7.a((Object) map2, "transitionValues.values");
            map2.put("android:widgetControl:bottomTextColor", Integer.valueOf(bundle.getInt("android:widgetControl:bottomTextColor", customizeWidget.getBottomTextColor())));
            Map map3 = transitionValues.values;
            ee7.a((Object) map3, "transitionValues.values");
            map3.put("android:widgetControl:iconTintColor", Integer.valueOf(bundle.getInt("android:widgetControl:iconTintColor", customizeWidget.getIconTintColor())));
            return;
        }
        Map map4 = transitionValues.values;
        ee7.a((Object) map4, "transitionValues.values");
        map4.put("android:widgetControl:backgroundDrawableColorgjhkh", Integer.valueOf(customizeWidget.getBackgroundDrawableColor()));
        Map map5 = transitionValues.values;
        ee7.a((Object) map5, "transitionValues.values");
        map5.put("android:widgetControl:bottomTextColor", Integer.valueOf(customizeWidget.getBottomTextColor()));
        Map map6 = transitionValues.values;
        ee7.a((Object) map6, "transitionValues.values");
        map6.put("android:widgetControl:iconTintColor", Integer.valueOf(customizeWidget.getIconTintColor()));
    }

    @DexIgnore
    public void captureEndValues(TransitionValues transitionValues) {
        ee7.b(transitionValues, "transitionValues");
        View view = transitionValues.view;
        if (!(view instanceof CustomizeWidget)) {
            return;
        }
        if (view != null) {
            a((CustomizeWidget) view, transitionValues);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomizeWidget");
    }

    @DexIgnore
    public void captureStartValues(TransitionValues transitionValues) {
        ee7.b(transitionValues, "transitionValues");
        View view = transitionValues.view;
        if (!(view instanceof CustomizeWidget)) {
            return;
        }
        if (view != null) {
            a((CustomizeWidget) view, transitionValues);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomizeWidget");
    }

    @DexIgnore
    public Animator createAnimator(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        ee7.b(viewGroup, "sceneRoot");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WidgetControlTransition", "createAnimator startValues " + transitionValues + " endValues " + transitionValues2);
        if (transitionValues == null || transitionValues2 == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        View view = transitionValues2.view;
        if (view != null) {
            CustomizeWidget customizeWidget = (CustomizeWidget) view;
            Object obj = transitionValues.values.get("android:widgetControl:bottomTextColor");
            if (obj != null) {
                int intValue = ((Integer) obj).intValue();
                Object obj2 = transitionValues2.values.get("android:widgetControl:bottomTextColor");
                if (obj2 != null) {
                    int intValue2 = ((Integer) obj2).intValue();
                    if (intValue != intValue2) {
                        ArgbEvaluator argbEvaluator = new ArgbEvaluator();
                        ValueAnimator ofFloat = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
                        ofFloat.addUpdateListener(new b(argbEvaluator, intValue, intValue2, (FlexibleTextView) customizeWidget.findViewById(2131363224)));
                        arrayList.add(ofFloat);
                    }
                    Object obj3 = transitionValues.values.get("android:widgetControl:backgroundDrawableColorgjhkh");
                    if (obj3 != null) {
                        int intValue3 = ((Integer) obj3).intValue();
                        Object obj4 = transitionValues2.values.get("android:widgetControl:backgroundDrawableColorgjhkh");
                        if (obj4 != null) {
                            int intValue4 = ((Integer) obj4).intValue();
                            if (intValue3 != intValue4) {
                                try {
                                    intValue3 = v6.a(PortfolioApp.g0.c(), intValue3);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    intValue4 = v6.a(PortfolioApp.g0.c(), intValue4);
                                } catch (Exception e2) {
                                    e2.printStackTrace();
                                }
                                arrayList.add(ObjectAnimator.ofArgb(customizeWidget, "backgroundDrawableColor", intValue3, intValue4));
                            }
                            Object obj5 = transitionValues.values.get("android:widgetControl:iconTintColor");
                            if (obj5 != null) {
                                int intValue5 = ((Integer) obj5).intValue();
                                Object obj6 = transitionValues2.values.get("android:widgetControl:iconTintColor");
                                if (obj6 != null) {
                                    int intValue6 = ((Integer) obj6).intValue();
                                    if (intValue5 != intValue6) {
                                        ArgbEvaluator argbEvaluator2 = new ArgbEvaluator();
                                        ValueAnimator ofFloat2 = ValueAnimator.ofFloat(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
                                        ofFloat2.addUpdateListener(new c(argbEvaluator2, intValue5, intValue6, (ImageView) customizeWidget.findViewById(2131362729)));
                                        arrayList.add(ofFloat2);
                                    }
                                    AnimatorSet animatorSet = new AnimatorSet();
                                    animatorSet.playTogether(arrayList);
                                    return animatorSet;
                                }
                                throw new x87("null cannot be cast to non-null type kotlin.Int");
                            }
                            throw new x87("null cannot be cast to non-null type kotlin.Int");
                        }
                        throw new x87("null cannot be cast to non-null type kotlin.Int");
                    }
                    throw new x87("null cannot be cast to non-null type kotlin.Int");
                }
                throw new x87("null cannot be cast to non-null type kotlin.Int");
            }
            throw new x87("null cannot be cast to non-null type kotlin.Int");
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.CustomizeWidget");
    }

    @DexIgnore
    public String[] getTransitionProperties() {
        return a;
    }
}
