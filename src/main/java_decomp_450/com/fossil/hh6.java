package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh6 extends eh6 {
    @DexIgnore
    public String e; // = PortfolioApp.g0.c().c();
    @DexIgnore
    public /* final */ b f; // = new b(this);
    @DexIgnore
    public /* final */ fh6 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ hh6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(hh6 hh6) {
            this.a = hh6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            if (otaEvent != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeUpdateFirmwarePresenter", "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
                if (!TextUtils.isEmpty(otaEvent.getSerial()) && mh7.b(otaEvent.getSerial(), PortfolioApp.g0.c().c(), true)) {
                    this.a.i().c((int) (otaEvent.getProcess() * ((float) 10)));
                }
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public hh6(fh6 fh6) {
        ee7.b(fh6, "mView");
        this.g = fh6;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        PortfolioApp c = PortfolioApp.g0.c();
        b bVar = this.f;
        c.registerReceiver(bVar, new IntentFilter(PortfolioApp.g0.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        h();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        try {
            PortfolioApp.g0.c().unregisterReceiver(this.f);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeUpdateFirmwarePresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public final void h() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeUpdateFirmwarePresenter", "serial=" + this.e + ", mCurrentDeviceType=" + deviceBySerial);
        if (be5.o.a(deviceBySerial)) {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886862));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886863));
            explore2.setBackground(2131231332);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886860));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886861));
            explore4.setBackground(2131231331);
        } else {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886869));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886868));
            explore2.setBackground(2131231336);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886866));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886867));
            explore4.setBackground(2131231333);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.g.d(arrayList);
    }

    @DexIgnore
    public final fh6 i() {
        return this.g;
    }

    @DexIgnore
    public void j() {
        this.g.a(this);
    }
}
