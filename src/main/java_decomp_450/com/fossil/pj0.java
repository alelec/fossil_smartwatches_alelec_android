package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pj0 extends sh0 {
    @DexIgnore
    public long K;
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public pj0(com.fossil.ri1 r7, long r8) {
        /*
            r6 = this;
            com.fossil.x81 r2 = com.fossil.x81.PROCESS_USER_AUTHORIZATION
            com.fossil.qa1 r3 = com.fossil.qa1.e0
            r4 = 0
            r5 = 8
            r0 = r6
            r1 = r7
            r0.<init>(r1, r2, r3, r4, r5)
            r6.L = r8
            r0 = 4294967295(0xffffffff, double:2.1219957905E-314)
            r2 = 0
            int r7 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r7 < 0) goto L_0x0021
            com.fossil.de7 r7 = com.fossil.de7.a
            int r7 = (r8 > r0 ? 1 : (r8 == r0 ? 0 : -1))
            if (r7 > 0) goto L_0x0021
            r7 = 1
            goto L_0x0022
        L_0x0021:
            r7 = 0
        L_0x0022:
            if (r7 == 0) goto L_0x0029
            long r7 = r6.L
            r6.K = r7
            return
        L_0x0029:
            java.lang.String r7 = "timeoutInMs ("
            java.lang.StringBuilder r7 = com.fossil.yh0.b(r7)
            long r8 = r6.L
            r7.append(r8)
            java.lang.String r8 = ") must be in [0, ["
            r7.append(r8)
            com.fossil.de7 r8 = com.fossil.de7.a
            r7.append(r0)
            java.lang.String r8 = "]]."
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            java.lang.IllegalArgumentException r8 = new java.lang.IllegalArgumentException
            java.lang.String r7 = r7.toString()
            r8.<init>(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pj0.<init>(com.fossil.ri1, long):void");
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.K = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.K4, Long.valueOf(this.L));
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        ee7.a((Object) array, "ByteBuffer.allocate(4)\n \u2026\n                .array()");
        return array;
    }
}
