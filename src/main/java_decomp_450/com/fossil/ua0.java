package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ua0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ g90 b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ i90 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ua0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public ua0 createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                g90 valueOf = g90.valueOf(readString);
                float readFloat = parcel.readFloat();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    return new ua0(readLong, valueOf, readFloat, i90.valueOf(readString2));
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public ua0[] newArray(int i) {
            return new ua0[i];
        }
    }

    @DexIgnore
    public ua0(long j, g90 g90, float f, i90 i90) {
        this.a = j;
        this.b = g90;
        this.c = yz0.a(f, 2);
        this.d = i90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.K2, Long.valueOf(this.a)), r51.t, yz0.a(this.b)), r51.R1, Float.valueOf(this.c)), r51.s, yz0.a(this.d));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("alive", this.a);
            jSONObject.put(Constants.PROFILE_KEY_UNIT, yz0.a(this.b));
            jSONObject.put("temp", Float.valueOf(this.c));
            jSONObject.put("cond_id", this.d.a());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ua0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ua0 ua0 = (ua0) obj;
            return this.a == ua0.a && this.b == ua0.b && this.c == ua0.c && this.d == ua0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CompactWeatherInfo");
    }

    @DexIgnore
    public final long getExpiredTimeStampInSecond() {
        return this.a;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.c;
    }

    @DexIgnore
    public final g90 getTemperatureUnit() {
        return this.b;
    }

    @DexIgnore
    public final i90 getWeatherCondition() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (((int) this.a) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
