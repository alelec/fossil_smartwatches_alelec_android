package com.fossil;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap5 extends RecyclerView.g<a> {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Typeface c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public ArrayList<WatchFaceWrapper> e;
    @DexIgnore
    public c f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ ap5 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ap5$a$a")
        /* renamed from: com.fossil.ap5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0013a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0013a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (!(this.a.d.getItemCount() <= this.a.getAdapterPosition() || this.a.getAdapterPosition() == -1 || (d = this.a.d.d()) == null)) {
                    Object obj = this.a.d.e.get(this.a.getAdapterPosition());
                    ee7.a(obj, "mData[adapterPosition]");
                    d.a((WatchFaceWrapper) obj);
                }
                a aVar = this.a;
                aVar.d.a(aVar.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ap5 ap5, View view) {
            super(view);
            ee7.b(view, "view");
            this.d = ap5;
            View findViewById = view.findViewById(2131362641);
            ee7.a((Object) findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363304);
            ee7.a((Object) findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363381);
            ee7.a((Object) findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new View$OnClickListenerC0013a(this));
        }

        @DexIgnore
        public final void a(WatchFaceWrapper watchFaceWrapper, int i) {
            ee7.b(watchFaceWrapper, "watchFaceWrapper");
            Drawable combination = watchFaceWrapper.getCombination();
            if (combination != null) {
                this.a.setBackground(combination);
            } else {
                this.a.setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131231251));
            }
            this.b.setText(watchFaceWrapper.getName());
            if (i == this.d.a) {
                this.b.setTextColor(this.d.e());
                this.b.setTypeface(this.d.c());
                this.c.setVisibility(0);
                return;
            }
            this.b.setTextColor(this.d.f());
            this.b.setTypeface(this.d.c());
            this.c.setVisibility(8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(WatchFaceWrapper watchFaceWrapper);
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ap5(ArrayList arrayList, c cVar, int i, zd7 zd7) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final Typeface c() {
        return this.c;
    }

    @DexIgnore
    public final c d() {
        return this.f;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public final int f() {
        return this.d;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558679, viewGroup, false);
        ee7.a((Object) inflate, "LayoutInflater.from(pare\u2026ackground, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public ap5(ArrayList<WatchFaceWrapper> arrayList, c cVar) {
        ee7.b(arrayList, "mData");
        this.e = arrayList;
        this.f = cVar;
        String b2 = eh5.l.a().b("primaryText");
        String str = "#FFFFFF";
        this.b = Color.parseColor(b2 == null ? str : b2);
        this.c = eh5.l.a().c("descriptionText1");
        String b3 = eh5.l.a().b("nonBrandNonReachGoal");
        this.d = Color.parseColor(b3 != null ? b3 : str);
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        ee7.b(list, "data");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        ee7.b(str, "id");
        Iterator<WatchFaceWrapper> it = this.e.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (ee7.a((Object) it.next().getId(), (Object) str)) {
                break;
            } else {
                i++;
            }
        }
        a(i);
        return i;
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchFaceWrapper watchFaceWrapper = this.e.get(i);
            ee7.a((Object) watchFaceWrapper, "mData[position]");
            aVar.a(watchFaceWrapper, i);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        ee7.b(cVar, "listener");
        this.f = cVar;
    }
}
