package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class se7<T> implements Serializable {
    @DexIgnore
    public T element;

    @DexIgnore
    public String toString() {
        return String.valueOf(this.element);
    }
}
