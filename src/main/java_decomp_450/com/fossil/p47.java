package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ a47 b;

    @DexIgnore
    public p47(Context context, a47 a47) {
        this.a = context;
        this.b = a47;
    }

    @DexIgnore
    public final void run() {
        Context context = this.a;
        if (context == null) {
            z37.m.d("The Context of StatService.onResume() can not be null!");
        } else {
            z37.a(context, v57.k(context), this.b);
        }
    }
}
