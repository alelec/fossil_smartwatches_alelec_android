package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z55 extends y55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i Q; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray R;
    @DexIgnore
    public long P;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        R = sparseIntArray;
        sparseIntArray.put(2131362776, 1);
        R.put(2131362656, 2);
        R.put(2131362927, 3);
        R.put(2131363342, 4);
        R.put(2131363254, 5);
        R.put(2131363100, 6);
        R.put(2131362608, 7);
        R.put(2131362223, 8);
        R.put(2131362649, 9);
        R.put(2131362614, 10);
        R.put(2131362231, 11);
        R.put(2131363266, 12);
        R.put(2131363265, 13);
        R.put(2131361920, 14);
        R.put(2131361922, 15);
        R.put(2131363330, 16);
        R.put(2131362684, 17);
        R.put(2131362689, 18);
        R.put(2131362631, 19);
        R.put(2131362740, 20);
        R.put(2131362739, 21);
        R.put(2131363275, 22);
        R.put(2131363293, 23);
        R.put(2131361936, 24);
    }
    */

    @DexIgnore
    public z55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 25, Q, R));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.P = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.P != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.P = 1;
        }
        g();
    }

    @DexIgnore
    public z55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (Barrier) objArr[14], (Barrier) objArr[15], (FlexibleButton) objArr[24], (FlexibleTextInputEditText) objArr[8], (FlexibleTextInputEditText) objArr[11], (FlexibleTextInputLayout) objArr[7], (FlexibleTextInputLayout) objArr[10], (FloatingActionButton) objArr[19], (RTLImageView) objArr[9], (RTLImageView) objArr[2], (ImageView) objArr[17], (ImageView) objArr[18], (ConstraintLayout) objArr[21], (ConstraintLayout) objArr[20], (ConstraintLayout) objArr[1], (DashBar) objArr[3], (ConstraintLayout) objArr[0], (ScrollView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[22], (FlexibleButton) objArr[23], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[4]);
        this.P = -1;
        ((y55) this).G.setTag(null);
        a(view);
        f();
    }
}
