package com.fossil;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uo4 implements Factory<to4> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> b;

    @DexIgnore
    public uo4(Provider<ch5> provider, Provider<ApiServiceV2> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static uo4 a(Provider<ch5> provider, Provider<ApiServiceV2> provider2) {
        return new uo4(provider, provider2);
    }

    @DexIgnore
    public static to4 a(ch5 ch5, ApiServiceV2 apiServiceV2) {
        return new to4(ch5, apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public to4 get() {
        return a(this.a.get(), this.b.get());
    }
}
