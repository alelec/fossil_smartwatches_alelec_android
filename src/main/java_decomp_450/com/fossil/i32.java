package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import com.fossil.u12;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i32 {
    @DexIgnore
    public /* final */ int a;

    @DexIgnore
    public i32(int i) {
        this.a = i;
    }

    @DexIgnore
    public static Status a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (v92.b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    @DexIgnore
    public abstract void a(j22 j22, boolean z);

    @DexIgnore
    public abstract void a(u12.a<?> aVar) throws DeadObjectException;

    @DexIgnore
    public abstract void a(Status status);

    @DexIgnore
    public abstract void a(Exception exc);
}
