package com.fossil;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.s62;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m62 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<m62> CREATOR; // = new o82();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public String d;
    @DexIgnore
    public IBinder e;
    @DexIgnore
    public Scope[] f;
    @DexIgnore
    public Bundle g;
    @DexIgnore
    public Account h;
    @DexIgnore
    public k02[] i;
    @DexIgnore
    public k02[] j;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public int q;

    @DexIgnore
    public m62(int i2) {
        this.a = 4;
        this.c = m02.a;
        this.b = i2;
        this.p = true;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d, false);
        k72.a(parcel, 5, this.e, false);
        k72.a(parcel, 6, (Parcelable[]) this.f, i2, false);
        k72.a(parcel, 7, this.g, false);
        k72.a(parcel, 8, (Parcelable) this.h, i2, false);
        k72.a(parcel, 10, (Parcelable[]) this.i, i2, false);
        k72.a(parcel, 11, (Parcelable[]) this.j, i2, false);
        k72.a(parcel, 12, this.p);
        k72.a(parcel, 13, this.q);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public m62(int i2, int i3, int i4, String str, IBinder iBinder, Scope[] scopeArr, Bundle bundle, Account account, k02[] k02Arr, k02[] k02Arr2, boolean z, int i5) {
        this.a = i2;
        this.b = i3;
        this.c = i4;
        if ("com.google.android.gms".equals(str)) {
            this.d = "com.google.android.gms";
        } else {
            this.d = str;
        }
        if (i2 < 2) {
            this.h = iBinder != null ? f62.a(s62.a.a(iBinder)) : null;
        } else {
            this.e = iBinder;
            this.h = account;
        }
        this.f = scopeArr;
        this.g = bundle;
        this.i = k02Arr;
        this.j = k02Arr2;
        this.p = z;
        this.q = i5;
    }
}
