package com.fossil;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on4 implements nn4 {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<mn4> b;
    @DexIgnore
    public /* final */ ku4 c; // = new ku4();
    @DexIgnore
    public /* final */ vh<in4> d;
    @DexIgnore
    public /* final */ vh<hn4> e;
    @DexIgnore
    public /* final */ kn4 f; // = new kn4();
    @DexIgnore
    public /* final */ vh<yn4> g;
    @DexIgnore
    public /* final */ zn4 h; // = new zn4();
    @DexIgnore
    public /* final */ vh<jn4> i;
    @DexIgnore
    public /* final */ uh<yn4> j;
    @DexIgnore
    public /* final */ ji k;
    @DexIgnore
    public /* final */ ji l;
    @DexIgnore
    public /* final */ ji m;
    @DexIgnore
    public /* final */ ji n;
    @DexIgnore
    public /* final */ ji o;
    @DexIgnore
    public /* final */ ji p;
    @DexIgnore
    public /* final */ ji q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ji {
        @DexIgnore
        public a(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM fitness";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ji {
        @DexIgnore
        public b(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM display_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ji {
        @DexIgnore
        public c(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM challenge_player";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ji {
        @DexIgnore
        public d(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM history_challenge";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends ji {
        @DexIgnore
        public e(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM players";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Callable<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public f(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public mn4 call() throws Exception {
            mn4 mn4;
            Integer num;
            Integer num2;
            Integer num3;
            Cursor a2 = pi.a(on4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "type");
                int b4 = oi.b(a2, "name");
                int b5 = oi.b(a2, "des");
                int b6 = oi.b(a2, "owner");
                int b7 = oi.b(a2, "numberOfPlayers");
                int b8 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
                int b9 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
                int b10 = oi.b(a2, "target");
                int b11 = oi.b(a2, "duration");
                int b12 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int b13 = oi.b(a2, "version");
                int b14 = oi.b(a2, "status");
                int b15 = oi.b(a2, "syncData");
                int b16 = oi.b(a2, "createdAt");
                int b17 = oi.b(a2, "updatedAt");
                int b18 = oi.b(a2, "category");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    eo4 a3 = on4.this.c.a(a2.getString(b6));
                    if (a2.isNull(b7)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b7));
                    }
                    Date b19 = on4.this.c.b(a2.getString(b8));
                    Date b20 = on4.this.c.b(a2.getString(b9));
                    if (a2.isNull(b10)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b10));
                    }
                    if (a2.isNull(b11)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b11));
                    }
                    mn4 = new mn4(string, string2, string3, string4, a3, num, b19, b20, num2, num3, a2.getString(b12), a2.getString(b13), a2.getString(b14), a2.getString(b15), on4.this.c.b(a2.getString(b16)), on4.this.c.b(a2.getString(b17)), a2.getString(b18));
                } else {
                    mn4 = null;
                }
                return mn4;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public g(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public mn4 call() throws Exception {
            mn4 mn4;
            Integer num;
            Integer num2;
            Integer num3;
            Cursor a2 = pi.a(on4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "type");
                int b4 = oi.b(a2, "name");
                int b5 = oi.b(a2, "des");
                int b6 = oi.b(a2, "owner");
                int b7 = oi.b(a2, "numberOfPlayers");
                int b8 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
                int b9 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
                int b10 = oi.b(a2, "target");
                int b11 = oi.b(a2, "duration");
                int b12 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int b13 = oi.b(a2, "version");
                int b14 = oi.b(a2, "status");
                int b15 = oi.b(a2, "syncData");
                int b16 = oi.b(a2, "createdAt");
                int b17 = oi.b(a2, "updatedAt");
                int b18 = oi.b(a2, "category");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    eo4 a3 = on4.this.c.a(a2.getString(b6));
                    if (a2.isNull(b7)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b7));
                    }
                    Date b19 = on4.this.c.b(a2.getString(b8));
                    Date b20 = on4.this.c.b(a2.getString(b9));
                    if (a2.isNull(b10)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b10));
                    }
                    if (a2.isNull(b11)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b11));
                    }
                    mn4 = new mn4(string, string2, string3, string4, a3, num, b19, b20, num2, num3, a2.getString(b12), a2.getString(b13), a2.getString(b14), a2.getString(b15), on4.this.c.b(a2.getString(b16)), on4.this.c.b(a2.getString(b17)), a2.getString(b18));
                } else {
                    mn4 = null;
                }
                return mn4;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends vh<mn4> {
        @DexIgnore
        public h(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, mn4 mn4) {
            if (mn4.f() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, mn4.f());
            }
            if (mn4.u() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, mn4.u());
            }
            if (mn4.g() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, mn4.g());
            }
            if (mn4.c() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, mn4.c());
            }
            String a2 = on4.this.c.a(mn4.i());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            if (mn4.h() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) mn4.h().intValue());
            }
            String a3 = on4.this.c.a(mn4.m());
            if (a3 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a3);
            }
            String a4 = on4.this.c.a(mn4.e());
            if (a4 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a4);
            }
            if (mn4.t() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindLong(9, (long) mn4.t().intValue());
            }
            if (mn4.d() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindLong(10, (long) mn4.d().intValue());
            }
            if (mn4.j() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, mn4.j());
            }
            if (mn4.w() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, mn4.w());
            }
            if (mn4.p() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, mn4.p());
            }
            if (mn4.s() == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, mn4.s());
            }
            String a5 = on4.this.c.a(mn4.b());
            if (a5 == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindString(15, a5);
            }
            String a6 = on4.this.c.a(mn4.v());
            if (a6 == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, a6);
            }
            if (mn4.a() == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, mn4.a());
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`syncData`,`createdAt`,`updatedAt`,`category`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements Callable<List<yn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public i(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<yn4> call() throws Exception {
            Integer num;
            Integer num2;
            Integer num3;
            Cursor a2 = pi.a(on4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "type");
                int b4 = oi.b(a2, "name");
                int b5 = oi.b(a2, "des");
                int b6 = oi.b(a2, "owner");
                int b7 = oi.b(a2, "numberOfPlayers");
                int b8 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
                int b9 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
                int b10 = oi.b(a2, "target");
                int b11 = oi.b(a2, "duration");
                int b12 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int b13 = oi.b(a2, "version");
                int b14 = oi.b(a2, "status");
                int b15 = oi.b(a2, "players");
                int b16 = oi.b(a2, "topPlayer");
                int b17 = oi.b(a2, "currentPlayer");
                int b18 = oi.b(a2, "createdAt");
                int b19 = oi.b(a2, "updatedAt");
                int b20 = oi.b(a2, "completedAt");
                int b21 = oi.b(a2, "isFirst");
                int i = b14;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    eo4 a3 = on4.this.h.a(a2.getString(b6));
                    if (a2.isNull(b7)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b7));
                    }
                    Date b22 = on4.this.c.b(a2.getString(b8));
                    Date b23 = on4.this.c.b(a2.getString(b9));
                    if (a2.isNull(b10)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b10));
                    }
                    if (a2.isNull(b11)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b11));
                    }
                    String string5 = a2.getString(b12);
                    String string6 = a2.getString(b13);
                    String string7 = a2.getString(i);
                    i = i;
                    String string8 = a2.getString(b15);
                    b15 = b15;
                    List<jn4> c = on4.this.h.c(string8);
                    String string9 = a2.getString(b16);
                    b16 = b16;
                    jn4 b24 = on4.this.h.b(string9);
                    String string10 = a2.getString(b17);
                    b17 = b17;
                    jn4 b25 = on4.this.h.b(string10);
                    String string11 = a2.getString(b18);
                    b18 = b18;
                    Date b26 = on4.this.c.b(string11);
                    String string12 = a2.getString(b19);
                    b19 = b19;
                    Date b27 = on4.this.c.b(string12);
                    String string13 = a2.getString(b20);
                    b20 = b20;
                    arrayList.add(new yn4(string, string2, string3, string4, a3, num, b22, b23, num2, num3, string5, string6, string7, c, b24, b25, b26, b27, on4.this.c.b(string13), a2.getInt(b21) != 0));
                    b21 = b21;
                    b2 = b2;
                }
                return arrayList;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements Callable<yn4> {
        @DexIgnore
        public /* final */ /* synthetic */ fi a;

        @DexIgnore
        public j(fi fiVar) {
            this.a = fiVar;
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.a.c();
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public yn4 call() throws Exception {
            yn4 yn4;
            Integer num;
            Integer num2;
            Integer num3;
            Cursor a2 = pi.a(on4.this.a, this.a, false, null);
            try {
                int b2 = oi.b(a2, "id");
                int b3 = oi.b(a2, "type");
                int b4 = oi.b(a2, "name");
                int b5 = oi.b(a2, "des");
                int b6 = oi.b(a2, "owner");
                int b7 = oi.b(a2, "numberOfPlayers");
                int b8 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
                int b9 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
                int b10 = oi.b(a2, "target");
                int b11 = oi.b(a2, "duration");
                int b12 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
                int b13 = oi.b(a2, "version");
                int b14 = oi.b(a2, "status");
                int b15 = oi.b(a2, "players");
                int b16 = oi.b(a2, "topPlayer");
                int b17 = oi.b(a2, "currentPlayer");
                int b18 = oi.b(a2, "createdAt");
                int b19 = oi.b(a2, "updatedAt");
                int b20 = oi.b(a2, "completedAt");
                int b21 = oi.b(a2, "isFirst");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b2);
                    String string2 = a2.getString(b3);
                    String string3 = a2.getString(b4);
                    String string4 = a2.getString(b5);
                    eo4 a3 = on4.this.h.a(a2.getString(b6));
                    if (a2.isNull(b7)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b7));
                    }
                    Date b22 = on4.this.c.b(a2.getString(b8));
                    Date b23 = on4.this.c.b(a2.getString(b9));
                    if (a2.isNull(b10)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b10));
                    }
                    if (a2.isNull(b11)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b11));
                    }
                    yn4 = new yn4(string, string2, string3, string4, a3, num, b22, b23, num2, num3, a2.getString(b12), a2.getString(b13), a2.getString(b14), on4.this.h.c(a2.getString(b15)), on4.this.h.b(a2.getString(b16)), on4.this.h.b(a2.getString(b17)), on4.this.c.b(a2.getString(b18)), on4.this.c.b(a2.getString(b19)), on4.this.c.b(a2.getString(b20)), a2.getInt(b21) != 0);
                } else {
                    yn4 = null;
                }
                return yn4;
            } finally {
                a2.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends vh<in4> {
        @DexIgnore
        public k(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, in4 in4) {
            ajVar.bindLong(1, (long) in4.e());
            ajVar.bindLong(2, (long) in4.c());
            ajVar.bindLong(3, (long) in4.a());
            if (in4.b() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, in4.b());
            }
            ajVar.bindLong(5, (long) in4.d());
            if (in4.f() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindString(6, in4.f());
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `fitness` (`pinType`,`id`,`encryptMethod`,`encryptedData`,`keyType`,`sequence`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends vh<hn4> {
        @DexIgnore
        public l(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, hn4 hn4) {
            if (hn4.a() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, hn4.a());
            }
            if (hn4.c() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindLong(2, (long) hn4.c().intValue());
            }
            String a2 = on4.this.f.a(hn4.d());
            if (a2 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a2);
            }
            String a3 = on4.this.f.a(hn4.b());
            if (a3 == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, a3);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `display_player` (`challengeId`,`numberOfPlayers`,`topPlayers`,`nearPlayers`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class m extends vh<gn4> {
        @DexIgnore
        public m(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, gn4 gn4) {
            if (gn4.a() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, gn4.a());
            }
            if (gn4.b() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, gn4.b());
            }
            String a2 = on4.this.f.a(gn4.c());
            if (a2 == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, a2);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `challenge_player` (`challengeId`,`challengeName`,`players`) VALUES (?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class n extends vh<yn4> {
        @DexIgnore
        public n(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, yn4 yn4) {
            if (yn4.g() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, yn4.g());
            }
            if (yn4.q() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, yn4.q());
            }
            if (yn4.h() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, yn4.h());
            }
            if (yn4.d() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, yn4.d());
            }
            String a2 = on4.this.h.a(yn4.j());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            if (yn4.i() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) yn4.i().intValue());
            }
            String a3 = on4.this.c.a(yn4.m());
            if (a3 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a3);
            }
            String a4 = on4.this.c.a(yn4.f());
            if (a4 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a4);
            }
            if (yn4.o() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindLong(9, (long) yn4.o().intValue());
            }
            if (yn4.e() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindLong(10, (long) yn4.e().intValue());
            }
            if (yn4.l() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, yn4.l());
            }
            if (yn4.s() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, yn4.s());
            }
            if (yn4.n() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, yn4.n());
            }
            String a5 = on4.this.h.a(yn4.k());
            if (a5 == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, a5);
            }
            String a6 = on4.this.h.a(yn4.p());
            if (a6 == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindString(15, a6);
            }
            String a7 = on4.this.h.a(yn4.c());
            if (a7 == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, a7);
            }
            String a8 = on4.this.c.a(yn4.b());
            if (a8 == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, a8);
            }
            String a9 = on4.this.c.a(yn4.r());
            if (a9 == null) {
                ajVar.bindNull(18);
            } else {
                ajVar.bindString(18, a9);
            }
            String a10 = on4.this.c.a(yn4.a());
            if (a10 == null) {
                ajVar.bindNull(19);
            } else {
                ajVar.bindString(19, a10);
            }
            ajVar.bindLong(20, yn4.t() ? 1 : 0);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `history_challenge` (`id`,`type`,`name`,`des`,`owner`,`numberOfPlayers`,`startTime`,`endTime`,`target`,`duration`,`privacy`,`version`,`status`,`players`,`topPlayer`,`currentPlayer`,`createdAt`,`updatedAt`,`completedAt`,`isFirst`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class o extends vh<jn4> {
        @DexIgnore
        public o(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, jn4 jn4) {
            if (jn4.d() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, jn4.d());
            }
            if (jn4.i() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, jn4.i());
            }
            if (jn4.c() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, jn4.c());
            }
            if (jn4.e() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, jn4.e());
            }
            Integer valueOf = jn4.s() == null ? null : Integer.valueOf(jn4.s().booleanValue() ? 1 : 0);
            if (valueOf == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindLong(5, (long) valueOf.intValue());
            }
            if (jn4.h() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) jn4.h().intValue());
            }
            if (jn4.j() == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, jn4.j());
            }
            if (jn4.m() == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindLong(8, (long) jn4.m().intValue());
            }
            if (jn4.p() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindLong(9, (long) jn4.p().intValue());
            }
            if (jn4.a() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindLong(10, (long) jn4.a().intValue());
            }
            if (jn4.b() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindLong(11, (long) jn4.b().intValue());
            }
            if (jn4.g() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, jn4.g());
            }
            String a2 = on4.this.c.a(jn4.f());
            if (a2 == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, a2);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR REPLACE INTO `players` (`id`,`socialId`,`firstName`,`lastName`,`isDeleted`,`ranking`,`status`,`stepsBase`,`stepsOffset`,`caloriesBase`,`caloriesOffset`,`profilePicture`,`lastSync`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class p extends uh<yn4> {
        @DexIgnore
        public p(ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, yn4 yn4) {
            if (yn4.g() == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, yn4.g());
            }
            if (yn4.q() == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, yn4.q());
            }
            if (yn4.h() == null) {
                ajVar.bindNull(3);
            } else {
                ajVar.bindString(3, yn4.h());
            }
            if (yn4.d() == null) {
                ajVar.bindNull(4);
            } else {
                ajVar.bindString(4, yn4.d());
            }
            String a2 = on4.this.h.a(yn4.j());
            if (a2 == null) {
                ajVar.bindNull(5);
            } else {
                ajVar.bindString(5, a2);
            }
            if (yn4.i() == null) {
                ajVar.bindNull(6);
            } else {
                ajVar.bindLong(6, (long) yn4.i().intValue());
            }
            String a3 = on4.this.c.a(yn4.m());
            if (a3 == null) {
                ajVar.bindNull(7);
            } else {
                ajVar.bindString(7, a3);
            }
            String a4 = on4.this.c.a(yn4.f());
            if (a4 == null) {
                ajVar.bindNull(8);
            } else {
                ajVar.bindString(8, a4);
            }
            if (yn4.o() == null) {
                ajVar.bindNull(9);
            } else {
                ajVar.bindLong(9, (long) yn4.o().intValue());
            }
            if (yn4.e() == null) {
                ajVar.bindNull(10);
            } else {
                ajVar.bindLong(10, (long) yn4.e().intValue());
            }
            if (yn4.l() == null) {
                ajVar.bindNull(11);
            } else {
                ajVar.bindString(11, yn4.l());
            }
            if (yn4.s() == null) {
                ajVar.bindNull(12);
            } else {
                ajVar.bindString(12, yn4.s());
            }
            if (yn4.n() == null) {
                ajVar.bindNull(13);
            } else {
                ajVar.bindString(13, yn4.n());
            }
            String a5 = on4.this.h.a(yn4.k());
            if (a5 == null) {
                ajVar.bindNull(14);
            } else {
                ajVar.bindString(14, a5);
            }
            String a6 = on4.this.h.a(yn4.p());
            if (a6 == null) {
                ajVar.bindNull(15);
            } else {
                ajVar.bindString(15, a6);
            }
            String a7 = on4.this.h.a(yn4.c());
            if (a7 == null) {
                ajVar.bindNull(16);
            } else {
                ajVar.bindString(16, a7);
            }
            String a8 = on4.this.c.a(yn4.b());
            if (a8 == null) {
                ajVar.bindNull(17);
            } else {
                ajVar.bindString(17, a8);
            }
            String a9 = on4.this.c.a(yn4.r());
            if (a9 == null) {
                ajVar.bindNull(18);
            } else {
                ajVar.bindString(18, a9);
            }
            String a10 = on4.this.c.a(yn4.a());
            if (a10 == null) {
                ajVar.bindNull(19);
            } else {
                ajVar.bindString(19, a10);
            }
            ajVar.bindLong(20, yn4.t() ? 1 : 0);
            if (yn4.g() == null) {
                ajVar.bindNull(21);
            } else {
                ajVar.bindString(21, yn4.g());
            }
        }

        @DexIgnore
        @Override // com.fossil.uh, com.fossil.ji
        public String createQuery() {
            return "UPDATE OR ABORT `history_challenge` SET `id` = ?,`type` = ?,`name` = ?,`des` = ?,`owner` = ?,`numberOfPlayers` = ?,`startTime` = ?,`endTime` = ?,`target` = ?,`duration` = ?,`privacy` = ?,`version` = ?,`status` = ?,`players` = ?,`topPlayer` = ?,`currentPlayer` = ?,`createdAt` = ?,`updatedAt` = ?,`completedAt` = ?,`isFirst` = ? WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class q extends ji {
        @DexIgnore
        public q(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM challenge WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class r extends ji {
        @DexIgnore
        public r(on4 on4, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "DELETE FROM challenge";
        }
    }

    @DexIgnore
    public on4(ci ciVar) {
        this.a = ciVar;
        this.b = new h(ciVar);
        this.d = new k(this, ciVar);
        this.e = new l(ciVar);
        new m(ciVar);
        this.g = new n(ciVar);
        this.i = new o(ciVar);
        this.j = new p(ciVar);
        this.k = new q(this, ciVar);
        this.l = new r(this, ciVar);
        this.m = new a(this, ciVar);
        this.n = new b(this, ciVar);
        this.o = new c(this, ciVar);
        this.p = new d(this, ciVar);
        this.q = new e(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void e() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.p.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.p.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void f() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.q.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.q.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void g() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.o.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.o.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void h() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.m.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.m.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void i() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.n.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.n.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public long a(mn4 mn4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.b.insertAndReturnId(mn4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public Long[] b(List<yn4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.g.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public Long[] c(List<jn4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.i.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public Long[] d(List<hn4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.e.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public Long[] a(List<mn4> list) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.b.insertAndReturnIdsArrayBox(list);
            this.a.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void b() {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.l.acquire();
        this.a.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.l.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public LiveData<List<yn4>> c() {
        return this.a.getInvalidationTracker().a(new String[]{"history_challenge"}, false, (Callable) new i(fi.b("SELECT * FROM history_challenge ORDER BY completedAt DESC LIMIT 15", 0)));
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public List<in4> d() {
        fi b2 = fi.b("SELECT * FROM fitness", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "pinType");
            int b4 = oi.b(a2, "id");
            int b5 = oi.b(a2, "encryptMethod");
            int b6 = oi.b(a2, "encryptedData");
            int b7 = oi.b(a2, "keyType");
            int b8 = oi.b(a2, "sequence");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                in4 in4 = new in4(a2.getInt(b4), a2.getInt(b5), a2.getString(b6), a2.getInt(b7), a2.getString(b8));
                in4.a(a2.getInt(b3));
                arrayList.add(in4);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public yn4 c(String str) {
        fi fiVar;
        yn4 yn4;
        Integer num;
        Integer num2;
        Integer num3;
        fi b2 = fi.b("SELECT * FROM history_challenge WHERE id = ?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "type");
            int b5 = oi.b(a2, "name");
            int b6 = oi.b(a2, "des");
            int b7 = oi.b(a2, "owner");
            int b8 = oi.b(a2, "numberOfPlayers");
            int b9 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
            int b10 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
            int b11 = oi.b(a2, "target");
            int b12 = oi.b(a2, "duration");
            int b13 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int b14 = oi.b(a2, "version");
            int b15 = oi.b(a2, "status");
            fiVar = b2;
            try {
                int b16 = oi.b(a2, "players");
                int b17 = oi.b(a2, "topPlayer");
                int b18 = oi.b(a2, "currentPlayer");
                int b19 = oi.b(a2, "createdAt");
                int b20 = oi.b(a2, "updatedAt");
                int b21 = oi.b(a2, "completedAt");
                int b22 = oi.b(a2, "isFirst");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b3);
                    String string2 = a2.getString(b4);
                    String string3 = a2.getString(b5);
                    String string4 = a2.getString(b6);
                    eo4 a3 = this.h.a(a2.getString(b7));
                    if (a2.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b8));
                    }
                    Date b23 = this.c.b(a2.getString(b9));
                    Date b24 = this.c.b(a2.getString(b10));
                    if (a2.isNull(b11)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b11));
                    }
                    if (a2.isNull(b12)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b12));
                    }
                    yn4 = new yn4(string, string2, string3, string4, a3, num, b23, b24, num2, num3, a2.getString(b13), a2.getString(b14), a2.getString(b15), this.h.c(a2.getString(b16)), this.h.b(a2.getString(b17)), this.h.b(a2.getString(b18)), this.c.b(a2.getString(b19)), this.c.b(a2.getString(b20)), this.c.b(a2.getString(b21)), a2.getInt(b22) != 0);
                } else {
                    yn4 = null;
                }
                a2.close();
                fiVar.c();
                return yn4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public mn4 e(String str) {
        fi fiVar;
        mn4 mn4;
        Integer num;
        Integer num2;
        Integer num3;
        fi b2 = fi.b("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "type");
            int b5 = oi.b(a2, "name");
            int b6 = oi.b(a2, "des");
            int b7 = oi.b(a2, "owner");
            int b8 = oi.b(a2, "numberOfPlayers");
            int b9 = oi.b(a2, SampleRaw.COLUMN_START_TIME);
            int b10 = oi.b(a2, SampleRaw.COLUMN_END_TIME);
            int b11 = oi.b(a2, "target");
            int b12 = oi.b(a2, "duration");
            int b13 = oi.b(a2, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int b14 = oi.b(a2, "version");
            int b15 = oi.b(a2, "status");
            fiVar = b2;
            try {
                int b16 = oi.b(a2, "syncData");
                int b17 = oi.b(a2, "createdAt");
                int b18 = oi.b(a2, "updatedAt");
                int b19 = oi.b(a2, "category");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b3);
                    String string2 = a2.getString(b4);
                    String string3 = a2.getString(b5);
                    String string4 = a2.getString(b6);
                    eo4 a3 = this.c.a(a2.getString(b7));
                    if (a2.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(b8));
                    }
                    Date b20 = this.c.b(a2.getString(b9));
                    Date b21 = this.c.b(a2.getString(b10));
                    if (a2.isNull(b11)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(b11));
                    }
                    if (a2.isNull(b12)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(b12));
                    }
                    mn4 = new mn4(string, string2, string3, string4, a3, num, b20, b21, num2, num3, a2.getString(b13), a2.getString(b14), a2.getString(b15), a2.getString(b16), this.c.b(a2.getString(b17)), this.c.b(a2.getString(b18)), a2.getString(b19));
                } else {
                    mn4 = null;
                }
                a2.close();
                fiVar.c();
                return mn4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public int f(String str) {
        fi b2 = fi.b("SELECT COUNT(*) FROM challenge WHERE id =?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        int i2 = 0;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                i2 = a2.getInt(0);
            }
            return i2;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public LiveData<mn4> g(String str) {
        fi b2 = fi.b("SELECT * FROM challenge WHERE id = ?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        return this.a.getInvalidationTracker().a(new String[]{"challenge"}, false, (Callable) new f(b2));
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public long a(in4 in4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            long insertAndReturnId = this.d.insertAndReturnId(in4);
            this.a.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public mn4 b(String[] strArr, Date date) {
        fi fiVar;
        mn4 mn4;
        Integer num;
        Integer num2;
        Integer num3;
        StringBuilder a2 = si.a();
        a2.append("SELECT ");
        a2.append(vt7.ANY_MARKER);
        a2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        si.a(a2, length);
        a2.append(") AND endTime > ");
        a2.append("?");
        a2.append(" LIMIT 1");
        int i2 = 1;
        int i3 = length + 1;
        fi b2 = fi.b(a2.toString(), i3);
        for (String str : strArr) {
            if (str == null) {
                b2.bindNull(i2);
            } else {
                b2.bindString(i2, str);
            }
            i2++;
        }
        String a3 = this.c.a(date);
        if (a3 == null) {
            b2.bindNull(i3);
        } else {
            b2.bindString(i3, a3);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a4 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a4, "id");
            int b4 = oi.b(a4, "type");
            int b5 = oi.b(a4, "name");
            int b6 = oi.b(a4, "des");
            int b7 = oi.b(a4, "owner");
            int b8 = oi.b(a4, "numberOfPlayers");
            int b9 = oi.b(a4, SampleRaw.COLUMN_START_TIME);
            int b10 = oi.b(a4, SampleRaw.COLUMN_END_TIME);
            int b11 = oi.b(a4, "target");
            int b12 = oi.b(a4, "duration");
            int b13 = oi.b(a4, ShareConstants.WEB_DIALOG_PARAM_PRIVACY);
            int b14 = oi.b(a4, "version");
            int b15 = oi.b(a4, "status");
            fiVar = b2;
            try {
                int b16 = oi.b(a4, "syncData");
                int b17 = oi.b(a4, "createdAt");
                int b18 = oi.b(a4, "updatedAt");
                int b19 = oi.b(a4, "category");
                if (a4.moveToFirst()) {
                    String string = a4.getString(b3);
                    String string2 = a4.getString(b4);
                    String string3 = a4.getString(b5);
                    String string4 = a4.getString(b6);
                    eo4 a5 = this.c.a(a4.getString(b7));
                    if (a4.isNull(b8)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a4.getInt(b8));
                    }
                    Date b20 = this.c.b(a4.getString(b9));
                    Date b21 = this.c.b(a4.getString(b10));
                    if (a4.isNull(b11)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a4.getInt(b11));
                    }
                    if (a4.isNull(b12)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a4.getInt(b12));
                    }
                    mn4 = new mn4(string, string2, string3, string4, a5, num, b20, b21, num2, num3, a4.getString(b13), a4.getString(b14), a4.getString(b15), a4.getString(b16), this.c.b(a4.getString(b17)), this.c.b(a4.getString(b18)), a4.getString(b19));
                } else {
                    mn4 = null;
                }
                a4.close();
                fiVar.c();
                return mn4;
            } catch (Throwable th) {
                th = th;
                a4.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fiVar = b2;
            a4.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public int a(yn4 yn4) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            int handle = this.j.handle(yn4) + 0;
            this.a.setTransactionSuccessful();
            return handle;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public int a(String str) {
        this.a.assertNotSuspendingTransaction();
        aj acquire = this.k.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.a.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.a.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.a.endTransaction();
            this.k.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public LiveData<yn4> d(String str) {
        fi b2 = fi.b("SELECT * FROM history_challenge WHERE id = ? limit 1", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        return this.a.getInvalidationTracker().a(new String[]{"history_challenge"}, false, (Callable) new j(b2));
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public LiveData<mn4> a(String[] strArr, Date date) {
        StringBuilder a2 = si.a();
        a2.append("SELECT ");
        a2.append(vt7.ANY_MARKER);
        a2.append(" FROM challenge WHERE status IN (");
        int length = strArr.length;
        si.a(a2, length);
        a2.append(") AND endTime > ");
        a2.append("?");
        a2.append(" LIMIT 1");
        int i2 = 1;
        int i3 = length + 1;
        fi b2 = fi.b(a2.toString(), i3);
        for (String str : strArr) {
            if (str == null) {
                b2.bindNull(i2);
            } else {
                b2.bindString(i2, str);
            }
            i2++;
        }
        String a3 = this.c.a(date);
        if (a3 == null) {
            b2.bindNull(i3);
        } else {
            b2.bindString(i3, a3);
        }
        return this.a.getInvalidationTracker().a(new String[]{"challenge"}, false, (Callable) new g(b2));
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public List<jn4> a() {
        fi fiVar;
        Integer num;
        Boolean bool;
        Integer num2;
        Integer num3;
        Integer num4;
        Integer num5;
        Integer num6;
        fi b2 = fi.b("SELECT * FROM players", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "id");
            int b4 = oi.b(a2, "socialId");
            int b5 = oi.b(a2, Constants.PROFILE_KEY_FIRST_NAME);
            int b6 = oi.b(a2, Constants.PROFILE_KEY_LAST_NAME);
            int b7 = oi.b(a2, "isDeleted");
            int b8 = oi.b(a2, "ranking");
            int b9 = oi.b(a2, "status");
            int b10 = oi.b(a2, "stepsBase");
            int b11 = oi.b(a2, "stepsOffset");
            int b12 = oi.b(a2, "caloriesBase");
            int b13 = oi.b(a2, "caloriesOffset");
            int b14 = oi.b(a2, Constants.PROFILE_KEY_PROFILE_PIC);
            int b15 = oi.b(a2, "lastSync");
            fiVar = b2;
            try {
                try {
                    ArrayList arrayList = new ArrayList(a2.getCount());
                    while (a2.moveToNext()) {
                        String string = a2.getString(b3);
                        String string2 = a2.getString(b4);
                        String string3 = a2.getString(b5);
                        String string4 = a2.getString(b6);
                        if (a2.isNull(b7)) {
                            num = null;
                        } else {
                            num = Integer.valueOf(a2.getInt(b7));
                        }
                        if (num == null) {
                            bool = null;
                        } else {
                            bool = Boolean.valueOf(num.intValue() != 0);
                        }
                        if (a2.isNull(b8)) {
                            num2 = null;
                        } else {
                            num2 = Integer.valueOf(a2.getInt(b8));
                        }
                        String string5 = a2.getString(b9);
                        if (a2.isNull(b10)) {
                            num3 = null;
                        } else {
                            num3 = Integer.valueOf(a2.getInt(b10));
                        }
                        if (a2.isNull(b11)) {
                            num4 = null;
                        } else {
                            num4 = Integer.valueOf(a2.getInt(b11));
                        }
                        if (a2.isNull(b12)) {
                            num5 = null;
                        } else {
                            num5 = Integer.valueOf(a2.getInt(b12));
                        }
                        if (a2.isNull(b13)) {
                            num6 = null;
                        } else {
                            num6 = Integer.valueOf(a2.getInt(b13));
                        }
                        try {
                            arrayList.add(new jn4(string, string2, string3, string4, bool, num2, string5, num3, num4, num5, num6, a2.getString(b14), this.c.b(a2.getString(b15))));
                            b15 = b15;
                            b14 = b14;
                        } catch (Throwable th) {
                            th = th;
                            a2.close();
                            fiVar.c();
                            throw th;
                        }
                    }
                    a2.close();
                    fiVar.c();
                    return arrayList;
                } catch (Throwable th2) {
                    th = th2;
                    a2.close();
                    fiVar.c();
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                a2.close();
                fiVar.c();
                throw th;
            }
        } catch (Throwable th4) {
            th = th4;
            fiVar = b2;
            a2.close();
            fiVar.c();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public hn4 b(String str) {
        fi b2 = fi.b("SELECT * FROM display_player WHERE challengeId = ?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        hn4 hn4 = null;
        Integer num = null;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            int b3 = oi.b(a2, "challengeId");
            int b4 = oi.b(a2, "numberOfPlayers");
            int b5 = oi.b(a2, "topPlayers");
            int b6 = oi.b(a2, "nearPlayers");
            if (a2.moveToFirst()) {
                String string = a2.getString(b3);
                if (!a2.isNull(b4)) {
                    num = Integer.valueOf(a2.getInt(b4));
                }
                hn4 = new hn4(string, num, this.f.a(a2.getString(b5)), this.f.a(a2.getString(b6)));
            }
            return hn4;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.nn4
    public void a(String[] strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = si.a();
        a2.append("DELETE FROM challenge WHERE status IN (");
        si.a(a2, strArr.length);
        a2.append(")");
        aj compileStatement = this.a.compileStatement(a2.toString());
        int i2 = 1;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.bindNull(i2);
            } else {
                compileStatement.bindString(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            compileStatement.executeUpdateDelete();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }
}
