package com.fossil;

import android.os.Bundle;
import android.os.IInterface;
import android.os.RemoteException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bg3 extends IInterface {
    @DexIgnore
    String a(nm3 nm3) throws RemoteException;

    @DexIgnore
    List<em3> a(nm3 nm3, boolean z) throws RemoteException;

    @DexIgnore
    List<wm3> a(String str, String str2, nm3 nm3) throws RemoteException;

    @DexIgnore
    List<wm3> a(String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    List<em3> a(String str, String str2, String str3, boolean z) throws RemoteException;

    @DexIgnore
    List<em3> a(String str, String str2, boolean z, nm3 nm3) throws RemoteException;

    @DexIgnore
    void a(long j, String str, String str2, String str3) throws RemoteException;

    @DexIgnore
    void a(Bundle bundle, nm3 nm3) throws RemoteException;

    @DexIgnore
    void a(em3 em3, nm3 nm3) throws RemoteException;

    @DexIgnore
    void a(ub3 ub3, nm3 nm3) throws RemoteException;

    @DexIgnore
    void a(ub3 ub3, String str, String str2) throws RemoteException;

    @DexIgnore
    void a(wm3 wm3) throws RemoteException;

    @DexIgnore
    void a(wm3 wm3, nm3 nm3) throws RemoteException;

    @DexIgnore
    byte[] a(ub3 ub3, String str) throws RemoteException;

    @DexIgnore
    void b(nm3 nm3) throws RemoteException;

    @DexIgnore
    void c(nm3 nm3) throws RemoteException;

    @DexIgnore
    void d(nm3 nm3) throws RemoteException;
}
