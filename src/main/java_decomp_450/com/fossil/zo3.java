package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zo3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ no3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ap3 b;

    @DexIgnore
    public zo3(ap3 ap3, no3 no3) {
        this.b = ap3;
        this.a = no3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onComplete(this.a);
            }
        }
    }
}
