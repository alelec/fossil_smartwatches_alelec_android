package com.fossil;

import android.os.Bundle;
import com.fossil.za2;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gb2 implements db2<T> {
    @DexIgnore
    public /* final */ /* synthetic */ za2 a;

    @DexIgnore
    public gb2(za2 za2) {
        this.a = za2;
    }

    @DexIgnore
    @Override // com.fossil.db2
    public final void a(T t) {
        bb2 unused = this.a.a = (bb2) t;
        Iterator it = this.a.c.iterator();
        while (it.hasNext()) {
            ((za2.a) it.next()).a(this.a.a);
        }
        this.a.c.clear();
        Bundle unused2 = this.a.b = null;
    }
}
