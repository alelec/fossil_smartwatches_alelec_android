package com.fossil;

import com.fossil.ix;
import com.fossil.m00;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p00<Model, Data> implements m00<Model, Data> {
    @DexIgnore
    public /* final */ List<m00<Model, Data>> a;
    @DexIgnore
    public /* final */ b9<List<Throwable>> b;

    @DexIgnore
    public p00(List<m00<Model, Data>> list, b9<List<Throwable>> b9Var) {
        this.a = list;
        this.b = b9Var;
    }

    @DexIgnore
    @Override // com.fossil.m00
    public m00.a<Data> a(Model model, int i, int i2, ax axVar) {
        m00.a<Data> a2;
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        yw ywVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            m00<Model, Data> m00 = this.a.get(i3);
            if (m00.a(model) && (a2 = m00.a(model, i, i2, axVar)) != null) {
                ywVar = a2.a;
                arrayList.add(a2.c);
            }
        }
        if (arrayList.isEmpty() || ywVar == null) {
            return null;
        }
        return new m00.a<>(ywVar, new a(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements ix<Data>, ix.a<Data> {
        @DexIgnore
        public /* final */ List<ix<Data>> a;
        @DexIgnore
        public /* final */ b9<List<Throwable>> b;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public ew d;
        @DexIgnore
        public ix.a<? super Data> e;
        @DexIgnore
        public List<Throwable> f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public a(List<ix<Data>> list, b9<List<Throwable>> b9Var) {
            this.b = b9Var;
            u50.a((Collection) list);
            this.a = list;
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a(ew ewVar, ix.a<? super Data> aVar) {
            this.d = ewVar;
            this.e = aVar;
            this.f = this.b.a();
            this.a.get(this.c).a(ewVar, this);
            if (this.g) {
                cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public sw b() {
            return this.a.get(0).b();
        }

        @DexIgnore
        public final void c() {
            if (!this.g) {
                if (this.c < this.a.size() - 1) {
                    this.c++;
                    a(this.d, this.e);
                    return;
                }
                u50.a((Object) this.f);
                this.e.a((Exception) new py("Fetch failed", new ArrayList(this.f)));
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void cancel() {
            this.g = true;
            for (ix<Data> ixVar : this.a) {
                ixVar.cancel();
            }
        }

        @DexIgnore
        @Override // com.fossil.ix
        public Class<Data> getDataClass() {
            return this.a.get(0).getDataClass();
        }

        @DexIgnore
        @Override // com.fossil.ix
        public void a() {
            List<Throwable> list = this.f;
            if (list != null) {
                this.b.a(list);
            }
            this.f = null;
            for (ix<Data> ixVar : this.a) {
                ixVar.a();
            }
        }

        @DexIgnore
        @Override // com.fossil.ix.a
        public void a(Data data) {
            if (data != null) {
                this.e.a((Object) data);
            } else {
                c();
            }
        }

        @DexIgnore
        @Override // com.fossil.ix.a
        public void a(Exception exc) {
            List<Throwable> list = this.f;
            u50.a((Object) list);
            list.add(exc);
            c();
        }
    }

    @DexIgnore
    @Override // com.fossil.m00
    public boolean a(Model model) {
        for (m00<Model, Data> m00 : this.a) {
            if (m00.a(model)) {
                return true;
            }
        }
        return false;
    }
}
