package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze4 {
    @DexIgnore
    public static /* final */ int a; // = a();

    @DexIgnore
    public static int a() {
        return b(System.getProperty("java.version"));
    }

    @DexIgnore
    public static int b(String str) {
        int c = c(str);
        if (c == -1) {
            c = a(str);
        }
        if (c == -1) {
            return 6;
        }
        return c;
    }

    @DexIgnore
    public static int c(String str) {
        try {
            String[] split = str.split("[._]");
            int parseInt = Integer.parseInt(split[0]);
            return (parseInt != 1 || split.length <= 1) ? parseInt : Integer.parseInt(split[1]);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static int a(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (!Character.isDigit(charAt)) {
                    break;
                }
                sb.append(charAt);
            }
            return Integer.parseInt(sb.toString());
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static int b() {
        return a;
    }

    @DexIgnore
    public static boolean c() {
        return a >= 9;
    }
}
