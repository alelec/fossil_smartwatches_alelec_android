package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum db0 {
    GET,
    SET;
    
    @DexIgnore
    public static /* final */ a b; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final db0 a(yr0 yr0) {
            int i = sq1.a[yr0.ordinal()];
            if (i == 1) {
                return db0.GET;
            }
            if (i == 2) {
                return db0.SET;
            }
            throw new p87();
        }
    }
}
