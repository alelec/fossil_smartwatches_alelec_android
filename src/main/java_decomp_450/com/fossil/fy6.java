package com.fossil;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ey6;
import com.fossil.i0;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.ColorPanelView;
import com.portfolio.platform.view.ColorPickerView;
import java.util.Arrays;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fy6 extends ac implements ColorPickerView.c, TextWatcher {
    @DexIgnore
    public static /* final */ int[] z; // = {-769226, -1499549, -54125, -6543440, -10011977, -12627531, -14575885, -16537100, -16728876, -16738680, -11751600, -7617718, -3285959, -5317, -16121, -26624, -8825528, -10453621, -6381922};
    @DexIgnore
    public gy6 a;
    @DexIgnore
    public FrameLayout b;
    @DexIgnore
    public int[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public ey6 i;
    @DexIgnore
    public LinearLayout j;
    @DexIgnore
    public SeekBar p;
    @DexIgnore
    public TextView q;
    @DexIgnore
    public ColorPickerView r;
    @DexIgnore
    public ColorPanelView s;
    @DexIgnore
    public EditText t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public int v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public int x;
    @DexIgnore
    public /* final */ View.OnTouchListener y; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements SeekBar.OnSeekBarChangeListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            ey6 ey6;
            fy6.this.q.setText(String.format(Locale.ENGLISH, "%d%%", Integer.valueOf((int) ((((double) i) * 100.0d) / 255.0d))));
            int i2 = 255 - i;
            int i3 = 0;
            while (true) {
                ey6 = fy6.this.i;
                int[] iArr = ey6.b;
                if (i3 >= iArr.length) {
                    break;
                }
                int i4 = iArr[i3];
                fy6.this.i.b[i3] = Color.argb(i2, Color.red(i4), Color.green(i4), Color.blue(i4));
                i3++;
            }
            ey6.notifyDataSetChanged();
            for (int i5 = 0; i5 < fy6.this.j.getChildCount(); i5++) {
                FrameLayout frameLayout = (FrameLayout) fy6.this.j.getChildAt(i5);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362157);
                ImageView imageView = (ImageView) frameLayout.findViewById(2131362154);
                if (frameLayout.getTag() == null) {
                    frameLayout.setTag(Integer.valueOf(colorPanelView.getBorderColor()));
                }
                int color = colorPanelView.getColor();
                int argb = Color.argb(i2, Color.red(color), Color.green(color), Color.blue(color));
                if (i2 <= 165) {
                    colorPanelView.setBorderColor(argb | -16777216);
                } else {
                    colorPanelView.setBorderColor(((Integer) frameLayout.getTag()).intValue());
                }
                if (colorPanelView.getTag() != null && ((Boolean) colorPanelView.getTag()).booleanValue()) {
                    if (i2 <= 165) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else if (e7.a(argb) >= 0.65d) {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    } else {
                        imageView.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
                    }
                }
                colorPanelView.setColor(argb);
            }
            int red = Color.red(fy6.this.d);
            int green = Color.green(fy6.this.d);
            int blue = Color.blue(fy6.this.d);
            fy6.this.d = Color.argb(i2, red, green, blue);
        }

        @DexIgnore
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @DexIgnore
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnTouchListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            EditText editText = fy6.this.t;
            if (view == editText || !editText.hasFocus()) {
                return false;
            }
            fy6.this.t.clearFocus();
            ((InputMethodManager) fy6.this.getActivity().getSystemService("input_method")).hideSoftInputFromWindow(fy6.this.t.getWindowToken(), 0);
            fy6.this.t.clearFocus();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements DialogInterface.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(DialogInterface dialogInterface, int i) {
            fy6 fy6 = fy6.this;
            fy6.p(fy6.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements View.OnClickListener {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onClick(View view) {
            fy6.this.b.removeAllViews();
            fy6 fy6 = fy6.this;
            int i = fy6.e;
            if (i == 0) {
                fy6.e = 1;
                ((Button) view).setText(fy6.x != 0 ? fy6.this.x : 2131887348);
                fy6 fy62 = fy6.this;
                fy62.b.addView(fy62.a1());
            } else if (i == 1) {
                fy6.e = 0;
                ((Button) view).setText(fy6.v != 0 ? fy6.this.v : 2131887474);
                fy6 fy63 = fy6.this;
                fy63.b.addView(fy63.Z0());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements View.OnClickListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onClick(View view) {
            int color = fy6.this.s.getColor();
            fy6 fy6 = fy6.this;
            int i = fy6.d;
            if (color == i) {
                fy6.p(i);
                fy6.this.dismiss();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements View.OnFocusChangeListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onFocusChange(View view, boolean z) {
            if (z) {
                ((InputMethodManager) fy6.this.getActivity().getSystemService("input_method")).showSoftInput(fy6.this.t, 1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements ey6.a {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        @Override // com.fossil.ey6.a
        public void a(int i) {
            fy6 fy6 = fy6.this;
            int i2 = fy6.d;
            if (i2 == i) {
                fy6.p(i2);
                fy6.this.dismiss();
                return;
            }
            fy6.d = i;
            if (fy6.g) {
                fy6.n(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public h(fy6 fy6, ColorPanelView colorPanelView, int i) {
            this.a = colorPanelView;
            this.b = i;
        }

        @DexIgnore
        public void run() {
            this.a.setColor(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;

        @DexIgnore
        public i(ColorPanelView colorPanelView) {
            this.a = colorPanelView;
        }

        @DexIgnore
        public void onClick(View view) {
            if (!(view.getTag() instanceof Boolean) || !((Boolean) view.getTag()).booleanValue()) {
                fy6.this.d = this.a.getColor();
                fy6.this.i.a();
                for (int i = 0; i < fy6.this.j.getChildCount(); i++) {
                    FrameLayout frameLayout = (FrameLayout) fy6.this.j.getChildAt(i);
                    ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362157);
                    ImageView imageView = (ImageView) frameLayout.findViewById(2131362154);
                    imageView.setImageResource(colorPanelView == view ? 2131230940 : 0);
                    if ((colorPanelView != view || e7.a(colorPanelView.getColor()) < 0.65d) && Color.alpha(colorPanelView.getColor()) > 165) {
                        imageView.setColorFilter((ColorFilter) null);
                    } else {
                        imageView.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
                    }
                    colorPanelView.setTag(Boolean.valueOf(colorPanelView == view));
                }
                return;
            }
            fy6 fy6 = fy6.this;
            fy6.p(fy6.d);
            fy6.this.dismiss();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements View.OnLongClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ColorPanelView a;

        @DexIgnore
        public j(fy6 fy6, ColorPanelView colorPanelView) {
            this.a = colorPanelView;
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            this.a.c();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k {
        @DexIgnore
        public int a; // = 2131887464;
        @DexIgnore
        public int b; // = 2131887474;
        @DexIgnore
        public int c; // = 2131887348;
        @DexIgnore
        public int d; // = 2131887525;
        @DexIgnore
        public int e; // = 1;
        @DexIgnore
        public int[] f; // = fy6.z;
        @DexIgnore
        public int g; // = -16777216;
        @DexIgnore
        public int h; // = 0;
        @DexIgnore
        public boolean i; // = false;
        @DexIgnore
        public boolean j; // = true;
        @DexIgnore
        public boolean k; // = true;
        @DexIgnore
        public boolean l; // = true;
        @DexIgnore
        public int m; // = 1;

        @DexIgnore
        public k a(int i2) {
            this.g = i2;
            return this;
        }

        @DexIgnore
        public k b(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        public k c(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        public k a(boolean z) {
            this.j = z;
            return this;
        }

        @DexIgnore
        public k b(boolean z) {
            this.i = z;
            return this;
        }

        @DexIgnore
        public fy6 a() {
            fy6 fy6 = new fy6();
            Bundle bundle = new Bundle();
            bundle.putInt("id", this.h);
            bundle.putInt("dialogType", this.e);
            bundle.putInt(BaseFeatureModel.COLUMN_COLOR, this.g);
            bundle.putIntArray("presets", this.f);
            bundle.putBoolean("alpha", this.i);
            bundle.putBoolean("allowCustom", this.k);
            bundle.putBoolean("allowPresets", this.j);
            bundle.putInt("dialogTitle", this.a);
            bundle.putBoolean("showColorShades", this.l);
            bundle.putInt("colorShape", this.m);
            bundle.putInt("presetsButtonText", this.b);
            bundle.putInt("customButtonText", this.c);
            bundle.putInt("selectedButtonText", this.d);
            fy6.setArguments(bundle);
            return fy6;
        }
    }

    @DexIgnore
    public static k e1() {
        return new k();
    }

    @DexIgnore
    public final int V(String str) throws NumberFormatException {
        int i2;
        int i3;
        int parseInt;
        int parseInt2;
        if (str.startsWith("#")) {
            str = str.substring(1);
        }
        int i4 = -1;
        int i5 = 0;
        if (str.length() == 0) {
            i2 = 0;
        } else if (str.length() <= 2) {
            i2 = Integer.parseInt(str, 16);
        } else {
            if (str.length() == 3) {
                parseInt = Integer.parseInt(str.substring(0, 1), 16);
                i3 = Integer.parseInt(str.substring(1, 2), 16);
                parseInt2 = Integer.parseInt(str.substring(2, 3), 16);
            } else if (str.length() == 4) {
                int parseInt3 = Integer.parseInt(str.substring(0, 2), 16);
                i2 = Integer.parseInt(str.substring(2, 4), 16);
                i3 = parseInt3;
                i4 = 255;
                return Color.argb(i4, i5, i3, i2);
            } else if (str.length() == 5) {
                parseInt = Integer.parseInt(str.substring(0, 1), 16);
                i3 = Integer.parseInt(str.substring(1, 3), 16);
                parseInt2 = Integer.parseInt(str.substring(3, 5), 16);
            } else if (str.length() == 6) {
                parseInt = Integer.parseInt(str.substring(0, 2), 16);
                i3 = Integer.parseInt(str.substring(2, 4), 16);
                parseInt2 = Integer.parseInt(str.substring(4, 6), 16);
            } else {
                if (str.length() == 7) {
                    i4 = Integer.parseInt(str.substring(0, 1), 16);
                    int parseInt4 = Integer.parseInt(str.substring(1, 3), 16);
                    int parseInt5 = Integer.parseInt(str.substring(3, 5), 16);
                    i2 = Integer.parseInt(str.substring(5, 7), 16);
                    i5 = parseInt4;
                    i3 = parseInt5;
                } else if (str.length() == 8) {
                    i4 = Integer.parseInt(str.substring(0, 2), 16);
                    int parseInt6 = Integer.parseInt(str.substring(2, 4), 16);
                    int parseInt7 = Integer.parseInt(str.substring(4, 6), 16);
                    i2 = Integer.parseInt(str.substring(6, 8), 16);
                    i5 = parseInt6;
                    i3 = parseInt7;
                } else {
                    i2 = -1;
                    i3 = -1;
                    i5 = -1;
                }
                return Color.argb(i4, i5, i3, i2);
            }
            i5 = parseInt;
            i4 = 255;
            return Color.argb(i4, i5, i3, i2);
        }
        i3 = 0;
        i4 = 255;
        return Color.argb(i4, i5, i3, i2);
    }

    @DexIgnore
    public View Z0() {
        View inflate = View.inflate(getActivity(), 2131558457, null);
        this.r = (ColorPickerView) inflate.findViewById(2131362158);
        this.s = (ColorPanelView) inflate.findViewById(2131362155);
        this.t = (EditText) inflate.findViewById(2131362159);
        try {
            TypedValue typedValue = new TypedValue();
            getActivity().obtainStyledAttributes(typedValue.data, new int[]{16842806}).recycle();
        } catch (Exception unused) {
        }
        this.r.setAlphaSliderVisible(this.u);
        this.r.a(this.d, true);
        this.s.setColor(this.d);
        q(this.d);
        if (!this.u) {
            this.t.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        }
        this.s.setOnClickListener(new e());
        inflate.setOnTouchListener(this.y);
        this.r.setOnColorChangedListener(this);
        this.t.addTextChangedListener(this);
        this.t.setOnFocusChangeListener(new f());
        return inflate;
    }

    @DexIgnore
    public View a1() {
        View inflate = View.inflate(getActivity(), 2131558458, null);
        this.j = (LinearLayout) inflate.findViewById(2131363059);
        this.p = (SeekBar) inflate.findViewById(2131363175);
        this.q = (TextView) inflate.findViewById(2131363176);
        GridView gridView = (GridView) inflate.findViewById(2131362540);
        b1();
        if (this.g) {
            n(this.d);
        } else {
            this.j.setVisibility(8);
            inflate.findViewById(2131363058).setVisibility(8);
        }
        ey6 ey6 = new ey6(new g(), this.c, getSelectedItemPosition(), this.h);
        this.i = ey6;
        gridView.setAdapter((ListAdapter) ey6);
        if (this.u) {
            d1();
        } else {
            inflate.findViewById(2131363174).setVisibility(8);
            inflate.findViewById(2131363177).setVisibility(8);
        }
        return inflate;
    }

    @DexIgnore
    public void afterTextChanged(Editable editable) {
        int V;
        if (this.t.isFocused() && (V = V(editable.toString())) != this.r.getColor()) {
            this.w = true;
            this.r.a(V, true);
        }
    }

    @DexIgnore
    public final void b1() {
        int alpha = Color.alpha(this.d);
        int[] intArray = getArguments().getIntArray("presets");
        this.c = intArray;
        if (intArray == null) {
            this.c = z;
        }
        boolean z2 = this.c == z;
        int[] iArr = this.c;
        this.c = Arrays.copyOf(iArr, iArr.length);
        if (alpha != 255) {
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.c;
                if (i2 >= iArr2.length) {
                    break;
                }
                int i3 = iArr2[i2];
                this.c[i2] = Color.argb(alpha, Color.red(i3), Color.green(i3), Color.blue(i3));
                i2++;
            }
        }
        this.c = b(this.c, this.d);
        int i4 = getArguments().getInt(BaseFeatureModel.COLUMN_COLOR);
        if (i4 != this.d) {
            this.c = b(this.c, i4);
        }
        if (z2) {
            int[] iArr3 = this.c;
            if (iArr3.length == 19) {
                this.c = a(iArr3, Color.argb(alpha, 0, 0, 0));
            }
        }
    }

    @DexIgnore
    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public final void c1() {
        if (this.a != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.a.j(this.f);
            return;
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof gy6) {
            ((gy6) activity).j(this.f);
        }
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager instanceof gy6) {
            ((gy6) fragmentManager).b(this.f, this.d);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ColorPickerDialog", "fm=" + fragmentManager);
    }

    @DexIgnore
    public final void d1() {
        int alpha = 255 - Color.alpha(this.d);
        this.p.setMax(255);
        this.p.setProgress(alpha);
        this.q.setText(String.format(Locale.ENGLISH, "%d%%", Integer.valueOf((int) ((((double) alpha) * 100.0d) / 255.0d))));
        this.p.setOnSeekBarChangeListener(new a());
    }

    @DexIgnore
    public final int getSelectedItemPosition() {
        int i2 = 0;
        while (true) {
            int[] iArr = this.c;
            if (i2 >= iArr.length) {
                return -1;
            }
            if (iArr[i2] == this.d) {
                return i2;
            }
            i2++;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.view.ColorPickerView.c
    public void m(int i2) {
        this.d = i2;
        ColorPanelView colorPanelView = this.s;
        if (colorPanelView != null) {
            colorPanelView.setColor(i2);
        }
        if (!this.w && this.t != null) {
            q(i2);
            if (this.t.hasFocus()) {
                ((InputMethodManager) getActivity().getSystemService("input_method")).hideSoftInputFromWindow(this.t.getWindowToken(), 0);
                this.t.clearFocus();
            }
        }
        this.w = false;
    }

    @DexIgnore
    public void n(int i2) {
        int[] o = o(i2);
        if (this.j.getChildCount() != 0) {
            for (int i3 = 0; i3 < this.j.getChildCount(); i3++) {
                FrameLayout frameLayout = (FrameLayout) this.j.getChildAt(i3);
                ColorPanelView colorPanelView = (ColorPanelView) frameLayout.findViewById(2131362157);
                colorPanelView.setColor(o[i3]);
                colorPanelView.setTag(false);
                ((ImageView) frameLayout.findViewById(2131362154)).setImageDrawable(null);
            }
            return;
        }
        int dimensionPixelSize = getResources().getDimensionPixelSize(2131165314);
        for (int i4 : o) {
            View inflate = View.inflate(getActivity(), this.h == 0 ? 2131558456 : 2131558455, null);
            ColorPanelView colorPanelView2 = (ColorPanelView) inflate.findViewById(2131362157);
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) colorPanelView2.getLayoutParams();
            marginLayoutParams.rightMargin = dimensionPixelSize;
            marginLayoutParams.leftMargin = dimensionPixelSize;
            colorPanelView2.setLayoutParams(marginLayoutParams);
            colorPanelView2.setColor(i4);
            this.j.addView(inflate);
            colorPanelView2.post(new h(this, colorPanelView2, i4));
            colorPanelView2.setOnClickListener(new i(colorPanelView2));
            colorPanelView2.setOnLongClickListener(new j(this, colorPanelView2));
        }
    }

    @DexIgnore
    public final int[] o(int i2) {
        return new int[]{a(i2, 0.9d), a(i2, 0.7d), a(i2, 0.5d), a(i2, 0.333d), a(i2, 0.166d), a(i2, -0.125d), a(i2, -0.25d), a(i2, -0.375d), a(i2, -0.5d), a(i2, -0.675d), a(i2, -0.7d), a(i2, -0.775d)};
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onAttach(Context context) {
        super.onAttach(context);
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null && (parentFragment instanceof gy6)) {
            this.a = (gy6) parentFragment;
        }
        if (this.a == null && (context instanceof gy6)) {
            this.a = (gy6) context;
        }
    }

    @DexIgnore
    @Override // com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        int i2;
        this.f = getArguments().getInt("id");
        this.u = getArguments().getBoolean("alpha");
        this.g = getArguments().getBoolean("showColorShades");
        this.h = getArguments().getInt("colorShape");
        if (bundle == null) {
            this.d = getArguments().getInt(BaseFeatureModel.COLUMN_COLOR);
            this.e = getArguments().getInt("dialogType");
        } else {
            this.d = bundle.getInt(BaseFeatureModel.COLUMN_COLOR);
            this.e = bundle.getInt("dialogType");
        }
        FrameLayout frameLayout = new FrameLayout(requireActivity());
        this.b = frameLayout;
        int i3 = this.e;
        if (i3 == 0) {
            frameLayout.addView(Z0());
        } else if (i3 == 1) {
            frameLayout.addView(a1());
        }
        int i4 = getArguments().getInt("selectedButtonText");
        if (i4 == 0) {
            i4 = 2131887525;
        }
        i0.a aVar = new i0.a(requireActivity());
        aVar.b(this.b);
        aVar.b(i4, new c());
        int i5 = getArguments().getInt("dialogTitle");
        if (i5 != 0) {
            aVar.a(i5);
        }
        this.v = getArguments().getInt("presetsButtonText");
        this.x = getArguments().getInt("customButtonText");
        if (this.e == 0 && getArguments().getBoolean("allowPresets")) {
            i2 = this.v;
            if (i2 == 0) {
                i2 = 2131887348;
            }
        } else if (this.e != 1 || !getArguments().getBoolean("allowCustom")) {
            i2 = 0;
        } else {
            i2 = this.x;
            if (i2 == 0) {
                i2 = 2131887474;
            }
        }
        if (i2 != 0) {
            aVar.a(i2, (DialogInterface.OnClickListener) null);
        }
        return aVar.a();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onDetach() {
        super.onDetach();
        this.a = null;
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        c1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt(BaseFeatureModel.COLUMN_COLOR, this.d);
        bundle.putInt("dialogType", this.e);
        super.onSaveInstanceState(bundle);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.ac
    public void onStart() {
        super.onStart();
        i0 i0Var = (i0) getDialog();
        i0Var.getWindow().clearFlags(131080);
        i0Var.getWindow().setSoftInputMode(4);
        Button b2 = i0Var.b(-3);
        if (b2 != null) {
            b2.setOnClickListener(new d());
        }
    }

    @DexIgnore
    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
    }

    @DexIgnore
    public final void p(int i2) {
        if (this.a != null) {
            Log.w("ColorPickerDialog", "Using deprecated listener which may be remove in future releases");
            this.a.b(this.f, i2);
            return;
        }
        FragmentActivity activity = getActivity();
        if (activity instanceof gy6) {
            ((gy6) activity).b(this.f, i2);
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager instanceof gy6) {
                ((gy6) fragmentManager).b(this.f, i2);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ColorPickerDialog", "onColorSelected fm=" + fragmentManager);
            return;
        }
        throw new IllegalStateException("The activity must implement ColorPickerDialogListener");
    }

    @DexIgnore
    public final void q(int i2) {
        if (this.u) {
            this.t.setText(String.format("%08X", Integer.valueOf(i2)));
            return;
        }
        this.t.setText(String.format("%06X", Integer.valueOf(i2 & 16777215)));
    }

    @DexIgnore
    public final int[] b(int[] iArr, int i2) {
        boolean z2;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z2 = false;
                break;
            } else if (iArr[i3] == i2) {
                z2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (z2) {
            return iArr;
        }
        int length2 = iArr.length + 1;
        int[] iArr2 = new int[length2];
        iArr2[0] = i2;
        System.arraycopy(iArr, 0, iArr2, 1, length2 - 1);
        return iArr2;
    }

    @DexIgnore
    public final int a(int i2, double d2) {
        long parseLong = Long.parseLong(String.format("#%06X", Integer.valueOf(16777215 & i2)).substring(1), 16);
        double d3 = 0.0d;
        int i3 = (d2 > 0.0d ? 1 : (d2 == 0.0d ? 0 : -1));
        if (i3 >= 0) {
            d3 = 255.0d;
        }
        if (i3 < 0) {
            d2 *= -1.0d;
        }
        long j2 = parseLong >> 16;
        long j3 = (parseLong >> 8) & 255;
        long j4 = parseLong & 255;
        return Color.argb(Color.alpha(i2), (int) (Math.round((d3 - ((double) j2)) * d2) + j2), (int) (Math.round((d3 - ((double) j3)) * d2) + j3), (int) (Math.round((d3 - ((double) j4)) * d2) + j4));
    }

    @DexIgnore
    public final int[] a(int[] iArr, int i2) {
        boolean z2;
        int length = iArr.length;
        int i3 = 0;
        while (true) {
            if (i3 >= length) {
                z2 = false;
                break;
            } else if (iArr[i3] == i2) {
                z2 = true;
                break;
            } else {
                i3++;
            }
        }
        if (z2) {
            return iArr;
        }
        int length2 = iArr.length + 1;
        int[] iArr2 = new int[length2];
        int i4 = length2 - 1;
        iArr2[i4] = i2;
        System.arraycopy(iArr, 0, iArr2, 0, i4);
        return iArr2;
    }
}
