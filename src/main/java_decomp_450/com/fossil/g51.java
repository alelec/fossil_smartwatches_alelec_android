package com.fossil;

import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum g51 {
    LEGACY_ABORT_FILE((byte) 7),
    LEGACY_PUT_FILE((byte) 11),
    LEGACY_VERIFY_FILE((byte) 13),
    LEGACY_PUT_FILE_EOF(DateTimeFieldType.CLOCKHOUR_OF_HALFDAY),
    LEGACY_VERIFY_SEGMENT(DateTimeFieldType.CLOCKHOUR_OF_DAY),
    LEGACY_ERASE_SEGMENT(DateTimeFieldType.MINUTE_OF_DAY),
    LEGACY_GET_SIZE_WRITTEN(DateTimeFieldType.SECOND_OF_DAY),
    LEGACY_LIST_FILE((byte) 5),
    LEGACY_GET_FILE((byte) 1),
    LEGACY_ERASE_FILE((byte) 3);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public g51(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.a + 1);
    }
}
