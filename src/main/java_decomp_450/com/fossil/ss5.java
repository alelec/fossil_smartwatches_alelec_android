package com.fossil;

import android.text.SpannableString;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ss5 extends io5<rs5> {
    @DexIgnore
    void a();

    @DexIgnore
    void a(SpannableString spannableString);

    @DexIgnore
    void a(String str, ArrayList<Alarm> arrayList, Alarm alarm);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void b();

    @DexIgnore
    void b(SpannableString spannableString);

    @DexIgnore
    void c();

    @DexIgnore
    void d(boolean z);

    @DexIgnore
    void f(List<Alarm> list);

    @DexIgnore
    void l(String str);

    @DexIgnore
    void r();

    @DexIgnore
    void t();

    @DexIgnore
    void z();

    @DexIgnore
    void z(boolean z);
}
