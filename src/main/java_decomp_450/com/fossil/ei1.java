package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ei1 extends Enum<ei1> {
    @DexIgnore
    public static /* final */ ei1 b;
    @DexIgnore
    public static /* final */ ei1 c;
    @DexIgnore
    public static /* final */ /* synthetic */ ei1[] d;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        ei1 ei1 = new ei1("CLOCK_WISE", 0, (byte) 0);
        b = ei1;
        ei1 ei12 = new ei1("SHORTEST_PATH", 2, (byte) 2);
        c = ei12;
        d = new ei1[]{ei1, new ei1("COUNTER_CLOCK_WISE", 1, (byte) 1), ei12};
    }
    */

    @DexIgnore
    public ei1(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static ei1 valueOf(String str) {
        return (ei1) Enum.valueOf(ei1.class, str);
    }

    @DexIgnore
    public static ei1[] values() {
        return (ei1[]) d.clone();
    }
}
