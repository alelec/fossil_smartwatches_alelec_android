package com.fossil;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc7 {
    @DexIgnore
    public static /* synthetic */ long a(InputStream inputStream, OutputStream outputStream, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 8192;
        }
        return a(inputStream, outputStream, i);
    }

    @DexIgnore
    public static final long a(InputStream inputStream, OutputStream outputStream, int i) {
        ee7.b(inputStream, "$this$copyTo");
        ee7.b(outputStream, "out");
        byte[] bArr = new byte[i];
        int read = inputStream.read(bArr);
        long j = 0;
        while (read >= 0) {
            outputStream.write(bArr, 0, read);
            j += (long) read;
            read = inputStream.read(bArr);
        }
        return j;
    }

    @DexIgnore
    public static final byte[] a(InputStream inputStream) {
        ee7.b(inputStream, "$this$readBytes");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(Math.max(8192, inputStream.available()));
        a(inputStream, byteArrayOutputStream, 0, 2, null);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "buffer.toByteArray()");
        return byteArray;
    }
}
