package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum a71 {
    SEND_PHONE_RANDOM_NUMBER((byte) 1),
    SEND_BOTH_RANDOM_NUMBER((byte) 2),
    EXCHANGE_PUBLIC_KEY((byte) 3),
    PROCESS_USER_AUTHORIZATION((byte) 4),
    STOP_PROCESS((byte) 5);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public a71(byte b) {
        this.a = b;
    }
}
