package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m14 extends Exception {
    @DexIgnore
    @Deprecated
    public m14() {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m14(String str) {
        super(str);
        a72.a(str, (Object) "Detail message must not be empty");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m14(String str, Throwable th) {
        super(str, th);
        a72.a(str, (Object) "Detail message must not be empty");
    }
}
