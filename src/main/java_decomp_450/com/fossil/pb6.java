package com.fossil;

import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb6 implements Factory<ob6> {
    @DexIgnore
    public static ob6 a(mb6 mb6, UserRepository userRepository, ch5 ch5, GoalTrackingRepository goalTrackingRepository) {
        return new ob6(mb6, userRepository, ch5, goalTrackingRepository);
    }
}
