package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yw5 extends cl4 {
    @DexIgnore
    public boolean e;

    @DexIgnore
    public abstract void a(int i, boolean z, boolean z2);

    @DexIgnore
    public abstract void a(ArrayList<bt5> arrayList);

    @DexIgnore
    public final void a(boolean z) {
        this.e = z;
    }

    @DexIgnore
    public abstract void b(ArrayList<bt5> arrayList);

    @DexIgnore
    public abstract void c(ArrayList<String> arrayList);

    @DexIgnore
    public abstract void h();

    @DexIgnore
    public final boolean i() {
        return this.e;
    }

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();

    @DexIgnore
    public abstract void m();
}
