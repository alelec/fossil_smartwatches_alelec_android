package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je1 extends b91 {
    @DexIgnore
    public short Q;
    @DexIgnore
    public long R;
    @DexIgnore
    public /* final */ byte[] S;
    @DexIgnore
    public byte[] T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ je1(short s, ri1 ri1, int i, int i2) {
        super(g51.LEGACY_LIST_FILE, s, qa1.U, ri1, (i2 & 4) != 0 ? 3 : i);
        byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_LIST_FILE.a).array();
        ee7.a((Object) array, "ByteBuffer.allocate(1)\n \u2026ode)\n            .array()");
        this.S = array;
        byte[] array2 = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_LIST_FILE.a()).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(1)\n \u2026e())\n            .array()");
        this.T = array2;
    }

    @DexIgnore
    @Override // com.fossil.e71
    public void b(byte[] bArr) {
        this.T = bArr;
    }

    @DexIgnore
    @Override // com.fossil.b91
    public void c(byte[] bArr) {
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.wrap(received\u2026(ByteOrder.LITTLE_ENDIAN)");
        this.Q = yz0.b(order.get(0));
        this.R = yz0.b(order.getInt(1));
    }

    @DexIgnore
    @Override // com.fossil.e71, com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), r51.y0, yz0.a(((e71) this).K));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(yz0.a(super.h(), r51.f3, Short.valueOf(this.Q)), r51.g3, Long.valueOf(this.R));
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.e71
    public byte[] n() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.e71
    public byte[] q() {
        return this.T;
    }
}
