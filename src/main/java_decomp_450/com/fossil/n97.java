package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class n97<T> implements Iterator<T>, ye7 {
    @DexIgnore
    public ua7 a; // = ua7.NotReady;
    @DexIgnore
    public T b;

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void b(T t) {
        this.b = t;
        this.a = ua7.Ready;
    }

    @DexIgnore
    public final boolean c() {
        this.a = ua7.Failed;
        a();
        return this.a == ua7.Ready;
    }

    @DexIgnore
    public boolean hasNext() {
        if (this.a != ua7.Failed) {
            int i = m97.a[this.a.ordinal()];
            if (i == 1) {
                return false;
            }
            if (i != 2) {
                return c();
            }
            return true;
        }
        throw new IllegalArgumentException("Failed requirement.".toString());
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        if (hasNext()) {
            this.a = ua7.NotReady;
            return this.b;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final void b() {
        this.a = ua7.Done;
    }
}
