package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.transition.Transition;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.HeaderViewListAdapter;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import androidx.appcompat.view.menu.ListMenuItemView;
import androidx.appcompat.widget.ListPopupWindow;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v2 extends ListPopupWindow implements u2 {
    @DexIgnore
    public static Method O;
    @DexIgnore
    public u2 N;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends r2 {
        @DexIgnore
        public /* final */ int t;
        @DexIgnore
        public /* final */ int u;
        @DexIgnore
        public u2 v;
        @DexIgnore
        public MenuItem w;

        @DexIgnore
        public a(Context context, boolean z) {
            super(context, z);
            Configuration configuration = context.getResources().getConfiguration();
            if (Build.VERSION.SDK_INT < 17 || 1 != configuration.getLayoutDirection()) {
                this.t = 22;
                this.u = 21;
                return;
            }
            this.t = 21;
            this.u = 22;
        }

        @DexIgnore
        @Override // com.fossil.r2
        public boolean onHoverEvent(MotionEvent motionEvent) {
            int i;
            o1 o1Var;
            int pointToPosition;
            int i2;
            if (this.v != null) {
                ListAdapter adapter = getAdapter();
                if (adapter instanceof HeaderViewListAdapter) {
                    HeaderViewListAdapter headerViewListAdapter = (HeaderViewListAdapter) adapter;
                    i = headerViewListAdapter.getHeadersCount();
                    o1Var = (o1) headerViewListAdapter.getWrappedAdapter();
                } else {
                    i = 0;
                    o1Var = (o1) adapter;
                }
                r1 r1Var = null;
                if (motionEvent.getAction() != 10 && (pointToPosition = pointToPosition((int) motionEvent.getX(), (int) motionEvent.getY())) != -1 && (i2 = pointToPosition - i) >= 0 && i2 < o1Var.getCount()) {
                    r1Var = o1Var.getItem(i2);
                }
                MenuItem menuItem = this.w;
                if (menuItem != r1Var) {
                    p1 b = o1Var.b();
                    if (menuItem != null) {
                        this.v.b(b, menuItem);
                    }
                    this.w = r1Var;
                    if (r1Var != null) {
                        this.v.a(b, r1Var);
                    }
                }
            }
            return super.onHoverEvent(motionEvent);
        }

        @DexIgnore
        public boolean onKeyDown(int i, KeyEvent keyEvent) {
            ListMenuItemView listMenuItemView = (ListMenuItemView) getSelectedView();
            if (listMenuItemView != null && i == this.t) {
                if (listMenuItemView.isEnabled() && listMenuItemView.getItemData().hasSubMenu()) {
                    performItemClick(listMenuItemView, getSelectedItemPosition(), getSelectedItemId());
                }
                return true;
            } else if (listMenuItemView == null || i != this.u) {
                return super.onKeyDown(i, keyEvent);
            } else {
                setSelection(-1);
                ((o1) getAdapter()).b().a(false);
                return true;
            }
        }

        @DexIgnore
        public void setHoverListener(u2 u2Var) {
            this.v = u2Var;
        }

        @DexIgnore
        @Override // android.widget.AbsListView, com.fossil.r2
        public /* bridge */ /* synthetic */ void setSelector(Drawable drawable) {
            super.setSelector(drawable);
        }
    }

    /*
    static {
        try {
            if (Build.VERSION.SDK_INT <= 28) {
                O = PopupWindow.class.getDeclaredMethod("setTouchModal", Boolean.TYPE);
            }
        } catch (NoSuchMethodException unused) {
            Log.i("MenuPopupWindow", "Could not find method setTouchModal() on PopupWindow. Oh well.");
        }
    }
    */

    @DexIgnore
    public v2(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ListPopupWindow
    public r2 a(Context context, boolean z) {
        a aVar = new a(context, z);
        aVar.setHoverListener(this);
        return aVar;
    }

    @DexIgnore
    public void b(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            ((ListPopupWindow) this).J.setExitTransition((Transition) obj);
        }
    }

    @DexIgnore
    public void d(boolean z) {
        if (Build.VERSION.SDK_INT <= 28) {
            Method method = O;
            if (method != null) {
                try {
                    method.invoke(((ListPopupWindow) this).J, Boolean.valueOf(z));
                } catch (Exception unused) {
                    Log.i("MenuPopupWindow", "Could not invoke setTouchModal() on PopupWindow. Oh well.");
                }
            }
        } else {
            ((ListPopupWindow) this).J.setTouchModal(z);
        }
    }

    @DexIgnore
    public void a(Object obj) {
        if (Build.VERSION.SDK_INT >= 23) {
            ((ListPopupWindow) this).J.setEnterTransition((Transition) obj);
        }
    }

    @DexIgnore
    @Override // com.fossil.u2
    public void b(p1 p1Var, MenuItem menuItem) {
        u2 u2Var = this.N;
        if (u2Var != null) {
            u2Var.b(p1Var, menuItem);
        }
    }

    @DexIgnore
    public void a(u2 u2Var) {
        this.N = u2Var;
    }

    @DexIgnore
    @Override // com.fossil.u2
    public void a(p1 p1Var, MenuItem menuItem) {
        u2 u2Var = this.N;
        if (u2Var != null) {
            u2Var.a(p1Var, menuItem);
        }
    }
}
