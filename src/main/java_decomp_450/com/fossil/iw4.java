package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iw4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ View s;

    @DexIgnore
    public iw4(Object obj, View view, int i, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, View view2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = rTLImageView;
        this.s = view2;
    }

    @DexIgnore
    public static iw4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static iw4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (iw4) ViewDataBinding.a(layoutInflater, 2131558487, viewGroup, z, obj);
    }
}
