package com.fossil;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.xp4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tp4 extends go5 implements xp4.a {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public qw6<ey4> f;
    @DexIgnore
    public vp4 g;
    @DexIgnore
    public qn4 h;
    @DexIgnore
    public rj4 i;
    @DexIgnore
    public xp4 j;
    @DexIgnore
    public xp4 p;
    @DexIgnore
    public Parcelable q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return tp4.s;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final tp4 a(qn4 qn4) {
            tp4 tp4 = new tp4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_draft_extra", qn4);
            tp4.setArguments(bundle);
            return tp4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public b(tp4 tp4, boolean z) {
            this.a = tp4;
        }

        @DexIgnore
        public final void onClick(View view) {
            tp4.g(this.a).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public c(tp4 tp4, boolean z) {
            this.a = tp4;
        }

        @DexIgnore
        public final void onClick(View view) {
            qd5 c = qd5.f.c();
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                c.a("bc_start_type_now", activity);
                tp4.g(this.a).q();
                return;
            }
            throw new x87("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public d(tp4 tp4, boolean z) {
            this.a = tp4;
        }

        @DexIgnore
        public final void onClick(View view) {
            qd5 c = qd5.f.c();
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                c.a("bc_start_type_later", activity);
                tp4.g(this.a).p();
                return;
            }
            throw new x87("null cannot be cast to non-null type android.app.Activity");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public e(tp4 tp4, boolean z) {
            this.a = tp4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.A();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public f(tp4 tp4, boolean z) {
            this.a = tp4;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            tp4.g(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ ey4 a;
        @DexIgnore
        public /* final */ /* synthetic */ tp4 b;

        @DexIgnore
        public g(ey4 ey4, tp4 tp4, boolean z) {
            this.a = ey4;
            this.b = tp4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.x;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            tp4.g(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ey4 a;
        @DexIgnore
        public /* final */ /* synthetic */ tp4 b;

        @DexIgnore
        public h(ey4 ey4, tp4 tp4, boolean z) {
            this.a = ey4;
            this.b = tp4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (charSequence == null || charSequence.length() == 0) {
                tp4.g(this.b).m();
                FlexibleTextView flexibleTextView = this.a.D;
                ee7.a((Object) flexibleTextView, "tvEmpty");
                flexibleTextView.setVisibility(8);
                ImageView imageView = this.a.r;
                ee7.a((Object) imageView, "btnSearchClear");
                imageView.setVisibility(8);
                return;
            }
            ImageView imageView2 = this.a.r;
            ee7.a((Object) imageView2, "btnSearchClear");
            imageView2.setVisibility(0);
            FlexibleCheckBox flexibleCheckBox = this.a.v;
            ee7.a((Object) flexibleCheckBox, "cbInviteAll");
            flexibleCheckBox.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.a.y;
            ee7.a((Object) flexibleTextView2, "ftvInviteAll");
            flexibleTextView2.setVisibility(8);
            if (this.b.q == null) {
                tp4 tp4 = this.b;
                RecyclerView recyclerView = this.a.B;
                ee7.a((Object) recyclerView, "rvFriends");
                RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                tp4.q = layoutManager != null ? layoutManager.y() : null;
                i97 i97 = i97.a;
            }
            tp4.g(this.b).a(charSequence.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ey4 a;

        @DexIgnore
        public i(ey4 ey4) {
            this.a = ey4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.w.setText("");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnFocusChangeListener {
        @DexIgnore
        public static /* final */ j a; // = new j();

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (z) {
                um4.a.b(PortfolioApp.g0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<r87<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public k(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, Boolean> r87) {
            ey4 ey4 = (ey4) tp4.c(this.a).a();
            if (ey4 != null) {
                Boolean first = r87.getFirst();
                Boolean second = r87.getSecond();
                if (first != null) {
                    if (first.booleanValue()) {
                        SwipeRefreshLayout swipeRefreshLayout = ey4.C;
                        ee7.a((Object) swipeRefreshLayout, "swipe");
                        swipeRefreshLayout.setEnabled(true);
                        SwipeRefreshLayout swipeRefreshLayout2 = ey4.C;
                        ee7.a((Object) swipeRefreshLayout2, "swipe");
                        swipeRefreshLayout2.setRefreshing(true);
                        FlexibleCheckBox flexibleCheckBox = ey4.v;
                        ee7.a((Object) flexibleCheckBox, "cbInviteAll");
                        flexibleCheckBox.setVisibility(8);
                        FlexibleTextView flexibleTextView = ey4.y;
                        ee7.a((Object) flexibleTextView, "ftvInviteAll");
                        flexibleTextView.setVisibility(8);
                    } else {
                        SwipeRefreshLayout swipeRefreshLayout3 = ey4.C;
                        ee7.a((Object) swipeRefreshLayout3, "swipe");
                        swipeRefreshLayout3.setRefreshing(false);
                        SwipeRefreshLayout swipeRefreshLayout4 = ey4.C;
                        ee7.a((Object) swipeRefreshLayout4, "swipe");
                        swipeRefreshLayout4.setEnabled(false);
                    }
                }
                if (second == null) {
                    return;
                }
                if (second.booleanValue()) {
                    this.a.b();
                } else {
                    this.a.a();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<r87<? extends List<? extends mo4>, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public l(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<? extends List<mo4>, ? extends ServerError> r87) {
            List<mo4> list = (List) r87.getFirst();
            if (list != null) {
                ey4 ey4 = (ey4) tp4.c(this.a).a();
                if (ey4 == null) {
                    return;
                }
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = ey4.D;
                    ee7.a((Object) flexibleTextView, "tvEmpty");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleCheckBox flexibleCheckBox = ey4.v;
                ee7.a((Object) flexibleCheckBox, "cbInviteAll");
                flexibleCheckBox.setVisibility(0);
                FlexibleTextView flexibleTextView2 = ey4.y;
                ee7.a((Object) flexibleTextView2, "ftvInviteAll");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = ey4.B;
                ee7.a((Object) recyclerView, "rvFriends");
                recyclerView.setAdapter(tp4.f(this.a));
                tp4.f(this.a).a(list);
                Parcelable d = this.a.q;
                if (d != null) {
                    RecyclerView recyclerView2 = ey4.B;
                    ee7.a((Object) recyclerView2, "rvFriends");
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        layoutManager.a(d);
                        return;
                    }
                    return;
                }
                return;
            }
            ey4 ey42 = (ey4) tp4.c(this.a).a();
            if (ey42 != null) {
                SwipeRefreshLayout swipeRefreshLayout = ey42.C;
                ee7.a((Object) swipeRefreshLayout, "swipe");
                swipeRefreshLayout.setEnabled(true);
                SwipeRefreshLayout swipeRefreshLayout2 = ey42.C;
                ee7.a((Object) swipeRefreshLayout2, "swipe");
                swipeRefreshLayout2.setRefreshing(false);
                FlexibleTextView flexibleTextView3 = ey42.x;
                ee7.a((Object) flexibleTextView3, "ftvError");
                flexibleTextView3.setVisibility(0);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public m(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleCheckBox flexibleCheckBox;
            ey4 ey4 = (ey4) tp4.c(this.a).a();
            if (ey4 != null && (flexibleCheckBox = ey4.v) != null) {
                ee7.a((Object) bool, "it");
                flexibleCheckBox.setChecked(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public n(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            if (r87.getFirst().booleanValue()) {
                this.a.g1();
                return;
            }
            ServerError serverError = (ServerError) r87.getSecond();
            bx6 bx6 = bx6.c;
            String str = null;
            Integer code = serverError != null ? serverError.getCode() : null;
            if (serverError != null) {
                str = serverError.getMessage();
            }
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(code, str, childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements zd<List<? extends Date>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public o(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<? extends Date> list) {
            tp4 tp4 = this.a;
            ee7.a((Object) list, "it");
            tp4.x(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements zd<List<? extends dn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public p(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<dn4> list) {
            tp4 tp4 = this.a;
            ee7.a((Object) list, "it");
            tp4.y(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements zd<r87<? extends List<? extends mo4>, ? extends String>> {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        public q(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<? extends List<mo4>, String> r87) {
            List<mo4> list = (List) r87.getFirst();
            String second = r87.getSecond();
            ey4 ey4 = (ey4) tp4.c(this.a).a();
            if (ey4 != null) {
                RecyclerView recyclerView = ey4.B;
                ee7.a((Object) recyclerView, "rvFriends");
                recyclerView.setAdapter(tp4.e(this.a));
                tp4.e(this.a).a(list);
                if (list.isEmpty()) {
                    FlexibleTextView flexibleTextView = ey4.D;
                    ee7.a((Object) flexibleTextView, "tvEmpty");
                    we7 we7 = we7.a;
                    FlexibleTextView flexibleTextView2 = ey4.D;
                    ee7.a((Object) flexibleTextView2, "tvEmpty");
                    String a2 = ig5.a(flexibleTextView2.getContext(), 2131886282);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026or__NothingFoundForInput)");
                    String format = String.format(a2, Arrays.copyOf(new Object[]{second}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView3 = ey4.D;
                    ee7.a((Object) flexibleTextView3, "tvEmpty");
                    flexibleTextView3.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView4 = ey4.D;
                ee7.a((Object) flexibleTextView4, "tvEmpty");
                flexibleTextView4.setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r<T> implements zd<v87<? extends mn4, ? extends Long, ? extends String>> {
        @DexIgnore
        public static /* final */ r a; // = new r();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<mn4, Long, String> v87) {
            um4.a.a(v87.getFirst(), v87.getSecond().longValue(), v87.getThird(), PortfolioApp.g0.c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s implements dr5 {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public s(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        @Override // com.fossil.dr5
        public void a(Date date) {
            ee7.b(date, "date");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = tp4.t.a();
            local.e(a2, "showDateTimePicker - updateValue - date: " + date);
            tp4.g(this.a).a(date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t implements er5 {
        @DexIgnore
        public /* final */ /* synthetic */ tp4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public t(tp4 tp4) {
            this.a = tp4;
        }

        @DexIgnore
        @Override // com.fossil.er5
        public void a(dn4 dn4) {
            ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = tp4.t.a();
            local.e(a2, "showSoonTime - updateValue - model: " + dn4);
            Object a3 = dn4.a();
            if (!(a3 instanceof Date)) {
                a3 = null;
            }
            Date date = (Date) a3;
            if (date != null) {
                tp4.g(this.a).a(date);
            }
        }
    }

    /*
    static {
        String simpleName = tp4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCInviteFriendFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 c(tp4 tp4) {
        qw6<ey4> qw6 = tp4.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xp4 e(tp4 tp4) {
        xp4 xp4 = tp4.p;
        if (xp4 != null) {
            return xp4;
        }
        ee7.d("searchingFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ xp4 f(tp4 tp4) {
        xp4 xp4 = tp4.j;
        if (xp4 != null) {
            return xp4;
        }
        ee7.d("suggestedFriendsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ vp4 g(tp4 tp4) {
        vp4 vp4 = tp4.g;
        if (vp4 != null) {
            return vp4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void A() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<ey4> qw6 = this.f;
        if (qw6 != null) {
            ey4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.E;
                ee7.a((Object) flexibleTextView, "tvTitle");
                qn4 qn4 = this.h;
                flexibleTextView.setText(qn4 != null ? qn4.e() : null);
                if (z) {
                    FlexibleButton flexibleButton = a2.t;
                    ee7.a((Object) flexibleButton, "btnStartNow");
                    flexibleButton.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.s;
                    ee7.a((Object) flexibleButton2, "btnStartLater");
                    flexibleButton2.setVisibility(8);
                    FlexibleButton flexibleButton3 = a2.q;
                    ee7.a((Object) flexibleButton3, "btnDone");
                    flexibleButton3.setVisibility(0);
                    f1();
                    FlexibleTextView flexibleTextView2 = a2.y;
                    ee7.a((Object) flexibleTextView2, "ftvInviteAll");
                    flexibleTextView2.setVisibility(8);
                    FlexibleCheckBox flexibleCheckBox = a2.v;
                    ee7.a((Object) flexibleCheckBox, "cbInviteAll");
                    flexibleCheckBox.setVisibility(8);
                    a2.q.setOnClickListener(new b(this, z));
                } else {
                    a2.t.setOnClickListener(new c(this, z));
                    a2.s.setOnClickListener(new d(this, z));
                }
                a2.z.setOnClickListener(new e(this, z));
                this.j = new xp4(this);
                this.p = new xp4(this);
                RecyclerView recyclerView = a2.B;
                recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
                xp4 xp4 = this.j;
                if (xp4 != null) {
                    recyclerView.setAdapter(xp4);
                    recyclerView.setHasFixedSize(true);
                    a2.v.setOnCheckedChangeListener(new f(this, z));
                    a2.C.setOnRefreshListener(new g(a2, this, z));
                    a2.r.setOnClickListener(new i(a2));
                    a2.w.setOnFocusChangeListener(j.a);
                    a2.w.addTextChangedListener(new h(a2, this, z));
                    return;
                }
                ee7.d("suggestedFriendsAdapter");
                throw null;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<ey4> qw6 = this.f;
        if (qw6 != null) {
            ey4 a2 = qw6.a();
            if (a2 != null) {
                u5 u5Var = new u5();
                u5Var.c(a2.A);
                RecyclerView recyclerView = a2.B;
                ee7.a((Object) recyclerView, "rvFriends");
                int id = recyclerView.getId();
                FlexibleButton flexibleButton = a2.q;
                ee7.a((Object) flexibleButton, "btnDone");
                u5Var.a(id, 4, flexibleButton.getId(), 3);
                u5Var.a(a2.A);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public final void h1() {
        vp4 vp4 = this.g;
        if (vp4 != null) {
            vp4.g().a(getViewLifecycleOwner(), new k(this));
            vp4 vp42 = this.g;
            if (vp42 != null) {
                vp42.l().a(getViewLifecycleOwner(), new l(this));
                vp4 vp43 = this.g;
                if (vp43 != null) {
                    vp43.e().a(getViewLifecycleOwner(), new m(this));
                    vp4 vp44 = this.g;
                    if (vp44 != null) {
                        vp44.k().a(getViewLifecycleOwner(), new n(this));
                        vp4 vp45 = this.g;
                        if (vp45 != null) {
                            vp45.f().a(getViewLifecycleOwner(), new o(this));
                            vp4 vp46 = this.g;
                            if (vp46 != null) {
                                vp46.j().a(getViewLifecycleOwner(), new p(this));
                                vp4 vp47 = this.g;
                                if (vp47 != null) {
                                    vp47.h().a(getViewLifecycleOwner(), new q(this));
                                    vp4 vp48 = this.g;
                                    if (vp48 != null) {
                                        vp48.d().a(getViewLifecycleOwner(), r.a);
                                    } else {
                                        ee7.d("viewModel");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("viewModel");
                                    throw null;
                                }
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().h().a(this);
        rj4 rj4 = this.i;
        qn4 qn4 = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(vp4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026endViewModel::class.java)");
            this.g = (vp4) a2;
            Bundle arguments = getArguments();
            if (arguments != null) {
                qn4 = (qn4) arguments.getParcelable("challenge_draft_extra");
            }
            this.h = qn4;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ey4 ey4 = (ey4) qb.a(layoutInflater, 2131558513, viewGroup, false, a1());
        this.f = new qw6<>(this, ey4);
        ee7.a((Object) ey4, "bd");
        return ey4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        vp4 vp4 = this.g;
        String str = null;
        if (vp4 != null) {
            vp4.a(this.h);
            qn4 qn4 = this.h;
            if (qn4 != null) {
                str = qn4.c();
            }
            T(str != null);
            h1();
            return;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void x(List<? extends Date> list) {
        cr5 cr5 = new cr5();
        String a2 = ig5.a(requireContext(), 2131886335);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026e_Invite_CTA__StartLater)");
        cr5.setTitle(a2);
        cr5.x(list);
        cr5.a(new s(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        cr5.show(childFragmentManager, cr5.C.a());
    }

    @DexIgnore
    public final void y(List<dn4> list) {
        bn4 bn4 = new bn4();
        String a2 = ig5.a(requireContext(), 2131886341);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026artNow_Title__QuickStart)");
        bn4.setTitle(a2);
        bn4.x(list);
        bn4.U(true);
        bn4.a(new t(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bn4.show(childFragmentManager, bn4.z.a());
    }

    @DexIgnore
    @Override // com.fossil.xp4.a
    public void a(mo4 mo4) {
        ee7.b(mo4, "friend");
        vp4 vp4 = this.g;
        if (vp4 != null) {
            vp4.a(mo4);
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }
}
