package com.fossil;

import android.os.RemoteException;
import com.fossil.b53;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z53 extends b22<yl2, c53> {
    @DexIgnore
    public /* final */ /* synthetic */ bm2 d;
    @DexIgnore
    public /* final */ /* synthetic */ y12 e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z53(b53 b53, y12 y12, bm2 bm2, y12 y122) {
        super(y12);
        this.d = bm2;
        this.e = y122;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b, com.fossil.oo3] */
    @Override // com.fossil.b22
    public final /* synthetic */ void a(yl2 yl2, oo3 oo3) throws RemoteException {
        yl2.a(this.d, this.e, new b53.a(oo3));
    }
}
