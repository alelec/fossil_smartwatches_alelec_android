package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class do6 extends me7 {
    @DexIgnore
    public static /* final */ bg7 INSTANCE; // = new do6();

    @DexIgnore
    @Override // com.fossil.bg7
    public Object get(Object obj) {
        return tc7.a((eo6) obj);
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vd7
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public uf7 getOwner() {
        return te7.a(tc7.class, "app_fossilRelease");
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
