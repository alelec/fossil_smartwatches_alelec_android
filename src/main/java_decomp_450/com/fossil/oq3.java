package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oq3 implements Parcelable.Creator<nq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        byte[] bArr = null;
        String str2 = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                i = j72.q(parcel, a);
            } else if (a2 == 3) {
                str = j72.e(parcel, a);
            } else if (a2 == 4) {
                bArr = j72.b(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                str2 = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new nq3(i, str, bArr, str2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ nq3[] newArray(int i) {
        return new nq3[i];
    }
}
