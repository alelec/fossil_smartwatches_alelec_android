package com.fossil;

import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gg7<T> implements hg7<T> {
    @DexIgnore
    public /* final */ vc7<T> a;
    @DexIgnore
    public /* final */ gd7<T, T> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Iterator<T>, ye7 {
        @DexIgnore
        public T a;
        @DexIgnore
        public int b; // = -2;
        @DexIgnore
        public /* final */ /* synthetic */ gg7 c;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(gg7 gg7) {
            this.c = gg7;
        }

        @DexIgnore
        public final void a() {
            T t;
            T t2;
            if (this.b == -2) {
                t2 = (T) this.c.a.invoke();
            } else {
                gd7 b2 = this.c.b;
                T t3 = this.a;
                if (t3 != null) {
                    t2 = (T) b2.invoke(t3);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            this.a = t;
            this.b = t == null ? 0 : 1;
        }

        @DexIgnore
        public boolean hasNext() {
            if (this.b < 0) {
                a();
            }
            return this.b == 1;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (this.b < 0) {
                a();
            }
            if (this.b != 0) {
                T t = this.a;
                if (t != null) {
                    this.b = -1;
                    return t;
                }
                throw new x87("null cannot be cast to non-null type T");
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.vc7<? extends T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.gd7<? super T, ? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public gg7(vc7<? extends T> vc7, gd7<? super T, ? extends T> gd7) {
        ee7.b(vc7, "getInitialValue");
        ee7.b(gd7, "getNextValue");
        this.a = vc7;
        this.b = gd7;
    }

    @DexIgnore
    @Override // com.fossil.hg7
    public Iterator<T> iterator() {
        return new a(this);
    }
}
