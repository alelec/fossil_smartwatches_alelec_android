package com.fossil;

import com.fossil.fitness.WorkoutSessionManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb1 extends fe7 implements gd7<i97, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ oe7 a;
    @DexIgnore
    public /* final */ /* synthetic */ bb0 b;
    @DexIgnore
    public /* final */ /* synthetic */ oe7 c;
    @DexIgnore
    public /* final */ /* synthetic */ km1 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qb1(oe7 oe7, bb0 bb0, oe7 oe72, km1 km1) {
        super(1);
        this.a = oe7;
        this.b = bb0;
        this.c = oe72;
        this.d = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(i97 i97) {
        if (this.a.element) {
            this.d.a(this.b);
        }
        if (this.c.element) {
            WorkoutSessionManager workoutSessionManager = this.d.s;
            Boolean valueOf = workoutSessionManager != null ? Boolean.valueOf(workoutSessionManager.clearAccumulateDistance()) : null;
            t11 t11 = t11.a;
            String str = "clearAccumulateDistance result = " + valueOf + '.';
        }
        return i97.a;
    }
}
