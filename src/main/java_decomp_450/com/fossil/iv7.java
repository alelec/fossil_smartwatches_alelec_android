package com.fossil;

import java.lang.annotation.Annotation;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv7 implements hv7 {
    @DexIgnore
    public static /* final */ hv7 a; // = new iv7();

    @DexIgnore
    public static Annotation[] a(Annotation[] annotationArr) {
        if (jv7.a(annotationArr, (Class<? extends Annotation>) hv7.class)) {
            return annotationArr;
        }
        Annotation[] annotationArr2 = new Annotation[(annotationArr.length + 1)];
        annotationArr2[0] = a;
        System.arraycopy(annotationArr, 0, annotationArr2, 1, annotationArr.length);
        return annotationArr2;
    }

    @DexIgnore
    @Override // java.lang.annotation.Annotation
    public Class<? extends Annotation> annotationType() {
        return hv7.class;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return obj instanceof hv7;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        return "@" + hv7.class.getName() + "()";
    }
}
