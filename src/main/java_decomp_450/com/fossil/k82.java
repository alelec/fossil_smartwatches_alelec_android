package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k82 implements Parcelable.Creator<c72> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ c72 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        IBinder iBinder = null;
        i02 i02 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                iBinder = j72.p(parcel, a);
            } else if (a2 == 3) {
                i02 = (i02) j72.a(parcel, a, i02.CREATOR);
            } else if (a2 == 4) {
                z = j72.i(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                z2 = j72.i(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new c72(i, iBinder, i02, z, z2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ c72[] newArray(int i) {
        return new c72[i];
    }
}
