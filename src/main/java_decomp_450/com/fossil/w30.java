package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w30 extends Fragment {
    @DexIgnore
    public /* final */ i30 a;
    @DexIgnore
    public /* final */ u30 b;
    @DexIgnore
    public /* final */ Set<w30> c;
    @DexIgnore
    public w30 d;
    @DexIgnore
    public iw e;
    @DexIgnore
    public Fragment f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements u30 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.u30
        public Set<iw> a() {
            Set<w30> Z0 = w30.this.Z0();
            HashSet hashSet = new HashSet(Z0.size());
            for (w30 w30 : Z0) {
                if (w30.c1() != null) {
                    hashSet.add(w30.c1());
                }
            }
            return hashSet;
        }

        @DexIgnore
        public String toString() {
            return super.toString() + "{fragment=" + w30.this + "}";
        }
    }

    @DexIgnore
    public w30() {
        this(new i30());
    }

    @DexIgnore
    public static FragmentManager c(Fragment fragment) {
        while (fragment.getParentFragment() != null) {
            fragment = fragment.getParentFragment();
        }
        return fragment.getFragmentManager();
    }

    @DexIgnore
    public Set<w30> Z0() {
        w30 w30 = this.d;
        if (w30 == null) {
            return Collections.emptySet();
        }
        if (equals(w30)) {
            return Collections.unmodifiableSet(this.c);
        }
        HashSet hashSet = new HashSet();
        for (w30 w302 : this.d.Z0()) {
            if (a(w302.b1())) {
                hashSet.add(w302);
            }
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public void a(iw iwVar) {
        this.e = iwVar;
    }

    @DexIgnore
    public i30 a1() {
        return this.a;
    }

    @DexIgnore
    public final void b(w30 w30) {
        this.c.remove(w30);
    }

    @DexIgnore
    public final Fragment b1() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? parentFragment : this.f;
    }

    @DexIgnore
    public iw c1() {
        return this.e;
    }

    @DexIgnore
    public u30 d1() {
        return this.b;
    }

    @DexIgnore
    public final void e1() {
        w30 w30 = this.d;
        if (w30 != null) {
            w30.b(this);
            this.d = null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        FragmentManager c2 = c(this);
        if (c2 != null) {
            try {
                a(getContext(), c2);
            } catch (IllegalStateException e2) {
                if (Log.isLoggable("SupportRMFragment", 5)) {
                    Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
                }
            }
        } else if (Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        this.a.a();
        e1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDetach() {
        super.onDetach();
        this.f = null;
        e1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        this.a.b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        this.a.c();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public String toString() {
        return super.toString() + "{parent=" + b1() + "}";
    }

    @DexIgnore
    @SuppressLint({"ValidFragment"})
    public w30(i30 i30) {
        this.b = new a();
        this.c = new HashSet();
        this.a = i30;
    }

    @DexIgnore
    public final void a(w30 w30) {
        this.c.add(w30);
    }

    @DexIgnore
    public void b(Fragment fragment) {
        FragmentManager c2;
        this.f = fragment;
        if (fragment != null && fragment.getContext() != null && (c2 = c(fragment)) != null) {
            a(fragment.getContext(), c2);
        }
    }

    @DexIgnore
    public final boolean a(Fragment fragment) {
        Fragment b1 = b1();
        while (true) {
            Fragment parentFragment = fragment.getParentFragment();
            if (parentFragment == null) {
                return false;
            }
            if (parentFragment.equals(b1)) {
                return true;
            }
            fragment = fragment.getParentFragment();
        }
    }

    @DexIgnore
    public final void a(Context context, FragmentManager fragmentManager) {
        e1();
        w30 a2 = aw.a(context).h().a(context, fragmentManager);
        this.d = a2;
        if (!equals(a2)) {
            this.d.a(this);
        }
    }
}
