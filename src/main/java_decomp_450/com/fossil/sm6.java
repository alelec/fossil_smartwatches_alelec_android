package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm6 extends he {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b(null, null, null, null, null, null, 63, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Integer d;
        @DexIgnore
        public Integer e;
        @DexIgnore
        public Integer f;

        @DexIgnore
        public b() {
            this(null, null, null, null, null, null, 63, null);
        }

        @DexIgnore
        public b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        public final Integer c() {
            return this.f;
        }

        @DexIgnore
        public final Integer d() {
            return this.e;
        }

        @DexIgnore
        public final Integer e() {
            return this.d;
        }

        @DexIgnore
        public final Integer f() {
            return this.c;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2, (i & 4) != 0 ? null : num3, (i & 8) != 0 ? null : num4, (i & 16) != 0 ? null : num5, (i & 32) != 0 ? null : num6);
        }

        @DexIgnore
        public static /* synthetic */ void a(b bVar, Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6, int i, Object obj) {
            if ((i & 1) != 0) {
                num = null;
            }
            if ((i & 2) != 0) {
                num2 = null;
            }
            if ((i & 4) != 0) {
                num3 = null;
            }
            if ((i & 8) != 0) {
                num4 = null;
            }
            if ((i & 16) != 0) {
                num5 = null;
            }
            if ((i & 32) != 0) {
                num6 = null;
            }
            bVar.a(num, num2, num3, num4, num5, num6);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2, Integer num3, Integer num4, Integer num5, Integer num6) {
            this.a = num;
            this.b = num2;
            this.c = num3;
            this.d = num4;
            this.e = num5;
            this.f = num6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel$saveColor$1", f = "CustomizeSleepChartViewModel.kt", l = {66, 67, 100}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $awakeColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $deepColor;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public /* final */ /* synthetic */ String $lightColor;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sm6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sm6 sm6, String str, String str2, String str3, String str4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sm6;
            this.$id = str;
            this.$awakeColor = str2;
            this.$lightColor = str3;
            this.$deepColor = str4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$id, this.$awakeColor, this.$lightColor, this.$deepColor, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00b4  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00ee  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x019b A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x01a4  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x0053
                if (r1 == r4) goto L_0x0043
                if (r1 == r3) goto L_0x0033
                if (r1 != r2) goto L_0x002b
                java.lang.Object r0 = r14.L$4
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r14.L$3
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r14.L$2
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r14.L$1
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r14.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r15)
                goto L_0x019c
            L_0x002b:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0033:
                java.lang.Object r1 = r14.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r3 = r14.L$1
                com.fossil.se7 r3 = (com.fossil.se7) r3
                java.lang.Object r4 = r14.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r15)
                goto L_0x0093
            L_0x0043:
                java.lang.Object r1 = r14.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r4 = r14.L$1
                com.fossil.se7 r4 = (com.fossil.se7) r4
                java.lang.Object r6 = r14.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r15)
                goto L_0x0077
            L_0x0053:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r15 = r14.p$
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                com.fossil.sm6 r6 = r14.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r6.c
                java.lang.String r7 = r14.$id
                r14.L$0 = r15
                r14.L$1 = r1
                r14.L$2 = r1
                r14.label = r4
                java.lang.Object r4 = r6.getThemeById(r7, r14)
                if (r4 != r0) goto L_0x0074
                return r0
            L_0x0074:
                r6 = r15
                r15 = r4
                r4 = r1
            L_0x0077:
                com.portfolio.platform.data.model.Theme r15 = (com.portfolio.platform.data.model.Theme) r15
                if (r15 == 0) goto L_0x007c
                goto L_0x0099
            L_0x007c:
                com.fossil.sm6 r15 = r14.this$0
                com.portfolio.platform.data.source.ThemeRepository r15 = r15.c
                r14.L$0 = r6
                r14.L$1 = r4
                r14.L$2 = r1
                r14.label = r3
                java.lang.Object r15 = r15.getCurrentTheme(r14)
                if (r15 != r0) goto L_0x0091
                return r0
            L_0x0091:
                r3 = r4
                r4 = r6
            L_0x0093:
                if (r15 == 0) goto L_0x01a4
                com.portfolio.platform.data.model.Theme r15 = (com.portfolio.platform.data.model.Theme) r15
                r6 = r4
                r4 = r3
            L_0x0099:
                r1.element = r15
                com.fossil.se7 r15 = new com.fossil.se7
                r15.<init>()
                r15.element = r5
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                r1.element = r5
                com.fossil.se7 r3 = new com.fossil.se7
                r3.<init>()
                r3.element = r5
                java.lang.String r5 = r14.$awakeColor
                if (r5 == 0) goto L_0x00ea
                T r5 = r4.element
                com.portfolio.platform.data.model.Theme r5 = (com.portfolio.platform.data.model.Theme) r5
                java.util.ArrayList r5 = r5.getStyles()
                java.util.Iterator r5 = r5.iterator()
            L_0x00c0:
                boolean r7 = r5.hasNext()
                if (r7 == 0) goto L_0x00ea
                java.lang.Object r7 = r5.next()
                com.portfolio.platform.data.model.Style r7 = (com.portfolio.platform.data.model.Style) r7
                java.lang.String r8 = r7.getKey()
                java.lang.String r9 = "awakeSleep"
                boolean r8 = com.fossil.ee7.a(r8, r9)
                if (r8 == 0) goto L_0x00c0
                java.lang.String r8 = r14.$awakeColor
                r7.setValue(r8)
                java.lang.String r7 = r14.$awakeColor
                int r7 = android.graphics.Color.parseColor(r7)
                java.lang.Integer r7 = com.fossil.pb7.a(r7)
                r15.element = r7
                goto L_0x00c0
            L_0x00ea:
                java.lang.String r5 = r14.$lightColor
                if (r5 == 0) goto L_0x0124
                T r5 = r4.element
                com.portfolio.platform.data.model.Theme r5 = (com.portfolio.platform.data.model.Theme) r5
                java.util.ArrayList r5 = r5.getStyles()
                java.util.Iterator r5 = r5.iterator()
            L_0x00fa:
                boolean r7 = r5.hasNext()
                if (r7 == 0) goto L_0x0124
                java.lang.Object r7 = r5.next()
                com.portfolio.platform.data.model.Style r7 = (com.portfolio.platform.data.model.Style) r7
                java.lang.String r8 = r7.getKey()
                java.lang.String r9 = "lightSleep"
                boolean r8 = com.fossil.ee7.a(r8, r9)
                if (r8 == 0) goto L_0x00fa
                java.lang.String r8 = r14.$lightColor
                r7.setValue(r8)
                java.lang.String r7 = r14.$lightColor
                int r7 = android.graphics.Color.parseColor(r7)
                java.lang.Integer r7 = com.fossil.pb7.a(r7)
                r1.element = r7
                goto L_0x00fa
            L_0x0124:
                java.lang.String r5 = r14.$deepColor
                if (r5 == 0) goto L_0x015e
                T r5 = r4.element
                com.portfolio.platform.data.model.Theme r5 = (com.portfolio.platform.data.model.Theme) r5
                java.util.ArrayList r5 = r5.getStyles()
                java.util.Iterator r5 = r5.iterator()
            L_0x0134:
                boolean r7 = r5.hasNext()
                if (r7 == 0) goto L_0x015e
                java.lang.Object r7 = r5.next()
                com.portfolio.platform.data.model.Style r7 = (com.portfolio.platform.data.model.Style) r7
                java.lang.String r8 = r7.getKey()
                java.lang.String r9 = "deepSleep"
                boolean r8 = com.fossil.ee7.a(r8, r9)
                if (r8 == 0) goto L_0x0134
                java.lang.String r8 = r14.$deepColor
                r7.setValue(r8)
                java.lang.String r7 = r14.$deepColor
                int r7 = android.graphics.Color.parseColor(r7)
                java.lang.Integer r7 = com.fossil.pb7.a(r7)
                r3.element = r7
                goto L_0x0134
            L_0x015e:
                com.fossil.sm6 r5 = r14.this$0
                com.fossil.sm6$b r7 = r5.b
                T r5 = r15.element
                r8 = r5
                java.lang.Integer r8 = (java.lang.Integer) r8
                r9 = r5
                java.lang.Integer r9 = (java.lang.Integer) r9
                T r5 = r1.element
                r10 = r5
                java.lang.Integer r10 = (java.lang.Integer) r10
                r11 = r5
                java.lang.Integer r11 = (java.lang.Integer) r11
                T r5 = r3.element
                r12 = r5
                java.lang.Integer r12 = (java.lang.Integer) r12
                r13 = r5
                java.lang.Integer r13 = (java.lang.Integer) r13
                r7.a(r8, r9, r10, r11, r12, r13)
                com.fossil.sm6 r5 = r14.this$0
                com.portfolio.platform.data.source.ThemeRepository r5 = r5.c
                T r7 = r4.element
                com.portfolio.platform.data.model.Theme r7 = (com.portfolio.platform.data.model.Theme) r7
                r14.L$0 = r6
                r14.L$1 = r4
                r14.L$2 = r15
                r14.L$3 = r1
                r14.L$4 = r3
                r14.label = r2
                java.lang.Object r15 = r5.upsertUserTheme(r7, r14)
                if (r15 != r0) goto L_0x019c
                return r0
            L_0x019c:
                com.fossil.sm6 r15 = r14.this$0
                r15.a()
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x01a4:
                com.fossil.ee7.a()
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.sm6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = sm6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeSleepChartViewM\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public sm6(ThemeRepository themeRepository) {
        ee7.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        int i2;
        int i3;
        String a2 = pm6.s.a();
        if (a2 != null) {
            i = Color.parseColor(a2);
        } else {
            i = Color.parseColor(e);
        }
        String c2 = pm6.s.c();
        if (c2 != null) {
            i2 = Color.parseColor(c2);
        } else {
            i2 = Color.parseColor(e);
        }
        String b2 = pm6.s.b();
        if (b2 != null) {
            i3 = Color.parseColor(b2);
        } else {
            i3 = Color.parseColor(e);
        }
        b bVar = this.b;
        Integer valueOf = Integer.valueOf(i);
        Integer valueOf2 = Integer.valueOf(i);
        Integer valueOf3 = Integer.valueOf(i2);
        bVar.a(valueOf, valueOf2, Integer.valueOf(i2), valueOf3, Integer.valueOf(i3), Integer.valueOf(i3));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        switch (i) {
            case 701:
                b.a(this.b, Integer.valueOf(i2), Integer.valueOf(i2), null, null, null, null, 60, null);
                a();
                return;
            case 702:
                b.a(this.b, null, null, Integer.valueOf(i2), Integer.valueOf(i2), null, null, 51, null);
                a();
                return;
            case 703:
                b.a(this.b, null, null, null, null, Integer.valueOf(i2), Integer.valueOf(i2), 15, null);
                a();
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, String str3, String str4) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str5 = d;
        local.d(str5, "saveColor awakeColor=" + str2 + " lightColor=" + str3 + " deepColor=" + str4);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, str, str2, str3, str4, null), 3, null);
    }
}
