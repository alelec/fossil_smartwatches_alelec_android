package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ap5;
import com.fossil.cy6;
import com.fossil.o26;
import com.fossil.xg5;
import com.fossil.y26;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s26 extends go5 implements r26, o26.b, cy6.g {
    @DexIgnore
    public qw6<a05> f;
    @DexIgnore
    public q26 g;
    @DexIgnore
    public ap5 h;
    @DexIgnore
    public y26 i;
    @DexIgnore
    public long j; // = 500;
    @DexIgnore
    public rj4 p;
    @DexIgnore
    public a06 q;
    @DexIgnore
    public o26 r;
    @DexIgnore
    public WatchFaceWrapper s;
    @DexIgnore
    public HashMap t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s26 a;

        @DexIgnore
        public b(s26 s26) {
            this.a = s26;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.G0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s26 a;

        @DexIgnore
        public c(s26 s26) {
            this.a = s26;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.O0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ s26 a;

        @DexIgnore
        public d(s26 s26) {
            this.a = s26;
        }

        @DexIgnore
        public final void onClick(View view) {
            y26 b = this.a.i;
            if (b != null) {
                b.a(false);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ap5.c {
        @DexIgnore
        public /* final */ /* synthetic */ s26 a;

        @DexIgnore
        public e(s26 s26) {
            this.a = s26;
        }

        @DexIgnore
        @Override // com.fossil.ap5.c
        public void a(WatchFaceWrapper watchFaceWrapper) {
            ee7.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked backgroundWrapper=" + watchFaceWrapper);
            s26.c(this.a).c(watchFaceWrapper);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Integer $selectedPos$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView $this_with;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s26 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(RecyclerView recyclerView, fb7 fb7, s26 s26, Integer num) {
            super(2, fb7);
            this.$this_with = recyclerView;
            this.this$0 = s26;
            this.$selectedPos$inlined = num;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.$this_with, fb7, this.this$0, this.$selectedPos$inlined);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                long a2 = this.this$0.j;
                this.L$0 = yi7;
                this.label = 1;
                if (kj7.a(a2, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j = 0;
            this.$this_with.smoothScrollToPosition(this.$selectedPos$inlined.intValue());
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Integer $selectedPos$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView $this_with;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ s26 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(RecyclerView recyclerView, fb7 fb7, s26 s26, Integer num) {
            super(2, fb7);
            this.$this_with = recyclerView;
            this.this$0 = s26;
            this.$selectedPos$inlined = num;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.$this_with, fb7, this.this$0, this.$selectedPos$inlined);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                long a2 = this.this$0.j;
                this.L$0 = yi7;
                this.label = 1;
                if (kj7.a(a2, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.j = 0;
            this.$this_with.smoothScrollToPosition(this.$selectedPos$inlined.intValue());
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ q26 c(s26 s26) {
        q26 q26 = s26.g;
        if (q26 != null) {
            return q26;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void G0() {
        qw6<a05> qw6 = this.f;
        if (qw6 != null) {
            a05 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                ee7.a((Object) flexibleTextView, "binding.tvBackground");
                a((TextView) flexibleTextView, true);
                FlexibleTextView flexibleTextView2 = a2.u;
                ee7.a((Object) flexibleTextView2, "binding.tvPhoto");
                a((TextView) flexibleTextView2, false);
                RecyclerView recyclerView = a2.s;
                ee7.a((Object) recyclerView, "binding.rvPhoto");
                recyclerView.setVisibility(4);
                RecyclerView recyclerView2 = a2.r;
                ee7.a((Object) recyclerView2, "binding.rvBackground");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.o26.b
    public void I0() {
        Intent intent = new Intent("android.intent.action.PICK", MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent, 200);
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void L() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showPhotoListReachToLimitedSizeDialog()");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.F(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void M0() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showPickupPhotoType()");
        if (isActive()) {
            o26 a2 = o26.s.a();
            this.r = a2;
            if (a2 != null) {
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                a2.show(childFragmentManager, "AddPhotoMenuFragment");
            }
            o26 o26 = this.r;
            if (o26 != null) {
                o26.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void O0() {
        qw6<a05> qw6 = this.f;
        if (qw6 != null) {
            a05 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "binding.tvPhoto");
                a((TextView) flexibleTextView, true);
                FlexibleTextView flexibleTextView2 = a2.t;
                ee7.a((Object) flexibleTextView2, "binding.tvBackground");
                a((TextView) flexibleTextView2, false);
                RecyclerView recyclerView = a2.r;
                ee7.a((Object) recyclerView, "binding.rvBackground");
                recyclerView.setVisibility(4);
                RecyclerView recyclerView2 = a2.s;
                ee7.a((Object) recyclerView2, "binding.rvPhoto");
                recyclerView2.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.o26.b
    public void Q() {
        FragmentActivity activity;
        if (xg5.a(xg5.b, (Context) getActivity(), xg5.a.CAPTURE_IMAGE, false, false, false, (Integer) null, 60, (Object) null) && (activity = getActivity()) != null) {
            ee7.a((Object) activity, "it");
            Intent a2 = je5.a(activity, activity.getPackageManager());
            if (a2 != null) {
                startActivityForResult(a2, Action.Selfie.TAKE_BURST);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.t;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "CustomizeThemeFragment";
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void g0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.A(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        Uri data;
        Bundle extras;
        if (i3 == -1) {
            switch (i2) {
                case 200:
                    if (intent != null && (data = intent.getData()) != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("CustomizeThemeFragment", "From Gallery, uri == " + data);
                        ee7.a((Object) data, "uri");
                        a(data);
                        return;
                    }
                    return;
                case 201:
                    if (intent != null && (extras = intent.getExtras()) != null) {
                        String string = extras.getString("WATCH_FACE_ID");
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("CustomizeThemeFragment", "Image is cropped and saved with watchFaceId = " + string);
                        if (string != null) {
                            q26 q26 = this.g;
                            if (q26 != null) {
                                q26.a(string);
                                return;
                            } else {
                                ee7.d("mPresenter");
                                throw null;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                case Action.Selfie.TAKE_BURST /*{ENCODED_INT: 202}*/:
                    Uri a2 = je5.a(intent, PortfolioApp.g0.c());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("CustomizeThemeFragment", "From Camera, uri = " + a2);
                    if (a2 != null) {
                        a(a2);
                        return;
                    }
                    return;
                default:
                    return;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        a05 a05 = (a05) qb.a(layoutInflater, 2131558539, viewGroup, false, a1());
        PortfolioApp.g0.c().f().a(new u26(this)).a(this);
        qw6<a05> qw6 = new qw6<>(this, a05);
        this.f = qw6;
        a05 a2 = qw6.a();
        if (a2 != null) {
            a2.t.setOnClickListener(new b(this));
            a2.u.setOnClickListener(new c(this));
            a2.q.setOnClickListener(new d(this));
        }
        ee7.a((Object) a05, "binding");
        return a05.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        o26 o26 = this.r;
        if (o26 != null) {
            o26.a((o26.b) null);
        }
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        q26 q26 = this.g;
        if (q26 != null) {
            q26.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        q26 q26 = this.g;
        if (q26 != null) {
            q26.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            DianaCustomizeEditActivity dianaCustomizeEditActivity = (DianaCustomizeEditActivity) activity;
            rj4 rj4 = this.p;
            if (rj4 != null) {
                he a2 = je.a(dianaCustomizeEditActivity, rj4).a(a06.class);
                ee7.a((Object) a2, "ViewModelProviders.of(ac\u2026izeViewModel::class.java)");
                a06 a06 = (a06) a2;
                this.q = a06;
                q26 q26 = this.g;
                if (q26 == null) {
                    ee7.d("mPresenter");
                    throw null;
                } else if (a06 != null) {
                    q26.a(a06);
                    ap5 ap5 = new ap5(null, null, 3, null);
                    ap5.a(new e(this));
                    this.h = ap5;
                    y26 y26 = new y26(null, null, 3, null);
                    y26.a(new f(this));
                    this.i = y26;
                    qw6<a05> qw6 = this.f;
                    if (qw6 != null) {
                        a05 a3 = qw6.a();
                        if (a3 != null) {
                            RecyclerView recyclerView = a3.r;
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setItemAnimator(null);
                            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext(), 0, false));
                            recyclerView.setAdapter(this.h);
                            RecyclerView recyclerView2 = a3.s;
                            recyclerView2.setHasFixedSize(true);
                            recyclerView2.setItemAnimator(null);
                            recyclerView2.setLayoutManager(new LinearLayoutManager(recyclerView2.getContext(), 0, false));
                            recyclerView2.setAdapter(this.i);
                            return;
                        }
                        return;
                    }
                    ee7.d("mBinding");
                    throw null;
                } else {
                    ee7.d("mShareViewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModelFactory");
                throw null;
            }
        } else {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void v(boolean z) {
        qw6<a05> qw6 = this.f;
        if (qw6 != null) {
            a05 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.u;
                ee7.a((Object) flexibleTextView, "it.tvPhoto");
                flexibleTextView.setClickable(z);
                FlexibleTextView flexibleTextView2 = a2.t;
                ee7.a((Object) flexibleTextView2, "it.tvBackground");
                flexibleTextView2.setClickable(z);
                RecyclerView recyclerView = a2.r;
                ee7.a((Object) recyclerView, "it.rvBackground");
                recyclerView.setClickable(z);
                RecyclerView recyclerView2 = a2.s;
                ee7.a((Object) recyclerView2, "it.rvPhoto");
                recyclerView2.setClickable(z);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void z0() {
        FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "showRemoveConfirmationDialog");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.J(childFragmentManager);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements y26.d {
        @DexIgnore
        public /* final */ /* synthetic */ s26 a;

        @DexIgnore
        public f(s26 s26) {
            this.a = s26;
        }

        @DexIgnore
        @Override // com.fossil.y26.d
        public void a(WatchFaceWrapper watchFaceWrapper) {
            ee7.b(watchFaceWrapper, "watchFaceWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CustomizeThemeFragment", "onItemClicked photoWrapper=" + watchFaceWrapper);
            s26.c(this.a).c(watchFaceWrapper);
        }

        @DexIgnore
        @Override // com.fossil.y26.d
        public void b(WatchFaceWrapper watchFaceWrapper) {
            ee7.b(watchFaceWrapper, "watchFaceWrapper");
            FLogger.INSTANCE.getLocal().d("CustomizeThemeFragment", "onItemRemoved");
            this.a.s = watchFaceWrapper;
            s26.c(this.a).a(watchFaceWrapper);
        }

        @DexIgnore
        @Override // com.fossil.y26.d
        public void a() {
            s26.c(this.a).h();
        }
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void b(WatchFaceWrapper watchFaceWrapper) {
        ee7.b(watchFaceWrapper, "watchFaceWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "removePhotoItem() " + watchFaceWrapper);
        y26 y26 = this.i;
        if (y26 != null) {
            y26.a(watchFaceWrapper);
        }
    }

    @DexIgnore
    public void a(q26 q26) {
        ee7.b(q26, "presenter");
        this.g = q26;
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void a(List<WatchFaceWrapper> list, qb5 qb5) {
        y26 y26;
        ee7.b(list, "watchFaceWrappers");
        ee7.b(qb5, "type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "showWatchFaces watchFaceWrappers size=" + list.size() + ", type = " + qb5);
        if (qb5 == qb5.BACKGROUND) {
            ap5 ap5 = this.h;
            if (ap5 != null) {
                ap5.a(list);
            }
        } else if (qb5 == qb5.PHOTO && (y26 = this.i) != null) {
            y26.a(list);
        }
    }

    @DexIgnore
    @Override // com.fossil.r26
    public void a(String str, qb5 qb5) {
        ee7.b(str, "watchFaceId");
        ee7.b(qb5, "type");
        if (qb5 == qb5.BACKGROUND) {
            ap5 ap5 = this.h;
            if (ap5 != null) {
                ap5.notifyDataSetChanged();
            }
            ap5 ap52 = this.h;
            Integer valueOf = ap52 != null ? Integer.valueOf(ap52.a(str)) : null;
            if (valueOf != null && valueOf.intValue() >= 0) {
                qw6<a05> qw6 = this.f;
                if (qw6 != null) {
                    a05 a2 = qw6.a();
                    if (a2 != null) {
                        RecyclerView recyclerView = a2.r;
                        RecyclerView.m layoutManager = recyclerView.getLayoutManager();
                        if (layoutManager != null) {
                            int J = ((LinearLayoutManager) layoutManager).J();
                            RecyclerView.m layoutManager2 = recyclerView.getLayoutManager();
                            if (layoutManager2 != null) {
                                if (valueOf.intValue() < ((LinearLayoutManager) layoutManager2).G() || valueOf.intValue() > J) {
                                    ik7 unused = xh7.b(zi7.a(qj7.c()), null, null, new g(recyclerView, null, this, valueOf), 3, null);
                                    return;
                                }
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
        } else if (qb5 == qb5.PHOTO) {
            y26 y26 = this.i;
            if (y26 != null) {
                y26.notifyDataSetChanged();
            }
            y26 y262 = this.i;
            Integer valueOf2 = y262 != null ? Integer.valueOf(y262.a(str)) : null;
            if (valueOf2 != null && valueOf2.intValue() >= 0) {
                qw6<a05> qw62 = this.f;
                if (qw62 != null) {
                    a05 a3 = qw62.a();
                    if (a3 != null) {
                        RecyclerView recyclerView2 = a3.s;
                        RecyclerView.m layoutManager3 = recyclerView2.getLayoutManager();
                        if (layoutManager3 != null) {
                            int J2 = ((LinearLayoutManager) layoutManager3).J();
                            RecyclerView.m layoutManager4 = recyclerView2.getLayoutManager();
                            if (layoutManager4 != null) {
                                if (valueOf2.intValue() < ((LinearLayoutManager) layoutManager4).G() || valueOf2.intValue() > J2) {
                                    ik7 unused2 = xh7.b(zi7.a(qj7.c()), null, null, new h(recyclerView2, null, this, valueOf2), 3, null);
                                    return;
                                }
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                        }
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        WatchFaceWrapper watchFaceWrapper;
        ee7.b(str, "tag");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CustomizeThemeFragment", "onDialogFragmentResult tag=" + str + ", actionId=" + i2);
        if (str.hashCode() == 1111782057 && str.equals("CONFIRM_REMOVE_WATCH_FACE") && i2 == 2131363307 && (watchFaceWrapper = this.s) != null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("CustomizeThemeFragment", "onItemRemoved " + watchFaceWrapper);
            q26 q26 = this.g;
            if (q26 != null) {
                q26.b(watchFaceWrapper);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(Uri uri) {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ArrayList<fz5> q2 = ((DianaCustomizeEditActivity) activity).q();
            Intent intent = new Intent(getContext(), EditPhotoActivity.class);
            intent.putExtra("IMAGE_URI_EXTRA", uri);
            if (q2 != null) {
                intent.putExtra("COMPLICATION_EXTRA", q2);
            }
            startActivityForResult(intent, 201);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity");
    }

    @DexIgnore
    public final void a(TextView textView, boolean z) {
        PortfolioApp c2 = PortfolioApp.g0.c();
        int a2 = v6.a(c2, 2131099820);
        int a3 = v6.a(c2, 2131099971);
        Drawable c3 = v6.c(c2, 2131231227);
        Drawable c4 = v6.c(c2, 2131231228);
        if (z) {
            textView.setTextColor(a3);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, c4);
        } else {
            textView.setTextColor(a2);
            textView.setCompoundDrawablesRelativeWithIntrinsicBounds((Drawable) null, (Drawable) null, (Drawable) null, c3);
        }
        textView.setClickable(!z);
    }
}
