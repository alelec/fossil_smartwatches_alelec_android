package com.fossil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y64 extends FileOutputStream {
    @DexIgnore
    public static /* final */ FilenameFilter d; // = new a();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public File b;
    @DexIgnore
    public boolean c; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements FilenameFilter {
        @DexIgnore
        public boolean accept(File file, String str) {
            return str.endsWith(".cls_temp");
        }
    }

    @DexIgnore
    public y64(File file, String str) throws FileNotFoundException {
        super(new File(file, str + ".cls_temp"));
        this.a = file + File.separator + str;
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append(".cls_temp");
        this.b = new File(sb.toString());
    }

    @DexIgnore
    public void a() throws IOException {
        if (!this.c) {
            this.c = true;
            super.flush();
            super.close();
        }
    }

    @DexIgnore
    @Override // java.io.OutputStream, java.io.Closeable, java.io.FileOutputStream, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (!this.c) {
            this.c = true;
            super.flush();
            super.close();
            File file = new File(this.a + ".cls");
            if (this.b.renameTo(file)) {
                this.b = null;
                return;
            }
            String str = "";
            if (file.exists()) {
                str = " (target already exists)";
            } else if (!this.b.exists()) {
                str = " (source does not exist)";
            }
            throw new IOException("Could not rename temp file: " + this.b + " -> " + file + str);
        }
    }
}
