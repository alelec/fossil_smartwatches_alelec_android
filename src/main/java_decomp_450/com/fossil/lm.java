package com.fossil;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface lm {
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final b.c a = new b.c();
    @DexIgnore
    @SuppressLint({"SyntheticAccessor"})
    public static final b.C0111b b = new b.C0111b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends b {
            @DexIgnore
            public /* final */ Throwable a;

            @DexIgnore
            public a(Throwable th) {
                this.a = th;
            }

            @DexIgnore
            public Throwable a() {
                return this.a;
            }

            @DexIgnore
            public String toString() {
                return String.format("FAILURE (%s)", this.a.getMessage());
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lm$b$b")
        /* renamed from: com.fossil.lm$b$b  reason: collision with other inner class name */
        public static final class C0111b extends b {
            @DexIgnore
            public String toString() {
                return "IN_PROGRESS";
            }

            @DexIgnore
            public C0111b() {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c extends b {
            @DexIgnore
            public String toString() {
                return "SUCCESS";
            }

            @DexIgnore
            public c() {
            }
        }
    }
}
