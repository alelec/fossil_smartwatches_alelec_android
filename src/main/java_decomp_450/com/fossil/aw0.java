package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class aw0 extends Enum<aw0> {
    @DexIgnore
    public static /* final */ aw0 a;
    @DexIgnore
    public static /* final */ aw0 b;
    @DexIgnore
    public static /* final */ aw0 c;
    @DexIgnore
    public static /* final */ aw0 d;
    @DexIgnore
    public static /* final */ aw0 e;
    @DexIgnore
    public static /* final */ aw0 f;
    @DexIgnore
    public static /* final */ /* synthetic */ aw0[] g;

    /*
    static {
        aw0 aw0 = new aw0("ENABLE_MAINTAINING_CONNECTION", 0);
        a = aw0;
        aw0 aw02 = new aw0("APP_DISCONNECT", 2);
        b = aw02;
        aw0 aw03 = new aw0("SET_SECRET_KEY", 4);
        c = aw03;
        aw0 aw04 = new aw0("DEVICE_STATE_CHANGED", 5);
        d = aw04;
        aw0 aw05 = new aw0("CLEAR_CACHE", 6);
        e = aw05;
        aw0 aw06 = new aw0("ENABLE_BACKGROUND_SYNC", 7);
        f = aw06;
        g = new aw0[]{aw0, new aw0("ENABLE_MAINTAINING_HID_CONNECTION", 1), aw02, new aw0("APP_DISCONNECT_HID", 3), aw03, aw04, aw05, aw06};
    }
    */

    @DexIgnore
    public aw0(String str, int i) {
    }

    @DexIgnore
    public static aw0 valueOf(String str) {
        return (aw0) Enum.valueOf(aw0.class, str);
    }

    @DexIgnore
    public static aw0[] values() {
        return (aw0[]) g.clone();
    }
}
