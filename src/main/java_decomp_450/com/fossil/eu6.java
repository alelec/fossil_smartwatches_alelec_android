package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu6 extends go5 implements du6 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ String q; // = q;
    @DexIgnore
    public static /* final */ String r; // = r;
    @DexIgnore
    public static /* final */ String s; // = s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public qw6<o65> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return eu6.q;
        }

        @DexIgnore
        public final String b() {
            return eu6.s;
        }

        @DexIgnore
        public final String c() {
            return eu6.r;
        }

        @DexIgnore
        public final String d() {
            return eu6.p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final eu6 a(boolean z, boolean z2, boolean z3) {
            eu6 eu6 = new eu6();
            Bundle bundle = new Bundle();
            bundle.putBoolean(a(), z);
            bundle.putBoolean(c(), z2);
            bundle.putBoolean(b(), z3);
            eu6.setArguments(bundle);
            return eu6;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ eu6 a;

        @DexIgnore
        public b(eu6 eu6) {
            this.a = eu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.h) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                Context context = this.a.getContext();
                if (context != null) {
                    ee7.a((Object) context, "context!!");
                    aVar.a(context, this.a.i, true);
                    return;
                }
                ee7.a();
                throw null;
            }
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ eu6 a;

        @DexIgnore
        public c(eu6 eu6) {
            this.a = eu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ eu6 a;

        @DexIgnore
        public d(eu6 eu6) {
            this.a = eu6;
        }

        @DexIgnore
        public final void onClick(View view) {
            tm4.a.a().a(this.a);
        }
    }

    /*
    static {
        String simpleName = eu6.class.getSimpleName();
        ee7.a((Object) simpleName, "TroubleshootingFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void a(cu6 cu6) {
        ee7.b(cu6, "presenter");
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return p;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<o65> qw6 = new qw6<>(this, (o65) qb.a(layoutInflater, 2131558629, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            o65 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        boolean z = false;
        this.g = arguments != null ? arguments.getBoolean(q) : false;
        Bundle arguments2 = getArguments();
        this.h = arguments2 != null ? arguments2.getBoolean(r) : false;
        Bundle arguments3 = getArguments();
        if (arguments3 != null) {
            z = arguments3.getBoolean(s);
        }
        this.i = z;
        qw6<o65> qw6 = this.f;
        if (qw6 != null) {
            o65 a2 = qw6.a();
            if (a2 != null) {
                View findViewById = a2.t.findViewById(2131362770);
                View findViewById2 = a2.t.findViewById(2131362795);
                if (!tm4.a.a().b()) {
                    ee7.a((Object) findViewById2, "touchScreenSmartwatchesText");
                    findViewById2.setVisibility(8);
                }
                if (this.g) {
                    ee7.a((Object) findViewById, "batteryText");
                    findViewById.setVisibility(8);
                }
                a2.r.setOnClickListener(new b(this));
                a2.u.setOnClickListener(new c(this));
                a2.q.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
