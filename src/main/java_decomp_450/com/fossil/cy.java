package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cy implements yw {
    @DexIgnore
    public /* final */ yw b;
    @DexIgnore
    public /* final */ yw c;

    @DexIgnore
    public cy(yw ywVar, yw ywVar2) {
        this.b = ywVar;
        this.c = ywVar2;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
        this.c.a(messageDigest);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (!(obj instanceof cy)) {
            return false;
        }
        cy cyVar = (cy) obj;
        if (!this.b.equals(cyVar.b) || !this.c.equals(cyVar.c)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return (this.b.hashCode() * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DataCacheKey{sourceKey=" + this.b + ", signature=" + this.c + '}';
    }
}
