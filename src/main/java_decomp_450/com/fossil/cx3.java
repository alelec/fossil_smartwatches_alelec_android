package com.fossil;

import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cx3 {
    @DexIgnore
    public static /* final */ ew3 a; // = ew3.c(", ").a("null");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements cw3<Object, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Collection a;

        @DexIgnore
        public a(Collection collection) {
            this.a = collection;
        }

        @DexIgnore
        @Override // com.fossil.cw3
        public Object apply(Object obj) {
            return obj == this.a ? "(this Collection)" : obj;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Object obj) {
        jw3.a(collection);
        try {
            return collection.contains(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static boolean a(Collection<?> collection, Collection<?> collection2) {
        return py3.a((Iterable) collection2, lw3.a((Collection) collection));
    }

    @DexIgnore
    public static String a(Collection<?> collection) {
        StringBuilder a2 = a(collection.size());
        a2.append('[');
        a.a(a2, py3.a((Iterable) collection, (cw3) new a(collection)));
        a2.append(']');
        return a2.toString();
    }

    @DexIgnore
    public static StringBuilder a(int i) {
        bx3.a(i, "size");
        return new StringBuilder((int) Math.min(((long) i) * 8, 1073741824L));
    }

    @DexIgnore
    public static <T> Collection<T> a(Iterable<T> iterable) {
        return (Collection) iterable;
    }
}
