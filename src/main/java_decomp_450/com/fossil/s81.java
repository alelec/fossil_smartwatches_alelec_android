package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s81 extends jr1 {
    @DexIgnore
    public /* final */ byte[] S;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ s81(ri1 ri1, en0 en0, wm0 wm0, boolean z, short s, byte[] bArr, float f, String str, int i) {
        super(ri1, en0, wm0, z, s, (i & 64) != 0 ? 0.001f : f, (i & 128) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 128);
        this.S = bArr;
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public byte[] n() {
        return this.S;
    }

    @DexIgnore
    public s81(ri1 ri1, en0 en0, wm0 wm0, boolean z, short s, byte[] bArr, float f, String str) {
        super(ri1, en0, wm0, z, s, f, str, false, 128);
        this.S = bArr;
    }
}
