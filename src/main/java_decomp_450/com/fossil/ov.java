package com.fossil;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ List<ru> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ InputStream d;

    @DexIgnore
    public ov(int i, List<ru> list) {
        this(i, list, -1, null);
    }

    @DexIgnore
    public final InputStream a() {
        return this.d;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final List<ru> c() {
        return Collections.unmodifiableList(this.b);
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public ov(int i, List<ru> list, int i2, InputStream inputStream) {
        this.a = i;
        this.b = list;
        this.c = i2;
        this.d = inputStream;
    }
}
