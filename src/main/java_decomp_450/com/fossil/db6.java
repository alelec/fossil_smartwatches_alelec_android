package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class db6 extends ab6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ LiveData<qx6<GoalTrackingSummary>> i;
    @DexIgnore
    public /* final */ LiveData<qx6<List<GoalTrackingData>>> j;
    @DexIgnore
    public /* final */ bb6 k;
    @DexIgnore
    public /* final */ ch5 l;
    @DexIgnore
    public /* final */ GoalTrackingRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ db6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$mGoalTrackingData$1$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {41, 41}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<GoalTrackingData>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<GoalTrackingData>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    GoalTrackingRepository d = this.this$0.a.m;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = d.getGoalTrackingDataList(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public b(db6 db6) {
            this.a = db6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<GoalTrackingData>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "mGoalTrackingData onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ db6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$mGoalTrackingSummary$1$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {35, 35}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends GoalTrackingSummary>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends GoalTrackingSummary>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    GoalTrackingRepository d = this.this$0.a.m;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = d.getSummary(date, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(db6 db6) {
            this.a = db6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<GoalTrackingSummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "mGoalTrackingSummary onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {101, 104}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ db6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$maxValue$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$data, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> c;
                ArrayList<BarChart.b> arrayList;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Iterator it = this.$data.iterator();
                    int i = 0;
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        obj2 = it.next();
                        if (it.hasNext()) {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                            ee7.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 += pb7.a(it2.next().e()).intValue();
                            }
                            Integer a = pb7.a(i2);
                            do {
                                Object next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                                ee7.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 += pb7.a(it3.next().e()).intValue();
                                }
                                Integer a2 = pb7.a(i3);
                                if (a.compareTo((Object) a2) < 0) {
                                    obj2 = next;
                                    a = a2;
                                }
                            } while (it.hasNext());
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += pb7.a(it4.next().e()).intValue();
                    }
                    return pb7.a(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayPresenter$showDetailChart$1$pair$1", f = "GoalTrackingOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    tg6 tg6 = tg6.a;
                    Date b = this.this$0.this$0.e;
                    List<GoalTrackingData> list = null;
                    if (b != null) {
                        qx6 qx6 = (qx6) this.this$0.this$0.j.a();
                        if (qx6 != null) {
                            list = (List) qx6.c();
                        }
                        return tg6.a(b, list);
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(db6 db6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = db6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00c6  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002f
                if (r1 == r4) goto L_0x0027
                if (r1 != r3) goto L_0x001f
                java.lang.Object r0 = r9.L$2
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.lang.Object r1 = r9.L$1
                com.fossil.r87 r1 = (com.fossil.r87) r1
                java.lang.Object r2 = r9.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r10)
                goto L_0x008c
            L_0x001f:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x0027:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                goto L_0x004a
            L_0x002f:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r1 = r9.p$
                com.fossil.db6 r10 = r9.this$0
                com.fossil.ti7 r10 = r10.b()
                com.fossil.db6$d$b r5 = new com.fossil.db6$d$b
                r5.<init>(r9, r2)
                r9.L$0 = r1
                r9.label = r4
                java.lang.Object r10 = com.fossil.vh7.a(r10, r5, r9)
                if (r10 != r0) goto L_0x004a
                return r0
            L_0x004a:
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r4 = r10.getFirst()
                java.util.ArrayList r4 = (java.util.ArrayList) r4
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "showDetailChart - data="
                r6.append(r7)
                r6.append(r4)
                java.lang.String r6 = r6.toString()
                java.lang.String r7 = "GoalTrackingOverviewDayPresenter"
                r5.d(r7, r6)
                com.fossil.db6 r5 = r9.this$0
                com.fossil.ti7 r5 = r5.b()
                com.fossil.db6$d$a r6 = new com.fossil.db6$d$a
                r6.<init>(r4, r2)
                r9.L$0 = r1
                r9.L$1 = r10
                r9.L$2 = r4
                r9.label = r3
                java.lang.Object r1 = com.fossil.vh7.a(r5, r6, r9)
                if (r1 != r0) goto L_0x0088
                return r0
            L_0x0088:
                r0 = r4
                r8 = r1
                r1 = r10
                r10 = r8
            L_0x008c:
                java.lang.Integer r10 = (java.lang.Integer) r10
                com.fossil.db6 r2 = r9.this$0
                androidx.lifecycle.LiveData r2 = r2.i
                java.lang.Object r2 = r2.a()
                com.fossil.qx6 r2 = (com.fossil.qx6) r2
                if (r2 == 0) goto L_0x00b5
                java.lang.Object r2 = r2.c()
                com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary r2 = (com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary) r2
                if (r2 == 0) goto L_0x00b5
                int r2 = r2.getGoalTarget()
                int r2 = r2 / 16
                java.lang.Integer r2 = com.fossil.pb7.a(r2)
                if (r2 == 0) goto L_0x00b5
                int r2 = r2.intValue()
                goto L_0x00b7
            L_0x00b5:
                r2 = 8
            L_0x00b7:
                com.fossil.db6 r3 = r9.this$0
                com.fossil.bb6 r3 = r3.k
                com.portfolio.platform.ui.view.chart.base.BarChart$c r4 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                if (r10 == 0) goto L_0x00c6
                int r10 = r10.intValue()
                goto L_0x00c7
            L_0x00c6:
                r10 = 0
            L_0x00c7:
                int r5 = r2 / 16
                int r10 = java.lang.Math.max(r10, r5)
                r4.<init>(r10, r2, r0)
                java.lang.Object r10 = r1.getSecond()
                java.util.ArrayList r10 = (java.util.ArrayList) r10
                r3.a(r4, r10)
                com.fossil.i97 r10 = com.fossil.i97.a
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.db6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<qx6<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ db6 a;

        @DexIgnore
        public e(db6 db6) {
            this.a = db6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<GoalTrackingSummary> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "start - mGoalTrackingSummary -- summary=" + qx6);
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    ik7 unused = this.a.i();
                }
                this.a.k.b(true ^ this.a.l.M());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<qx6<? extends List<GoalTrackingData>>> {
        @DexIgnore
        public /* final */ /* synthetic */ db6 a;

        @DexIgnore
        public f(db6 db6) {
            this.a = db6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<GoalTrackingData>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingData -- goalTrackingSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    ik7 unused = this.a.i();
                }
                this.a.k.b(true ^ this.a.l.M());
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public db6(bb6 bb6, ch5 ch5, GoalTrackingRepository goalTrackingRepository) {
        ee7.b(bb6, "mView");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.k = bb6;
        this.l = ch5;
        this.m = goalTrackingRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<qx6<GoalTrackingSummary>> b2 = ge.b(mutableLiveData, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.i = b2;
        LiveData<qx6<List<GoalTrackingData>>> b3 = ge.b(this.f, new b(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.j = b3;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.g = false;
            this.h = false;
            Date date2 = new Date();
            this.e = date2;
            this.f.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewDayPresenter", "loadData - mDate=" + this.e);
        LiveData<qx6<GoalTrackingSummary>> liveData = this.i;
        bb6 bb6 = this.k;
        if (bb6 != null) {
            liveData.a((cb6) bb6, new e(this));
            this.j.a((LifecycleOwner) this.k, new f(this));
            this.k.b(!this.l.M());
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewDayPresenter", "stop");
        try {
            LiveData<qx6<List<GoalTrackingData>>> liveData = this.j;
            bb6 bb6 = this.k;
            if (bb6 != null) {
                liveData.a((cb6) bb6);
                this.i.a((LifecycleOwner) this.k);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void h() {
        this.k.a(this);
    }

    @DexIgnore
    public final ik7 i() {
        return xh7.b(e(), null, null, new d(this, null), 3, null);
    }
}
