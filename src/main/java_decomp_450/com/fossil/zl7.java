package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zl7 extends bm7 {
    @DexIgnore
    @Override // com.fossil.bm7
    public boolean f() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public final boolean g() {
        throw new IllegalStateException("head cannot be removed".toString());
    }

    @DexIgnore
    public final boolean k() {
        return b() == this;
    }
}
