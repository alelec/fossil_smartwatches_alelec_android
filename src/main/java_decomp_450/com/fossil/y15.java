package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y15 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ s95 r;
    @DexIgnore
    public /* final */ s95 s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ LinearLayout u;
    @DexIgnore
    public /* final */ LinearLayout v;
    @DexIgnore
    public /* final */ TodayHeartRateChart w;

    @DexIgnore
    public y15(Object obj, View view, int i, FlexibleTextView flexibleTextView, s95 s95, s95 s952, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, TodayHeartRateChart todayHeartRateChart) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = s95;
        a((ViewDataBinding) s95);
        this.s = s952;
        a((ViewDataBinding) s952);
        this.t = imageView;
        this.u = linearLayout;
        this.v = linearLayout2;
        this.w = todayHeartRateChart;
    }
}
