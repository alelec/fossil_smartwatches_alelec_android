package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y20 implements cx<nw, Bitmap> {
    @DexIgnore
    public /* final */ dz a;

    @DexIgnore
    public y20(dz dzVar) {
        this.a = dzVar;
    }

    @DexIgnore
    public boolean a(nw nwVar, ax axVar) {
        return true;
    }

    @DexIgnore
    public uy<Bitmap> a(nw nwVar, int i, int i2, ax axVar) {
        return k10.a(nwVar.a(), this.a);
    }
}
