package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y90 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public long a;
    @DexIgnore
    public byte b;
    @DexIgnore
    public String c;
    @DexIgnore
    public short d;
    @DexIgnore
    public aa0 e;
    @DexIgnore
    public sg0 f;
    @DexIgnore
    public ca0 g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public y90 createFromParcel(Parcel parcel) {
            return new y90(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public y90[] newArray(int i) {
            return new y90[i];
        }
    }

    /*
    static {
        ud7 ud7 = ud7.a;
    }
    */

    @DexIgnore
    public y90(String str) {
        this.h = str;
        ik1 ik1 = ik1.a;
        Charset c2 = b21.x.c();
        if (str != null) {
            byte[] bytes = str.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            this.a = ik1.a(s97.a(bytes, (byte) 0), ng1.CRC32);
            this.c = "";
            this.d = 255;
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] a(byte b2, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b2).put((byte) bArr.length).put(bArr).array();
        ee7.a((Object) array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] b() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte b2 = bz0.c.a;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a).array();
        ee7.a((Object) array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        byteArrayOutputStream.write(a(b2, array));
        byteArrayOutputStream.write(a(bz0.d.a, new byte[]{this.b}));
        if (this.c.length() > 0) {
            byte b3 = bz0.b.a;
            String a2 = yz0.a(this.c);
            Charset c2 = b21.x.c();
            if (a2 != null) {
                byte[] bytes = a2.getBytes(c2);
                ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                byteArrayOutputStream.write(a(b3, bytes));
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        short s = this.d;
        if (s != ((short) -1)) {
            byteArrayOutputStream.write(a(bz0.f.a, new byte[]{(byte) s}));
        }
        aa0 aa0 = this.e;
        if (aa0 != null) {
            byteArrayOutputStream.write(a(bz0.g.a, aa0.b()));
        }
        ca0 ca0 = this.g;
        if (ca0 != null) {
            byteArrayOutputStream.write(a(bz0.h.a, new byte[]{ca0.a()}));
        }
        sg0 sg0 = this.f;
        if (sg0 != null) {
            byteArrayOutputStream.write(a(bz0.e.a, sg0.c()));
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
        ee7.a((Object) array2, "ByteBuffer\n             \u2026\n                .array()");
        return array2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(y90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            y90 y90 = (y90) obj;
            return this.a == y90.a && this.b == y90.b && !(ee7.a(this.c, y90.c) ^ true) && this.d == y90.d && !(ee7.a(this.e, y90.e) ^ true) && this.g == y90.g && !(ee7.a(this.f, y90.f) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.a;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.h;
    }

    @DexIgnore
    public final aa0 getHandMovingConfig() {
        return this.e;
    }

    @DexIgnore
    public final sg0 getIconConfig() {
        return this.f;
    }

    @DexIgnore
    public final short getPriority() {
        return this.d;
    }

    @DexIgnore
    public final String getSender() {
        return this.c;
    }

    @DexIgnore
    public final ca0 getVibePattern() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.c.hashCode() + (((Long.valueOf(this.a).hashCode() * 31) + this.b) * 31)) * 31) + this.d;
        aa0 aa0 = this.e;
        if (aa0 != null) {
            hashCode = (hashCode * 31) + aa0.hashCode();
        }
        ca0 ca0 = this.g;
        if (ca0 != null) {
            hashCode = (hashCode * 31) + ca0.hashCode();
        }
        sg0 sg0 = this.f;
        return sg0 != null ? (hashCode * 31) + sg0.hashCode() : hashCode;
    }

    @DexIgnore
    public final y90 setHandMovingConfig(aa0 aa0) {
        this.e = aa0;
        return this;
    }

    @DexIgnore
    public final y90 setIconConfig(sg0 sg0) {
        this.f = sg0;
        return this;
    }

    @DexIgnore
    public final y90 setPriority(short s) {
        ud7 ud7 = ud7.a;
        if (s < 0 || 255 < s) {
            s = -1;
        }
        this.d = s;
        return this;
    }

    @DexIgnore
    public final y90 setSender(String str) {
        this.c = yz0.a(str, 97, null, null, 6);
        return this;
    }

    @DexIgnore
    public final y90 setVibePatternConfig(ca0 ca0) {
        this.g = ca0;
        return this;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.h);
        }
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.g);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.f, i);
        }
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m87setPriority(short s) {
        ud7 ud7 = ud7.a;
        if (s < 0 || 255 < s) {
            s = -1;
        }
        this.d = s;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m88setSender(String str) {
        this.c = yz0.a(str, 97, null, null, 6);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(jSONObject, r51.V2, this.h);
            yz0.a(jSONObject, r51.U2, yz0.a((int) this.a));
            yz0.a(jSONObject, r51.S, Byte.valueOf(this.b));
            yz0.a(jSONObject, r51.h, this.c);
            yz0.a(jSONObject, r51.P, Short.valueOf(this.d));
            r51 r51 = r51.z2;
            aa0 aa0 = this.e;
            JSONObject jSONObject2 = null;
            yz0.a(jSONObject, r51, aa0 != null ? aa0.a() : null);
            r51 r512 = r51.A2;
            ca0 ca0 = this.g;
            yz0.a(jSONObject, r512, ca0 != null ? Byte.valueOf(ca0.a()) : null);
            r51 r513 = r51.Z2;
            sg0 sg0 = this.f;
            if (sg0 != null) {
                jSONObject2 = sg0.a();
            }
            yz0.a(jSONObject, r513, jSONObject2);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public y90(String str, aa0 aa0, ca0 ca0) {
        this(str);
        this.e = aa0;
        this.g = ca0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ y90(android.os.Parcel r3, com.fossil.zd7 r4) {
        /*
            r2 = this;
            java.lang.String r4 = r3.readString()
            if (r4 == 0) goto L_0x0053
            java.lang.String r0 = "parcel.readString()!!"
            com.fossil.ee7.a(r4, r0)
            r2.<init>(r4)
            long r0 = r3.readLong()
            r2.a = r0
            byte r4 = r3.readByte()
            r2.b = r4
            java.lang.String r4 = r3.readString()
            if (r4 == 0) goto L_0x0021
            goto L_0x0023
        L_0x0021:
            java.lang.String r4 = ""
        L_0x0023:
            r2.m88setSender(r4)
            int r4 = r3.readInt()
            short r4 = (short) r4
            r2.m87setPriority(r4)
            java.lang.Class<com.fossil.aa0> r4 = com.fossil.aa0.class
            java.lang.ClassLoader r4 = r4.getClassLoader()
            android.os.Parcelable r4 = r3.readParcelable(r4)
            com.fossil.aa0 r4 = (com.fossil.aa0) r4
            r2.e = r4
            java.io.Serializable r4 = r3.readSerializable()
            com.fossil.ca0 r4 = (com.fossil.ca0) r4
            r2.g = r4
            java.lang.Class<com.fossil.sg0> r4 = com.fossil.sg0.class
            java.lang.ClassLoader r4 = r4.getClassLoader()
            android.os.Parcelable r3 = r3.readParcelable(r4)
            com.fossil.sg0 r3 = (com.fossil.sg0) r3
            r2.f = r3
            return
        L_0x0053:
            com.fossil.ee7.a()
            r3 = 0
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.y90.<init>(android.os.Parcel, com.fossil.zd7):void");
    }
}
