package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum f44 {
    NONE,
    JAVA_ONLY,
    ALL;
    
    @DexIgnore
    public static /* final */ int REPORT_UPLOAD_VARIANT_DATATRANSPORT; // = 2;
    @DexIgnore
    public static /* final */ int REPORT_UPLOAD_VARIANT_LEGACY; // = 1;

    @DexIgnore
    public static f44 getState(boolean z, boolean z2) {
        if (!z) {
            return NONE;
        }
        if (!z2) {
            return JAVA_ONLY;
        }
        return ALL;
    }

    @DexIgnore
    public static f44 getState(y74 y74) {
        boolean z = true;
        boolean z2 = y74.g == 2;
        if (y74.h != 2) {
            z = false;
        }
        return getState(z2, z);
    }
}
