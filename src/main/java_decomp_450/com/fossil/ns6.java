package com.fossil;

import com.portfolio.platform.data.model.PermissionData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ns6 extends dl4<ms6> {
    @DexIgnore
    void close();

    @DexIgnore
    void t(List<PermissionData> list);
}
