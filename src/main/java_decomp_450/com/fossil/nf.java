package com.fossil;

import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import com.fossil.lf;
import com.fossil.qf;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nf<Key, Value> {
    @DexIgnore
    public Key a;
    @DexIgnore
    public qf.f b;
    @DexIgnore
    public lf.b<Key, Value> c;
    @DexIgnore
    public qf.c d;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public Executor e; // = o3.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends cd<qf<Value>> {
        @DexIgnore
        public qf<Value> g;
        @DexIgnore
        public lf<Key, Value> h;
        @DexIgnore
        public /* final */ lf.c i; // = new C0139a();
        @DexIgnore
        public /* final */ /* synthetic */ Object j;
        @DexIgnore
        public /* final */ /* synthetic */ lf.b k;
        @DexIgnore
        public /* final */ /* synthetic */ qf.f l;
        @DexIgnore
        public /* final */ /* synthetic */ Executor m;
        @DexIgnore
        public /* final */ /* synthetic */ Executor n;
        @DexIgnore
        public /* final */ /* synthetic */ qf.c o;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nf$a$a")
        /* renamed from: com.fossil.nf$a$a  reason: collision with other inner class name */
        public class C0139a implements lf.c {
            @DexIgnore
            public C0139a() {
            }

            @DexIgnore
            @Override // com.fossil.lf.c
            public void a() {
                a.this.c();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Executor executor, Object obj, lf.b bVar, qf.f fVar, Executor executor2, Executor executor3, qf.c cVar) {
            super(executor);
            this.j = obj;
            this.k = bVar;
            this.l = fVar;
            this.m = executor2;
            this.n = executor3;
            this.o = cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r1v4, resolved type: com.fossil.qf$d */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.cd
        public qf<Value> a() {
            qf<Value> a;
            Object obj = this.j;
            qf<Value> qfVar = this.g;
            if (qfVar != null) {
                obj = qfVar.e();
            }
            do {
                lf<Key, Value> lfVar = this.h;
                if (lfVar != null) {
                    lfVar.removeInvalidatedCallback(this.i);
                }
                lf<Key, Value> create = this.k.create();
                this.h = create;
                create.addInvalidatedCallback(this.i);
                qf.d dVar = new qf.d(this.h, this.l);
                dVar.b(this.m);
                dVar.a(this.n);
                dVar.a(this.o);
                dVar.a(obj);
                a = dVar.a();
                this.g = a;
            } while (a.h());
            return this.g;
        }
    }

    @DexIgnore
    public nf(lf.b<Key, Value> bVar, qf.f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("PagedList.Config must be provided");
        } else if (bVar != null) {
            this.c = bVar;
            this.b = fVar;
        } else {
            throw new IllegalArgumentException("DataSource.Factory must be provided");
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public LiveData<qf<Value>> a() {
        return a(this.a, this.b, this.d, this.c, o3.d(), this.e);
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public static <Key, Value> LiveData<qf<Value>> a(Key key, qf.f fVar, qf.c cVar, lf.b<Key, Value> bVar, Executor executor, Executor executor2) {
        return new a(executor2, key, bVar, fVar, executor, executor2, cVar).b();
    }
}
