package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.internal.Excluder;
import com.google.gson.internal.bind.TreeTypeAdapter;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be4 {
    @DexIgnore
    public Excluder a; // = Excluder.g;
    @DexIgnore
    public pe4 b; // = pe4.DEFAULT;
    @DexIgnore
    public ae4 c; // = zd4.IDENTITY;
    @DexIgnore
    public /* final */ Map<Type, ce4<?>> d; // = new HashMap();
    @DexIgnore
    public /* final */ List<qe4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<qe4> f; // = new ArrayList();
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public String h;
    @DexIgnore
    public int i; // = 2;
    @DexIgnore
    public int j; // = 2;
    @DexIgnore
    public boolean k; // = false;
    @DexIgnore
    public boolean l; // = false;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = false;
    @DexIgnore
    public boolean o; // = false;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public be4 a(int... iArr) {
        this.a = this.a.a(iArr);
        return this;
    }

    @DexIgnore
    public be4 b(xd4 xd4) {
        this.a = this.a.a(xd4, true, false);
        return this;
    }

    @DexIgnore
    public be4 a(zd4 zd4) {
        this.c = zd4;
        return this;
    }

    @DexIgnore
    public be4 a(xd4 xd4) {
        this.a = this.a.a(xd4, false, true);
        return this;
    }

    @DexIgnore
    public be4 a(String str) {
        this.h = str;
        return this;
    }

    @DexIgnore
    public be4 a(Type type, Object obj) {
        boolean z = obj instanceof ne4;
        we4.a(z || (obj instanceof fe4) || (obj instanceof ce4) || (obj instanceof TypeAdapter));
        if (obj instanceof ce4) {
            this.d.put(type, (ce4) obj);
        }
        if (z || (obj instanceof fe4)) {
            this.e.add(TreeTypeAdapter.a(TypeToken.get(type), obj));
        }
        if (obj instanceof TypeAdapter) {
            this.e.add(TypeAdapters.a(TypeToken.get(type), (TypeAdapter) obj));
        }
        return this;
    }

    @DexIgnore
    public Gson a() {
        ArrayList arrayList = new ArrayList(this.e.size() + this.f.size() + 3);
        arrayList.addAll(this.e);
        Collections.reverse(arrayList);
        ArrayList arrayList2 = new ArrayList(this.f);
        Collections.reverse(arrayList2);
        arrayList.addAll(arrayList2);
        a(this.h, this.i, this.j, arrayList);
        return new Gson(this.a, this.c, this.d, this.g, this.k, this.o, this.m, this.n, this.p, this.l, this.b, this.h, this.i, this.j, this.e, this.f, arrayList);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, List<qe4> list) {
        wd4 wd4;
        wd4 wd42;
        wd4 wd43;
        if (str != null && !"".equals(str.trim())) {
            wd42 = new wd4(Date.class, str);
            wd4 = new wd4(Timestamp.class, str);
            wd43 = new wd4(java.sql.Date.class, str);
        } else if (i2 != 2 && i3 != 2) {
            wd4 wd44 = new wd4(Date.class, i2, i3);
            wd4 wd45 = new wd4(Timestamp.class, i2, i3);
            wd4 wd46 = new wd4(java.sql.Date.class, i2, i3);
            wd42 = wd44;
            wd4 = wd45;
            wd43 = wd46;
        } else {
            return;
        }
        list.add(TypeAdapters.a(Date.class, wd42));
        list.add(TypeAdapters.a(Timestamp.class, wd4));
        list.add(TypeAdapters.a(java.sql.Date.class, wd43));
    }
}
