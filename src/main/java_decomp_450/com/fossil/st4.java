package com.fossil;

import java.util.List;
import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class st4 {
    @DexIgnore
    public static /* final */ st4 c; // = d.a(w97.c(-1739917, -1023342, -4560696, -6982195, -8812853, -10177034, -11549705, -11677471, -11684180, -8271996, -5319295, -30107, -2825897, -10929, -18611, -6190977, -7297874));
    @DexIgnore
    public static /* final */ a d;
    @DexIgnore
    public /* final */ Random a;
    @DexIgnore
    public /* final */ List<Integer> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final st4 a() {
            return st4.c;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final st4 a(List<Integer> list) {
            ee7.b(list, "colorList");
            return new st4(list, null);
        }
    }

    /*
    static {
        a aVar = new a(null);
        d = aVar;
        aVar.a(w97.c(-957596, -686759, -416706, -1784274, -9977996, -10902850, -14642227, -5414233, -8366207));
    }
    */

    @DexIgnore
    public st4(List<Integer> list) {
        this.b = list;
        this.a = new Random(System.currentTimeMillis());
    }

    @DexIgnore
    public final int a() {
        List<Integer> list = this.b;
        return list.get(this.a.nextInt(list.size())).intValue();
    }

    @DexIgnore
    public final int a(Object obj) {
        ee7.b(obj, "key");
        return this.b.get(Math.abs(obj.hashCode()) % this.b.size()).intValue();
    }

    @DexIgnore
    public /* synthetic */ st4(List list, zd7 zd7) {
        this(list);
    }
}
