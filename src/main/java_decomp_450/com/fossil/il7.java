package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class il7 extends CancellationException implements ri7<il7> {
    @DexIgnore
    public /* final */ ik7 coroutine;

    @DexIgnore
    public il7(String str, ik7 ik7) {
        super(str);
        this.coroutine = ik7;
    }

    @DexIgnore
    public il7(String str) {
        this(str, null);
    }

    @DexIgnore
    @Override // com.fossil.ri7
    public il7 createCopy() {
        String message = getMessage();
        if (message == null) {
            message = "";
        }
        il7 il7 = new il7(message, this.coroutine);
        il7.initCause(this);
        return il7;
    }
}
