package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.uirenew.welcome.CompatibleModelsActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv6 extends go5 implements qv6, View.OnClickListener {
    @DexIgnore
    public pv6 f;
    @DexIgnore
    public qw6<g75> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ g75 a;

        @DexIgnore
        public b(g75 g75) {
            this.a = g75;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            FlexibleButton flexibleButton = this.a.r;
            ee7.a((Object) flexibleButton, "it.fbGetStarted");
            flexibleButton.setEnabled(z);
            this.a.r.a(z ? "flexible_button_secondary" : "flexible_button_disabled");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public rv6() {
        String b2 = eh5.l.a().b("nonBrandBlack");
        this.h = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void K0() {
        CompatibleModelsActivity.a aVar = CompatibleModelsActivity.y;
        Context requireContext = requireContext();
        ee7.a((Object) requireContext, "requireContext()");
        aVar.a(requireContext);
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void R0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            SignUpActivity.a aVar = SignUpActivity.D;
            ee7.a((Object) activity, "it");
            aVar.a(activity);
        }
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void Z() {
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.e(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public void onClick(View view) {
        if (view != null) {
            int id = view.getId();
            if (id == 2131362256) {
                pv6 pv6 = this.f;
                if (pv6 != null) {
                    pv6.h();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            } else if (id == 2131362708) {
                pv6 pv62 = this.f;
                if (pv62 != null) {
                    pv62.i();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        qw6<g75> qw6 = new qw6<>(this, (g75) qb.a(layoutInflater, 2131558639, viewGroup, false, a1()));
        this.g = qw6;
        if (qw6 != null) {
            g75 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onPause();
        qw6<g75> qw6 = this.g;
        if (qw6 != null) {
            g75 a2 = qw6.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.stopShimmerAnimation();
            }
            pv6 pv6 = this.f;
            if (pv6 != null) {
                pv6.g();
                jf5 c1 = c1();
                if (c1 != null) {
                    c1.a("");
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        ShimmerFrameLayout shimmerFrameLayout;
        super.onResume();
        qw6<g75> qw6 = this.g;
        if (qw6 != null) {
            g75 a2 = qw6.a();
            if (!(a2 == null || (shimmerFrameLayout = a2.w) == null)) {
                shimmerFrameLayout.startShimmerAnimation();
            }
            pv6 pv6 = this.f;
            if (pv6 != null) {
                pv6.f();
                jf5 c1 = c1();
                if (c1 != null) {
                    c1.d();
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<g75> qw6 = this.g;
        if (qw6 != null) {
            g75 a2 = qw6.a();
            if (a2 != null) {
                if (!tm4.a.a().d()) {
                    ShimmerFrameLayout shimmerFrameLayout = a2.w;
                    ee7.a((Object) shimmerFrameLayout, "it.shmNote");
                    shimmerFrameLayout.setVisibility(4);
                }
                a2.r.setOnClickListener(this);
                a2.v.setOnClickListener(this);
                if (ee7.a((Object) tm4.a.a().h(), (Object) "CN")) {
                    FlexibleCheckBox flexibleCheckBox = a2.q;
                    ee7.a((Object) flexibleCheckBox, "it.cbTermsService");
                    flexibleCheckBox.setVisibility(0);
                    FlexibleTextView flexibleTextView = a2.u;
                    ee7.a((Object) flexibleTextView, "it.ftvTermsService");
                    flexibleTextView.setVisibility(0);
                    FlexibleButton flexibleButton = a2.r;
                    ee7.a((Object) flexibleButton, "it.fbGetStarted");
                    flexibleButton.setEnabled(false);
                    a2.r.a("flexible_button_disabled");
                    a2.u.setLinkTextColor(this.h);
                    FlexibleTextView flexibleTextView2 = a2.u;
                    ee7.a((Object) flexibleTextView2, "it.ftvTermsService");
                    flexibleTextView2.setMovementMethod(new LinkMovementMethod());
                    a2.q.setOnCheckedChangeListener(new b(a2));
                } else {
                    FlexibleCheckBox flexibleCheckBox2 = a2.q;
                    ee7.a((Object) flexibleCheckBox2, "it.cbTermsService");
                    flexibleCheckBox2.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.u;
                    ee7.a((Object) flexibleTextView3, "it.ftvTermsService");
                    flexibleTextView3.setVisibility(8);
                    FlexibleButton flexibleButton2 = a2.r;
                    ee7.a((Object) flexibleButton2, "it.fbGetStarted");
                    flexibleButton2.setEnabled(true);
                    a2.r.a("flexible_button_secondary");
                }
            }
            V("tutorial_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.qv6
    public void a(Spanned spanned) {
        FlexibleTextView flexibleTextView;
        ee7.b(spanned, "message");
        if (isActive()) {
            qw6<g75> qw6 = this.g;
            if (qw6 != null) {
                g75 a2 = qw6.a();
                if (a2 != null && (flexibleTextView = a2.u) != null) {
                    flexibleTextView.setText(spanned);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(pv6 pv6) {
        ee7.b(pv6, "presenter");
        this.f = pv6;
    }
}
