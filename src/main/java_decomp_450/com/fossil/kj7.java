package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj7 {
    @DexIgnore
    public static final Object a(long j, fb7<? super i97> fb7) {
        if (j <= 0) {
            return i97.a;
        }
        bi7 bi7 = new bi7(mb7.a(fb7), 1);
        a(bi7.getContext()).a(j, (ai7<? super i97>) bi7);
        Object g = bi7.g();
        if (g == nb7.a()) {
            vb7.c(fb7);
        }
        return g;
    }

    @DexIgnore
    public static final jj7 a(ib7 ib7) {
        ib7.b bVar = ib7.get(gb7.m);
        if (!(bVar instanceof jj7)) {
            bVar = null;
        }
        jj7 jj7 = (jj7) bVar;
        return jj7 != null ? jj7 : gj7.a();
    }
}
