package com.fossil;

import android.graphics.Matrix;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qj implements oj {
    @DexIgnore
    public static Class<?> b;
    @DexIgnore
    public static boolean c;
    @DexIgnore
    public static Method d;
    @DexIgnore
    public static boolean e;
    @DexIgnore
    public static Method f;
    @DexIgnore
    public static boolean g;
    @DexIgnore
    public /* final */ View a;

    @DexIgnore
    public qj(View view) {
        this.a = view;
    }

    @DexIgnore
    public static oj a(View view, ViewGroup viewGroup, Matrix matrix) {
        a();
        Method method = d;
        if (method != null) {
            try {
                return new qj((View) method.invoke(null, view, viewGroup, matrix));
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
        return null;
    }

    @DexIgnore
    public static void b() {
        if (!c) {
            try {
                b = Class.forName("android.view.GhostView");
            } catch (ClassNotFoundException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve GhostView class", e2);
            }
            c = true;
        }
    }

    @DexIgnore
    public static void c() {
        if (!g) {
            try {
                b();
                Method declaredMethod = b.getDeclaredMethod("removeGhost", View.class);
                f = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve removeGhost method", e2);
            }
            g = true;
        }
    }

    @DexIgnore
    @Override // com.fossil.oj
    public void a(ViewGroup viewGroup, View view) {
    }

    @DexIgnore
    @Override // com.fossil.oj
    public void setVisibility(int i) {
        this.a.setVisibility(i);
    }

    @DexIgnore
    public static void a(View view) {
        c();
        Method method = f;
        if (method != null) {
            try {
                method.invoke(null, view);
            } catch (IllegalAccessException unused) {
            } catch (InvocationTargetException e2) {
                throw new RuntimeException(e2.getCause());
            }
        }
    }

    @DexIgnore
    public static void a() {
        if (!e) {
            try {
                b();
                Method declaredMethod = b.getDeclaredMethod("addGhost", View.class, ViewGroup.class, Matrix.class);
                d = declaredMethod;
                declaredMethod.setAccessible(true);
            } catch (NoSuchMethodException e2) {
                Log.i("GhostViewApi21", "Failed to retrieve addGhost method", e2);
            }
            e = true;
        }
    }
}
