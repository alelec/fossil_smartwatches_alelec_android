package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz0 extends uh1 {
    @DexIgnore
    public /* final */ qk1 G;
    @DexIgnore
    public /* final */ qk1 H;
    @DexIgnore
    public /* final */ byte[] I;
    @DexIgnore
    public byte[] J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public qk1 L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ wz0(short s, ri1 ri1, int i, int i2) {
        super(qa1.W, ri1, (i2 & 4) != 0 ? 3 : i);
        qk1 qk1 = qk1.FTC;
        this.G = qk1;
        this.H = qk1;
        byte[] array = ByteBuffer.allocate(11).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_GET_FILE.a).putShort(s).putInt(0).putInt(1).array();
        ee7.a((Object) array, "ByteBuffer.allocate(11)\n\u2026ILE)\n            .array()");
        this.I = array;
        byte[] array2 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).put(g51.LEGACY_GET_FILE.a()).put((byte) 0).putShort(s).array();
        ee7.a((Object) array2, "ByteBuffer.allocate(4)\n \u2026dle)\n            .array()");
        this.J = array2;
        this.L = qk1.FTD;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public mw0 a(byte b) {
        return tu0.j.a(b);
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean c(rk1 rk1) {
        return rk1.a == this.L;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public void f(rk1 rk1) {
        byte[] bArr;
        if (((v81) this).s) {
            bArr = ir0.b.a(((v81) this).y.u, this.L, rk1.b);
        } else {
            bArr = rk1.b;
        }
        if (bArr.length == 0) {
            ((v81) this).v = sz0.a(((v81) this).v, null, null, ay0.d, null, null, 27);
        }
        ((uh1) this).E = true;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 l() {
        return this.H;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] n() {
        return this.I;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public qk1 o() {
        return this.G;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public boolean p() {
        return this.K;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public byte[] q() {
        return this.J;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        ((uh1) this).E = ((v81) this).v.c != ay0.a;
        return jSONObject;
    }
}
