package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ft5 extends io5<et5> {
    @DexIgnore
    void a();

    @DexIgnore
    void b();

    @DexIgnore
    void c();

    @DexIgnore
    void close();

    @DexIgnore
    void j(boolean z);

    @DexIgnore
    void k(List<at5> list);

    @DexIgnore
    void l();
}
