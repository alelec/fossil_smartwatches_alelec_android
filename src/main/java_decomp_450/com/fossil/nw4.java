package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nw4 extends mw4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131362110, 1);
        Q.put(2131362636, 2);
        Q.put(2131363342, 3);
        Q.put(2131362083, 4);
        Q.put(2131362637, 5);
        Q.put(2131362374, 6);
        Q.put(2131362373, 7);
        Q.put(2131362751, 8);
        Q.put(2131362371, 9);
        Q.put(2131362369, 10);
        Q.put(2131362401, 11);
        Q.put(2131362705, 12);
        Q.put(2131361889, 13);
        Q.put(2131362044, 14);
        Q.put(2131362085, 15);
        Q.put(2131362886, 16);
        Q.put(2131362476, 17);
        Q.put(2131362417, 18);
        Q.put(2131362180, 19);
        Q.put(2131362764, 20);
        Q.put(2131363386, 21);
        Q.put(2131362465, 22);
        Q.put(2131362974, 23);
    }
    */

    @DexIgnore
    public nw4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 24, P, Q));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public nw4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (AppBarLayout) objArr[13], (ConstraintLayout) objArr[14], (ConstraintLayout) objArr[4], (ConstraintLayout) objArr[15], (ConstraintLayout) objArr[1], (OverviewDayChart) objArr[19], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[18], (FlexibleTextView) objArr[22], (FlexibleTextView) objArr[17], (RTLImageView) objArr[2], (RTLImageView) objArr[5], (RTLImageView) objArr[12], (View) objArr[8], (LinearLayout) objArr[20], (FlexibleProgressBar) objArr[16], (ConstraintLayout) objArr[0], (RecyclerView) objArr[23], (FlexibleTextView) objArr[3], (View) objArr[21]);
        this.O = -1;
        ((mw4) this).K.setTag(null);
        a(view);
        f();
    }
}
