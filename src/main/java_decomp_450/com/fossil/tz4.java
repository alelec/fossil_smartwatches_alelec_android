package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.chart.TodayHeartRateChart;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tz4 extends sz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public /* final */ NestedScrollView A;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131362076, 1);
        D.put(2131363295, 2);
        D.put(2131363418, 3);
        D.put(2131362675, 4);
        D.put(2131362079, 5);
        D.put(2131363302, 6);
        D.put(2131363420, 7);
        D.put(2131362677, 8);
        D.put(2131362180, 9);
        D.put(2131362245, 10);
    }
    */

    @DexIgnore
    public tz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 11, C, D));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.B != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }

    @DexIgnore
    public tz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[5], (TodayHeartRateChart) objArr[9], (FlexibleButton) objArr[10], (ImageView) objArr[4], (ImageView) objArr[8], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[6], (View) objArr[3], (View) objArr[7]);
        this.B = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.A = nestedScrollView;
        nestedScrollView.setTag(null);
        a(view);
        f();
    }
}
