package com.fossil;

import android.content.Intent;
import com.fossil.fl4;
import com.fossil.nj5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.microapp.MicroAppLastSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a56 extends fl4<c, d, b> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public c e;
    @DexIgnore
    public /* final */ e f; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository g;
    @DexIgnore
    public /* final */ HybridPresetRepository h;
    @DexIgnore
    public /* final */ MicroAppRepository i;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public b(int i, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "mBLEErrorCodes");
            this.a = i;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
        @DexIgnore
        public /* final */ HybridPreset a;

        @DexIgnore
        public c(HybridPreset hybridPreset) {
            ee7.b(hybridPreset, "mPreset");
            this.a = hybridPreset;
        }

        @DexIgnore
        public final HybridPreset a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            if (a56.this.d()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
                if (communicateMode == CommunicateMode.SET_LINK_MAPPING) {
                    a56.this.a(false);
                    if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                        FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - success");
                        a56.this.f();
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d("SetHybridPresetToWatchUseCase", "onReceive - failed");
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    if (integerArrayListExtra == null) {
                        integerArrayListExtra = new ArrayList<>(intExtra2);
                    }
                    a56.this.a(new b(intExtra2, integerArrayListExtra));
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.domain.usecase.SetHybridPresetToWatchUseCase$setPresetToDb$1", f = "SetHybridPresetToWatchUseCase.kt", l = {80}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ a56 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(a56 a56, fb7 fb7) {
            super(2, fb7);
            this.this$0 = a56;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            HybridPreset hybridPreset;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                HybridPreset a2 = a56.c(this.this$0).a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SetHybridPresetToWatchUseCase", "set to watch success, set preset " + a2 + " to db");
                a2.setActive(true);
                HybridPresetRepository a3 = this.this$0.h;
                this.L$0 = yi7;
                this.L$1 = a2;
                this.label = 1;
                if (a3.upsertHybridPreset(a2, this) == a) {
                    return a;
                }
                hybridPreset = a2;
            } else if (i == 1) {
                hybridPreset = (HybridPreset) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Calendar instance = Calendar.getInstance();
            ee7.a((Object) instance, "Calendar.getInstance()");
            String y = zd5.y(instance.getTime());
            Iterator<HybridPresetAppSetting> it = hybridPreset.getButtons().iterator();
            while (it.hasNext()) {
                HybridPresetAppSetting next = it.next();
                if (!sc5.a(next.getSettings())) {
                    a56 a56 = this.this$0;
                    String appId = next.getAppId();
                    String settings = next.getSettings();
                    if (settings != null) {
                        a56.a(appId, settings);
                        MicroAppLastSettingRepository b = this.this$0.j;
                        String appId2 = next.getAppId();
                        ee7.a((Object) y, "updatedAt");
                        String settings2 = next.getSettings();
                        if (settings2 != null) {
                            b.upsertMicroAppLastSetting(new MicroAppLastSetting(appId2, y, settings2));
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            this.this$0.a(new d());
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public a56(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(hybridPresetRepository, "mHybridPresetRepository");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        this.g = deviceRepository;
        this.h = hybridPresetRepository;
        this.i = microAppRepository;
        this.j = microAppLastSettingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ c c(a56 a56) {
        c cVar = a56.e;
        if (cVar != null) {
            return cVar;
        }
        ee7.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "SetHybridPresetToWatchUseCase";
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final void e() {
        nj5.d.a(this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    public final ik7 f() {
        return xh7.b(b(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        nj5.d.b(this.f, CommunicateMode.SET_LINK_MAPPING);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x007b, code lost:
        if (com.fossil.pb7.a(com.portfolio.platform.PortfolioApp.g0.c().c(r6, r7)) != null) goto L_0x0094;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.a56.c r6, com.fossil.fb7<java.lang.Object> r7) {
        /*
            r5 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r0 = "SetHybridPresetToWatchUseCase"
            java.lang.String r1 = "executeUseCase"
            r7.d(r0, r1)
            if (r6 == 0) goto L_0x0086
            r5.e = r6
            r6 = 1
            r5.d = r6
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            java.lang.String r6 = r6.c()
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "execute with preset "
            r1.append(r2)
            com.fossil.a56$c r2 = r5.e
            r3 = 0
            java.lang.String r4 = "mRequestValues"
            if (r2 == 0) goto L_0x0082
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r2.a()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r7.d(r0, r1)
            com.fossil.a56$c r7 = r5.e
            if (r7 == 0) goto L_0x007e
            com.portfolio.platform.data.model.room.microapp.HybridPreset r7 = r7.a()
            com.portfolio.platform.data.source.DeviceRepository r1 = r5.g
            com.portfolio.platform.data.source.MicroAppRepository r2 = r5.i
            java.util.List r7 = com.fossil.xc5.a(r7, r6, r1, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "set preset to watch with bleMappings "
            r2.append(r3)
            r2.append(r7)
            java.lang.String r2 = r2.toString()
            r1.d(r0, r2)
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            long r6 = r0.c(r6, r7)
            java.lang.Long r6 = com.fossil.pb7.a(r6)
            if (r6 == 0) goto L_0x0086
            goto L_0x0094
        L_0x007e:
            com.fossil.ee7.d(r4)
            throw r3
        L_0x0082:
            com.fossil.ee7.d(r4)
            throw r3
        L_0x0086:
            r6 = -1
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            com.fossil.a56$b r0 = new com.fossil.a56$b
            r0.<init>(r6, r7)
            r5.a(r0)
        L_0x0094:
            java.lang.Object r6 = new java.lang.Object
            r6.<init>()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a56.a(com.fossil.a56$c, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        try {
            if (ee7.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_TIME2_ID.getValue())) {
                PortfolioApp.g0.c().o(((SecondTimezoneSetting) new Gson().a(str2, SecondTimezoneSetting.class)).getTimeZoneId());
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("SetHybridPresetToWatchUseCase", "setAutoSettingToWatch exception " + e2);
        }
    }
}
