package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mh7 extends lh7 {
    @DexIgnore
    public static /* synthetic */ String a(String str, char c, char c2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, c, c2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean b(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(str, str2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean c(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(str, str2, z);
    }

    @DexIgnore
    public static final String d(String str) {
        ee7.b(str, "$this$capitalize");
        if (!(str.length() > 0) || !Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        String substring = str.substring(0, 1);
        ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring != null) {
            String upperCase = substring.toUpperCase();
            ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            sb.append(upperCase);
            String substring2 = str.substring(1);
            ee7.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring2);
            return sb.toString();
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(String str, char c, char c2, boolean z) {
        ee7.b(str, "$this$replace");
        if (!z) {
            String replace = str.replace(c, c2);
            ee7.a((Object) replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return og7.a(nh7.a(str, new char[]{c}, z, 0, 4, (Object) null), String.valueOf(c2), null, null, 0, null, null, 62, null);
    }

    @DexIgnore
    public static final boolean b(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        }
        if (!z) {
            return str.equals(str2);
        }
        return str.equalsIgnoreCase(str2);
    }

    @DexIgnore
    public static final boolean c(String str, String str2, boolean z) {
        ee7.b(str, "$this$startsWith");
        ee7.b(str2, "prefix");
        if (!z) {
            return str.startsWith(str2);
        }
        return a(str, 0, str2, 0, str2.length(), z);
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, String str2, String str3, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, str2, str3, z);
    }

    @DexIgnore
    public static final String a(String str, String str2, String str3, boolean z) {
        ee7.b(str, "$this$replace");
        ee7.b(str2, "oldValue");
        ee7.b(str3, "newValue");
        return og7.a(nh7.b(str, new String[]{str2}, z, 0, 4, (Object) null), str3, null, null, 0, null, null, 62, null);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(str, str2, z);
    }

    @DexIgnore
    public static final boolean a(String str, String str2, boolean z) {
        ee7.b(str, "$this$endsWith");
        ee7.b(str2, "suffix");
        if (!z) {
            return str.endsWith(str2);
        }
        return a(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean a(java.lang.CharSequence r4) {
        /*
            java.lang.String r0 = "$this$isBlank"
            com.fossil.ee7.b(r4, r0)
            int r0 = r4.length()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x003e
            com.fossil.lf7 r0 = com.fossil.nh7.b(r4)
            boolean r3 = r0 instanceof java.util.Collection
            if (r3 == 0) goto L_0x0020
            r3 = r0
            java.util.Collection r3 = (java.util.Collection) r3
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x0020
        L_0x001e:
            r4 = 1
            goto L_0x003c
        L_0x0020:
            java.util.Iterator r0 = r0.iterator()
        L_0x0024:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x001e
            r3 = r0
            com.fossil.ja7 r3 = (com.fossil.ja7) r3
            int r3 = r3.a()
            char r3 = r4.charAt(r3)
            boolean r3 = com.fossil.qg7.a(r3)
            if (r3 != 0) goto L_0x0024
            r4 = 0
        L_0x003c:
            if (r4 == 0) goto L_0x003f
        L_0x003e:
            r1 = 1
        L_0x003f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mh7.a(java.lang.CharSequence):boolean");
    }

    @DexIgnore
    public static final boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        ee7.b(str, "$this$regionMatches");
        ee7.b(str2, FacebookRequestErrorClassification.KEY_OTHER);
        if (!z) {
            return str.regionMatches(i, str2, i2, i3);
        }
        return str.regionMatches(z, i, str2, i2, i3);
    }
}
