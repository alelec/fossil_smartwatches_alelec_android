package com.fossil;

import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yu3 implements zu3 {
    @DexIgnore
    public /* final */ zu3 a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore
    public yu3(float f, zu3 zu3) {
        while (zu3 instanceof yu3) {
            zu3 = ((yu3) zu3).a;
            f += ((yu3) zu3).b;
        }
        this.a = zu3;
        this.b = f;
    }

    @DexIgnore
    @Override // com.fossil.zu3
    public float a(RectF rectF) {
        return Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.a.a(rectF) + this.b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof yu3)) {
            return false;
        }
        yu3 yu3 = (yu3) obj;
        if (!this.a.equals(yu3.a) || this.b != yu3.b) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.a, Float.valueOf(this.b)});
    }
}
