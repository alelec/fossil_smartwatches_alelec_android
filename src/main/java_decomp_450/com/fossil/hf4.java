package com.fossil;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hf4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends hf4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public a(Method method, Object obj) {
            this.a = method;
            this.b = obj;
        }

        @DexIgnore
        @Override // com.fossil.hf4
        public <T> T a(Class<T> cls) throws Exception {
            hf4.b(cls);
            return (T) this.a.invoke(this.b, cls);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends hf4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(Method method, int i) {
            this.a = method;
            this.b = i;
        }

        @DexIgnore
        @Override // com.fossil.hf4
        public <T> T a(Class<T> cls) throws Exception {
            hf4.b(cls);
            return (T) this.a.invoke(null, cls, Integer.valueOf(this.b));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends hf4 {
        @DexIgnore
        public /* final */ /* synthetic */ Method a;

        @DexIgnore
        public c(Method method) {
            this.a = method;
        }

        @DexIgnore
        @Override // com.fossil.hf4
        public <T> T a(Class<T> cls) throws Exception {
            hf4.b(cls);
            return (T) this.a.invoke(null, cls, Object.class);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends hf4 {
        @DexIgnore
        @Override // com.fossil.hf4
        public <T> T a(Class<T> cls) {
            throw new UnsupportedOperationException("Cannot allocate " + cls);
        }
    }

    @DexIgnore
    public static hf4 a() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new a(cls.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception unused) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new b(declaredMethod2, intValue);
            } catch (Exception unused2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new c(declaredMethod3);
                } catch (Exception unused3) {
                    return new d();
                }
            }
        }
    }

    @DexIgnore
    public static void b(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + cls.getName());
        } else if (Modifier.isAbstract(modifiers)) {
            throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + cls.getName());
        }
    }

    @DexIgnore
    public abstract <T> T a(Class<T> cls) throws Exception;
}
