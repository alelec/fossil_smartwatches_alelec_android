package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om3 extends yl3 {
    @DexIgnore
    public String d;
    @DexIgnore
    public Set<Integer> e;
    @DexIgnore
    public Map<Integer, qm3> f;
    @DexIgnore
    public Long g;
    @DexIgnore
    public Long h;

    @DexIgnore
    public om3(xl3 xl3) {
        super(xl3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:210:0x0643, code lost:
        if (r8.zza() == false) goto L_0x064e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0645, code lost:
        r8 = java.lang.Integer.valueOf(r8.p());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x064e, code lost:
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x064f, code lost:
        r7.a("Invalid property filter ID. appId, id", r9, java.lang.String.valueOf(r8));
        r8 = false;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:108:0x02c8  */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x02cf A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.pp2> a(java.lang.String r47, java.util.List<com.fossil.rp2> r48, java.util.List<com.fossil.zp2> r49, java.lang.Long r50, java.lang.Long r51) {
        /*
            r46 = this;
            r10 = r46
            com.fossil.a72.b(r47)
            com.fossil.a72.a(r48)
            com.fossil.a72.a(r49)
            r0 = r47
            r10.d = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r10.e = r0
            com.fossil.n4 r0 = new com.fossil.n4
            r0.<init>()
            r10.f = r0
            r0 = r50
            r10.g = r0
            r0 = r51
            r10.h = r0
            java.util.Iterator r0 = r48.iterator()
        L_0x0029:
            boolean r1 = r0.hasNext()
            r11 = 0
            r12 = 1
            if (r1 == 0) goto L_0x0045
            java.lang.Object r1 = r0.next()
            com.fossil.rp2 r1 = (com.fossil.rp2) r1
            java.lang.String r1 = r1.q()
            java.lang.String r2 = "_s"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0029
            r1 = 1
            goto L_0x0046
        L_0x0045:
            r1 = 0
        L_0x0046:
            boolean r0 = com.fossil.b13.a()
            if (r0 == 0) goto L_0x005c
            com.fossil.ym3 r0 = r46.l()
            java.lang.String r2 = r10.d
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.g0
            boolean r0 = r0.d(r2, r3)
            if (r0 == 0) goto L_0x005c
            r13 = 1
            goto L_0x005d
        L_0x005c:
            r13 = 0
        L_0x005d:
            boolean r0 = com.fossil.b13.a()
            if (r0 == 0) goto L_0x0073
            com.fossil.ym3 r0 = r46.l()
            java.lang.String r2 = r10.d
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.f0
            boolean r0 = r0.d(r2, r3)
            if (r0 == 0) goto L_0x0073
            r14 = 1
            goto L_0x0074
        L_0x0073:
            r14 = 0
        L_0x0074:
            if (r1 == 0) goto L_0x00b5
            com.fossil.jb3 r2 = r46.n()
            java.lang.String r3 = r10.d
            r2.q()
            r2.g()
            com.fossil.a72.b(r3)
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r11)
            java.lang.String r5 = "current_session_count"
            r0.put(r5, r4)
            android.database.sqlite.SQLiteDatabase r4 = r2.u()     // Catch:{ SQLiteException -> 0x00a3 }
            java.lang.String r5 = "events"
            java.lang.String r6 = "app_id = ?"
            java.lang.String[] r7 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x00a3 }
            r7[r11] = r3     // Catch:{ SQLiteException -> 0x00a3 }
            r4.update(r5, r0, r6, r7)     // Catch:{ SQLiteException -> 0x00a3 }
            goto L_0x00b5
        L_0x00a3:
            r0 = move-exception
            com.fossil.jg3 r2 = r2.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.Object r3 = com.fossil.jg3.a(r3)
            java.lang.String r4 = "Error resetting session-scoped event counts. appId"
            r2.a(r4, r3, r0)
        L_0x00b5:
            java.util.Map r0 = java.util.Collections.emptyMap()
            if (r14 == 0) goto L_0x00c7
            if (r13 == 0) goto L_0x00c7
            com.fossil.jb3 r0 = r46.n()
            java.lang.String r2 = r10.d
            java.util.Map r0 = r0.e(r2)
        L_0x00c7:
            com.fossil.jb3 r2 = r46.n()
            java.lang.String r3 = r10.d
            java.util.Map r15 = r2.g(r3)
            boolean r2 = com.fossil.n13.a()
            if (r2 == 0) goto L_0x00e5
            com.fossil.ym3 r2 = r46.l()
            java.lang.String r3 = r10.d
            com.fossil.yf3<java.lang.Boolean> r4 = com.fossil.wb3.T0
            boolean r2 = r2.d(r3, r4)
            if (r2 != 0) goto L_0x00e7
        L_0x00e5:
            if (r15 == 0) goto L_0x036a
        L_0x00e7:
            boolean r2 = r15.isEmpty()
            if (r2 == 0) goto L_0x00ef
            goto L_0x036a
        L_0x00ef:
            java.util.HashSet r2 = new java.util.HashSet
            java.util.Set r3 = r15.keySet()
            r2.<init>(r3)
            if (r1 == 0) goto L_0x01d5
            java.lang.String r1 = r10.d
            com.fossil.a72.b(r1)
            com.fossil.a72.a(r15)
            com.fossil.n4 r3 = new com.fossil.n4
            r3.<init>()
            boolean r4 = r15.isEmpty()
            if (r4 != 0) goto L_0x01d3
            com.fossil.jb3 r4 = r46.n()
            java.util.Map r1 = r4.f(r1)
            java.util.Set r4 = r15.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x011d:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x01d3
            java.lang.Object r5 = r4.next()
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r5 = r5.intValue()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            java.lang.Object r6 = r15.get(r6)
            com.fossil.xp2 r6 = (com.fossil.xp2) r6
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)
            java.lang.Object r7 = r1.get(r7)
            java.util.List r7 = (java.util.List) r7
            if (r7 == 0) goto L_0x01c9
            boolean r8 = r7.isEmpty()
            if (r8 == 0) goto L_0x014b
            goto L_0x01c9
        L_0x014b:
            com.fossil.fm3 r8 = r46.m()
            java.util.List r9 = r6.q()
            java.util.List r8 = r8.a(r9, r7)
            boolean r9 = r8.isEmpty()
            if (r9 != 0) goto L_0x011d
            com.fossil.bw2$a r9 = r6.l()
            com.fossil.xp2$a r9 = (com.fossil.xp2.a) r9
            r9.o()
            r9.b(r8)
            com.fossil.fm3 r8 = r46.m()
            java.util.List r11 = r6.zza()
            java.util.List r8 = r8.a(r11, r7)
            r9.zza()
            r9.a(r8)
            r8 = 0
        L_0x017c:
            int r11 = r6.t()
            if (r8 >= r11) goto L_0x019a
            com.fossil.qp2 r11 = r6.b(r8)
            int r11 = r11.p()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            boolean r11 = r7.contains(r11)
            if (r11 == 0) goto L_0x0197
            r9.a(r8)
        L_0x0197:
            int r8 = r8 + 1
            goto L_0x017c
        L_0x019a:
            r8 = 0
        L_0x019b:
            int r11 = r6.v()
            if (r8 >= r11) goto L_0x01b9
            com.fossil.yp2 r11 = r6.c(r8)
            int r11 = r11.p()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            boolean r11 = r7.contains(r11)
            if (r11 == 0) goto L_0x01b6
            r9.b(r8)
        L_0x01b6:
            int r8 = r8 + 1
            goto L_0x019b
        L_0x01b9:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            com.fossil.jx2 r6 = r9.g()
            com.fossil.bw2 r6 = (com.fossil.bw2) r6
            com.fossil.xp2 r6 = (com.fossil.xp2) r6
            r3.put(r5, r6)
            goto L_0x01d0
        L_0x01c9:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r3.put(r5, r6)
        L_0x01d0:
            r11 = 0
            goto L_0x011d
        L_0x01d3:
            r11 = r3
            goto L_0x01d6
        L_0x01d5:
            r11 = r15
        L_0x01d6:
            java.util.Iterator r16 = r2.iterator()
        L_0x01da:
            boolean r1 = r16.hasNext()
            if (r1 == 0) goto L_0x036a
            java.lang.Object r1 = r16.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r17 = r1.intValue()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r17)
            java.lang.Object r1 = r11.get(r1)
            com.fossil.xp2 r1 = (com.fossil.xp2) r1
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>()
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>()
            com.fossil.n4 r7 = new com.fossil.n4
            r7.<init>()
            if (r1 == 0) goto L_0x0242
            int r2 = r1.t()
            if (r2 != 0) goto L_0x020c
            goto L_0x0242
        L_0x020c:
            java.util.List r2 = r1.s()
            java.util.Iterator r2 = r2.iterator()
        L_0x0214:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0242
            java.lang.Object r3 = r2.next()
            com.fossil.qp2 r3 = (com.fossil.qp2) r3
            boolean r4 = r3.zza()
            if (r4 == 0) goto L_0x0214
            int r4 = r3.p()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            boolean r8 = r3.q()
            if (r8 == 0) goto L_0x023d
            long r8 = r3.r()
            java.lang.Long r3 = java.lang.Long.valueOf(r8)
            goto L_0x023e
        L_0x023d:
            r3 = 0
        L_0x023e:
            r7.put(r4, r3)
            goto L_0x0214
        L_0x0242:
            com.fossil.n4 r8 = new com.fossil.n4
            r8.<init>()
            if (r1 == 0) goto L_0x0289
            int r2 = r1.v()
            if (r2 != 0) goto L_0x0250
            goto L_0x0289
        L_0x0250:
            java.util.List r2 = r1.u()
            java.util.Iterator r2 = r2.iterator()
        L_0x0258:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0289
            java.lang.Object r3 = r2.next()
            com.fossil.yp2 r3 = (com.fossil.yp2) r3
            boolean r4 = r3.zza()
            if (r4 == 0) goto L_0x0258
            int r4 = r3.r()
            if (r4 <= 0) goto L_0x0258
            int r4 = r3.p()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            int r9 = r3.r()
            int r9 = r9 - r12
            long r18 = r3.b(r9)
            java.lang.Long r3 = java.lang.Long.valueOf(r18)
            r8.put(r4, r3)
            goto L_0x0258
        L_0x0289:
            if (r1 == 0) goto L_0x02d3
            r2 = 0
        L_0x028c:
            int r3 = r1.p()
            int r3 = r3 << 6
            if (r2 >= r3) goto L_0x02d3
            java.util.List r3 = r1.zza()
            boolean r3 = com.fossil.fm3.a(r3, r2)
            if (r3 == 0) goto L_0x02c5
            com.fossil.jg3 r3 = r46.e()
            com.fossil.mg3 r3 = r3.B()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r17)
            java.lang.Integer r9 = java.lang.Integer.valueOf(r2)
            java.lang.String r12 = "Filter already evaluated. audience ID, filter ID"
            r3.a(r12, r4, r9)
            r6.set(r2)
            java.util.List r3 = r1.q()
            boolean r3 = com.fossil.fm3.a(r3, r2)
            if (r3 == 0) goto L_0x02c5
            r5.set(r2)
            r3 = 1
            goto L_0x02c6
        L_0x02c5:
            r3 = 0
        L_0x02c6:
            if (r3 != 0) goto L_0x02cf
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            r7.remove(r3)
        L_0x02cf:
            int r2 = r2 + 1
            r12 = 1
            goto L_0x028c
        L_0x02d3:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r17)
            java.lang.Object r1 = r15.get(r1)
            r4 = r1
            com.fossil.xp2 r4 = (com.fossil.xp2) r4
            if (r14 == 0) goto L_0x034e
            if (r13 == 0) goto L_0x034e
            java.lang.Integer r1 = java.lang.Integer.valueOf(r17)
            java.lang.Object r1 = r0.get(r1)
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x034e
            java.lang.Long r2 = r10.h
            if (r2 == 0) goto L_0x034e
            java.lang.Long r2 = r10.g
            if (r2 != 0) goto L_0x02f7
            goto L_0x034e
        L_0x02f7:
            java.util.Iterator r1 = r1.iterator()
        L_0x02fb:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x034e
            java.lang.Object r2 = r1.next()
            com.fossil.ap2 r2 = (com.fossil.ap2) r2
            int r3 = r2.p()
            java.lang.Long r9 = r10.h
            long r18 = r9.longValue()
            r20 = 1000(0x3e8, double:4.94E-321)
            long r18 = r18 / r20
            boolean r2 = r2.w()
            if (r2 == 0) goto L_0x0323
            java.lang.Long r2 = r10.g
            long r18 = r2.longValue()
            long r18 = r18 / r20
        L_0x0323:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            boolean r2 = r7.containsKey(r2)
            if (r2 == 0) goto L_0x0338
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            java.lang.Long r9 = java.lang.Long.valueOf(r18)
            r7.put(r2, r9)
        L_0x0338:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            boolean r2 = r8.containsKey(r2)
            if (r2 == 0) goto L_0x02fb
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            java.lang.Long r3 = java.lang.Long.valueOf(r18)
            r8.put(r2, r3)
            goto L_0x02fb
        L_0x034e:
            com.fossil.qm3 r12 = new com.fossil.qm3
            java.lang.String r3 = r10.d
            r9 = 0
            r1 = r12
            r2 = r46
            r18 = r11
            r11 = 0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.Map<java.lang.Integer, com.fossil.qm3> r1 = r10.f
            java.lang.Integer r2 = java.lang.Integer.valueOf(r17)
            r1.put(r2, r12)
            r11 = r18
            r12 = 1
            goto L_0x01da
        L_0x036a:
            r11 = 0
            boolean r0 = r48.isEmpty()
            java.lang.String r1 = "Skipping failed audience ID"
            if (r0 != 0) goto L_0x04fd
            com.fossil.tm3 r0 = new com.fossil.tm3
            r0.<init>(r10, r11)
            com.fossil.n4 r2 = new com.fossil.n4
            r2.<init>()
            java.util.Iterator r3 = r48.iterator()
        L_0x0381:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x04fd
            java.lang.Object r4 = r3.next()
            com.fossil.rp2 r4 = (com.fossil.rp2) r4
            java.lang.String r5 = r10.d
            com.fossil.rp2 r5 = r0.a(r5, r4)
            if (r5 == 0) goto L_0x0381
            com.fossil.jb3 r6 = r46.n()
            java.lang.String r13 = r10.d
            java.lang.String r7 = r5.q()
            java.lang.String r8 = r4.q()
            com.fossil.qb3 r8 = r6.a(r13, r8)
            if (r8 != 0) goto L_0x03e1
            com.fossil.jg3 r8 = r6.e()
            com.fossil.mg3 r8 = r8.w()
            java.lang.Object r9 = com.fossil.jg3.a(r13)
            com.fossil.hg3 r6 = r6.i()
            java.lang.String r6 = r6.a(r7)
            java.lang.String r7 = "Event aggregate wasn't created during raw event logging. appId, event"
            r8.a(r7, r9, r6)
            com.fossil.qb3 r6 = new com.fossil.qb3
            r12 = r6
            java.lang.String r14 = r4.q()
            r15 = 1
            r17 = 1
            r19 = 1
            long r21 = r4.s()
            r23 = 0
            r25 = 0
            r26 = 0
            r27 = 0
            r28 = 0
            r12.<init>(r13, r14, r15, r17, r19, r21, r23, r25, r26, r27, r28)
            goto L_0x0416
        L_0x03e1:
            com.fossil.qb3 r6 = new com.fossil.qb3
            r29 = r6
            java.lang.String r4 = r8.a
            r30 = r4
            java.lang.String r4 = r8.b
            r31 = r4
            long r12 = r8.c
            r14 = 1
            long r32 = r12 + r14
            long r12 = r8.d
            long r34 = r12 + r14
            long r12 = r8.e
            long r36 = r12 + r14
            long r12 = r8.f
            r38 = r12
            long r12 = r8.g
            r40 = r12
            java.lang.Long r4 = r8.h
            r42 = r4
            java.lang.Long r4 = r8.i
            r43 = r4
            java.lang.Long r4 = r8.j
            r44 = r4
            java.lang.Boolean r4 = r8.k
            r45 = r4
            r29.<init>(r30, r31, r32, r34, r36, r38, r40, r42, r43, r44, r45)
        L_0x0416:
            com.fossil.jb3 r4 = r46.n()
            r4.a(r6)
            long r7 = r6.c
            java.lang.String r4 = r5.q()
            java.lang.Object r9 = r2.get(r4)
            java.util.Map r9 = (java.util.Map) r9
            if (r9 != 0) goto L_0x0453
            com.fossil.jb3 r9 = r46.n()
            java.lang.String r12 = r10.d
            java.util.Map r9 = r9.f(r12, r4)
            boolean r12 = com.fossil.n13.a()
            if (r12 == 0) goto L_0x0449
            com.fossil.ym3 r12 = r46.l()
            java.lang.String r13 = r10.d
            com.fossil.yf3<java.lang.Boolean> r14 = com.fossil.wb3.T0
            boolean r12 = r12.d(r13, r14)
            if (r12 != 0) goto L_0x0450
        L_0x0449:
            if (r9 != 0) goto L_0x0450
            com.fossil.n4 r9 = new com.fossil.n4
            r9.<init>()
        L_0x0450:
            r2.put(r4, r9)
        L_0x0453:
            java.util.Set r4 = r9.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x045b:
            boolean r12 = r4.hasNext()
            if (r12 == 0) goto L_0x0381
            java.lang.Object r12 = r4.next()
            java.lang.Integer r12 = (java.lang.Integer) r12
            int r15 = r12.intValue()
            java.util.Set<java.lang.Integer> r12 = r10.e
            java.lang.Integer r13 = java.lang.Integer.valueOf(r15)
            boolean r12 = r12.contains(r13)
            if (r12 == 0) goto L_0x0487
            com.fossil.jg3 r12 = r46.e()
            com.fossil.mg3 r12 = r12.B()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r15)
            r12.a(r1, r13)
            goto L_0x045b
        L_0x0487:
            java.lang.Integer r12 = java.lang.Integer.valueOf(r15)
            java.lang.Object r12 = r9.get(r12)
            java.util.List r12 = (java.util.List) r12
            java.util.Iterator r20 = r12.iterator()
            r12 = 1
        L_0x0496:
            boolean r13 = r20.hasNext()
            if (r13 == 0) goto L_0x04ea
            java.lang.Object r12 = r20.next()
            com.fossil.ap2 r12 = (com.fossil.ap2) r12
            com.fossil.sm3 r14 = new com.fossil.sm3
            java.lang.String r13 = r10.d
            r14.<init>(r10, r13, r15, r12)
            java.lang.Long r13 = r10.g
            java.lang.Long r11 = r10.h
            int r12 = r12.p()
            boolean r19 = r10.a(r15, r12)
            r12 = r14
            r21 = r0
            r0 = r14
            r14 = r11
            r11 = r15
            r15 = r5
            r16 = r7
            r18 = r6
            boolean r12 = r12.a(r13, r14, r15, r16, r18, r19)
            com.fossil.ym3 r13 = r46.l()
            java.lang.String r14 = r10.d
            com.fossil.yf3<java.lang.Boolean> r15 = com.fossil.wb3.d0
            boolean r13 = r13.d(r14, r15)
            if (r13 == 0) goto L_0x04de
            if (r12 != 0) goto L_0x04de
            java.util.Set<java.lang.Integer> r0 = r10.e
            java.lang.Integer r13 = java.lang.Integer.valueOf(r11)
            r0.add(r13)
            goto L_0x04ed
        L_0x04de:
            com.fossil.qm3 r13 = r10.a(r11)
            r13.a(r0)
            r15 = r11
            r0 = r21
            r11 = 0
            goto L_0x0496
        L_0x04ea:
            r21 = r0
            r11 = r15
        L_0x04ed:
            if (r12 != 0) goto L_0x04f8
            java.util.Set<java.lang.Integer> r0 = r10.e
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            r0.add(r11)
        L_0x04f8:
            r0 = r21
            r11 = 0
            goto L_0x045b
        L_0x04fd:
            boolean r0 = r49.isEmpty()
            if (r0 != 0) goto L_0x0666
            com.fossil.n4 r0 = new com.fossil.n4
            r0.<init>()
            java.util.Iterator r2 = r49.iterator()
        L_0x050c:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0666
            java.lang.Object r3 = r2.next()
            com.fossil.zp2 r3 = (com.fossil.zp2) r3
            java.lang.String r4 = r3.q()
            java.lang.Object r5 = r0.get(r4)
            java.util.Map r5 = (java.util.Map) r5
            if (r5 != 0) goto L_0x054c
            com.fossil.jb3 r5 = r46.n()
            java.lang.String r6 = r10.d
            java.util.Map r5 = r5.g(r6, r4)
            boolean r6 = com.fossil.n13.a()
            if (r6 == 0) goto L_0x0542
            com.fossil.ym3 r6 = r46.l()
            java.lang.String r7 = r10.d
            com.fossil.yf3<java.lang.Boolean> r8 = com.fossil.wb3.T0
            boolean r6 = r6.d(r7, r8)
            if (r6 != 0) goto L_0x0549
        L_0x0542:
            if (r5 != 0) goto L_0x0549
            com.fossil.n4 r5 = new com.fossil.n4
            r5.<init>()
        L_0x0549:
            r0.put(r4, r5)
        L_0x054c:
            java.util.Set r4 = r5.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0554:
            boolean r6 = r4.hasNext()
            if (r6 == 0) goto L_0x050c
            java.lang.Object r6 = r4.next()
            java.lang.Integer r6 = (java.lang.Integer) r6
            int r6 = r6.intValue()
            java.util.Set<java.lang.Integer> r7 = r10.e
            java.lang.Integer r8 = java.lang.Integer.valueOf(r6)
            boolean r7 = r7.contains(r8)
            if (r7 == 0) goto L_0x0580
            com.fossil.jg3 r3 = r46.e()
            com.fossil.mg3 r3 = r3.B()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r6)
            r3.a(r1, r4)
            goto L_0x050c
        L_0x0580:
            java.lang.Integer r7 = java.lang.Integer.valueOf(r6)
            java.lang.Object r7 = r5.get(r7)
            java.util.List r7 = (java.util.List) r7
            java.util.Iterator r7 = r7.iterator()
            r8 = 1
        L_0x058f:
            boolean r9 = r7.hasNext()
            if (r9 == 0) goto L_0x0659
            java.lang.Object r8 = r7.next()
            com.fossil.dp2 r8 = (com.fossil.dp2) r8
            com.fossil.jg3 r9 = r46.e()
            r11 = 2
            boolean r9 = r9.a(r11)
            if (r9 == 0) goto L_0x05e8
            com.fossil.jg3 r9 = r46.e()
            com.fossil.mg3 r9 = r9.B()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r6)
            boolean r12 = r8.zza()
            if (r12 == 0) goto L_0x05c1
            int r12 = r8.p()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            goto L_0x05c2
        L_0x05c1:
            r12 = 0
        L_0x05c2:
            com.fossil.hg3 r13 = r46.i()
            java.lang.String r14 = r8.q()
            java.lang.String r13 = r13.c(r14)
            java.lang.String r14 = "Evaluating filter. audience, filter, property"
            r9.a(r14, r11, r12, r13)
            com.fossil.jg3 r9 = r46.e()
            com.fossil.mg3 r9 = r9.B()
            com.fossil.fm3 r11 = r46.m()
            java.lang.String r11 = r11.a(r8)
            java.lang.String r12 = "Filter definition"
            r9.a(r12, r11)
        L_0x05e8:
            boolean r9 = r8.zza()
            if (r9 == 0) goto L_0x0631
            int r9 = r8.p()
            r11 = 256(0x100, float:3.59E-43)
            if (r9 <= r11) goto L_0x05f7
            goto L_0x0631
        L_0x05f7:
            com.fossil.um3 r9 = new com.fossil.um3
            java.lang.String r11 = r10.d
            r9.<init>(r10, r11, r6, r8)
            java.lang.Long r11 = r10.g
            java.lang.Long r12 = r10.h
            int r8 = r8.p()
            boolean r8 = r10.a(r6, r8)
            boolean r8 = r9.a(r11, r12, r3, r8)
            com.fossil.ym3 r11 = r46.l()
            java.lang.String r12 = r10.d
            com.fossil.yf3<java.lang.Boolean> r13 = com.fossil.wb3.d0
            boolean r11 = r11.d(r12, r13)
            if (r11 == 0) goto L_0x0628
            if (r8 != 0) goto L_0x0628
            java.util.Set<java.lang.Integer> r7 = r10.e
            java.lang.Integer r9 = java.lang.Integer.valueOf(r6)
            r7.add(r9)
            goto L_0x0659
        L_0x0628:
            com.fossil.qm3 r11 = r10.a(r6)
            r11.a(r9)
            goto L_0x058f
        L_0x0631:
            com.fossil.jg3 r7 = r46.e()
            com.fossil.mg3 r7 = r7.w()
            java.lang.String r9 = r10.d
            java.lang.Object r9 = com.fossil.jg3.a(r9)
            boolean r11 = r8.zza()
            if (r11 == 0) goto L_0x064e
            int r8 = r8.p()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            goto L_0x064f
        L_0x064e:
            r8 = 0
        L_0x064f:
            java.lang.String r8 = java.lang.String.valueOf(r8)
            java.lang.String r11 = "Invalid property filter ID. appId, id"
            r7.a(r11, r9, r8)
            r8 = 0
        L_0x0659:
            if (r8 != 0) goto L_0x0554
            java.util.Set<java.lang.Integer> r7 = r10.e
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r7.add(r6)
            goto L_0x0554
        L_0x0666:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Map<java.lang.Integer, com.fossil.qm3> r0 = r10.f
            java.util.Set r0 = r0.keySet()
            java.util.Set<java.lang.Integer> r2 = r10.e
            r0.removeAll(r2)
            java.util.Iterator r2 = r0.iterator()
        L_0x067a:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x070a
            java.lang.Object r0 = r2.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.util.Map<java.lang.Integer, com.fossil.qm3> r3 = r10.f
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)
            java.lang.Object r3 = r3.get(r4)
            com.fossil.qm3 r3 = (com.fossil.qm3) r3
            com.fossil.pp2 r3 = r3.a(r0)
            r1.add(r3)
            com.fossil.jb3 r4 = r46.n()
            java.lang.String r5 = r10.d
            com.fossil.xp2 r3 = r3.q()
            r4.q()
            r4.g()
            com.fossil.a72.b(r5)
            com.fossil.a72.a(r3)
            byte[] r3 = r3.a()
            android.content.ContentValues r6 = new android.content.ContentValues
            r6.<init>()
            java.lang.String r7 = "app_id"
            r6.put(r7, r5)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r7 = "audience_id"
            r6.put(r7, r0)
            java.lang.String r0 = "current_results"
            r6.put(r0, r3)
            android.database.sqlite.SQLiteDatabase r0 = r4.u()     // Catch:{ SQLiteException -> 0x06f5 }
            java.lang.String r3 = "audience_filter_values"
            r7 = 5
            r8 = 0
            long r6 = r0.insertWithOnConflict(r3, r8, r6, r7)     // Catch:{ SQLiteException -> 0x06f3 }
            r11 = -1
            int r0 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r0 != 0) goto L_0x067a
            com.fossil.jg3 r0 = r4.e()     // Catch:{ SQLiteException -> 0x06f3 }
            com.fossil.mg3 r0 = r0.t()     // Catch:{ SQLiteException -> 0x06f3 }
            java.lang.String r3 = "Failed to insert filter results (got -1). appId"
            java.lang.Object r6 = com.fossil.jg3.a(r5)     // Catch:{ SQLiteException -> 0x06f3 }
            r0.a(r3, r6)     // Catch:{ SQLiteException -> 0x06f3 }
            goto L_0x067a
        L_0x06f3:
            r0 = move-exception
            goto L_0x06f7
        L_0x06f5:
            r0 = move-exception
            r8 = 0
        L_0x06f7:
            com.fossil.jg3 r3 = r4.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.Object r4 = com.fossil.jg3.a(r5)
            java.lang.String r5 = "Error storing filter results. appId"
            r3.a(r5, r4, r0)
            goto L_0x067a
        L_0x070a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.om3.a(java.lang.String, java.util.List, java.util.List, java.lang.Long, java.lang.Long):java.util.List");
    }

    @DexIgnore
    @Override // com.fossil.yl3
    public final boolean s() {
        return false;
    }

    @DexIgnore
    public final qm3 a(int i) {
        if (this.f.containsKey(Integer.valueOf(i))) {
            return this.f.get(Integer.valueOf(i));
        }
        qm3 qm3 = new qm3(this, this.d, null);
        this.f.put(Integer.valueOf(i), qm3);
        return qm3;
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        if (this.f.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.f.get(Integer.valueOf(i)).d.get(i2);
    }
}
