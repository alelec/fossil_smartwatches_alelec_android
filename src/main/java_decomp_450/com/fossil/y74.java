package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y74 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore
    public y74(String str, String str2, String str3, String str4, String str5, String str6, boolean z, int i, int i2) {
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = str4;
        this.e = str6;
        this.f = z;
        this.g = i;
        this.h = i2;
    }

    @DexIgnore
    public y74(String str, String str2, String str3, String str4, boolean z) {
        this(str, str2, str3, str4, null, null, z, 0, 0);
    }
}
