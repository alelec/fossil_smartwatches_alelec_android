package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g60 extends je0 {
    @DexIgnore
    public /* final */ h60 b;

    @DexIgnore
    public g60(h60 h60) {
        super(h60);
        this.b = h60;
    }

    @DexIgnore
    @Override // com.fossil.je0
    public h60 getErrorCode() {
        return this.b;
    }
}
