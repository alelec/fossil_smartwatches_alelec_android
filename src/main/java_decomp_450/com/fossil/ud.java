package com.fossil;

import androidx.lifecycle.CompositeGeneratedAdaptersObserver;
import androidx.lifecycle.FullLifecycleObserverAdapter;
import androidx.lifecycle.ReflectiveGenericLifecycleObserver;
import androidx.lifecycle.SingleGeneratedAdapterObserver;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ud {
    @DexIgnore
    public static Map<Class<?>, Integer> a; // = new HashMap();
    @DexIgnore
    public static Map<Class<?>, List<Constructor<? extends ld>>> b; // = new HashMap();

    @DexIgnore
    public static qd a(Object obj) {
        boolean z = obj instanceof qd;
        boolean z2 = obj instanceof kd;
        if (z && z2) {
            return new FullLifecycleObserverAdapter((kd) obj, (qd) obj);
        }
        if (z2) {
            return new FullLifecycleObserverAdapter((kd) obj, null);
        }
        if (z) {
            return (qd) obj;
        }
        Class<?> cls = obj.getClass();
        if (b(cls) != 2) {
            return new ReflectiveGenericLifecycleObserver(obj);
        }
        List<Constructor<? extends ld>> list = b.get(cls);
        if (list.size() == 1) {
            return new SingleGeneratedAdapterObserver(a(list.get(0), obj));
        }
        ld[] ldVarArr = new ld[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ldVarArr[i] = a(list.get(i), obj);
        }
        return new CompositeGeneratedAdaptersObserver(ldVarArr);
    }

    @DexIgnore
    public static int b(Class<?> cls) {
        Integer num = a.get(cls);
        if (num != null) {
            return num.intValue();
        }
        int d = d(cls);
        a.put(cls, Integer.valueOf(d));
        return d;
    }

    @DexIgnore
    public static boolean c(Class<?> cls) {
        return cls != null && rd.class.isAssignableFrom(cls);
    }

    @DexIgnore
    public static int d(Class<?> cls) {
        if (cls.getCanonicalName() == null) {
            return 1;
        }
        Constructor<? extends ld> a2 = a(cls);
        if (a2 != null) {
            b.put(cls, Collections.singletonList(a2));
            return 2;
        } else if (ad.c.c(cls)) {
            return 1;
        } else {
            Class<? super Object> superclass = cls.getSuperclass();
            ArrayList arrayList = null;
            if (c(superclass)) {
                if (b(superclass) == 1) {
                    return 1;
                }
                arrayList = new ArrayList(b.get(superclass));
            }
            Class<?>[] interfaces = cls.getInterfaces();
            for (Class<?> cls2 : interfaces) {
                if (c(cls2)) {
                    if (b(cls2) == 1) {
                        return 1;
                    }
                    if (arrayList == null) {
                        arrayList = new ArrayList();
                    }
                    arrayList.addAll(b.get(cls2));
                }
            }
            if (arrayList == null) {
                return 1;
            }
            b.put(cls, arrayList);
            return 2;
        }
    }

    @DexIgnore
    public static ld a(Constructor<? extends ld> constructor, Object obj) {
        try {
            return (ld) constructor.newInstance(obj);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e2) {
            throw new RuntimeException(e2);
        } catch (InvocationTargetException e3) {
            throw new RuntimeException(e3);
        }
    }

    @DexIgnore
    public static Constructor<? extends ld> a(Class<?> cls) {
        try {
            Package r0 = cls.getPackage();
            String canonicalName = cls.getCanonicalName();
            String name = r0 != null ? r0.getName() : "";
            if (!name.isEmpty()) {
                canonicalName = canonicalName.substring(name.length() + 1);
            }
            String a2 = a(canonicalName);
            if (!name.isEmpty()) {
                a2 = name + CodelessMatcher.CURRENT_CLASS_NAME + a2;
            }
            Constructor declaredConstructor = Class.forName(a2).getDeclaredConstructor(cls);
            if (!declaredConstructor.isAccessible()) {
                declaredConstructor.setAccessible(true);
            }
            return declaredConstructor;
        } catch (ClassNotFoundException unused) {
            return null;
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    @DexIgnore
    public static String a(String str) {
        return str.replace(CodelessMatcher.CURRENT_CLASS_NAME, LocaleConverter.LOCALE_DELIMITER) + "_LifecycleAdapter";
    }
}
