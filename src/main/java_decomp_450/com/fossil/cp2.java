package com.fossil;

import com.fossil.bw2;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp2 extends bw2<cp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ cp2 zzi;
    @DexIgnore
    public static volatile wx2<cp2> zzj;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public boolean zze;
    @DexIgnore
    public String zzf; // = "";
    @DexIgnore
    public String zzg; // = "";
    @DexIgnore
    public String zzh; // = "";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<cp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(cp2.zzi);
        }

        @DexIgnore
        public /* synthetic */ a(yo2 yo2) {
            this();
        }
    }

    @DexIgnore
    public enum b implements dw2 {
        UNKNOWN_COMPARISON_TYPE(0),
        LESS_THAN(1),
        GREATER_THAN(2),
        EQUAL(3),
        BETWEEN(4);
        
        @DexIgnore
        public /* final */ int zzg;

        /*
        static {
            new fp2();
        }
        */

        @DexIgnore
        public b(int i) {
            this.zzg = i;
        }

        @DexIgnore
        public static fw2 zzb() {
            return gp2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzg + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.dw2
        public final int zza() {
            return this.zzg;
        }

        @DexIgnore
        public static b zza(int i) {
            if (i == 0) {
                return UNKNOWN_COMPARISON_TYPE;
            }
            if (i == 1) {
                return LESS_THAN;
            }
            if (i == 2) {
                return GREATER_THAN;
            }
            if (i == 3) {
                return EQUAL;
            }
            if (i != 4) {
                return null;
            }
            return BETWEEN;
        }
    }

    /*
    static {
        cp2 cp2 = new cp2();
        zzi = cp2;
        bw2.a(cp2.class, cp2);
    }
    */

    @DexIgnore
    public static cp2 y() {
        return zzi;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (yo2.a[i - 1]) {
            case 1:
                return new cp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\u100c\u0000\u0002\u1007\u0001\u0003\u1008\u0002\u0004\u1008\u0003\u0005\u1008\u0004", new Object[]{"zzc", "zzd", b.zzb(), "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                wx2<cp2> wx2 = zzj;
                if (wx2 == null) {
                    synchronized (cp2.class) {
                        wx2 = zzj;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzi);
                            zzj = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final b p() {
        b zza = b.zza(this.zzd);
        return zza == null ? b.UNKNOWN_COMPARISON_TYPE : zza;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final boolean r() {
        return this.zze;
    }

    @DexIgnore
    public final boolean s() {
        return (this.zzc & 4) != 0;
    }

    @DexIgnore
    public final String t() {
        return this.zzf;
    }

    @DexIgnore
    public final boolean u() {
        return (this.zzc & 8) != 0;
    }

    @DexIgnore
    public final String v() {
        return this.zzg;
    }

    @DexIgnore
    public final boolean w() {
        return (this.zzc & 16) != 0;
    }

    @DexIgnore
    public final String x() {
        return this.zzh;
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }
}
