package com.fossil;

import com.google.gson.JsonElement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class he4 extends JsonElement {
    @DexIgnore
    public static /* final */ he4 a; // = new he4();

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || (obj instanceof he4);
    }

    @DexIgnore
    public int hashCode() {
        return he4.class.hashCode();
    }
}
