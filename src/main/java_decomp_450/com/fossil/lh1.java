package com.fossil;

import com.fossil.blesdk.database.SdkDatabase;
import com.fossil.fitness.AlgorithmManager;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.fossil.blesdk.device.DeviceImplementation$clearCache$1", f = "DeviceImplementation.kt", l = {}, m = "invokeSuspend")
public final class lh1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ /* synthetic */ km1 c;
    @DexIgnore
    public /* final */ /* synthetic */ ie0 d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public lh1(km1 km1, ie0 ie0, fb7 fb7) {
        super(2, fb7);
        this.c = km1;
        this.d = ie0;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        lh1 lh1 = new lh1(this.c, this.d, fb7);
        lh1.a = (yi7) obj;
        return lh1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        lh1 lh1 = new lh1(this.c, this.d, fb7);
        lh1.a = yi7;
        return lh1.invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        rc1 a2;
        nb7.a();
        if (this.b == 0) {
            t87.a(obj);
            String str = this.c.c.u;
            SdkDatabase a3 = SdkDatabase.e.a();
            Integer valueOf = (a3 == null || (a2 = a3.a()) == null) ? null : Integer.valueOf(a2.a(str));
            yz0.a(new JSONObject(), r51.w4, valueOf != null ? valueOf : -1);
            s11 s11 = s11.DELETE_ALL;
            t11 t11 = t11.a;
            if (valueOf != null) {
                valueOf.intValue();
            }
            AlgorithmManager.defaultManager().clearCache();
            this.d.c(i97.a);
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
