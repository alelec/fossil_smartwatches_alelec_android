package com.fossil;

import android.graphics.drawable.Drawable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p40<R> implements m40<R>, q40<R> {
    @DexIgnore
    public static /* final */ a p; // = new a();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public R e;
    @DexIgnore
    public n40 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public py j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public void a(Object obj, long j) throws InterruptedException {
            obj.wait(j);
        }

        @DexIgnore
        public void a(Object obj) {
            obj.notifyAll();
        }
    }

    @DexIgnore
    public p40(int i2, int i3) {
        this(i2, i3, true, p);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void a(b50 b50) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public synchronized void a(n40 n40) {
        this.f = n40;
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void b(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void b(b50 b50) {
        b50.a(this.a, this.b);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void c(Drawable drawable) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        if (r1 == null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        r1.clear();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0020, code lost:
        return true;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean cancel(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.isDone()     // Catch:{ all -> 0x0021 }
            if (r0 == 0) goto L_0x000a
            r3 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x0021 }
            return r3
        L_0x000a:
            r0 = 1
            r2.g = r0     // Catch:{ all -> 0x0021 }
            com.fossil.p40$a r1 = r2.d     // Catch:{ all -> 0x0021 }
            r1.a(r2)     // Catch:{ all -> 0x0021 }
            r1 = 0
            if (r3 == 0) goto L_0x001a
            com.fossil.n40 r3 = r2.f     // Catch:{ all -> 0x0021 }
            r2.f = r1     // Catch:{ all -> 0x0021 }
            r1 = r3
        L_0x001a:
            monitor-exit(r2)     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x0020
            r1.clear()
        L_0x0020:
            return r0
        L_0x0021:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.p40.cancel(boolean):boolean");
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public R get() throws InterruptedException, ExecutionException {
        try {
            return a((Long) null);
        } catch (TimeoutException e2) {
            throw new AssertionError(e2);
        }
    }

    @DexIgnore
    public synchronized boolean isCancelled() {
        return this.g;
    }

    @DexIgnore
    public synchronized boolean isDone() {
        return this.g || this.h || this.i;
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onDestroy() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStart() {
    }

    @DexIgnore
    @Override // com.fossil.q30
    public void onStop() {
    }

    @DexIgnore
    public p40(int i2, int i3, boolean z, a aVar) {
        this.a = i2;
        this.b = i3;
        this.c = z;
        this.d = aVar;
    }

    @DexIgnore
    @Override // com.fossil.c50
    public synchronized n40 a() {
        return this.f;
    }

    @DexIgnore
    @Override // java.util.concurrent.Future
    public R get(long j2, TimeUnit timeUnit) throws InterruptedException, ExecutionException, TimeoutException {
        return a(Long.valueOf(timeUnit.toMillis(j2)));
    }

    @DexIgnore
    @Override // com.fossil.c50
    public synchronized void a(Drawable drawable) {
    }

    @DexIgnore
    @Override // com.fossil.c50
    public synchronized void a(R r, f50<? super R> f50) {
    }

    @DexIgnore
    public final synchronized R a(Long l) throws ExecutionException, InterruptedException, TimeoutException {
        if (this.c && !isDone()) {
            v50.a();
        }
        if (this.g) {
            throw new CancellationException();
        } else if (this.i) {
            throw new ExecutionException(this.j);
        } else if (this.h) {
            return this.e;
        } else {
            if (l == null) {
                this.d.a(this, 0);
            } else if (l.longValue() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                long longValue = l.longValue() + currentTimeMillis;
                while (!isDone() && currentTimeMillis < longValue) {
                    this.d.a(this, longValue - currentTimeMillis);
                    currentTimeMillis = System.currentTimeMillis();
                }
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            } else if (this.i) {
                throw new ExecutionException(this.j);
            } else if (this.g) {
                throw new CancellationException();
            } else if (this.h) {
                return this.e;
            } else {
                throw new TimeoutException();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.q40
    public synchronized boolean a(py pyVar, Object obj, c50<R> c50, boolean z) {
        this.i = true;
        this.j = pyVar;
        this.d.a(this);
        return false;
    }

    @DexIgnore
    @Override // com.fossil.q40
    public synchronized boolean a(R r, Object obj, c50<R> c50, sw swVar, boolean z) {
        this.h = true;
        this.e = r;
        this.d.a(this);
        return false;
    }
}
