package com.fossil;

import com.facebook.internal.Utility;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.sina.weibo.sdk.statistic.LogBuilder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f03 implements c03 {
    @DexIgnore
    public static /* final */ tq2<Long> A;
    @DexIgnore
    public static /* final */ tq2<Long> B;
    @DexIgnore
    public static /* final */ tq2<Long> C;
    @DexIgnore
    public static /* final */ tq2<Long> D;
    @DexIgnore
    public static /* final */ tq2<Long> E;
    @DexIgnore
    public static /* final */ tq2<String> F;
    @DexIgnore
    public static /* final */ tq2<Long> G;
    @DexIgnore
    public static /* final */ tq2<Long> a;
    @DexIgnore
    public static /* final */ tq2<Long> b;
    @DexIgnore
    public static /* final */ tq2<String> c;
    @DexIgnore
    public static /* final */ tq2<String> d;
    @DexIgnore
    public static /* final */ tq2<Long> e;
    @DexIgnore
    public static /* final */ tq2<Long> f;
    @DexIgnore
    public static /* final */ tq2<Long> g;
    @DexIgnore
    public static /* final */ tq2<Long> h;
    @DexIgnore
    public static /* final */ tq2<Long> i;
    @DexIgnore
    public static /* final */ tq2<Long> j;
    @DexIgnore
    public static /* final */ tq2<Long> k;
    @DexIgnore
    public static /* final */ tq2<Long> l;
    @DexIgnore
    public static /* final */ tq2<Long> m;
    @DexIgnore
    public static /* final */ tq2<Long> n;
    @DexIgnore
    public static /* final */ tq2<Long> o;
    @DexIgnore
    public static /* final */ tq2<Long> p;
    @DexIgnore
    public static /* final */ tq2<Long> q;
    @DexIgnore
    public static /* final */ tq2<Long> r;
    @DexIgnore
    public static /* final */ tq2<Long> s;
    @DexIgnore
    public static /* final */ tq2<Long> t;
    @DexIgnore
    public static /* final */ tq2<Long> u;
    @DexIgnore
    public static /* final */ tq2<Long> v;
    @DexIgnore
    public static /* final */ tq2<Long> w;
    @DexIgnore
    public static /* final */ tq2<Long> x;
    @DexIgnore
    public static /* final */ tq2<Long> y;
    @DexIgnore
    public static /* final */ tq2<Long> z;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.ad_id_cache_time", ButtonService.CONNECT_TIMEOUT);
        b = dr2.a("measurement.config.cache_time", LogBuilder.MAX_INTERVAL);
        dr2.a("measurement.log_tag", "FA");
        c = dr2.a("measurement.config.url_authority", "app-measurement.com");
        d = dr2.a("measurement.config.url_scheme", Utility.URL_SCHEME);
        e = dr2.a("measurement.upload.debug_upload_interval", 1000L);
        f = dr2.a("measurement.lifetimevalue.max_currency_tracked", 4L);
        g = dr2.a("measurement.store.max_stored_events_per_app", 100000L);
        h = dr2.a("measurement.experiment.max_ids", 50L);
        i = dr2.a("measurement.audience.filter_result_max_count", 200L);
        j = dr2.a("measurement.alarm_manager.minimum_interval", 60000L);
        k = dr2.a("measurement.upload.minimum_delay", 500L);
        l = dr2.a("measurement.monitoring.sample_period_millis", LogBuilder.MAX_INTERVAL);
        m = dr2.a("measurement.upload.realtime_upload_interval", ButtonService.CONNECT_TIMEOUT);
        n = dr2.a("measurement.upload.refresh_blacklisted_config_interval", 604800000L);
        dr2.a("measurement.config.cache_time.service", 3600000L);
        o = dr2.a("measurement.service_client.idle_disconnect_millis", 5000L);
        dr2.a("measurement.log_tag.service", "FA-SVC");
        p = dr2.a("measurement.upload.stale_data_deletion_interval", LogBuilder.MAX_INTERVAL);
        q = dr2.a("measurement.upload.backoff_period", 43200000L);
        r = dr2.a("measurement.upload.initial_upload_delay_time", 15000L);
        s = dr2.a("measurement.upload.interval", 3600000L);
        t = dr2.a("measurement.upload.max_bundle_size", 65536L);
        u = dr2.a("measurement.upload.max_bundles", 100L);
        v = dr2.a("measurement.upload.max_conversions_per_day", 500L);
        w = dr2.a("measurement.upload.max_error_events_per_day", 1000L);
        x = dr2.a("measurement.upload.max_events_per_bundle", 1000L);
        y = dr2.a("measurement.upload.max_events_per_day", 100000L);
        z = dr2.a("measurement.upload.max_public_events_per_day", 50000L);
        A = dr2.a("measurement.upload.max_queue_time", 2419200000L);
        B = dr2.a("measurement.upload.max_realtime_events_per_day", 10L);
        C = dr2.a("measurement.upload.max_batch_size", 65536L);
        D = dr2.a("measurement.upload.retry_count", 6L);
        E = dr2.a("measurement.upload.retry_time", 1800000L);
        F = dr2.a("measurement.upload.url", "https://app-measurement.com/a");
        G = dr2.a("measurement.upload.window_interval", 3600000L);
    }
    */

    @DexIgnore
    @Override // com.fossil.c03
    public final long a() {
        return j.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long b() {
        return u.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long c() {
        return q.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long d() {
        return i.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long e() {
        return r.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long f() {
        return n.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long g() {
        return v.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long h() {
        return k.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long i() {
        return p.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long j() {
        return y.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long k() {
        return G.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long l() {
        return z.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long m() {
        return w.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long n() {
        return E.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long o() {
        return o.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long p() {
        return x.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final String q() {
        return F.b();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long r() {
        return C.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long s() {
        return D.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long t() {
        return A.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long u() {
        return s.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long v() {
        return B.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long w() {
        return t.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zza() {
        return a.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzb() {
        return b.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final String zzc() {
        return c.b();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final String zzd() {
        return d.b();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zze() {
        return e.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzf() {
        return f.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzg() {
        return g.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzh() {
        return h.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzl() {
        return l.b().longValue();
    }

    @DexIgnore
    @Override // com.fossil.c03
    public final long zzm() {
        return m.b().longValue();
    }
}
