package com.fossil;

import com.fossil.h5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public ArrayList<a> e; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public h5 a;
        @DexIgnore
        public h5 b;
        @DexIgnore
        public int c;
        @DexIgnore
        public h5.c d;
        @DexIgnore
        public int e;

        @DexIgnore
        public a(h5 h5Var) {
            this.a = h5Var;
            this.b = h5Var.g();
            this.c = h5Var.b();
            this.d = h5Var.f();
            this.e = h5Var.a();
        }

        @DexIgnore
        public void a(i5 i5Var) {
            i5Var.a(this.a.h()).a(this.b, this.c, this.d, this.e);
        }

        @DexIgnore
        public void b(i5 i5Var) {
            h5 a2 = i5Var.a(this.a.h());
            this.a = a2;
            if (a2 != null) {
                this.b = a2.g();
                this.c = this.a.b();
                this.d = this.a.f();
                this.e = this.a.a();
                return;
            }
            this.b = null;
            this.c = 0;
            this.d = h5.c.STRONG;
            this.e = 0;
        }
    }

    @DexIgnore
    public s5(i5 i5Var) {
        this.a = i5Var.w();
        this.b = i5Var.x();
        this.c = i5Var.t();
        this.d = i5Var.j();
        ArrayList<h5> c2 = i5Var.c();
        int size = c2.size();
        for (int i = 0; i < size; i++) {
            this.e.add(new a(c2.get(i)));
        }
    }

    @DexIgnore
    public void a(i5 i5Var) {
        i5Var.s(this.a);
        i5Var.t(this.b);
        i5Var.p(this.c);
        i5Var.h(this.d);
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).a(i5Var);
        }
    }

    @DexIgnore
    public void b(i5 i5Var) {
        this.a = i5Var.w();
        this.b = i5Var.x();
        this.c = i5Var.t();
        this.d = i5Var.j();
        int size = this.e.size();
        for (int i = 0; i < size; i++) {
            this.e.get(i).b(i5Var);
        }
    }
}
