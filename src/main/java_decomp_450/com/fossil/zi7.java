package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zi7 {
    @DexIgnore
    public static final yi7 a(yi7 yi7, ib7 ib7) {
        return new vl7(yi7.a().plus(ib7));
    }

    @DexIgnore
    public static final boolean b(yi7 yi7) {
        ik7 ik7 = (ik7) yi7.a().get(ik7.o);
        if (ik7 != null) {
            return ik7.isActive();
        }
        return true;
    }

    @DexIgnore
    public static final yi7 a() {
        return new vl7(dl7.a(null, 1, null).plus(qj7.c()));
    }

    @DexIgnore
    public static final <R> Object a(kd7<? super yi7, ? super fb7<? super R>, ? extends Object> kd7, fb7<? super R> fb7) {
        jm7 jm7 = new jm7(fb7.getContext(), fb7);
        Object a = um7.a(jm7, jm7, kd7);
        if (a == nb7.a()) {
            vb7.c(fb7);
        }
        return a;
    }

    @DexIgnore
    public static final yi7 a(ib7 ib7) {
        if (ib7.get(ik7.o) == null) {
            ib7 = ib7.plus(nk7.a(null, 1, null));
        }
        return new vl7(ib7);
    }

    @DexIgnore
    public static /* synthetic */ void a(yi7 yi7, CancellationException cancellationException, int i, Object obj) {
        if ((i & 1) != 0) {
            cancellationException = null;
        }
        a(yi7, cancellationException);
    }

    @DexIgnore
    public static final void a(yi7 yi7, CancellationException cancellationException) {
        ik7 ik7 = (ik7) yi7.a().get(ik7.o);
        if (ik7 != null) {
            ik7.a(cancellationException);
            return;
        }
        throw new IllegalStateException(("Scope cannot be cancelled because it does not have a job: " + yi7).toString());
    }

    @DexIgnore
    public static final void a(yi7 yi7) {
        mk7.a(yi7.a());
    }
}
