package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xt implements yt {
    @DexIgnore
    public static /* final */ PorterDuffXfermode a; // = new PorterDuffXfermode(PorterDuff.Mode.SRC_IN);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.yt
    public Object a(rq rqVar, Bitmap bitmap, rt rtVar, fb7<? super Bitmap> fb7) {
        Paint paint = new Paint(3);
        int min = Math.min(bitmap.getWidth(), bitmap.getHeight());
        float f = ((float) min) / 2.0f;
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "input.config");
        Bitmap a2 = rqVar.a(min, min, config);
        Canvas canvas = new Canvas(a2);
        canvas.drawCircle(f, f, f, paint);
        paint.setXfermode(a);
        canvas.drawBitmap(bitmap, f - (((float) bitmap.getWidth()) / 2.0f), f - (((float) bitmap.getHeight()) / 2.0f), paint);
        rqVar.a(bitmap);
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.yt
    public String key() {
        String name = xt.class.getName();
        ee7.a((Object) name, "CircleCropTransformation::class.java.name");
        return name;
    }
}
