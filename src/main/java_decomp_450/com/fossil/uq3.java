package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uq3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<uq3> CREATOR; // = new vq3();
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ byte h;
    @DexIgnore
    public /* final */ byte i;
    @DexIgnore
    public /* final */ byte j;
    @DexIgnore
    public /* final */ byte p;
    @DexIgnore
    public /* final */ String q;

    @DexIgnore
    public uq3(int i2, String str, String str2, String str3, String str4, String str5, String str6, byte b2, byte b3, byte b4, byte b5, String str7) {
        this.a = i2;
        this.b = str;
        this.c = str2;
        this.d = str3;
        this.e = str4;
        this.f = str5;
        this.g = str6;
        this.h = b2;
        this.i = b3;
        this.j = b4;
        this.p = b5;
        this.q = str7;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && uq3.class == obj.getClass()) {
            uq3 uq3 = (uq3) obj;
            if (this.a != uq3.a || this.h != uq3.h || this.i != uq3.i || this.j != uq3.j || this.p != uq3.p || !this.b.equals(uq3.b)) {
                return false;
            }
            String str = this.c;
            if (str == null ? uq3.c != null : !str.equals(uq3.c)) {
                return false;
            }
            if (!this.d.equals(uq3.d) || !this.e.equals(uq3.e) || !this.f.equals(uq3.f)) {
                return false;
            }
            String str2 = this.g;
            if (str2 == null ? uq3.g != null : !str2.equals(uq3.g)) {
                return false;
            }
            String str3 = this.q;
            String str4 = uq3.q;
            if (str3 != null) {
                return str3.equals(str4);
            }
            if (str4 == null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        int hashCode = (((this.a + 31) * 31) + this.b.hashCode()) * 31;
        String str = this.c;
        int i2 = 0;
        int hashCode2 = (((((((hashCode + (str != null ? str.hashCode() : 0)) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31;
        String str2 = this.g;
        int hashCode3 = (((((((((hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31) + this.h) * 31) + this.i) * 31) + this.j) * 31) + this.p) * 31;
        String str3 = this.q;
        if (str3 != null) {
            i2 = str3.hashCode();
        }
        return hashCode3 + i2;
    }

    @DexIgnore
    public final String toString() {
        int i2 = this.a;
        String str = this.b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = this.e;
        String str5 = this.f;
        String str6 = this.g;
        byte b2 = this.h;
        byte b3 = this.i;
        byte b4 = this.j;
        byte b5 = this.p;
        String str7 = this.q;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 211 + String.valueOf(str2).length() + String.valueOf(str3).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str6).length() + String.valueOf(str7).length());
        sb.append("AncsNotificationParcelable{, id=");
        sb.append(i2);
        sb.append(", appId='");
        sb.append(str);
        sb.append('\'');
        sb.append(", dateTime='");
        sb.append(str2);
        sb.append('\'');
        sb.append(", notificationText='");
        sb.append(str3);
        sb.append('\'');
        sb.append(", title='");
        sb.append(str4);
        sb.append('\'');
        sb.append(", subtitle='");
        sb.append(str5);
        sb.append('\'');
        sb.append(", displayName='");
        sb.append(str6);
        sb.append('\'');
        sb.append(", eventId=");
        sb.append((int) b2);
        sb.append(", eventFlags=");
        sb.append((int) b3);
        sb.append(", categoryId=");
        sb.append((int) b4);
        sb.append(", categoryCount=");
        sb.append((int) b5);
        sb.append(", packageName='");
        sb.append(str7);
        sb.append('\'');
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        k72.a(parcel, 3, this.b, false);
        k72.a(parcel, 4, this.c, false);
        k72.a(parcel, 5, this.d, false);
        k72.a(parcel, 6, this.e, false);
        k72.a(parcel, 7, this.f, false);
        String str = this.g;
        if (str == null) {
            str = this.b;
        }
        k72.a(parcel, 8, str, false);
        k72.a(parcel, 9, this.h);
        k72.a(parcel, 10, this.i);
        k72.a(parcel, 11, this.j);
        k72.a(parcel, 12, this.p);
        k72.a(parcel, 13, this.q, false);
        k72.a(parcel, a2);
    }
}
