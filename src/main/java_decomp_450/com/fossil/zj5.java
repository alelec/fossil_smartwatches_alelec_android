package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.service.complicationapp.weather.ComplicationWeatherService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zj5 implements MembersInjector<ComplicationWeatherService> {
    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, gw6 gw6) {
        complicationWeatherService.h = gw6;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, rl4 rl4) {
        complicationWeatherService.i = rl4;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, ch5 ch5) {
        complicationWeatherService.j = ch5;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, PortfolioApp portfolioApp) {
        complicationWeatherService.p = portfolioApp;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, CustomizeRealDataRepository customizeRealDataRepository) {
        complicationWeatherService.q = customizeRealDataRepository;
    }

    @DexIgnore
    public static void a(ComplicationWeatherService complicationWeatherService, UserRepository userRepository) {
        complicationWeatherService.r = userRepository;
    }
}
