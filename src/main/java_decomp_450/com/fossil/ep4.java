package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ep4 {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {31}, m = "changeSocialId")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ep4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ep4 ep4, fb7 fb7) {
            super(fb7);
            this.this$0 = ep4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$changeSocialId$response$1", f = "ProfileRemoteDataSource.kt", l = {31}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<fo4>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ie4 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ep4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ep4 ep4, ie4 ie4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = ep4;
            this.$jsonObject = ie4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$jsonObject, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<fo4>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                ie4 ie4 = this.$jsonObject;
                this.label = 1;
                obj = a2.socialId(ie4, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource", f = "ProfileRemoteDataSource.kt", l = {16}, m = "fetchSocialProfile")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ep4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ep4 ep4, fb7 fb7) {
            super(fb7);
            this.this$0 = ep4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.ProfileRemoteDataSource$fetchSocialProfile$response$1", f = "ProfileRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
    public static final class d extends zb7 implements gd7<fb7<? super fv7<ie4>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ep4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ep4 ep4, fb7 fb7) {
            super(1, fb7);
            this.this$0 = ep4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.this$0, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ie4>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                this.label = 1;
                obj = a2.getMySocialProfile(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public ep4(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.zi5<com.fossil.fo4>> r10) {
        /*
            r9 = this;
            boolean r0 = r10 instanceof com.fossil.ep4.c
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.ep4$c r0 = (com.fossil.ep4.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ep4$c r0 = new com.fossil.ep4$c
            r0.<init>(r9, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0036
            if (r2 != r3) goto L_0x002e
            java.lang.Object r0 = r0.L$0
            com.fossil.ep4 r0 = (com.fossil.ep4) r0
            com.fossil.t87.a(r10)
            goto L_0x0049
        L_0x002e:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r0)
            throw r10
        L_0x0036:
            com.fossil.t87.a(r10)
            com.fossil.ep4$d r10 = new com.fossil.ep4$d
            r10.<init>(r9, r4)
            r0.L$0 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x0049
            return r1
        L_0x0049:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r0 = r10 instanceof com.fossil.bj5
            if (r0 == 0) goto L_0x0077
            com.fossil.fu4 r0 = com.fossil.fu4.a
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            com.fossil.ie4 r0 = (com.fossil.ie4) r0
            if (r0 != 0) goto L_0x005c
            goto L_0x006d
        L_0x005c:
            com.google.gson.Gson r1 = new com.google.gson.Gson     // Catch:{ oe4 -> 0x0069 }
            r1.<init>()     // Catch:{ oe4 -> 0x0069 }
            java.lang.Class<com.fossil.fo4> r2 = com.fossil.fo4.class
            java.lang.Object r0 = r1.a(r0, r2)     // Catch:{ oe4 -> 0x0069 }
            r4 = r0
            goto L_0x006d
        L_0x0069:
            r0 = move-exception
            r0.printStackTrace()
        L_0x006d:
            boolean r10 = r10.b()
            com.fossil.bj5 r0 = new com.fossil.bj5
            r0.<init>(r4, r10)
            goto L_0x0094
        L_0x0077:
            boolean r0 = r10 instanceof com.fossil.yi5
            if (r0 == 0) goto L_0x0095
            com.fossil.yi5 r0 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r2 = r10.a()
            com.portfolio.platform.data.model.ServerError r3 = r10.c()
            java.lang.Throwable r4 = r10.d()
            r5 = 0
            r6 = 0
            r7 = 24
            r8 = 0
            r1 = r0
            r1.<init>(r2, r3, r4, r5, r6, r7, r8)
        L_0x0094:
            return r0
        L_0x0095:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ep4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r9, com.fossil.fb7<? super com.fossil.zi5<com.fossil.fo4>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.ep4.a
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.ep4$a r0 = (com.fossil.ep4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ep4$a r0 = new com.fossil.ep4$a
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            r4 = 1
            if (r2 == 0) goto L_0x003e
            if (r2 != r4) goto L_0x0036
            java.lang.Object r9 = r0.L$2
            com.fossil.ie4 r9 = (com.fossil.ie4) r9
            java.lang.Object r9 = r0.L$1
            java.lang.String r9 = (java.lang.String) r9
            java.lang.Object r9 = r0.L$0
            com.fossil.ep4 r9 = (com.fossil.ep4) r9
            com.fossil.t87.a(r10)
            goto L_0x005f
        L_0x0036:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x003e:
            com.fossil.t87.a(r10)
            com.fossil.ie4 r10 = new com.fossil.ie4
            r10.<init>()
            java.lang.String r2 = "socialId"
            r10.a(r2, r9)
            com.fossil.ep4$b r2 = new com.fossil.ep4$b
            r2.<init>(r8, r10, r3)
            r0.L$0 = r8
            r0.L$1 = r9
            r0.L$2 = r10
            r0.label = r4
            java.lang.Object r10 = com.fossil.aj5.a(r2, r0)
            if (r10 != r1) goto L_0x005f
            return r1
        L_0x005f:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0073
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r10 = r10.a()
            r0 = 0
            r1 = 2
            r9.<init>(r10, r0, r1, r3)
            goto L_0x0090
        L_0x0073:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x0091
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            java.lang.Throwable r3 = r10.d()
            r4 = 0
            r5 = 0
            r6 = 24
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x0090:
            return r9
        L_0x0091:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ep4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }
}
