package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.StreetViewPanoramaView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class na3 extends a83 {
    @DexIgnore
    public /* final */ /* synthetic */ q63 a;

    @DexIgnore
    public na3(StreetViewPanoramaView.a aVar, q63 q63) {
        this.a = q63;
    }

    @DexIgnore
    @Override // com.fossil.z73
    public final void a(a73 a73) throws RemoteException {
        this.a.a(new t63(a73));
    }
}
