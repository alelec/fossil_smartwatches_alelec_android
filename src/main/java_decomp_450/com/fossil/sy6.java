package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import com.fossil.wy6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sy6 {
    @DexIgnore
    public static /* final */ String a; // = "sy6";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ vy6 c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ ty6 e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sy6$a$a")
        /* renamed from: com.fossil.sy6$a$a  reason: collision with other inner class name */
        public class C0181a implements wy6.b {
            @DexIgnore
            public /* final */ /* synthetic */ ImageView a;

            @DexIgnore
            public C0181a(ImageView imageView) {
                this.a = imageView;
            }

            @DexIgnore
            @Override // com.fossil.wy6.b
            public void a(BitmapDrawable bitmapDrawable) {
                ty6 ty6 = a.this.e;
                if (ty6 == null) {
                    this.a.setImageDrawable(bitmapDrawable);
                } else {
                    ty6.a(bitmapDrawable);
                }
            }
        }

        @DexIgnore
        public a(Context context, Bitmap bitmap, vy6 vy6, boolean z, ty6 ty6) {
            this.a = context;
            this.b = bitmap;
            this.c = vy6;
            this.d = z;
            this.e = ty6;
        }

        @DexIgnore
        public void a(ImageView imageView) {
            this.c.a = this.b.getWidth();
            this.c.b = this.b.getHeight();
            if (this.d) {
                new wy6(imageView.getContext(), this.b, this.c, new C0181a(imageView)).a();
            } else {
                imageView.setImageDrawable(new BitmapDrawable(this.a.getResources(), ry6.a(imageView.getContext(), this.b, this.c)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ Context b;
        @DexIgnore
        public /* final */ vy6 c; // = new vy6();
        @DexIgnore
        public boolean d;
        @DexIgnore
        public ty6 e;

        @DexIgnore
        public b(Context context) {
            this.b = context;
            View view = new View(context);
            this.a = view;
            view.setTag(sy6.a);
        }

        @DexIgnore
        public b a(int i) {
            this.c.c = i;
            return this;
        }

        @DexIgnore
        public b b(int i) {
            this.c.d = i;
            return this;
        }

        @DexIgnore
        public a a(Bitmap bitmap) {
            return new a(this.b, bitmap, this.c, this.d, this.e);
        }
    }

    @DexIgnore
    public static b a(Context context) {
        return new b(context);
    }
}
