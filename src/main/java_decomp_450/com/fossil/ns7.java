package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ns7 extends js7 {
    @DexIgnore
    public String option;

    @DexIgnore
    public ns7(String str) {
        super(str);
    }

    @DexIgnore
    public String getOption() {
        return this.option;
    }

    @DexIgnore
    public ns7(String str, String str2) {
        this(str);
        this.option = str2;
    }
}
