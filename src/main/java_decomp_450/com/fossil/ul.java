package com.fossil;

import androidx.viewpager2.widget.ViewPager2;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ul extends ViewPager2.i {
    @DexIgnore
    public /* final */ List<ViewPager2.i> a;

    @DexIgnore
    public ul(int i) {
        this.a = new ArrayList(i);
    }

    @DexIgnore
    public void a(ViewPager2.i iVar) {
        this.a.add(iVar);
    }

    @DexIgnore
    public void b(ViewPager2.i iVar) {
        this.a.remove(iVar);
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void a(int i, float f, int i2) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.a(i, f, i2);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void b(int i) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.b(i);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.viewpager2.widget.ViewPager2.i
    public void a(int i) {
        try {
            for (ViewPager2.i iVar : this.a) {
                iVar.a(i);
            }
        } catch (ConcurrentModificationException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public final void a(ConcurrentModificationException concurrentModificationException) {
        throw new IllegalStateException("Adding and removing callbacks during dispatch to callbacks is not supported", concurrentModificationException);
    }
}
