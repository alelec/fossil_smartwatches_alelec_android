package com.fossil;

import com.fossil.ng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tf {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ng.b {
        @DexIgnore
        public /* final */ /* synthetic */ sf a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ sf c;
        @DexIgnore
        public /* final */ /* synthetic */ ng.d d;
        @DexIgnore
        public /* final */ /* synthetic */ int e;
        @DexIgnore
        public /* final */ /* synthetic */ int f;

        @DexIgnore
        public a(sf sfVar, int i, sf sfVar2, ng.d dVar, int i2, int i3) {
            this.a = sfVar;
            this.b = i;
            this.c = sfVar2;
            this.d = dVar;
            this.e = i2;
            this.f = i3;
        }

        @DexIgnore
        @Override // com.fossil.ng.b
        public int a() {
            return this.f;
        }

        @DexIgnore
        @Override // com.fossil.ng.b
        public int b() {
            return this.e;
        }

        @DexIgnore
        @Override // com.fossil.ng.b
        public Object c(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sf sfVar = this.c;
            Object obj2 = sfVar.get(i2 + sfVar.e());
            if (obj == null || obj2 == null) {
                return null;
            }
            return this.d.getChangePayload(obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.ng.b
        public boolean a(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sf sfVar = this.c;
            Object obj2 = sfVar.get(i2 + sfVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areContentsTheSame(obj, obj2);
        }

        @DexIgnore
        @Override // com.fossil.ng.b
        public boolean b(int i, int i2) {
            Object obj = this.a.get(i + this.b);
            sf sfVar = this.c;
            Object obj2 = sfVar.get(i2 + sfVar.e());
            if (obj == obj2) {
                return true;
            }
            if (obj == null || obj2 == null) {
                return false;
            }
            return this.d.areItemsTheSame(obj, obj2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements xg {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ xg b;

        @DexIgnore
        public b(int i, xg xgVar) {
            this.a = i;
            this.b = xgVar;
        }

        @DexIgnore
        @Override // com.fossil.xg
        public void a(int i, int i2) {
            xg xgVar = this.b;
            int i3 = this.a;
            xgVar.a(i + i3, i2 + i3);
        }

        @DexIgnore
        @Override // com.fossil.xg
        public void b(int i, int i2) {
            this.b.b(i + this.a, i2);
        }

        @DexIgnore
        @Override // com.fossil.xg
        public void c(int i, int i2) {
            this.b.c(i + this.a, i2);
        }

        @DexIgnore
        @Override // com.fossil.xg
        public void a(int i, int i2, Object obj) {
            this.b.a(i + this.a, i2, obj);
        }
    }

    @DexIgnore
    public static <T> ng.c a(sf<T> sfVar, sf<T> sfVar2, ng.d<T> dVar) {
        int a2 = sfVar.a();
        return ng.a(new a(sfVar, a2, sfVar2, dVar, (sfVar.size() - a2) - sfVar.b(), (sfVar2.size() - sfVar2.a()) - sfVar2.b()), true);
    }

    @DexIgnore
    public static <T> void a(xg xgVar, sf<T> sfVar, sf<T> sfVar2, ng.c cVar) {
        int b2 = sfVar.b();
        int b3 = sfVar2.b();
        int a2 = sfVar.a();
        int a3 = sfVar2.a();
        if (b2 == 0 && b3 == 0 && a2 == 0 && a3 == 0) {
            cVar.a(xgVar);
            return;
        }
        if (b2 > b3) {
            int i = b2 - b3;
            xgVar.c(sfVar.size() - i, i);
        } else if (b2 < b3) {
            xgVar.b(sfVar.size(), b3 - b2);
        }
        if (a2 > a3) {
            xgVar.c(0, a2 - a3);
        } else if (a2 < a3) {
            xgVar.b(0, a3 - a2);
        }
        if (a3 != 0) {
            cVar.a(new b(a3, xgVar));
        } else {
            cVar.a(xgVar);
        }
    }

    @DexIgnore
    public static int a(ng.c cVar, sf sfVar, sf sfVar2, int i) {
        int a2 = sfVar.a();
        int i2 = i - a2;
        int size = (sfVar.size() - a2) - sfVar.b();
        if (i2 >= 0 && i2 < size) {
            for (int i3 = 0; i3 < 30; i3++) {
                int i4 = ((i3 / 2) * (i3 % 2 == 1 ? -1 : 1)) + i2;
                if (i4 >= 0 && i4 < sfVar.k()) {
                    try {
                        int a3 = cVar.a(i4);
                        if (a3 != -1) {
                            return a3 + sfVar2.e();
                        }
                    } catch (IndexOutOfBoundsException unused) {
                    }
                }
            }
        }
        return Math.max(0, Math.min(i, sfVar2.size() - 1));
    }
}
