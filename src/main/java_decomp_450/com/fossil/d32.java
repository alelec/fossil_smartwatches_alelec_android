package com.fossil;

import android.os.Bundle;
import com.fossil.v02;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d32 implements m32 {
    @DexIgnore
    public /* final */ l32 a;

    @DexIgnore
    public d32(l32 l32) {
        this.a = l32;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(int i) {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(i02 i02, v02<?> v02, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t) {
        this.a.s.i.add(t);
        return t;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b(Bundle bundle) {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void c() {
        for (v02.f fVar : this.a.f.values()) {
            fVar.a();
        }
        this.a.s.q = Collections.emptySet();
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b() {
        this.a.h();
    }
}
