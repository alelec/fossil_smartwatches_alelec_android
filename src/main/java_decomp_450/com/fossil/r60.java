package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.codeless.CodelessMatcher;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r60 extends k60 implements Parcelable, Comparable<r60> {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public r60(byte b2, byte b3) {
        this.a = b2;
        this.b = b3;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject put = new JSONObject().put("major", Byte.valueOf(this.a)).put("minor", Byte.valueOf(this.b));
        ee7.a((Object) put, "JSONObject()\n           \u2026     .put(\"minor\", minor)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(r60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            r60 r60 = (r60) obj;
            return this.a == r60.a && this.b == r60.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.Version");
    }

    @DexIgnore
    public final byte getMajor() {
        return this.a;
    }

    @DexIgnore
    public final byte getMinor() {
        return this.b;
    }

    @DexIgnore
    public final String getShortDescription() {
        StringBuilder sb = new StringBuilder();
        sb.append((int) this.a);
        sb.append('.');
        sb.append((int) this.b);
        return sb.toString();
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append((int) this.a);
        sb.append('.');
        sb.append((int) this.b);
        return sb.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<r60> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final r60 a(byte[] bArr) {
            if (bArr.length < 2) {
                return null;
            }
            return new r60(bArr[0], bArr[1]);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public r60 createFromParcel(Parcel parcel) {
            return new r60(parcel.readByte(), parcel.readByte());
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public r60[] newArray(int i) {
            return new r60[i];
        }

        @DexIgnore
        public final r60 a(String str) {
            List a = nh7.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null);
            if (a.size() >= 2) {
                Byte a2 = lh7.a((String) a.get(0));
                Byte a3 = lh7.a((String) a.get(1));
                if (!(a2 == null || a3 == null)) {
                    return new r60(a2.byteValue(), a3.byteValue());
                }
            }
            return new r60((byte) 0, (byte) 0);
        }
    }

    @DexIgnore
    /* renamed from: a */
    public int compareTo(r60 r60) {
        byte b2 = this.a;
        byte b3 = r60.a;
        if (b2 <= b3) {
            if (b2 >= b3) {
                byte b4 = this.b;
                byte b5 = r60.b;
                if (b4 <= b5) {
                    return b4 < b5 ? -1 : 0;
                }
            }
        }
        return 1;
    }
}
