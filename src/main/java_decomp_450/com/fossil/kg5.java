package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kg5 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";

    @DexIgnore
    public void a(ie4 ie4) {
        if (ie4.d("platform")) {
            ie4.a("platform").f();
        }
        if (ie4.d("data")) {
            this.a = ie4.a("data").d().a("url").f();
        }
        if (ie4.d("metadata")) {
            this.b = ie4.a("metadata").d().a("checksum").f();
        }
        if (ie4.d("metadata")) {
            this.c = ie4.a("metadata").d().a("appVersion").f();
        }
        if (ie4.d("createdAt")) {
            ie4.a("createdAt").f();
        }
        if (ie4.d("updatedAt")) {
            this.d = ie4.a("updatedAt").f();
        }
        if (ie4.d("objectId")) {
            ie4.a("objectId").f();
        }
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public String c() {
        return this.d;
    }

    @DexIgnore
    public String toString() {
        return "[LocalizationResponse:, \ndownloadUrl=" + this.a + ", \nchecksum=" + this.b + ", \nappVersion=" + this.c + ", \nupdatedAt=" + this.d + "]";
    }

    @DexIgnore
    public String a() {
        return this.b;
    }
}
