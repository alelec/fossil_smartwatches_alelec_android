package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.ActionMenuView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class c2 extends ViewGroup {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public ActionMenuView c;
    @DexIgnore
    public e2 d;
    @DexIgnore
    public int e;
    @DexIgnore
    public ha f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;

    @DexIgnore
    public c2(Context context) {
        this(context, null);
    }

    @DexIgnore
    public static int a(int i, int i2, boolean z) {
        return z ? i - i2 : i + i2;
    }

    @DexIgnore
    public int getAnimatedVisibility() {
        if (this.f != null) {
            return this.a.b;
        }
        return getVisibility();
    }

    @DexIgnore
    public int getContentHeight() {
        return this.e;
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(null, h0.ActionBar, y.actionBarStyle, 0);
        setContentHeight(obtainStyledAttributes.getLayoutDimension(h0.ActionBar_height, 0));
        obtainStyledAttributes.recycle();
        e2 e2Var = this.d;
        if (e2Var != null) {
            e2Var.a(configuration);
        }
    }

    @DexIgnore
    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 9) {
            this.h = false;
        }
        if (!this.h) {
            boolean onHoverEvent = super.onHoverEvent(motionEvent);
            if (actionMasked == 9 && !onHoverEvent) {
                this.h = true;
            }
        }
        if (actionMasked == 10 || actionMasked == 3) {
            this.h = false;
        }
        return true;
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.g = false;
        }
        if (!this.g) {
            boolean onTouchEvent = super.onTouchEvent(motionEvent);
            if (actionMasked == 0 && !onTouchEvent) {
                this.g = true;
            }
        }
        if (actionMasked == 1 || actionMasked == 3) {
            this.g = false;
        }
        return true;
    }

    @DexIgnore
    public abstract void setContentHeight(int i);

    @DexIgnore
    public void setVisibility(int i) {
        if (i != getVisibility()) {
            ha haVar = this.f;
            if (haVar != null) {
                haVar.a();
            }
            super.setVisibility(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ia {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public int b;

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public a a(ha haVar, int i) {
            c2.this.f = haVar;
            this.b = i;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            if (!this.a) {
                c2 c2Var = c2.this;
                c2Var.f = null;
                c2.super.setVisibility(this.b);
            }
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void c(View view) {
            c2.super.setVisibility(0);
            this.a = false;
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void a(View view) {
            this.a = true;
        }
    }

    @DexIgnore
    public c2(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ha a(int i, long j) {
        ha haVar = this.f;
        if (haVar != null) {
            haVar.a();
        }
        if (i == 0) {
            if (getVisibility() != 0) {
                setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            ha a2 = da.a(this);
            a2.a(1.0f);
            a2.a(j);
            a aVar = this.a;
            aVar.a(a2, i);
            a2.a(aVar);
            return a2;
        }
        ha a3 = da.a(this);
        a3.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a3.a(j);
        a aVar2 = this.a;
        aVar2.a(a3, i);
        a3.a(aVar2);
        return a3;
    }

    @DexIgnore
    public c2(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.a = new a();
        TypedValue typedValue = new TypedValue();
        if (!context.getTheme().resolveAttribute(y.actionBarPopupTheme, typedValue, true) || typedValue.resourceId == 0) {
            this.b = context;
        } else {
            this.b = new ContextThemeWrapper(context, typedValue.resourceId);
        }
    }

    @DexIgnore
    public int a(View view, int i, int i2, int i3) {
        view.measure(View.MeasureSpec.makeMeasureSpec(i, RecyclerView.UNDEFINED_DURATION), i2);
        return Math.max(0, (i - view.getMeasuredWidth()) - i3);
    }

    @DexIgnore
    public int a(View view, int i, int i2, int i3, boolean z) {
        int measuredWidth = view.getMeasuredWidth();
        int measuredHeight = view.getMeasuredHeight();
        int i4 = i2 + ((i3 - measuredHeight) / 2);
        if (z) {
            view.layout(i - measuredWidth, i4, i, measuredHeight + i4);
        } else {
            view.layout(i, i4, i + measuredWidth, measuredHeight + i4);
        }
        return z ? -measuredWidth : measuredWidth;
    }
}
