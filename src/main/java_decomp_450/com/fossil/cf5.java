package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf5 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = w97.a((Object[]) new String[]{"weather", "commute-time"});
    @DexIgnore
    public static /* final */ ArrayList<String> b; // = w97.a((Object[]) new String[]{"commute-time"});
    @DexIgnore
    public static /* final */ cf5 c; // = new cf5();

    /*
    static {
        w97.a((Object[]) new String[]{Constants.MUSIC, "weather", "commute-time"});
    }
    */

    @DexIgnore
    public final String a(String str) {
        ee7.b(str, "watchAppId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 1223440372 && str.equals("weather")) {
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886527);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return a2;
            }
        } else if (str.equals("commute-time")) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886363);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        }
        return "";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003b, code lost:
        if (r4.equals("commute-time") != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001f, code lost:
        if (r4.equals("weather") == false) goto L_0x0077;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> b(java.lang.String r4) {
        /*
            r3 = this;
            java.lang.String r0 = "watchAppId"
            com.fossil.ee7.b(r4, r0)
            int r0 = r4.hashCode()
            r1 = -829740640(0xffffffffce8b29a0, float:-1.16738048E9)
            if (r0 == r1) goto L_0x0035
            r1 = 104263205(0x636ee25, float:3.4405356E-35)
            if (r0 == r1) goto L_0x0022
            r1 = 1223440372(0x48ec37f4, float:483775.62)
            if (r0 == r1) goto L_0x0019
            goto L_0x0077
        L_0x0019:
            java.lang.String r0 = "weather"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0077
            goto L_0x003d
        L_0x0022:
            java.lang.String r0 = "music"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0077
            java.lang.String r4 = "NOTIFICATION_ACCESS"
            java.lang.String[] r4 = new java.lang.String[]{r4}
            java.util.ArrayList r4 = com.fossil.w97.a(r4)
            goto L_0x007c
        L_0x0035:
            java.lang.String r0 = "commute-time"
            boolean r4 = r4.equals(r0)
            if (r4 == 0) goto L_0x0077
        L_0x003d:
            int r4 = android.os.Build.VERSION.SDK_INT
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "android.os.Build.VERSION.SDK_INT="
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "WatchAppHelper"
            r0.d(r2, r1)
            r0 = 29
            java.lang.String r1 = "LOCATION_SERVICE"
            java.lang.String r2 = "ACCESS_FINE_LOCATION"
            if (r4 < r0) goto L_0x006e
            java.lang.String r4 = "ACCESS_BACKGROUND_LOCATION"
            java.lang.String[] r4 = new java.lang.String[]{r2, r1, r4}
            java.util.ArrayList r4 = com.fossil.w97.a(r4)
            goto L_0x007c
        L_0x006e:
            java.lang.String[] r4 = new java.lang.String[]{r2, r1}
            java.util.ArrayList r4 = com.fossil.w97.a(r4)
            goto L_0x007c
        L_0x0077:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
        L_0x007c:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.cf5.b(java.lang.String):java.util.List");
    }

    @DexIgnore
    public final Integer c(String str) {
        ee7.b(str, "watchAppId");
        return (str.hashCode() == -829740640 && str.equals("commute-time")) ? 2131820544 : null;
    }

    @DexIgnore
    public final boolean d(String str) {
        ee7.b(str, "watchAppId");
        List<String> b2 = b(str);
        String[] a2 = px6.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppHelper", "isPermissionGrantedForWatchApp " + str + " granted=" + a2 + " required=" + b2);
        Iterator<T> it = b2.iterator();
        while (it.hasNext()) {
            if (!t97.a((Object[]) a2, (Object) it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public final boolean e(String str) {
        ee7.b(str, "watchAppId");
        return b.contains(str);
    }

    @DexIgnore
    public final boolean f(String str) {
        ee7.b(str, "watchAppId");
        return a.contains(str);
    }

    @DexIgnore
    public final WatchApp a() {
        return new WatchApp("empty_watch_app_id", "", "", "", "", w97.a((Object[]) new String[]{"category_all"}), "", "", "");
    }
}
