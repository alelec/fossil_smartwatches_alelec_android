package com.fossil;

import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.LegacyTokenHelper;
import java.util.Arrays;
import java.util.MissingFormatArgumentException;
import org.json.JSONArray;
import org.json.JSONException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dd4 {
    @DexIgnore
    public /* final */ Bundle a;

    @DexIgnore
    public dd4(Bundle bundle) {
        if (bundle != null) {
            this.a = new Bundle(bundle);
            return;
        }
        throw new NullPointerException("data");
    }

    @DexIgnore
    public static boolean k(String str) {
        return str.startsWith("google.c.") || str.startsWith("gcm.n.") || str.startsWith("gcm.notification.");
    }

    @DexIgnore
    public static String l(String str) {
        if (!str.startsWith("gcm.n.")) {
            return str;
        }
        return str.replace("gcm.n.", "gcm.notification.");
    }

    @DexIgnore
    public static String m(String str) {
        return str.startsWith("gcm.n.") ? str.substring(6) : str;
    }

    @DexIgnore
    public boolean a(String str) {
        String g = g(str);
        return "1".equals(g) || Boolean.parseBoolean(g);
    }

    @DexIgnore
    public Integer b(String str) {
        String g = g(str);
        if (TextUtils.isEmpty(g)) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(g));
        } catch (NumberFormatException unused) {
            String m = m(str);
            StringBuilder sb = new StringBuilder(String.valueOf(m).length() + 38 + String.valueOf(g).length());
            sb.append("Couldn't parse value of ");
            sb.append(m);
            sb.append("(");
            sb.append(g);
            sb.append(") into an int");
            Log.w("NotificationParams", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public JSONArray c(String str) {
        String g = g(str);
        if (TextUtils.isEmpty(g)) {
            return null;
        }
        try {
            return new JSONArray(g);
        } catch (JSONException unused) {
            String m = m(str);
            StringBuilder sb = new StringBuilder(String.valueOf(m).length() + 50 + String.valueOf(g).length());
            sb.append("Malformed JSON for key ");
            sb.append(m);
            sb.append(": ");
            sb.append(g);
            sb.append(", falling back to default");
            Log.w("NotificationParams", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public Integer d() {
        Integer b = b("gcm.n.notification_count");
        if (b == null) {
            return null;
        }
        if (b.intValue() >= 0) {
            return b;
        }
        String valueOf = String.valueOf(b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
        sb.append("notificationCount is invalid: ");
        sb.append(valueOf);
        sb.append(". Skipping setting notificationCount.");
        Log.w("FirebaseMessaging", sb.toString());
        return null;
    }

    @DexIgnore
    public Integer e() {
        Integer b = b("gcm.n.notification_priority");
        if (b == null) {
            return null;
        }
        if (b.intValue() >= -2 && b.intValue() <= 2) {
            return b;
        }
        String valueOf = String.valueOf(b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 72);
        sb.append("notificationPriority is invalid ");
        sb.append(valueOf);
        sb.append(". Skipping setting notificationPriority.");
        Log.w("FirebaseMessaging", sb.toString());
        return null;
    }

    @DexIgnore
    public Long f(String str) {
        String g = g(str);
        if (TextUtils.isEmpty(g)) {
            return null;
        }
        try {
            return Long.valueOf(Long.parseLong(g));
        } catch (NumberFormatException unused) {
            String m = m(str);
            StringBuilder sb = new StringBuilder(String.valueOf(m).length() + 38 + String.valueOf(g).length());
            sb.append("Couldn't parse value of ");
            sb.append(m);
            sb.append("(");
            sb.append(g);
            sb.append(") into a long");
            Log.w("NotificationParams", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public String g(String str) {
        return this.a.getString(h(str));
    }

    @DexIgnore
    public Integer h() {
        Integer b = b("gcm.n.visibility");
        if (b == null) {
            return null;
        }
        if (b.intValue() >= -1 && b.intValue() <= 1) {
            return b;
        }
        String valueOf = String.valueOf(b);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 53);
        sb.append("visibility is invalid: ");
        sb.append(valueOf);
        sb.append(". Skipping setting visibility.");
        Log.w("NotificationParams", sb.toString());
        return null;
    }

    @DexIgnore
    public Bundle i() {
        Bundle bundle = new Bundle(this.a);
        for (String str : this.a.keySet()) {
            if (!j(str)) {
                bundle.remove(str);
            }
        }
        return bundle;
    }

    @DexIgnore
    public Bundle j() {
        Bundle bundle = new Bundle(this.a);
        for (String str : this.a.keySet()) {
            if (k(str)) {
                bundle.remove(str);
            }
        }
        return bundle;
    }

    @DexIgnore
    public long[] g() {
        JSONArray c = c("gcm.n.vibrate_timings");
        if (c == null) {
            return null;
        }
        try {
            if (c.length() > 1) {
                int length = c.length();
                long[] jArr = new long[length];
                for (int i = 0; i < length; i++) {
                    jArr[i] = c.optLong(i);
                }
                return jArr;
            }
            throw new JSONException("vibrateTimings have invalid length");
        } catch (NumberFormatException | JSONException unused) {
            String valueOf = String.valueOf(c);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 74);
            sb.append("User defined vibrateTimings is invalid: ");
            sb.append(valueOf);
            sb.append(". Skipping setting vibrateTimings.");
            Log.w("NotificationParams", sb.toString());
            return null;
        }
    }

    @DexIgnore
    public int[] a() {
        JSONArray c = c("gcm.n.light_settings");
        if (c == null) {
            return null;
        }
        int[] iArr = new int[3];
        try {
            if (c.length() == 3) {
                iArr[0] = i(c.optString(0));
                iArr[1] = c.optInt(1);
                iArr[2] = c.optInt(2);
                return iArr;
            }
            throw new JSONException("lightSettings don't have all three fields");
        } catch (JSONException unused) {
            String valueOf = String.valueOf(c);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 58);
            sb.append("LightSettings is invalid: ");
            sb.append(valueOf);
            sb.append(". Skipping setting LightSettings");
            Log.w("NotificationParams", sb.toString());
            return null;
        } catch (IllegalArgumentException e) {
            String valueOf2 = String.valueOf(c);
            String message = e.getMessage();
            StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 60 + String.valueOf(message).length());
            sb2.append("LightSettings is invalid: ");
            sb2.append(valueOf2);
            sb2.append(". ");
            sb2.append(message);
            sb2.append(". Skipping setting LightSettings");
            Log.w("NotificationParams", sb2.toString());
            return null;
        }
    }

    @DexIgnore
    public Object[] d(String str) {
        String valueOf = String.valueOf(str);
        JSONArray c = c("_loc_args".length() != 0 ? valueOf.concat("_loc_args") : new String(valueOf));
        if (c == null) {
            return null;
        }
        int length = c.length();
        String[] strArr = new String[length];
        for (int i = 0; i < length; i++) {
            strArr[i] = c.optString(i);
        }
        return strArr;
    }

    @DexIgnore
    public final String h(String str) {
        if (!this.a.containsKey(str) && str.startsWith("gcm.n.")) {
            String l = l(str);
            if (this.a.containsKey(l)) {
                return l;
            }
        }
        return str;
    }

    @DexIgnore
    public static int i(String str) {
        int parseColor = Color.parseColor(str);
        if (parseColor != -16777216) {
            return parseColor;
        }
        throw new IllegalArgumentException("Transparent color is invalid");
    }

    @DexIgnore
    public static boolean j(String str) {
        return str.startsWith("google.c.a.") || str.equals("from");
    }

    @DexIgnore
    public String e(String str) {
        String valueOf = String.valueOf(str);
        return g("_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
    }

    @DexIgnore
    public Uri b() {
        String g = g("gcm.n.link_android");
        if (TextUtils.isEmpty(g)) {
            g = g("gcm.n.link");
        }
        if (!TextUtils.isEmpty(g)) {
            return Uri.parse(g);
        }
        return null;
    }

    @DexIgnore
    public String c() {
        return g("gcm.n.android_channel_id");
    }

    @DexIgnore
    public String f() {
        String g = g("gcm.n.sound2");
        return TextUtils.isEmpty(g) ? g("gcm.n.sound") : g;
    }

    @DexIgnore
    public String b(Resources resources, String str, String str2) {
        String g = g(str2);
        if (!TextUtils.isEmpty(g)) {
            return g;
        }
        return a(resources, str, str2);
    }

    @DexIgnore
    public String a(Resources resources, String str, String str2) {
        String e = e(str2);
        if (TextUtils.isEmpty(e)) {
            return null;
        }
        int identifier = resources.getIdentifier(e, LegacyTokenHelper.TYPE_STRING, str);
        if (identifier == 0) {
            String valueOf = String.valueOf(str2);
            String m = m("_loc_key".length() != 0 ? valueOf.concat("_loc_key") : new String(valueOf));
            StringBuilder sb = new StringBuilder(String.valueOf(m).length() + 49 + String.valueOf(str2).length());
            sb.append(m);
            sb.append(" resource not found: ");
            sb.append(str2);
            sb.append(" Default value will be used.");
            Log.w("NotificationParams", sb.toString());
            return null;
        }
        Object[] d = d(str2);
        if (d == null) {
            return resources.getString(identifier);
        }
        try {
            return resources.getString(identifier, d);
        } catch (MissingFormatArgumentException e2) {
            String m2 = m(str2);
            String arrays = Arrays.toString(d);
            StringBuilder sb2 = new StringBuilder(String.valueOf(m2).length() + 58 + String.valueOf(arrays).length());
            sb2.append("Missing format argument for ");
            sb2.append(m2);
            sb2.append(": ");
            sb2.append(arrays);
            sb2.append(" Default value will be used.");
            Log.w("NotificationParams", sb2.toString(), e2);
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Bundle bundle) {
        return "1".equals(bundle.getString("gcm.n.e")) || "1".equals(bundle.getString(l("gcm.n.e")));
    }
}
