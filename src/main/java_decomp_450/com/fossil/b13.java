package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b13 implements tr2<a13> {
    @DexIgnore
    public static b13 b; // = new b13();
    @DexIgnore
    public /* final */ tr2<a13> a;

    @DexIgnore
    public b13(tr2<a13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((a13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((a13) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((a13) b.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((a13) b.zza()).zzd();
    }

    @DexIgnore
    public static boolean e() {
        return ((a13) b.zza()).zze();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ a13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public b13() {
        this(sr2.a(new d13()));
    }
}
