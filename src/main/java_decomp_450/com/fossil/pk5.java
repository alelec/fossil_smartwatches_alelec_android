package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.util.SparseArray;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.data.NotificationSource;
import com.portfolio.platform.data.model.NotificationInfo;
import java.util.concurrent.ConcurrentLinkedQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk5 {
    @DexIgnore
    public /* final */ SparseArray<c> a; // = new SparseArray<>();
    @DexIgnore
    public /* final */ Handler b; // = new Handler();
    @DexIgnore
    public b c; // = new b(0, null, 0, 7, null);
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ ConcurrentLinkedQueue<c> f; // = new ConcurrentLinkedQueue<>();
    @DexIgnore
    public /* final */ String[] g;
    @DexIgnore
    public String h; // = "date DESC LIMIT 1";
    @DexIgnore
    public boolean i;
    @DexIgnore
    public /* final */ Runnable j; // = new e(this);
    @DexIgnore
    public /* final */ Runnable k; // = new f(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public long c;

        @DexIgnore
        public b() {
            this(0, null, 0, 7, null);
        }

        @DexIgnore
        public b(long j, String str, long j2) {
            ee7.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = j;
            this.b = str;
            this.c = j2;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final void a(long j) {
            this.c = j;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final long c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && ee7.a(this.b, bVar.b) && this.c == bVar.c;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = c.a(this.a) * 31;
            String str = this.b;
            return ((a2 + (str != null ? str.hashCode() : 0)) * 31) + c.a(this.c);
        }

        @DexIgnore
        public String toString() {
            return "InboxMessage(id=" + this.a + ", sender=" + this.b + ", dateSent=" + this.c + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(long j, String str, long j2, int i, zd7 zd7) {
            this((i & 1) != 0 ? 0 : j, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? 0 : j2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public c(String str, String str2, boolean z) {
            ee7.b(str, "packageName");
            ee7.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            this.a = str;
            this.b = str2;
            this.c = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return ee7.a(this.a, cVar.a) && ee7.a(this.b, cVar.b) && this.c == cVar.c;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.a;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            String str2 = this.b;
            if (str2 != null) {
                i = str2.hashCode();
            }
            int i2 = (hashCode + i) * 31;
            boolean z = this.c;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return i2 + i3;
        }

        @DexIgnore
        public String toString() {
            return "MessageNotification(packageName=" + this.a + ", sender=" + this.b + ", shouldCheckAppAndAllText=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.notification.HybridMessageNotificationComponent$handleNotificationPosted$1", f = "HybridMessageNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ pk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(pk5 pk5, StatusBarNotification statusBarNotification, fb7 fb7) {
            super(2, fb7);
            this.this$0 = pk5;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$sbn, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:42:0x010a A[SYNTHETIC, Splitter:B:42:0x010a] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                com.fossil.nb7.a()
                int r0 = r14.label
                if (r0 != 0) goto L_0x0178
                com.fossil.t87.a(r15)
                com.fossil.pk5 r15 = r14.this$0
                android.service.notification.StatusBarNotification r0 = r14.$sbn
                boolean r15 = r15.b(r0)
                if (r15 == 0) goto L_0x0175
                com.fossil.pk5 r15 = r14.this$0
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                android.content.Context r0 = r0.getApplicationContext()
                java.lang.String r1 = "PortfolioApp.instance.applicationContext"
                com.fossil.ee7.a(r0, r1)
                com.fossil.pk5 r2 = r14.this$0
                java.lang.String r2 = r2.d
                com.fossil.pk5$b r15 = r15.b(r0, r2)
                java.lang.String r0 = "sbn.packageName"
                java.lang.String r2 = "null cannot be cast to non-null type kotlin.CharSequence"
                r3 = 0
                r4 = 1
                if (r15 == 0) goto L_0x00e7
                java.lang.String r5 = r15.a()
                long r6 = r15.b()
                boolean r8 = android.text.TextUtils.isEmpty(r5)
                if (r8 != 0) goto L_0x00e7
                com.fossil.pk5 r8 = r14.this$0
                com.fossil.pk5$b r8 = r8.c
                long r8 = r8.c()
                int r10 = (r8 > r6 ? 1 : (r8 == r6 ? 0 : -1))
                if (r10 >= 0) goto L_0x00e7
                java.lang.String r6 = " "
                java.lang.String[] r6 = new java.lang.String[]{r6}
                r7 = 0
                r8 = 0
                r9 = 6
                r10 = 0
                java.util.List r5 = com.fossil.nh7.a(r5, r6, r7, r8, r9, r10)
                java.util.Iterator r5 = r5.iterator()
                r6 = 0
            L_0x0066:
                boolean r7 = r5.hasNext()
                if (r7 == 0) goto L_0x00e8
                java.lang.Object r7 = r5.next()
                java.lang.String r7 = (java.lang.String) r7
                if (r7 == 0) goto L_0x00e1
                java.lang.CharSequence r7 = com.fossil.nh7.d(r7)
                java.lang.String r7 = r7.toString()
                java.lang.Long r7 = com.fossil.lh7.c(r7)
                if (r7 == 0) goto L_0x0066
                long r7 = r7.longValue()
                com.fossil.pk5 r9 = r14.this$0
                com.portfolio.platform.PortfolioApp$a r10 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r10 = r10.c()
                android.content.Context r10 = r10.getApplicationContext()
                com.fossil.ee7.a(r10, r1)
                java.util.List r7 = r9.a(r10, r7)
                java.util.Iterator r8 = r7.iterator()
                r9 = 0
            L_0x009e:
                boolean r10 = r8.hasNext()
                if (r10 == 0) goto L_0x0066
                java.lang.Object r10 = r8.next()
                int r11 = r9 + 1
                if (r9 < 0) goto L_0x00dc
                java.lang.Integer r9 = com.fossil.pb7.a(r9)
                java.lang.String r10 = (java.lang.String) r10
                int r9 = r9.intValue()
                boolean r12 = android.text.TextUtils.isEmpty(r10)
                if (r12 != 0) goto L_0x00da
                com.fossil.pk5 r6 = r14.this$0
                r6.c = r15
                com.fossil.pk5 r6 = r14.this$0
                android.service.notification.StatusBarNotification r12 = r14.$sbn
                java.lang.String r12 = r12.getPackageName()
                com.fossil.ee7.a(r12, r0)
                int r13 = r7.size()
                int r13 = r13 - r4
                if (r9 != r13) goto L_0x00d5
                r9 = 1
                goto L_0x00d6
            L_0x00d5:
                r9 = 0
            L_0x00d6:
                r6.a(r10, r12, r9)
                r6 = 1
            L_0x00da:
                r9 = r11
                goto L_0x009e
            L_0x00dc:
                com.fossil.w97.c()
                r15 = 0
                throw r15
            L_0x00e1:
                com.fossil.x87 r15 = new com.fossil.x87
                r15.<init>(r2)
                throw r15
            L_0x00e7:
                r6 = 0
            L_0x00e8:
                if (r6 != 0) goto L_0x0175
                android.service.notification.StatusBarNotification r15 = r14.$sbn
                java.lang.String r15 = r15.getTag()
                if (r15 == 0) goto L_0x0175
                java.lang.String r1 = "one_to_one"
                boolean r15 = com.fossil.nh7.a(r15, r1, r4)
                if (r15 == 0) goto L_0x0175
                android.service.notification.StatusBarNotification r15 = r14.$sbn
                java.lang.String r15 = r15.getPackageName()
                android.service.notification.StatusBarNotification r1 = r14.$sbn
                android.app.Notification r1 = r1.getNotification()
                java.lang.CharSequence r1 = r1.tickerText
                if (r1 == 0) goto L_0x0162
                int r5 = r1.length()     // Catch:{ Exception -> 0x0152 }
                r6 = 0
            L_0x010f:
                r7 = -1
                if (r6 >= r5) goto L_0x0136
                char r8 = r1.charAt(r6)     // Catch:{ Exception -> 0x0152 }
                java.lang.Character r8 = com.fossil.pb7.a(r8)     // Catch:{ Exception -> 0x0152 }
                char r8 = r8.charValue()     // Catch:{ Exception -> 0x0152 }
                java.lang.String r8 = java.lang.String.valueOf(r8)     // Catch:{ Exception -> 0x0152 }
                java.lang.String r9 = ":"
                boolean r8 = com.fossil.ee7.a(r8, r9)     // Catch:{ Exception -> 0x0152 }
                java.lang.Boolean r8 = com.fossil.pb7.a(r8)     // Catch:{ Exception -> 0x0152 }
                boolean r8 = r8.booleanValue()     // Catch:{ Exception -> 0x0152 }
                if (r8 == 0) goto L_0x0133
                goto L_0x0137
            L_0x0133:
                int r6 = r6 + 1
                goto L_0x010f
            L_0x0136:
                r6 = -1
            L_0x0137:
                if (r6 == r7) goto L_0x0162
                java.lang.CharSequence r1 = r1.subSequence(r3, r6)     // Catch:{ Exception -> 0x0152 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0152 }
                if (r1 == 0) goto L_0x014c
                java.lang.CharSequence r1 = com.fossil.nh7.d(r1)     // Catch:{ Exception -> 0x0152 }
                java.lang.String r15 = r1.toString()     // Catch:{ Exception -> 0x0152 }
                goto L_0x0162
            L_0x014c:
                com.fossil.x87 r1 = new com.fossil.x87     // Catch:{ Exception -> 0x0152 }
                r1.<init>(r2)     // Catch:{ Exception -> 0x0152 }
                throw r1     // Catch:{ Exception -> 0x0152 }
            L_0x0152:
                r1 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.String r1 = r1.getMessage()
                java.lang.String r3 = "HybridMessageNotificationComponent"
                r2.d(r3, r1)
            L_0x0162:
                com.fossil.pk5 r1 = r14.this$0
                java.lang.String r2 = "contact"
                com.fossil.ee7.a(r15, r2)
                android.service.notification.StatusBarNotification r2 = r14.$sbn
                java.lang.String r2 = r2.getPackageName()
                com.fossil.ee7.a(r2, r0)
                r1.a(r15, r2, r4)
            L_0x0175:
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
            L_0x0178:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk5.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ pk5 a;

        @DexIgnore
        public e(pk5 pk5) {
            this.a = pk5;
        }

        @DexIgnore
        public final void run() {
            if (this.a.a.size() == 0) {
                this.a.d();
                return;
            }
            this.a.a.clear();
            this.a.b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ pk5 a;

        @DexIgnore
        public f(pk5 pk5) {
            this.a = pk5;
        }

        @DexIgnore
        public final void run() {
            if (this.a.f.isEmpty()) {
                this.a.e();
                return;
            }
            this.a.a();
            this.a.c();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public pk5() {
        String str = TextUtils.equals(Build.MANUFACTURER, "samsung") ? "display_recipient_ids" : "recipient_ids";
        this.d = str;
        this.g = new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "date", str};
        this.c.a(System.currentTimeMillis());
    }

    @DexIgnore
    public final void b() {
        d();
        this.b.postDelayed(this.j, ButtonService.CONNECT_TIMEOUT);
    }

    @DexIgnore
    public final void c() {
        this.i = true;
        this.e.postDelayed(this.k, 500);
    }

    @DexIgnore
    public final void d() {
        this.b.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final void e() {
        this.i = false;
        this.e.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    public final boolean b(StatusBarNotification statusBarNotification) {
        String tag = statusBarNotification.getTag();
        if (tag == null || (!nh7.a((CharSequence) tag, (CharSequence) "sms", true) && !nh7.a((CharSequence) tag, (CharSequence) "mms", true) && !nh7.a((CharSequence) tag, (CharSequence) "rcs", true))) {
            return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
        }
        return true;
    }

    @DexIgnore
    public final ik7 a(StatusBarNotification statusBarNotification) {
        ee7.b(statusBarNotification, "sbn");
        return xh7.b(zi7.a(qj7.a()), null, null, new d(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d("HybridMessageNotificationComponent", "processMessage()");
        c poll = this.f.poll();
        if (poll != null) {
            sg5.i.a().a(new NotificationInfo(NotificationSource.TEXT, poll.b(), "", poll.a()), poll.c());
            this.a.put(poll.hashCode(), poll);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0056, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0057, code lost:
        com.fossil.hc7.a(r8, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x005a, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.pk5.b b(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = this;
            r0 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ Exception -> 0x005b }
            java.lang.String r8 = "content://mms-sms/conversations?simple=true"
            android.net.Uri r2 = android.net.Uri.parse(r8)     // Catch:{ Exception -> 0x005b }
            java.lang.String[] r3 = r7.g     // Catch:{ Exception -> 0x005b }
            r4 = 0
            r5 = 0
            java.lang.String r6 = r7.h     // Catch:{ Exception -> 0x005b }
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x005b }
            if (r8 == 0) goto L_0x006b
            boolean r1 = r8.moveToFirst()     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x004b
            int r9 = r8.getColumnIndex(r9)     // Catch:{ all -> 0x0054 }
            java.lang.String r4 = r8.getString(r9)     // Catch:{ all -> 0x0054 }
            java.lang.String r9 = "_id"
            int r9 = r8.getColumnIndex(r9)     // Catch:{ all -> 0x0054 }
            long r2 = r8.getLong(r9)     // Catch:{ all -> 0x0054 }
            java.lang.String r9 = "date"
            int r9 = r8.getColumnIndex(r9)     // Catch:{ all -> 0x0054 }
            long r5 = r8.getLong(r9)     // Catch:{ all -> 0x0054 }
            r8.close()     // Catch:{ all -> 0x0054 }
            com.fossil.pk5$b r9 = new com.fossil.pk5$b     // Catch:{ all -> 0x0054 }
            java.lang.String r1 = "recipientIds"
            com.fossil.ee7.a(r4, r1)     // Catch:{ all -> 0x0054 }
            r1 = r9
            r1.<init>(r2, r4, r5)     // Catch:{ all -> 0x0054 }
            com.fossil.hc7.a(r8, r0)
            return r9
        L_0x004b:
            r8.close()
            com.fossil.i97 r9 = com.fossil.i97.a
            com.fossil.hc7.a(r8, r0)
            goto L_0x006b
        L_0x0054:
            r9 = move-exception
            throw r9     // Catch:{ all -> 0x0056 }
        L_0x0056:
            r1 = move-exception
            com.fossil.hc7.a(r8, r9)
            throw r1
        L_0x005b:
            r8 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r8 = r8.getMessage()
            java.lang.String r1 = "HybridMessageNotificationComponent"
            r9.e(r1, r8)
        L_0x006b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk5.b(android.content.Context, java.lang.String):com.fossil.pk5$b");
    }

    @DexIgnore
    public final void a(String str, String str2, boolean z) {
        if (this.a.size() == 0) {
            b();
        }
        c cVar = new c(str2, str, z);
        if (this.a.indexOfKey(cVar.hashCode()) < 0) {
            this.f.offer(cVar);
            if (!this.i) {
                c();
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0049, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004a, code lost:
        com.fossil.hc7.a(r11, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004d, code lost:
        throw r12;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> a(android.content.Context r10, long r11) {
        /*
            r9 = this;
            java.lang.String r0 = "address"
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r2 = "content://mms-sms/canonical-address"
            android.net.Uri r2 = android.net.Uri.parse(r2)
            android.net.Uri r4 = android.content.ContentUris.withAppendedId(r2, r11)
            android.content.ContentResolver r3 = r10.getContentResolver()     // Catch:{ Exception -> 0x004e }
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r11 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x004e }
            if (r11 == 0) goto L_0x005e
            r12 = 0
            boolean r2 = r11.moveToFirst()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0041
            int r2 = r11.getColumnIndex(r0)     // Catch:{ all -> 0x0047 }
            java.lang.String r2 = r11.getString(r2)     // Catch:{ all -> 0x0047 }
            boolean r3 = android.text.TextUtils.isEmpty(r2)     // Catch:{ all -> 0x0047 }
            if (r3 != 0) goto L_0x003e
            com.fossil.ee7.a(r2, r0)     // Catch:{ all -> 0x0047 }
            java.util.List r10 = r9.a(r10, r2)     // Catch:{ all -> 0x0047 }
            r1.addAll(r10)     // Catch:{ all -> 0x0047 }
        L_0x003e:
            r11.close()     // Catch:{ all -> 0x0047 }
        L_0x0041:
            com.fossil.i97 r10 = com.fossil.i97.a     // Catch:{ all -> 0x0047 }
            com.fossil.hc7.a(r11, r12)
            goto L_0x005e
        L_0x0047:
            r10 = move-exception
            throw r10     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r12 = move-exception
            com.fossil.hc7.a(r11, r10)
            throw r12
        L_0x004e:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r10 = r10.getMessage()
            java.lang.String r12 = "HybridMessageNotificationComponent"
            r11.e(r12, r10)
        L_0x005e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk5.a(android.content.Context, long):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005c, code lost:
        com.fossil.hc7.a(r10, r11);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x005f, code lost:
        throw r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> a(android.content.Context r10, java.lang.String r11) {
        /*
            r9 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            android.net.Uri r1 = android.provider.ContactsContract.PhoneLookup.CONTENT_FILTER_URI
            java.lang.String r2 = android.net.Uri.encode(r11)
            android.net.Uri r4 = android.net.Uri.withAppendedPath(r1, r2)
            java.lang.String r1 = "normalized_number"
            java.lang.String r2 = "display_name"
            java.lang.String[] r5 = new java.lang.String[]{r2, r1}
            android.content.ContentResolver r3 = r10.getContentResolver()     // Catch:{ Exception -> 0x0060 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r10 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0060 }
            if (r10 == 0) goto L_0x0070
            r3 = 0
            boolean r4 = r10.moveToFirst()     // Catch:{ all -> 0x0059 }
            if (r4 == 0) goto L_0x004d
        L_0x002b:
            int r11 = r10.getColumnIndex(r1)     // Catch:{ all -> 0x0059 }
            java.lang.String r11 = r10.getString(r11)     // Catch:{ all -> 0x0059 }
            int r4 = r10.getColumnIndex(r2)     // Catch:{ all -> 0x0059 }
            java.lang.String r4 = r10.getString(r4)     // Catch:{ all -> 0x0059 }
            if (r4 == 0) goto L_0x003e
            r11 = r4
        L_0x003e:
            java.lang.String r4 = "name"
            com.fossil.ee7.a(r11, r4)     // Catch:{ all -> 0x0059 }
            r0.add(r11)     // Catch:{ all -> 0x0059 }
            boolean r11 = r10.moveToNext()     // Catch:{ all -> 0x0059 }
            if (r11 != 0) goto L_0x002b
            goto L_0x0050
        L_0x004d:
            r0.add(r11)     // Catch:{ all -> 0x0059 }
        L_0x0050:
            r10.close()     // Catch:{ all -> 0x0059 }
            com.fossil.i97 r11 = com.fossil.i97.a     // Catch:{ all -> 0x0059 }
            com.fossil.hc7.a(r10, r3)
            goto L_0x0070
        L_0x0059:
            r11 = move-exception
            throw r11     // Catch:{ all -> 0x005b }
        L_0x005b:
            r1 = move-exception
            com.fossil.hc7.a(r10, r11)
            throw r1
        L_0x0060:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r10 = r10.getMessage()
            java.lang.String r1 = "HybridMessageNotificationComponent"
            r11.e(r1, r10)
        L_0x0070:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pk5.a(android.content.Context, java.lang.String):java.util.List");
    }
}
