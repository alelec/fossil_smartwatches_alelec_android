package com.fossil;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d83 extends wm2 implements c83 {
    @DexIgnore
    public d83() {
        super("com.google.android.gms.maps.internal.ISnapshotReadyCallback");
    }

    @DexIgnore
    @Override // com.fossil.wm2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i == 1) {
            onSnapshotReady((Bitmap) xm2.a(parcel, Bitmap.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            c(ab2.a.a(parcel.readStrongBinder()));
        }
        parcel2.writeNoException();
        return true;
    }
}
