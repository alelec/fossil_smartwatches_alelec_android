package com.fossil;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.fossil.v54;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a44 {
    @DexIgnore
    public static /* final */ String e; // = String.format(Locale.US, "Crashlytics Android SDK/%s", "17.1.1");
    @DexIgnore
    public static /* final */ Map<String, Integer> f;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ j44 b;
    @DexIgnore
    public /* final */ n34 c;
    @DexIgnore
    public /* final */ m84 d;

    /*
    static {
        HashMap hashMap = new HashMap();
        f = hashMap;
        hashMap.put("armeabi", 5);
        f.put("armeabi-v7a", 6);
        f.put("arm64-v8a", 9);
        f.put("x86", 0);
        f.put("x86_64", 1);
    }
    */

    @DexIgnore
    public a44(Context context, j44 j44, n34 n34, m84 m84) {
        this.a = context;
        this.b = j44;
        this.c = n34;
        this.d = m84;
    }

    @DexIgnore
    public static int h() {
        Integer num;
        String str = Build.CPU_ABI;
        if (!TextUtils.isEmpty(str) && (num = f.get(str.toLowerCase(Locale.US))) != null) {
            return num.intValue();
        }
        return 7;
    }

    @DexIgnore
    public v54 a(String str, long j) {
        v54.a a2 = a();
        a2.a(b(str, j));
        return a2.a();
    }

    @DexIgnore
    public final v54.d b(String str, long j) {
        v54.d.b n = v54.d.n();
        n.a(j);
        n.b(str);
        n.a(e);
        n.a(d());
        n.a(f());
        n.a(e());
        n.a(3);
        return n.a();
    }

    @DexIgnore
    public final w54<v54.d.AbstractC0206d.a.b.AbstractC0208a> c() {
        return w54.a(b());
    }

    @DexIgnore
    public final v54.d.a d() {
        v54.d.a.AbstractC0205a f2 = v54.d.a.f();
        f2.b(this.b.b());
        f2.d(this.c.e);
        f2.a(this.c.f);
        f2.c(this.b.a());
        return f2.a();
    }

    @DexIgnore
    public final v54.d.c e() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        int h = h();
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        long b2 = t34.b();
        long blockCount = ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
        boolean j = t34.j(this.a);
        int c2 = t34.c(this.a);
        String str = Build.MANUFACTURER;
        String str2 = Build.PRODUCT;
        v54.d.c.a j2 = v54.d.c.j();
        j2.a(h);
        j2.b(Build.MODEL);
        j2.b(availableProcessors);
        j2.b(b2);
        j2.a(blockCount);
        j2.a(j);
        j2.c(c2);
        j2.a(str);
        j2.c(str2);
        return j2.a();
    }

    @DexIgnore
    public final v54.d.e f() {
        v54.d.e.a e2 = v54.d.e.e();
        e2.a(3);
        e2.b(Build.VERSION.RELEASE);
        e2.a(Build.VERSION.CODENAME);
        e2.a(t34.k(this.a));
        return e2.a();
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.AbstractC0212d g() {
        v54.d.AbstractC0206d.a.b.AbstractC0212d.AbstractC0213a d2 = v54.d.AbstractC0206d.a.b.AbstractC0212d.d();
        d2.b("0");
        d2.a("0");
        d2.a(0);
        return d2.a();
    }

    @DexIgnore
    public v54.d.AbstractC0206d a(Throwable th, Thread thread, String str, long j, int i, int i2, boolean z) {
        int i3 = this.a.getResources().getConfiguration().orientation;
        n84 n84 = new n84(th, this.d);
        v54.d.AbstractC0206d.b g = v54.d.AbstractC0206d.g();
        g.a(str);
        g.a(j);
        g.a(a(i3, n84, thread, i, i2, z));
        g.a(a(i3));
        return g.a();
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.AbstractC0208a b() {
        v54.d.AbstractC0206d.a.b.AbstractC0208a.AbstractC0209a f2 = v54.d.AbstractC0206d.a.b.AbstractC0208a.f();
        f2.a(0);
        f2.b(0);
        f2.a(this.c.d);
        f2.b(this.c.b);
        return f2.a();
    }

    @DexIgnore
    public final v54.a a() {
        v54.a l = v54.l();
        l.e("17.1.1");
        l.c(this.c.a);
        l.d(this.b.a());
        l.a(this.c.e);
        l.b(this.c.f);
        l.a(4);
        return l;
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a a(int i, n84 n84, Thread thread, int i2, int i3, boolean z) {
        Boolean bool;
        ActivityManager.RunningAppProcessInfo a2 = t34.a(this.c.d, this.a);
        if (a2 != null) {
            bool = Boolean.valueOf(a2.importance != 100);
        } else {
            bool = null;
        }
        v54.d.AbstractC0206d.a.AbstractC0207a f2 = v54.d.AbstractC0206d.a.f();
        f2.a(bool);
        f2.a(i);
        f2.a(a(n84, thread, i2, i3, z));
        return f2.a();
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.c a(int i) {
        q34 a2 = q34.a(this.a);
        Float a3 = a2.a();
        Double valueOf = a3 != null ? Double.valueOf(a3.doubleValue()) : null;
        int b2 = a2.b();
        boolean f2 = t34.f(this.a);
        long b3 = t34.b() - t34.a(this.a);
        long a4 = t34.a(Environment.getDataDirectory().getPath());
        v54.d.AbstractC0206d.c.a g = v54.d.AbstractC0206d.c.g();
        g.a(valueOf);
        g.a(b2);
        g.a(f2);
        g.b(i);
        g.b(b3);
        g.a(a4);
        return g.a();
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b a(n84 n84, Thread thread, int i, int i2, boolean z) {
        v54.d.AbstractC0206d.a.b.AbstractC0210b e2 = v54.d.AbstractC0206d.a.b.e();
        e2.b(a(n84, thread, i, z));
        e2.a(a(n84, i, i2));
        e2.a(g());
        e2.a(c());
        return e2.a();
    }

    @DexIgnore
    public final w54<v54.d.AbstractC0206d.a.b.e> a(n84 n84, Thread thread, int i, boolean z) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(a(thread, n84.c, i));
        if (z) {
            for (Map.Entry<Thread, StackTraceElement[]> entry : Thread.getAllStackTraces().entrySet()) {
                Thread key = entry.getKey();
                if (!key.equals(thread)) {
                    arrayList.add(a(key, this.d.a(entry.getValue())));
                }
            }
        }
        return w54.a(arrayList);
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.e a(Thread thread, StackTraceElement[] stackTraceElementArr) {
        return a(thread, stackTraceElementArr, 0);
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.e a(Thread thread, StackTraceElement[] stackTraceElementArr, int i) {
        v54.d.AbstractC0206d.a.b.e.AbstractC0214a d2 = v54.d.AbstractC0206d.a.b.e.d();
        d2.a(thread.getName());
        d2.a(i);
        d2.a(w54.a(a(stackTraceElementArr, i)));
        return d2.a();
    }

    @DexIgnore
    public final w54<v54.d.AbstractC0206d.a.b.e.AbstractC0215b> a(StackTraceElement[] stackTraceElementArr, int i) {
        ArrayList arrayList = new ArrayList();
        for (StackTraceElement stackTraceElement : stackTraceElementArr) {
            v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a f2 = v54.d.AbstractC0206d.a.b.e.AbstractC0215b.f();
            f2.a(i);
            arrayList.add(a(stackTraceElement, f2));
        }
        return w54.a(arrayList);
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.c a(n84 n84, int i, int i2) {
        return a(n84, i, i2, 0);
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.c a(n84 n84, int i, int i2, int i3) {
        String str = n84.b;
        String str2 = n84.a;
        StackTraceElement[] stackTraceElementArr = n84.c;
        int i4 = 0;
        if (stackTraceElementArr == null) {
            stackTraceElementArr = new StackTraceElement[0];
        }
        n84 n842 = n84.d;
        if (i3 >= i2) {
            n84 n843 = n842;
            while (n843 != null) {
                n843 = n843.d;
                i4++;
            }
        }
        v54.d.AbstractC0206d.a.b.c.AbstractC0211a f2 = v54.d.AbstractC0206d.a.b.c.f();
        f2.b(str);
        f2.a(str2);
        f2.a(w54.a(a(stackTraceElementArr, i)));
        f2.a(i4);
        if (n842 != null && i4 == 0) {
            f2.a(a(n842, i, i2, i3 + 1));
        }
        return f2.a();
    }

    @DexIgnore
    public final v54.d.AbstractC0206d.a.b.e.AbstractC0215b a(StackTraceElement stackTraceElement, v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a aVar) {
        long j = 0;
        long max = stackTraceElement.isNativeMethod() ? Math.max((long) stackTraceElement.getLineNumber(), 0L) : 0;
        String str = stackTraceElement.getClassName() + CodelessMatcher.CURRENT_CLASS_NAME + stackTraceElement.getMethodName();
        String fileName = stackTraceElement.getFileName();
        if (!stackTraceElement.isNativeMethod() && stackTraceElement.getLineNumber() > 0) {
            j = (long) stackTraceElement.getLineNumber();
        }
        aVar.b(max);
        aVar.b(str);
        aVar.a(fileName);
        aVar.a(j);
        return aVar.a();
    }
}
