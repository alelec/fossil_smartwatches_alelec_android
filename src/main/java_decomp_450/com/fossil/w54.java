package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w54<E> implements List<E>, RandomAccess {
    @DexIgnore
    public /* final */ List<E> a;

    @DexIgnore
    public w54(List<E> list) {
        this.a = Collections.unmodifiableList(list);
    }

    @DexIgnore
    public static <E> w54<E> a(E... eArr) {
        return new w54<>(Arrays.asList(eArr));
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean add(E e) {
        return this.a.add(e);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean addAll(Collection<? extends E> collection) {
        return this.a.addAll(collection);
    }

    @DexIgnore
    public void clear() {
        this.a.clear();
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.a.contains(obj);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean containsAll(Collection<?> collection) {
        return this.a.containsAll(collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this.a.equals(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public int indexOf(Object obj) {
        return this.a.indexOf(obj);
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.a.isEmpty();
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return this.a.iterator();
    }

    @DexIgnore
    public int lastIndexOf(Object obj) {
        return this.a.lastIndexOf(obj);
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator() {
        return this.a.listIterator();
    }

    @DexIgnore
    @Override // java.util.List
    public boolean remove(Object obj) {
        return this.a.remove(obj);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean removeAll(Collection<?> collection) {
        return this.a.removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public boolean retainAll(Collection<?> collection) {
        return this.a.retainAll(collection);
    }

    @DexIgnore
    @Override // java.util.List
    public E set(int i, E e) {
        return this.a.set(i, e);
    }

    @DexIgnore
    public int size() {
        return this.a.size();
    }

    @DexIgnore
    @Override // java.util.List
    public List<E> subList(int i, int i2) {
        return this.a.subList(i, i2);
    }

    @DexIgnore
    public Object[] toArray() {
        return this.a.toArray();
    }

    @DexIgnore
    public static <E> w54<E> a(List<E> list) {
        return new w54<>(list);
    }

    @DexIgnore
    @Override // java.util.List
    public void add(int i, E e) {
        this.a.add(i, e);
    }

    @DexIgnore
    @Override // java.util.List
    public boolean addAll(int i, Collection<? extends E> collection) {
        return this.a.addAll(i, collection);
    }

    @DexIgnore
    @Override // java.util.List
    public ListIterator<E> listIterator(int i) {
        return this.a.listIterator(i);
    }

    @DexIgnore
    @Override // java.util.List
    public E remove(int i) {
        return this.a.remove(i);
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection
    public <T> T[] toArray(T[] tArr) {
        return (T[]) this.a.toArray(tArr);
    }
}
