package com.fossil;

import com.fossil.ly3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class my3<K, V> extends by3<K, V> {
    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3.b<K, V> builder() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3<K, V> of(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3<K, V> of(K k, V v, K k2, V v2) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.by3
    @Deprecated
    public static <K, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        throw new UnsupportedOperationException();
    }
}
