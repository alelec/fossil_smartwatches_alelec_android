package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ke4 {
    @DexIgnore
    public JsonElement a(String str) throws oe4 {
        return a(new StringReader(str));
    }

    @DexIgnore
    public JsonElement a(Reader reader) throws ge4, oe4 {
        try {
            JsonReader jsonReader = new JsonReader(reader);
            JsonElement a = a(jsonReader);
            if (!a.h()) {
                if (jsonReader.peek() != pf4.END_DOCUMENT) {
                    throw new oe4("Did not consume the entire document.");
                }
            }
            return a;
        } catch (qf4 e) {
            throw new oe4(e);
        } catch (IOException e2) {
            throw new ge4(e2);
        } catch (NumberFormatException e3) {
            throw new oe4(e3);
        }
    }

    @DexIgnore
    public JsonElement a(JsonReader jsonReader) throws ge4, oe4 {
        boolean o = jsonReader.o();
        jsonReader.b(true);
        try {
            JsonElement a = gf4.a(jsonReader);
            jsonReader.b(o);
            return a;
        } catch (StackOverflowError e) {
            throw new je4("Failed parsing JSON source: " + jsonReader + " to Json", e);
        } catch (OutOfMemoryError e2) {
            throw new je4("Failed parsing JSON source: " + jsonReader + " to Json", e2);
        } catch (Throwable th) {
            jsonReader.b(o);
            throw th;
        }
    }
}
