package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface yr<T, V> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <T, V> boolean a(yr<T, V> yrVar, T t) {
            ee7.b(t, "data");
            return true;
        }
    }

    @DexIgnore
    boolean a(T t);

    @DexIgnore
    V b(T t);
}
