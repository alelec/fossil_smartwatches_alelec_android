package com.fossil;

import android.annotation.SuppressLint;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tk extends yk {
    @DexIgnore
    public static boolean f; // = true;

    @DexIgnore
    @Override // com.fossil.yk
    public void a(View view) {
    }

    @DexIgnore
    @Override // com.fossil.yk
    @SuppressLint({"NewApi"})
    public void a(View view, float f2) {
        if (f) {
            try {
                view.setTransitionAlpha(f2);
                return;
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        view.setAlpha(f2);
    }

    @DexIgnore
    @Override // com.fossil.yk
    @SuppressLint({"NewApi"})
    public float b(View view) {
        if (f) {
            try {
                return view.getTransitionAlpha();
            } catch (NoSuchMethodError unused) {
                f = false;
            }
        }
        return view.getAlpha();
    }

    @DexIgnore
    @Override // com.fossil.yk
    public void c(View view) {
    }
}
