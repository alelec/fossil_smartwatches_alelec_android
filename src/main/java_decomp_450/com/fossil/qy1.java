package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy1 {
    @DexIgnore
    public static sy1 a(Context context, GoogleSignInOptions googleSignInOptions) {
        a72.a(googleSignInOptions);
        return new sy1(context, googleSignInOptions);
    }

    @DexIgnore
    public static GoogleSignInAccount a(Context context) {
        return kz1.a(context).b();
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, ty1 ty1) {
        a72.a(ty1, "Please provide a non-null GoogleSignInOptionsExtension");
        return a(googleSignInAccount, a(ty1.a()));
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        if (googleSignInAccount == null) {
            return false;
        }
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, scopeArr);
        return googleSignInAccount.x().containsAll(hashSet);
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, ty1 ty1) {
        a72.a(fragment, "Please provide a non-null Fragment");
        a72.a(ty1, "Please provide a non-null GoogleSignInOptionsExtension");
        a(fragment, i, googleSignInAccount, a(ty1.a()));
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        a72.a(fragment, "Please provide a non-null Fragment");
        a72.a(scopeArr, "Please provide at least one scope");
        fragment.startActivityForResult(a(fragment.getActivity(), googleSignInAccount, scopeArr), i);
    }

    @DexIgnore
    public static Intent a(Activity activity, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        GoogleSignInOptions.a aVar = new GoogleSignInOptions.a();
        if (scopeArr.length > 0) {
            aVar.a(scopeArr[0], scopeArr);
        }
        if (googleSignInAccount != null && !TextUtils.isEmpty(googleSignInAccount.g())) {
            aVar.c(googleSignInAccount.g());
        }
        return new sy1(activity, aVar.a()).i();
    }

    @DexIgnore
    public static Scope[] a(List<Scope> list) {
        return list == null ? new Scope[0] : (Scope[]) list.toArray(new Scope[list.size()]);
    }
}
