package com.fossil;

import com.fossil.fl4;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm5 implements fl4.a {
    @DexIgnore
    public /* final */ nb5 a;
    @DexIgnore
    public /* final */ ArrayList<Integer> b;

    @DexIgnore
    public jm5(nb5 nb5, ArrayList<Integer> arrayList) {
        ee7.b(nb5, "lastErrorCode");
        this.a = nb5;
        this.b = arrayList;
    }

    @DexIgnore
    public final nb5 a() {
        return this.a;
    }

    @DexIgnore
    public final ArrayList<Integer> b() {
        return this.b;
    }
}
