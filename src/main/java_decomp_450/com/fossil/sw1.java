package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sw1 extends Closeable {
    @DexIgnore
    yw1 a(pu1 pu1, ku1 ku1);

    @DexIgnore
    Iterable<yw1> a(pu1 pu1);

    @DexIgnore
    void a(pu1 pu1, long j);

    @DexIgnore
    void a(Iterable<yw1> iterable);

    @DexIgnore
    long b(pu1 pu1);

    @DexIgnore
    void b(Iterable<yw1> iterable);

    @DexIgnore
    boolean c(pu1 pu1);

    @DexIgnore
    int cleanUp();

    @DexIgnore
    Iterable<pu1> i();
}
