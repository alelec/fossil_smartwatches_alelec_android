package com.fossil;

import com.fossil.c12;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j22 {
    @DexIgnore
    public /* final */ Map<BasePendingResult<?>, Boolean> a; // = Collections.synchronizedMap(new WeakHashMap());
    @DexIgnore
    public /* final */ Map<oo3<?>, Boolean> b; // = Collections.synchronizedMap(new WeakHashMap());

    @DexIgnore
    public final void a(BasePendingResult<? extends i12> basePendingResult, boolean z) {
        this.a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.a((c12.a) new i22(this, basePendingResult));
    }

    @DexIgnore
    public final void b() {
        a(false, u12.n);
    }

    @DexIgnore
    public final void c() {
        a(true, n42.d);
    }

    @DexIgnore
    public final <TResult> void a(oo3<TResult> oo3, boolean z) {
        this.b.put(oo3, Boolean.valueOf(z));
        oo3.a().a(new l22(this, oo3));
    }

    @DexIgnore
    public final boolean a() {
        return !this.a.isEmpty() || !this.b.isEmpty();
    }

    @DexIgnore
    public final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.a) {
            hashMap = new HashMap(this.a);
        }
        synchronized (this.b) {
            hashMap2 = new HashMap(this.b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).b(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((oo3) entry2.getKey()).b((Exception) new w02(status));
            }
        }
    }
}
