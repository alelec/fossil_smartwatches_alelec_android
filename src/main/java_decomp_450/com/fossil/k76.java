package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k76 implements Factory<j76> {
    @DexIgnore
    public static j76 a(h76 h76, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        return new j76(h76, summariesRepository, fitnessDataRepository, userRepository, pj4);
    }
}
