package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hk4 implements Factory<qg5> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public hk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static hk4 a(wj4 wj4) {
        return new hk4(wj4);
    }

    @DexIgnore
    public static qg5 b(wj4 wj4) {
        qg5 g = wj4.g();
        c87.a(g, "Cannot return null from a non-@Nullable @Provides method");
        return g;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public qg5 get() {
        return b(this.a);
    }
}
