package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qd1 extends fe7 implements kd7<v81, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zm1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qd1(zm1 zm1) {
        super(2);
        this.a = zm1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(v81 v81, Float f) {
        float floatValue = f.floatValue();
        Iterator<T> it = this.a.G.iterator();
        int i = 0;
        while (it.hasNext()) {
            i += (int) ((bi1) it.next()).f;
        }
        float e = ((floatValue * ((float) this.a.E)) + ((float) i)) / ((float) this.a.E);
        zm1 zm1 = this.a;
        if (e - zm1.J > zm1.N || e == 1.0f) {
            zm1 zm12 = this.a;
            zm12.J = e;
            zm12.a(e);
        }
        return i97.a;
    }
}
