package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class o32 {
    @DexIgnore
    public /* final */ m32 a;

    @DexIgnore
    public o32(m32 m32) {
        this.a = m32;
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void a(l32 l32) {
        l32.a.lock();
        try {
            if (l32.p == this.a) {
                a();
                l32.a.unlock();
            }
        } finally {
            l32.a.unlock();
        }
    }
}
