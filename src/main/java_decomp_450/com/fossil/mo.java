package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo implements lo {
    @DexIgnore
    public /* final */ ci a;
    @DexIgnore
    public /* final */ vh<ko> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends vh<ko> {
        @DexIgnore
        public a(mo moVar, ci ciVar) {
            super(ciVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(aj ajVar, ko koVar) {
            String str = koVar.a;
            if (str == null) {
                ajVar.bindNull(1);
            } else {
                ajVar.bindString(1, str);
            }
            String str2 = koVar.b;
            if (str2 == null) {
                ajVar.bindNull(2);
            } else {
                ajVar.bindString(2, str2);
            }
        }

        @DexIgnore
        @Override // com.fossil.ji
        public String createQuery() {
            return "INSERT OR IGNORE INTO `Dependency` (`work_spec_id`,`prerequisite_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public mo(ci ciVar) {
        this.a = ciVar;
        this.b = new a(this, ciVar);
    }

    @DexIgnore
    @Override // com.fossil.lo
    public void a(ko koVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(koVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.fossil.lo
    public boolean b(String str) {
        boolean z = true;
        fi b2 = fi.b("SELECT COUNT(*)=0 FROM dependency WHERE work_spec_id=? AND prerequisite_id IN (SELECT id FROM workspec WHERE state!=2)", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z2 = false;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                if (a2.getInt(0) == 0) {
                    z = false;
                }
                z2 = z;
            }
            return z2;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.lo
    public boolean c(String str) {
        boolean z = true;
        fi b2 = fi.b("SELECT COUNT(*)>0 FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        boolean z2 = false;
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            if (a2.moveToFirst()) {
                if (a2.getInt(0) == 0) {
                    z = false;
                }
                z2 = z;
            }
            return z2;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.lo
    public List<String> a(String str) {
        fi b2 = fi.b("SELECT work_spec_id FROM dependency WHERE prerequisite_id=?", 1);
        if (str == null) {
            b2.bindNull(1);
        } else {
            b2.bindString(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = pi.a(this.a, b2, false, null);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
