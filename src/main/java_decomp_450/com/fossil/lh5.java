package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh5 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public SsoHandler a;
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lh5.c;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ lh5 a;
        @DexIgnore
        public /* final */ /* synthetic */ mh5 b;

        @DexIgnore
        public b(lh5 lh5, mh5 mh5) {
            this.a = lh5;
            this.b = mh5;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.a()) {
                this.b.a(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WbAuthListener {
        @DexIgnore
        public /* final */ /* synthetic */ lh5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ mh5 c;

        @DexIgnore
        public c(lh5 lh5, Handler handler, mh5 mh5) {
            this.a = lh5;
            this.b = handler;
            this.c = mh5;
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void cancel() {
            FLogger.INSTANCE.getLocal().d(lh5.d.a(), "Weibo loginWithEmail cancel");
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.a(2, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onFailure(WbConnectErrorMessage wbConnectErrorMessage) {
            ee7.b(wbConnectErrorMessage, "wbConnectErrorMessage");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lh5.d.a();
            local.d(a2, "Weibo loginWithEmail failed - message: " + wbConnectErrorMessage.getErrorMessage() + "- code: " + wbConnectErrorMessage.getErrorCode());
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            this.c.a(600, null, null);
        }

        @DexIgnore
        @Override // com.sina.weibo.sdk.auth.WbAuthListener
        public void onSuccess(Oauth2AccessToken oauth2AccessToken) {
            ee7.b(oauth2AccessToken, "oauth2AccessToken");
            this.a.a(true);
            this.b.removeCallbacksAndMessages(null);
            String token = oauth2AccessToken.getToken();
            String format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).format(new Date(oauth2AccessToken.getExpiresTime()));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = lh5.d.a();
            local.d(a2, "Get access token from weibo success: " + token + " ,expire date: " + format);
            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
            signUpSocialAuth.setClientId(rd5.g.a(""));
            ee7.a((Object) token, "authToken");
            signUpSocialAuth.setToken(token);
            signUpSocialAuth.setService("weibo");
            this.c.a(signUpSocialAuth);
        }
    }

    /*
    static {
        String simpleName = lh5.class.getSimpleName();
        ee7.a((Object) simpleName, "MFLoginWeiboManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final boolean a() {
        return this.b;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        ee7.b(str, "apiKey");
        ee7.b(str2, "redirectUrl");
        ee7.b(str3, "scope");
        try {
            WbSdk.install(PortfolioApp.g0.c(), new AuthInfo(PortfolioApp.g0.c(), str, str2, str3));
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(c, "exception when install WBSdk");
        }
    }

    @DexIgnore
    public final void a(int i, int i2, Intent intent) {
        ee7.b(intent, "data");
        SsoHandler ssoHandler = this.a;
        if (ssoHandler == null) {
            return;
        }
        if (ssoHandler != null) {
            ssoHandler.authorizeCallBack(i, i2, intent);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Activity activity, mh5 mh5) {
        ee7.b(activity, Constants.ACTIVITY);
        ee7.b(mh5, Constants.CALLBACK);
        try {
            this.b = false;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new b(this, mh5), 60000);
            SsoHandler ssoHandler = new SsoHandler(activity);
            this.a = ssoHandler;
            if (ssoHandler != null) {
                ssoHandler.authorize(new c(this, handler, mh5));
            } else {
                ee7.a();
                throw null;
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "Weibo loginWithEmail failed - ex=" + e);
            mh5.a(600, null, "");
        }
    }
}
