package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d13 implements a13 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;
    @DexIgnore
    public static /* final */ tq2<Boolean> c;
    @DexIgnore
    public static /* final */ tq2<Boolean> d;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.service.audience.fix_skip_audience_with_failed_filters", true);
        b = dr2.a("measurement.audience.refresh_event_count_filters_timestamp", false);
        c = dr2.a("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false);
        d = dr2.a("measurement.audience.use_bundle_timestamp_for_event_count_filters", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.a13
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.a13
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.a13
    public final boolean zzc() {
        return b.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.a13
    public final boolean zzd() {
        return c.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.a13
    public final boolean zze() {
        return d.b().booleanValue();
    }
}
