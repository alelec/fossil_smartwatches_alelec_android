package com.fossil;

import android.animation.Animator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vt3 {
    @DexIgnore
    public Animator a;

    @DexIgnore
    public void a(Animator animator) {
        a();
        this.a = animator;
    }

    @DexIgnore
    public void b() {
        this.a = null;
    }

    @DexIgnore
    public void a() {
        Animator animator = this.a;
        if (animator != null) {
            animator.cancel();
        }
    }
}
