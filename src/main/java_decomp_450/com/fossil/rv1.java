package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rv1 implements Factory<qv1> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<bv1> b;
    @DexIgnore
    public /* final */ Provider<pw1> c;
    @DexIgnore
    public /* final */ Provider<sw1> d;
    @DexIgnore
    public /* final */ Provider<ay1> e;

    @DexIgnore
    public rv1(Provider<Executor> provider, Provider<bv1> provider2, Provider<pw1> provider3, Provider<sw1> provider4, Provider<ay1> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static rv1 a(Provider<Executor> provider, Provider<bv1> provider2, Provider<pw1> provider3, Provider<sw1> provider4, Provider<ay1> provider5) {
        return new rv1(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public qv1 get() {
        return new qv1(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
