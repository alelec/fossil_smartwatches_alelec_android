package com.fossil;

import android.database.Cursor;
import com.fossil.rx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class kx1 implements rx1.b {
    @DexIgnore
    public static /* final */ kx1 a; // = new kx1();

    @DexIgnore
    public static rx1.b a() {
        return a;
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return Boolean.valueOf(((Cursor) obj).moveToNext());
    }
}
