package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz1 extends jz1<Status> {
    @DexIgnore
    public fz1(a12 a12) {
        super(a12);
    }

    @DexIgnore
    @Override // com.google.android.gms.common.api.internal.BasePendingResult
    public final /* synthetic */ i12 a(Status status) {
        return status;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b] */
    @Override // com.fossil.r12
    public final /* synthetic */ void a(dz1 dz1) throws RemoteException {
        dz1 dz12 = dz1;
        ((pz1) dz12.A()).a(new gz1(this), dz12.I());
    }
}
