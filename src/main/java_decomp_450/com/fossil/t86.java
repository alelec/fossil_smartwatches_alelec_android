package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.be5;
import com.fossil.cc5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.home.details.activity.ActivityDetailActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t86 extends go5 implements s86 {
    @DexIgnore
    public qw6<ax4> f;
    @DexIgnore
    public r86 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActivityDetailActivity.a aVar = ActivityDetailActivity.A;
            Date date = new Date();
            ee7.a((Object) view, "it");
            Context context = view.getContext();
            ee7.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.s86
    public void b(do5 do5, ArrayList<String> arrayList) {
        ax4 a2;
        OverviewDayChart overviewDayChart;
        ee7.b(do5, "baseModel");
        ee7.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActivityOverviewDayFragment", "showDayDetails - baseModel=" + do5);
        qw6<ax4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) do5;
            cVar.b(do5.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) xe5.b.b(), false, 2, (Object) null);
            }
            overviewDayChart.a(do5);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "ActivityOverviewDayFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void f1() {
        ax4 a2;
        OverviewDayChart overviewDayChart;
        qw6<ax4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null && (overviewDayChart = a2.q) != null) {
            be5.a aVar = be5.o;
            r86 r86 = this.g;
            if (aVar.a(r86 != null ? r86.h() : null)) {
                overviewDayChart.a("dianaStepsTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridStepsTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ax4 a2;
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onCreateView");
        ax4 ax4 = (ax4) qb.a(layoutInflater, 2131558498, viewGroup, false, a1());
        ax4.r.setOnClickListener(b.a);
        this.f = new qw6<>(this, ax4);
        f1();
        qw6<ax4> qw6 = this.f;
        if (qw6 == null || (a2 = qw6.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onResume");
        f1();
        r86 r86 = this.g;
        if (r86 != null) {
            r86.f();
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onStop");
        r86 r86 = this.g;
        if (r86 != null) {
            r86.g();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActivityOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    @Override // com.fossil.s86
    public void a(boolean z, List<WorkoutSession> list) {
        ax4 a2;
        View d;
        View d2;
        ee7.b(list, "workoutSessions");
        qw6<ax4> qw6 = this.f;
        if (qw6 != null && (a2 = qw6.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                ee7.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    c85 c85 = a2.t;
                    if (c85 != null && (d2 = c85.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                c85 c852 = a2.t;
                if (!(c852 == null || (d = c852.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            ee7.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(r86 r86) {
        ee7.b(r86, "presenter");
        this.g = r86;
    }

    @DexIgnore
    public final void a(c85 c85, WorkoutSession workoutSession) {
        String str;
        String str2;
        int i;
        if (c85 != null) {
            View d = c85.d();
            ee7.a((Object) d, "binding.root");
            Context context = d.getContext();
            cc5.a aVar = cc5.Companion;
            r87<Integer, Integer> a2 = aVar.a(aVar.a(workoutSession.getEditedType(), workoutSession.getEditedMode()));
            String a3 = ig5.a(context, a2.getSecond().intValue());
            c85.t.setImageResource(a2.getFirst().intValue());
            be5.a aVar2 = be5.o;
            r86 r86 = this.g;
            if (aVar2.a(r86 != null ? r86.h() : null)) {
                str = eh5.l.a().b("dianaStepsTab");
            } else {
                str = eh5.l.a().b("hybridStepsTab");
            }
            if (str != null) {
                c85.t.setColorFilter(Color.parseColor(str));
            }
            FlexibleTextView flexibleTextView = c85.r;
            ee7.a((Object) flexibleTextView, "it.ftvWorkoutTitle");
            flexibleTextView.setText(a3);
            ac5 editedType = workoutSession.getEditedType();
            if (editedType == null) {
                editedType = workoutSession.getWorkoutType();
            }
            FlexibleTextView flexibleTextView2 = c85.s;
            ee7.a((Object) flexibleTextView2, "it.ftvWorkoutValue");
            if (editedType != null && ((i = u86.a[editedType.ordinal()]) == 1 || i == 2)) {
                str2 = "";
            } else {
                we7 we7 = we7.a;
                String a4 = ig5.a(context, 2131886636);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
                Object[] objArr = new Object[1];
                Integer totalSteps = workoutSession.getTotalSteps();
                objArr[0] = re5.b(totalSteps != null ? (float) totalSteps.intValue() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1);
                str2 = String.format(a4, Arrays.copyOf(objArr, 1));
                ee7.a((Object) str2, "java.lang.String.format(format, *args)");
            }
            flexibleTextView2.setText(str2);
            FlexibleTextView flexibleTextView3 = c85.q;
            ee7.a((Object) flexibleTextView3, "it.ftvWorkoutTime");
            flexibleTextView3.setText(zd5.a(workoutSession.getEditedStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
        }
    }
}
