package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn2 extends ln2 implements on2 {
    @DexIgnore
    public rn2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IStringProvider");
    }
}
