package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ba7 extends aa7 {
    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, Iterable<? extends T> iterable) {
        ee7.b(collection, "$this$addAll");
        ee7.b(iterable, MessengerShareContentUtility.ELEMENTS);
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        Iterator<? extends T> it = iterable.iterator();
        while (it.hasNext()) {
            if (collection.add((Object) it.next())) {
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static final <T> boolean a(Collection<? super T> collection, T[] tArr) {
        ee7.b(collection, "$this$addAll");
        ee7.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return collection.addAll(s97.b(tArr));
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, gd7<? super T, Boolean> gd7) {
        ee7.b(iterable, "$this$retainAll");
        ee7.b(gd7, "predicate");
        return a((Iterable) iterable, (gd7) gd7, false);
    }

    @DexIgnore
    public static final <T> boolean a(Iterable<? extends T> iterable, gd7<? super T, Boolean> gd7, boolean z) {
        Iterator<? extends T> it = iterable.iterator();
        boolean z2 = false;
        while (it.hasNext()) {
            if (gd7.invoke((Object) it.next()).booleanValue() == z) {
                it.remove();
                z2 = true;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, gd7<? super T, Boolean> gd7) {
        ee7.b(list, "$this$removeAll");
        ee7.b(gd7, "predicate");
        return a((List) list, (gd7) gd7, true);
    }

    @DexIgnore
    public static final <T> boolean a(List<T> list, gd7<? super T, Boolean> gd7, boolean z) {
        int i;
        if (list instanceof RandomAccess) {
            int a = w97.a((List) list);
            if (a >= 0) {
                int i2 = 0;
                i = 0;
                while (true) {
                    T t = list.get(i2);
                    if (gd7.invoke(t).booleanValue() != z) {
                        if (i != i2) {
                            list.set(i, t);
                        }
                        i++;
                    }
                    if (i2 == a) {
                        break;
                    }
                    i2++;
                }
            } else {
                i = 0;
            }
            if (i >= list.size()) {
                return false;
            }
            int a2 = w97.a((List) list);
            if (a2 < i) {
                return true;
            }
            while (true) {
                list.remove(a2);
                if (a2 == i) {
                    return true;
                }
                a2--;
            }
        } else if (list != null) {
            return a(xe7.a(list), gd7, z);
        } else {
            throw new x87("null cannot be cast to non-null type kotlin.collections.MutableIterable<T>");
        }
    }
}
