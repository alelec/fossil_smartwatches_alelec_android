package com.fossil;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hj4 {
    @DexIgnore
    public static /* final */ int[][] a; // = {new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] b; // = {new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, 102, -1, -1}, new int[]{6, 28, 54, 80, 106, -1, -1}, new int[]{6, 32, 58, 84, 110, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, 102, 126, -1}, new int[]{6, 26, 52, 78, 104, 130, -1}, new int[]{6, 30, 56, 82, 108, 134, -1}, new int[]{6, 34, 60, 86, 112, 138, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, 102, 126, 150}, new int[]{6, 24, 50, 76, 102, 128, 154}, new int[]{6, 28, 54, 80, 106, 132, 158}, new int[]{6, 32, 58, 84, 110, 136, 162}, new int[]{6, 26, 54, 82, 110, 138, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};
    @DexIgnore
    public static /* final */ int[][] d; // = {new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};

    @DexIgnore
    public static void a(ej4 ej4) {
        ej4.a((byte) -1);
    }

    @DexIgnore
    public static void b(ej4 ej4) throws tg4 {
        if (ej4.a(8, ej4.b() - 8) != 0) {
            ej4.a(8, ej4.b() - 8, 1);
            return;
        }
        throw new tg4();
    }

    @DexIgnore
    public static boolean b(int i) {
        return i == -1;
    }

    @DexIgnore
    public static void c(cj4 cj4, ej4 ej4) throws tg4 {
        if (cj4.c() >= 7) {
            ch4 ch4 = new ch4();
            a(cj4, ch4);
            int i = 17;
            for (int i2 = 0; i2 < 6; i2++) {
                for (int i3 = 0; i3 < 3; i3++) {
                    boolean b2 = ch4.b(i);
                    i--;
                    ej4.a(i2, (ej4.b() - 11) + i3, b2);
                    ej4.a((ej4.b() - 11) + i3, i2, b2);
                }
            }
        }
    }

    @DexIgnore
    public static void d(ej4 ej4) {
        int i = 8;
        while (i < ej4.c() - 8) {
            int i2 = i + 1;
            int i3 = i2 % 2;
            if (b(ej4.a(i, 6))) {
                ej4.a(i, 6, i3);
            }
            if (b(ej4.a(6, i))) {
                ej4.a(6, i, i3);
            }
            i = i2;
        }
    }

    @DexIgnore
    public static void a(ch4 ch4, aj4 aj4, cj4 cj4, int i, ej4 ej4) throws tg4 {
        a(ej4);
        a(cj4, ej4);
        a(aj4, i, ej4);
        c(cj4, ej4);
        a(ch4, i, ej4);
    }

    @DexIgnore
    public static void b(int i, int i2, ej4 ej4) {
        for (int i3 = 0; i3 < 5; i3++) {
            for (int i4 = 0; i4 < 5; i4++) {
                ej4.a(i + i4, i2 + i3, b[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void b(cj4 cj4, ej4 ej4) {
        if (cj4.c() >= 2) {
            int c2 = cj4.c() - 1;
            int[][] iArr = c;
            int[] iArr2 = iArr[c2];
            int length = iArr[c2].length;
            for (int i = 0; i < length; i++) {
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = iArr2[i];
                    int i4 = iArr2[i2];
                    if (!(i4 == -1 || i3 == -1 || !b(ej4.a(i4, i3)))) {
                        b(i4 - 2, i3 - 2, ej4);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(cj4 cj4, ej4 ej4) throws tg4 {
        c(ej4);
        b(ej4);
        b(cj4, ej4);
        d(ej4);
    }

    @DexIgnore
    public static void c(int i, int i2, ej4 ej4) {
        for (int i3 = 0; i3 < 7; i3++) {
            for (int i4 = 0; i4 < 7; i4++) {
                ej4.a(i + i4, i2 + i3, a[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void d(int i, int i2, ej4 ej4) throws tg4 {
        int i3 = 0;
        while (i3 < 7) {
            int i4 = i2 + i3;
            if (b(ej4.a(i, i4))) {
                ej4.a(i, i4, 0);
                i3++;
            } else {
                throw new tg4();
            }
        }
    }

    @DexIgnore
    public static void c(ej4 ej4) throws tg4 {
        int length = a[0].length;
        c(0, 0, ej4);
        c(ej4.c() - length, 0, ej4);
        c(0, ej4.c() - length, ej4);
        a(0, 7, ej4);
        a(ej4.c() - 8, 7, ej4);
        a(0, ej4.c() - 8, ej4);
        d(7, 0, ej4);
        d((ej4.b() - 7) - 1, 0, ej4);
        d(7, ej4.b() - 7, ej4);
    }

    @DexIgnore
    public static void a(aj4 aj4, int i, ej4 ej4) throws tg4 {
        ch4 ch4 = new ch4();
        a(aj4, i, ch4);
        for (int i2 = 0; i2 < ch4.d(); i2++) {
            boolean b2 = ch4.b((ch4.d() - 1) - i2);
            int[][] iArr = d;
            ej4.a(iArr[i2][0], iArr[i2][1], b2);
            if (i2 < 8) {
                ej4.a((ej4.c() - i2) - 1, 8, b2);
            } else {
                ej4.a(8, (ej4.b() - 7) + (i2 - 8), b2);
            }
        }
    }

    @DexIgnore
    public static void a(ch4 ch4, int i, ej4 ej4) throws tg4 {
        boolean z;
        int c2 = ej4.c() - 1;
        int b2 = ej4.b() - 1;
        int i2 = 0;
        int i3 = -1;
        while (c2 > 0) {
            if (c2 == 6) {
                c2--;
            }
            while (b2 >= 0 && b2 < ej4.b()) {
                for (int i4 = 0; i4 < 2; i4++) {
                    int i5 = c2 - i4;
                    if (b(ej4.a(i5, b2))) {
                        if (i2 < ch4.d()) {
                            z = ch4.b(i2);
                            i2++;
                        } else {
                            z = false;
                        }
                        if (i != -1 && gj4.a(i, i5, b2)) {
                            z = !z;
                        }
                        ej4.a(i5, b2, z);
                    }
                }
                b2 += i3;
            }
            i3 = -i3;
            b2 += i3;
            c2 -= 2;
        }
        if (i2 != ch4.d()) {
            throw new tg4("Not all bits consumed: " + i2 + '/' + ch4.d());
        }
    }

    @DexIgnore
    public static int a(int i) {
        return 32 - Integer.numberOfLeadingZeros(i);
    }

    @DexIgnore
    public static int a(int i, int i2) {
        if (i2 != 0) {
            int a2 = a(i2);
            int i3 = i << (a2 - 1);
            while (a(i3) >= a2) {
                i3 ^= i2 << (a(i3) - a2);
            }
            return i3;
        }
        throw new IllegalArgumentException("0 polynomial");
    }

    @DexIgnore
    public static void a(aj4 aj4, int i, ch4 ch4) throws tg4 {
        if (ij4.b(i)) {
            int bits = (aj4.getBits() << 3) | i;
            ch4.a(bits, 5);
            ch4.a(a(bits, (int) FailureCode.FAILED_TO_START_STREAMING), 10);
            ch4 ch42 = new ch4();
            ch42.a(21522, 15);
            ch4.b(ch42);
            if (ch4.d() != 15) {
                throw new tg4("should not happen but we got: " + ch4.d());
            }
            return;
        }
        throw new tg4("Invalid mask pattern");
    }

    @DexIgnore
    public static void a(cj4 cj4, ch4 ch4) throws tg4 {
        ch4.a(cj4.c(), 6);
        ch4.a(a(cj4.c(), 7973), 12);
        if (ch4.d() != 18) {
            throw new tg4("should not happen but we got: " + ch4.d());
        }
    }

    @DexIgnore
    public static void a(int i, int i2, ej4 ej4) throws tg4 {
        int i3 = 0;
        while (i3 < 8) {
            int i4 = i + i3;
            if (b(ej4.a(i4, i2))) {
                ej4.a(i4, i2, 0);
                i3++;
            } else {
                throw new tg4();
            }
        }
    }
}
