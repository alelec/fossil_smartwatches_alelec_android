package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y72 extends uf2 implements w72 {
    @DexIgnore
    public y72(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.service.ICommonService");
    }

    @DexIgnore
    @Override // com.fossil.w72
    public final void a(u72 u72) throws RemoteException {
        Parcel E = E();
        vf2.a(E, u72);
        c(1, E);
    }
}
