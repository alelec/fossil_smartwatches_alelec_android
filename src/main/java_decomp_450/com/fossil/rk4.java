package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rk4 implements Factory<ph5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<ll4> c;

    @DexIgnore
    public rk4(wj4 wj4, Provider<ch5> provider, Provider<ll4> provider2) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static rk4 a(wj4 wj4, Provider<ch5> provider, Provider<ll4> provider2) {
        return new rk4(wj4, provider, provider2);
    }

    @DexIgnore
    public static ph5 a(wj4 wj4, ch5 ch5, ll4 ll4) {
        ph5 a2 = wj4.a(ch5, ll4);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ph5 get() {
        return a(this.a, this.b.get(), this.c.get());
    }
}
