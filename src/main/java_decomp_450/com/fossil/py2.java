package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class py2 implements sy2 {
    @DexIgnore
    public /* final */ /* synthetic */ tu2 a;

    @DexIgnore
    public py2(tu2 tu2) {
        this.a = tu2;
    }

    @DexIgnore
    @Override // com.fossil.sy2
    public final int zza() {
        return this.a.zza();
    }

    @DexIgnore
    @Override // com.fossil.sy2
    public final byte zza(int i) {
        return this.a.zza(i);
    }
}
