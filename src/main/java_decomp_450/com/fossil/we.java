package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.service.media.MediaBrowserService;
import android.support.v4.media.session.MediaSessionCompat;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class we {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ Bundle b;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends MediaBrowserService {
        @DexIgnore
        public /* final */ d a;

        @DexIgnore
        public b(Context context, d dVar) {
            attachBaseContext(context);
            this.a = dVar;
        }

        @DexIgnore
        public MediaBrowserService.BrowserRoot onGetRoot(String str, int i, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            a a2 = this.a.a(str, i, bundle == null ? null : new Bundle(bundle));
            if (a2 == null) {
                return null;
            }
            return new MediaBrowserService.BrowserRoot(a2.a, a2.b);
        }

        @DexIgnore
        @Override // android.service.media.MediaBrowserService
        public void onLoadChildren(String str, MediaBrowserService.Result<List<MediaBrowser.MediaItem>> result) {
            this.a.b(str, new c<>(result));
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        a a(String str, int i, Bundle bundle);

        @DexIgnore
        void b(String str, c<List<Parcel>> cVar);
    }

    @DexIgnore
    public static Object a(Context context, d dVar) {
        return new b(context, dVar);
    }

    @DexIgnore
    public static void a(Object obj) {
        ((MediaBrowserService) obj).onCreate();
    }

    @DexIgnore
    public static IBinder a(Object obj, Intent intent) {
        return ((MediaBrowserService) obj).onBind(intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<T> {
        @DexIgnore
        public MediaBrowserService.Result a;

        @DexIgnore
        public c(MediaBrowserService.Result result) {
            this.a = result;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: android.service.media.MediaBrowserService$Result */
        /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: android.service.media.MediaBrowserService$Result */
        /* JADX WARN: Multi-variable type inference failed */
        public void a(T t) {
            if (t instanceof List) {
                this.a.sendResult(a((List<Parcel>) t));
            } else if (t instanceof Parcel) {
                T t2 = t;
                t2.setDataPosition(0);
                this.a.sendResult(MediaBrowser.MediaItem.CREATOR.createFromParcel(t2));
                t2.recycle();
            } else {
                this.a.sendResult(null);
            }
        }

        @DexIgnore
        public List<MediaBrowser.MediaItem> a(List<Parcel> list) {
            if (list == null) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Parcel parcel : list) {
                parcel.setDataPosition(0);
                arrayList.add(MediaBrowser.MediaItem.CREATOR.createFromParcel(parcel));
                parcel.recycle();
            }
            return arrayList;
        }
    }
}
