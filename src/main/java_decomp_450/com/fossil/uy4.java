package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uy4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat q;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat r;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ RTLImageView x;
    @DexIgnore
    public /* final */ RTLImageView y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    public uy4(Object obj, View view, int i, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleSwitchCompat flexibleSwitchCompat2, FlexibleSwitchCompat flexibleSwitchCompat3, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, ConstraintLayout constraintLayout4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4) {
        super(obj, view, i);
        this.q = flexibleSwitchCompat;
        this.r = flexibleSwitchCompat2;
        this.s = flexibleSwitchCompat3;
        this.t = constraintLayout;
        this.u = constraintLayout2;
        this.v = constraintLayout3;
        this.w = rTLImageView;
        this.x = rTLImageView2;
        this.y = rTLImageView3;
        this.z = rTLImageView4;
        this.A = constraintLayout4;
        this.B = flexibleTextView;
        this.C = flexibleTextView2;
        this.D = flexibleTextView3;
        this.E = flexibleTextView4;
    }
}
