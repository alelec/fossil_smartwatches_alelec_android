package com.fossil;

import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class au {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends au {
        @DexIgnore
        public a(Drawable drawable) {
            super(null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends au {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Drawable drawable, boolean z) {
            super(null);
            ee7.b(drawable, ResourceManager.DRAWABLE);
        }
    }

    @DexIgnore
    public au() {
    }

    @DexIgnore
    public /* synthetic */ au(zd7 zd7) {
        this();
    }
}
