package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v33 implements tr2<u33> {
    @DexIgnore
    public static v33 b; // = new v33();
    @DexIgnore
    public /* final */ tr2<u33> a;

    @DexIgnore
    public v33(tr2<u33> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((u33) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((u33) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ u33 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public v33() {
        this(sr2.a(new x33()));
    }
}
