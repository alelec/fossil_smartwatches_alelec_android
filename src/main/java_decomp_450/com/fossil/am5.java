package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.UserSettingDao;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public String d;
    @DexIgnore
    public /* final */ nj5.b e; // = new e(this);
    @DexIgnore
    public /* final */ PortfolioApp f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ UserSettingDao h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return am5.i;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ActivitySettings b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public b(String str, ActivitySettings activitySettings, boolean z) {
            ee7.b(str, "serial");
            ee7.b(activitySettings, com.fossil.wearables.fsl.fitness.ActivitySettings.TABLE_NAME);
            this.a = str;
            this.b = activitySettings;
            this.c = z;
        }

        @DexIgnore
        public final ActivitySettings a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, String str, ArrayList<Integer> arrayList) {
            this.a = i;
            this.b = str;
            this.c = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, String str, ArrayList arrayList, int i2, zd7 zd7) {
            this(i, str, (i2 & 4) != 0 ? null : arrayList);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ am5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase$mSetGoalReceiver$1$receive$1", f = "SetActivityGoalUserCase.kt", l = {51, 60}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ boolean $isGoalRingEnabled;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, boolean z, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$isGoalRingEnabled = z;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$isGoalRingEnabled, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    UserRepository c = this.this$0.a.g;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    UserSettings userSettings = (UserSettings) this.L$2;
                    MFUser mFUser = (MFUser) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    this.this$0.a.a(new d());
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFUser mFUser2 = (MFUser) obj;
                if (mFUser2 == null) {
                    return i97.a;
                }
                UserSettings userSetting = this.this$0.a.g.getUserSetting();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = am5.j.a();
                StringBuilder sb = new StringBuilder();
                sb.append(".updateUserSettingGoalRing, userSetting=");
                sb.append(userSetting);
                sb.append(", dbIsRingEnable=");
                sb.append(userSetting != null ? pb7.a(userSetting.isShowGoalRing()) : null);
                sb.append(" isRingEnable=");
                sb.append(this.$isGoalRingEnabled);
                sb.append(", currentUserId=");
                sb.append(mFUser2.getUserId());
                local.d(a2, sb.toString());
                if (userSetting == null) {
                    userSetting = new UserSettings();
                    userSetting.setUid(mFUser2.getUserId());
                }
                userSetting.setShowGoalRing(this.$isGoalRingEnabled);
                this.this$0.a.g.insertUserSetting(userSetting);
                am5 am5 = this.this$0.a;
                this.L$0 = yi7;
                this.L$1 = mFUser2;
                this.L$2 = userSetting;
                this.label = 2;
                if (am5.a(userSetting, this) == a) {
                    return a;
                }
                this.this$0.a.a(new d());
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(am5 am5) {
            this.a = am5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            Bundle extras = intent.getExtras();
            boolean z = true;
            if (extras != null) {
                z = extras.getBoolean(ButtonService.GOAL_RING_ENABLED, true);
            }
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            if (communicateMode == CommunicateMode.SET_STEP_GOAL && ee7.a((Object) stringExtra, (Object) this.a.d)) {
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    ik7 unused = xh7.b(this.a.b(), null, null, new a(this, z, null), 3, null);
                } else if (intExtra == ServiceActionResult.FAILED.ordinal()) {
                    ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                    int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
                    if (intExtra2 == 1101) {
                        this.a.a(new c(125, "", integerArrayListExtra));
                    } else if (intExtra2 == 1505) {
                        this.a.a(new c(123, "", null, 4, null));
                    } else if (intExtra2 != 1920) {
                        this.a.a(new c(600, "", null, 4, null));
                    } else {
                        this.a.a(new c(124, "", null, 4, null));
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SetActivityGoalUserCase", f = "SetActivityGoalUserCase.kt", l = {86}, m = "sendUserSettingToServer")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ am5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(am5 am5, fb7 fb7) {
            super(fb7);
            this.this$0 = am5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((UserSettings) null, this);
        }
    }

    /*
    static {
        String simpleName = am5.class.getSimpleName();
        ee7.a((Object) simpleName, "SetActivityGoalUserCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public am5(PortfolioApp portfolioApp, UserRepository userRepository, UserSettingDao userSettingDao) {
        ee7.b(portfolioApp, "mPortfolioApp");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(userSettingDao, "mUserSettingDao");
        this.f = portfolioApp;
        this.g = userRepository;
        this.h = userSettingDao;
    }

    @DexIgnore
    public final void d() {
        FLogger.INSTANCE.getLocal().d(i, "registerReceiver");
        nj5.d.b(this.e, CommunicateMode.SET_STEP_GOAL);
        nj5.d.a(this.e, CommunicateMode.SET_STEP_GOAL);
        nj5.d.a(CommunicateMode.SET_STEP_GOAL);
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d(i, "unRegisterReceiver");
        nj5.d.b(this.e, CommunicateMode.SET_STEP_GOAL);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return i;
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        if (bVar == null) {
            return new c(600, "", null, 4, null);
        }
        FLogger.INSTANCE.getLocal().d(i, "running UseCase");
        this.d = bVar.b();
        this.f.a(bVar.b(), bVar.a().getCurrentStepGoal(), bVar.a().getCurrentCaloriesGoal(), bVar.a().getCurrentActiveTimeGoal(), bVar.c());
        return new Object();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    @android.annotation.SuppressLint({"VisibleForTests"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.portfolio.platform.data.model.UserSettings r5, com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r4 = this;
            boolean r0 = r6 instanceof com.fossil.am5.f
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.am5$f r0 = (com.fossil.am5.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.am5$f r0 = new com.fossil.am5$f
            r0.<init>(r4, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r5 = r0.L$1
            com.portfolio.platform.data.model.UserSettings r5 = (com.portfolio.platform.data.model.UserSettings) r5
            java.lang.Object r0 = r0.L$0
            com.fossil.am5 r0 = (com.fossil.am5) r0
            com.fossil.t87.a(r6)
            goto L_0x004c
        L_0x0031:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r6 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r6)
            throw r5
        L_0x0039:
            com.fossil.t87.a(r6)
            com.portfolio.platform.data.source.UserRepository r6 = r4.g
            r0.L$0 = r4
            r0.L$1 = r5
            r0.label = r3
            java.lang.Object r6 = r6.sendUserSettingToServer(r5, r0)
            if (r6 != r1) goto L_0x004b
            return r1
        L_0x004b:
            r0 = r4
        L_0x004c:
            com.fossil.zi5 r6 = (com.fossil.zi5) r6
            if (r6 == 0) goto L_0x0081
            boolean r1 = r6 instanceof com.fossil.bj5
            if (r1 == 0) goto L_0x0075
            com.fossil.bj5 r6 = (com.fossil.bj5) r6
            java.lang.Object r5 = r6.a()
            com.portfolio.platform.data.model.UserSettings r5 = (com.portfolio.platform.data.model.UserSettings) r5
            if (r5 == 0) goto L_0x0062
            r1 = 0
            r5.setPinType(r1)
        L_0x0062:
            com.portfolio.platform.data.source.UserSettingDao r5 = r0.h
            java.lang.Object r6 = r6.a()
            if (r6 == 0) goto L_0x0070
            com.portfolio.platform.data.model.UserSettings r6 = (com.portfolio.platform.data.model.UserSettings) r6
            r5.addOrUpdateUserSetting(r6)
            goto L_0x0081
        L_0x0070:
            com.fossil.ee7.a()
            r5 = 0
            throw r5
        L_0x0075:
            boolean r6 = r6 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x0081
            r5.setPinType(r3)
            com.portfolio.platform.data.source.UserSettingDao r6 = r0.h
            r6.addOrUpdateUserSetting(r5)
        L_0x0081:
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.am5.a(com.portfolio.platform.data.model.UserSettings, com.fossil.fb7):java.lang.Object");
    }
}
