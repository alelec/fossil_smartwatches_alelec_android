package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm3 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<wm3> CREATOR; // = new zm3();
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public em3 c;
    @DexIgnore
    public long d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public String f;
    @DexIgnore
    public ub3 g;
    @DexIgnore
    public long h;
    @DexIgnore
    public ub3 i;
    @DexIgnore
    public long j;
    @DexIgnore
    public ub3 p;

    @DexIgnore
    public wm3(wm3 wm3) {
        a72.a(wm3);
        this.a = wm3.a;
        this.b = wm3.b;
        this.c = wm3.c;
        this.d = wm3.d;
        this.e = wm3.e;
        this.f = wm3.f;
        this.g = wm3.g;
        this.h = wm3.h;
        this.i = wm3.i;
        this.j = wm3.j;
        this.p = wm3.p;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a, false);
        k72.a(parcel, 3, this.b, false);
        k72.a(parcel, 4, (Parcelable) this.c, i2, false);
        k72.a(parcel, 5, this.d);
        k72.a(parcel, 6, this.e);
        k72.a(parcel, 7, this.f, false);
        k72.a(parcel, 8, (Parcelable) this.g, i2, false);
        k72.a(parcel, 9, this.h);
        k72.a(parcel, 10, (Parcelable) this.i, i2, false);
        k72.a(parcel, 11, this.j);
        k72.a(parcel, 12, (Parcelable) this.p, i2, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public wm3(String str, String str2, em3 em3, long j2, boolean z, String str3, ub3 ub3, long j3, ub3 ub32, long j4, ub3 ub33) {
        this.a = str;
        this.b = str2;
        this.c = em3;
        this.d = j2;
        this.e = z;
        this.f = str3;
        this.g = ub3;
        this.h = j3;
        this.i = ub32;
        this.j = j4;
        this.p = ub33;
    }
}
