package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xf4 extends Exception {
    @DexIgnore
    public a errorType;
    @DexIgnore
    public String message;

    @DexIgnore
    public enum a {
        INVALID_COUNTRY_CODE,
        NOT_A_NUMBER,
        TOO_SHORT_AFTER_IDD,
        TOO_SHORT_NSN,
        TOO_LONG
    }

    @DexIgnore
    public xf4(a aVar, String str) {
        super(str);
        this.message = str;
        this.errorType = aVar;
    }

    @DexIgnore
    public a getErrorType() {
        return this.errorType;
    }

    @DexIgnore
    public String toString() {
        String valueOf = String.valueOf(String.valueOf(this.errorType));
        String valueOf2 = String.valueOf(String.valueOf(this.message));
        StringBuilder sb = new StringBuilder(valueOf.length() + 14 + valueOf2.length());
        sb.append("Error type: ");
        sb.append(valueOf);
        sb.append(". ");
        sb.append(valueOf2);
        return sb.toString();
    }
}
