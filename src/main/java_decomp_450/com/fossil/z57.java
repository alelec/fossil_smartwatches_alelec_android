package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z57 {
    @DexIgnore
    public static SharedPreferences a;

    @DexIgnore
    public static int a(Context context, String str, int i) {
        return a(context).getInt(v57.a(context, "wxop_" + str), i);
    }

    @DexIgnore
    public static long a(Context context, String str, long j) {
        return a(context).getLong(v57.a(context, "wxop_" + str), j);
    }

    @DexIgnore
    public static synchronized SharedPreferences a(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (z57.class) {
            SharedPreferences sharedPreferences2 = context.getSharedPreferences(".mta-wxop", 0);
            a = sharedPreferences2;
            if (sharedPreferences2 == null) {
                a = PreferenceManager.getDefaultSharedPreferences(context);
            }
            sharedPreferences = a;
        }
        return sharedPreferences;
    }

    @DexIgnore
    public static String a(Context context, String str, String str2) {
        return a(context).getString(v57.a(context, "wxop_" + str), str2);
    }

    @DexIgnore
    public static void b(Context context, String str, int i) {
        String a2 = v57.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putInt(a2, i);
        edit.commit();
    }

    @DexIgnore
    public static void b(Context context, String str, long j) {
        String a2 = v57.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putLong(a2, j);
        edit.commit();
    }

    @DexIgnore
    public static void b(Context context, String str, String str2) {
        String a2 = v57.a(context, "wxop_" + str);
        SharedPreferences.Editor edit = a(context).edit();
        edit.putString(a2, str2);
        edit.commit();
    }
}
