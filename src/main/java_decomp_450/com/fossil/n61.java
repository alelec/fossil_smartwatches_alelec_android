package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n61 extends zk0 {
    @DexIgnore
    public m60 C;

    @DexIgnore
    public n61(ri1 ri1, en0 en0, String str) {
        super(ri1, en0, wm0.d, str, false, 16);
        this.C = new m60(ri1.d(), ri1.u, "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, 262112);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new nj0(((zk0) this).w), new u21(this), new q41(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(super.k(), r51.a, this.C.a());
    }
}
