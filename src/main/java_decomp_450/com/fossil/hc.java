package com.fossil;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"BanParcelableUsage"})
public final class hc implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hc> CREATOR; // = new a();
    @DexIgnore
    public ArrayList<kc> a;
    @DexIgnore
    public ArrayList<String> b;
    @DexIgnore
    public zb[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public String e; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<hc> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public hc createFromParcel(Parcel parcel) {
            return new hc(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public hc[] newArray(int i) {
            return new hc[i];
        }
    }

    @DexIgnore
    public hc() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(this.a);
        parcel.writeStringList(this.b);
        parcel.writeTypedArray(this.c, i);
        parcel.writeInt(this.d);
        parcel.writeString(this.e);
    }

    @DexIgnore
    public hc(Parcel parcel) {
        this.a = parcel.createTypedArrayList(kc.CREATOR);
        this.b = parcel.createStringArrayList();
        this.c = (zb[]) parcel.createTypedArray(zb.CREATOR);
        this.d = parcel.readInt();
        this.e = parcel.readString();
    }
}
