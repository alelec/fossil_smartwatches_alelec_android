package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fa1 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zk0 a;
    @DexIgnore
    public /* final */ /* synthetic */ gd7 b;
    @DexIgnore
    public /* final */ /* synthetic */ gd7 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fa1(zk0 zk0, gd7 gd7, gd7 gd72) {
        super(1);
        this.a = zk0;
        this.b = gd7;
        this.c = gd72;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        zk0 zk02 = zk0;
        if (((Boolean) this.b.invoke(zk02.v)).booleanValue()) {
            zk0 zk03 = this.a;
            zk03.a(eu0.a(zk02.v, zk03.y, null, null, 6));
        } else {
            this.c.invoke(zk02);
        }
        return i97.a;
    }
}
