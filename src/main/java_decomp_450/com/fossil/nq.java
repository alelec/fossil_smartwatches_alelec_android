package com.fossil;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.Log;
import androidx.lifecycle.Lifecycle;
import coil.memory.RequestDelegate;
import com.fossil.eu;
import com.fossil.ib7;
import com.fossil.it;
import com.fossil.jq;
import com.fossil.qn7;
import com.fossil.rs;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq implements lq, eu {
    @DexIgnore
    public /* final */ yi7 a; // = zi7.a(dl7.a(null, 1, null).plus(qj7.c().g()));
    @DexIgnore
    public /* final */ CoroutineExceptionHandler b; // = new a(CoroutineExceptionHandler.n);
    @DexIgnore
    public /* final */ es c; // = new es(this, this.q);
    @DexIgnore
    public /* final */ rs d; // = new rs();
    @DexIgnore
    public /* final */ gr e; // = new gr(this.p);
    @DexIgnore
    public /* final */ ys f; // = new ys(this.i);
    @DexIgnore
    public /* final */ jq g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ Context i;
    @DexIgnore
    public /* final */ kq j;
    @DexIgnore
    public /* final */ rq p;
    @DexIgnore
    public /* final */ ds q;
    @DexIgnore
    public /* final */ ns r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends cb7 implements CoroutineExceptionHandler {
        @DexIgnore
        public a(ib7.c cVar) {
            super(cVar);
        }

        @DexIgnore
        @Override // kotlinx.coroutines.CoroutineExceptionHandler
        public void handleException(ib7 ib7, Throwable th) {
            ee7.b(ib7, "context");
            ee7.b(th, "exception");
            ju.a("RealImageLoader", th);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public rt a;
        @DexIgnore
        public /* final */ yi7 b;
        @DexIgnore
        public /* final */ st c;
        @DexIgnore
        public /* final */ ts d;
        @DexIgnore
        public /* final */ it e;

        @DexIgnore
        public c(yi7 yi7, st stVar, ts tsVar, it itVar) {
            ee7.b(yi7, "scope");
            ee7.b(stVar, "sizeResolver");
            ee7.b(tsVar, "targetDelegate");
            ee7.b(itVar, "request");
            this.b = yi7;
            this.c = stVar;
            this.d = tsVar;
            this.e = itVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.RealImageLoader$execute$2", f = "RealImageLoader.kt", l = {258}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super Drawable>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Object $data;
        @DexIgnore
        public /* final */ /* synthetic */ it $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends fe7 implements gd7<Throwable, i97> {
            @DexIgnore
            public /* final */ /* synthetic */ RequestDelegate $requestDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ ts $targetDelegate;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.nq$d$a$a")
            @tb7(c = "coil.RealImageLoader$execute$2$1$1", f = "RealImageLoader.kt", l = {251}, m = "invokeSuspend")
            /* renamed from: com.fossil.nq$d$a$a  reason: collision with other inner class name */
            public static final class C0142a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Throwable $throwable;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0142a(a aVar, Throwable th, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                    this.$throwable = th;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0142a aVar = new C0142a(this.this$0, this.$throwable, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0142a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        this.this$0.$requestDelegate.a();
                        Throwable th = this.$throwable;
                        if (th == null) {
                            return i97.a;
                        }
                        if (th instanceof CancellationException) {
                            if (cu.c.a() && cu.c.b() <= 4) {
                                Log.println(4, "RealImageLoader", "\ud83c\udfd7  Cancelled - " + this.this$0.this$0.$data);
                            }
                            it.a m = this.this$0.this$0.$request.m();
                            if (m != null) {
                                m.onCancel(this.this$0.this$0.$data);
                            }
                            return i97.a;
                        }
                        if (cu.c.a() && cu.c.b() <= 4) {
                            Log.println(4, "RealImageLoader", "\ud83d\udea8 Failed - " + this.this$0.this$0.$data + " - " + this.$throwable);
                        }
                        Drawable j = this.$throwable instanceof gt ? this.this$0.this$0.$request.j() : this.this$0.this$0.$request.i();
                        a aVar = this.this$0;
                        ts tsVar = aVar.$targetDelegate;
                        zt w = aVar.this$0.$request.w();
                        this.L$0 = yi7;
                        this.L$1 = j;
                        this.label = 1;
                        if (tsVar.a(j, w, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        Drawable drawable = (Drawable) this.L$1;
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    it.a m2 = this.this$0.this$0.$request.m();
                    if (m2 != null) {
                        m2.a(this.this$0.this$0.$data, this.$throwable);
                    }
                    return i97.a;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, RequestDelegate requestDelegate, ts tsVar) {
                super(1);
                this.this$0 = dVar;
                this.$requestDelegate = requestDelegate;
                this.$targetDelegate = tsVar;
            }

            @DexIgnore
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.gd7
            public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
                invoke(th);
                return i97.a;
            }

            @DexIgnore
            public final void invoke(Throwable th) {
                ik7 unused = xh7.b(this.this$0.this$0.a, qj7.c().g(), null, new C0142a(this, th, null), 2, null);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "coil.RealImageLoader$execute$2$deferred$1", f = "RealImageLoader.kt", l = {503, 548, 568, 215, 578, 230}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super Drawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Lifecycle $lifecycle;
            @DexIgnore
            public /* final */ /* synthetic */ ts $targetDelegate;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$13;
            @DexIgnore
            public Object L$14;
            @DexIgnore
            public Object L$15;
            @DexIgnore
            public Object L$16;
            @DexIgnore
            public Object L$17;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, Lifecycle lifecycle, ts tsVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$lifecycle = lifecycle;
                this.$targetDelegate = tsVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$lifecycle, this.$targetDelegate, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Drawable> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v22, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r2v32, resolved type: com.fossil.ns$b */
            /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r3v18, resolved type: android.graphics.Bitmap */
            /* JADX DEBUG: Multi-variable search result rejected for r3v19, resolved type: android.graphics.Bitmap */
            /* JADX DEBUG: Multi-variable search result rejected for r0v30, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r3v20, resolved type: android.graphics.Bitmap */
            /* JADX DEBUG: Multi-variable search result rejected for r2v34, resolved type: com.fossil.ns$b */
            /* JADX DEBUG: Multi-variable search result rejected for r2v38, resolved type: com.fossil.ns$b */
            /* JADX DEBUG: Multi-variable search result rejected for r2v42, resolved type: com.fossil.ns$b */
            /* JADX DEBUG: Multi-variable search result rejected for r0v31, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v32, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v33, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r3v27, resolved type: T */
            /* JADX DEBUG: Multi-variable search result rejected for r0v51, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v52, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v53, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v54, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r0v55, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX DEBUG: Multi-variable search result rejected for r3v40, resolved type: T */
            /* JADX DEBUG: Multi-variable search result rejected for r0v66, resolved type: android.graphics.drawable.BitmapDrawable */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARNING: Removed duplicated region for block: B:105:0x052f  */
            /* JADX WARNING: Removed duplicated region for block: B:107:0x0532  */
            /* JADX WARNING: Removed duplicated region for block: B:110:0x0553  */
            /* JADX WARNING: Removed duplicated region for block: B:111:0x055f  */
            /* JADX WARNING: Removed duplicated region for block: B:122:0x05c6  */
            /* JADX WARNING: Removed duplicated region for block: B:135:0x064f  */
            /* JADX WARNING: Removed duplicated region for block: B:138:0x0664  */
            /* JADX WARNING: Removed duplicated region for block: B:141:0x06df A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:142:0x06e0  */
            /* JADX WARNING: Removed duplicated region for block: B:145:0x070c  */
            /* JADX WARNING: Removed duplicated region for block: B:148:0x071f  */
            /* JADX WARNING: Removed duplicated region for block: B:151:0x075b  */
            /* JADX WARNING: Removed duplicated region for block: B:155:0x0795 A[RETURN] */
            /* JADX WARNING: Removed duplicated region for block: B:156:0x0796  */
            /* JADX WARNING: Removed duplicated region for block: B:159:0x07a2  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0259  */
            /* JADX WARNING: Removed duplicated region for block: B:47:0x0336  */
            /* JADX WARNING: Removed duplicated region for block: B:49:0x0347  */
            /* JADX WARNING: Removed duplicated region for block: B:60:0x039b  */
            /* JADX WARNING: Removed duplicated region for block: B:61:0x03a5  */
            /* JADX WARNING: Removed duplicated region for block: B:91:0x04cb  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r31) {
                /*
                    r30 = this;
                    r0 = r30
                    java.lang.Object r1 = com.fossil.nb7.a()
                    int r2 = r0.label
                    java.lang.String r3 = "RealImageLoader"
                    switch(r2) {
                        case 0: goto L_0x01a3;
                        case 1: goto L_0x013b;
                        case 2: goto L_0x00f1;
                        case 3: goto L_0x00b9;
                        case 4: goto L_0x008b;
                        case 5: goto L_0x004b;
                        case 6: goto L_0x0015;
                        default: goto L_0x000d;
                    }
                L_0x000d:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0015:
                    java.lang.Object r1 = r0.L$12
                    com.fossil.br r1 = (com.fossil.br) r1
                    java.lang.Object r2 = r0.L$11
                    android.graphics.drawable.Drawable r2 = (android.graphics.drawable.Drawable) r2
                    java.lang.Object r3 = r0.L$10
                    com.fossil.qt r3 = (com.fossil.qt) r3
                    java.lang.Object r3 = r0.L$9
                    com.fossil.rt r3 = (com.fossil.rt) r3
                    java.lang.Object r3 = r0.L$8
                    android.graphics.drawable.BitmapDrawable r3 = (android.graphics.drawable.BitmapDrawable) r3
                    java.lang.Object r3 = r0.L$7
                    com.fossil.ns$b r3 = (com.fossil.ns.b) r3
                    java.lang.Object r3 = r0.L$6
                    java.lang.String r3 = (java.lang.String) r3
                    java.lang.Object r3 = r0.L$5
                    com.fossil.pr r3 = (com.fossil.pr) r3
                    java.lang.Object r3 = r0.L$3
                    com.fossil.nq$c r3 = (com.fossil.nq.c) r3
                    java.lang.Object r3 = r0.L$2
                    com.fossil.st r3 = (com.fossil.st) r3
                    java.lang.Object r3 = r0.L$1
                    com.fossil.vt r3 = (com.fossil.vt) r3
                    java.lang.Object r3 = r0.L$0
                    com.fossil.yi7 r3 = (com.fossil.yi7) r3
                    com.fossil.t87.a(r31)
                    r13 = r0
                    goto L_0x0798
                L_0x004b:
                    java.lang.Object r2 = r0.L$13
                    com.fossil.it r2 = (com.fossil.it) r2
                    java.lang.Object r2 = r0.L$11
                    com.fossil.nq r2 = (com.fossil.nq) r2
                    java.lang.Object r2 = r0.L$10
                    com.fossil.qt r2 = (com.fossil.qt) r2
                    java.lang.Object r6 = r0.L$9
                    com.fossil.rt r6 = (com.fossil.rt) r6
                    java.lang.Object r7 = r0.L$8
                    android.graphics.drawable.BitmapDrawable r7 = (android.graphics.drawable.BitmapDrawable) r7
                    java.lang.Object r8 = r0.L$7
                    com.fossil.ns$b r8 = (com.fossil.ns.b) r8
                    java.lang.Object r9 = r0.L$6
                    java.lang.String r9 = (java.lang.String) r9
                    java.lang.Object r10 = r0.L$5
                    com.fossil.pr r10 = (com.fossil.pr) r10
                    java.lang.Object r11 = r0.L$4
                    java.lang.Object r12 = r0.L$3
                    com.fossil.nq$c r12 = (com.fossil.nq.c) r12
                    java.lang.Object r13 = r0.L$2
                    com.fossil.st r13 = (com.fossil.st) r13
                    java.lang.Object r14 = r0.L$1
                    com.fossil.vt r14 = (com.fossil.vt) r14
                    java.lang.Object r15 = r0.L$0
                    com.fossil.yi7 r15 = (com.fossil.yi7) r15
                    com.fossil.t87.a(r31)
                    r5 = r31
                    r25 = r3
                    r29 = r13
                    r13 = r0
                    r0 = r29
                    goto L_0x06ee
                L_0x008b:
                    java.lang.Object r1 = r0.L$10
                    com.fossil.qt r1 = (com.fossil.qt) r1
                    java.lang.Object r1 = r0.L$9
                    com.fossil.rt r1 = (com.fossil.rt) r1
                    java.lang.Object r1 = r0.L$8
                    android.graphics.drawable.BitmapDrawable r1 = (android.graphics.drawable.BitmapDrawable) r1
                    java.lang.Object r2 = r0.L$7
                    com.fossil.ns$b r2 = (com.fossil.ns.b) r2
                    java.lang.Object r2 = r0.L$6
                    java.lang.String r2 = (java.lang.String) r2
                    java.lang.Object r2 = r0.L$5
                    com.fossil.pr r2 = (com.fossil.pr) r2
                    java.lang.Object r2 = r0.L$3
                    com.fossil.nq$c r2 = (com.fossil.nq.c) r2
                    java.lang.Object r2 = r0.L$2
                    com.fossil.st r2 = (com.fossil.st) r2
                    java.lang.Object r2 = r0.L$1
                    com.fossil.vt r2 = (com.fossil.vt) r2
                    java.lang.Object r2 = r0.L$0
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    com.fossil.t87.a(r31)
                    r13 = r0
                    goto L_0x0645
                L_0x00b9:
                    java.lang.Object r2 = r0.L$10
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    java.lang.Object r6 = r0.L$9
                    com.fossil.nq$c r6 = (com.fossil.nq.c) r6
                    java.lang.Object r8 = r0.L$8
                    android.graphics.drawable.BitmapDrawable r8 = (android.graphics.drawable.BitmapDrawable) r8
                    java.lang.Object r9 = r0.L$7
                    com.fossil.ns$b r9 = (com.fossil.ns.b) r9
                    java.lang.Object r10 = r0.L$6
                    java.lang.String r10 = (java.lang.String) r10
                    java.lang.Object r11 = r0.L$5
                    com.fossil.pr r11 = (com.fossil.pr) r11
                    java.lang.Object r12 = r0.L$4
                    java.lang.Object r13 = r0.L$3
                    com.fossil.nq$c r13 = (com.fossil.nq.c) r13
                    java.lang.Object r14 = r0.L$2
                    com.fossil.st r14 = (com.fossil.st) r14
                    java.lang.Object r15 = r0.L$1
                    com.fossil.vt r15 = (com.fossil.vt) r15
                    java.lang.Object r5 = r0.L$0
                    com.fossil.yi7 r5 = (com.fossil.yi7) r5
                    com.fossil.t87.a(r31)
                    r4 = r31
                    r20 = r3
                    r29 = r9
                    r9 = r0
                    r0 = r29
                    goto L_0x059a
                L_0x00f1:
                    java.lang.Object r2 = r0.L$15
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    java.lang.Object r5 = r0.L$14
                    java.lang.StringBuilder r5 = (java.lang.StringBuilder) r5
                    java.lang.Object r8 = r0.L$13
                    android.graphics.drawable.BitmapDrawable r8 = (android.graphics.drawable.BitmapDrawable) r8
                    java.lang.Object r8 = r0.L$12
                    com.fossil.nq$c r8 = (com.fossil.nq.c) r8
                    java.lang.Object r9 = r0.L$11
                    java.lang.StringBuilder r9 = (java.lang.StringBuilder) r9
                    java.lang.Object r9 = r0.L$10
                    java.lang.StringBuilder r9 = (java.lang.StringBuilder) r9
                    java.lang.Object r10 = r0.L$9
                    java.lang.String r10 = (java.lang.String) r10
                    java.lang.Object r10 = r0.L$8
                    java.util.List r10 = (java.util.List) r10
                    java.lang.Object r10 = r0.L$7
                    com.fossil.ht r10 = (com.fossil.ht) r10
                    java.lang.Object r10 = r0.L$6
                    com.fossil.nq r10 = (com.fossil.nq) r10
                    java.lang.Object r10 = r0.L$5
                    com.fossil.pr r10 = (com.fossil.pr) r10
                    java.lang.Object r11 = r0.L$4
                    java.lang.Object r12 = r0.L$3
                    com.fossil.nq$c r12 = (com.fossil.nq.c) r12
                    java.lang.Object r13 = r0.L$2
                    com.fossil.st r13 = (com.fossil.st) r13
                    java.lang.Object r14 = r0.L$1
                    com.fossil.vt r14 = (com.fossil.vt) r14
                    java.lang.Object r15 = r0.L$0
                    com.fossil.yi7 r15 = (com.fossil.yi7) r15
                    com.fossil.t87.a(r31)
                    r4 = r0
                    r20 = r3
                    r0 = 0
                    r3 = r1
                    r1 = r31
                    goto L_0x048b
                L_0x013b:
                    java.lang.Object r2 = r0.L$17
                    com.fossil.yi7 r2 = (com.fossil.yi7) r2
                    java.lang.Object r5 = r0.L$16
                    android.graphics.drawable.BitmapDrawable r5 = (android.graphics.drawable.BitmapDrawable) r5
                    java.lang.Object r5 = r0.L$15
                    com.fossil.se7 r5 = (com.fossil.se7) r5
                    java.lang.Object r8 = r0.L$14
                    com.fossil.zr r8 = (com.fossil.zr) r8
                    java.lang.Object r9 = r0.L$13
                    java.lang.Object r10 = r0.L$12
                    com.fossil.nq$c r10 = (com.fossil.nq.c) r10
                    java.lang.Object r11 = r0.L$11
                    com.fossil.zr r11 = (com.fossil.zr) r11
                    java.lang.Object r11 = r0.L$10
                    java.lang.Class r11 = (java.lang.Class) r11
                    java.lang.Object r11 = r0.L$9
                    com.fossil.r87 r11 = (com.fossil.r87) r11
                    int r11 = r0.I$1
                    int r12 = r0.I$0
                    java.lang.Object r13 = r0.L$8
                    java.util.List r13 = (java.util.List) r13
                    java.lang.Object r14 = r0.L$7
                    com.fossil.se7 r14 = (com.fossil.se7) r14
                    java.lang.Object r15 = r0.L$6
                    com.fossil.nq$d$b r15 = (com.fossil.nq.d.b) r15
                    java.lang.Object r4 = r0.L$5
                    java.lang.Object r7 = r0.L$4
                    com.fossil.nq r7 = (com.fossil.nq) r7
                    java.lang.Object r6 = r0.L$3
                    com.fossil.nq$c r6 = (com.fossil.nq.c) r6
                    r19 = r1
                    java.lang.Object r1 = r0.L$2
                    com.fossil.st r1 = (com.fossil.st) r1
                    r20 = r1
                    java.lang.Object r1 = r0.L$1
                    com.fossil.vt r1 = (com.fossil.vt) r1
                    r21 = r1
                    java.lang.Object r1 = r0.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r31)
                    r22 = r15
                    r15 = r13
                    r13 = r12
                    r12 = r11
                    r11 = r10
                    r10 = r2
                    r2 = r20
                    r20 = r3
                    r3 = r9
                    r9 = r0
                    r0 = r31
                    r29 = r21
                    r21 = r14
                    r14 = r29
                    goto L_0x0304
                L_0x01a3:
                    r19 = r1
                    com.fossil.t87.a(r31)
                    com.fossil.yi7 r1 = r0.p$
                    com.fossil.nq$d r2 = r0.this$0
                    java.lang.Object r4 = r2.$data
                    if (r4 == 0) goto L_0x07ac
                    com.fossil.it r2 = r2.$request
                    com.fossil.it$a r2 = r2.m()
                    if (r2 == 0) goto L_0x01c1
                    com.fossil.nq$d r4 = r0.this$0
                    java.lang.Object r4 = r4.$data
                    r2.a(r4)
                    com.fossil.i97 r2 = com.fossil.i97.a
                L_0x01c1:
                    com.fossil.nq$d r2 = r0.this$0
                    java.lang.Object r4 = r2.$data
                    boolean r5 = r4 instanceof android.graphics.drawable.BitmapDrawable
                    if (r5 == 0) goto L_0x01e2
                    com.fossil.nq r2 = r2.this$0
                    com.fossil.ds r2 = r2.q
                    com.fossil.nq$d r4 = r0.this$0
                    java.lang.Object r4 = r4.$data
                    android.graphics.drawable.BitmapDrawable r4 = (android.graphics.drawable.BitmapDrawable) r4
                    android.graphics.Bitmap r4 = r4.getBitmap()
                    java.lang.String r5 = "data.bitmap"
                    com.fossil.ee7.a(r4, r5)
                    r2.c(r4)
                    goto L_0x01f5
                L_0x01e2:
                    boolean r4 = r4 instanceof android.graphics.Bitmap
                    if (r4 == 0) goto L_0x01f5
                    com.fossil.nq r2 = r2.this$0
                    com.fossil.ds r2 = r2.q
                    com.fossil.nq$d r4 = r0.this$0
                    java.lang.Object r4 = r4.$data
                    android.graphics.Bitmap r4 = (android.graphics.Bitmap) r4
                    r2.c(r4)
                L_0x01f5:
                    com.fossil.nq$d r2 = r0.this$0
                    com.fossil.it r2 = r2.$request
                    com.fossil.vt r2 = r2.u()
                    boolean r4 = r2 instanceof com.fossil.wt
                    if (r4 == 0) goto L_0x020d
                    boolean r4 = r2 instanceof com.fossil.rd
                    if (r4 == 0) goto L_0x020d
                    androidx.lifecycle.Lifecycle r4 = r0.$lifecycle
                    r5 = r2
                    com.fossil.rd r5 = (com.fossil.rd) r5
                    r4.a(r5)
                L_0x020d:
                    com.fossil.nq$d r4 = r0.this$0
                    com.fossil.nq r4 = r4.this$0
                    com.fossil.rs r4 = r4.d
                    com.fossil.nq$d r5 = r0.this$0
                    com.fossil.it r6 = r5.$request
                    com.fossil.nq r5 = r5.this$0
                    android.content.Context r5 = r5.i
                    com.fossil.st r4 = r4.a(r6, r5)
                    com.fossil.nq$c r5 = new com.fossil.nq$c
                    com.fossil.ts r6 = r0.$targetDelegate
                    com.fossil.nq$d r7 = r0.this$0
                    com.fossil.it r7 = r7.$request
                    r5.<init>(r1, r4, r6, r7)
                    com.fossil.nq$d r6 = r0.this$0
                    com.fossil.nq r7 = r6.this$0
                    java.lang.Object r6 = r6.$data
                    com.fossil.se7 r8 = new com.fossil.se7
                    r8.<init>()
                    r8.element = r6
                    com.fossil.jq r9 = r7.g
                    java.util.List r9 = r9.d()
                    int r10 = r9.size()
                    r15 = r0
                    r14 = r2
                    r2 = r4
                    r4 = r6
                    r13 = r9
                    r11 = r10
                    r12 = 0
                    r9 = r15
                    r6 = r1
                    r1 = r19
                    r29 = r8
                    r8 = r5
                    r5 = r29
                L_0x0257:
                    if (r12 >= r11) goto L_0x0336
                    java.lang.Object r10 = r13.get(r12)
                    com.fossil.r87 r10 = (com.fossil.r87) r10
                    java.lang.Object r19 = r10.component1()
                    r0 = r19
                    java.lang.Class r0 = (java.lang.Class) r0
                    java.lang.Object r19 = r10.component2()
                    r20 = r3
                    r3 = r19
                    com.fossil.zr r3 = (com.fossil.zr) r3
                    r19 = r1
                    T r1 = r5.element
                    java.lang.Class r1 = r1.getClass()
                    boolean r1 = r0.isAssignableFrom(r1)
                    if (r1 == 0) goto L_0x032c
                    if (r3 == 0) goto L_0x0324
                    T r1 = r5.element
                    boolean r1 = r3.a(r1)
                    if (r1 == 0) goto L_0x032c
                    T r1 = r5.element
                    r21 = r1
                    com.fossil.yi7 r1 = r8.b
                    com.fossil.rt r22 = r8.a
                    if (r22 == 0) goto L_0x029f
                    r1 = r21
                    r0 = r22
                    r21 = r5
                    goto L_0x0318
                L_0x029f:
                    r31 = r1
                    com.fossil.ts r1 = r8.d
                    com.fossil.it r22 = r8.e
                    r23 = r3
                    android.graphics.drawable.Drawable r3 = r22.q()
                    r22 = r0
                    r0 = 0
                    r1.a(r0, r3)
                    com.fossil.st r0 = r8.c
                    r9.L$0 = r6
                    r9.L$1 = r14
                    r9.L$2 = r2
                    r9.L$3 = r8
                    r9.L$4 = r7
                    r9.L$5 = r4
                    r9.L$6 = r15
                    r9.L$7 = r5
                    r9.L$8 = r13
                    r9.I$0 = r12
                    r9.I$1 = r11
                    r9.L$9 = r10
                    r1 = r22
                    r9.L$10 = r1
                    r1 = r23
                    r9.L$11 = r1
                    r9.L$12 = r8
                    r3 = r21
                    r9.L$13 = r3
                    r9.L$14 = r1
                    r9.L$15 = r5
                    r10 = 0
                    r9.L$16 = r10
                    r10 = r31
                    r9.L$17 = r10
                    r1 = 1
                    r9.label = r1
                    java.lang.Object r0 = r0.a(r15)
                    r1 = r19
                    if (r0 != r1) goto L_0x02f6
                    return r1
                L_0x02f6:
                    r19 = r1
                    r21 = r5
                    r1 = r6
                    r6 = r8
                    r22 = r15
                    r15 = r13
                    r13 = r12
                    r12 = r11
                    r11 = r6
                    r8 = r23
                L_0x0304:
                    com.fossil.rt r0 = (com.fossil.rt) r0
                    r11.a = r0
                    com.fossil.zi7.a(r10)
                    r11 = r12
                    r12 = r13
                    r13 = r15
                    r15 = r22
                    r29 = r6
                    r6 = r1
                    r1 = r3
                    r3 = r8
                    r8 = r29
                L_0x0318:
                    java.lang.Object r0 = r3.a(r1, r0)
                    r5.element = r0
                    r1 = r19
                    r5 = r21
                L_0x0322:
                    r0 = 1
                    goto L_0x032f
                L_0x0324:
                    com.fossil.x87 r0 = new com.fossil.x87
                    java.lang.String r1 = "null cannot be cast to non-null type coil.map.MeasuredMapper<kotlin.Any, *>"
                    r0.<init>(r1)
                    throw r0
                L_0x032c:
                    r1 = r19
                    goto L_0x0322
                L_0x032f:
                    int r12 = r12 + r0
                    r0 = r30
                    r3 = r20
                    goto L_0x0257
                L_0x0336:
                    r20 = r3
                    com.fossil.jq r0 = r7.g
                    java.util.List r0 = r0.c()
                    int r3 = r0.size()
                    r4 = 0
                L_0x0345:
                    if (r4 >= r3) goto L_0x0383
                    java.lang.Object r7 = r0.get(r4)
                    com.fossil.r87 r7 = (com.fossil.r87) r7
                    java.lang.Object r10 = r7.component1()
                    java.lang.Class r10 = (java.lang.Class) r10
                    java.lang.Object r7 = r7.component2()
                    com.fossil.yr r7 = (com.fossil.yr) r7
                    T r11 = r5.element
                    java.lang.Class r11 = r11.getClass()
                    boolean r10 = r10.isAssignableFrom(r11)
                    if (r10 == 0) goto L_0x0380
                    if (r7 == 0) goto L_0x0378
                    T r10 = r5.element
                    boolean r10 = r7.a(r10)
                    if (r10 == 0) goto L_0x0380
                    T r10 = r5.element
                    java.lang.Object r7 = r7.b(r10)
                    r5.element = r7
                    goto L_0x0380
                L_0x0378:
                    com.fossil.x87 r0 = new com.fossil.x87
                    java.lang.String r1 = "null cannot be cast to non-null type coil.map.Mapper<kotlin.Any, *>"
                    r0.<init>(r1)
                    throw r0
                L_0x0380:
                    int r4 = r4 + 1
                    goto L_0x0345
                L_0x0383:
                    T r11 = r5.element
                    com.fossil.nq$d r0 = r9.this$0
                    com.fossil.nq r0 = r0.this$0
                    com.fossil.jq r0 = r0.g
                    com.fossil.pr r10 = r0.a(r11)
                    com.fossil.nq$d r0 = r9.this$0
                    com.fossil.it r0 = r0.$request
                    java.lang.String r0 = r0.l()
                    if (r0 == 0) goto L_0x03a5
                    r5 = r6
                    r6 = r8
                    r12 = r11
                    r15 = r14
                    r14 = r2
                    r11 = r10
                    r10 = r0
                    r0 = 0
                    goto L_0x04bd
                L_0x03a5:
                    com.fossil.nq$d r0 = r9.this$0
                    com.fossil.nq r3 = r0.this$0
                    com.fossil.it r0 = r0.$request
                    com.fossil.ht r0 = r0.p()
                    com.fossil.nq$d r4 = r9.this$0
                    com.fossil.it r4 = r4.$request
                    java.util.List r4 = r4.v()
                    java.lang.String r5 = r10.b(r11)
                    if (r5 == 0) goto L_0x04b4
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder
                    r7.<init>()
                    r7.append(r5)
                    boolean r12 = r0.isEmpty()
                    r13 = 1
                    r12 = r12 ^ r13
                    if (r12 == 0) goto L_0x0406
                    java.util.Iterator r12 = r0.iterator()
                L_0x03d1:
                    boolean r15 = r12.hasNext()
                    if (r15 == 0) goto L_0x0406
                    java.lang.Object r15 = r12.next()
                    com.fossil.r87 r15 = (com.fossil.r87) r15
                    java.lang.Object r19 = r15.component1()
                    r13 = r19
                    java.lang.String r13 = (java.lang.String) r13
                    java.lang.Object r15 = r15.component2()
                    com.fossil.ht$c r15 = (com.fossil.ht.c) r15
                    java.lang.String r15 = r15.a()
                    r19 = r12
                    if (r15 == 0) goto L_0x0403
                    r12 = 35
                    r7.append(r12)
                    r7.append(r13)
                    r12 = 61
                    r7.append(r12)
                    r7.append(r15)
                L_0x0403:
                    r12 = r19
                    goto L_0x03d1
                L_0x0406:
                    boolean r12 = r4.isEmpty()
                    r13 = 1
                    r12 = r12 ^ r13
                    if (r12 == 0) goto L_0x049f
                    int r12 = r4.size()
                    r13 = 0
                L_0x0413:
                    if (r13 >= r12) goto L_0x042e
                    java.lang.Object r15 = r4.get(r13)
                    com.fossil.yt r15 = (com.fossil.yt) r15
                    r19 = r12
                    r12 = 35
                    r7.append(r12)
                    java.lang.String r15 = r15.key()
                    r7.append(r15)
                    int r13 = r13 + 1
                    r12 = r19
                    goto L_0x0413
                L_0x042e:
                    r12 = 35
                    r7.append(r12)
                    com.fossil.yi7 r12 = r8.b
                    com.fossil.rt r13 = r8.a
                    if (r13 == 0) goto L_0x0441
                    r4 = r9
                    r0 = 0
                    r9 = r7
                    goto L_0x0499
                L_0x0441:
                    com.fossil.ts r13 = r8.d
                    com.fossil.it r15 = r8.e
                    android.graphics.drawable.Drawable r15 = r15.q()
                    r19 = r1
                    r1 = 0
                    r13.a(r1, r15)
                    com.fossil.st r1 = r8.c
                    r9.L$0 = r6
                    r9.L$1 = r14
                    r9.L$2 = r2
                    r9.L$3 = r8
                    r9.L$4 = r11
                    r9.L$5 = r10
                    r9.L$6 = r3
                    r9.L$7 = r0
                    r9.L$8 = r4
                    r9.L$9 = r5
                    r9.L$10 = r7
                    r9.L$11 = r7
                    r9.L$12 = r8
                    r0 = 0
                    r9.L$13 = r0
                    r9.L$14 = r7
                    r9.L$15 = r12
                    r3 = 2
                    r9.label = r3
                    java.lang.Object r1 = r1.a(r9)
                    r3 = r19
                    if (r1 != r3) goto L_0x0484
                    return r3
                L_0x0484:
                    r13 = r2
                    r15 = r6
                    r5 = r7
                    r4 = r9
                    r2 = r12
                    r9 = r5
                    r12 = r8
                L_0x048b:
                    com.fossil.rt r1 = (com.fossil.rt) r1
                    r8.a = r1
                    com.fossil.zi7.a(r2)
                    r7 = r5
                    r8 = r12
                    r2 = r13
                    r6 = r15
                    r13 = r1
                    r1 = r3
                L_0x0499:
                    r7.append(r13)
                    r7 = r9
                    r9 = r4
                    goto L_0x04a1
                L_0x049f:
                    r3 = r1
                    r0 = 0
                L_0x04a1:
                    com.fossil.i97 r3 = com.fossil.i97.a
                    java.lang.String r3 = r7.toString()
                    java.lang.String r4 = "StringBuilder().apply(builderAction).toString()"
                    com.fossil.ee7.a(r3, r4)
                    r5 = r6
                    r6 = r8
                    r12 = r11
                    r15 = r14
                    r14 = r2
                    r11 = r10
                    r10 = r3
                    goto L_0x04bd
                L_0x04b4:
                    r3 = r1
                    r0 = 0
                    r5 = r6
                    r6 = r8
                    r12 = r11
                    r15 = r14
                    r14 = r2
                    r11 = r10
                    r10 = r0
                L_0x04bd:
                    com.fossil.nq$d r2 = r9.this$0
                    com.fossil.it r2 = r2.$request
                    com.fossil.dt r2 = r2.n()
                    boolean r2 = r2.getReadEnabled()
                    if (r2 == 0) goto L_0x0502
                    com.fossil.nq$d r2 = r9.this$0
                    com.fossil.nq r2 = r2.this$0
                    com.fossil.ns r2 = r2.r
                    com.fossil.ns$b r2 = com.fossil.iu.a(r2, r10)
                    if (r2 == 0) goto L_0x04da
                    goto L_0x0503
                L_0x04da:
                    com.fossil.nq$d r2 = r9.this$0
                    com.fossil.it r2 = r2.$request
                    java.util.List r2 = r2.a()
                    int r3 = r2.size()
                    r4 = 0
                L_0x04e7:
                    if (r4 >= r3) goto L_0x0502
                    java.lang.Object r7 = r2.get(r4)
                    java.lang.String r7 = (java.lang.String) r7
                    com.fossil.nq$d r8 = r9.this$0
                    com.fossil.nq r8 = r8.this$0
                    com.fossil.ns r8 = r8.r
                    com.fossil.ns$b r7 = com.fossil.iu.a(r8, r7)
                    if (r7 == 0) goto L_0x04ff
                    r2 = r7
                    goto L_0x0503
                L_0x04ff:
                    int r4 = r4 + 1
                    goto L_0x04e7
                L_0x0502:
                    r2 = r0
                L_0x0503:
                    if (r2 == 0) goto L_0x0549
                    android.graphics.Bitmap r3 = r2.a()
                    if (r3 == 0) goto L_0x0549
                    com.fossil.nq$d r4 = r9.this$0
                    com.fossil.nq r4 = r4.this$0
                    com.fossil.rs r4 = r4.d
                    com.fossil.nq$d r7 = r9.this$0
                    com.fossil.it r7 = r7.$request
                    android.graphics.Bitmap$Config r8 = r3.getConfig()
                    java.lang.String r13 = "it.config"
                    com.fossil.ee7.a(r8, r13)
                    boolean r4 = r4.a(r7, r8)
                    java.lang.Boolean r4 = com.fossil.pb7.a(r4)
                    boolean r4 = r4.booleanValue()
                    if (r4 == 0) goto L_0x052f
                    goto L_0x0530
                L_0x052f:
                    r3 = r0
                L_0x0530:
                    if (r3 == 0) goto L_0x0549
                    com.fossil.nq$d r0 = r9.this$0
                    com.fossil.nq r0 = r0.this$0
                    android.content.Context r0 = r0.i
                    android.content.res.Resources r0 = r0.getResources()
                    java.lang.String r4 = "context.resources"
                    com.fossil.ee7.a(r0, r4)
                    android.graphics.drawable.BitmapDrawable r4 = new android.graphics.drawable.BitmapDrawable
                    r4.<init>(r0, r3)
                    r0 = r4
                L_0x0549:
                    com.fossil.yi7 r3 = r6.b
                    com.fossil.rt r4 = r6.a
                    if (r4 == 0) goto L_0x055f
                    r13 = r9
                    r8 = r14
                    r7 = r15
                    r9 = r6
                    r14 = r10
                    r15 = r11
                    r10 = r12
                    r11 = r0
                    r12 = r2
                    r0 = r4
                    r6 = r5
                    goto L_0x05b4
                L_0x055f:
                    com.fossil.ts r4 = r6.d
                    if (r0 == 0) goto L_0x0567
                    r7 = r0
                    goto L_0x056f
                L_0x0567:
                    com.fossil.it r7 = r6.e
                    android.graphics.drawable.Drawable r7 = r7.q()
                L_0x056f:
                    r4.a(r0, r7)
                    com.fossil.st r4 = r6.c
                    r9.L$0 = r5
                    r9.L$1 = r15
                    r9.L$2 = r14
                    r9.L$3 = r6
                    r9.L$4 = r12
                    r9.L$5 = r11
                    r9.L$6 = r10
                    r9.L$7 = r2
                    r9.L$8 = r0
                    r9.L$9 = r6
                    r9.L$10 = r3
                    r7 = 3
                    r9.label = r7
                    java.lang.Object r4 = r4.a(r9)
                    if (r4 != r1) goto L_0x0596
                    return r1
                L_0x0596:
                    r8 = r0
                    r0 = r2
                    r2 = r3
                    r13 = r6
                L_0x059a:
                    com.fossil.rt r4 = (com.fossil.rt) r4
                    r6.a = r4
                    com.fossil.i97 r3 = com.fossil.i97.a
                    com.fossil.zi7.a(r2)
                    com.fossil.i97 r2 = com.fossil.i97.a
                    r6 = r5
                    r7 = r15
                    r15 = r11
                    r11 = r8
                    r8 = r14
                    r14 = r10
                    r10 = r12
                    r12 = r0
                    r0 = r4
                    r29 = r13
                    r13 = r9
                    r9 = r29
                L_0x05b4:
                    com.fossil.nq$d r2 = r13.this$0
                    com.fossil.nq r2 = r2.this$0
                    com.fossil.rs r2 = r2.d
                    com.fossil.nq$d r3 = r13.this$0
                    com.fossil.it r3 = r3.$request
                    com.fossil.qt r5 = r2.a(r3, r8)
                    if (r11 == 0) goto L_0x0664
                    com.fossil.nq$d r2 = r13.this$0
                    com.fossil.nq r2 = r2.this$0
                    boolean r4 = r12.c()
                    com.fossil.nq$d r3 = r13.this$0
                    com.fossil.it r3 = r3.$request
                    r18 = r3
                    r3 = r11
                    r31 = r5
                    r5 = r0
                    r19 = r1
                    r1 = r6
                    r6 = r31
                    r21 = r0
                    r0 = r7
                    r7 = r18
                    boolean r2 = r2.a(r3, r4, r5, r6, r7)
                    if (r2 == 0) goto L_0x065b
                    com.fossil.cu r2 = com.fossil.cu.c
                    boolean r2 = r2.a()
                    if (r2 == 0) goto L_0x0613
                    com.fossil.cu r2 = com.fossil.cu.c
                    int r2 = r2.b()
                    r3 = 4
                    if (r2 > r3) goto L_0x0613
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder
                    r2.<init>()
                    java.lang.String r4 = "\ud83e\udde0  Cached - "
                    r2.append(r4)
                    com.fossil.nq$d r4 = r13.this$0
                    java.lang.Object r4 = r4.$data
                    r2.append(r4)
                    java.lang.String r2 = r2.toString()
                    r7 = r20
                    android.util.Log.println(r3, r7, r2)
                L_0x0613:
                    com.fossil.ts r2 = r13.$targetDelegate
                    com.fossil.nq$d r3 = r13.this$0
                    com.fossil.it r3 = r3.$request
                    com.fossil.zt r3 = r3.w()
                    r13.L$0 = r1
                    r13.L$1 = r0
                    r13.L$2 = r8
                    r13.L$3 = r9
                    r13.L$4 = r10
                    r13.L$5 = r15
                    r13.L$6 = r14
                    r13.L$7 = r12
                    r13.L$8 = r11
                    r6 = r21
                    r13.L$9 = r6
                    r5 = r31
                    r13.L$10 = r5
                    r0 = 4
                    r13.label = r0
                    r0 = 1
                    java.lang.Object r0 = r2.a(r11, r0, r3, r13)
                    r4 = r19
                    if (r0 != r4) goto L_0x0644
                    return r4
                L_0x0644:
                    r1 = r11
                L_0x0645:
                    com.fossil.nq$d r0 = r13.this$0
                    com.fossil.it r0 = r0.$request
                    com.fossil.it$a r0 = r0.m()
                    if (r0 == 0) goto L_0x065a
                    com.fossil.nq$d r2 = r13.this$0
                    java.lang.Object r2 = r2.$data
                    com.fossil.br r3 = com.fossil.br.MEMORY
                    r0.a(r2, r3)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x065a:
                    return r1
                L_0x065b:
                    r5 = r31
                    r4 = r19
                    r7 = r20
                    r6 = r21
                    goto L_0x066a
                L_0x0664:
                    r4 = r1
                    r1 = r6
                    r6 = r0
                    r0 = r7
                    r7 = r20
                L_0x066a:
                    com.fossil.nq$d r2 = r13.this$0
                    com.fossil.nq r3 = r2.this$0
                    r31 = r11
                    java.lang.Object r11 = r2.$data
                    com.fossil.it r2 = r2.$request
                    r17 = r12
                    com.fossil.ti7 r12 = r2.h()
                    r18 = r12
                    com.fossil.oq r12 = new com.fossil.oq
                    r19 = 0
                    r20 = r2
                    r2 = r12
                    r21 = r3
                    r24 = r4
                    r4 = r20
                    r22 = r5
                    r5 = r6
                    r23 = r12
                    r12 = r6
                    r6 = r22
                    r25 = r7
                    r7 = r15
                    r26 = r12
                    r12 = r8
                    r8 = r10
                    r27 = r14
                    r14 = r9
                    r9 = r11
                    r28 = r11
                    r11 = r10
                    r10 = r19
                    r2.<init>(r3, r4, r5, r6, r7, r8, r9, r10)
                    r13.L$0 = r1
                    r13.L$1 = r0
                    r13.L$2 = r12
                    r13.L$3 = r14
                    r13.L$4 = r11
                    r13.L$5 = r15
                    r10 = r27
                    r13.L$6 = r10
                    r2 = r17
                    r13.L$7 = r2
                    r8 = r31
                    r13.L$8 = r8
                    r4 = r26
                    r13.L$9 = r4
                    r3 = r22
                    r13.L$10 = r3
                    r5 = r21
                    r13.L$11 = r5
                    r5 = r28
                    r13.L$12 = r5
                    r5 = r20
                    r13.L$13 = r5
                    r5 = 5
                    r13.label = r5
                    r5 = r18
                    r6 = r23
                    java.lang.Object r5 = com.fossil.vh7.a(r5, r6, r13)
                    r6 = r24
                    if (r5 != r6) goto L_0x06e0
                    return r6
                L_0x06e0:
                    r7 = r8
                    r9 = r10
                    r10 = r15
                    r15 = r1
                    r8 = r2
                    r2 = r3
                    r1 = r6
                    r6 = r4
                    r29 = r14
                    r14 = r0
                    r0 = r12
                    r12 = r29
                L_0x06ee:
                    com.fossil.nr r5 = (com.fossil.nr) r5
                    android.graphics.drawable.Drawable r3 = r5.a()
                    boolean r4 = r5.b()
                    com.fossil.br r5 = r5.c()
                    r17 = r1
                    com.fossil.nq$d r1 = r13.this$0
                    com.fossil.it r1 = r1.$request
                    com.fossil.dt r1 = r1.n()
                    boolean r1 = r1.getWriteEnabled()
                    if (r1 == 0) goto L_0x0717
                    com.fossil.nq$d r1 = r13.this$0
                    com.fossil.nq r1 = r1.this$0
                    com.fossil.ns r1 = r1.r
                    com.fossil.iu.a(r1, r9, r3, r4)
                L_0x0717:
                    com.fossil.cu r1 = com.fossil.cu.c
                    boolean r1 = r1.a()
                    if (r1 == 0) goto L_0x075b
                    com.fossil.cu r1 = com.fossil.cu.c
                    int r1 = r1.b()
                    r31 = r4
                    r4 = 4
                    if (r1 > r4) goto L_0x075d
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r4 = com.fossil.iu.a(r5)
                    r1.append(r4)
                    java.lang.String r4 = " Successful ("
                    r1.append(r4)
                    java.lang.String r4 = r5.name()
                    r1.append(r4)
                    java.lang.String r4 = ") - "
                    r1.append(r4)
                    com.fossil.nq$d r4 = r13.this$0
                    java.lang.Object r4 = r4.$data
                    r1.append(r4)
                    java.lang.String r1 = r1.toString()
                    r16 = r5
                    r4 = r25
                    r5 = 4
                    android.util.Log.println(r5, r4, r1)
                    goto L_0x075f
                L_0x075b:
                    r31 = r4
                L_0x075d:
                    r16 = r5
                L_0x075f:
                    com.fossil.ts r1 = r13.$targetDelegate
                    com.fossil.nq$d r4 = r13.this$0
                    com.fossil.it r4 = r4.$request
                    com.fossil.zt r4 = r4.w()
                    r13.L$0 = r15
                    r13.L$1 = r14
                    r13.L$2 = r0
                    r13.L$3 = r12
                    r13.L$4 = r11
                    r13.L$5 = r10
                    r13.L$6 = r9
                    r13.L$7 = r8
                    r13.L$8 = r7
                    r13.L$9 = r6
                    r13.L$10 = r2
                    r13.L$11 = r3
                    r0 = r31
                    r13.Z$0 = r0
                    r0 = r16
                    r13.L$12 = r0
                    r2 = 6
                    r13.label = r2
                    r2 = 0
                    java.lang.Object r1 = r1.a(r3, r2, r4, r13)
                    r6 = r17
                    if (r1 != r6) goto L_0x0796
                    return r6
                L_0x0796:
                    r1 = r0
                    r2 = r3
                L_0x0798:
                    com.fossil.nq$d r0 = r13.this$0
                    com.fossil.it r0 = r0.$request
                    com.fossil.it$a r0 = r0.m()
                    if (r0 == 0) goto L_0x07ab
                    com.fossil.nq$d r3 = r13.this$0
                    java.lang.Object r3 = r3.$data
                    r0.a(r3, r1)
                    com.fossil.i97 r0 = com.fossil.i97.a
                L_0x07ab:
                    return r2
                L_0x07ac:
                    com.fossil.gt r0 = new com.fossil.gt
                    r0.<init>()
                    throw r0
                    switch-data {0->0x01a3, 1->0x013b, 2->0x00f1, 3->0x00b9, 4->0x008b, 5->0x004b, 6->0x0015, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.nq.d.b.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(nq nqVar, it itVar, Object obj, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nqVar;
            this.$request = itVar;
            this.$data = obj;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$request, this.$data, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Drawable> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.b();
                rs.b c = this.this$0.d.c(this.$request);
                Lifecycle a3 = c.a();
                ti7 b2 = c.b();
                ts a4 = this.this$0.c.a(this.$request);
                hj7<? extends Drawable> a5 = vh7.a(yi7, b2, bj7.LAZY, new b(this, a3, a4, null));
                RequestDelegate a6 = this.this$0.c.a(this.$request, a4, a3, b2, a5);
                a5.b(new a(this, a6, a4));
                this.L$0 = yi7;
                this.L$1 = a3;
                this.L$2 = b2;
                this.L$3 = a4;
                this.L$4 = a5;
                this.L$5 = a6;
                this.label = 1;
                obj = a5.c(this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                RequestDelegate requestDelegate = (RequestDelegate) this.L$5;
                hj7 hj7 = (hj7) this.L$4;
                ts tsVar = (ts) this.L$3;
                ti7 ti7 = (ti7) this.L$2;
                Lifecycle lifecycle = (Lifecycle) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.RealImageLoader$load$job$1", f = "RealImageLoader.kt", l = {ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ et $request;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nq this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(nq nqVar, et etVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nqVar;
            this.$request = etVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$request, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                nq nqVar = this.this$0;
                Object y = this.$request.y();
                et etVar = this.$request;
                this.L$0 = yi7;
                this.label = 1;
                if (nqVar.a(y, etVar, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new b(null);
    }
    */

    @DexIgnore
    public nq(Context context, kq kqVar, rq rqVar, ds dsVar, ns nsVar, qn7.a aVar, jq jqVar) {
        ee7.b(context, "context");
        ee7.b(kqVar, Constants.DEFAULTS);
        ee7.b(rqVar, "bitmapPool");
        ee7.b(dsVar, "referenceCounter");
        ee7.b(nsVar, "memoryCache");
        ee7.b(aVar, "callFactory");
        ee7.b(jqVar, "registry");
        this.i = context;
        this.j = kqVar;
        this.p = rqVar;
        this.q = dsVar;
        this.r = nsVar;
        jq.a e2 = jqVar.e();
        e2.a(String.class, new cs());
        e2.a(Uri.class, new xr());
        e2.a(Uri.class, new bs(this.i));
        e2.a(Integer.class, new as(this.i));
        e2.a(Uri.class, new sr(aVar));
        e2.a(go7.class, new tr(aVar));
        e2.a(File.class, new qr());
        e2.a(Uri.class, new jr(this.i));
        e2.a(Uri.class, new lr(this.i));
        e2.a(Uri.class, new ur(this.i, this.e));
        e2.a(Drawable.class, new mr(this.i, this.e));
        e2.a(Bitmap.class, new kr(this.i));
        e2.a(new ar(this.i));
        this.g = e2.a();
        this.i.registerComponentCallbacks(this);
    }

    @DexIgnore
    public void onConfigurationChanged(Configuration configuration) {
        eu.a.a(this, configuration);
    }

    @DexIgnore
    public void onLowMemory() {
        eu.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.eu
    public void onTrimMemory(int i2) {
        this.r.a(i2);
        this.p.a(i2);
    }

    @DexIgnore
    @Override // com.fossil.lq
    public synchronized void shutdown() {
        if (!this.h) {
            this.h = true;
            zi7.a(this.a, null, 1, null);
            this.i.unregisterComponentCallbacks(this);
            this.f.c();
            c();
        }
    }

    @DexIgnore
    @Override // com.fossil.lq
    public kq a() {
        return this.j;
    }

    @DexIgnore
    public final void b() {
        if (!(!this.h)) {
            throw new IllegalStateException("The image loader is shutdown!".toString());
        }
    }

    @DexIgnore
    public void c() {
        onTrimMemory(80);
    }

    @DexIgnore
    @Override // com.fossil.lq
    public kt a(et etVar) {
        ee7.b(etVar, "request");
        ik7 b2 = xh7.b(this.a, this.b, null, new e(this, etVar, null), 2, null);
        if (etVar.u() instanceof wt) {
            return new lt(iu.a(((wt) etVar.u()).getView()).a(b2), (wt) etVar.u());
        }
        return new ct(b2);
    }

    @DexIgnore
    public final /* synthetic */ Object a(Object obj, it itVar, fb7<? super Drawable> fb7) {
        return vh7.a(qj7.c().g(), new d(this, itVar, obj, null), fb7);
    }

    @DexIgnore
    public final boolean a(BitmapDrawable bitmapDrawable, boolean z, rt rtVar, qt qtVar, it itVar) {
        ee7.b(bitmapDrawable, "cached");
        ee7.b(rtVar, "size");
        ee7.b(qtVar, "scale");
        ee7.b(itVar, "request");
        Bitmap bitmap = bitmapDrawable.getBitmap();
        if (rtVar instanceof nt) {
            if (z) {
                return false;
            }
        } else if (rtVar instanceof ot) {
            ee7.a((Object) bitmap, "bitmap");
            ot otVar = (ot) rtVar;
            double b2 = er.b(bitmap.getWidth(), bitmap.getHeight(), otVar.d(), otVar.c(), qtVar);
            if (b2 != 1.0d && !this.d.a(itVar)) {
                return false;
            }
            if (b2 > 1.0d && z) {
                return false;
            }
        }
        rs rsVar = this.d;
        ee7.a((Object) bitmap, "bitmap");
        Bitmap.Config config = bitmap.getConfig();
        ee7.a((Object) config, "bitmap.config");
        if (!rsVar.a(itVar, config)) {
            return false;
        }
        if ((!itVar.c() || bitmap.getConfig() != Bitmap.Config.RGB_565) && iu.c(bitmap.getConfig()) != iu.c(itVar.d())) {
            return false;
        }
        return true;
    }
}
