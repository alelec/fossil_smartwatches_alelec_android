package com.fossil;

import com.portfolio.platform.workers.TimeChangeReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c07 implements MembersInjector<TimeChangeReceiver> {
    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, pd5 pd5) {
        timeChangeReceiver.a = pd5;
    }

    @DexIgnore
    public static void a(TimeChangeReceiver timeChangeReceiver, ch5 ch5) {
        timeChangeReceiver.b = ch5;
    }
}
