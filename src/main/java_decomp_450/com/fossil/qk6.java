package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk6 implements Factory<pk6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public qk6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static qk6 a(Provider<ThemeRepository> provider) {
        return new qk6(provider);
    }

    @DexIgnore
    public static pk6 a(ThemeRepository themeRepository) {
        return new pk6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pk6 get() {
        return a(this.a.get());
    }
}
