package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kd5 extends iw {
    @DexIgnore
    public kd5(aw awVar, p30 p30, u30 u30, Context context) {
        super(awVar, p30, u30, context);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Bitmap> b() {
        return (jd5) super.b();
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Drawable> c() {
        return (jd5) super.c();
    }

    @DexIgnore
    @Override // com.fossil.iw
    public <ResourceType> jd5<ResourceType> a(Class<ResourceType> cls) {
        return new jd5<>(((iw) this).a, this, cls, ((iw) this).b);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Drawable> a(String str) {
        return (jd5) super.a(str);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Drawable> a(Uri uri) {
        return (jd5) super.a(uri);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Drawable> a(Integer num) {
        return (jd5) super.a(num);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public jd5<Drawable> a(Object obj) {
        return (jd5) super.a(obj);
    }

    @DexIgnore
    @Override // com.fossil.iw
    public void a(r40 r40) {
        if (r40 instanceof id5) {
            super.a(r40);
        } else {
            super.a((r40) new id5().a((k40<?>) r40));
        }
    }
}
