package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq0 {
    @DexIgnore
    public /* synthetic */ nq0(zd7 zd7) {
    }

    @DexIgnore
    public final is0 a(sz0 sz0) {
        switch (ro0.a[sz0.c.ordinal()]) {
            case 1:
                return is0.SUCCESS;
            case 2:
                return is0.INTERRUPTED;
            case 3:
                return is0.CONNECTION_DROPPED;
            case 4:
                return is0.REQUEST_UNSUPPORTED;
            case 5:
                return is0.UNSUPPORTED_FILE_HANDLE;
            case 6:
                return is0.BLUETOOTH_OFF;
            case 7:
                return is0.REQUEST_UNSUPPORTED;
            case 8:
                return is0.HID_INPUT_DEVICE_DISABLED;
            case 9:
                if (sz0.e == dr0.f) {
                    return is0.UNSUPPORTED_FILE_HANDLE;
                }
                return is0.REQUEST_ERROR;
            default:
                return is0.REQUEST_ERROR;
        }
    }
}
