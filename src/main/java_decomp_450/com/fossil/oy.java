package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oy<Z> implements uy<Z> {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ uy<Z> c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ yw e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(yw ywVar, oy<?> oyVar);
    }

    @DexIgnore
    public oy(uy<Z> uyVar, boolean z, boolean z2, yw ywVar, a aVar) {
        u50.a(uyVar);
        this.c = uyVar;
        this.a = z;
        this.b = z2;
        this.e = ywVar;
        u50.a(aVar);
        this.d = aVar;
    }

    @DexIgnore
    public synchronized void a() {
        if (!this.g) {
            this.f++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public synchronized void b() {
        if (this.f > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.g) {
            this.g = true;
            if (this.b) {
                this.c.b();
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return this.c.c();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<Z> d() {
        return this.c.d();
    }

    @DexIgnore
    public uy<Z> e() {
        return this.c;
    }

    @DexIgnore
    public boolean f() {
        return this.a;
    }

    @DexIgnore
    public void g() {
        boolean z;
        synchronized (this) {
            if (this.f > 0) {
                z = true;
                int i = this.f - 1;
                this.f = i;
                if (i != 0) {
                    z = false;
                }
            } else {
                throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
            }
        }
        if (z) {
            this.d.a(this.e, this);
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Z get() {
        return this.c.get();
    }

    @DexIgnore
    public synchronized String toString() {
        return "EngineResource{isMemoryCacheable=" + this.a + ", listener=" + this.d + ", key=" + this.e + ", acquired=" + this.f + ", isRecycled=" + this.g + ", resource=" + this.c + '}';
    }
}
