package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca3 implements Parcelable.Creator<k93> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ k93 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        LatLng latLng = null;
        String str = null;
        String str2 = null;
        IBinder iBinder = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f4 = 0.5f;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f6 = 1.0f;
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    latLng = (LatLng) j72.a(parcel, a, LatLng.CREATOR);
                    break;
                case 3:
                    str = j72.e(parcel, a);
                    break;
                case 4:
                    str2 = j72.e(parcel, a);
                    break;
                case 5:
                    iBinder = j72.p(parcel, a);
                    break;
                case 6:
                    f = j72.n(parcel, a);
                    break;
                case 7:
                    f2 = j72.n(parcel, a);
                    break;
                case 8:
                    z = j72.i(parcel, a);
                    break;
                case 9:
                    z2 = j72.i(parcel, a);
                    break;
                case 10:
                    z3 = j72.i(parcel, a);
                    break;
                case 11:
                    f3 = j72.n(parcel, a);
                    break;
                case 12:
                    f4 = j72.n(parcel, a);
                    break;
                case 13:
                    f5 = j72.n(parcel, a);
                    break;
                case 14:
                    f6 = j72.n(parcel, a);
                    break;
                case 15:
                    f7 = j72.n(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new k93(latLng, str, str2, iBinder, f, f2, z, z2, z3, f3, f4, f5, f6, f7);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ k93[] newArray(int i) {
        return new k93[i];
    }
}
