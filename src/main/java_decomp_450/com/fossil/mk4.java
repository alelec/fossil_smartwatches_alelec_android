package com.fossil;

import com.portfolio.platform.data.source.local.inapp.InAppNotificationRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mk4 implements Factory<co6> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<InAppNotificationRepository> b;

    @DexIgnore
    public mk4(wj4 wj4, Provider<InAppNotificationRepository> provider) {
        this.a = wj4;
        this.b = provider;
    }

    @DexIgnore
    public static mk4 a(wj4 wj4, Provider<InAppNotificationRepository> provider) {
        return new mk4(wj4, provider);
    }

    @DexIgnore
    public static co6 a(wj4 wj4, InAppNotificationRepository inAppNotificationRepository) {
        co6 a2 = wj4.a(inAppNotificationRepository);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public co6 get() {
        return a(this.a, this.b.get());
    }
}
