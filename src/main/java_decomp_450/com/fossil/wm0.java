package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class wm0 extends Enum<wm0> {
    @DexIgnore
    public static /* final */ wm0 A;
    @DexIgnore
    public static /* final */ wm0 A0;
    @DexIgnore
    public static /* final */ wm0 B;
    @DexIgnore
    public static /* final */ wm0 B0;
    @DexIgnore
    public static /* final */ wm0 C;
    @DexIgnore
    public static /* final */ wm0 C0;
    @DexIgnore
    public static /* final */ wm0 D;
    @DexIgnore
    public static /* final */ wm0 D0;
    @DexIgnore
    public static /* final */ wm0 E;
    @DexIgnore
    public static /* final */ wm0 E0;
    @DexIgnore
    public static /* final */ wm0 F;
    @DexIgnore
    public static /* final */ wm0 F0;
    @DexIgnore
    public static /* final */ wm0 G;
    @DexIgnore
    public static /* final */ wm0 G0;
    @DexIgnore
    public static /* final */ wm0 H;
    @DexIgnore
    public static /* final */ wm0 H0;
    @DexIgnore
    public static /* final */ wm0 I;
    @DexIgnore
    public static /* final */ wm0 I0;
    @DexIgnore
    public static /* final */ wm0 J;
    @DexIgnore
    public static /* final */ wm0 J0;
    @DexIgnore
    public static /* final */ wm0 K;
    @DexIgnore
    public static /* final */ /* synthetic */ wm0[] K0;
    @DexIgnore
    public static /* final */ wm0 L;
    @DexIgnore
    public static /* final */ wm0 M;
    @DexIgnore
    public static /* final */ wm0 N;
    @DexIgnore
    public static /* final */ wm0 O;
    @DexIgnore
    public static /* final */ wm0 P;
    @DexIgnore
    public static /* final */ wm0 Q;
    @DexIgnore
    public static /* final */ wm0 R;
    @DexIgnore
    public static /* final */ wm0 S;
    @DexIgnore
    public static /* final */ wm0 T;
    @DexIgnore
    public static /* final */ wm0 U;
    @DexIgnore
    public static /* final */ wm0 V;
    @DexIgnore
    public static /* final */ wm0 W;
    @DexIgnore
    public static /* final */ wm0 X;
    @DexIgnore
    public static /* final */ wm0 Y;
    @DexIgnore
    public static /* final */ wm0 Z;
    @DexIgnore
    public static /* final */ wm0 a;
    @DexIgnore
    public static /* final */ wm0 a0;
    @DexIgnore
    public static /* final */ wm0 b;
    @DexIgnore
    public static /* final */ wm0 b0;
    @DexIgnore
    public static /* final */ wm0 c;
    @DexIgnore
    public static /* final */ wm0 c0;
    @DexIgnore
    public static /* final */ wm0 d;
    @DexIgnore
    public static /* final */ wm0 d0;
    @DexIgnore
    public static /* final */ wm0 e;
    @DexIgnore
    public static /* final */ wm0 e0;
    @DexIgnore
    public static /* final */ wm0 f;
    @DexIgnore
    public static /* final */ wm0 f0;
    @DexIgnore
    public static /* final */ wm0 g;
    @DexIgnore
    public static /* final */ wm0 g0;
    @DexIgnore
    public static /* final */ wm0 h;
    @DexIgnore
    public static /* final */ wm0 h0;
    @DexIgnore
    public static /* final */ wm0 i;
    @DexIgnore
    public static /* final */ wm0 i0;
    @DexIgnore
    public static /* final */ wm0 j;
    @DexIgnore
    public static /* final */ wm0 j0;
    @DexIgnore
    public static /* final */ wm0 k;
    @DexIgnore
    public static /* final */ wm0 k0;
    @DexIgnore
    public static /* final */ wm0 l;
    @DexIgnore
    public static /* final */ wm0 l0;
    @DexIgnore
    public static /* final */ wm0 m;
    @DexIgnore
    public static /* final */ wm0 m0;
    @DexIgnore
    public static /* final */ wm0 n;
    @DexIgnore
    public static /* final */ wm0 n0;
    @DexIgnore
    public static /* final */ wm0 o;
    @DexIgnore
    public static /* final */ wm0 o0;
    @DexIgnore
    public static /* final */ wm0 p;
    @DexIgnore
    public static /* final */ wm0 p0;
    @DexIgnore
    public static /* final */ wm0 q;
    @DexIgnore
    public static /* final */ wm0 q0;
    @DexIgnore
    public static /* final */ wm0 r;
    @DexIgnore
    public static /* final */ wm0 r0;
    @DexIgnore
    public static /* final */ wm0 s;
    @DexIgnore
    public static /* final */ wm0 s0;
    @DexIgnore
    public static /* final */ wm0 t;
    @DexIgnore
    public static /* final */ wm0 t0;
    @DexIgnore
    public static /* final */ wm0 u;
    @DexIgnore
    public static /* final */ wm0 u0;
    @DexIgnore
    public static /* final */ wm0 v;
    @DexIgnore
    public static /* final */ wm0 v0;
    @DexIgnore
    public static /* final */ wm0 w;
    @DexIgnore
    public static /* final */ wm0 w0;
    @DexIgnore
    public static /* final */ wm0 x;
    @DexIgnore
    public static /* final */ wm0 x0;
    @DexIgnore
    public static /* final */ wm0 y;
    @DexIgnore
    public static /* final */ wm0 y0;
    @DexIgnore
    public static /* final */ wm0 z;
    @DexIgnore
    public static /* final */ wm0 z0;

    /*
    static {
        wm0 wm0 = new wm0("UNKNOWN", 0);
        a = wm0;
        wm0 wm02 = new wm0("MAKE_DEVICE_READY", 1);
        b = wm02;
        wm0 wm03 = new wm0("READ_DEVICE_INFO_FILE", 2);
        c = wm03;
        wm0 wm04 = new wm0("READ_DEVICE_INFO_CHARACTERISTICS", 3);
        d = wm04;
        wm0 wm05 = new wm0("READ_RSSI", 4);
        e = wm05;
        wm0 wm06 = new wm0("STREAMING", 5);
        f = wm06;
        wm0 wm07 = new wm0("OTA", 6);
        g = wm07;
        wm0 wm08 = new wm0("DISCONNECT", 7);
        h = wm08;
        wm0 wm09 = new wm0("PUT_FILE", 9);
        i = wm09;
        wm0 wm010 = new wm0("SET_CONNECTION_PARAMS", 11);
        j = wm010;
        wm0 wm011 = new wm0("PLAY_ANIMATION", 13);
        k = wm011;
        wm0 wm012 = new wm0("GET_FILE", 14);
        l = wm012;
        wm0 wm013 = new wm0("SYNC", 15);
        m = wm013;
        wm0 wm014 = new wm0("GET_HARDWARE_LOG", 16);
        n = wm014;
        wm0 wm015 = new wm0("SET_ALARMS", 17);
        o = wm015;
        wm0 wm016 = new wm0("GET_ALARMS", 18);
        p = wm016;
        wm0 wm017 = new wm0("REQUEST_HANDS", 19);
        q = wm017;
        wm0 wm018 = new wm0("RELEASE_HANDS", 20);
        r = wm018;
        wm0 wm019 = new wm0("MOVE_HANDS", 21);
        s = wm019;
        wm0 wm020 = new wm0("SET_CALIBRATION_POSITION", 22);
        t = wm020;
        wm0 wm021 = new wm0("SET_DEVICE_CONFIGS", 23);
        u = wm021;
        wm0 wm022 = new wm0("GET_DEVICE_CONFIGS", 24);
        v = wm022;
        wm0 wm023 = new wm0("SET_COMPLICATION", 26);
        w = wm023;
        wm0 wm024 = new wm0("SET_WATCH_APP", 28);
        x = wm024;
        wm0 wm025 = new wm0("SEND_APP_NOTIFICATION", 33);
        y = wm025;
        wm0 wm026 = new wm0("SEND_TRACK_INFO", 34);
        z = wm026;
        wm0 wm027 = new wm0("NOTIFY_MUSIC_EVENT", 35);
        A = wm027;
        wm0 wm028 = new wm0("SET_FRONT_LIGHT_ENABLE", 37);
        B = wm028;
        wm0 wm029 = new wm0("SET_BACKGROUND_IMAGE", 38);
        C = wm029;
        wm0 wm030 = new wm0("GET_BACKGROUND_IMAGE", 39);
        D = wm030;
        wm0 wm031 = new wm0("PUT_BACKGROUND_IMAGE_DATA", 40);
        E = wm031;
        wm0 wm032 = new wm0("PUT_BACKGROUND_IMAGE_CONFIG", 41);
        F = wm032;
        wm0 wm033 = new wm0("GET_NOTIFICATION_FILTER", 42);
        G = wm033;
        wm0 wm034 = new wm0("SET_NOTIFICATION_FILTER", 43);
        H = wm034;
        wm0 wm035 = new wm0("GET_CURRENT_WORKOUT_SESSION", 44);
        I = wm035;
        wm0 wm036 = new wm0("STOP_CURRENT_WORKOUT_SESSION", 45);
        J = wm036;
        wm0 wm037 = new wm0("CLEAN_UP_DEVICE", 46);
        K = wm037;
        wm0 wm038 = new wm0("SEND_ASYNC_EVENT_ACK", 50);
        L = wm038;
        wm0 wm039 = new wm0("FIND_CORRECT_OFFSET", 51);
        M = wm039;
        wm0 wm040 = new wm0("SYNC_IN_BACKGROUND", 52);
        N = wm040;
        wm0 wm041 = new wm0("GET_HARDWARE_LOG_IN_BACKGROUND", 53);
        O = wm041;
        wm0 wm042 = new wm0("START_AUTHENTICATION", 55);
        P = wm042;
        wm0 wm043 = new wm0("EXCHANGE_SECRET_KEY", 56);
        Q = wm043;
        wm0 wm044 = new wm0("AUTHENTICATE", 57);
        R = wm044;
        wm0 wm045 = new wm0("TRY_SET_CONNECTION_PARAMS", 58);
        S = wm045;
        wm0 wm046 = new wm0("VERIFY_SECRET_KEY", 59);
        T = wm046;
        wm0 wm047 = new wm0("SEND_DEVICE_DATA", 60);
        U = wm047;
        wm0 wm048 = new wm0("PUT_NOTIFICATION_ICON", 61);
        V = wm048;
        wm0 wm049 = new wm0("PUT_NOTIFICATION_FILTER_RULE", 62);
        W = wm049;
        wm0 wm050 = new wm0("GET_NOTIFICATION_ICON", 63);
        X = wm050;
        wm0 wm051 = new wm0("GET_NOTIFICATION_FILTER_RULE", 64);
        Y = wm051;
        wm0 wm052 = new wm0("SET_DEVICE_PRESET", 65);
        Z = wm052;
        wm0 wm053 = new wm0("PUT_PRESET_CONFIG", 66);
        a0 = wm053;
        wm0 wm054 = new wm0("GET_DEVICE_PRESET", 67);
        b0 = wm054;
        wm0 wm055 = new wm0("GET_PRESET_CONFIG", 68);
        c0 = wm055;
        wm0 wm056 = new wm0("PUT_LOCALIZATION_FILE", 69);
        d0 = wm056;
        wm0 wm057 = new wm0("CREATE_BOND", 70);
        e0 = wm057;
        wm0 wm058 = new wm0("CONFIGURE_MICRO_APP", 71);
        f0 = wm058;
        wm0 wm059 = new wm0("CONNECT_HID", 72);
        g0 = wm059;
        wm0 wm060 = new wm0("DISCONNECT_HID", 73);
        h0 = wm060;
        wm0 wm061 = new wm0("PUT_WATCH_PARAMETERS_FILE", 74);
        i0 = wm061;
        wm0 wm062 = new wm0("LEGACY_OTA", 76);
        j0 = wm062;
        wm0 wm063 = new wm0("LEGACY_SYNC", 77);
        k0 = wm063;
        wm0 wm064 = new wm0("SEND_CUSTOM_COMMAND", 78);
        l0 = wm064;
        wm0 wm065 = new wm0("SYNC_FLOW", 79);
        m0 = wm065;
        wm0 wm066 = new wm0("NOTIFY_APP_NOTIFICATION_EVENT", 80);
        n0 = wm066;
        wm0 wm067 = new wm0("GET_DATA_COLLECTION_FILE", 81);
        o0 = wm067;
        wm0 wm068 = new wm0("GET_DATA_COLLECTION_FILE_IN_BACKGROUND", 82);
        p0 = wm068;
        wm0 wm069 = new wm0("CDG_GET_BATTERY", 83);
        q0 = wm069;
        wm0 wm070 = new wm0("CDG_GET_AVERAGE_RSSI", 84);
        r0 = wm070;
        wm0 wm071 = new wm0("CDG_VIBE", 85);
        s0 = wm071;
        wm0 wm072 = new wm0("CDG_START_STREAMING_ACCEL", 88);
        t0 = wm072;
        wm0 wm073 = new wm0("CDG_ABORT_STREAMING_ACCEL", 89);
        u0 = wm073;
        wm0 wm074 = new wm0("GET_INSTALLED_UI_PACKAGE", 93);
        v0 = wm074;
        wm0 wm075 = new wm0("INSTALL_UI_PACKAGE", 94);
        w0 = wm075;
        wm0 wm076 = new wm0("UNINSTALL_UI_PACKAGE", 95);
        x0 = wm076;
        wm0 wm077 = new wm0("FETCH_DEVICE_INFORMATION", 96);
        y0 = wm077;
        wm0 wm078 = new wm0("CONFIRM_AUTHORIZATION", 97);
        z0 = wm078;
        wm0 wm079 = new wm0("SEND_ENCRYPTED_DATA", 99);
        A0 = wm079;
        wm0 wm080 = new wm0("SET_REPLY_MESSAGE", 101);
        B0 = wm080;
        wm0 wm081 = new wm0("PUT_REPLY_MESSAGE_ICON", 102);
        C0 = wm081;
        wm0 wm082 = new wm0("PUT_REPLY_MESSAGE_DATA", 103);
        D0 = wm082;
        wm0 wm083 = new wm0("SET_BUDDY_CHALLENGE_MINIMUM_STEP_THRESHOLD", 104);
        E0 = wm083;
        wm0 wm084 = new wm0("SET_UP_WATCH_APPS", 105);
        F0 = wm084;
        wm0 wm085 = new wm0("DELETE_FILE", 106);
        G0 = wm085;
        wm0 wm086 = new wm0("SET_WORKOUT_ROUTE_IMAGE", 107);
        H0 = wm086;
        wm0 wm087 = new wm0("PUT_WORKOUT_ROUTE_IMAGE_DATA", 108);
        I0 = wm087;
        wm0 wm088 = new wm0("PUT_ELABEL_FILE", 109);
        J0 = wm088;
        K0 = new wm0[]{wm0, wm02, wm03, wm04, wm05, wm06, wm07, wm08, new wm0("CLOSE", 8), wm09, new wm0("SET_CONNECTION_PRIORITY", 10), wm010, new wm0("GET_CONNECTION_PARAMS", 12), wm011, wm012, wm013, wm014, wm015, wm016, wm017, wm018, wm019, wm020, wm021, wm022, new wm0("PUT_JSON_OBJECT", 25), wm023, new wm0("SET_COMPLICATION_CONFIG", 27), wm024, new wm0("SEND_DEVICE_RESPONSE", 29), new wm0("FIND_THE_FASTEST_CONNECTION_INTERVAL", 30), new wm0("FIND_MAXIMUM_CONNECTION_INTERVAL", 31), new wm0("FIND_OPTIMAL_CONNECTION_PARAMETERS", 32), wm025, wm026, wm027, new wm0("ERASE_DATA", 36), wm028, wm029, wm030, wm031, wm032, wm033, wm034, wm035, wm036, wm037, new wm0("GET_HEARTBEAT_STATISTIC", 47), new wm0("GET_HEARTBEAT_INTERVAL", 48), new wm0("SET_HEARTBEAT_INTERVAL", 49), wm038, wm039, wm040, wm041, new wm0("SEND_BACKGROUND_SYNC_ACK", 54), wm042, wm043, wm044, wm045, wm046, wm047, wm048, wm049, wm050, wm051, wm052, wm053, wm054, wm055, wm056, wm057, wm058, wm059, wm060, wm061, new wm0("TROUBLESHOOT_DEVICE_BLE", 75), wm062, wm063, wm064, wm065, wm066, wm067, wm068, wm069, wm070, wm071, new wm0("CDG_SUBSCRIBE_HEART_RATE_CHARACTERISTIC", 86), new wm0("CDG_UNSUBSCRIBE_HEART_RATE_CHARACTERISTIC", 87), wm072, wm073, new wm0("CDG_GET_CHARGING_STATUS", 90), new wm0("CDG_SUBSCRIBE_ASYNC_CHARACTERISTIC", 91), new wm0("CDG_UNSUBSCRIBE_ASYNC_CHARACTERISTIC", 92), wm074, wm075, wm076, wm077, wm078, new wm0("GET_PDK_CONFIG", 98), wm079, new wm0("SET_BUDDY_CHALLENGE_FITNESS_DATA", 100), wm080, wm081, wm082, wm083, wm084, wm085, wm086, wm087, wm088};
    }
    */

    @DexIgnore
    public wm0(String str, int i2) {
    }

    @DexIgnore
    public static wm0 valueOf(String str) {
        return (wm0) Enum.valueOf(wm0.class, str);
    }

    @DexIgnore
    public static wm0[] values() {
        return (wm0[]) K0.clone();
    }
}
