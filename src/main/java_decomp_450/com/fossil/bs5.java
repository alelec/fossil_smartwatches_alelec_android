package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl4;
import com.fossil.fn5;
import com.fossil.nj5;
import com.fossil.pm5;
import com.fossil.ql4;
import com.fossil.sn5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs5 extends qr5 {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ a y; // = new a(null);
    @DexIgnore
    public int e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public LiveData<List<InAppNotification>> h;
    @DexIgnore
    public /* final */ f i; // = new f(this);
    @DexIgnore
    public /* final */ i j; // = new i(this);
    @DexIgnore
    public /* final */ h k; // = new h(this);
    @DexIgnore
    public /* final */ rr5 l;
    @DexIgnore
    public /* final */ ch5 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ pm5 p;
    @DexIgnore
    public /* final */ rl4 q;
    @DexIgnore
    public /* final */ ad5 r;
    @DexIgnore
    public /* final */ sn5 s;
    @DexIgnore
    public /* final */ wx6 t;
    @DexIgnore
    public /* final */ ServerSettingRepository u;
    @DexIgnore
    public /* final */ fn5 v;
    @DexIgnore
    public /* final */ lm4 w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bs5.x;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1", f = "HomePresenter.kt", l = {281}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1", f = "HomePresenter.kt", l = {281}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    yw6 a2 = yw6.h.a();
                    String str = this.this$0.$activeDeviceSerial;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = a2.a(str, null, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(bs5 bs5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bs5;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$activeDeviceSerial, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                FLogger.INSTANCE.getLocal().d(bs5.y.a(), "checkFirmware() enter checkFirmware");
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean booleanValue = ((Boolean) obj).booleanValue();
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.$activeDeviceSerial;
            String a4 = bs5.y.a();
            remote.i(component, session, str, a4, "[Sync OK] Is device has latest FW " + booleanValue);
            if (booleanValue) {
                FLogger.INSTANCE.getLocal().d(bs5.y.a(), "checkFirmware() fw is latest, skip OTA after sync success");
            } else {
                this.this$0.p();
            }
            FLogger.INSTANCE.getLocal().d(bs5.y.a(), "checkFirmware() exit checkFirmware");
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ql4.d<sn5.a.c, sn5.a.C0172a> {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(sn5.a.c cVar) {
            FLogger.INSTANCE.getLocal().d(bs5.y.a(), "GetServerSettingUseCase onSuccess");
            this.a.a(true);
        }

        @DexIgnore
        public void a(sn5.a.C0172a aVar) {
            FLogger.INSTANCE.getLocal().d(bs5.y.a(), "GetServerSettingUseCase onError");
            this.a.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1", f = "HomePresenter.kt", l = {331}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $hasServerSettings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1", f = "HomePresenter.kt", l = {351, 352}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bs5$d$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.bs5$d$a$a  reason: collision with other inner class name */
            public static final class C0020a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0020a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0020a aVar = new C0020a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0020a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.this$0.l.v0();
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                yi7 yi7;
                ServerSetting serverSetting;
                ServerSetting serverSetting2;
                ServerSetting serverSetting3;
                long j;
                int i;
                int i2;
                Long a;
                Integer a2;
                Integer a3;
                Object a4 = nb7.a();
                int i3 = this.label;
                if (i3 == 0) {
                    t87.a(obj);
                    yi7 = this.p$;
                    ServerSetting generateSetting = this.this$0.this$0.u.generateSetting("crash-threshold", String.valueOf(3));
                    ServerSetting generateSetting2 = this.this$0.this$0.u.generateSetting("successful-sync-threshold", String.valueOf(4));
                    serverSetting3 = this.this$0.this$0.u.generateSetting("post-sync-threshold", String.valueOf(5));
                    d dVar = this.this$0;
                    if (dVar.$hasServerSettings) {
                        ServerSetting serverSettingByKey = dVar.this$0.u.getServerSettingByKey("crash-threshold");
                        if (serverSettingByKey != null) {
                            generateSetting = serverSettingByKey;
                        }
                        ServerSetting serverSettingByKey2 = this.this$0.this$0.u.getServerSettingByKey("successful-sync-threshold");
                        if (serverSettingByKey2 != null) {
                            generateSetting2 = serverSettingByKey2;
                        }
                        ServerSetting serverSettingByKey3 = this.this$0.this$0.u.getServerSettingByKey("post-sync-threshold");
                        if (serverSettingByKey3 != null) {
                            serverSetting3 = serverSettingByKey3;
                        }
                    }
                    serverSetting = generateSetting;
                    serverSetting2 = generateSetting2;
                    String value = serverSetting.getValue();
                    int intValue = (value == null || (a3 = pb7.a(Integer.parseInt(value))) == null) ? 0 : a3.intValue();
                    String value2 = serverSetting2.getValue();
                    int intValue2 = (value2 == null || (a2 = pb7.a(Integer.parseInt(value2))) == null) ? 0 : a2.intValue();
                    String value3 = serverSetting3.getValue();
                    long longValue = (value3 == null || (a = pb7.a(Long.parseLong(value3))) == null) ? 0 : a.longValue();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a5 = bs5.y.a();
                    local.d(a5, "Crash threshold: " + serverSetting + ", sync success: " + serverSetting2 + ", post sync: " + serverSetting3);
                    if (this.this$0.this$0.t.a() && this.this$0.this$0.t.a(intValue, intValue2)) {
                        this.L$0 = yi7;
                        this.L$1 = serverSetting;
                        this.L$2 = serverSetting2;
                        this.L$3 = serverSetting3;
                        this.I$0 = intValue;
                        this.I$1 = intValue2;
                        this.J$0 = longValue;
                        this.label = 1;
                        if (kj7.a(longValue, this) == a4) {
                            return a4;
                        }
                        i = intValue;
                        i2 = intValue2;
                        j = longValue;
                    }
                    return i97.a;
                } else if (i3 == 1) {
                    j = this.J$0;
                    i2 = this.I$1;
                    i = this.I$0;
                    serverSetting3 = (ServerSetting) this.L$3;
                    serverSetting2 = (ServerSetting) this.L$2;
                    serverSetting = (ServerSetting) this.L$1;
                    yi7 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i3 == 2) {
                    ServerSetting serverSetting4 = (ServerSetting) this.L$3;
                    ServerSetting serverSetting5 = (ServerSetting) this.L$2;
                    ServerSetting serverSetting6 = (ServerSetting) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    this.this$0.this$0.m.p(false);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                tk7 l = this.this$0.this$0.d();
                C0020a aVar = new C0020a(this, null);
                this.L$0 = yi7;
                this.L$1 = serverSetting;
                this.L$2 = serverSetting2;
                this.L$3 = serverSetting3;
                this.I$0 = i;
                this.I$1 = i2;
                this.J$0 = j;
                this.label = 2;
                if (vh7.a(l, aVar, this) == a4) {
                    return a4;
                }
                this.this$0.this$0.m.p(false);
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(bs5 bs5, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bs5;
            this.$hasServerSettings = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$hasServerSettings, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 d = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(d, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1", f = "HomePresenter.kt", l = {252}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.n.getDeviceBySerial(this.this$0.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(bs5 bs5, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bs5;
            this.$activeSerial = str;
            this.$lastFwTemp = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$activeSerial, this.$lastFwTemp, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            String str = null;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 d = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(d, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a3 = bs5.y.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
            if (device != null) {
                str = device.getFirmwareRevision();
            }
            sb.append(str);
            local.d(a3, sb.toString());
            if (device != null && !mh7.b(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(bs5.y.a(), "Handle OTA complete on check last OTA success");
                this.this$0.m.n(this.$activeSerial);
                this.this$0.m.a(this.this$0.o.c(), -1);
                this.this$0.l.c(true);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, this.a.o.c(), true)) {
                this.a.l.a(intent);
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                int intExtra3 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = bs5.y.a();
                local.d(a2, "Inside .syncReceiver syncResult=" + intExtra + ", serial=" + stringExtra + ", " + "errorCode=" + intExtra2 + " permissionErrors " + integerArrayListExtra + " originalSyncMode " + intExtra3);
                if (intExtra == 1) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    ee7.a((Object) stringExtra, "serial");
                    remote.i(component, session, stringExtra, bs5.y.a(), "[Sync OK] Check FW to OTA");
                    this.a.k();
                    this.a.l();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.HomePresenter$onActiveSerialChange$1", f = "HomePresenter.kt", l = {173}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $appMode;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bs5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(bs5 bs5, String str, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bs5;
            this.$serial = str;
            this.$appMode = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$appMode, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0096  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r5) {
            /*
                r4 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r4.label
                r2 = 1
                if (r1 == 0) goto L_0x001b
                if (r1 != r2) goto L_0x0013
                java.lang.Object r0 = r4.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r5)
                goto L_0x0052
            L_0x0013:
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r5.<init>(r0)
                throw r5
            L_0x001b:
                com.fossil.t87.a(r5)
                com.fossil.yi7 r5 = r4.p$
                com.fossil.bs5 r1 = r4.this$0
                boolean r1 = r1.g
                if (r1 == 0) goto L_0x003c
                com.fossil.bs5 r5 = r4.this$0
                com.fossil.ch5 r5 = r5.m
                java.lang.Boolean r5 = r5.a()
                java.lang.String r0 = "mSharedPreferencesManager.bcStatus()"
                com.fossil.ee7.a(r5, r0)
                boolean r5 = r5.booleanValue()
                goto L_0x0065
            L_0x003c:
                com.fossil.bs5 r1 = r4.this$0
                r1.g = r2
                com.fossil.bs5 r1 = r4.this$0
                com.fossil.lm4 r1 = r1.w
                r4.L$0 = r5
                r4.label = r2
                java.lang.Object r5 = r1.a(r4)
                if (r5 != r0) goto L_0x0052
                return r0
            L_0x0052:
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                com.fossil.bs5 r0 = r4.this$0
                com.fossil.ch5 r0 = r0.m
                java.lang.Boolean r1 = com.fossil.pb7.a(r5)
                r0.b(r1)
            L_0x0065:
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                com.fossil.bs5$a r1 = com.fossil.bs5.y
                java.lang.String r1 = r1.a()
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "isBCOn: "
                r2.append(r3)
                r2.append(r5)
                java.lang.String r2 = r2.toString()
                r0.e(r1, r2)
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r0.a(r5)
                java.lang.String r0 = r4.$serial
                boolean r0 = android.text.TextUtils.isEmpty(r0)
                if (r0 != 0) goto L_0x00a8
                com.fossil.bs5 r0 = r4.this$0
                com.fossil.rr5 r0 = r0.l
                java.lang.String r1 = r4.$serial
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r1)
                int r2 = r4.$appMode
                r0.a(r1, r2, r5)
                goto L_0x00b4
            L_0x00a8:
                com.fossil.bs5 r0 = r4.this$0
                com.fossil.rr5 r0 = r0.l
                r1 = 0
                int r2 = r4.$appMode
                r0.a(r1, r2, r5)
            L_0x00b4:
                com.fossil.i97 r5 = com.fossil.i97.a
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.bs5.g.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements nj5.b {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public h(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            this.a.b(false);
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            boolean D = this.a.o.D();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bs5.y.a();
            local.d(a2, "ota complete isSuccess" + booleanExtra + " isDeviceStillOta " + D);
            if (!D || !booleanExtra) {
                this.a.l.c(booleanExtra);
            }
            if (booleanExtra) {
                this.a.m.a(this.a.o.c(), -1);
                PortfolioApp.g0.c().a(this.a.r, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public i(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bs5.y.a();
            local.d(a2, "---Inside .otaProgressReceiver ota progress " + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial())) {
                if (!this.a.n()) {
                    this.a.b(true);
                }
                if (mh7.b(otaEvent.getSerial(), this.a.o.c(), true)) {
                    this.a.l.f((int) otaEvent.getProcess());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements fl4.e<fn5.d, fn5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ fn5.d $responseValue;

            @DexIgnore
            public a(fn5.d dVar) {
                this.$responseValue = dVar;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a = bs5.y.a();
                local.d(a, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public j(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        public void a(fn5.b bVar) {
            ee7.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(bs5.y.a(), "startCollectingUserFeedback onError");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(fn5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(bs5.y.a(), "startCollectingUserFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.l.b(new a(dVar));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements fl4.e<pm5.d, pm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ bs5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(bs5 bs5) {
            this.a = bs5;
        }

        @DexIgnore
        public void a(pm5.c cVar) {
            ee7.b(cVar, "errorValue");
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(pm5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.b(true);
            this.a.l.W();
        }
    }

    /*
    static {
        String simpleName = bs5.class.getSimpleName();
        ee7.a((Object) simpleName, "HomePresenter::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public bs5(rr5 rr5, ch5 ch5, DeviceRepository deviceRepository, PortfolioApp portfolioApp, pm5 pm5, rl4 rl4, ad5 ad5, sn5 sn5, wx6 wx6, ServerSettingRepository serverSettingRepository, fn5 fn5, lm4 lm4) {
        ee7.b(rr5, "mView");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(portfolioApp, "mApp");
        ee7.b(pm5, "mUpdateFwUseCase");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(sn5, "mGetServerSettingUseCase");
        ee7.b(wx6, "mUserUtils");
        ee7.b(serverSettingRepository, "mServerSettingRepository");
        ee7.b(fn5, "mGetZendeskInformation");
        ee7.b(lm4, "flagRepository");
        this.l = rr5;
        this.m = ch5;
        this.n = deviceRepository;
        this.o = portfolioApp;
        this.p = pm5;
        this.q = rl4;
        this.r = ad5;
        this.s = sn5;
        this.t = wx6;
        this.u = serverSettingRepository;
        this.v = fn5;
        this.w = lm4;
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public void a(InAppNotification inAppNotification) {
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public void j() {
    }

    @DexIgnore
    public final boolean n() {
        return this.f;
    }

    @DexIgnore
    public void o() {
        this.l.a(this);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .updateFirmware");
        String c2 = this.o.c();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, c2, x, "[Sync OK] Start update FW");
        if (!TextUtils.isEmpty(c2)) {
            this.p.a(new pm5.b(c2, false, 2, null), new k(this));
        }
    }

    @DexIgnore
    public final void b(boolean z) {
        this.f = z;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(x, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        PortfolioApp portfolioApp = this.o;
        i iVar = this.j;
        portfolioApp.registerReceiver(iVar, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        nj5.d.a(this.k, CommunicateMode.OTA);
        nj5.d.a(this.i, CommunicateMode.SYNC);
        nj5.d.a(CommunicateMode.SYNC, CommunicateMode.OTA);
        m();
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(x, "stop");
        try {
            nj5.d.b(this.k, CommunicateMode.OTA);
            nj5.d.b(this.i, CommunicateMode.SYNC);
            this.o.unregisterReceiver(this.j);
            LiveData<List<InAppNotification>> liveData = this.h;
            if (liveData != null) {
                rr5 rr5 = this.l;
                if (rr5 != null) {
                    liveData.a((wr5) rr5);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "Exception when stop presenter=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public int h() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public boolean i() {
        return this.f || this.o.D();
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "checkFirmware(), isSkipOTA=" + this.m.a0());
        if (!PortfolioApp.g0.e() || !this.m.a0()) {
            String c2 = this.o.c();
            boolean D = this.o.D();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "checkFirmware() isDeviceOtaing=" + D);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str3 = x;
            remote.i(component, session, c2, str3, "[Sync OK] Is device OTAing " + D);
            if (!D) {
                ik7 unused = xh7.b(e(), null, null, new b(this, c2, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final void l() {
        this.q.a(this.s, new sn5.a.b(), new c(this));
    }

    @DexIgnore
    public final void m() {
        String c2 = this.o.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "Inside .checkLastOTASuccess, activeSerial=" + c2);
        if (!TextUtils.isEmpty(c2)) {
            String g2 = this.m.g(c2);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "Inside .checkLastOTASuccess, lastTempFw=" + g2);
            if (!TextUtils.isEmpty(g2)) {
                ik7 unused = xh7.b(e(), null, null, new e(this, c2, g2, null), 3, null);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public void a(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setTab " + i2);
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public void b(String str) {
        ee7.b(str, "subject");
        this.v.a(new fn5.c(str), new j(this));
    }

    @DexIgnore
    @Override // com.fossil.qr5
    public void a(String str) {
        int i2;
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "on active serial change serial " + str);
        if (TextUtils.isEmpty(this.o.v().B())) {
            i2 = 0;
        } else {
            i2 = (TextUtils.isEmpty(str) || !be5.o.e(str)) ? 1 : 2;
        }
        ik7 unused = xh7.b(e(), null, null, new g(this, str, i2, null), 3, null);
    }

    @DexIgnore
    public final void a(boolean z) {
        ik7 unused = xh7.b(e(), null, null, new d(this, z, null), 3, null);
    }

    @DexIgnore
    public void a(int i2, int i3, Intent intent) {
        this.l.onActivityResult(i2, i3, intent);
    }
}
