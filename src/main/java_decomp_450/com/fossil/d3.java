package com.fossil;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.os.Build;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d3 extends ContextWrapper {
    @DexIgnore
    public static /* final */ Object c; // = new Object();
    @DexIgnore
    public static ArrayList<WeakReference<d3>> d;
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ Resources.Theme b;

    @DexIgnore
    public d3(Context context) {
        super(context);
        if (l3.b()) {
            l3 l3Var = new l3(this, context.getResources());
            this.a = l3Var;
            Resources.Theme newTheme = l3Var.newTheme();
            this.b = newTheme;
            newTheme.setTo(context.getTheme());
            return;
        }
        this.a = new f3(this, context.getResources());
        this.b = null;
    }

    @DexIgnore
    public static boolean a(Context context) {
        if ((context instanceof d3) || (context.getResources() instanceof f3) || (context.getResources() instanceof l3)) {
            return false;
        }
        if (Build.VERSION.SDK_INT < 21 || l3.b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static Context b(Context context) {
        if (!a(context)) {
            return context;
        }
        synchronized (c) {
            if (d == null) {
                d = new ArrayList<>();
            } else {
                for (int size = d.size() - 1; size >= 0; size--) {
                    WeakReference<d3> weakReference = d.get(size);
                    if (weakReference == null || weakReference.get() == null) {
                        d.remove(size);
                    }
                }
                for (int size2 = d.size() - 1; size2 >= 0; size2--) {
                    WeakReference<d3> weakReference2 = d.get(size2);
                    d3 d3Var = weakReference2 != null ? weakReference2.get() : null;
                    if (d3Var != null && d3Var.getBaseContext() == context) {
                        return d3Var;
                    }
                }
            }
            d3 d3Var2 = new d3(context);
            d.add(new WeakReference<>(d3Var2));
            return d3Var2;
        }
    }

    @DexIgnore
    public AssetManager getAssets() {
        return this.a.getAssets();
    }

    @DexIgnore
    public Resources getResources() {
        return this.a;
    }

    @DexIgnore
    public Resources.Theme getTheme() {
        Resources.Theme theme = this.b;
        return theme == null ? super.getTheme() : theme;
    }

    @DexIgnore
    public void setTheme(int i) {
        Resources.Theme theme = this.b;
        if (theme == null) {
            super.setTheme(i);
        } else {
            theme.applyStyle(i, true);
        }
    }
}
