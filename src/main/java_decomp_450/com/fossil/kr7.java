package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kr7 implements sr7 {
    @DexIgnore
    public /* final */ yq7 a;
    @DexIgnore
    public nr7 b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public long e;
    @DexIgnore
    public /* final */ ar7 f;

    @DexIgnore
    public kr7(ar7 ar7) {
        ee7.b(ar7, "upstream");
        this.f = ar7;
        yq7 buffer = ar7.getBuffer();
        this.a = buffer;
        nr7 nr7 = buffer.a;
        this.b = nr7;
        this.c = nr7 != null ? nr7.b : -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r5 == r7.b) goto L_0x002f;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0078  */
    @Override // com.fossil.sr7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public long b(com.fossil.yq7 r9, long r10) {
        /*
            r8 = this;
            java.lang.String r0 = "sink"
            com.fossil.ee7.b(r9, r0)
            r0 = 0
            r1 = 0
            r3 = 1
            int r4 = (r10 > r1 ? 1 : (r10 == r1 ? 0 : -1))
            if (r4 < 0) goto L_0x000f
            r5 = 1
            goto L_0x0010
        L_0x000f:
            r5 = 0
        L_0x0010:
            if (r5 == 0) goto L_0x0090
            boolean r5 = r8.d
            r5 = r5 ^ r3
            if (r5 == 0) goto L_0x0084
            com.fossil.nr7 r5 = r8.b
            r6 = 0
            if (r5 == 0) goto L_0x002f
            com.fossil.yq7 r7 = r8.a
            com.fossil.nr7 r7 = r7.a
            if (r5 != r7) goto L_0x0030
            int r5 = r8.c
            if (r7 == 0) goto L_0x002b
            int r7 = r7.b
            if (r5 != r7) goto L_0x0030
            goto L_0x002f
        L_0x002b:
            com.fossil.ee7.a()
            throw r6
        L_0x002f:
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0078
            if (r4 != 0) goto L_0x0035
            return r1
        L_0x0035:
            com.fossil.ar7 r0 = r8.f
            long r1 = r8.e
            r3 = 1
            long r1 = r1 + r3
            boolean r0 = r0.d(r1)
            if (r0 != 0) goto L_0x0045
            r9 = -1
            return r9
        L_0x0045:
            com.fossil.nr7 r0 = r8.b
            if (r0 != 0) goto L_0x005c
            com.fossil.yq7 r0 = r8.a
            com.fossil.nr7 r0 = r0.a
            if (r0 == 0) goto L_0x005c
            r8.b = r0
            if (r0 == 0) goto L_0x0058
            int r0 = r0.b
            r8.c = r0
            goto L_0x005c
        L_0x0058:
            com.fossil.ee7.a()
            throw r6
        L_0x005c:
            com.fossil.yq7 r0 = r8.a
            long r0 = r0.x()
            long r2 = r8.e
            long r0 = r0 - r2
            long r10 = java.lang.Math.min(r10, r0)
            com.fossil.yq7 r2 = r8.a
            long r4 = r8.e
            r3 = r9
            r6 = r10
            r2.a(r3, r4, r6)
            long r0 = r8.e
            long r0 = r0 + r10
            r8.e = r0
            return r10
        L_0x0078:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "Peek source is invalid because upstream source was used"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x0084:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "closed"
            java.lang.String r10 = r10.toString()
            r9.<init>(r10)
            throw r9
        L_0x0090:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r0 = "byteCount < 0: "
            r9.append(r0)
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.String r9 = r9.toString()
            r10.<init>(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.kr7.b(com.fossil.yq7, long):long");
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
    public void close() {
        this.d = true;
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.f.d();
    }
}
