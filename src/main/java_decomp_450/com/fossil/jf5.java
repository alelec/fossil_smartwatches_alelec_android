package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jf5 extends if5 {
    @DexIgnore
    public String i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jf5(qd5 qd5, String str, String str2) {
        super(qd5, str, str2);
        ee7.b(qd5, "analyticsHelper");
        ee7.b(str, "traceName");
        ys7.a(ee7.a((Object) str, (Object) "view_appearance"), "traceName should be view_appearance", new Object[0]);
    }

    @DexIgnore
    public final boolean a(jf5 jf5) {
        if (jf5 == null) {
            return false;
        }
        return ee7.a((Object) jf5.i, (Object) this.i);
    }

    @DexIgnore
    public final jf5 b(String str) {
        ee7.b(str, "viewName");
        this.i = str;
        a("view_name", str);
        return this;
    }

    @DexIgnore
    public final String e() {
        return this.i;
    }

    @DexIgnore
    @Override // com.fossil.if5
    public String toString() {
        return "View name: " + this.i + ", running: " + b();
    }
}
