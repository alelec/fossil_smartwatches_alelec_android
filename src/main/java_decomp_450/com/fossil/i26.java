package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.xo5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i26 extends go5 implements h26 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<qy4> f;
    @DexIgnore
    public xo5 g;
    @DexIgnore
    public g26 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return i26.j;
        }

        @DexIgnore
        public final i26 b() {
            return new i26();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i26 a;

        @DexIgnore
        public b(i26 i26) {
            this.a = i26;
        }

        @DexIgnore
        public final void onClick(View view) {
            qy4 a2 = this.a.g1().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ i26 a;

        @DexIgnore
        public c(i26 i26) {
            this.a = i26;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            FlexibleTextView flexibleTextView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                qy4 a2 = this.a.g1().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.a.b("");
                this.a.h1().h();
                return;
            }
            qy4 a3 = this.a.g1().a();
            if (!(a3 == null || (flexibleTextView = a3.w) == null)) {
                flexibleTextView.setVisibility(8);
            }
            qy4 a4 = this.a.g1().a();
            if (!(a4 == null || (imageView = a4.r) == null)) {
                imageView.setVisibility(0);
            }
            this.a.b(String.valueOf(charSequence));
            this.a.h1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ i26 a;

        @DexIgnore
        public d(i26 i26) {
            this.a = i26;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements xo5.d {
        @DexIgnore
        public /* final */ /* synthetic */ i26 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(i26 i26) {
            this.a = i26;
        }

        @DexIgnore
        @Override // com.fossil.xo5.d
        public void a(String str) {
            ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            qy4 a2 = this.a.g1().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                ee7.a((Object) flexibleTextView2, "it.tvNotFound");
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886760);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{str}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements xo5.e {
        @DexIgnore
        public /* final */ /* synthetic */ i26 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(i26 i26) {
            this.a = i26;
        }

        @DexIgnore
        @Override // com.fossil.xo5.e
        public void a(Complication complication) {
            ee7.b(complication, "item");
            this.a.h1().a(complication);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ qy4 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(qy4 qy4, long j) {
            this.a = qy4;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleButton flexibleButton = this.a.q;
            ee7.a((Object) flexibleButton, "it");
            if (flexibleButton.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                flexibleButton.animate().setDuration(this.b).alpha(1.0f);
            } else {
                flexibleButton.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = i26.class.getSimpleName();
        ee7.a((Object) simpleName, "ComplicationSearchFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.h26
    public void b(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        xo5 xo5 = this.g;
        if (xo5 != null) {
            xo5.a(str);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.h26
    public void c(List<r87<Complication, String>> list) {
        ee7.b(list, "recentSearchResult");
        xo5 xo5 = this.g;
        if (xo5 != null) {
            xo5.a(list);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void f1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            le5 le5 = le5.a;
            qw6<qy4> qw6 = this.f;
            FlexibleButton flexibleButton = null;
            if (qw6 != null) {
                qy4 a2 = qw6.a();
                if (a2 != null) {
                    flexibleButton = a2.q;
                }
                if (flexibleButton != null) {
                    ee7.a((Object) activity, "it");
                    le5.a(flexibleButton, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.View");
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final qw6<qy4> g1() {
        qw6<qy4> qw6 = this.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final g26 h1() {
        g26 g26 = this.h;
        if (g26 != null) {
            return g26;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.h26
    public void n() {
        xo5 xo5 = this.g;
        if (xo5 != null) {
            xo5.b(null);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<qy4> qw6 = new qw6<>(this, (qy4) qb.a(layoutInflater, 2131558520, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            qy4 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        g26 g26 = this.h;
        if (g26 != null) {
            g26.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        g26 g26 = this.h;
        if (g26 != null) {
            g26.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, "it");
            a(activity, 550);
        }
        this.g = new xo5();
        qw6<qy4> qw6 = this.f;
        if (qw6 != null) {
            qy4 a2 = qw6.a();
            if (a2 != null) {
                qy4 qy4 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = qy4.v;
                ee7.a((Object) recyclerViewEmptySupport, "this.rvResults");
                xo5 xo5 = this.g;
                if (xo5 != null) {
                    recyclerViewEmptySupport.setAdapter(xo5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = qy4.v;
                    ee7.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = qy4.v;
                    FlexibleTextView flexibleTextView = qy4.w;
                    ee7.a((Object) flexibleTextView, "tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = qy4.r;
                    ee7.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    qy4.r.setOnClickListener(new b(this));
                    qy4.t.addTextChangedListener(new c(this));
                    qy4.q.setOnClickListener(new d(this));
                    xo5 xo52 = this.g;
                    if (xo52 != null) {
                        xo52.a(new e(this));
                        xo5 xo53 = this.g;
                        if (xo53 != null) {
                            xo53.a(new f(this));
                        } else {
                            ee7.d("mAdapter");
                            throw null;
                        }
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = zk5.a.a(j2);
        Window window = fragmentActivity.getWindow();
        ee7.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        qw6<qy4> qw6 = this.f;
        if (qw6 != null) {
            qy4 a3 = qw6.a();
            if (a3 != null) {
                ee7.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.h26
    public void b(List<r87<Complication, String>> list) {
        ee7.b(list, "results");
        xo5 xo5 = this.g;
        if (xo5 != null) {
            xo5.b(list);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, qy4 qy4) {
        FlexibleButton flexibleButton = qy4.q;
        ee7.a((Object) flexibleButton, "binding.btnCancel");
        flexibleButton.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new g(qy4, j2));
    }

    @DexIgnore
    public void a(g26 g26) {
        ee7.b(g26, "presenter");
        this.h = g26;
    }

    @DexIgnore
    @Override // com.fossil.h26
    public void a(Complication complication) {
        ee7.b(complication, "selectedComplication");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, new Intent().putExtra("SEARCH_COMPLICATION_RESULT_ID", complication.getComplicationId()));
            le5 le5 = le5.a;
            qw6<qy4> qw6 = this.f;
            FlexibleButton flexibleButton = null;
            if (qw6 != null) {
                qy4 a2 = qw6.a();
                if (a2 != null) {
                    flexibleButton = a2.q;
                }
                if (flexibleButton != null) {
                    ee7.a((Object) activity, "it");
                    le5.a(flexibleButton, activity);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.View");
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
