package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.portfolio.platform.view.ColorPanelView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ey6 extends BaseAdapter {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ int[] b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b {
        @DexIgnore
        public View a;
        @DexIgnore
        public ColorPanelView b;
        @DexIgnore
        public ImageView c;
        @DexIgnore
        public int d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public a(int i) {
                this.a = i;
            }

            @DexIgnore
            public void onClick(View view) {
                ey6 ey6 = ey6.this;
                int i = ey6.c;
                int i2 = this.a;
                if (i != i2) {
                    ey6.c = i2;
                    ey6.notifyDataSetChanged();
                }
                ey6 ey62 = ey6.this;
                ey62.a.a(ey62.b[this.a]);
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ey6$b$b")
        /* renamed from: com.fossil.ey6$b$b  reason: collision with other inner class name */
        public class View$OnLongClickListenerC0064b implements View.OnLongClickListener {
            @DexIgnore
            public View$OnLongClickListenerC0064b() {
            }

            @DexIgnore
            public boolean onLongClick(View view) {
                b.this.b.c();
                return true;
            }
        }

        @DexIgnore
        public b(Context context) {
            View inflate = View.inflate(context, ey6.this.d == 0 ? 2131558456 : 2131558455, null);
            this.a = inflate;
            this.b = (ColorPanelView) inflate.findViewById(2131362157);
            this.c = (ImageView) this.a.findViewById(2131362154);
            this.d = this.b.getBorderColor();
            this.a.setTag(this);
        }

        @DexIgnore
        public final void a(int i) {
            ey6 ey6 = ey6.this;
            if (i != ey6.c || e7.a(ey6.b[i]) < 0.65d) {
                this.c.setColorFilter((ColorFilter) null);
            } else {
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            }
        }

        @DexIgnore
        public final void b(int i) {
            this.b.setOnClickListener(new a(i));
            this.b.setOnLongClickListener(new View$OnLongClickListenerC0064b());
        }

        @DexIgnore
        public void c(int i) {
            int i2 = ey6.this.b[i];
            int alpha = Color.alpha(i2);
            this.b.setColor(i2);
            this.c.setImageResource(ey6.this.c == i ? 2131230940 : 0);
            if (alpha == 255) {
                a(i);
            } else if (alpha <= 165) {
                this.b.setBorderColor(i2 | -16777216);
                this.c.setColorFilter(-16777216, PorterDuff.Mode.SRC_IN);
            } else {
                this.b.setBorderColor(this.d);
                this.c.setColorFilter(-1, PorterDuff.Mode.SRC_IN);
            }
            b(i);
        }
    }

    @DexIgnore
    public ey6(a aVar, int[] iArr, int i, int i2) {
        this.a = aVar;
        this.b = iArr;
        this.c = i;
        this.d = i2;
    }

    @DexIgnore
    public void a() {
        this.c = -1;
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getCount() {
        return this.b.length;
    }

    @DexIgnore
    public Object getItem(int i) {
        return Integer.valueOf(this.b[i]);
    }

    @DexIgnore
    public long getItemId(int i) {
        return (long) i;
    }

    @DexIgnore
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        b bVar;
        if (view == null) {
            bVar = new b(viewGroup.getContext());
            view2 = bVar.a;
        } else {
            view2 = view;
            bVar = (b) view.getTag();
        }
        bVar.c(i);
        return view2;
    }
}
