package com.fossil;

import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w26 implements Factory<v26> {
    @DexIgnore
    public static v26 a(r26 r26, WatchFaceRepository watchFaceRepository, DianaPresetRepository dianaPresetRepository) {
        return new v26(r26, watchFaceRepository, dianaPresetRepository);
    }
}
