package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.service.MFDeviceService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qj5 implements MembersInjector<MFDeviceService> {
    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ch5 ch5) {
        mFDeviceService.b = ch5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, DeviceRepository deviceRepository) {
        mFDeviceService.c = deviceRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ActivitiesRepository activitiesRepository) {
        mFDeviceService.d = activitiesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SummariesRepository summariesRepository) {
        mFDeviceService.e = summariesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SleepSessionsRepository sleepSessionsRepository) {
        mFDeviceService.f = sleepSessionsRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, SleepSummariesRepository sleepSummariesRepository) {
        mFDeviceService.g = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, UserRepository userRepository) {
        mFDeviceService.h = userRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HybridPresetRepository hybridPresetRepository) {
        mFDeviceService.i = hybridPresetRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, MicroAppRepository microAppRepository) {
        mFDeviceService.j = microAppRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HeartRateSampleRepository heartRateSampleRepository) {
        mFDeviceService.p = heartRateSampleRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, HeartRateSummaryRepository heartRateSummaryRepository) {
        mFDeviceService.q = heartRateSummaryRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, WorkoutSessionRepository workoutSessionRepository) {
        mFDeviceService.r = workoutSessionRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, FitnessDataRepository fitnessDataRepository) {
        mFDeviceService.s = fitnessDataRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, GoalTrackingRepository goalTrackingRepository) {
        mFDeviceService.t = goalTrackingRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, pj4 pj4) {
        mFDeviceService.u = pj4;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, qd5 qd5) {
        mFDeviceService.v = qd5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, PortfolioApp portfolioApp) {
        mFDeviceService.w = portfolioApp;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, vg5 vg5) {
        mFDeviceService.x = vg5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ThirdPartyRepository thirdPartyRepository) {
        mFDeviceService.y = thirdPartyRepository;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, cw6 cw6) {
        mFDeviceService.z = cw6;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, nw6 nw6) {
        mFDeviceService.A = nw6;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ge5 ge5) {
        mFDeviceService.B = ge5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ff5 ff5) {
        mFDeviceService.C = ff5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, uj5 uj5) {
        mFDeviceService.D = uj5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, wk5 wk5) {
        mFDeviceService.E = wk5;
    }

    @DexIgnore
    public static void a(MFDeviceService mFDeviceService, ek5 ek5) {
        mFDeviceService.F = ek5;
    }
}
