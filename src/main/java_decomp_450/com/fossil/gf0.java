package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ob0 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<gf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public gf0 createFromParcel(Parcel parcel) {
            return new gf0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public gf0[] newArray(int i) {
            return new gf0[i];
        }
    }

    @DexIgnore
    public gf0(ob0 ob0, df0 df0) {
        super(null, df0);
        this.c = ob0;
    }

    @DexIgnore
    @Override // com.fossil.cf0, com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = super.a();
        try {
            yz0.a(a2, r51.n, this.c.a());
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public /* synthetic */ gf0(Parcel parcel, zd7 zd7) {
        super((yb0) parcel.readParcelable(yb0.class.getClassLoader()), (df0) parcel.readParcelable(df0.class.getClassLoader()));
        Parcelable readParcelable = parcel.readParcelable(ob0.class.getClassLoader());
        if (readParcelable != null) {
            this.c = (ob0) readParcelable;
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        Integer valueOf = Integer.valueOf(this.c.c());
        JSONObject jSONObject = new JSONObject();
        try {
            df0 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.a());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    wl0.h.a(e);
                }
                String jSONObject4 = jSONObject2.toString();
                ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c2 = b21.x.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c2);
                    ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            ee7.a();
            throw null;
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
    }
}
