package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c6 {
    @DexIgnore
    public static /* final */ int alpha; // = 2130968707;
    @DexIgnore
    public static /* final */ int font; // = 2130969196;
    @DexIgnore
    public static /* final */ int fontProviderAuthority; // = 2130969198;
    @DexIgnore
    public static /* final */ int fontProviderCerts; // = 2130969199;
    @DexIgnore
    public static /* final */ int fontProviderFetchStrategy; // = 2130969200;
    @DexIgnore
    public static /* final */ int fontProviderFetchTimeout; // = 2130969201;
    @DexIgnore
    public static /* final */ int fontProviderPackage; // = 2130969202;
    @DexIgnore
    public static /* final */ int fontProviderQuery; // = 2130969203;
    @DexIgnore
    public static /* final */ int fontStyle; // = 2130969204;
    @DexIgnore
    public static /* final */ int fontVariationSettings; // = 2130969205;
    @DexIgnore
    public static /* final */ int fontWeight; // = 2130969206;
    @DexIgnore
    public static /* final */ int ttcIndex; // = 2130969842;
}
