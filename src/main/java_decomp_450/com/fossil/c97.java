package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c97 implements Comparable<c97> {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public /* synthetic */ c97(long j) {
        this.a = j;
    }

    @DexIgnore
    public static boolean a(long j, Object obj) {
        return (obj instanceof c97) && j == ((c97) obj).a();
    }

    @DexIgnore
    public static final /* synthetic */ c97 b(long j) {
        return new c97(j);
    }

    @DexIgnore
    public static long c(long j) {
        return j;
    }

    @DexIgnore
    public static int d(long j) {
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public static String e(long j) {
        return j97.a(j);
    }

    @DexIgnore
    public final int a(long j) {
        throw null;
    }

    @DexIgnore
    public final /* synthetic */ long a() {
        return this.a;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.lang.Comparable
    public /* bridge */ /* synthetic */ int compareTo(c97 c97) {
        a(c97.a());
        throw null;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return d(this.a);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }
}
