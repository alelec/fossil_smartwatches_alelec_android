package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lf7 extends jf7 {
    @DexIgnore
    public static /* final */ lf7 e; // = new lf7(1, 0);
    @DexIgnore
    public static /* final */ a f; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final lf7 a() {
            return lf7.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public lf7(int i, int i2) {
        super(i, i2, 1);
    }

    @DexIgnore
    public Integer b() {
        return Integer.valueOf(getLast());
    }

    @DexIgnore
    public Integer c() {
        return Integer.valueOf(getFirst());
    }

    @DexIgnore
    @Override // com.fossil.jf7
    public boolean equals(Object obj) {
        if (obj instanceof lf7) {
            if (!isEmpty() || !((lf7) obj).isEmpty()) {
                lf7 lf7 = (lf7) obj;
                if (!(getFirst() == lf7.getFirst() && getLast() == lf7.getLast())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.jf7
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (getFirst() * 31) + getLast();
    }

    @DexIgnore
    @Override // com.fossil.jf7
    public boolean isEmpty() {
        return getFirst() > getLast();
    }

    @DexIgnore
    @Override // com.fossil.jf7
    public String toString() {
        return getFirst() + ".." + getLast();
    }
}
