package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm7 {
    @DexIgnore
    public static final <T> void a(gd7<? super fb7<? super T>, ? extends Object> gd7, fb7<? super T> fb7) {
        try {
            fb7 a = mb7.a(mb7.a(gd7, fb7));
            s87.a aVar = s87.Companion;
            mj7.a(a, s87.m60constructorimpl(i97.a));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            fb7.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7, R r, fb7<? super T> fb7) {
        try {
            fb7 a = mb7.a(mb7.a(kd7, r, fb7));
            s87.a aVar = s87.Companion;
            mj7.a(a, s87.m60constructorimpl(i97.a));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            fb7.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }
    }

    @DexIgnore
    public static final void a(fb7<? super i97> fb7, fb7<?> fb72) {
        try {
            fb7 a = mb7.a(fb7);
            s87.a aVar = s87.Companion;
            mj7.a(a, s87.m60constructorimpl(i97.a));
        } catch (Throwable th) {
            s87.a aVar2 = s87.Companion;
            fb72.resumeWith(s87.m60constructorimpl(t87.a(th)));
        }
    }
}
