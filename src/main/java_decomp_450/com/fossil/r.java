package com.fossil;

import android.media.session.PlaybackState;
import android.os.Bundle;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static String a(Object obj) {
            return ((PlaybackState.CustomAction) obj).getAction();
        }

        @DexIgnore
        public static Bundle b(Object obj) {
            return ((PlaybackState.CustomAction) obj).getExtras();
        }

        @DexIgnore
        public static int c(Object obj) {
            return ((PlaybackState.CustomAction) obj).getIcon();
        }

        @DexIgnore
        public static CharSequence d(Object obj) {
            return ((PlaybackState.CustomAction) obj).getName();
        }
    }

    @DexIgnore
    public static long a(Object obj) {
        return ((PlaybackState) obj).getActions();
    }

    @DexIgnore
    public static long b(Object obj) {
        return ((PlaybackState) obj).getActiveQueueItemId();
    }

    @DexIgnore
    public static long c(Object obj) {
        return ((PlaybackState) obj).getBufferedPosition();
    }

    @DexIgnore
    public static List<Object> d(Object obj) {
        return ((PlaybackState) obj).getCustomActions();
    }

    @DexIgnore
    public static CharSequence e(Object obj) {
        return ((PlaybackState) obj).getErrorMessage();
    }

    @DexIgnore
    public static long f(Object obj) {
        return ((PlaybackState) obj).getLastPositionUpdateTime();
    }

    @DexIgnore
    public static float g(Object obj) {
        return ((PlaybackState) obj).getPlaybackSpeed();
    }

    @DexIgnore
    public static long h(Object obj) {
        return ((PlaybackState) obj).getPosition();
    }

    @DexIgnore
    public static int i(Object obj) {
        return ((PlaybackState) obj).getState();
    }
}
