package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq4 implements Factory<gq4> {
    @DexIgnore
    public /* final */ Provider<ro4> a;

    @DexIgnore
    public hq4(Provider<ro4> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static hq4 a(Provider<ro4> provider) {
        return new hq4(provider);
    }

    @DexIgnore
    public static gq4 a(ro4 ro4) {
        return new gq4(ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public gq4 get() {
        return a(this.a.get());
    }
}
