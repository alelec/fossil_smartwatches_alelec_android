package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pz3<E> extends zx3<E> {
    @DexIgnore
    public static /* final */ zx3<Object> EMPTY; // = new pz3(iz3.a);
    @DexIgnore
    public /* final */ transient Object[] a;

    @DexIgnore
    public pz3(Object[] objArr) {
        this.a = objArr;
    }

    @DexIgnore
    @Override // com.fossil.vx3, com.fossil.zx3
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.a;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return i + this.a.length;
    }

    @DexIgnore
    @Override // java.util.List
    public E get(int i) {
        return (E) this.a[i];
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.a.length;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.zx3, com.fossil.zx3
    public k04<E> listIterator(int i) {
        Object[] objArr = this.a;
        return qy3.a(objArr, 0, objArr.length, i);
    }
}
