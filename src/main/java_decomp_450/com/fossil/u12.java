package com.fossil;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.fossil.a12;
import com.fossil.h62;
import com.fossil.v02;
import com.fossil.y12;
import com.fossil.y62;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u12 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static u12 q;
    @DexIgnore
    public long a; // = 5000;
    @DexIgnore
    public long b; // = 120000;
    @DexIgnore
    public long c; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ l02 e;
    @DexIgnore
    public /* final */ r62 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<p12<?>, a<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public k22 j; // = null;
    @DexIgnore
    public /* final */ Set<p12<?>> k; // = new o4();
    @DexIgnore
    public /* final */ Set<p12<?>> l; // = new o4();
    @DexIgnore
    public /* final */ Handler m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements j42, h62.c {
        @DexIgnore
        public /* final */ v02.f a;
        @DexIgnore
        public /* final */ p12<?> b;
        @DexIgnore
        public s62 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public c(v02.f fVar, p12<?> p12) {
            this.a = fVar;
            this.b = p12;
        }

        @DexIgnore
        @Override // com.fossil.h62.c
        public final void a(i02 i02) {
            u12.this.m.post(new w32(this, i02));
        }

        @DexIgnore
        @Override // com.fossil.j42
        public final void b(i02 i02) {
            ((a) u12.this.i.get(this.b)).b(i02);
        }

        @DexIgnore
        @Override // com.fossil.j42
        public final void a(s62 s62, Set<Scope> set) {
            if (s62 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new i02(4));
                return;
            }
            this.c = s62;
            this.d = set;
            a();
        }

        @DexIgnore
        public final void a() {
            s62 s62;
            if (this.e && (s62 = this.c) != null) {
                this.a.a(s62, this.d);
            }
        }
    }

    @DexIgnore
    public u12(Context context, Looper looper, l02 l02) {
        this.d = context;
        this.m = new bg2(looper, this);
        this.e = l02;
        this.f = new r62(l02);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static u12 a(Context context) {
        u12 u12;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new u12(context.getApplicationContext(), handlerThread.getLooper(), l02.a());
            }
            u12 = q;
        }
        return u12;
    }

    @DexIgnore
    public static void d() {
        synchronized (p) {
            if (q != null) {
                u12 u12 = q;
                u12.h.incrementAndGet();
                u12.m.sendMessageAtFrontOfQueue(u12.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static u12 e() {
        u12 u12;
        synchronized (p) {
            a72.a(q, "Must guarantee manager is non-null before using getInstance");
            u12 = q;
        }
        return u12;
    }

    @DexIgnore
    public final int b() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final void c() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        int i2 = message.what;
        long j2 = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
        a<?> aVar = null;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = ButtonService.CONNECT_TIMEOUT;
                }
                this.c = j2;
                this.m.removeMessages(12);
                for (p12<?> p12 : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, p12), this.c);
                }
                break;
            case 2:
                y42 y42 = (y42) message.obj;
                Iterator<p12<?>> it = y42.b().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        p12<?> next = it.next();
                        a<?> aVar2 = this.i.get(next);
                        if (aVar2 == null) {
                            y42.a(next, new i02(13), null);
                            break;
                        } else if (aVar2.c()) {
                            y42.a(next, i02.e, aVar2.r().g());
                        } else if (aVar2.m() != null) {
                            y42.a(next, aVar2.m(), null);
                        } else {
                            aVar2.a(y42);
                            aVar2.a();
                        }
                    }
                }
            case 3:
                for (a<?> aVar3 : this.i.values()) {
                    aVar3.l();
                    aVar3.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                c42 c42 = (c42) message.obj;
                a<?> aVar4 = this.i.get(c42.c.a());
                if (aVar4 == null) {
                    b(c42.c);
                    aVar4 = this.i.get(c42.c.a());
                }
                if (aVar4.d() && this.h.get() != c42.b) {
                    c42.a.a(n);
                    aVar4.j();
                    break;
                } else {
                    aVar4.a(c42.a);
                    break;
                }
            case 5:
                int i3 = message.arg1;
                i02 i02 = (i02) message.obj;
                Iterator<a<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        a<?> next2 = it2.next();
                        if (next2.b() == i3) {
                            aVar = next2;
                        }
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i3);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b2 = this.e.b(i02.e());
                    String g2 = i02.g();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(g2).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b2);
                    sb2.append(": ");
                    sb2.append(g2);
                    aVar.a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (this.d.getApplicationContext() instanceof Application) {
                    q12.a((Application) this.d.getApplicationContext());
                    q12.b().a(new q32(this));
                    if (!q12.b().b(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                b((z02) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).e();
                    break;
                }
                break;
            case 10:
                for (p12<?> p122 : this.l) {
                    this.i.remove(p122).j();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).f();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).p();
                    break;
                }
                break;
            case 14:
                n22 n22 = (n22) message.obj;
                p12<?> a2 = n22.a();
                if (this.i.containsKey(a2)) {
                    n22.b().a(Boolean.valueOf(((a) this.i.get(a2)).a(false)));
                    break;
                } else {
                    n22.b().a((Boolean) false);
                    break;
                }
            case 15:
                b bVar = (b) message.obj;
                if (this.i.containsKey(bVar.a)) {
                    this.i.get(bVar.a).a(bVar);
                    break;
                }
                break;
            case 16:
                b bVar2 = (b) message.obj;
                if (this.i.containsKey(bVar2.a)) {
                    this.i.get(bVar2.a).b(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(z02<?> z02) {
        p12<?> a2 = z02.a();
        a<?> aVar = this.i.get(a2);
        if (aVar == null) {
            aVar = new a<>(z02);
            this.i.put(a2, aVar);
        }
        if (aVar.d()) {
            this.l.add(a2);
        }
        aVar.a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a<O extends v02.d> implements a12.b, a12.c, e52 {
        @DexIgnore
        public /* final */ Queue<i32> a; // = new LinkedList();
        @DexIgnore
        public /* final */ v02.f b;
        @DexIgnore
        public /* final */ v02.b c;
        @DexIgnore
        public /* final */ p12<O> d;
        @DexIgnore
        public /* final */ j22 e;
        @DexIgnore
        public /* final */ Set<y42> f; // = new HashSet();
        @DexIgnore
        public /* final */ Map<y12.a<?>, d42> g; // = new HashMap();
        @DexIgnore
        public /* final */ int h;
        @DexIgnore
        public /* final */ g42 i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public /* final */ List<b> p; // = new ArrayList();
        @DexIgnore
        public i02 q; // = null;

        @DexIgnore
        public a(z02<O> z02) {
            v02.f a2 = z02.a(u12.this.m.getLooper(), this);
            this.b = a2;
            if (a2 instanceof f72) {
                this.c = ((f72) a2).I();
            } else {
                this.c = a2;
            }
            this.d = z02.a();
            this.e = new j22();
            this.h = z02.g();
            if (this.b.n()) {
                this.i = z02.a(u12.this.d, u12.this.m);
            } else {
                this.i = null;
            }
        }

        @DexIgnore
        @Override // com.fossil.t12
        public final void a(int i2) {
            if (Looper.myLooper() == u12.this.m.getLooper()) {
                h();
            } else {
                u12.this.m.post(new r32(this));
            }
        }

        @DexIgnore
        @Override // com.fossil.t12
        public final void b(Bundle bundle) {
            if (Looper.myLooper() == u12.this.m.getLooper()) {
                g();
            } else {
                u12.this.m.post(new s32(this));
            }
        }

        @DexIgnore
        public final boolean c(i02 i02) {
            synchronized (u12.p) {
                if (u12.this.j == null || !u12.this.k.contains(this.d)) {
                    return false;
                }
                u12.this.j.b(i02, this.h);
                return true;
            }
        }

        @DexIgnore
        public final void d(i02 i02) {
            for (y42 y42 : this.f) {
                String str = null;
                if (y62.a(i02, i02.e)) {
                    str = this.b.g();
                }
                y42.a(this.d, i02, str);
            }
            this.f.clear();
        }

        @DexIgnore
        public final void e() {
            a72.a(u12.this.m);
            if (this.j) {
                a();
            }
        }

        @DexIgnore
        public final void f() {
            Status status;
            a72.a(u12.this.m);
            if (this.j) {
                n();
                if (u12.this.e.c(u12.this.d) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                a(status);
                this.b.a();
            }
        }

        @DexIgnore
        public final void g() {
            l();
            d(i02.e);
            n();
            Iterator<d42> it = this.g.values().iterator();
            while (it.hasNext()) {
                d42 next = it.next();
                if (a(next.a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.a.a(this.c, new oo3<>());
                    } catch (DeadObjectException unused) {
                        a(1);
                        this.b.a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            i();
            o();
        }

        @DexIgnore
        public final void h() {
            l();
            this.j = true;
            this.e.c();
            u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 9, this.d), u12.this.a);
            u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 11, this.d), u12.this.b);
            u12.this.f.a();
            for (d42 d42 : this.g.values()) {
                d42.c.run();
            }
        }

        @DexIgnore
        public final void i() {
            ArrayList arrayList = new ArrayList(this.a);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i32 i32 = (i32) obj;
                if (!this.b.c()) {
                    return;
                }
                if (b(i32)) {
                    this.a.remove(i32);
                }
            }
        }

        @DexIgnore
        public final void j() {
            a72.a(u12.this.m);
            a(u12.n);
            this.e.b();
            for (y12.a aVar : (y12.a[]) this.g.keySet().toArray(new y12.a[this.g.size()])) {
                a(new v42(aVar, new oo3()));
            }
            d(new i02(4));
            if (this.b.c()) {
                this.b.a(new t32(this));
            }
        }

        @DexIgnore
        public final Map<y12.a<?>, d42> k() {
            return this.g;
        }

        @DexIgnore
        public final void l() {
            a72.a(u12.this.m);
            this.q = null;
        }

        @DexIgnore
        public final i02 m() {
            a72.a(u12.this.m);
            return this.q;
        }

        @DexIgnore
        public final void n() {
            if (this.j) {
                u12.this.m.removeMessages(11, this.d);
                u12.this.m.removeMessages(9, this.d);
                this.j = false;
            }
        }

        @DexIgnore
        public final void o() {
            u12.this.m.removeMessages(12, this.d);
            u12.this.m.sendMessageDelayed(u12.this.m.obtainMessage(12, this.d), u12.this.c);
        }

        @DexIgnore
        public final boolean p() {
            return a(true);
        }

        @DexIgnore
        public final xn3 q() {
            g42 g42 = this.i;
            if (g42 == null) {
                return null;
            }
            return g42.E();
        }

        @DexIgnore
        public final v02.f r() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.e52
        public final void a(i02 i02, v02<?> v02, boolean z) {
            if (Looper.myLooper() == u12.this.m.getLooper()) {
                a(i02);
            } else {
                u12.this.m.post(new u32(this, i02));
            }
        }

        @DexIgnore
        public final void b(i02 i02) {
            a72.a(u12.this.m);
            this.b.a();
            a(i02);
        }

        @DexIgnore
        public final Status e(i02 i02) {
            String a2 = this.d.a();
            String valueOf = String.valueOf(i02);
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 63 + String.valueOf(valueOf).length());
            sb.append("API: ");
            sb.append(a2);
            sb.append(" is not available on this device. Connection failed with: ");
            sb.append(valueOf);
            return new Status(17, sb.toString());
        }

        @DexIgnore
        public final boolean d() {
            return this.b.n();
        }

        @DexIgnore
        @Override // com.fossil.a22
        public final void a(i02 i02) {
            a(i02, (Exception) null);
        }

        @DexIgnore
        public final boolean b(i32 i32) {
            if (!(i32 instanceof s42)) {
                c(i32);
                return true;
            }
            s42 s42 = (s42) i32;
            k02 a2 = a(s42.b((a<?>) this));
            if (a2 == null) {
                c(i32);
                return true;
            }
            String name = this.c.getClass().getName();
            String e2 = a2.e();
            long g2 = a2.g();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 77 + String.valueOf(e2).length());
            sb.append(name);
            sb.append(" could not execute call because it requires feature (");
            sb.append(e2);
            sb.append(", ");
            sb.append(g2);
            sb.append(").");
            Log.w("GoogleApiManager", sb.toString());
            if (s42.c(this)) {
                b bVar = new b(this.d, a2, null);
                int indexOf = this.p.indexOf(bVar);
                if (indexOf >= 0) {
                    b bVar2 = this.p.get(indexOf);
                    u12.this.m.removeMessages(15, bVar2);
                    u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 15, bVar2), u12.this.a);
                    return false;
                }
                this.p.add(bVar);
                u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 15, bVar), u12.this.a);
                u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 16, bVar), u12.this.b);
                i02 i02 = new i02(2, null);
                if (c(i02)) {
                    return false;
                }
                u12.this.b(i02, this.h);
                return false;
            }
            s42.a(new n12(a2));
            return true;
        }

        @DexIgnore
        public final void c(i32 i32) {
            i32.a(this.e, d());
            try {
                i32.a((a<?>) this);
            } catch (DeadObjectException unused) {
                a(1);
                this.b.a();
            } catch (Throwable th) {
                throw new IllegalStateException(String.format("Error in GoogleApi implementation for client %s.", this.c.getClass().getName()), th);
            }
        }

        @DexIgnore
        public final void a(i02 i02, Exception exc) {
            a72.a(u12.this.m);
            g42 g42 = this.i;
            if (g42 != null) {
                g42.F();
            }
            l();
            u12.this.f.a();
            d(i02);
            if (i02.e() == 4) {
                a(u12.o);
            } else if (this.a.isEmpty()) {
                this.q = i02;
            } else if (exc != null) {
                a72.a(u12.this.m);
                a((Status) null, exc, false);
            } else {
                a(e(i02), (Exception) null, true);
                if (!this.a.isEmpty() && !c(i02) && !u12.this.b(i02, this.h)) {
                    if (i02.e() == 18) {
                        this.j = true;
                    }
                    if (this.j) {
                        u12.this.m.sendMessageDelayed(Message.obtain(u12.this.m, 9, this.d), u12.this.a);
                    } else {
                        a(e(i02));
                    }
                }
            }
        }

        @DexIgnore
        public final boolean c() {
            return this.b.c();
        }

        @DexIgnore
        public final void a(i32 i32) {
            a72.a(u12.this.m);
            if (!this.b.c()) {
                this.a.add(i32);
                i02 i02 = this.q;
                if (i02 == null || !i02.w()) {
                    a();
                } else {
                    a(this.q);
                }
            } else if (b(i32)) {
                o();
            } else {
                this.a.add(i32);
            }
        }

        @DexIgnore
        public final int b() {
            return this.h;
        }

        @DexIgnore
        public final void b(b bVar) {
            k02[] b2;
            if (this.p.remove(bVar)) {
                u12.this.m.removeMessages(15, bVar);
                u12.this.m.removeMessages(16, bVar);
                k02 b3 = bVar.b;
                ArrayList arrayList = new ArrayList(this.a.size());
                for (i32 i32 : this.a) {
                    if ((i32 instanceof s42) && (b2 = ((s42) i32).b((a<?>) this)) != null && k92.a(b2, b3)) {
                        arrayList.add(i32);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    i32 i322 = (i32) obj;
                    this.a.remove(i322);
                    i322.a(new n12(b3));
                }
            }
        }

        @DexIgnore
        public final void a(Status status, Exception exc, boolean z) {
            a72.a(u12.this.m);
            boolean z2 = true;
            boolean z3 = status == null;
            if (exc != null) {
                z2 = false;
            }
            if (z3 != z2) {
                Iterator<i32> it = this.a.iterator();
                while (it.hasNext()) {
                    i32 next = it.next();
                    if (!z || next.a == 2) {
                        if (status != null) {
                            next.a(status);
                        } else {
                            next.a(exc);
                        }
                        it.remove();
                    }
                }
                return;
            }
            throw new IllegalArgumentException("Status XOR exception should be null");
        }

        @DexIgnore
        public final void a(Status status) {
            a72.a(u12.this.m);
            a(status, (Exception) null, false);
        }

        @DexIgnore
        public final boolean a(boolean z) {
            a72.a(u12.this.m);
            if (!this.b.c() || this.g.size() != 0) {
                return false;
            }
            if (this.e.a()) {
                if (z) {
                    o();
                }
                return false;
            }
            this.b.a();
            return true;
        }

        @DexIgnore
        public final void a() {
            a72.a(u12.this.m);
            if (!this.b.c() && !this.b.f()) {
                try {
                    int a2 = u12.this.f.a(u12.this.d, this.b);
                    if (a2 != 0) {
                        i02 i02 = new i02(a2, null);
                        String name = this.c.getClass().getName();
                        String valueOf = String.valueOf(i02);
                        StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 35 + String.valueOf(valueOf).length());
                        sb.append("The service for ");
                        sb.append(name);
                        sb.append(" is not available: ");
                        sb.append(valueOf);
                        Log.w("GoogleApiManager", sb.toString());
                        a(i02);
                        return;
                    }
                    c cVar = new c(this.b, this.d);
                    if (this.b.n()) {
                        this.i.a(cVar);
                    }
                    try {
                        this.b.a(cVar);
                    } catch (SecurityException e2) {
                        a(new i02(10), e2);
                    }
                } catch (IllegalStateException e3) {
                    a(new i02(10), e3);
                }
            }
        }

        @DexIgnore
        public final void a(y42 y42) {
            a72.a(u12.this.m);
            this.f.add(y42);
        }

        @DexIgnore
        public final k02 a(k02[] k02Arr) {
            if (!(k02Arr == null || k02Arr.length == 0)) {
                k02[] l = this.b.l();
                if (l == null) {
                    l = new k02[0];
                }
                n4 n4Var = new n4(l.length);
                for (k02 k02 : l) {
                    n4Var.put(k02.e(), Long.valueOf(k02.g()));
                }
                for (k02 k022 : k02Arr) {
                    if (!n4Var.containsKey(k022.e()) || ((Long) n4Var.get(k022.e())).longValue() < k022.g()) {
                        return k022;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public final void a(b bVar) {
            if (!this.p.contains(bVar) || this.j) {
                return;
            }
            if (!this.b.c()) {
                a();
            } else {
                i();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ p12<?> a;
        @DexIgnore
        public /* final */ k02 b;

        @DexIgnore
        public b(p12<?> p12, k02 k02) {
            this.a = p12;
            this.b = k02;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof b)) {
                b bVar = (b) obj;
                if (!y62.a(this.a, bVar.a) || !y62.a(this.b, bVar.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return y62.a(this.a, this.b);
        }

        @DexIgnore
        public final String toString() {
            y62.a a2 = y62.a(this);
            a2.a("key", this.a);
            a2.a("feature", this.b);
            return a2.toString();
        }

        @DexIgnore
        public /* synthetic */ b(p12 p12, k02 k02, q32 q32) {
            this(p12, k02);
        }
    }

    @DexIgnore
    public final void b(k22 k22) {
        synchronized (p) {
            if (this.j == k22) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final void a(z02<?> z02) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, z02));
    }

    @DexIgnore
    public final void a(k22 k22) {
        synchronized (p) {
            if (this.j != k22) {
                this.j = k22;
                this.k.clear();
            }
            this.k.addAll(k22.h());
        }
    }

    @DexIgnore
    public final boolean b(i02 i02, int i2) {
        return this.e.a(this.d, i02, i2);
    }

    @DexIgnore
    public final no3<Map<p12<?>, String>> a(Iterable<? extends b12<?>> iterable) {
        y42 y42 = new y42(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, y42));
        return y42.a();
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final <O extends v02.d> void a(z02<O> z02, int i2, r12<? extends i12, v02.b> r12) {
        u42 u42 = new u42(i2, r12);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new c42(u42, this.h.get(), z02)));
    }

    @DexIgnore
    public final <O extends v02.d, ResultT> void a(z02<O> z02, int i2, f22<v02.b, ResultT> f22, oo3<ResultT> oo3, d22 d22) {
        w42 w42 = new w42(i2, f22, oo3, d22);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new c42(w42, this.h.get(), z02)));
    }

    @DexIgnore
    public final <O extends v02.d> no3<Void> a(z02<O> z02, b22<v02.b, ?> b22, h22<v02.b, ?> h22, Runnable runnable) {
        oo3 oo3 = new oo3();
        t42 t42 = new t42(new d42(b22, h22, runnable), oo3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new c42(t42, this.h.get(), z02)));
        return oo3.a();
    }

    @DexIgnore
    public final <O extends v02.d> no3<Boolean> a(z02<O> z02, y12.a<?> aVar) {
        oo3 oo3 = new oo3();
        v42 v42 = new v42(aVar, oo3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new c42(v42, this.h.get(), z02)));
        return oo3.a();
    }

    @DexIgnore
    public final PendingIntent a(p12<?> p12, int i2) {
        xn3 q2;
        a<?> aVar = this.i.get(p12);
        if (aVar == null || (q2 = aVar.q()) == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, q2.m(), 134217728);
    }

    @DexIgnore
    public final void a(i02 i02, int i2) {
        if (!b(i02, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, i02));
        }
    }
}
