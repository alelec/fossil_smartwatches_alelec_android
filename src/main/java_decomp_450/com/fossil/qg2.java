package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qg2 extends Handler {
    @DexIgnore
    public static sg2 a;

    @DexIgnore
    public qg2(Looper looper) {
        super(looper);
    }

    @DexIgnore
    public void a(Message message) {
        super.dispatchMessage(message);
    }

    @DexIgnore
    public final void dispatchMessage(Message message) {
        sg2 sg2 = a;
        if (sg2 == null) {
            a(message);
            return;
        }
        Object a2 = sg2.a(this, message);
        try {
            a(message);
            sg2.a(this, message, a2);
        } catch (Throwable th) {
            sg2.a(this, message, a2);
            throw th;
        }
    }

    @DexIgnore
    public boolean sendMessageAtTime(Message message, long j) {
        a(message, j);
        return super.sendMessageAtTime(message, j);
    }

    @DexIgnore
    public qg2(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }

    @DexIgnore
    public final void a(Message message, long j) {
        sg2 sg2 = a;
        if (sg2 != null) {
            sg2.a(this, message, j);
        }
    }
}
