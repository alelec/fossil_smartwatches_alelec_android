package com.fossil;

import com.fossil.yy3;
import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sw3<K, V> extends vw3<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 2447537837011683357L;
    @DexIgnore
    public transient Map<K, Collection<V>> f;
    @DexIgnore
    public transient int g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends sw3<K, V>.d {
        @DexIgnore
        public a(sw3 sw3) {
            super();
        }

        @DexIgnore
        @Override // com.fossil.sw3.d
        public V a(K k, V v) {
            return v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends sw3<K, V>.d {
        @DexIgnore
        public b(sw3 sw3) {
            super();
        }

        @DexIgnore
        @Override // com.fossil.sw3.d
        public Map.Entry<K, V> a(K k, V v) {
            return yy3.a((Object) k, (Object) v);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends yy3.g<K, Collection<V>> {
        @DexIgnore
        public /* final */ transient Map<K, Collection<V>> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends yy3.d<K, Collection<V>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.yy3.d
            public Map<K, Collection<V>> a() {
                return c.this;
            }

            @DexIgnore
            @Override // com.fossil.yy3.d
            public boolean contains(Object obj) {
                return cx3.a(c.this.c.entrySet(), obj);
            }

            @DexIgnore
            @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
            public Iterator<Map.Entry<K, Collection<V>>> iterator() {
                return new b();
            }

            @DexIgnore
            public boolean remove(Object obj) {
                if (!contains(obj)) {
                    return false;
                }
                sw3.this.b(((Map.Entry) obj).getKey());
                return true;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Iterator<Map.Entry<K, Collection<V>>> {
            @DexIgnore
            public /* final */ Iterator<Map.Entry<K, Collection<V>>> a; // = c.this.c.entrySet().iterator();
            @DexIgnore
            public Collection<V> b;

            @DexIgnore
            public b() {
            }

            @DexIgnore
            public boolean hasNext() {
                return this.a.hasNext();
            }

            @DexIgnore
            public void remove() {
                this.a.remove();
                sw3.access$220(sw3.this, this.b.size());
                this.b.clear();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public Map.Entry<K, Collection<V>> next() {
                Map.Entry<K, Collection<V>> next = this.a.next();
                this.b = next.getValue();
                return c.this.a(next);
            }
        }

        @DexIgnore
        public c(Map<K, Collection<V>> map) {
            this.c = map;
        }

        @DexIgnore
        @Override // com.fossil.yy3.g
        public Set<Map.Entry<K, Collection<V>>> a() {
            return new a();
        }

        @DexIgnore
        public void clear() {
            if (this.c == sw3.this.f) {
                sw3.this.clear();
            } else {
                qy3.a((Iterator<?>) new b());
            }
        }

        @DexIgnore
        public boolean containsKey(Object obj) {
            return yy3.d(this.c, obj);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || this.c.equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return this.c.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Set<K> keySet() {
            return sw3.this.keySet();
        }

        @DexIgnore
        public int size() {
            return this.c.size();
        }

        @DexIgnore
        public String toString() {
            return this.c.toString();
        }

        @DexIgnore
        public Map.Entry<K, Collection<V>> a(Map.Entry<K, Collection<V>> entry) {
            K key = entry.getKey();
            return yy3.a((Object) key, (Object) sw3.this.wrapCollection(key, entry.getValue()));
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Collection<V> get(Object obj) {
            Collection<V> collection = (Collection) yy3.e(this.c, obj);
            if (collection == null) {
                return null;
            }
            return sw3.this.wrapCollection(obj, collection);
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Collection<V> remove(Object obj) {
            Collection<V> remove = this.c.remove(obj);
            if (remove == null) {
                return null;
            }
            Collection<V> createCollection = sw3.this.createCollection();
            createCollection.addAll(remove);
            sw3.access$220(sw3.this, remove.size());
            remove.clear();
            return createCollection;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class d<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<K, Collection<V>>> a;
        @DexIgnore
        public K b; // = null;
        @DexIgnore
        public Collection<V> c; // = null;
        @DexIgnore
        public Iterator<V> d; // = qy3.c();

        @DexIgnore
        public d() {
            this.a = sw3.this.f.entrySet().iterator();
        }

        @DexIgnore
        public abstract T a(K k, V v);

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext() || this.d.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (!this.d.hasNext()) {
                Map.Entry<K, Collection<V>> next = this.a.next();
                this.b = next.getKey();
                Collection<V> value = next.getValue();
                this.c = value;
                this.d = value.iterator();
            }
            return a(this.b, this.d.next());
        }

        @DexIgnore
        public void remove() {
            this.d.remove();
            if (this.c.isEmpty()) {
                this.a.remove();
            }
            sw3.access$210(sw3.this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends yy3.e<K, Collection<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<K> {
            @DexIgnore
            public Map.Entry<K, Collection<V>> a;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexIgnore
            public a(Iterator it) {
                this.b = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public K next() {
                Map.Entry<K, Collection<V>> entry = (Map.Entry) this.b.next();
                this.a = entry;
                return entry.getKey();
            }

            @DexIgnore
            public void remove() {
                bx3.a(this.a != null);
                Collection<V> value = this.a.getValue();
                this.b.remove();
                sw3.access$220(sw3.this, value.size());
                value.clear();
            }
        }

        @DexIgnore
        public e(Map<K, Collection<V>> map) {
            super(map);
        }

        @DexIgnore
        @Override // com.fossil.yy3.e
        public void clear() {
            qy3.a((Iterator<?>) iterator());
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().keySet().containsAll(collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || a().keySet().equals(obj);
        }

        @DexIgnore
        public int hashCode() {
            return a().keySet().hashCode();
        }

        @DexIgnore
        @Override // com.fossil.yy3.e, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new a(a().entrySet().iterator());
        }

        @DexIgnore
        @Override // com.fossil.yy3.e
        public boolean remove(Object obj) {
            int i;
            V remove = a().remove(obj);
            if (remove != null) {
                i = remove.size();
                remove.clear();
                sw3.access$220(sw3.this, i);
            } else {
                i = 0;
            }
            if (i > 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends sw3<K, V>.j implements RandomAccess {
        @DexIgnore
        public f(sw3 sw3, K k, List<V> list, sw3<K, V>.i iVar) {
            super(k, list, iVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends sw3<K, V>.c implements SortedMap<K, Collection<V>> {
        @DexIgnore
        public SortedSet<K> e;

        @DexIgnore
        public g(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedSet<K> c() {
            return new h(d());
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public Comparator<? super K> comparator() {
            return d().comparator();
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> d() {
            return (SortedMap) ((c) this).c;
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K firstKey() {
            return d().firstKey();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> headMap(K k) {
            return new g(d().headMap(k));
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public K lastKey() {
            return d().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> subMap(K k, K k2) {
            return new g(d().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedMap
        public SortedMap<K, Collection<V>> tailMap(K k) {
            return new g(d().tailMap(k));
        }

        @DexIgnore
        @Override // com.fossil.sw3.c, java.util.AbstractMap, java.util.Map, java.util.SortedMap
        public SortedSet<K> keySet() {
            SortedSet<K> sortedSet = this.e;
            if (sortedSet != null) {
                return sortedSet;
            }
            SortedSet<K> c = c();
            this.e = c;
            return c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends sw3<K, V>.e implements SortedSet<K> {
        @DexIgnore
        public h(SortedMap<K, Collection<V>> sortedMap) {
            super(sortedMap);
        }

        @DexIgnore
        public SortedMap<K, Collection<V>> b() {
            return (SortedMap) super.a();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super K> comparator() {
            return b().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K first() {
            return b().firstKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> headSet(K k) {
            return new h(b().headMap(k));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public K last() {
            return b().lastKey();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> subSet(K k, K k2) {
            return new h(b().subMap(k, k2));
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<K> tailSet(K k) {
            return new h(b().tailMap(k));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class k extends sw3<K, V>.i implements Set<V> {
        @DexIgnore
        public k(K k, Set<V> set) {
            super(k, set, null);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.sw3.i, java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean a = yz3.a((Set<?>) ((Set) ((i) this).b), collection);
            if (a) {
                sw3.access$212(sw3.this, ((i) this).b.size() - size);
                f();
            }
            return a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class l extends sw3<K, V>.i implements SortedSet<V> {
        @DexIgnore
        public l(K k, SortedSet<V> sortedSet, sw3<K, V>.i iVar) {
            super(k, sortedSet, iVar);
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public Comparator<? super V> comparator() {
            return g().comparator();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V first() {
            e();
            return g().first();
        }

        @DexIgnore
        public SortedSet<V> g() {
            return (SortedSet) c();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> headSet(V v) {
            e();
            return new l(d(), g().headSet(v), b() == null ? this : b());
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public V last() {
            e();
            return g().last();
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> subSet(V v, V v2) {
            e();
            return new l(d(), g().subSet(v, v2), b() == null ? this : b());
        }

        @DexIgnore
        @Override // java.util.SortedSet
        public SortedSet<V> tailSet(V v) {
            e();
            return new l(d(), g().tailSet(v), b() == null ? this : b());
        }
    }

    @DexIgnore
    public sw3(Map<K, Collection<V>> map) {
        jw3.a(map.isEmpty());
        this.f = map;
    }

    @DexIgnore
    public static /* synthetic */ int access$208(sw3 sw3) {
        int i2 = sw3.g;
        sw3.g = i2 + 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$210(sw3 sw3) {
        int i2 = sw3.g;
        sw3.g = i2 - 1;
        return i2;
    }

    @DexIgnore
    public static /* synthetic */ int access$212(sw3 sw3, int i2) {
        int i3 = sw3.g + i2;
        sw3.g = i3;
        return i3;
    }

    @DexIgnore
    public static /* synthetic */ int access$220(sw3 sw3, int i2) {
        int i3 = sw3.g - i2;
        sw3.g = i3;
        return i3;
    }

    @DexIgnore
    public final Collection<V> a(K k2) {
        Collection<V> collection = this.f.get(k2);
        if (collection != null) {
            return collection;
        }
        Collection<V> createCollection = createCollection(k2);
        this.f.put(k2, createCollection);
        return createCollection;
    }

    @DexIgnore
    public final void b(Object obj) {
        Collection collection = (Collection) yy3.f(this.f, obj);
        if (collection != null) {
            int size = collection.size();
            collection.clear();
            this.g -= size;
        }
    }

    @DexIgnore
    public Map<K, Collection<V>> backingMap() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public void clear() {
        for (Collection<V> collection : this.f.values()) {
            collection.clear();
        }
        this.f.clear();
        this.g = 0;
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public boolean containsKey(Object obj) {
        return this.f.containsKey(obj);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Map<K, Collection<V>> createAsMap() {
        return this.f instanceof SortedMap ? new g((SortedMap) this.f) : new c(this.f);
    }

    @DexIgnore
    public abstract Collection<V> createCollection();

    @DexIgnore
    public Collection<V> createCollection(K k2) {
        return createCollection();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Set<K> createKeySet() {
        return this.f instanceof SortedMap ? new h((SortedMap) this.f) : new e(this.f);
    }

    @DexIgnore
    public Collection<V> createUnmodifiableEmptyCollection() {
        return unmodifiableCollectionSubclass(createCollection());
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public Collection<Map.Entry<K, V>> entries() {
        return super.entries();
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Iterator<Map.Entry<K, V>> entryIterator() {
        return new b(this);
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public Collection<V> get(K k2) {
        Collection<V> collection = this.f.get(k2);
        if (collection == null) {
            collection = createCollection(k2);
        }
        return wrapCollection(k2, collection);
    }

    @DexIgnore
    @Override // com.fossil.vw3, com.fossil.zy3
    public boolean put(K k2, V v) {
        Collection<V> collection = this.f.get(k2);
        if (collection == null) {
            Collection<V> createCollection = createCollection(k2);
            if (createCollection.add(v)) {
                this.g++;
                this.f.put(k2, createCollection);
                return true;
            }
            throw new AssertionError("New Collection violated the Collection spec");
        } else if (!collection.add(v)) {
            return false;
        } else {
            this.g++;
            return true;
        }
    }

    @DexIgnore
    public Collection<V> removeAll(Object obj) {
        Collection<V> remove = this.f.remove(obj);
        if (remove == null) {
            return createUnmodifiableEmptyCollection();
        }
        Collection<V> createCollection = createCollection();
        createCollection.addAll(remove);
        this.g -= remove.size();
        remove.clear();
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Collection<V> replaceValues(K k2, Iterable<? extends V> iterable) {
        Iterator<? extends V> it = iterable.iterator();
        if (!it.hasNext()) {
            return removeAll(k2);
        }
        Collection<? extends V> a2 = a((Object) k2);
        Collection<V> createCollection = createCollection();
        createCollection.addAll(a2);
        this.g -= a2.size();
        a2.clear();
        while (it.hasNext()) {
            if (a2.add((Object) it.next())) {
                this.g++;
            }
        }
        return unmodifiableCollectionSubclass(createCollection);
    }

    @DexIgnore
    public final void setMap(Map<K, Collection<V>> map) {
        this.f = map;
        this.g = 0;
        for (Collection<V> collection : map.values()) {
            jw3.a(!collection.isEmpty());
            this.g += collection.size();
        }
    }

    @DexIgnore
    @Override // com.fossil.zy3
    public int size() {
        return this.g;
    }

    @DexIgnore
    public Collection<V> unmodifiableCollectionSubclass(Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return Collections.unmodifiableSortedSet((SortedSet) collection);
        }
        if (collection instanceof Set) {
            return Collections.unmodifiableSet((Set) collection);
        }
        if (collection instanceof List) {
            return Collections.unmodifiableList((List) collection);
        }
        return Collections.unmodifiableCollection(collection);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Iterator<V> valueIterator() {
        return new a(this);
    }

    @DexIgnore
    @Override // com.fossil.vw3
    public Collection<V> values() {
        return super.values();
    }

    @DexIgnore
    public Collection<V> wrapCollection(K k2, Collection<V> collection) {
        if (collection instanceof SortedSet) {
            return new l(k2, (SortedSet) collection, null);
        }
        if (collection instanceof Set) {
            return new k(k2, (Set) collection);
        }
        if (collection instanceof List) {
            return a(k2, (List) collection, null);
        }
        return new i(k2, collection, null);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends AbstractCollection<V> {
        @DexIgnore
        public /* final */ K a;
        @DexIgnore
        public Collection<V> b;
        @DexIgnore
        public /* final */ sw3<K, V>.i c;
        @DexIgnore
        public /* final */ Collection<V> d;

        @DexIgnore
        public i(K k, Collection<V> collection, sw3<K, V>.i iVar) {
            Collection<V> collection2;
            this.a = k;
            this.b = collection;
            this.c = iVar;
            if (iVar == null) {
                collection2 = null;
            } else {
                collection2 = iVar.c();
            }
            this.d = collection2;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: java.util.Map */
        /* JADX WARN: Multi-variable type inference failed */
        public void a() {
            sw3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.a();
            } else {
                sw3.this.f.put(this.a, this.b);
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean add(V v) {
            e();
            boolean isEmpty = this.b.isEmpty();
            boolean add = this.b.add(v);
            if (add) {
                sw3.access$208(sw3.this);
                if (isEmpty) {
                    a();
                }
            }
            return add;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = this.b.addAll(collection);
            if (addAll) {
                sw3.access$212(sw3.this, this.b.size() - size);
                if (size == 0) {
                    a();
                }
            }
            return addAll;
        }

        @DexIgnore
        public sw3<K, V>.i b() {
            return this.c;
        }

        @DexIgnore
        public Collection<V> c() {
            return this.b;
        }

        @DexIgnore
        public void clear() {
            int size = size();
            if (size != 0) {
                this.b.clear();
                sw3.access$220(sw3.this, size);
                f();
            }
        }

        @DexIgnore
        public boolean contains(Object obj) {
            e();
            return this.b.contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            e();
            return this.b.containsAll(collection);
        }

        @DexIgnore
        public K d() {
            return this.a;
        }

        @DexIgnore
        public void e() {
            Collection<V> collection;
            sw3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.e();
                if (this.c.c() != this.d) {
                    throw new ConcurrentModificationException();
                }
            } else if (this.b.isEmpty() && (collection = (Collection) sw3.this.f.get(this.a)) != null) {
                this.b = collection;
            }
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            e();
            return this.b.equals(obj);
        }

        @DexIgnore
        public void f() {
            sw3<K, V>.i iVar = this.c;
            if (iVar != null) {
                iVar.f();
            } else if (this.b.isEmpty()) {
                sw3.this.f.remove(this.a);
            }
        }

        @DexIgnore
        public int hashCode() {
            e();
            return this.b.hashCode();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            e();
            return new a();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            e();
            boolean remove = this.b.remove(obj);
            if (remove) {
                sw3.access$210(sw3.this);
                f();
            }
            return remove;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean removeAll = this.b.removeAll(collection);
            if (removeAll) {
                sw3.access$212(sw3.this, this.b.size() - size);
                f();
            }
            return removeAll;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            jw3.a(collection);
            int size = size();
            boolean retainAll = this.b.retainAll(collection);
            if (retainAll) {
                sw3.access$212(sw3.this, this.b.size() - size);
                f();
            }
            return retainAll;
        }

        @DexIgnore
        public int size() {
            e();
            return this.b.size();
        }

        @DexIgnore
        public String toString() {
            e();
            return this.b.toString();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<V> {
            @DexIgnore
            public /* final */ Iterator<V> a;
            @DexIgnore
            public /* final */ Collection<V> b; // = i.this.b;

            @DexIgnore
            public a() {
                this.a = sw3.this.a(i.this.b);
            }

            @DexIgnore
            public Iterator<V> a() {
                b();
                return this.a;
            }

            @DexIgnore
            public void b() {
                i.this.e();
                if (i.this.b != this.b) {
                    throw new ConcurrentModificationException();
                }
            }

            @DexIgnore
            public boolean hasNext() {
                b();
                return this.a.hasNext();
            }

            @DexIgnore
            @Override // java.util.Iterator
            public V next() {
                b();
                return this.a.next();
            }

            @DexIgnore
            public void remove() {
                this.a.remove();
                sw3.access$210(sw3.this);
                i.this.f();
            }

            @DexIgnore
            public a(Iterator<V> it) {
                this.a = it;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j extends sw3<K, V>.i implements List<V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends sw3<K, V>.i.a implements ListIterator<V> {
            @DexIgnore
            public a() {
                super();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void add(V v) {
                boolean isEmpty = j.this.isEmpty();
                c().add(v);
                sw3.access$208(sw3.this);
                if (isEmpty) {
                    j.this.a();
                }
            }

            @DexIgnore
            public final ListIterator<V> c() {
                return (ListIterator) a();
            }

            @DexIgnore
            public boolean hasPrevious() {
                return c().hasPrevious();
            }

            @DexIgnore
            public int nextIndex() {
                return c().nextIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public V previous() {
                return c().previous();
            }

            @DexIgnore
            public int previousIndex() {
                return c().previousIndex();
            }

            @DexIgnore
            @Override // java.util.ListIterator
            public void set(V v) {
                c().set(v);
            }

            @DexIgnore
            public a(int i) {
                super(j.this.g().listIterator(i));
            }
        }

        @DexIgnore
        public j(K k, List<V> list, sw3<K, V>.i iVar) {
            super(k, list, iVar);
        }

        @DexIgnore
        @Override // java.util.List
        public void add(int i, V v) {
            e();
            boolean isEmpty = c().isEmpty();
            g().add(i, v);
            sw3.access$208(sw3.this);
            if (isEmpty) {
                a();
            }
        }

        @DexIgnore
        @Override // java.util.List
        public boolean addAll(int i, Collection<? extends V> collection) {
            if (collection.isEmpty()) {
                return false;
            }
            int size = size();
            boolean addAll = g().addAll(i, collection);
            if (addAll) {
                sw3.access$212(sw3.this, c().size() - size);
                if (size == 0) {
                    a();
                }
            }
            return addAll;
        }

        @DexIgnore
        public List<V> g() {
            return (List) c();
        }

        @DexIgnore
        @Override // java.util.List
        public V get(int i) {
            e();
            return g().get(i);
        }

        @DexIgnore
        public int indexOf(Object obj) {
            e();
            return g().indexOf(obj);
        }

        @DexIgnore
        public int lastIndexOf(Object obj) {
            e();
            return g().lastIndexOf(obj);
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator() {
            e();
            return new a();
        }

        @DexIgnore
        @Override // java.util.List
        public V remove(int i) {
            e();
            V remove = g().remove(i);
            sw3.access$210(sw3.this);
            f();
            return remove;
        }

        @DexIgnore
        @Override // java.util.List
        public V set(int i, V v) {
            e();
            return g().set(i, v);
        }

        @DexIgnore
        @Override // java.util.List
        public List<V> subList(int i, int i2) {
            e();
            return sw3.this.a(d(), g().subList(i, i2), b() == null ? this : b());
        }

        @DexIgnore
        @Override // java.util.List
        public ListIterator<V> listIterator(int i) {
            e();
            return new a(i);
        }
    }

    @DexIgnore
    public final List<V> a(K k2, List<V> list, sw3<K, V>.i iVar) {
        return list instanceof RandomAccess ? new f(this, k2, list, iVar) : new j(k2, list, iVar);
    }

    @DexIgnore
    public final Iterator<V> a(Collection<V> collection) {
        return collection instanceof List ? ((List) collection).listIterator() : collection.iterator();
    }
}
