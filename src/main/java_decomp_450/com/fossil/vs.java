package com.fossil;

import android.view.View;
import coil.memory.ViewTargetRequestDelegate;
import com.fossil.ik7;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vs implements View.OnAttachStateChangeListener {
    @DexIgnore
    public ViewTargetRequestDelegate a;
    @DexIgnore
    public volatile UUID b;
    @DexIgnore
    public volatile ik7 c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e; // = true;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "coil.memory.ViewTargetRequestManager$clearCurrentRequest$1", f = "ViewTargetRequestManager.kt", l = {}, m = "invokeSuspend")
    public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ vs this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(vs vsVar, fb7 fb7) {
            super(2, fb7);
            this.this$0 = vsVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            a aVar = new a(this.this$0, fb7);
            aVar.p$ = (yi7) obj;
            return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                this.this$0.a((ViewTargetRequestDelegate) null);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    public final void a(ViewTargetRequestDelegate viewTargetRequestDelegate) {
        if (this.d) {
            this.d = false;
        } else {
            ik7 ik7 = this.c;
            if (ik7 != null) {
                ik7.a.a(ik7, null, 1, null);
            }
            this.c = null;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate2 = this.a;
        if (viewTargetRequestDelegate2 != null) {
            viewTargetRequestDelegate2.b();
        }
        this.a = viewTargetRequestDelegate;
        this.e = true;
    }

    @DexIgnore
    public final UUID b() {
        UUID uuid = this.b;
        if (uuid != null && iu.a() && this.d) {
            return uuid;
        }
        UUID randomUUID = UUID.randomUUID();
        ee7.a((Object) randomUUID, "UUID.randomUUID()");
        return randomUUID;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        ee7.b(view, "v");
        if (this.e) {
            this.e = false;
            return;
        }
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.a;
        if (viewTargetRequestDelegate != null) {
            this.d = true;
            viewTargetRequestDelegate.c();
        }
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        ee7.b(view, "v");
        this.e = false;
        ViewTargetRequestDelegate viewTargetRequestDelegate = this.a;
        if (viewTargetRequestDelegate != null) {
            viewTargetRequestDelegate.b();
        }
    }

    @DexIgnore
    public final UUID a(ik7 ik7) {
        ee7.b(ik7, "job");
        UUID b2 = b();
        this.b = b2;
        return b2;
    }

    @DexIgnore
    public final void a() {
        this.b = null;
        ik7 ik7 = this.c;
        if (ik7 != null) {
            ik7.a.a(ik7, null, 1, null);
        }
        this.c = xh7.b(zi7.a(qj7.c().g()), null, null, new a(this, null), 3, null);
    }
}
