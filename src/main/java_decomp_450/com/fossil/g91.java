package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g91 extends pe1 {
    @DexIgnore
    public static /* final */ k71 CREATOR; // = new k71(null);
    @DexIgnore
    public /* final */ am1 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public /* synthetic */ g91(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = am1.values()[parcel.readInt()];
        this.c = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.V3, yz0.a(this.b)), r51.W3, Boolean.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(3)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.putShort(this.b.a);
        order.put((byte) ((this.b.b.a << 7) | this.c));
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(g91.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            g91 g91 = (g91) obj;
            return this.b == g91.b && this.c == g91.c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.HIDInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode() + (this.b.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.b.ordinal());
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
