package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap4 {
    @DexIgnore
    public /* final */ ApiServiceV2 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource", f = "NotificationRemoteDataSource.kt", l = {16}, m = "notifications")
    public static final class a extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ap4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ap4 ap4, fb7 fb7) {
            super(fb7);
            this.this$0 = ap4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(0, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource$notifications$response$1", f = "NotificationRemoteDataSource.kt", l = {16}, m = "invokeSuspend")
    public static final class b extends zb7 implements gd7<fb7<? super fv7<ApiResponse<ao4>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $limit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ ap4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ap4 ap4, int i, fb7 fb7) {
            super(1, fb7);
            this.this$0 = ap4;
            this.$limit = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new b(this.this$0, this.$limit, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<ao4>>> fb7) {
            return ((b) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a;
                Integer a3 = pb7.a(this.$limit);
                this.label = 1;
                obj = a2.notifications(a3, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore
    public ap4(ApiServiceV2 apiServiceV2) {
        ee7.b(apiServiceV2, "api");
        this.a = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0063  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(int r9, com.fossil.fb7<? super com.fossil.zi5<com.portfolio.platform.data.source.remote.ApiResponse<com.fossil.ao4>>> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.ap4.a
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.ap4$a r0 = (com.fossil.ap4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ap4$a r0 = new com.fossil.ap4$a
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0037
            if (r2 != r3) goto L_0x002f
            int r9 = r0.I$0
            java.lang.Object r9 = r0.L$0
            com.fossil.ap4 r9 = (com.fossil.ap4) r9
            com.fossil.t87.a(r10)
            goto L_0x004d
        L_0x002f:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0037:
            com.fossil.t87.a(r10)
            com.fossil.ap4$b r10 = new com.fossil.ap4$b
            r2 = 0
            r10.<init>(r8, r9, r2)
            r0.L$0 = r8
            r0.I$0 = r9
            r0.label = r3
            java.lang.Object r10 = com.fossil.aj5.a(r10, r0)
            if (r10 != r1) goto L_0x004d
            return r1
        L_0x004d:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r9 = r10 instanceof com.fossil.bj5
            if (r9 == 0) goto L_0x0063
            com.fossil.bj5 r9 = new com.fossil.bj5
            com.fossil.bj5 r10 = (com.fossil.bj5) r10
            java.lang.Object r0 = r10.a()
            boolean r10 = r10.b()
            r9.<init>(r0, r10)
            goto L_0x007d
        L_0x0063:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x007e
            com.fossil.yi5 r9 = new com.fossil.yi5
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r1 = r10.a()
            com.portfolio.platform.data.model.ServerError r2 = r10.c()
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 28
            r7 = 0
            r0 = r9
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
        L_0x007d:
            return r9
        L_0x007e:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ap4.a(int, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(ap4 ap4, int i, fb7 fb7, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 100;
        }
        return ap4.a(i, fb7);
    }
}
