package com.fossil;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wq7 extends tr7 {
    @DexIgnore
    public static /* final */ long h; // = TimeUnit.SECONDS.toMillis(60);
    @DexIgnore
    public static /* final */ long i; // = TimeUnit.MILLISECONDS.toNanos(h);
    @DexIgnore
    public static wq7 j;
    @DexIgnore
    public static /* final */ a k; // = new a(null);
    @DexIgnore
    public boolean e;
    @DexIgnore
    public wq7 f;
    @DexIgnore
    public long g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final void a(wq7 wq7, long j, boolean z) {
            synchronized (wq7.class) {
                if (wq7.j == null) {
                    wq7.j = new wq7();
                    new b().start();
                }
                long nanoTime = System.nanoTime();
                int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
                if (i != 0 && z) {
                    wq7.g = Math.min(j, wq7.c() - nanoTime) + nanoTime;
                } else if (i != 0) {
                    wq7.g = j + nanoTime;
                } else if (z) {
                    wq7.g = wq7.c();
                } else {
                    throw new AssertionError();
                }
                long a = wq7.b(nanoTime);
                wq7 j2 = wq7.j;
                if (j2 != null) {
                    while (true) {
                        if (j2.f == null) {
                            break;
                        }
                        wq7 a2 = j2.f;
                        if (a2 == null) {
                            ee7.a();
                            throw null;
                        } else if (a < a2.b(nanoTime)) {
                            break;
                        } else {
                            j2 = j2.f;
                            if (j2 == null) {
                                ee7.a();
                                throw null;
                            }
                        }
                    }
                    wq7.f = j2.f;
                    j2.f = wq7;
                    if (j2 == wq7.j) {
                        wq7.class.notify();
                    }
                    i97 i97 = i97.a;
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        public final boolean a(wq7 wq7) {
            synchronized (wq7.class) {
                for (wq7 j = wq7.j; j != null; j = j.f) {
                    if (j.f == wq7) {
                        j.f = wq7.f;
                        wq7.f = (wq7) null;
                        return false;
                    }
                }
                return true;
            }
        }

        @DexIgnore
        public final wq7 a() throws InterruptedException {
            wq7 j = wq7.j;
            if (j != null) {
                wq7 a = j.f;
                if (a == null) {
                    long nanoTime = System.nanoTime();
                    wq7.class.wait(wq7.h);
                    wq7 j2 = wq7.j;
                    if (j2 == null) {
                        ee7.a();
                        throw null;
                    } else if (j2.f != null || System.nanoTime() - nanoTime < wq7.i) {
                        return null;
                    } else {
                        return wq7.j;
                    }
                } else {
                    long a2 = a.b(System.nanoTime());
                    if (a2 > 0) {
                        long j3 = a2 / 1000000;
                        wq7.class.wait(j3, (int) (a2 - (1000000 * j3)));
                        return null;
                    }
                    wq7 j4 = wq7.j;
                    if (j4 != null) {
                        j4.f = a.f;
                        a.f = (wq7) null;
                        return a;
                    }
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends Thread {
        @DexIgnore
        public b() {
            super("Okio Watchdog");
            setDaemon(true);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0018, code lost:
            if (r1 == null) goto L_0x0000;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
            r1.i();
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r3 = this;
            L_0x0000:
                java.lang.Class<com.fossil.wq7> r0 = com.fossil.wq7.class
                monitor-enter(r0)     // Catch:{ InterruptedException -> 0x0000 }
                com.fossil.wq7$a r1 = com.fossil.wq7.k     // Catch:{ all -> 0x001e }
                com.fossil.wq7 r1 = r1.a()     // Catch:{ all -> 0x001e }
                com.fossil.wq7 r2 = com.fossil.wq7.j     // Catch:{ all -> 0x001e }
                if (r1 != r2) goto L_0x0015
                r1 = 0
                com.fossil.wq7.j = r1     // Catch:{ all -> 0x001e }
                monitor-exit(r0)
                return
            L_0x0015:
                com.fossil.i97 r2 = com.fossil.i97.a
                monitor-exit(r0)
                if (r1 == 0) goto L_0x0000
                r1.i()
                goto L_0x0000
            L_0x001e:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.wq7.b.run():void");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements qr7 {
        @DexIgnore
        public /* final */ /* synthetic */ wq7 a;
        @DexIgnore
        public /* final */ /* synthetic */ qr7 b;

        @DexIgnore
        public c(wq7 wq7, qr7 qr7) {
            this.a = wq7;
            this.b = qr7;
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public void a(yq7 yq7, long j) {
            ee7.b(yq7, "source");
            vq7.a(yq7.x(), 0, j);
            while (true) {
                long j2 = 0;
                if (j > 0) {
                    nr7 nr7 = yq7.a;
                    if (nr7 != null) {
                        while (true) {
                            if (j2 >= ((long) 65536)) {
                                break;
                            }
                            j2 += (long) (nr7.c - nr7.b);
                            if (j2 >= j) {
                                j2 = j;
                                break;
                            }
                            nr7 = nr7.f;
                            if (nr7 == null) {
                                ee7.a();
                                throw null;
                            }
                        }
                        wq7 wq7 = this.a;
                        wq7.g();
                        try {
                            this.b.a(yq7, j2);
                            i97 i97 = i97.a;
                            if (!wq7.h()) {
                                j -= j2;
                            } else {
                                throw wq7.a((IOException) null);
                            }
                        } catch (IOException e) {
                            if (!wq7.h()) {
                                throw e;
                            }
                            throw wq7.a(e);
                        } finally {
                            wq7.h();
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    return;
                }
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.qr7, java.lang.AutoCloseable
        public void close() {
            wq7 wq7 = this.a;
            wq7.g();
            try {
                this.b.close();
                i97 i97 = i97.a;
                if (wq7.h()) {
                    throw wq7.a((IOException) null);
                }
            } catch (IOException e) {
                if (!wq7.h()) {
                    throw e;
                }
                throw wq7.a(e);
            } finally {
                wq7.h();
            }
        }

        @DexIgnore
        @Override // com.fossil.qr7, java.io.Flushable
        public void flush() {
            wq7 wq7 = this.a;
            wq7.g();
            try {
                this.b.flush();
                i97 i97 = i97.a;
                if (wq7.h()) {
                    throw wq7.a((IOException) null);
                }
            } catch (IOException e) {
                if (!wq7.h()) {
                    throw e;
                }
                throw wq7.a(e);
            } finally {
                wq7.h();
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.sink(" + this.b + ')';
        }

        @DexIgnore
        @Override // com.fossil.qr7
        public wq7 d() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements sr7 {
        @DexIgnore
        public /* final */ /* synthetic */ wq7 a;
        @DexIgnore
        public /* final */ /* synthetic */ sr7 b;

        @DexIgnore
        public d(wq7 wq7, sr7 sr7) {
            this.a = wq7;
            this.b = sr7;
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public long b(yq7 yq7, long j) {
            ee7.b(yq7, "sink");
            wq7 wq7 = this.a;
            wq7.g();
            try {
                long b2 = this.b.b(yq7, j);
                if (!wq7.h()) {
                    return b2;
                }
                throw wq7.a((IOException) null);
            } catch (IOException e) {
                if (!wq7.h()) {
                    throw e;
                }
                throw wq7.a(e);
            } finally {
                wq7.h();
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() {
            wq7 wq7 = this.a;
            wq7.g();
            try {
                this.b.close();
                i97 i97 = i97.a;
                if (wq7.h()) {
                    throw wq7.a((IOException) null);
                }
            } catch (IOException e) {
                if (!wq7.h()) {
                    throw e;
                }
                throw wq7.a(e);
            } finally {
                wq7.h();
            }
        }

        @DexIgnore
        public String toString() {
            return "AsyncTimeout.source(" + this.b + ')';
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public wq7 d() {
            return this.a;
        }
    }

    @DexIgnore
    public final void g() {
        if (!this.e) {
            long f2 = f();
            boolean d2 = d();
            if (f2 != 0 || d2) {
                this.e = true;
                k.a(this, f2, d2);
                return;
            }
            return;
        }
        throw new IllegalStateException("Unbalanced enter/exit".toString());
    }

    @DexIgnore
    public final boolean h() {
        if (!this.e) {
            return false;
        }
        this.e = false;
        return k.a(this);
    }

    @DexIgnore
    public void i() {
    }

    @DexIgnore
    public final long b(long j2) {
        return this.g - j2;
    }

    @DexIgnore
    public final qr7 a(qr7 qr7) {
        ee7.b(qr7, "sink");
        return new c(this, qr7);
    }

    @DexIgnore
    public IOException b(IOException iOException) {
        InterruptedIOException interruptedIOException = new InterruptedIOException("timeout");
        if (iOException != null) {
            interruptedIOException.initCause(iOException);
        }
        return interruptedIOException;
    }

    @DexIgnore
    public final sr7 a(sr7 sr7) {
        ee7.b(sr7, "source");
        return new d(this, sr7);
    }

    @DexIgnore
    public final IOException a(IOException iOException) {
        return b(iOException);
    }
}
