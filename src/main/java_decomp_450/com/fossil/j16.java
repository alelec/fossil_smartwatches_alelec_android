package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j16 implements Factory<i16> {
    @DexIgnore
    public static i16 a(w06 w06, ch5 ch5, UserRepository userRepository) {
        return new i16(w06, ch5, userRepository);
    }
}
