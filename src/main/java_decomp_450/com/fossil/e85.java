package com.fossil;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ ImageButton t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ View v;

    @DexIgnore
    public e85(Object obj, View view, int i, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ImageButton imageButton, ImageView imageView, View view2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = imageButton;
        this.u = imageView;
        this.v = view2;
    }
}
