package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ue7 {
    @DexIgnore
    public ag7 a(le7 le7) {
        return le7;
    }

    @DexIgnore
    public bg7 a(me7 me7) {
        return me7;
    }

    @DexIgnore
    public uf7 a(Class cls, String str) {
        return new ke7(cls, str);
    }

    @DexIgnore
    public vf7 a(ce7 ce7) {
        return ce7;
    }

    @DexIgnore
    public xf7 a(ge7 ge7) {
        return ge7;
    }

    @DexIgnore
    public yf7 a(he7 he7) {
        return he7;
    }

    @DexIgnore
    public tf7 a(Class cls) {
        return new xd7(cls);
    }

    @DexIgnore
    public String a(fe7 fe7) {
        return a((be7) fe7);
    }

    @DexIgnore
    public String a(be7 be7) {
        String obj = be7.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
