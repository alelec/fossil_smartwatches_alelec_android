package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface tt7 {
    @DexIgnore
    void debug(String str);

    @DexIgnore
    void debug(String str, Object obj);

    @DexIgnore
    void debug(String str, Object obj, Object obj2);

    @DexIgnore
    void debug(String str, Throwable th);

    @DexIgnore
    void debug(String str, Object... objArr);

    @DexIgnore
    void error(String str);

    @DexIgnore
    void error(String str, Object obj);

    @DexIgnore
    void error(String str, Object obj, Object obj2);

    @DexIgnore
    void error(String str, Throwable th);

    @DexIgnore
    void error(String str, Object... objArr);

    @DexIgnore
    void info(String str);

    @DexIgnore
    void info(String str, Object obj);

    @DexIgnore
    void info(String str, Object obj, Object obj2);

    @DexIgnore
    void info(String str, Throwable th);

    @DexIgnore
    void info(String str, Object... objArr);

    @DexIgnore
    boolean isDebugEnabled();

    @DexIgnore
    boolean isErrorEnabled();

    @DexIgnore
    boolean isInfoEnabled();

    @DexIgnore
    boolean isTraceEnabled();

    @DexIgnore
    boolean isWarnEnabled();

    @DexIgnore
    void trace(String str);

    @DexIgnore
    void trace(String str, Object obj);

    @DexIgnore
    void trace(String str, Object obj, Object obj2);

    @DexIgnore
    void trace(String str, Throwable th);

    @DexIgnore
    void trace(String str, Object... objArr);

    @DexIgnore
    void warn(String str);

    @DexIgnore
    void warn(String str, Object obj);

    @DexIgnore
    void warn(String str, Object obj, Object obj2);

    @DexIgnore
    void warn(String str, Throwable th);

    @DexIgnore
    void warn(String str, Object... objArr);
}
