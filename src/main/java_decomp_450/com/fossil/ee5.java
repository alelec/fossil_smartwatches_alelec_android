package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import java.io.File;
import java.io.FileInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ee5 {
    @DexIgnore
    public static /* final */ ee5 a; // = new ee5();

    @DexIgnore
    public final boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            if (str != null) {
                return new File(str).exists();
            }
            ee7.a();
            throw null;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public final Drawable b(String str, int i, int i2, FileType fileType) {
        ee7.b(fileType, "type");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapDrawable b = hg5.b().b(str);
        if (b != null) {
            return b;
        }
        try {
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.g0.c().getResources(), a(str, i, i2, fileType));
            hg5.b().a(str, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0041 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0046 A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x004d A[Catch:{ IOException -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0052 A[Catch:{ IOException -> 0x0056 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(com.fossil.mo7 r5, java.lang.String r6) {
        /*
            r4 = this;
            java.lang.String r0 = "body"
            com.fossil.ee7.b(r5, r0)
            java.lang.String r0 = "path"
            com.fossil.ee7.b(r6, r0)
            r0 = 4096(0x1000, float:5.74E-42)
            r1 = 0
            r2 = 0
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x004a, all -> 0x003d }
            java.io.InputStream r5 = r5.byteStream()     // Catch:{ IOException -> 0x004a, all -> 0x003d }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x003a, all -> 0x0036 }
            r3.<init>(r6)     // Catch:{ IOException -> 0x003a, all -> 0x0036 }
            if (r5 == 0) goto L_0x0029
        L_0x001b:
            int r6 = r5.read(r0)     // Catch:{ IOException -> 0x003b, all -> 0x0027 }
            r1 = -1
            if (r6 != r1) goto L_0x0023
            goto L_0x0029
        L_0x0023:
            r3.write(r0, r2, r6)     // Catch:{ IOException -> 0x003b, all -> 0x0027 }
            goto L_0x001b
        L_0x0027:
            r6 = move-exception
            goto L_0x0038
        L_0x0029:
            r3.flush()     // Catch:{ IOException -> 0x003b, all -> 0x0027 }
            r6 = 1
            if (r5 == 0) goto L_0x0032
            r5.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0032:
            r3.close()     // Catch:{ IOException -> 0x0056 }
            return r6
        L_0x0036:
            r6 = move-exception
            r3 = r1
        L_0x0038:
            r1 = r5
            goto L_0x003f
        L_0x003a:
            r3 = r1
        L_0x003b:
            r1 = r5
            goto L_0x004b
        L_0x003d:
            r6 = move-exception
            r3 = r1
        L_0x003f:
            if (r1 == 0) goto L_0x0044
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0044:
            if (r3 == 0) goto L_0x0049
            r3.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0049:
            throw r6     // Catch:{ IOException -> 0x0056 }
        L_0x004a:
            r3 = r1
        L_0x004b:
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0050:
            if (r3 == 0) goto L_0x0056
            r3.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0056:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ee5.a(com.fossil.mo7, java.lang.String):boolean");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0028 A[Catch:{ IOException -> 0x0032 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x002e A[Catch:{ IOException -> 0x0032 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.graphics.Bitmap r4, java.lang.String r5) {
        /*
            r3 = this;
            java.lang.String r0 = "body"
            com.fossil.ee7.b(r4, r0)
            java.lang.String r0 = "path"
            com.fossil.ee7.b(r5, r0)
            r0 = 0
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x002c, all -> 0x0025 }
            r2.<init>(r5)     // Catch:{ IOException -> 0x002c, all -> 0x0025 }
            android.graphics.Bitmap$CompressFormat r5 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ IOException -> 0x0023, all -> 0x0020 }
            r0 = 100
            r4.compress(r5, r0, r2)     // Catch:{ IOException -> 0x0023, all -> 0x0020 }
            r2.flush()     // Catch:{ IOException -> 0x0023, all -> 0x0020 }
            r4 = 1
            r2.close()     // Catch:{ IOException -> 0x0032 }
            return r4
        L_0x0020:
            r4 = move-exception
            r0 = r2
            goto L_0x0026
        L_0x0023:
            r0 = r2
            goto L_0x002c
        L_0x0025:
            r4 = move-exception
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            r0.close()     // Catch:{ IOException -> 0x0032 }
        L_0x002b:
            throw r4     // Catch:{ IOException -> 0x0032 }
        L_0x002c:
            if (r0 == 0) goto L_0x0032
            r0.close()     // Catch:{ IOException -> 0x0032 }
        L_0x0032:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ee5.a(android.graphics.Bitmap, java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006d, code lost:
        com.fossil.hc7.a(r4, r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0070, code lost:
        throw r0;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r4, com.misfit.frameworks.buttonservice.model.FileType r5) {
        /*
            r3 = this;
            java.lang.String r0 = "nameFile"
            com.fossil.ee7.b(r4, r0)
            java.lang.String r0 = "type"
            com.fossil.ee7.b(r5, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "open nameFile "
            r1.append(r2)
            r1.append(r4)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "FileHelper"
            r0.d(r2, r1)
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x002f
            goto L_0x007a
        L_0x002f:
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x0071 }
            com.portfolio.platform.PortfolioApp r0 = r0.c()     // Catch:{ Exception -> 0x0071 }
            android.content.Context r0 = r0.getApplicationContext()     // Catch:{ Exception -> 0x0071 }
            java.lang.String r5 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r0, r5)     // Catch:{ Exception -> 0x0071 }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0071 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0071 }
            r2.<init>()     // Catch:{ Exception -> 0x0071 }
            r2.append(r5)     // Catch:{ Exception -> 0x0071 }
            java.lang.String r5 = java.io.File.separator     // Catch:{ Exception -> 0x0071 }
            r2.append(r5)     // Catch:{ Exception -> 0x0071 }
            r2.append(r4)     // Catch:{ Exception -> 0x0071 }
            java.lang.String r4 = r2.toString()     // Catch:{ Exception -> 0x0071 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0071 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0071 }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0071 }
            r5 = 0
            byte[] r0 = com.fossil.rs7.b(r4)     // Catch:{ all -> 0x006a }
            r2 = 0
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r2)     // Catch:{ all -> 0x006a }
            com.fossil.hc7.a(r4, r5)
            r1 = r0
            goto L_0x0075
        L_0x006a:
            r5 = move-exception
            throw r5     // Catch:{ all -> 0x006c }
        L_0x006c:
            r0 = move-exception
            com.fossil.hc7.a(r4, r5)
            throw r0
        L_0x0071:
            r4 = move-exception
            r4.printStackTrace()
        L_0x0075:
            java.lang.String r4 = "try {\n                va\u2026         \"\"\n            }"
            com.fossil.ee7.a(r1, r4)
        L_0x007a:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ee5.a(java.lang.String, com.misfit.frameworks.buttonservice.model.FileType):java.lang.String");
    }

    @DexIgnore
    public final Drawable a(String str, int i, int i2, int i3, FileType fileType) {
        String str2;
        Bitmap bitmap;
        ee7.b(fileType, "type");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (i3 == qb5.BACKGROUND.getValue()) {
            str2 = str;
        } else if (i3 == qb5.PHOTO.getValue()) {
            str2 = str + i + i2;
        } else {
            str2 = null;
        }
        BitmapDrawable b = hg5.b().b(str2);
        if (b != null) {
            return b;
        }
        try {
            if (i3 == qb5.BACKGROUND.getValue()) {
                bitmap = a(str, i, i2, fileType);
            } else if (i3 == qb5.PHOTO.getValue()) {
                BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
                if (str != null) {
                    bitmap = bitmapUtils.decodeBitmapFromDirectory(str, i, i2);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                bitmap = null;
            }
            if (bitmap == null) {
                return null;
            }
            BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.g0.c().getResources(), Bitmap.createScaledBitmap(bitmap, i, i2, false));
            hg5.b().a(str2, bitmapDrawable);
            return bitmapDrawable;
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public final Bitmap a(String str, int i, int i2, FileType fileType) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return je5.a(new FileInputStream(new File(FileUtils.getDirectory(PortfolioApp.g0.c().getApplicationContext(), fileType) + File.separator + mg5.a(str))), i, i2);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
