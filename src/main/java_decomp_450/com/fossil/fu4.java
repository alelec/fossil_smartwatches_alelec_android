package com.fossil;

import android.content.Intent;
import com.portfolio.platform.PortfolioApp;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu4 {
    @DexIgnore
    public static /* final */ fu4 a; // = new fu4();

    @DexIgnore
    public final String a(String str, String str2, String str3) {
        ee7.b(str3, "socialId");
        boolean z = false;
        if (!(str == null || str.length() == 0)) {
            return str;
        }
        if (str2 == null || str2.length() == 0) {
            z = true;
        }
        return !z ? str2 : str3;
    }

    @DexIgnore
    public final String b(String str, String str2, String str3) {
        ee7.b(str3, "socialId");
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887270);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026allenge_format_full_name)");
        Object[] objArr = new Object[2];
        if (str == null) {
            str = "";
        }
        objArr[0] = str;
        if (str2 == null) {
            str2 = "";
        }
        objArr[1] = str2;
        String format = String.format(a2, Arrays.copyOf(objArr, 2));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return ee7.a(format, " ") ? str3 : format;
    }

    @DexIgnore
    public final void a(boolean z) {
        Intent intent = new Intent("set_sync_data_event");
        intent.putExtra("set_sync_data_extra", z);
        qe.a(PortfolioApp.g0.c()).a(intent);
    }
}
