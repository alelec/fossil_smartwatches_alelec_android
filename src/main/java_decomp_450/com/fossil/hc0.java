package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hc0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hc0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hc0 createFromParcel(Parcel parcel) {
            return new hc0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hc0[] newArray(int i) {
            return new hc0[i];
        }
    }

    @DexIgnore
    public hc0(byte b, int i, long j) {
        super(cb0.WORKOUT_GPS_DISTANCE, b, i);
        this.d = j;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60, com.fossil.yb0
    public JSONObject a() {
        return yz0.a(super.a(), r51.G5, Long.valueOf(this.d));
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(hc0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((hc0) obj).d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutGPSDistanceRequest");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public int hashCode() {
        return Long.valueOf(this.d).hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.yb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ hc0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = parcel.readLong();
    }
}
