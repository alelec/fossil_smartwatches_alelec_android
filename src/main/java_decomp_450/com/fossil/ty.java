package com.fossil;

import com.fossil.w50;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ty<Z> implements uy<Z>, w50.f {
    @DexIgnore
    public static /* final */ b9<ty<?>> e; // = w50.a(20, new a());
    @DexIgnore
    public /* final */ y50 a; // = y50.b();
    @DexIgnore
    public uy<Z> b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements w50.d<ty<?>> {
        @DexIgnore
        @Override // com.fossil.w50.d
        public ty<?> create() {
            return new ty<>();
        }
    }

    @DexIgnore
    public static <Z> ty<Z> b(uy<Z> uyVar) {
        ty<?> a2 = e.a();
        u50.a(a2);
        ty<Z> tyVar = (ty<Z>) a2;
        tyVar.a(uyVar);
        return tyVar;
    }

    @DexIgnore
    public final void a(uy<Z> uyVar) {
        this.d = false;
        this.c = true;
        this.b = uyVar;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return this.b.c();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<Z> d() {
        return this.b.d();
    }

    @DexIgnore
    public final void e() {
        this.b = null;
        e.a(this);
    }

    @DexIgnore
    public synchronized void f() {
        this.a.a();
        if (this.c) {
            this.c = false;
            if (this.d) {
                b();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Z get() {
        return this.b.get();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public synchronized void b() {
        this.a.a();
        this.d = true;
        if (!this.c) {
            this.b.b();
            e();
        }
    }

    @DexIgnore
    @Override // com.fossil.w50.f
    public y50 a() {
        return this.a;
    }
}
