package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur2<T> implements tr2<T> {
    @DexIgnore
    public volatile tr2<T> a;
    @DexIgnore
    public volatile boolean b;
    @DexIgnore
    @NullableDecl
    public T c;

    @DexIgnore
    public ur2(tr2<T> tr2) {
        or2.a(tr2);
        this.a = tr2;
    }

    @DexIgnore
    public final String toString() {
        Object obj = this.a;
        if (obj == null) {
            String valueOf = String.valueOf(this.c);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(SimpleComparison.GREATER_THAN_OPERATION);
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }

    @DexIgnore
    @Override // com.fossil.tr2
    public final T zza() {
        if (!this.b) {
            synchronized (this) {
                if (!this.b) {
                    T zza = this.a.zza();
                    this.c = zza;
                    this.b = true;
                    this.a = null;
                    return zza;
                }
            }
        }
        return this.c;
    }
}
