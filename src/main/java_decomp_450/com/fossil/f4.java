package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f4 {
    @DexIgnore
    public static /* final */ int[] CardView; // = {16843071, 16843072, 2130968874, 2130968875, 2130968876, 2130968878, 2130968879, 2130968880, 2130968985, 2130968986, 2130968987, 2130968988, 2130968989};
    @DexIgnore
    public static /* final */ int CardView_android_minHeight; // = 1;
    @DexIgnore
    public static /* final */ int CardView_android_minWidth; // = 0;
    @DexIgnore
    public static /* final */ int CardView_cardBackgroundColor; // = 2;
    @DexIgnore
    public static /* final */ int CardView_cardCornerRadius; // = 3;
    @DexIgnore
    public static /* final */ int CardView_cardElevation; // = 4;
    @DexIgnore
    public static /* final */ int CardView_cardMaxElevation; // = 5;
    @DexIgnore
    public static /* final */ int CardView_cardPreventCornerOverlap; // = 6;
    @DexIgnore
    public static /* final */ int CardView_cardUseCompatPadding; // = 7;
    @DexIgnore
    public static /* final */ int CardView_contentPadding; // = 8;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingBottom; // = 9;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingLeft; // = 10;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingRight; // = 11;
    @DexIgnore
    public static /* final */ int CardView_contentPaddingTop; // = 12;
}
