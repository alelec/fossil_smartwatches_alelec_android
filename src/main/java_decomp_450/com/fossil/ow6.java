package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow6 implements Factory<nw6> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<UserRepository> b;
    @DexIgnore
    public /* final */ Provider<aw6> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;

    @DexIgnore
    public ow6(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<aw6> provider3, Provider<PortfolioApp> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static ow6 a(Provider<DeviceRepository> provider, Provider<UserRepository> provider2, Provider<aw6> provider3, Provider<PortfolioApp> provider4) {
        return new ow6(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public static nw6 a(DeviceRepository deviceRepository, UserRepository userRepository, aw6 aw6, PortfolioApp portfolioApp) {
        return new nw6(deviceRepository, userRepository, aw6, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nw6 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get());
    }
}
