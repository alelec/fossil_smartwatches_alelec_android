package com.fossil;

import android.graphics.Matrix;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sj {
    @DexIgnore
    public static oj a(View view, ViewGroup viewGroup, Matrix matrix) {
        if (Build.VERSION.SDK_INT == 28) {
            return qj.a(view, viewGroup, matrix);
        }
        return rj.a(view, viewGroup, matrix);
    }

    @DexIgnore
    public static void a(View view) {
        if (Build.VERSION.SDK_INT == 28) {
            qj.a(view);
        } else {
            rj.b(view);
        }
    }
}
