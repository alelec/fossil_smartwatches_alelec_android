package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rd4 {
    @DexIgnore
    public static volatile rd4 b;
    @DexIgnore
    public /* final */ Set<td4> a; // = new HashSet();

    @DexIgnore
    public static rd4 b() {
        rd4 rd4 = b;
        if (rd4 == null) {
            synchronized (rd4.class) {
                rd4 = b;
                if (rd4 == null) {
                    rd4 = new rd4();
                    b = rd4;
                }
            }
        }
        return rd4;
    }

    @DexIgnore
    public Set<td4> a() {
        Set<td4> unmodifiableSet;
        synchronized (this.a) {
            unmodifiableSet = Collections.unmodifiableSet(this.a);
        }
        return unmodifiableSet;
    }
}
