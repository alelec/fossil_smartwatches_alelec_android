package com.fossil;

import android.content.Context;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class do5 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(int i) {
            int i2 = (int) (((double) i) * 1.15d);
            if (i2 >= 0 && 9 >= i2) {
                if (i2 % 2 != 0) {
                    return ((i2 / 2) * 2) + 2;
                }
                return i2;
            } else if (10 <= i2 && 99 >= i2) {
                if (i2 % 10 != 0) {
                    return ((i2 / 10) * 10) + 10;
                }
                return i2;
            } else if (100 > i2 || 999 < i2) {
                return i2 % 200 != 0 ? ((i2 / 200) * 200) + 200 : i2;
            } else {
                if (i2 % 20 != 0) {
                    return ((i2 / 20) * 20) + 20;
                }
                return i2;
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final ArrayList<String> a(Context context, BarChart.c cVar) {
            ee7.b(context, "context");
            ee7.b(cVar, "chartModel");
            ArrayList<String> arrayList = new ArrayList<>();
            Calendar instance = Calendar.getInstance();
            for (T t : cVar.a()) {
                if (t.b() > 0) {
                    ee7.a((Object) instance, "calendar");
                    instance.setTimeInMillis(t.b());
                    Boolean w = zd5.w(instance.getTime());
                    ee7.a((Object) w, "DateHelper.isToday(calendar.time)");
                    if (w.booleanValue()) {
                        arrayList.add(ig5.a(context, 2131886602));
                    } else {
                        switch (instance.get(7)) {
                            case 1:
                                arrayList.add(ig5.a(context, 2131886719));
                                continue;
                            case 2:
                                arrayList.add(ig5.a(context, 2131886718));
                                continue;
                            case 3:
                                arrayList.add(ig5.a(context, 2131886721));
                                continue;
                            case 4:
                                arrayList.add(ig5.a(context, 2131886723));
                                continue;
                            case 5:
                                arrayList.add(ig5.a(context, 2131886722));
                                continue;
                            case 6:
                                arrayList.add(ig5.a(context, 2131886717));
                                continue;
                            case 7:
                                arrayList.add(ig5.a(context, 2131886720));
                                continue;
                        }
                    }
                } else {
                    arrayList.add("");
                }
            }
            return arrayList;
        }
    }
}
