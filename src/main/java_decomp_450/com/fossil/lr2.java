package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lr2<T> extends pr2<T> {
    @DexIgnore
    public static /* final */ lr2<Object> zza; // = new lr2<>();

    @DexIgnore
    public final boolean equals(@NullableDecl Object obj) {
        return obj == this;
    }

    @DexIgnore
    public final int hashCode() {
        return 2040732332;
    }

    @DexIgnore
    public final String toString() {
        return "Optional.absent()";
    }

    @DexIgnore
    @Override // com.fossil.pr2
    public final boolean zza() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.pr2
    public final T zzb() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }
}
