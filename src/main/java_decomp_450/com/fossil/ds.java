package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import android.util.SparseIntArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ds {
    @DexIgnore
    public /* final */ SparseIntArray a; // = new SparseIntArray();
    @DexIgnore
    public /* final */ zq b; // = new zq(0, 1, null);
    @DexIgnore
    public /* final */ rq c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ds(rq rqVar) {
        ee7.b(rqVar, "bitmapPool");
        this.c = rqVar;
    }

    @DexIgnore
    public final void a(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.a.get(identityHashCode) - 1;
        this.a.put(identityHashCode, i);
        if (cu.c.a() && cu.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "DECREMENT: [" + identityHashCode + ", " + i + ']');
        }
        if (i <= 0) {
            this.a.delete(identityHashCode);
            if (!this.b.b(identityHashCode)) {
                this.c.a(bitmap);
            }
        }
    }

    @DexIgnore
    public final void b(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        int identityHashCode = System.identityHashCode(bitmap);
        int i = this.a.get(identityHashCode) + 1;
        this.a.put(identityHashCode, i);
        if (cu.c.a() && cu.c.b() <= 2) {
            Log.println(2, "BitmapReferenceCounter", "INCREMENT: [" + identityHashCode + ", " + i + ']');
        }
    }

    @DexIgnore
    public final void c(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        this.b.a(System.identityHashCode(bitmap));
    }
}
