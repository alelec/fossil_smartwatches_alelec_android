package com.fossil;

import io.flutter.util.Predicate;
import io.flutter.view.AccessibilityBridge;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class h87 implements Predicate {
    @DexIgnore
    public static /* final */ /* synthetic */ h87 a; // = new h87();

    @DexIgnore
    private /* synthetic */ h87() {
    }

    @DexIgnore
    @Override // io.flutter.util.Predicate
    public final boolean test(Object obj) {
        return ((AccessibilityBridge.SemanticsNode) obj).hasFlag(AccessibilityBridge.Flag.HAS_IMPLICIT_SCROLLING);
    }
}
