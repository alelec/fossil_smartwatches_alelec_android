package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn5 implements Factory<gn5> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ hn5 a; // = new hn5();
    }

    @DexIgnore
    public static hn5 a() {
        return a.a;
    }

    @DexIgnore
    public static gn5 b() {
        return new gn5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public gn5 get() {
        return b();
    }
}
