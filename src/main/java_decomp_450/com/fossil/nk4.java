package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nk4 implements Factory<vg5> {
    @DexIgnore
    public /* final */ wj4 a;
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> b;
    @DexIgnore
    public /* final */ Provider<uj5> c;
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> d;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> e;
    @DexIgnore
    public /* final */ Provider<ch5> f;
    @DexIgnore
    public /* final */ Provider<kw6> g;
    @DexIgnore
    public /* final */ Provider<ek5> h;
    @DexIgnore
    public /* final */ Provider<wk5> i;

    @DexIgnore
    public nk4(wj4 wj4, Provider<HybridPresetRepository> provider, Provider<uj5> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<ch5> provider5, Provider<kw6> provider6, Provider<ek5> provider7, Provider<wk5> provider8) {
        this.a = wj4;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
        this.e = provider4;
        this.f = provider5;
        this.g = provider6;
        this.h = provider7;
        this.i = provider8;
    }

    @DexIgnore
    public static nk4 a(wj4 wj4, Provider<HybridPresetRepository> provider, Provider<uj5> provider2, Provider<QuickResponseRepository> provider3, Provider<AlarmsRepository> provider4, Provider<ch5> provider5, Provider<kw6> provider6, Provider<ek5> provider7, Provider<wk5> provider8) {
        return new nk4(wj4, provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8);
    }

    @DexIgnore
    public static vg5 a(wj4 wj4, HybridPresetRepository hybridPresetRepository, uj5 uj5, QuickResponseRepository quickResponseRepository, AlarmsRepository alarmsRepository, ch5 ch5, kw6 kw6, ek5 ek5, wk5 wk5) {
        vg5 a2 = wj4.a(hybridPresetRepository, uj5, quickResponseRepository, alarmsRepository, ch5, kw6, ek5, wk5);
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vg5 get() {
        return a(this.a, this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }
}
