package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qo2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Long e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ String g;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle h;
    @DexIgnore
    public /* final */ /* synthetic */ boolean i;
    @DexIgnore
    public /* final */ /* synthetic */ boolean j;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 p;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qo2(sn2 sn2, Long l, String str, String str2, Bundle bundle, boolean z, boolean z2) {
        super(sn2);
        this.p = sn2;
        this.e = l;
        this.f = str;
        this.g = str2;
        this.h = bundle;
        this.i = z;
        this.j = z2;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        Long l = this.e;
        this.p.h.logEvent(this.f, this.g, this.h, this.i, this.j, l == null ? ((sn2.a) this).a : l.longValue());
    }
}
