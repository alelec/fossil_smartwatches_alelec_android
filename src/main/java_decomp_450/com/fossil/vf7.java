package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vf7<R> extends sf7<R>, j87<R> {
    @DexIgnore
    boolean isExternal();

    @DexIgnore
    boolean isInfix();

    @DexIgnore
    boolean isInline();

    @DexIgnore
    boolean isOperator();

    @DexIgnore
    @Override // com.fossil.sf7
    boolean isSuspend();
}
