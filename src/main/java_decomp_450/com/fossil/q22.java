package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.j62;
import com.fossil.v02;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q22 implements m32 {
    @DexIgnore
    public /* final */ l32 a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ m02 d;
    @DexIgnore
    public i02 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Bundle i; // = new Bundle();
    @DexIgnore
    public /* final */ Set<v02.c> j; // = new HashSet();
    @DexIgnore
    public xn3 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public s62 o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ j62 r;
    @DexIgnore
    public /* final */ Map<v02<?>, Boolean> s;
    @DexIgnore
    public /* final */ v02.a<? extends xn3, fn3> t;
    @DexIgnore
    public ArrayList<Future<?>> u; // = new ArrayList<>();

    @DexIgnore
    public q22(l32 l32, j62 j62, Map<v02<?>, Boolean> map, m02 m02, v02.a<? extends xn3, fn3> aVar, Lock lock, Context context) {
        this.a = l32;
        this.r = j62;
        this.s = map;
        this.d = m02;
        this.t = aVar;
        this.b = lock;
        this.c = context;
    }

    @DexIgnore
    public static String c(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    @DexIgnore
    public final void a(tn3 tn3) {
        if (b(0)) {
            i02 e2 = tn3.e();
            if (e2.x()) {
                c72 g2 = tn3.g();
                i02 g3 = g2.g();
                if (!g3.x()) {
                    String valueOf = String.valueOf(g3);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GACConnecting", sb.toString(), new Exception());
                    b(g3);
                    return;
                }
                this.n = true;
                this.o = g2.e();
                this.p = g2.v();
                this.q = g2.w();
                e();
            } else if (a(e2)) {
                g();
                e();
            } else {
                b(e2);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b() {
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void b(Bundle bundle) {
        if (b(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void c() {
        this.a.g.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (v02<?> v02 : this.s.keySet()) {
            v02.f fVar = this.a.f.get(v02.a());
            z |= v02.c().a() == 1;
            boolean booleanValue = this.s.get(v02).booleanValue();
            if (fVar.n()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(v02.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fVar, new s22(this, v02, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.a(Integer.valueOf(System.identityHashCode(this.a.s)));
            b32 b32 = new b32(this, null);
            v02.a<? extends xn3, fn3> aVar = this.t;
            Context context = this.c;
            Looper f2 = this.a.s.f();
            j62 j62 = this.r;
            this.k = (xn3) aVar.a(context, f2, j62, j62.j(), b32, b32);
        }
        this.h = this.a.f.size();
        this.u.add(p32.a().submit(new v22(this, hashMap)));
    }

    @DexIgnore
    public final boolean d() {
        int i2 = this.h - 1;
        this.h = i2;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GACConnecting", this.a.s.q());
            Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new i02(8, null));
            return false;
        }
        i02 i02 = this.e;
        if (i02 == null) {
            return true;
        }
        this.a.r = this.f;
        b(i02);
        return false;
    }

    @DexIgnore
    public final void e() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.a.f.size();
                for (v02.c<?> cVar : this.a.f.keySet()) {
                    if (!this.a.g.containsKey(cVar)) {
                        arrayList.add(this.a.f.get(cVar));
                    } else if (d()) {
                        f();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(p32.a().submit(new w22(this, arrayList)));
                }
            }
        }
    }

    @DexIgnore
    public final void f() {
        this.a.i();
        p32.a().execute(new t22(this));
        xn3 xn3 = this.k;
        if (xn3 != null) {
            if (this.p) {
                xn3.a(this.o, this.q);
            }
            a(false);
        }
        for (v02.c<?> cVar : this.a.g.keySet()) {
            this.a.f.get(cVar).a();
        }
        this.a.t.a(this.i.isEmpty() ? null : this.i);
    }

    @DexIgnore
    public final void g() {
        this.m = false;
        this.a.s.q = Collections.emptySet();
        for (v02.c<?> cVar : this.j) {
            if (!this.a.g.containsKey(cVar)) {
                this.a.g.put(cVar, new i02(17, null));
            }
        }
    }

    @DexIgnore
    public final void h() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    @DexIgnore
    public final Set<Scope> i() {
        if (this.r == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(this.r.i());
        Map<v02<?>, j62.b> f2 = this.r.f();
        for (v02<?> v02 : f2.keySet()) {
            if (!this.a.g.containsKey(v02.a())) {
                hashSet.addAll(f2.get(v02).a);
            }
        }
        return hashSet;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t2) {
        this.a.s.i.add(t2);
        return t2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.fossil.i02 r5, com.fossil.v02<?> r6, boolean r7) {
        /*
            r4 = this;
            com.fossil.v02$e r0 = r6.c()
            int r0 = r0.a()
            r1 = 0
            r2 = 1
            if (r7 == 0) goto L_0x0024
            boolean r7 = r5.w()
            if (r7 == 0) goto L_0x0014
        L_0x0012:
            r7 = 1
            goto L_0x0022
        L_0x0014:
            com.fossil.m02 r7 = r4.d
            int r3 = r5.e()
            android.content.Intent r7 = r7.a(r3)
            if (r7 == 0) goto L_0x0021
            goto L_0x0012
        L_0x0021:
            r7 = 0
        L_0x0022:
            if (r7 == 0) goto L_0x002d
        L_0x0024:
            com.fossil.i02 r7 = r4.e
            if (r7 == 0) goto L_0x002c
            int r7 = r4.f
            if (r0 >= r7) goto L_0x002d
        L_0x002c:
            r1 = 1
        L_0x002d:
            if (r1 == 0) goto L_0x0033
            r4.e = r5
            r4.f = r0
        L_0x0033:
            com.fossil.l32 r7 = r4.a
            java.util.Map<com.fossil.v02$c<?>, com.fossil.i02> r7 = r7.g
            com.fossil.v02$c r6 = r6.a()
            r7.put(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.q22.b(com.fossil.i02, com.fossil.v02, boolean):void");
    }

    @DexIgnore
    public final void b(i02 i02) {
        h();
        a(!i02.w());
        this.a.a(i02);
        this.a.t.a(i02);
    }

    @DexIgnore
    public final boolean b(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GACConnecting", this.a.s.q());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GACConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GACConnecting", sb2.toString());
        String c2 = c(this.g);
        String c3 = c(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(c2).length() + 70 + String.valueOf(c3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(c2);
        sb3.append(" but received callback for step ");
        sb3.append(c3);
        Log.e("GACConnecting", sb3.toString(), new Exception());
        b(new i02(8, null));
        return false;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(i02 i02, v02<?> v02, boolean z) {
        if (b(1)) {
            b(i02, v02, z);
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final boolean a() {
        h();
        a(true);
        this.a.a((i02) null);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.m32
    public final void a(int i2) {
        b(new i02(8, null));
    }

    @DexIgnore
    public final boolean a(i02 i02) {
        return this.l && !i02.w();
    }

    @DexIgnore
    public final void a(boolean z) {
        xn3 xn3 = this.k;
        if (xn3 != null) {
            if (xn3.c() && z) {
                this.k.h();
            }
            this.k.a();
            if (this.r.k()) {
                this.k = null;
            }
            this.o = null;
        }
    }
}
