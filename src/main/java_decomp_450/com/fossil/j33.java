package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j33 implements tr2<i33> {
    @DexIgnore
    public static j33 b; // = new j33();
    @DexIgnore
    public /* final */ tr2<i33> a;

    @DexIgnore
    public j33(tr2<i33> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((i33) b.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ i33 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public j33() {
        this(sr2.a(new l33()));
    }
}
