package com.fossil;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm2 implements a53 {
    @DexIgnore
    @Override // com.fossil.a53
    public final c12<Status> a(a12 a12, d53 d53) {
        return a12.b(new sm2(this, a12, d53));
    }

    @DexIgnore
    @Override // com.fossil.a53
    public final c12<Status> a(a12 a12, LocationRequest locationRequest, d53 d53) {
        a72.a(Looper.myLooper(), "Calling thread must be a prepared Looper thread.");
        return a12.b(new rm2(this, a12, locationRequest, d53));
    }
}
