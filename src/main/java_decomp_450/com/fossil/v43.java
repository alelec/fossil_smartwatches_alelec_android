package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v43 implements IInterface {
    @DexIgnore
    public /* final */ IBinder a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public v43(IBinder iBinder, String str) {
        this.a = iBinder;
        this.b = str;
    }

    @DexIgnore
    public final Parcel E() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.b);
        return obtain;
    }

    @DexIgnore
    public final Parcel a(int i, Parcel parcel) throws RemoteException {
        parcel = Parcel.obtain();
        try {
            this.a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.a;
    }
}
