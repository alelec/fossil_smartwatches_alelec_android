package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa0 extends na0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<qa0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public qa0 createFromParcel(Parcel parcel) {
            return new qa0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public qa0[] newArray(int i) {
            return new qa0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public qa0 m54createFromParcel(Parcel parcel) {
            return new qa0(parcel, null);
        }
    }

    @DexIgnore
    public qa0() {
        super(pa0.WEATHER, null, null, 6);
    }

    @DexIgnore
    public qa0(zg0 zg0) {
        super(pa0.WEATHER, zg0, null, 4);
    }

    @DexIgnore
    public /* synthetic */ qa0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
