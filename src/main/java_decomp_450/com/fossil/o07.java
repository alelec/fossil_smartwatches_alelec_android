package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.NetworkInfo;
import com.fossil.c17;
import com.fossil.i17;
import com.squareup.picasso.Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o07 implements Runnable {
    @DexIgnore
    public static /* final */ AtomicInteger A; // = new AtomicInteger();
    @DexIgnore
    public static /* final */ i17 B; // = new b();
    @DexIgnore
    public static /* final */ Object y; // = new Object();
    @DexIgnore
    public static /* final */ ThreadLocal<StringBuilder> z; // = new a();
    @DexIgnore
    public /* final */ int a; // = A.incrementAndGet();
    @DexIgnore
    public /* final */ Picasso b;
    @DexIgnore
    public /* final */ u07 c;
    @DexIgnore
    public /* final */ p07 d;
    @DexIgnore
    public /* final */ k17 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ g17 g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public /* final */ i17 j;
    @DexIgnore
    public m07 p;
    @DexIgnore
    public List<m07> q;
    @DexIgnore
    public Bitmap r;
    @DexIgnore
    public Future<?> s;
    @DexIgnore
    public Picasso.LoadedFrom t;
    @DexIgnore
    public Exception u;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public Picasso.e x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ThreadLocal<StringBuilder> {
        @DexIgnore
        @Override // java.lang.ThreadLocal
        public StringBuilder initialValue() {
            return new StringBuilder("Picasso-");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends i17 {
        @DexIgnore
        @Override // com.fossil.i17
        public i17.a a(g17 g17, int i) throws IOException {
            throw new IllegalStateException("Unrecognized type of request: " + g17);
        }

        @DexIgnore
        @Override // com.fossil.i17
        public boolean a(g17 g17) {
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;
        @DexIgnore
        public /* final */ /* synthetic */ RuntimeException b;

        @DexIgnore
        public c(Transformation transformation, RuntimeException runtimeException) {
            this.a = transformation;
            this.b = runtimeException;
        }

        @DexIgnore
        public void run() {
            throw new RuntimeException("Transformation " + this.a.key() + " crashed with exception.", this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder a;

        @DexIgnore
        public d(StringBuilder sb) {
            this.a = sb;
        }

        @DexIgnore
        public void run() {
            throw new NullPointerException(this.a.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;

        @DexIgnore
        public e(Transformation transformation) {
            this.a = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " returned input Bitmap but recycled it.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Transformation a;

        @DexIgnore
        public f(Transformation transformation) {
            this.a = transformation;
        }

        @DexIgnore
        public void run() {
            throw new IllegalStateException("Transformation " + this.a.key() + " mutated input Bitmap but failed to recycle the original.");
        }
    }

    @DexIgnore
    public o07(Picasso picasso, u07 u07, p07 p07, k17 k17, m07 m07, i17 i17) {
        this.b = picasso;
        this.c = u07;
        this.d = p07;
        this.e = k17;
        this.p = m07;
        this.f = m07.c();
        this.g = m07.h();
        this.x = m07.g();
        this.h = m07.d();
        this.i = m07.e();
        this.j = i17;
        this.w = i17.a();
    }

    @DexIgnore
    public static Bitmap a(InputStream inputStream, g17 g17) throws IOException {
        y07 y07 = new y07(inputStream);
        long a2 = y07.a(65536);
        BitmapFactory.Options b2 = i17.b(g17);
        boolean a3 = i17.a(b2);
        boolean b3 = o17.b(y07);
        y07.e(a2);
        if (b3) {
            byte[] c2 = o17.c(y07);
            if (a3) {
                BitmapFactory.decodeByteArray(c2, 0, c2.length, b2);
                i17.a(g17.h, g17.i, b2, g17);
            }
            return BitmapFactory.decodeByteArray(c2, 0, c2.length, b2);
        }
        if (a3) {
            BitmapFactory.decodeStream(y07, null, b2);
            i17.a(g17.h, g17.i, b2, g17);
            y07.e(a2);
        }
        Bitmap decodeStream = BitmapFactory.decodeStream(y07, null, b2);
        if (decodeStream != null) {
            return decodeStream;
        }
        throw new IOException("Failed to decode stream.");
    }

    @DexIgnore
    public static boolean a(boolean z2, int i2, int i3, int i4, int i5) {
        return !z2 || i2 > i4 || i3 > i5;
    }

    @DexIgnore
    public void b(m07 m07) {
        boolean z2;
        if (this.p == m07) {
            this.p = null;
            z2 = true;
        } else {
            List<m07> list = this.q;
            z2 = list != null ? list.remove(m07) : false;
        }
        if (z2 && m07.g() == this.x) {
            this.x = b();
        }
        if (this.b.n) {
            o17.a("Hunter", "removed", m07.b.d(), o17.a(this, "from "));
        }
    }

    @DexIgnore
    public m07 c() {
        return this.p;
    }

    @DexIgnore
    public List<m07> d() {
        return this.q;
    }

    @DexIgnore
    public g17 e() {
        return this.g;
    }

    @DexIgnore
    public Exception f() {
        return this.u;
    }

    @DexIgnore
    public String g() {
        return this.f;
    }

    @DexIgnore
    public Picasso.LoadedFrom h() {
        return this.t;
    }

    @DexIgnore
    public int i() {
        return this.h;
    }

    @DexIgnore
    public Picasso j() {
        return this.b;
    }

    @DexIgnore
    public Picasso.e k() {
        return this.x;
    }

    @DexIgnore
    public Bitmap l() {
        return this.r;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public Bitmap m() throws IOException {
        Bitmap bitmap;
        if (a17.shouldReadFromMemoryCache(this.h)) {
            bitmap = this.d.a(this.f);
            if (bitmap != null) {
                this.e.b();
                this.t = Picasso.LoadedFrom.MEMORY;
                if (this.b.n) {
                    o17.a("Hunter", "decoded", this.g.d(), "from cache");
                }
                return bitmap;
            }
        } else {
            bitmap = null;
        }
        this.g.c = this.w == 0 ? b17.OFFLINE.index : this.i;
        i17.a a2 = this.j.a(this.g, this.i);
        if (a2 != null) {
            this.t = a2.c();
            this.v = a2.b();
            bitmap = a2.a();
            if (bitmap == null) {
                InputStream d2 = a2.d();
                try {
                    Bitmap a3 = a(d2, this.g);
                    o17.a(d2);
                    bitmap = a3;
                } catch (Throwable th) {
                    o17.a(d2);
                    throw th;
                }
            }
        }
        if (bitmap != null) {
            if (this.b.n) {
                o17.a("Hunter", "decoded", this.g.d());
            }
            this.e.a(bitmap);
            if (this.g.f() || this.v != 0) {
                synchronized (y) {
                    if (this.g.e() || this.v != 0) {
                        bitmap = a(this.g, bitmap, this.v);
                        if (this.b.n) {
                            o17.a("Hunter", "transformed", this.g.d());
                        }
                    }
                    if (this.g.b()) {
                        bitmap = a(this.g.g, bitmap);
                        if (this.b.n) {
                            o17.a("Hunter", "transformed", this.g.d(), "from custom transformations");
                        }
                    }
                }
                if (bitmap != null) {
                    this.e.b(bitmap);
                }
            }
        }
        return bitmap;
    }

    @DexIgnore
    public boolean n() {
        Future<?> future = this.s;
        return future != null && future.isCancelled();
    }

    @DexIgnore
    public boolean o() {
        return this.j.b();
    }

    @DexIgnore
    public void run() {
        try {
            a(this.g);
            if (this.b.n) {
                o17.a("Hunter", "executing", o17.a(this));
            }
            Bitmap m = m();
            this.r = m;
            if (m == null) {
                this.c.c(this);
            } else {
                this.c.b(this);
            }
        } catch (Downloader.a e2) {
            if (!e2.localCacheOnly || e2.responseCode != 504) {
                this.u = e2;
            }
            this.c.c(this);
        } catch (c17.a e3) {
            this.u = e3;
            this.c.d(this);
        } catch (IOException e4) {
            this.u = e4;
            this.c.d(this);
        } catch (OutOfMemoryError e5) {
            StringWriter stringWriter = new StringWriter();
            this.e.a().a(new PrintWriter(stringWriter));
            this.u = new RuntimeException(stringWriter.toString(), e5);
            this.c.c(this);
        } catch (Exception e6) {
            this.u = e6;
            this.c.c(this);
        } catch (Throwable th) {
            Thread.currentThread().setName("Picasso-Idle");
            throw th;
        }
        Thread.currentThread().setName("Picasso-Idle");
    }

    @DexIgnore
    public final Picasso.e b() {
        Picasso.e eVar = Picasso.e.LOW;
        List<m07> list = this.q;
        boolean z2 = true;
        boolean z3 = list != null && !list.isEmpty();
        if (this.p == null && !z3) {
            z2 = false;
        }
        if (!z2) {
            return eVar;
        }
        m07 m07 = this.p;
        if (m07 != null) {
            eVar = m07.g();
        }
        if (z3) {
            int size = this.q.size();
            for (int i2 = 0; i2 < size; i2++) {
                Picasso.e g2 = this.q.get(i2).g();
                if (g2.ordinal() > eVar.ordinal()) {
                    eVar = g2;
                }
            }
        }
        return eVar;
    }

    @DexIgnore
    public void a(m07 m07) {
        boolean z2 = this.b.n;
        g17 g17 = m07.b;
        if (this.p == null) {
            this.p = m07;
            if (z2) {
                List<m07> list = this.q;
                if (list == null || list.isEmpty()) {
                    o17.a("Hunter", "joined", g17.d(), "to empty hunter");
                } else {
                    o17.a("Hunter", "joined", g17.d(), o17.a(this, "to "));
                }
            }
        } else {
            if (this.q == null) {
                this.q = new ArrayList(3);
            }
            this.q.add(m07);
            if (z2) {
                o17.a("Hunter", "joined", g17.d(), o17.a(this, "to "));
            }
            Picasso.e g2 = m07.g();
            if (g2.ordinal() > this.x.ordinal()) {
                this.x = g2;
            }
        }
    }

    @DexIgnore
    public boolean a() {
        Future<?> future;
        if (this.p != null) {
            return false;
        }
        List<m07> list = this.q;
        if ((list == null || list.isEmpty()) && (future = this.s) != null && future.cancel(false)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public boolean a(boolean z2, NetworkInfo networkInfo) {
        if (!(this.w > 0)) {
            return false;
        }
        this.w--;
        return this.j.a(z2, networkInfo);
    }

    @DexIgnore
    public static void a(g17 g17) {
        String a2 = g17.a();
        StringBuilder sb = z.get();
        sb.ensureCapacity(a2.length() + 8);
        sb.replace(8, sb.length(), a2);
        Thread.currentThread().setName(sb.toString());
    }

    @DexIgnore
    public static o07 a(Picasso picasso, u07 u07, p07 p07, k17 k17, m07 m07) {
        g17 h2 = m07.h();
        List<i17> a2 = picasso.a();
        int size = a2.size();
        for (int i2 = 0; i2 < size; i2++) {
            i17 i17 = a2.get(i2);
            if (i17.a(h2)) {
                return new o07(picasso, u07, p07, k17, m07, i17);
            }
        }
        return new o07(picasso, u07, p07, k17, m07, B);
    }

    @DexIgnore
    public static Bitmap a(List<Transformation> list, Bitmap bitmap) {
        int size = list.size();
        int i2 = 0;
        while (i2 < size) {
            Transformation transformation = list.get(i2);
            try {
                Bitmap transform = transformation.transform(bitmap);
                if (transform == null) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Transformation ");
                    sb.append(transformation.key());
                    sb.append(" returned null after ");
                    sb.append(i2);
                    sb.append(" previous transformation(s).\n\nTransformation list:\n");
                    for (Transformation transformation2 : list) {
                        sb.append(transformation2.key());
                        sb.append('\n');
                    }
                    Picasso.p.post(new d(sb));
                    return null;
                } else if (transform == bitmap && bitmap.isRecycled()) {
                    Picasso.p.post(new e(transformation));
                    return null;
                } else if (transform == bitmap || bitmap.isRecycled()) {
                    i2++;
                    bitmap = transform;
                } else {
                    Picasso.p.post(new f(transformation));
                    return null;
                }
            } catch (RuntimeException e2) {
                Picasso.p.post(new c(transformation, e2));
                return null;
            }
        }
        return bitmap;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap a(com.fossil.g17 r13, android.graphics.Bitmap r14, int r15) {
        /*
            int r0 = r14.getWidth()
            int r1 = r14.getHeight()
            boolean r2 = r13.l
            android.graphics.Matrix r8 = new android.graphics.Matrix
            r8.<init>()
            boolean r3 = r13.e()
            r4 = 0
            if (r3 == 0) goto L_0x00af
            int r3 = r13.h
            int r5 = r13.i
            float r6 = r13.m
            r7 = 0
            int r7 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r7 == 0) goto L_0x0030
            boolean r7 = r13.p
            if (r7 == 0) goto L_0x002d
            float r7 = r13.n
            float r9 = r13.o
            r8.setRotate(r6, r7, r9)
            goto L_0x0030
        L_0x002d:
            r8.setRotate(r6)
        L_0x0030:
            boolean r6 = r13.j
            if (r6 == 0) goto L_0x0074
            float r13 = (float) r3
            float r6 = (float) r0
            float r7 = r13 / r6
            float r9 = (float) r5
            float r10 = (float) r1
            float r11 = r9 / r10
            int r12 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r12 <= 0) goto L_0x0053
            float r11 = r11 / r7
            float r10 = r10 * r11
            double r10 = (double) r10
            double r10 = java.lang.Math.ceil(r10)
            int r13 = (int) r10
            int r6 = r1 - r13
            int r6 = r6 / 2
            float r10 = (float) r13
            float r11 = r9 / r10
            r9 = r7
            r7 = r0
            goto L_0x0067
        L_0x0053:
            float r7 = r7 / r11
            float r6 = r6 * r7
            double r6 = (double) r6
            double r6 = java.lang.Math.ceil(r6)
            int r6 = (int) r6
            int r7 = r0 - r6
            int r7 = r7 / 2
            float r9 = (float) r6
            float r13 = r13 / r9
            r9 = r13
            r13 = r1
            r4 = r7
            r7 = r6
            r6 = 0
        L_0x0067:
            boolean r0 = a(r2, r0, r1, r3, r5)
            if (r0 == 0) goto L_0x0070
            r8.preScale(r9, r11)
        L_0x0070:
            r5 = r6
            r6 = r7
            r7 = r13
            goto L_0x00b2
        L_0x0074:
            boolean r13 = r13.k
            if (r13 == 0) goto L_0x008e
            float r13 = (float) r3
            float r6 = (float) r0
            float r13 = r13 / r6
            float r6 = (float) r5
            float r7 = (float) r1
            float r6 = r6 / r7
            int r7 = (r13 > r6 ? 1 : (r13 == r6 ? 0 : -1))
            if (r7 >= 0) goto L_0x0083
            goto L_0x0084
        L_0x0083:
            r13 = r6
        L_0x0084:
            boolean r2 = a(r2, r0, r1, r3, r5)
            if (r2 == 0) goto L_0x00af
            r8.preScale(r13, r13)
            goto L_0x00af
        L_0x008e:
            if (r3 != 0) goto L_0x0092
            if (r5 == 0) goto L_0x00af
        L_0x0092:
            if (r3 != r0) goto L_0x0096
            if (r5 == r1) goto L_0x00af
        L_0x0096:
            if (r3 == 0) goto L_0x009b
            float r13 = (float) r3
            float r6 = (float) r0
            goto L_0x009d
        L_0x009b:
            float r13 = (float) r5
            float r6 = (float) r1
        L_0x009d:
            float r13 = r13 / r6
            if (r5 == 0) goto L_0x00a3
            float r6 = (float) r5
            float r7 = (float) r1
            goto L_0x00a5
        L_0x00a3:
            float r6 = (float) r3
            float r7 = (float) r0
        L_0x00a5:
            float r6 = r6 / r7
            boolean r2 = a(r2, r0, r1, r3, r5)
            if (r2 == 0) goto L_0x00af
            r8.preScale(r13, r6)
        L_0x00af:
            r6 = r0
            r7 = r1
            r5 = 0
        L_0x00b2:
            if (r15 == 0) goto L_0x00b8
            float r13 = (float) r15
            r8.preRotate(r13)
        L_0x00b8:
            r9 = 1
            r3 = r14
            android.graphics.Bitmap r13 = android.graphics.Bitmap.createBitmap(r3, r4, r5, r6, r7, r8, r9)
            if (r13 == r14) goto L_0x00c4
            r14.recycle()
            r14 = r13
        L_0x00c4:
            return r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.o07.a(com.fossil.g17, android.graphics.Bitmap, int):android.graphics.Bitmap");
    }
}
