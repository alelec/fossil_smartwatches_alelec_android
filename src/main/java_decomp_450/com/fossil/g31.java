package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g31 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ he0 a;
    @DexIgnore
    public /* final */ /* synthetic */ km1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public g31(he0 he0, km1 km1, zk0 zk0) {
        super(1);
        this.a = he0;
        this.b = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        zk0 zk02 = zk0;
        if (zk02.v.b == is0.SUCCESS) {
            Object d = zk02.d();
            if (d instanceof String) {
                this.b.b.post(new dy0(this, d));
            } else {
                this.b.b.post(new uz0(this));
            }
        } else {
            this.b.b.post(new o11(this, zk02));
            this.b.a(zk02.v);
        }
        return i97.a;
    }
}
