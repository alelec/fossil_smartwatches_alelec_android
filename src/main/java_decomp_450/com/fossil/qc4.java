package com.fossil;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.fossil.o6;
import com.fossil.oc4;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qc4 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public /* final */ dd4 c;

    @DexIgnore
    public qc4(Context context, dd4 dd4, Executor executor) {
        this.a = executor;
        this.b = context;
        this.c = dd4;
    }

    @DexIgnore
    public boolean a() {
        if (this.c.a("gcm.n.noui")) {
            return true;
        }
        if (b()) {
            return false;
        }
        bd4 c2 = c();
        oc4.a b2 = oc4.b(this.b, this.c);
        a(b2.a, c2);
        a(b2);
        return true;
    }

    @DexIgnore
    public final boolean b() {
        if (((KeyguardManager) this.b.getSystemService("keyguard")).inKeyguardRestrictedInputMode()) {
            return false;
        }
        if (!v92.h()) {
            SystemClock.sleep(10);
        }
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) this.b.getSystemService(Constants.ACTIVITY)).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            Iterator<ActivityManager.RunningAppProcessInfo> it = runningAppProcesses.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    if (next.importance == 100) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final bd4 c() {
        bd4 b2 = bd4.b(this.c.g("gcm.n.image"));
        if (b2 != null) {
            b2.a(this.a);
        }
        return b2;
    }

    @DexIgnore
    public final void a(o6.e eVar, bd4 bd4) {
        if (bd4 != null) {
            try {
                Bitmap bitmap = (Bitmap) qo3.a(bd4.c(), 5, TimeUnit.SECONDS);
                eVar.b(bitmap);
                o6.b bVar = new o6.b();
                bVar.b(bitmap);
                bVar.a((Bitmap) null);
                eVar.a(bVar);
            } catch (ExecutionException e) {
                String valueOf = String.valueOf(e.getCause());
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 26);
                sb.append("Failed to download image: ");
                sb.append(valueOf);
                Log.w("FirebaseMessaging", sb.toString());
            } catch (InterruptedException unused) {
                Log.w("FirebaseMessaging", "Interrupted while downloading image, showing notification without it");
                bd4.close();
                Thread.currentThread().interrupt();
            } catch (TimeoutException unused2) {
                Log.w("FirebaseMessaging", "Failed to download image in time, showing notification without it");
                bd4.close();
            }
        }
    }

    @DexIgnore
    public final void a(oc4.a aVar) {
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            Log.d("FirebaseMessaging", "Showing notification");
        }
        ((NotificationManager) this.b.getSystemService("notification")).notify(aVar.b, aVar.c, aVar.a.a());
    }
}
