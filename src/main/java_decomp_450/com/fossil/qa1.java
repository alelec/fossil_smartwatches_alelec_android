package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class qa1 extends Enum<qa1> {
    @DexIgnore
    public static /* final */ qa1 A;
    @DexIgnore
    public static /* final */ qa1 B;
    @DexIgnore
    public static /* final */ qa1 C;
    @DexIgnore
    public static /* final */ qa1 D;
    @DexIgnore
    public static /* final */ qa1 E;
    @DexIgnore
    public static /* final */ qa1 F;
    @DexIgnore
    public static /* final */ qa1 G;
    @DexIgnore
    public static /* final */ qa1 H;
    @DexIgnore
    public static /* final */ qa1 I;
    @DexIgnore
    public static /* final */ qa1 J;
    @DexIgnore
    public static /* final */ qa1 K;
    @DexIgnore
    public static /* final */ qa1 L;
    @DexIgnore
    public static /* final */ qa1 M;
    @DexIgnore
    public static /* final */ qa1 N;
    @DexIgnore
    public static /* final */ qa1 O;
    @DexIgnore
    public static /* final */ qa1 P;
    @DexIgnore
    public static /* final */ qa1 Q;
    @DexIgnore
    public static /* final */ qa1 R;
    @DexIgnore
    public static /* final */ qa1 S;
    @DexIgnore
    public static /* final */ qa1 T;
    @DexIgnore
    public static /* final */ qa1 U;
    @DexIgnore
    public static /* final */ qa1 V;
    @DexIgnore
    public static /* final */ qa1 W;
    @DexIgnore
    public static /* final */ qa1 X;
    @DexIgnore
    public static /* final */ qa1 Y;
    @DexIgnore
    public static /* final */ qa1 Z;
    @DexIgnore
    public static /* final */ qa1 a;
    @DexIgnore
    public static /* final */ qa1 a0;
    @DexIgnore
    public static /* final */ qa1 b;
    @DexIgnore
    public static /* final */ qa1 b0;
    @DexIgnore
    public static /* final */ qa1 c;
    @DexIgnore
    public static /* final */ qa1 c0;
    @DexIgnore
    public static /* final */ qa1 d;
    @DexIgnore
    public static /* final */ qa1 d0;
    @DexIgnore
    public static /* final */ qa1 e;
    @DexIgnore
    public static /* final */ qa1 e0;
    @DexIgnore
    public static /* final */ qa1 f;
    @DexIgnore
    public static /* final */ qa1 f0;
    @DexIgnore
    public static /* final */ qa1 g;
    @DexIgnore
    public static /* final */ /* synthetic */ qa1[] g0;
    @DexIgnore
    public static /* final */ qa1 h;
    @DexIgnore
    public static /* final */ qa1 i;
    @DexIgnore
    public static /* final */ qa1 j;
    @DexIgnore
    public static /* final */ qa1 k;
    @DexIgnore
    public static /* final */ qa1 l;
    @DexIgnore
    public static /* final */ qa1 m;
    @DexIgnore
    public static /* final */ qa1 n;
    @DexIgnore
    public static /* final */ qa1 o;
    @DexIgnore
    public static /* final */ qa1 p;
    @DexIgnore
    public static /* final */ qa1 q;
    @DexIgnore
    public static /* final */ qa1 r;
    @DexIgnore
    public static /* final */ qa1 s;
    @DexIgnore
    public static /* final */ qa1 t;
    @DexIgnore
    public static /* final */ qa1 u;
    @DexIgnore
    public static /* final */ qa1 v;
    @DexIgnore
    public static /* final */ qa1 w;
    @DexIgnore
    public static /* final */ qa1 x;
    @DexIgnore
    public static /* final */ qa1 y;
    @DexIgnore
    public static /* final */ qa1 z;

    /*
    static {
        qa1 qa1 = new qa1("UNKNOWN", 0);
        a = qa1;
        qa1 qa12 = new qa1("CONNECT", 1);
        b = qa12;
        qa1 qa13 = new qa1("DISCONNECT", 2);
        c = qa13;
        qa1 qa14 = new qa1("DISCOVER_SERVICES", 4);
        d = qa14;
        qa1 qa15 = new qa1("SUBSCRIBE_CHARACTERISTIC", 5);
        e = qa15;
        qa1 qa16 = new qa1("REQUEST_MTU", 6);
        f = qa16;
        qa1 qa17 = new qa1("GET_OPTIMAL_PAYLOAD", 7);
        g = qa17;
        qa1 qa18 = new qa1("READ_RSSI", 8);
        h = qa18;
        qa1 qa19 = new qa1("READ_FIRMWARE_VERSION", 10);
        i = qa19;
        qa1 qa110 = new qa1("READ_MODEL_NUMBER", 11);
        j = qa110;
        qa1 qa111 = new qa1("STREAMING", 12);
        k = qa111;
        qa1 qa112 = new qa1("PUT_FILE", 13);
        l = qa112;
        qa1 qa113 = new qa1("VERIFY_FILE", 14);
        m = qa113;
        qa1 qa114 = new qa1("GET_FILE_SIZE_WRITTEN", 15);
        n = qa114;
        qa1 qa115 = new qa1("VERIFY_DATA", 16);
        o = qa115;
        qa1 qa116 = new qa1("ABORT_FILE", 17);
        p = qa116;
        qa1 qa117 = new qa1("TRANSFER_DATA", 18);
        q = qa117;
        qa1 qa118 = new qa1("GET_CONNECTION_PARAMS", 19);
        r = qa118;
        qa1 qa119 = new qa1("SET_CONNECTION_PARAMS", 20);
        s = qa119;
        qa1 qa120 = new qa1("PLAY_ANIMATION", 21);
        t = qa120;
        qa1 qa121 = new qa1("LIST_FILE", 22);
        u = qa121;
        qa1 qa122 = new qa1("GET_FILE", 23);
        v = qa122;
        qa1 qa123 = new qa1("ERASE_FILE", 24);
        w = qa123;
        qa1 qa124 = new qa1("REQUEST_HANDS", 25);
        x = qa124;
        qa1 qa125 = new qa1("RELEASE_HANDS", 26);
        y = qa125;
        qa1 qa126 = new qa1("MOVE_HANDS", 27);
        z = qa126;
        qa1 qa127 = new qa1("SET_CALIBRATION_POSITION", 28);
        A = qa127;
        qa1 qa128 = new qa1("NOTIFY_MUSIC_EVENT", 29);
        B = qa128;
        qa1 qa129 = new qa1("GET_CURRENT_WORKOUT_SESSION", 30);
        C = qa129;
        qa1 qa130 = new qa1("STOP_CURRENT_WORKOUT_SESSION", 31);
        D = qa130;
        qa1 qa131 = new qa1("SET_HEARTBEAT_INTERVAL", 34);
        E = qa131;
        qa1 qa132 = new qa1("SEND_ASYNC_EVENT_ACK", 35);
        F = qa132;
        qa1 qa133 = new qa1("SEND_PHONE_RANDOM_NUMBER", 36);
        G = qa133;
        qa1 qa134 = new qa1("SEND_BOTH_SIDES_RANDOM_NUMBERS", 37);
        H = qa134;
        qa1 qa135 = new qa1("EXCHANGE_PUBLIC_KEYS", 38);
        I = qa135;
        qa1 qa136 = new qa1("CONNECT_HID", 39);
        J = qa136;
        qa1 qa137 = new qa1("DISCONNECT_HID", 40);
        K = qa137;
        qa1 qa138 = new qa1("LEGACY_OTA_ENTER", 42);
        L = qa138;
        qa1 qa139 = new qa1("LEGACY_GET_FILE_SIZE_WRITTEN", 43);
        M = qa139;
        qa1 qa140 = new qa1("LEGACY_PUT_FILE", 44);
        N = qa140;
        qa1 qa141 = new qa1("LEGACY_TRANSFER_DATA", 45);
        O = qa141;
        qa1 qa142 = new qa1("LEGACY_VERIFY_FILE", 46);
        P = qa142;
        qa1 qa143 = new qa1("LEGACY_VERIFY_SEGMENT", 47);
        Q = qa143;
        qa1 qa144 = new qa1("LEGACY_OTA_RESET", 48);
        R = qa144;
        qa1 qa145 = new qa1("LEGACY_ERASE_SEGMENT", 49);
        S = qa145;
        qa1 qa146 = new qa1("LEGACY_ABORT_FILE", 50);
        T = qa146;
        qa1 qa147 = new qa1("LEGACY_LIST_FILE", 51);
        U = qa147;
        qa1 qa148 = new qa1("LEGACY_GET_ACTIVITY_FILE", 52);
        V = qa148;
        qa1 qa149 = new qa1("LEGACY_CLOSE_CURRENT_ACTIVITY_FILE", 53);
        W = qa149;
        qa1 qa150 = new qa1("LEGACY_ERASE_ACTIVITY_FILE", 54);
        X = qa150;
        qa1 qa151 = new qa1("CREATE_BOND", 55);
        Y = qa151;
        qa1 qa152 = new qa1("REMOVE_BOND", 56);
        Z = qa152;
        qa1 qa153 = new qa1("CUSTOM_COMMAND", 57);
        a0 = qa153;
        qa1 qa154 = new qa1("NOTIFY_APP_NOTIFICATION_EVENT", 58);
        b0 = qa154;
        qa1 qa155 = new qa1("READ_SOFTWARE_REVISION", 59);
        c0 = qa155;
        qa1 qa156 = new qa1("CLEAN_UP_DEVICE", 62);
        d0 = qa156;
        qa1 qa157 = new qa1("CONFIRM_AUTHORIZATION", 63);
        e0 = qa157;
        qa1 qa158 = new qa1("STOP_AUTHORIZATION_PROCESS", 64);
        f0 = qa158;
        g0 = new qa1[]{qa1, qa12, qa13, new qa1("CLOSE", 3), qa14, qa15, qa16, qa17, qa18, new qa1("READ_SERIAL_NUMBER", 9), qa19, qa110, qa111, qa112, qa113, qa114, qa115, qa116, qa117, qa118, qa119, qa120, qa121, qa122, qa123, qa124, qa125, qa126, qa127, qa128, qa129, qa130, new qa1("GET_HEARTBEAT_STATISTIC", 32), new qa1("GET_HEARTBEAT_INTERVAL", 33), qa131, qa132, qa133, qa134, qa135, qa136, qa137, new qa1("TROUBLESHOOT_DEVICE_BLE", 41), qa138, qa139, qa140, qa141, qa142, qa143, qa144, qa145, qa146, qa147, qa148, qa149, qa150, qa151, qa152, qa153, qa154, qa155, new qa1("CUSTOM_COMMAND_WITH_RESPONSE", 60), new qa1("REQUEST_DISCOVER_SERVICE", 61), qa156, qa157, qa158};
    }
    */

    @DexIgnore
    public qa1(String str, int i2) {
    }

    @DexIgnore
    public static qa1 valueOf(String str) {
        return (qa1) Enum.valueOf(qa1.class, str);
    }

    @DexIgnore
    public static qa1[] values() {
        return (qa1[]) g0.clone();
    }
}
