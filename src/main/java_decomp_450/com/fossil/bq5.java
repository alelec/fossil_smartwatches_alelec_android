package com.fossil;

import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.os.Handler;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.aq5;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bq5<VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> implements Filterable, aq5.a {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public boolean a;
    @DexIgnore
    public Cursor b;
    @DexIgnore
    public bq5<VH>.a c;
    @DexIgnore
    public DataSetObserver d;
    @DexIgnore
    public aq5 e;
    @DexIgnore
    public FilterQueryProvider f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            bq5.this.c();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends DataSetObserver {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c() {
        }

        @DexIgnore
        public void onChanged() {
            bq5.this.a = true;
            bq5.this.notifyDataSetChanged();
        }

        @DexIgnore
        public void onInvalidated() {
            bq5.this.a = false;
            bq5 bq5 = bq5.this;
            bq5.notifyItemRangeRemoved(0, bq5.getItemCount());
        }
    }

    /*
    static {
        new b(null);
        String simpleName = bq5.class.getSimpleName();
        ee7.a((Object) simpleName, "CursorRecyclerViewAdapter::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public bq5(Cursor cursor) {
        boolean z = cursor != null;
        this.b = cursor;
        this.a = z;
        this.c = new a();
        this.d = new c();
        if (z) {
            bq5<VH>.a aVar = this.c;
            if (aVar != null) {
                if (cursor != null) {
                    cursor.registerContentObserver(aVar);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            DataSetObserver dataSetObserver = this.d;
            if (dataSetObserver == null) {
                return;
            }
            if (cursor != null) {
                cursor.registerDataSetObserver(dataSetObserver);
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public abstract void a(VH vh, Cursor cursor, int i);

    @DexIgnore
    @Override // com.fossil.aq5.a
    public CharSequence b(Cursor cursor) {
        String obj;
        return (cursor == null || (obj = cursor.toString()) == null) ? "" : obj;
    }

    @DexIgnore
    public Cursor c(Cursor cursor) {
        if (ee7.a(cursor, this.b)) {
            return null;
        }
        Cursor cursor2 = this.b;
        if (cursor2 != null) {
            bq5<VH>.a aVar = this.c;
            if (aVar != null) {
                cursor2.unregisterContentObserver(aVar);
            }
            DataSetObserver dataSetObserver = this.d;
            if (dataSetObserver != null) {
                cursor2.unregisterDataSetObserver(dataSetObserver);
            }
        }
        this.b = cursor;
        if (cursor != null) {
            bq5<VH>.a aVar2 = this.c;
            if (aVar2 != null) {
                cursor.registerContentObserver(aVar2);
            }
            DataSetObserver dataSetObserver2 = this.d;
            if (dataSetObserver2 != null) {
                cursor.registerDataSetObserver(dataSetObserver2);
            }
            this.a = true;
            notifyDataSetChanged();
        } else {
            this.a = false;
            notifyItemRangeRemoved(0, getItemCount());
        }
        return cursor2;
    }

    @DexIgnore
    public Filter getFilter() {
        if (this.e == null) {
            this.e = new aq5(this);
        }
        aq5 aq5 = this.e;
        if (aq5 != null) {
            return aq5;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Cursor cursor;
        if (!this.a || (cursor = this.b) == null) {
            return 0;
        }
        if (cursor != null) {
            return cursor.getCount();
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(VH vh, int i) {
        ee7.b(vh, "holder");
        if (!this.a) {
            FLogger.INSTANCE.getLocal().d(g, ".Inside onBindViewHolder the cursor is invalid");
        }
        a(vh, this.b, i);
    }

    @DexIgnore
    @Override // com.fossil.aq5.a
    public Cursor a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.aq5.a
    public void a(Cursor cursor) {
        ee7.b(cursor, "cursor");
        Cursor c2 = c(cursor);
        if (c2 != null) {
            c2.close();
        }
    }

    @DexIgnore
    @Override // com.fossil.aq5.a
    public Cursor a(CharSequence charSequence) {
        ee7.b(charSequence, "constraint");
        FilterQueryProvider filterQueryProvider = this.f;
        if (filterQueryProvider == null) {
            return this.b;
        }
        if (filterQueryProvider != null) {
            return filterQueryProvider.runQuery(charSequence);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(FilterQueryProvider filterQueryProvider) {
        ee7.b(filterQueryProvider, "filterQueryProvider");
        this.f = filterQueryProvider;
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d(g, ".Inside onContentChanged");
    }
}
