package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class af0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ r60 c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<af0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public af0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(wb0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<Co\u2026class.java.classLoader)!!");
                wb0 wb0 = (wb0) readParcelable;
                df0 df0 = (df0) parcel.readParcelable(df0.class.getClassLoader());
                Parcelable readParcelable2 = parcel.readParcelable(r60.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new af0(wb0, df0, (r60) readParcelable2, parcel.readInt(), parcel.readInt() == 1);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public af0[] newArray(int i) {
            return new af0[i];
        }
    }

    @DexIgnore
    public af0(wb0 wb0, r60 r60, int i) {
        super(wb0, null);
        this.d = true;
        this.e = i;
        this.c = r60;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        try {
            qs1 qs1 = qs1.d;
            yb0 deviceRequest = getDeviceRequest();
            if (deviceRequest != null) {
                return qs1.a(s, r60, new jt0(((ac0) deviceRequest).d(), new r60(this.c.getMajor(), this.c.getMinor()), this.d, this.e).getData());
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.MicroAppRequest");
        } catch (f41 e2) {
            wl0.h.a(e2);
            return new byte[0];
        }
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        return yz0.a(yz0.a(yz0.a(super.b(), r51.r3, this.c.toString()), r51.u3, Boolean.valueOf(this.d)), r51.v3, Integer.valueOf(this.e));
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(af0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            af0 af0 = (af0) obj;
            return !(ee7.a(this.c, af0.c) ^ true) && this.d == af0.d && this.e == af0.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeTravelMicroAppData");
    }

    @DexIgnore
    public final r60 getMicroAppVersion() {
        return this.c;
    }

    @DexIgnore
    public final boolean getShipHandsToTwelve() {
        return this.d;
    }

    @DexIgnore
    public final int getTravelTimeInMinute() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = Boolean.valueOf(this.d).hashCode();
        return Integer.valueOf(this.e).hashCode() + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(this.d ? 1 : 0);
        }
    }

    @DexIgnore
    public af0(wb0 wb0, df0 df0, r60 r60, int i, boolean z) {
        super(wb0, df0);
        this.d = z;
        this.e = i;
        this.c = r60;
    }
}
