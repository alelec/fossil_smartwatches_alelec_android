package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class at7 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ bt7 b;
    @DexIgnore
    public /* final */ it7 c;

    @DexIgnore
    public at7(String str, it7 it7) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (it7 != null) {
            this.a = str;
            this.c = it7;
            this.b = new bt7();
            a(it7);
            b(it7);
            c(it7);
        } else {
            throw new IllegalArgumentException("Body may not be null");
        }
    }

    @DexIgnore
    public it7 a() {
        return this.c;
    }

    @DexIgnore
    public bt7 b() {
        return this.b;
    }

    @DexIgnore
    public String c() {
        return this.a;
    }

    @DexIgnore
    public void a(String str, String str2) {
        if (str != null) {
            this.b.a(new ft7(str, str2));
            return;
        }
        throw new IllegalArgumentException("Field name may not be null");
    }

    @DexIgnore
    public void b(it7 it7) {
        StringBuilder sb = new StringBuilder();
        sb.append(it7.c());
        if (it7.b() != null) {
            sb.append("; charset=");
            sb.append(it7.b());
        }
        a("Content-Type", sb.toString());
    }

    @DexIgnore
    public void c(it7 it7) {
        a("Content-Transfer-Encoding", it7.a());
    }

    @DexIgnore
    public void a(it7 it7) {
        StringBuilder sb = new StringBuilder();
        sb.append("form-data; name=\"");
        sb.append(c());
        sb.append("\"");
        if (it7.d() != null) {
            sb.append("; filename=\"");
            sb.append(it7.d());
            sb.append("\"");
        }
        a("Content-Disposition", sb.toString());
    }
}
