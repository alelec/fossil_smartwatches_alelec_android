package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class na7 extends ma7 {
    @DexIgnore
    public static final int a(int i) {
        if (i < 0) {
            return i;
        }
        if (i < 3) {
            return i + 1;
        }
        if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        }
        return Integer.MAX_VALUE;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> a(r87<? extends K, ? extends V> r87) {
        ee7.b(r87, "pair");
        Map<K, V> singletonMap = Collections.singletonMap(r87.getFirst(), r87.getSecond());
        ee7.a((Object) singletonMap, "java.util.Collections.si\u2026(pair.first, pair.second)");
        return singletonMap;
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> b(Map<? extends K, ? extends V> map) {
        ee7.b(map, "$this$toSortedMap");
        return new TreeMap(map);
    }

    @DexIgnore
    public static final <K extends Comparable<? super K>, V> SortedMap<K, V> a(r87<? extends K, ? extends V>... r87Arr) {
        ee7.b(r87Arr, "pairs");
        TreeMap treeMap = new TreeMap();
        oa7.a((Map) treeMap, (r87[]) r87Arr);
        return treeMap;
    }

    @DexIgnore
    public static final <K, V> Map<K, V> a(Map<? extends K, ? extends V> map) {
        ee7.b(map, "$this$toSingletonMap");
        Map.Entry<? extends K, ? extends V> next = map.entrySet().iterator().next();
        Map<K, V> singletonMap = Collections.singletonMap(next.getKey(), next.getValue());
        ee7.a((Object) singletonMap, "java.util.Collections.singletonMap(key, value)");
        ee7.a((Object) singletonMap, "with(entries.iterator().\u2026ingletonMap(key, value) }");
        return singletonMap;
    }
}
