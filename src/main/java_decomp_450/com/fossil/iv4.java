package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv4 {
    @DexIgnore
    public final ArrayList<String> a(String str) {
        ee7.b(str, "arrayValue");
        ArrayList<String> arrayList = new ArrayList<>();
        if (TextUtils.isEmpty(str)) {
            return arrayList;
        }
        JSONArray jSONArray = new JSONArray(str);
        int i = 0;
        int length = jSONArray.length();
        while (i < length) {
            Object obj = jSONArray.get(i);
            if (obj != null) {
                arrayList.add((String) obj);
                i++;
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.String");
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final String a(ArrayList<String> arrayList) {
        if (arrayList == null) {
            return "";
        }
        String jSONArray = new JSONArray((Collection) arrayList).toString();
        ee7.a((Object) jSONArray, "JSONArray(value).toString()");
        return jSONArray;
    }
}
