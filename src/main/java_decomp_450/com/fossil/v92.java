package com.fossil;

import android.os.Build;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v92 {
    @DexIgnore
    public static Boolean a;

    @DexIgnore
    public static boolean a() {
        return true;
    }

    @DexIgnore
    public static boolean b() {
        return Build.VERSION.SDK_INT >= 15;
    }

    @DexIgnore
    public static boolean c() {
        return Build.VERSION.SDK_INT >= 16;
    }

    @DexIgnore
    public static boolean d() {
        return Build.VERSION.SDK_INT >= 17;
    }

    @DexIgnore
    public static boolean e() {
        return Build.VERSION.SDK_INT >= 18;
    }

    @DexIgnore
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 19;
    }

    @DexIgnore
    public static boolean g() {
        return Build.VERSION.SDK_INT >= 20;
    }

    @DexIgnore
    public static boolean h() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public static boolean i() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public static boolean j() {
        return Build.VERSION.SDK_INT >= 26;
    }

    @DexIgnore
    public static boolean k() {
        return Build.VERSION.SDK_INT >= 28;
    }

    @DexIgnore
    public static boolean l() {
        return Build.VERSION.SDK_INT >= 29;
    }

    @DexIgnore
    public static boolean m() {
        boolean z = false;
        if (!l()) {
            return false;
        }
        if (!(b8.a() || (Build.VERSION.CODENAME.equals("REL") && Build.VERSION.SDK_INT >= 30) || (Build.VERSION.CODENAME.length() == 1 && Build.VERSION.CODENAME.charAt(0) >= 'R' && Build.VERSION.CODENAME.charAt(0) <= 'Z'))) {
            return false;
        }
        Boolean bool = a;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            if ("google".equals(Build.BRAND) && Integer.parseInt(Build.VERSION.INCREMENTAL) >= 5954562) {
                z = true;
            }
            a = Boolean.valueOf(z);
        } catch (NumberFormatException unused) {
            a = true;
        }
        if (!a.booleanValue()) {
            Log.w("PlatformVersion", "Build version must be at least 5954562 to support R in gmscore");
        }
        return a.booleanValue();
    }
}
