package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q54 extends v54.d.AbstractC0206d.a.b.e.AbstractC0215b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Integer e;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a a(String str) {
            this.c = str;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a a(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a b(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null symbol");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a a(int i) {
            this.e = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b.AbstractC0216a
        public v54.d.AbstractC0206d.a.b.e.AbstractC0215b a() {
            String str = "";
            if (this.a == null) {
                str = str + " pc";
            }
            if (this.b == null) {
                str = str + " symbol";
            }
            if (this.d == null) {
                str = str + " offset";
            }
            if (this.e == null) {
                str = str + " importance";
            }
            if (str.isEmpty()) {
                return new q54(this.a.longValue(), this.b, this.c, this.d.longValue(), this.e.intValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b
    public String a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b
    public int b() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b
    public long c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b
    public long d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d.a.b.e.AbstractC0215b
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        String str;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d.a.b.e.AbstractC0215b)) {
            return false;
        }
        v54.d.AbstractC0206d.a.b.e.AbstractC0215b bVar = (v54.d.AbstractC0206d.a.b.e.AbstractC0215b) obj;
        if (this.a == bVar.d() && this.b.equals(bVar.e()) && ((str = this.c) != null ? str.equals(bVar.a()) : bVar.a() == null) && this.d == bVar.c() && this.e == bVar.b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int hashCode = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003;
        String str = this.c;
        int hashCode2 = str == null ? 0 : str.hashCode();
        long j2 = this.d;
        return this.e ^ ((((hashCode ^ hashCode2) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "Frame{pc=" + this.a + ", symbol=" + this.b + ", file=" + this.c + ", offset=" + this.d + ", importance=" + this.e + "}";
    }

    @DexIgnore
    public q54(long j, String str, String str2, long j2, int i) {
        this.a = j;
        this.b = str;
        this.c = str2;
        this.d = j2;
        this.e = i;
    }
}
