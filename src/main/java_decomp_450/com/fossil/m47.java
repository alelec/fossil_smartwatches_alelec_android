package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;

    @DexIgnore
    public m47(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void run() {
        try {
            new Thread(new s47(this.a, null, null), "NetworkMonitorTask").start();
        } catch (Throwable th) {
            z37.m.a(th);
            z37.a(this.a, th);
        }
    }
}
