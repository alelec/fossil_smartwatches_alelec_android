package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface l94 {

    @DexIgnore
    public enum a {
        NONE(0),
        SDK(1),
        GLOBAL(2),
        COMBINED(3);
        
        @DexIgnore
        public /* final */ int code;

        @DexIgnore
        public a(int i) {
            this.code = i;
        }

        @DexIgnore
        public int getCode() {
            return this.code;
        }
    }

    @DexIgnore
    a a(String str);
}
