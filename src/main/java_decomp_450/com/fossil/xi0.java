package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi0 extends fe7 implements kd7<v81, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xi0(vv0 vv0, byte[] bArr, long j) {
        super(2);
        this.a = vv0;
        this.b = bArr;
        this.c = j;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(v81 v81, Float f) {
        vv0 vv0 = this.a;
        long floatValue = this.c + ((long) (f.floatValue() * ((float) this.b.length)));
        vv0.E = floatValue;
        float f2 = (((float) floatValue) * 1.0f) / ((float) vv0.D);
        if (Math.abs(f2 - vv0.F) > this.a.Q || f2 == 1.0f) {
            vv0 vv02 = this.a;
            vv02.F = f2;
            vv02.a(f2);
        }
        return i97.a;
    }
}
