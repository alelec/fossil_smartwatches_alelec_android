package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rw1 extends yw1 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ ku1 c;

    @DexIgnore
    public rw1(long j, pu1 pu1, ku1 ku1) {
        this.a = j;
        if (pu1 != null) {
            this.b = pu1;
            if (ku1 != null) {
                this.c = ku1;
                return;
            }
            throw new NullPointerException("Null event");
        }
        throw new NullPointerException("Null transportContext");
    }

    @DexIgnore
    @Override // com.fossil.yw1
    public ku1 a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.yw1
    public long b() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.yw1
    public pu1 c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof yw1)) {
            return false;
        }
        yw1 yw1 = (yw1) obj;
        if (this.a != yw1.b() || !this.b.equals(yw1.c()) || !this.c.equals(yw1.a())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "PersistedEvent{id=" + this.a + ", transportContext=" + this.b + ", event=" + this.c + "}";
    }
}
