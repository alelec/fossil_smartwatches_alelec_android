package com.fossil;

import com.fossil.o60;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yh0 {
    @DexIgnore
    public static r87 a(km1 km1, o60.b bVar) {
        return w87.a(bVar, Boolean.valueOf(km1.u()));
    }

    @DexIgnore
    public static String a(String str) {
        String uuid = UUID.randomUUID().toString();
        ee7.a((Object) uuid, str);
        return uuid;
    }

    @DexIgnore
    public static String a(StringBuilder sb, int i, String str) {
        sb.append(i);
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public static String a(StringBuilder sb, int i, String str, String str2) {
        sb.append(i);
        sb.append(str);
        sb.append(str2);
        return sb.toString();
    }

    @DexIgnore
    public static StringBuilder b(String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        return sb;
    }
}
