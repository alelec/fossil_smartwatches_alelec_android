package com.fossil;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import com.fossil.xi;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cj implements xi {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ xi.a c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ Object e; // = new Object();
    @DexIgnore
    public a f;
    @DexIgnore
    public boolean g;

    @DexIgnore
    public cj(Context context, String str, xi.a aVar, boolean z) {
        this.a = context;
        this.b = str;
        this.c = aVar;
        this.d = z;
    }

    @DexIgnore
    public final a a() {
        a aVar;
        synchronized (this.e) {
            if (this.f == null) {
                bj[] bjVarArr = new bj[1];
                if (Build.VERSION.SDK_INT < 23 || this.b == null || !this.d) {
                    this.f = new a(this.a, this.b, bjVarArr, this.c);
                } else {
                    this.f = new a(this.a, new File(this.a.getNoBackupFilesDir(), this.b).getAbsolutePath(), bjVarArr, this.c);
                }
                if (Build.VERSION.SDK_INT >= 16) {
                    this.f.setWriteAheadLoggingEnabled(this.g);
                }
            }
            aVar = this.f;
        }
        return aVar;
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.xi, java.lang.AutoCloseable
    public void close() {
        a().close();
    }

    @DexIgnore
    @Override // com.fossil.xi
    public String getDatabaseName() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.xi
    public wi getWritableDatabase() {
        return a().a();
    }

    @DexIgnore
    @Override // com.fossil.xi
    public void setWriteAheadLoggingEnabled(boolean z) {
        synchronized (this.e) {
            if (this.f != null) {
                this.f.setWriteAheadLoggingEnabled(z);
            }
            this.g = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends SQLiteOpenHelper {
        @DexIgnore
        public /* final */ bj[] a;
        @DexIgnore
        public /* final */ xi.a b;
        @DexIgnore
        public boolean c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cj$a$a")
        /* renamed from: com.fossil.cj$a$a  reason: collision with other inner class name */
        public class C0028a implements DatabaseErrorHandler {
            @DexIgnore
            public /* final */ /* synthetic */ xi.a a;
            @DexIgnore
            public /* final */ /* synthetic */ bj[] b;

            @DexIgnore
            public C0028a(xi.a aVar, bj[] bjVarArr) {
                this.a = aVar;
                this.b = bjVarArr;
            }

            @DexIgnore
            public void onCorruption(SQLiteDatabase sQLiteDatabase) {
                this.a.b(a.a(this.b, sQLiteDatabase));
            }
        }

        @DexIgnore
        public a(Context context, String str, bj[] bjVarArr, xi.a aVar) {
            super(context, str, null, aVar.a, new C0028a(aVar, bjVarArr));
            this.b = aVar;
            this.a = bjVarArr;
        }

        @DexIgnore
        public synchronized wi a() {
            this.c = false;
            SQLiteDatabase writableDatabase = super.getWritableDatabase();
            if (this.c) {
                close();
                return a();
            }
            return a(writableDatabase);
        }

        @DexIgnore
        @Override // java.lang.AutoCloseable
        public synchronized void close() {
            super.close();
            this.a[0] = null;
        }

        @DexIgnore
        public void onConfigure(SQLiteDatabase sQLiteDatabase) {
            this.b.a(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            this.b.c(a(sQLiteDatabase));
        }

        @DexIgnore
        public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.a(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public void onOpen(SQLiteDatabase sQLiteDatabase) {
            if (!this.c) {
                this.b.d(a(sQLiteDatabase));
            }
        }

        @DexIgnore
        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            this.c = true;
            this.b.b(a(sQLiteDatabase), i, i2);
        }

        @DexIgnore
        public bj a(SQLiteDatabase sQLiteDatabase) {
            return a(this.a, sQLiteDatabase);
        }

        @DexIgnore
        public static bj a(bj[] bjVarArr, SQLiteDatabase sQLiteDatabase) {
            bj bjVar = bjVarArr[0];
            if (bjVar == null || !bjVar.a(sQLiteDatabase)) {
                bjVarArr[0] = new bj(sQLiteDatabase);
            }
            return bjVarArr[0];
        }
    }
}
