package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys5 implements Factory<xs5> {
    @DexIgnore
    public static xs5 a(ss5 ss5, rl4 rl4, pd5 pd5, nw5 nw5, vu5 vu5, mt5 mt5, NotificationSettingsDatabase notificationSettingsDatabase, qq5 qq5, AlarmsRepository alarmsRepository, ch5 ch5, DNDSettingsDatabase dNDSettingsDatabase) {
        return new xs5(ss5, rl4, pd5, nw5, vu5, mt5, notificationSettingsDatabase, qq5, alarmsRepository, ch5, dNDSettingsDatabase);
    }
}
