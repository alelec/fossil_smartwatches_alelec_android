package com.fossil;

import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn0 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<qk1, dt0> d; // = new Hashtable<>();

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        ee7.a((Object) copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final void b(qk1 qk1) {
        dt0 a2 = a(qk1);
        byte[] bArr = this.b;
        byte[] bArr2 = this.c;
        if (!Arrays.equals(a2.b, bArr) || !Arrays.equals(a2.c, bArr2)) {
            a2.d = 0;
        }
        a2.b = bArr;
        a2.c = bArr2;
        a2.d++;
        a2.e = 0;
    }

    @DexIgnore
    public final void a(byte[] bArr, byte[] bArr2) {
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final dt0 a(qk1 qk1) {
        dt0 dt0 = this.d.get(qk1);
        if (dt0 == null) {
            dt0 = new dt0(qk1, this.b, this.c, 0, 0);
        }
        this.d.put(qk1, dt0);
        return dt0;
    }
}
