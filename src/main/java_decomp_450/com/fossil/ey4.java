package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ey4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ConstraintLayout A;
    @DexIgnore
    public /* final */ RecyclerView B;
    @DexIgnore
    public /* final */ SwipeRefreshLayout C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ View F;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleCheckBox v;
    @DexIgnore
    public /* final */ FlexibleEditText w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    public ey4(Object obj, View view, int i, FlexibleButton flexibleButton, ImageView imageView, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, ConstraintLayout constraintLayout, FlexibleCheckBox flexibleCheckBox, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView, ConstraintLayout constraintLayout2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view2) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = flexibleButton2;
        this.t = flexibleButton3;
        this.u = constraintLayout;
        this.v = flexibleCheckBox;
        this.w = flexibleEditText;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = rTLImageView;
        this.A = constraintLayout2;
        this.B = recyclerView;
        this.C = swipeRefreshLayout;
        this.D = flexibleTextView3;
        this.E = flexibleTextView4;
        this.F = view2;
    }
}
