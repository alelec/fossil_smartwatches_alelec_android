package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qj1 extends sj1 {
    @DexIgnore
    public qj1(qa1 qa1, ri1 ri1) {
        super(qa1, ri1);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final void b(eo0 eo0) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONObject a;
        ((v81) this).v = sz0.a(((v81) this).v, null, null, sz0.f.a(eo0.d).c, eo0.d, null, 19);
        if (eo0.d.c.a == z51.START_FAIL) {
            wr1 wr1 = ((v81) this).f;
            if (wr1 != null) {
                wr1.i = false;
            }
            wr1 wr12 = ((v81) this).f;
            if (!(wr12 == null || (jSONObject2 = wr12.m) == null || (a = yz0.a(jSONObject2, r51.j, yz0.a(xr0.e))) == null)) {
                yz0.a(a, r51.N0, eo0.d.a());
            }
        } else {
            wr1 wr13 = ((v81) this).f;
            if (wr13 != null) {
                wr13.i = true;
            }
            wr1 wr14 = ((v81) this).f;
            if (!(wr14 == null || (jSONObject = wr14.m) == null)) {
                yz0.a(jSONObject, r51.j, yz0.a(ay0.a));
            }
        }
        j();
        a(((v81) this).v);
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final String c(sz0 sz0) {
        ay0 ay0 = sz0.c;
        ay0 ay02 = ay0.r;
        if (ay0 == ay02) {
            return yz0.a(ay02);
        }
        x71 x71 = sz0.d.c;
        z51 z51 = x71.a;
        z51 z512 = z51.START_FAIL;
        if (z51 == z512) {
            return yz0.a(z512);
        }
        if (ay0 == ay0.p || ay0 == ay0.q) {
            return yz0.a(ay0.q);
        }
        return yz0.a(xr0.m.a(x71.b));
    }

    @DexIgnore
    @Override // com.fossil.v81
    public final String b(sz0 sz0) {
        z51 z51 = sz0.d.c.a;
        z51 z512 = z51.START_FAIL;
        if (z51 == z512) {
            return yz0.a(z512);
        }
        return yz0.a(ay0.a);
    }
}
