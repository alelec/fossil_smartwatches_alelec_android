package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol6 implements Factory<nl6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public ol6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ol6 a(Provider<ThemeRepository> provider) {
        return new ol6(provider);
    }

    @DexIgnore
    public static nl6 a(ThemeRepository themeRepository) {
        return new nl6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nl6 get() {
        return a(this.a.get());
    }
}
