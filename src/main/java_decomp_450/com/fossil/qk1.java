package com.fossil;

import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX WARN: Init of enum c can be incorrect */
/* JADX WARN: Init of enum d can be incorrect */
/* JADX WARN: Init of enum e can be incorrect */
/* JADX WARN: Init of enum f can be incorrect */
/* JADX WARN: Init of enum g can be incorrect */
/* JADX WARN: Init of enum h can be incorrect */
/* JADX WARN: Init of enum i can be incorrect */
/* JADX WARN: Init of enum k can be incorrect */
/* JADX WARN: Init of enum m can be incorrect */
/* JADX WARN: Init of enum n can be incorrect */
public enum qk1 {
    UNKNOWN("unknown", r2),
    MODEL_NUMBER("model_number", r2),
    SERIAL_NUMBER(Constants.SERIAL_NUMBER, r2),
    FIRMWARE_VERSION(Constants.FIRMWARE_VERSION, r2),
    SOFTWARE_REVISION("software_revision", r2),
    DC("device_config", r2),
    FTC("file_transfer_control", r2),
    FTD("file_transfer_data", (byte) 0),
    ASYNC("async", r2),
    FTD_1("file_transfer_data_1", (byte) 1),
    AUTHENTICATION("authentication", r2),
    HEART_RATE("heart_rate", r2);
    
    @DexIgnore
    public static /* final */ vg1 p; // = new vg1(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        byte b2 = (byte) 255;
    }
    */

    @DexIgnore
    public qk1(String str, byte b2) {
        this.a = str;
        this.b = b2;
    }

    @DexIgnore
    public final ul0 a() {
        switch (si1.a[ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
                return ul0.DEVICE_INFORMATION;
            case 5:
                return ul0.DEVICE_CONFIG;
            case 6:
                return ul0.FILE_CONFIG;
            case 7:
            case 8:
                return ul0.TRANSFER_DATA;
            case 9:
                return ul0.AUTHENTICATION;
            case 10:
                return ul0.ASYNC;
            default:
                return ul0.UNKNOWN;
        }
    }
}
