package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class tm1 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b;

    /*
    static {
        int[] iArr = new int[ru0.values().length];
        a = iArr;
        iArr[ru0.JSON_FILE_EVENT.ordinal()] = 1;
        a[ru0.HEARTBEAT_EVENT.ordinal()] = 2;
        a[ru0.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 3;
        a[ru0.APP_NOTIFICATION_EVENT.ordinal()] = 4;
        a[ru0.MUSIC_EVENT.ordinal()] = 5;
        a[ru0.BACKGROUND_SYNC_EVENT.ordinal()] = 6;
        a[ru0.SERVICE_CHANGE_EVENT.ordinal()] = 7;
        a[ru0.MICRO_APP_EVENT.ordinal()] = 8;
        a[ru0.TIME_SYNC_EVENT.ordinal()] = 9;
        a[ru0.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        a[ru0.BATTERY_EVENT.ordinal()] = 11;
        a[ru0.ENCRYPTED_DATA.ordinal()] = 12;
        int[] iArr2 = new int[s90.values().length];
        b = iArr2;
        iArr2[s90.ACCEPT_PHONE_CALL.ordinal()] = 1;
        b[s90.REJECT_PHONE_CALL.ordinal()] = 2;
        b[s90.DISMISS_NOTIFICATION.ordinal()] = 3;
        b[s90.REPLY_MESSAGE.ordinal()] = 4;
    }
    */
}
