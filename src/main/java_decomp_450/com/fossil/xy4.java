package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xy4 extends wy4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i P; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Q;
    @DexIgnore
    public long O;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        Q = sparseIntArray;
        sparseIntArray.put(2131363088, 1);
        Q.put(2131362049, 2);
        Q.put(2131362727, 3);
        Q.put(2131362163, 4);
        Q.put(2131362636, 5);
        Q.put(2131362611, 6);
        Q.put(2131362212, 7);
        Q.put(2131362607, 8);
        Q.put(2131362209, 9);
        Q.put(2131362615, 10);
        Q.put(2131362213, 11);
        Q.put(2131363260, 12);
        Q.put(2131362435, 13);
        Q.put(2131362773, 14);
        Q.put(2131362210, 15);
        Q.put(2131363277, 16);
        Q.put(2131362211, 17);
        Q.put(2131363301, 18);
        Q.put(2131362791, 19);
        Q.put(2131362214, 20);
        Q.put(2131363416, 21);
        Q.put(2131361968, 22);
        Q.put(2131361966, 23);
    }
    */

    @DexIgnore
    public xy4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 24, P, Q));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.O = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.O != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.O = 1;
        }
        g();
    }

    @DexIgnore
    public xy4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleButton) objArr[23], (FlexibleButton) objArr[22], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[4], (FlexibleTextInputEditText) objArr[9], (FlexibleEditText) objArr[15], (FlexibleEditText) objArr[17], (FlexibleTextInputEditText) objArr[7], (FlexibleTextInputEditText) objArr[11], (FlexibleEditText) objArr[20], (FlexibleTextView) objArr[13], (FlexibleTextInputLayout) objArr[8], (FlexibleTextInputLayout) objArr[6], (FlexibleTextInputLayout) objArr[10], (RTLImageView) objArr[5], (ImageView) objArr[3], (LinearLayout) objArr[14], (LinearLayout) objArr[19], (ConstraintLayout) objArr[0], (ScrollView) objArr[1], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[16], (FlexibleTextView) objArr[18], (View) objArr[21]);
        this.O = -1;
        ((wy4) this).I.setTag(null);
        a(view);
        f();
    }
}
