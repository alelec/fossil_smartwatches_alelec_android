package com.fossil;

import com.fossil.nz;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dy<DataType> implements nz.b {
    @DexIgnore
    public /* final */ vw<DataType> a;
    @DexIgnore
    public /* final */ DataType b;
    @DexIgnore
    public /* final */ ax c;

    @DexIgnore
    public dy(vw<DataType> vwVar, DataType datatype, ax axVar) {
        this.a = vwVar;
        this.b = datatype;
        this.c = axVar;
    }

    @DexIgnore
    @Override // com.fossil.nz.b
    public boolean a(File file) {
        return this.a.a(this.b, file, this.c);
    }
}
