package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pz2 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(tu2.zza),
    ENUM(null),
    MESSAGE(null);
    
    @DexIgnore
    public /* final */ Object zzj;

    @DexIgnore
    public pz2(Object obj) {
        this.zzj = obj;
    }
}
