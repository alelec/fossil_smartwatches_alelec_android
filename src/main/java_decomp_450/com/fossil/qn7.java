package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface qn7 extends Cloneable {

    @DexIgnore
    public interface a {
        @DexIgnore
        qn7 a(lo7 lo7);
    }

    @DexIgnore
    Response a() throws IOException;

    @DexIgnore
    void a(rn7 rn7);

    @DexIgnore
    lo7 c();

    @DexIgnore
    void cancel();

    @DexIgnore
    boolean e();
}
