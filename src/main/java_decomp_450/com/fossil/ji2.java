package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ji2 extends gi2 implements ii2 {
    @DexIgnore
    public ji2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    @Override // com.fossil.ii2
    public final void a(pd2 pd2) throws RemoteException {
        Parcel zza = zza();
        ej2.a(zza, pd2);
        b(1, zza);
    }
}
