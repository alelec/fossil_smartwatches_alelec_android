package com.fossil;

import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gs5 {
    @DexIgnore
    public /* final */ c76 a;
    @DexIgnore
    public /* final */ d06 b;
    @DexIgnore
    public /* final */ c56 c;
    @DexIgnore
    public /* final */ nh6 d;
    @DexIgnore
    public /* final */ ss5 e;
    @DexIgnore
    public /* final */ qw5 f;
    @DexIgnore
    public /* final */ fh6 g;

    @DexIgnore
    public gs5(c76 c76, d06 d06, c56 c56, nh6 nh6, ss5 ss5, qw5 qw5, fh6 fh6) {
        ee7.b(c76, "mDashboardView");
        ee7.b(d06, "mDianaCustomizeView");
        ee7.b(c56, "mHybridCustomizeView");
        ee7.b(nh6, "mProfileView");
        ee7.b(ss5, "mAlertsView");
        ee7.b(qw5, "mAlertsHybridView");
        ee7.b(fh6, "mUpdateFirmwareView");
        this.a = c76;
        this.b = d06;
        this.c = c56;
        this.d = nh6;
        this.e = ss5;
        this.f = qw5;
        this.g = fh6;
    }

    @DexIgnore
    public final qw5 a() {
        return this.f;
    }

    @DexIgnore
    public final ss5 b() {
        return this.e;
    }

    @DexIgnore
    public final c76 c() {
        return this.a;
    }

    @DexIgnore
    public final d06 d() {
        return this.b;
    }

    @DexIgnore
    public final c56 e() {
        return this.c;
    }

    @DexIgnore
    public final WeakReference<nh6> f() {
        return new WeakReference<>(this.d);
    }

    @DexIgnore
    public final fh6 g() {
        return this.g;
    }
}
