package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp5 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public c a;
    @DexIgnore
    public List<? extends Object> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ r40 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ w95 a;
        @DexIgnore
        public /* final */ /* synthetic */ cp5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a2 = this.a.b.b;
                    if (a2 != null) {
                        Object obj = a2.get(adapterPosition);
                        if (obj instanceof bt5) {
                            bt5 bt5 = (bt5) obj;
                            Contact contact = bt5.getContact();
                            if (contact == null || contact.getContactId() != -100) {
                                Contact contact2 = bt5.getContact();
                                if ((contact2 == null || contact2.getContactId() != -200) && (b = this.a.b.a) != null) {
                                    b.a(bt5);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cp5$b$b")
        /* renamed from: com.fossil.cp5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0029b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public View$OnClickListenerC0029b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                FLogger.INSTANCE.getLocal().d(cp5.e, "ivRemove.setOnClickListener");
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a2 = this.a.b.b;
                    if (a2 != null) {
                        Object obj = a2.get(adapterPosition);
                        if (obj instanceof bt5) {
                            bt5 bt5 = (bt5) obj;
                            bt5.setAdded(!bt5.isAdded());
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String d = cp5.e;
                            local.d(d, "isAdded=" + bt5.isAdded());
                            if (bt5.isAdded()) {
                                cp5 cp5 = this.a.b;
                                cp5.c = cp5.c + 1;
                            } else {
                                cp5 cp52 = this.a.b;
                                cp52.c = cp52.c - 1;
                            }
                        } else if (obj != null) {
                            at5 at5 = (at5) obj;
                            InstalledApp installedApp = at5.getInstalledApp();
                            if (installedApp != null) {
                                InstalledApp installedApp2 = at5.getInstalledApp();
                                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                                if (isSelected != null) {
                                    installedApp.setSelected(!isSelected.booleanValue());
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String d2 = cp5.e;
                            StringBuilder sb = new StringBuilder();
                            sb.append("isSelected=");
                            InstalledApp installedApp3 = at5.getInstalledApp();
                            Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                            if (isSelected2 != null) {
                                sb.append(isSelected2.booleanValue());
                                local2.d(d2, sb.toString());
                                InstalledApp installedApp4 = at5.getInstalledApp();
                                Boolean isSelected3 = installedApp4 != null ? installedApp4.isSelected() : null;
                                if (isSelected3 == null) {
                                    ee7.a();
                                    throw null;
                                } else if (isSelected3.booleanValue()) {
                                    cp5 cp53 = this.a.b;
                                    cp53.c = cp53.c + 1;
                                } else {
                                    cp5 cp54 = this.a.b;
                                    cp54.c = cp54.c - 1;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                        }
                        c b = this.a.b.a;
                        if (b != null) {
                            b.a();
                        }
                        this.a.b.notifyItemChanged(adapterPosition);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements q40<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;
            @DexIgnore
            public /* final */ /* synthetic */ bt5 b;

            @DexIgnore
            public c(b bVar, bt5 bt5) {
                this.a = bVar;
                this.b = bt5;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
                FLogger.INSTANCE.getLocal().d(cp5.e, "renderContactData onResourceReady");
                this.a.a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            @Override // com.fossil.q40
            public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
                FLogger.INSTANCE.getLocal().d(cp5.e, "renderContactData onLoadFailed");
                Contact contact = this.b.getContact();
                if (contact == null) {
                    return false;
                }
                contact.setPhotoThumbUri(null);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(cp5 cp5, w95 w95) {
            super(w95.d());
            ee7.b(w95, "binding");
            this.b = cp5;
            this.a = w95;
            String b2 = eh5.l.a().b("nonBrandSeparatorLine");
            String b3 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                this.a.v.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.a.q.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.q.setOnClickListener(new a(this));
            this.a.u.setOnClickListener(new View$OnClickListenerC0029b(this));
        }

        @DexIgnore
        public final void a(bt5 bt5) {
            Uri uri;
            String str;
            String str2;
            String str3;
            Contact contact;
            String str4;
            ee7.b(bt5, "contactWrapper");
            Contact contact2 = bt5.getContact();
            if (!TextUtils.isEmpty(contact2 != null ? contact2.getPhotoThumbUri() : null)) {
                Contact contact3 = bt5.getContact();
                uri = Uri.parse(contact3 != null ? contact3.getPhotoThumbUri() : null);
            } else {
                uri = null;
            }
            Contact contact4 = bt5.getContact();
            String str5 = "";
            if (contact4 == null || (str = contact4.getFirstName()) == null) {
                str = str5;
            }
            Contact contact5 = bt5.getContact();
            if (contact5 == null || (str2 = contact5.getLastName()) == null) {
                str2 = str5;
            }
            Contact contact6 = bt5.getContact();
            if (contact6 == null || contact6.getContactId() != -100) {
                Contact contact7 = bt5.getContact();
                if (contact7 == null || contact7.getContactId() != -200) {
                    String str6 = str + " " + str2;
                    kd5 a2 = hd5.a(this.a.t);
                    Contact contact8 = bt5.getContact();
                    jd5<Drawable> a3 = a2.a((Object) new gd5(uri, contact8 != null ? contact8.getDisplayName() : null)).a((k40<?>) this.b.d);
                    kd5 a4 = hd5.a(this.a.t);
                    Contact contact9 = bt5.getContact();
                    ee7.a((Object) a3.a((hw<Drawable>) a4.a((Object) new gd5((Uri) null, contact9 != null ? contact9.getDisplayName() : null)).a((k40<?>) this.b.d)).b((q40<Drawable>) new c(this, bt5)).a(this.a.t), "GlideApp.with(binding.iv\u2026nto(binding.ivHybridIcon)");
                    str3 = str6;
                } else {
                    this.a.t.setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131231109));
                    str3 = ig5.a(PortfolioApp.g0.c(), 2131886155);
                    ee7.a((Object) str3, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
                }
            } else {
                this.a.t.setImageDrawable(v6.c(PortfolioApp.g0.c(), 2131231108));
                str3 = ig5.a(PortfolioApp.g0.c(), 2131886154);
                ee7.a((Object) str3, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
            }
            FlexibleTextView flexibleTextView = this.a.s;
            ee7.a((Object) flexibleTextView, "binding.ftvHybridName");
            flexibleTextView.setText(str3);
            Contact contact10 = bt5.getContact();
            if ((contact10 == null || contact10.getContactId() != -100) && ((contact = bt5.getContact()) == null || contact.getContactId() != -200)) {
                Contact contact11 = bt5.getContact();
                Boolean valueOf = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                if (valueOf != null) {
                    if (valueOf.booleanValue()) {
                        Contact contact12 = bt5.getContact();
                        Boolean valueOf2 = contact12 != null ? Boolean.valueOf(contact12.isUseCall()) : null;
                        if (valueOf2 == null) {
                            ee7.a();
                            throw null;
                        } else if (valueOf2.booleanValue()) {
                            str4 = ig5.a(PortfolioApp.g0.c(), 2131886147);
                            ee7.a((Object) str4, "LanguageHelper.getString\u2026pped_Text__CallsMessages)");
                            FlexibleTextView flexibleTextView2 = this.a.r;
                            ee7.a((Object) flexibleTextView2, "binding.ftvHybridFeature");
                            flexibleTextView2.setText(str4);
                            FlexibleTextView flexibleTextView3 = this.a.r;
                            ee7.a((Object) flexibleTextView3, "binding.ftvHybridFeature");
                            flexibleTextView3.setVisibility(0);
                        }
                    }
                    Contact contact13 = bt5.getContact();
                    Boolean valueOf3 = contact13 != null ? Boolean.valueOf(contact13.isUseSms()) : null;
                    if (valueOf3 != null) {
                        if (valueOf3.booleanValue()) {
                            str5 = ig5.a(PortfolioApp.g0.c(), 2131886148);
                            ee7.a((Object) str5, "LanguageHelper.getString\u2026nedTapped_Text__Messages)");
                        }
                        Contact contact14 = bt5.getContact();
                        Boolean valueOf4 = contact14 != null ? Boolean.valueOf(contact14.isUseCall()) : null;
                        if (valueOf4 != null) {
                            if (valueOf4.booleanValue()) {
                                str4 = ig5.a(PortfolioApp.g0.c(), 2131886146);
                                ee7.a((Object) str4, "LanguageHelper.getString\u2026signedTapped_Text__Calls)");
                            } else {
                                str4 = str5;
                            }
                            FlexibleTextView flexibleTextView22 = this.a.r;
                            ee7.a((Object) flexibleTextView22, "binding.ftvHybridFeature");
                            flexibleTextView22.setText(str4);
                            FlexibleTextView flexibleTextView32 = this.a.r;
                            ee7.a((Object) flexibleTextView32, "binding.ftvHybridFeature");
                            flexibleTextView32.setVisibility(0);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                FlexibleTextView flexibleTextView4 = this.a.r;
                ee7.a((Object) flexibleTextView4, "binding.ftvHybridFeature");
                flexibleTextView4.setText(str5);
                FlexibleTextView flexibleTextView5 = this.a.r;
                ee7.a((Object) flexibleTextView5, "binding.ftvHybridFeature");
                flexibleTextView5.setVisibility(8);
            }
            if (bt5.isAdded()) {
                this.a.u.setImageResource(2131231152);
            } else {
                this.a.u.setImageResource(2131231119);
            }
        }

        @DexIgnore
        public final void a(at5 at5) {
            ee7.b(at5, "appWrapper");
            kd5 a2 = hd5.a(this.a.t);
            InstalledApp installedApp = at5.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new ed5(installedApp)).a(((r40) new r40().a((ex<Bitmap>) new vd5())).h()).a(this.a.t);
                FlexibleTextView flexibleTextView = this.a.s;
                ee7.a((Object) flexibleTextView, "binding.ftvHybridName");
                InstalledApp installedApp2 = at5.getInstalledApp();
                flexibleTextView.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                FlexibleTextView flexibleTextView2 = this.a.r;
                ee7.a((Object) flexibleTextView2, "binding.ftvHybridFeature");
                flexibleTextView2.setVisibility(8);
                InstalledApp installedApp3 = at5.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected == null) {
                    ee7.a();
                    throw null;
                } else if (isSelected.booleanValue()) {
                    this.a.u.setImageResource(2131231152);
                } else {
                    this.a.u.setImageResource(2131231119);
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(bt5 bt5);
    }

    /*
    static {
        new a(null);
        String name = cp5.class.getName();
        ee7.a((Object) name, "NotificationContactsAndA\u2026dAdapter::class.java.name");
        e = name;
    }
    */

    @DexIgnore
    public cp5() {
        k40 a2 = ((r40) ((r40) ((r40) new r40().a((ex<Bitmap>) new vd5())).h()).a(iy.a)).a(true);
        ee7.a((Object) a2, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        this.d = (r40) a2;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends Object> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final boolean c() {
        return getItemCount() + this.c <= 12;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        w95 a2 = w95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemNotificationHybridBi\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(List<? extends Object> list) {
        ee7.b(list, "data");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        List<? extends Object> list = this.b;
        if (list != null) {
            Object obj = list.get(i);
            if (obj instanceof bt5) {
                bVar.a((bt5) obj);
            } else if (obj != null) {
                bVar.a((at5) obj);
            } else {
                throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        ee7.b(cVar, "listener");
        this.a = cVar;
    }
}
