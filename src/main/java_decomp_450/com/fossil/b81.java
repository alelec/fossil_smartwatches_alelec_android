package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b81 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ d61 CREATOR; // = new d61(null);
    @DexIgnore
    public /* final */ double a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public b81(double d, int i) {
        this.a = d;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.T1, Double.valueOf(this.a)), r51.K5, Integer.valueOf(this.b));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(b81.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            b81 b81 = (b81) obj;
            return this.a == b81.a && this.b == b81.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutGPSDistance");
    }

    @DexIgnore
    public int hashCode() {
        return Integer.valueOf(this.b).hashCode() + (Double.valueOf(this.a).hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeDouble(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }
}
