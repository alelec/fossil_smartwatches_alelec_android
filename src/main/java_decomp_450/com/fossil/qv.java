package com.fossil;

import com.facebook.internal.Utility;
import java.io.DataOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qv extends iv {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ SSLSocketFactory b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends FilterInputStream {
        @DexIgnore
        public /* final */ HttpURLConnection a;

        @DexIgnore
        public a(HttpURLConnection httpURLConnection) {
            super(qv.b(httpURLConnection));
            this.a = httpURLConnection;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.Closeable, java.lang.AutoCloseable, java.io.InputStream
        public void close() throws IOException {
            super.close();
            this.a.disconnect();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        String a(String str);
    }

    @DexIgnore
    public qv() {
        this(null);
    }

    @DexIgnore
    public static boolean a(int i, int i2) {
        return (i == 4 || (100 <= i2 && i2 < 200) || i2 == 204 || i2 == 304) ? false : true;
    }

    @DexIgnore
    @Override // com.fossil.iv
    public ov b(yu<?> yuVar, Map<String, String> map) throws IOException, lu {
        String url = yuVar.getUrl();
        HashMap hashMap = new HashMap();
        hashMap.putAll(map);
        hashMap.putAll(yuVar.getHeaders());
        b bVar = this.a;
        if (bVar != null) {
            String a2 = bVar.a(url);
            if (a2 != null) {
                url = a2;
            } else {
                throw new IOException("URL blocked by rewriter: " + url);
            }
        }
        HttpURLConnection a3 = a(new URL(url), yuVar);
        try {
            for (String str : hashMap.keySet()) {
                a3.setRequestProperty(str, (String) hashMap.get(str));
            }
            b(a3, yuVar);
            int responseCode = a3.getResponseCode();
            if (responseCode == -1) {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            } else if (a(yuVar.getMethod(), responseCode)) {
                return new ov(responseCode, a(a3.getHeaderFields()), a3.getContentLength(), new a(a3));
            } else {
                ov ovVar = new ov(responseCode, a(a3.getHeaderFields()));
                a3.disconnect();
                return ovVar;
            }
        } catch (Throwable th) {
            if (0 == 0) {
                a3.disconnect();
            }
            throw th;
        }
    }

    @DexIgnore
    public qv(b bVar) {
        this(bVar, null);
    }

    @DexIgnore
    public static List<ru> a(Map<String, List<String>> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, List<String>> entry : map.entrySet()) {
            if (entry.getKey() != null) {
                for (String str : entry.getValue()) {
                    arrayList.add(new ru(entry.getKey(), str));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public qv(b bVar, SSLSocketFactory sSLSocketFactory) {
        this.a = bVar;
        this.b = sSLSocketFactory;
    }

    @DexIgnore
    public HttpURLConnection a(URL url) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setInstanceFollowRedirects(HttpURLConnection.getFollowRedirects());
        return httpURLConnection;
    }

    @DexIgnore
    public final HttpURLConnection a(URL url, yu<?> yuVar) throws IOException {
        SSLSocketFactory sSLSocketFactory;
        HttpURLConnection a2 = a(url);
        int timeoutMs = yuVar.getTimeoutMs();
        a2.setConnectTimeout(timeoutMs);
        a2.setReadTimeout(timeoutMs);
        a2.setUseCaches(false);
        a2.setDoInput(true);
        if (Utility.URL_SCHEME.equals(url.getProtocol()) && (sSLSocketFactory = this.b) != null) {
            ((HttpsURLConnection) a2).setSSLSocketFactory(sSLSocketFactory);
        }
        return a2;
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, yu<?> yuVar) throws IOException, lu {
        byte[] body = yuVar.getBody();
        if (body != null) {
            a(httpURLConnection, yuVar, body);
        }
    }

    @DexIgnore
    public static void a(HttpURLConnection httpURLConnection, yu<?> yuVar, byte[] bArr) throws IOException {
        httpURLConnection.setDoOutput(true);
        if (!httpURLConnection.getRequestProperties().containsKey("Content-Type")) {
            httpURLConnection.setRequestProperty("Content-Type", yuVar.getBodyContentType());
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(bArr);
        dataOutputStream.close();
    }

    @DexIgnore
    public static InputStream b(HttpURLConnection httpURLConnection) {
        try {
            return httpURLConnection.getInputStream();
        } catch (IOException unused) {
            return httpURLConnection.getErrorStream();
        }
    }

    @DexIgnore
    public static void b(HttpURLConnection httpURLConnection, yu<?> yuVar) throws IOException, lu {
        switch (yuVar.getMethod()) {
            case -1:
                byte[] postBody = yuVar.getPostBody();
                if (postBody != null) {
                    httpURLConnection.setRequestMethod("POST");
                    a(httpURLConnection, yuVar, postBody);
                    return;
                }
                return;
            case 0:
                httpURLConnection.setRequestMethod("GET");
                return;
            case 1:
                httpURLConnection.setRequestMethod("POST");
                a(httpURLConnection, yuVar);
                return;
            case 2:
                httpURLConnection.setRequestMethod("PUT");
                a(httpURLConnection, yuVar);
                return;
            case 3:
                httpURLConnection.setRequestMethod("DELETE");
                return;
            case 4:
                httpURLConnection.setRequestMethod("HEAD");
                return;
            case 5:
                httpURLConnection.setRequestMethod("OPTIONS");
                return;
            case 6:
                httpURLConnection.setRequestMethod("TRACE");
                return;
            case 7:
                httpURLConnection.setRequestMethod("PATCH");
                a(httpURLConnection, yuVar);
                return;
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }
}
