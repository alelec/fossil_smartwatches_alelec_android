package com.fossil;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.text.TextUtils;
import com.fossil.cn5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uw6 extends ContentObserver implements cn5.a {
    @DexIgnore
    public static /* final */ String l; // = uw6.class.getSimpleName();
    @DexIgnore
    public /* final */ HashMap<String, Integer> a; // = new HashMap<>();
    @DexIgnore
    public /* final */ NotificationsRepository b;
    @DexIgnore
    public /* final */ ContentResolver c;
    @DexIgnore
    public /* final */ pj4 d;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ cn5 f;
    @DexIgnore
    public /* final */ ch5 g;
    @DexIgnore
    public DeviceRepository h;
    @DexIgnore
    public NotificationSettingsDatabase i;
    @DexIgnore
    public long j;
    @DexIgnore
    public long k;

    @DexIgnore
    public uw6(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, pj4 pj4, ContentResolver contentResolver, cn5 cn5, ch5 ch5) {
        super(null);
        new HashMap();
        this.j = System.currentTimeMillis();
        this.k = -1;
        this.e = portfolioApp;
        this.f = cn5;
        jw3.a(pj4, "appExecutors cannot be NULL");
        this.d = pj4;
        jw3.a(notificationsRepository, "repository cannot be nulL!");
        this.b = notificationsRepository;
        jw3.a(deviceRepository, "deviceRepository cannot be nulL!");
        this.h = deviceRepository;
        jw3.a(notificationSettingsDatabase, "notificationSettingsDatabase cannot be null!");
        this.i = notificationSettingsDatabase;
        jw3.a(contentResolver, "contentResolver cannot be null!");
        this.c = contentResolver;
        this.g = ch5;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0028, code lost:
        r10 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002a, code lost:
        r0 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002e, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0030, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0013, code lost:
        if (r1 != null) goto L_0x0015;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
        r10 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001a, code lost:
        if (r1.moveToNext() == false) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0026, code lost:
        if (r1.getInt(r1.getColumnIndex("starred")) != 1) goto L_0x0015;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003e A[DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r10) {
        /*
            r9 = this;
            r0 = 0
            r1 = 0
            android.content.ContentResolver r2 = r9.c     // Catch:{ Exception -> 0x0036 }
            android.net.Uri r3 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0036 }
            r4 = 0
            java.lang.String r5 = "contact_id = ?"
            r8 = 1
            java.lang.String[] r6 = new java.lang.String[r8]     // Catch:{ Exception -> 0x0036 }
            r6[r0] = r10     // Catch:{ Exception -> 0x0036 }
            r7 = 0
            android.database.Cursor r1 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0036 }
            if (r1 == 0) goto L_0x002e
        L_0x0015:
            r10 = 0
        L_0x0016:
            boolean r2 = r1.moveToNext()     // Catch:{ Exception -> 0x002c }
            if (r2 == 0) goto L_0x002a
            java.lang.String r2 = "starred"
            int r2 = r1.getColumnIndex(r2)     // Catch:{ Exception -> 0x002c }
            int r10 = r1.getInt(r2)     // Catch:{ Exception -> 0x002c }
            if (r10 != r8) goto L_0x0015
            r10 = 1
            goto L_0x0016
        L_0x002a:
            r0 = r10
            goto L_0x002e
        L_0x002c:
            r0 = move-exception
            goto L_0x0039
        L_0x002e:
            if (r1 == 0) goto L_0x0042
            r1.close()
            goto L_0x0042
        L_0x0034:
            r10 = move-exception
            goto L_0x0043
        L_0x0036:
            r10 = move-exception
            r0 = r10
            r10 = 0
        L_0x0039:
            r0.printStackTrace()     // Catch:{ all -> 0x0034 }
            if (r1 == 0) goto L_0x0041
            r1.close()
        L_0x0041:
            r0 = r10
        L_0x0042:
            return r0
        L_0x0043:
            if (r1 == 0) goto L_0x0048
            r1.close()
        L_0x0048:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw6.a(java.lang.String):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        if (0 == 0) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x003a, code lost:
        return r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002b, code lost:
        if (r2 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002d, code lost:
        r2.close();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String b(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r0 = "photo_thumb_uri"
            java.lang.String r1 = ""
            r2 = 0
            android.content.ContentResolver r3 = r9.c     // Catch:{ Exception -> 0x0033 }
            android.net.Uri r4 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0033 }
            java.lang.String[] r5 = new java.lang.String[]{r0}     // Catch:{ Exception -> 0x0033 }
            java.lang.String r6 = "contact_id = ?"
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ Exception -> 0x0033 }
            r8 = 0
            r7[r8] = r10     // Catch:{ Exception -> 0x0033 }
            r8 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0033 }
            if (r2 == 0) goto L_0x002b
        L_0x001c:
            boolean r10 = r2.moveToNext()     // Catch:{ Exception -> 0x0033 }
            if (r10 == 0) goto L_0x002b
            int r10 = r2.getColumnIndex(r0)     // Catch:{ Exception -> 0x0033 }
            java.lang.String r1 = r2.getString(r10)     // Catch:{ Exception -> 0x0033 }
            goto L_0x001c
        L_0x002b:
            if (r2 == 0) goto L_0x003a
        L_0x002d:
            r2.close()
            goto L_0x003a
        L_0x0031:
            r10 = move-exception
            goto L_0x003b
        L_0x0033:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x0031 }
            if (r2 == 0) goto L_0x003a
            goto L_0x002d
        L_0x003a:
            return r1
        L_0x003b:
            if (r2 == 0) goto L_0x0040
            r2.close()
        L_0x0040:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw6.b(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0034, code lost:
        if (0 == 0) goto L_0x0037;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0037, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0028, code lost:
        if (r1 != null) goto L_0x002a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002a, code lost:
        r1.close();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String c(java.lang.String r9) {
        /*
            r8 = this;
            java.lang.String r0 = ""
            r1 = 0
            android.content.ContentResolver r2 = r8.c     // Catch:{ Exception -> 0x0030 }
            android.net.Uri r3 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0030 }
            r4 = 0
            java.lang.String r5 = "contact_id = ?"
            r6 = 1
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ Exception -> 0x0030 }
            r7 = 0
            r6[r7] = r9     // Catch:{ Exception -> 0x0030 }
            r7 = 0
            android.database.Cursor r1 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x0030 }
            if (r1 == 0) goto L_0x0028
        L_0x0017:
            boolean r9 = r1.moveToNext()     // Catch:{ Exception -> 0x0030 }
            if (r9 == 0) goto L_0x0028
            java.lang.String r9 = "display_name"
            int r9 = r1.getColumnIndex(r9)     // Catch:{ Exception -> 0x0030 }
            java.lang.String r0 = r1.getString(r9)     // Catch:{ Exception -> 0x0030 }
            goto L_0x0017
        L_0x0028:
            if (r1 == 0) goto L_0x0037
        L_0x002a:
            r1.close()
            goto L_0x0037
        L_0x002e:
            r9 = move-exception
            goto L_0x0038
        L_0x0030:
            r9 = move-exception
            r9.printStackTrace()     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0037
            goto L_0x002a
        L_0x0037:
            return r0
        L_0x0038:
            if (r1 == 0) goto L_0x003d
            r1.close()
        L_0x003d:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw6.c(java.lang.String):java.lang.String");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0041, code lost:
        if (r2 != null) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004a, code lost:
        if (0 == 0) goto L_0x004f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x004c, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x004f, code lost:
        return r1;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> d(java.lang.String r10) {
        /*
            r9 = this;
            java.lang.String r0 = "data1"
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            android.content.ContentResolver r3 = r9.c     // Catch:{ Exception -> 0x0046 }
            android.net.Uri r4 = android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI     // Catch:{ Exception -> 0x0046 }
            java.lang.String[] r5 = new java.lang.String[]{r0}     // Catch:{ Exception -> 0x0046 }
            java.lang.String r6 = "contact_id = ?"
            r7 = 1
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ Exception -> 0x0046 }
            r8 = 0
            r7[r8] = r10     // Catch:{ Exception -> 0x0046 }
            r8 = 0
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8)     // Catch:{ Exception -> 0x0046 }
            if (r2 == 0) goto L_0x0041
        L_0x001f:
            boolean r10 = r2.moveToNext()     // Catch:{ Exception -> 0x0046 }
            if (r10 == 0) goto L_0x0041
            int r10 = r2.getColumnIndex(r0)     // Catch:{ Exception -> 0x0046 }
            java.lang.String r10 = r2.getString(r10)     // Catch:{ Exception -> 0x0046 }
            boolean r3 = android.text.TextUtils.isEmpty(r10)     // Catch:{ Exception -> 0x0046 }
            if (r3 != 0) goto L_0x001f
            java.lang.Boolean r3 = com.fossil.yx6.a(r1, r10)     // Catch:{ Exception -> 0x0046 }
            boolean r3 = r3.booleanValue()     // Catch:{ Exception -> 0x0046 }
            if (r3 != 0) goto L_0x001f
            r1.add(r10)     // Catch:{ Exception -> 0x0046 }
            goto L_0x001f
        L_0x0041:
            if (r2 == 0) goto L_0x004f
            goto L_0x004c
        L_0x0044:
            r10 = move-exception
            goto L_0x0050
        L_0x0046:
            r10 = move-exception
            r10.printStackTrace()     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x004f
        L_0x004c:
            r2.close()
        L_0x004f:
            return r1
        L_0x0050:
            if (r2 == 0) goto L_0x0055
            r2.close()
        L_0x0055:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw6.d(java.lang.String):java.util.List");
    }

    @DexIgnore
    public boolean deliverSelfNotifications() {
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0051, code lost:
        if (0 == 0) goto L_0x0054;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        return !a(r11, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x002c, code lost:
        if (r3 != null) goto L_0x002e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        r3.close();
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean e(java.lang.String r11) {
        /*
            r10 = this;
            java.lang.String r0 = "version"
            r1 = 1
            r2 = 0
            r3 = 0
            android.content.ContentResolver r4 = r10.c     // Catch:{ Exception -> 0x0034 }
            android.net.Uri r5 = android.provider.ContactsContract.RawContacts.CONTENT_URI     // Catch:{ Exception -> 0x0034 }
            java.lang.String r6 = "contact_id"
            java.lang.String[] r6 = new java.lang.String[]{r6, r0}     // Catch:{ Exception -> 0x0034 }
            java.lang.String r7 = "contact_id = ?"
            java.lang.String[] r8 = new java.lang.String[r1]     // Catch:{ Exception -> 0x0034 }
            r8[r2] = r11     // Catch:{ Exception -> 0x0034 }
            r9 = 0
            android.database.Cursor r3 = r4.query(r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0034 }
            if (r3 == 0) goto L_0x002c
        L_0x001c:
            boolean r4 = r3.moveToNext()     // Catch:{ Exception -> 0x0034 }
            if (r4 == 0) goto L_0x002c
            int r4 = r3.getColumnIndex(r0)     // Catch:{ Exception -> 0x0034 }
            int r4 = r3.getInt(r4)     // Catch:{ Exception -> 0x0034 }
            int r2 = r2 + r4
            goto L_0x001c
        L_0x002c:
            if (r3 == 0) goto L_0x0054
        L_0x002e:
            r3.close()
            goto L_0x0054
        L_0x0032:
            r11 = move-exception
            goto L_0x005a
        L_0x0034:
            r0 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0032 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()     // Catch:{ all -> 0x0032 }
            java.lang.String r5 = com.fossil.uw6.l     // Catch:{ all -> 0x0032 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0032 }
            r6.<init>()     // Catch:{ all -> 0x0032 }
            java.lang.String r7 = "isHasNewVersion e="
            r6.append(r7)     // Catch:{ all -> 0x0032 }
            r6.append(r0)     // Catch:{ all -> 0x0032 }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x0032 }
            r4.d(r5, r0)     // Catch:{ all -> 0x0032 }
            if (r3 == 0) goto L_0x0054
            goto L_0x002e
        L_0x0054:
            boolean r11 = r10.a(r11, r2)
            r11 = r11 ^ r1
            return r11
        L_0x005a:
            if (r3 == 0) goto L_0x005f
            r3.close()
        L_0x005f:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uw6.e(java.lang.String):boolean");
    }

    @DexIgnore
    public final void f(String str) {
        synchronized (this.a) {
            int size = this.a.size();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, "removeOtherContactVersionHashMap. Size = " + size);
            LinkedList<String> linkedList = new LinkedList();
            for (String str3 : this.a.keySet()) {
                if (!str3.equals(str)) {
                    linkedList.add(str3);
                }
            }
            for (String str4 : linkedList) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str5 = l;
                local2.d(str5, "remove from contact versions, contactId= " + str4);
                this.a.remove(str4);
            }
        }
    }

    @DexIgnore
    public void onChange(boolean z, Uri uri) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = l;
        local.d(str2, "onChange selftChange=" + z + ", uri=" + uri + "update = " + this.k);
        if (System.currentTimeMillis() - this.j >= 1000) {
            this.j = System.currentTimeMillis();
            if (px6.a.a(this.e, "android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.READ_CONTACTS", "android.permission.READ_SMS")) {
                Cursor query = this.c.query(ContactsContract.DeletedContacts.CONTENT_URI, new String[]{"contact_id", "contact_deleted_timestamp"}, null, null, "contact_deleted_timestamp Desc");
                if (query != null && query.moveToFirst()) {
                    String string = query.getString(query.getColumnIndex("contact_id"));
                    Long valueOf = Long.valueOf(query.getLong(query.getColumnIndex("contact_deleted_timestamp")));
                    if (!(string == null || this.k == valueOf.longValue())) {
                        boolean z2 = this.k == -1;
                        this.k = valueOf.longValue();
                        int intValue = Integer.valueOf(string).intValue();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = l;
                        local2.d(str3, "Last deleted happen for contactId=" + intValue);
                        ArrayList<ContactGroup> arrayList = new ArrayList();
                        List<ContactGroup> allContactGroups = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue());
                        List<ContactGroup> allContactGroups2 = this.b.getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                        if (allContactGroups != null) {
                            arrayList.addAll(allContactGroups);
                        }
                        if (allContactGroups2 != null) {
                            arrayList.addAll(allContactGroups2);
                        }
                        ArrayList<PhoneFavoritesContact> arrayList2 = new ArrayList();
                        if (!arrayList.isEmpty()) {
                            boolean z3 = false;
                            for (ContactGroup contactGroup : arrayList) {
                                arrayList2.clear();
                                if (contactGroup.getContacts().get(0).getContactId() == intValue) {
                                    Contact contact = contactGroup.getContacts().get(0);
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String str4 = l;
                                    local3.d(str4, "Found deleted contact in app with contactName = " + contact.getDisplayName());
                                    for (PhoneNumber phoneNumber : contact.getPhoneNumbers()) {
                                        arrayList2.add(new PhoneFavoritesContact(phoneNumber.getNumber()));
                                    }
                                    this.b.removeContact(contact);
                                    this.b.removeContactGroup(contactGroup);
                                    for (PhoneFavoritesContact phoneFavoritesContact : arrayList2) {
                                        this.b.removePhoneFavoritesContact(phoneFavoritesContact);
                                    }
                                    z3 = true;
                                }
                            }
                            if (z3) {
                                d();
                            }
                        }
                        if (!z2) {
                            query.close();
                            return;
                        }
                    }
                    query.close();
                }
                Cursor query2 = this.c.query(ContactsContract.Contacts.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX}, null, null, "contact_last_updated_timestamp Desc");
                if (query2 == null || !query2.moveToFirst()) {
                    str = "";
                } else {
                    str = query2.getString(query2.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str5 = l;
                    local4.d(str5, "Last updated happen for contactId=" + str);
                    query2.close();
                }
                if (TextUtils.isEmpty(str)) {
                    FLogger.INSTANCE.getLocal().d(l, "No change for contact, return");
                    return;
                }
                String c2 = c(str);
                boolean a2 = a(str);
                String b2 = b(str);
                List<String> d2 = d(str);
                this.g.k(true);
                if (e(str)) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str6 = l;
                    local5.d(str6, "Update Contact: " + str + " - contactName: " + c2 + " - contactImageUri: " + b2 + " - contactPhone: " + d2 + " - isFavorites:" + a2);
                    this.f.a(str, c2, d2, b2);
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2) {
        synchronized (this.a) {
            Integer num = this.a.get(str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = l;
            local.d(str2, " Enter isVersionLasted contactId= " + str + " savedVersion=" + num + " newVersion:" + i2);
            if (num == null) {
                FLogger.INSTANCE.getLocal().d(l, "Contact not found, let's update");
                this.a.put(str, Integer.valueOf(i2));
                return false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = l;
            local2.d(str3, "Found contact " + str);
            if (i2 > num.intValue()) {
                FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is old, let's update");
                this.a.put(str, Integer.valueOf(i2));
                return false;
            }
            FLogger.INSTANCE.getLocal().d(l, "Saved version of this contact is lasted, skip!");
            f(str);
            return true;
        }
    }

    @DexIgnore
    public void b() {
        this.f.a(this);
    }

    @DexIgnore
    public void c() {
        this.f.a(null);
    }

    @DexIgnore
    public final void d() {
        AppNotificationFilterSettings appNotificationFilterSettings;
        FLogger.INSTANCE.getLocal().d(l, "updateNotificationSettings()");
        String c2 = PortfolioApp.c0.c();
        if (!TextUtils.isEmpty(c2)) {
            if (!FossilDeviceSerialPatternUtil.isDianaDevice(c2)) {
                appNotificationFilterSettings = nx6.b.a(this.b.getAllNotificationsByHour(c2, MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()), nx6.b.a(this.h.getSkuModelBySerialPrefix(be5.o.b(c2)), c2));
            } else {
                appNotificationFilterSettings = new AppNotificationFilterSettings(nx6.b.a(this.e, this.i, this.g), System.currentTimeMillis());
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = l;
            local.d(str, "updateNotificationSettings - data = " + appNotificationFilterSettings);
            this.d.b().execute(new pw6(appNotificationFilterSettings, c2));
        }
    }

    @DexIgnore
    @Override // com.fossil.cn5.a
    public void a() {
        d();
    }
}
