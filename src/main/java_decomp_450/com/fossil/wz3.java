package com.fossil;

import com.fossil.dz3;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wz3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> {
        @DexIgnore
        public /* final */ Field a;

        @DexIgnore
        public void a(T t, Object obj) {
            try {
                this.a.set(t, obj);
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }

        @DexIgnore
        public b(Field field) {
            this.a = field;
            field.setAccessible(true);
        }

        @DexIgnore
        public void a(T t, int i) {
            try {
                this.a.set(t, Integer.valueOf(i));
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    @DexIgnore
    public static int a(ObjectInputStream objectInputStream) throws IOException {
        return objectInputStream.readInt();
    }

    @DexIgnore
    public static <E> void a(dz3<E> dz3, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(dz3.entrySet().size());
        for (dz3.a<E> aVar : dz3.entrySet()) {
            objectOutputStream.writeObject(aVar.getElement());
            objectOutputStream.writeInt(aVar.getCount());
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.dz3<E> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public static <E> void a(dz3<E> dz3, ObjectInputStream objectInputStream, int i) throws IOException, ClassNotFoundException {
        for (int i2 = 0; i2 < i; i2++) {
            dz3.add(objectInputStream.readObject(), objectInputStream.readInt());
        }
    }

    @DexIgnore
    public static <K, V> void a(zy3<K, V> zy3, ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(zy3.asMap().size());
        for (Map.Entry<K, Collection<V>> entry : zy3.asMap().entrySet()) {
            objectOutputStream.writeObject(entry.getKey());
            objectOutputStream.writeInt(entry.getValue().size());
            for (V v : entry.getValue()) {
                objectOutputStream.writeObject(v);
            }
        }
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls, String str) {
        try {
            return new b<>(cls.getDeclaredField(str));
        } catch (NoSuchFieldException e) {
            throw new AssertionError(e);
        }
    }
}
