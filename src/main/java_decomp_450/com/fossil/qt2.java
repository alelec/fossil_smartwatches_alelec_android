package com.fossil;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qt2<K> extends ws2<K> {
    @DexIgnore
    public /* final */ transient ss2<K, ?> c;
    @DexIgnore
    public /* final */ transient os2<K> d;

    @DexIgnore
    public qt2(ss2<K, ?> ss2, os2<K> os2) {
        this.c = ss2;
        this.d = os2;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean contains(@NullableDecl Object obj) {
        return this.c.get(obj) != null;
    }

    @DexIgnore
    public final int size() {
        return this.c.size();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<K> iterator() {
        return (yt2) zzc().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ps2, com.fossil.ws2
    public final os2<K> zzc() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        return zzc().zzb(objArr, i);
    }
}
