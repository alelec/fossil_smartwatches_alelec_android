package com.fossil;

import com.fossil.f50;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e50<R> implements f50<R> {
    @DexIgnore
    public static /* final */ e50<?> a; // = new e50<>();
    @DexIgnore
    public static /* final */ g50<?> b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements g50<R> {
        @DexIgnore
        @Override // com.fossil.g50
        public f50<R> a(sw swVar, boolean z) {
            return e50.a;
        }
    }

    @DexIgnore
    public static <R> g50<R> a() {
        return (g50<R>) b;
    }

    @DexIgnore
    @Override // com.fossil.f50
    public boolean a(Object obj, f50.a aVar) {
        return false;
    }
}
