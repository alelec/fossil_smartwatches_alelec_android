package com.fossil;

import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp7 {
    @DexIgnore
    public /* final */ Set<no7> a; // = new LinkedHashSet();

    @DexIgnore
    public synchronized void a(no7 no7) {
        this.a.remove(no7);
    }

    @DexIgnore
    public synchronized void b(no7 no7) {
        this.a.add(no7);
    }

    @DexIgnore
    public synchronized boolean c(no7 no7) {
        return this.a.contains(no7);
    }
}
