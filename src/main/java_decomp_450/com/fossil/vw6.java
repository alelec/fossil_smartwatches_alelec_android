package com.fossil;

import android.content.ContentResolver;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vw6 implements Factory<uw6> {
    @DexIgnore
    public static uw6 a(PortfolioApp portfolioApp, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, pj4 pj4, ContentResolver contentResolver, cn5 cn5, ch5 ch5) {
        return new uw6(portfolioApp, notificationsRepository, deviceRepository, notificationSettingsDatabase, pj4, contentResolver, cn5, ch5);
    }
}
