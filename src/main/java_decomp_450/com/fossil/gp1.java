package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp1 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ fr1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gp1(fr1 fr1) {
        super(1);
        this.a = fr1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        fr1 fr1 = this.a;
        yb0 deviceRequest = fr1.C.getDeviceRequest();
        if (deviceRequest != null) {
            fr1.a(new rf0((jc0) deviceRequest, new df0("", uf0.ERROR)));
            return i97.a;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.WorkoutRouteImageRequest");
    }
}
