package com.fossil;

import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static Object a() {
            return new MediaDescription.Builder();
        }

        @DexIgnore
        public static void b(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setSubtitle(charSequence);
        }

        @DexIgnore
        public static void c(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setTitle(charSequence);
        }

        @DexIgnore
        public static void a(Object obj, String str) {
            ((MediaDescription.Builder) obj).setMediaId(str);
        }

        @DexIgnore
        public static void a(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setDescription(charSequence);
        }

        @DexIgnore
        public static void a(Object obj, Bitmap bitmap) {
            ((MediaDescription.Builder) obj).setIconBitmap(bitmap);
        }

        @DexIgnore
        public static void a(Object obj, Uri uri) {
            ((MediaDescription.Builder) obj).setIconUri(uri);
        }

        @DexIgnore
        public static void a(Object obj, Bundle bundle) {
            ((MediaDescription.Builder) obj).setExtras(bundle);
        }

        @DexIgnore
        public static Object a(Object obj) {
            return ((MediaDescription.Builder) obj).build();
        }
    }

    @DexIgnore
    public static CharSequence a(Object obj) {
        return ((MediaDescription) obj).getDescription();
    }

    @DexIgnore
    public static Bundle b(Object obj) {
        return ((MediaDescription) obj).getExtras();
    }

    @DexIgnore
    public static Bitmap c(Object obj) {
        return ((MediaDescription) obj).getIconBitmap();
    }

    @DexIgnore
    public static Uri d(Object obj) {
        return ((MediaDescription) obj).getIconUri();
    }

    @DexIgnore
    public static String e(Object obj) {
        return ((MediaDescription) obj).getMediaId();
    }

    @DexIgnore
    public static CharSequence f(Object obj) {
        return ((MediaDescription) obj).getSubtitle();
    }

    @DexIgnore
    public static CharSequence g(Object obj) {
        return ((MediaDescription) obj).getTitle();
    }

    @DexIgnore
    public static void a(Object obj, Parcel parcel, int i) {
        ((MediaDescription) obj).writeToParcel(parcel, i);
    }

    @DexIgnore
    public static Object a(Parcel parcel) {
        return MediaDescription.CREATOR.createFromParcel(parcel);
    }
}
