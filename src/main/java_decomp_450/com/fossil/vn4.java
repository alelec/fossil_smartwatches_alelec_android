package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vn4 {
    @DexIgnore
    int a(String str);

    @DexIgnore
    int a(String[] strArr);

    @DexIgnore
    long a(un4 un4);

    @DexIgnore
    void a();

    @DexIgnore
    Long[] a(List<un4> list);

    @DexIgnore
    LiveData<List<un4>> b();

    @DexIgnore
    un4 b(String str);

    @DexIgnore
    List<un4> c();

    @DexIgnore
    int d();

    @DexIgnore
    List<un4> e();

    @DexIgnore
    void f();

    @DexIgnore
    void g();

    @DexIgnore
    List<un4> h();

    @DexIgnore
    void i();

    @DexIgnore
    List<un4> j();

    @DexIgnore
    List<un4> k();

    @DexIgnore
    List<un4> l();
}
