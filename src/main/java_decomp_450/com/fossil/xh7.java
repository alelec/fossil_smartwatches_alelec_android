package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class xh7 {
    @DexIgnore
    public static /* synthetic */ hj7 a(yi7 yi7, ib7 ib7, bj7 bj7, kd7 kd7, int i, Object obj) {
        if ((i & 1) != 0) {
            ib7 = jb7.INSTANCE;
        }
        if ((i & 2) != 0) {
            bj7 = bj7.DEFAULT;
        }
        return vh7.a(yi7, ib7, bj7, kd7);
    }

    @DexIgnore
    public static /* synthetic */ ik7 b(yi7 yi7, ib7 ib7, bj7 bj7, kd7 kd7, int i, Object obj) {
        if ((i & 1) != 0) {
            ib7 = jb7.INSTANCE;
        }
        if ((i & 2) != 0) {
            bj7 = bj7.DEFAULT;
        }
        return vh7.b(yi7, ib7, bj7, kd7);
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r2v2, types: [com.fossil.rh7, com.fossil.hj7<T>, java.lang.Object] */
    public static final <T> hj7<T> a(yi7 yi7, ib7 ib7, bj7 bj7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) {
        Object r2;
        ij7 ij7;
        ib7 a = si7.a(yi7, ib7);
        if (bj7.isLazy()) {
            ij7 = new rk7(a, kd7);
        } else {
            ij7 = new ij7(a, true);
        }
        r2.a(bj7, r2, kd7);
        return r2;
    }

    @DexIgnore
    public static final ik7 b(yi7 yi7, ib7 ib7, bj7 bj7, kd7<? super yi7, ? super fb7<? super i97>, ? extends Object> kd7) {
        rh7 rh7;
        ib7 a = si7.a(yi7, ib7);
        if (bj7.isLazy()) {
            rh7 = new sk7(a, kd7);
        } else {
            rh7 = new al7(a, true);
        }
        rh7.a(bj7, rh7, kd7);
        return rh7;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static final <T> Object a(ib7 ib7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7, fb7<? super T> fb7) {
        Object obj;
        ib7 context = fb7.getContext();
        ib7 plus = context.plus(ib7);
        ol7.a(plus);
        if (plus == context) {
            jm7 jm7 = new jm7(plus, fb7);
            obj = um7.a(jm7, jm7, kd7);
        } else if (ee7.a((gb7) plus.get(gb7.m), (gb7) context.get(gb7.m))) {
            ml7 ml7 = new ml7(plus, fb7);
            Object b = pm7.b(plus, null);
            try {
                Object a = um7.a(ml7, ml7, kd7);
                pm7.a(plus, b);
                obj = a;
            } catch (Throwable th) {
                pm7.a(plus, b);
                throw th;
            }
        } else {
            nj7 nj7 = new nj7(plus, fb7);
            nj7.n();
            tm7.a(kd7, nj7, nj7);
            obj = nj7.p();
        }
        if (obj == nb7.a()) {
            vb7.c(fb7);
        }
        return obj;
    }
}
