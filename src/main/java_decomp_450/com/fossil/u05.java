package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u05 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ LinearLayout s;
    @DexIgnore
    public /* final */ NumberPicker t;
    @DexIgnore
    public /* final */ NumberPicker u;
    @DexIgnore
    public /* final */ NumberPicker v;
    @DexIgnore
    public /* final */ ConstraintLayout w;

    @DexIgnore
    public u05(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, LinearLayout linearLayout, NumberPicker numberPicker, NumberPicker numberPicker2, NumberPicker numberPicker3, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleTextView;
        this.s = linearLayout;
        this.t = numberPicker;
        this.u = numberPicker2;
        this.v = numberPicker3;
        this.w = constraintLayout;
    }
}
