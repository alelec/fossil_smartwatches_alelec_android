package com.fossil;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ha7 implements Map, Serializable, ye7 {
    @DexIgnore
    public static /* final */ ha7 INSTANCE; // = new ha7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 8246714829545688274L;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return false;
    }

    @DexIgnore
    public final /* bridge */ boolean containsValue(Object obj) {
        if (obj instanceof Void) {
            return containsValue((Void) obj);
        }
        return false;
    }

    @DexIgnore
    public boolean containsValue(Void r2) {
        ee7.b(r2, "value");
        return false;
    }

    @DexIgnore
    @Override // java.util.Map
    public final /* bridge */ Set<Map.Entry> entrySet() {
        return getEntries();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof Map) && ((Map) obj).isEmpty();
    }

    @DexIgnore
    @Override // java.util.Map
    public final /* bridge */ Object get(Object obj) {
        return get(obj);
    }

    @DexIgnore
    @Override // java.util.Map
    public Void get(Object obj) {
        return null;
    }

    @DexIgnore
    public Set<Map.Entry> getEntries() {
        return ia7.INSTANCE;
    }

    @DexIgnore
    public Set<Object> getKeys() {
        return ia7.INSTANCE;
    }

    @DexIgnore
    public int getSize() {
        return 0;
    }

    @DexIgnore
    public Collection getValues() {
        return ga7.INSTANCE;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public boolean isEmpty() {
        return true;
    }

    @DexIgnore
    @Override // java.util.Map
    public final /* bridge */ Set<Object> keySet() {
        return getKeys();
    }

    @DexIgnore
    @Override // java.util.Map
    public /* synthetic */ Object put(Object obj, Object obj2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public Void put(Object obj, Void r2) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Map
    public void putAll(Map map) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    @Override // java.util.Map
    public Object remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return getSize();
    }

    @DexIgnore
    public String toString() {
        return "{}";
    }

    @DexIgnore
    @Override // java.util.Map
    public final /* bridge */ Collection values() {
        return getValues();
    }
}
