package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g32 implements j12<Status> {
    @DexIgnore
    public /* final */ /* synthetic */ e22 a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ a12 c;
    @DexIgnore
    public /* final */ /* synthetic */ c32 d;

    @DexIgnore
    public g32(c32 c32, e22 e22, boolean z, a12 a12) {
        this.d = c32;
        this.a = e22;
        this.b = z;
        this.c = a12;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.i12] */
    @Override // com.fossil.j12
    public final /* synthetic */ void a(Status status) {
        Status status2 = status;
        xy1.a(this.d.g).e();
        if (status2.y() && this.d.g()) {
            this.d.k();
        }
        this.a.a((i12) status2);
        if (this.b) {
            this.c.d();
        }
    }
}
