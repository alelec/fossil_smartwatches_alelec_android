package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum pa0 {
    DIAGNOSTICS("diagnosticsApp"),
    WELLNESS("wellnessApp"),
    WORKOUT("workoutApp"),
    MUSIC("musicApp"),
    NOTIFICATIONS_PANEL("notificationsPanelApp"),
    EMPTY("empty"),
    STOP_WATCH("stopwatchApp"),
    ASSISTANT("assistantApp"),
    TIMER("timerApp"),
    WEATHER("weatherApp"),
    COMMUTE("commuteApp"),
    BUDDY_CHALLENGE("buddyChallengeApp");
    
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ String a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final pa0 a(Object obj) {
            pa0 pa0;
            pa0[] values = pa0.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    pa0 = null;
                    break;
                }
                pa0 = values[i];
                if (ee7.a(pa0.a(), obj)) {
                    break;
                }
                i++;
            }
            return pa0 != null ? pa0 : pa0.EMPTY;
        }
    }

    @DexIgnore
    public pa0(String str) {
        this.a = str;
    }

    @DexIgnore
    public final Object a() {
        Object obj;
        if (g41.a[ordinal()] != 1) {
            obj = this.a;
        } else {
            obj = JSONObject.NULL;
        }
        ee7.a(obj, "when (this) {\n          \u2026 -> rawName\n            }");
        return obj;
    }
}
