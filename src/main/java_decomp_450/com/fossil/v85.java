package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v85 extends u85 {
    @DexIgnore
    public static /* final */ SparseIntArray A;
    @DexIgnore
    public static /* final */ ViewDataBinding.i z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        A = sparseIntArray;
        sparseIntArray.put(2131363167, 1);
        A.put(2131362049, 2);
        A.put(2131362727, 3);
        A.put(2131362250, 4);
        A.put(2131363254, 5);
        A.put(2131363304, 6);
        A.put(2131361916, 7);
    }
    */

    @DexIgnore
    public v85(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 8, z, A));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public v85(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (View) objArr[7], (ConstraintLayout) objArr[2], (ConstraintLayout) objArr[0], (FlexibleButton) objArr[4], (ImageView) objArr[3], (View) objArr[1], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[6]);
        this.y = -1;
        ((u85) this).s.setTag(null);
        a(view);
        f();
    }
}
