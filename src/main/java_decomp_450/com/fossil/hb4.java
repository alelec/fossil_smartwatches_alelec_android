package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hb4 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.MINUTES.toMillis(1);
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static ao3 c;

    @DexIgnore
    public static void a(Context context) {
        if (c == null) {
            ao3 ao3 = new ao3(context, 1, "wake:com.google.firebase.iid.WakeLockHolder");
            c = ao3;
            ao3.a(true);
        }
    }

    @DexIgnore
    public static boolean b(Intent intent) {
        return intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
    }

    @DexIgnore
    public static ComponentName a(Context context, Intent intent) {
        synchronized (b) {
            a(context);
            boolean b2 = b(intent);
            a(intent, true);
            ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            if (!b2) {
                c.a(a);
            }
            return startService;
        }
    }

    @DexIgnore
    public static void a(Intent intent, boolean z) {
        intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", z);
    }

    @DexIgnore
    public static void a(Intent intent) {
        synchronized (b) {
            if (c != null && b(intent)) {
                a(intent, false);
                c.a();
            }
        }
    }
}
