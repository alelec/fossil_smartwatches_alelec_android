package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yt2<E> implements Iterator<E> {
    @DexIgnore
    @Deprecated
    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
