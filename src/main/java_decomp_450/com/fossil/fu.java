package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Xml;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fu {
    @DexIgnore
    public static final Drawable a(Context context, int i) {
        ee7.b(context, "$this$getDrawableCompat");
        Drawable c = t0.c(context, i);
        if (c != null) {
            return c;
        }
        throw new IllegalStateException(("Invalid resource ID: " + i).toString());
    }

    @DexIgnore
    @SuppressLint({"ResourceType"})
    public static final Drawable a(Context context, Resources resources, int i) {
        String name;
        ee7.b(context, "$this$getXmlDrawableCompat");
        ee7.b(resources, "resources");
        XmlResourceParser xml = resources.getXml(i);
        ee7.a((Object) xml, "resources.getXml(resId)");
        int next = xml.next();
        while (next != 2 && next != 1) {
            next = xml.next();
        }
        if (next == 2) {
            if (Build.VERSION.SDK_INT < 24 && (name = xml.getName()) != null) {
                int hashCode = name.hashCode();
                if (hashCode != -820387517) {
                    if (hashCode == 2118620333 && name.equals("animated-vector")) {
                        fl a = fl.a(context, resources, xml, Xml.asAttributeSet(xml), context.getTheme());
                        ee7.a((Object) a, "AnimatedVectorDrawableCo\u2026es, parser, attrs, theme)");
                        return a;
                    }
                } else if (name.equals("vector")) {
                    ll createFromXmlInner = ll.createFromXmlInner(resources, (XmlPullParser) xml, Xml.asAttributeSet(xml), context.getTheme());
                    ee7.a((Object) createFromXmlInner, "VectorDrawableCompat.cre\u2026es, parser, attrs, theme)");
                    return createFromXmlInner;
                }
            }
            return iu.a(resources, i, context.getTheme());
        }
        throw new XmlPullParserException("No start tag found.");
    }

    @DexIgnore
    public static final Lifecycle a(Context context) {
        ee7.b(context, "$this$getLifecycle");
        while (!(context instanceof LifecycleOwner)) {
            if (!(context instanceof ContextWrapper)) {
                return null;
            }
            context = ((ContextWrapper) context).getBaseContext();
            ee7.a((Object) context, "context.baseContext");
        }
        return ((LifecycleOwner) context).getLifecycle();
    }
}
