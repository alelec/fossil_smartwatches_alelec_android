package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up0 {
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;

    @DexIgnore
    public up0(int i, long j) {
        this.a = i;
        this.b = j;
    }

    @DexIgnore
    public final up0 a(int i, long j) {
        return new up0(i, j);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof up0)) {
            return false;
        }
        up0 up0 = (up0) obj;
        return this.a == up0.a && this.b == up0.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return (this.a * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = yh0.b("ExponentBackOffInfo(currentExponent=");
        b2.append(this.a);
        b2.append(", maxRate=");
        b2.append(this.b);
        b2.append(")");
        return b2.toString();
    }
}
