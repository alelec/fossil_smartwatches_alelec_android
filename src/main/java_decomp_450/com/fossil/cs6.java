package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cs6 {
    @DexIgnore
    public static /* final */ String d; // = d;
    @DexIgnore
    public /* final */ b a; // = new b(this);
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ cr6 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ cs6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(cs6 cs6) {
            this.a = cs6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = bq6.r.a();
            local.d(a2, "otaProgressReceiver - progress=" + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial()) && mh7.b(otaEvent.getSerial(), this.a.c(), true)) {
                this.a.b().c((int) (otaEvent.getProcess() * ((float) 10)));
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public cs6(String str, cr6 cr6) {
        ee7.b(str, "serial");
        ee7.b(cr6, "mView");
        this.b = str;
        this.c = cr6;
    }

    @DexIgnore
    public final void a() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        Explore explore2 = new Explore();
        Explore explore3 = new Explore();
        Explore explore4 = new Explore();
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.b);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = d;
        local.d(str, "serial=" + this.b + ", mCurrentDeviceType=" + deviceBySerial);
        if (be5.o.a(deviceBySerial)) {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886862));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886863));
            explore2.setBackground(2131231332);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886860));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886861));
            explore4.setBackground(2131231331);
        } else {
            explore.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886869));
            explore.setBackground(2131231334);
            explore2.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886868));
            explore2.setBackground(2131231336);
            explore3.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886866));
            explore3.setBackground(2131231335);
            explore4.setDescription(ig5.a(PortfolioApp.g0.c(), 2131886867));
            explore4.setBackground(2131231333);
        }
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        arrayList.add(explore4);
        this.c.d(arrayList);
    }

    @DexIgnore
    public final cr6 b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final void d() {
        PortfolioApp c2 = PortfolioApp.g0.c();
        b bVar = this.a;
        c2.registerReceiver(bVar, new IntentFilter(PortfolioApp.g0.c().getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        a();
        this.c.e();
    }

    @DexIgnore
    public final void e() {
        PortfolioApp.g0.c().unregisterReceiver(this.a);
    }

    @DexIgnore
    public final void f() {
        if (!DeviceIdentityUtils.isDianaDevice(this.b)) {
            this.c.g1();
        }
    }
}
