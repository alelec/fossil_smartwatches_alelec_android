package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ao4 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public String c;
    @DexIgnore
    public Date d;
    @DexIgnore
    public pn4 e;
    @DexIgnore
    public io4 f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public int h;
    @DexIgnore
    public Date i;

    @DexIgnore
    public ao4(String str, String str2, String str3, Date date, pn4 pn4, io4 io4, boolean z, int i2, Date date2) {
        ee7.b(str, "id");
        ee7.b(str2, "titleKey");
        ee7.b(str3, "bodyKey");
        ee7.b(date, "createdAt");
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = date;
        this.e = pn4;
        this.f = io4;
        this.g = z;
        this.h = i2;
        this.i = date2;
    }

    @DexIgnore
    public final String a() {
        return this.c;
    }

    @DexIgnore
    public final pn4 b() {
        return this.e;
    }

    @DexIgnore
    public final boolean c() {
        return this.g;
    }

    @DexIgnore
    public final Date d() {
        return this.d;
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ao4)) {
            return false;
        }
        ao4 ao4 = (ao4) obj;
        return ee7.a(this.a, ao4.a) && ee7.a(this.b, ao4.b) && ee7.a(this.c, ao4.c) && ee7.a(this.d, ao4.d) && ee7.a(this.e, ao4.e) && ee7.a(this.f, ao4.f) && this.g == ao4.g && this.h == ao4.h && ee7.a(this.i, ao4.i);
    }

    @DexIgnore
    public final io4 f() {
        return this.f;
    }

    @DexIgnore
    public final int g() {
        return this.h;
    }

    @DexIgnore
    public final Date h() {
        return this.i;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.c;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        Date date = this.d;
        int hashCode4 = (hashCode3 + (date != null ? date.hashCode() : 0)) * 31;
        pn4 pn4 = this.e;
        int hashCode5 = (hashCode4 + (pn4 != null ? pn4.hashCode() : 0)) * 31;
        io4 io4 = this.f;
        int hashCode6 = (hashCode5 + (io4 != null ? io4.hashCode() : 0)) * 31;
        boolean z = this.g;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (((hashCode6 + i3) * 31) + this.h) * 31;
        Date date2 = this.i;
        if (date2 != null) {
            i2 = date2.hashCode();
        }
        return i5 + i2;
    }

    @DexIgnore
    public final String i() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "Notification(id=" + this.a + ", titleKey=" + this.b + ", bodyKey=" + this.c + ", createdAt=" + this.d + ", challengeData=" + this.e + ", profileData=" + this.f + ", confirmChallenge=" + this.g + ", rank=" + this.h + ", reachGoalAt=" + this.i + ")";
    }
}
