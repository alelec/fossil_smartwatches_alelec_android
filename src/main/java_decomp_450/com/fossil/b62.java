package com.fossil;

import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b62 {
    @DexIgnore
    public /* final */ DataHolder a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public b62(DataHolder dataHolder, int i) {
        a72.a(dataHolder);
        this.a = dataHolder;
        a(i);
    }

    @DexIgnore
    public final void a(int i) {
        a72.b(i >= 0 && i < this.a.getCount());
        this.b = i;
        this.c = this.a.a(i);
    }

    @DexIgnore
    public int b(String str) {
        return this.a.b(str, this.b, this.c);
    }

    @DexIgnore
    public String c(String str) {
        return this.a.c(str, this.b, this.c);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof b62) {
            b62 b62 = (b62) obj;
            if (!y62.a(Integer.valueOf(b62.b), Integer.valueOf(this.b)) || !y62.a(Integer.valueOf(b62.c), Integer.valueOf(this.c)) || b62.a != this.a) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(Integer.valueOf(this.b), Integer.valueOf(this.c), this.a);
    }

    @DexIgnore
    public byte[] a(String str) {
        return this.a.a(str, this.b, this.c);
    }
}
