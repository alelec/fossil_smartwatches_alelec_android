package com.fossil;

import androidx.work.impl.WorkDatabase;
import com.fossil.qm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class np implements Runnable {
    @DexIgnore
    public static /* final */ String d; // = im.a("StopWorkRunnable");
    @DexIgnore
    public /* final */ dn a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public np(dn dnVar, String str, boolean z) {
        this.a = dnVar;
        this.b = str;
        this.c = z;
    }

    @DexIgnore
    public void run() {
        boolean z;
        WorkDatabase f = this.a.f();
        xm d2 = this.a.d();
        ap f2 = f.f();
        f.beginTransaction();
        try {
            boolean d3 = d2.d(this.b);
            if (this.c) {
                z = this.a.d().g(this.b);
            } else {
                if (!d3 && f2.d(this.b) == qm.a.RUNNING) {
                    f2.a(qm.a.ENQUEUED, this.b);
                }
                z = this.a.d().h(this.b);
            }
            im.a().a(d, String.format("StopWorkRunnable for %s; Processor.stopWork = %s", this.b, Boolean.valueOf(z)), new Throwable[0]);
            f.setTransactionSuccessful();
        } finally {
            f.endTransaction();
        }
    }
}
