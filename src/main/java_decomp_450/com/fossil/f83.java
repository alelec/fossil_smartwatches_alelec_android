package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f83 extends tm2 implements b73 {
    @DexIgnore
    public f83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void a(z73 z73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z73);
        b(9, zza);
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final ab2 getView() throws RemoteException {
        Parcel a = a(8, zza());
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bundle);
        b(2, zza);
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onDestroy() throws RemoteException {
        b(5, zza());
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onLowMemory() throws RemoteException {
        b(6, zza());
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onPause() throws RemoteException {
        b(4, zza());
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onResume() throws RemoteException {
        b(3, zza());
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bundle);
        Parcel a = a(7, zza);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onStart() throws RemoteException {
        b(10, zza());
    }

    @DexIgnore
    @Override // com.fossil.b73
    public final void onStop() throws RemoteException {
        b(11, zza());
    }
}
