package com.fossil;

import com.fossil.hc4;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gc4 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ l14 b;

    @DexIgnore
    public enum a {
        ATTEMPT_MIGRATION,
        NOT_GENERATED,
        UNREGISTERED,
        REGISTERED,
        REGISTER_ERROR
    }

    @DexIgnore
    public gc4(l14 l14) {
        File filesDir = l14.b().getFilesDir();
        this.a = new File(filesDir, "PersistedInstallation." + l14.e() + ".json");
        this.b = l14;
    }

    @DexIgnore
    public final JSONObject a() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[16384];
        FileInputStream fileInputStream = new FileInputStream(this.a);
        while (true) {
            try {
                int read = fileInputStream.read(bArr, 0, 16384);
                if (read < 0) {
                    JSONObject jSONObject = new JSONObject(byteArrayOutputStream.toString());
                    try {
                        fileInputStream.close();
                        return jSONObject;
                    } catch (IOException | JSONException unused) {
                        return new JSONObject();
                    }
                } else {
                    byteArrayOutputStream.write(bArr, 0, read);
                }
            } catch (Throwable unused2) {
            }
        }
        throw th;
    }

    @DexIgnore
    public hc4 b() {
        JSONObject a2 = a();
        String optString = a2.optString("Fid", null);
        int optInt = a2.optInt("Status", a.ATTEMPT_MIGRATION.ordinal());
        String optString2 = a2.optString("AuthToken", null);
        String optString3 = a2.optString("RefreshToken", null);
        long optLong = a2.optLong("TokenCreationEpochInSecs", 0);
        long optLong2 = a2.optLong("ExpiresInSecs", 0);
        String optString4 = a2.optString("FisError", null);
        hc4.a p = hc4.p();
        p.b(optString);
        p.a(a.values()[optInt]);
        p.a(optString2);
        p.d(optString3);
        p.b(optLong);
        p.a(optLong2);
        p.c(optString4);
        return p.a();
    }

    @DexIgnore
    public hc4 a(hc4 hc4) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("Fid", hc4.c());
            jSONObject.put("Status", hc4.f().ordinal());
            jSONObject.put("AuthToken", hc4.a());
            jSONObject.put("RefreshToken", hc4.e());
            jSONObject.put("TokenCreationEpochInSecs", hc4.g());
            jSONObject.put("ExpiresInSecs", hc4.b());
            jSONObject.put("FisError", hc4.d());
            File createTempFile = File.createTempFile("PersistedInstallation", "tmp", this.b.b().getFilesDir());
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            fileOutputStream.write(jSONObject.toString().getBytes("UTF-8"));
            fileOutputStream.close();
            if (createTempFile.renameTo(this.a)) {
                return hc4;
            }
            throw new IOException("unable to rename the tmpfile to PersistedInstallation");
        } catch (IOException | JSONException unused) {
        }
    }
}
