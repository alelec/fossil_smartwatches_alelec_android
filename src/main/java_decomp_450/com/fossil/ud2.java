package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ud2 implements Parcelable.Creator<sd2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sd2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        long j = 0;
        long j2 = 0;
        gc2 gc2 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                gc2 = (gc2) j72.a(parcel, a, gc2.CREATOR);
            } else if (a2 == 2) {
                iBinder = j72.p(parcel, a);
            } else if (a2 == 3) {
                j = j72.s(parcel, a);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                j2 = j72.s(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new sd2(gc2, iBinder, j, j2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ sd2[] newArray(int i) {
        return new sd2[i];
    }
}
