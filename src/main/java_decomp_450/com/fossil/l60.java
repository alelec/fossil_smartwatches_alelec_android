package com.fossil;

import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface l60 extends Parcelable {

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceStateChanged(l60 l60, c cVar, c cVar2);

        @DexIgnore
        void onEventReceived(l60 l60, bb0 bb0);
    }

    @DexIgnore
    public enum c {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        UPGRADING_FIRMWARE,
        DISCONNECTING
    }

    @DexIgnore
    public enum d {
        DISCONNECTED,
        CONNECTING,
        CONNECTED,
        DISCONNECTING;
        
        @DexIgnore
        public static /* final */ ks0 f; // = new ks0(null);
    }

    @DexIgnore
    <T> T a(Class<T> cls);

    @DexIgnore
    void a(b bVar);

    @DexIgnore
    a getBondState();

    @DexIgnore
    c getState();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    ie0<i97> k();

    @DexIgnore
    m60 n();

    @DexIgnore
    public enum a {
        BOND_NONE,
        BONDING,
        BONDED;
        
        @DexIgnore
        public static /* final */ C0109a b; // = new C0109a(null);

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.l60$a$a")
        /* renamed from: com.fossil.l60$a$a  reason: collision with other inner class name */
        public static final class C0109a {
            @DexIgnore
            public /* synthetic */ C0109a(zd7 zd7) {
            }

            @DexIgnore
            public final a a(m01 m01) {
                int i = to0.a[m01.ordinal()];
                if (i == 1) {
                    return a.BOND_NONE;
                }
                if (i == 2) {
                    return a.BONDING;
                }
                if (i == 3) {
                    return a.BONDED;
                }
                throw new p87();
            }

            @DexIgnore
            public final a a(int i) {
                if (i == 11) {
                    return a.BONDING;
                }
                if (i != 12) {
                    return a.BOND_NONE;
                }
                return a.BONDED;
            }
        }
    }
}
