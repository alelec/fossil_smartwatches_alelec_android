package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zk extends fk {
    @DexIgnore
    public static /* final */ String[] a; // = {"android:visibilityPropagation:visibility", "android:visibilityPropagation:center"};

    @DexIgnore
    @Override // com.fossil.fk
    public void a(hk hkVar) {
        View view = hkVar.b;
        Integer num = (Integer) hkVar.a.get("android:visibility:visibility");
        if (num == null) {
            num = Integer.valueOf(view.getVisibility());
        }
        hkVar.a.put("android:visibilityPropagation:visibility", num);
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        iArr[0] = iArr[0] + Math.round(view.getTranslationX());
        iArr[0] = iArr[0] + (view.getWidth() / 2);
        iArr[1] = iArr[1] + Math.round(view.getTranslationY());
        iArr[1] = iArr[1] + (view.getHeight() / 2);
        hkVar.a.put("android:visibilityPropagation:center", iArr);
    }

    @DexIgnore
    public int b(hk hkVar) {
        Integer num;
        if (hkVar == null || (num = (Integer) hkVar.a.get("android:visibilityPropagation:visibility")) == null) {
            return 8;
        }
        return num.intValue();
    }

    @DexIgnore
    public int c(hk hkVar) {
        return a(hkVar, 0);
    }

    @DexIgnore
    public int d(hk hkVar) {
        return a(hkVar, 1);
    }

    @DexIgnore
    @Override // com.fossil.fk
    public String[] a() {
        return a;
    }

    @DexIgnore
    public static int a(hk hkVar, int i) {
        int[] iArr;
        if (hkVar == null || (iArr = (int[]) hkVar.a.get("android:visibilityPropagation:center")) == null) {
            return -1;
        }
        return iArr[i];
    }
}
