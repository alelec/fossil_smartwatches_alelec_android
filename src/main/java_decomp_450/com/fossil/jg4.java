package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jg4 {
    @DexIgnore
    public abstract jg4 a(gg4 gg4) throws IOException;

    @DexIgnore
    public String toString() {
        return kg4.a(this);
    }

    @DexIgnore
    public jg4 clone() throws CloneNotSupportedException {
        return (jg4) super.clone();
    }
}
