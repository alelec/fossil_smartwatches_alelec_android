package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v55 extends u55 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131361905, 1);
        C.put(2131362517, 2);
        C.put(2131363042, 3);
        C.put(2131362121, 4);
        C.put(2131362439, 5);
        C.put(2131362751, 6);
        C.put(2131362367, 7);
        C.put(2131363155, 8);
        C.put(2131363010, 9);
    }
    */

    @DexIgnore
    public v55(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 10, B, C));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public v55(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (RTLImageView) objArr[1], (ImageView) objArr[4], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[2], (View) objArr[6], (ConstraintLayout) objArr[0], (RecyclerViewAlphabetIndex) objArr[9], (FlexibleEditText) objArr[3], (RecyclerView) objArr[8]);
        this.A = -1;
        ((u55) this).w.setTag(null);
        a(view);
        f();
    }
}
