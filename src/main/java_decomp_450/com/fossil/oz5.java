package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oz5 implements MembersInjector<DianaCustomizeEditActivity> {
    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, wz5 wz5) {
        dianaCustomizeEditActivity.y = wz5;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, rj4 rj4) {
        dianaCustomizeEditActivity.z = rj4;
    }
}
