package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.activity.overview.ActivityOverviewFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y86 implements MembersInjector<ActivityOverviewFragment> {
    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, v86 v86) {
        activityOverviewFragment.g = v86;
    }

    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, m96 m96) {
        activityOverviewFragment.h = m96;
    }

    @DexIgnore
    public static void a(ActivityOverviewFragment activityOverviewFragment, g96 g96) {
        activityOverviewFragment.i = g96;
    }
}
