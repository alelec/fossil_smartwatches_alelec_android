package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.jp5;
import com.fossil.zl4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z36 extends go5 implements j46, zl4.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<e75> f;
    @DexIgnore
    public i46 g;
    @DexIgnore
    public jp5 h;
    @DexIgnore
    public zl4 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final z36 a() {
            return new z36();
        }

        @DexIgnore
        public final String b() {
            return z36.p;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ e75 b;

        @DexIgnore
        public b(View view, e75 e75) {
            this.a = view;
            this.b = e75;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.v.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                e75 e75 = this.b;
                ee7.a((Object) e75, "binding");
                e75.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = r06.r.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.b.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 a;
        @DexIgnore
        public /* final */ /* synthetic */ e75 b;

        @DexIgnore
        public c(z36 z36, e75 e75) {
            this.a = z36;
            this.b = e75;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            zl4 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText(null);
                ee7.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = z36.q.b();
                local.i(b2, "Autocomplete item selected: " + ((Object) fullText));
                if (this.a.h != null && !TextUtils.isEmpty(item.getPlaceId())) {
                    this.b.q.setText("");
                    i46 c = z36.c(this.a);
                    String spannableString = fullText.toString();
                    ee7.a((Object) spannableString, "primaryText.toString()");
                    String placeId = item.getPlaceId();
                    ee7.a((Object) placeId, "item.placeId");
                    zl4 a3 = this.a.i;
                    if (a3 != null) {
                        c.a(spannableString, placeId, a3.b());
                        zl4 a4 = this.a.i;
                        if (a4 != null) {
                            a4.a();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ e75 a;

        @DexIgnore
        public d(z36 z36, e75 e75) {
            this.a = e75;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.a.s;
            ee7.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 a;

        @DexIgnore
        public e(z36 z36, e75 e75) {
            this.a = z36;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            ee7.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.e1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements jp5.b {
        @DexIgnore
        public /* final */ /* synthetic */ z36 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(z36 z36) {
            this.a = z36;
        }

        @DexIgnore
        @Override // com.fossil.jp5.b
        public void a(int i, boolean z) {
            z36.c(this.a).a(i, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ z36 a;

        @DexIgnore
        public g(z36 z36) {
            this.a = z36;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.e1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ e75 a;

        @DexIgnore
        public h(e75 e75) {
            this.a = e75;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = z36.class.getSimpleName();
        ee7.a((Object) simpleName, "WeatherSettingFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ i46 c(z36 z36) {
        i46 i46 = z36.g;
        if (i46 != null) {
            return i46;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j46
    public void D0() {
        FLogger.INSTANCE.getLocal().d(p, "showLocationError");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.t(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.j46
    public void S() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.u(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        qw6<e75> qw6 = this.f;
        if (qw6 == null) {
            ee7.d("mBinding");
            throw null;
        } else if (qw6.a() == null || this.h == null) {
            return true;
        } else {
            i46 i46 = this.g;
            if (i46 != null) {
                i46.i();
                return true;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<e75> qw6 = this.f;
        if (qw6 != null) {
            e75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                ee7.a((Object) flexibleTextView, "it.ftvLocationError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.y;
                ee7.a((Object) flexibleTextView2, "it.tvAdded");
                flexibleTextView2.setVisibility(0);
                RecyclerView recyclerView = a2.w;
                ee7.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        qw6<e75> qw6 = this.f;
        if (qw6 != null) {
            e75 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.t;
                ee7.a((Object) flexibleTextView, "it.ftvLocationError");
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886760);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                String format = String.format(a3, Arrays.copyOf(new Object[]{flexibleAutoCompleteTextView.getText()}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView.setText(format);
                FlexibleTextView flexibleTextView2 = a2.t;
                ee7.a((Object) flexibleTextView2, "it.ftvLocationError");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.y;
                ee7.a((Object) flexibleTextView3, "it.tvAdded");
                flexibleTextView3.setVisibility(8);
                RecyclerView recyclerView = a2.w;
                ee7.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.j46
    public void h(boolean z) {
        if (z) {
            Intent intent = new Intent();
            i46 i46 = this.g;
            if (i46 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", i46.h());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    @Override // com.fossil.j46
    public void i(List<WeatherLocationWrapper> list) {
        ee7.b(list, "locations");
        jp5 jp5 = this.h;
        if (jp5 != null) {
            jp5.a(xe7.b(list));
        }
    }

    @DexIgnore
    @Override // com.fossil.zl4.b
    public void m(boolean z) {
        qw6<e75> qw6 = this.f;
        if (qw6 != null) {
            e75 a2 = qw6.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        g1();
                        return;
                    }
                }
                f1();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        e75 e75 = (e75) qb.a(layoutInflater, 2131558638, viewGroup, false, a1());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = e75.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(v6.c(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new c(this, e75));
        flexibleAutoCompleteTextView.addTextChangedListener(new d(this, e75));
        flexibleAutoCompleteTextView.setOnKeyListener(new e(this, e75));
        jp5 jp5 = new jp5();
        this.h = jp5;
        jp5.a(new f(this));
        RecyclerView recyclerView = e75.w;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        ee7.a((Object) e75, "binding");
        View d2 = e75.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, e75));
        e75.r.setOnClickListener(new g(this));
        e75.s.setOnClickListener(new h(e75));
        this.f = new qw6<>(this, e75);
        return e75.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        i46 i46 = this.g;
        if (i46 != null) {
            i46.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        i46 i46 = this.g;
        if (i46 != null) {
            i46.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(i46 i46) {
        ee7.b(i46, "presenter");
        this.g = i46;
    }

    @DexIgnore
    @Override // com.fossil.j46
    public void a(PlacesClient placesClient) {
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView;
        if (isActive()) {
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            zl4 zl4 = new zl4(requireContext, placesClient);
            this.i = zl4;
            if (zl4 != null) {
                zl4.a(this);
            }
            qw6<e75> qw6 = this.f;
            if (qw6 != null) {
                e75 a2 = qw6.a();
                if (a2 != null && (flexibleAutoCompleteTextView = a2.q) != null) {
                    flexibleAutoCompleteTextView.setAdapter(this.i);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
