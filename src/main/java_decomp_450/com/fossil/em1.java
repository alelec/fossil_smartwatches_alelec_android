package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em1 implements Parcelable.Creator<do1> {
    @DexIgnore
    public /* synthetic */ em1(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public do1 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            ee7.a((Object) readString, "parcel.readString()!!");
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray == null) {
                createByteArray = new byte[0];
            }
            return new do1(readString, createByteArray);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public do1[] newArray(int i) {
        return new do1[i];
    }
}
