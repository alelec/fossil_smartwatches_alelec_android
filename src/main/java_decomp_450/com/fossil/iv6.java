package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.fl4;
import com.fossil.pu6;
import com.fossil.rm5;
import com.fossil.tm5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wm5;
import com.fossil.xg5;
import com.fossil.xm5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.service.MFDeviceService;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv6 extends ev6 {
    @DexIgnore
    public static /* final */ long v; // = TimeUnit.SECONDS.toMillis(1);
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<pu6.d> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ LiveData<pu6.d> g;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Handler i;
    @DexIgnore
    public /* final */ Runnable j;
    @DexIgnore
    public /* final */ f k;
    @DexIgnore
    public /* final */ e l;
    @DexIgnore
    public /* final */ qe m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ ch5 o;
    @DexIgnore
    public /* final */ fv6 p;
    @DexIgnore
    public /* final */ tm5 q;
    @DexIgnore
    public /* final */ wm5 r;
    @DexIgnore
    public /* final */ rm5 s;
    @DexIgnore
    public /* final */ xm5 t;
    @DexIgnore
    public /* final */ PortfolioApp u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<tm5.d, tm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        public b(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(tm5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onSuccess");
            this.a.a(dVar.a());
        }

        @DexIgnore
        public void a(tm5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation onError");
            this.a.p.X0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<rm5.d, rm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public c(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rm5.d dVar) {
            ee7.b(dVar, "responseValue");
            String a2 = dVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "GetCityName onSuccess - address: " + a2);
            this.a.p.v(a2);
        }

        @DexIgnore
        public void a(rm5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName onError");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<wm5.d, wm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(wm5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onSuccess");
            if (ee7.a((Object) this.a.k(), (Object) dVar.a().getDeviceSerial())) {
                this.a.a(dVar.a());
            }
        }

        @DexIgnore
        public void a(wm5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation onError");
            this.a.h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            DeviceLocation deviceLocation = (DeviceLocation) intent.getSerializableExtra("device_location");
            String stringExtra = intent.getStringExtra("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDevicePresenter", "onReceive - location: " + deviceLocation + ",serial: " + stringExtra);
            if (ee7.a((Object) stringExtra, (Object) this.a.k()) && deviceLocation != null) {
                this.a.a(deviceLocation);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            if (ee7.a((Object) stringExtra, (Object) this.a.k()) && ee7.a((Object) stringExtra, (Object) this.a.i())) {
                this.a.j().a(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1", f = "FindDevicePresenter.kt", l = {66, 69}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $serial;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.iv6$g$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.watchsetting.finddevice.FindDevicePresenter$mDeviceWrapperTransformation$1$1$deviceModel$1", f = "FindDevicePresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.iv6$g$a$a  reason: collision with other inner class name */
            public static final class C0083a extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0083a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0083a aVar = new C0083a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
                    return ((C0083a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        DeviceRepository b = this.this$0.this$0.a.n;
                        String str = this.this$0.$serial;
                        ee7.a((Object) str, "serial");
                        return b.getDeviceBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(fb7 fb7, a aVar) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    b bVar = new b(fb7, this.this$0);
                    bVar.p$ = (yi7) obj;
                    return bVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                    return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        DeviceRepository b = this.this$0.this$0.a.n;
                        String str = this.this$0.$serial;
                        ee7.a((Object) str, "serial");
                        return b.getDeviceNameBySerial(str);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$serial = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$serial, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
            /* JADX WARNING: Removed duplicated region for block: B:30:0x00b9  */
            @Override // com.fossil.ob7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                    r14 = this;
                    java.lang.Object r0 = com.fossil.nb7.a()
                    int r1 = r14.label
                    r2 = 0
                    r3 = 0
                    r4 = 2
                    r5 = 1
                    if (r1 == 0) goto L_0x0031
                    if (r1 == r5) goto L_0x0029
                    if (r1 != r4) goto L_0x0021
                    java.lang.Object r0 = r14.L$2
                    com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
                    java.lang.Object r1 = r14.L$1
                    com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
                    java.lang.Object r1 = r14.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r15)
                    r7 = r0
                    goto L_0x0070
                L_0x0021:
                    java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                    java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                    r15.<init>(r0)
                    throw r15
                L_0x0029:
                    java.lang.Object r1 = r14.L$0
                    com.fossil.yi7 r1 = (com.fossil.yi7) r1
                    com.fossil.t87.a(r15)
                    goto L_0x004e
                L_0x0031:
                    com.fossil.t87.a(r15)
                    com.fossil.yi7 r1 = r14.p$
                    com.fossil.iv6$g r15 = r14.this$0
                    com.fossil.iv6 r15 = r15.a
                    com.fossil.ti7 r15 = r15.c()
                    com.fossil.iv6$g$a$a r6 = new com.fossil.iv6$g$a$a
                    r6.<init>(r14, r3)
                    r14.L$0 = r1
                    r14.label = r5
                    java.lang.Object r15 = com.fossil.vh7.a(r15, r6, r14)
                    if (r15 != r0) goto L_0x004e
                    return r0
                L_0x004e:
                    com.portfolio.platform.data.model.Device r15 = (com.portfolio.platform.data.model.Device) r15
                    if (r15 == 0) goto L_0x00d0
                    com.fossil.iv6$g r6 = r14.this$0
                    com.fossil.iv6 r6 = r6.a
                    com.fossil.ti7 r6 = r6.c()
                    com.fossil.iv6$g$a$b r7 = new com.fossil.iv6$g$a$b
                    r7.<init>(r3, r14)
                    r14.L$0 = r1
                    r14.L$1 = r15
                    r14.L$2 = r15
                    r14.label = r4
                    java.lang.Object r1 = com.fossil.vh7.a(r6, r7, r14)
                    if (r1 != r0) goto L_0x006e
                    return r0
                L_0x006e:
                    r7 = r15
                    r15 = r1
                L_0x0070:
                    r8 = r15
                    java.lang.String r8 = (java.lang.String) r8
                    java.lang.String r15 = r14.$serial
                    com.fossil.iv6$g r0 = r14.this$0
                    com.fossil.iv6 r0 = r0.a
                    java.lang.String r0 = r0.i()
                    boolean r15 = com.fossil.ee7.a(r15, r0)
                    if (r15 == 0) goto L_0x00b9
                    com.portfolio.platform.PortfolioApp$a r15 = com.portfolio.platform.PortfolioApp.g0
                    com.misfit.frameworks.buttonservice.IButtonConnectivity r15 = r15.b()
                    if (r15 == 0) goto L_0x0099
                    java.lang.String r0 = r14.$serial
                    int r15 = r15.getGattState(r0)
                    if (r15 != r4) goto L_0x0094
                    goto L_0x0095
                L_0x0094:
                    r5 = 0
                L_0x0095:
                    java.lang.Boolean r3 = com.fossil.pb7.a(r5)
                L_0x0099:
                    com.fossil.iv6$g r15 = r14.this$0
                    com.fossil.iv6 r15 = r15.a
                    androidx.lifecycle.MutableLiveData r15 = r15.f
                    com.fossil.pu6$d r0 = new com.fossil.pu6$d
                    if (r3 == 0) goto L_0x00ab
                    boolean r1 = r3.booleanValue()
                    r9 = r1
                    goto L_0x00ac
                L_0x00ab:
                    r9 = 0
                L_0x00ac:
                    r10 = 1
                    r11 = 0
                    r12 = 16
                    r13 = 0
                    r6 = r0
                    r6.<init>(r7, r8, r9, r10, r11, r12, r13)
                    r15.a(r0)
                    goto L_0x00d0
                L_0x00b9:
                    com.fossil.iv6$g r15 = r14.this$0
                    com.fossil.iv6 r15 = r15.a
                    androidx.lifecycle.MutableLiveData r15 = r15.f
                    com.fossil.pu6$d r0 = new com.fossil.pu6$d
                    r9 = 0
                    r10 = 0
                    r11 = 0
                    r12 = 16
                    r13 = 0
                    r6 = r0
                    r6.<init>(r7, r8, r9, r10, r11, r12, r13)
                    r15.a(r0)
                L_0x00d0:
                    com.fossil.iv6$g r15 = r14.this$0
                    com.fossil.iv6 r15 = r15.a
                    com.fossil.ch5 r15 = r15.o
                    boolean r15 = r15.K()
                    com.fossil.iv6$g r0 = r14.this$0
                    com.fossil.iv6 r0 = r0.a
                    com.fossil.fv6 r0 = r0.p
                    r0.a(r15, r2)
                    com.fossil.i97 r15 = com.fossil.i97.a
                    return r15
                */
                throw new UnsupportedOperationException("Method not decompiled: com.fossil.iv6.g.a.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public g(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<pu6.d> apply(String str) {
            this.a.j.run();
            ik7 unused = xh7.b(this.a.e(), null, null, new a(this, str, null), 3, null);
            return this.a.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements fl4.e<xm5.e, xm5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ h a;

            @DexIgnore
            public a(h hVar) {
                this.a = hVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(xm5.e eVar) {
                ee7.b(eVar, "responseValue");
                int a2 = eVar.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("FindDevicePresenter", "getRssi onSuccess - rssi: " + a2);
                this.a.a.a(a2);
                this.a.a.b(a2);
            }

            @DexIgnore
            public void a(xm5.b bVar) {
                ee7.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "getRssi onError");
                this.a.a.a(-9999);
                this.a.a.b(-9999);
            }
        }

        @DexIgnore
        public h(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        public final void run() {
            String k = this.a.k();
            if (k != null) {
                this.a.t.a(new xm5.d(k), new a(this));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<pu6.d> {
        @DexIgnore
        public /* final */ /* synthetic */ iv6 a;

        @DexIgnore
        public i(iv6 iv6) {
            this.a = iv6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(pu6.d dVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = pu6.z.a();
            local.d(a2, "observer device " + dVar);
            if (dVar != null) {
                this.a.p.a(dVar);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public iv6(qe qeVar, DeviceRepository deviceRepository, ch5 ch5, fv6 fv6, tm5 tm5, wm5 wm5, rm5 rm5, xm5 xm5, PortfolioApp portfolioApp) {
        ee7.b(qeVar, "mLocalBroadcastManager");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(fv6, "mView");
        ee7.b(tm5, "mGetLocation");
        ee7.b(wm5, "mLoadLocation");
        ee7.b(rm5, "mGetAddress");
        ee7.b(xm5, "mGetRssi");
        ee7.b(portfolioApp, "mApp");
        this.m = qeVar;
        this.n = deviceRepository;
        this.o = ch5;
        this.p = fv6;
        this.q = tm5;
        this.r = wm5;
        this.s = rm5;
        this.t = xm5;
        this.u = portfolioApp;
        LiveData<pu6.d> b2 = ge.b(this.e, new g(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026viceWrapperLiveData\n    }");
        this.g = b2;
        this.i = new Handler();
        this.j = new h(this);
        this.k = new f(this);
        this.l = new e(this);
    }

    @DexIgnore
    public final String i() {
        return PortfolioApp.g0.c().c();
    }

    @DexIgnore
    public final MutableLiveData<String> j() {
        return this.e;
    }

    @DexIgnore
    public String k() {
        return this.e.a();
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "loadLocation");
        this.r.a(new wm5.b(), new d(this));
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "locateOnMap");
        if (!NetworkUtils.isNetworkAvailable(this.u)) {
            this.p.a(601, (String) null);
            return;
        }
        xg5 xg5 = xg5.b;
        fv6 fv6 = this.p;
        if (fv6 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment");
        } else if (!xg5.a(xg5, ((lu6) fv6).getContext(), xg5.a.FIND_DEVICE, false, false, false, (Integer) null, 60, (Object) null)) {
            this.p.M();
        } else {
            String k2 = k();
            if (k2 != null) {
                if (TextUtils.equals(k2, i()) && a(k2)) {
                    Context applicationContext = this.u.getApplicationContext();
                    ee7.a((Object) applicationContext, "mApp.applicationContext");
                    if (a(applicationContext)) {
                        l();
                        return;
                    }
                }
                h();
            }
        }
    }

    @DexIgnore
    public void n() {
        this.p.a(this);
    }

    @DexIgnore
    public final void a(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public void b(String str) {
        ee7.b(str, "serial");
        this.e.a(str);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.m.a(this.l, new IntentFilter(MFDeviceService.Z.a()));
        PortfolioApp portfolioApp = this.u;
        f fVar = this.k;
        portfolioApp.registerReceiver(fVar, new IntentFilter(this.u.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        LiveData<pu6.d> liveData = this.g;
        fv6 fv6 = this.p;
        if (fv6 != null) {
            liveData.a((go5) fv6, new i(this));
            this.t.f();
            if (ee7.a((Object) this.u.c(), (Object) this.e.a())) {
                FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "update RSSI only for active device");
                b(this.h);
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        try {
            MutableLiveData<pu6.d> mutableLiveData = this.f;
            fv6 fv6 = this.p;
            if (fv6 != null) {
                mutableLiveData.a((go5) fv6);
                this.e.a((LifecycleOwner) this.p);
                this.g.a((LifecycleOwner) this.p);
                this.m.a(this.l);
                this.u.unregisterReceiver(this.k);
                this.t.g();
                this.i.removeCallbacksAndMessages(null);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("FindDevicePresenter", "stop with " + e2);
        }
    }

    @DexIgnore
    public final void h() {
        FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetLocation");
        String k2 = k();
        if (k2 != null) {
            this.q.a(new tm5.b(k2), new b(this));
        }
    }

    @DexIgnore
    public final void a(DeviceLocation deviceLocation) {
        ee7.b(deviceLocation, PlaceFields.LOCATION);
        this.p.a(deviceLocation.getTimeStamp());
        double latitude = deviceLocation.getLatitude();
        double longitude = deviceLocation.getLongitude();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "handleLocation latitude=" + latitude + ", longitude=" + longitude);
        if (latitude != 0.0d && longitude != 0.0d) {
            this.p.a(Double.valueOf(latitude), Double.valueOf(longitude));
            FLogger.INSTANCE.getLocal().d("FindDevicePresenter", "GetCityName");
            this.s.a(new rm5.b(latitude, longitude), new c(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.ev6
    public void b(boolean z) {
        this.o.f(z);
    }

    @DexIgnore
    public final void b(int i2) {
        if (i2 != -9999) {
            this.p.g(i2);
        }
        this.i.removeCallbacks(this.j);
        this.i.postDelayed(this.j, v);
    }

    @DexIgnore
    @Override // com.fossil.ev6
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("FindDevicePresenter", "enableLocate: enable = " + z);
        b(z);
        this.p.a(z, this.o.L());
        if (z) {
            m();
        }
    }

    @DexIgnore
    public final boolean a(String str) {
        IButtonConnectivity b2 = PortfolioApp.g0.b();
        if (b2 == null || b2.getGattState(str) != 2) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean a(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0;
    }
}
