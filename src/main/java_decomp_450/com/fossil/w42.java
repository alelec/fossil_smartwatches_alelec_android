package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.u12;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w42<ResultT> extends s42 {
    @DexIgnore
    public /* final */ f22<v02.b, ResultT> b;
    @DexIgnore
    public /* final */ oo3<ResultT> c;
    @DexIgnore
    public /* final */ d22 d;

    @DexIgnore
    public w42(int i, f22<v02.b, ResultT> f22, oo3<ResultT> oo3, d22 d22) {
        super(i);
        this.c = oo3;
        this.b = f22;
        this.d = d22;
        if (i == 2 && f22.a()) {
            throw new IllegalArgumentException("Best-effort write calls cannot pass methods that should auto-resolve missing features.");
        }
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(u12.a<?> aVar) throws DeadObjectException {
        try {
            this.b.a(aVar.r(), this.c);
        } catch (DeadObjectException e) {
            throw e;
        } catch (RemoteException e2) {
            a(i32.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final k02[] b(u12.a<?> aVar) {
        return this.b.b();
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final boolean c(u12.a<?> aVar) {
        return this.b.a();
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(Status status) {
        this.c.b(this.d.a(status));
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(Exception exc) {
        this.c.b(exc);
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(j22 j22, boolean z) {
        j22.a(this.c, z);
    }
}
