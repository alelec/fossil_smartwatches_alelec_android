package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t84 extends RuntimeException {
    @DexIgnore
    public t84(String str) {
        super(str);
    }

    @DexIgnore
    public t84(String str, Exception exc) {
        super(str, exc);
    }
}
