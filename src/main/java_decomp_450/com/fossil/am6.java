package com.fossil;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am6 extends he {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public b b; // = new b(null, null, 3, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;

        @DexIgnore
        public b() {
            this(null, null, 3, null);
        }

        @DexIgnore
        public b(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(Integer num, Integer num2, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2);
        }

        @DexIgnore
        public final void a(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeGoalTrackingChartViewModel$saveColor$1", f = "CustomizeGoalTrackingChartViewModel.kt", l = {45, 46, 57}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $colorChart;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ am6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(am6 am6, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = am6;
            this.$id = str;
            this.$colorChart = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$id, this.$colorChart, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x008d  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x009e  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00fc A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0105  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 0
                r3 = 3
                r4 = 2
                r5 = 1
                if (r1 == 0) goto L_0x004b
                if (r1 == r5) goto L_0x003b
                if (r1 == r4) goto L_0x002b
                if (r1 != r3) goto L_0x0023
                java.lang.Object r0 = r8.L$2
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r8.L$1
                com.fossil.se7 r0 = (com.fossil.se7) r0
                java.lang.Object r0 = r8.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r9)
                goto L_0x00fd
            L_0x0023:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x002b:
                java.lang.Object r1 = r8.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r4 = r8.L$1
                com.fossil.se7 r4 = (com.fossil.se7) r4
                java.lang.Object r5 = r8.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r9)
                goto L_0x008b
            L_0x003b:
                java.lang.Object r1 = r8.L$2
                com.fossil.se7 r1 = (com.fossil.se7) r1
                java.lang.Object r5 = r8.L$1
                com.fossil.se7 r5 = (com.fossil.se7) r5
                java.lang.Object r6 = r8.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r9)
                goto L_0x006f
            L_0x004b:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r9 = r8.p$
                com.fossil.se7 r1 = new com.fossil.se7
                r1.<init>()
                com.fossil.am6 r6 = r8.this$0
                com.portfolio.platform.data.source.ThemeRepository r6 = r6.c
                java.lang.String r7 = r8.$id
                r8.L$0 = r9
                r8.L$1 = r1
                r8.L$2 = r1
                r8.label = r5
                java.lang.Object r5 = r6.getThemeById(r7, r8)
                if (r5 != r0) goto L_0x006c
                return r0
            L_0x006c:
                r6 = r9
                r9 = r5
                r5 = r1
            L_0x006f:
                com.portfolio.platform.data.model.Theme r9 = (com.portfolio.platform.data.model.Theme) r9
                if (r9 == 0) goto L_0x0074
                goto L_0x0091
            L_0x0074:
                com.fossil.am6 r9 = r8.this$0
                com.portfolio.platform.data.source.ThemeRepository r9 = r9.c
                r8.L$0 = r6
                r8.L$1 = r5
                r8.L$2 = r1
                r8.label = r4
                java.lang.Object r9 = r9.getCurrentTheme(r8)
                if (r9 != r0) goto L_0x0089
                return r0
            L_0x0089:
                r4 = r5
                r5 = r6
            L_0x008b:
                if (r9 == 0) goto L_0x0105
                com.portfolio.platform.data.model.Theme r9 = (com.portfolio.platform.data.model.Theme) r9
                r6 = r5
                r5 = r4
            L_0x0091:
                r1.element = r9
                com.fossil.se7 r9 = new com.fossil.se7
                r9.<init>()
                r9.element = r2
                java.lang.String r1 = r8.$colorChart
                if (r1 == 0) goto L_0x00d4
                T r1 = r5.element
                com.portfolio.platform.data.model.Theme r1 = (com.portfolio.platform.data.model.Theme) r1
                java.util.ArrayList r1 = r1.getStyles()
                java.util.Iterator r1 = r1.iterator()
            L_0x00aa:
                boolean r2 = r1.hasNext()
                if (r2 == 0) goto L_0x00d4
                java.lang.Object r2 = r1.next()
                com.portfolio.platform.data.model.Style r2 = (com.portfolio.platform.data.model.Style) r2
                java.lang.String r4 = r2.getKey()
                java.lang.String r7 = "hybridGoalTrackingTab"
                boolean r4 = com.fossil.ee7.a(r4, r7)
                if (r4 == 0) goto L_0x00aa
                java.lang.String r4 = r8.$colorChart
                r2.setValue(r4)
                java.lang.String r2 = r8.$colorChart
                int r2 = android.graphics.Color.parseColor(r2)
                java.lang.Integer r2 = com.fossil.pb7.a(r2)
                r9.element = r2
                goto L_0x00aa
            L_0x00d4:
                com.fossil.am6 r1 = r8.this$0
                com.fossil.am6$b r1 = r1.b
                T r2 = r9.element
                r4 = r2
                java.lang.Integer r4 = (java.lang.Integer) r4
                java.lang.Integer r2 = (java.lang.Integer) r2
                r1.a(r4, r2)
                com.fossil.am6 r1 = r8.this$0
                com.portfolio.platform.data.source.ThemeRepository r1 = r1.c
                T r2 = r5.element
                com.portfolio.platform.data.model.Theme r2 = (com.portfolio.platform.data.model.Theme) r2
                r8.L$0 = r6
                r8.L$1 = r5
                r8.L$2 = r9
                r8.label = r3
                java.lang.Object r9 = r1.upsertUserTheme(r2, r8)
                if (r9 != r0) goto L_0x00fd
                return r0
            L_0x00fd:
                com.fossil.am6 r9 = r8.this$0
                r9.a()
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            L_0x0105:
                com.fossil.ee7.a()
                throw r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.am6.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = am6.class.getSimpleName();
        ee7.a((Object) simpleName, "CustomizeGoalTrackingCha\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public am6(ThemeRepository themeRepository) {
        ee7.b(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.b);
    }

    @DexIgnore
    public final MutableLiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        int i;
        String a2 = xl6.q.a();
        if (a2 != null) {
            i = Color.parseColor(a2);
        } else {
            i = Color.parseColor(e);
        }
        this.b.a(Integer.valueOf(i), Integer.valueOf(i));
        a();
    }

    @DexIgnore
    public final void a(int i, int i2) {
        if (i == 801) {
            this.b.a(Integer.valueOf(i2), Integer.valueOf(i2));
            a();
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = d;
        local.d(str3, "saveColor colorChart=" + str2);
        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new c(this, str, str2, null), 3, null);
    }
}
