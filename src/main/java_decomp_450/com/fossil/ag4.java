package com.fossil;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ag4 {
    @DexIgnore
    public a<String, Pattern> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> {
        @DexIgnore
        public LinkedHashMap<K, V> a;
        @DexIgnore
        public int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ag4$a$a")
        /* renamed from: com.fossil.ag4$a$a  reason: collision with other inner class name */
        public class C0010a extends LinkedHashMap<K, V> {
            @DexIgnore
            public C0010a(int i, float f, boolean z) {
                super(i, f, z);
            }

            @DexIgnore
            @Override // java.util.LinkedHashMap
            public boolean removeEldestEntry(Map.Entry<K, V> entry) {
                return size() > a.this.b;
            }
        }

        @DexIgnore
        public a(int i) {
            this.b = i;
            this.a = new C0010a(((i * 4) / 3) + 1, 0.75f, true);
        }

        @DexIgnore
        public synchronized V a(K k) {
            return this.a.get(k);
        }

        @DexIgnore
        public synchronized void a(K k, V v) {
            this.a.put(k, v);
        }
    }

    @DexIgnore
    public ag4(int i) {
        this.a = new a<>(i);
    }

    @DexIgnore
    public Pattern a(String str) {
        Pattern a2 = this.a.a(str);
        if (a2 != null) {
            return a2;
        }
        Pattern compile = Pattern.compile(str);
        this.a.a(str, compile);
        return compile;
    }
}
