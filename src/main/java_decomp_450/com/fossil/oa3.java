package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.SupportMapFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa3 extends p73 {
    @DexIgnore
    public /* final */ /* synthetic */ p63 a;

    @DexIgnore
    public oa3(SupportMapFragment.a aVar, p63 p63) {
        this.a = p63;
    }

    @DexIgnore
    @Override // com.fossil.o73
    public final void a(w63 w63) throws RemoteException {
        this.a.onMapReady(new n63(w63));
    }
}
