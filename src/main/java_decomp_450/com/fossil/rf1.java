package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rf1 extends zk0 {
    @DexIgnore
    public ArrayList<hg0> C; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<xn0> D; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<hg0> E; // = new ArrayList<>(t97.h(this.H));
    @DexIgnore
    public int F; // = -1;
    @DexIgnore
    public float G;
    @DexIgnore
    public /* final */ hg0[] H;

    @DexIgnore
    public rf1(ri1 ri1, en0 en0, hg0[] hg0Arr) {
        super(ri1, en0, wm0.F0, null, false, 24);
        this.H = hg0Arr;
    }

    @DexIgnore
    public static final /* synthetic */ void c(rf1 rf1) {
        if (!rf1.D.isEmpty()) {
            xn0 remove = rf1.D.remove(0);
            ee7.a((Object) remove, "needToUninstallWatchApps.removeAt(0)");
            zk0.a(rf1, new xq0(((zk0) rf1).w, ((zk0) rf1).x, remove), ja1.a, dc1.a, (kd7) null, new xd1(rf1), (gd7) null, 40, (Object) null);
        } else if (rf1.E.isEmpty()) {
            rf1.a(1.0f);
            rf1.a(eu0.a(((zk0) rf1).v, null, is0.SUCCESS, null, 5));
        } else {
            rf1.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        boolean z;
        T t;
        boolean z2;
        hg0[] hg0Arr = this.H;
        ArrayList arrayList = new ArrayList();
        for (hg0 hg0 : hg0Arr) {
            Iterator<T> it = this.C.iterator();
            while (true) {
                z = true;
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                T t2 = t;
                if (!ee7.a((Object) t2.getBundleId(), (Object) hg0.getBundleId()) || t2.d() != hg0.d()) {
                    z2 = false;
                    continue;
                } else {
                    z2 = true;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            if (t == null) {
                z = false;
            }
            if (z) {
                arrayList.add(hg0);
            }
        }
        Object[] array = arrayList.toArray(new hg0[0]);
        if (array != null) {
            return (hg0[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new ts1(((zk0) this).w, ((zk0) this).x), new u41(this), new r61(this), new o81(this), (gd7) null, (gd7) null, 48, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.H5, yz0.a(this.H));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.I5;
        Object[] array = this.C.toArray(new hg0[0]);
        if (array != null) {
            return yz0.a(k, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void m() {
        int i = this.F + 1;
        this.F = i;
        if (i < this.E.size()) {
            hg0 hg0 = this.E.get(this.F);
            ee7.a((Object) hg0, "needToInstallWatchApps[watchAppToInstallIndex]");
            hg0 hg02 = hg0;
            zk0.a(this, new x01(((zk0) this).w, ((zk0) this).x, hg02), new mz0(this, hg02), new f11(this), new x21(this), (gd7) null, (gd7) null, 48, (Object) null);
            return;
        }
        a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
    }
}
