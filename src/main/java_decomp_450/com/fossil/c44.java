package com.fossil;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c44 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ t74 b;
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler c;
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(t74 t74, Thread thread, Throwable th);
    }

    @DexIgnore
    public c44(a aVar, t74 t74, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.a = aVar;
        this.b = t74;
        this.c = uncaughtExceptionHandler;
    }

    @DexIgnore
    public boolean a() {
        return this.d.get();
    }

    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        this.d.set(true);
        if (thread == null) {
            try {
                z24.a().b("Could not handle uncaught exception; null thread");
            } catch (Exception e) {
                z24.a().b("An error occurred in the uncaught exception handler", e);
            } catch (Throwable th2) {
                z24.a().a("Crashlytics completed exception processing. Invoking default exception handler.");
                this.c.uncaughtException(thread, th);
                this.d.set(false);
                throw th2;
            }
        } else if (th == null) {
            z24.a().b("Could not handle uncaught exception; null throwable");
        } else {
            this.a.a(this.b, thread, th);
        }
        z24.a().a("Crashlytics completed exception processing. Invoking default exception handler.");
        this.c.uncaughtException(thread, th);
        this.d.set(false);
    }
}
