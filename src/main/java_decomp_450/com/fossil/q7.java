package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface q7 {
    @DexIgnore
    void setTint(int i);

    @DexIgnore
    void setTintList(ColorStateList colorStateList);

    @DexIgnore
    void setTintMode(PorterDuff.Mode mode);
}
