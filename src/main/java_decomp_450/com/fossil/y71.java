package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y71<T> {
    @DexIgnore
    public /* final */ r60 a;

    @DexIgnore
    public y71(r60 r60) {
        this.a = r60;
    }

    @DexIgnore
    public abstract byte[] a(T t);

    @DexIgnore
    public abstract byte[] a(short s, T t);
}
