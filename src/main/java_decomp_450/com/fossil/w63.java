package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface w63 extends IInterface {
    @DexIgnore
    float C() throws RemoteException;

    @DexIgnore
    bn2 a(d93 d93) throws RemoteException;

    @DexIgnore
    en2 a(k93 k93) throws RemoteException;

    @DexIgnore
    hn2 a(n93 n93) throws RemoteException;

    @DexIgnore
    kn2 a(p93 p93) throws RemoteException;

    @DexIgnore
    void a(int i, int i2, int i3, int i4) throws RemoteException;

    @DexIgnore
    void a(ab2 ab2) throws RemoteException;

    @DexIgnore
    void a(ab2 ab2, j83 j83) throws RemoteException;

    @DexIgnore
    void a(c83 c83, ab2 ab2) throws RemoteException;

    @DexIgnore
    void a(g73 g73) throws RemoteException;

    @DexIgnore
    void a(i73 i73) throws RemoteException;

    @DexIgnore
    void a(k73 k73) throws RemoteException;

    @DexIgnore
    void a(m73 m73) throws RemoteException;

    @DexIgnore
    void a(q73 q73) throws RemoteException;

    @DexIgnore
    void a(q83 q83) throws RemoteException;

    @DexIgnore
    void a(s73 s73) throws RemoteException;

    @DexIgnore
    void a(s83 s83) throws RemoteException;

    @DexIgnore
    void a(u83 u83) throws RemoteException;

    @DexIgnore
    void a(v73 v73) throws RemoteException;

    @DexIgnore
    void a(w83 w83) throws RemoteException;

    @DexIgnore
    void a(x73 x73) throws RemoteException;

    @DexIgnore
    void a(LatLngBounds latLngBounds) throws RemoteException;

    @DexIgnore
    boolean a(i93 i93) throws RemoteException;

    @DexIgnore
    void b(float f) throws RemoteException;

    @DexIgnore
    void c(float f) throws RemoteException;

    @DexIgnore
    void clear() throws RemoteException;

    @DexIgnore
    void d(ab2 ab2) throws RemoteException;

    @DexIgnore
    float g() throws RemoteException;

    @DexIgnore
    CameraPosition m() throws RemoteException;

    @DexIgnore
    boolean s() throws RemoteException;

    @DexIgnore
    void setBuildingsEnabled(boolean z) throws RemoteException;

    @DexIgnore
    boolean setIndoorEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setMapType(int i) throws RemoteException;

    @DexIgnore
    void setMyLocationEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void setTrafficEnabled(boolean z) throws RemoteException;

    @DexIgnore
    void u() throws RemoteException;

    @DexIgnore
    c73 w() throws RemoteException;

    @DexIgnore
    boolean x() throws RemoteException;

    @DexIgnore
    z63 y() throws RemoteException;
}
