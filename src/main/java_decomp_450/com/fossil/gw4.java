package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gw4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ RecyclerView u;
    @DexIgnore
    public /* final */ View v;

    @DexIgnore
    public gw4(Object obj, View view, int i, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, RecyclerView recyclerView, View view2) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = constraintLayout;
        this.s = flexibleTextView;
        this.t = rTLImageView;
        this.u = recyclerView;
        this.v = view2;
    }
}
