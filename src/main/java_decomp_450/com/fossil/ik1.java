package com.fossil;

import java.util.zip.CRC32;
import java.util.zip.Checksum;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik1 {
    @DexIgnore
    public static /* final */ ik1 a; // = new ik1();

    @DexIgnore
    public final long a(byte[] bArr, ng1 ng1) {
        Checksum a2 = a(ng1);
        a2.update(bArr, 0, bArr.length);
        return a2.getValue();
    }

    @DexIgnore
    public final long a(byte[] bArr, int i, int i2, ng1 ng1) {
        if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            return 0;
        }
        Checksum a2 = a(ng1);
        a2.update(bArr, i, i2);
        return a2.getValue();
    }

    @DexIgnore
    public final Checksum a(ng1 ng1) {
        int i = ki1.a[ng1.ordinal()];
        if (i == 1) {
            return new CRC32();
        }
        if (i == 2) {
            return new re1();
        }
        throw new p87();
    }
}
