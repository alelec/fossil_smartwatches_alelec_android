package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o74 {
    @DexIgnore
    public static /* final */ e64 b; // = new e64();
    @DexIgnore
    public static /* final */ String c; // = a("hts/cahyiseot-agolai.o/1frlglgc/aclg", "tp:/rsltcrprsp.ogepscmv/ieo/eaybtho");
    @DexIgnore
    public static /* final */ String d; // = a("AzSBpY4F0rHiHFdinTvM", "IayrSTFL9eJ69YeSUO2");
    @DexIgnore
    public static /* final */ et1<v54, byte[]> e; // = n74.a();
    @DexIgnore
    public /* final */ ft1<v54> a;

    @DexIgnore
    public o74(ft1<v54> ft1, et1<v54, byte[]> et1) {
        this.a = ft1;
    }

    @DexIgnore
    public static o74 a(Context context) {
        uu1.a(context);
        return new o74(uu1.b().a(new it1(c, d)).a("FIREBASE_CRASHLYTICS_REPORT", v54.class, bt1.a("json"), e), e);
    }

    @DexIgnore
    public no3<b44> a(b44 b44) {
        v54 a2 = b44.a();
        oo3 oo3 = new oo3();
        this.a.a(ct1.c(a2), m74.a(oo3, b44));
        return oo3.a();
    }

    @DexIgnore
    public static /* synthetic */ void a(oo3 oo3, b44 b44, Exception exc) {
        if (exc != null) {
            oo3.b(exc);
        } else {
            oo3.b(b44);
        }
    }

    @DexIgnore
    public static String a(String str, String str2) {
        int length = str.length() - str2.length();
        if (length < 0 || length > 1) {
            throw new IllegalArgumentException("Invalid input received");
        }
        StringBuilder sb = new StringBuilder(str.length() + str2.length());
        for (int i = 0; i < str.length(); i++) {
            sb.append(str.charAt(i));
            if (str2.length() > i) {
                sb.append(str2.charAt(i));
            }
        }
        return sb.toString();
    }
}
