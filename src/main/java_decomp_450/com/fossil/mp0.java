package com.fossil;

import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp0 {
    @DexIgnore
    public static /* final */ Hashtable<String, qn0> a; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ mp0 b; // = new mp0();

    @DexIgnore
    public final void a(String str, byte[] bArr) {
        synchronized (a) {
            b.a(str).a = bArr;
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final void a(String str, byte[] bArr, byte[] bArr2) {
        synchronized (a) {
            b.a(str).a(bArr, bArr2);
            i97 i97 = i97.a;
        }
    }

    @DexIgnore
    public final qn0 a(String str) {
        qn0 qn0;
        synchronized (a) {
            qn0 = a.get(str);
            if (qn0 == null) {
                qn0 = new qn0();
            }
            a.put(str, qn0);
        }
        return qn0;
    }
}
