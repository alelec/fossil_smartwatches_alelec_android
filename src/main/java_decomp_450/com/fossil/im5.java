package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class im5 implements Factory<hm5> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> b;
    @DexIgnore
    public /* final */ Provider<ad5> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<ch5> e;

    @DexIgnore
    public im5(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<ad5> provider3, Provider<PortfolioApp> provider4, Provider<ch5> provider5) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
    }

    @DexIgnore
    public static im5 a(Provider<UserRepository> provider, Provider<DeviceRepository> provider2, Provider<ad5> provider3, Provider<PortfolioApp> provider4, Provider<ch5> provider5) {
        return new im5(provider, provider2, provider3, provider4, provider5);
    }

    @DexIgnore
    public static hm5 a(UserRepository userRepository, DeviceRepository deviceRepository, ad5 ad5, PortfolioApp portfolioApp, ch5 ch5) {
        return new hm5(userRepository, deviceRepository, ad5, portfolioApp, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public hm5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get());
    }
}
