package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class iy4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView A;
    @DexIgnore
    public /* final */ RTLImageView B;
    @DexIgnore
    public /* final */ ConstraintLayout C;
    @DexIgnore
    public /* final */ View D;
    @DexIgnore
    public /* final */ ImageView E;
    @DexIgnore
    public /* final */ LinearLayout F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public /* final */ RecyclerView I;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ RTLImageView w;
    @DexIgnore
    public /* final */ a95 x;
    @DexIgnore
    public /* final */ a95 y;
    @DexIgnore
    public /* final */ RTLImageView z;

    @DexIgnore
    public iy4(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, View view2, ConstraintLayout constraintLayout, RTLImageView rTLImageView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, RTLImageView rTLImageView2, a95 a95, a95 a952, RTLImageView rTLImageView3, RTLImageView rTLImageView4, RTLImageView rTLImageView5, ConstraintLayout constraintLayout2, View view3, ImageView imageView, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout3, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, View view4) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = view2;
        this.s = constraintLayout;
        this.t = rTLImageView;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = rTLImageView2;
        this.x = a95;
        a((ViewDataBinding) a95);
        this.y = a952;
        a((ViewDataBinding) a952);
        this.z = rTLImageView3;
        this.A = rTLImageView4;
        this.B = rTLImageView5;
        this.C = constraintLayout2;
        this.D = view3;
        this.E = imageView;
        this.F = linearLayout;
        this.G = linearLayout2;
        this.H = constraintLayout3;
        this.I = recyclerView;
        this.J = flexibleSwitchCompat;
        this.K = view4;
    }
}
