package com.fossil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NumberPicker;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps5 extends dy6 implements jw5 {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public /* final */ pb j; // = new pm4(this);
    @DexIgnore
    public qw6<u05> p;
    @DexIgnore
    public iw5 q;
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ps5.s;
        }

        @DexIgnore
        public final ps5 b() {
            return new ps5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ps5 a;
        @DexIgnore
        public /* final */ /* synthetic */ u05 b;

        @DexIgnore
        public b(ps5 ps5, u05 u05) {
            this.a = ps5;
            this.b = u05;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            iw5 a2 = ps5.a(this.a);
            String valueOf = String.valueOf(i2);
            NumberPicker numberPicker2 = this.b.v;
            ee7.a((Object) numberPicker2, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.u;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ps5 a;
        @DexIgnore
        public /* final */ /* synthetic */ u05 b;

        @DexIgnore
        public c(ps5 ps5, u05 u05) {
            this.a = ps5;
            this.b = u05;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            iw5 a2 = ps5.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            String valueOf2 = String.valueOf(i2);
            NumberPicker numberPicker3 = this.b.u;
            ee7.a((Object) numberPicker3, "binding.numberPickerThree");
            boolean z = true;
            if (numberPicker3.getValue() != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ ps5 a;
        @DexIgnore
        public /* final */ /* synthetic */ u05 b;

        @DexIgnore
        public d(ps5 ps5, u05 u05) {
            this.a = ps5;
            this.b = u05;
        }

        @DexIgnore
        @Override // com.portfolio.platform.view.NumberPicker.g
        public final void a(NumberPicker numberPicker, int i, int i2) {
            iw5 a2 = ps5.a(this.a);
            NumberPicker numberPicker2 = this.b.t;
            ee7.a((Object) numberPicker2, "binding.numberPickerOne");
            String valueOf = String.valueOf(numberPicker2.getValue());
            NumberPicker numberPicker3 = this.b.v;
            ee7.a((Object) numberPicker3, "binding.numberPickerTwo");
            String valueOf2 = String.valueOf(numberPicker3.getValue());
            boolean z = true;
            if (i2 != 1) {
                z = false;
            }
            a2.a(valueOf, valueOf2, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ps5 a;

        @DexIgnore
        public e(ps5 ps5) {
            this.a = ps5;
        }

        @DexIgnore
        public final void onClick(View view) {
            ps5.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ u05 a;
        @DexIgnore
        public /* final */ /* synthetic */ se7 b;

        @DexIgnore
        public f(u05 u05, se7 se7) {
            this.a = u05;
            this.b = se7;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.w;
            ee7.a((Object) constraintLayout, "it.rootBackground");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                ViewGroup.LayoutParams layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior bottomSheetBehavior = (BottomSheetBehavior) ((CoordinatorLayout.e) layoutParams).d();
                    if (bottomSheetBehavior != null) {
                        bottomSheetBehavior.e(3);
                        u05 u05 = this.a;
                        ee7.a((Object) u05, "it");
                        View d = u05.d();
                        ee7.a((Object) d, "it.root");
                        d.getViewTreeObserver().removeOnGlobalLayoutListener(this.b.element);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new x87("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = ps5.class.getSimpleName();
        ee7.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026nt::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ iw5 a(ps5 ps5) {
        iw5 iw5 = ps5.q;
        if (iw5 != null) {
            return iw5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.dy6
    public void a1() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.jw5
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public final void n(int i) {
        iw5 iw5 = this.q;
        if (iw5 != null) {
            iw5.a(i);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        u05 u05 = (u05) qb.a(layoutInflater, 2131558549, viewGroup, false, this.j);
        u05.q.setOnClickListener(new e(this));
        ee7.a((Object) u05, "binding");
        a(u05);
        this.p = new qw6<>(this, u05);
        return u05.d();
    }

    @DexIgnore
    @Override // com.fossil.dy6, androidx.fragment.app.Fragment, com.fossil.ac
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        a1();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        iw5 iw5 = this.q;
        if (iw5 != null) {
            iw5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        iw5 iw5 = this.q;
        if (iw5 != null) {
            iw5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null) {
                se7 se7 = new se7();
                se7.element = null;
                se7.element = (T) new f(a2, se7);
                ee7.a((Object) a2, "it");
                View d2 = a2.d();
                ee7.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener(se7.element);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(iw5 iw5) {
        ee7.b(iw5, "presenter");
        this.q = iw5;
    }

    @DexIgnore
    public final void a(u05 u05) {
        String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            u05.w.setBackgroundColor(Color.parseColor(b2));
        }
        NumberPicker numberPicker = u05.t;
        ee7.a((Object) numberPicker, "binding.numberPickerOne");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = u05.t;
        ee7.a((Object) numberPicker2, "binding.numberPickerOne");
        numberPicker2.setMaxValue(12);
        u05.t.setOnValueChangedListener(new b(this, u05));
        NumberPicker numberPicker3 = u05.v;
        ee7.a((Object) numberPicker3, "binding.numberPickerTwo");
        numberPicker3.setMinValue(0);
        NumberPicker numberPicker4 = u05.v;
        ee7.a((Object) numberPicker4, "binding.numberPickerTwo");
        numberPicker4.setMaxValue(59);
        u05.v.setOnValueChangedListener(new c(this, u05));
        String[] strArr = {ig5.a(PortfolioApp.g0.c(), 2131886102), ig5.a(PortfolioApp.g0.c(), 2131886104)};
        NumberPicker numberPicker5 = u05.u;
        ee7.a((Object) numberPicker5, "binding.numberPickerThree");
        numberPicker5.setMinValue(0);
        NumberPicker numberPicker6 = u05.u;
        ee7.a((Object) numberPicker6, "binding.numberPickerThree");
        numberPicker6.setMaxValue(1);
        u05.u.setDisplayedValues(strArr);
        u05.u.setOnValueChangedListener(new d(this, u05));
    }

    @DexIgnore
    @Override // com.fossil.jw5
    public void a(String str) {
        FlexibleTextView flexibleTextView;
        ee7.b(str, "title");
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null && (flexibleTextView = a2.r) != null) {
                flexibleTextView.setText(str);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.jw5
    public void a(int i) {
        int i2;
        int i3 = i / 60;
        int i4 = i % 60;
        int i5 = 12;
        if (i3 >= 12) {
            i2 = 1;
            i3 -= 12;
        } else {
            i2 = 0;
        }
        if (i3 != 0) {
            i5 = i3;
        }
        qw6<u05> qw6 = this.p;
        if (qw6 != null) {
            u05 a2 = qw6.a();
            if (a2 != null) {
                NumberPicker numberPicker = a2.t;
                ee7.a((Object) numberPicker, "it.numberPickerOne");
                numberPicker.setValue(i5);
                NumberPicker numberPicker2 = a2.v;
                ee7.a((Object) numberPicker2, "it.numberPickerTwo");
                numberPicker2.setValue(i4);
                NumberPicker numberPicker3 = a2.u;
                ee7.a((Object) numberPicker3, "it.numberPickerThree");
                numberPicker3.setValue(i2);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
