package com.fossil;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d94 implements z84<d94> {
    @DexIgnore
    public static /* final */ u84<Object> e; // = a94.a();
    @DexIgnore
    public static /* final */ w84<String> f; // = b94.a();
    @DexIgnore
    public static /* final */ w84<Boolean> g; // = c94.a();
    @DexIgnore
    public static /* final */ b h; // = new b(null);
    @DexIgnore
    public /* final */ Map<Class<?>, u84<?>> a; // = new HashMap();
    @DexIgnore
    public /* final */ Map<Class<?>, w84<?>> b; // = new HashMap();
    @DexIgnore
    public u84<Object> c; // = e;
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements w84<Date> {
        @DexIgnore
        public static /* final */ DateFormat a;

        /*
        static {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
            a = simpleDateFormat;
            simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        }
        */

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public void a(Date date, x84 x84) throws IOException {
            x84.a(a.format(date));
        }
    }

    @DexIgnore
    public d94() {
        a(String.class, f);
        a(Boolean.class, g);
        a(Date.class, h);
    }

    @DexIgnore
    public static /* synthetic */ void a(Object obj, v84 v84) throws IOException {
        throw new t84("Couldn't find encoder for type " + obj.getClass().getCanonicalName());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements r84 {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.r84
        public void a(Object obj, Writer writer) throws IOException {
            e94 e94 = new e94(writer, d94.this.a, d94.this.b, d94.this.c, d94.this.d);
            e94.a(obj, false);
            e94.a();
        }

        @DexIgnore
        @Override // com.fossil.r84
        public String a(Object obj) {
            StringWriter stringWriter = new StringWriter();
            try {
                a(obj, stringWriter);
            } catch (IOException unused) {
            }
            return stringWriter.toString();
        }
    }

    @DexIgnore
    @Override // com.fossil.z84
    public <T> d94 a(Class<T> cls, u84<? super T> u84) {
        this.a.put(cls, u84);
        this.b.remove(cls);
        return this;
    }

    @DexIgnore
    public <T> d94 a(Class<T> cls, w84<? super T> w84) {
        this.b.put(cls, w84);
        this.a.remove(cls);
        return this;
    }

    @DexIgnore
    public d94 a(y84 y84) {
        y84.a(this);
        return this;
    }

    @DexIgnore
    public d94 a(boolean z) {
        this.d = z;
        return this;
    }

    @DexIgnore
    public r84 a() {
        return new a();
    }
}
