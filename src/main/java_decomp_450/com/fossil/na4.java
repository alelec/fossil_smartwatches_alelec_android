package com.fossil;

import android.os.Handler;
import android.os.Message;
import com.fossil.ma4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class na4 implements Handler.Callback {
    @DexIgnore
    public /* final */ ma4.b a;

    @DexIgnore
    public na4(ma4.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final boolean handleMessage(Message message) {
        return this.a.a(message);
    }
}
