package com.fossil;

import com.fossil.bg7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class me7 extends ne7 implements bg7 {
    @DexIgnore
    public me7() {
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public sf7 computeReflected() {
        te7.a(this);
        return this;
    }

    @DexIgnore
    @Override // com.fossil.bg7
    public Object getDelegate(Object obj) {
        return ((bg7) getReflected()).getDelegate(obj);
    }

    @DexIgnore
    @Override // com.fossil.gd7
    public Object invoke(Object obj) {
        return get(obj);
    }

    @DexIgnore
    public me7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.bg7
    public bg7.a getGetter() {
        return ((bg7) getReflected()).getGetter();
    }
}
