package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w53 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<w53> CREATOR; // = new x53();
    @DexIgnore
    public boolean a;
    @DexIgnore
    public long b;
    @DexIgnore
    public float c;
    @DexIgnore
    public long d;
    @DexIgnore
    public int e;

    @DexIgnore
    public w53() {
        this(true, 50, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Long.MAX_VALUE, Integer.MAX_VALUE);
    }

    @DexIgnore
    public w53(boolean z, long j, float f, long j2, int i) {
        this.a = z;
        this.b = j;
        this.c = f;
        this.d = j2;
        this.e = i;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof w53)) {
            return false;
        }
        w53 w53 = (w53) obj;
        return this.a == w53.a && this.b == w53.b && Float.compare(this.c, w53.c) == 0 && this.d == w53.d && this.e == w53.e;
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(Boolean.valueOf(this.a), Long.valueOf(this.b), Float.valueOf(this.c), Long.valueOf(this.d), Integer.valueOf(this.e));
    }

    @DexIgnore
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DeviceOrientationRequest[mShouldUseMag=");
        sb.append(this.a);
        sb.append(" mMinimumSamplingPeriodMs=");
        sb.append(this.b);
        sb.append(" mSmallestAngleChangeRadians=");
        sb.append(this.c);
        long j = this.d;
        if (j != Long.MAX_VALUE) {
            sb.append(" expireIn=");
            sb.append(j - SystemClock.elapsedRealtime());
            sb.append("ms");
        }
        if (this.e != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.e);
        }
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, this.c);
        k72.a(parcel, 4, this.d);
        k72.a(parcel, 5, this.e);
        k72.a(parcel, a2);
    }
}
