package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q47 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ a47 b;

    @DexIgnore
    public q47(Context context, a47 a47) {
        this.a = context;
        this.b = a47;
    }

    @DexIgnore
    public final void run() {
        try {
            z37.a(this.a, false, this.b);
        } catch (Throwable th) {
            z37.m.a(th);
        }
    }
}
