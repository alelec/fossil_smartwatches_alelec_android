package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb7 implements fb7<Object> {
    @DexIgnore
    public static /* final */ qb7 a; // = new qb7();

    @DexIgnore
    @Override // com.fossil.fb7
    public ib7 getContext() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    @Override // com.fossil.fb7
    public void resumeWith(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    @DexIgnore
    public String toString() {
        return "This continuation is already complete";
    }
}
