package com.fossil;

import com.fossil.iy3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sz3<E> extends iy3.b<E> {
    @DexIgnore
    public static /* final */ sz3<Object> EMPTY; // = new sz3<>(iz3.a, 0, null, 0);
    @DexIgnore
    public /* final */ transient Object[] b;
    @DexIgnore
    public /* final */ transient int c;
    @DexIgnore
    public /* final */ transient int d;
    @DexIgnore
    public /* final */ transient Object[] table;

    @DexIgnore
    public sz3(Object[] objArr, int i, Object[] objArr2, int i2) {
        this.b = objArr;
        this.table = objArr2;
        this.c = i2;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        Object[] objArr = this.table;
        if (obj == null || objArr == null) {
            return false;
        }
        int a = sx3.a(obj);
        while (true) {
            int i = a & this.c;
            Object obj2 = objArr[i];
            if (obj2 == null) {
                return false;
            }
            if (obj2.equals(obj)) {
                return true;
            }
            a = i + 1;
        }
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.b;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return i + this.b.length;
    }

    @DexIgnore
    @Override // com.fossil.iy3.b, com.fossil.iy3
    public zx3<E> createAsList() {
        return this.table == null ? zx3.of() : new nz3(this, this.b);
    }

    @DexIgnore
    @Override // com.fossil.iy3.b
    public E get(int i) {
        return (E) this.b[i];
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public int hashCode() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.b.length;
    }
}
