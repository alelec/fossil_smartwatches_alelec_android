package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bo extends Cdo<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = im.a("BatteryChrgTracker");

    @DexIgnore
    public bo(Context context, vp vpVar) {
        super(context, vpVar);
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public IntentFilter d() {
        IntentFilter intentFilter = new IntentFilter();
        if (Build.VERSION.SDK_INT >= 23) {
            intentFilter.addAction("android.os.action.CHARGING");
            intentFilter.addAction("android.os.action.DISCHARGING");
        } else {
            intentFilter.addAction("android.intent.action.ACTION_POWER_CONNECTED");
            intentFilter.addAction("android.intent.action.ACTION_POWER_DISCONNECTED");
        }
        return intentFilter;
    }

    @DexIgnore
    @Override // com.fossil.eo
    public Boolean a() {
        Intent registerReceiver = ((eo) this).b.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        if (registerReceiver != null) {
            return Boolean.valueOf(a(registerReceiver));
        }
        im.a().b(i, "getInitialState - null intent received", new Throwable[0]);
        return null;
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public void a(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null) {
            im.a().a(i, String.format("Received %s", action), new Throwable[0]);
            char c = '\uffff';
            switch (action.hashCode()) {
                case -1886648615:
                    if (action.equals("android.intent.action.ACTION_POWER_DISCONNECTED")) {
                        c = 3;
                        break;
                    }
                    break;
                case -54942926:
                    if (action.equals("android.os.action.DISCHARGING")) {
                        c = 1;
                        break;
                    }
                    break;
                case 948344062:
                    if (action.equals("android.os.action.CHARGING")) {
                        c = 0;
                        break;
                    }
                    break;
                case 1019184907:
                    if (action.equals("android.intent.action.ACTION_POWER_CONNECTED")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                a((Object) true);
            } else if (c == 1) {
                a((Object) false);
            } else if (c == 2) {
                a((Object) true);
            } else if (c == 3) {
                a((Object) false);
            }
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        if (Build.VERSION.SDK_INT >= 23) {
            int intExtra = intent.getIntExtra("status", -1);
            if (intExtra == 2 || intExtra == 5) {
                return true;
            }
        } else if (intent.getIntExtra("plugged", 0) != 0) {
            return true;
        }
        return false;
    }
}
