package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on1 extends qj1 {
    @DexIgnore
    public /* final */ qk1 A;
    @DexIgnore
    public /* final */ boolean B;

    @DexIgnore
    public on1(qk1 qk1, boolean z, ri1 ri1) {
        super(qa1.e, ri1);
        this.A = qk1;
        this.B = z;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(yz0.a(super.g(), r51.P0, this.A.a), r51.E, Boolean.valueOf(this.B));
    }

    @DexIgnore
    @Override // com.fossil.yf1
    public eo0 k() {
        return new fd1(this.A, this.B, ((v81) this).y.w);
    }
}
