package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp4 implements Factory<fp4> {
    @DexIgnore
    public /* final */ Provider<dp4> a;
    @DexIgnore
    public /* final */ Provider<ep4> b;
    @DexIgnore
    public /* final */ Provider<ch5> c;

    @DexIgnore
    public gp4(Provider<dp4> provider, Provider<ep4> provider2, Provider<ch5> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static gp4 a(Provider<dp4> provider, Provider<ep4> provider2, Provider<ch5> provider3) {
        return new gp4(provider, provider2, provider3);
    }

    @DexIgnore
    public static fp4 a(dp4 dp4, ep4 ep4, ch5 ch5) {
        return new fp4(dp4, ep4, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fp4 get() {
        return a(this.a.get(), this.b.get(), this.c.get());
    }
}
