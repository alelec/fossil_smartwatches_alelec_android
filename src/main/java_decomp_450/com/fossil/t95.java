package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t95 extends s95 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362741, 1);
        B.put(2131362533, 2);
        B.put(2131362488, 3);
        B.put(2131362487, 4);
        B.put(2131363056, 5);
        B.put(2131362453, 6);
        B.put(2131362452, 7);
        B.put(2131362532, 8);
    }
    */

    @DexIgnore
    public t95(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public t95(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[2], (AppCompatImageView) objArr[1], (View) objArr[5]);
        this.z = -1;
        ConstraintLayout constraintLayout = (ConstraintLayout) objArr[0];
        this.y = constraintLayout;
        constraintLayout.setTag(null);
        a(view);
        f();
    }
}
