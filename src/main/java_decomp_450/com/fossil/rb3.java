package com.fossil;

import android.os.Bundle;
import android.text.TextUtils;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rb3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ tb3 f;

    @DexIgnore
    public rb3(oh3 oh3, String str, String str2, String str3, long j, long j2, tb3 tb3) {
        a72.b(str2);
        a72.b(str3);
        a72.a(tb3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            oh3.e().w().a("Event created with reverse previous/current timestamps. appId, name", jg3.a(str2), jg3.a(str3));
        }
        this.f = tb3;
    }

    @DexIgnore
    public final rb3 a(oh3 oh3, long j) {
        return new rb3(oh3, this.c, this.a, this.b, this.d, j, this.f);
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 33 + String.valueOf(str2).length() + String.valueOf(valueOf).length());
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public rb3(oh3 oh3, String str, String str2, String str3, long j, long j2, Bundle bundle) {
        tb3 tb3;
        a72.b(str2);
        a72.b(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j;
        this.e = j2;
        if (j2 != 0 && j2 > j) {
            oh3.e().w().a("Event created with reverse previous/current timestamps. appId", jg3.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            tb3 = new tb3(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    oh3.e().t().a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = oh3.v().a(next, bundle2.get(next));
                    if (a2 == null) {
                        oh3.e().w().a("Param value can't be null", oh3.w().b(next));
                        it.remove();
                    } else {
                        oh3.v().a(bundle2, next, a2);
                    }
                }
            }
            tb3 = new tb3(bundle2);
        }
        this.f = tb3;
    }
}
