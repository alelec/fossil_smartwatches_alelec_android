package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jm7<T> extends rh7<T> implements sb7 {
    @DexIgnore
    public /* final */ fb7<T> d;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: com.fossil.fb7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public jm7(ib7 ib7, fb7<? super T> fb7) {
        super(ib7, true);
        this.d = fb7;
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public void a(Object obj) {
        mj7.a(mb7.a(this.d), mi7.a(obj, this.d));
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public final sb7 getCallerFrame() {
        return (sb7) this.d;
    }

    @DexIgnore
    @Override // com.fossil.sb7
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.pk7
    public final boolean j() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.rh7
    public void l(Object obj) {
        fb7<T> fb7 = this.d;
        fb7.resumeWith(mi7.a(obj, fb7));
    }
}
