package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lb1 extends eo0 {
    @DexIgnore
    public int j; // = 23;
    @DexIgnore
    public /* final */ int k;

    @DexIgnore
    public lb1(int i, cx0 cx0) {
        super(aq0.f, cx0);
        this.k = i;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        ri1.a(this.k);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return s91 instanceof af1;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.c;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.j = ((af1) s91).b;
    }
}
