package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ht implements Iterable<r87<? extends String, ? extends c>>, ye7 {
    @DexIgnore
    public static /* final */ ht b; // = new a().a();
    @DexIgnore
    public static /* final */ b c; // = new b(null);
    @DexIgnore
    public /* final */ SortedMap<String, c> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ SortedMap<String, c> a; // = na7.a(new r87[0]);

        @DexIgnore
        public final ht a() {
            return new ht(na7.b(this.a), null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return ee7.a(this.a, cVar.a) && ee7.a(this.b, cVar.b);
        }

        @DexIgnore
        public int hashCode() {
            Object obj = this.a;
            int i = 0;
            int hashCode = (obj != null ? obj.hashCode() : 0) * 31;
            String str = this.b;
            if (str != null) {
                i = str.hashCode();
            }
            return hashCode + i;
        }

        @DexIgnore
        public String toString() {
            return "Entry(value=" + this.a + ", cacheKey=" + this.b + ")";
        }
    }

    @DexIgnore
    public ht(SortedMap<String, c> sortedMap) {
        this.a = sortedMap;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return ee7.a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final boolean isEmpty() {
        return this.a.isEmpty();
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator<com.fossil.r87<java.lang.String, com.fossil.ht$c>>' to match base method */
    @Override // java.lang.Iterable
    public Iterator<r87<? extends String, ? extends c>> iterator() {
        SortedMap<String, c> sortedMap = this.a;
        ArrayList arrayList = new ArrayList(sortedMap.size());
        for (Map.Entry<String, c> entry : sortedMap.entrySet()) {
            arrayList.add(w87.a(entry.getKey(), entry.getValue()));
        }
        return arrayList.iterator();
    }

    @DexIgnore
    public String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public /* synthetic */ ht(SortedMap sortedMap, zd7 zd7) {
        this(sortedMap);
    }
}
