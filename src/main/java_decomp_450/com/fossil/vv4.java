package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class vv4 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon19 a;
    @DexIgnore
    private /* final */ /* synthetic */ ActivePreset b;

    @DexIgnore
    public /* synthetic */ vv4(PresetRepository.Anon19 anon19, ActivePreset activePreset) {
        this.a = anon19;
        this.b = activePreset;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
