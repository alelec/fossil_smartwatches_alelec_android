package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o23 implements p23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;
    @DexIgnore
    public static /* final */ tq2<Boolean> c;
    @DexIgnore
    public static /* final */ tq2<Boolean> d;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.sdk.collection.enable_extend_user_property_size", true);
        b = dr2.a("measurement.sdk.collection.last_deep_link_referrer2", true);
        c = dr2.a("measurement.sdk.collection.last_deep_link_referrer_campaign2", false);
        d = dr2.a("measurement.sdk.collection.last_gclid_from_referrer2", false);
        dr2.a("measurement.id.sdk.collection.last_deep_link_referrer2", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.p23
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.p23
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.p23
    public final boolean zzc() {
        return c.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.p23
    public final boolean zzd() {
        return d.b().booleanValue();
    }
}
