package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d01 implements Parcelable.Creator<x11> {
    @DexIgnore
    public /* synthetic */ d01(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public x11 createFromParcel(Parcel parcel) {
        return new x11(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public x11[] newArray(int i) {
        return new x11[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public x11 m10createFromParcel(Parcel parcel) {
        return new x11(parcel, null);
    }
}
