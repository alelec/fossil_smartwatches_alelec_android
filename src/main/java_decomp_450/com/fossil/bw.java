package com.fossil;

import android.content.Context;
import com.fossil.aw;
import com.fossil.nz;
import com.fossil.t30;
import com.fossil.vz;
import java.util.Collections;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bw {
    @DexIgnore
    public /* final */ Map<Class<?>, jw<?, ?>> a; // = new n4();
    @DexIgnore
    public jy b;
    @DexIgnore
    public dz c;
    @DexIgnore
    public az d;
    @DexIgnore
    public uz e;
    @DexIgnore
    public xz f;
    @DexIgnore
    public xz g;
    @DexIgnore
    public nz.a h;
    @DexIgnore
    public vz i;
    @DexIgnore
    public l30 j;
    @DexIgnore
    public int k; // = 4;
    @DexIgnore
    public aw.a l; // = new a(this);
    @DexIgnore
    public t30.b m;
    @DexIgnore
    public xz n;
    @DexIgnore
    public boolean o;
    @DexIgnore
    public List<q40<Object>> p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public boolean r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements aw.a {
        @DexIgnore
        public a(bw bwVar) {
        }

        @DexIgnore
        @Override // com.fossil.aw.a
        public r40 build() {
            return new r40();
        }
    }

    @DexIgnore
    public void a(t30.b bVar) {
        this.m = bVar;
    }

    @DexIgnore
    public aw a(Context context) {
        if (this.f == null) {
            this.f = xz.g();
        }
        if (this.g == null) {
            this.g = xz.e();
        }
        if (this.n == null) {
            this.n = xz.c();
        }
        if (this.i == null) {
            this.i = new vz.a(context).a();
        }
        if (this.j == null) {
            this.j = new n30();
        }
        if (this.c == null) {
            int b2 = this.i.b();
            if (b2 > 0) {
                this.c = new jz((long) b2);
            } else {
                this.c = new ez();
            }
        }
        if (this.d == null) {
            this.d = new iz(this.i.a());
        }
        if (this.e == null) {
            this.e = new tz((long) this.i.c());
        }
        if (this.h == null) {
            this.h = new sz(context);
        }
        if (this.b == null) {
            this.b = new jy(this.e, this.h, this.g, this.f, xz.h(), this.n, this.o);
        }
        List<q40<Object>> list = this.p;
        if (list == null) {
            this.p = Collections.emptyList();
        } else {
            this.p = Collections.unmodifiableList(list);
        }
        return new aw(context, this.b, this.e, this.c, this.d, new t30(this.m), this.j, this.k, this.l, this.a, this.p, this.q, this.r);
    }
}
