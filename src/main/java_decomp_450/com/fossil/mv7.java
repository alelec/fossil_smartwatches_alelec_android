package com.fossil;

import java.io.IOException;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mv7<T> implements tu7<T, RequestBody> {
    @DexIgnore
    public static /* final */ mv7<Object> a; // = new mv7<>();
    @DexIgnore
    public static /* final */ ho7 b; // = ho7.b("text/plain; charset=UTF-8");

    @DexIgnore
    @Override // com.fossil.tu7
    public RequestBody a(T t) throws IOException {
        return RequestBody.a(b, String.valueOf(t));
    }
}
