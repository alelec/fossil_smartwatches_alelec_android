package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kg0 extends lg0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public static /* final */ short c; // = 255;
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public kg0 createFromParcel(Parcel parcel) {
            return new kg0(yz0.b(parcel.readByte()));
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public kg0[] newArray(int i) {
            return new kg0[i];
        }
    }

    /*
    static {
        ud7 ud7 = ud7.a;
    }
    */

    @DexIgnore
    public kg0(short s) throws IllegalArgumentException {
        this.b = s;
        if (!(s >= 0 && c >= s)) {
            StringBuilder b2 = yh0.b("goalId(");
            b2.append((int) this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            throw new IllegalArgumentException(yh0.a(b2, c, "]."));
        }
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(new JSONObject(), r51.L3, Short.valueOf(this.b));
    }

    @DexIgnore
    @Override // com.fossil.lg0
    public int c() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.lg0
    public byte[] d() {
        ByteBuffer allocate = ByteBuffer.allocate(3);
        ee7.a((Object) allocate, "ByteBuffer.allocate(GOAL\u2026ACKING_TYPE_FRAME_LENGTH)");
        allocate.order(ByteOrder.LITTLE_ENDIAN);
        allocate.put(yr1.b.a);
        allocate.put((byte) 1);
        allocate.put((byte) this.b);
        byte[] array = allocate.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getGoalId() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) this.b);
    }
}
