package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r16 implements Factory<o16> {
    @DexIgnore
    public static o16 a(q16 q16) {
        o16 a = q16.a();
        c87.a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }
}
