package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.a12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class km2 extends n62<nl2> {
    @DexIgnore
    public /* final */ String E;
    @DexIgnore
    public /* final */ fm2<nl2> F; // = new lm2(this);

    @DexIgnore
    public km2(Context context, Looper looper, a12.b bVar, a12.c cVar, String str, j62 j62) {
        super(context, looper, 23, j62, bVar, cVar);
        this.E = str;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof nl2 ? (nl2) queryLocalInterface : new ol2(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String i() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    @DexIgnore
    @Override // com.fossil.v02.f, com.fossil.h62, com.fossil.n62
    public int k() {
        return 11925000;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String p() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }

    @DexIgnore
    @Override // com.fossil.h62
    public Bundle x() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.E);
        return bundle;
    }
}
