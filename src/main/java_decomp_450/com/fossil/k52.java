package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.u12;
import com.fossil.v02;
import com.fossil.v02.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k52<O extends v02.d> extends z02<O> {
    @DexIgnore
    public /* final */ v02.f j;
    @DexIgnore
    public /* final */ f52 k;
    @DexIgnore
    public /* final */ j62 l;
    @DexIgnore
    public /* final */ v02.a<? extends xn3, fn3> m;

    @DexIgnore
    public k52(Context context, v02<O> v02, Looper looper, v02.f fVar, f52 f52, j62 j62, v02.a<? extends xn3, fn3> aVar) {
        super(context, v02, looper);
        this.j = fVar;
        this.k = f52;
        this.l = j62;
        this.m = aVar;
        ((z02) this).i.a(this);
    }

    @DexIgnore
    @Override // com.fossil.z02
    public final v02.f a(Looper looper, u12.a<O> aVar) {
        this.k.a(aVar);
        return this.j;
    }

    @DexIgnore
    public final v02.f i() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.z02
    public final g42 a(Context context, Handler handler) {
        return new g42(context, handler, this.l, this.m);
    }
}
