package com.fossil;

import com.fossil.dz3;
import com.fossil.yz3;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ez3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends jz3<dz3.a<?>> {
        @DexIgnore
        /* renamed from: a */
        public int compare(dz3.a<?> aVar, dz3.a<?> aVar2) {
            return x04.a(aVar2.getCount(), aVar.getCount());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b<E> implements dz3.a<E> {
        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof dz3.a)) {
                return false;
            }
            dz3.a aVar = (dz3.a) obj;
            if (getCount() != aVar.getCount() || !gw3.a(getElement(), aVar.getElement())) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            E element = getElement();
            if (element == null) {
                i = 0;
            } else {
                i = element.hashCode();
            }
            return i ^ getCount();
        }

        @DexIgnore
        public String toString() {
            String valueOf = String.valueOf(getElement());
            int count = getCount();
            if (count == 1) {
                return valueOf;
            }
            return valueOf + " x " + count;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<E> extends yz3.a<E> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends h04<dz3.a<E>, E> {
            @DexIgnore
            public a(c cVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            @Override // com.fossil.h04
            public /* bridge */ /* synthetic */ Object a(Object obj) {
                return a((dz3.a) ((dz3.a) obj));
            }

            @DexIgnore
            public E a(dz3.a<E> aVar) {
                return aVar.getElement();
            }
        }

        @DexIgnore
        public abstract dz3<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().contains(obj);
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return a().containsAll(collection);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<E> iterator() {
            return new a(this, a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            return a().remove(obj, Integer.MAX_VALUE) > 0;
        }

        @DexIgnore
        public int size() {
            return a().entrySet().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<E> extends yz3.a<dz3.a<E>> {
        @DexIgnore
        public abstract dz3<E> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof dz3.a)) {
                return false;
            }
            dz3.a aVar = (dz3.a) obj;
            if (aVar.getCount() > 0 && a().count(aVar.getElement()) == aVar.getCount()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (obj instanceof dz3.a) {
                dz3.a aVar = (dz3.a) obj;
                E e = (E) aVar.getElement();
                int count = aVar.getCount();
                if (count != 0) {
                    return a().setCount(e, count, 0);
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<E> extends b<E> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ int count;
        @DexIgnore
        public /* final */ E element;

        @DexIgnore
        public e(E e, int i) {
            this.element = e;
            this.count = i;
            bx3.a(i, "count");
        }

        @DexIgnore
        @Override // com.fossil.dz3.a
        public final int getCount() {
            return this.count;
        }

        @DexIgnore
        @Override // com.fossil.dz3.a
        public final E getElement() {
            return this.element;
        }

        @DexIgnore
        public e<E> nextInBucket() {
            return null;
        }
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public static <E> dz3.a<E> a(E e2, int i) {
        return new e(e2, i);
    }

    @DexIgnore
    public static int b(Iterable<?> iterable) {
        if (iterable instanceof dz3) {
            return ((dz3) iterable).elementSet().size();
        }
        return 11;
    }

    @DexIgnore
    public static boolean c(dz3<?> dz3, Collection<?> collection) {
        jw3.a(collection);
        if (collection instanceof dz3) {
            collection = ((dz3) collection).elementSet();
        }
        return dz3.elementSet().retainAll(collection);
    }

    @DexIgnore
    public static boolean a(dz3<?> dz3, Object obj) {
        if (obj == dz3) {
            return true;
        }
        if (obj instanceof dz3) {
            dz3 dz32 = (dz3) obj;
            if (dz3.size() == dz32.size() && dz3.entrySet().size() == dz32.entrySet().size()) {
                for (dz3.a aVar : dz32.entrySet()) {
                    if (dz3.count(aVar.getElement()) != aVar.getCount()) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static boolean b(dz3<?> dz3, Collection<?> collection) {
        if (collection instanceof dz3) {
            collection = ((dz3) collection).elementSet();
        }
        return dz3.elementSet().removeAll(collection);
    }

    @DexIgnore
    public static <E> boolean a(dz3<E> dz3, Collection<? extends E> collection) {
        if (collection.isEmpty()) {
            return false;
        }
        if (collection instanceof dz3) {
            for (dz3.a<E> aVar : a(collection).entrySet()) {
                dz3.add(aVar.getElement(), aVar.getCount());
            }
            return true;
        }
        qy3.a(dz3, collection.iterator());
        return true;
    }

    @DexIgnore
    public static <E> int a(dz3<E> dz3, E e2, int i) {
        bx3.a(i, "count");
        int count = dz3.count(e2);
        int i2 = i - count;
        if (i2 > 0) {
            dz3.add(e2, i2);
        } else if (i2 < 0) {
            dz3.remove(e2, -i2);
        }
        return count;
    }

    @DexIgnore
    public static <E> boolean a(dz3<E> dz3, E e2, int i, int i2) {
        bx3.a(i, "oldCount");
        bx3.a(i2, "newCount");
        if (dz3.count(e2) != i) {
            return false;
        }
        dz3.setCount(e2, i2);
        return true;
    }

    @DexIgnore
    public static int a(dz3<?> dz3) {
        long j = 0;
        for (dz3.a<?> aVar : dz3.entrySet()) {
            j += (long) aVar.getCount();
        }
        return x04.a(j);
    }

    @DexIgnore
    public static <T> dz3<T> a(Iterable<T> iterable) {
        return (dz3) iterable;
    }
}
