package com.fossil;

import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr7 implements sr7 {
    @DexIgnore
    public /* final */ InputStream a;
    @DexIgnore
    public /* final */ tr7 b;

    @DexIgnore
    public hr7(InputStream inputStream, tr7 tr7) {
        ee7.b(inputStream, "input");
        ee7.b(tr7, "timeout");
        this.a = inputStream;
        this.b = tr7;
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public long b(yq7 yq7, long j) {
        ee7.b(yq7, "sink");
        int i = (j > 0 ? 1 : (j == 0 ? 0 : -1));
        if (i == 0) {
            return 0;
        }
        if (i >= 0) {
            try {
                this.b.e();
                nr7 b2 = yq7.b(1);
                int read = this.a.read(b2.a, b2.c, (int) Math.min(j, (long) (8192 - b2.c)));
                if (read != -1) {
                    b2.c += read;
                    long j2 = (long) read;
                    yq7.j(yq7.x() + j2);
                    return j2;
                } else if (b2.b != b2.c) {
                    return -1;
                } else {
                    yq7.a = b2.b();
                    or7.c.a(b2);
                    return -1;
                }
            } catch (AssertionError e) {
                if (ir7.a(e)) {
                    throw new IOException(e);
                }
                throw e;
            }
        } else {
            throw new IllegalArgumentException(("byteCount < 0: " + j).toString());
        }
    }

    @DexIgnore
    @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
    public void close() {
        this.a.close();
    }

    @DexIgnore
    @Override // com.fossil.sr7
    public tr7 d() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        return "source(" + this.a + ')';
    }
}
