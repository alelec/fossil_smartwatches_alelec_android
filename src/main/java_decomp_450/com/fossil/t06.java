package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t06 implements MembersInjector<CommuteTimeSettingsActivity> {
    @DexIgnore
    public static void a(CommuteTimeSettingsActivity commuteTimeSettingsActivity, i16 i16) {
        commuteTimeSettingsActivity.y = i16;
    }
}
