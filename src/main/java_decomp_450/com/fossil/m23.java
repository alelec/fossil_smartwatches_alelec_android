package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m23 implements tr2<p23> {
    @DexIgnore
    public static m23 b; // = new m23();
    @DexIgnore
    public /* final */ tr2<p23> a;

    @DexIgnore
    public m23(tr2<p23> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((p23) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((p23) b.zza()).zzb();
    }

    @DexIgnore
    public static boolean c() {
        return ((p23) b.zza()).zzc();
    }

    @DexIgnore
    public static boolean d() {
        return ((p23) b.zza()).zzd();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ p23 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public m23() {
        this(sr2.a(new o23()));
    }
}
