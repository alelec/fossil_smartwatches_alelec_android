package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface bb2 {
    @DexIgnore
    View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    @DexIgnore
    void a(Activity activity, Bundle bundle, Bundle bundle2);

    @DexIgnore
    void e();

    @DexIgnore
    void onCreate(Bundle bundle);

    @DexIgnore
    void onDestroy();

    @DexIgnore
    void onLowMemory();

    @DexIgnore
    void onPause();

    @DexIgnore
    void onResume();

    @DexIgnore
    void onSaveInstanceState(Bundle bundle);

    @DexIgnore
    void onStart();

    @DexIgnore
    void onStop();
}
