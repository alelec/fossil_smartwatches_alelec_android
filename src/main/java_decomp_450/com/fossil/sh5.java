package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.fossil.fl4;
import com.fossil.qh5;
import com.fossil.zv6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Calendar;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sh5 extends BroadcastReceiver {
    @DexIgnore
    public ch5 a;
    @DexIgnore
    public zv6 b;
    @DexIgnore
    public pd5 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<zv6.d, zv6.c> {
        @DexIgnore
        public /* final */ /* synthetic */ sh5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(sh5 sh5) {
            this.a = sh5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(zv6.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "Re-schedule SyncDataReminder");
            pd5 a2 = this.a.a();
            Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
            ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            a2.e(applicationContext);
        }

        @DexIgnore
        public void a(zv6.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("TimeTickReceiver", cVar.toString());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public sh5() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final pd5 a() {
        pd5 pd5 = this.c;
        if (pd5 != null) {
            return pd5;
        }
        ee7.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("TimeTickReceiver", "onReceive() - action = " + action);
        ch5 ch5 = this.a;
        if (ch5 != null) {
            long e = ch5.e(PortfolioApp.g0.c().c());
            if (!TextUtils.isEmpty(action) && ee7.a((Object) action, (Object) "android.intent.action.TIME_TICK") && System.currentTimeMillis() - e <= 120000) {
                qh5.a aVar = qh5.c;
                if (context != null) {
                    aVar.a(context);
                } else {
                    ee7.a();
                    throw null;
                }
            }
            if (ee7.a((Object) "android.intent.action.TIME_TICK", (Object) action)) {
                TimeZone timeZone = TimeZone.getDefault();
                try {
                    Calendar instance = Calendar.getInstance();
                    ee7.a((Object) instance, "calendar");
                    Calendar instance2 = Calendar.getInstance();
                    ee7.a((Object) instance2, "Calendar.getInstance()");
                    instance.setTimeInMillis(instance2.getTimeInMillis() - ((long) 60000));
                    Calendar instance3 = Calendar.getInstance();
                    ee7.a((Object) instance3, "Calendar.getInstance()");
                    if (timeZone.getOffset(instance3.getTimeInMillis()) != timeZone.getOffset(instance.getTimeInMillis())) {
                        FLogger.INSTANCE.getLocal().d("TimeTickReceiver", "DST Changed.");
                        zv6 zv6 = this.b;
                        if (zv6 != null) {
                            zv6.a(new zv6.b(), new b(this));
                        } else {
                            ee7.d("mDstChangeUseCase");
                            throw null;
                        }
                    }
                } catch (Exception e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.e("TimeTickReceiver", ".timeZoneChangeReceiver - ex=" + e2);
                }
            }
        } else {
            ee7.d("mSharedPreferencesManager");
            throw null;
        }
    }
}
