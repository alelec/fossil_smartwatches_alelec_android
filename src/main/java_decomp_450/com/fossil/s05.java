package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s05 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ View B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ ConstraintLayout D;
    @DexIgnore
    public /* final */ ViewPager2 E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ FlexibleTextView J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ CustomizeWidget L;
    @DexIgnore
    public /* final */ CustomizeWidget M;
    @DexIgnore
    public /* final */ CustomizeWidget N;
    @DexIgnore
    public /* final */ CustomizeWidget O;
    @DexIgnore
    public /* final */ CustomizeWidget P;
    @DexIgnore
    public /* final */ CustomizeWidget Q;
    @DexIgnore
    public /* final */ CustomizeWidget R;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ CardView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    public s05(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, CardView cardView, FlexibleTextView flexibleTextView, View view3, View view4, ImageView imageView, ImageView imageView2, View view5, View view6, View view7, ConstraintLayout constraintLayout4, ViewPager2 viewPager2, RTLImageView rTLImageView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, View view8, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3, CustomizeWidget customizeWidget4, CustomizeWidget customizeWidget5, CustomizeWidget customizeWidget6, CustomizeWidget customizeWidget7) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = cardView;
        this.v = flexibleTextView;
        this.w = view3;
        this.x = view4;
        this.y = imageView;
        this.z = imageView2;
        this.A = view5;
        this.B = view6;
        this.C = view7;
        this.D = constraintLayout4;
        this.E = viewPager2;
        this.F = rTLImageView;
        this.G = flexibleTextView2;
        this.H = flexibleTextView3;
        this.I = flexibleTextView4;
        this.J = flexibleTextView5;
        this.K = view8;
        this.L = customizeWidget;
        this.M = customizeWidget2;
        this.N = customizeWidget3;
        this.O = customizeWidget4;
        this.P = customizeWidget5;
        this.Q = customizeWidget6;
        this.R = customizeWidget7;
    }
}
