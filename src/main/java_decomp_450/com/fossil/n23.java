package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n23 implements k23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", true);

    @DexIgnore
    @Override // com.fossil.k23
    public final boolean zza() {
        return a.b().booleanValue();
    }
}
