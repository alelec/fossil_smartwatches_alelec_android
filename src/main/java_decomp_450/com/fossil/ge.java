package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ge {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements zd<X> {
        @DexIgnore
        public /* final */ /* synthetic */ xd a;
        @DexIgnore
        public /* final */ /* synthetic */ t3 b;

        @DexIgnore
        public a(xd xdVar, t3 t3Var) {
            this.a = xdVar;
            this.b = t3Var;
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(X x) {
            this.a.b(this.b.apply(x));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements zd<X> {
        @DexIgnore
        public LiveData<Y> a;
        @DexIgnore
        public /* final */ /* synthetic */ t3 b;
        @DexIgnore
        public /* final */ /* synthetic */ xd c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements zd<Y> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            @Override // com.fossil.zd
            public void onChanged(Y y) {
                b.this.c.b((Object) y);
            }
        }

        @DexIgnore
        public b(t3 t3Var, xd xdVar) {
            this.b = t3Var;
            this.c = xdVar;
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.a((LiveData) liveData2);
                }
                this.a = liveData;
                if (liveData != null) {
                    this.c.a(liveData, new a());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements zd<X> {
        @DexIgnore
        public boolean a; // = true;
        @DexIgnore
        public /* final */ /* synthetic */ xd b;

        @DexIgnore
        public c(xd xdVar) {
            this.b = xdVar;
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(X x) {
            Object a2 = this.b.a();
            if (this.a || ((a2 == null && x != null) || (a2 != null && !a2.equals(x)))) {
                this.a = false;
                this.b.b((Object) x);
            }
        }
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> a(LiveData<X> liveData, t3<X, Y> t3Var) {
        xd xdVar = new xd();
        xdVar.a(liveData, new a(xdVar, t3Var));
        return xdVar;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, t3<X, LiveData<Y>> t3Var) {
        xd xdVar = new xd();
        xdVar.a(liveData, new b(t3Var, xdVar));
        return xdVar;
    }

    @DexIgnore
    public static <X> LiveData<X> a(LiveData<X> liveData) {
        xd xdVar = new xd();
        xdVar.a(liveData, new c(xdVar));
        return xdVar;
    }
}
