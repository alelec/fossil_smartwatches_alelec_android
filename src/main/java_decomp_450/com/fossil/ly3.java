package com.fossil;

import com.fossil.by3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.SortedMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ly3<K, V> extends my3<K, V> implements NavigableMap<K, V> {
    @DexIgnore
    public static /* final */ Comparator<Comparable> h; // = jz3.natural();
    @DexIgnore
    public static /* final */ ly3<Comparable, Object> i; // = new ly3<>(ny3.emptySet(jz3.natural()), zx3.of());
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient tz3<K> e;
    @DexIgnore
    public /* final */ transient zx3<V> f;
    @DexIgnore
    public transient ly3<K, V> g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends dy3<K, V> {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ly3$a$a")
        /* renamed from: com.fossil.ly3$a$a  reason: collision with other inner class name */
        public class C0119a extends tx3<Map.Entry<K, V>> {
            @DexIgnore
            public C0119a() {
            }

            @DexIgnore
            @Override // com.fossil.tx3
            public vx3<Map.Entry<K, V>> delegateCollection() {
                return a.this;
            }

            @DexIgnore
            @Override // java.util.List
            public Map.Entry<K, V> get(int i) {
                return yy3.a(ly3.this.e.asList().get(i), ly3.this.f.get(i));
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.iy3
        public zx3<Map.Entry<K, V>> createAsList() {
            return new C0119a();
        }

        @DexIgnore
        @Override // com.fossil.dy3
        public by3<K, V> map() {
            return ly3.this;
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
        public j04<Map.Entry<K, V>> iterator() {
            return asList().iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends by3.e {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Comparator<Object> comparator;

        @DexIgnore
        public c(ly3<?, ?> ly3) {
            super(ly3);
            this.comparator = ly3.comparator();
        }

        @DexIgnore
        @Override // com.fossil.by3.e
        public Object readResolve() {
            return createMap(new b(this.comparator));
        }
    }

    @DexIgnore
    public ly3(tz3<K> tz3, zx3<V> zx3) {
        this(tz3, zx3, null);
    }

    @DexIgnore
    public static <K, V> ly3<K, V> a(Comparator<? super K> comparator, K k, V v) {
        zx3 of = zx3.of(k);
        jw3.a(comparator);
        return new ly3<>(new tz3(of, comparator), zx3.of(v));
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ly3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        return a(map, (jz3) h);
    }

    @DexIgnore
    public static <K, V> ly3<K, V> copyOfSorted(SortedMap<K, ? extends V> sortedMap) {
        Comparator<? super K> comparator = sortedMap.comparator();
        if (comparator == null) {
            comparator = h;
        }
        if (sortedMap instanceof ly3) {
            ly3<K, V> ly3 = (ly3) sortedMap;
            if (!ly3.isPartialView()) {
                return ly3;
            }
        }
        return a((Comparator) comparator, true, (Iterable) sortedMap.entrySet());
    }

    @DexIgnore
    public static <K, V> ly3<K, V> emptyMap(Comparator<? super K> comparator) {
        if (jz3.natural().equals(comparator)) {
            return of();
        }
        return new ly3<>(ny3.emptySet(comparator), zx3.of());
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> naturalOrder() {
        return new b<>(jz3.natural());
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ly3<K, V> of() {
        return (ly3<K, V>) i;
    }

    @DexIgnore
    public static <K, V> b<K, V> orderedBy(Comparator<K> comparator) {
        return new b<>(comparator);
    }

    @DexIgnore
    public static <K extends Comparable<?>, V> b<K, V> reverseOrder() {
        return new b<>(jz3.natural().reverse());
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> ceilingEntry(K k) {
        return tailMap((Object) k, true).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K ceilingKey(K k) {
        return (K) yy3.a(ceilingEntry(k));
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public Comparator<? super K> comparator() {
        return keySet().comparator();
    }

    @DexIgnore
    @Override // com.fossil.by3
    public iy3<Map.Entry<K, V>> createEntrySet() {
        return isEmpty() ? iy3.of() : new a();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> firstEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(0);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K firstKey() {
        return keySet().first();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> floorEntry(K k) {
        return headMap((Object) k, true).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K floorKey(K k) {
        return (K) yy3.a(floorEntry(k));
    }

    @DexIgnore
    @Override // java.util.Map, com.fossil.by3
    public V get(Object obj) {
        int indexOf = this.e.indexOf(obj);
        if (indexOf == -1) {
            return null;
        }
        return this.f.get(indexOf);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> higherEntry(K k) {
        return tailMap((Object) k, false).firstEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K higherKey(K k) {
        return (K) yy3.a(higherEntry(k));
    }

    @DexIgnore
    @Override // com.fossil.by3
    public boolean isPartialView() {
        return this.e.isPartialView() || this.f.isPartialView();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lastEntry() {
        if (isEmpty()) {
            return null;
        }
        return entrySet().asList().get(size() - 1);
    }

    @DexIgnore
    @Override // java.util.SortedMap
    public K lastKey() {
        return keySet().last();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public Map.Entry<K, V> lowerEntry(K k) {
        return headMap((Object) k, false).lastEntry();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public K lowerKey(K k) {
        return (K) yy3.a(lowerEntry(k));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollFirstEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    @CanIgnoreReturnValue
    @Deprecated
    public final Map.Entry<K, V> pollLastEntry() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public int size() {
        return this.f.size();
    }

    @DexIgnore
    @Override // com.fossil.by3
    public Object writeReplace() {
        return new c(this);
    }

    @DexIgnore
    public ly3(tz3<K> tz3, zx3<V> zx3, ly3<K, V> ly3) {
        this.e = tz3;
        this.f = zx3;
        this.g = ly3;
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> a(cy3<K, V>... cy3Arr) {
        return a(jz3.natural(), false, cy3Arr, cy3Arr.length);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> of(K k, V v) {
        return a(jz3.natural(), k, v);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ny3<K> descendingKeySet() {
        return this.e.descendingSet();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ly3<K, V> descendingMap() {
        ly3<K, V> ly3 = this.g;
        if (ly3 != null) {
            return ly3;
        }
        if (isEmpty()) {
            return emptyMap(jz3.from(comparator()).reverse());
        }
        return new ly3<>((tz3) this.e.descendingSet(), this.f.reverse(), this);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.SortedMap, com.fossil.by3, com.fossil.by3
    public iy3<Map.Entry<K, V>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ny3<K> navigableKeySet() {
        return this.e;
    }

    @DexIgnore
    @Override // java.util.Map, java.util.SortedMap, com.fossil.by3, com.fossil.by3
    public vx3<V> values() {
        return this.f;
    }

    @DexIgnore
    public static <K, V> ly3<K, V> a(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        boolean z = false;
        if (map instanceof SortedMap) {
            Comparator<? super K> comparator2 = ((SortedMap) map).comparator();
            if (comparator2 != null) {
                z = comparator.equals(comparator2);
            } else if (comparator == h) {
                z = true;
            }
        }
        if (z && (map instanceof ly3)) {
            ly3<K, V> ly3 = (ly3) map;
            if (!ly3.isPartialView()) {
                return ly3;
            }
        }
        return a(comparator, z, map.entrySet());
    }

    @DexIgnore
    public static <K, V> ly3<K, V> copyOf(Map<? extends K, ? extends V> map, Comparator<? super K> comparator) {
        jw3.a(comparator);
        return a(map, comparator);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> of(K k, V v, K k2, V v2) {
        return a(by3.entryOf(k, v), by3.entryOf(k2, v2));
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public ly3<K, V> headMap(K k) {
        return headMap((Object) k, false);
    }

    @DexIgnore
    @Override // java.util.Map, java.util.SortedMap, com.fossil.by3, com.fossil.by3
    public ny3<K> keySet() {
        return this.e;
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public ly3<K, V> subMap(K k, K k2) {
        return subMap((Object) k, true, (Object) k2, false);
    }

    @DexIgnore
    @Override // java.util.NavigableMap, java.util.SortedMap
    public ly3<K, V> tailMap(K k) {
        return tailMap((Object) k, true);
    }

    @DexIgnore
    @Override // com.fossil.by3
    public static <K, V> ly3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        return copyOf(iterable, (jz3) h);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return a(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ly3<K, V> headMap(K k, boolean z) {
        tz3<K> tz3 = this.e;
        jw3.a(k);
        return a(0, tz3.headIndex(k, z));
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ly3<K, V> subMap(K k, boolean z, K k2, boolean z2) {
        jw3.a(k);
        jw3.a(k2);
        jw3.a(comparator().compare(k, k2) <= 0, "expected fromKey <= toKey but %s > %s", k, k2);
        return headMap((Object) k2, z2).tailMap((Object) k, z);
    }

    @DexIgnore
    @Override // java.util.NavigableMap
    public ly3<K, V> tailMap(K k, boolean z) {
        tz3<K> tz3 = this.e;
        jw3.a(k);
        return a(tz3.tailIndex(k, z), size());
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends by3.b<K, V> {
        @DexIgnore
        public /* final */ Comparator<? super K> e;

        @DexIgnore
        public b(Comparator<? super K> comparator) {
            jw3.a(comparator);
            this.e = comparator;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public b<K, V> a(K k, V v) {
            super.a((Object) k, (Object) v);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public b<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            super.a((Map.Entry) entry);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public b<K, V> a(Map<? extends K, ? extends V> map) {
            super.a((Map) map);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        @CanIgnoreReturnValue
        public b<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a((Iterable) iterable);
            return this;
        }

        @DexIgnore
        @Override // com.fossil.by3.b
        public ly3<K, V> a() {
            int i = ((by3.b) this).c;
            if (i == 0) {
                return ly3.emptyMap(this.e);
            }
            if (i != 1) {
                return ly3.a(this.e, false, ((by3.b) this).b, i);
            }
            return ly3.a(this.e, ((by3.b) this).b[0].getKey(), ((by3.b) this).b[0].getValue());
        }
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return a(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3), by3.entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> ly3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable, Comparator<? super K> comparator) {
        jw3.a(comparator);
        return a((Comparator) comparator, false, (Iterable) iterable);
    }

    @DexIgnore
    public static <K extends Comparable<? super K>, V> ly3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return a(by3.entryOf(k, v), by3.entryOf(k2, v2), by3.entryOf(k3, v3), by3.entryOf(k4, v4), by3.entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> ly3<K, V> a(Comparator<? super K> comparator, boolean z, Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) py3.a((Iterable) iterable, (Object[]) by3.EMPTY_ENTRY_ARRAY);
        return a(comparator, z, entryArr, entryArr.length);
    }

    @DexIgnore
    public static <K, V> ly3<K, V> a(Comparator<? super K> comparator, boolean z, Map.Entry<K, V>[] entryArr, int i2) {
        if (i2 == 0) {
            return emptyMap(comparator);
        }
        if (i2 == 1) {
            return a(comparator, entryArr[0].getKey(), entryArr[0].getValue());
        }
        Object[] objArr = new Object[i2];
        Object[] objArr2 = new Object[i2];
        if (z) {
            for (int i3 = 0; i3 < i2; i3++) {
                K key = entryArr[i3].getKey();
                V value = entryArr[i3].getValue();
                bx3.a(key, value);
                objArr[i3] = key;
                objArr2[i3] = value;
            }
        } else {
            Arrays.sort(entryArr, 0, i2, jz3.from(comparator).onKeys());
            K key2 = entryArr[0].getKey();
            objArr[0] = key2;
            objArr2[0] = entryArr[0].getValue();
            int i4 = 1;
            while (i4 < i2) {
                K key3 = entryArr[i4].getKey();
                V value2 = entryArr[i4].getValue();
                bx3.a(key3, value2);
                objArr[i4] = key3;
                objArr2[i4] = value2;
                by3.checkNoConflict(comparator.compare(key2, key3) != 0, "key", entryArr[i4 - 1], entryArr[i4]);
                i4++;
                key2 = key3;
            }
        }
        return new ly3<>(new tz3(new pz3(objArr), comparator), new pz3(objArr2));
    }

    @DexIgnore
    public final ly3<K, V> a(int i2, int i3) {
        if (i2 == 0 && i3 == size()) {
            return this;
        }
        if (i2 == i3) {
            return emptyMap(comparator());
        }
        return new ly3<>(this.e.getSubSet(i2, i3), this.f.subList(i2, i3));
    }
}
