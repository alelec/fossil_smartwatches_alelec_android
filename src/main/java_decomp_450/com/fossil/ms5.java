package com.fossil;

import dagger.internal.Factory;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms5 implements Factory<WeakReference<nh6>> {
    @DexIgnore
    public static WeakReference<nh6> a(gs5 gs5) {
        WeakReference<nh6> f = gs5.f();
        c87.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }
}
