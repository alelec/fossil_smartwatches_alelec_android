package com.fossil;

import com.fossil.fitness.SleepState;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tx6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            ee7.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Integer invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Integer.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends fe7 implements gd7<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            ee7.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Long invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Long.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore
    public static final List<r87<Long, Long>> a(List<FitnessDataWrapper> list) {
        ee7.b(list, "$this$getActiveTimeList");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        ea7.a((Iterable) list, (Comparator) new a());
        for (T t : list) {
            DateTime startTimeTZ = t.getStartTimeTZ();
            int resolutionInSecond = t.getActiveMinute().getResolutionInSecond();
            if (!t.getActiveMinute().getValues().isEmpty()) {
                int i = 0;
                boolean z = false;
                for (T t2 : t.getActiveMinute().getValues()) {
                    int i2 = i + 1;
                    if (i >= 0) {
                        boolean booleanValue = t2.booleanValue();
                        if (booleanValue && booleanValue != z) {
                            startTimeTZ = startTimeTZ.plus(((long) (i * resolutionInSecond)) * 1000);
                            ee7.a((Object) startTimeTZ, "startTime.plus(index * resolutionInSecond * 1000L)");
                        }
                        if (!booleanValue && booleanValue != z) {
                            arrayList.add(new r87(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf(t.getStartLongTime() + (((long) (i * resolutionInSecond)) * 1000))));
                        }
                        z = booleanValue;
                        i = i2;
                    } else {
                        w97.c();
                        throw null;
                    }
                }
                if (((Boolean) ea7.f((List) t.getActiveMinute().getValues())).booleanValue()) {
                    arrayList.add(new r87(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf(t.getStartLongTime() + (((long) (t.getActiveMinute().getValues().size() * resolutionInSecond)) * 1000))));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<WrapperTapEventSummary> b(List<FitnessDataWrapper> list) {
        ee7.b(list, "$this$getGoalTrackings");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        ea7.a((Iterable) list, (Comparator) new b());
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            Iterator<T> it2 = it.next().getGoalTrackings().iterator();
            while (it2.hasNext()) {
                arrayList.add(it2.next());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final v87<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a(List<FitnessDataWrapper> list, String str, String str2, long j) {
        String str3;
        ArrayList arrayList;
        TimeZone timeZone;
        String str4;
        ArrayList arrayList2;
        int i;
        int i2;
        String str5;
        ActivitySample activitySample;
        ArrayList arrayList3;
        String str6;
        ArrayList arrayList4;
        String str7;
        String str8;
        String str9;
        ArrayList arrayList5;
        String str10;
        ArrayList arrayList6;
        ArrayList arrayList7;
        String str11;
        TimeZone timeZone2;
        String str12;
        TimeZone timeZone3;
        String str13;
        ArrayList arrayList8;
        TimeZone timeZone4;
        String str14;
        ArrayList arrayList9;
        int i3;
        int i4;
        int i5;
        String str15;
        ArrayList arrayList10;
        String str16;
        double d2;
        ActivityIntensities activityIntensities;
        ArrayList arrayList11;
        ArrayList arrayList12;
        TimeZone timeZone5;
        List<FitnessDataWrapper> list2 = list;
        ee7.b(list2, "$this$getListActivityData");
        ee7.b(str, "serial");
        ee7.b(str2, ButtonService.USER_ID);
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "getListActivityData");
        ArrayList arrayList13 = new ArrayList();
        ArrayList arrayList14 = new ArrayList();
        ArrayList arrayList15 = new ArrayList();
        if (list.isEmpty()) {
            return new v87<>(arrayList13, arrayList14, arrayList15);
        }
        ea7.a((Iterable) list2, bb7.a(c.INSTANCE, d.INSTANCE));
        FitnessDataWrapper fitnessDataWrapper = list2.get(0);
        int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
        DateTime withMillisOfSecond = fitnessDataWrapper.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
        DateTime plusMinutes = withMillisOfSecond.plusMinutes(1);
        ee7.a((Object) withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        int hourOfDay = withMillisOfSecond.getHourOfDay();
        ee7.a((Object) withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        int minuteOfHour = withMillisOfSecond.getMinuteOfHour();
        TimeZone a2 = zd5.a(timezoneOffsetInSecond);
        Date date = withMillisOfSecond.toLocalDateTime().toDate();
        ee7.a((Object) date, "startTime.toLocalDateTime().toDate()");
        ee7.a((Object) withMillisOfSecond, SampleRaw.COLUMN_START_TIME);
        ee7.a((Object) plusMinutes, SampleRaw.COLUMN_END_TIME);
        String str17 = SampleRaw.COLUMN_END_TIME;
        String str18 = "startTime.toLocalDateTime().toDate()";
        ArrayList arrayList16 = arrayList15;
        ArrayList arrayList17 = arrayList14;
        String str19 = SampleRaw.COLUMN_START_TIME;
        String str20 = "SyncDataExtensions";
        ActivitySample activitySample2 = new ActivitySample(str2, date, withMillisOfSecond, plusMinutes, 0.0d, 0.0d, 0.0d, 0, new ActivityIntensities(), timezoneOffsetInSecond, str, j);
        DateTime startTimeTZ = fitnessDataWrapper.getStartTimeTZ();
        double steps = activitySample2.getSteps();
        double calories = activitySample2.getCalories();
        double distance = activitySample2.getDistance();
        int activeTime = activitySample2.getActiveTime();
        List<Integer> intensitiesInArray = activitySample2.getIntensityDistInSteps().getIntensitiesInArray();
        String str21 = "timeZone";
        ee7.a((Object) a2, str21);
        ActivitySummary a3 = hx6.a(startTimeTZ, steps, calories, distance, activeTime, intensitiesInArray, a2.getID());
        int size = list.size();
        TimeZone timeZone6 = a2;
        int i6 = 0;
        for (T t : list) {
            int i7 = i6 + 1;
            ActivitySummary activitySummary = null;
            if (i6 >= 0) {
                T t2 = t;
                int timezoneOffsetInSecond2 = t2.getTimezoneOffsetInSecond();
                FitnessDataWrapper fitnessDataWrapper2 = i7 < size ? list2.get(i7) : null;
                boolean z = fitnessDataWrapper2 == null || timezoneOffsetInSecond2 != fitnessDataWrapper2.getTimezoneOffsetInSecond();
                int size2 = t2.getStep().getValues().size();
                int size3 = t2.getCalorie().getValues().size();
                int size4 = t2.getDistance().getValues().size();
                int size5 = t2.getActiveMinute().getValues().size();
                String str22 = "currentSampleRaw.startTi\u2026nuteOfHour(currentMinute)";
                String str23 = "currentActivitySummary";
                if (size2 == size3 && size3 == size4 && size4 == size5) {
                    List<Short> values = t2.getStep().getValues();
                    int resolutionInSecond = t2.getStep().getResolutionInSecond();
                    int i8 = hourOfDay;
                    activitySample = activitySample2;
                    int i9 = 0;
                    for (T t3 : values) {
                        int i10 = i9 + 1;
                        if (i9 >= 0) {
                            short shortValue = t3.shortValue();
                            DateTime withMillisOfSecond2 = t2.getStartTimeTZ().plusMillis(i9 * resolutionInSecond * 1000).withSecondOfMinute(0).withMillisOfSecond(0);
                            DateTime plusSeconds = withMillisOfSecond2.plusSeconds(resolutionInSecond);
                            ee7.a((Object) withMillisOfSecond2, "currentStartTime");
                            int hourOfDay2 = withMillisOfSecond2.getHourOfDay();
                            int minuteOfHour2 = withMillisOfSecond2.getMinuteOfHour();
                            float floatValue = t2.getCalorie().getValues().get(i9).floatValue();
                            double doubleValue = t2.getDistance().getValues().get(i9).doubleValue();
                            ActivityIntensities activityIntensities2 = new ActivityIntensities();
                            double d3 = (double) shortValue;
                            activityIntensities2.setIntensity(d3);
                            int resolutionInSecond2 = t2.getActiveMinute().getValues().get(i9).booleanValue() ? t2.getActiveMinute().getResolutionInSecond() / 60 : 0;
                            if (hourOfDay2 == i8 && minuteOfHour2 / 15 == minuteOfHour / 15) {
                                activitySample.setSteps(activitySample.getSteps() + d3);
                                activitySample.setCalories(activitySample.getCalories() + ((double) floatValue));
                                activitySample.setDistance(activitySample.getDistance() + doubleValue);
                                ee7.a((Object) plusSeconds, "currentEndTime");
                                activitySample.setEndTime(plusSeconds);
                                activitySample.setActiveTime(activitySample.getActiveTime() + resolutionInSecond2);
                                ActivityIntensities intensityDistInSteps = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps.setLight(intensityDistInSteps.getLight() + activityIntensities2.getLight());
                                ActivityIntensities intensityDistInSteps2 = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps2.setModerate(intensityDistInSteps2.getModerate() + activityIntensities2.getModerate());
                                ActivityIntensities intensityDistInSteps3 = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps3.setIntense(intensityDistInSteps3.getIntense() + activityIntensities2.getIntense());
                                arrayList9 = arrayList13;
                                str15 = str22;
                                str14 = str23;
                                str13 = str21;
                                i4 = size;
                                arrayList8 = arrayList17;
                                arrayList10 = arrayList16;
                                str16 = str20;
                                i3 = timezoneOffsetInSecond2;
                                timeZone4 = timeZone6;
                                i5 = i7;
                            } else {
                                if (a(activitySample)) {
                                    d2 = d3;
                                    activityIntensities = activityIntensities2;
                                    arrayList11 = arrayList16;
                                    arrayList11.add(new GFitSample(af7.a(activitySample.getSteps()), (float) activitySample.getDistance(), (float) activitySample.getCalories(), activitySample.getStartTime().getMillis(), activitySample.getEndTime().getMillis()));
                                    int minuteOfHour3 = activitySample.getStartTime().getMinuteOfHour();
                                    DateTime withMinuteOfHour = activitySample.getStartTime().withMinuteOfHour(minuteOfHour3 - (minuteOfHour3 % 15));
                                    ee7.a((Object) withMinuteOfHour, str22);
                                    activitySample.setStartTimeId(withMinuteOfHour);
                                    arrayList13.add(activitySample);
                                    if (zd5.d(a3.getDate(), activitySample.getDate())) {
                                        ee7.a((Object) a3, str23);
                                        a(a3, activitySample);
                                        arrayList12 = arrayList17;
                                        timeZone5 = timeZone6;
                                    } else {
                                        ee7.a((Object) a3, str23);
                                        arrayList12 = arrayList17;
                                        arrayList12.add(a3);
                                        DateTime dateTime = new DateTime(activitySample.getDate());
                                        double steps2 = activitySample.getSteps();
                                        double calories2 = activitySample.getCalories();
                                        double distance2 = activitySample.getDistance();
                                        int activeTime2 = activitySample.getActiveTime();
                                        List<Integer> intensitiesInArray2 = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
                                        timeZone5 = timeZone6;
                                        ee7.a((Object) timeZone5, str21);
                                        a3 = hx6.a(dateTime, steps2, calories2, distance2, activeTime2, intensitiesInArray2, timeZone5.getID());
                                    }
                                } else {
                                    d2 = d3;
                                    arrayList11 = arrayList16;
                                    timeZone5 = timeZone6;
                                    activityIntensities = activityIntensities2;
                                    arrayList12 = arrayList17;
                                }
                                int hourOfDay3 = withMillisOfSecond2.getHourOfDay();
                                int minuteOfHour4 = withMillisOfSecond2.getMinuteOfHour();
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                local.d(str20, "getListActivityData - startTime=" + withMillisOfSecond2 + ", endTime=" + plusSeconds);
                                Date date2 = withMillisOfSecond2.toLocalDateTime().toDate();
                                ee7.a((Object) date2, "currentStartTime.toLocalDateTime().toDate()");
                                ee7.a((Object) plusSeconds, "currentEndTime");
                                str16 = str20;
                                str14 = str23;
                                arrayList9 = arrayList13;
                                i3 = timezoneOffsetInSecond2;
                                i5 = i7;
                                timeZone4 = timeZone5;
                                str15 = str22;
                                arrayList10 = arrayList11;
                                arrayList8 = arrayList12;
                                i4 = size;
                                str13 = str21;
                                ActivitySample activitySample3 = new ActivitySample(str2, date2, withMillisOfSecond2, plusSeconds, d2, (double) floatValue, doubleValue, resolutionInSecond2, activityIntensities, i3, str, j);
                                a3 = a3;
                                i8 = hourOfDay3;
                                minuteOfHour = minuteOfHour4;
                                activitySample = activitySample3;
                            }
                            i97 i97 = i97.a;
                            str20 = str16;
                            arrayList16 = arrayList10;
                            i9 = i10;
                            str22 = str15;
                            resolutionInSecond = resolutionInSecond;
                            i7 = i5;
                            size = i4;
                            timezoneOffsetInSecond2 = i3;
                            arrayList13 = arrayList9;
                            str23 = str14;
                            timeZone6 = timeZone4;
                            arrayList17 = arrayList8;
                            str21 = str13;
                        } else {
                            w97.c();
                            throw null;
                        }
                    }
                    arrayList2 = arrayList13;
                    str5 = str22;
                    str4 = str23;
                    i2 = i7;
                    timeZone = timeZone6;
                    i = size;
                    str3 = str21;
                    arrayList = arrayList17;
                    arrayList3 = arrayList16;
                    str6 = str20;
                    hourOfDay = i8;
                } else {
                    arrayList2 = arrayList13;
                    str5 = str22;
                    str4 = str23;
                    i2 = i7;
                    timeZone = timeZone6;
                    i = size;
                    str3 = str21;
                    arrayList = arrayList17;
                    arrayList3 = arrayList16;
                    str6 = str20;
                    FLogger.INSTANCE.getLocal().e(str6, "getSampleRawList(), steps, calories and distances data sizes do not match from SDK");
                    activitySample = activitySample2;
                }
                if (z) {
                    if (a(activitySample)) {
                        arrayList3.add(new GFitSample(af7.a(activitySample.getSteps()), (float) activitySample.getDistance(), (float) activitySample.getCalories(), activitySample.getStartTime().getMillis(), activitySample.getEndTime().getMillis()));
                        int minuteOfHour5 = activitySample.getStartTime().getMinuteOfHour();
                        DateTime withMinuteOfHour2 = activitySample.getStartTime().withMinuteOfHour(minuteOfHour5 - (minuteOfHour5 % 15));
                        ee7.a((Object) withMinuteOfHour2, str5);
                        activitySample.setStartTimeId(withMinuteOfHour2);
                        arrayList6 = arrayList2;
                        arrayList6.add(activitySample);
                        if (zd5.d(a3.getDate(), activitySample.getDate())) {
                            ee7.a((Object) a3, str4);
                            a(a3, activitySample);
                            arrayList7 = arrayList;
                            arrayList7.add(a3);
                            timeZone2 = timeZone;
                            str11 = str3;
                        } else {
                            arrayList7 = arrayList;
                            ee7.a((Object) a3, str4);
                            arrayList7.add(a3);
                            DateTime dateTime2 = new DateTime(activitySample.getDate());
                            double steps3 = activitySample.getSteps();
                            double calories3 = activitySample.getCalories();
                            double distance3 = activitySample.getDistance();
                            int activeTime3 = activitySample.getActiveTime();
                            List<Integer> intensitiesInArray3 = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
                            timeZone2 = timeZone;
                            str11 = str3;
                            ee7.a((Object) timeZone2, str11);
                            ActivitySummary a4 = hx6.a(dateTime2, steps3, calories3, distance3, activeTime3, intensitiesInArray3, timeZone2.getID());
                            ee7.a((Object) a4, "Interpolator.createActiv\u2026esInArray(), timeZone.id)");
                            arrayList7.add(a4);
                        }
                    } else {
                        arrayList6 = arrayList2;
                        timeZone2 = timeZone;
                        arrayList7 = arrayList;
                        str11 = str3;
                        ee7.a((Object) a3, str4);
                        arrayList7.add(a3);
                    }
                    if (fitnessDataWrapper2 != null) {
                        DateTime withMillisOfSecond3 = fitnessDataWrapper2.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
                        DateTime plusMinutes2 = withMillisOfSecond3.plusMinutes(1);
                        ee7.a((Object) withMillisOfSecond3, str19);
                        hourOfDay = withMillisOfSecond3.getHourOfDay();
                        ee7.a((Object) withMillisOfSecond3, str19);
                        minuteOfHour = withMillisOfSecond3.getMinuteOfHour();
                        TimeZone a5 = zd5.a(fitnessDataWrapper2.getTimezoneOffsetInSecond());
                        Date date3 = withMillisOfSecond3.toLocalDateTime().toDate();
                        ee7.a((Object) date3, str18);
                        ee7.a((Object) withMillisOfSecond3, str19);
                        ee7.a((Object) plusMinutes2, str17);
                        str9 = str17;
                        str8 = str19;
                        str7 = str18;
                        arrayList4 = arrayList6;
                        ActivitySample activitySample4 = new ActivitySample(str2, date3, withMillisOfSecond3, plusMinutes2, 0.0d, 0.0d, 0.0d, 0, new ActivityIntensities(), timezoneOffsetInSecond, str, j);
                        Iterator it = arrayList7.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                break;
                            }
                            Object next = it.next();
                            if (zd5.d(((ActivitySummary) next).getDate(), a(fitnessDataWrapper2.getStartTimeTZ(), true))) {
                                activitySummary = next;
                                break;
                            }
                        }
                        ActivitySummary activitySummary2 = activitySummary;
                        if (activitySummary2 == null) {
                            DateTime startTimeTZ2 = fitnessDataWrapper2.getStartTimeTZ();
                            double steps4 = activitySample4.getSteps();
                            double calories4 = activitySample4.getCalories();
                            double distance4 = activitySample4.getDistance();
                            int activeTime4 = activitySample4.getActiveTime();
                            List<Integer> intensitiesInArray4 = activitySample4.getIntensityDistInSteps().getIntensitiesInArray();
                            timeZone3 = a5;
                            str12 = str11;
                            ee7.a((Object) timeZone3, str12);
                            activitySummary2 = hx6.a(startTimeTZ2, steps4, calories4, distance4, activeTime4, intensitiesInArray4, timeZone3.getID());
                            i97 i972 = i97.a;
                            arrayList5 = arrayList7;
                        } else {
                            arrayList5 = arrayList7;
                            timeZone3 = a5;
                            str12 = str11;
                            arrayList5.remove(activitySummary2);
                        }
                        timeZone6 = timeZone3;
                        a3 = activitySummary2;
                        str10 = str12;
                        activitySample = activitySample4;
                    } else {
                        str10 = str11;
                        arrayList5 = arrayList7;
                        arrayList4 = arrayList6;
                        str9 = str17;
                        str7 = str18;
                        str8 = str19;
                        timeZone6 = timeZone2;
                    }
                    activitySample2 = activitySample;
                } else {
                    str9 = str17;
                    str7 = str18;
                    str8 = str19;
                    arrayList4 = arrayList2;
                    arrayList5 = arrayList;
                    str10 = str3;
                    activitySample2 = activitySample;
                    timeZone6 = timeZone;
                }
                i97 i973 = i97.a;
                str20 = str6;
                arrayList16 = arrayList3;
                str21 = str10;
                arrayList17 = arrayList5;
                str19 = str8;
                str18 = str7;
                i6 = i2;
                size = i;
                arrayList13 = arrayList4;
                list2 = list;
                str17 = str9;
            } else {
                w97.c();
                throw null;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(str20, "sampleRaw " + arrayList13 + " \n summary " + arrayList17 + " \n gFit " + arrayList16);
        return new v87<>(arrayList13, arrayList17, arrayList16);
    }

    @DexIgnore
    public static final ActivitySummary a(ActivitySummary activitySummary, ActivitySample activitySample) {
        List<Integer> list;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SyncDataExtensions", "calculateSample - currentSummary=" + activitySummary + ", newSample=" + activitySample);
        double steps = activitySample.getSteps() + activitySummary.getSteps();
        double calories = activitySample.getCalories() + activitySummary.getCalories();
        double distance = activitySample.getDistance() + activitySummary.getDistance();
        int activeTime = activitySample.getActiveTime() + activitySummary.getActiveTime();
        List<Integer> intensitiesInArray = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
        List<Integer> intensities = activitySummary.getIntensities();
        if (intensitiesInArray.size() > intensities.size()) {
            ArrayList arrayList = new ArrayList(x97.a(intensitiesInArray, 10));
            int i = 0;
            for (T t : intensitiesInArray) {
                int i2 = i + 1;
                if (i >= 0) {
                    arrayList.add(Integer.valueOf(t.intValue() + (i < intensities.size() ? intensities.get(i).intValue() : 0)));
                    i = i2;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list = ea7.d((Collection) arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(x97.a(intensities, 10));
            int i3 = 0;
            for (T t2 : intensities) {
                int i4 = i3 + 1;
                if (i3 >= 0) {
                    arrayList2.add(Integer.valueOf(t2.intValue() + (i3 < intensitiesInArray.size() ? intensitiesInArray.get(i3).intValue() : 0)));
                    i3 = i4;
                } else {
                    w97.c();
                    throw null;
                }
            }
            list = ea7.d((Collection) arrayList2);
        }
        activitySummary.setSteps(steps);
        activitySummary.setCalories(calories);
        activitySummary.setDistance(distance);
        activitySummary.setActiveTime(activeTime);
        activitySummary.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary.setIntensities(list);
        return activitySummary;
    }

    @DexIgnore
    public static final List<MFSleepSession> a(List<FitnessDataWrapper> list, String str) {
        SleepSessionHeartRate sleepSessionHeartRate;
        SleepState sleepState;
        ee7.b(list, "$this$getSleepSessions");
        ee7.b(str, "serial");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        ea7.a((Iterable) list, (Comparator) new e());
        Gson gson = new Gson();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            T next = it.next();
            for (T t : next.getSleeps()) {
                long millis = t.getStartTime().getMillis();
                int normalizedSleepQuality = t.getNormalizedSleepQuality();
                SleepDistribution sleepDistribution = new SleepDistribution(t.getAwakeMinutes(), t.getLightSleepMinutes(), t.getDeepSleepMinutes());
                ArrayList arrayList2 = new ArrayList();
                for (T t2 : t.getStateChanges()) {
                    int component1 = t2.component1();
                    int component2 = t2.component2();
                    if (component1 == SleepState.AWAKE.ordinal()) {
                        sleepState = SleepState.AWAKE;
                    } else if (component1 == SleepState.SLEEP.ordinal()) {
                        sleepState = SleepState.SLEEP;
                    } else if (component1 == SleepState.DEEP_SLEEP.ordinal()) {
                        sleepState = SleepState.DEEP_SLEEP;
                    } else {
                        sleepState = SleepState.AWAKE;
                    }
                    arrayList2.add(new WrapperSleepStateChange(sleepState.ordinal(), (long) component2));
                }
                String a2 = gson.a(arrayList2);
                if (t.getHeartRate() != null) {
                    List<Short> values = t.getHeartRate().getValues();
                    short s = Short.MIN_VALUE;
                    short s2 = Short.MAX_VALUE;
                    int resolutionInSecond = t.getHeartRate().getResolutionInSecond();
                    int size = values.size();
                    int i = 0;
                    int i2 = 0;
                    for (int i3 = 0; i3 < size; i3++) {
                        short shortValue = values.get(i3).shortValue();
                        if (shortValue > s) {
                            s = shortValue;
                        } else if (shortValue < s2) {
                            s2 = shortValue;
                        }
                        if (shortValue > 0) {
                            i2 += shortValue;
                            i++;
                        }
                    }
                    sleepSessionHeartRate = new SleepSessionHeartRate((float) (i > 0 ? i2 / i : 0), s, s2, resolutionInSecond, values);
                } else {
                    sleepSessionHeartRate = null;
                }
                long j = (long) 1000;
                int currentTimeMillis = (int) (System.currentTimeMillis() / j);
                long millis2 = t.getEndTime().getMillis();
                Date date = t.getEndTime().toDate();
                ee7.a((Object) date, "sleepSession.endTime.toDate()");
                ee7.a((Object) a2, "sleepStatesJson");
                DateTime now = DateTime.now();
                ee7.a((Object) now, "DateTime.now()");
                arrayList.add(new MFSleepSession(millis2, date, next.getTimezoneOffsetInSecond(), str, currentTimeMillis, currentTimeMillis, (double) normalizedSleepQuality, db5.Device.ordinal(), (int) (millis / j), (int) (t.getEndTime().getMillis() / j), t.getDeepSleepMinutes() + t.getLightSleepMinutes(), sleepDistribution, a2, sleepSessionHeartRate, now));
                it = it;
                gson = gson;
            }
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "sleepSessions " + arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(ActivitySample activitySample) {
        ee7.b(activitySample, "$this$isDataAvailable");
        double d2 = (double) 0;
        return activitySample.getSteps() > d2 || activitySample.getCalories() > d2 || activitySample.getDistance() > d2 || activitySample.getActiveTime() > 0;
    }

    @DexIgnore
    public static final Date a(DateTime dateTime, boolean z) {
        ee7.b(dateTime, "$this$toDate");
        if (z) {
            Date date = dateTime.toLocalDateTime().toDate();
            ee7.a((Object) date, "toLocalDateTime().toDate()");
            return date;
        }
        Date date2 = dateTime.toDate();
        ee7.a((Object) date2, "toDate()");
        return date2;
    }
}
