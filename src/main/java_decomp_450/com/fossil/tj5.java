package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj5 implements Factory<sj5> {
    @DexIgnore
    public /* final */ Provider<UserRepository> a;

    @DexIgnore
    public tj5(Provider<UserRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static tj5 a(Provider<UserRepository> provider) {
        return new tj5(provider);
    }

    @DexIgnore
    public static sj5 a(UserRepository userRepository) {
        return new sj5(userRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public sj5 get() {
        return a(this.a.get());
    }
}
