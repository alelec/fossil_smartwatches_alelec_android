package com.fossil;

import com.fossil.c5;

public class h5 {
    public p5 a = new p5(this);
    public final i5 b;
    public final d c;
    public h5 d;
    public int e = 0;
    public int f = -1;
    public c g = c.NONE;
    public int h;
    public c5 i;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|(3:17|18|20)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.fossil.h5$d[] r0 = com.fossil.h5.d.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.h5.a.a = r0
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.h5$d r1 = com.fossil.h5.d.LEFT     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.h5$d r1 = com.fossil.h5.d.RIGHT     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.h5$d r1 = com.fossil.h5.d.TOP     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.h5$d r1 = com.fossil.h5.d.BOTTOM     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.fossil.h5$d r1 = com.fossil.h5.d.BASELINE     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_X     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.fossil.h5$d r1 = com.fossil.h5.d.CENTER_Y     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.fossil.h5.a.a     // Catch:{ NoSuchFieldError -> 0x006c }
                com.fossil.h5$d r1 = com.fossil.h5.d.NONE     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.h5.a.<clinit>():void");
        }
        */
    }

    public enum b {
        RELAXED,
        STRICT
    }

    public enum c {
        NONE,
        STRONG,
        WEAK
    }

    public enum d {
        NONE,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        BASELINE,
        CENTER,
        CENTER_X,
        CENTER_Y
    }

    public h5(i5 i5Var, d dVar) {
        b bVar = b.RELAXED;
        this.h = 0;
        this.b = i5Var;
        this.c = dVar;
    }

    public void a(w4 w4Var) {
        c5 c5Var = this.i;
        if (c5Var == null) {
            this.i = new c5(c5.a.UNRESTRICTED, null);
        } else {
            c5Var.a();
        }
    }

    public int b() {
        h5 h5Var;
        if (this.b.s() == 8) {
            return 0;
        }
        if (this.f <= -1 || (h5Var = this.d) == null || h5Var.b.s() != 8) {
            return this.e;
        }
        return this.f;
    }

    public i5 c() {
        return this.b;
    }

    public p5 d() {
        return this.a;
    }

    public c5 e() {
        return this.i;
    }

    public c f() {
        return this.g;
    }

    public h5 g() {
        return this.d;
    }

    public d h() {
        return this.c;
    }

    public boolean i() {
        return this.d != null;
    }

    public void j() {
        this.d = null;
        this.e = 0;
        this.f = -1;
        this.g = c.STRONG;
        this.h = 0;
        b bVar = b.RELAXED;
        this.a.d();
    }

    public String toString() {
        return this.b.g() + ":" + this.c.toString();
    }

    public int a() {
        return this.h;
    }

    public boolean a(h5 h5Var, int i2, c cVar, int i3) {
        return a(h5Var, i2, -1, cVar, i3, false);
    }

    public boolean a(h5 h5Var, int i2, int i3, c cVar, int i4, boolean z) {
        if (h5Var == null) {
            this.d = null;
            this.e = 0;
            this.f = -1;
            this.g = c.NONE;
            this.h = 2;
            return true;
        } else if (!z && !a(h5Var)) {
            return false;
        } else {
            this.d = h5Var;
            if (i2 > 0) {
                this.e = i2;
            } else {
                this.e = 0;
            }
            this.f = i3;
            this.g = cVar;
            this.h = i4;
            return true;
        }
    }

    public boolean a(h5 h5Var) {
        boolean z = false;
        if (h5Var == null) {
            return false;
        }
        d h2 = h5Var.h();
        d dVar = this.c;
        if (h2 != dVar) {
            switch (a.a[dVar.ordinal()]) {
                case 1:
                    if (h2 == d.BASELINE || h2 == d.CENTER_X || h2 == d.CENTER_Y) {
                        return false;
                    }
                    return true;
                case 2:
                case 3:
                    boolean z2 = h2 == d.LEFT || h2 == d.RIGHT;
                    if (!(h5Var.c() instanceof l5)) {
                        return z2;
                    }
                    if (z2 || h2 == d.CENTER_X) {
                        z = true;
                    }
                    return z;
                case 4:
                case 5:
                    boolean z3 = h2 == d.TOP || h2 == d.BOTTOM;
                    if (!(h5Var.c() instanceof l5)) {
                        return z3;
                    }
                    if (z3 || h2 == d.CENTER_Y) {
                        z = true;
                    }
                    return z;
                case 6:
                case 7:
                case 8:
                case 9:
                    return false;
                default:
                    throw new AssertionError(this.c.name());
            }
        } else if (dVar != d.BASELINE || (h5Var.c().y() && c().y())) {
            return true;
        } else {
            return false;
        }
    }
}
