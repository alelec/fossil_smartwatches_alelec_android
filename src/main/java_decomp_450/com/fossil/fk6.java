package com.fossil;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import java.util.HashMap;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fk6 extends go5 implements ek6 {
    @DexIgnore
    public static /* final */ Pattern y;
    @DexIgnore
    public static /* final */ a z; // = new a(null);
    @DexIgnore
    public dk6 f;
    @DexIgnore
    public TextInputEditText g;
    @DexIgnore
    public TextInputEditText h;
    @DexIgnore
    public TextInputEditText i;
    @DexIgnore
    public TextInputLayout j;
    @DexIgnore
    public TextInputLayout p;
    @DexIgnore
    public TextInputLayout q;
    @DexIgnore
    public ProgressButton r;
    @DexIgnore
    public FlexibleTextView s;
    @DexIgnore
    public FlexibleTextView t;
    @DexIgnore
    public RTLImageView u;
    @DexIgnore
    public String v;
    @DexIgnore
    public boolean w; // = true;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final fk6 a() {
            return new fk6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        public b(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            fk6.c(this.a).clearFocus();
            fk6.b(this.a).clearFocus();
            fk6.a(this.a).clearFocus();
            ee7.a((Object) view, "it");
            view.setFocusable(true);
            if (this.a.f1()) {
                fk6.d(this.a).a(fk6.b(this.a).getEditableText().toString(), fk6.a(this.a).getEditableText().toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        public c(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                FragmentActivity requireActivity = this.a.requireActivity();
                ee7.a((Object) requireActivity, "requireActivity()");
                if (!requireActivity.isFinishing()) {
                    FragmentActivity requireActivity2 = this.a.requireActivity();
                    ee7.a((Object) requireActivity2, "requireActivity()");
                    if (!requireActivity2.isDestroyed()) {
                        this.a.requireActivity().finish();
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ee7.b(editable, "s");
            fk6.e(this.a).setEnabled(this.a.f1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ee7.b(editable, "s");
            fk6.e(this.a).setEnabled(this.a.f1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        public f(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            boolean z2 = false;
            if (z) {
                fk6.f(this.a).setVisibility(0);
                fk6.g(this.a).setVisibility(0);
                return;
            }
            Editable editableText = fk6.a(this.a).getEditableText();
            ee7.a((Object) editableText, "mEdtNew.editableText");
            if (editableText.length() == 0) {
                z2 = true;
            }
            if (z2 || this.a.w) {
                fk6.f(this.a).setVisibility(8);
                fk6.g(this.a).setVisibility(8);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ fk6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(fk6 fk6) {
            this.a = fk6;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ee7.b(editable, "s");
            fk6.e(this.a).setEnabled(this.a.f1());
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ee7.b(charSequence, "s");
        }
    }

    /*
    static {
        ee7.a((Object) fk6.class.getSimpleName(), "ProfileChangePasswordFra\u2026nt::class.java.simpleName");
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            y = compile;
        } else {
            ee7.a();
            throw null;
        }
    }
    */

    @DexIgnore
    public static final /* synthetic */ TextInputEditText a(fk6 fk6) {
        TextInputEditText textInputEditText = fk6.h;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        ee7.d("mEdtNew");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText b(fk6 fk6) {
        TextInputEditText textInputEditText = fk6.g;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        ee7.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ TextInputEditText c(fk6 fk6) {
        TextInputEditText textInputEditText = fk6.i;
        if (textInputEditText != null) {
            return textInputEditText;
        }
        ee7.d("mEdtRepeat");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ dk6 d(fk6 fk6) {
        dk6 dk6 = fk6.f;
        if (dk6 != null) {
            return dk6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ ProgressButton e(fk6 fk6) {
        ProgressButton progressButton = fk6.r;
        if (progressButton != null) {
            return progressButton;
        }
        ee7.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView f(fk6 fk6) {
        FlexibleTextView flexibleTextView = fk6.s;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        ee7.d("mTvCheckChar");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ FlexibleTextView g(fk6 fk6) {
        FlexibleTextView flexibleTextView = fk6.t;
        if (flexibleTextView != null) {
            return flexibleTextView;
        }
        ee7.d("mTvCheckCombine");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z2) {
        TextInputLayout textInputLayout = this.j;
        if (textInputLayout != null) {
            textInputLayout.setErrorEnabled(z2);
            if (z2) {
                TextInputLayout textInputLayout2 = this.j;
                if (textInputLayout2 != null) {
                    textInputLayout2.setError(ig5.a(PortfolioApp.g0.c(), 2131887032));
                } else {
                    ee7.d("mTvOldError");
                    throw null;
                }
            } else {
                TextInputLayout textInputLayout3 = this.j;
                if (textInputLayout3 != null) {
                    textInputLayout3.setError("");
                } else {
                    ee7.d("mTvOldError");
                    throw null;
                }
            }
        } else {
            ee7.d("mTvOldError");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void V0() {
        if (isActive()) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886932);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026urPasswordHasBeenChanged)");
            X(a2);
            requireActivity().finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        if (getActivity() == null) {
            return false;
        }
        FragmentActivity requireActivity = requireActivity();
        ee7.a((Object) requireActivity, "requireActivity()");
        if (requireActivity.isFinishing()) {
            return false;
        }
        FragmentActivity requireActivity2 = requireActivity();
        ee7.a((Object) requireActivity2, "requireActivity()");
        if (requireActivity2.isDestroyed()) {
            return false;
        }
        requireActivity().finish();
        return true;
    }

    @DexIgnore
    public final boolean f1() {
        boolean z2;
        TextInputEditText textInputEditText = this.g;
        if (textInputEditText != null) {
            String obj = textInputEditText.getEditableText().toString();
            TextInputEditText textInputEditText2 = this.h;
            if (textInputEditText2 != null) {
                String obj2 = textInputEditText2.getEditableText().toString();
                TextInputEditText textInputEditText3 = this.i;
                if (textInputEditText3 != null) {
                    String obj3 = textInputEditText3.getEditableText().toString();
                    boolean z3 = true;
                    if (obj2.length() >= 7) {
                        FlexibleTextView flexibleTextView = this.s;
                        if (flexibleTextView != null) {
                            flexibleTextView.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231050), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = true;
                        } else {
                            ee7.d("mTvCheckChar");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView2 = this.s;
                        if (flexibleTextView2 != null) {
                            flexibleTextView2.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            ee7.d("mTvCheckChar");
                            throw null;
                        }
                    }
                    if (y.matcher(obj2).matches()) {
                        FlexibleTextView flexibleTextView3 = this.t;
                        if (flexibleTextView3 != null) {
                            flexibleTextView3.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131231050), (Drawable) null, (Drawable) null, (Drawable) null);
                        } else {
                            ee7.d("mTvCheckCombine");
                            throw null;
                        }
                    } else {
                        FlexibleTextView flexibleTextView4 = this.t;
                        if (flexibleTextView4 != null) {
                            flexibleTextView4.setCompoundDrawablesWithIntrinsicBounds(v6.c(PortfolioApp.g0.c(), 2131230964), (Drawable) null, (Drawable) null, (Drawable) null);
                            z2 = false;
                        } else {
                            ee7.d("mTvCheckCombine");
                            throw null;
                        }
                    }
                    if (obj.length() == 0) {
                        TextInputLayout textInputLayout = this.j;
                        if (textInputLayout != null) {
                            textInputLayout.setErrorEnabled(false);
                            TextInputLayout textInputLayout2 = this.j;
                            if (textInputLayout2 != null) {
                                textInputLayout2.setError("");
                            } else {
                                ee7.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            ee7.d("mTvOldError");
                            throw null;
                        }
                    } else {
                        if (ee7.a((Object) this.v, (Object) obj)) {
                            TextInputLayout textInputLayout3 = this.j;
                            if (textInputLayout3 != null) {
                                textInputLayout3.setErrorEnabled(true);
                                TextInputLayout textInputLayout4 = this.j;
                                if (textInputLayout4 != null) {
                                    textInputLayout4.setError(ig5.a(PortfolioApp.g0.c(), 2131887032));
                                } else {
                                    ee7.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvOldError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout5 = this.j;
                            if (textInputLayout5 != null) {
                                textInputLayout5.setErrorEnabled(false);
                                TextInputLayout textInputLayout6 = this.j;
                                if (textInputLayout6 != null) {
                                    textInputLayout6.setError("");
                                } else {
                                    ee7.d("mTvOldError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvOldError");
                                throw null;
                            }
                        }
                        if ((obj2.length() == 0) || !ee7.a((Object) obj2, (Object) obj)) {
                            TextInputLayout textInputLayout7 = this.p;
                            if (textInputLayout7 != null) {
                                textInputLayout7.setErrorEnabled(false);
                                TextInputLayout textInputLayout8 = this.p;
                                if (textInputLayout8 != null) {
                                    textInputLayout8.setError("");
                                } else {
                                    ee7.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            TextInputLayout textInputLayout9 = this.p;
                            if (textInputLayout9 != null) {
                                textInputLayout9.setErrorEnabled(true);
                                TextInputLayout textInputLayout10 = this.p;
                                if (textInputLayout10 != null) {
                                    textInputLayout10.setError(ig5.a(PortfolioApp.g0.c(), 2131886938));
                                } else {
                                    ee7.d("mTvNewError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (obj2.length() == 0) {
                        TextInputLayout textInputLayout11 = this.p;
                        if (textInputLayout11 != null) {
                            textInputLayout11.setErrorEnabled(false);
                            TextInputLayout textInputLayout12 = this.p;
                            if (textInputLayout12 != null) {
                                textInputLayout12.setError("");
                                TextInputLayout textInputLayout13 = this.q;
                                if (textInputLayout13 != null) {
                                    textInputLayout13.setErrorEnabled(false);
                                    TextInputLayout textInputLayout14 = this.q;
                                    if (textInputLayout14 != null) {
                                        textInputLayout14.setError("");
                                    } else {
                                        ee7.d("mTvRepeatError");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvNewError");
                                throw null;
                            }
                        } else {
                            ee7.d("mTvNewError");
                            throw null;
                        }
                    }
                    if ((obj3.length() == 0) || ee7.a((Object) obj3, (Object) obj2)) {
                        TextInputLayout textInputLayout15 = this.q;
                        if (textInputLayout15 != null) {
                            textInputLayout15.setErrorEnabled(false);
                            TextInputLayout textInputLayout16 = this.q;
                            if (textInputLayout16 != null) {
                                textInputLayout16.setError("");
                            } else {
                                ee7.d("mTvRepeatError");
                                throw null;
                            }
                        } else {
                            ee7.d("mTvRepeatError");
                            throw null;
                        }
                    } else {
                        if (!(obj2.length() == 0) && (!ee7.a((Object) obj3, (Object) obj2))) {
                            TextInputLayout textInputLayout17 = this.q;
                            if (textInputLayout17 != null) {
                                textInputLayout17.setErrorEnabled(true);
                                TextInputLayout textInputLayout18 = this.q;
                                if (textInputLayout18 != null) {
                                    textInputLayout18.setError(ig5.a(PortfolioApp.g0.c(), 2131887035));
                                } else {
                                    ee7.d("mTvRepeatError");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvRepeatError");
                                throw null;
                            }
                        }
                    }
                    this.w = z2;
                    if (z2) {
                        if (!(obj.length() == 0)) {
                            if (!(obj2.length() == 0) && (!ee7.a((Object) obj, (Object) obj2)) && ee7.a((Object) obj2, (Object) obj3) && (!ee7.a((Object) obj, (Object) this.v))) {
                                TextInputLayout textInputLayout19 = this.p;
                                if (textInputLayout19 != null) {
                                    textInputLayout19.setErrorEnabled(false);
                                    TextInputLayout textInputLayout20 = this.p;
                                    if (textInputLayout20 != null) {
                                        textInputLayout20.setError("");
                                        TextInputLayout textInputLayout21 = this.q;
                                        if (textInputLayout21 != null) {
                                            textInputLayout21.setErrorEnabled(false);
                                            TextInputLayout textInputLayout22 = this.q;
                                            if (textInputLayout22 != null) {
                                                textInputLayout22.setError("");
                                                return true;
                                            }
                                            ee7.d("mTvRepeatError");
                                            throw null;
                                        }
                                        ee7.d("mTvRepeatError");
                                        throw null;
                                    }
                                    ee7.d("mTvNewError");
                                    throw null;
                                }
                                ee7.d("mTvNewError");
                                throw null;
                            }
                        }
                    }
                    if (!this.w) {
                        if (obj2.length() != 0) {
                            z3 = false;
                        }
                        if (!z3) {
                            FlexibleTextView flexibleTextView5 = this.s;
                            if (flexibleTextView5 != null) {
                                flexibleTextView5.setVisibility(0);
                                FlexibleTextView flexibleTextView6 = this.t;
                                if (flexibleTextView6 != null) {
                                    flexibleTextView6.setVisibility(0);
                                } else {
                                    ee7.d("mTvCheckCombine");
                                    throw null;
                                }
                            } else {
                                ee7.d("mTvCheckChar");
                                throw null;
                            }
                        }
                    }
                    return false;
                }
                ee7.d("mEdtRepeat");
                throw null;
            }
            ee7.d("mEdtNew");
            throw null;
        }
        ee7.d("mEdtOld");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void i0() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager requireFragmentManager = requireFragmentManager();
            ee7.a((Object) requireFragmentManager, "requireFragmentManager()");
            bx6.E(requireFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void j() {
        if (isActive()) {
            ProgressButton progressButton = this.r;
            if (progressButton != null) {
                progressButton.c();
            } else {
                ee7.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        e55 e55 = (e55) qb.a(LayoutInflater.from(getContext()), 2131558608, null, false, a1());
        ee7.a((Object) e55, "binding");
        a(e55);
        new qw6(this, e55);
        ProgressButton progressButton = this.r;
        if (progressButton != null) {
            progressButton.setEnabled(false);
            return e55.d();
        }
        ee7.d("mSaveButton");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        dk6 dk6 = this.f;
        if (dk6 != null) {
            dk6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        dk6 dk6 = this.f;
        if (dk6 != null) {
            dk6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void x0() {
        if (isActive()) {
            ProgressButton progressButton = this.r;
            if (progressButton != null) {
                progressButton.setEnabled(false);
                TextInputEditText textInputEditText = this.g;
                if (textInputEditText != null) {
                    this.v = textInputEditText.getEditableText().toString();
                    T(true);
                    return;
                }
                ee7.d("mEdtOld");
                throw null;
            }
            ee7.d("mSaveButton");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void c(int i2, String str) {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.ek6
    public void h() {
        if (isActive()) {
            ProgressButton progressButton = this.r;
            if (progressButton != null) {
                progressButton.b();
            } else {
                ee7.d("mSaveButton");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(dk6 dk6) {
        ee7.b(dk6, "presenter");
        jw3.a(dk6);
        ee7.a((Object) dk6, "checkNotNull(presenter)");
        this.f = dk6;
    }

    @DexIgnore
    public final void a(e55 e55) {
        FlexibleTextInputEditText flexibleTextInputEditText = e55.s;
        ee7.a((Object) flexibleTextInputEditText, "binding.etOldPass");
        this.g = flexibleTextInputEditText;
        FlexibleTextInputEditText flexibleTextInputEditText2 = e55.r;
        ee7.a((Object) flexibleTextInputEditText2, "binding.etNewPass");
        this.h = flexibleTextInputEditText2;
        FlexibleTextInputEditText flexibleTextInputEditText3 = e55.t;
        ee7.a((Object) flexibleTextInputEditText3, "binding.etRepeatPass");
        this.i = flexibleTextInputEditText3;
        FlexibleTextInputLayout flexibleTextInputLayout = e55.v;
        ee7.a((Object) flexibleTextInputLayout, "binding.inputOldPass");
        this.j = flexibleTextInputLayout;
        FlexibleTextInputLayout flexibleTextInputLayout2 = e55.u;
        ee7.a((Object) flexibleTextInputLayout2, "binding.inputNewPass");
        this.p = flexibleTextInputLayout2;
        FlexibleTextInputLayout flexibleTextInputLayout3 = e55.w;
        ee7.a((Object) flexibleTextInputLayout3, "binding.inputRepeatPass");
        this.q = flexibleTextInputLayout3;
        ProgressButton progressButton = e55.z;
        ee7.a((Object) progressButton, "binding.save");
        this.r = progressButton;
        FlexibleTextView flexibleTextView = e55.A;
        ee7.a((Object) flexibleTextView, "binding.tvErrorCheckCharacter");
        this.s = flexibleTextView;
        FlexibleTextView flexibleTextView2 = e55.B;
        ee7.a((Object) flexibleTextView2, "binding.tvErrorCheckCombine");
        this.t = flexibleTextView2;
        RTLImageView rTLImageView = e55.x;
        ee7.a((Object) rTLImageView, "binding.ivBack");
        this.u = rTLImageView;
        ProgressButton progressButton2 = this.r;
        if (progressButton2 != null) {
            progressButton2.setOnClickListener(new b(this));
            RTLImageView rTLImageView2 = this.u;
            if (rTLImageView2 != null) {
                rTLImageView2.setOnClickListener(new c(this));
                TextInputEditText textInputEditText = this.g;
                if (textInputEditText != null) {
                    textInputEditText.addTextChangedListener(new d(this));
                    TextInputEditText textInputEditText2 = this.h;
                    if (textInputEditText2 != null) {
                        textInputEditText2.addTextChangedListener(new e(this));
                        TextInputEditText textInputEditText3 = this.h;
                        if (textInputEditText3 != null) {
                            textInputEditText3.setOnFocusChangeListener(new f(this));
                            TextInputEditText textInputEditText4 = this.i;
                            if (textInputEditText4 != null) {
                                textInputEditText4.addTextChangedListener(new g(this));
                            } else {
                                ee7.d("mEdtRepeat");
                                throw null;
                            }
                        } else {
                            ee7.d("mEdtNew");
                            throw null;
                        }
                    } else {
                        ee7.d("mEdtNew");
                        throw null;
                    }
                } else {
                    ee7.d("mEdtOld");
                    throw null;
                }
            } else {
                ee7.d("mBackButton");
                throw null;
            }
        } else {
            ee7.d("mSaveButton");
            throw null;
        }
    }
}
