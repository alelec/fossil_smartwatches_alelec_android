package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u24 extends z14 {
    @DexIgnore
    public /* final */ Set<Class<?>> a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ Set<Class<?>> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;
    @DexIgnore
    public /* final */ d24 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements h94 {
        @DexIgnore
        public a(Set<Class<?>> set, h94 h94) {
        }
    }

    @DexIgnore
    public u24(c24<?> c24, d24 d24) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (m24 m24 : c24.a()) {
            if (m24.b()) {
                if (m24.d()) {
                    hashSet3.add(m24.a());
                } else {
                    hashSet.add(m24.a());
                }
            } else if (m24.d()) {
                hashSet4.add(m24.a());
            } else {
                hashSet2.add(m24.a());
            }
        }
        if (!c24.d().isEmpty()) {
            hashSet.add(h94.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.e = c24.d();
        this.f = d24;
    }

    @DexIgnore
    @Override // com.fossil.d24
    public <T> ob4<T> a(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.f.a(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.d24
    public <T> ob4<Set<T>> b(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.f.b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.z14, com.fossil.d24
    public <T> Set<T> c(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.f.c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", cls));
    }

    @DexIgnore
    @Override // com.fossil.z14, com.fossil.d24
    public <T> T get(Class<T> cls) {
        if (this.a.contains(cls)) {
            T t = (T) this.f.get(cls);
            return !cls.equals(h94.class) ? t : (T) new a(this.e, t);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", cls));
    }
}
