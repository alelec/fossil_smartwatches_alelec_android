package com.fossil;

import com.fossil.m00;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x00 implements m00<f00, InputStream> {
    @DexIgnore
    public static /* final */ zw<Integer> b; // = zw.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", (Object) 2500);
    @DexIgnore
    public /* final */ l00<f00, f00> a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements n00<f00, InputStream> {
        @DexIgnore
        public /* final */ l00<f00, f00> a; // = new l00<>(500);

        @DexIgnore
        @Override // com.fossil.n00
        public m00<f00, InputStream> a(q00 q00) {
            return new x00(this.a);
        }
    }

    @DexIgnore
    public x00(l00<f00, f00> l00) {
        this.a = l00;
    }

    @DexIgnore
    public boolean a(f00 f00) {
        return true;
    }

    @DexIgnore
    public m00.a<InputStream> a(f00 f00, int i, int i2, ax axVar) {
        l00<f00, f00> l00 = this.a;
        if (l00 != null) {
            f00 a2 = l00.a(f00, 0, 0);
            if (a2 == null) {
                this.a.a(f00, 0, 0, f00);
            } else {
                f00 = a2;
            }
        }
        return new m00.a<>(f00, new ox(f00, ((Integer) axVar.a(b)).intValue()));
    }
}
