package com.fossil;

import android.location.Location;
import com.fossil.fl4;
import com.fossil.nj4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm5 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ nj4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public d(DeviceLocation deviceLocation) {
            ee7.b(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements nj4.b {
        @DexIgnore
        public /* final */ /* synthetic */ wm5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(wm5 wm5) {
            this.a = wm5;
        }

        @DexIgnore
        @Override // com.fossil.nj4.b
        public void a(Location location, int i) {
            if (i >= 0) {
                FLogger.INSTANCE.getLocal().d("LoadLocation", "onLocationUpdated OK");
                if (location != null) {
                    float accuracy = location.getAccuracy();
                    if (accuracy <= 500.0f) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.e("LoadLocation", "onLocationUpdated - accuracy: " + accuracy);
                        try {
                            DeviceLocation deviceLocation = new DeviceLocation(PortfolioApp.g0.c().c(), location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                            ah5.p.a().f().saveDeviceLocation(deviceLocation);
                            this.a.a(new d(deviceLocation));
                        } catch (Exception unused) {
                            this.a.a(new c());
                        }
                        this.a.d().b(this);
                        return;
                    }
                    return;
                }
                return;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("LoadLocation", "onLocationUpdated error - error: " + i);
            this.a.d().b(this);
            this.a.a(new c());
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public wm5(nj4 nj4) {
        ee7.b(nj4, "mfLocationService");
        jw3.a(nj4, "mfLocationService cannot be null!", new Object[0]);
        ee7.a((Object) nj4, "checkNotNull(mfLocationS\u2026Service cannot be null!\")");
        this.d = nj4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "LoadLocation";
    }

    @DexIgnore
    public final nj4 d() {
        return this.d;
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        FLogger.INSTANCE.getLocal().d("LoadLocation", "running UseCase");
        this.d.a(new e(this));
        return new Object();
    }
}
