package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt3<S> extends lt3<S> {
    @DexIgnore
    public static /* final */ Object q; // = "MONTHS_VIEW_GROUP_TAG";
    @DexIgnore
    public static /* final */ Object r; // = "NAVIGATION_PREV_TAG";
    @DexIgnore
    public static /* final */ Object s; // = "NAVIGATION_NEXT_TAG";
    @DexIgnore
    public static /* final */ Object t; // = "SELECTOR_TOGGLE_TAG";
    @DexIgnore
    public int b;
    @DexIgnore
    public zs3<S> c;
    @DexIgnore
    public ws3 d;
    @DexIgnore
    public ht3 e;
    @DexIgnore
    public k f;
    @DexIgnore
    public ys3 g;
    @DexIgnore
    public RecyclerView h;
    @DexIgnore
    public RecyclerView i;
    @DexIgnore
    public View j;
    @DexIgnore
    public View p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public void run() {
            dt3.this.i.smoothScrollToPosition(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends g9 {
        @DexIgnore
        public b(dt3 dt3) {
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void a(View view, oa oaVar) {
            super.a(view, oaVar);
            oaVar.a((Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends mt3 {
        @DexIgnore
        public /* final */ /* synthetic */ int I;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(Context context, int i, boolean z, int i2) {
            super(context, i, z);
            this.I = i2;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.LinearLayoutManager
        public void a(RecyclerView.State state, int[] iArr) {
            if (this.I == 0) {
                iArr[0] = dt3.this.i.getWidth();
                iArr[1] = dt3.this.i.getWidth();
                return;
            }
            iArr[0] = dt3.this.i.getHeight();
            iArr[1] = dt3.this.i.getHeight();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements l {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        @Override // com.fossil.dt3.l
        public void a(long j) {
            if (dt3.this.d.a().e(j)) {
                dt3.this.c.g(j);
                Iterator<kt3<S>> it = ((lt3) dt3.this).a.iterator();
                while (it.hasNext()) {
                    it.next().a((S) dt3.this.c.r());
                }
                dt3.this.i.getAdapter().notifyDataSetChanged();
                if (dt3.this.h != null) {
                    dt3.this.h.getAdapter().notifyDataSetChanged();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends RecyclerView.l {
        @DexIgnore
        public /* final */ Calendar a; // = nt3.d();
        @DexIgnore
        public /* final */ Calendar b; // = nt3.d();

        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.l
        public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
            int i;
            if ((recyclerView.getAdapter() instanceof ot3) && (recyclerView.getLayoutManager() instanceof GridLayoutManager)) {
                ot3 ot3 = (ot3) recyclerView.getAdapter();
                GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                for (a9<Long, Long> a9Var : dt3.this.c.l()) {
                    F f = a9Var.a;
                    if (!(f == null || a9Var.b == null)) {
                        this.a.setTimeInMillis(f.longValue());
                        this.b.setTimeInMillis(a9Var.b.longValue());
                        int b2 = ot3.b(this.a.get(1));
                        int b3 = ot3.b(this.b.get(1));
                        View c2 = gridLayoutManager.c(b2);
                        View c3 = gridLayoutManager.c(b3);
                        int Y = b2 / gridLayoutManager.Y();
                        int Y2 = b3 / gridLayoutManager.Y();
                        int i2 = Y;
                        while (i2 <= Y2) {
                            View c4 = gridLayoutManager.c(gridLayoutManager.Y() * i2);
                            if (c4 != null) {
                                int top = c4.getTop() + dt3.this.g.d.b();
                                int bottom = c4.getBottom() - dt3.this.g.d.a();
                                int left = i2 == Y ? c2.getLeft() + (c2.getWidth() / 2) : 0;
                                if (i2 == Y2) {
                                    i = c3.getLeft() + (c3.getWidth() / 2);
                                } else {
                                    i = recyclerView.getWidth();
                                }
                                canvas.drawRect((float) left, (float) top, (float) i, (float) bottom, dt3.this.g.h);
                            }
                            i2++;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends g9 {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        @Override // com.fossil.g9
        public void a(View view, oa oaVar) {
            String str;
            super.a(view, oaVar);
            if (dt3.this.p.getVisibility() == 0) {
                str = dt3.this.getString(rr3.mtrl_picker_toggle_to_year_selection);
            } else {
                str = dt3.this.getString(rr3.mtrl_picker_toggle_to_day_selection);
            }
            oaVar.d(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends RecyclerView.q {
        @DexIgnore
        public /* final */ /* synthetic */ jt3 a;
        @DexIgnore
        public /* final */ /* synthetic */ MaterialButton b;

        @DexIgnore
        public g(jt3 jt3, MaterialButton materialButton) {
            this.a = jt3;
            this.b = materialButton;
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 0) {
                CharSequence text = this.b.getText();
                if (Build.VERSION.SDK_INT >= 16) {
                    recyclerView.announceForAccessibility(text);
                } else {
                    recyclerView.sendAccessibilityEvent(2048);
                }
            }
        }

        @DexIgnore
        @Override // androidx.recyclerview.widget.RecyclerView.q
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            int i3;
            if (i < 0) {
                i3 = dt3.this.f1().I();
            } else {
                i3 = dt3.this.f1().L();
            }
            ht3 unused = dt3.this.e = this.a.a(i3);
            this.b.setText(this.a.b(i3));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements View.OnClickListener {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void onClick(View view) {
            dt3.this.g1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jt3 a;

        @DexIgnore
        public i(jt3 jt3) {
            this.a = jt3;
        }

        @DexIgnore
        public void onClick(View view) {
            int I = dt3.this.f1().I() + 1;
            if (I < dt3.this.i.getAdapter().getItemCount()) {
                dt3.this.a(this.a.a(I));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jt3 a;

        @DexIgnore
        public j(jt3 jt3) {
            this.a = jt3;
        }

        @DexIgnore
        public void onClick(View view) {
            int L = dt3.this.f1().L() - 1;
            if (L >= 0) {
                dt3.this.a(this.a.a(L));
            }
        }
    }

    @DexIgnore
    public enum k {
        DAY,
        YEAR
    }

    @DexIgnore
    public interface l {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public final RecyclerView.l a1() {
        return new e();
    }

    @DexIgnore
    public ws3 b1() {
        return this.d;
    }

    @DexIgnore
    public ys3 c1() {
        return this.g;
    }

    @DexIgnore
    public ht3 d1() {
        return this.e;
    }

    @DexIgnore
    public zs3<S> e1() {
        return this.c;
    }

    @DexIgnore
    public LinearLayoutManager f1() {
        return (LinearLayoutManager) this.i.getLayoutManager();
    }

    @DexIgnore
    public void g1() {
        k kVar = this.f;
        if (kVar == k.YEAR) {
            a(k.DAY);
        } else if (kVar == k.DAY) {
            a(k.YEAR);
        }
    }

    @DexIgnore
    public final void n(int i2) {
        this.i.post(new a(i2));
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle == null) {
            bundle = getArguments();
        }
        this.b = bundle.getInt("THEME_RES_ID_KEY");
        this.c = (zs3) bundle.getParcelable("GRID_SELECTOR_KEY");
        this.d = (ws3) bundle.getParcelable("CALENDAR_CONSTRAINTS_KEY");
        this.e = (ht3) bundle.getParcelable("CURRENT_MONTH_KEY");
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        int i2;
        int i3;
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), this.b);
        this.g = new ys3(contextThemeWrapper);
        LayoutInflater cloneInContext = layoutInflater.cloneInContext(contextThemeWrapper);
        ht3 e2 = this.d.e();
        if (et3.f(contextThemeWrapper)) {
            i3 = pr3.mtrl_calendar_vertical;
            i2 = 1;
        } else {
            i3 = pr3.mtrl_calendar_horizontal;
            i2 = 0;
        }
        View inflate = cloneInContext.inflate(i3, viewGroup, false);
        GridView gridView = (GridView) inflate.findViewById(nr3.mtrl_calendar_days_of_week);
        da.a(gridView, new b(this));
        gridView.setAdapter((ListAdapter) new ct3());
        gridView.setNumColumns(e2.e);
        gridView.setEnabled(false);
        this.i = (RecyclerView) inflate.findViewById(nr3.mtrl_calendar_months);
        this.i.setLayoutManager(new c(getContext(), i2, false, i2));
        this.i.setTag(q);
        jt3 jt3 = new jt3(contextThemeWrapper, this.c, this.d, new d());
        this.i.setAdapter(jt3);
        int integer = contextThemeWrapper.getResources().getInteger(or3.mtrl_calendar_year_selector_span);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(nr3.mtrl_calendar_year_selector_frame);
        this.h = recyclerView;
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            this.h.setLayoutManager(new GridLayoutManager((Context) contextThemeWrapper, integer, 1, false));
            this.h.setAdapter(new ot3(this));
            this.h.addItemDecoration(a1());
        }
        if (inflate.findViewById(nr3.month_navigation_fragment_toggle) != null) {
            a(inflate, jt3);
        }
        if (!et3.f(contextThemeWrapper)) {
            new vg().a(this.i);
        }
        this.i.scrollToPosition(jt3.a(this.e));
        return inflate;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("THEME_RES_ID_KEY", this.b);
        bundle.putParcelable("GRID_SELECTOR_KEY", this.c);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", this.d);
        bundle.putParcelable("CURRENT_MONTH_KEY", this.e);
    }

    @DexIgnore
    public static <T> dt3<T> a(zs3<T> zs3, int i2, ws3 ws3) {
        dt3<T> dt3 = new dt3<>();
        Bundle bundle = new Bundle();
        bundle.putInt("THEME_RES_ID_KEY", i2);
        bundle.putParcelable("GRID_SELECTOR_KEY", zs3);
        bundle.putParcelable("CALENDAR_CONSTRAINTS_KEY", ws3);
        bundle.putParcelable("CURRENT_MONTH_KEY", ws3.d());
        dt3.setArguments(bundle);
        return dt3;
    }

    @DexIgnore
    public void a(ht3 ht3) {
        jt3 jt3 = (jt3) this.i.getAdapter();
        int a2 = jt3.a(ht3);
        int a3 = a2 - jt3.a(this.e);
        boolean z = true;
        boolean z2 = Math.abs(a3) > 3;
        if (a3 <= 0) {
            z = false;
        }
        this.e = ht3;
        if (z2 && z) {
            this.i.scrollToPosition(a2 - 3);
            n(a2);
        } else if (z2) {
            this.i.scrollToPosition(a2 + 3);
            n(a2);
        } else {
            n(a2);
        }
    }

    @DexIgnore
    public static int a(Context context) {
        return context.getResources().getDimensionPixelSize(lr3.mtrl_calendar_day_height);
    }

    @DexIgnore
    public void a(k kVar) {
        this.f = kVar;
        if (kVar == k.YEAR) {
            this.h.getLayoutManager().i(((ot3) this.h.getAdapter()).b(this.e.d));
            this.j.setVisibility(0);
            this.p.setVisibility(8);
        } else if (kVar == k.DAY) {
            this.j.setVisibility(8);
            this.p.setVisibility(0);
            a(this.e);
        }
    }

    @DexIgnore
    public final void a(View view, jt3 jt3) {
        MaterialButton materialButton = (MaterialButton) view.findViewById(nr3.month_navigation_fragment_toggle);
        materialButton.setTag(t);
        da.a(materialButton, new f());
        MaterialButton materialButton2 = (MaterialButton) view.findViewById(nr3.month_navigation_previous);
        materialButton2.setTag(r);
        MaterialButton materialButton3 = (MaterialButton) view.findViewById(nr3.month_navigation_next);
        materialButton3.setTag(s);
        this.j = view.findViewById(nr3.mtrl_calendar_year_selector_frame);
        this.p = view.findViewById(nr3.mtrl_calendar_day_selector_frame);
        a(k.DAY);
        materialButton.setText(this.e.b());
        this.i.addOnScrollListener(new g(jt3, materialButton));
        materialButton.setOnClickListener(new h());
        materialButton3.setOnClickListener(new i(jt3));
        materialButton2.setOnClickListener(new j(jt3));
    }
}
