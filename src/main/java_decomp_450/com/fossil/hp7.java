package com.fossil;

import com.facebook.stetho.websocket.WebSocketHandler;
import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp7 implements Interceptor {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends cr7 {
        @DexIgnore
        public long b;

        @DexIgnore
        public a(qr7 qr7) {
            super(qr7);
        }

        @DexIgnore
        @Override // com.fossil.qr7, com.fossil.cr7
        public void a(yq7 yq7, long j) throws IOException {
            super.a(yq7, j);
            this.b += j;
        }
    }

    @DexIgnore
    public hp7(boolean z) {
        this.a = z;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response;
        mp7 mp7 = (mp7) chain;
        ip7 h = mp7.h();
        fp7 i = mp7.i();
        bp7 bp7 = (bp7) mp7.d();
        lo7 c = mp7.c();
        long currentTimeMillis = System.currentTimeMillis();
        mp7.g().d(mp7.f());
        h.a(c);
        mp7.g().a(mp7.f(), c);
        Response.a aVar = null;
        if (lp7.b(c.e()) && c.a() != null) {
            if ("100-continue".equalsIgnoreCase(c.a("Expect"))) {
                h.b();
                mp7.g().f(mp7.f());
                aVar = h.a(true);
            }
            if (aVar == null) {
                mp7.g().c(mp7.f());
                a aVar2 = new a(h.a(c, c.a().a()));
                zq7 a2 = ir7.a(aVar2);
                c.a().a(a2);
                a2.close();
                mp7.g().a(mp7.f(), aVar2.b);
            } else if (!bp7.e()) {
                i.e();
            }
        }
        h.a();
        if (aVar == null) {
            mp7.g().f(mp7.f());
            aVar = h.a(false);
        }
        aVar.a(c);
        aVar.a(i.c().d());
        aVar.b(currentTimeMillis);
        aVar.a(System.currentTimeMillis());
        Response a3 = aVar.a();
        int e = a3.e();
        if (e == 100) {
            Response.a a4 = h.a(false);
            a4.a(c);
            a4.a(i.c().d());
            a4.b(currentTimeMillis);
            a4.a(System.currentTimeMillis());
            a3 = a4.a();
            e = a3.e();
        }
        mp7.g().a(mp7.f(), a3);
        if (!this.a || e != 101) {
            Response.a q = a3.q();
            q.a(h.a(a3));
            response = q.a();
        } else {
            Response.a q2 = a3.q();
            q2.a(ro7.c);
            response = q2.a();
        }
        if ("close".equalsIgnoreCase(response.x().a(WebSocketHandler.HEADER_CONNECTION)) || "close".equalsIgnoreCase(response.b(WebSocketHandler.HEADER_CONNECTION))) {
            i.e();
        }
        if ((e != 204 && e != 205) || response.a().contentLength() <= 0) {
            return response;
        }
        throw new ProtocolException("HTTP " + e + " had non-zero Content-Length: " + response.a().contentLength());
    }
}
