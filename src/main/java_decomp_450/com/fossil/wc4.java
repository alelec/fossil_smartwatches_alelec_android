package com.fossil;

import com.google.firebase.messaging.FirebaseMessaging;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wc4 implements jo3 {
    @DexIgnore
    public /* final */ FirebaseMessaging a;

    @DexIgnore
    public wc4(FirebaseMessaging firebaseMessaging) {
        this.a = firebaseMessaging;
    }

    @DexIgnore
    @Override // com.fossil.jo3
    public final void onSuccess(Object obj) {
        this.a.a((md4) obj);
    }
}
