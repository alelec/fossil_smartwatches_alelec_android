package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.portfolio.platform.view.indicator.CustomPageIndicator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nx4 extends mx4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i u; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray v;
    @DexIgnore
    public long t;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        v = sparseIntArray;
        sparseIntArray.put(2131363095, 1);
        v.put(2131362602, 2);
    }
    */

    @DexIgnore
    public nx4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 3, u, v));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.t = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.t != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.t = 1;
        }
        g();
    }

    @DexIgnore
    public nx4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (CustomPageIndicator) objArr[2], (ConstraintLayout) objArr[0], (ViewPager2) objArr[1]);
        this.t = -1;
        ((mx4) this).r.setTag(null);
        a(view);
        f();
    }
}
