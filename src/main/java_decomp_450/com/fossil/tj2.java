package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj2 implements Parcelable.Creator<qj2> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qj2 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        gc2 gc2 = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 1) {
                j72.v(parcel, a);
            } else {
                gc2 = (gc2) j72.a(parcel, a, gc2.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new qj2(gc2);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ qj2[] newArray(int i) {
        return new qj2[i];
    }
}
