package com.fossil;

import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface l07 {
    @DexIgnore
    public static final l07 a = new a();
    @DexIgnore
    public static final l07 b = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements l07 {
        @DexIgnore
        @Override // com.fossil.l07
        public void a(e07 e07) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements l07 {
        @DexIgnore
        @Override // com.fossil.l07
        public void a(e07 e07) {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                throw new IllegalStateException("Event bus " + e07 + " accessed from non-main thread " + Looper.myLooper());
            }
        }
    }

    @DexIgnore
    void a(e07 e07);
}
