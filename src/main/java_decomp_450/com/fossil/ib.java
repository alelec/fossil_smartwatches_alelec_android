package com.fossil;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import androidx.collection.SparseArrayCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.jb;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ib extends g9 {
    @DexIgnore
    public static /* final */ Rect n; // = new Rect(Integer.MAX_VALUE, Integer.MAX_VALUE, RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    @DexIgnore
    public static /* final */ jb.a<oa> o; // = new a();
    @DexIgnore
    public static /* final */ jb.b<SparseArrayCompat<oa>, oa> p; // = new b();
    @DexIgnore
    public /* final */ Rect d; // = new Rect();
    @DexIgnore
    public /* final */ Rect e; // = new Rect();
    @DexIgnore
    public /* final */ Rect f; // = new Rect();
    @DexIgnore
    public /* final */ int[] g; // = new int[2];
    @DexIgnore
    public /* final */ AccessibilityManager h;
    @DexIgnore
    public /* final */ View i;
    @DexIgnore
    public c j;
    @DexIgnore
    public int k; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int l; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int m; // = RecyclerView.UNDEFINED_DURATION;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements jb.a<oa> {
        @DexIgnore
        public void a(oa oaVar, Rect rect) {
            oaVar.a(rect);
        }
    }

    @DexIgnore
    public ib(View view) {
        if (view != null) {
            this.i = view;
            this.h = (AccessibilityManager) view.getContext().getSystemService("accessibility");
            view.setFocusable(true);
            if (da.n(view) == 0) {
                da.h(view, 1);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("View may not be null");
    }

    @DexIgnore
    public static int i(int i2) {
        if (i2 == 19) {
            return 33;
        }
        if (i2 != 21) {
            return i2 != 22 ? 130 : 66;
        }
        return 17;
    }

    @DexIgnore
    public abstract int a(float f2, float f3);

    @DexIgnore
    @Override // com.fossil.g9
    public pa a(View view) {
        if (this.j == null) {
            this.j = new c();
        }
        return this.j;
    }

    @DexIgnore
    public void a(int i2, AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public abstract void a(int i2, oa oaVar);

    @DexIgnore
    public void a(int i2, boolean z) {
    }

    @DexIgnore
    public void a(AccessibilityEvent accessibilityEvent) {
    }

    @DexIgnore
    public void a(oa oaVar) {
    }

    @DexIgnore
    public abstract void a(List<Integer> list);

    @DexIgnore
    public abstract boolean a(int i2, int i3, Bundle bundle);

    @DexIgnore
    public final boolean b(int i2, Rect rect) {
        oa oaVar;
        oa oaVar2;
        SparseArrayCompat<oa> e2 = e();
        int i3 = this.l;
        int i4 = RecyclerView.UNDEFINED_DURATION;
        if (i3 == Integer.MIN_VALUE) {
            oaVar = null;
        } else {
            oaVar = e2.a(i3);
        }
        if (i2 == 1 || i2 == 2) {
            oaVar2 = (oa) jb.a(e2, p, o, oaVar, i2, da.p(this.i) == 1, false);
        } else if (i2 == 17 || i2 == 33 || i2 == 66 || i2 == 130) {
            Rect rect2 = new Rect();
            int i5 = this.l;
            if (i5 != Integer.MIN_VALUE) {
                a(i5, rect2);
            } else if (rect != null) {
                rect2.set(rect);
            } else {
                a(this.i, i2, rect2);
            }
            oaVar2 = (oa) jb.a(e2, p, o, oaVar, rect2, i2);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_FORWARD, FOCUS_BACKWARD, FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        if (oaVar2 != null) {
            i4 = e2.c(e2.a(oaVar2));
        }
        return g(i4);
    }

    @DexIgnore
    public final boolean c(int i2, int i3) {
        ViewParent parent;
        if (i2 == Integer.MIN_VALUE || !this.h.isEnabled() || (parent = this.i.getParent()) == null) {
            return false;
        }
        return ga.a(parent, this.i, a(i2, i3));
    }

    @DexIgnore
    public final int d() {
        return this.k;
    }

    @DexIgnore
    public final SparseArrayCompat<oa> e() {
        ArrayList arrayList = new ArrayList();
        a(arrayList);
        SparseArrayCompat<oa> sparseArrayCompat = new SparseArrayCompat<>();
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            sparseArrayCompat.c(i2, d(i2));
        }
        return sparseArrayCompat;
    }

    @DexIgnore
    public final int f() {
        return this.l;
    }

    @DexIgnore
    public final boolean g(int i2) {
        int i3;
        if ((!this.i.isFocused() && !this.i.requestFocus()) || (i3 = this.l) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            b(i3);
        }
        this.l = i2;
        a(i2, true);
        c(i2, 8);
        return true;
    }

    @DexIgnore
    public final void h(int i2) {
        int i3 = this.m;
        if (i3 != i2) {
            this.m = i2;
            c(i2, 128);
            c(i3, 256);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements jb.b<SparseArrayCompat<oa>, oa> {
        @DexIgnore
        public oa a(SparseArrayCompat<oa> sparseArrayCompat, int i) {
            return sparseArrayCompat.e(i);
        }

        @DexIgnore
        public int a(SparseArrayCompat<oa> sparseArrayCompat) {
            return sparseArrayCompat.h();
        }
    }

    @DexIgnore
    public final oa d(int i2) {
        oa B = oa.B();
        B.h(true);
        B.i(true);
        B.a("android.view.View");
        B.c(n);
        B.d(n);
        B.e(this.i);
        a(i2, B);
        if (B.j() == null && B.f() == null) {
            throw new RuntimeException("Callbacks must add text or a content description in populateNodeForVirtualViewId()");
        }
        B.a(this.e);
        if (!this.e.equals(n)) {
            int c2 = B.c();
            if ((c2 & 64) != 0) {
                throw new RuntimeException("Callbacks must not add ACTION_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            } else if ((c2 & 128) == 0) {
                B.e(this.i.getContext().getPackageName());
                B.c(this.i, i2);
                if (this.k == i2) {
                    B.a(true);
                    B.a(128);
                } else {
                    B.a(false);
                    B.a(64);
                }
                boolean z = this.l == i2;
                if (z) {
                    B.a(2);
                } else if (B.r()) {
                    B.a(1);
                }
                B.j(z);
                this.i.getLocationOnScreen(this.g);
                B.b(this.d);
                if (this.d.equals(n)) {
                    B.a(this.d);
                    if (B.b != -1) {
                        oa B2 = oa.B();
                        for (int i3 = B.b; i3 != -1; i3 = B2.b) {
                            B2.b(this.i, -1);
                            B2.c(n);
                            a(i3, B2);
                            B2.a(this.e);
                            Rect rect = this.d;
                            Rect rect2 = this.e;
                            rect.offset(rect2.left, rect2.top);
                        }
                        B2.z();
                    }
                    this.d.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                }
                if (this.i.getLocalVisibleRect(this.f)) {
                    this.f.offset(this.g[0] - this.i.getScrollX(), this.g[1] - this.i.getScrollY());
                    if (this.d.intersect(this.f)) {
                        B.d(this.d);
                        if (a(this.d)) {
                            B.q(true);
                        }
                    }
                }
                return B;
            } else {
                throw new RuntimeException("Callbacks must not add ACTION_CLEAR_ACCESSIBILITY_FOCUS in populateNodeForVirtualViewId()");
            }
        } else {
            throw new RuntimeException("Callbacks must set parent bounds in populateNodeForVirtualViewId()");
        }
    }

    @DexIgnore
    public final boolean f(int i2) {
        int i3;
        if (!this.h.isEnabled() || !this.h.isTouchExplorationEnabled() || (i3 = this.k) == i2) {
            return false;
        }
        if (i3 != Integer.MIN_VALUE) {
            a(i3);
        }
        this.k = i2;
        this.i.invalidate();
        c(i2, 32768);
        return true;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends pa {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.pa
        public oa a(int i) {
            return oa.a(ib.this.e(i));
        }

        @DexIgnore
        @Override // com.fossil.pa
        public oa b(int i) {
            int i2 = i == 2 ? ib.this.k : ib.this.l;
            if (i2 == Integer.MIN_VALUE) {
                return null;
            }
            return a(i2);
        }

        @DexIgnore
        @Override // com.fossil.pa
        public boolean a(int i, int i2, Bundle bundle) {
            return ib.this.b(i, i2, bundle);
        }
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        if (!this.h.isEnabled() || !this.h.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 7 || action == 9) {
            int a2 = a(motionEvent.getX(), motionEvent.getY());
            h(a2);
            if (a2 != Integer.MIN_VALUE) {
                return true;
            }
            return false;
        } else if (action != 10 || this.m == Integer.MIN_VALUE) {
            return false;
        } else {
            h(RecyclerView.UNDEFINED_DURATION);
            return true;
        }
    }

    @DexIgnore
    public final AccessibilityEvent c(int i2) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i2);
        this.i.onInitializeAccessibilityEvent(obtain);
        return obtain;
    }

    @DexIgnore
    public final oa c() {
        oa g2 = oa.g(this.i);
        da.a(this.i, g2);
        ArrayList arrayList = new ArrayList();
        a(arrayList);
        if (g2.d() <= 0 || arrayList.size() <= 0) {
            int size = arrayList.size();
            for (int i2 = 0; i2 < size; i2++) {
                g2.a(this.i, ((Integer) arrayList.get(i2)).intValue());
            }
            return g2;
        }
        throw new RuntimeException("Views cannot have both real and virtual children");
    }

    @DexIgnore
    public oa e(int i2) {
        if (i2 == -1) {
            return c();
        }
        return d(i2);
    }

    @DexIgnore
    public final boolean a(KeyEvent keyEvent) {
        int i2 = 0;
        if (keyEvent.getAction() == 1) {
            return false;
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode != 61) {
            if (keyCode != 66) {
                switch (keyCode) {
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                        if (!keyEvent.hasNoModifiers()) {
                            return false;
                        }
                        int i3 = i(keyCode);
                        int repeatCount = keyEvent.getRepeatCount() + 1;
                        boolean z = false;
                        while (i2 < repeatCount && b(i3, (Rect) null)) {
                            i2++;
                            z = true;
                        }
                        return z;
                    case 23:
                        break;
                    default:
                        return false;
                }
            }
            if (!keyEvent.hasNoModifiers() || keyEvent.getRepeatCount() != 0) {
                return false;
            }
            b();
            return true;
        } else if (keyEvent.hasNoModifiers()) {
            return b(2, (Rect) null);
        } else {
            if (keyEvent.hasModifiers(1)) {
                return b(1, (Rect) null);
            }
            return false;
        }
    }

    @DexIgnore
    public final boolean c(int i2, int i3, Bundle bundle) {
        if (i3 == 1) {
            return g(i2);
        }
        if (i3 == 2) {
            return b(i2);
        }
        if (i3 == 64) {
            return f(i2);
        }
        if (i3 != 128) {
            return a(i2, i3, bundle);
        }
        return a(i2);
    }

    @DexIgnore
    public final boolean b() {
        int i2 = this.l;
        return i2 != Integer.MIN_VALUE && a(i2, 16, null);
    }

    @DexIgnore
    @Override // com.fossil.g9
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.b(view, accessibilityEvent);
        a(accessibilityEvent);
    }

    @DexIgnore
    public final AccessibilityEvent b(int i2, int i3) {
        AccessibilityEvent obtain = AccessibilityEvent.obtain(i3);
        oa e2 = e(i2);
        obtain.getText().add(e2.j());
        obtain.setContentDescription(e2.f());
        obtain.setScrollable(e2.v());
        obtain.setPassword(e2.u());
        obtain.setEnabled(e2.q());
        obtain.setChecked(e2.o());
        a(i2, obtain);
        if (!obtain.getText().isEmpty() || obtain.getContentDescription() != null) {
            obtain.setClassName(e2.e());
            qa.a(obtain, this.i, i2);
            obtain.setPackageName(this.i.getContext().getPackageName());
            return obtain;
        }
        throw new RuntimeException("Callbacks must add text or a content description in populateEventForVirtualViewId()");
    }

    @DexIgnore
    public final void a(boolean z, int i2, Rect rect) {
        int i3 = this.l;
        if (i3 != Integer.MIN_VALUE) {
            b(i3);
        }
        if (z) {
            b(i2, rect);
        }
    }

    @DexIgnore
    public final void a(int i2, Rect rect) {
        e(i2).a(rect);
    }

    @DexIgnore
    public static Rect a(View view, int i2, Rect rect) {
        int width = view.getWidth();
        int height = view.getHeight();
        if (i2 == 17) {
            rect.set(width, 0, width, height);
        } else if (i2 == 33) {
            rect.set(0, height, width, height);
        } else if (i2 == 66) {
            rect.set(-1, 0, -1, height);
        } else if (i2 == 130) {
            rect.set(0, -1, width, -1);
        } else {
            throw new IllegalArgumentException("direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT}.");
        }
        return rect;
    }

    @DexIgnore
    public boolean b(int i2, int i3, Bundle bundle) {
        if (i2 != -1) {
            return c(i2, i3, bundle);
        }
        return a(i3, bundle);
    }

    @DexIgnore
    public final AccessibilityEvent a(int i2, int i3) {
        if (i2 != -1) {
            return b(i2, i3);
        }
        return c(i3);
    }

    @DexIgnore
    public final boolean b(int i2) {
        if (this.l != i2) {
            return false;
        }
        this.l = RecyclerView.UNDEFINED_DURATION;
        a(i2, false);
        c(i2, 8);
        return true;
    }

    @DexIgnore
    @Override // com.fossil.g9
    public void a(View view, oa oaVar) {
        super.a(view, oaVar);
        a(oaVar);
    }

    @DexIgnore
    public final boolean a(int i2, Bundle bundle) {
        return da.a(this.i, i2, bundle);
    }

    @DexIgnore
    public final boolean a(Rect rect) {
        if (rect == null || rect.isEmpty() || this.i.getWindowVisibility() != 0) {
            return false;
        }
        ViewParent parent = this.i.getParent();
        while (parent instanceof View) {
            View view = (View) parent;
            if (view.getAlpha() <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || view.getVisibility() != 0) {
                return false;
            }
            parent = view.getParent();
        }
        if (parent != null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean a(int i2) {
        if (this.k != i2) {
            return false;
        }
        this.k = RecyclerView.UNDEFINED_DURATION;
        this.i.invalidate();
        c(i2, 65536);
        return true;
    }
}
