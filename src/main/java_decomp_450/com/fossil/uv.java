package com.fossil;

import com.fossil.av;
import java.io.UnsupportedEncodingException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uv<T> extends yu<T> {
    @DexIgnore
    public static /* final */ String PROTOCOL_CHARSET; // = "utf-8";
    @DexIgnore
    public static /* final */ String PROTOCOL_CONTENT_TYPE; // = String.format("application/json; charset=%s", PROTOCOL_CHARSET);
    @DexIgnore
    public av.b<T> mListener;
    @DexIgnore
    public /* final */ Object mLock;
    @DexIgnore
    public /* final */ String mRequestBody;

    @DexIgnore
    @Deprecated
    public uv(String str, String str2, av.b<T> bVar, av.a aVar) {
        this(-1, str, str2, bVar, aVar);
    }

    @DexIgnore
    @Override // com.fossil.yu
    public void cancel() {
        super.cancel();
        synchronized (this.mLock) {
            this.mListener = null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yu
    public void deliverResponse(T t) {
        av.b<T> bVar;
        synchronized (this.mLock) {
            bVar = this.mListener;
        }
        if (bVar != null) {
            bVar.onResponse(t);
        }
    }

    @DexIgnore
    @Override // com.fossil.yu
    public byte[] getBody() {
        try {
            if (this.mRequestBody == null) {
                return null;
            }
            return this.mRequestBody.getBytes(PROTOCOL_CHARSET);
        } catch (UnsupportedEncodingException unused) {
            gv.e("Unsupported Encoding while trying to get the bytes of %s using %s", this.mRequestBody, PROTOCOL_CHARSET);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.yu
    public String getBodyContentType() {
        return PROTOCOL_CONTENT_TYPE;
    }

    @DexIgnore
    @Override // com.fossil.yu
    @Deprecated
    public byte[] getPostBody() {
        return getBody();
    }

    @DexIgnore
    @Override // com.fossil.yu
    @Deprecated
    public String getPostBodyContentType() {
        return getBodyContentType();
    }

    @DexIgnore
    @Override // com.fossil.yu
    public abstract av<T> parseNetworkResponse(vu vuVar);

    @DexIgnore
    public uv(int i, String str, String str2, av.b<T> bVar, av.a aVar) {
        super(i, str, aVar);
        this.mLock = new Object();
        this.mListener = bVar;
        this.mRequestBody = str2;
    }
}
