package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<pb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public pb0 createFromParcel(Parcel parcel) {
            return new pb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public pb0[] newArray(int i) {
            return new pb0[i];
        }
    }

    @DexIgnore
    public pb0(byte b, long j, int i) {
        super(cb0.WORKOUT_STOP, b);
        this.c = j;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.G5, Long.valueOf(this.c)), r51.p, Integer.valueOf(this.d));
    }

    @DexIgnore
    public final int c() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(pb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            pb0 pb0 = (pb0) obj;
            return this.c == pb0.c && this.d == pb0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.StopWorkoutNotification");
    }

    @DexIgnore
    public final long getSessionId() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = Long.valueOf(this.c).hashCode();
        return Integer.valueOf(this.d).hashCode() + ((hashCode + (super.hashCode() * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeLong(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ pb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.c = parcel.readLong();
        this.d = parcel.readInt();
    }
}
