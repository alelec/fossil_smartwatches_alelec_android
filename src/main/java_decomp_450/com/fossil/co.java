package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.zendesk.sdk.support.help.HelpRecyclerViewAdapter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class co extends Cdo<Boolean> {
    @DexIgnore
    public static /* final */ String i; // = im.a("BatteryNotLowTracker");

    @DexIgnore
    public co(Context context, vp vpVar) {
        super(context, vpVar);
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public IntentFilter d() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.BATTERY_OKAY");
        intentFilter.addAction("android.intent.action.BATTERY_LOW");
        return intentFilter;
    }

    @DexIgnore
    @Override // com.fossil.eo
    public Boolean a() {
        Intent registerReceiver = ((eo) this).b.registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        boolean z = false;
        if (registerReceiver == null) {
            im.a().b(i, "getInitialState - null intent received", new Throwable[0]);
            return null;
        }
        int intExtra = registerReceiver.getIntExtra("status", -1);
        float intExtra2 = ((float) registerReceiver.getIntExtra(HelpRecyclerViewAdapter.CategoryViewHolder.ROTATION_PROPERTY_NAME, -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
        if (intExtra == 1 || intExtra2 > 0.15f) {
            z = true;
        }
        return Boolean.valueOf(z);
    }

    @DexIgnore
    @Override // com.fossil.Cdo
    public void a(Context context, Intent intent) {
        if (intent.getAction() != null) {
            im.a().a(i, String.format("Received %s", intent.getAction()), new Throwable[0]);
            String action = intent.getAction();
            char c = '\uffff';
            int hashCode = action.hashCode();
            if (hashCode != -1980154005) {
                if (hashCode == 490310653 && action.equals("android.intent.action.BATTERY_LOW")) {
                    c = 1;
                }
            } else if (action.equals("android.intent.action.BATTERY_OKAY")) {
                c = 0;
            }
            if (c == 0) {
                a((Object) true);
            } else if (c == 1) {
                a((Object) false);
            }
        }
    }
}
