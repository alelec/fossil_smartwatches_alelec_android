package com.fossil;

import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.places.model.PlaceFields;
import com.fossil.fl4;
import com.fossil.rm5;
import com.fossil.um5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.portfolio.platform.PortfolioApp;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to6 extends he {
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public Location b;
    @DexIgnore
    public /* final */ Geocoder c; // = new Geocoder(this.d);
    @DexIgnore
    public /* final */ PortfolioApp d;
    @DexIgnore
    public /* final */ um5 e;
    @DexIgnore
    public /* final */ rm5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Double b;
        @DexIgnore
        public Double c;
        @DexIgnore
        public String d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public Boolean f;
        @DexIgnore
        public Boolean g;

        @DexIgnore
        public b(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
            this.a = num;
            this.b = d2;
            this.c = d3;
            this.d = str;
            this.e = bool;
            this.f = bool2;
            this.g = bool3;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final Boolean b() {
            return this.e;
        }

        @DexIgnore
        public final Integer c() {
            return this.a;
        }

        @DexIgnore
        public final Double d() {
            return this.b;
        }

        @DexIgnore
        public final Double e() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b) && ee7.a(this.c, bVar.c) && ee7.a(this.d, bVar.d) && ee7.a(this.e, bVar.e) && ee7.a(this.f, bVar.f) && ee7.a(this.g, bVar.g);
        }

        @DexIgnore
        public final Boolean f() {
            return this.g;
        }

        @DexIgnore
        public final Boolean g() {
            return this.f;
        }

        @DexIgnore
        public int hashCode() {
            Integer num = this.a;
            int i = 0;
            int hashCode = (num != null ? num.hashCode() : 0) * 31;
            Double d2 = this.b;
            int hashCode2 = (hashCode + (d2 != null ? d2.hashCode() : 0)) * 31;
            Double d3 = this.c;
            int hashCode3 = (hashCode2 + (d3 != null ? d3.hashCode() : 0)) * 31;
            String str = this.d;
            int hashCode4 = (hashCode3 + (str != null ? str.hashCode() : 0)) * 31;
            Boolean bool = this.e;
            int hashCode5 = (hashCode4 + (bool != null ? bool.hashCode() : 0)) * 31;
            Boolean bool2 = this.f;
            int hashCode6 = (hashCode5 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
            Boolean bool3 = this.g;
            if (bool3 != null) {
                i = bool3.hashCode();
            }
            return hashCode6 + i;
        }

        @DexIgnore
        public String toString() {
            return "UiDataWrapper(errorNetwork=" + this.a + ", latitude=" + this.b + ", longitude=" + this.c + ", address=" + this.d + ", enableBtnConfirm=" + this.e + ", isShowDialogLoading=" + this.f + ", isPermissionGranted=" + this.g + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<rm5.d, rm5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ to6 a;
        @DexIgnore
        public /* final */ /* synthetic */ double b;
        @DexIgnore
        public /* final */ /* synthetic */ double c;

        @DexIgnore
        public c(to6 to6, double d, double d2) {
            this.a = to6;
            this.b = d;
            this.c = d2;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(rm5.d dVar) {
            ee7.b(dVar, "responseValue");
            String a2 = dVar.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "GetCityName onSuccess - address: " + a2 + " lat " + this.b + " long " + this.c);
            this.a.b = new Location("gps");
            Location a3 = this.a.b;
            if (a3 != null) {
                a3.setLatitude(this.b);
                Location a4 = this.a.b;
                if (a4 != null) {
                    a4.setLongitude(this.c);
                    to6.a(this.a, null, null, null, a2, true, false, null, 71, null);
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public void a(rm5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetCityName onError");
            to6.a(this.a, null, null, null, ig5.a(PortfolioApp.g0.c(), 2131886547), null, false, null, 87, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<um5.d, um5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ to6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public d(to6 to6) {
            this.a = to6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(um5.d dVar) {
            ee7.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onSuccess");
            this.a.b(dVar.a(), dVar.b());
        }

        @DexIgnore
        public void a(um5.c cVar) {
            ee7.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation onError");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public to6(PortfolioApp portfolioApp, um5 um5, rm5 rm5) {
        ee7.b(portfolioApp, "mApp");
        ee7.b(um5, "mGetLocation");
        ee7.b(rm5, "mGetAddress");
        this.d = portfolioApp;
        this.e = um5;
        this.f = rm5;
    }

    @DexIgnore
    public final LiveData<b> b() {
        return this.a;
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "GetLocation");
        this.e.a(new um5.b(), new d(this));
    }

    @DexIgnore
    public final Location d() {
        return this.b;
    }

    @DexIgnore
    public final void b(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "handleLocation latitude=" + d2 + ", longitude=" + d3);
        if (d2 != 0.0d && d3 != 0.0d) {
            a(this, null, Double.valueOf(d2), Double.valueOf(d3), null, null, null, null, 121, null);
            a(d2, d3);
        }
    }

    @DexIgnore
    public final void a(double d2, double d3, String str) {
        ee7.b(str, "address");
        if (d2 != 0.0d && d3 != 0.0d && !TextUtils.isEmpty(str)) {
            a(this, null, Double.valueOf(d2), Double.valueOf(d3), str, null, false, null, 81, null);
        } else if (!TextUtils.isEmpty(str)) {
            a(str);
        } else {
            c();
        }
    }

    @DexIgnore
    public final boolean a() {
        if (NetworkUtils.isNetworkAvailable(this.d)) {
            return true;
        }
        FLogger.INSTANCE.getLocal().d("MapPickerViewModel", "No internet connection");
        a(this, 601, null, null, null, null, null, null, 126, null);
        return false;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "address");
        try {
            List<Address> fromLocationName = this.c.getFromLocationName(str, 5);
            ee7.a((Object) fromLocationName, PlaceFields.LOCATION);
            if (!fromLocationName.isEmpty()) {
                Address address = fromLocationName.get(0);
                ee7.a((Object) address, "location[0]");
                Double valueOf = Double.valueOf(address.getLatitude());
                Address address2 = fromLocationName.get(0);
                ee7.a((Object) address2, "location[0]");
                a(this, null, valueOf, Double.valueOf(address2.getLongitude()), str, null, null, null, 113, null);
                return;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MapPickerViewModel", "No lat lng found for address " + str);
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MapPickerViewModel", "Exception when get lat lng from address " + str + ' ' + e2);
            c();
        }
    }

    @DexIgnore
    public final void a(double d2, double d3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MapPickerViewModel", "GetCityName at lat: " + d2 + ", lng: " + d3);
        this.f.a(new rm5.b(d2, d3), new c(this, d2, d3));
    }

    @DexIgnore
    public static /* synthetic */ void a(to6 to6, Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3, int i, Object obj) {
        if ((i & 1) != 0) {
            num = null;
        }
        if ((i & 2) != 0) {
            d2 = null;
        }
        if ((i & 4) != 0) {
            d3 = null;
        }
        if ((i & 8) != 0) {
            str = null;
        }
        if ((i & 16) != 0) {
            bool = null;
        }
        if ((i & 32) != 0) {
            bool2 = null;
        }
        if ((i & 64) != 0) {
            bool3 = null;
        }
        to6.a(num, d2, d3, str, bool, bool2, bool3);
    }

    @DexIgnore
    public final void a(Integer num, Double d2, Double d3, String str, Boolean bool, Boolean bool2, Boolean bool3) {
        this.a.a(new b(num, d2, d3, str, bool, bool2, bool3));
    }
}
