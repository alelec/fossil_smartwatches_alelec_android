package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.PermissionData;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<PermissionData> a;
    @DexIgnore
    public a b;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(PermissionData permissionData);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ y95 a;
        @DexIgnore
        public /* final */ /* synthetic */ dp5 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.B;
                    FlexibleImageButton flexibleImageButton = this.a.a.s;
                    ee7.a((Object) flexibleImageButton, "binding.ibInfo");
                    Context context = flexibleImageButton.getContext();
                    ee7.a((Object) context, "binding.ibInfo.context");
                    List b = this.a.b.a;
                    if (b != null) {
                        aVar.a(context, "", ((PermissionData) b.get(adapterPosition)).getExternalLink());
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.dp5$b$b")
        /* renamed from: com.fossil.dp5$b$b  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0041b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public View$OnClickListenerC0041b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.a.b.a;
                    if (b != null) {
                        PermissionData permissionData = (PermissionData) b.get(adapterPosition);
                        a a2 = this.a.b.b;
                        if (a2 != null) {
                            a2.a(permissionData);
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dp5 dp5, y95 y95) {
            super(y95.d());
            ee7.b(y95, "binding");
            this.b = dp5;
            this.a = y95;
            y95.s.setOnClickListener(new a(this));
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            if (b2 != null) {
                this.a.t.setBackgroundColor(Color.parseColor(b2));
                this.a.u.setColorFilter(Color.parseColor(b2), PorterDuff.Mode.SRC_ATOP);
            }
            this.a.q.setOnClickListener(new View$OnClickListenerC0041b(this));
        }

        @DexIgnore
        public final void a(PermissionData permissionData) {
            ee7.b(permissionData, "permissionData");
            FlexibleTextView flexibleTextView = this.a.r;
            ee7.a((Object) flexibleTextView, "binding.ftvDescription");
            flexibleTextView.setText(permissionData.getShortDescription());
            FlexibleButton flexibleButton = this.a.q;
            ee7.a((Object) flexibleButton, "binding.fbGrantPermission");
            flexibleButton.setEnabled(!permissionData.isGranted());
            ImageView imageView = this.a.u;
            ee7.a((Object) imageView, "binding.ivCheck");
            int i = 0;
            imageView.setVisibility(permissionData.isGranted() ? 0 : 4);
            FlexibleImageButton flexibleImageButton = this.a.s;
            ee7.a((Object) flexibleImageButton, "binding.ibInfo");
            if (TextUtils.isEmpty(permissionData.getExternalLink())) {
                i = 4;
            }
            flexibleImageButton.setVisibility(i);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<PermissionData> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        b bVar = (b) viewHolder;
        List<PermissionData> list = this.a;
        if (list != null) {
            bVar.a(list.get(i));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        y95 a2 = y95.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemPermissionBinding.in\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(List<PermissionData> list) {
        ee7.b(list, "permissionList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(a aVar) {
        ee7.b(aVar, "listener");
        this.b = aVar;
    }
}
