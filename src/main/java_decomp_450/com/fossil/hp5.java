package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hp5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<SecondTimezoneSetting> a; // = new ArrayList();
    @DexIgnore
    public /* final */ ArrayList<c> b; // = new ArrayList<>();
    @DexIgnore
    public b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hp5 hp5, View view) {
            super(view);
            ee7.b(view, "view");
            View findViewById = view.findViewById(2131362512);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
                String b = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b)) {
                    this.a.setBackgroundColor(Color.parseColor(b));
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(int i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public SecondTimezoneSetting a;
        @DexIgnore
        public String b;
        @DexIgnore
        public a c; // = a.TYPE_HEADER;

        @DexIgnore
        public enum a {
            TYPE_HEADER,
            TYPE_VALUE
        }

        @DexIgnore
        public final void a(SecondTimezoneSetting secondTimezoneSetting) {
            this.a = secondTimezoneSetting;
        }

        @DexIgnore
        public final a b() {
            return this.c;
        }

        @DexIgnore
        public final SecondTimezoneSetting c() {
            return this.a;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final void a(String str) {
            this.b = str;
        }

        @DexIgnore
        public final void a(a aVar) {
            ee7.b(aVar, "<set-?>");
            this.c = aVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ b b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2 = this.a.b;
                if (a2 != null) {
                    a2.a(this.a.getAdapterPosition());
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(hp5 hp5, View view, b bVar) {
            super(view);
            ee7.b(view, "view");
            this.b = bVar;
            View findViewById = view.findViewById(2131362961);
            View findViewById2 = view.findViewById(2131362751);
            String b2 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
            String b3 = eh5.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                findViewById.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                findViewById2.setBackgroundColor(Color.parseColor(b3));
            }
            view.setOnClickListener(new a(this));
            View findViewById3 = view.findViewById(2131363321);
            if (findViewById3 != null) {
                this.a = (FlexibleTextView) findViewById3;
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return bb7.a(t.getTimeZoneName(), t2.getTimeZoneName());
        }
    }

    @DexIgnore
    public final void a(List<SecondTimezoneSetting> list) {
        ee7.b(list, "secondTimeZoneList");
        this.b.clear();
        this.a = list;
        List a2 = ea7.a((Iterable) list, (Comparator) new e());
        int size = a2.size();
        String str = "";
        int i = 0;
        while (i < size) {
            SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) a2.get(i);
            String timeZoneName = secondTimezoneSetting.getTimeZoneName();
            if (timeZoneName != null) {
                char[] charArray = timeZoneName.toCharArray();
                ee7.a((Object) charArray, "(this as java.lang.String).toCharArray()");
                String valueOf = String.valueOf(Character.toUpperCase(charArray[0]));
                if (!TextUtils.equals(str, valueOf)) {
                    c cVar = new c();
                    cVar.a(valueOf);
                    cVar.a(c.a.TYPE_HEADER);
                    this.b.add(cVar);
                    str = valueOf;
                }
                c cVar2 = new c();
                cVar2.a(secondTimezoneSetting);
                cVar2.a(c.a.TYPE_VALUE);
                this.b.add(cVar2);
                i++;
            } else {
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int b(String str) {
        ee7.b(str, "letter");
        int i = 0;
        for (c cVar : this.b) {
            if (cVar.b() == c.a.TYPE_HEADER && ee7.a(cVar.a(), str)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public final List<c> c() {
        return this.b;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.b.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return this.b.get(i).b().ordinal();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        if (getItemViewType(i) == c.a.TYPE_HEADER.ordinal()) {
            FlexibleTextView a2 = ((a) viewHolder).a();
            String a3 = this.b.get(i).a();
            if (a3 != null) {
                a2.setText(a3);
            } else {
                ee7.a();
                throw null;
            }
        } else {
            SecondTimezoneSetting c2 = this.b.get(i).c();
            if (c2 != null) {
                FlexibleTextView a4 = ((d) viewHolder).a();
                a4.setText(c2.getTimeZoneName() + " (" + c2.getCityCode() + ')');
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == c.a.TYPE_HEADER.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558710, viewGroup, false);
            ee7.a((Object) inflate, "v");
            return new a(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558727, viewGroup, false);
        ee7.a((Object) inflate2, "v");
        return new d(this, inflate2, this.c);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "filterText");
        if (TextUtils.isEmpty(str)) {
            a(this.a);
        } else {
            this.b.clear();
            for (SecondTimezoneSetting secondTimezoneSetting : this.a) {
                String timeZoneName = secondTimezoneSetting.getTimeZoneName();
                if (timeZoneName != null) {
                    String lowerCase = timeZoneName.toLowerCase();
                    ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    String lowerCase2 = str.toLowerCase();
                    ee7.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                    if (nh7.a((CharSequence) lowerCase, (CharSequence) lowerCase2, false, 2, (Object) null)) {
                        c cVar = new c();
                        cVar.a(secondTimezoneSetting);
                        cVar.a(c.a.TYPE_VALUE);
                        this.b.add(cVar);
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "mItemClickListener");
        this.c = bVar;
    }
}
