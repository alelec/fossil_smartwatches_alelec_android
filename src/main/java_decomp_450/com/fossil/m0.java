package com.fossil;

import android.app.Dialog;
import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m0 extends ac {
    @DexIgnore
    @Override // com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        return new l0(getContext(), getTheme());
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void setupDialog(Dialog dialog, int i) {
        if (dialog instanceof l0) {
            l0 l0Var = (l0) dialog;
            if (!(i == 1 || i == 2)) {
                if (i == 3) {
                    dialog.getWindow().addFlags(24);
                } else {
                    return;
                }
            }
            l0Var.a(1);
            return;
        }
        super.setupDialog(dialog, i);
    }
}
