package com.fossil;

import com.fossil.bw2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax2 implements by2 {
    @DexIgnore
    public static /* final */ kx2 b; // = new yw2();
    @DexIgnore
    public /* final */ kx2 a;

    @DexIgnore
    public ax2() {
        this(new cx2(yv2.a(), a()));
    }

    @DexIgnore
    public static boolean a(hx2 hx2) {
        return hx2.zza() == bw2.f.i;
    }

    @DexIgnore
    @Override // com.fossil.by2
    public final <T> cy2<T> zza(Class<T> cls) {
        ey2.a((Class<?>) cls);
        hx2 zzb = this.a.zzb(cls);
        if (zzb.zzb()) {
            if (bw2.class.isAssignableFrom(cls)) {
                return px2.a(ey2.c(), rv2.a(), zzb.zzc());
            }
            return px2.a(ey2.a(), rv2.b(), zzb.zzc());
        } else if (bw2.class.isAssignableFrom(cls)) {
            if (a(zzb)) {
                return nx2.a(cls, zzb, tx2.b(), sw2.b(), ey2.c(), rv2.a(), ix2.b());
            }
            return nx2.a(cls, zzb, tx2.b(), sw2.b(), ey2.c(), (pv2<?>) null, ix2.b());
        } else if (a(zzb)) {
            return nx2.a(cls, zzb, tx2.a(), sw2.a(), ey2.a(), rv2.b(), ix2.a());
        } else {
            return nx2.a(cls, zzb, tx2.a(), sw2.a(), ey2.b(), (pv2<?>) null, ix2.a());
        }
    }

    @DexIgnore
    public static kx2 a() {
        try {
            return (kx2) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return b;
        }
    }

    @DexIgnore
    public ax2(kx2 kx2) {
        ew2.a((Object) kx2, "messageInfoFactory");
        this.a = kx2;
    }
}
