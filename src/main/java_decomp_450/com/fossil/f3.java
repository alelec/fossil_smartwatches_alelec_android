package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f3 extends x2 {
    @DexIgnore
    public /* final */ WeakReference<Context> b;

    @DexIgnore
    public f3(Context context, Resources resources) {
        super(resources);
        this.b = new WeakReference<>(context);
    }

    @DexIgnore
    @Override // com.fossil.x2, android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        Drawable drawable = super.getDrawable(i);
        Context context = this.b.get();
        if (!(drawable == null || context == null)) {
            w2.a().a(context, i, drawable);
        }
        return drawable;
    }
}
