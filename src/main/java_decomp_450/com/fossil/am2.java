package com.fossil;

import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am2 extends ql2 {
    @DexIgnore
    public s12<h53> a;

    @DexIgnore
    public am2(s12<h53> s12) {
        a72.a(s12 != null, "listener can't be null.");
        this.a = s12;
    }

    @DexIgnore
    @Override // com.fossil.pl2
    public final void a(h53 h53) throws RemoteException {
        this.a.a(h53);
        this.a = null;
    }
}
