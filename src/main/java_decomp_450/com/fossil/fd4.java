package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fd4 implements Parcelable.Creator<ed4> {
    @DexIgnore
    public static void a(ed4 ed4, Parcel parcel, int i) {
        int a = k72.a(parcel);
        k72.a(parcel, 2, ed4.a, false);
        k72.a(parcel, a);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ed4 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 2) {
                j72.v(parcel, a);
            } else {
                bundle = j72.a(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new ed4(bundle);
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public ed4[] newArray(int i) {
        return new ed4[i];
    }
}
