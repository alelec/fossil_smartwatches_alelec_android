package com.fossil;

import android.text.TextUtils;
import com.fossil.lo7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj5 implements on7 {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a(null);
    @DexIgnore
    public AtomicInteger b; // = new AtomicInteger(0);
    @DexIgnore
    public Auth c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ ch5 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return lj5.f;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Auth $auth$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Response $response$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ lj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(fb7 fb7, lj5 lj5, MFUser mFUser, Auth auth, Response response) {
            super(2, fb7);
            this.this$0 = lj5;
            this.$user$inlined = mFUser;
            this.$auth$inlined = auth;
            this.$response$inlined = response;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(fb7, this.this$0, this.$user$inlined, this.$auth$inlined, this.$response$inlined);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                UserDatabase a = pg5.i.a();
                if (!(a == null || (userDao = a.userDao()) == null)) {
                    userDao.updateUser(this.$user$inlined);
                }
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = lj5.class.getSimpleName();
        ee7.a((Object) simpleName, "TokenAuthenticator::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public lj5(AuthApiGuestService authApiGuestService, ch5 ch5) {
        ee7.b(authApiGuestService, "mAuthApiGuestService");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = authApiGuestService;
        this.e = ch5;
    }

    @DexIgnore
    @Override // com.fossil.on7
    public lo7 authenticate(no7 no7, Response response) {
        String str;
        String str2;
        String accessToken;
        MFUser.Auth auth;
        UserDao userDao;
        ee7.b(response, "response");
        UserDatabase a2 = pg5.i.a();
        MFUser currentUser = (a2 == null || (userDao = a2.userDao()) == null) ? null : userDao.getCurrentUser();
        if (TextUtils.isEmpty((currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken())) {
            FLogger.INSTANCE.getLocal().d(f, "unauthorized: userAccessToken is null or empty");
            return null;
        }
        int i = 0;
        boolean z = this.b.getAndIncrement() == 0;
        this.c = null;
        if (currentUser != null) {
            fv7<Auth> a3 = a(currentUser, response, z);
            Auth auth2 = this.c;
            if (this.b.decrementAndGet() == 0) {
                this.c = null;
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = f;
            local.d(str3, "authenticate decrementAndGet mCount=" + this.b + " address=" + this);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = f;
            local2.d(str4, "old headers=" + response.x().c().toString());
            if (auth2 != null && (accessToken = auth2.getAccessToken()) != null) {
                currentUser.getAuth().setAccessToken(accessToken);
                MFUser.Auth auth3 = currentUser.getAuth();
                String refreshToken = auth2.getRefreshToken();
                if (refreshToken != null) {
                    auth3.setRefreshToken(refreshToken);
                    MFUser.Auth auth4 = currentUser.getAuth();
                    String y = zd5.y(auth2.getAccessTokenExpiresAt());
                    ee7.a((Object) y, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                    auth4.setAccessTokenExpiresAt(y);
                    MFUser.Auth auth5 = currentUser.getAuth();
                    Integer accessTokenExpiresIn = auth2.getAccessTokenExpiresIn();
                    if (accessTokenExpiresIn != null) {
                        i = accessTokenExpiresIn.intValue();
                    }
                    auth5.setAccessTokenExpiresIn(i);
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str5 = f;
                    local3.d(str5, "accessToken = " + accessToken + ", refreshToken = " + auth2.getRefreshToken());
                    this.e.y(accessToken);
                    this.e.a(System.currentTimeMillis());
                    ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(null, this, currentUser, auth2, response), 3, null);
                    lo7.a f2 = response.x().f();
                    f2.b("Authorization", "Bearer " + accessToken);
                    return f2.a();
                }
                ee7.a();
                throw null;
            } else if (a3 == null || a3.b() < 500 || a3.b() >= 600) {
                if (z && a3 != null) {
                    try {
                        Gson gson = new Gson();
                        mo7 a4 = a3.f().a();
                        if (a4 == null || (str = a4.string()) == null) {
                            str = a3.e();
                        }
                        ServerError serverError = (ServerError) gson.a(str, ServerError.class);
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str6 = f;
                        StringBuilder sb = new StringBuilder();
                        sb.append("forceLogout userMessage=");
                        ee7.a((Object) serverError, "serverError");
                        sb.append(serverError.getUserMessage());
                        local4.d(str6, sb.toString());
                        PortfolioApp.g0.c().a(serverError);
                    } catch (Exception e2) {
                        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                        String str7 = f;
                        local5.d(str7, "forceLogout ex=" + e2.getMessage());
                        e2.printStackTrace();
                        PortfolioApp.g0.c().a((ServerError) null);
                    }
                }
                return null;
            } else {
                ServerError serverError2 = new ServerError();
                serverError2.setCode(Integer.valueOf(a3.b()));
                mo7 c2 = a3.c();
                if (c2 == null || (str2 = c2.string()) == null) {
                    str2 = a3.e();
                }
                serverError2.setMessage(str2);
                throw new ServerErrorException(serverError2);
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0218 A[Catch:{ Exception -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0221 A[Catch:{ Exception -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x022c A[Catch:{ Exception -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0231 A[Catch:{ Exception -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x023e A[Catch:{ Exception -> 0x024a }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0245 A[Catch:{ Exception -> 0x024a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized com.fossil.fv7<com.portfolio.platform.data.Auth> a(com.portfolio.platform.data.model.MFUser r6, okhttp3.Response r7, boolean r8) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0288 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = com.fossil.lj5.f     // Catch:{ all -> 0x0288 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0288 }
            r2.<init>()     // Catch:{ all -> 0x0288 }
            java.lang.String r3 = "getAuth needProcess="
            r2.append(r3)     // Catch:{ all -> 0x0288 }
            r2.append(r8)     // Catch:{ all -> 0x0288 }
            java.lang.String r3 = " mCount="
            r2.append(r3)     // Catch:{ all -> 0x0288 }
            java.util.concurrent.atomic.AtomicInteger r3 = r5.b     // Catch:{ all -> 0x0288 }
            r2.append(r3)     // Catch:{ all -> 0x0288 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0288 }
            r0.d(r1, r2)     // Catch:{ all -> 0x0288 }
            r0 = 0
            if (r8 == 0) goto L_0x0286
            com.portfolio.platform.data.model.MFUser$Auth r1 = r6.getAuth()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = r1.getRefreshToken()     // Catch:{ all -> 0x0288 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ all -> 0x0288 }
            if (r1 == 0) goto L_0x007a
            com.fossil.ie4 r7 = new com.fossil.ie4     // Catch:{ all -> 0x0288 }
            r7.<init>()     // Catch:{ all -> 0x0288 }
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0288 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = com.fossil.lj5.f     // Catch:{ all -> 0x0288 }
            java.lang.String r2 = "Exchange Legacy Token"
            r8.d(r1, r2)     // Catch:{ all -> 0x0288 }
            java.lang.String r8 = "accessToken"
            com.portfolio.platform.data.model.MFUser$Auth r1 = r6.getAuth()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = r1.getAccessToken()     // Catch:{ all -> 0x0288 }
            r7.a(r8, r1)     // Catch:{ all -> 0x0288 }
            java.lang.String r8 = "clientId"
            com.fossil.rd5$a r1 = com.fossil.rd5.g     // Catch:{ all -> 0x0288 }
            java.lang.String r6 = r6.getUserId()     // Catch:{ all -> 0x0288 }
            java.lang.String r6 = r1.a(r6)     // Catch:{ all -> 0x0288 }
            r7.a(r8, r6)     // Catch:{ all -> 0x0288 }
            com.portfolio.platform.data.source.remote.AuthApiGuestService r6 = r5.d     // Catch:{ all -> 0x0288 }
            retrofit2.Call r6 = r6.tokenExchangeLegacy(r7)     // Catch:{ all -> 0x0288 }
            com.fossil.fv7 r6 = r6.a()     // Catch:{ all -> 0x0288 }
            java.lang.Object r6 = r6.a()     // Catch:{ all -> 0x0288 }
            com.portfolio.platform.data.Auth r6 = (com.portfolio.platform.data.Auth) r6     // Catch:{ all -> 0x0288 }
            r5.c = r6     // Catch:{ all -> 0x0288 }
            goto L_0x0286
        L_0x007a:
            com.fossil.mo7 r7 = r7.a()     // Catch:{ all -> 0x0288 }
            if (r7 == 0) goto L_0x0286
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0288 }
            r1.<init>()     // Catch:{ all -> 0x0288 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0288 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x0288 }
            java.io.InputStream r7 = r7.byteStream()     // Catch:{ all -> 0x0288 }
            r3.<init>(r7)     // Catch:{ all -> 0x0288 }
            r2.<init>(r3)     // Catch:{ all -> 0x0288 }
            java.lang.String r7 = r2.readLine()     // Catch:{ all -> 0x0288 }
            int r2 = r7.length()     // Catch:{ all -> 0x0288 }
            r3 = 0
        L_0x009c:
            if (r3 >= r2) goto L_0x00a8
            char r4 = r7.charAt(r3)     // Catch:{ all -> 0x0288 }
            r1.append(r4)     // Catch:{ all -> 0x0288 }
            int r3 = r3 + 1
            goto L_0x009c
        L_0x00a8:
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = "sb.toString()"
            com.fossil.ee7.a(r7, r1)     // Catch:{ all -> 0x0288 }
            com.fossil.ke4 r1 = new com.fossil.ke4     // Catch:{ all -> 0x0288 }
            r1.<init>()     // Catch:{ all -> 0x0288 }
            com.google.gson.JsonElement r1 = r1.a(r7)     // Catch:{ all -> 0x0288 }
            if (r1 == 0) goto L_0x00d3
            com.fossil.ie4 r1 = r1.d()     // Catch:{ all -> 0x0288 }
            if (r1 == 0) goto L_0x00d3
            java.lang.String r2 = "_error"
            com.google.gson.JsonElement r1 = r1.a(r2)     // Catch:{ all -> 0x0288 }
            if (r1 == 0) goto L_0x00d3
            int r1 = r1.b()     // Catch:{ all -> 0x0288 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0288 }
            goto L_0x00d4
        L_0x00d3:
            r1 = r0
        L_0x00d4:
            r2 = 401026(0x61e82, float:5.61957E-40)
            if (r1 != 0) goto L_0x00da
            goto L_0x00ef
        L_0x00da:
            int r3 = r1.intValue()     // Catch:{ all -> 0x0288 }
            if (r3 != r2) goto L_0x00ef
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0288 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ all -> 0x0288 }
            java.lang.String r7 = com.fossil.lj5.f     // Catch:{ all -> 0x0288 }
            java.lang.String r8 = "Exchange Legacy Token failed"
            r6.d(r7, r8)     // Catch:{ all -> 0x0288 }
            goto L_0x0286
        L_0x00ef:
            r2 = 401011(0x61e73, float:5.61936E-40)
            if (r1 != 0) goto L_0x00f6
            goto L_0x01a4
        L_0x00f6:
            int r3 = r1.intValue()     // Catch:{ all -> 0x0288 }
            if (r3 != r2) goto L_0x01a4
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0288 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = com.fossil.lj5.f     // Catch:{ all -> 0x0288 }
            java.lang.String r2 = "Refresh Token"
            r7.d(r1, r2)     // Catch:{ all -> 0x0288 }
            com.fossil.ie4 r7 = new com.fossil.ie4     // Catch:{ all -> 0x0288 }
            r7.<init>()     // Catch:{ all -> 0x0288 }
            java.lang.String r1 = "refreshToken"
            com.portfolio.platform.data.model.MFUser$Auth r6 = r6.getAuth()     // Catch:{ all -> 0x0288 }
            java.lang.String r6 = r6.getRefreshToken()     // Catch:{ all -> 0x0288 }
            r7.a(r1, r6)     // Catch:{ all -> 0x0288 }
            com.portfolio.platform.data.source.remote.AuthApiGuestService r6 = r5.d     // Catch:{ Exception -> 0x016c }
            retrofit2.Call r6 = r6.tokenRefresh(r7)     // Catch:{ Exception -> 0x016c }
            com.fossil.fv7 r6 = r6.a()     // Catch:{ Exception -> 0x016c }
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x016c }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ Exception -> 0x016c }
            java.lang.String r1 = com.fossil.lj5.f     // Catch:{ Exception -> 0x016c }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016c }
            r2.<init>()     // Catch:{ Exception -> 0x016c }
            java.lang.String r3 = "Refresh Token error="
            r2.append(r3)     // Catch:{ Exception -> 0x016c }
            if (r6 == 0) goto L_0x0142
            int r3 = r6.b()     // Catch:{ Exception -> 0x016c }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x016c }
            goto L_0x0143
        L_0x0142:
            r3 = r0
        L_0x0143:
            r2.append(r3)     // Catch:{ Exception -> 0x016c }
            java.lang.String r3 = " body="
            r2.append(r3)     // Catch:{ Exception -> 0x016c }
            if (r6 == 0) goto L_0x0152
            com.fossil.mo7 r3 = r6.c()     // Catch:{ Exception -> 0x016c }
            goto L_0x0153
        L_0x0152:
            r3 = r0
        L_0x0153:
            r2.append(r3)     // Catch:{ Exception -> 0x016c }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x016c }
            r7.d(r1, r2)     // Catch:{ Exception -> 0x016c }
            if (r6 == 0) goto L_0x0166
            java.lang.Object r7 = r6.a()     // Catch:{ Exception -> 0x016c }
            com.portfolio.platform.data.Auth r7 = (com.portfolio.platform.data.Auth) r7     // Catch:{ Exception -> 0x016c }
            goto L_0x0167
        L_0x0166:
            r7 = r0
        L_0x0167:
            r5.c = r7     // Catch:{ Exception -> 0x016c }
        L_0x0169:
            r0 = r6
            goto L_0x0286
        L_0x016c:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r1 = com.fossil.lj5.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getAuth needProcess="
            r2.append(r3)
            r2.append(r8)
            java.lang.String r8 = " mCount="
            r2.append(r8)
            java.util.concurrent.atomic.AtomicInteger r8 = r5.b
            r2.append(r8)
            java.lang.String r8 = " exception="
            r2.append(r8)
            java.lang.String r8 = r6.getMessage()
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            r7.d(r1, r8)
            r6.printStackTrace()
            monitor-exit(r5)
            return r0
        L_0x01a4:
            r2 = 401005(0x61e6d, float:5.61928E-40)
            if (r1 != 0) goto L_0x01aa
            goto L_0x01bf
        L_0x01aa:
            int r1 = r1.intValue()
            if (r1 != r2) goto L_0x01bf
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.fossil.lj5.f
            java.lang.String r8 = "Token is in black list"
            r6.d(r7, r8)
            goto L_0x0286
        L_0x01bf:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.lj5.f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "unauthorized: error = "
            r3.append(r4)
            r3.append(r7)
            java.lang.String r7 = r3.toString()
            r1.d(r2, r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r1 = com.fossil.lj5.f
            java.lang.String r2 = "Refresh Token"
            r7.d(r1, r2)
            com.fossil.ie4 r7 = new com.fossil.ie4
            r7.<init>()
            java.lang.String r1 = "refreshToken"
            com.portfolio.platform.data.model.MFUser$Auth r6 = r6.getAuth()
            java.lang.String r6 = r6.getRefreshToken()
            r7.a(r1, r6)
            com.portfolio.platform.data.source.remote.AuthApiGuestService r6 = r5.d     // Catch:{ Exception -> 0x024a }
            retrofit2.Call r6 = r6.tokenRefresh(r7)     // Catch:{ Exception -> 0x024a }
            com.fossil.fv7 r6 = r6.a()     // Catch:{ Exception -> 0x024a }
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x024a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ Exception -> 0x024a }
            java.lang.String r1 = com.fossil.lj5.f     // Catch:{ Exception -> 0x024a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x024a }
            r2.<init>()     // Catch:{ Exception -> 0x024a }
            java.lang.String r3 = "Refresh Token error="
            r2.append(r3)     // Catch:{ Exception -> 0x024a }
            if (r6 == 0) goto L_0x0221
            int r3 = r6.b()     // Catch:{ Exception -> 0x024a }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x024a }
            goto L_0x0222
        L_0x0221:
            r3 = r0
        L_0x0222:
            r2.append(r3)     // Catch:{ Exception -> 0x024a }
            java.lang.String r3 = " body="
            r2.append(r3)     // Catch:{ Exception -> 0x024a }
            if (r6 == 0) goto L_0x0231
            com.fossil.mo7 r3 = r6.c()     // Catch:{ Exception -> 0x024a }
            goto L_0x0232
        L_0x0231:
            r3 = r0
        L_0x0232:
            r2.append(r3)     // Catch:{ Exception -> 0x024a }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x024a }
            r7.d(r1, r2)     // Catch:{ Exception -> 0x024a }
            if (r6 == 0) goto L_0x0245
            java.lang.Object r7 = r6.a()     // Catch:{ Exception -> 0x024a }
            com.portfolio.platform.data.Auth r7 = (com.portfolio.platform.data.Auth) r7     // Catch:{ Exception -> 0x024a }
            goto L_0x0246
        L_0x0245:
            r7 = r0
        L_0x0246:
            r5.c = r7     // Catch:{ Exception -> 0x024a }
            goto L_0x0169
        L_0x024a:
            r6 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            com.fossil.hj5$a r1 = com.fossil.hj5.g
            java.lang.String r1 = r1.a()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getAuth needProcess="
            r2.append(r3)
            r2.append(r8)
            java.lang.String r8 = " mCount="
            r2.append(r8)
            java.util.concurrent.atomic.AtomicInteger r8 = r5.b
            r2.append(r8)
            java.lang.String r8 = " exception="
            r2.append(r8)
            java.lang.String r8 = r6.getMessage()
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            r7.d(r1, r8)
            r6.printStackTrace()
            monitor-exit(r5)
            return r0
        L_0x0286:
            monitor-exit(r5)
            return r0
        L_0x0288:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.lj5.a(com.portfolio.platform.data.model.MFUser, okhttp3.Response, boolean):com.fossil.fv7");
    }
}
