package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ni7 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Object b;

    @DexIgnore
    public ni7(Object obj, Object obj2) {
        this.a = obj;
        this.b = obj2;
    }

    @DexIgnore
    public String toString() {
        return "CompletedIdempotentResult[" + this.b + ']';
    }
}
