package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ct3 extends BaseAdapter {
    @DexIgnore
    public static /* final */ int d; // = (Build.VERSION.SDK_INT >= 26 ? 4 : 1);
    @DexIgnore
    public /* final */ Calendar a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c; // = this.a.getFirstDayOfWeek();

    @DexIgnore
    public ct3() {
        Calendar d2 = nt3.d();
        this.a = d2;
        this.b = d2.getMaximum(7);
    }

    @DexIgnore
    public final int a(int i) {
        int i2 = i + this.c;
        int i3 = this.b;
        return i2 > i3 ? i2 - i3 : i2;
    }

    @DexIgnore
    public int getCount() {
        return this.b;
    }

    @DexIgnore
    public long getItemId(int i) {
        return 0;
    }

    @DexIgnore
    @SuppressLint({"WrongConstant"})
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) view;
        if (view == null) {
            textView = (TextView) LayoutInflater.from(viewGroup.getContext()).inflate(pr3.mtrl_calendar_day_of_week, viewGroup, false);
        }
        this.a.set(7, a(i));
        textView.setText(this.a.getDisplayName(7, d, Locale.getDefault()));
        textView.setContentDescription(String.format(viewGroup.getContext().getString(rr3.mtrl_picker_day_of_week_column_header), this.a.getDisplayName(7, 2, Locale.getDefault())));
        return textView;
    }

    @DexIgnore
    public Integer getItem(int i) {
        if (i >= this.b) {
            return null;
        }
        return Integer.valueOf(a(i));
    }
}
