package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es4 implements Factory<ds4> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static /* final */ es4 a; // = new es4();
    }

    @DexIgnore
    public static es4 a() {
        return a.a;
    }

    @DexIgnore
    public static ds4 b() {
        return new ds4();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ds4 get() {
        return b();
    }
}
