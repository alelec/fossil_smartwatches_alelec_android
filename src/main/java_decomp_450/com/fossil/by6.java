package com.fossil;

import android.widget.RadioGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class by6 implements RadioGroup.OnCheckedChangeListener {
    @DexIgnore
    private /* final */ /* synthetic */ cy6 a;
    @DexIgnore
    private /* final */ /* synthetic */ Integer b;

    @DexIgnore
    public /* synthetic */ by6(cy6 cy6, Integer num) {
        this.a = cy6;
        this.b = num;
    }

    @DexIgnore
    public final void onCheckedChanged(RadioGroup radioGroup, int i) {
        this.a.a(this.b, radioGroup, i);
    }
}
