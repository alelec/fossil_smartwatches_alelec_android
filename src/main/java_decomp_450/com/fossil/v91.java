package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v91 {
    @DexIgnore
    public /* synthetic */ v91(zd7 zd7) {
    }

    @DexIgnore
    public final pb1 a(short s) {
        pb1[] values = pb1.values();
        for (pb1 pb1 : values) {
            short s2 = pb1.a;
            if (((short) (s | s2)) == s2) {
                return pb1;
            }
        }
        return null;
    }

    @DexIgnore
    public final pb1 a(byte b) {
        pb1[] values = pb1.values();
        for (pb1 pb1 : values) {
            if (pb1.b == b) {
                return pb1;
            }
        }
        return null;
    }
}
