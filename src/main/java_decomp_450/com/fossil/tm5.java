package com.fossil;

import com.fossil.fl4;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tm5 extends fl4<b, d, c> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, "deviceId");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ DeviceLocation a;

        @DexIgnore
        public d(DeviceLocation deviceLocation) {
            ee7.b(deviceLocation, "deviceLocation");
            this.a = deviceLocation;
        }

        @DexIgnore
        public final DeviceLocation a() {
            return this.a;
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "GetLocation";
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        FLogger.INSTANCE.getLocal().d("GetLocation", "running UseCase");
        DeviceLocation deviceLocation = ah5.p.a().f().getDeviceLocation(bVar != null ? bVar.a() : null);
        if (deviceLocation != null) {
            return new d(deviceLocation);
        }
        return new c();
    }
}
