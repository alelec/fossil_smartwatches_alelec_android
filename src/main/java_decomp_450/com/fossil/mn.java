package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mn implements ym {
    @DexIgnore
    public static /* final */ String b; // = im.a("SystemAlarmScheduler");
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public mn(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    @Override // com.fossil.ym
    public void a(zo... zoVarArr) {
        for (zo zoVar : zoVarArr) {
            a(zoVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.ym
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.ym
    public void a(String str) {
        this.a.startService(in.c(this.a, str));
    }

    @DexIgnore
    public final void a(zo zoVar) {
        im.a().a(b, String.format("Scheduling work with workSpecId %s", zoVar.a), new Throwable[0]);
        this.a.startService(in.b(this.a, zoVar.a));
    }
}
