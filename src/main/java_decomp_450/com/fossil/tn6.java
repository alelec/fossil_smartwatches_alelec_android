package com.fossil;

import com.portfolio.platform.data.source.WorkoutSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn6 implements Factory<sn6> {
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> a;

    @DexIgnore
    public tn6(Provider<WorkoutSettingRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static tn6 a(Provider<WorkoutSettingRepository> provider) {
        return new tn6(provider);
    }

    @DexIgnore
    public static sn6 a(WorkoutSettingRepository workoutSettingRepository) {
        return new sn6(workoutSettingRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public sn6 get() {
        return a(this.a.get());
    }
}
