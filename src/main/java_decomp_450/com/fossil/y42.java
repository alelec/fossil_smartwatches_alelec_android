package com.fossil;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y42 {
    @DexIgnore
    public /* final */ n4<p12<?>, i02> a; // = new n4<>();
    @DexIgnore
    public /* final */ n4<p12<?>, String> b; // = new n4<>();
    @DexIgnore
    public /* final */ oo3<Map<p12<?>, String>> c; // = new oo3<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public y42(Iterable<? extends b12<?>> iterable) {
        Iterator<? extends b12<?>> it = iterable.iterator();
        while (it.hasNext()) {
            this.a.put(((b12) it.next()).a(), null);
        }
        this.d = this.a.keySet().size();
    }

    @DexIgnore
    public final no3<Map<p12<?>, String>> a() {
        return this.c.a();
    }

    @DexIgnore
    public final Set<p12<?>> b() {
        return this.a.keySet();
    }

    @DexIgnore
    public final void a(p12<?> p12, i02 i02, String str) {
        this.a.put(p12, i02);
        this.b.put(p12, str);
        this.d--;
        if (!i02.x()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.a(new x02(this.a));
            return;
        }
        this.c.a(this.b);
    }
}
