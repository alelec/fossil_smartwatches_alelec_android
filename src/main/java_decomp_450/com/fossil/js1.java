package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class js1 implements Parcelable.Creator<gm0> {
    @DexIgnore
    public /* synthetic */ js1(zd7 zd7) {
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public gm0 createFromParcel(Parcel parcel) {
        return new gm0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public gm0[] newArray(int i) {
        return new gm0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public gm0 m30createFromParcel(Parcel parcel) {
        return new gm0(parcel, null);
    }
}
