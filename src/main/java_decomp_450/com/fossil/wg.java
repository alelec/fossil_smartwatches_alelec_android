package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.fossil.ig;
import com.fossil.jg;
import com.fossil.ng;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wg<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.g<VH> {
    @DexIgnore
    public /* final */ jg<T> mDiffer;
    @DexIgnore
    public /* final */ jg.b<T> mListener; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements jg.b<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.jg.b
        public void a(List<T> list, List<T> list2) {
            wg.this.onCurrentListChanged(list, list2);
        }
    }

    @DexIgnore
    public wg(ng.d<T> dVar) {
        jg<T> jgVar = new jg<>(new hg(this), new ig.a(dVar).a());
        this.mDiffer = jgVar;
        jgVar.a(this.mListener);
    }

    @DexIgnore
    public List<T> getCurrentList() {
        return this.mDiffer.a();
    }

    @DexIgnore
    public T getItem(int i) {
        return this.mDiffer.a().get(i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.mDiffer.a().size();
    }

    @DexIgnore
    public void onCurrentListChanged(List<T> list, List<T> list2) {
    }

    @DexIgnore
    public void submitList(List<T> list) {
        this.mDiffer.a(list);
    }

    @DexIgnore
    public void submitList(List<T> list, Runnable runnable) {
        this.mDiffer.b(list, runnable);
    }

    @DexIgnore
    public wg(ig<T> igVar) {
        jg<T> jgVar = new jg<>(new hg(this), igVar);
        this.mDiffer = jgVar;
        jgVar.a(this.mListener);
    }
}
