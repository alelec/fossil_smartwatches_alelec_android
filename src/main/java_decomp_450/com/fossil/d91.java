package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d91 implements Runnable {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ gg1 b;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public d91(gg1 gg1) {
        this.b = gg1;
    }

    @DexIgnore
    public void run() {
        if (!this.a) {
            this.b.a();
            gg1 gg1 = this.b;
            d91 d91 = gg1.d;
            if (d91 != null) {
                gg1.j.postDelayed(d91, gg1.f);
            }
        }
    }
}
