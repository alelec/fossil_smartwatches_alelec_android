package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t25 extends s25 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i F; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray G;
    @DexIgnore
    public long E;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        G = sparseIntArray;
        sparseIntArray.put(2131362998, 1);
        G.put(2131363339, 2);
        G.put(2131363116, 3);
        G.put(2131362564, 4);
        G.put(2131362114, 5);
        G.put(2131363342, 6);
        G.put(2131363254, 7);
        G.put(2131362887, 8);
        G.put(2131362081, 9);
        G.put(2131362368, 10);
        G.put(2131362635, 11);
        G.put(2131362472, 12);
        G.put(2131362378, 13);
    }
    */

    @DexIgnore
    public t25(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 14, F, G));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.E = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.E != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.E = 1;
        }
        g();
    }

    @DexIgnore
    public t25(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[5], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleButton) objArr[12], (ImageButton) objArr[4], (RTLImageView) objArr[11], (ProgressBar) objArr[8], (RelativeLayout) objArr[0], (ViewPager2) objArr[1], (TabLayout) objArr[3], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[6]);
        this.E = -1;
        ((s25) this).y.setTag(null);
        a(view);
        f();
    }
}
