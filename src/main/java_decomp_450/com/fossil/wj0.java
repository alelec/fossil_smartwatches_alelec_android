package com.fossil;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wj0 {
    @DexIgnore
    public /* final */ Hashtable<ul0, LinkedHashSet<zk0>> a; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Hashtable<ul0, Integer> b;

    @DexIgnore
    public /* synthetic */ wj0(Hashtable hashtable, zd7 zd7) {
        this.b = hashtable;
        Set<ul0> keySet = this.b.keySet();
        ee7.a((Object) keySet, "resourceQuotas.keys");
        Iterator<T> it = keySet.iterator();
        while (it.hasNext()) {
            this.a.put(it.next(), new LinkedHashSet<>());
        }
    }

    @DexIgnore
    public final boolean a(zk0 zk0) {
        T t;
        synchronized (this.a) {
            synchronized (this.b) {
                t11 t11 = t11.a;
                yz0.a(zk0.y);
                String str = zk0.z;
                Iterator<ul0> it = zk0.f().iterator();
                while (it.hasNext()) {
                    ul0 next = it.next();
                    LinkedHashSet<zk0> linkedHashSet = this.a.get(next);
                    if (linkedHashSet == null) {
                        linkedHashSet = new LinkedHashSet<>();
                    }
                    Integer num = this.b.get(next);
                    if (num == null) {
                        num = 0;
                    }
                    ee7.a((Object) num, "resourceQuotas[requiredResource] ?: 0");
                    int intValue = num.intValue();
                    Iterator<T> it2 = linkedHashSet.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it2.next();
                        if (t.b(zk0)) {
                            break;
                        }
                    }
                    if (t == null) {
                        if (linkedHashSet.size() < intValue) {
                            linkedHashSet.add(zk0);
                            this.a.put(next, linkedHashSet);
                        } else {
                            Set<Map.Entry<ul0, LinkedHashSet<zk0>>> entrySet = this.a.entrySet();
                            ee7.a((Object) entrySet, "resourceHolders.entries");
                            Iterator<T> it3 = entrySet.iterator();
                            while (it3.hasNext()) {
                                ((LinkedHashSet) it3.next().getValue()).remove(zk0);
                            }
                            t11 t112 = t11.a;
                            yz0.a(zk0.y);
                            String str2 = zk0.z;
                            return false;
                        }
                    }
                }
                t11 t113 = t11.a;
                yz0.a(zk0.y);
                String str3 = zk0.z;
                return true;
            }
        }
    }

    @DexIgnore
    public final void b(zk0 zk0) {
        synchronized (this.a) {
            synchronized (this.b) {
                t11 t11 = t11.a;
                yz0.a(zk0.y);
                String str = zk0.z;
                Iterator<ul0> it = zk0.f().iterator();
                while (it.hasNext()) {
                    LinkedHashSet<zk0> linkedHashSet = this.a.get(it.next());
                    if (linkedHashSet != null) {
                        linkedHashSet.remove(zk0);
                    }
                }
                t11 t112 = t11.a;
                yz0.a(zk0.y);
                String str2 = zk0.z;
                i97 i97 = i97.a;
            }
            i97 i972 = i97.a;
        }
    }
}
