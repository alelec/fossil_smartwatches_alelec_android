package com.fossil;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rt2<E> extends ct2<E> {
    @DexIgnore
    public static /* final */ rt2<Comparable> zzb; // = new rt2<>(os2.zza(), ht2.zza);
    @DexIgnore
    public /* final */ transient os2<E> d;

    @DexIgnore
    public rt2(os2<E> os2, Comparator<? super E> comparator) {
        super(comparator);
        this.d = os2;
    }

    @DexIgnore
    public final int a(E e, boolean z) {
        os2<E> os2 = this.d;
        or2.a(e);
        int binarySearch = Collections.binarySearch(os2, e, comparator());
        if (binarySearch >= 0) {
            return z ? binarySearch + 1 : binarySearch;
        }
        return ~binarySearch;
    }

    @DexIgnore
    public final int b(E e, boolean z) {
        os2<E> os2 = this.d;
        or2.a(e);
        int binarySearch = Collections.binarySearch(os2, e, comparator());
        if (binarySearch >= 0) {
            return z ? binarySearch : binarySearch + 1;
        }
        return ~binarySearch;
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.NavigableSet
    public final E ceiling(E e) {
        int b = b(e, true);
        if (b == size()) {
            return null;
        }
        return this.d.get(b);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean contains(@NullableDecl Object obj) {
        if (obj != null) {
            try {
                if (Collections.binarySearch(this.d, obj, ((ct2) this).zza) >= 0) {
                    return true;
                }
            } catch (ClassCastException unused) {
            }
        }
        return false;
    }

    @DexIgnore
    @Override // java.util.Collection, java.util.Set, java.util.AbstractCollection
    public final boolean containsAll(Collection<?> collection) {
        if (collection instanceof it2) {
            collection = ((it2) collection).zza();
        }
        if (!vt2.a(comparator(), collection) || collection.size() <= 1) {
            return super.containsAll(collection);
        }
        yt2 yt2 = (yt2) iterator();
        Iterator<?> it = collection.iterator();
        if (!yt2.hasNext()) {
            return false;
        }
        Object next = it.next();
        Object next2 = yt2.next();
        while (true) {
            try {
                int zza = zza(next2, next);
                if (zza < 0) {
                    if (!yt2.hasNext()) {
                        return false;
                    }
                    next2 = yt2.next();
                } else if (zza == 0) {
                    if (!it.hasNext()) {
                        return true;
                    }
                    next = it.next();
                } else if (zza > 0) {
                    break;
                }
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }] */
    @Override // com.fossil.ws2
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(@org.checkerframework.checker.nullness.compatqual.NullableDecl java.lang.Object r6) {
        /*
            r5 = this;
            r0 = 1
            if (r6 != r5) goto L_0x0004
            return r0
        L_0x0004:
            boolean r1 = r6 instanceof java.util.Set
            r2 = 0
            if (r1 != 0) goto L_0x000a
            return r2
        L_0x000a:
            java.util.Set r6 = (java.util.Set) r6
            int r1 = r5.size()
            int r3 = r6.size()
            if (r1 == r3) goto L_0x0017
            return r2
        L_0x0017:
            boolean r1 = r5.isEmpty()
            if (r1 == 0) goto L_0x001e
            return r0
        L_0x001e:
            java.util.Comparator<? super E> r1 = r5.zza
            boolean r1 = com.fossil.vt2.a(r1, r6)
            if (r1 == 0) goto L_0x0049
            java.util.Iterator r6 = r6.iterator()
            java.util.Iterator r1 = r5.iterator()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
            com.fossil.yt2 r1 = (com.fossil.yt2) r1     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
        L_0x0030:
            boolean r3 = r1.hasNext()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
            if (r3 == 0) goto L_0x0047
            java.lang.Object r3 = r1.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
            java.lang.Object r4 = r6.next()     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
            if (r4 == 0) goto L_0x0046
            int r3 = r5.zza(r3, r4)     // Catch:{ ClassCastException | NoSuchElementException -> 0x0048 }
            if (r3 == 0) goto L_0x0030
        L_0x0046:
            return r2
        L_0x0047:
            return r0
        L_0x0048:
            return r2
        L_0x0049:
            boolean r6 = r5.containsAll(r6)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rt2.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.SortedSet
    public final E first() {
        if (!isEmpty()) {
            return this.d.get(0);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.NavigableSet
    public final E floor(E e) {
        int a = a(e, true) - 1;
        if (a == -1) {
            return null;
        }
        return this.d.get(a);
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.NavigableSet
    public final E higher(E e) {
        int b = b(e, false);
        if (b == size()) {
            return null;
        }
        return this.d.get(b);
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.SortedSet
    public final E last() {
        if (!isEmpty()) {
            return this.d.get(size() - 1);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    @Override // com.fossil.ct2, java.util.NavigableSet
    public final E lower(E e) {
        int a = a(e, false) - 1;
        if (a == -1) {
            return null;
        }
        return this.d.get(a);
    }

    @DexIgnore
    public final int size() {
        return this.d.size();
    }

    @DexIgnore
    @Override // com.fossil.ct2
    public final ct2<E> zza(E e, boolean z) {
        return zza(0, a(e, z));
    }

    @DexIgnore
    @Override // com.fossil.ps2
    /* renamed from: zzb */
    public final yt2<E> iterator() {
        return (yt2) this.d.iterator();
    }

    @DexIgnore
    @Override // com.fossil.ws2, com.fossil.ps2
    public final os2<E> zzc() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final Object[] zze() {
        return this.d.zze();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzf() {
        return this.d.zzf();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzg() {
        return this.d.zzg();
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return this.d.zzh();
    }

    @DexIgnore
    @Override // com.fossil.ct2
    public final ct2<E> zzi() {
        Comparator reverseOrder = Collections.reverseOrder(((ct2) this).zza);
        if (isEmpty()) {
            return ct2.zza(reverseOrder);
        }
        return new rt2(this.d.zzd(), reverseOrder);
    }

    @DexIgnore
    @Override // com.fossil.ct2
    /* renamed from: zzj */
    public final yt2<E> descendingIterator() {
        return (yt2) this.d.zzd().iterator();
    }

    @DexIgnore
    @Override // com.fossil.ct2
    public final ct2<E> zza(E e, boolean z, E e2, boolean z2) {
        return zzb(e, z).zza(e2, z2);
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        return this.d.zzb(objArr, i);
    }

    @DexIgnore
    public final rt2<E> zza(int i, int i2) {
        if (i == 0 && i2 == size()) {
            return this;
        }
        if (i < i2) {
            return new rt2<>((os2) this.d.subList(i, i2), ((ct2) this).zza);
        }
        return ct2.zza(((ct2) this).zza);
    }

    @DexIgnore
    @Override // com.fossil.ct2
    public final ct2<E> zzb(E e, boolean z) {
        return zza(b(e, z), size());
    }
}
