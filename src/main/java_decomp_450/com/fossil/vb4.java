package com.fossil;

import android.text.TextUtils;
import com.fossil.xb4;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class vb4 implements wb4 {
    public static final Object l = new Object();
    public static final ThreadFactory m = new a();
    public final l14 a;
    public final kc4 b;
    public final gc4 c;
    public final dc4 d;
    public final fc4 e;
    public final bc4 f;
    public final Object g;
    public final ExecutorService h;
    public final ExecutorService i;
    public String j;
    public final List<cc4> k;

    public class a implements ThreadFactory {
        public final AtomicInteger a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, String.format("firebase-installations-executor-%d", Integer.valueOf(this.a.getAndIncrement())));
        }
    }

    public static /* synthetic */ class b {
        public static final /* synthetic */ int[] a;
        public static final /* synthetic */ int[] b;

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001d */
        /*
        static {
            /*
                com.fossil.mc4$b[] r0 = com.fossil.mc4.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.vb4.b.b = r0
                r1 = 1
                com.fossil.mc4$b r2 = com.fossil.mc4.b.OK     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                r0 = 2
                int[] r2 = com.fossil.vb4.b.b     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.mc4$b r3 = com.fossil.mc4.b.BAD_CONFIG     // Catch:{ NoSuchFieldError -> 0x001d }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r2 = com.fossil.vb4.b.b     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.mc4$b r3 = com.fossil.mc4.b.AUTH_ERROR     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r4 = 3
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                com.fossil.lc4$b[] r2 = com.fossil.lc4.b.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.fossil.vb4.b.a = r2
                com.fossil.lc4$b r3 = com.fossil.lc4.b.OK     // Catch:{ NoSuchFieldError -> 0x0039 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = com.fossil.vb4.b.a     // Catch:{ NoSuchFieldError -> 0x0043 }
                com.fossil.lc4$b r2 = com.fossil.lc4.b.BAD_CONFIG     // Catch:{ NoSuchFieldError -> 0x0043 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0043 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0043 }
            L_0x0043:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.vb4.b.<clinit>():void");
        }
        */
    }

    public vb4(l14 l14, vd4 vd4, l94 l94) {
        this(new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m), l14, new kc4(l14.b(), vd4, l94), new gc4(l14), new dc4(), new fc4(l14), new bc4());
    }

    public static vb4 a(l14 l14) {
        a72.a(l14 != null, "Null is not a valid value of FirebaseApp.");
        return (vb4) l14.a(wb4.class);
    }

    public static vb4 j() {
        return a(l14.j());
    }

    public final String c() {
        String str = this.j;
        if (str != null) {
            return str;
        }
        hc4 g2 = g();
        this.i.execute(tb4.a(this));
        return g2.c();
    }

    public String d() {
        return this.a.d().a();
    }

    @Override // com.fossil.wb4
    public no3<Void> delete() {
        return qo3.a(this.h, sb4.a(this));
    }

    public String e() {
        return this.a.d().b();
    }

    public final hc4 f() {
        hc4 b2;
        synchronized (l) {
            qb4 a2 = qb4.a(this.a.b(), "generatefid.lock");
            try {
                b2 = this.c.b();
            } finally {
                if (a2 != null) {
                    a2.a();
                }
            }
        }
        return b2;
    }

    public final hc4 g() {
        hc4 b2;
        synchronized (l) {
            qb4 a2 = qb4.a(this.a.b(), "generatefid.lock");
            try {
                b2 = this.c.b();
                if (b2.i()) {
                    String c2 = c(b2);
                    gc4 gc4 = this.c;
                    b2 = b2.b(c2);
                    gc4.a(b2);
                }
            } finally {
                if (a2 != null) {
                    a2.a();
                }
            }
        }
        return b2;
    }

    @Override // com.fossil.wb4
    public no3<String> getId() {
        i();
        oo3 oo3 = new oo3();
        oo3.b(c());
        return oo3.a();
    }

    public String h() {
        return this.a.d().d();
    }

    public final void i() {
        a72.b(e());
        a72.b(h());
        a72.b(d());
        a72.a(dc4.b(e()), "Please set your Application ID. A valid Firebase App ID is required to communicate with Firebase server APIs: It identifies your application with Firebase.Please refer to https://firebase.google.com/support/privacy/init-options.");
        a72.a(dc4.a(d()), "Please set a valid API key. A Firebase API key is required to communicate with Firebase server APIs: It authenticates your project with Google.Please refer to https://firebase.google.com/support/privacy/init-options.");
    }

    public final hc4 d(hc4 hc4) throws IOException {
        lc4 a2 = this.b.a(d(), hc4.c(), h(), e(), hc4.c().length() == 11 ? this.e.d() : null);
        int i2 = b.a[a2.d().ordinal()];
        if (i2 == 1) {
            return hc4.a(a2.b(), a2.c(), this.d.a(), a2.a().b(), a2.a().c());
        }
        if (i2 == 2) {
            return hc4.a("BAD CONFIG");
        }
        throw new IOException();
    }

    public final void e(hc4 hc4) {
        synchronized (this.g) {
            Iterator<cc4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().a(hc4)) {
                    it.remove();
                }
            }
        }
    }

    @Override // com.fossil.wb4
    public no3<ac4> a(boolean z) {
        i();
        no3<ac4> a2 = a();
        this.h.execute(rb4.a(this, z));
        return a2;
    }

    public final void b(boolean z) {
        hc4 g2 = g();
        if (z) {
            g2 = g2.n();
        }
        e(g2);
        this.i.execute(ub4.a(this, z));
    }

    public vb4(ExecutorService executorService, l14 l14, kc4 kc4, gc4 gc4, dc4 dc4, fc4 fc4, bc4 bc4) {
        this.g = new Object();
        this.j = null;
        this.k = new ArrayList();
        this.a = l14;
        this.b = kc4;
        this.c = gc4;
        this.d = dc4;
        this.e = fc4;
        this.f = bc4;
        this.h = executorService;
        this.i = new ThreadPoolExecutor(0, 1, 30, TimeUnit.SECONDS, new LinkedBlockingQueue(), m);
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void c(boolean r3) {
        /*
            r2 = this;
            com.fossil.hc4 r0 = r2.f()
            boolean r1 = r0.h()     // Catch:{ IOException -> 0x005b }
            if (r1 != 0) goto L_0x0022
            boolean r1 = r0.k()     // Catch:{ IOException -> 0x005b }
            if (r1 == 0) goto L_0x0011
            goto L_0x0022
        L_0x0011:
            if (r3 != 0) goto L_0x001d
            com.fossil.dc4 r3 = r2.d     // Catch:{ IOException -> 0x005b }
            boolean r3 = r3.a(r0)     // Catch:{ IOException -> 0x005b }
            if (r3 == 0) goto L_0x001c
            goto L_0x001d
        L_0x001c:
            return
        L_0x001d:
            com.fossil.hc4 r3 = r2.a(r0)     // Catch:{ IOException -> 0x005b }
            goto L_0x0026
        L_0x0022:
            com.fossil.hc4 r3 = r2.d(r0)     // Catch:{ IOException -> 0x005b }
        L_0x0026:
            r2.b(r3)
            boolean r0 = r3.j()
            if (r0 == 0) goto L_0x0035
            java.lang.String r0 = r3.c()
            r2.j = r0
        L_0x0035:
            boolean r0 = r3.h()
            if (r0 == 0) goto L_0x0046
            com.fossil.xb4 r0 = new com.fossil.xb4
            com.fossil.xb4$a r1 = com.fossil.xb4.a.BAD_CONFIG
            r0.<init>(r1)
            r2.a(r3, r0)
            goto L_0x005a
        L_0x0046:
            boolean r0 = r3.i()
            if (r0 == 0) goto L_0x0057
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "cleared fid due to auth error"
            r0.<init>(r1)
            r2.a(r3, r0)
            goto L_0x005a
        L_0x0057:
            r2.e(r3)
        L_0x005a:
            return
        L_0x005b:
            r3 = move-exception
            r2.a(r0, r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vb4.c(boolean):void");
    }

    public final no3<ac4> a() {
        oo3 oo3 = new oo3();
        zb4 zb4 = new zb4(this.d, oo3);
        synchronized (this.g) {
            this.k.add(zb4);
        }
        return oo3.a();
    }

    public final void b(hc4 hc4) {
        synchronized (l) {
            qb4 a2 = qb4.a(this.a.b(), "generatefid.lock");
            try {
                this.c.a(hc4);
            } finally {
                if (a2 != null) {
                    a2.a();
                }
            }
        }
    }

    public final void a(hc4 hc4, Exception exc) {
        synchronized (this.g) {
            Iterator<cc4> it = this.k.iterator();
            while (it.hasNext()) {
                if (it.next().a(hc4, exc)) {
                    it.remove();
                }
            }
        }
    }

    public final Void b() throws xb4, IOException {
        this.j = null;
        hc4 f2 = f();
        if (f2.j()) {
            try {
                this.b.a(d(), f2.c(), h(), f2.e());
            } catch (m14 unused) {
                throw new xb4("Failed to delete a Firebase Installation.", xb4.a.BAD_CONFIG);
            }
        }
        b(f2.o());
        return null;
    }

    public final String c(hc4 hc4) {
        if ((!this.a.c().equals("CHIME_ANDROID_SDK") && !this.a.h()) || !hc4.l()) {
            return this.f.a();
        }
        String a2 = this.e.a();
        return TextUtils.isEmpty(a2) ? this.f.a() : a2;
    }

    public final hc4 a(hc4 hc4) throws IOException {
        mc4 b2 = this.b.b(d(), hc4.c(), h(), hc4.e());
        int i2 = b.b[b2.a().ordinal()];
        if (i2 == 1) {
            return hc4.a(b2.b(), b2.c(), this.d.a());
        }
        if (i2 == 2) {
            return hc4.a("BAD CONFIG");
        }
        if (i2 == 3) {
            this.j = null;
            return hc4.o();
        }
        throw new IOException();
    }
}
