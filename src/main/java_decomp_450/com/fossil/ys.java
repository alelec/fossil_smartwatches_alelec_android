package com.fossil;

import android.content.Context;
import android.util.Log;
import com.fossil.zs;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys implements zs.b {
    @DexIgnore
    public /* final */ zs a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public ys(Context context) {
        ee7.b(context, "context");
        zs a2 = zs.a.a(context, this);
        this.a = a2;
        this.b = a2.a();
        b();
        this.a.start();
    }

    @DexIgnore
    @Override // com.fossil.zs.b
    public void a(boolean z) {
        this.b = z;
        b();
    }

    @DexIgnore
    public final void b() {
        if (cu.c.a() && cu.c.b() <= 4) {
            Log.println(4, "NetworkObserver", this.b ? "ONLINE" : "OFFLINE");
        }
    }

    @DexIgnore
    public final void c() {
        if (!this.c) {
            this.c = true;
            this.a.stop();
        }
    }

    @DexIgnore
    public final boolean a() {
        return this.b;
    }
}
