package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bn0 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C;
    @DexIgnore
    public /* final */ v81 D;

    @DexIgnore
    public bn0(ri1 ri1, en0 en0, wm0 wm0, v81 v81) {
        super(ri1, en0, wm0, null, false, 24);
        this.D = v81;
        ArrayList<ul0> arrayList = ((zk0) this).i;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        v81 v812 = this.D;
        if (!(v812 instanceof qh0) && !(v812 instanceof pl1)) {
            if (v812 instanceof ey0) {
                linkedHashSet.add(ul0.DEVICE_CONFIG);
            } else if (v812 instanceof tj0) {
                linkedHashSet.add(ul0.FILE_CONFIG);
                linkedHashSet.add(ul0.TRANSFER_DATA);
            } else if (v812 instanceof nr1) {
                linkedHashSet.add(ul0.TRANSFER_DATA);
            } else if (v812 instanceof rr1) {
                linkedHashSet.add(ul0.FILE_CONFIG);
            } else if ((v812 instanceof e71) || (v812 instanceof wz0)) {
                linkedHashSet.add(ul0.FILE_CONFIG);
                linkedHashSet.add(ul0.TRANSFER_DATA);
            } else if (v812 instanceof sh0) {
                linkedHashSet.add(ul0.AUTHENTICATION);
            } else if ((v812 instanceof nl1) || (v812 instanceof mn1) || (v812 instanceof ee1)) {
                linkedHashSet.add(ul0.ASYNC);
            } else if (v812 instanceof ic1) {
                linkedHashSet.add(((ic1) v812).L.a());
            } else if (v812 instanceof uh1) {
                linkedHashSet.add(((uh1) v812).l().a());
                linkedHashSet.add(((uh1) this.D).o().a());
            } else {
                linkedHashSet.add(ul0.DEVICE_INFORMATION);
            }
        }
        this.C = yz0.a(arrayList, new ArrayList(linkedHashSet));
    }

    @DexIgnore
    public void b(v81 v81) {
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public final void h() {
        zk0.a(this, this.D, new kh0(this), hj0.a, (kd7) null, new fl0(this), (gd7) null, 40, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), this.D.g());
    }
}
