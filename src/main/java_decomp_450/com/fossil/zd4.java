package com.fossil;

import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.lang.reflect.Field;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum zd4 implements ae4 {
    IDENTITY {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return field.getName();
        }
    },
    UPPER_CAMEL_CASE {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return zd4.upperCaseFirstLetter(field.getName());
        }
    },
    UPPER_CAMEL_CASE_WITH_SPACES {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return zd4.upperCaseFirstLetter(zd4.separateCamelCase(field.getName(), " "));
        }
    },
    LOWER_CASE_WITH_UNDERSCORES {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return zd4.separateCamelCase(field.getName(), LocaleConverter.LOCALE_DELIMITER).toLowerCase(Locale.ENGLISH);
        }
    },
    LOWER_CASE_WITH_DASHES {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return zd4.separateCamelCase(field.getName(), ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).toLowerCase(Locale.ENGLISH);
        }
    },
    LOWER_CASE_WITH_DOTS {
        @DexIgnore
        @Override // com.fossil.ae4
        public String translateName(Field field) {
            return zd4.separateCamelCase(field.getName(), CodelessMatcher.CURRENT_CLASS_NAME).toLowerCase(Locale.ENGLISH);
        }
    };

    @DexIgnore
    public static String a(char c2, String str, int i) {
        if (i >= str.length()) {
            return String.valueOf(c2);
        }
        return c2 + str.substring(i);
    }

    @DexIgnore
    public static String separateCamelCase(String str, String str2) {
        StringBuilder sb = new StringBuilder();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt) && sb.length() != 0) {
                sb.append(str2);
            }
            sb.append(charAt);
        }
        return sb.toString();
    }

    @DexIgnore
    public static String upperCaseFirstLetter(String str) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        char charAt = str.charAt(0);
        int length = str.length();
        while (i < length - 1 && !Character.isLetter(charAt)) {
            sb.append(charAt);
            i++;
            charAt = str.charAt(i);
        }
        if (Character.isUpperCase(charAt)) {
            return str;
        }
        sb.append(a(Character.toUpperCase(charAt), str, i + 1));
        return sb.toString();
    }
}
