package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x90 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ da0 c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x90> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public x90 createFromParcel(Parcel parcel) {
            byte readByte = parcel.readByte();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                Parcelable readParcelable = parcel.readParcelable(da0.class.getClassLoader());
                if (readParcelable != null) {
                    return new x90(readByte, readString, (da0) readParcelable);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public x90[] newArray(int i) {
            return new x90[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ x90(byte b2, String str, da0 da0, int i, zd7 zd7) {
        this(b2, str, (i & 4) != 0 ? new da0("", new byte[0]) : da0);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.I4, Byte.valueOf(this.a)), r51.j, this.b), r51.f5, this.c.a());
    }

    @DexIgnore
    public final byte[] b() {
        byte[] bArr;
        String a2 = yz0.a(this.b);
        Charset c2 = b21.x.c();
        if (a2 != null) {
            byte[] bytes = a2.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            if (this.c.e()) {
                bArr = new byte[0];
            } else {
                String fileName = this.c.getFileName();
                Charset defaultCharset = Charset.defaultCharset();
                ee7.a((Object) defaultCharset, "Charset.defaultCharset()");
                if (fileName != null) {
                    byte[] bytes2 = fileName.getBytes(defaultCharset);
                    ee7.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                    bArr = s97.a(bytes2, (byte) 0);
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            int length = bytes.length;
            int length2 = bArr.length;
            int i = length + 8 + length2;
            byte[] array = ByteBuffer.allocate(i).order(ByteOrder.LITTLE_ENDIAN).putShort((short) i).put((byte) 8).put(this.a).putShort((short) length).putShort((short) length2).put(bytes).put(bArr).array();
            ee7.a((Object) array, "ByteBuffer.allocate(entr\u2026\n                .array()");
            return array;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] c() {
        return this.d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(x90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x90 x90 = (x90) obj;
            return this.a == x90.a && !(ee7.a(this.b, x90.b) ^ true) && !(ee7.a(this.c, x90.c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.AppNotificationReplyMessage");
    }

    @DexIgnore
    public final String getMessageContent() {
        return this.b;
    }

    @DexIgnore
    public final da0 getMessageIcon() {
        return this.c;
    }

    @DexIgnore
    public final byte getMessageId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        return this.c.hashCode() + ((hashCode + (yz0.b(this.a) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.a);
        parcel.writeString(this.b);
        parcel.writeParcelable(this.c, i);
    }

    @DexIgnore
    public x90(byte b2, String str, da0 da0) {
        this.a = b2;
        this.b = str;
        this.c = da0;
        this.d = b();
    }
}
