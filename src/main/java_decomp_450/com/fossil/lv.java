package com.fossil;

import android.os.SystemClock;
import android.text.TextUtils;
import com.fossil.mu;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lv implements mu {
    @DexIgnore
    public /* final */ Map<String, a> a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public lv(File file, int i) {
        this.a = new LinkedHashMap(16, 0.75f, true);
        this.b = 0;
        this.c = file;
        this.d = i;
    }

    @DexIgnore
    @Override // com.fossil.mu
    public synchronized mu.a a(String str) {
        a aVar = this.a.get(str);
        if (aVar == null) {
            return null;
        }
        File b2 = b(str);
        try {
            b bVar = new b(new BufferedInputStream(a(b2)), b2.length());
            try {
                a a2 = a.a(bVar);
                if (!TextUtils.equals(str, a2.b)) {
                    gv.b("%s: key=%s, found=%s", b2.getAbsolutePath(), str, a2.b);
                    e(str);
                    return null;
                }
                mu.a a3 = aVar.a(a(bVar, bVar.a()));
                bVar.close();
                return a3;
            } finally {
                bVar.close();
            }
        } catch (IOException e) {
            gv.b("%s: %s", b2.getAbsolutePath(), e.toString());
            d(str);
            return null;
        }
    }

    @DexIgnore
    public File b(String str) {
        return new File(this.c, c(str));
    }

    @DexIgnore
    public final String c(String str) {
        int length = str.length() / 2;
        String valueOf = String.valueOf(str.substring(0, length).hashCode());
        return valueOf + String.valueOf(str.substring(length).hashCode());
    }

    @DexIgnore
    public synchronized void d(String str) {
        boolean delete = b(str).delete();
        e(str);
        if (!delete) {
            gv.b("Could not delete cache entry for key=%s, filename=%s", str, c(str));
        }
    }

    @DexIgnore
    public final void e(String str) {
        a remove = this.a.remove(str);
        if (remove != null) {
            this.b -= remove.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public long a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ String c;
        @DexIgnore
        public /* final */ long d;
        @DexIgnore
        public /* final */ long e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public /* final */ long g;
        @DexIgnore
        public /* final */ List<ru> h;

        @DexIgnore
        public a(String str, String str2, long j, long j2, long j3, long j4, List<ru> list) {
            this.b = str;
            this.c = "".equals(str2) ? null : str2;
            this.d = j;
            this.e = j2;
            this.f = j3;
            this.g = j4;
            this.h = list;
        }

        @DexIgnore
        public static List<ru> a(mu.a aVar) {
            List<ru> list = aVar.h;
            if (list != null) {
                return list;
            }
            return nv.a(aVar.g);
        }

        @DexIgnore
        public static a a(b bVar) throws IOException {
            if (lv.b((InputStream) bVar) == 538247942) {
                return new a(lv.b(bVar), lv.b(bVar), lv.c(bVar), lv.c(bVar), lv.c(bVar), lv.c(bVar), lv.a(bVar));
            }
            throw new IOException();
        }

        @DexIgnore
        public a(String str, mu.a aVar) {
            this(str, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, a(aVar));
            this.a = (long) aVar.a.length;
        }

        @DexIgnore
        public mu.a a(byte[] bArr) {
            mu.a aVar = new mu.a();
            aVar.a = bArr;
            aVar.b = this.c;
            aVar.c = this.d;
            aVar.d = this.e;
            aVar.e = this.f;
            aVar.f = this.g;
            aVar.g = nv.a(this.h);
            aVar.h = Collections.unmodifiableList(this.h);
            return aVar;
        }

        @DexIgnore
        public boolean a(OutputStream outputStream) {
            try {
                lv.a(outputStream, 538247942);
                lv.a(outputStream, this.b);
                lv.a(outputStream, this.c == null ? "" : this.c);
                lv.a(outputStream, this.d);
                lv.a(outputStream, this.e);
                lv.a(outputStream, this.f);
                lv.a(outputStream, this.g);
                lv.a(this.h, outputStream);
                outputStream.flush();
                return true;
            } catch (IOException e2) {
                gv.b("%s", e2.toString());
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends FilterInputStream {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public long b;

        @DexIgnore
        public b(InputStream inputStream, long j) {
            super(inputStream);
            this.a = j;
        }

        @DexIgnore
        public long a() {
            return this.a - this.b;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read() throws IOException {
            int read = super.read();
            if (read != -1) {
                this.b++;
            }
            return read;
        }

        @DexIgnore
        @Override // java.io.FilterInputStream, java.io.InputStream
        public int read(byte[] bArr, int i, int i2) throws IOException {
            int read = super.read(bArr, i, i2);
            if (read != -1) {
                this.b += (long) read;
            }
            return read;
        }
    }

    @DexIgnore
    public OutputStream b(File file) throws FileNotFoundException {
        return new FileOutputStream(file);
    }

    @DexIgnore
    public static int b(InputStream inputStream) throws IOException {
        return (a(inputStream) << 24) | (a(inputStream) << 0) | 0 | (a(inputStream) << 8) | (a(inputStream) << 16);
    }

    @DexIgnore
    public static long c(InputStream inputStream) throws IOException {
        return ((((long) a(inputStream)) & 255) << 0) | 0 | ((((long) a(inputStream)) & 255) << 8) | ((((long) a(inputStream)) & 255) << 16) | ((((long) a(inputStream)) & 255) << 24) | ((((long) a(inputStream)) & 255) << 32) | ((((long) a(inputStream)) & 255) << 40) | ((((long) a(inputStream)) & 255) << 48) | ((255 & ((long) a(inputStream))) << 56);
    }

    @DexIgnore
    public lv(File file) {
        this(file, FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    public static String b(b bVar) throws IOException {
        return new String(a(bVar, c(bVar)), "UTF-8");
    }

    @DexIgnore
    @Override // com.fossil.mu
    public synchronized void a() {
        int i = 0;
        if (!this.c.exists()) {
            if (!this.c.mkdirs()) {
                gv.c("Unable to create cache dir %s", this.c.getAbsolutePath());
            }
            return;
        }
        File[] listFiles = this.c.listFiles();
        if (listFiles != null) {
            int length = listFiles.length;
            while (i < length) {
                File file = listFiles[i];
                try {
                    long length2 = file.length();
                    b bVar = new b(new BufferedInputStream(a(file)), length2);
                    try {
                        a a2 = a.a(bVar);
                        a2.a = length2;
                        a(a2.b, a2);
                        i++;
                    } finally {
                        bVar.close();
                    }
                } catch (IOException unused) {
                    file.delete();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.mu
    public synchronized void a(String str, mu.a aVar) {
        a(aVar.a.length);
        File b2 = b(str);
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(b(b2));
            a aVar2 = new a(str, aVar);
            if (aVar2.a(bufferedOutputStream)) {
                bufferedOutputStream.write(aVar.a);
                bufferedOutputStream.close();
                a(str, aVar2);
            } else {
                bufferedOutputStream.close();
                gv.b("Failed to write header for %s", b2.getAbsolutePath());
                throw new IOException();
            }
        } catch (IOException unused) {
            if (!b2.delete()) {
                gv.b("Could not clean up file %s", b2.getAbsolutePath());
            }
        }
    }

    @DexIgnore
    public final void a(int i) {
        long j;
        long j2 = (long) i;
        if (this.b + j2 >= ((long) this.d)) {
            if (gv.b) {
                gv.d("Pruning old cache entries.", new Object[0]);
            }
            long j3 = this.b;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Iterator<Map.Entry<String, a>> it = this.a.entrySet().iterator();
            int i2 = 0;
            while (it.hasNext()) {
                a value = it.next().getValue();
                if (b(value.b).delete()) {
                    j = j2;
                    this.b -= value.a;
                } else {
                    j = j2;
                    String str = value.b;
                    gv.b("Could not delete cache entry for key=%s, filename=%s", str, c(str));
                }
                it.remove();
                i2++;
                if (((float) (this.b + j)) < ((float) this.d) * 0.9f) {
                    break;
                }
                j2 = j;
            }
            if (gv.b) {
                gv.d("pruned %d files, %d bytes, %d ms", Integer.valueOf(i2), Long.valueOf(this.b - j3), Long.valueOf(SystemClock.elapsedRealtime() - elapsedRealtime));
            }
        }
    }

    @DexIgnore
    public final void a(String str, a aVar) {
        if (!this.a.containsKey(str)) {
            this.b += aVar.a;
        } else {
            this.b += aVar.a - this.a.get(str).a;
        }
        this.a.put(str, aVar);
    }

    @DexIgnore
    public static byte[] a(b bVar, long j) throws IOException {
        long a2 = bVar.a();
        if (j >= 0 && j <= a2) {
            int i = (int) j;
            if (((long) i) == j) {
                byte[] bArr = new byte[i];
                new DataInputStream(bVar).readFully(bArr);
                return bArr;
            }
        }
        throw new IOException("streamToBytes length=" + j + ", maxLength=" + a2);
    }

    @DexIgnore
    public InputStream a(File file) throws FileNotFoundException {
        return new FileInputStream(file);
    }

    @DexIgnore
    public static int a(InputStream inputStream) throws IOException {
        int read = inputStream.read();
        if (read != -1) {
            return read;
        }
        throw new EOFException();
    }

    @DexIgnore
    public static void a(OutputStream outputStream, int i) throws IOException {
        outputStream.write((i >> 0) & 255);
        outputStream.write((i >> 8) & 255);
        outputStream.write((i >> 16) & 255);
        outputStream.write((i >> 24) & 255);
    }

    @DexIgnore
    public static void a(OutputStream outputStream, long j) throws IOException {
        outputStream.write((byte) ((int) (j >>> 0)));
        outputStream.write((byte) ((int) (j >>> 8)));
        outputStream.write((byte) ((int) (j >>> 16)));
        outputStream.write((byte) ((int) (j >>> 24)));
        outputStream.write((byte) ((int) (j >>> 32)));
        outputStream.write((byte) ((int) (j >>> 40)));
        outputStream.write((byte) ((int) (j >>> 48)));
        outputStream.write((byte) ((int) (j >>> 56)));
    }

    @DexIgnore
    public static void a(OutputStream outputStream, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        a(outputStream, (long) bytes.length);
        outputStream.write(bytes, 0, bytes.length);
    }

    @DexIgnore
    public static void a(List<ru> list, OutputStream outputStream) throws IOException {
        if (list != null) {
            a(outputStream, list.size());
            for (ru ruVar : list) {
                a(outputStream, ruVar.a());
                a(outputStream, ruVar.b());
            }
            return;
        }
        a(outputStream, 0);
    }

    @DexIgnore
    public static List<ru> a(b bVar) throws IOException {
        int b2 = b((InputStream) bVar);
        if (b2 >= 0) {
            List<ru> emptyList = b2 == 0 ? Collections.emptyList() : new ArrayList<>();
            for (int i = 0; i < b2; i++) {
                emptyList.add(new ru(b(bVar).intern(), b(bVar).intern()));
            }
            return emptyList;
        }
        throw new IOException("readHeaderList size=" + b2);
    }
}
