package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface cw3<F, T> {
    @DexIgnore
    @CanIgnoreReturnValue
    T apply(F f);

    @DexIgnore
    boolean equals(Object obj);
}
