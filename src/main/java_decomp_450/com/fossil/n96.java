package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n96 implements Factory<m96> {
    @DexIgnore
    public static m96 a(k96 k96, UserRepository userRepository, SummariesRepository summariesRepository, PortfolioApp portfolioApp) {
        return new m96(k96, userRepository, summariesRepository, portfolioApp);
    }
}
