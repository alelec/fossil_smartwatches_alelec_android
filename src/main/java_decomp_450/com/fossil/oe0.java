package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe0 extends k60 implements Serializable, Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<oe0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public oe0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    ee7.a((Object) readString2, "parcel.readString()!!");
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        ee7.a((Object) readString3, "parcel.readString()!!");
                        return new oe0(readString, readString2, readString3);
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public oe0[] newArray(int i) {
            return new oe0[i];
        }
    }

    @DexIgnore
    public oe0(String str, String str2, String str3) {
        this.a = str;
        this.b = str2;
        this.c = str3;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D4, this.a), r51.G, this.b), r51.j, this.c);
    }

    @DexIgnore
    public final JSONObject b() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.D4, this.a), r51.G, this.b), r51.c5, this.c);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getChallengeId() {
        return this.a;
    }

    @DexIgnore
    public final String getMessage() {
        return this.c;
    }

    @DexIgnore
    public final String getName() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
    }
}
