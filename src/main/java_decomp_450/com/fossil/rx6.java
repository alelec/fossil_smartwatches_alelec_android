package com.fossil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx6 {
    @DexIgnore
    public static /* final */ a a; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final <T extends Service> void a(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            ee7.b(context, "context");
            ee7.b(cls, Constants.SERVICE);
            ee7.b(serviceConnection, "serviceConnection");
            a(context, new Intent(context, (Class<?>) cls), serviceConnection, i);
        }

        @DexIgnore
        public final <T extends Service> void b(Context context, Class<T> cls, ServiceConnection serviceConnection, int i) {
            ee7.b(context, "context");
            ee7.b(cls, Constants.SERVICE);
            ee7.b(serviceConnection, "serviceConnection");
            String str = !TextUtils.isEmpty(PortfolioApp.g0.c().c()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            Intent intent = new Intent(context, (Class<?>) cls);
            intent.setAction(str);
            FLogger.INSTANCE.getLocal().d("ServiceUtils", "startAndBindServiceConnection() - start foreground");
            a(context, intent);
            FLogger.INSTANCE.getLocal().d("ServiceUtils", "startAndBindServiceConnection() - start bind");
            a(context, intent, serviceConnection, i);
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, Class cls, String str, int i, Object obj) {
            if ((i & 4) != 0) {
                str = "";
            }
            aVar.a(context, cls, str);
        }

        @DexIgnore
        public final <T extends Service> void a(Context context, Class<T> cls, String str) {
            ee7.b(context, "context");
            ee7.b(cls, Constants.SERVICE);
            ee7.b(str, "foregroundAction");
            String str2 = !TextUtils.isEmpty(PortfolioApp.g0.c().c()) ? com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION : com.misfit.frameworks.buttonservice.utils.Constants.STOP_FOREGROUND_ACTION;
            if (TextUtils.isEmpty(str)) {
                str = str2;
            }
            Intent intent = new Intent(context, (Class<?>) cls);
            intent.setAction(str);
            a(context, intent);
        }

        @DexIgnore
        public final void a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "Service Tracking - Service Utils bindService - context=" + context + ", intent=" + intent + ", serviceConnection=" + serviceConnection + ", flags=" + i);
            try {
                context.bindService(intent, serviceConnection, i);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "bindService() - e=" + e);
            }
        }

        @DexIgnore
        public final void a(Context context, ServiceConnection serviceConnection) {
            ee7.b(context, "context");
            ee7.b(serviceConnection, "serviceConnection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "unbindService() - serviceConnection = " + serviceConnection);
            try {
                context.unbindService(serviceConnection);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "unbindService() - e=" + e);
            }
        }

        @DexIgnore
        public final void a(Context context, Intent intent) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ServiceUtils", "Service Tracking - Service Utils startForegroundServiceByIntent - intent=" + intent);
            try {
                v6.a(context, intent);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("ServiceUtils", "Service Tracking - startForegroundServiceByIntent() - e=" + e);
            }
        }
    }
}
