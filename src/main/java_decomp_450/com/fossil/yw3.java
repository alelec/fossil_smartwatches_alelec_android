package com.fossil;

import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw3 extends jz3<Object> implements Serializable {
    @DexIgnore
    public static /* final */ yw3 INSTANCE; // = new yw3();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(Object obj, Object obj2) {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E> zx3<E> immutableSortedCopy(Iterable<E> iterable) {
        return zx3.copyOf(iterable);
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <S> jz3<S> reverse() {
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jz3
    public <E> List<E> sortedCopy(Iterable<E> iterable) {
        return uy3.a(iterable);
    }

    @DexIgnore
    public String toString() {
        return "Ordering.allEqual()";
    }
}
