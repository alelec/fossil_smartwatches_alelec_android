package com.fossil;

import com.fossil.ib7;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb7 implements ib7, Serializable {
    @DexIgnore
    public static /* final */ jb7 INSTANCE; // = new jb7();
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    private final Object readResolve() {
        return INSTANCE;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <R> R fold(R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
        ee7.b(kd7, "operation");
        return r;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public <E extends ib7.b> E get(ib7.c<E> cVar) {
        ee7.b(cVar, "key");
        return null;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 minusKey(ib7.c<?> cVar) {
        ee7.b(cVar, "key");
        return this;
    }

    @DexIgnore
    @Override // com.fossil.ib7
    public ib7 plus(ib7 ib7) {
        ee7.b(ib7, "context");
        return ib7;
    }

    @DexIgnore
    public String toString() {
        return "EmptyCoroutineContext";
    }
}
