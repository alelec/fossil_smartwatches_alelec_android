package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.receiver.AppPackageRemoveReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class li5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public NotificationsRepository a;
    @DexIgnore
    public ch5 b;
    @DexIgnore
    public ad5 c;
    @DexIgnore
    public nw5 d;
    @DexIgnore
    public vu5 e;
    @DexIgnore
    public NotificationSettingsDatabase f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver$onReceive$1", f = "AppPackageInstallReceiver.kt", l = {67}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ li5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(li5 li5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = li5;
            this.$packageName = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$packageName, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                FLogger.INSTANCE.getLocal().d(li5.g, "new app installed, start full sync");
                AppFilter appFilter = new AppFilter();
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.ordinal());
                appFilter.setType(this.$packageName);
                this.this$0.a().saveAppFilter(appFilter);
                li5 li5 = this.this$0;
                this.L$0 = yi7;
                this.L$1 = appFilter;
                this.label = 1;
                if (li5.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                AppFilter appFilter2 = (AppFilter) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver", f = "AppPackageInstallReceiver.kt", l = {73}, m = "setRuleNotificationFilterToDevice")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ li5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(li5 li5, fb7 fb7) {
            super(fb7);
            this.this$0 = li5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        ee7.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public li5() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        ee7.d("mNotificationsRepository");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Uri data;
        ch5 ch5 = this.b;
        if (ch5 != null) {
            boolean F = ch5.F();
            String c2 = PortfolioApp.g0.c().c();
            String encodedSchemeSpecificPart = (intent == null || (data = intent.getData()) == null) ? null : data.getEncodedSchemeSpecificPart();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceive action ");
            sb.append(intent != null ? intent.getAction() : null);
            sb.append(" new app install ");
            sb.append(encodedSchemeSpecificPart);
            sb.append(", isAllAppEnable ");
            sb.append(F);
            local.d(str, sb.toString());
            if (F && be5.o.f(c2) && !TextUtils.isEmpty(encodedSchemeSpecificPart)) {
                ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new b(this, encodedSchemeSpecificPart, null), 3, null);
                return;
            }
            return;
        }
        ee7.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r9) {
        /*
            r8 = this;
            boolean r0 = r9 instanceof com.fossil.li5.c
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.li5$c r0 = (com.fossil.li5.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.li5$c r0 = new com.fossil.li5$c
            r0.<init>(r8, r9)
        L_0x0018:
            r6 = r0
            java.lang.Object r9 = r6.result
            java.lang.Object r0 = com.fossil.nb7.a()
            int r1 = r6.label
            r2 = 1
            if (r1 == 0) goto L_0x0036
            if (r1 != r2) goto L_0x002e
            java.lang.Object r0 = r6.L$0
            com.fossil.li5 r0 = (com.fossil.li5) r0
            com.fossil.t87.a(r9)
            goto L_0x005b
        L_0x002e:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r0)
            throw r9
        L_0x0036:
            com.fossil.t87.a(r9)
            com.fossil.nx6 r1 = com.fossil.nx6.b
            com.fossil.nw5 r9 = r8.d
            r3 = 0
            if (r9 == 0) goto L_0x008e
            com.fossil.vu5 r4 = r8.e
            if (r4 == 0) goto L_0x0088
            com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r5 = r8.f
            if (r5 == 0) goto L_0x0082
            com.fossil.ch5 r7 = r8.b
            if (r7 == 0) goto L_0x007c
            r6.L$0 = r8
            r6.label = r2
            r2 = r9
            r3 = r4
            r4 = r5
            r5 = r7
            java.lang.Object r9 = r1.a(r2, r3, r4, r5, r6)
            if (r9 != r0) goto L_0x005b
            return r0
        L_0x005b:
            java.util.List r9 = (java.util.List) r9
            com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings r0 = new com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings
            long r1 = java.lang.System.currentTimeMillis()
            r0.<init>(r9, r1)
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r9 = r9.c()
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r1 = r1.c()
            r9.a(r0, r1)
            com.fossil.i97 r9 = com.fossil.i97.a
            return r9
        L_0x007c:
            java.lang.String r9 = "mSharedPreferencesManager"
            com.fossil.ee7.d(r9)
            throw r3
        L_0x0082:
            java.lang.String r9 = "mNotificationSettingsDatabase"
            com.fossil.ee7.d(r9)
            throw r3
        L_0x0088:
            java.lang.String r9 = "mGetAllContactGroup"
            com.fossil.ee7.d(r9)
            throw r3
        L_0x008e:
            java.lang.String r9 = "mGetApps"
            com.fossil.ee7.d(r9)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.li5.a(com.fossil.fb7):java.lang.Object");
    }
}
