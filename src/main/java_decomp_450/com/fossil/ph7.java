package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ph7 extends oh7 {
    @DexIgnore
    public static final char e(CharSequence charSequence) {
        ee7.b(charSequence, "$this$first");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(0);
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }

    @DexIgnore
    public static final char f(CharSequence charSequence) {
        ee7.b(charSequence, "$this$last");
        if (!(charSequence.length() == 0)) {
            return charSequence.charAt(nh7.c(charSequence));
        }
        throw new NoSuchElementException("Char sequence is empty.");
    }
}
