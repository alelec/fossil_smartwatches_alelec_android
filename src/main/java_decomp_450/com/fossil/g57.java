package com.fossil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g57 extends SQLiteOpenHelper {
    @DexIgnore
    public String a; // = "";

    @DexIgnore
    public g57(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 3);
        this.a = str;
        context.getApplicationContext();
        if (w37.q()) {
            k57 j = x47.l;
            j.e("SQLiteOpenHelper " + this.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053 A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:25:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.database.sqlite.SQLiteDatabase r10) {
        /*
            r9 = this;
            r0 = 0
            java.lang.String r2 = "user"
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r10
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0049 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ all -> 0x0046 }
            r2.<init>()     // Catch:{ all -> 0x0046 }
            boolean r3 = r1.moveToNext()     // Catch:{ all -> 0x0046 }
            r4 = 1
            r5 = 0
            if (r3 == 0) goto L_0x0033
            java.lang.String r0 = r1.getString(r5)     // Catch:{ all -> 0x0046 }
            r1.getInt(r4)     // Catch:{ all -> 0x0046 }
            r3 = 2
            r1.getString(r3)     // Catch:{ all -> 0x0046 }
            r3 = 3
            r1.getLong(r3)     // Catch:{ all -> 0x0046 }
            java.lang.String r3 = com.fossil.a67.b(r0)     // Catch:{ all -> 0x0046 }
            java.lang.String r6 = "uid"
            r2.put(r6, r3)     // Catch:{ all -> 0x0046 }
        L_0x0033:
            if (r0 == 0) goto L_0x0040
            java.lang.String r3 = "user"
            java.lang.String r6 = "uid=?"
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0046 }
            r4[r5] = r0     // Catch:{ all -> 0x0046 }
            r10.update(r3, r2, r6, r4)     // Catch:{ all -> 0x0046 }
        L_0x0040:
            if (r1 == 0) goto L_0x0056
            r1.close()
            return
        L_0x0046:
            r10 = move-exception
            r0 = r1
            goto L_0x004a
        L_0x0049:
            r10 = move-exception
        L_0x004a:
            com.fossil.k57 r1 = com.fossil.x47.l     // Catch:{ all -> 0x0057 }
            r1.a(r10)     // Catch:{ all -> 0x0057 }
            if (r0 == 0) goto L_0x0056
            r0.close()
        L_0x0056:
            return
        L_0x0057:
            r10 = move-exception
            if (r0 == 0) goto L_0x005d
            r0.close()
        L_0x005d:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.g57.a(android.database.sqlite.SQLiteDatabase):void");
    }

    @DexIgnore
    public final void b(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            Cursor query = sQLiteDatabase.query("events", null, null, null, null, null, null);
            ArrayList<h57> arrayList = new ArrayList();
            while (query.moveToNext()) {
                arrayList.add(new h57(query.getLong(0), query.getString(1), query.getInt(2), query.getInt(3)));
            }
            ContentValues contentValues = new ContentValues();
            for (h57 h57 : arrayList) {
                contentValues.put("content", a67.b(h57.b));
                sQLiteDatabase.update("events", contentValues, "event_id=?", new String[]{Long.toString(h57.a)});
            }
            if (query != null) {
                query.close();
            }
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    @Override // java.lang.AutoCloseable
    public synchronized void close() {
        super.close();
    }

    @DexIgnore
    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("create table if not exists events(event_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, content TEXT, status INTEGER, send_count INTEGER, timestamp LONG)");
        sQLiteDatabase.execSQL("create table if not exists user(uid TEXT PRIMARY KEY, user_type INTEGER, app_ver TEXT, ts INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists config(type INTEGER PRIMARY KEY NOT NULL, content TEXT, md5sum TEXT, version INTEGER)");
        sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
        sQLiteDatabase.execSQL("CREATE INDEX if not exists status_idx ON events(status)");
    }

    @DexIgnore
    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        k57 j = x47.l;
        j.b("upgrade DB from oldVersion " + i + " to newVersion " + i2);
        if (i == 1) {
            sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
        if (i == 2) {
            a(sQLiteDatabase);
            b(sQLiteDatabase);
        }
    }
}
