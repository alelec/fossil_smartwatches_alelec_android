package com.fossil;

import com.fossil.jx;
import com.misfit.frameworks.buttonservice.log.FileLogWriter;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px implements jx<InputStream> {
    @DexIgnore
    public /* final */ c20 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements jx.a<InputStream> {
        @DexIgnore
        public /* final */ az a;

        @DexIgnore
        public a(az azVar) {
            this.a = azVar;
        }

        @DexIgnore
        @Override // com.fossil.jx.a
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public jx<InputStream> a(InputStream inputStream) {
            return new px(inputStream, this.a);
        }
    }

    @DexIgnore
    public px(InputStream inputStream, az azVar) {
        c20 c20 = new c20(inputStream, azVar);
        this.a = c20;
        c20.mark(FileLogWriter.FILE_LOG_SIZE_THRESHOLD);
    }

    @DexIgnore
    @Override // com.fossil.jx
    public void a() {
        this.a.b();
    }

    @DexIgnore
    public void c() {
        this.a.a();
    }

    @DexIgnore
    @Override // com.fossil.jx
    public InputStream b() throws IOException {
        this.a.reset();
        return this.a;
    }
}
