package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            ee7.b(str, Constants.EMAIL);
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMesagge");
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ boolean a;

        @DexIgnore
        public d(boolean z) {
            this.a = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.usecase.CheckAuthenticationEmailExisting", f = "CheckAuthenticationEmailExisting.kt", l = {27}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xv6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(xv6 xv6, fb7 fb7) {
            super(fb7);
            this.this$0 = xv6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public xv6(UserRepository userRepository) {
        ee7.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "CheckAuthenticationEmailExisting";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.xv6.b r8, com.fossil.fb7<java.lang.Object> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.fossil.xv6.e
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.xv6$e r0 = (com.fossil.xv6.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xv6$e r0 = new com.fossil.xv6$e
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 600(0x258, float:8.41E-43)
            java.lang.String r5 = ""
            if (r2 == 0) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r8 = r0.L$2
            com.portfolio.platform.data.Access r8 = (com.portfolio.platform.data.Access) r8
            java.lang.Object r8 = r0.L$1
            com.fossil.xv6$b r8 = (com.fossil.xv6.b) r8
            java.lang.Object r8 = r0.L$0
            com.fossil.xv6 r8 = (com.fossil.xv6) r8
            com.fossil.t87.a(r9)
            goto L_0x0077
        L_0x0039:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0041:
            com.fossil.t87.a(r9)
            if (r8 != 0) goto L_0x004c
            com.fossil.xv6$c r8 = new com.fossil.xv6$c
            r8.<init>(r4, r5)
            return r8
        L_0x004c:
            com.fossil.ng5 r9 = com.fossil.ng5.a()
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            com.portfolio.platform.data.Access r9 = r9.a(r2)
            if (r9 != 0) goto L_0x0062
            com.fossil.xv6$c r8 = new com.fossil.xv6$c
            r8.<init>(r4, r5)
            return r8
        L_0x0062:
            com.portfolio.platform.data.source.UserRepository r2 = r7.d
            java.lang.String r6 = r8.a()
            r0.L$0 = r7
            r0.L$1 = r8
            r0.L$2 = r9
            r0.label = r3
            java.lang.Object r9 = r2.checkAuthenticationEmailExisting(r6, r0)
            if (r9 != r1) goto L_0x0077
            return r1
        L_0x0077:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r8 = r9 instanceof com.fossil.bj5
            java.lang.String r0 = "CheckAuthenticationEmailExisting"
            if (r8 == 0) goto L_0x00bc
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "email existing "
            r1.append(r2)
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r2 = r9.a()
            if (r2 == 0) goto L_0x00b7
            java.lang.Boolean r2 = (java.lang.Boolean) r2
            boolean r2 = r2.booleanValue()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r8.d(r0, r1)
            com.fossil.xv6$d r8 = new com.fossil.xv6$d
            java.lang.Object r9 = r9.a()
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            r8.<init>(r9)
            goto L_0x00ef
        L_0x00b7:
            com.fossil.ee7.a()
            r8 = 0
            throw r8
        L_0x00bc:
            boolean r8 = r9 instanceof com.fossil.yi5
            if (r8 == 0) goto L_0x00ea
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "email existing failed "
            r1.append(r2)
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            com.portfolio.platform.data.model.ServerError r2 = r9.c()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r8.d(r0, r1)
            com.fossil.xv6$c r8 = new com.fossil.xv6$c
            int r9 = r9.a()
            r8.<init>(r9, r5)
            goto L_0x00ef
        L_0x00ea:
            com.fossil.xv6$c r8 = new com.fossil.xv6$c
            r8.<init>(r4, r5)
        L_0x00ef:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xv6.a(com.fossil.xv6$b, com.fossil.fb7):java.lang.Object");
    }
}
