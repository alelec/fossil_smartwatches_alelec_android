package com.fossil;

import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import com.fossil.i;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j {

    @DexIgnore
    public interface a extends i.d {
        @DexIgnore
        void a(String str, Bundle bundle);

        @DexIgnore
        void a(String str, List<?> list, Bundle bundle);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T extends a> extends i.e<T> {
        @DexIgnore
        public b(T t) {
            super(t);
        }

        @DexIgnore
        @Override // android.media.browse.MediaBrowser.SubscriptionCallback
        public void onChildrenLoaded(String str, List<MediaBrowser.MediaItem> list, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) ((i.e) this).a).a(str, list, bundle);
        }

        @DexIgnore
        public void onError(String str, Bundle bundle) {
            MediaSessionCompat.a(bundle);
            ((a) ((i.e) this).a).a(str, bundle);
        }
    }

    @DexIgnore
    public static Object a(a aVar) {
        return new b(aVar);
    }
}
