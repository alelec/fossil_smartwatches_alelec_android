package com.fossil;

import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m20 extends l20<Drawable> {
    @DexIgnore
    public m20(Drawable drawable) {
        super(drawable);
    }

    @DexIgnore
    public static uy<Drawable> a(Drawable drawable) {
        if (drawable != null) {
            return new m20(drawable);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.uy
    public int c() {
        return Math.max(1, ((l20) this).a.getIntrinsicWidth() * ((l20) this).a.getIntrinsicHeight() * 4);
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v1. Raw type applied. Possible types: java.lang.Class<?>, java.lang.Class<android.graphics.drawable.Drawable> */
    @Override // com.fossil.uy
    public Class<Drawable> d() {
        return ((l20) this).a.getClass();
    }
}
