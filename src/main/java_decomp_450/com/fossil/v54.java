package com.fossil;

import com.fossil.b54;
import com.fossil.c54;
import com.fossil.d54;
import com.fossil.e54;
import com.fossil.f54;
import com.fossil.g54;
import com.fossil.i54;
import com.fossil.j54;
import com.fossil.k54;
import com.fossil.l54;
import com.fossil.m54;
import com.fossil.n54;
import com.fossil.o54;
import com.fossil.p54;
import com.fossil.q54;
import com.fossil.r54;
import com.fossil.s54;
import com.fossil.t54;
import com.fossil.u54;
import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v54 {
    @DexIgnore
    public static /* final */ Charset a; // = Charset.forName("UTF-8");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(int i);

        @DexIgnore
        public abstract a a(c cVar);

        @DexIgnore
        public abstract a a(d dVar);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract v54 a();

        @DexIgnore
        public abstract a b(String str);

        @DexIgnore
        public abstract a c(String str);

        @DexIgnore
        public abstract a d(String str);

        @DexIgnore
        public abstract a e(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract a a(String str);

            @DexIgnore
            public abstract b a();

            @DexIgnore
            public abstract a b(String str);
        }

        @DexIgnore
        public static a c() {
            return new c54.b();
        }

        @DexIgnore
        public abstract String a();

        @DexIgnore
        public abstract String b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {
            @DexIgnore
            public abstract a a(w54<b> w54);

            @DexIgnore
            public abstract a a(String str);

            @DexIgnore
            public abstract c a();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class b {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract a a(String str);

                @DexIgnore
                public abstract a a(byte[] bArr);

                @DexIgnore
                public abstract b a();
            }

            @DexIgnore
            public static a c() {
                return new e54.b();
            }

            @DexIgnore
            public abstract byte[] a();

            @DexIgnore
            public abstract String b();
        }

        @DexIgnore
        public static a c() {
            return new d54.b();
        }

        @DexIgnore
        public abstract w54<b> a();

        @DexIgnore
        public abstract String b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class a {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$a$a")
            /* renamed from: com.fossil.v54$d$a$a  reason: collision with other inner class name */
            public static abstract class AbstractC0205a {
                @DexIgnore
                public abstract AbstractC0205a a(String str);

                @DexIgnore
                public abstract a a();

                @DexIgnore
                public abstract AbstractC0205a b(String str);

                @DexIgnore
                public abstract AbstractC0205a c(String str);

                @DexIgnore
                public abstract AbstractC0205a d(String str);
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class b {
                @DexIgnore
                public abstract String a();
            }

            @DexIgnore
            public static AbstractC0205a f() {
                return new g54.b();
            }

            @DexIgnore
            public abstract String a();

            @DexIgnore
            public abstract String b();

            @DexIgnore
            public abstract String c();

            @DexIgnore
            public abstract b d();

            @DexIgnore
            public abstract String e();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class b {
            @DexIgnore
            public abstract b a(int i);

            @DexIgnore
            public abstract b a(long j);

            @DexIgnore
            public abstract b a(a aVar);

            @DexIgnore
            public abstract b a(c cVar);

            @DexIgnore
            public abstract b a(e eVar);

            @DexIgnore
            public abstract b a(f fVar);

            @DexIgnore
            public abstract b a(w54<AbstractC0206d> w54);

            @DexIgnore
            public abstract b a(Long l);

            @DexIgnore
            public abstract b a(String str);

            @DexIgnore
            public abstract b a(boolean z);

            @DexIgnore
            public b a(byte[] bArr) {
                b(new String(bArr, v54.a));
                return this;
            }

            @DexIgnore
            public abstract d a();

            @DexIgnore
            public abstract b b(String str);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class c {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract a a(int i);

                @DexIgnore
                public abstract a a(long j);

                @DexIgnore
                public abstract a a(String str);

                @DexIgnore
                public abstract a a(boolean z);

                @DexIgnore
                public abstract c a();

                @DexIgnore
                public abstract a b(int i);

                @DexIgnore
                public abstract a b(long j);

                @DexIgnore
                public abstract a b(String str);

                @DexIgnore
                public abstract a c(int i);

                @DexIgnore
                public abstract a c(String str);
            }

            @DexIgnore
            public static a j() {
                return new i54.b();
            }

            @DexIgnore
            public abstract int a();

            @DexIgnore
            public abstract int b();

            @DexIgnore
            public abstract long c();

            @DexIgnore
            public abstract String d();

            @DexIgnore
            public abstract String e();

            @DexIgnore
            public abstract String f();

            @DexIgnore
            public abstract long g();

            @DexIgnore
            public abstract int h();

            @DexIgnore
            public abstract boolean i();
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d")
        /* renamed from: com.fossil.v54$d$d  reason: collision with other inner class name */
        public static abstract class AbstractC0206d {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a")
            /* renamed from: com.fossil.v54$d$d$a */
            public static abstract class a {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$a")
                /* renamed from: com.fossil.v54$d$d$a$a  reason: collision with other inner class name */
                public static abstract class AbstractC0207a {
                    @DexIgnore
                    public abstract AbstractC0207a a(int i);

                    @DexIgnore
                    public abstract AbstractC0207a a(b bVar);

                    @DexIgnore
                    public abstract AbstractC0207a a(w54<b> w54);

                    @DexIgnore
                    public abstract AbstractC0207a a(Boolean bool);

                    @DexIgnore
                    public abstract a a();
                }

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b")
                /* renamed from: com.fossil.v54$d$d$a$b */
                public static abstract class b {

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$a")
                    /* renamed from: com.fossil.v54$d$d$a$b$a  reason: collision with other inner class name */
                    public static abstract class AbstractC0208a {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$a$a")
                        /* renamed from: com.fossil.v54$d$d$a$b$a$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0209a {
                            @DexIgnore
                            public abstract AbstractC0209a a(long j);

                            @DexIgnore
                            public abstract AbstractC0209a a(String str);

                            @DexIgnore
                            public AbstractC0209a a(byte[] bArr) {
                                b(new String(bArr, v54.a));
                                return this;
                            }

                            @DexIgnore
                            public abstract AbstractC0208a a();

                            @DexIgnore
                            public abstract AbstractC0209a b(long j);

                            @DexIgnore
                            public abstract AbstractC0209a b(String str);
                        }

                        @DexIgnore
                        public static AbstractC0209a f() {
                            return new m54.b();
                        }

                        @DexIgnore
                        public abstract long a();

                        @DexIgnore
                        public abstract String b();

                        @DexIgnore
                        public abstract long c();

                        @DexIgnore
                        public abstract String d();

                        @DexIgnore
                        public byte[] e() {
                            String d = d();
                            if (d != null) {
                                return d.getBytes(v54.a);
                            }
                            return null;
                        }
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$b")
                    /* renamed from: com.fossil.v54$d$d$a$b$b  reason: collision with other inner class name */
                    public static abstract class AbstractC0210b {
                        @DexIgnore
                        public abstract AbstractC0210b a(c cVar);

                        @DexIgnore
                        public abstract AbstractC0210b a(AbstractC0212d dVar);

                        @DexIgnore
                        public abstract AbstractC0210b a(w54<AbstractC0208a> w54);

                        @DexIgnore
                        public abstract b a();

                        @DexIgnore
                        public abstract AbstractC0210b b(w54<e> w54);
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$c")
                    /* renamed from: com.fossil.v54$d$d$a$b$c */
                    public static abstract class c {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$c$a")
                        /* renamed from: com.fossil.v54$d$d$a$b$c$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0211a {
                            @DexIgnore
                            public abstract AbstractC0211a a(int i);

                            @DexIgnore
                            public abstract AbstractC0211a a(c cVar);

                            @DexIgnore
                            public abstract AbstractC0211a a(w54<e.AbstractC0215b> w54);

                            @DexIgnore
                            public abstract AbstractC0211a a(String str);

                            @DexIgnore
                            public abstract c a();

                            @DexIgnore
                            public abstract AbstractC0211a b(String str);
                        }

                        @DexIgnore
                        public static AbstractC0211a f() {
                            return new n54.b();
                        }

                        @DexIgnore
                        public abstract c a();

                        @DexIgnore
                        public abstract w54<e.AbstractC0215b> b();

                        @DexIgnore
                        public abstract int c();

                        @DexIgnore
                        public abstract String d();

                        @DexIgnore
                        public abstract String e();
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$d")
                    /* renamed from: com.fossil.v54$d$d$a$b$d  reason: collision with other inner class name */
                    public static abstract class AbstractC0212d {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$d$a")
                        /* renamed from: com.fossil.v54$d$d$a$b$d$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0213a {
                            @DexIgnore
                            public abstract AbstractC0213a a(long j);

                            @DexIgnore
                            public abstract AbstractC0213a a(String str);

                            @DexIgnore
                            public abstract AbstractC0212d a();

                            @DexIgnore
                            public abstract AbstractC0213a b(String str);
                        }

                        @DexIgnore
                        public static AbstractC0213a d() {
                            return new o54.b();
                        }

                        @DexIgnore
                        public abstract long a();

                        @DexIgnore
                        public abstract String b();

                        @DexIgnore
                        public abstract String c();
                    }

                    @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$e")
                    /* renamed from: com.fossil.v54$d$d$a$b$e */
                    public static abstract class e {

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$e$a")
                        /* renamed from: com.fossil.v54$d$d$a$b$e$a  reason: collision with other inner class name */
                        public static abstract class AbstractC0214a {
                            @DexIgnore
                            public abstract AbstractC0214a a(int i);

                            @DexIgnore
                            public abstract AbstractC0214a a(w54<AbstractC0215b> w54);

                            @DexIgnore
                            public abstract AbstractC0214a a(String str);

                            @DexIgnore
                            public abstract e a();
                        }

                        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$e$b")
                        /* renamed from: com.fossil.v54$d$d$a$b$e$b  reason: collision with other inner class name */
                        public static abstract class AbstractC0215b {

                            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$a$b$e$b$a")
                            /* renamed from: com.fossil.v54$d$d$a$b$e$b$a  reason: collision with other inner class name */
                            public static abstract class AbstractC0216a {
                                @DexIgnore
                                public abstract AbstractC0216a a(int i);

                                @DexIgnore
                                public abstract AbstractC0216a a(long j);

                                @DexIgnore
                                public abstract AbstractC0216a a(String str);

                                @DexIgnore
                                public abstract AbstractC0215b a();

                                @DexIgnore
                                public abstract AbstractC0216a b(long j);

                                @DexIgnore
                                public abstract AbstractC0216a b(String str);
                            }

                            @DexIgnore
                            public static AbstractC0216a f() {
                                return new q54.b();
                            }

                            @DexIgnore
                            public abstract String a();

                            @DexIgnore
                            public abstract int b();

                            @DexIgnore
                            public abstract long c();

                            @DexIgnore
                            public abstract long d();

                            @DexIgnore
                            public abstract String e();
                        }

                        @DexIgnore
                        public static AbstractC0214a d() {
                            return new p54.b();
                        }

                        @DexIgnore
                        public abstract w54<AbstractC0215b> a();

                        @DexIgnore
                        public abstract int b();

                        @DexIgnore
                        public abstract String c();
                    }

                    @DexIgnore
                    public static AbstractC0210b e() {
                        return new l54.b();
                    }

                    @DexIgnore
                    public abstract w54<AbstractC0208a> a();

                    @DexIgnore
                    public abstract c b();

                    @DexIgnore
                    public abstract AbstractC0212d c();

                    @DexIgnore
                    public abstract w54<e> d();
                }

                @DexIgnore
                public static AbstractC0207a f() {
                    return new k54.b();
                }

                @DexIgnore
                public abstract Boolean a();

                @DexIgnore
                public abstract w54<b> b();

                @DexIgnore
                public abstract b c();

                @DexIgnore
                public abstract int d();

                @DexIgnore
                public abstract AbstractC0207a e();
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$b")
            /* renamed from: com.fossil.v54$d$d$b */
            public static abstract class b {
                @DexIgnore
                public abstract b a(long j);

                @DexIgnore
                public abstract b a(a aVar);

                @DexIgnore
                public abstract b a(c cVar);

                @DexIgnore
                public abstract b a(AbstractC0217d dVar);

                @DexIgnore
                public abstract b a(String str);

                @DexIgnore
                public abstract AbstractC0206d a();
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$c")
            /* renamed from: com.fossil.v54$d$d$c */
            public static abstract class c {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$c$a")
                /* renamed from: com.fossil.v54$d$d$c$a */
                public static abstract class a {
                    @DexIgnore
                    public abstract a a(int i);

                    @DexIgnore
                    public abstract a a(long j);

                    @DexIgnore
                    public abstract a a(Double d);

                    @DexIgnore
                    public abstract a a(boolean z);

                    @DexIgnore
                    public abstract c a();

                    @DexIgnore
                    public abstract a b(int i);

                    @DexIgnore
                    public abstract a b(long j);
                }

                @DexIgnore
                public static a g() {
                    return new r54.b();
                }

                @DexIgnore
                public abstract Double a();

                @DexIgnore
                public abstract int b();

                @DexIgnore
                public abstract long c();

                @DexIgnore
                public abstract int d();

                @DexIgnore
                public abstract long e();

                @DexIgnore
                public abstract boolean f();
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$d")
            /* renamed from: com.fossil.v54$d$d$d  reason: collision with other inner class name */
            public static abstract class AbstractC0217d {

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.v54$d$d$d$a")
                /* renamed from: com.fossil.v54$d$d$d$a */
                public static abstract class a {
                    @DexIgnore
                    public abstract a a(String str);

                    @DexIgnore
                    public abstract AbstractC0217d a();
                }

                @DexIgnore
                public static a b() {
                    return new s54.b();
                }

                @DexIgnore
                public abstract String a();
            }

            @DexIgnore
            public static b g() {
                return new j54.b();
            }

            @DexIgnore
            public abstract a a();

            @DexIgnore
            public abstract c b();

            @DexIgnore
            public abstract AbstractC0217d c();

            @DexIgnore
            public abstract long d();

            @DexIgnore
            public abstract String e();

            @DexIgnore
            public abstract b f();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class e {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract a a(int i);

                @DexIgnore
                public abstract a a(String str);

                @DexIgnore
                public abstract a a(boolean z);

                @DexIgnore
                public abstract e a();

                @DexIgnore
                public abstract a b(String str);
            }

            @DexIgnore
            public static a e() {
                return new t54.b();
            }

            @DexIgnore
            public abstract String a();

            @DexIgnore
            public abstract int b();

            @DexIgnore
            public abstract String c();

            @DexIgnore
            public abstract boolean d();
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static abstract class f {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static abstract class a {
                @DexIgnore
                public abstract a a(String str);

                @DexIgnore
                public abstract f a();
            }

            @DexIgnore
            public static a b() {
                return new u54.b();
            }

            @DexIgnore
            public abstract String a();
        }

        @DexIgnore
        public static b n() {
            f54.b bVar = new f54.b();
            bVar.a(false);
            return bVar;
        }

        @DexIgnore
        public abstract a a();

        @DexIgnore
        public d a(w54<AbstractC0206d> w54) {
            b m = m();
            m.a(w54);
            return m.a();
        }

        @DexIgnore
        public abstract c b();

        @DexIgnore
        public abstract Long c();

        @DexIgnore
        public abstract w54<AbstractC0206d> d();

        @DexIgnore
        public abstract String e();

        @DexIgnore
        public abstract int f();

        @DexIgnore
        public abstract String g();

        @DexIgnore
        public byte[] h() {
            return g().getBytes(v54.a);
        }

        @DexIgnore
        public abstract e i();

        @DexIgnore
        public abstract long j();

        @DexIgnore
        public abstract f k();

        @DexIgnore
        public abstract boolean l();

        @DexIgnore
        public abstract b m();

        @DexIgnore
        public d a(long j, boolean z, String str) {
            b m = m();
            m.a(Long.valueOf(j));
            m.a(z);
            if (str != null) {
                f.a b2 = f.b();
                b2.a(str);
                m.a(b2.a());
                m.a();
            }
            return m.a();
        }
    }

    @DexIgnore
    public enum e {
        INCOMPLETE,
        JAVA,
        NATIVE
    }

    @DexIgnore
    public static a l() {
        return new b54.b();
    }

    @DexIgnore
    public v54 a(w54<d.AbstractC0206d> w54) {
        if (h() != null) {
            a j = j();
            j.a(h().a(w54));
            return j.a();
        }
        throw new IllegalStateException("Reports without sessions cannot have events added to them.");
    }

    @DexIgnore
    public abstract String a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract String c();

    @DexIgnore
    public abstract String d();

    @DexIgnore
    public abstract c e();

    @DexIgnore
    public abstract int f();

    @DexIgnore
    public abstract String g();

    @DexIgnore
    public abstract d h();

    @DexIgnore
    public e i() {
        if (h() != null) {
            return e.JAVA;
        }
        if (e() != null) {
            return e.NATIVE;
        }
        return e.INCOMPLETE;
    }

    @DexIgnore
    public abstract a j();

    @DexIgnore
    public v54 a(c cVar) {
        a j = j();
        j.a((d) null);
        j.a(cVar);
        return j.a();
    }

    @DexIgnore
    public v54 a(long j, boolean z, String str) {
        a j2 = j();
        if (h() != null) {
            j2.a(h().a(j, z, str));
        }
        return j2.a();
    }
}
