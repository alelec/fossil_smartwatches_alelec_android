package com.fossil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.OperationCanceledException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u6 {
    @DexIgnore
    public static Cursor a(ContentResolver contentResolver, Uri uri, String[] strArr, String str, String[] strArr2, String str2, d8 d8Var) {
        Object obj;
        if (Build.VERSION.SDK_INT >= 16) {
            if (d8Var != null) {
                try {
                    obj = d8Var.b();
                } catch (Exception e) {
                    if (e instanceof OperationCanceledException) {
                        throw new k8();
                    }
                    throw e;
                }
            } else {
                obj = null;
            }
            return contentResolver.query(uri, strArr, str, strArr2, str2, (CancellationSignal) obj);
        }
        if (d8Var != null) {
            d8Var.d();
        }
        return contentResolver.query(uri, strArr, str, strArr2, str2);
    }
}
