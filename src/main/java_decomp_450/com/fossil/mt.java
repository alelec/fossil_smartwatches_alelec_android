package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt implements st {
    @DexIgnore
    public /* final */ Context b;

    @DexIgnore
    public mt(Context context) {
        ee7.b(context, "context");
        this.b = context;
    }

    @DexIgnore
    @Override // com.fossil.st
    public Object a(fb7<? super rt> fb7) {
        Resources resources = this.b.getResources();
        ee7.a((Object) resources, "context.resources");
        DisplayMetrics displayMetrics = resources.getDisplayMetrics();
        return new ot(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }
}
