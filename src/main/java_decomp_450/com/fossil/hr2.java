package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr2 {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore
    public hr2(gr2 gr2) {
        or2.a(gr2, "BuildInfo must be non-null");
        this.a = !gr2.zza();
    }

    @DexIgnore
    public final boolean a(String str) {
        or2.a(str, "flagName must not be null");
        if (!this.a) {
            return true;
        }
        return jr2.a.zza().zza(str);
    }
}
