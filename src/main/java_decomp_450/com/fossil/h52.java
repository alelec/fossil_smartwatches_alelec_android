package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h52 implements b42 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ c32 b;
    @DexIgnore
    public /* final */ Looper c;
    @DexIgnore
    public /* final */ l32 d;
    @DexIgnore
    public /* final */ l32 e;
    @DexIgnore
    public /* final */ Map<v02.c<?>, l32> f;
    @DexIgnore
    public /* final */ Set<c22> g; // = Collections.newSetFromMap(new WeakHashMap());
    @DexIgnore
    public /* final */ v02.f h;
    @DexIgnore
    public Bundle i;
    @DexIgnore
    public i02 j; // = null;
    @DexIgnore
    public i02 p; // = null;
    @DexIgnore
    public boolean q; // = false;
    @DexIgnore
    public /* final */ Lock r;
    @DexIgnore
    public int s; // = 0;

    @DexIgnore
    public h52(Context context, c32 c32, Lock lock, Looper looper, m02 m02, Map<v02.c<?>, v02.f> map, Map<v02.c<?>, v02.f> map2, j62 j62, v02.a<? extends xn3, fn3> aVar, v02.f fVar, ArrayList<f52> arrayList, ArrayList<f52> arrayList2, Map<v02<?>, Boolean> map3, Map<v02<?>, Boolean> map4) {
        this.a = context;
        this.b = c32;
        this.r = lock;
        this.c = looper;
        this.h = fVar;
        this.d = new l32(context, c32, lock, looper, m02, map2, null, map4, null, arrayList2, new j52(this, null));
        this.e = new l32(context, this.b, lock, looper, m02, map, j62, map3, aVar, arrayList, new i52(this, null));
        n4 n4Var = new n4();
        for (v02.c<?> cVar : map2.keySet()) {
            n4Var.put(cVar, this.d);
        }
        for (v02.c<?> cVar2 : map.keySet()) {
            n4Var.put(cVar2, this.e);
        }
        this.f = Collections.unmodifiableMap(n4Var);
    }

    @DexIgnore
    public static h52 a(Context context, c32 c32, Lock lock, Looper looper, m02 m02, Map<v02.c<?>, v02.f> map, j62 j62, Map<v02<?>, Boolean> map2, v02.a<? extends xn3, fn3> aVar, ArrayList<f52> arrayList) {
        n4 n4Var = new n4();
        n4 n4Var2 = new n4();
        v02.f fVar = null;
        for (Map.Entry<v02.c<?>, v02.f> entry : map.entrySet()) {
            v02.f value = entry.getValue();
            if (value.d()) {
                fVar = value;
            }
            if (value.n()) {
                n4Var.put(entry.getKey(), value);
            } else {
                n4Var2.put(entry.getKey(), value);
            }
        }
        a72.b(!n4Var.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        n4 n4Var3 = new n4();
        n4 n4Var4 = new n4();
        for (v02<?> v02 : map2.keySet()) {
            v02.c<?> a2 = v02.a();
            if (n4Var.containsKey(a2)) {
                n4Var3.put(v02, map2.get(v02));
            } else if (n4Var2.containsKey(a2)) {
                n4Var4.put(v02, map2.get(v02));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            f52 f52 = arrayList.get(i2);
            i2++;
            f52 f522 = f52;
            if (n4Var3.containsKey(f522.a)) {
                arrayList2.add(f522);
            } else if (n4Var4.containsKey(f522.a)) {
                arrayList3.add(f522);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new h52(context, c32, lock, looper, m02, n4Var, n4Var2, j62, aVar, fVar, arrayList2, arrayList3, n4Var3, n4Var4);
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, R extends i12, T extends r12<R, A>> T b(T t) {
        if (!c(t)) {
            return (T) this.d.b(t);
        }
        if (!j()) {
            return (T) this.e.b(t);
        }
        t.c(new Status(4, null, k()));
        return t;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final boolean c() {
        this.r.lock();
        try {
            boolean z = true;
            if (!this.d.c() || (!this.e.c() && !j() && this.s != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.r.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void d() {
        this.r.lock();
        try {
            boolean g2 = g();
            this.e.a();
            this.p = new i02(4);
            if (g2) {
                new bg2(this.c).post(new g52(this));
            } else {
                i();
            }
        } finally {
            this.r.unlock();
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void e() {
        this.d.e();
        this.e.e();
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final i02 f() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final boolean g() {
        this.r.lock();
        try {
            return this.s == 2;
        } finally {
            this.r.unlock();
        }
    }

    @DexIgnore
    public final void h() {
        i02 i02;
        if (b(this.j)) {
            if (b(this.p) || j()) {
                int i2 = this.s;
                if (i2 != 1) {
                    if (i2 != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.s = 0;
                        return;
                    }
                    this.b.a(this.i);
                }
                i();
                this.s = 0;
                return;
            }
            i02 i022 = this.p;
            if (i022 == null) {
                return;
            }
            if (this.s == 1) {
                i();
                return;
            }
            a(i022);
            this.d.a();
        } else if (this.j == null || !b(this.p)) {
            i02 i023 = this.j;
            if (i023 != null && (i02 = this.p) != null) {
                if (this.e.r < this.d.r) {
                    i023 = i02;
                }
                a(i023);
            }
        } else {
            this.e.a();
            a(this.j);
        }
    }

    @DexIgnore
    public final void i() {
        for (c22 c22 : this.g) {
            c22.onComplete();
        }
        this.g.clear();
    }

    @DexIgnore
    public final boolean j() {
        i02 i02 = this.p;
        return i02 != null && i02.e() == 4;
    }

    @DexIgnore
    public final PendingIntent k() {
        if (this.h == null) {
            return null;
        }
        return PendingIntent.getActivity(this.a, System.identityHashCode(this.b), this.h.m(), 134217728);
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void b() {
        this.s = 2;
        this.q = false;
        this.p = null;
        this.j = null;
        this.d.b();
        this.e.b();
    }

    @DexIgnore
    public final boolean c(r12<? extends i12, ? extends v02.b> r12) {
        v02.c<? extends v02.b> i2 = r12.i();
        a72.a(this.f.containsKey(i2), "GoogleApiClient is not configured to use the API required for this call.");
        return this.f.get(i2).equals(this.e);
    }

    @DexIgnore
    public static boolean b(i02 i02) {
        return i02 != null && i02.x();
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final <A extends v02.b, T extends r12<? extends i12, A>> T a(T t) {
        if (!c(t)) {
            return (T) this.d.a(t);
        }
        if (!j()) {
            return (T) this.e.a(t);
        }
        t.c(new Status(4, null, k()));
        return t;
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a() {
        this.p = null;
        this.j = null;
        this.s = 0;
        this.d.a();
        this.e.a();
        i();
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final boolean a(c22 c22) {
        this.r.lock();
        try {
            if ((g() || c()) && !this.e.c()) {
                this.g.add(c22);
                if (this.s == 0) {
                    this.s = 1;
                }
                this.p = null;
                this.e.b();
                return true;
            }
            this.r.unlock();
            return false;
        } finally {
            this.r.unlock();
        }
    }

    @DexIgnore
    public final void a(i02 i02) {
        int i2 = this.s;
        if (i2 != 1) {
            if (i2 != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.s = 0;
            }
            this.b.a(i02);
        }
        i();
        this.s = 0;
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        this.b.a(i2, z);
        this.p = null;
        this.j = null;
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        Bundle bundle2 = this.i;
        if (bundle2 == null) {
            this.i = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    @DexIgnore
    @Override // com.fossil.b42
    public final void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append("authClient").println(":");
        this.e.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append("anonClient").println(":");
        this.d.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }
}
