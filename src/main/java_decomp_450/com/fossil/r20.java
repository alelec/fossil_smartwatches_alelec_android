package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.nw;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r20 implements cx<ByteBuffer, t20> {
    @DexIgnore
    public static /* final */ a f; // = new a();
    @DexIgnore
    public static /* final */ b g; // = new b();
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ List<ImageHeaderParser> b;
    @DexIgnore
    public /* final */ b c;
    @DexIgnore
    public /* final */ a d;
    @DexIgnore
    public /* final */ s20 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public nw a(nw.a aVar, pw pwVar, ByteBuffer byteBuffer, int i) {
            return new rw(aVar, pwVar, byteBuffer, i);
        }
    }

    @DexIgnore
    public r20(Context context, List<ImageHeaderParser> list, dz dzVar, az azVar) {
        this(context, list, dzVar, azVar, g, f);
    }

    @DexIgnore
    public r20(Context context, List<ImageHeaderParser> list, dz dzVar, az azVar, b bVar, a aVar) {
        this.a = context.getApplicationContext();
        this.b = list;
        this.d = aVar;
        this.e = new s20(dzVar, azVar);
        this.c = bVar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ Queue<qw> a; // = v50.a(0);

        @DexIgnore
        public synchronized qw a(ByteBuffer byteBuffer) {
            qw poll;
            poll = this.a.poll();
            if (poll == null) {
                poll = new qw();
            }
            poll.a(byteBuffer);
            return poll;
        }

        @DexIgnore
        public synchronized void a(qw qwVar) {
            qwVar.a();
            this.a.offer(qwVar);
        }
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, ax axVar) throws IOException {
        return !((Boolean) axVar.a(z20.b)).booleanValue() && xw.a(this.b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    @DexIgnore
    public v20 a(ByteBuffer byteBuffer, int i, int i2, ax axVar) {
        qw a2 = this.c.a(byteBuffer);
        try {
            return a(byteBuffer, i, i2, a2, axVar);
        } finally {
            this.c.a(a2);
        }
    }

    @DexIgnore
    public final v20 a(ByteBuffer byteBuffer, int i, int i2, qw qwVar, ax axVar) {
        Bitmap.Config config;
        long a2 = q50.a();
        try {
            pw c2 = qwVar.c();
            if (c2.b() > 0) {
                if (c2.c() == 0) {
                    if (axVar.a(z20.a) == tw.PREFER_RGB_565) {
                        config = Bitmap.Config.RGB_565;
                    } else {
                        config = Bitmap.Config.ARGB_8888;
                    }
                    nw a3 = this.d.a(this.e, c2, byteBuffer, a(c2, i, i2));
                    a3.a(config);
                    a3.b();
                    Bitmap a4 = a3.a();
                    if (a4 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + q50.a(a2));
                        }
                        return null;
                    }
                    v20 v20 = new v20(new t20(this.a, a3, f10.a(), i, i2, a4));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + q50.a(a2));
                    }
                    return v20;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + q50.a(a2));
            }
        }
    }

    @DexIgnore
    public static int a(pw pwVar, int i, int i2) {
        int i3;
        int min = Math.min(pwVar.a() / i2, pwVar.d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + pwVar.d() + "x" + pwVar.a() + "]");
        }
        return max;
    }
}
