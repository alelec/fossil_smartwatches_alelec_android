package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv0 implements Parcelable.Creator<bx0> {
    @DexIgnore
    public /* synthetic */ jv0(zd7 zd7) {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0059 A[LOOP:1: B:11:0x0048->B:13:0x0059, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005b A[EDGE_INSN: B:30:0x005b->B:14:0x005b ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.bx0 a(byte r12, byte[] r13) throws java.lang.IllegalArgumentException {
        /*
            r11 = this;
            java.nio.ByteBuffer r0 = java.nio.ByteBuffer.wrap(r13)
            java.nio.ByteOrder r1 = java.nio.ByteOrder.LITTLE_ENDIAN
            java.nio.ByteBuffer r0 = r0.order(r1)
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            r3 = 0
        L_0x0011:
            int r4 = r13.length
            if (r3 >= r4) goto L_0x00a3
            byte r4 = r0.get(r3)
            short r4 = com.fossil.yz0.b(r4)
            int r5 = r3 + 1
            int r6 = r5 + r4
            int r7 = r13.length
            if (r6 > r7) goto L_0x0074
            byte[] r5 = com.fossil.s97.a(r13, r5, r6)
            int r6 = r5.length
            r7 = 3
            int r6 = r6 % r7
            if (r6 != 0) goto L_0x005f
            int r6 = r5.length
            com.fossil.lf7 r6 = com.fossil.qf7.d(r2, r6)
            com.fossil.jf7 r6 = com.fossil.qf7.a(r6, r7)
            int r7 = r6.getFirst()
            int r8 = r6.getLast()
            int r6 = r6.a()
            if (r6 < 0) goto L_0x0046
            if (r7 > r8) goto L_0x005b
            goto L_0x0048
        L_0x0046:
            if (r7 < r8) goto L_0x005b
        L_0x0048:
            com.fossil.ut0 r9 = com.fossil.ov0.CREATOR
            int r10 = r7 + 3
            byte[] r10 = com.fossil.s97.a(r5, r7, r10)
            com.fossil.ov0 r9 = r9.a(r10)
            r1.add(r9)
            if (r7 == r8) goto L_0x005b
            int r7 = r7 + r6
            goto L_0x0048
        L_0x005b:
            int r4 = r4 + 1
            int r3 = r3 + r4
            goto L_0x0011
        L_0x005f:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.String r13 = "Chain size ("
            java.lang.StringBuilder r13 = com.fossil.yh0.b(r13)
            int r0 = r5.length
            java.lang.String r1 = ") is not "
            java.lang.String r2 = "divide to 3."
            java.lang.String r13 = com.fossil.yh0.a(r13, r0, r1, r2)
            r12.<init>(r13)
            throw r12
        L_0x0074:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid Chain length "
            r0.append(r1)
            r1 = 40
            r0.append(r1)
            r0.append(r6)
            java.lang.String r1 = "), "
            r0.append(r1)
            java.lang.String r1 = "remain "
            r0.append(r1)
            int r13 = r13.length
            r0.append(r13)
            r13 = 46
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            r12.<init>(r13)
            throw r12
        L_0x00a3:
            com.fossil.ov0[] r0 = new com.fossil.ov0[r2]
            java.lang.Object[] r0 = r1.toArray(r0)
            if (r0 == 0) goto L_0x00b3
            com.fossil.ov0[] r0 = (com.fossil.ov0[]) r0
            com.fossil.bx0 r1 = new com.fossil.bx0
            r1.<init>(r12, r0, r13)
            return r1
        L_0x00b3:
            com.fossil.x87 r12 = new com.fossil.x87
            java.lang.String r13 = "null cannot be cast to non-null type kotlin.Array<T>"
            r12.<init>(r13)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jv0.a(byte, byte[]):com.fossil.bx0");
    }

    @DexIgnore
    @Override // android.os.Parcelable.Creator
    public bx0 createFromParcel(Parcel parcel) {
        return new bx0(parcel, null);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public bx0[] newArray(int i) {
        return new bx0[i];
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public bx0 m31createFromParcel(Parcel parcel) {
        return new bx0(parcel, null);
    }
}
