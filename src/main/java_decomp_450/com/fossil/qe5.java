package com.fossil;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.widget.RemoteViews;
import com.fossil.o6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.util.BCNotificationActionReceiver;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qe5 {
    @DexIgnore
    public static /* final */ NotificationManager a;
    @DexIgnore
    public static NotificationChannel b;
    @DexIgnore
    public static /* final */ qe5 c; // = new qe5();

    /*
    static {
        Object systemService = PortfolioApp.g0.c().getSystemService("notification");
        if (systemService != null) {
            a = (NotificationManager) systemService;
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }
    */

    @DexIgnore
    public final void a(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends o6.a> list) {
        ee7.b(context, "context");
        ee7.b(str, "title");
        ee7.b(str2, "text");
        o6.c cVar = new o6.c();
        cVar.a(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        if (Build.VERSION.SDK_INT >= 26) {
            c();
        }
        o6.e eVar = new o6.e(context);
        eVar.b("FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.b((CharSequence) str);
        eVar.a((CharSequence) str2);
        eVar.f(R.drawable.ic_launcher_transparent);
        eVar.a(cVar);
        eVar.e(2);
        eVar.a(new long[]{1000, 1000, 1000});
        eVar.a(defaultUri);
        eVar.a(pendingIntent);
        eVar.a(true);
        Notification a2 = eVar.a();
        a2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.g0.c().getResources();
            Package r9 = android.R.class.getPackage();
            if (r9 != null) {
                ee7.a((Object) r9, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r9.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = a2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = a2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = a2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        if (list != null) {
            for (o6.a aVar : list) {
                eVar.a(aVar);
            }
        }
        a.notify(i, a2);
    }

    @DexIgnore
    public final void b(tn4 tn4, Context context) {
        int i;
        Notification notification;
        String a2;
        ee7.b(tn4, "notification");
        ee7.b(context, "context");
        pn4 b2 = tn4.b();
        if (b2 == null || (a2 = b2.a()) == null) {
            io4 d = tn4.d();
            String c2 = d != null ? d.c() : null;
            i = c2 != null ? c2.hashCode() : 0;
        } else {
            i = a2.hashCode();
        }
        if (Build.VERSION.SDK_INT >= 26) {
            c();
        }
        String e = tn4.e();
        int hashCode = e.hashCode();
        if (hashCode != 1433363166) {
            if (hashCode == 1898363949 && e.equals("Title_Send_Invitation_Challenge")) {
                notification = a(tn4, context);
            }
            throw new Exception("Wrong type");
        }
        if (e.equals("Title_Send_Friend_Request")) {
            notification = a(tn4);
        }
        throw new Exception("Wrong type");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("Helper", "notificationID: " + i);
        a.notify(i, notification);
    }

    @DexIgnore
    public final void c() {
        if (b == null) {
            NotificationChannel notificationChannel = new NotificationChannel("FOSSIL_NOTIFICATION_CHANNEL_ID", "Fossil Smartwatches Notification", 2);
            b = notificationChannel;
            if (notificationChannel != null) {
                notificationChannel.enableLights(true);
                NotificationChannel notificationChannel2 = b;
                if (notificationChannel2 != null) {
                    notificationChannel2.enableVibration(true);
                    NotificationChannel notificationChannel3 = b;
                    if (notificationChannel3 != null) {
                        notificationChannel3.setImportance(4);
                        NotificationChannel notificationChannel4 = b;
                        if (notificationChannel4 != null) {
                            notificationChannel4.setShowBadge(true);
                            NotificationManager notificationManager = a;
                            NotificationChannel notificationChannel5 = b;
                            if (notificationChannel5 != null) {
                                notificationManager.createNotificationChannel(notificationChannel5);
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void d() {
        Intent intent = new Intent(PortfolioApp.g0.c(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.set_sync_data");
        PendingIntent broadcast = PendingIntent.getBroadcast(PortfolioApp.g0.c(), 9, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        o6.e eVar = new o6.e(PortfolioApp.g0.c(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.b((CharSequence) ig5.a(PortfolioApp.g0.c(), 2131887188));
        eVar.f(R.drawable.ic_launcher_transparent);
        eVar.a(0, ig5.a(PortfolioApp.g0.c(), 2131886130), broadcast);
        eVar.a(0);
        o6.c cVar = new o6.c();
        cVar.a(ig5.a(PortfolioApp.g0.c(), 2131887184));
        eVar.a(cVar);
        eVar.c(false);
        eVar.e(2);
        Notification a2 = eVar.a();
        Object systemService = PortfolioApp.g0.c().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(999, a2);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void b(Context context, int i, String str, String str2, PendingIntent pendingIntent, List<? extends o6.a> list) {
        ee7.b(context, "context");
        ee7.b(str, "title");
        ee7.b(str2, "text");
        ee7.b(pendingIntent, "pendingIntent");
        o6.c cVar = new o6.c();
        cVar.a(str2);
        Uri defaultUri = RingtoneManager.getDefaultUri(2);
        Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), 2131689472);
        o6.e eVar = new o6.e(context);
        eVar.b((CharSequence) str);
        eVar.a((CharSequence) str2);
        eVar.f(2131689472);
        eVar.b(decodeResource);
        eVar.a(cVar);
        eVar.a(new long[]{1000, 1000, 1000});
        eVar.a(defaultUri);
        eVar.a(pendingIntent);
        Notification a2 = eVar.a();
        a2.flags |= 16;
        if (Build.VERSION.SDK_INT >= 21) {
            Resources resources = PortfolioApp.g0.c().getResources();
            Package r10 = android.R.class.getPackage();
            if (r10 != null) {
                ee7.a((Object) r10, "android.R::class.java.`package`!!");
                int identifier = resources.getIdentifier("right_icon", "id", r10.getName());
                if (identifier != 0) {
                    RemoteViews remoteViews = a2.contentView;
                    if (remoteViews != null) {
                        remoteViews.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews2 = a2.headsUpContentView;
                    if (remoteViews2 != null) {
                        remoteViews2.setViewVisibility(identifier, 4);
                    }
                    RemoteViews remoteViews3 = a2.bigContentView;
                    if (remoteViews3 != null) {
                        remoteViews3.setViewVisibility(identifier, 4);
                    }
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        if (list != null) {
            for (o6.a aVar : list) {
                eVar.a(aVar);
            }
        }
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).notify(i, a2);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00db  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f8  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00fd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.app.Notification a(com.fossil.tn4 r12, android.content.Context r13) {
        /*
            r11 = this;
            java.lang.String r0 = r12.a()
            com.fossil.fu4 r1 = com.fossil.fu4.a
            com.fossil.io4 r2 = r12.d()
            r3 = 0
            if (r2 == 0) goto L_0x0012
            java.lang.String r2 = r2.b()
            goto L_0x0013
        L_0x0012:
            r2 = r3
        L_0x0013:
            com.fossil.io4 r4 = r12.d()
            if (r4 == 0) goto L_0x001e
            java.lang.String r4 = r4.d()
            goto L_0x001f
        L_0x001e:
            r4 = r3
        L_0x001f:
            com.fossil.io4 r5 = r12.d()
            java.lang.String r6 = "-"
            if (r5 == 0) goto L_0x002e
            java.lang.String r5 = r5.e()
            if (r5 == 0) goto L_0x002e
            goto L_0x002f
        L_0x002e:
            r5 = r6
        L_0x002f:
            java.lang.String r1 = r1.a(r2, r4, r5)
            com.fossil.pn4 r2 = r12.b()
            if (r2 == 0) goto L_0x0040
            java.lang.String r2 = r2.b()
            if (r2 == 0) goto L_0x0040
            r6 = r2
        L_0x0040:
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            r4 = 2131887175(0x7f120447, float:1.940895E38)
            java.lang.String r2 = com.fossil.ig5.a(r2, r4)
            int r4 = r0.hashCode()
            r5 = -1500498103(0xffffffffa6903749, float:-1.0006992E-15)
            java.lang.String r7 = "java.lang.String.format(format, *args)"
            r8 = 1
            r9 = 2
            r10 = 0
            if (r4 == r5) goto L_0x009e
            r5 = -1500497618(0xffffffffa690392e, float:-1.0007506E-15)
            if (r4 == r5) goto L_0x0061
            goto L_0x00cc
        L_0x0061:
            java.lang.String r4 = "Body_Send_Invitation_Challenge_ARG"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x00cc
            com.fossil.we7 r0 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r4 = 2131886197(0x7f120075, float:1.9406966E38)
            java.lang.String r0 = com.fossil.ig5.a(r0, r4)
            java.lang.String r4 = "LanguageHelper.getString\u2026Invitation_Challenge_ARG)"
            com.fossil.ee7.a(r0, r4)
            r4 = 3
            java.lang.Object[] r5 = new java.lang.Object[r4]
            r5[r10] = r1
            r5[r8] = r6
            com.fossil.pn4 r1 = r12.b()
            if (r1 == 0) goto L_0x008f
            java.lang.Integer r1 = r1.c()
            goto L_0x0090
        L_0x008f:
            r1 = r3
        L_0x0090:
            r5[r9] = r1
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r5, r4)
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.fossil.ee7.a(r0, r7)
            goto L_0x00ce
        L_0x009e:
            java.lang.String r4 = "Body_Send_Invitation_Challenge_ABR"
            boolean r0 = r0.equals(r4)
            if (r0 == 0) goto L_0x00cc
            com.fossil.we7 r0 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            r4 = 2131886196(0x7f120074, float:1.9406964E38)
            java.lang.String r0 = com.fossil.ig5.a(r0, r4)
            java.lang.String r4 = "LanguageHelper.getString\u2026Invitation_Challenge_ABR)"
            com.fossil.ee7.a(r0, r4)
            java.lang.Object[] r4 = new java.lang.Object[r9]
            r4[r10] = r1
            r4[r8] = r6
            java.lang.Object[] r1 = java.util.Arrays.copyOf(r4, r9)
            java.lang.String r0 = java.lang.String.format(r0, r1)
            com.fossil.ee7.a(r0, r7)
            goto L_0x00ce
        L_0x00cc:
            java.lang.String r0 = ""
        L_0x00ce:
            android.content.Intent r1 = new android.content.Intent
            java.lang.Class<com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity> r4 = com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity.class
            r1.<init>(r13, r4)
            com.fossil.pn4 r4 = r12.b()
            if (r4 == 0) goto L_0x00df
            java.lang.String r3 = r4.a()
        L_0x00df:
            java.lang.String r4 = "challenge_id_extra"
            r1.putExtra(r4, r3)
            java.lang.String r3 = "category_extra"
            java.lang.String r4 = "pending-invitation"
            r1.putExtra(r3, r4)
            android.app.TaskStackBuilder r13 = android.app.TaskStackBuilder.create(r13)
            r13.addNextIntentWithParentStack(r1)
            java.lang.String r12 = r12.c()
            if (r12 == 0) goto L_0x00fd
            int r12 = r12.hashCode()
            goto L_0x00fe
        L_0x00fd:
            r12 = 0
        L_0x00fe:
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r12 = r13.getPendingIntent(r12, r1)
            com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r13 = r13.c()
            r1 = 2131886327(0x7f1200f7, float:1.940723E38)
            java.lang.String r13 = r13.getString(r1)
            java.lang.String r1 = "PortfolioApp.instance.ge\u2026ication_CTA__ViewDetails)"
            com.fossil.ee7.a(r13, r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            android.content.res.Resources r1 = r1.getResources()
            r3 = 2131231161(0x7f0801b9, float:1.8078395E38)
            android.graphics.BitmapFactory.decodeResource(r1, r3)
            com.fossil.o6$e r1 = new com.fossil.o6$e
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            java.lang.String r4 = "FOSSIL_NOTIFICATION_CHANNEL_ID"
            r1.<init>(r3, r4)
            r1.b(r2)
            r2 = 2131231090(0x7f080172, float:1.8078251E38)
            r1.f(r2)
            r1.a(r10, r13, r12)
            r1.a(r10)
            com.fossil.o6$c r12 = new com.fossil.o6$c
            r12.<init>()
            r12.a(r0)
            r1.a(r12)
            r1.c(r10)
            r1.e(r9)
            android.app.Notification r12 = r1.a()
            java.lang.String r13 = "NotificationCompat.Build\u2026\n                .build()"
            com.fossil.ee7.a(r12, r13)
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qe5.a(com.fossil.tn4, android.content.Context):android.app.Notification");
    }

    @DexIgnore
    public final void b() {
        a(PortfolioApp.g0.c(), 999);
    }

    @DexIgnore
    public final Notification a(tn4 tn4) {
        String str;
        un4 un4;
        String str2;
        fu4 fu4 = fu4.a;
        io4 d = tn4.d();
        String b2 = d != null ? d.b() : null;
        io4 d2 = tn4.d();
        String d3 = d2 != null ? d2.d() : null;
        io4 d4 = tn4.d();
        if (d4 == null || (str = d4.e()) == null) {
            str = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
        }
        String a2 = fu4.a(b2, d3, str);
        String a3 = ig5.a(PortfolioApp.g0.c(), 2131887174);
        we7 we7 = we7.a;
        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886195);
        ee7.a((Object) a4, "LanguageHelper.getString\u2026Body_Send_Friend_Request)");
        String format = String.format(a4, Arrays.copyOf(new Object[]{a2}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        io4 d5 = tn4.d();
        if (d5 != null) {
            String c2 = d5.c();
            String e = d5.e();
            String str3 = e != null ? e : "";
            String b3 = d5.b();
            String str4 = b3 != null ? b3 : "";
            String d6 = d5.d();
            String str5 = d6 != null ? d6 : "";
            String a5 = d5.a();
            if (a5 != null) {
                str2 = a5;
            } else {
                str2 = "";
            }
            un4 = new un4(c2, str3, str4, str5, null, str2, false, 0, 2);
        } else {
            un4 = null;
        }
        Intent intent = new Intent(PortfolioApp.g0.c(), BCNotificationActionReceiver.class);
        intent.setAction("com.buddy_challenge.friend.accept");
        intent.putExtra("friend_extra", un4);
        PortfolioApp c3 = PortfolioApp.g0.c();
        String c4 = tn4.c();
        PendingIntent broadcast = PendingIntent.getBroadcast(c3, c4 != null ? c4.hashCode() : 0, intent, SQLiteDatabase.CREATE_IF_NECESSARY);
        Intent intent2 = new Intent(PortfolioApp.g0.c(), BCNotificationActionReceiver.class);
        intent2.setAction("com.buddy_challenge.friend.decline");
        intent2.putExtra("friend_extra", un4);
        PortfolioApp c5 = PortfolioApp.g0.c();
        String c6 = tn4.c();
        PendingIntent broadcast2 = PendingIntent.getBroadcast(c5, c6 != null ? c6.hashCode() : 0, intent2, SQLiteDatabase.CREATE_IF_NECESSARY);
        String string = PortfolioApp.g0.c().getString(2131886290);
        ee7.a((Object) string, "PortfolioApp.instance.ge\u2026Friends_List_CTA__Accept)");
        String string2 = PortfolioApp.g0.c().getString(2131886291);
        ee7.a((Object) string2, "PortfolioApp.instance.ge\u2026riends_List_CTA__Decline)");
        BitmapFactory.decodeResource(PortfolioApp.g0.c().getResources(), 2131230907);
        o6.e eVar = new o6.e(PortfolioApp.g0.c(), "FOSSIL_NOTIFICATION_CHANNEL_ID");
        eVar.b((CharSequence) a3);
        eVar.a((CharSequence) format);
        eVar.f(R.drawable.ic_launcher_transparent);
        eVar.a(0, string, broadcast);
        eVar.a(0, string2, broadcast2);
        eVar.a(0);
        eVar.a((o6.f) null);
        eVar.c(false);
        eVar.e(2);
        Notification a6 = eVar.a();
        ee7.a((Object) a6, "NotificationCompat.Build\u2026\n                .build()");
        return a6;
    }

    @DexIgnore
    public final void a(Context context, int i) {
        ee7.b(context, "context");
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancel(i);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public final void a() {
        Object systemService = PortfolioApp.g0.c().getSystemService("notification");
        if (systemService != null) {
            ((NotificationManager) systemService).cancelAll();
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.NotificationManager");
    }
}
