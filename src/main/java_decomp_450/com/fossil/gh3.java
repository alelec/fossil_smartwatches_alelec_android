package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh3 {
    @DexIgnore
    public /* final */ fh3 a;

    @DexIgnore
    public gh3(fh3 fh3) {
        a72.a(fh3);
        this.a = fh3;
    }

    @DexIgnore
    public static boolean a(Context context) {
        ActivityInfo receiverInfo;
        a72.a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @DexIgnore
    public final void a(Context context, Intent intent) {
        oh3 a2 = oh3.a(context, null, null);
        jg3 e = a2.e();
        if (intent == null) {
            e.w().a("Receiver called with null intent");
            return;
        }
        a2.b();
        String action = intent.getAction();
        e.B().a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            e.B().a("Starting wakeful intent.");
            this.a.a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            e.w().a("Install Referrer Broadcasts are deprecated");
        }
    }
}
