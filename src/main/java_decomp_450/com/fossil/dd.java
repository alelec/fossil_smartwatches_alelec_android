package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dd<T> extends xd<T> {
    @DexIgnore
    public zc<T> l;
    @DexIgnore
    public id m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ dd this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(dd ddVar) {
            super(0);
            this.this$0 = ddVar;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            this.this$0.l = (zc) null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "androidx.lifecycle.CoroutineLiveData", f = "CoroutineLiveData.kt", l = {234}, m = "clearSource$lifecycle_livedata_ktx_release")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dd this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(dd ddVar, fb7 fb7) {
            super(fb7);
            this.this$0 = ddVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((fb7<? super i97>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "androidx.lifecycle.CoroutineLiveData", f = "CoroutineLiveData.kt", l = {227, 228}, m = "emitSource$lifecycle_livedata_ktx_release")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ dd this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(dd ddVar, fb7 fb7) {
            super(fb7);
            this.this$0 = ddVar;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((LiveData) null, this);
        }
    }

    @DexIgnore
    public dd(ib7 ib7, long j, kd7<? super vd<T>, ? super fb7<? super i97>, ? extends Object> kd7) {
        ee7.b(ib7, "context");
        ee7.b(kd7, "block");
        this.l = new zc<>(this, kd7, j, zi7.a(qj7.c().g().plus(ib7).plus(dl7.a((ik7) ib7.get(ik7.o)))), new a(this));
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData, com.fossil.xd
    public void d() {
        super.d();
        zc<T> zcVar = this.l;
        if (zcVar != null) {
            zcVar.b();
        }
    }

    @DexIgnore
    @Override // androidx.lifecycle.LiveData, com.fossil.xd
    public void e() {
        super.e();
        zc<T> zcVar = this.l;
        if (zcVar != null) {
            zcVar.a();
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0068 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(androidx.lifecycle.LiveData<T> r6, com.fossil.fb7<? super com.fossil.rj7> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.dd.c
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.dd$c r0 = (com.fossil.dd.c) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.dd$c r0 = new com.fossil.dd$c
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x004a
            if (r2 == r4) goto L_0x003c
            if (r2 != r3) goto L_0x0034
            java.lang.Object r6 = r0.L$1
            androidx.lifecycle.LiveData r6 = (androidx.lifecycle.LiveData) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.dd r6 = (com.fossil.dd) r6
            com.fossil.t87.a(r7)
            goto L_0x0069
        L_0x0034:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003c:
            java.lang.Object r6 = r0.L$1
            androidx.lifecycle.LiveData r6 = (androidx.lifecycle.LiveData) r6
            java.lang.Object r2 = r0.L$0
            com.fossil.dd r2 = (com.fossil.dd) r2
            com.fossil.t87.a(r7)
            r7 = r6
            r6 = r2
            goto L_0x005c
        L_0x004a:
            com.fossil.t87.a(r7)
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r4
            java.lang.Object r7 = r5.a(r0)
            if (r7 != r1) goto L_0x005a
            return r1
        L_0x005a:
            r7 = r6
            r6 = r5
        L_0x005c:
            r0.L$0 = r6
            r0.L$1 = r7
            r0.label = r3
            java.lang.Object r7 = com.fossil.ed.a(r6, r7, r0)
            if (r7 != r1) goto L_0x0069
            return r1
        L_0x0069:
            com.fossil.id r7 = (com.fossil.id) r7
            r6.m = r7
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dd.a(androidx.lifecycle.LiveData, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r5) {
        /*
            r4 = this;
            boolean r0 = r5 instanceof com.fossil.dd.b
            if (r0 == 0) goto L_0x0013
            r0 = r5
            com.fossil.dd$b r0 = (com.fossil.dd.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.dd$b r0 = new com.fossil.dd$b
            r0.<init>(r4, r5)
        L_0x0018:
            java.lang.Object r5 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.dd r0 = (com.fossil.dd) r0
            com.fossil.t87.a(r5)
            goto L_0x0048
        L_0x002d:
            java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r5.<init>(r0)
            throw r5
        L_0x0035:
            com.fossil.t87.a(r5)
            com.fossil.id r5 = r4.m
            if (r5 == 0) goto L_0x004b
            r0.L$0 = r4
            r0.label = r3
            java.lang.Object r5 = r5.a(r0)
            if (r5 != r1) goto L_0x0047
            return r1
        L_0x0047:
            r0 = r4
        L_0x0048:
            com.fossil.i97 r5 = (com.fossil.i97) r5
            goto L_0x004c
        L_0x004b:
            r0 = r4
        L_0x004c:
            r5 = 0
            r0.m = r5
            com.fossil.i97 r5 = com.fossil.i97.a
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dd.a(com.fossil.fb7):java.lang.Object");
    }
}
