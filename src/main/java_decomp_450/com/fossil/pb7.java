package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pb7 {
    @DexIgnore
    public static final Boolean a(boolean z) {
        return Boolean.valueOf(z);
    }

    @DexIgnore
    public static final Short a(short s) {
        return new Short(s);
    }

    @DexIgnore
    public static final Integer a(int i) {
        return new Integer(i);
    }

    @DexIgnore
    public static final Long a(long j) {
        return new Long(j);
    }

    @DexIgnore
    public static final Float a(float f) {
        return new Float(f);
    }

    @DexIgnore
    public static final Double a(double d) {
        return new Double(d);
    }

    @DexIgnore
    public static final Character a(char c) {
        return new Character(c);
    }
}
