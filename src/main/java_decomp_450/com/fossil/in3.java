package com.fossil;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class in3 extends i72 implements i12 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<in3> CREATOR; // = new hn3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Intent c;

    @DexIgnore
    public in3(int i, int i2, Intent intent) {
        this.a = i;
        this.b = i2;
        this.c = intent;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public final Status a() {
        if (this.b == 0) {
            return Status.e;
        }
        return Status.i;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, this.b);
        k72.a(parcel, 3, (Parcelable) this.c, i, false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public in3() {
        this(0, null);
    }

    @DexIgnore
    public in3(int i, Intent intent) {
        this(2, 0, null);
    }
}
