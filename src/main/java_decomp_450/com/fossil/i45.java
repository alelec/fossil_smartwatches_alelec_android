package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class i45 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ FlexibleTextView J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ RTLImageView L;
    @DexIgnore
    public /* final */ LinearLayout M;
    @DexIgnore
    public /* final */ LinearLayout N;
    @DexIgnore
    public /* final */ LinearLayout O;
    @DexIgnore
    public /* final */ ConstraintLayout P;
    @DexIgnore
    public /* final */ ScrollView Q;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat R;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat S;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat T;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat U;
    @DexIgnore
    public /* final */ View V;
    @DexIgnore
    public /* final */ View W;
    @DexIgnore
    public /* final */ View X;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public i45(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, FlexibleButton flexibleButton, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ConstraintLayout constraintLayout6, ScrollView scrollView, FlexibleSwitchCompat flexibleSwitchCompat, FlexibleSwitchCompat flexibleSwitchCompat2, FlexibleSwitchCompat flexibleSwitchCompat3, FlexibleSwitchCompat flexibleSwitchCompat4, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = constraintLayout3;
        this.t = constraintLayout4;
        this.u = constraintLayout5;
        this.v = flexibleButton;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
        this.y = flexibleTextView3;
        this.z = flexibleTextView4;
        this.A = flexibleTextView5;
        this.B = flexibleTextView6;
        this.C = flexibleTextView7;
        this.D = flexibleTextView8;
        this.E = flexibleTextView9;
        this.F = flexibleTextView10;
        this.G = flexibleTextView11;
        this.H = flexibleTextView12;
        this.I = flexibleTextView13;
        this.J = flexibleTextView14;
        this.K = flexibleTextView15;
        this.L = rTLImageView;
        this.M = linearLayout;
        this.N = linearLayout2;
        this.O = linearLayout3;
        this.P = constraintLayout6;
        this.Q = scrollView;
        this.R = flexibleSwitchCompat;
        this.S = flexibleSwitchCompat2;
        this.T = flexibleSwitchCompat3;
        this.U = flexibleSwitchCompat4;
        this.V = view2;
        this.W = view3;
        this.X = view4;
    }
}
