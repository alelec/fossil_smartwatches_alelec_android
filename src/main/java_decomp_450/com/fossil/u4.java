package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.c5;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u4 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public /* final */ v4 b;
    @DexIgnore
    public /* final */ w4 c;
    @DexIgnore
    public int d; // = 8;
    @DexIgnore
    public c5 e; // = null;
    @DexIgnore
    public int[] f; // = new int[8];
    @DexIgnore
    public int[] g; // = new int[8];
    @DexIgnore
    public float[] h; // = new float[8];
    @DexIgnore
    public int i; // = -1;
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public boolean k; // = false;

    @DexIgnore
    public u4(v4 v4Var, w4 w4Var) {
        this.b = v4Var;
        this.c = w4Var;
    }

    @DexIgnore
    public final void a(c5 c5Var, float f2) {
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            a(c5Var, true);
            return;
        }
        int i2 = this.i;
        if (i2 == -1) {
            this.i = 0;
            this.h[0] = f2;
            this.f[0] = c5Var.b;
            this.g[0] = -1;
            c5Var.j++;
            c5Var.a(this.b);
            this.a++;
            if (!this.k) {
                int i3 = this.j + 1;
                this.j = i3;
                int[] iArr = this.f;
                if (i3 >= iArr.length) {
                    this.k = true;
                    this.j = iArr.length - 1;
                    return;
                }
                return;
            }
            return;
        }
        int i4 = 0;
        int i5 = -1;
        while (i2 != -1 && i4 < this.a) {
            int[] iArr2 = this.f;
            int i6 = iArr2[i2];
            int i7 = c5Var.b;
            if (i6 == i7) {
                this.h[i2] = f2;
                return;
            }
            if (iArr2[i2] < i7) {
                i5 = i2;
            }
            i2 = this.g[i2];
            i4++;
        }
        int i8 = this.j;
        int i9 = i8 + 1;
        if (this.k) {
            int[] iArr3 = this.f;
            if (iArr3[i8] != -1) {
                i8 = iArr3.length;
            }
        } else {
            i8 = i9;
        }
        int[] iArr4 = this.f;
        if (i8 >= iArr4.length && this.a < iArr4.length) {
            int i10 = 0;
            while (true) {
                int[] iArr5 = this.f;
                if (i10 >= iArr5.length) {
                    break;
                } else if (iArr5[i10] == -1) {
                    i8 = i10;
                    break;
                } else {
                    i10++;
                }
            }
        }
        int[] iArr6 = this.f;
        if (i8 >= iArr6.length) {
            i8 = iArr6.length;
            int i11 = this.d * 2;
            this.d = i11;
            this.k = false;
            this.j = i8 - 1;
            this.h = Arrays.copyOf(this.h, i11);
            this.f = Arrays.copyOf(this.f, this.d);
            this.g = Arrays.copyOf(this.g, this.d);
        }
        this.f[i8] = c5Var.b;
        this.h[i8] = f2;
        if (i5 != -1) {
            int[] iArr7 = this.g;
            iArr7[i8] = iArr7[i5];
            iArr7[i5] = i8;
        } else {
            this.g[i8] = this.i;
            this.i = i8;
        }
        c5Var.j++;
        c5Var.a(this.b);
        this.a++;
        if (!this.k) {
            this.j++;
        }
        if (this.a >= this.f.length) {
            this.k = true;
        }
        int i12 = this.j;
        int[] iArr8 = this.f;
        if (i12 >= iArr8.length) {
            this.k = true;
            this.j = iArr8.length - 1;
        }
    }

    @DexIgnore
    public void b() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] * -1.0f;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public String toString() {
        int i2 = this.i;
        String str = "";
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            str = ((str + " -> ") + this.h[i2] + " : ") + this.c.c[this.f[i2]];
            i2 = this.g[i2];
            i3++;
        }
        return str;
    }

    @DexIgnore
    public final float b(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.h[i3];
            }
            i3 = this.g[i3];
            i4++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final float b(c5 c5Var) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == c5Var.b) {
                return this.h[i2];
            }
            i2 = this.g[i2];
            i3++;
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void a(c5 c5Var, float f2, boolean z) {
        if (f2 != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            int i2 = this.i;
            if (i2 == -1) {
                this.i = 0;
                this.h[0] = f2;
                this.f[0] = c5Var.b;
                this.g[0] = -1;
                c5Var.j++;
                c5Var.a(this.b);
                this.a++;
                if (!this.k) {
                    int i3 = this.j + 1;
                    this.j = i3;
                    int[] iArr = this.f;
                    if (i3 >= iArr.length) {
                        this.k = true;
                        this.j = iArr.length - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            int i4 = 0;
            int i5 = -1;
            while (i2 != -1 && i4 < this.a) {
                int[] iArr2 = this.f;
                int i6 = iArr2[i2];
                int i7 = c5Var.b;
                if (i6 == i7) {
                    float[] fArr = this.h;
                    fArr[i2] = fArr[i2] + f2;
                    if (fArr[i2] == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        if (i2 == this.i) {
                            this.i = this.g[i2];
                        } else {
                            int[] iArr3 = this.g;
                            iArr3[i5] = iArr3[i2];
                        }
                        if (z) {
                            c5Var.b(this.b);
                        }
                        if (this.k) {
                            this.j = i2;
                        }
                        c5Var.j--;
                        this.a--;
                        return;
                    }
                    return;
                }
                if (iArr2[i2] < i7) {
                    i5 = i2;
                }
                i2 = this.g[i2];
                i4++;
            }
            int i8 = this.j;
            int i9 = i8 + 1;
            if (this.k) {
                int[] iArr4 = this.f;
                if (iArr4[i8] != -1) {
                    i8 = iArr4.length;
                }
            } else {
                i8 = i9;
            }
            int[] iArr5 = this.f;
            if (i8 >= iArr5.length && this.a < iArr5.length) {
                int i10 = 0;
                while (true) {
                    int[] iArr6 = this.f;
                    if (i10 >= iArr6.length) {
                        break;
                    } else if (iArr6[i10] == -1) {
                        i8 = i10;
                        break;
                    } else {
                        i10++;
                    }
                }
            }
            int[] iArr7 = this.f;
            if (i8 >= iArr7.length) {
                i8 = iArr7.length;
                int i11 = this.d * 2;
                this.d = i11;
                this.k = false;
                this.j = i8 - 1;
                this.h = Arrays.copyOf(this.h, i11);
                this.f = Arrays.copyOf(this.f, this.d);
                this.g = Arrays.copyOf(this.g, this.d);
            }
            this.f[i8] = c5Var.b;
            this.h[i8] = f2;
            if (i5 != -1) {
                int[] iArr8 = this.g;
                iArr8[i8] = iArr8[i5];
                iArr8[i5] = i8;
            } else {
                this.g[i8] = this.i;
                this.i = i8;
            }
            c5Var.j++;
            c5Var.a(this.b);
            this.a++;
            if (!this.k) {
                this.j++;
            }
            int i12 = this.j;
            int[] iArr9 = this.f;
            if (i12 >= iArr9.length) {
                this.k = true;
                this.j = iArr9.length - 1;
            }
        }
    }

    @DexIgnore
    public final float a(c5 c5Var, boolean z) {
        if (this.e == c5Var) {
            this.e = null;
        }
        int i2 = this.i;
        if (i2 == -1) {
            return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        int i3 = 0;
        int i4 = -1;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == c5Var.b) {
                if (i2 == this.i) {
                    this.i = this.g[i2];
                } else {
                    int[] iArr = this.g;
                    iArr[i4] = iArr[i2];
                }
                if (z) {
                    c5Var.b(this.b);
                }
                c5Var.j--;
                this.a--;
                this.f[i2] = -1;
                if (this.k) {
                    this.j = i2;
                }
                return this.h[i2];
            }
            i3++;
            i4 = i2;
            i2 = this.g[i2];
        }
        return LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public final void a() {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            c5 c5Var = this.c.c[this.f[i2]];
            if (c5Var != null) {
                c5Var.b(this.b);
            }
            i2 = this.g[i2];
            i3++;
        }
        this.i = -1;
        this.j = -1;
        this.k = false;
        this.a = 0;
    }

    @DexIgnore
    public final boolean a(c5 c5Var) {
        int i2 = this.i;
        if (i2 == -1) {
            return false;
        }
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            if (this.f[i2] == c5Var.b) {
                return true;
            }
            i2 = this.g[i2];
            i3++;
        }
        return false;
    }

    @DexIgnore
    public void a(float f2) {
        int i2 = this.i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            float[] fArr = this.h;
            fArr[i2] = fArr[i2] / f2;
            i2 = this.g[i2];
            i3++;
        }
    }

    @DexIgnore
    public final boolean a(c5 c5Var, y4 y4Var) {
        return c5Var.j <= 1;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0090 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fossil.c5 a(com.fossil.y4 r15) {
        /*
            r14 = this;
            int r0 = r14.i
            r1 = 0
            r2 = 0
            r3 = 0
            r2 = r1
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
        L_0x000b:
            r9 = -1
            if (r0 == r9) goto L_0x0098
            int r9 = r14.a
            if (r4 >= r9) goto L_0x0098
            float[] r9 = r14.h
            r10 = r9[r0]
            r11 = 981668463(0x3a83126f, float:0.001)
            com.fossil.w4 r12 = r14.c
            com.fossil.c5[] r12 = r12.c
            int[] r13 = r14.f
            r13 = r13[r0]
            r12 = r12[r13]
            int r13 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r13 >= 0) goto L_0x0036
            r11 = -1165815185(0xffffffffba83126f, float:-0.001)
            int r11 = (r10 > r11 ? 1 : (r10 == r11 ? 0 : -1))
            if (r11 <= 0) goto L_0x0042
            r9[r0] = r3
            com.fossil.v4 r9 = r14.b
            r12.b(r9)
            goto L_0x0041
        L_0x0036:
            int r11 = (r10 > r11 ? 1 : (r10 == r11 ? 0 : -1))
            if (r11 >= 0) goto L_0x0042
            r9[r0] = r3
            com.fossil.v4 r9 = r14.b
            r12.b(r9)
        L_0x0041:
            r10 = 0
        L_0x0042:
            r9 = 1
            int r11 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r11 == 0) goto L_0x0090
            com.fossil.c5$a r11 = r12.g
            com.fossil.c5$a r13 = com.fossil.c5.a.UNRESTRICTED
            if (r11 != r13) goto L_0x006c
            if (r2 != 0) goto L_0x0057
            boolean r2 = r14.a(r12, r15)
        L_0x0053:
            r5 = r2
            r7 = r10
            r2 = r12
            goto L_0x0090
        L_0x0057:
            int r11 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
            if (r11 <= 0) goto L_0x0060
            boolean r2 = r14.a(r12, r15)
            goto L_0x0053
        L_0x0060:
            if (r5 != 0) goto L_0x0090
            boolean r11 = r14.a(r12, r15)
            if (r11 == 0) goto L_0x0090
            r7 = r10
            r2 = r12
            r5 = 1
            goto L_0x0090
        L_0x006c:
            if (r2 != 0) goto L_0x0090
            int r11 = (r10 > r3 ? 1 : (r10 == r3 ? 0 : -1))
            if (r11 >= 0) goto L_0x0090
            if (r1 != 0) goto L_0x007c
            boolean r1 = r14.a(r12, r15)
        L_0x0078:
            r6 = r1
            r8 = r10
            r1 = r12
            goto L_0x0090
        L_0x007c:
            int r11 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r11 <= 0) goto L_0x0085
            boolean r1 = r14.a(r12, r15)
            goto L_0x0078
        L_0x0085:
            if (r6 != 0) goto L_0x0090
            boolean r11 = r14.a(r12, r15)
            if (r11 == 0) goto L_0x0090
            r8 = r10
            r1 = r12
            r6 = 1
        L_0x0090:
            int[] r9 = r14.g
            r0 = r9[r0]
            int r4 = r4 + 1
            goto L_0x000b
        L_0x0098:
            if (r2 == 0) goto L_0x009b
            return r2
        L_0x009b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.u4.a(com.fossil.y4):com.fossil.c5");
    }

    @DexIgnore
    public final void a(v4 v4Var, v4 v4Var2, boolean z) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            while (i2 != -1 && i3 < this.a) {
                int i4 = this.f[i2];
                c5 c5Var = v4Var2.a;
                if (i4 == c5Var.b) {
                    float f2 = this.h[i2];
                    a(c5Var, z);
                    u4 u4Var = v4Var2.d;
                    int i5 = u4Var.i;
                    int i6 = 0;
                    while (i5 != -1 && i6 < u4Var.a) {
                        a(this.c.c[u4Var.f[i5]], u4Var.h[i5] * f2, z);
                        i5 = u4Var.g[i5];
                        i6++;
                    }
                    v4Var.b += v4Var2.b * f2;
                    if (z) {
                        v4Var2.a.b(v4Var);
                    }
                    i2 = this.i;
                } else {
                    i2 = this.g[i2];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public void a(v4 v4Var, v4[] v4VarArr) {
        int i2 = this.i;
        while (true) {
            int i3 = 0;
            while (i2 != -1 && i3 < this.a) {
                c5 c5Var = this.c.c[this.f[i2]];
                if (c5Var.c != -1) {
                    float f2 = this.h[i2];
                    a(c5Var, true);
                    v4 v4Var2 = v4VarArr[c5Var.c];
                    if (!v4Var2.e) {
                        u4 u4Var = v4Var2.d;
                        int i4 = u4Var.i;
                        int i5 = 0;
                        while (i4 != -1 && i5 < u4Var.a) {
                            a(this.c.c[u4Var.f[i4]], u4Var.h[i4] * f2, true);
                            i4 = u4Var.g[i4];
                            i5++;
                        }
                    }
                    v4Var.b += v4Var2.b * f2;
                    v4Var2.a.b(v4Var);
                    i2 = this.i;
                } else {
                    i2 = this.g[i2];
                    i3++;
                }
            }
            return;
        }
    }

    @DexIgnore
    public c5 a(boolean[] zArr, c5 c5Var) {
        c5.a aVar;
        int i2 = this.i;
        int i3 = 0;
        c5 c5Var2 = null;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (i2 != -1 && i3 < this.a) {
            if (this.h[i2] < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                c5 c5Var3 = this.c.c[this.f[i2]];
                if ((zArr == null || !zArr[c5Var3.b]) && c5Var3 != c5Var && ((aVar = c5Var3.g) == c5.a.SLACK || aVar == c5.a.ERROR)) {
                    float f3 = this.h[i2];
                    if (f3 < f2) {
                        c5Var2 = c5Var3;
                        f2 = f3;
                    }
                }
            }
            i2 = this.g[i2];
            i3++;
        }
        return c5Var2;
    }

    @DexIgnore
    public final c5 a(int i2) {
        int i3 = this.i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.c.c[this.f[i3]];
            }
            i3 = this.g[i3];
            i4++;
        }
        return null;
    }
}
