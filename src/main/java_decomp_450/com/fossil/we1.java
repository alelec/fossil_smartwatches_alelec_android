package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we1 extends xp0 {
    @DexIgnore
    public static /* final */ cd1 CREATOR; // = new cd1(null);
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ JSONObject e;

    @DexIgnore
    public we1(byte b, int i, JSONObject jSONObject) {
        super(ru0.JSON_FILE_EVENT, b, false, 4);
        this.d = i;
        this.e = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.xp0
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.p, Integer.valueOf(this.d)), r51.b3, this.e);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(we1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            we1 we1 = (we1) obj;
            return ((xp0) this).a == ((xp0) we1).a && ((xp0) this).b == ((xp0) we1).b && this.d == we1.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.JSONRequestEvent");
    }

    @DexIgnore
    public int hashCode() {
        return (((((xp0) this).a.hashCode() * 31) + ((xp0) this).b) * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.xp0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.toString());
        }
    }

    @DexIgnore
    public /* synthetic */ we1(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.d = parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            this.e = new JSONObject(readString);
        } else {
            ee7.a();
            throw null;
        }
    }
}
