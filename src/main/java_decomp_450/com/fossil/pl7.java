package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl7 extends ql7 implements jj7 {
    @DexIgnore
    public volatile pl7 _immediate;
    @DexIgnore
    public /* final */ pl7 b;
    @DexIgnore
    public /* final */ Handler c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ boolean e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements rj7 {
        @DexIgnore
        public /* final */ /* synthetic */ pl7 a;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable b;

        @DexIgnore
        public a(pl7 pl7, Runnable runnable) {
            this.a = pl7;
            this.b = runnable;
        }

        @DexIgnore
        @Override // com.fossil.rj7
        public void dispose() {
            this.a.c.removeCallbacks(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ pl7 a;
        @DexIgnore
        public /* final */ /* synthetic */ ai7 b;

        @DexIgnore
        public b(pl7 pl7, ai7 ai7) {
            this.a = pl7;
            this.b = ai7;
        }

        @DexIgnore
        public final void run() {
            this.b.a((ti7) this.a, (Object) i97.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements gd7<Throwable, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable $block;
        @DexIgnore
        public /* final */ /* synthetic */ pl7 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(pl7 pl7, Runnable runnable) {
            super(1);
            this.this$0 = pl7;
            this.$block = runnable;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
            invoke(th);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.this$0.c.removeCallbacks(this.$block);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public pl7(Handler handler, String str, boolean z) {
        super(null);
        pl7 pl7 = null;
        this.c = handler;
        this.d = str;
        this.e = z;
        this._immediate = z ? this : pl7;
        pl7 pl72 = this._immediate;
        if (pl72 == null) {
            pl72 = new pl7(this.c, this.d, true);
            this._immediate = pl72;
        }
        this.b = pl72;
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public boolean b(ib7 ib7) {
        return !this.e || (ee7.a(Looper.myLooper(), this.c.getLooper()) ^ true);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof pl7) && ((pl7) obj).c == this.c;
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(this.c);
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public String toString() {
        String str = this.d;
        if (str == null) {
            return this.c.toString();
        }
        if (!this.e) {
            return str;
        }
        return this.d + " [immediate]";
    }

    @DexIgnore
    @Override // com.fossil.ti7
    public void a(ib7 ib7, Runnable runnable) {
        this.c.post(runnable);
    }

    @DexIgnore
    @Override // com.fossil.tk7
    public pl7 g() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.jj7, com.fossil.ql7
    public rj7 a(long j, Runnable runnable) {
        this.c.postDelayed(runnable, qf7.b(j, 4611686018427387903L));
        return new a(this, runnable);
    }

    @DexIgnore
    public pl7(Handler handler, String str) {
        this(handler, str, false);
    }

    @DexIgnore
    @Override // com.fossil.jj7
    public void a(long j, ai7<? super i97> ai7) {
        b bVar = new b(this, ai7);
        this.c.postDelayed(bVar, qf7.b(j, 4611686018427387903L));
        ai7.a(new c(this, bVar));
    }
}
