package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xv0 extends fe7 implements gd7<sz0, Boolean> {
    @DexIgnore
    public static /* final */ xv0 a; // = new xv0();

    @DexIgnore
    public xv0() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public Boolean invoke(sz0 sz0) {
        return Boolean.valueOf(sz0.c == ay0.n);
    }
}
