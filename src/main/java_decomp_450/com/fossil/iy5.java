package com.fossil;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FilterQueryProvider;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.cq5;
import com.fossil.cy6;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy5 extends go5 implements hy5, cy6.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public gy5 f;
    @DexIgnore
    public qw6<c45> g;
    @DexIgnore
    public cq5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return iy5.j;
        }

        @DexIgnore
        public final iy5 b() {
            return new iy5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements cq5.b {
        @DexIgnore
        public /* final */ /* synthetic */ iy5 a;

        @DexIgnore
        public b(iy5 iy5) {
            this.a = iy5;
        }

        @DexIgnore
        @Override // com.fossil.cq5.b
        public void a(bt5 bt5) {
            ee7.b(bt5, "contactWrapper");
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, bt5, bt5.getCurrentHandGroup(), iy5.b(this.a).h());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ iy5 a;

        @DexIgnore
        public c(iy5 iy5) {
            this.a = iy5;
        }

        @DexIgnore
        public final void onClick(View view) {
            iy5.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ iy5 a;
        @DexIgnore
        public /* final */ /* synthetic */ c45 b;

        @DexIgnore
        public d(iy5 iy5, c45 c45) {
            this.a = iy5;
            this.b = c45;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.b.t;
            ee7.a((Object) rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 > 0 ? 0 : 4);
            cq5 a2 = this.a.h;
            if (a2 != null) {
                a2.a(String.valueOf(charSequence));
            }
            AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = this.b.w;
            ee7.a((Object) alphabetFastScrollRecyclerView, "binding.rvContacts");
            RecyclerView.m layoutManager = alphabetFastScrollRecyclerView.getLayoutManager();
            if (layoutManager != null) {
                ((LinearLayoutManager) layoutManager).f(0, 0);
                return;
            }
            throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ c45 a;

        @DexIgnore
        public e(c45 c45) {
            this.a = c45;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                c45 c45 = this.a;
                ee7.a((Object) c45, "binding");
                c45.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ c45 a;

        @DexIgnore
        public f(c45 c45) {
            this.a = c45;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = iy5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationHybridContac\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gy5 b(iy5 iy5) {
        gy5 gy5 = iy5.f;
        if (gy5 != null) {
            return gy5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        gy5 gy5 = this.f;
        if (gy5 != null) {
            gy5.i();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        c45 c45 = (c45) qb.a(layoutInflater, 2131558593, viewGroup, false, a1());
        c45.s.setOnClickListener(new c(this));
        c45.q.addTextChangedListener(new d(this, c45));
        c45.q.setOnFocusChangeListener(new e(c45));
        c45.t.setOnClickListener(new f(c45));
        c45.w.setIndexBarVisibility(true);
        c45.w.setIndexBarHighLateTextVisibility(true);
        c45.w.setIndexbarHighLateTextColor(2131099703);
        c45.w.setIndexBarTransparentValue(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        c45.w.setIndexTextSize(9);
        Typeface a2 = c7.a(requireContext(), 2131296256);
        String b2 = eh5.l.a().b("primaryText");
        Typeface c2 = eh5.l.a().c("nonBrandTextStyle5");
        if (c2 != null) {
            a2 = c2;
        }
        if (!TextUtils.isEmpty(b2)) {
            int parseColor = Color.parseColor(b2);
            c45.w.setIndexBarTextColor(parseColor);
            c45.w.setIndexbarHighLateTextColor(parseColor);
        }
        if (a2 != null) {
            c45.w.setTypeface(a2);
        }
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView = c45.w;
        alphabetFastScrollRecyclerView.setLayoutManager(new LinearLayoutManager(alphabetFastScrollRecyclerView.getContext()));
        alphabetFastScrollRecyclerView.setAdapter(this.h);
        this.g = new qw6<>(this, c45);
        ee7.a((Object) c45, "binding");
        return c45.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        gy5 gy5 = this.f;
        if (gy5 != null) {
            gy5.g();
            super.onPause();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        gy5 gy5 = this.f;
        if (gy5 != null) {
            gy5.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.hy5
    public void s() {
    }

    @DexIgnore
    @Override // com.fossil.hy5
    public void u0() {
        cq5 cq5 = this.h;
        if (cq5 != null) {
            cq5.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public void a(gy5 gy5) {
        ee7.b(gy5, "presenter");
        this.f = gy5;
    }

    @DexIgnore
    @Override // com.fossil.hy5
    public void a(List<bt5> list, FilterQueryProvider filterQueryProvider, int i2) {
        AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView;
        ee7.b(list, "listContactWrapper");
        ee7.b(filterQueryProvider, "filterQueryProvider");
        cq5 cq5 = new cq5(null, list, i2);
        cq5.a(new b(this));
        this.h = cq5;
        if (cq5 != null) {
            cq5.a(filterQueryProvider);
            qw6<c45> qw6 = this.g;
            if (qw6 != null) {
                c45 a2 = qw6.a();
                if (a2 != null && (alphabetFastScrollRecyclerView = a2.w) != null) {
                    alphabetFastScrollRecyclerView.setAdapter(this.h);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.hy5
    public void a(ArrayList<bt5> arrayList) {
        ee7.b(arrayList, "contactWrappersSelected");
        Intent intent = new Intent();
        intent.putExtra("CONTACT_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.hy5
    public void a(Cursor cursor) {
        cq5 cq5 = this.h;
        if (cq5 != null) {
            cq5.c(cursor);
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        bt5 bt5;
        ee7.b(str, "tag");
        if (str.hashCode() != 1018078562 || !str.equals("CONFIRM_REASSIGN_CONTACT")) {
            return;
        }
        if (i2 != 2131363307) {
            cq5 cq5 = this.h;
            if (cq5 != null) {
                cq5.notifyDataSetChanged();
            } else {
                ee7.a();
                throw null;
            }
        } else {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (bt5 = (bt5) extras.getSerializable("CONFIRM_REASSIGN_CONTACT_CONTACT_WRAPPER")) != null) {
                gy5 gy5 = this.f;
                if (gy5 != null) {
                    gy5.a(bt5);
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
