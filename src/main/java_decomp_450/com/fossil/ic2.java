package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ic2 extends i72 {
    @DexIgnore
    public static /* final */ ic2 A; // = d("percentage");
    @DexIgnore
    public static /* final */ ic2 B; // = d(PlaceManager.PARAM_SPEED);
    @DexIgnore
    public static /* final */ ic2 C; // = d("rpm");
    @DexIgnore
    public static /* final */ Parcelable.Creator<ic2> CREATOR; // = new cd2();
    @DexIgnore
    public static /* final */ ic2 D; // = g("google.android.fitness.GoalV2");
    @DexIgnore
    public static /* final */ ic2 E; // = g("symptom");
    @DexIgnore
    public static /* final */ ic2 F; // = g("google.android.fitness.StrideModel");
    @DexIgnore
    public static /* final */ ic2 G; // = g("google.android.fitness.Device");
    @DexIgnore
    public static /* final */ ic2 H; // = b("revolutions");
    @DexIgnore
    public static /* final */ ic2 I; // = d("calories");
    @DexIgnore
    public static /* final */ ic2 J; // = d("watts");
    @DexIgnore
    public static /* final */ ic2 K; // = d("volume");
    @DexIgnore
    public static /* final */ ic2 L; // = c("meal_type");
    @DexIgnore
    public static /* final */ ic2 M; // = new ic2("food_item", 3, true);
    @DexIgnore
    public static /* final */ ic2 N; // = f("nutrients");
    @DexIgnore
    public static /* final */ ic2 O; // = d("elevation.change");
    @DexIgnore
    public static /* final */ ic2 P; // = f("elevation.gain");
    @DexIgnore
    public static /* final */ ic2 Q; // = f("elevation.loss");
    @DexIgnore
    public static /* final */ ic2 R; // = d("floors");
    @DexIgnore
    public static /* final */ ic2 S; // = f("floor.gain");
    @DexIgnore
    public static /* final */ ic2 T; // = f("floor.loss");
    @DexIgnore
    public static /* final */ ic2 U; // = new ic2("exercise", 3);
    @DexIgnore
    public static /* final */ ic2 V; // = c("repetitions");
    @DexIgnore
    public static /* final */ ic2 W; // = e("resistance");
    @DexIgnore
    public static /* final */ ic2 X; // = c("resistance_type");
    @DexIgnore
    public static /* final */ ic2 Y; // = b("num_segments");
    @DexIgnore
    public static /* final */ ic2 Z; // = d(GoalTrackingSummary.COLUMN_AVERAGE);
    @DexIgnore
    public static /* final */ ic2 a0; // = d("max");
    @DexIgnore
    public static /* final */ ic2 b0; // = d("min");
    @DexIgnore
    public static /* final */ ic2 c0; // = d("low_latitude");
    @DexIgnore
    public static /* final */ ic2 d; // = b(Constants.ACTIVITY);
    @DexIgnore
    public static /* final */ ic2 d0; // = d("low_longitude");
    @DexIgnore
    public static /* final */ ic2 e; // = d("confidence");
    @DexIgnore
    public static /* final */ ic2 e0; // = d("high_latitude");
    @DexIgnore
    @Deprecated
    public static /* final */ ic2 f; // = f("activity_confidence");
    @DexIgnore
    public static /* final */ ic2 f0; // = d("high_longitude");
    @DexIgnore
    public static /* final */ ic2 g; // = b("steps");
    @DexIgnore
    public static /* final */ ic2 g0; // = b("occurrences");
    @DexIgnore
    public static /* final */ ic2 h; // = d("step_length");
    @DexIgnore
    public static /* final */ ic2 h0; // = b("sensor_type");
    @DexIgnore
    public static /* final */ ic2 i; // = b("duration");
    @DexIgnore
    public static /* final */ ic2 i0; // = b("sensor_types");
    @DexIgnore
    public static /* final */ ic2 j; // = c("duration");
    @DexIgnore
    public static /* final */ ic2 j0; // = new ic2("timestamps", 5);
    @DexIgnore
    public static /* final */ ic2 k0; // = b("sample_period");
    @DexIgnore
    public static /* final */ ic2 l0; // = b("num_samples");
    @DexIgnore
    public static /* final */ ic2 m0; // = b("num_dimensions");
    @DexIgnore
    public static /* final */ ic2 n0; // = new ic2("sensor_values", 6);
    @DexIgnore
    public static /* final */ ic2 o0; // = d("intensity");
    @DexIgnore
    public static /* final */ ic2 p; // = f("activity_duration.ascending");
    @DexIgnore
    public static /* final */ ic2 p0; // = d("probability");
    @DexIgnore
    public static /* final */ ic2 q; // = f("activity_duration.descending");
    @DexIgnore
    public static /* final */ ic2 r; // = d("bpm");
    @DexIgnore
    public static /* final */ ic2 s; // = d("latitude");
    @DexIgnore
    public static /* final */ ic2 t; // = d("longitude");
    @DexIgnore
    public static /* final */ ic2 u; // = d(PlaceManager.PARAM_ACCURACY);
    @DexIgnore
    public static /* final */ ic2 v; // = e(PlaceManager.PARAM_ALTITUDE);
    @DexIgnore
    public static /* final */ ic2 w; // = d("distance");
    @DexIgnore
    public static /* final */ ic2 x; // = d("height");
    @DexIgnore
    public static /* final */ ic2 y; // = d(Constants.PROFILE_KEY_UNITS_WEIGHT);
    @DexIgnore
    public static /* final */ ic2 z; // = d("circumference");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ ic2 a; // = ic2.d("x");
        @DexIgnore
        public static /* final */ ic2 b; // = ic2.d("y");
        @DexIgnore
        public static /* final */ ic2 c; // = ic2.d("z");
        @DexIgnore
        public static /* final */ ic2 d; // = ic2.h("debug_session");
        @DexIgnore
        public static /* final */ ic2 e; // = ic2.h("google.android.fitness.SessionV2");
        @DexIgnore
        public static /* final */ ic2 f; // = ic2.g("google.android.fitness.DataPointSession");
    }

    @DexIgnore
    public ic2(String str, int i2) {
        this(str, i2, null);
    }

    @DexIgnore
    public static ic2 b(String str) {
        return new ic2(str, 1);
    }

    @DexIgnore
    public static ic2 c(String str) {
        return new ic2(str, 1, true);
    }

    @DexIgnore
    public static ic2 d(String str) {
        return new ic2(str, 2);
    }

    @DexIgnore
    public static ic2 e(String str) {
        return new ic2(str, 2, true);
    }

    @DexIgnore
    public static ic2 f(String str) {
        return new ic2(str, 4);
    }

    @DexIgnore
    public static ic2 g(String str) {
        return new ic2(str, 7);
    }

    @DexIgnore
    public static ic2 h(String str) {
        return new ic2(str, 7, true);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ic2)) {
            return false;
        }
        ic2 ic2 = (ic2) obj;
        return this.a.equals(ic2.a) && this.b == ic2.b;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        Object[] objArr = new Object[2];
        objArr[0] = this.a;
        objArr[1] = this.b == 1 ? "i" : "f";
        return String.format("%s(%s)", objArr);
    }

    @DexIgnore
    public final Boolean v() {
        return this.c;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, g(), false);
        k72.a(parcel, 2, e());
        k72.a(parcel, 3, v(), false);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public ic2(String str, int i2, Boolean bool) {
        a72.a((Object) str);
        this.a = str;
        this.b = i2;
        this.c = bool;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public final String g() {
        return this.a;
    }
}
