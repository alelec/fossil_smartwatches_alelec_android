package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z63 extends IInterface {
    @DexIgnore
    ab2 b(LatLng latLng) throws RemoteException;

    @DexIgnore
    LatLng e(ab2 ab2) throws RemoteException;

    @DexIgnore
    v93 q() throws RemoteException;
}
