package com.fossil;

import com.fossil.ep2;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vm3 {
    @DexIgnore
    public String a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Boolean c;
    @DexIgnore
    public Boolean d;
    @DexIgnore
    public Long e;
    @DexIgnore
    public Long f;

    @DexIgnore
    public vm3(String str, int i) {
        this.a = str;
        this.b = i;
    }

    @DexIgnore
    public static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public static Boolean a(String str, ep2 ep2, jg3 jg3) {
        String str2;
        List<String> list;
        a72.a(ep2);
        if (str == null || !ep2.zza() || ep2.p() == ep2.a.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (ep2.p() == ep2.a.IN_LIST) {
            if (ep2.v() == 0) {
                return null;
            }
        } else if (!ep2.q()) {
            return null;
        }
        ep2.a p = ep2.p();
        boolean t = ep2.t();
        if (t || p == ep2.a.REGEXP || p == ep2.a.IN_LIST) {
            str2 = ep2.r();
        } else {
            str2 = ep2.r().toUpperCase(Locale.ENGLISH);
        }
        if (ep2.v() == 0) {
            list = null;
        } else {
            List<String> u = ep2.u();
            if (!t) {
                ArrayList arrayList = new ArrayList(u.size());
                for (String str3 : u) {
                    arrayList.add(str3.toUpperCase(Locale.ENGLISH));
                }
                u = Collections.unmodifiableList(arrayList);
            }
            list = u;
        }
        return a(str, p, t, str2, list, p == ep2.a.REGEXP ? str2 : null, jg3);
    }

    @DexIgnore
    public static Boolean a(String str, ep2.a aVar, boolean z, String str2, List<String> list, String str3, jg3 jg3) {
        if (str == null) {
            return null;
        }
        if (aVar == ep2.a.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && aVar != ep2.a.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (rm3.a[aVar.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    if (jg3 != null) {
                        jg3.w().a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    }
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    @DexIgnore
    public static Boolean a(long j, cp2 cp2) {
        try {
            return a(new BigDecimal(j), cp2, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean a(double d2, cp2 cp2) {
        try {
            return a(new BigDecimal(d2), cp2, Math.ulp(d2));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    @DexIgnore
    public static Boolean a(String str, cp2 cp2) {
        if (!fm3.a(str)) {
            return null;
        }
        try {
            return a(new BigDecimal(str), cp2, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0085, code lost:
        if (r2 != null) goto L_0x0087;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Boolean a(java.math.BigDecimal r9, com.fossil.cp2 r10, double r11) {
        /*
            com.fossil.a72.a(r10)
            boolean r0 = r10.zza()
            r1 = 0
            if (r0 == 0) goto L_0x0110
            com.fossil.cp2$b r0 = r10.p()
            com.fossil.cp2$b r2 = com.fossil.cp2.b.UNKNOWN_COMPARISON_TYPE
            if (r0 != r2) goto L_0x0014
            goto L_0x0110
        L_0x0014:
            com.fossil.cp2$b r0 = r10.p()
            com.fossil.cp2$b r2 = com.fossil.cp2.b.BETWEEN
            if (r0 != r2) goto L_0x0029
            boolean r0 = r10.u()
            if (r0 == 0) goto L_0x0028
            boolean r0 = r10.w()
            if (r0 != 0) goto L_0x0030
        L_0x0028:
            return r1
        L_0x0029:
            boolean r0 = r10.s()
            if (r0 != 0) goto L_0x0030
            return r1
        L_0x0030:
            com.fossil.cp2$b r0 = r10.p()
            com.fossil.cp2$b r2 = r10.p()
            com.fossil.cp2$b r3 = com.fossil.cp2.b.BETWEEN
            if (r2 != r3) goto L_0x0067
            java.lang.String r2 = r10.v()
            boolean r2 = com.fossil.fm3.a(r2)
            if (r2 == 0) goto L_0x0066
            java.lang.String r2 = r10.x()
            boolean r2 = com.fossil.fm3.a(r2)
            if (r2 != 0) goto L_0x0051
            goto L_0x0066
        L_0x0051:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r3 = r10.v()     // Catch:{ NumberFormatException -> 0x0066 }
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x0066 }
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r10 = r10.x()     // Catch:{ NumberFormatException -> 0x0066 }
            r3.<init>(r10)     // Catch:{ NumberFormatException -> 0x0066 }
            r10 = r2
            r2 = r1
            goto L_0x007d
        L_0x0066:
            return r1
        L_0x0067:
            java.lang.String r2 = r10.t()
            boolean r2 = com.fossil.fm3.a(r2)
            if (r2 != 0) goto L_0x0072
            return r1
        L_0x0072:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0110 }
            java.lang.String r10 = r10.t()     // Catch:{ NumberFormatException -> 0x0110 }
            r2.<init>(r10)     // Catch:{ NumberFormatException -> 0x0110 }
            r10 = r1
            r3 = r10
        L_0x007d:
            com.fossil.cp2$b r4 = com.fossil.cp2.b.BETWEEN
            if (r0 != r4) goto L_0x0085
            if (r10 == 0) goto L_0x0084
            goto L_0x0087
        L_0x0084:
            return r1
        L_0x0085:
            if (r2 == 0) goto L_0x0110
        L_0x0087:
            int[] r4 = com.fossil.rm3.b
            int r0 = r0.ordinal()
            r0 = r4[r0]
            r4 = -1
            r5 = 0
            r6 = 1
            if (r0 == r6) goto L_0x0104
            r7 = 2
            if (r0 == r7) goto L_0x00f8
            r8 = 3
            if (r0 == r8) goto L_0x00b0
            r11 = 4
            if (r0 == r11) goto L_0x009e
            goto L_0x0110
        L_0x009e:
            int r10 = r9.compareTo(r10)
            if (r10 == r4) goto L_0x00ab
            int r9 = r9.compareTo(r3)
            if (r9 == r6) goto L_0x00ab
            r5 = 1
        L_0x00ab:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00b0:
            r0 = 0
            int r10 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r10 == 0) goto L_0x00ec
            java.math.BigDecimal r10 = new java.math.BigDecimal
            r10.<init>(r11)
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r7)
            java.math.BigDecimal r10 = r10.multiply(r0)
            java.math.BigDecimal r10 = r2.subtract(r10)
            int r10 = r9.compareTo(r10)
            if (r10 != r6) goto L_0x00e7
            java.math.BigDecimal r10 = new java.math.BigDecimal
            r10.<init>(r11)
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r7)
            java.math.BigDecimal r10 = r10.multiply(r11)
            java.math.BigDecimal r10 = r2.add(r10)
            int r9 = r9.compareTo(r10)
            if (r9 != r4) goto L_0x00e7
            r5 = 1
        L_0x00e7:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00ec:
            int r9 = r9.compareTo(r2)
            if (r9 != 0) goto L_0x00f3
            r5 = 1
        L_0x00f3:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00f8:
            int r9 = r9.compareTo(r2)
            if (r9 != r6) goto L_0x00ff
            r5 = 1
        L_0x00ff:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x0104:
            int r9 = r9.compareTo(r2)
            if (r9 != r4) goto L_0x010b
            r5 = 1
        L_0x010b:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x0110:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vm3.a(java.math.BigDecimal, com.fossil.cp2, double):java.lang.Boolean");
    }
}
