package com.fossil;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.share.internal.VideoUploader;
import com.fossil.be5;
import com.fossil.cy6;
import com.fossil.gg5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku6 extends el5 implements vu6, cy6.g, gg5.b, cy6.h {
    @DexIgnore
    public static /* final */ String s;
    @DexIgnore
    public static /* final */ a t; // = new a(null);
    @DexIgnore
    public uu6 i;
    @DexIgnore
    public qw6<ox4> j;
    @DexIgnore
    public kd5 p;
    @DexIgnore
    public List<be5.b> q; // = new ArrayList();
    @DexIgnore
    public HashMap r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ku6 a() {
            return new ku6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku6 a;

        @DexIgnore
        public b(ku6 ku6) {
            this.a = ku6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ku6.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ku6 a;

        @DexIgnore
        public c(ku6 ku6) {
            this.a = ku6;
        }

        @DexIgnore
        public final void onClick(View view) {
            ku6.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ku6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements q40<Drawable> {
            @DexIgnore
            @Override // com.fossil.q40
            public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String v1 = ku6.s;
                local.d(v1, "showHour onLoadFail e=" + pyVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
                ee7.b(drawable, "resource");
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ee7.b(swVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(ku6.s, "showHour onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public d(ox4 ox4, ku6 ku6, String str) {
            this.a = ox4;
            this.b = ku6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            kd5 a2;
            jd5<Drawable> a3;
            jd5<Drawable> b2;
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String v1 = ku6.s;
            local.d(v1, "showHour onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.p);
            if (this.b.isActive() && (a2 = this.b.p) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b((q40<Drawable>) new a())) != null) {
                b2.a((ImageView) this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ku6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements q40<Drawable> {
            @DexIgnore
            @Override // com.fossil.q40
            public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String v1 = ku6.s;
                local.d(v1, "showMinute onLoadFail e=" + pyVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
                ee7.b(drawable, "resource");
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ee7.b(swVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(ku6.s, "showMinute onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public e(ox4 ox4, ku6 ku6, String str) {
            this.a = ox4;
            this.b = ku6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            kd5 a2;
            jd5<Drawable> a3;
            jd5<Drawable> b2;
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String v1 = ku6.s;
            local.d(v1, "showMinute onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.p);
            if (this.b.isActive() && (a2 = this.b.p) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b((q40<Drawable>) new a())) != null) {
                b2.a((ImageView) this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ ox4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ku6 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements q40<Drawable> {
            @DexIgnore
            @Override // com.fossil.q40
            public boolean a(py pyVar, Object obj, c50<Drawable> c50, boolean z) {
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String v1 = ku6.s;
                local.d(v1, "showSubeye onLoadFail e=" + pyVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, c50<Drawable> c50, sw swVar, boolean z) {
                ee7.b(drawable, "resource");
                ee7.b(obj, DeviceRequestsHelper.DEVICE_INFO_MODEL);
                ee7.b(c50, "target");
                ee7.b(swVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(ku6.s, "showSubeye onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public f(ox4 ox4, ku6 ku6, String str) {
            this.a = ox4;
            this.b = ku6;
        }

        @DexIgnore
        @Override // com.portfolio.platform.cloudimage.CloudImageHelper.OnImageCallbackListener
        public void onImageCallback(String str, String str2) {
            kd5 a2;
            jd5<Drawable> a3;
            jd5<Drawable> b2;
            ee7.b(str, "serial");
            ee7.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String v1 = ku6.s;
            local.d(v1, "showSubeye onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.p);
            if (this.b.isActive() && (a2 = this.b.p) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b((q40<Drawable>) new a())) != null) {
                b2.a((ImageView) this.a.r);
            }
        }
    }

    /*
    static {
        String simpleName = ku6.class.getSimpleName();
        ee7.a((Object) simpleName, "CalibrationFragment::class.java.simpleName");
        s = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ uu6 b(ku6 ku6) {
        uu6 uu6 = ku6.i;
        if (uu6 != null) {
            return uu6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void D(String str) {
        ee7.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local.d(str2, "showMinute: deviceId = " + str);
            qw6<ox4> qw6 = this.j;
            if (qw6 != null) {
                ox4 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    ee7.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131887092));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131887091));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(be5.o.b(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    ee7.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, be5.o.b(str, this.q.get(1))).setImageCallback(new e(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void O() {
        FLogger.INSTANCE.getLocal().d(s, "showBluetoothError");
        if (isActive()) {
            uu6 uu6 = this.i;
            if (uu6 != null) {
                uu6.a(true);
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.g(childFragmentManager);
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.el5
    public void Z0() {
        HashMap hashMap = this.r;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void a(View view, Boolean bool) {
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void c(View view) {
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d(s, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void d(int i2, int i3) {
        String str;
        if (isActive()) {
            qw6<ox4> qw6 = this.j;
            if (qw6 != null) {
                ox4 a2 = qw6.a();
                if (a2 != null) {
                    a2.A.setLength(i3);
                    int i4 = i2 + 1;
                    a2.A.setProgress((int) ((((float) i4) / ((float) i3)) * ((float) 100)));
                    if (i4 < i3) {
                        str = ig5.a(PortfolioApp.g0.c(), 2131887087);
                    } else {
                        str = ig5.a(PortfolioApp.g0.c(), 2131887090);
                    }
                    FlexibleButton flexibleButton = a2.y;
                    ee7.a((Object) flexibleButton, "it.ftvNext");
                    flexibleButton.setText(str);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.h();
            return true;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.cy6.h
    public void k(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            uu6 uu6 = this.i;
            if (uu6 != null) {
                uu6.a(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.el5
    public void m1() {
        v();
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void n(String str) {
        ee7.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = s;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.q.size() > 2) {
                qw6<ox4> qw6 = this.j;
                if (qw6 != null) {
                    ox4 a2 = qw6.a();
                    if (a2 != null) {
                        FlexibleTextView flexibleTextView = a2.z;
                        ee7.a((Object) flexibleTextView, "binding.ftvTitle");
                        flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131887094));
                        FlexibleTextView flexibleTextView2 = a2.x;
                        ee7.a((Object) flexibleTextView2, "binding.ftvDescription");
                        flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131887093));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(be5.o.b(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        ee7.a((Object) appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, be5.o.b(str, this.q.get(2))).setImageCallback(new f(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                ee7.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.el5
    public void n1() {
        v();
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void o(String str) {
        ee7.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = s;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            qw6<ox4> qw6 = this.j;
            if (qw6 != null) {
                ox4 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.z;
                    ee7.a((Object) flexibleTextView, "binding.ftvTitle");
                    flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131887089));
                    FlexibleTextView flexibleTextView2 = a2.x;
                    ee7.a((Object) flexibleTextView2, "binding.ftvDescription");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131887088));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(be5.o.b(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    ee7.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, be5.o.b(str, this.q.get(0))).setImageCallback(new d(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.el5
    public void o1() {
        v();
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void onClick(View view) {
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d(s, "GestureDetectorCallback onClick");
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.b(view.getId() == 2131361855);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        ox4 ox4 = (ox4) qb.a(layoutInflater, 2131558505, viewGroup, false, a1());
        ox4.q.setOnClickListener(new b(this));
        ox4.y.setOnClickListener(new c(this));
        new gg5().a(ox4.s, this);
        new gg5().a(ox4.t, this);
        this.j = new qw6<>(this, ox4);
        this.p = hd5.a(this);
        uu6 uu6 = this.i;
        if (uu6 != null) {
            this.q = uu6.i();
            ee7.a((Object) ox4, "binding");
            return ox4.d();
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.el5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.el5
    public void p1() {
    }

    @DexIgnore
    @Override // com.fossil.el5
    public void q1() {
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void v() {
        FLogger.INSTANCE.getLocal().d(s, VideoUploader.PARAM_VALUE_UPLOAD_FINISH_PHASE);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.vu6
    public void y0() {
        FLogger.INSTANCE.getLocal().d(s, "showGeneralError");
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.a(true);
            if (isActive()) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.D(childFragmentManager);
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void b(View view) {
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d(s, "GestureDetectorCallback onLongPressEnded");
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.k();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(uu6 uu6) {
        ee7.b(uu6, "presenter");
        this.i = uu6;
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void a(View view) {
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d(s, "GestureDetectorCallback onLongPressEvent");
        uu6 uu6 = this.i;
        if (uu6 != null) {
            uu6.c(view.getId() == 2131361855);
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.el5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        super.a(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((cl5) activity).a(str, i2, intent);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        v();
    }

    @DexIgnore
    @Override // com.fossil.gg5.b
    public void d(View view) {
        ee7.b(view, "view");
        FLogger.INSTANCE.getLocal().d(s, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }
}
