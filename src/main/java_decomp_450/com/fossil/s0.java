package com.fossil;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.ActionBarContainer;
import androidx.appcompat.widget.ActionBarContextView;
import androidx.appcompat.widget.ActionBarOverlayLayout;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.p1;
import java.lang.ref.WeakReference;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s0 extends ActionBar implements ActionBarOverlayLayout.d {
    @DexIgnore
    public static /* final */ Interpolator C; // = new AccelerateInterpolator();
    @DexIgnore
    public static /* final */ Interpolator D; // = new DecelerateInterpolator();
    @DexIgnore
    public /* final */ ia A; // = new b();
    @DexIgnore
    public /* final */ ka B; // = new c();
    @DexIgnore
    public Context a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public Activity c;
    @DexIgnore
    public ActionBarOverlayLayout d;
    @DexIgnore
    public ActionBarContainer e;
    @DexIgnore
    public p2 f;
    @DexIgnore
    public ActionBarContextView g;
    @DexIgnore
    public View h;
    @DexIgnore
    public z2 i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public d k;
    @DexIgnore
    public ActionMode l;
    @DexIgnore
    public ActionMode.Callback m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public ArrayList<ActionBar.a> o; // = new ArrayList<>();
    @DexIgnore
    public boolean p;
    @DexIgnore
    public int q; // = 0;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v; // = true;
    @DexIgnore
    public h1 w;
    @DexIgnore
    public boolean x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public /* final */ ia z; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ja {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            View view2;
            s0 s0Var = s0.this;
            if (s0Var.r && (view2 = s0Var.h) != null) {
                view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                s0.this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            s0.this.e.setVisibility(8);
            s0.this.e.setTransitioning(false);
            s0 s0Var2 = s0.this;
            s0Var2.w = null;
            s0Var2.l();
            ActionBarOverlayLayout actionBarOverlayLayout = s0.this.d;
            if (actionBarOverlayLayout != null) {
                da.L(actionBarOverlayLayout);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ja {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.ia
        public void b(View view) {
            s0 s0Var = s0.this;
            s0Var.w = null;
            s0Var.e.requestLayout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements ka {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // com.fossil.ka
        public void a(View view) {
            ((View) s0.this.e.getParent()).invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends ActionMode implements p1.a {
        @DexIgnore
        public /* final */ Context c;
        @DexIgnore
        public /* final */ p1 d;
        @DexIgnore
        public ActionMode.Callback e;
        @DexIgnore
        public WeakReference<View> f;

        @DexIgnore
        public d(Context context, ActionMode.Callback callback) {
            this.c = context;
            this.e = callback;
            p1 p1Var = new p1(context);
            p1Var.c(1);
            this.d = p1Var;
            p1Var.a(this);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void a() {
            s0 s0Var = s0.this;
            if (s0Var.k == this) {
                if (!s0.a(s0Var.s, s0Var.t, false)) {
                    s0 s0Var2 = s0.this;
                    s0Var2.l = this;
                    s0Var2.m = this.e;
                } else {
                    this.e.a(this);
                }
                this.e = null;
                s0.this.f(false);
                s0.this.g.a();
                s0.this.f.k().sendAccessibilityEvent(32);
                s0 s0Var3 = s0.this;
                s0Var3.d.setHideOnContentScrollEnabled(s0Var3.y);
                s0.this.k = null;
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void b(CharSequence charSequence) {
            s0.this.g.setTitle(charSequence);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public Menu c() {
            return this.d;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public MenuInflater d() {
            return new g1(this.c);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public CharSequence e() {
            return s0.this.g.getSubtitle();
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public CharSequence g() {
            return s0.this.g.getTitle();
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void i() {
            if (s0.this.k == this) {
                this.d.s();
                try {
                    this.e.b(this, this.d);
                } finally {
                    this.d.r();
                }
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public boolean j() {
            return s0.this.g.c();
        }

        @DexIgnore
        public boolean k() {
            this.d.s();
            try {
                return this.e.a(this, this.d);
            } finally {
                this.d.r();
            }
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void b(int i) {
            b(s0.this.a.getResources().getString(i));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public View b() {
            WeakReference<View> weakReference = this.f;
            if (weakReference != null) {
                return weakReference.get();
            }
            return null;
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void a(View view) {
            s0.this.g.setCustomView(view);
            this.f = new WeakReference<>(view);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void a(CharSequence charSequence) {
            s0.this.g.setSubtitle(charSequence);
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void a(int i) {
            a((CharSequence) s0.this.a.getResources().getString(i));
        }

        @DexIgnore
        @Override // androidx.appcompat.view.ActionMode
        public void a(boolean z) {
            super.a(z);
            s0.this.g.setTitleOptional(z);
        }

        @DexIgnore
        @Override // com.fossil.p1.a
        public boolean a(p1 p1Var, MenuItem menuItem) {
            ActionMode.Callback callback = this.e;
            if (callback != null) {
                return callback.a(this, menuItem);
            }
            return false;
        }

        @DexIgnore
        @Override // com.fossil.p1.a
        public void a(p1 p1Var) {
            if (this.e != null) {
                i();
                s0.this.g.e();
            }
        }
    }

    @DexIgnore
    public s0(Activity activity, boolean z2) {
        new ArrayList();
        this.c = activity;
        View decorView = activity.getWindow().getDecorView();
        b(decorView);
        if (!z2) {
            this.h = decorView.findViewById(16908290);
        }
    }

    @DexIgnore
    public static boolean a(boolean z2, boolean z3, boolean z4) {
        if (z4) {
            return true;
        }
        return !z2 && !z3;
    }

    @DexIgnore
    public final p2 a(View view) {
        if (view instanceof p2) {
            return (p2) view;
        }
        if (view instanceof Toolbar) {
            return ((Toolbar) view).getWrapper();
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Can't make a decor toolbar out of ");
        sb.append(view != null ? view.getClass().getSimpleName() : "null");
        throw new IllegalStateException(sb.toString());
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void b() {
    }

    @DexIgnore
    public final void b(View view) {
        ActionBarOverlayLayout actionBarOverlayLayout = (ActionBarOverlayLayout) view.findViewById(d0.decor_content_parent);
        this.d = actionBarOverlayLayout;
        if (actionBarOverlayLayout != null) {
            actionBarOverlayLayout.setActionBarVisibilityCallback(this);
        }
        this.f = a(view.findViewById(d0.action_bar));
        this.g = (ActionBarContextView) view.findViewById(d0.action_context_bar);
        ActionBarContainer actionBarContainer = (ActionBarContainer) view.findViewById(d0.action_bar_container);
        this.e = actionBarContainer;
        p2 p2Var = this.f;
        if (p2Var == null || this.g == null || actionBarContainer == null) {
            throw new IllegalStateException(s0.class.getSimpleName() + " can only be used with a compatible window decor layout");
        }
        this.a = p2Var.getContext();
        boolean z2 = (this.f.l() & 4) != 0;
        if (z2) {
            this.j = true;
        }
        b1 a2 = b1.a(this.a);
        k(a2.a() || z2);
        i(a2.f());
        TypedArray obtainStyledAttributes = this.a.obtainStyledAttributes(null, h0.ActionBar, y.actionBarStyle, 0);
        if (obtainStyledAttributes.getBoolean(h0.ActionBar_hideOnContentScroll, false)) {
            j(true);
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(h0.ActionBar_elevation, 0);
        if (dimensionPixelSize != 0) {
            a((float) dimensionPixelSize);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void c() {
        if (!this.t) {
            this.t = true;
            l(true);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void d(boolean z2) {
        a(z2 ? 4 : 0, 4);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void e(boolean z2) {
        h1 h1Var;
        this.x = z2;
        if (!z2 && (h1Var = this.w) != null) {
            h1Var.a();
        }
    }

    @DexIgnore
    public void f(boolean z2) {
        ha haVar;
        ha haVar2;
        if (z2) {
            p();
        } else {
            n();
        }
        if (o()) {
            if (z2) {
                haVar = this.f.a(4, 100);
                haVar2 = this.g.a(0, 200);
            } else {
                haVar2 = this.f.a(0, 200);
                haVar = this.g.a(8, 100);
            }
            h1 h1Var = new h1();
            h1Var.a(haVar, haVar2);
            h1Var.c();
        } else if (z2) {
            this.f.setVisibility(4);
            this.g.setVisibility(0);
        } else {
            this.f.setVisibility(0);
            this.g.setVisibility(8);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public int g() {
        return this.f.l();
    }

    @DexIgnore
    public void h(boolean z2) {
        View view;
        View view2;
        h1 h1Var = this.w;
        if (h1Var != null) {
            h1Var.a();
        }
        this.e.setVisibility(0);
        if (this.q != 0 || (!this.x && !z2)) {
            this.e.setAlpha(1.0f);
            this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            if (this.r && (view = this.h) != null) {
                view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.A.b(null);
        } else {
            this.e.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float f2 = (float) (-this.e.getHeight());
            if (z2) {
                int[] iArr = {0, 0};
                this.e.getLocationInWindow(iArr);
                f2 -= (float) iArr[1];
            }
            this.e.setTranslationY(f2);
            h1 h1Var2 = new h1();
            ha a2 = da.a(this.e);
            a2.b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.a(this.B);
            h1Var2.a(a2);
            if (this.r && (view2 = this.h) != null) {
                view2.setTranslationY(f2);
                ha a3 = da.a(this.h);
                a3.b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                h1Var2.a(a3);
            }
            h1Var2.a(D);
            h1Var2.a(250);
            h1Var2.a(this.A);
            this.w = h1Var2;
            h1Var2.c();
        }
        ActionBarOverlayLayout actionBarOverlayLayout = this.d;
        if (actionBarOverlayLayout != null) {
            da.L(actionBarOverlayLayout);
        }
    }

    @DexIgnore
    public final void i(boolean z2) {
        this.p = z2;
        if (!z2) {
            this.f.a((z2) null);
            this.e.setTabContainer(this.i);
        } else {
            this.e.setTabContainer(null);
            this.f.a(this.i);
        }
        boolean z3 = true;
        boolean z4 = m() == 2;
        z2 z2Var = this.i;
        if (z2Var != null) {
            if (z4) {
                z2Var.setVisibility(0);
                ActionBarOverlayLayout actionBarOverlayLayout = this.d;
                if (actionBarOverlayLayout != null) {
                    da.L(actionBarOverlayLayout);
                }
            } else {
                z2Var.setVisibility(8);
            }
        }
        this.f.b(!this.p && z4);
        ActionBarOverlayLayout actionBarOverlayLayout2 = this.d;
        if (this.p || !z4) {
            z3 = false;
        }
        actionBarOverlayLayout2.setHasNonEmbeddedTabs(z3);
    }

    @DexIgnore
    public void j(boolean z2) {
        if (!z2 || this.d.j()) {
            this.y = z2;
            this.d.setHideOnContentScrollEnabled(z2);
            return;
        }
        throw new IllegalStateException("Action bar must be in overlay mode (Window.FEATURE_OVERLAY_ACTION_BAR) to enable hide on content scroll");
    }

    @DexIgnore
    public void k(boolean z2) {
        this.f.a(z2);
    }

    @DexIgnore
    public void l() {
        ActionMode.Callback callback = this.m;
        if (callback != null) {
            callback.a(this.l);
            this.l = null;
            this.m = null;
        }
    }

    @DexIgnore
    public int m() {
        return this.f.j();
    }

    @DexIgnore
    public final void n() {
        if (this.u) {
            this.u = false;
            ActionBarOverlayLayout actionBarOverlayLayout = this.d;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(false);
            }
            l(false);
        }
    }

    @DexIgnore
    public final boolean o() {
        return da.G(this.e);
    }

    @DexIgnore
    public final void p() {
        if (!this.u) {
            this.u = true;
            ActionBarOverlayLayout actionBarOverlayLayout = this.d;
            if (actionBarOverlayLayout != null) {
                actionBarOverlayLayout.setShowingForActionMode(true);
            }
            l(false);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void d() {
        h1 h1Var = this.w;
        if (h1Var != null) {
            h1Var.a();
            this.w = null;
        }
    }

    @DexIgnore
    public void g(boolean z2) {
        View view;
        h1 h1Var = this.w;
        if (h1Var != null) {
            h1Var.a();
        }
        if (this.q != 0 || (!this.x && !z2)) {
            this.z.b(null);
            return;
        }
        this.e.setAlpha(1.0f);
        this.e.setTransitioning(true);
        h1 h1Var2 = new h1();
        float f2 = (float) (-this.e.getHeight());
        if (z2) {
            int[] iArr = {0, 0};
            this.e.getLocationInWindow(iArr);
            f2 -= (float) iArr[1];
        }
        ha a2 = da.a(this.e);
        a2.b(f2);
        a2.a(this.B);
        h1Var2.a(a2);
        if (this.r && (view = this.h) != null) {
            ha a3 = da.a(view);
            a3.b(f2);
            h1Var2.a(a3);
        }
        h1Var2.a(C);
        h1Var2.a(250);
        h1Var2.a(this.z);
        this.w = h1Var2;
        h1Var2.c();
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void c(boolean z2) {
        if (!this.j) {
            d(z2);
        }
    }

    @DexIgnore
    public final void l(boolean z2) {
        if (a(this.s, this.t, this.u)) {
            if (!this.v) {
                this.v = true;
                h(z2);
            }
        } else if (this.v) {
            this.v = false;
            g(z2);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(float f2) {
        da.a(this.e, f2);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(Configuration configuration) {
        i(b1.a(this.a).f());
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void a(int i2) {
        this.q = i2;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void a(CharSequence charSequence) {
        this.f.setTitle(charSequence);
    }

    @DexIgnore
    public void a(int i2, int i3) {
        int l2 = this.f.l();
        if ((i3 & 4) != 0) {
            this.j = true;
        }
        this.f.a((i2 & i3) | ((~i3) & l2));
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public ActionMode a(ActionMode.Callback callback) {
        d dVar = this.k;
        if (dVar != null) {
            dVar.a();
        }
        this.d.setHideOnContentScrollEnabled(false);
        this.g.d();
        d dVar2 = new d(this.g.getContext(), callback);
        if (!dVar2.k()) {
            return null;
        }
        this.k = dVar2;
        dVar2.i();
        this.g.a(dVar2);
        f(true);
        this.g.sendAccessibilityEvent(32);
        return dVar2;
    }

    @DexIgnore
    public s0(Dialog dialog) {
        new ArrayList();
        b(dialog.getWindow().getDecorView());
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean f() {
        p2 p2Var = this.f;
        if (p2Var == null || !p2Var.h()) {
            return false;
        }
        this.f.collapseActionView();
        return true;
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void b(boolean z2) {
        if (z2 != this.n) {
            this.n = z2;
            int size = this.o.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.o.get(i2).a(z2);
            }
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public void b(CharSequence charSequence) {
        this.f.setWindowTitle(charSequence);
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void a(boolean z2) {
        this.r = z2;
    }

    @DexIgnore
    @Override // androidx.appcompat.widget.ActionBarOverlayLayout.d
    public void a() {
        if (this.t) {
            this.t = false;
            l(true);
        }
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public boolean a(int i2, KeyEvent keyEvent) {
        Menu c2;
        d dVar = this.k;
        if (dVar == null || (c2 = dVar.c()) == null) {
            return false;
        }
        boolean z2 = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z2 = false;
        }
        c2.setQwertyMode(z2);
        return c2.performShortcut(i2, keyEvent, 0);
    }

    @DexIgnore
    @Override // androidx.appcompat.app.ActionBar
    public Context h() {
        if (this.b == null) {
            TypedValue typedValue = new TypedValue();
            this.a.getTheme().resolveAttribute(y.actionBarWidgetTheme, typedValue, true);
            int i2 = typedValue.resourceId;
            if (i2 != 0) {
                this.b = new ContextThemeWrapper(this.a, i2);
            } else {
                this.b = this.a;
            }
        }
        return this.b;
    }
}
