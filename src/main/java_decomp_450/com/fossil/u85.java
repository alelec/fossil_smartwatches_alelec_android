package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class u85 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ FlexibleButton t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ View v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public u85(Object obj, View view, int i, View view2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleButton flexibleButton, ImageView imageView, View view3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = view2;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = flexibleButton;
        this.u = imageView;
        this.v = view3;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
    }

    @DexIgnore
    public static u85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static u85 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (u85) ViewDataBinding.a(layoutInflater, 2131558666, viewGroup, z, obj);
    }
}
