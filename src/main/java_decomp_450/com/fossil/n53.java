package com.fossil;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.location.LocationResult;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n53 implements Parcelable.Creator<LocationResult> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationResult createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        List<Location> list = LocationResult.b;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            if (j72.a(a) != 1) {
                j72.v(parcel, a);
            } else {
                list = j72.c(parcel, a, Location.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new LocationResult(list);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ LocationResult[] newArray(int i) {
        return new LocationResult[i];
    }
}
