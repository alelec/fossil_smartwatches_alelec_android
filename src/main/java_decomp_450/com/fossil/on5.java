package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class on5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public b(MFUser mFUser) {
            ee7.b(mFUser, "mfUser");
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public c(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser", f = "UpdateUser.kt", l = {22, 24}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ on5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(on5 on5, fb7 fb7) {
            super(fb7);
            this.this$0 = on5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = on5.class.getSimpleName();
        ee7.a((Object) simpleName, "UpdateUser::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public on5(UserRepository userRepository) {
        ee7.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.on5.b r9, com.fossil.fb7<java.lang.Object> r10) {
        /*
            r8 = this;
            boolean r0 = r10 instanceof com.fossil.on5.e
            if (r0 == 0) goto L_0x0013
            r0 = r10
            com.fossil.on5$e r0 = (com.fossil.on5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.on5$e r0 = new com.fossil.on5$e
            r0.<init>(r8, r10)
        L_0x0018:
            java.lang.Object r10 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 0
            java.lang.String r4 = ""
            r5 = 2
            r6 = 1
            if (r2 == 0) goto L_0x0053
            if (r2 == r6) goto L_0x0047
            if (r2 != r5) goto L_0x003f
            java.lang.Object r9 = r0.L$3
            com.portfolio.platform.PortfolioApp r9 = (com.portfolio.platform.PortfolioApp) r9
            java.lang.Object r1 = r0.L$2
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            java.lang.Object r2 = r0.L$1
            com.fossil.on5$b r2 = (com.fossil.on5.b) r2
            java.lang.Object r0 = r0.L$0
            com.fossil.on5 r0 = (com.fossil.on5) r0
            com.fossil.t87.a(r10)
            goto L_0x00a7
        L_0x003f:
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
            java.lang.String r10 = "call to 'resume' before 'invoke' with coroutine"
            r9.<init>(r10)
            throw r9
        L_0x0047:
            java.lang.Object r9 = r0.L$1
            com.fossil.on5$b r9 = (com.fossil.on5.b) r9
            java.lang.Object r2 = r0.L$0
            com.fossil.on5 r2 = (com.fossil.on5) r2
            com.fossil.t87.a(r10)
            goto L_0x0081
        L_0x0053:
            com.fossil.t87.a(r10)
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()
            java.lang.String r2 = com.fossil.on5.e
            java.lang.String r7 = "running UseCase"
            r10.d(r2, r7)
            if (r9 != 0) goto L_0x006d
            com.fossil.on5$c r9 = new com.fossil.on5$c
            r10 = 600(0x258, float:8.41E-43)
            r9.<init>(r10, r4)
            return r9
        L_0x006d:
            com.portfolio.platform.data.source.UserRepository r10 = r8.d
            com.portfolio.platform.data.model.MFUser r2 = r9.a()
            r0.L$0 = r8
            r0.L$1 = r9
            r0.label = r6
            java.lang.Object r10 = r10.updateUser(r2, r6, r0)
            if (r10 != r1) goto L_0x0080
            return r1
        L_0x0080:
            r2 = r8
        L_0x0081:
            com.fossil.zi5 r10 = (com.fossil.zi5) r10
            boolean r6 = r10 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x00c0
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r0.L$0 = r2
            r0.L$1 = r9
            r0.L$2 = r10
            r0.L$3 = r4
            r0.label = r5
            java.lang.Object r9 = r6.d(r0)
            if (r9 != r1) goto L_0x00a4
            return r1
        L_0x00a4:
            r1 = r10
            r10 = r9
            r9 = r4
        L_0x00a7:
            if (r10 == 0) goto L_0x00bc
            com.misfit.frameworks.buttonservice.model.UserProfile r10 = (com.misfit.frameworks.buttonservice.model.UserProfile) r10
            r9.a(r10)
            com.fossil.on5$d r9 = new com.fossil.on5$d
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r10 = r1.a()
            com.portfolio.platform.data.model.MFUser r10 = (com.portfolio.platform.data.model.MFUser) r10
            r9.<init>(r10)
            goto L_0x00ea
        L_0x00bc:
            com.fossil.ee7.a()
            throw r3
        L_0x00c0:
            boolean r9 = r10 instanceof com.fossil.yi5
            if (r9 == 0) goto L_0x00eb
            com.fossil.on5$c r9 = new com.fossil.on5$c
            com.fossil.yi5 r10 = (com.fossil.yi5) r10
            int r0 = r10.a()
            com.portfolio.platform.data.model.ServerError r1 = r10.c()
            if (r1 == 0) goto L_0x00da
            java.lang.String r1 = r1.getUserMessage()
            if (r1 == 0) goto L_0x00da
            r3 = r1
            goto L_0x00e4
        L_0x00da:
            com.portfolio.platform.data.model.ServerError r10 = r10.c()
            if (r10 == 0) goto L_0x00e4
            java.lang.String r3 = r10.getMessage()
        L_0x00e4:
            if (r3 == 0) goto L_0x00e7
            r4 = r3
        L_0x00e7:
            r9.<init>(r0, r4)
        L_0x00ea:
            return r9
        L_0x00eb:
            com.fossil.p87 r9 = new com.fossil.p87
            r9.<init>()
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.on5.a(com.fossil.on5$b, com.fossil.fb7):java.lang.Object");
    }
}
