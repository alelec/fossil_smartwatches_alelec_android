package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.nio.charset.Charset;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cw6 extends fl4<b, d, c> {
    @DexIgnore
    public /* final */ ch5 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ kb5 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public b(String str, kb5 kb5, String str2) {
            ee7.b(str, "aliasName");
            ee7.b(kb5, "scope");
            ee7.b(str2, "value");
            this.a = str;
            this.b = kb5;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final kb5 b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public c(int i, String str) {
            ee7.b(str, "errorMessage");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public cw6(ch5 ch5) {
        ee7.b(ch5, "mSharedPreferencesManager");
        this.d = ch5;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return "EncryptValueKeyStoreUseCase";
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Start encrypt ");
        sb.append(bVar != null ? bVar.c() : null);
        sb.append(" with alias ");
        sb.append(bVar != null ? bVar.a() : null);
        local.d("EncryptValueKeyStoreUseCase", sb.toString());
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                ix6 ix6 = ix6.b;
                if (bVar != null) {
                    SecretKey d2 = ix6.d(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(1, d2);
                    String c2 = bVar.c();
                    Charset charset = sg7.a;
                    if (c2 != null) {
                        byte[] bytes = c2.getBytes(charset);
                        ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        byte[] doFinal = instance.doFinal(bytes);
                        ee7.a((Object) instance, "cipher");
                        String encodeToString = Base64.encodeToString(instance.getIV(), 0);
                        String encodeToString2 = Base64.encodeToString(doFinal, 0);
                        this.d.a(bVar.a(), bVar.b(), encodeToString);
                        this.d.b(bVar.a(), bVar.b(), encodeToString2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("EncryptValueKeyStoreUseCase", "Encryption done iv " + encodeToString + " value " + encodeToString2);
                    } else {
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ix6 ix62 = ix6.b;
                if (bVar != null) {
                    KeyPair c3 = ix62.c(bVar.a());
                    if (c3 == null) {
                        c3 = ix6.b.a(bVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(1, c3.getPublic());
                    String c4 = bVar.c();
                    Charset charset2 = sg7.a;
                    if (c4 != null) {
                        byte[] bytes2 = c4.getBytes(charset2);
                        ee7.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                        String encodeToString3 = Base64.encodeToString(instance2.doFinal(bytes2), 0);
                        this.d.b(bVar.a(), bVar.b(), encodeToString3);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("EncryptValueKeyStoreUseCase", "Encryption done value " + encodeToString3);
                    } else {
                        throw new x87("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
            a(new d());
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("EncryptValueKeyStoreUseCase", "Exception when encrypt values " + e);
            a(new c(600, ""));
        }
        return new Object();
    }
}
