package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq3 extends i72 implements qp3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<aq3> CREATOR; // = new bq3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ List<pq3> b;

    @DexIgnore
    public aq3(String str, List<pq3> list) {
        this.a = str;
        this.b = list;
        a72.a((Object) str);
        a72.a(this.b);
    }

    @DexIgnore
    public final String e() {
        return this.a;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || aq3.class != obj.getClass()) {
            return false;
        }
        aq3 aq3 = (aq3) obj;
        String str = this.a;
        if (str == null ? aq3.a != null : !str.equals(aq3.a)) {
            return false;
        }
        List<pq3> list = this.b;
        List<pq3> list2 = aq3.b;
        return list == null ? list2 == null : list.equals(list2);
    }

    @DexIgnore
    public final int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = ((str != null ? str.hashCode() : 0) + 31) * 31;
        List<pq3> list = this.b;
        if (list != null) {
            i = list.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final String toString() {
        String str = this.a;
        String valueOf = String.valueOf(this.b);
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 18 + String.valueOf(valueOf).length());
        sb.append("CapabilityInfo{");
        sb.append(str);
        sb.append(", ");
        sb.append(valueOf);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, e(), false);
        k72.c(parcel, 3, this.b, false);
        k72.a(parcel, a2);
    }
}
