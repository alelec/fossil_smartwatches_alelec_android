package com.fossil;

import android.os.IBinder;
import android.os.IInterface;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class of2 implements IInterface {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public of2(IBinder iBinder, String str) {
        this.a = iBinder;
    }

    @DexIgnore
    public IBinder asBinder() {
        return this.a;
    }
}
