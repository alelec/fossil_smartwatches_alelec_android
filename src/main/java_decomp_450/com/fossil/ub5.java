package com.fossil;

import com.fossil.fitness.WorkoutMode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ub5 {
    UNKNOWN("UNKNOWN"),
    INDOOR("INDOOR"),
    OUTDOOR("OUTDOOR");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ub5 a(int i) {
            if (i == WorkoutMode.OUTDOOR.ordinal()) {
                return ub5.OUTDOOR;
            }
            if (i == WorkoutMode.INDOOR.ordinal()) {
                return ub5.INDOOR;
            }
            return ub5.INDOOR;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final WorkoutMode a(ub5 ub5) {
            if (ub5 != null) {
                int i = tb5.a[ub5.ordinal()];
                if (i == 1) {
                    return WorkoutMode.OUTDOOR;
                }
                if (i == 2) {
                    return WorkoutMode.INDOOR;
                }
            }
            return WorkoutMode.INDOOR;
        }
    }

    @DexIgnore
    public ub5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
