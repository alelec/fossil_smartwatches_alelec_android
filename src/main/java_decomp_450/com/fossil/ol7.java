package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ol7 {
    @DexIgnore
    public static final Object a(fb7<? super i97> fb7) {
        Object obj;
        ib7 context = fb7.getContext();
        a(context);
        fb7 a = mb7.a(fb7);
        if (!(a instanceof lj7)) {
            a = null;
        }
        lj7 lj7 = (lj7) a;
        if (lj7 != null) {
            if (lj7.g.b(context)) {
                lj7.a(context, i97.a);
            } else {
                nl7 nl7 = new nl7();
                lj7.a(context.plus(nl7), i97.a);
                if (nl7.a) {
                    obj = mj7.a(lj7) ? nb7.a() : i97.a;
                }
            }
            obj = nb7.a();
        } else {
            obj = i97.a;
        }
        if (obj == nb7.a()) {
            vb7.c(fb7);
        }
        return obj == nb7.a() ? obj : i97.a;
    }

    @DexIgnore
    public static final void a(ib7 ib7) {
        ik7 ik7 = (ik7) ib7.get(ik7.o);
        if (ik7 != null && !ik7.isActive()) {
            throw ik7.b();
        }
    }
}
