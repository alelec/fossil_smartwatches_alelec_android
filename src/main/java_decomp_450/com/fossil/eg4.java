package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg4 extends jg4 {
    @DexIgnore
    public dg4[] a;

    @DexIgnore
    public eg4() {
        a();
    }

    @DexIgnore
    public eg4 a() {
        this.a = dg4.b();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jg4
    public eg4 a(gg4 gg4) throws IOException {
        while (true) {
            int j = gg4.j();
            if (j == 0) {
                return this;
            }
            if (j == 10) {
                int a2 = lg4.a(gg4, 10);
                dg4[] dg4Arr = this.a;
                int length = dg4Arr == null ? 0 : dg4Arr.length;
                int i = a2 + length;
                dg4[] dg4Arr2 = new dg4[i];
                if (length != 0) {
                    System.arraycopy(this.a, 0, dg4Arr2, 0, length);
                }
                while (length < i - 1) {
                    dg4Arr2[length] = new dg4();
                    gg4.a(dg4Arr2[length]);
                    gg4.j();
                    length++;
                }
                dg4Arr2[length] = new dg4();
                gg4.a(dg4Arr2[length]);
                this.a = dg4Arr2;
            } else if (!lg4.b(gg4, j)) {
                return this;
            }
        }
    }
}
