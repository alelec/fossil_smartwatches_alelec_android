package com.fossil;

import com.facebook.internal.FileLruCache;
import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qa7<T> extends o97<T> implements RandomAccess {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ Object[] e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends n97<T> {
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ /* synthetic */ qa7 e;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public a(qa7 qa7) {
            this.e = qa7;
            this.c = qa7.size();
            this.d = qa7.c;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.qa7$a */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // com.fossil.n97
        public void a() {
            if (this.c == 0) {
                b();
                return;
            }
            b(this.e.e[this.d]);
            this.d = (this.d + 1) % this.e.b;
            this.c--;
        }
    }

    @DexIgnore
    public qa7(Object[] objArr, int i) {
        ee7.b(objArr, FileLruCache.BufferFile.FILE_NAME_PREFIX);
        this.e = objArr;
        boolean z = true;
        if (i >= 0) {
            if (i > this.e.length ? false : z) {
                this.b = this.e.length;
                this.d = i;
                return;
            }
            throw new IllegalArgumentException(("ring buffer filled size: " + i + " cannot be larger than the buffer size: " + this.e.length).toString());
        }
        throw new IllegalArgumentException(("ring buffer filled size should not be negative but it is " + i).toString());
    }

    @DexIgnore
    @Override // java.util.List, java.util.Collection, com.fossil.l97
    public final void add(T t) {
        if (!b()) {
            this.e[(this.c + size()) % this.b] = t;
            this.d = size() + 1;
            return;
        }
        throw new IllegalStateException("ring buffer is full");
    }

    @DexIgnore
    @Override // com.fossil.o97, java.util.List
    public T get(int i) {
        o97.a.a(i, size());
        return (T) this.e[(this.c + i) % this.b];
    }

    @DexIgnore
    @Override // com.fossil.o97, java.util.List, java.util.Collection, java.lang.Iterable
    public Iterator<T> iterator() {
        return new a(this);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r6v1, resolved type: T[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // java.util.List, java.util.Collection, com.fossil.l97
    public <T> T[] toArray(T[] tArr) {
        T[] tArr2;
        ee7.b(tArr, "array");
        if (tArr.length < size()) {
            tArr = (T[]) Arrays.copyOf(tArr, size());
            ee7.a((Object) tArr, "java.util.Arrays.copyOf(this, newSize)");
        }
        int size = size();
        int i = this.c;
        int i2 = 0;
        int i3 = 0;
        while (i3 < size && i < this.b) {
            tArr2[i3] = this.e[i];
            i3++;
            i++;
        }
        while (i3 < size) {
            tArr2[i3] = this.e[i2];
            i3++;
            i2++;
        }
        if (tArr2.length > size()) {
            tArr2[size()] = 0;
        }
        if (tArr2 != 0) {
            return tArr2;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // com.fossil.l97
    public int a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return size() == this.b;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.qa7<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r3v3, resolved type: java.lang.Object[] */
    /* JADX WARN: Multi-variable type inference failed */
    public final qa7<T> a(int i) {
        Object[] objArr;
        int i2 = this.b;
        int b2 = qf7.b(i2 + (i2 >> 1) + 1, i);
        if (this.c == 0) {
            objArr = Arrays.copyOf(this.e, b2);
            ee7.a((Object) objArr, "java.util.Arrays.copyOf(this, newSize)");
        } else {
            objArr = toArray(new Object[b2]);
        }
        return new qa7<>(objArr, size());
    }

    @DexIgnore
    public final void b(int i) {
        boolean z = true;
        if (i >= 0) {
            if (i > size()) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(("n shouldn't be greater than the buffer size: n = " + i + ", size = " + size()).toString());
            } else if (i > 0) {
                int i2 = this.c;
                int b2 = (i2 + i) % this.b;
                if (i2 > b2) {
                    s97.a(this.e, (Object) null, i2, this.b);
                    s97.a(this.e, (Object) null, 0, b2);
                } else {
                    s97.a(this.e, (Object) null, i2, b2);
                }
                this.c = b2;
                this.d = size() - i;
            }
        } else {
            throw new IllegalArgumentException(("n shouldn't be negative but it is " + i).toString());
        }
    }

    @DexIgnore
    public qa7(int i) {
        this(new Object[i], 0);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.qa7<T> */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: java.lang.Object[] */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.l97
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }
}
