package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q81 extends fe7 implements gd7<xp0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ec1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q81(ec1 ec1) {
        super(1);
        this.a = ec1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(xp0 xp0) {
        xp0 xp02 = xp0;
        gd7<? super xp0, i97> gd7 = this.a.C;
        if (gd7 != null) {
            gd7.invoke(xp02);
        }
        return i97.a;
    }
}
