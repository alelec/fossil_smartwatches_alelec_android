package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class it {

    @DexIgnore
    public interface a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.it$a$a")
        /* renamed from: com.fossil.it$a$a  reason: collision with other inner class name */
        public static final class C0082a {
            @DexIgnore
            public static void a(a aVar, Object obj) {
            }

            @DexIgnore
            public static void a(a aVar, Object obj, br brVar) {
                ee7.b(obj, "data");
                ee7.b(brVar, "source");
            }

            @DexIgnore
            public static void a(a aVar, Object obj, Throwable th) {
                ee7.b(th, "throwable");
            }

            @DexIgnore
            public static void b(a aVar, Object obj) {
                ee7.b(obj, "data");
            }
        }

        @DexIgnore
        void a(Object obj);

        @DexIgnore
        void a(Object obj, br brVar);

        @DexIgnore
        void a(Object obj, Throwable th);

        @DexIgnore
        void onCancel(Object obj);
    }

    @DexIgnore
    public it() {
    }

    @DexIgnore
    public abstract List<String> a();

    @DexIgnore
    public abstract boolean b();

    @DexIgnore
    public abstract boolean c();

    @DexIgnore
    public abstract Bitmap.Config d();

    @DexIgnore
    public abstract ColorSpace e();

    @DexIgnore
    public abstract fr f();

    @DexIgnore
    public abstract dt g();

    @DexIgnore
    public abstract ti7 h();

    @DexIgnore
    public abstract Drawable i();

    @DexIgnore
    public abstract Drawable j();

    @DexIgnore
    public abstract fo7 k();

    @DexIgnore
    public abstract String l();

    @DexIgnore
    public abstract a m();

    @DexIgnore
    public abstract dt n();

    @DexIgnore
    public abstract dt o();

    @DexIgnore
    public abstract ht p();

    @DexIgnore
    public abstract Drawable q();

    @DexIgnore
    public abstract pt r();

    @DexIgnore
    public abstract qt s();

    @DexIgnore
    public abstract st t();

    @DexIgnore
    public abstract vt u();

    @DexIgnore
    public abstract List<yt> v();

    @DexIgnore
    public abstract zt w();

    @DexIgnore
    public /* synthetic */ it(zd7 zd7) {
        this();
    }
}
