package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class un2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle e;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public un2(sn2 sn2, Bundle bundle) {
        super(sn2);
        this.f = sn2;
        this.e = bundle;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.f.h.setConditionalUserProperty(this.e, ((sn2.a) this).a);
    }
}
