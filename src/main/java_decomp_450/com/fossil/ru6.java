package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ru6 implements Factory<pu6> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<fm5> b;
    @DexIgnore
    public /* final */ Provider<mm5> c;
    @DexIgnore
    public /* final */ Provider<ad5> d;
    @DexIgnore
    public /* final */ Provider<ch5> e;
    @DexIgnore
    public /* final */ Provider<hm5> f;
    @DexIgnore
    public /* final */ Provider<yl5> g;
    @DexIgnore
    public /* final */ Provider<ro4> h;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> i;

    @DexIgnore
    public ru6(Provider<DeviceRepository> provider, Provider<fm5> provider2, Provider<mm5> provider3, Provider<ad5> provider4, Provider<ch5> provider5, Provider<hm5> provider6, Provider<yl5> provider7, Provider<ro4> provider8, Provider<PortfolioApp> provider9) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
    }

    @DexIgnore
    public static ru6 a(Provider<DeviceRepository> provider, Provider<fm5> provider2, Provider<mm5> provider3, Provider<ad5> provider4, Provider<ch5> provider5, Provider<hm5> provider6, Provider<yl5> provider7, Provider<ro4> provider8, Provider<PortfolioApp> provider9) {
        return new ru6(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9);
    }

    @DexIgnore
    public static pu6 a(DeviceRepository deviceRepository, fm5 fm5, mm5 mm5, ad5 ad5, ch5 ch5, hm5 hm5, yl5 yl5, ro4 ro4, PortfolioApp portfolioApp) {
        return new pu6(deviceRepository, fm5, mm5, ad5, ch5, hm5, yl5, ro4, portfolioApp);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public pu6 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get());
    }
}
