package com.fossil;

import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class du4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ gd7 $onSafeClick;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gd7 gd7) {
            super(1);
            this.$onSafeClick = gd7;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.$onSafeClick.invoke(view);
        }
    }

    @DexIgnore
    public static final void a(View view, gd7<? super View, i97> gd7) {
        ee7.b(view, "$this$setOnSafeClickListener");
        ee7.b(gd7, "onSafeClick");
        view.setOnClickListener(new cu4(0, new a(gd7), 1, null));
    }
}
