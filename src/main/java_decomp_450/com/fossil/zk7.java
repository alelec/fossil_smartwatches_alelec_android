package com.fossil;

import com.fossil.s87;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zk7<T> extends ok7<pk7> {
    @DexIgnore
    public /* final */ bi7<T> e;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.bi7<? super T> */
    /* JADX WARN: Multi-variable type inference failed */
    public zk7(pk7 pk7, bi7<? super T> bi7) {
        super(pk7);
        this.e = bi7;
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void b(Throwable th) {
        Object h = ((pk7) ((ok7) this).d).h();
        if (dj7.a() && !(!(h instanceof dk7))) {
            throw new AssertionError();
        } else if (h instanceof li7) {
            bi7<T> bi7 = this.e;
            Throwable th2 = ((li7) h).a;
            s87.a aVar = s87.Companion;
            bi7.resumeWith(s87.m60constructorimpl(t87.a(th2)));
        } else {
            bi7<T> bi72 = this.e;
            Object b = qk7.b(h);
            s87.a aVar2 = s87.Companion;
            bi72.resumeWith(s87.m60constructorimpl(b));
        }
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        b(th);
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public String toString() {
        return "ResumeAwaitOnCompletion[" + this.e + ']';
    }
}
