package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jd1<T> extends y71<T> {
    @DexIgnore
    public jd1() {
        super(new r60((byte) 1, (byte) 0));
    }

    @DexIgnore
    @Override // com.fossil.y71
    public byte[] a(short s, T t) {
        byte[] a = a(t);
        byte[] array = ByteBuffer.allocate(a.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put(((y71) this).a.getMajor()).put(((y71) this).a.getMinor()).putInt(0).putInt(a.length).put(a).putInt((int) ik1.a.a(a, ng1.CRC32)).array();
        ee7.a((Object) array, "result.array()");
        return array;
    }
}
