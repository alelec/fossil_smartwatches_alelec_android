package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSessionUpdateWrapper;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh6 extends he {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public WorkoutSession a;
    @DexIgnore
    public WorkoutSession b;
    @DexIgnore
    public MutableLiveData<b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutSessionRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public List<cc5> a;
        @DexIgnore
        public WorkoutSession b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public r87<Integer, String> d;
        @DexIgnore
        public Boolean e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public b(List<cc5> list, WorkoutSession workoutSession, Boolean bool, r87<Integer, String> r87, Boolean bool2, boolean z) {
            this.a = list;
            this.b = workoutSession;
            this.c = bool;
            this.d = r87;
            this.e = bool2;
            this.f = z;
        }

        @DexIgnore
        public final boolean a() {
            return this.f;
        }

        @DexIgnore
        public final r87<Integer, String> b() {
            return this.d;
        }

        @DexIgnore
        public final Boolean c() {
            return this.e;
        }

        @DexIgnore
        public final WorkoutSession d() {
            return this.b;
        }

        @DexIgnore
        public final List<cc5> e() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return ee7.a(this.a, bVar.a) && ee7.a(this.b, bVar.b) && ee7.a(this.c, bVar.c) && ee7.a(this.d, bVar.d) && ee7.a(this.e, bVar.e) && this.f == bVar.f;
        }

        @DexIgnore
        public final Boolean f() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            List<cc5> list = this.a;
            int i = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            WorkoutSession workoutSession = this.b;
            int hashCode2 = (hashCode + (workoutSession != null ? workoutSession.hashCode() : 0)) * 31;
            Boolean bool = this.c;
            int hashCode3 = (hashCode2 + (bool != null ? bool.hashCode() : 0)) * 31;
            r87<Integer, String> r87 = this.d;
            int hashCode4 = (hashCode3 + (r87 != null ? r87.hashCode() : 0)) * 31;
            Boolean bool2 = this.e;
            if (bool2 != null) {
                i = bool2.hashCode();
            }
            int i2 = (hashCode4 + i) * 31;
            boolean z = this.f;
            if (z) {
                z = true;
            }
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return i2 + i3;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(workoutWrapperTypes=" + this.a + ", workoutSession=" + this.b + ", isWorkoutSessionChanged=" + this.c + ", showServerError=" + this.d + ", showSuccess=" + this.e + ", showLoading=" + this.f + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1", f = "WorkoutEditViewModel.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $workoutSessionId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutEditViewModel$start$1$2", f = "WorkoutEditViewModel.kt", l = {41}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                bh6 bh6;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    bh6 bh62 = this.this$0.this$0;
                    WorkoutSessionRepository c = bh62.d;
                    String str = this.this$0.$workoutSessionId;
                    this.L$0 = yi7;
                    this.L$1 = bh62;
                    this.label = 1;
                    obj2 = c.getWorkoutSessionById(str, this);
                    if (obj2 == a) {
                        return a;
                    }
                    bh6 = bh62;
                } else if (i == 1) {
                    bh6 = (bh6) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                bh6.a = (WorkoutSession) obj2;
                bh6 bh63 = this.this$0.this$0;
                WorkoutSession a2 = bh63.a;
                bh63.b = a2 != null ? WorkoutSession.copy$default(a2, null, null, null, null, null, null, null, null, null, null, null, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null, 16777215, null) : null;
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(bh6 bh6, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = bh6;
            this.$workoutSessionId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$workoutSessionId, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                bh6 bh6 = this.this$0;
                cc5[] values = cc5.values();
                ArrayList arrayList = new ArrayList();
                int length = values.length;
                for (int i2 = 0; i2 < length; i2++) {
                    cc5 cc5 = values[i2];
                    if (pb7.a(cc5 != cc5.UNKNOWN).booleanValue()) {
                        arrayList.add(cc5);
                    }
                }
                bh6.a(bh6, ea7.d((Collection) arrayList), null, null, null, null, true, 30, null);
                ti7 a3 = qj7.a();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(a3, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            bh6 bh62 = this.this$0;
            bh6.a(bh62, null, bh62.b, null, null, null, false, 29, null);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ bh6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    WorkoutSession b = this.this$0.this$0.b;
                    if (b != null) {
                        WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper = new WorkoutSessionUpdateWrapper(b);
                        WorkoutSessionRepository c = this.this$0.this$0.d;
                        List<WorkoutSessionUpdateWrapper> d = ea7.d((Collection) w97.a((Object[]) new WorkoutSessionUpdateWrapper[]{workoutSessionUpdateWrapper}));
                        this.L$0 = yi7;
                        this.L$1 = workoutSessionUpdateWrapper;
                        this.label = 1;
                        obj2 = c.updateWorkoutSession(d, this);
                        if (obj2 == a) {
                            return a;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i == 1) {
                    WorkoutSessionUpdateWrapper workoutSessionUpdateWrapper2 = (WorkoutSessionUpdateWrapper) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ko4 ko4 = (ko4) obj2;
                if (ko4.c() != null) {
                    bh6.a(this.this$0.this$0, null, null, null, null, pb7.a(true), false, 15, null);
                } else {
                    ServerError a2 = ko4.a();
                    if (a2 != null) {
                        bh6.a(this.this$0.this$0, null, null, null, new r87(a2.getCode(), ""), pb7.a(false), false, 7, null);
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(fb7 fb7, bh6 bh6) {
            super(2, fb7);
            this.this$0 = bh6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(fb7, this.this$0);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = bh6.class.getSimpleName();
        ee7.a((Object) simpleName, "WorkoutEditViewModel::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public bh6(WorkoutSessionRepository workoutSessionRepository) {
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.d = workoutSessionRepository;
    }

    @DexIgnore
    public final Long d() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getDate().getTime());
        }
        return null;
    }

    @DexIgnore
    public final Long e() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedEndTime().getMillis() - workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final Long f() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            return Long.valueOf(workoutSession.getEditedStartTime().getMillis());
        }
        return null;
    }

    @DexIgnore
    public final boolean g() {
        WorkoutSession workoutSession = this.a;
        if (workoutSession == null || this.b == null) {
            return false;
        }
        Long l = null;
        if (workoutSession != null) {
            ac5 editedType = workoutSession.getEditedType();
            Integer valueOf = editedType != null ? Integer.valueOf(editedType.ordinal()) : null;
            WorkoutSession workoutSession2 = this.b;
            if (workoutSession2 != null) {
                ac5 editedType2 = workoutSession2.getEditedType();
                if (!(!ee7.a(valueOf, editedType2 != null ? Integer.valueOf(editedType2.ordinal()) : null))) {
                    WorkoutSession workoutSession3 = this.a;
                    if (workoutSession3 != null) {
                        ub5 editedMode = workoutSession3.getEditedMode();
                        Integer valueOf2 = editedMode != null ? Integer.valueOf(editedMode.ordinal()) : null;
                        WorkoutSession workoutSession4 = this.b;
                        if (workoutSession4 != null) {
                            ub5 editedMode2 = workoutSession4.getEditedMode();
                            if (!(!ee7.a(valueOf2, editedMode2 != null ? Integer.valueOf(editedMode2.ordinal()) : null))) {
                                WorkoutSession workoutSession5 = this.a;
                                if (workoutSession5 != null) {
                                    DateTime editedStartTime = workoutSession5.getEditedStartTime();
                                    if (editedStartTime != null) {
                                        long millis = editedStartTime.getMillis();
                                        WorkoutSession workoutSession6 = this.b;
                                        if (workoutSession6 != null) {
                                            DateTime editedStartTime2 = workoutSession6.getEditedStartTime();
                                            if (millis == (editedStartTime2 != null ? Long.valueOf(editedStartTime2.getMillis()) : null).longValue()) {
                                                WorkoutSession workoutSession7 = this.a;
                                                if (workoutSession7 != null) {
                                                    DateTime editedEndTime = workoutSession7.getEditedEndTime();
                                                    if (editedEndTime != null) {
                                                        long millis2 = editedEndTime.getMillis();
                                                        WorkoutSession workoutSession8 = this.b;
                                                        if (workoutSession8 != null) {
                                                            DateTime editedEndTime2 = workoutSession8.getEditedEndTime();
                                                            if (editedEndTime2 != null) {
                                                                l = Long.valueOf(editedEndTime2.getMillis());
                                                            }
                                                            if (millis2 != l.longValue()) {
                                                                return true;
                                                            }
                                                            return false;
                                                        }
                                                        ee7.a();
                                                        throw null;
                                                    }
                                                } else {
                                                    ee7.a();
                                                    throw null;
                                                }
                                            }
                                        } else {
                                            ee7.a();
                                            throw null;
                                        }
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
                return true;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void h() {
        if (this.b != null) {
            a(this, null, null, null, null, null, true, 31, null);
            ik7 unused = xh7.b(ie.a(this), null, null, new d(null, this), 3, null);
            a(this, null, null, null, null, null, true, 31, null);
        }
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.c;
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "workoutSessionId");
        ik7 unused = xh7.b(ie.a(this), null, null, new c(this, str, null), 3, null);
    }

    @DexIgnore
    public final void b(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onStartTimeChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            DateTime editedEndTime = workoutSession.getEditedEndTime();
            if (editedEndTime != null) {
                long millis = editedEndTime.getMillis();
                DateTime editedStartTime = workoutSession.getEditedStartTime();
                if (editedStartTime != null) {
                    long millis2 = (millis - editedStartTime.getMillis()) / ((long) 1000);
                    Calendar instance = Calendar.getInstance();
                    TimeZone timeZone = TimeZone.getDefault();
                    ee7.a((Object) timeZone, "timezone");
                    timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
                    ee7.a((Object) instance, "calendar");
                    instance.setTimeZone(timeZone);
                    DateTime editedStartTime2 = workoutSession.getEditedStartTime();
                    if (editedStartTime2 != null) {
                        instance.setTimeInMillis(editedStartTime2.getMillis());
                        instance.set(11, i);
                        instance.set(12, i2);
                        workoutSession.setEditedStartTime(new DateTime(instance.getTimeInMillis()));
                        instance.add(13, (int) millis2);
                        workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                        a(this, null, this.b, Boolean.valueOf(g()), null, null, false, 57, null);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(cc5 cc5) {
        ee7.b(cc5, "workoutWrapperTypes");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onWorkoutTypeChanged workoutTypes=" + cc5);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            workoutSession.setEditedType(cc5.Companion.c(cc5));
            workoutSession.setEditedMode(cc5.Companion.b(cc5));
            if (workoutSession.getEditedMode() == null) {
                ub5 mode = workoutSession.getMode();
                if (mode == null) {
                    mode = ub5.INDOOR;
                }
                workoutSession.setEditedMode(mode);
            }
            a(this, null, this.b, Boolean.valueOf(g()), null, null, false, 57, null);
        }
    }

    @DexIgnore
    public final void a(int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDurationChanged hour=" + i + " min=" + i2);
        WorkoutSession workoutSession = this.b;
        if (workoutSession != null) {
            Calendar instance = Calendar.getInstance();
            TimeZone timeZone = TimeZone.getDefault();
            ee7.a((Object) timeZone, "timezone");
            timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
            ee7.a((Object) instance, "calendar");
            instance.setTimeZone(timeZone);
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                instance.setTimeInMillis(editedStartTime.getMillis());
                instance.add(12, (i * 60) + i2);
                workoutSession.setEditedEndTime(new DateTime(instance.getTimeInMillis()));
                a(this, null, workoutSession, Boolean.valueOf(g()), null, null, false, 57, null);
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final r87<wb5, wb5> b() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        Calendar instance = Calendar.getInstance();
        TimeZone timeZone = TimeZone.getDefault();
        ee7.a((Object) timeZone, "timezone");
        timeZone.setRawOffset(workoutSession.getTimezoneOffsetInSecond() * 1000);
        ee7.a((Object) instance, "calendar");
        instance.setTimeZone(timeZone);
        DateTime editedStartTime = workoutSession.getEditedStartTime();
        if (editedStartTime != null) {
            instance.setTimeInMillis(editedStartTime.getMillis());
            return new r87<>(new wb5(0, 23, instance.get(11), 1.0f), new wb5(0, 59, instance.get(12), 1.0f));
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(double d2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onDistanceChanged distance=" + d2);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final void a(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = e;
        local.d(str, ".Inside onCaloriesChanged calories=" + i);
        WorkoutSession workoutSession = this.b;
    }

    @DexIgnore
    public final r87<wb5, wb5> a() {
        WorkoutSession workoutSession = this.b;
        if (workoutSession == null) {
            return null;
        }
        DateTime editedEndTime = workoutSession.getEditedEndTime();
        if (editedEndTime != null) {
            long millis = editedEndTime.getMillis();
            DateTime editedStartTime = workoutSession.getEditedStartTime();
            if (editedStartTime != null) {
                v87<Integer, Integer, Integer> d2 = zd5.d((int) ((millis - editedStartTime.getMillis()) / ((long) 1000)));
                ee7.a((Object) d2, "DateHelper.getTimeValues(duration.toInt())");
                Integer first = d2.getFirst();
                ee7.a((Object) first, "timeValues.first");
                wb5 wb5 = new wb5(0, 23, first.intValue(), 1.0f);
                Integer second = d2.getSecond();
                ee7.a((Object) second, "timeValues.second");
                return new r87<>(wb5, new wb5(0, 59, second.intValue(), 1.0f));
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.fossil.bh6 */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ void a(bh6 bh6, List list, WorkoutSession workoutSession, Boolean bool, r87 r87, Boolean bool2, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            list = null;
        }
        if ((i & 2) != 0) {
            workoutSession = null;
        }
        if ((i & 4) != 0) {
            bool = null;
        }
        if ((i & 8) != 0) {
            r87 = null;
        }
        if ((i & 16) != 0) {
            bool2 = null;
        }
        if ((i & 32) != 0) {
            z = false;
        }
        bh6.a(list, workoutSession, bool, r87, bool2, z);
    }

    @DexIgnore
    public final void a(List<cc5> list, WorkoutSession workoutSession, Boolean bool, r87<Integer, String> r87, Boolean bool2, boolean z) {
        this.c.a(new b(list, workoutSession, bool, r87, bool2, z));
    }
}
