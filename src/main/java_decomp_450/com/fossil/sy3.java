package com.fossil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sy3<E> extends tw3<E> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;

    @DexIgnore
    public sy3() {
        super(new LinkedHashMap());
    }

    @DexIgnore
    public static <E> sy3<E> create() {
        return new sy3<>();
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        int a = wz3.a(objectInputStream);
        setBackingMap(new LinkedHashMap());
        wz3.a(this, objectInputStream, a);
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        wz3.a(this, objectOutputStream);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.dz3, com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ int add(Object obj, int i) {
        return super.add(obj, i);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.ww3, java.util.Collection
    public /* bridge */ /* synthetic */ boolean addAll(Collection collection) {
        return super.addAll(collection);
    }

    @DexIgnore
    @Override // com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ void clear() {
        super.clear();
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    public /* bridge */ /* synthetic */ boolean contains(Object obj) {
        return super.contains(obj);
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ int count(Object obj) {
        return super.count(obj);
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    public /* bridge */ /* synthetic */ Set elementSet() {
        return super.elementSet();
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ Set entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public /* bridge */ /* synthetic */ boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public /* bridge */ /* synthetic */ int hashCode() {
        return super.hashCode();
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public /* bridge */ /* synthetic */ boolean isEmpty() {
        return super.isEmpty();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.tw3, com.fossil.ww3, java.util.Collection, java.lang.Iterable
    public /* bridge */ /* synthetic */ Iterator iterator() {
        return super.iterator();
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ int remove(Object obj, int i) {
        return super.remove(obj, i);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.ww3, java.util.Collection
    public /* bridge */ /* synthetic */ boolean removeAll(Collection collection) {
        return super.removeAll(collection);
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.ww3, java.util.Collection
    public /* bridge */ /* synthetic */ boolean retainAll(Collection collection) {
        return super.retainAll(collection);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.dz3, com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ int setCount(Object obj, int i) {
        return super.setCount(obj, i);
    }

    @DexIgnore
    @Override // com.fossil.tw3, com.fossil.ww3
    public /* bridge */ /* synthetic */ int size() {
        return super.size();
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public /* bridge */ /* synthetic */ String toString() {
        return super.toString();
    }

    @DexIgnore
    public sy3(int i) {
        super(yy3.b(i));
    }

    @DexIgnore
    public static <E> sy3<E> create(int i) {
        return new sy3<>(i);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.dz3, java.util.AbstractCollection, com.fossil.ww3, java.util.Collection
    public /* bridge */ /* synthetic */ boolean add(Object obj) {
        return super.add(obj);
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public /* bridge */ /* synthetic */ boolean remove(Object obj) {
        return super.remove(obj);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.dz3, com.fossil.ww3
    public /* bridge */ /* synthetic */ boolean setCount(Object obj, int i, int i2) {
        return super.setCount(obj, i, i2);
    }

    @DexIgnore
    public static <E> sy3<E> create(Iterable<? extends E> iterable) {
        sy3<E> create = create(ez3.b(iterable));
        py3.a((Collection) create, (Iterable) iterable);
        return create;
    }
}
