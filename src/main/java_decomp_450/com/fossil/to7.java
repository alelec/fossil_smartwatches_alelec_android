package com.fossil;

import com.facebook.GraphRequest;
import com.facebook.stetho.server.http.HttpHeaders;
import com.facebook.stetho.websocket.WebSocketHandler;
import com.fossil.fo7;
import com.fossil.vo7;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to7 implements Interceptor {
    @DexIgnore
    public /* final */ yo7 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements sr7 {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ ar7 b;
        @DexIgnore
        public /* final */ /* synthetic */ uo7 c;
        @DexIgnore
        public /* final */ /* synthetic */ zq7 d;

        @DexIgnore
        public a(to7 to7, ar7 ar7, uo7 uo7, zq7 zq7) {
            this.b = ar7;
            this.c = uo7;
            this.d = zq7;
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public long b(yq7 yq7, long j) throws IOException {
            try {
                long b2 = this.b.b(yq7, j);
                if (b2 == -1) {
                    if (!this.a) {
                        this.a = true;
                        this.d.close();
                    }
                    return -1;
                }
                yq7.a(this.d.buffer(), yq7.x() - b2, b2);
                this.d.j();
                return b2;
            } catch (IOException e) {
                if (!this.a) {
                    this.a = true;
                    this.c.abort();
                }
                throw e;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.sr7, java.lang.AutoCloseable
        public void close() throws IOException {
            if (!this.a && !ro7.a(this, 100, TimeUnit.MILLISECONDS)) {
                this.a = true;
                this.c.abort();
            }
            this.b.close();
        }

        @DexIgnore
        @Override // com.fossil.sr7
        public tr7 d() {
            return this.b.d();
        }
    }

    @DexIgnore
    public to7(yo7 yo7) {
        this.a = yo7;
    }

    @DexIgnore
    public static Response a(Response response) {
        if (response == null || response.a() == null) {
            return response;
        }
        Response.a q = response.q();
        q.a((mo7) null);
        return q.a();
    }

    @DexIgnore
    public static boolean b(String str) {
        return !WebSocketHandler.HEADER_CONNECTION.equalsIgnoreCase(str) && !"Keep-Alive".equalsIgnoreCase(str) && !"Proxy-Authenticate".equalsIgnoreCase(str) && !"Proxy-Authorization".equalsIgnoreCase(str) && !"TE".equalsIgnoreCase(str) && !"Trailers".equalsIgnoreCase(str) && !"Transfer-Encoding".equalsIgnoreCase(str) && !"Upgrade".equalsIgnoreCase(str);
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        yo7 yo7 = this.a;
        Response b = yo7 != null ? yo7.b(chain.c()) : null;
        vo7 c = new vo7.a(System.currentTimeMillis(), chain.c(), b).c();
        lo7 lo7 = c.a;
        Response response = c.b;
        yo7 yo72 = this.a;
        if (yo72 != null) {
            yo72.a(c);
        }
        if (b != null && response == null) {
            ro7.a(b.a());
        }
        if (lo7 == null && response == null) {
            Response.a aVar = new Response.a();
            aVar.a(chain.c());
            aVar.a(jo7.HTTP_1_1);
            aVar.a(504);
            aVar.a("Unsatisfiable Request (only-if-cached)");
            aVar.a(ro7.c);
            aVar.b(-1);
            aVar.a(System.currentTimeMillis());
            return aVar.a();
        } else if (lo7 == null) {
            Response.a q = response.q();
            q.a(a(response));
            return q.a();
        } else {
            try {
                Response a2 = chain.a(lo7);
                if (a2 == null && b != null) {
                }
                if (response != null) {
                    if (a2.e() == 304) {
                        Response.a q2 = response.q();
                        q2.a(a(response.k(), a2.k()));
                        q2.b(a2.y());
                        q2.a(a2.w());
                        q2.a(a(response));
                        q2.c(a(a2));
                        Response a3 = q2.a();
                        a2.a().close();
                        this.a.a();
                        this.a.a(response, a3);
                        return a3;
                    }
                    ro7.a(response.a());
                }
                Response.a q3 = a2.q();
                q3.a(a(response));
                q3.c(a(a2));
                Response a4 = q3.a();
                if (this.a != null) {
                    if (kp7.b(a4) && vo7.a(a4, lo7)) {
                        return a(this.a.a(a4), a4);
                    }
                    if (lp7.a(lo7.e())) {
                        try {
                            this.a.a(lo7);
                        } catch (IOException unused) {
                        }
                    }
                }
                return a4;
            } finally {
                if (b != null) {
                    ro7.a(b.a());
                }
            }
        }
    }

    @DexIgnore
    public final Response a(uo7 uo7, Response response) throws IOException {
        qr7 body;
        if (uo7 == null || (body = uo7.body()) == null) {
            return response;
        }
        a aVar = new a(this, response.a().source(), uo7, ir7.a(body));
        String b = response.b("Content-Type");
        long contentLength = response.a().contentLength();
        Response.a q = response.q();
        q.a(new np7(b, contentLength, ir7.a(aVar)));
        return q.a();
    }

    @DexIgnore
    public static fo7 a(fo7 fo7, fo7 fo72) {
        fo7.a aVar = new fo7.a();
        int b = fo7.b();
        for (int i = 0; i < b; i++) {
            String a2 = fo7.a(i);
            String b2 = fo7.b(i);
            if ((!"Warning".equalsIgnoreCase(a2) || !b2.startsWith("1")) && (a(a2) || !b(a2) || fo72.a(a2) == null)) {
                po7.a.a(aVar, a2, b2);
            }
        }
        int b3 = fo72.b();
        for (int i2 = 0; i2 < b3; i2++) {
            String a3 = fo72.a(i2);
            if (!a(a3) && b(a3)) {
                po7.a.a(aVar, a3, fo72.b(i2));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static boolean a(String str) {
        return HttpHeaders.CONTENT_LENGTH.equalsIgnoreCase(str) || GraphRequest.CONTENT_ENCODING_HEADER.equalsIgnoreCase(str) || "Content-Type".equalsIgnoreCase(str);
    }
}
