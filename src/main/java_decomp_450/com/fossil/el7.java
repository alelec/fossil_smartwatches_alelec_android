package com.fossil;

import com.fossil.ib7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface el7<S> extends ib7.b {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <S, E extends ib7.b> E a(el7<S> el7, ib7.c<E> cVar) {
            return (E) ib7.b.a.a(el7, cVar);
        }

        @DexIgnore
        public static <S> ib7 a(el7<S> el7, ib7 ib7) {
            return ib7.b.a.a(el7, ib7);
        }

        @DexIgnore
        public static <S, R> R a(el7<S> el7, R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
            return (R) ib7.b.a.a(el7, r, kd7);
        }

        @DexIgnore
        public static <S> ib7 b(el7<S> el7, ib7.c<?> cVar) {
            return ib7.b.a.b(el7, cVar);
        }
    }

    @DexIgnore
    S a(ib7 ib7);

    @DexIgnore
    void a(ib7 ib7, S s);
}
