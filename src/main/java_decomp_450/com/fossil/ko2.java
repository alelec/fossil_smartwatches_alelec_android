package com.fossil;

import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ko2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ int e; // = 5;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Object g;
    @DexIgnore
    public /* final */ /* synthetic */ Object h;
    @DexIgnore
    public /* final */ /* synthetic */ Object i;
    @DexIgnore
    public /* final */ /* synthetic */ sn2 j;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ko2(sn2 sn2, boolean z, int i2, String str, Object obj, Object obj2, Object obj3) {
        super(false);
        this.j = sn2;
        this.f = str;
        this.g = obj;
        this.h = null;
        this.i = null;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        this.j.h.logHealthData(this.e, this.f, cb2.a(this.g), cb2.a(this.h), cb2.a(this.i));
    }
}
