package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.b46;
import com.fossil.zl4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u36 extends go5 implements zl4.b {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<my4> f;
    @DexIgnore
    public b46 g;
    @DexIgnore
    public rj4 h;
    @DexIgnore
    public zl4 i;
    @DexIgnore
    public Boolean j; // = false;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return u36.q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final u36 a(AddressWrapper addressWrapper, ArrayList<String> arrayList, boolean z) {
            u36 u36 = new u36();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_BUNDLE_IS_MAP_RESULT", z);
            u36.setArguments(bundle);
            return u36;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ my4 b;

        @DexIgnore
        public b(View view, my4 my4) {
            this.a = view;
            this.b = my4;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.z.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                my4 my4 = this.b;
                ee7.a((Object) my4, "binding");
                my4.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = u36.r.a();
                local.d(a2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = this.b.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "binding.autocompletePlaces");
                flexibleAutoCompleteTextView.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public c(u36 u36, my4 my4) {
            this.a = u36;
        }

        @DexIgnore
        @Override // android.widget.AdapterView.OnItemClickListener
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            zl4 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText(null);
                ee7.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = u36.r.a();
                local.i(a3, "Autocomplete item selected: " + ((Object) fullText));
                b46 b = u36.b(this.a);
                String spannableString = fullText.toString();
                ee7.a((Object) spannableString, "primaryText.toString()");
                if (spannableString != null) {
                    String obj = nh7.d((CharSequence) spannableString).toString();
                    String placeId = item.getPlaceId();
                    zl4 a4 = this.a.i;
                    if (a4 != null) {
                        b.a(obj, placeId, a4.b());
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ my4 a;

        @DexIgnore
        public d(u36 u36, my4 my4) {
            this.a = my4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.a.t;
            ee7.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public e(u36 u36, my4 my4) {
            this.a = u36;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            ee7.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.e1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(u36 u36) {
            this.a = u36;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            b46 b = u36.b(this.a);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = nh7.d((CharSequence) valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    ee7.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    b.a(upperCase);
                    return;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            throw new x87("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public g(u36 u36) {
            this.a = u36;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.h(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public h(u36 u36) {
            this.a = u36;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (u36.b(this.a).c()) {
                bx6 bx6 = bx6.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                ee7.a((Object) childFragmentManager, "childFragmentManager");
                bx6.w(childFragmentManager);
            } else if (u36.b(this.a).d()) {
                this.a.h(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;
        @DexIgnore
        public /* final */ /* synthetic */ my4 b;

        @DexIgnore
        public i(u36 u36, my4 my4) {
            this.a = u36;
            this.b = my4;
        }

        @DexIgnore
        public final void onClick(View view) {
            b46.a(u36.b(this.a), null, null, null, 7, null);
            this.b.q.setText("");
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public j(u36 u36) {
            this.a = u36;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            u36.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;

        @DexIgnore
        public k(u36 u36) {
            this.a = u36;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.y;
            u36 u36 = this.a;
            AddressWrapper a2 = u36.b(u36).a();
            double d = 0.0d;
            double lat = a2 != null ? a2.getLat() : 0.0d;
            AddressWrapper a3 = u36.b(this.a).a();
            if (a3 != null) {
                d = a3.getLng();
            }
            AddressWrapper a4 = u36.b(this.a).a();
            if (a4 == null || (str = a4.getAddress()) == null) {
                str = "";
            }
            aVar.a(u36, lat, d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<b46.b> {
        @DexIgnore
        public /* final */ /* synthetic */ u36 a;
        @DexIgnore
        public /* final */ /* synthetic */ my4 b;

        @DexIgnore
        public l(u36 u36, my4 my4) {
            this.a = u36;
            this.b = my4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(b46.b bVar) {
            AddressWrapper.AddressType e;
            PlacesClient d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            String c = bVar.c();
            if (!(c == null || (e = bVar.e()) == null)) {
                int i = v36.a[e.ordinal()];
                if (i == 1) {
                    this.b.u.setText(ig5.a(PortfolioApp.g0.c(), 2131886348));
                } else if (i == 2) {
                    this.b.u.setText(ig5.a(PortfolioApp.g0.c(), 2131886350));
                } else if (i == 3) {
                    this.b.u.setText(c);
                }
            }
            String a2 = bVar.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = bVar.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                FlexibleSwitchCompat flexibleSwitchCompat = this.b.D;
                ee7.a((Object) flexibleSwitchCompat, "binding.scAvoidTolls");
                flexibleSwitchCompat.setChecked(booleanValue);
            }
            Boolean h = bVar.h();
            if (h != null) {
                boolean booleanValue2 = h.booleanValue();
                FlexibleEditText flexibleEditText = this.b.u;
                ee7.a((Object) flexibleEditText, "binding.fetAddressName");
                flexibleEditText.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.u.requestFocus();
                }
            }
            Boolean i2 = bVar.i();
            if (i2 != null) {
                if (i2.booleanValue()) {
                    this.b.s.a("flexible_button_primary");
                } else {
                    this.b.s.a("flexible_button_disabled");
                }
            }
            Boolean g = bVar.g();
            if (g != null && g.booleanValue()) {
                this.a.D0();
            }
            Boolean f = bVar.f();
            if (f == null) {
                return;
            }
            if (f.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    /*
    static {
        String simpleName = u36.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ b46 b(u36 u36) {
        b46 b46 = u36.g;
        if (b46 != null) {
            return b46;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void D0() {
        FLogger.INSTANCE.getLocal().d(q, "showLocationError");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.t(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<my4> qw6 = this.f;
        if (qw6 != null) {
            my4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                flexibleTextView.setVisibility(8);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void g1() {
        qw6<my4> qw6 = this.f;
        if (qw6 != null) {
            my4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.v;
                ee7.a((Object) flexibleTextView, "it.ftvAddressError");
                PortfolioApp c2 = PortfolioApp.g0.c();
                FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                flexibleTextView.setText(c2.getString(2131886354, new Object[]{flexibleAutoCompleteTextView.getText()}));
                FlexibleTextView flexibleTextView2 = a2.v;
                ee7.a((Object) flexibleTextView2, "it.ftvAddressError");
                flexibleTextView2.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void h(boolean z) {
        if (z) {
            b46 b46 = this.g;
            if (b46 != null) {
                AddressWrapper a2 = b46.a();
                if (a2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", a2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                ee7.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.zl4.b
    public void m(boolean z) {
        qw6<my4> qw6 = this.f;
        if (qw6 != null) {
            my4 a2 = qw6.a();
            if (a2 != null) {
                if (z) {
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(flexibleAutoCompleteTextView.getText())) {
                        Boolean bool = this.j;
                        if (bool == null) {
                            ee7.a();
                            throw null;
                        } else if (!bool.booleanValue()) {
                            g1();
                            return;
                        }
                    }
                }
                f1();
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            Location location = bundleExtra != null ? (Location) bundleExtra.getParcelable(PlaceFields.LOCATION) : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = q;
            StringBuilder sb = new StringBuilder();
            sb.append("addressResult: ");
            sb.append(string);
            sb.append(", lat: ");
            sb.append(location != null ? Double.valueOf(location.getLatitude()) : null);
            sb.append(", lng: ");
            sb.append(location != null ? Double.valueOf(location.getLongitude()) : null);
            local.d(str, sb.toString());
            if (string != null && location != null) {
                b46 b46 = this.g;
                if (b46 != null) {
                    b46.a(string, location);
                    this.j = true;
                    return;
                }
                ee7.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().z().a(this);
        rj4 rj4 = this.h;
        Boolean bool = null;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(b46.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            b46 b46 = (b46) a2;
            this.g = b46;
            if (b46 != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                b46.a(addressWrapper, arguments2 != null ? arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS") : null);
                Bundle arguments3 = getArguments();
                if (arguments3 != null) {
                    bool = Boolean.valueOf(arguments3.getBoolean("KEY_BUNDLE_IS_MAP_RESULT"));
                }
                this.j = bool;
                return;
            }
            ee7.d("mViewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        my4 my4 = (my4) qb.a(layoutInflater, 2131558517, viewGroup, false, a1());
        FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = my4.q;
        flexibleAutoCompleteTextView.setDropDownBackgroundDrawable(v6.c(flexibleAutoCompleteTextView.getContext(), 2131230821));
        flexibleAutoCompleteTextView.setOnItemClickListener(new c(this, my4));
        flexibleAutoCompleteTextView.addTextChangedListener(new d(this, my4));
        flexibleAutoCompleteTextView.setOnKeyListener(new e(this, my4));
        my4.u.addTextChangedListener(new f(this));
        my4.w.setOnClickListener(new g(this));
        my4.s.setOnClickListener(new h(this));
        my4.t.setOnClickListener(new i(this, my4));
        my4.D.setOnCheckedChangeListener(new j(this));
        ee7.a((Object) my4, "binding");
        View d2 = my4.d();
        ee7.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, my4));
        my4.x.setOnClickListener(new k(this));
        b46 b46 = this.g;
        if (b46 != null) {
            b46.b().a(getViewLifecycleOwner(), new l(this, my4));
            this.f = new qw6<>(this, my4);
            return my4.d();
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        b46 b46 = this.g;
        if (b46 != null) {
            b46.f();
            super.onPause();
            return;
        }
        ee7.d("mViewModel");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        b46 b46 = this.g;
        if (b46 != null) {
            b46.e();
        } else {
            ee7.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void a(PlacesClient placesClient) {
        if (isActive()) {
            Context requireContext = requireContext();
            ee7.a((Object) requireContext, "requireContext()");
            zl4 zl4 = new zl4(requireContext, placesClient);
            this.i = zl4;
            if (zl4 != null) {
                zl4.a(this);
            }
            qw6<my4> qw6 = this.f;
            if (qw6 != null) {
                my4 a2 = qw6.a();
                if (a2 != null) {
                    a2.q.setAdapter(this.i);
                    FlexibleAutoCompleteTextView flexibleAutoCompleteTextView = a2.q;
                    ee7.a((Object) flexibleAutoCompleteTextView, "it.autocompletePlaces");
                    Editable text = flexibleAutoCompleteTextView.getText();
                    ee7.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = nh7.d(text);
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        a2.q.setSelection(d2.length());
                        return;
                    }
                    f1();
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
