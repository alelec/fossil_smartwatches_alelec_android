package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<i93> CREATOR; // = new ba3();
    @DexIgnore
    public String a;

    @DexIgnore
    public i93(String str) {
        this.a = str;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a, false);
        k72.a(parcel, a2);
    }
}
