package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xo4 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ vo4 b;
    @DexIgnore
    public /* final */ wo4 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {309}, m = "block")
    public static final class a extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((un4) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {285}, m = "cancelFriendRequest")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {387, 389}, m = "completePendingConfirmedFriends")
    public static final class c extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {225}, m = "fetchBlockedFriends")
    public static final class d extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {204}, m = "fetchFriends")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {175}, m = "fetchReceivedRequestFriends")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {129, 131}, m = "fetchSentRequestFriends")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {65}, m = "respondRequest")
    public static final class h extends rb7 {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(false, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {26}, m = "searchProfile")
    public static final class i extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {46}, m = "sendRequest")
    public static final class j extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {254}, m = "unFriend")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.domain.FriendRepository", f = "FriendRepository.kt", l = {344}, m = "unblock")
    public static final class l extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ xo4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(xo4 xo4, fb7 fb7) {
            super(fb7);
            this.this$0 = xo4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(null, this);
        }
    }

    @DexIgnore
    public xo4(vo4 vo4, wo4 wo4, ch5 ch5) {
        ee7.b(vo4, "local");
        ee7.b(wo4, "remote");
        ee7.b(ch5, "shared");
        this.b = vo4;
        this.c = wo4;
        String simpleName = xo4.class.getSimpleName();
        ee7.a((Object) simpleName, "FriendRepository::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r6, com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.un4>>> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.xo4.i
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.xo4$i r0 = (com.fossil.xo4.i) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$i r0 = new com.fossil.xo4$i
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r7)
            goto L_0x004c
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.fossil.wo4 r7 = r5.c
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.d(r6, r0)
            if (r7 != r1) goto L_0x004b
            return r1
        L_0x004b:
            r0 = r5
        L_0x004c:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x007c
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r6 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r6 = (com.portfolio.platform.data.source.remote.ApiResponse) r6
            if (r6 == 0) goto L_0x0062
            java.util.List r6 = r6.get_items()
            goto L_0x0063
        L_0x0062:
            r6 = r2
        L_0x0063:
            if (r6 != 0) goto L_0x0074
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r7 = new com.portfolio.platform.data.model.ServerError
            r0 = 600(0x258, float:8.41E-43)
            java.lang.String r1 = "code: 200 but null response"
            r7.<init>(r0, r1)
            r6.<init>(r7)
            goto L_0x00c2
        L_0x0074:
            com.fossil.ko4 r7 = new com.fossil.ko4
            r0 = 2
            r7.<init>(r6, r2, r0, r2)
            r6 = r7
            goto L_0x00c2
        L_0x007c:
            boolean r1 = r7 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00c3
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "searchProfile - "
            r3.append(r4)
            r3.append(r6)
            java.lang.String r6 = " - "
            r3.append(r6)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r6 = r7.a()
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            r1.e(r0, r6)
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r1 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00bc
            java.lang.String r2 = r7.getMessage()
        L_0x00bc:
            r0.<init>(r1, r2)
            r6.<init>(r0)
        L_0x00c2:
            return r6
        L_0x00c3:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.un4>>> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.xo4.d
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.xo4$d r0 = (com.fossil.xo4.d) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$d r0 = new com.fossil.xo4$d
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r8)
            goto L_0x0048
        L_0x002d:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0035:
            com.fossil.t87.a(r8)
            com.fossil.wo4 r8 = r7.c
            r0.L$0 = r7
            r0.label = r3
            java.lang.String r2 = "blocked"
            java.lang.Object r8 = r8.c(r2, r0)
            if (r8 != r1) goto L_0x0047
            return r1
        L_0x0047:
            r0 = r7
        L_0x0048:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00be
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            java.lang.Object r8 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r8 = (com.portfolio.platform.data.source.remote.ApiResponse) r8
            if (r8 == 0) goto L_0x005e
            java.util.List r8 = r8.get_items()
            goto L_0x005f
        L_0x005e:
            r8 = r2
        L_0x005f:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "fetchBlockedFriends - data: "
            r5.append(r6)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            r1.e(r4, r5)
            if (r8 == 0) goto L_0x0085
            boolean r1 = r8.isEmpty()
            if (r1 == 0) goto L_0x0084
            goto L_0x0085
        L_0x0084:
            r3 = 0
        L_0x0085:
            if (r3 == 0) goto L_0x008d
            com.fossil.vo4 r0 = r0.b
            r0.c()
            goto L_0x00b0
        L_0x008d:
            java.util.Iterator r1 = r8.iterator()
        L_0x0091:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x00a2
            java.lang.Object r3 = r1.next()
            com.fossil.un4 r3 = (com.fossil.un4) r3
            r4 = -1
            r3.a(r4)
            goto L_0x0091
        L_0x00a2:
            com.fossil.vo4 r1 = r0.b
            r1.a(r8)
            com.fossil.vo4 r1 = r0.b
            java.util.List r1 = r1.f()
            r0.a(r1, r8)
        L_0x00b0:
            if (r8 == 0) goto L_0x00b3
            goto L_0x00b7
        L_0x00b3:
            java.util.List r8 = com.fossil.w97.a()
        L_0x00b7:
            r0 = 2
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r8, r2, r0, r2)
            goto L_0x00fc
        L_0x00be:
            boolean r1 = r8 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00fd
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchBlockedFriends - "
            r3.append(r4)
            com.fossil.yi5 r8 = (com.fossil.yi5) r8
            int r4 = r8.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            com.fossil.ko4 r1 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r3 = r8.a()
            com.portfolio.platform.data.model.ServerError r8 = r8.c()
            if (r8 == 0) goto L_0x00f6
            java.lang.String r2 = r8.getMessage()
        L_0x00f6:
            r0.<init>(r3, r2)
            r1.<init>(r0)
        L_0x00fc:
            return r1
        L_0x00fd:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.b(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.un4 r6, com.fossil.fb7<? super com.fossil.ko4<java.lang.Boolean>> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.xo4.j
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.xo4$j r0 = (com.fossil.xo4.j) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$j r0 = new com.fossil.xo4$j
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r6 = r0.L$2
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Object r6 = r0.L$1
            com.fossil.un4 r6 = (com.fossil.un4) r6
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r7)
            goto L_0x005a
        L_0x0035:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x003d:
            com.fossil.t87.a(r7)
            java.lang.String r7 = r6.d()
            com.fossil.wo4 r2 = r5.c
            java.lang.String r4 = r6.i()
            r0.L$0 = r5
            r0.L$1 = r6
            r0.L$2 = r7
            r0.label = r3
            java.lang.Object r7 = r2.a(r7, r4, r0)
            if (r7 != r1) goto L_0x0059
            return r1
        L_0x0059:
            r0 = r5
        L_0x005a:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x006c
            com.fossil.ko4 r6 = new com.fossil.ko4
            java.lang.Boolean r7 = com.fossil.pb7.a(r3)
            r0 = 2
            r6.<init>(r7, r2, r0, r2)
            goto L_0x00c7
        L_0x006c:
            boolean r1 = r7 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00c8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "sendRequest - "
            r3.append(r4)
            java.lang.String r6 = r6.d()
            r3.append(r6)
            java.lang.String r6 = " - "
            r3.append(r6)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r6 = r7.a()
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            r1.e(r0, r6)
            com.fossil.ko4 r6 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r7.c()
            if (r1 == 0) goto L_0x00b3
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x00b3
            int r1 = r1.intValue()
            goto L_0x00b7
        L_0x00b3:
            int r1 = r7.a()
        L_0x00b7:
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00c1
            java.lang.String r2 = r7.getMessage()
        L_0x00c1:
            r0.<init>(r1, r2)
            r6.<init>(r0)
        L_0x00c7:
            return r6
        L_0x00c8:
            com.fossil.p87 r6 = new com.fossil.p87
            r6.<init>()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.c(com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ac  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.un4>>> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.xo4.f
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.xo4$f r0 = (com.fossil.xo4.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$f r0 = new com.fossil.xo4$f
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r7)
            goto L_0x0048
        L_0x002d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0035:
            com.fossil.t87.a(r7)
            com.fossil.wo4 r7 = r6.c
            r0.L$0 = r6
            r0.label = r3
            java.lang.String r2 = "sent_request"
            java.lang.Object r7 = r7.c(r2, r0)
            if (r7 != r1) goto L_0x0047
            return r1
        L_0x0047:
            r0 = r6
        L_0x0048:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00ac
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            if (r7 == 0) goto L_0x005e
            java.util.List r7 = r7.get_items()
            goto L_0x005f
        L_0x005e:
            r7 = r2
        L_0x005f:
            if (r7 == 0) goto L_0x006a
            boolean r1 = r7.isEmpty()
            if (r1 == 0) goto L_0x0068
            goto L_0x006a
        L_0x0068:
            r1 = 0
            goto L_0x006b
        L_0x006a:
            r1 = 1
        L_0x006b:
            if (r1 == 0) goto L_0x0073
            com.fossil.vo4 r0 = r0.b
            r0.d()
            goto L_0x009e
        L_0x0073:
            com.fossil.vo4 r1 = r0.b
            java.util.List r1 = r1.k()
            java.util.Iterator r4 = r7.iterator()
        L_0x007d:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x008d
            java.lang.Object r5 = r4.next()
            com.fossil.un4 r5 = (com.fossil.un4) r5
            r5.a(r3)
            goto L_0x007d
        L_0x008d:
            boolean r1 = com.fossil.ee7.a(r7, r1)
            r1 = r1 ^ r3
            if (r1 == 0) goto L_0x009e
            com.fossil.vo4 r1 = r0.b
            r1.d()
            com.fossil.vo4 r0 = r0.b
            r0.a(r7)
        L_0x009e:
            if (r7 == 0) goto L_0x00a1
            goto L_0x00a5
        L_0x00a1:
            java.util.List r7 = com.fossil.w97.a()
        L_0x00a5:
            r0 = 2
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r7, r2, r0, r2)
            goto L_0x00ea
        L_0x00ac:
            boolean r1 = r7 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00eb
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFriendsRequested - "
            r3.append(r4)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r4 = r7.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            com.fossil.ko4 r1 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r3 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00e4
            java.lang.String r2 = r7.getMessage()
        L_0x00e4:
            r0.<init>(r3, r2)
            r1.<init>(r0)
        L_0x00ea:
            return r1
        L_0x00eb:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.d(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.un4>>> r13) {
        /*
            r12 = this;
            boolean r0 = r13 instanceof com.fossil.xo4.g
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.xo4$g r0 = (com.fossil.xo4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$g r0 = new com.fossil.xo4$g
            r0.<init>(r12, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x0040
            if (r2 == r4) goto L_0x0038
            if (r2 != r3) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r13)
            goto L_0x005f
        L_0x0030:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r0)
            throw r13
        L_0x0038:
            java.lang.Object r2 = r0.L$0
            com.fossil.xo4 r2 = (com.fossil.xo4) r2
            com.fossil.t87.a(r13)
            goto L_0x004f
        L_0x0040:
            com.fossil.t87.a(r13)
            r0.L$0 = r12
            r0.label = r4
            java.lang.Object r13 = r12.a(r0)
            if (r13 != r1) goto L_0x004e
            return r1
        L_0x004e:
            r2 = r12
        L_0x004f:
            com.fossil.wo4 r13 = r2.c
            r0.L$0 = r2
            r0.label = r3
            java.lang.String r5 = "received_request"
            java.lang.Object r13 = r13.c(r5, r0)
            if (r13 != r1) goto L_0x005e
            return r1
        L_0x005e:
            r0 = r2
        L_0x005f:
            com.fossil.zi5 r13 = (com.fossil.zi5) r13
            boolean r1 = r13 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0163
            com.fossil.bj5 r13 = (com.fossil.bj5) r13
            java.lang.Object r13 = r13.a()
            com.portfolio.platform.data.source.remote.ApiResponse r13 = (com.portfolio.platform.data.source.remote.ApiResponse) r13
            if (r13 == 0) goto L_0x0075
            java.util.List r13 = r13.get_items()
            goto L_0x0076
        L_0x0075:
            r13 = r2
        L_0x0076:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r5 = r0.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "fetchSentRequestFriends - newSent: "
            r6.append(r7)
            r6.append(r13)
            java.lang.String r6 = r6.toString()
            r1.e(r5, r6)
            r1 = 0
            if (r13 == 0) goto L_0x009e
            boolean r5 = r13.isEmpty()
            if (r5 == 0) goto L_0x009c
            goto L_0x009e
        L_0x009c:
            r5 = 0
            goto L_0x009f
        L_0x009e:
            r5 = 1
        L_0x009f:
            if (r5 == 0) goto L_0x00a8
            com.fossil.vo4 r0 = r0.b
            r0.e()
            goto L_0x0156
        L_0x00a8:
            com.fossil.vo4 r5 = r0.b
            java.util.List r5 = r5.j()
            java.util.Iterator r5 = r5.iterator()
        L_0x00b2:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x00f1
            java.lang.Object r6 = r5.next()
            com.fossil.un4 r6 = (com.fossil.un4) r6
            java.util.Iterator r7 = r13.iterator()
            r8 = 0
        L_0x00c3:
            boolean r9 = r7.hasNext()
            r10 = -1
            if (r9 == 0) goto L_0x00ea
            java.lang.Object r9 = r7.next()
            com.fossil.un4 r9 = (com.fossil.un4) r9
            java.lang.String r9 = r9.d()
            java.lang.String r11 = r6.d()
            boolean r9 = com.fossil.ee7.a(r9, r11)
            java.lang.Boolean r9 = com.fossil.pb7.a(r9)
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x00e7
            goto L_0x00eb
        L_0x00e7:
            int r8 = r8 + 1
            goto L_0x00c3
        L_0x00ea:
            r8 = -1
        L_0x00eb:
            if (r8 == r10) goto L_0x00b2
            r13.remove(r8)
            goto L_0x00b2
        L_0x00f1:
            boolean r5 = r13.isEmpty()
            r4 = r4 ^ r5
            if (r4 == 0) goto L_0x0151
            java.util.Iterator r4 = r13.iterator()
        L_0x00fc:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x010c
            java.lang.Object r5 = r4.next()
            com.fossil.un4 r5 = (com.fossil.un4) r5
            r5.a(r3)
            goto L_0x00fc
        L_0x010c:
            com.fossil.vo4 r4 = r0.b
            java.lang.Long[] r4 = r4.a(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = r0.a
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "fetchSentRequestFriends - rowIds: "
            r7.append(r8)
            java.util.ArrayList r8 = new java.util.ArrayList
            int r9 = r4.length
            r8.<init>(r9)
            int r9 = r4.length
        L_0x012b:
            if (r1 >= r9) goto L_0x013d
            r10 = r4[r1]
            long r10 = r10.longValue()
            java.lang.Long r10 = com.fossil.pb7.a(r10)
            r8.add(r10)
            int r1 = r1 + 1
            goto L_0x012b
        L_0x013d:
            r7.append(r8)
            java.lang.String r1 = r7.toString()
            r5.e(r6, r1)
            com.fossil.vo4 r1 = r0.b
            java.util.List r1 = r1.l()
            r0.a(r1, r13)
            goto L_0x0156
        L_0x0151:
            com.fossil.vo4 r0 = r0.b
            r0.e()
        L_0x0156:
            if (r13 == 0) goto L_0x0159
            goto L_0x015d
        L_0x0159:
            java.util.List r13 = com.fossil.w97.a()
        L_0x015d:
            com.fossil.ko4 r0 = new com.fossil.ko4
            r0.<init>(r13, r2, r3, r2)
            goto L_0x01a1
        L_0x0163:
            boolean r1 = r13 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x01a2
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFriendsRequesting - "
            r3.append(r4)
            com.fossil.yi5 r13 = (com.fossil.yi5) r13
            int r4 = r13.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            int r3 = r13.a()
            com.portfolio.platform.data.model.ServerError r13 = r13.c()
            if (r13 == 0) goto L_0x019b
            java.lang.String r2 = r13.getMessage()
        L_0x019b:
            r1.<init>(r3, r2)
            r0.<init>(r1)
        L_0x01a1:
            return r0
        L_0x01a2:
            com.fossil.p87 r13 = new com.fossil.p87
            r13.<init>()
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.e(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object c(com.fossil.fb7<? super com.fossil.ko4<java.util.List<com.fossil.un4>>> r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof com.fossil.xo4.e
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.xo4$e r0 = (com.fossil.xo4.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$e r0 = new com.fossil.xo4$e
            r0.<init>(r6, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r7)
            goto L_0x0048
        L_0x002d:
            java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r7.<init>(r0)
            throw r7
        L_0x0035:
            com.fossil.t87.a(r7)
            com.fossil.wo4 r7 = r6.c
            r0.L$0 = r6
            r0.label = r3
            java.lang.String r2 = "friend"
            java.lang.Object r7 = r7.c(r2, r0)
            if (r7 != r1) goto L_0x0047
            return r1
        L_0x0047:
            r0 = r6
        L_0x0048:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r1 = r7 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x0099
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            com.portfolio.platform.data.source.remote.ApiResponse r7 = (com.portfolio.platform.data.source.remote.ApiResponse) r7
            if (r7 == 0) goto L_0x005e
            java.util.List r7 = r7.get_items()
            goto L_0x005f
        L_0x005e:
            r7 = r2
        L_0x005f:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = r0.a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "fetchFriends - data: "
            r4.append(r5)
            r4.append(r7)
            java.lang.String r4 = r4.toString()
            r1.e(r3, r4)
            if (r7 == 0) goto L_0x008b
            com.fossil.vo4 r1 = r0.b
            r1.a(r7)
            com.fossil.vo4 r1 = r0.b
            java.util.List r1 = r1.g()
            r0.a(r1, r7)
        L_0x008b:
            if (r7 == 0) goto L_0x008e
            goto L_0x0092
        L_0x008e:
            java.util.List r7 = com.fossil.w97.a()
        L_0x0092:
            r0 = 2
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r7, r2, r0, r2)
            goto L_0x00d7
        L_0x0099:
            boolean r1 = r7 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00d8
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r0 = r0.a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "fetchFriends - "
            r3.append(r4)
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r4 = r7.a()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r0, r3)
            com.fossil.ko4 r1 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            int r3 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x00d1
            java.lang.String r2 = r7.getMessage()
        L_0x00d1:
            r0.<init>(r3, r2)
            r1.<init>(r0)
        L_0x00d7:
            return r1
        L_0x00d8:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.c(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c8  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(boolean r18, com.fossil.un4 r19, com.fossil.fb7<? super com.fossil.ko4<com.fossil.un4>> r20) {
        /*
            r17 = this;
            r0 = r17
            r11 = r18
            r1 = r20
            boolean r2 = r1 instanceof com.fossil.xo4.h
            if (r2 == 0) goto L_0x0019
            r2 = r1
            com.fossil.xo4$h r2 = (com.fossil.xo4.h) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0019
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001e
        L_0x0019:
            com.fossil.xo4$h r2 = new com.fossil.xo4$h
            r2.<init>(r0, r1)
        L_0x001e:
            r12 = r2
            java.lang.Object r1 = r12.result
            java.lang.Object r13 = com.fossil.nb7.a()
            int r2 = r12.label
            r14 = 1
            if (r2 == 0) goto L_0x004b
            if (r2 != r14) goto L_0x0043
            long r2 = r12.J$0
            java.lang.Object r2 = r12.L$2
            com.fossil.un4 r2 = (com.fossil.un4) r2
            int r3 = r12.I$0
            java.lang.Object r3 = r12.L$1
            com.fossil.un4 r3 = (com.fossil.un4) r3
            boolean r4 = r12.Z$0
            java.lang.Object r5 = r12.L$0
            com.fossil.xo4 r5 = (com.fossil.xo4) r5
            com.fossil.t87.a(r1)
            goto L_0x00c0
        L_0x0043:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004b:
            com.fossil.t87.a(r1)
            if (r11 == 0) goto L_0x0052
            r10 = 0
            goto L_0x0054
        L_0x0052:
            r1 = 3
            r10 = 3
        L_0x0054:
            com.fossil.un4 r9 = new com.fossil.un4
            java.lang.String r2 = r19.d()
            java.lang.String r3 = r19.i()
            java.lang.String r4 = r19.b()
            java.lang.String r5 = r19.e()
            java.lang.Integer r6 = r19.g()
            java.lang.String r7 = r19.h()
            r16 = 2
            r1 = r9
            r8 = r18
            r15 = r9
            r9 = r16
            r16 = r10
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            com.fossil.vo4 r1 = r0.b
            long r1 = r1.a(r15)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "respondRequest - editFriend - rowId= "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r5 = r5.toString()
            r3.e(r4, r5)
            com.fossil.wo4 r3 = r0.c
            java.lang.String r4 = r19.d()
            r12.L$0 = r0
            r12.Z$0 = r11
            r5 = r19
            r12.L$1 = r5
            r6 = r16
            r12.I$0 = r6
            r12.L$2 = r15
            r12.J$0 = r1
            r12.label = r14
            java.lang.Object r1 = r3.a(r11, r4, r12)
            if (r1 != r13) goto L_0x00bc
            return r13
        L_0x00bc:
            r3 = r5
            r4 = r11
            r2 = r15
            r5 = r0
        L_0x00c0:
            com.fossil.zi5 r1 = (com.fossil.zi5) r1
            boolean r6 = r1 instanceof com.fossil.bj5
            r7 = 2
            r8 = 0
            if (r6 == 0) goto L_0x0120
            if (r4 == 0) goto L_0x00d8
            r2 = 0
            r3.a(r2)
            r3.b(r2)
            com.fossil.vo4 r2 = r5.b
            long r9 = r2.a(r3)
            goto L_0x00e3
        L_0x00d8:
            com.fossil.vo4 r2 = r5.b
            java.lang.String r6 = r3.d()
            int r2 = r2.a(r6)
            long r9 = (long) r2
        L_0x00e3:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r11 = "respondRequest - success - confirmation = "
            r6.append(r11)
            r6.append(r4)
            java.lang.String r4 = " - newRowIdOrDeletedId= "
            r6.append(r4)
            r6.append(r9)
            java.lang.String r4 = r6.toString()
            r2.e(r5, r4)
            com.fossil.bj5 r1 = (com.fossil.bj5) r1
            java.lang.Object r1 = r1.a()
            com.fossil.un4 r1 = (com.fossil.un4) r1
            if (r1 != 0) goto L_0x0118
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r3, r8, r7, r8)
            goto L_0x0224
        L_0x0118:
            com.fossil.ko4 r2 = new com.fossil.ko4
            r2.<init>(r1, r8, r7, r8)
        L_0x011d:
            r1 = r2
            goto L_0x0224
        L_0x0120:
            boolean r6 = r1 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x0225
            com.fossil.yi5 r1 = (com.fossil.yi5) r1
            com.portfolio.platform.data.model.ServerError r6 = r1.c()
            if (r6 == 0) goto L_0x0137
            java.lang.Integer r6 = r6.getCode()
            if (r6 == 0) goto L_0x0137
            int r6 = r6.intValue()
            goto L_0x013b
        L_0x0137:
            int r6 = r1.a()
        L_0x013b:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = r5.a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "respondRequest - confirmation: "
            r11.append(r12)
            r11.append(r4)
            java.lang.String r12 = " - failed - "
            r11.append(r12)
            int r12 = r1.a()
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            r9.e(r10, r11)
            r9 = 404001(0x62a21, float:5.66126E-40)
            if (r6 == r9) goto L_0x01fe
            r9 = 404702(0x62cde, float:5.67108E-40)
            if (r6 == r9) goto L_0x01df
            r9 = 409001(0x63da9, float:5.73132E-40)
            if (r6 == r9) goto L_0x0187
            com.fossil.ko4 r2 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x0180
            java.lang.String r8 = r1.getMessage()
        L_0x0180:
            r3.<init>(r6, r8)
            r2.<init>(r3)
            goto L_0x011d
        L_0x0187:
            if (r4 == 0) goto L_0x01b3
            r1 = 0
            r2.a(r1)
            r2.b(r1)
            com.fossil.vo4 r1 = r5.b
            long r1 = r1.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r5.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = "respondRequest - error - insert: "
            r6.append(r9)
            r6.append(r1)
            java.lang.String r1 = r6.toString()
            r4.e(r5, r1)
            goto L_0x01d9
        L_0x01b3:
            com.fossil.vo4 r1 = r5.b
            java.lang.String r2 = r3.d()
            int r1 = r1.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = r5.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "respondRequest - error - deletedRowId: "
            r5.append(r6)
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r2.e(r4, r1)
        L_0x01d9:
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r3, r8, r7, r8)
            goto L_0x0224
        L_0x01df:
            com.fossil.vo4 r2 = r5.b
            java.lang.String r3 = r3.d()
            r2.a(r3)
            com.fossil.ko4 r2 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x01f6
            java.lang.String r8 = r1.getMessage()
        L_0x01f6:
            r3.<init>(r6, r8)
            r2.<init>(r3)
            goto L_0x011d
        L_0x01fe:
            com.fossil.vo4 r2 = r5.b
            java.lang.String r5 = r3.d()
            r2.a(r5)
            if (r4 == 0) goto L_0x021f
            com.fossil.ko4 r2 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r3 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r1.c()
            if (r1 == 0) goto L_0x0217
            java.lang.String r8 = r1.getMessage()
        L_0x0217:
            r3.<init>(r6, r8)
            r2.<init>(r3)
            goto L_0x011d
        L_0x021f:
            com.fossil.ko4 r1 = new com.fossil.ko4
            r1.<init>(r3, r8, r7, r8)
        L_0x0224:
            return r1
        L_0x0225:
            com.fossil.p87 r1 = new com.fossil.p87
            r1.<init>()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.a(boolean, com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object b(com.fossil.un4 r10, com.fossil.fb7<? super com.fossil.ko4<java.lang.String>> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.xo4.b
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.xo4$b r0 = (com.fossil.xo4.b) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$b r0 = new com.fossil.xo4$b
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r1 = r0.L$1
            com.fossil.un4 r1 = (com.fossil.un4) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r11)
            goto L_0x005d
        L_0x0035:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003d:
            com.fossil.t87.a(r11)
            java.lang.String r11 = r10.d()
            com.fossil.wo4 r2 = r9.c
            java.lang.String r4 = r10.d()
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r3
            java.lang.Object r10 = r2.b(r4, r0)
            if (r10 != r1) goto L_0x0059
            return r1
        L_0x0059:
            r0 = r9
            r8 = r11
            r11 = r10
            r10 = r8
        L_0x005d:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r1 = r11 instanceof com.fossil.bj5
            r2 = 2
            r3 = 0
            if (r1 == 0) goto L_0x0071
            com.fossil.vo4 r11 = r0.b
            r11.a(r10)
            com.fossil.ko4 r11 = new com.fossil.ko4
            r11.<init>(r10, r3, r2, r3)
            goto L_0x00e8
        L_0x0071:
            boolean r1 = r11 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00e9
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r1 = r11.c()
            if (r1 == 0) goto L_0x0088
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x0088
            int r1 = r1.intValue()
            goto L_0x008c
        L_0x0088:
            int r1 = r11.a()
        L_0x008c:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r0.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "cancelFriendRequest - fail - "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r6 = r6.toString()
            r4.e(r5, r6)
            r4 = 404001(0x62a21, float:5.66126E-40)
            if (r1 == r4) goto L_0x00de
            r4 = 400709(0x61d45, float:5.61513E-40)
            if (r1 != r4) goto L_0x00b3
            goto L_0x00de
        L_0x00b3:
            com.fossil.ko4 r10 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r1 = r11.c()
            if (r1 == 0) goto L_0x00c8
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x00c8
            int r1 = r1.intValue()
            goto L_0x00cc
        L_0x00c8:
            int r1 = r11.a()
        L_0x00cc:
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x00d6
            java.lang.String r3 = r11.getMessage()
        L_0x00d6:
            r0.<init>(r1, r3)
            r10.<init>(r0)
            r11 = r10
            goto L_0x00e8
        L_0x00de:
            com.fossil.vo4 r11 = r0.b
            r11.a(r10)
            com.fossil.ko4 r11 = new com.fossil.ko4
            r11.<init>(r10, r3, r2, r3)
        L_0x00e8:
            return r11
        L_0x00e9:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.b(com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object d(com.fossil.un4 r10, com.fossil.fb7<? super com.fossil.ko4<java.lang.String>> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.xo4.k
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.xo4$k r0 = (com.fossil.xo4.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$k r0 = new com.fossil.xo4$k
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x003d
            if (r2 != r3) goto L_0x0035
            java.lang.Object r10 = r0.L$2
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r1 = r0.L$1
            com.fossil.un4 r1 = (com.fossil.un4) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r11)
            goto L_0x005d
        L_0x0035:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x003d:
            com.fossil.t87.a(r11)
            java.lang.String r11 = r10.d()
            com.fossil.wo4 r2 = r9.c
            java.lang.String r4 = r10.d()
            r0.L$0 = r9
            r0.L$1 = r10
            r0.L$2 = r11
            r0.label = r3
            java.lang.Object r10 = r2.e(r4, r0)
            if (r10 != r1) goto L_0x0059
            return r1
        L_0x0059:
            r0 = r9
            r8 = r11
            r11 = r10
            r10 = r8
        L_0x005d:
            com.fossil.zi5 r11 = (com.fossil.zi5) r11
            boolean r1 = r11 instanceof com.fossil.bj5
            r2 = 2
            r3 = 0
            if (r1 == 0) goto L_0x0071
            com.fossil.vo4 r11 = r0.b
            r11.a(r10)
            com.fossil.ko4 r11 = new com.fossil.ko4
            r11.<init>(r10, r3, r2, r3)
            goto L_0x00f1
        L_0x0071:
            boolean r1 = r11 instanceof com.fossil.yi5
            if (r1 == 0) goto L_0x00f2
            com.fossil.yi5 r11 = (com.fossil.yi5) r11
            com.portfolio.platform.data.model.ServerError r1 = r11.c()
            if (r1 == 0) goto L_0x0088
            java.lang.Integer r1 = r1.getCode()
            if (r1 == 0) goto L_0x0088
            int r1 = r1.intValue()
            goto L_0x008c
        L_0x0088:
            int r1 = r11.a()
        L_0x008c:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = r0.a
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "unFriend - fail - "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r6 = r6.toString()
            r4.e(r5, r6)
            r4 = 400709(0x61d45, float:5.61513E-40)
            if (r1 == r4) goto L_0x00e7
            r4 = 404001(0x62a21, float:5.66126E-40)
            if (r1 == r4) goto L_0x00e7
            r2 = 404701(0x62cdd, float:5.67107E-40)
            if (r1 == r2) goto L_0x00cc
            com.fossil.ko4 r10 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x00c5
            java.lang.String r3 = r11.getMessage()
        L_0x00c5:
            r0.<init>(r1, r3)
            r10.<init>(r0)
            goto L_0x00e5
        L_0x00cc:
            com.fossil.vo4 r0 = r0.b
            r0.a(r10)
            com.fossil.ko4 r10 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r11 = r11.c()
            if (r11 == 0) goto L_0x00df
            java.lang.String r3 = r11.getMessage()
        L_0x00df:
            r0.<init>(r1, r3)
            r10.<init>(r0)
        L_0x00e5:
            r11 = r10
            goto L_0x00f1
        L_0x00e7:
            com.fossil.vo4 r11 = r0.b
            r11.a(r10)
            com.fossil.ko4 r11 = new com.fossil.ko4
            r11.<init>(r10, r3, r2, r3)
        L_0x00f1:
            return r11
        L_0x00f2:
            com.fossil.p87 r10 = new com.fossil.p87
            r10.<init>()
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.d(com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<un4> c() {
        return this.b.h();
    }

    @DexIgnore
    public final int b() {
        return this.b.b();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object e(com.fossil.un4 r8, com.fossil.fb7<? super com.fossil.ko4<java.lang.String>> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.fossil.xo4.l
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.xo4$l r0 = (com.fossil.xo4.l) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$l r0 = new com.fossil.xo4$l
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r8 = r0.L$3
            com.fossil.un4 r8 = (com.fossil.un4) r8
            java.lang.Object r1 = r0.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r0.L$1
            com.fossil.un4 r2 = (com.fossil.un4) r2
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r9)
            goto L_0x0067
        L_0x0039:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0041:
            com.fossil.t87.a(r9)
            java.lang.String r9 = r8.d()
            com.fossil.un4 r2 = com.fossil.uc5.a(r8)
            com.fossil.wo4 r4 = r7.c
            java.lang.String r5 = r8.d()
            r0.L$0 = r7
            r0.L$1 = r8
            r0.L$2 = r9
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r8 = r4.f(r5, r0)
            if (r8 != r1) goto L_0x0063
            return r1
        L_0x0063:
            r0 = r7
            r1 = r9
            r9 = r8
            r8 = r2
        L_0x0067:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r2 = r9 instanceof com.fossil.bj5
            r3 = 0
            if (r2 == 0) goto L_0x009d
            com.fossil.bj5 r9 = (com.fossil.bj5) r9
            java.lang.Object r9 = r9.a()
            com.fossil.ln4 r9 = (com.fossil.ln4) r9
            if (r9 == 0) goto L_0x007d
            java.lang.String r9 = r9.a()
            goto L_0x007e
        L_0x007d:
            r9 = r3
        L_0x007e:
            java.lang.String r2 = "blocked_friend"
            boolean r9 = com.fossil.ee7.a(r9, r2)
            if (r9 == 0) goto L_0x0090
            r9 = 0
            r8.a(r9)
            com.fossil.vo4 r9 = r0.b
            r9.a(r8)
            goto L_0x0095
        L_0x0090:
            com.fossil.vo4 r8 = r0.b
            r8.a(r1)
        L_0x0095:
            com.fossil.ko4 r8 = new com.fossil.ko4
            r9 = 2
            r8.<init>(r1, r3, r9, r3)
            goto L_0x010d
        L_0x009d:
            boolean r8 = r9 instanceof com.fossil.yi5
            if (r8 == 0) goto L_0x010e
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            com.portfolio.platform.data.model.ServerError r8 = r9.c()
            if (r8 == 0) goto L_0x00b4
            java.lang.Integer r8 = r8.getCode()
            if (r8 == 0) goto L_0x00b4
            int r8 = r8.intValue()
            goto L_0x00b8
        L_0x00b4:
            int r8 = r9.a()
        L_0x00b8:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "unBlock - fail - "
            r5.append(r6)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            r2.e(r4, r5)
            r2 = 400709(0x61d45, float:5.61513E-40)
            if (r8 == r2) goto L_0x00f3
            r2 = 404001(0x62a21, float:5.66126E-40)
            if (r8 == r2) goto L_0x00f3
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x00ec
            java.lang.String r3 = r9.getMessage()
        L_0x00ec:
            r1.<init>(r8, r3)
            r0.<init>(r1)
            goto L_0x010c
        L_0x00f3:
            com.fossil.vo4 r0 = r0.b
            r0.a(r1)
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x0106
            java.lang.String r3 = r9.getMessage()
        L_0x0106:
            r1.<init>(r8, r3)
            r0.<init>(r1)
        L_0x010c:
            r8 = r0
        L_0x010d:
            return r8
        L_0x010e:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.e(com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final List<un4> d() {
        return this.b.g();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.un4 r8, com.fossil.fb7<? super com.fossil.ko4<java.lang.String>> r9) {
        /*
            r7 = this;
            boolean r0 = r9 instanceof com.fossil.xo4.a
            if (r0 == 0) goto L_0x0013
            r0 = r9
            com.fossil.xo4$a r0 = (com.fossil.xo4.a) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.xo4$a r0 = new com.fossil.xo4$a
            r0.<init>(r7, r9)
        L_0x0018:
            java.lang.Object r9 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r8 = r0.L$3
            com.fossil.un4 r8 = (com.fossil.un4) r8
            java.lang.Object r1 = r0.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r0.L$1
            com.fossil.un4 r2 = (com.fossil.un4) r2
            java.lang.Object r0 = r0.L$0
            com.fossil.xo4 r0 = (com.fossil.xo4) r0
            com.fossil.t87.a(r9)
            goto L_0x0067
        L_0x0039:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r9 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r9)
            throw r8
        L_0x0041:
            com.fossil.t87.a(r9)
            java.lang.String r9 = r8.d()
            com.fossil.un4 r2 = com.fossil.uc5.a(r8)
            com.fossil.wo4 r4 = r7.c
            java.lang.String r5 = r8.d()
            r0.L$0 = r7
            r0.L$1 = r8
            r0.L$2 = r9
            r0.L$3 = r2
            r0.label = r3
            java.lang.Object r8 = r4.a(r5, r0)
            if (r8 != r1) goto L_0x0063
            return r1
        L_0x0063:
            r0 = r7
            r1 = r9
            r9 = r8
            r8 = r2
        L_0x0067:
            com.fossil.zi5 r9 = (com.fossil.zi5) r9
            boolean r2 = r9 instanceof com.fossil.bj5
            r3 = 0
            if (r2 == 0) goto L_0x007f
            r9 = -1
            r8.a(r9)
            com.fossil.vo4 r9 = r0.b
            r9.a(r8)
            com.fossil.ko4 r8 = new com.fossil.ko4
            r9 = 2
            r8.<init>(r1, r3, r9, r3)
            goto L_0x0107
        L_0x007f:
            boolean r8 = r9 instanceof com.fossil.yi5
            if (r8 == 0) goto L_0x0108
            com.fossil.yi5 r9 = (com.fossil.yi5) r9
            com.portfolio.platform.data.model.ServerError r8 = r9.c()
            if (r8 == 0) goto L_0x0096
            java.lang.Integer r8 = r8.getCode()
            if (r8 == 0) goto L_0x0096
            int r8 = r8.intValue()
            goto L_0x009a
        L_0x0096:
            int r8 = r9.a()
        L_0x009a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = r0.a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "block - fail - "
            r5.append(r6)
            r5.append(r8)
            java.lang.String r5 = r5.toString()
            r2.e(r4, r5)
            switch(r8) {
                case 400709: goto L_0x00e5;
                case 404001: goto L_0x00e5;
                case 404701: goto L_0x00c8;
                case 409001: goto L_0x00c8;
                default: goto L_0x00b9;
            }
        L_0x00b9:
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x0100
            java.lang.String r3 = r9.getMessage()
            goto L_0x0100
        L_0x00c8:
            com.fossil.vo4 r8 = r0.b
            r8.a(r1)
            com.fossil.ko4 r8 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r0 = new com.portfolio.platform.data.model.ServerError
            r1 = 404701(0x62cdd, float:5.67107E-40)
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x00de
            java.lang.String r3 = r9.getMessage()
        L_0x00de:
            r0.<init>(r1, r3)
            r8.<init>(r0)
            goto L_0x0107
        L_0x00e5:
            com.fossil.vo4 r0 = r0.b
            r0.a(r1)
            com.fossil.ko4 r0 = new com.fossil.ko4
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            com.portfolio.platform.data.model.ServerError r9 = r9.c()
            if (r9 == 0) goto L_0x00f8
            java.lang.String r3 = r9.getMessage()
        L_0x00f8:
            r1.<init>(r8, r3)
            r0.<init>(r1)
        L_0x00fe:
            r8 = r0
            goto L_0x0107
        L_0x0100:
            r1.<init>(r8, r3)
            r0.<init>(r1)
            goto L_0x00fe
        L_0x0107:
            return r8
        L_0x0108:
            com.fossil.p87 r8 = new com.fossil.p87
            r8.<init>()
            throw r8
            switch-data {400709->0x00e5, 404001->0x00e5, 404701->0x00c8, 409001->0x00c8, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.a(com.fossil.un4, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<un4>> e() {
        return this.b.i();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x012e  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0133  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0170  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            boolean r2 = r1 instanceof com.fossil.xo4.c
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.xo4$c r2 = (com.fossil.xo4.c) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.xo4$c r2 = new com.fossil.xo4$c
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 0
            r6 = 2
            r7 = 1
            if (r4 == 0) goto L_0x0073
            if (r4 == r7) goto L_0x0054
            if (r4 != r6) goto L_0x004c
            java.lang.Object r4 = r2.L$6
            com.fossil.un4 r4 = (com.fossil.un4) r4
            java.lang.Object r4 = r2.L$5
            java.lang.Object r4 = r2.L$4
            java.util.Iterator r4 = (java.util.Iterator) r4
            java.lang.Object r8 = r2.L$3
            java.lang.Iterable r8 = (java.lang.Iterable) r8
            java.lang.Object r9 = r2.L$2
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r2.L$1
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$0
            com.fossil.xo4 r11 = (com.fossil.xo4) r11
            com.fossil.t87.a(r1)
            goto L_0x0126
        L_0x004c:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0054:
            java.lang.Object r4 = r2.L$6
            com.fossil.un4 r4 = (com.fossil.un4) r4
            java.lang.Object r4 = r2.L$5
            java.lang.Object r4 = r2.L$4
            java.util.Iterator r4 = (java.util.Iterator) r4
            java.lang.Object r8 = r2.L$3
            java.lang.Iterable r8 = (java.lang.Iterable) r8
            java.lang.Object r9 = r2.L$2
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r2.L$1
            com.fossil.oe7 r10 = (com.fossil.oe7) r10
            java.lang.Object r11 = r2.L$0
            com.fossil.xo4 r11 = (com.fossil.xo4) r11
            com.fossil.t87.a(r1)
            goto L_0x010c
        L_0x0073:
            com.fossil.t87.a(r1)
            com.fossil.oe7 r1 = new com.fossil.oe7
            r1.<init>()
            r1.element = r5
            com.fossil.vo4 r4 = r0.b
            java.util.List r4 = r4.j()
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = r0.a
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "completePendingConfirmedFriends - pendingConfirmedFriends: "
            r10.append(r11)
            r10.append(r4)
            java.lang.String r10 = r10.toString()
            r8.e(r9, r10)
            java.util.Iterator r8 = r4.iterator()
            r11 = r0
            r10 = r1
            r9 = r4
            r4 = r8
            r8 = r9
        L_0x00a8:
            boolean r1 = r4.hasNext()
            if (r1 == 0) goto L_0x0170
            java.lang.Object r1 = r4.next()
            r12 = r1
            com.fossil.un4 r12 = (com.fossil.un4) r12
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = r11.a
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r7 = "completePendingConfirmedFriends - shouldStopAdding:"
            r15.append(r7)
            boolean r7 = r10.element
            r15.append(r7)
            java.lang.String r7 = " - pendingFriend: "
            r15.append(r7)
            r15.append(r12)
            java.lang.String r7 = r15.toString()
            r13.e(r14, r7)
            boolean r7 = r10.element
            if (r7 == 0) goto L_0x00ed
            r12.a(r6)
            r12.b(r5)
            com.fossil.vo4 r1 = r11.b
            r1.a(r12)
        L_0x00ea:
            r1 = 1
            goto L_0x016d
        L_0x00ed:
            boolean r7 = r12.a()
            if (r7 == 0) goto L_0x010f
            r2.L$0 = r11
            r2.L$1 = r10
            r2.L$2 = r9
            r2.L$3 = r8
            r2.L$4 = r4
            r2.L$5 = r1
            r2.L$6 = r12
            r1 = 1
            r2.label = r1
            java.lang.Object r7 = r11.a(r1, r12, r2)
            if (r7 != r3) goto L_0x010b
            return r3
        L_0x010b:
            r1 = r7
        L_0x010c:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            goto L_0x0128
        L_0x010f:
            r2.L$0 = r11
            r2.L$1 = r10
            r2.L$2 = r9
            r2.L$3 = r8
            r2.L$4 = r4
            r2.L$5 = r1
            r2.L$6 = r12
            r2.label = r6
            java.lang.Object r1 = r11.a(r5, r12, r2)
            if (r1 != r3) goto L_0x0126
            return r3
        L_0x0126:
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
        L_0x0128:
            com.portfolio.platform.data.model.ServerError r1 = r1.a()
            if (r1 == 0) goto L_0x0133
            java.lang.Integer r1 = r1.getCode()
            goto L_0x0134
        L_0x0133:
            r1 = 0
        L_0x0134:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r12 = r11.a
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "completePendingConfirmedFriends - error: "
            r13.append(r14)
            r13.append(r1)
            java.lang.String r13 = r13.toString()
            r7.e(r12, r13)
            if (r1 == 0) goto L_0x015e
            r7 = 400700(0x61d3c, float:5.615E-40)
            int r12 = r1.intValue()
            if (r12 == r7) goto L_0x015c
            goto L_0x015e
        L_0x015c:
            r1 = 1
            goto L_0x016b
        L_0x015e:
            r7 = 400701(0x61d3d, float:5.61502E-40)
            if (r1 != 0) goto L_0x0164
            goto L_0x00ea
        L_0x0164:
            int r1 = r1.intValue()
            if (r1 != r7) goto L_0x00ea
            goto L_0x015c
        L_0x016b:
            r10.element = r1
        L_0x016d:
            r7 = 1
            goto L_0x00a8
        L_0x0170:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xo4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(List<un4> list, List<un4> list2) {
        List b2 = ea7.b((Collection) list, (Iterable) list2);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : b2) {
            String d2 = ((un4) obj).d();
            Object obj2 = linkedHashMap.get(d2);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(d2, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<un4> arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            ba7.a((Collection) arrayList, (Iterable) ((List) entry2.getValue()));
        }
        ArrayList arrayList2 = new ArrayList(x97.a(arrayList, 10));
        for (un4 un4 : arrayList) {
            arrayList2.add(un4.d());
        }
        vo4 vo4 = this.b;
        Object[] array = arrayList2.toArray(new String[0]);
        if (array != null) {
            vo4.a((String[]) array);
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final long a(un4 un4) {
        ee7.b(un4, "friend");
        return this.b.a(un4);
    }

    @DexIgnore
    public final un4 a(String str) {
        ee7.b(str, "id");
        return this.b.b(str);
    }

    @DexIgnore
    public final void a() {
        this.b.a();
    }
}
