package com.fossil;

import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gs7 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public Map optionMap; // = new HashMap();
    @DexIgnore
    public boolean required;
    @DexIgnore
    public String selected;

    @DexIgnore
    public gs7 addOption(fs7 fs7) {
        this.optionMap.put(fs7.getKey(), fs7);
        return this;
    }

    @DexIgnore
    public Collection getNames() {
        return this.optionMap.keySet();
    }

    @DexIgnore
    public Collection getOptions() {
        return this.optionMap.values();
    }

    @DexIgnore
    public String getSelected() {
        return this.selected;
    }

    @DexIgnore
    public boolean isRequired() {
        return this.required;
    }

    @DexIgnore
    public void setRequired(boolean z) {
        this.required = z;
    }

    @DexIgnore
    public void setSelected(fs7 fs7) throws xr7 {
        String str = this.selected;
        if (str == null || str.equals(fs7.getOpt())) {
            this.selected = fs7.getOpt();
            return;
        }
        throw new xr7(this, fs7);
    }

    @DexIgnore
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = getOptions().iterator();
        stringBuffer.append("[");
        while (it.hasNext()) {
            fs7 fs7 = (fs7) it.next();
            if (fs7.getOpt() != null) {
                stringBuffer.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                stringBuffer.append(fs7.getOpt());
            } else {
                stringBuffer.append("--");
                stringBuffer.append(fs7.getLongOpt());
            }
            stringBuffer.append(" ");
            stringBuffer.append(fs7.getDescription());
            if (it.hasNext()) {
                stringBuffer.append(", ");
            }
        }
        stringBuffer.append("]");
        return stringBuffer.toString();
    }
}
