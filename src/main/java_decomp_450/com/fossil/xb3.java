package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xb3 implements Parcelable.Creator<ub3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ub3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        tb3 tb3 = null;
        String str2 = null;
        long j = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                str = j72.e(parcel, a);
            } else if (a2 == 3) {
                tb3 = (tb3) j72.a(parcel, a, tb3.CREATOR);
            } else if (a2 == 4) {
                str2 = j72.e(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                j = j72.s(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new ub3(str, tb3, str2, j);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ ub3[] newArray(int i) {
        return new ub3[i];
    }
}
