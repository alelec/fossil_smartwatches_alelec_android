package com.fossil;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl4;
import com.fossil.qq5;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sw5 extends pw5 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public static /* final */ a o; // = new a(null);
    @DexIgnore
    public LiveData<String> e; // = PortfolioApp.g0.c().d();
    @DexIgnore
    public ArrayList<Alarm> f; // = new ArrayList<>();
    @DexIgnore
    public boolean g;
    @DexIgnore
    public volatile boolean h;
    @DexIgnore
    public /* final */ qw5 i;
    @DexIgnore
    public /* final */ pd5 j;
    @DexIgnore
    public /* final */ qq5 k;
    @DexIgnore
    public /* final */ AlarmsRepository l;
    @DexIgnore
    public /* final */ ch5 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return sw5.n;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<qq5.d, qq5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ sw5 a;
        @DexIgnore
        public /* final */ /* synthetic */ Alarm b;

        @DexIgnore
        public b(sw5 sw5, Alarm alarm) {
            this.a = sw5;
            this.b = alarm;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(qq5.d dVar) {
            ee7.b(dVar, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sw5.o.a();
            local.d(a2, "enableAlarm - onSuccess: alarmUri = " + dVar.a().getUri() + ", alarmId = " + dVar.a().getId());
            this.a.i.a();
            this.a.b(this.b, true);
        }

        @DexIgnore
        public void a(qq5.b bVar) {
            ee7.b(bVar, "errorValue");
            this.a.i.a();
            int c = bVar.c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sw5.o.a();
            local.d(a2, "enableAlarm() - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.i.c();
                } else if (!(c == 1112 || c == 1113)) {
                    this.a.i.z();
                }
                this.a.b(bVar.a(), false);
                return;
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(bVar.b());
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            qw5 h = this.a.i;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                h.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                this.a.b(bVar.a(), false);
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ sw5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1", f = "HomeAlertsHybridPresenter.kt", l = {62}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $deviceId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public boolean Z$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sw5$c$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridPresenter$start$1$1$allAlarms$1", f = "HomeAlertsHybridPresenter.kt", l = {63, 68}, m = "invokeSuspend")
            /* renamed from: com.fossil.sw5$c$a$a  reason: collision with other inner class name */
            public static final class C0180a extends zb7 implements kd7<yi7, fb7<? super List<Alarm>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0180a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0180a aVar = new C0180a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<Alarm>> fb7) {
                    return ((C0180a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    yi7 yi7;
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 = this.p$;
                        pd5 b = this.this$0.this$0.a.j;
                        this.L$0 = yi7;
                        this.label = 1;
                        if (b.a(this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 = (yi7) this.L$0;
                        t87.a(obj);
                    } else if (i == 2) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.this$0.a.h) {
                        this.this$0.this$0.a.h = true;
                        pd5 b2 = this.this$0.this$0.a.j;
                        Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                        ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                        b2.d(applicationContext);
                    }
                    AlarmsRepository c = this.this$0.this$0.a.l;
                    this.L$0 = yi7;
                    this.label = 2;
                    obj = c.getAllAlarmIgnoreDeletePinType(this);
                    return obj == a ? a : obj;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class b<T> implements Comparator<T> {
                @DexIgnore
                @Override // java.util.Comparator
                public final int compare(T t, T t2) {
                    return bb7.a(Integer.valueOf(t.getTotalMinutes()), Integer.valueOf(t2.getTotalMinutes()));
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$deviceId = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$deviceId, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    boolean b2 = be5.o.e().b(this.$deviceId);
                    this.this$0.a.i.G(b2);
                    if (b2) {
                        ti7 a2 = this.this$0.a.c();
                        C0180a aVar = new C0180a(this, null);
                        this.L$0 = yi7;
                        this.Z$0 = b2;
                        this.label = 1;
                        obj2 = vh7.a(a2, aVar, this);
                        if (obj2 == a) {
                            return a;
                        }
                    }
                    sw5 sw5 = this.this$0.a;
                    sw5.g = sw5.m.I();
                    this.this$0.a.i.o(this.this$0.a.g);
                    this.this$0.a.i.B(this.this$0.a.g);
                    return i97.a;
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                    obj2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<Alarm> list = (List) obj2;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = sw5.o.a();
                local.d(a3, "GetAlarms onSuccess: size = " + list.size());
                this.this$0.a.f.clear();
                for (Alarm alarm : list) {
                    this.this$0.a.f.add(Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                }
                ArrayList d = this.this$0.a.f;
                if (d.size() > 1) {
                    aa7.a(d, new b());
                }
                this.this$0.a.i.f(this.this$0.a.f);
                sw5 sw52 = this.this$0.a;
                sw52.g = sw52.m.I();
                this.this$0.a.i.o(this.this$0.a.g);
                this.this$0.a.i.B(this.this$0.a.g);
                return i97.a;
            }
        }

        @DexIgnore
        public c(sw5 sw5) {
            this.a = sw5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            if (str == null || str.length() == 0) {
                this.a.i.a(true);
            } else {
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, str, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<String> {
        @DexIgnore
        public /* final */ /* synthetic */ sw5 a;

        @DexIgnore
        public d(sw5 sw5) {
            this.a = sw5;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(String str) {
            qw5 unused = this.a.i;
        }
    }

    /*
    static {
        String simpleName = sw5.class.getSimpleName();
        ee7.a((Object) simpleName, "HomeAlertsHybridPresenter::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public sw5(qw5 qw5, pd5 pd5, qq5 qq5, AlarmsRepository alarmsRepository, ch5 ch5) {
        ee7.b(qw5, "mView");
        ee7.b(pd5, "mAlarmHelper");
        ee7.b(qq5, "mSetAlarms");
        ee7.b(alarmsRepository, "mAlarmRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.i = qw5;
        this.j = pd5;
        this.k = qq5;
        this.l = alarmsRepository;
        this.m = ch5;
    }

    @DexIgnore
    public final void j() {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmsSuccess");
        this.j.d(PortfolioApp.g0.c());
        PortfolioApp c2 = PortfolioApp.g0.c();
        String a2 = this.e.a();
        if (a2 != null) {
            ee7.a((Object) a2, "mActiveSerial.value!!");
            c2.j(a2);
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public void k() {
        this.i.a(this);
    }

    @DexIgnore
    @k07
    public final void onSetAlarmEventEndComplete(ec5 ec5) {
        FLogger.INSTANCE.getLocal().d(n, "onSetAlarmEventEndComplete()");
        if (ec5 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "onSetAlarmEventEndComplete() - event = " + ec5);
            if (ec5.b()) {
                String a2 = ec5.a();
                Iterator<Alarm> it = this.f.iterator();
                while (it.hasNext()) {
                    Alarm next = it.next();
                    if (ee7.a((Object) next.getUri(), (Object) a2)) {
                        next.setActive(false);
                    }
                }
                this.i.t();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(n, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.k.f();
        PortfolioApp.g0.b(this);
        LiveData<String> liveData = this.e;
        qw5 qw5 = this.i;
        if (qw5 != null) {
            liveData.a((rw5) qw5, new c(this));
            nj5.d.a(CommunicateMode.SET_LIST_ALARM);
            qw5 qw52 = this.i;
            qw52.d(true ^ xg5.a(xg5.b, ((rw5) qw52).getContext(), xg5.a.NOTIFICATION_HYBRID, false, false, false, (Integer) null, 56, (Object) null));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(n, "stop");
        this.e.b(new d(this));
        this.k.g();
        PortfolioApp.g0.c(this);
    }

    @DexIgnore
    @Override // com.fossil.pw5
    public void h() {
        xg5 xg5 = xg5.b;
        qw5 qw5 = this.i;
        if (qw5 != null) {
            xg5.a(xg5, ((rw5) qw5).getContext(), xg5.a.NOTIFICATION_HYBRID, false, false, false, (Integer) null, 60, (Object) null);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.HomeAlertsHybridFragment");
    }

    @DexIgnore
    @Override // com.fossil.pw5
    public void i() {
        boolean z = !this.g;
        this.g = z;
        this.m.d(z);
        this.i.o(this.g);
        this.i.B(this.g);
    }

    @DexIgnore
    @Override // com.fossil.pw5
    public void a(Alarm alarm) {
        String a2 = this.e.a();
        if (a2 == null || a2.length() == 0) {
            FLogger.INSTANCE.getLocal().d(n, "Current Active Device Serial Is Empty");
        } else if (alarm != null || this.f.size() < 32) {
            qw5 qw5 = this.i;
            String a3 = this.e.a();
            if (a3 != null) {
                ee7.a((Object) a3, "mActiveSerial.value!!");
                qw5.a(a3, this.f, alarm);
                return;
            }
            ee7.a();
            throw null;
        } else {
            this.i.r();
        }
    }

    @DexIgnore
    public final void b(Alarm alarm, boolean z) {
        ee7.b(alarm, "editAlarm");
        Iterator<Alarm> it = this.f.iterator();
        while (it.hasNext()) {
            Alarm next = it.next();
            if (ee7.a((Object) next.getUri(), (Object) alarm.getUri())) {
                ArrayList<Alarm> arrayList = this.f;
                arrayList.set(arrayList.indexOf(next), Alarm.copy$default(alarm, null, null, null, null, 0, 0, null, false, false, null, null, 0, 4095, null));
                if (!z) {
                    break;
                }
                j();
            }
        }
        this.i.f(this.f);
    }

    @DexIgnore
    @Override // com.fossil.pw5
    public void a(Alarm alarm, boolean z) {
        ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
        String a2 = this.e.a();
        if (!(a2 == null || a2.length() == 0)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = n;
            local.d(str, "enableAlarm - alarmTotalMinues: " + alarm.getTotalMinutes() + " - enable: " + z);
            alarm.setActive(z);
            this.i.b();
            qq5 qq5 = this.k;
            String a3 = this.e.a();
            if (a3 != null) {
                ee7.a((Object) a3, "mActiveSerial.value!!");
                qq5.a(new qq5.c(a3, this.f, alarm), new b(this, alarm));
                return;
            }
            ee7.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(n, "enableAlarm - Current Active Device Serial Is Empty");
    }
}
