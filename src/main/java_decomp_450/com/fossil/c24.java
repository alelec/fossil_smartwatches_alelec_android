package com.fossil;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c24<T> {
    @DexIgnore
    public /* final */ Set<Class<? super T>> a;
    @DexIgnore
    public /* final */ Set<m24> b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ f24<T> e;
    @DexIgnore
    public /* final */ Set<Class<?>> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<T> {
        @DexIgnore
        public /* final */ Set<Class<? super T>> a;
        @DexIgnore
        public /* final */ Set<m24> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public f24<T> e;
        @DexIgnore
        public Set<Class<?>> f;

        @DexIgnore
        public c24<T> b() {
            t24.b(this.e != null, "Missing required property: factory.");
            return new c24<>(new HashSet(this.a), new HashSet(this.b), this.c, this.d, this.e, this.f);
        }

        @DexIgnore
        public b<T> c() {
            a(2);
            return this;
        }

        @DexIgnore
        public final b<T> d() {
            this.d = 1;
            return this;
        }

        @DexIgnore
        @SafeVarargs
        public b(Class<T> cls, Class<? super T>... clsArr) {
            this.a = new HashSet();
            this.b = new HashSet();
            this.c = 0;
            this.d = 0;
            this.f = new HashSet();
            t24.a(cls, "Null interface");
            this.a.add(cls);
            for (Class<? super T> cls2 : clsArr) {
                t24.a(cls2, "Null interface");
            }
            Collections.addAll(this.a, clsArr);
        }

        @DexIgnore
        public b<T> a(m24 m24) {
            t24.a(m24, "Null dependency");
            a(m24.a());
            this.b.add(m24);
            return this;
        }

        @DexIgnore
        public b<T> a() {
            a(1);
            return this;
        }

        @DexIgnore
        public final b<T> a(int i) {
            t24.b(this.c == 0, "Instantiation type has already been set.");
            this.c = i;
            return this;
        }

        @DexIgnore
        public final void a(Class<?> cls) {
            t24.a(!this.a.contains(cls), "Components are not allowed to depend on interfaces they themselves provide.");
        }

        @DexIgnore
        public b<T> a(f24<T> f24) {
            t24.a(f24, "Null factory");
            this.e = f24;
            return this;
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(Object obj, d24 d24) {
        return obj;
    }

    @DexIgnore
    public static /* synthetic */ Object b(Object obj, d24 d24) {
        return obj;
    }

    @DexIgnore
    public Set<m24> a() {
        return this.b;
    }

    @DexIgnore
    public f24<T> b() {
        return this.e;
    }

    @DexIgnore
    public Set<Class<? super T>> c() {
        return this.a;
    }

    @DexIgnore
    public Set<Class<?>> d() {
        return this.f;
    }

    @DexIgnore
    public boolean e() {
        return this.c == 1;
    }

    @DexIgnore
    public boolean f() {
        return this.c == 2;
    }

    @DexIgnore
    public boolean g() {
        return this.d == 0;
    }

    @DexIgnore
    public String toString() {
        return "Component<" + Arrays.toString(this.a.toArray()) + ">{" + this.c + ", type=" + this.d + ", deps=" + Arrays.toString(this.b.toArray()) + "}";
    }

    @DexIgnore
    public c24(Set<Class<? super T>> set, Set<m24> set2, int i, int i2, f24<T> f24, Set<Class<?>> set3) {
        this.a = Collections.unmodifiableSet(set);
        this.b = Collections.unmodifiableSet(set2);
        this.c = i;
        this.d = i2;
        this.e = f24;
        this.f = Collections.unmodifiableSet(set3);
    }

    @DexIgnore
    public static <T> b<T> a(Class<T> cls) {
        return new b<>(cls, new Class[0]);
    }

    @DexIgnore
    public static <T> b<T> b(Class<T> cls) {
        b<T> a2 = a(cls);
        b unused = a2.d();
        return a2;
    }

    @DexIgnore
    @SafeVarargs
    public static <T> b<T> a(Class<T> cls, Class<? super T>... clsArr) {
        return new b<>(cls, clsArr);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> c24<T> a(T t, Class<T> cls, Class<? super T>... clsArr) {
        b a2 = a(cls, clsArr);
        a2.a(a24.a((Object) t));
        return a2.b();
    }

    @DexIgnore
    public static <T> c24<T> a(T t, Class<T> cls) {
        b b2 = b(cls);
        b2.a(b24.a((Object) t));
        return b2.b();
    }
}
