package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i13 implements tr2<l13> {
    @DexIgnore
    public static i13 b; // = new i13();
    @DexIgnore
    public /* final */ tr2<l13> a;

    @DexIgnore
    public i13(tr2<l13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((l13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((l13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ l13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public i13() {
        this(sr2.a(new k13()));
    }
}
