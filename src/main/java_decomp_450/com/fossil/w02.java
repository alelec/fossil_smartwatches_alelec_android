package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w02 extends Exception {
    @DexIgnore
    @Deprecated
    public /* final */ Status mStatus;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public w02(com.google.android.gms.common.api.Status r5) {
        /*
            r4 = this;
            int r0 = r5.g()
            java.lang.String r1 = r5.v()
            if (r1 == 0) goto L_0x000f
            java.lang.String r1 = r5.v()
            goto L_0x0011
        L_0x000f:
            java.lang.String r1 = ""
        L_0x0011:
            java.lang.String r2 = java.lang.String.valueOf(r1)
            int r2 = r2.length()
            int r2 = r2 + 13
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r2)
            r3.append(r0)
            java.lang.String r0 = ": "
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = r3.toString()
            r4.<init>(r0)
            r4.mStatus = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.w02.<init>(com.google.android.gms.common.api.Status):void");
    }

    @DexIgnore
    public Status getStatus() {
        return this.mStatus;
    }

    @DexIgnore
    public int getStatusCode() {
        return this.mStatus.g();
    }

    @DexIgnore
    @Deprecated
    public String getStatusMessage() {
        return this.mStatus.v();
    }
}
