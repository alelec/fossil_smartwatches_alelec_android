package com.fossil;

import android.content.Context;
import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.v54;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s44 {
    @DexIgnore
    public /* final */ a44 a;
    @DexIgnore
    public /* final */ u64 b;
    @DexIgnore
    public /* final */ o74 c;
    @DexIgnore
    public /* final */ x44 d;
    @DexIgnore
    public /* final */ u44 e;
    @DexIgnore
    public String f;

    @DexIgnore
    public s44(a44 a44, u64 u64, o74 o74, x44 x44, u44 u44) {
        this.a = a44;
        this.b = u64;
        this.c = o74;
        this.d = x44;
        this.e = u44;
    }

    @DexIgnore
    public static s44 a(Context context, j44 j44, v64 v64, n34 n34, x44 x44, u44 u44, m84 m84, t74 t74) {
        return new s44(new a44(context, j44, n34, m84), new u64(new File(v64.a()), t74), o74.a(context), x44, u44);
    }

    @DexIgnore
    public void b() {
        String str = this.f;
        if (str == null) {
            z24.a().a("Could not persist user ID; no current session");
            return;
        }
        String b2 = this.e.b();
        if (b2 == null) {
            z24.a().a("Could not persist user ID; no user ID available");
        } else {
            this.b.a(b2, str);
        }
    }

    @DexIgnore
    public void c() {
        this.b.b();
    }

    @DexIgnore
    public void a(String str, long j) {
        this.f = str;
        this.b.a(this.a.a(str, j));
    }

    @DexIgnore
    public void a() {
        this.f = null;
    }

    @DexIgnore
    public void a(Throwable th, Thread thread, long j) {
        a(th, thread, CrashDumperPlugin.NAME, j, true);
    }

    @DexIgnore
    public void a(String str, List<n44> list) {
        ArrayList arrayList = new ArrayList();
        for (n44 n44 : list) {
            v54.c.b c2 = n44.c();
            if (c2 != null) {
                arrayList.add(c2);
            }
        }
        u64 u64 = this.b;
        v54.c.a c3 = v54.c.c();
        c3.a(w54.a(arrayList));
        u64.a(str, c3.a());
    }

    @DexIgnore
    public void a(long j) {
        this.b.a(this.f, j);
    }

    @DexIgnore
    public no3<Void> a(Executor executor, f44 f44) {
        if (f44 == f44.NONE) {
            z24.a().a("Send via DataTransport disabled. Removing DataTransport reports.");
            this.b.b();
            return qo3.a((Object) null);
        }
        List<b44> d2 = this.b.d();
        ArrayList arrayList = new ArrayList();
        for (b44 b44 : d2) {
            if (b44.a().i() != v54.e.NATIVE || f44 == f44.ALL) {
                arrayList.add(this.c.a(b44).a(executor, q44.a(this)));
            } else {
                z24.a().a("Send native reports via DataTransport disabled. Removing DataTransport reports.");
                this.b.b(b44.b());
            }
        }
        return qo3.a((Collection<? extends no3<?>>) arrayList);
    }

    @DexIgnore
    public final void a(Throwable th, Thread thread, String str, long j, boolean z) {
        String str2 = this.f;
        if (str2 == null) {
            z24.a().a("Cannot persist event, no currently open session");
            return;
        }
        boolean equals = str.equals(CrashDumperPlugin.NAME);
        v54.d.AbstractC0206d a2 = this.a.a(th, thread, str, j, 4, 8, z);
        v54.d.AbstractC0206d.b f2 = a2.f();
        String c2 = this.d.c();
        if (c2 != null) {
            v54.d.AbstractC0206d.AbstractC0217d.a b2 = v54.d.AbstractC0206d.AbstractC0217d.b();
            b2.a(c2);
            f2.a(b2.a());
        } else {
            z24.a().a("No log data to include with this event.");
        }
        List<v54.b> a3 = a(this.e.a());
        if (!a3.isEmpty()) {
            v54.d.AbstractC0206d.a.AbstractC0207a e2 = a2.a().e();
            e2.a(w54.a(a3));
            f2.a(e2.a());
        }
        this.b.a(f2.a(), str2, equals);
    }

    @DexIgnore
    public final boolean a(no3<b44> no3) {
        if (no3.e()) {
            b44 b2 = no3.b();
            z24 a2 = z24.a();
            a2.a("Crashlytics report successfully enqueued to DataTransport: " + b2.b());
            this.b.b(b2.b());
            return true;
        }
        z24.a().a("Crashlytics report could not be enqueued to DataTransport", no3.a());
        return false;
    }

    @DexIgnore
    public static List<v54.b> a(Map<String, String> map) {
        ArrayList arrayList = new ArrayList();
        arrayList.ensureCapacity(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            v54.b.a c2 = v54.b.c();
            c2.a(entry.getKey());
            c2.b(entry.getValue());
            arrayList.add(c2.a());
        }
        Collections.sort(arrayList, r44.a());
        return arrayList;
    }
}
