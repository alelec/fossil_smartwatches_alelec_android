package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pz1 extends IInterface {
    @DexIgnore
    void a(nz1 nz1, GoogleSignInOptions googleSignInOptions) throws RemoteException;

    @DexIgnore
    void b(nz1 nz1, GoogleSignInOptions googleSignInOptions) throws RemoteException;
}
