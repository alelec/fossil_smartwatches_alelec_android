package com.fossil;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ka2 implements Parcelable.Creator<i02> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ i02 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        PendingIntent pendingIntent = null;
        String str = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 == 2) {
                i2 = j72.q(parcel, a);
            } else if (a2 == 3) {
                pendingIntent = (PendingIntent) j72.a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 4) {
                j72.v(parcel, a);
            } else {
                str = j72.e(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new i02(i, i2, pendingIntent, str);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ i02[] newArray(int i) {
        return new i02[i];
    }
}
