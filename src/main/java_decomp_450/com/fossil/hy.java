package com.fossil;

import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hy<DataType, ResourceType, Transcode> {
    @DexIgnore
    public /* final */ Class<DataType> a;
    @DexIgnore
    public /* final */ List<? extends cx<DataType, ResourceType>> b;
    @DexIgnore
    public /* final */ f30<ResourceType, Transcode> c;
    @DexIgnore
    public /* final */ b9<List<Throwable>> d;
    @DexIgnore
    public /* final */ String e;

    @DexIgnore
    public interface a<ResourceType> {
        @DexIgnore
        uy<ResourceType> a(uy<ResourceType> uyVar);
    }

    @DexIgnore
    public hy(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends cx<DataType, ResourceType>> list, f30<ResourceType, Transcode> f30, b9<List<Throwable>> b9Var) {
        this.a = cls;
        this.b = list;
        this.c = f30;
        this.d = b9Var;
        this.e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    @DexIgnore
    public uy<Transcode> a(jx<DataType> jxVar, int i, int i2, ax axVar, a<ResourceType> aVar) throws py {
        return this.c.a(aVar.a(a(jxVar, i, i2, axVar)), axVar);
    }

    @DexIgnore
    public String toString() {
        return "DecodePath{ dataClass=" + this.a + ", decoders=" + this.b + ", transcoder=" + this.c + '}';
    }

    @DexIgnore
    public final uy<ResourceType> a(jx<DataType> jxVar, int i, int i2, ax axVar) throws py {
        List<Throwable> a2 = this.d.a();
        u50.a((Object) a2);
        List<Throwable> list = a2;
        try {
            return a(jxVar, i, i2, axVar, list);
        } finally {
            this.d.a(list);
        }
    }

    @DexIgnore
    public final uy<ResourceType> a(jx<DataType> jxVar, int i, int i2, ax axVar, List<Throwable> list) throws py {
        int size = this.b.size();
        uy<ResourceType> uyVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            cx cxVar = (cx) this.b.get(i3);
            try {
                if (cxVar.a(jxVar.b(), axVar)) {
                    uyVar = cxVar.a(jxVar.b(), i, i2, axVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e2) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + cxVar, e2);
                }
                list.add(e2);
            }
            if (uyVar != null) {
                break;
            }
        }
        if (uyVar != null) {
            return uyVar;
        }
        throw new py(this.e, new ArrayList(list));
    }
}
