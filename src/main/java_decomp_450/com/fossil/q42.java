package com.fossil;

import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q42 implements r42 {
    @DexIgnore
    public /* final */ /* synthetic */ n42 a;

    @DexIgnore
    public q42(n42 n42) {
        this.a = n42;
    }

    @DexIgnore
    @Override // com.fossil.r42
    public final void a(BasePendingResult<?> basePendingResult) {
        this.a.a.remove(basePendingResult);
        basePendingResult.e();
    }
}
