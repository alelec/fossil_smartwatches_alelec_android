package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import androidx.lifecycle.LiveData;
import com.baseflow.geolocator.utils.LocaleConverter;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zh {
    @DexIgnore
    public static /* final */ String[] m; // = {"UPDATE", "DELETE", "INSERT"};
    @DexIgnore
    public /* final */ HashMap<String, Integer> a;
    @DexIgnore
    public /* final */ String[] b;
    @DexIgnore
    public Map<String, Set<String>> c;
    @DexIgnore
    public /* final */ ci d;
    @DexIgnore
    public AtomicBoolean e; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile boolean f; // = false;
    @DexIgnore
    public volatile aj g;
    @DexIgnore
    public b h;
    @DexIgnore
    public /* final */ yh i;
    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public /* final */ s3<c, d> j; // = new s3<>();
    @DexIgnore
    public ai k;
    @DexIgnore
    public Runnable l; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX INFO: finally extract failed */
        public final Set<Integer> a() {
            HashSet hashSet = new HashSet();
            Cursor query = zh.this.d.query(new vi("SELECT * FROM room_table_modification_log WHERE invalidated = 1;"));
            while (query.moveToNext()) {
                try {
                    hashSet.add(Integer.valueOf(query.getInt(0)));
                } catch (Throwable th) {
                    query.close();
                    throw th;
                }
            }
            query.close();
            if (!hashSet.isEmpty()) {
                zh.this.g.executeUpdateDelete();
            }
            return hashSet;
        }

        @DexIgnore
        public void run() {
            Lock closeLock = zh.this.d.getCloseLock();
            Set<Integer> set = null;
            try {
                closeLock.lock();
                if (!zh.this.a()) {
                    closeLock.unlock();
                } else if (!zh.this.e.compareAndSet(true, false)) {
                    closeLock.unlock();
                } else if (zh.this.d.inTransaction()) {
                    closeLock.unlock();
                } else {
                    if (zh.this.d.mWriteAheadLoggingEnabled) {
                        wi writableDatabase = zh.this.d.getOpenHelper().getWritableDatabase();
                        writableDatabase.beginTransaction();
                        try {
                            set = a();
                            writableDatabase.setTransactionSuccessful();
                        } finally {
                            writableDatabase.endTransaction();
                        }
                    } else {
                        set = a();
                    }
                    closeLock.unlock();
                    if (set != null && !set.isEmpty()) {
                        synchronized (zh.this.j) {
                            Iterator<Map.Entry<c, d>> it = zh.this.j.iterator();
                            while (it.hasNext()) {
                                it.next().getValue().a(set);
                            }
                        }
                    }
                }
            } catch (SQLiteException | IllegalStateException e) {
                Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e);
            } catch (Throwable th) {
                closeLock.unlock();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends c {
        @DexIgnore
        public /* final */ zh a;
        @DexIgnore
        public /* final */ WeakReference<c> b;

        @DexIgnore
        public e(zh zhVar, c cVar) {
            super(cVar.mTables);
            this.a = zhVar;
            this.b = new WeakReference<>(cVar);
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            c cVar = this.b.get();
            if (cVar == null) {
                this.a.c(this);
            } else {
                cVar.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public zh(ci ciVar, Map<String, String> map, Map<String, Set<String>> map2, String... strArr) {
        this.d = ciVar;
        this.h = new b(strArr.length);
        this.a = new HashMap<>();
        this.c = map2;
        this.i = new yh(this.d);
        int length = strArr.length;
        this.b = new String[length];
        for (int i2 = 0; i2 < length; i2++) {
            String lowerCase = strArr[i2].toLowerCase(Locale.US);
            this.a.put(lowerCase, Integer.valueOf(i2));
            String str = map.get(strArr[i2]);
            if (str != null) {
                this.b[i2] = str.toLowerCase(Locale.US);
            } else {
                this.b[i2] = lowerCase;
            }
        }
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String lowerCase2 = entry.getValue().toLowerCase(Locale.US);
            if (this.a.containsKey(lowerCase2)) {
                String lowerCase3 = entry.getKey().toLowerCase(Locale.US);
                HashMap<String, Integer> hashMap = this.a;
                hashMap.put(lowerCase3, hashMap.get(lowerCase2));
            }
        }
    }

    @DexIgnore
    public void a(wi wiVar) {
        synchronized (this) {
            if (this.f) {
                Log.e("ROOM", "Invalidation tracker is initialized twice :/.");
                return;
            }
            wiVar.execSQL("PRAGMA temp_store = MEMORY;");
            wiVar.execSQL("PRAGMA recursive_triggers='ON';");
            wiVar.execSQL("CREATE TEMP TABLE room_table_modification_log(table_id INTEGER PRIMARY KEY, invalidated INTEGER NOT NULL DEFAULT 0)");
            b(wiVar);
            this.g = wiVar.compileStatement("UPDATE room_table_modification_log SET invalidated = 0 WHERE invalidated = 1 ");
            this.f = true;
        }
    }

    @DexIgnore
    public final void b(wi wiVar, int i2) {
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        String[] strArr = m;
        for (String str2 : strArr) {
            sb.setLength(0);
            sb.append("DROP TRIGGER IF EXISTS ");
            a(sb, str, str2);
            wiVar.execSQL(sb.toString());
        }
    }

    @DexIgnore
    public final String[] c(String[] strArr) {
        String[] b2 = b(strArr);
        int length = b2.length;
        int i2 = 0;
        while (i2 < length) {
            String str = b2[i2];
            if (this.a.containsKey(str.toLowerCase(Locale.US))) {
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + str);
            }
        }
        return b2;
    }

    @DexIgnore
    public void d() {
        ai aiVar = this.k;
        if (aiVar != null) {
            aiVar.a();
            this.k = null;
        }
    }

    @DexIgnore
    public void e() {
        if (this.d.isOpen()) {
            b(this.d.getOpenHelper().getWritableDatabase());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c {
        @DexIgnore
        public /* final */ String[] mTables;

        @DexIgnore
        public c(String str, String... strArr) {
            String[] strArr2 = (String[]) Arrays.copyOf(strArr, strArr.length + 1);
            this.mTables = strArr2;
            strArr2[strArr.length] = str;
        }

        @DexIgnore
        public boolean isRemote() {
            return false;
        }

        @DexIgnore
        public abstract void onInvalidated(Set<String> set);

        @DexIgnore
        public c(String[] strArr) {
            this.mTables = (String[]) Arrays.copyOf(strArr, strArr.length);
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void c(c cVar) {
        d remove;
        synchronized (this.j) {
            remove = this.j.remove(cVar);
        }
        if (remove != null && this.h.b(remove.a)) {
            e();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ long[] a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b(int i) {
            long[] jArr = new long[i];
            this.a = jArr;
            this.b = new boolean[i];
            this.c = new int[i];
            Arrays.fill(jArr, 0L);
            Arrays.fill(this.b, false);
        }

        @DexIgnore
        public boolean a(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = 1 + j;
                    if (j == 0) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public boolean b(int... iArr) {
            boolean z;
            synchronized (this) {
                z = false;
                for (int i : iArr) {
                    long j = this.a[i];
                    this.a[i] = j - 1;
                    if (j == 1) {
                        this.d = true;
                        z = true;
                    }
                }
            }
            return z;
        }

        @DexIgnore
        public int[] a() {
            synchronized (this) {
                if (this.d) {
                    if (!this.e) {
                        int length = this.a.length;
                        int i = 0;
                        while (true) {
                            int i2 = 1;
                            if (i < length) {
                                boolean z = this.a[i] > 0;
                                if (z != this.b[i]) {
                                    int[] iArr = this.c;
                                    if (!z) {
                                        i2 = 2;
                                    }
                                    iArr[i] = i2;
                                } else {
                                    this.c[i] = 0;
                                }
                                this.b[i] = z;
                                i++;
                            } else {
                                this.e = true;
                                this.d = false;
                                return this.c;
                            }
                        }
                    }
                }
                return null;
            }
        }

        @DexIgnore
        public void b() {
            synchronized (this) {
                this.e = false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ String[] b;
        @DexIgnore
        public /* final */ c c;
        @DexIgnore
        public /* final */ Set<String> d;

        @DexIgnore
        public d(c cVar, int[] iArr, String[] strArr) {
            this.c = cVar;
            this.a = iArr;
            this.b = strArr;
            if (iArr.length == 1) {
                HashSet hashSet = new HashSet();
                hashSet.add(this.b[0]);
                this.d = Collections.unmodifiableSet(hashSet);
                return;
            }
            this.d = null;
        }

        @DexIgnore
        public void a(Set<Integer> set) {
            int length = this.a.length;
            Set<String> set2 = null;
            for (int i = 0; i < length; i++) {
                if (set.contains(Integer.valueOf(this.a[i]))) {
                    if (length == 1) {
                        set2 = this.d;
                    } else {
                        if (set2 == null) {
                            set2 = new HashSet<>(length);
                        }
                        set2.add(this.b[i]);
                    }
                }
            }
            if (set2 != null) {
                this.c.onInvalidated(set2);
            }
        }

        @DexIgnore
        public void a(String[] strArr) {
            Set<String> set = null;
            if (this.b.length == 1) {
                int length = strArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (strArr[i].equalsIgnoreCase(this.b[0])) {
                        set = this.d;
                        break;
                    } else {
                        i++;
                    }
                }
            } else {
                HashSet hashSet = new HashSet();
                for (String str : strArr) {
                    String[] strArr2 = this.b;
                    int length2 = strArr2.length;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length2) {
                            break;
                        }
                        String str2 = strArr2[i2];
                        if (str2.equalsIgnoreCase(str)) {
                            hashSet.add(str2);
                            break;
                        }
                        i2++;
                    }
                }
                if (hashSet.size() > 0) {
                    set = hashSet;
                }
            }
            if (set != null) {
                this.c.onInvalidated(set);
            }
        }
    }

    @DexIgnore
    public final String[] b(String[] strArr) {
        HashSet hashSet = new HashSet();
        for (String str : strArr) {
            String lowerCase = str.toLowerCase(Locale.US);
            if (this.c.containsKey(lowerCase)) {
                hashSet.addAll(this.c.get(lowerCase));
            } else {
                hashSet.add(str);
            }
        }
        return (String[]) hashSet.toArray(new String[hashSet.size()]);
    }

    @DexIgnore
    public void c() {
        e();
        this.l.run();
    }

    @DexIgnore
    public void a(Context context, String str) {
        this.k = new ai(context, str, this, this.d.getQueryExecutor());
    }

    @DexIgnore
    public static void a(StringBuilder sb, String str, String str2) {
        sb.append("`");
        sb.append("room_table_modification_trigger_");
        sb.append(str);
        sb.append(LocaleConverter.LOCALE_DELIMITER);
        sb.append(str2);
        sb.append("`");
    }

    @DexIgnore
    public void b(c cVar) {
        a(new e(this, cVar));
    }

    @DexIgnore
    public void b() {
        if (this.e.compareAndSet(false, true)) {
            this.d.getQueryExecutor().execute(this.l);
        }
    }

    @DexIgnore
    public void b(wi wiVar) {
        if (!wiVar.inTransaction()) {
            while (true) {
                try {
                    Lock closeLock = this.d.getCloseLock();
                    closeLock.lock();
                    try {
                        int[] a2 = this.h.a();
                        if (a2 == null) {
                            closeLock.unlock();
                            return;
                        }
                        int length = a2.length;
                        wiVar.beginTransaction();
                        int i2 = 0;
                        while (i2 < length) {
                            try {
                                int i3 = a2[i2];
                                if (i3 == 1) {
                                    a(wiVar, i2);
                                } else if (i3 == 2) {
                                    b(wiVar, i2);
                                }
                                i2++;
                            } catch (Throwable th) {
                                wiVar.endTransaction();
                                throw th;
                            }
                        }
                        wiVar.setTransactionSuccessful();
                        wiVar.endTransaction();
                        this.h.b();
                    } finally {
                        closeLock.unlock();
                    }
                } catch (SQLiteException | IllegalStateException e2) {
                    Log.e("ROOM", "Cannot run invalidation tracker. Is the db closed?", e2);
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void a(wi wiVar, int i2) {
        wiVar.execSQL("INSERT OR IGNORE INTO room_table_modification_log VALUES(" + i2 + ", 0)");
        String str = this.b[i2];
        StringBuilder sb = new StringBuilder();
        String[] strArr = m;
        for (String str2 : strArr) {
            sb.setLength(0);
            sb.append("CREATE TEMP TRIGGER IF NOT EXISTS ");
            a(sb, str, str2);
            sb.append(" AFTER ");
            sb.append(str2);
            sb.append(" ON `");
            sb.append(str);
            sb.append("` BEGIN UPDATE ");
            sb.append("room_table_modification_log");
            sb.append(" SET ");
            sb.append("invalidated");
            sb.append(" = 1");
            sb.append(" WHERE ");
            sb.append("table_id");
            sb.append(" = ");
            sb.append(i2);
            sb.append(" AND ");
            sb.append("invalidated");
            sb.append(" = 0");
            sb.append("; END");
            wiVar.execSQL(sb.toString());
        }
    }

    @DexIgnore
    @SuppressLint({"RestrictedApi"})
    public void a(c cVar) {
        d b2;
        String[] b3 = b(cVar.mTables);
        int[] iArr = new int[b3.length];
        int length = b3.length;
        int i2 = 0;
        while (i2 < length) {
            Integer num = this.a.get(b3[i2].toLowerCase(Locale.US));
            if (num != null) {
                iArr[i2] = num.intValue();
                i2++;
            } else {
                throw new IllegalArgumentException("There is no table with name " + b3[i2]);
            }
        }
        d dVar = new d(cVar, iArr, b3);
        synchronized (this.j) {
            b2 = this.j.b(cVar, dVar);
        }
        if (b2 == null && this.h.a(iArr)) {
            e();
        }
    }

    @DexIgnore
    public boolean a() {
        if (!this.d.isOpen()) {
            return false;
        }
        if (!this.f) {
            this.d.getOpenHelper().getWritableDatabase();
        }
        if (this.f) {
            return true;
        }
        Log.e("ROOM", "database is not initialized even though it is open");
        return false;
    }

    @DexIgnore
    public void a(String... strArr) {
        synchronized (this.j) {
            Iterator<Map.Entry<c, d>> it = this.j.iterator();
            while (it.hasNext()) {
                Map.Entry<c, d> next = it.next();
                if (!next.getKey().isRemote()) {
                    next.getValue().a(strArr);
                }
            }
        }
    }

    @DexIgnore
    public <T> LiveData<T> a(String[] strArr, boolean z, Callable<T> callable) {
        return this.i.a(c(strArr), z, callable);
    }
}
