package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import com.portfolio.platform.view.ProgressButton;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.ruler.RulerValuePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g55 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout C;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout D;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ FossilCircleImageView G;
    @DexIgnore
    public /* final */ ConstraintLayout H;
    @DexIgnore
    public /* final */ RulerValuePicker I;
    @DexIgnore
    public /* final */ RulerValuePicker J;
    @DexIgnore
    public /* final */ ProgressButton K;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ FossilCircleImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText s;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText t;
    @DexIgnore
    public /* final */ FlexibleButton u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public g55(Object obj, View view, int i, FossilCircleImageView fossilCircleImageView, ConstraintLayout constraintLayout, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, RTLImageView rTLImageView, FossilCircleImageView fossilCircleImageView2, ConstraintLayout constraintLayout2, RulerValuePicker rulerValuePicker, RulerValuePicker rulerValuePicker2, ProgressButton progressButton, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextView flexibleTextView6) {
        super(obj, view, i);
        this.q = fossilCircleImageView;
        this.r = constraintLayout;
        this.s = flexibleTextInputEditText;
        this.t = flexibleTextInputEditText2;
        this.u = flexibleButton;
        this.v = flexibleButton2;
        this.w = flexibleButton3;
        this.x = flexibleTextView;
        this.y = flexibleTextView2;
        this.z = flexibleTextView3;
        this.A = flexibleTextView4;
        this.B = flexibleTextView5;
        this.C = flexibleTextInputLayout;
        this.D = flexibleTextInputLayout2;
        this.E = flexibleTextInputLayout3;
        this.F = rTLImageView;
        this.G = fossilCircleImageView2;
        this.H = constraintLayout2;
        this.I = rulerValuePicker;
        this.J = rulerValuePicker2;
        this.K = progressButton;
        this.L = flexibleTextInputEditText3;
        this.M = flexibleTextView6;
    }
}
