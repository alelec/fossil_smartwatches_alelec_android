package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dn1 extends jr1 {
    @DexIgnore
    public /* final */ cf0 S;
    @DexIgnore
    public /* final */ pb1 T;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ dn1(ri1 ri1, en0 en0, cf0 cf0, pb1 pb1, String str, int i) {
        super(ri1, en0, wm0.U, true, gq0.b.b(ri1.u, pb1), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, true, 32);
        boolean z = cf0 instanceof pf0;
        this.S = cf0;
        this.T = pb1;
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.X2, this.S.a());
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public byte[] n() {
        cf0 cf0 = this.S;
        short b = gq0.b.b(((zk0) this).w.u, this.T);
        r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(this.T.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        return cf0.a(b, r60);
    }
}
