package com.fossil;

import android.content.Context;
import com.fossil.sm4;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm4 implements sm4 {
    @DexIgnore
    public /* final */ List<String> a; // = w97.d("Agree terms of use and privacy");

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean a() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean a(List<String> list) {
        ee7.b(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean b() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean c() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean d() {
        return !ee7.a((Object) h(), (Object) "CN");
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean e() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean f() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean g() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public String h() {
        return sm4.a.a(this);
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean i() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean j() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean k() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public boolean l() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.sm4
    public void a(go5 go5) {
        ee7.b(go5, "fragment");
        HelpActivity.a aVar = HelpActivity.z;
        Context requireContext = go5.requireContext();
        ee7.a((Object) requireContext, "fragment.requireContext()");
        aVar.a(requireContext);
    }
}
