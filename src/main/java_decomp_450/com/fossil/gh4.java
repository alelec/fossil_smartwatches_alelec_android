package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh4 {
    @DexIgnore
    public /* final */ fh4 a;
    @DexIgnore
    public /* final */ int[] b;

    @DexIgnore
    public gh4(fh4 fh4, int[] iArr) {
        if (iArr.length != 0) {
            this.a = fh4;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.b = new int[]{0};
                return;
            }
            int[] iArr2 = new int[(length - i)];
            this.b = iArr2;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public int[] a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.b.length - 1;
    }

    @DexIgnore
    public boolean c() {
        return this.b[0] == 0;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(b() * 8);
        for (int b2 = b(); b2 >= 0; b2--) {
            int a2 = a(b2);
            if (a2 != 0) {
                if (a2 < 0) {
                    sb.append(" - ");
                    a2 = -a2;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (b2 == 0 || a2 != 1) {
                    int c = this.a.c(a2);
                    if (c == 0) {
                        sb.append('1');
                    } else if (c == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(c);
                    }
                }
                if (b2 != 0) {
                    if (b2 == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(b2);
                    }
                }
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public int a(int i) {
        int[] iArr = this.b;
        return iArr[(iArr.length - 1) - i];
    }

    @DexIgnore
    public gh4[] b(gh4 gh4) {
        if (!this.a.equals(gh4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!gh4.c()) {
            gh4 b2 = this.a.b();
            int b3 = this.a.b(gh4.a(gh4.b()));
            gh4 gh42 = this;
            while (gh42.b() >= gh4.b() && !gh42.c()) {
                int b4 = gh42.b() - gh4.b();
                int b5 = this.a.b(gh42.a(gh42.b()), b3);
                gh4 a2 = gh4.a(b4, b5);
                b2 = b2.a(this.a.a(b4, b5));
                gh42 = gh42.a(a2);
            }
            return new gh4[]{b2, gh42};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    @DexIgnore
    public gh4 c(gh4 gh4) {
        if (!this.a.equals(gh4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c() || gh4.c()) {
            return this.a.b();
        } else {
            int[] iArr = this.b;
            int length = iArr.length;
            int[] iArr2 = gh4.b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = fh4.c(iArr3[i4], this.a.b(i2, iArr2[i3]));
                }
            }
            return new gh4(this.a, iArr3);
        }
    }

    @DexIgnore
    public gh4 a(gh4 gh4) {
        if (!this.a.equals(gh4.a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (c()) {
            return gh4;
        } else {
            if (gh4.c()) {
                return this;
            }
            int[] iArr = this.b;
            int[] iArr2 = gh4.b;
            if (iArr.length <= iArr2.length) {
                iArr = iArr2;
                iArr2 = iArr;
            }
            int[] iArr3 = new int[iArr.length];
            int length = iArr.length - iArr2.length;
            System.arraycopy(iArr, 0, iArr3, 0, length);
            for (int i = length; i < iArr.length; i++) {
                iArr3[i] = fh4.c(iArr2[i - length], iArr[i]);
            }
            return new gh4(this.a, iArr3);
        }
    }

    @DexIgnore
    public gh4 a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.a.b();
        } else {
            int length = this.b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.a.b(this.b[i3], i2);
            }
            return new gh4(this.a, iArr);
        }
    }
}
