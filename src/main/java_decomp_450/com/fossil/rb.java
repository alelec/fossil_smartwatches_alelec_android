package com.fossil;

import android.util.Log;
import android.view.View;
import androidx.databinding.ViewDataBinding;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class rb extends nb {
    @DexIgnore
    public Set<Class<? extends nb>> a; // = new HashSet();
    @DexIgnore
    public List<nb> b; // = new CopyOnWriteArrayList();
    @DexIgnore
    public List<String> c; // = new CopyOnWriteArrayList();

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: java.lang.Class<?> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Set<java.lang.Class<? extends com.fossil.nb>> */
    /* JADX WARN: Multi-variable type inference failed */
    public void a(nb nbVar) {
        if (this.a.add(nbVar.getClass())) {
            this.b.add(nbVar);
            for (nb nbVar2 : nbVar.a()) {
                a(nbVar2);
            }
        }
    }

    @DexIgnore
    public final boolean b() {
        boolean z = false;
        for (String str : this.c) {
            try {
                Class<?> cls = Class.forName(str);
                if (nb.class.isAssignableFrom(cls)) {
                    a((nb) cls.newInstance());
                    this.c.remove(str);
                    z = true;
                }
            } catch (ClassNotFoundException unused) {
            } catch (IllegalAccessException e) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + str, e);
            } catch (InstantiationException e2) {
                Log.e("MergedDataBinderMapper", "unable to add feature mapper for " + str, e2);
            }
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.nb
    public ViewDataBinding a(pb pbVar, View view, int i) {
        for (nb nbVar : this.b) {
            ViewDataBinding a2 = nbVar.a(pbVar, view, i);
            if (a2 != null) {
                return a2;
            }
        }
        if (b()) {
            return a(pbVar, view, i);
        }
        return null;
    }

    @DexIgnore
    @Override // com.fossil.nb
    public ViewDataBinding a(pb pbVar, View[] viewArr, int i) {
        for (nb nbVar : this.b) {
            ViewDataBinding a2 = nbVar.a(pbVar, viewArr, i);
            if (a2 != null) {
                return a2;
            }
        }
        if (b()) {
            return a(pbVar, viewArr, i);
        }
        return null;
    }
}
