package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hl2 extends i72 implements i12 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<hl2> CREATOR; // = new il2();
    @DexIgnore
    public /* final */ Status a;

    /*
    static {
        new hl2(Status.e);
    }
    */

    @DexIgnore
    public hl2(Status status) {
        this.a = status;
    }

    @DexIgnore
    @Override // com.fossil.i12
    public final Status a() {
        return this.a;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) a(), i, false);
        k72.a(parcel, a2);
    }
}
