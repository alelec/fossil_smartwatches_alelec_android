package com.fossil;

import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w97 extends v97 {
    @DexIgnore
    public static final <T> List<T> a() {
        return ga7.INSTANCE;
    }

    @DexIgnore
    public static final <T> Collection<T> b(T[] tArr) {
        ee7.b(tArr, "$this$asCollection");
        return new p97(tArr, false);
    }

    @DexIgnore
    public static final <T> List<T> c(T... tArr) {
        ee7.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length > 0 ? s97.b(tArr) : a();
    }

    @DexIgnore
    public static final <T> List<T> d(T... tArr) {
        ee7.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new p97(tArr, true));
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(T... tArr) {
        ee7.b(tArr, MessengerShareContentUtility.ELEMENTS);
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new p97(tArr, true));
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends T> */
    /* JADX WARN: Multi-variable type inference failed */
    public static final <T> List<T> b(List<? extends T> list) {
        ee7.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return list;
        }
        return v97.a(list.get(0));
    }

    @DexIgnore
    public static final void c() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    @DexIgnore
    public static final lf7 a(Collection<?> collection) {
        ee7.b(collection, "$this$indices");
        return new lf7(0, collection.size() - 1);
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list) {
        ee7.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    @DexIgnore
    public static final void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }
}
