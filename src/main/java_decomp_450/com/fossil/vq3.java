package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vq3 implements Parcelable.Creator<uq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        int i = 0;
        byte b2 = 0;
        byte b3 = 0;
        byte b4 = 0;
        byte b5 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            switch (j72.a(a)) {
                case 2:
                    i = j72.q(parcel, a);
                    break;
                case 3:
                    str = j72.e(parcel, a);
                    break;
                case 4:
                    str2 = j72.e(parcel, a);
                    break;
                case 5:
                    str3 = j72.e(parcel, a);
                    break;
                case 6:
                    str4 = j72.e(parcel, a);
                    break;
                case 7:
                    str5 = j72.e(parcel, a);
                    break;
                case 8:
                    str6 = j72.e(parcel, a);
                    break;
                case 9:
                    b2 = j72.k(parcel, a);
                    break;
                case 10:
                    b3 = j72.k(parcel, a);
                    break;
                case 11:
                    b4 = j72.k(parcel, a);
                    break;
                case 12:
                    b5 = j72.k(parcel, a);
                    break;
                case 13:
                    str7 = j72.e(parcel, a);
                    break;
                default:
                    j72.v(parcel, a);
                    break;
            }
        }
        j72.h(parcel, b);
        return new uq3(i, str, str2, str3, str4, str5, str6, b2, b3, b4, b5, str7);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ uq3[] newArray(int i) {
        return new uq3[i];
    }
}
