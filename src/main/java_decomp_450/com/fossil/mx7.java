package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mx7 {
    @DexIgnore
    public static boolean a;

    @DexIgnore
    public static void a(Object obj) {
        String str;
        if (a) {
            if (obj == null) {
                str = "null";
            } else {
                str = obj.toString();
            }
            Log.i("PhotoManagerPlugin", str);
        }
    }

    @DexIgnore
    public static void a(Object obj, Throwable th) {
        String str;
        if (a) {
            if (obj == null) {
                str = "null";
            } else {
                str = obj.toString();
            }
            Log.e("PhotoManagerPlugin", str, th);
        }
    }
}
