package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu1 implements Factory<Executor> {
    @DexIgnore
    public static /* final */ mu1 a; // = new mu1();

    @DexIgnore
    public static mu1 a() {
        return a;
    }

    @DexIgnore
    public static Executor b() {
        Executor a2 = lu1.a();
        c87.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Executor get() {
        return b();
    }
}
