package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xp0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ bo0 CREATOR; // = new bo0(null);
    @DexIgnore
    public /* final */ ru0 a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public boolean c;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ xp0(ru0 ru0, byte b2, boolean z, int i) {
        this(ru0, b2, (i & 4) != 0 ? true : z);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(new JSONObject(), r51.G, yz0.a(this.a)), r51.x1, Short.valueOf(yz0.b(this.b)));
    }

    @DexIgnore
    public byte[] b() {
        return new byte[0];
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    @DexIgnore
    public xp0(ru0 ru0, byte b2, boolean z) {
        this.c = true;
        this.a = ru0;
        this.b = b2;
        this.c = z;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public xp0(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = r3.readString()
            if (r0 == 0) goto L_0x0020
            java.lang.String r1 = "parcel.readString()!!"
            com.fossil.ee7.a(r0, r1)
            com.fossil.ru0 r0 = com.fossil.ru0.valueOf(r0)
            byte r1 = r3.readByte()
            int r3 = r3.readInt()
            if (r3 == 0) goto L_0x001b
            r3 = 1
            goto L_0x001c
        L_0x001b:
            r3 = 0
        L_0x001c:
            r2.<init>(r0, r1, r3)
            return
        L_0x0020:
            com.fossil.ee7.a()
            r3 = 0
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.xp0.<init>(android.os.Parcel):void");
    }
}
