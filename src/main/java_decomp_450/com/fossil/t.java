package com.fossil;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t extends IInterface {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a extends Binder implements t {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.t$a$a")
        /* renamed from: com.fossil.t$a$a  reason: collision with other inner class name */
        public static class C0182a implements t {
            @DexIgnore
            public static t b;
            @DexIgnore
            public IBinder a;

            @DexIgnore
            public C0182a(IBinder iBinder) {
                this.a = iBinder;
            }

            @DexIgnore
            public IBinder asBinder() {
                return this.a;
            }

            @DexIgnore
            @Override // com.fossil.t
            public void b(int i, Bundle bundle) throws RemoteException {
                Parcel obtain = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("android.support.v4.os.IResultReceiver");
                    obtain.writeInt(i);
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    if (this.a.transact(1, obtain, null, 1) || a.E() == null) {
                        obtain.recycle();
                    } else {
                        a.E().b(i, bundle);
                    }
                } finally {
                    obtain.recycle();
                }
            }
        }

        @DexIgnore
        public a() {
            attachInterface(this, "android.support.v4.os.IResultReceiver");
        }

        @DexIgnore
        public static t E() {
            return C0182a.b;
        }

        @DexIgnore
        public static t a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("android.support.v4.os.IResultReceiver");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof t)) {
                return new C0182a(iBinder);
            }
            return (t) queryLocalInterface;
        }

        @DexIgnore
        public IBinder asBinder() {
            return this;
        }

        @DexIgnore
        @Override // android.os.Binder
        public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
            if (i == 1) {
                parcel.enforceInterface("android.support.v4.os.IResultReceiver");
                b(parcel.readInt(), parcel.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(parcel) : null);
                return true;
            } else if (i != 1598968902) {
                return super.onTransact(i, parcel, parcel2, i2);
            } else {
                parcel2.writeString("android.support.v4.os.IResultReceiver");
                return true;
            }
        }
    }

    @DexIgnore
    void b(int i, Bundle bundle) throws RemoteException;
}
