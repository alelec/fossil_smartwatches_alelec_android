package com.fossil;

import android.app.job.JobParameters;
import android.content.Intent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface jl3 {
    @DexIgnore
    void a(JobParameters jobParameters, boolean z);

    @DexIgnore
    void a(Intent intent);

    @DexIgnore
    boolean zza(int i);
}
