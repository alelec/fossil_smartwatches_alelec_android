package com.fossil;

import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe5 {
    @DexIgnore
    public static /* final */ PortfolioApp a; // = PortfolioApp.g0.c();
    @DexIgnore
    public static /* final */ xe5 b; // = new xe5();

    @DexIgnore
    public final SpannableString a(String str, String str2, float f) {
        ee7.b(str, "bigText");
        ee7.b(str2, "smallText");
        SpannableString spannableString = new SpannableString(str + str2);
        spannableString.setSpan(new RelativeSizeSpan(f), str.length(), str.length() + str2.length(), 0);
        return spannableString;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.add(ig5.a(PortfolioApp.g0.c(), 2131886621));
        arrayList.add(ig5.a(PortfolioApp.g0.c(), 2131886624));
        arrayList.add(ig5.a(PortfolioApp.g0.c(), 2131886623));
        arrayList.add(ig5.a(PortfolioApp.g0.c(), 2131886625));
        arrayList.add(ig5.a(PortfolioApp.g0.c(), 2131886622));
        return arrayList;
    }

    @DexIgnore
    public final String c(int i) {
        String c = re5.c(i);
        ee7.a((Object) c, "NumberHelper.formatNumber(steps)");
        return c;
    }

    @DexIgnore
    public final CharSequence d(int i) {
        String b2 = re5.b(i / 60);
        ee7.a((Object) b2, "NumberHelper.formatBigNumber(hours)");
        String a2 = ig5.a(a, 2131887062);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_SetGoalsSleep_Label__Hr)");
        String a3 = ig5.a(a, 2131887063);
        ee7.a((Object) a3, "LanguageHelper.getString\u2026SetGoalsSleep_Label__Min)");
        CharSequence concat = TextUtils.concat(a(b2, a2, 0.7f), a(' ' + re5.b(i % 60), a3, 0.7f));
        ee7.a((Object) concat, "TextUtils.concat(activeH\u2026ing, remainMinutesString)");
        return concat;
    }

    @DexIgnore
    public final String e(int i) {
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886322);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026r_List_Subtitle__Friends)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String f(int i) {
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886323);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026r_List_Subtitle__Members)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String g(int i) {
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131886324);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026itle__PendingInvitations)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final String h(int i) {
        int i2 = i / 3600;
        int i3 = i - ((i2 * 60) * 60);
        int i4 = i3 / 60;
        we7 we7 = we7.a;
        String a2 = ig5.a(PortfolioApp.g0.c(), 2131887274);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026_time_hour_minute_second)");
        String format = String.format(a2, Arrays.copyOf(new Object[]{Integer.valueOf(i2), re5.a(i4), re5.a(i3 - (i4 * 60))}, 3));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }

    @DexIgnore
    public final char a(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final String a(int i, float f) {
        float c = re5.c(f * ((float) 100), 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getSleepDaySummaryText", "roundedPercent : " + c);
        StringBuilder sb = new StringBuilder();
        we7 we7 = we7.a;
        String string = PortfolioApp.g0.c().getString(i);
        ee7.a((Object) string, "PortfolioApp.instance.getString(stringId)");
        String format = String.format(string, Arrays.copyOf(new Object[]{String.valueOf(c)}, 1));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(" %");
        return sb.toString();
    }

    @DexIgnore
    public final String b(int i) {
        switch (i) {
            case 1:
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886638);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sun)");
                return a2;
            case 2:
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886635);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026ain_StepsToday_Text__Mon)");
                return a3;
            case 3:
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886641);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Text__Tues)");
                return a4;
            case 4:
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886642);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026ain_StepsToday_Text__Wed)");
                return a5;
            case 5:
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886639);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026in_StepsToday_Text__Thur)");
                return a6;
            case 6:
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886634);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026ain_StepsToday_Text__Fri)");
                return a7;
            case 7:
                String a8 = ig5.a(PortfolioApp.g0.c(), 2131886637);
                ee7.a((Object) a8, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sat)");
                return a8;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String a(int i) {
        switch (i) {
            case 1:
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886137);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__S)");
                return a2;
            case 2:
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886136);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__M)");
                return a3;
            case 3:
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886139);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__T)");
                return a4;
            case 4:
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886141);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__W)");
                return a5;
            case 5:
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886140);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026RepeatEnabled_Label__T_1)");
                return a6;
            case 6:
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886135);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__F)");
                return a7;
            case 7:
                String a8 = ig5.a(PortfolioApp.g0.c(), 2131886138);
                ee7.a((Object) a8, "LanguageHelper.getString\u2026RepeatEnabled_Label__S_1)");
                return a8;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String b(String str) {
        ee7.b(str, LogBuilder.KEY_TIME);
        String string = PortfolioApp.g0.c().getString(2131886080);
        ee7.a((Object) string, "PortfolioApp.instance.getString(R.string.AM)");
        if (mh7.a(str, string, false, 2, null)) {
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, str.length() - 2);
            ee7.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(ig5.a(PortfolioApp.g0.c(), 2131886712));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        String substring2 = str.substring(0, str.length() - 2);
        ee7.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb2.append(substring2);
        sb2.append(ig5.a(PortfolioApp.g0.c(), 2131886715));
        return sb2.toString();
    }

    @DexIgnore
    public final String a(long j) {
        String str;
        if (j == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886309);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026erBoard_Label__NotSynced)");
            return a2;
        }
        if (vt4.a.b() - j < 3600000) {
            long b2 = (vt4.a.b() - j) / 60000;
            if (b2 == 0) {
                str = ig5.a(PortfolioApp.g0.c(), 2131886310);
            } else {
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886205);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026lenge_Label__StartInMins)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{String.valueOf(b2)}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                we7 we72 = we7.a;
                String a4 = ig5.a(PortfolioApp.g0.c(), 2131886311);
                ee7.a((Object) a4, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                str = String.format(a4, Arrays.copyOf(new Object[]{format}, 1));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            }
        } else {
            long b3 = (vt4.a.b() - j) / 3600000;
            long j2 = (long) 24;
            if (b3 < j2) {
                we7 we73 = we7.a;
                String a5 = ig5.a(PortfolioApp.g0.c(), 2131886203);
                ee7.a((Object) a5, "LanguageHelper.getString\u2026enge_Label__StartInHours)");
                String format2 = String.format(a5, Arrays.copyOf(new Object[]{String.valueOf(b3)}, 1));
                ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                we7 we74 = we7.a;
                String a6 = ig5.a(PortfolioApp.g0.c(), 2131886311);
                ee7.a((Object) a6, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                str = String.format(a6, Arrays.copyOf(new Object[]{format2}, 1));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            } else {
                we7 we75 = we7.a;
                String a7 = ig5.a(PortfolioApp.g0.c(), 2131886202);
                ee7.a((Object) a7, "LanguageHelper.getString\u2026lenge_Label__StartInDays)");
                String format3 = String.format(a7, Arrays.copyOf(new Object[]{String.valueOf(b3 / j2)}, 1));
                ee7.a((Object) format3, "java.lang.String.format(format, *args)");
                we7 we76 = we7.a;
                String a8 = ig5.a(PortfolioApp.g0.c(), 2131886311);
                ee7.a((Object) a8, "LanguageHelper.getString\u2026d_Label__SyncedTimeAgo_1)");
                str = String.format(a8, Arrays.copyOf(new Object[]{format3}, 1));
                ee7.a((Object) str, "java.lang.String.format(format, *args)");
            }
        }
        ee7.a((Object) str, "if ((ExactTime.milliseco\u2026)\n            }\n        }");
        return str;
    }

    @DexIgnore
    public final String b(Integer num, String str) {
        ee7.b(str, "errorMes");
        if (num != null && num.intValue() == 400701) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886288);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026UsersFriendRequestsAreAt)");
            return a2;
        } else if ((num != null && num.intValue() == 400700) || (num != null && num.intValue() == 400707)) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886289);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026LimitReachedPleaseReview)");
            return a3;
        } else if (num != null && num.intValue() == 404001) {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886295);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026t__ThisAccountWasDeleted)");
            return a4;
        } else if (num != null && num.intValue() == 400709) {
            String a5 = ig5.a(PortfolioApp.g0.c(), 2131886298);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026t__YouAreNoLongerFriends)");
            return a5;
        } else if (num != null && num.intValue() == 404701) {
            String a6 = ig5.a(PortfolioApp.g0.c(), 2131886300);
            ee7.a((Object) a6, "LanguageHelper.getString\u2026ge_Friends_not_available)");
            return a6;
        } else if (num == null || num.intValue() != 400705) {
            return a(num, str);
        } else {
            String a7 = ig5.a(PortfolioApp.g0.c(), 2131887449);
            ee7.a((Object) a7, "LanguageHelper.getString\u2026_friend_to_blocked_users)");
            return a7;
        }
    }

    @DexIgnore
    public static /* synthetic */ Spannable a(xe5 xe5, String str, String str2, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 2131099703;
        }
        return xe5.a(str, str2, i);
    }

    @DexIgnore
    public final Spannable a(String str, String str2, int i) {
        int a2;
        ee7.b(str, "text");
        ee7.b(str2, MessengerShareContentUtility.WEBVIEW_RATIO_FULL);
        SpannableString spannableString = new SpannableString(str2);
        if (i != 0 && (a2 = nh7.a((CharSequence) str2, str, 0, false, 6, (Object) null)) >= 0) {
            spannableString.setSpan(new StyleSpan(1), a2, str.length() + a2, 33);
            spannableString.setSpan(new ForegroundColorSpan(v6.a(PortfolioApp.g0.c(), i)), a2, str.length() + a2, 33);
        }
        return spannableString;
    }

    @DexIgnore
    public final String a(Integer num, String str) {
        ee7.b(str, "errorMessage");
        if (num != null && num.intValue() == 601) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886743);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ourInternetConnectionAnd)");
            return a2;
        } else if (num != null && num.intValue() == 408) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886779);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026asAProblemProcessingThat)");
            return a3;
        } else if ((num != null && num.intValue() == 504) || ((num != null && num.intValue() == 503) || (num != null && num.intValue() == 500))) {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886829);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026tlyUndergoingMaintenance)");
            return a4;
        } else if (num != null && num.intValue() == 429) {
            String a5 = ig5.a(PortfolioApp.g0.c(), 2131886933);
            ee7.a((Object) a5, "LanguageHelper.getString\u2026_PleaseTryAgainAfterAFew)");
            return a5;
        } else {
            if (TextUtils.isEmpty(str)) {
                str = ig5.a(PortfolioApp.g0.c(), 2131886779);
            }
            ee7.a((Object) str, "if (TextUtils.isEmpty(er\u2026   errorMessage\n        }");
            return str;
        }
    }

    @DexIgnore
    public final String a() {
        String b2 = rd5.g.b().b();
        we7 we7 = we7.a;
        String format = String.format("%s/%s (Linux; U; Android %s; %s; Build/%s)", Arrays.copyOf(new Object[]{PortfolioApp.g0.c().h(), b2, Build.VERSION.RELEASE, Build.MODEL, b2}, 5));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        return format;
    }
}
