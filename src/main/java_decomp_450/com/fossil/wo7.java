package com.fossil;

import java.io.Closeable;
import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.Flushable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wo7 implements Closeable, Flushable {
    @DexIgnore
    public static /* final */ Pattern z; // = Pattern.compile("[a-z0-9_-]{1,120}");
    @DexIgnore
    public /* final */ gq7 a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ File c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public /* final */ File e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public long g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public long i; // = 0;
    @DexIgnore
    public zq7 j;
    @DexIgnore
    public /* final */ LinkedHashMap<String, d> p; // = new LinkedHashMap<>(0, 0.75f, true);
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public long w; // = 0;
    @DexIgnore
    public /* final */ Executor x;
    @DexIgnore
    public /* final */ Runnable y; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            synchronized (wo7.this) {
                if (!(!wo7.this.s) && !wo7.this.t) {
                    try {
                        wo7.this.q();
                    } catch (IOException unused) {
                        wo7.this.u = true;
                    }
                    try {
                        if (wo7.this.g()) {
                            wo7.this.o();
                            wo7.this.q = 0;
                        }
                    } catch (IOException unused2) {
                        wo7.this.v = true;
                        wo7.this.j = ir7.a(ir7.a());
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends xo7 {
        @DexIgnore
        public b(qr7 qr7) {
            super(qr7);
        }

        @DexIgnore
        @Override // com.fossil.xo7
        public void a(IOException iOException) {
            wo7.this.r = true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Closeable {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ sr7[] c;

        @DexIgnore
        public e(String str, long j, sr7[] sr7Arr, long[] jArr) {
            this.a = str;
            this.b = j;
            this.c = sr7Arr;
        }

        @DexIgnore
        public c a() throws IOException {
            return wo7.this.a(this.a, this.b);
        }

        @DexIgnore
        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() {
            for (sr7 sr7 : this.c) {
                ro7.a(sr7);
            }
        }

        @DexIgnore
        public sr7 a(int i) {
            return this.c[i];
        }
    }

    @DexIgnore
    public wo7(gq7 gq7, File file, int i2, int i3, long j2, Executor executor) {
        this.a = gq7;
        this.b = file;
        this.f = i2;
        this.c = new File(file, "journal");
        this.d = new File(file, "journal.tmp");
        this.e = new File(file, "journal.bkp");
        this.h = i3;
        this.g = j2;
        this.x = executor;
    }

    @DexIgnore
    public static wo7 a(gq7 gq7, File file, int i2, int i3, long j2) {
        if (j2 <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i3 > 0) {
            return new wo7(gq7, file, i2, i3, j2, new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), ro7.a("OkHttp DiskLruCache", true)));
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    @DexIgnore
    public c b(String str) throws IOException {
        return a(str, -1);
    }

    @DexIgnore
    public synchronized e c(String str) throws IOException {
        e();
        a();
        f(str);
        d dVar = this.p.get(str);
        if (dVar != null) {
            if (dVar.e) {
                e a2 = dVar.a();
                if (a2 == null) {
                    return null;
                }
                this.q++;
                this.j.a("READ").writeByte(32).a(str).writeByte(10);
                if (g()) {
                    this.x.execute(this.y);
                }
                return a2;
            }
        }
        return null;
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public synchronized void close() throws IOException {
        if (this.s) {
            if (!this.t) {
                d[] dVarArr = (d[]) this.p.values().toArray(new d[this.p.size()]);
                for (d dVar : dVarArr) {
                    if (dVar.f != null) {
                        dVar.f.a();
                    }
                }
                q();
                this.j.close();
                this.j = null;
                this.t = true;
                return;
            }
        }
        this.t = true;
    }

    @DexIgnore
    public final void d(String str) throws IOException {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i2 = indexOf + 1;
            int indexOf2 = str.indexOf(32, i2);
            if (indexOf2 == -1) {
                str2 = str.substring(i2);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.p.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i2, indexOf2);
            }
            d dVar = this.p.get(str2);
            if (dVar == null) {
                dVar = new d(str2);
                this.p.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                dVar.e = true;
                dVar.f = null;
                dVar.b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                dVar.f = new c(dVar);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    @DexIgnore
    public synchronized void e() throws IOException {
        if (!this.s) {
            if (this.a.d(this.e)) {
                if (this.a.d(this.c)) {
                    this.a.e(this.e);
                } else {
                    this.a.a(this.e, this.c);
                }
            }
            if (this.a.d(this.c)) {
                try {
                    n();
                    l();
                    this.s = true;
                    return;
                } catch (IOException e2) {
                    mq7 d2 = mq7.d();
                    d2.a(5, "DiskLruCache " + this.b + " is corrupt: " + e2.getMessage() + ", removing", e2);
                    b();
                    this.t = false;
                } catch (Throwable th) {
                    this.t = false;
                    throw th;
                }
            }
            o();
            this.s = true;
        }
    }

    @DexIgnore
    public final void f(String str) {
        if (!z.matcher(str).matches()) {
            throw new IllegalArgumentException("keys must match regex [a-z0-9_-]{1,120}: \"" + str + "\"");
        }
    }

    @DexIgnore
    @Override // java.io.Flushable
    public synchronized void flush() throws IOException {
        if (this.s) {
            a();
            q();
            this.j.flush();
        }
    }

    @DexIgnore
    public boolean g() {
        int i2 = this.q;
        return i2 >= 2000 && i2 >= this.p.size();
    }

    @DexIgnore
    public synchronized boolean isClosed() {
        return this.t;
    }

    @DexIgnore
    public final zq7 k() throws FileNotFoundException {
        return ir7.a(new b(this.a.f(this.c)));
    }

    @DexIgnore
    public final void l() throws IOException {
        this.a.e(this.d);
        Iterator<d> it = this.p.values().iterator();
        while (it.hasNext()) {
            d next = it.next();
            int i2 = 0;
            if (next.f == null) {
                while (i2 < this.h) {
                    this.i += next.b[i2];
                    i2++;
                }
            } else {
                next.f = null;
                while (i2 < this.h) {
                    this.a.e(next.c[i2]);
                    this.a.e(next.d[i2]);
                    i2++;
                }
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void n() throws IOException {
        ar7 a2 = ir7.a(this.a.a(this.c));
        try {
            String p2 = a2.p();
            String p3 = a2.p();
            String p4 = a2.p();
            String p5 = a2.p();
            String p6 = a2.p();
            if (!"libcore.io.DiskLruCache".equals(p2) || !"1".equals(p3) || !Integer.toString(this.f).equals(p4) || !Integer.toString(this.h).equals(p5) || !"".equals(p6)) {
                throw new IOException("unexpected journal header: [" + p2 + ", " + p3 + ", " + p5 + ", " + p6 + "]");
            }
            int i2 = 0;
            while (true) {
                try {
                    d(a2.p());
                    i2++;
                } catch (EOFException unused) {
                    this.q = i2 - this.p.size();
                    if (!a2.h()) {
                        o();
                    } else {
                        this.j = k();
                    }
                    return;
                }
            }
        } finally {
            ro7.a(a2);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public synchronized void o() throws IOException {
        if (this.j != null) {
            this.j.close();
        }
        zq7 a2 = ir7.a(this.a.b(this.d));
        try {
            a2.a("libcore.io.DiskLruCache").writeByte(10);
            a2.a("1").writeByte(10);
            a2.i((long) this.f).writeByte(10);
            a2.i((long) this.h).writeByte(10);
            a2.writeByte(10);
            for (d dVar : this.p.values()) {
                if (dVar.f != null) {
                    a2.a("DIRTY").writeByte(32);
                    a2.a(dVar.a);
                    a2.writeByte(10);
                } else {
                    a2.a("CLEAN").writeByte(32);
                    a2.a(dVar.a);
                    dVar.a(a2);
                    a2.writeByte(10);
                }
            }
            a2.close();
            if (this.a.d(this.c)) {
                this.a.a(this.c, this.e);
            }
            this.a.a(this.d, this.c);
            this.a.e(this.e);
            this.j = k();
            this.r = false;
            this.v = false;
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    @DexIgnore
    public void q() throws IOException {
        while (this.i > this.g) {
            a(this.p.values().iterator().next());
        }
        this.u = false;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ long[] b;
        @DexIgnore
        public /* final */ File[] c;
        @DexIgnore
        public /* final */ File[] d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public c f;
        @DexIgnore
        public long g;

        @DexIgnore
        public d(String str) {
            this.a = str;
            int i = wo7.this.h;
            this.b = new long[i];
            this.c = new File[i];
            this.d = new File[i];
            StringBuilder sb = new StringBuilder(str);
            sb.append('.');
            int length = sb.length();
            for (int i2 = 0; i2 < wo7.this.h; i2++) {
                sb.append(i2);
                this.c[i2] = new File(wo7.this.b, sb.toString());
                sb.append(".tmp");
                this.d[i2] = new File(wo7.this.b, sb.toString());
                sb.setLength(length);
            }
        }

        @DexIgnore
        public void a(zq7 zq7) throws IOException {
            for (long j : this.b) {
                zq7.writeByte(32).i(j);
            }
        }

        @DexIgnore
        public void b(String[] strArr) throws IOException {
            if (strArr.length == wo7.this.h) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        a(strArr);
                        throw null;
                    }
                }
                return;
            }
            a(strArr);
            throw null;
        }

        @DexIgnore
        public final IOException a(String[] strArr) throws IOException {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        @DexIgnore
        public e a() {
            if (Thread.holdsLock(wo7.this)) {
                sr7[] sr7Arr = new sr7[wo7.this.h];
                long[] jArr = (long[]) this.b.clone();
                int i = 0;
                int i2 = 0;
                while (i2 < wo7.this.h) {
                    try {
                        sr7Arr[i2] = wo7.this.a.a(this.c[i2]);
                        i2++;
                    } catch (FileNotFoundException unused) {
                        while (i < wo7.this.h && sr7Arr[i] != null) {
                            ro7.a(sr7Arr[i]);
                            i++;
                        }
                        try {
                            wo7.this.a(this);
                            return null;
                        } catch (IOException unused2) {
                            return null;
                        }
                    }
                }
                return new e(this.a, this.g, sr7Arr, jArr);
            }
            throw new AssertionError();
        }
    }

    @DexIgnore
    public void b() throws IOException {
        close();
        this.a.c(this.b);
    }

    @DexIgnore
    public synchronized c a(String str, long j2) throws IOException {
        e();
        a();
        f(str);
        d dVar = this.p.get(str);
        if (j2 != -1 && (dVar == null || dVar.g != j2)) {
            return null;
        }
        if (dVar != null && dVar.f != null) {
            return null;
        }
        if (this.u || this.v) {
            this.x.execute(this.y);
            return null;
        }
        this.j.a("DIRTY").writeByte(32).a(str).writeByte(10);
        this.j.flush();
        if (this.r) {
            return null;
        }
        if (dVar == null) {
            dVar = new d(str);
            this.p.put(str, dVar);
        }
        c cVar = new c(dVar);
        dVar.f = cVar;
        return cVar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c {
        @DexIgnore
        public /* final */ d a;
        @DexIgnore
        public /* final */ boolean[] b;
        @DexIgnore
        public boolean c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends xo7 {
            @DexIgnore
            public a(qr7 qr7) {
                super(qr7);
            }

            @DexIgnore
            @Override // com.fossil.xo7
            public void a(IOException iOException) {
                synchronized (wo7.this) {
                    c.this.c();
                }
            }
        }

        @DexIgnore
        public c(d dVar) {
            this.a = dVar;
            this.b = dVar.e ? null : new boolean[wo7.this.h];
        }

        @DexIgnore
        public qr7 a(int i) {
            synchronized (wo7.this) {
                if (this.c) {
                    throw new IllegalStateException();
                } else if (this.a.f != this) {
                    return ir7.a();
                } else {
                    if (!this.a.e) {
                        this.b[i] = true;
                    }
                    try {
                        return new a(wo7.this.a.b(this.a.d[i]));
                    } catch (FileNotFoundException unused) {
                        return ir7.a();
                    }
                }
            }
        }

        @DexIgnore
        public void b() throws IOException {
            synchronized (wo7.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        wo7.this.a(this, true);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }

        @DexIgnore
        public void c() {
            if (this.a.f == this) {
                int i = 0;
                while (true) {
                    wo7 wo7 = wo7.this;
                    if (i < wo7.h) {
                        try {
                            wo7.a.e(this.a.d[i]);
                        } catch (IOException unused) {
                        }
                        i++;
                    } else {
                        this.a.f = null;
                        return;
                    }
                }
            }
        }

        @DexIgnore
        public void a() throws IOException {
            synchronized (wo7.this) {
                if (!this.c) {
                    if (this.a.f == this) {
                        wo7.this.a(this, false);
                    }
                    this.c = true;
                } else {
                    throw new IllegalStateException();
                }
            }
        }
    }

    @DexIgnore
    public synchronized void c() throws IOException {
        e();
        for (d dVar : (d[]) this.p.values().toArray(new d[this.p.size()])) {
            a(dVar);
        }
        this.u = false;
    }

    @DexIgnore
    public synchronized boolean e(String str) throws IOException {
        e();
        a();
        f(str);
        d dVar = this.p.get(str);
        if (dVar == null) {
            return false;
        }
        boolean a2 = a(dVar);
        if (a2 && this.i <= this.g) {
            this.u = false;
        }
        return a2;
    }

    @DexIgnore
    public synchronized void a(c cVar, boolean z2) throws IOException {
        d dVar = cVar.a;
        if (dVar.f == cVar) {
            if (z2 && !dVar.e) {
                int i2 = 0;
                while (i2 < this.h) {
                    if (!cVar.b[i2]) {
                        cVar.a();
                        throw new IllegalStateException("Newly created entry didn't create value for index " + i2);
                    } else if (!this.a.d(dVar.d[i2])) {
                        cVar.a();
                        return;
                    } else {
                        i2++;
                    }
                }
            }
            for (int i3 = 0; i3 < this.h; i3++) {
                File file = dVar.d[i3];
                if (!z2) {
                    this.a.e(file);
                } else if (this.a.d(file)) {
                    File file2 = dVar.c[i3];
                    this.a.a(file, file2);
                    long j2 = dVar.b[i3];
                    long g2 = this.a.g(file2);
                    dVar.b[i3] = g2;
                    this.i = (this.i - j2) + g2;
                }
            }
            this.q++;
            dVar.f = null;
            if (dVar.e || z2) {
                dVar.e = true;
                this.j.a("CLEAN").writeByte(32);
                this.j.a(dVar.a);
                dVar.a(this.j);
                this.j.writeByte(10);
                if (z2) {
                    long j3 = this.w;
                    this.w = 1 + j3;
                    dVar.g = j3;
                }
            } else {
                this.p.remove(dVar.a);
                this.j.a("REMOVE").writeByte(32);
                this.j.a(dVar.a);
                this.j.writeByte(10);
            }
            this.j.flush();
            if (this.i > this.g || g()) {
                this.x.execute(this.y);
            }
            return;
        }
        throw new IllegalStateException();
    }

    @DexIgnore
    public boolean a(d dVar) throws IOException {
        c cVar = dVar.f;
        if (cVar != null) {
            cVar.c();
        }
        for (int i2 = 0; i2 < this.h; i2++) {
            this.a.e(dVar.c[i2]);
            long j2 = this.i;
            long[] jArr = dVar.b;
            this.i = j2 - jArr[i2];
            jArr[i2] = 0;
        }
        this.q++;
        this.j.a("REMOVE").writeByte(32).a(dVar.a).writeByte(10);
        this.p.remove(dVar.a);
        if (g()) {
            this.x.execute(this.y);
        }
        return true;
    }

    @DexIgnore
    public final synchronized void a() {
        if (isClosed()) {
            throw new IllegalStateException("cache is closed");
        }
    }
}
