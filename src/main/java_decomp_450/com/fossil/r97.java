package com.fossil;

import com.facebook.internal.FacebookRequestErrorClassification;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r97 extends q97 {
    @DexIgnore
    public static final <T> boolean a(T[] tArr, T[] tArr2) {
        ee7.b(tArr, "$this$contentDeepEqualsImpl");
        ee7.b(tArr2, FacebookRequestErrorClassification.KEY_OTHER);
        if (tArr == tArr2) {
            return true;
        }
        if (tArr.length != tArr2.length) {
            return false;
        }
        int length = tArr.length;
        for (int i = 0; i < length; i++) {
            T t = tArr[i];
            T t2 = tArr2[i];
            if (t != t2) {
                if (t == null || t2 == null) {
                    return false;
                }
                if (!(t instanceof Object[]) || !(t2 instanceof Object[])) {
                    if (!(t instanceof byte[]) || !(t2 instanceof byte[])) {
                        if (!(t instanceof short[]) || !(t2 instanceof short[])) {
                            if (!(t instanceof int[]) || !(t2 instanceof int[])) {
                                if (!(t instanceof long[]) || !(t2 instanceof long[])) {
                                    if (!(t instanceof float[]) || !(t2 instanceof float[])) {
                                        if (!(t instanceof double[]) || !(t2 instanceof double[])) {
                                            if (!(t instanceof char[]) || !(t2 instanceof char[])) {
                                                if (!(t instanceof boolean[]) || !(t2 instanceof boolean[])) {
                                                    if (!(t instanceof z87) || !(t2 instanceof z87)) {
                                                        if (!(t instanceof g97) || !(t2 instanceof g97)) {
                                                            if (!(t instanceof b97) || !(t2 instanceof b97)) {
                                                                if (!(t instanceof d97) || !(t2 instanceof d97)) {
                                                                    if (!ee7.a((Object) t, (Object) t2)) {
                                                                        return false;
                                                                    }
                                                                } else if (!ab7.a(t.b(), t2.b())) {
                                                                    return false;
                                                                }
                                                            } else if (!ab7.a(t.b(), t2.b())) {
                                                                return false;
                                                            }
                                                        } else if (!ab7.a(t.b(), t2.b())) {
                                                            return false;
                                                        }
                                                    } else if (!ab7.a(t.b(), t2.b())) {
                                                        return false;
                                                    }
                                                } else if (!Arrays.equals((boolean[]) t, (boolean[]) t2)) {
                                                    return false;
                                                }
                                            } else if (!Arrays.equals((char[]) t, (char[]) t2)) {
                                                return false;
                                            }
                                        } else if (!Arrays.equals((double[]) t, (double[]) t2)) {
                                            return false;
                                        }
                                    } else if (!Arrays.equals((float[]) t, (float[]) t2)) {
                                        return false;
                                    }
                                } else if (!Arrays.equals((long[]) t, (long[]) t2)) {
                                    return false;
                                }
                            } else if (!Arrays.equals((int[]) t, (int[]) t2)) {
                                return false;
                            }
                        } else if (!Arrays.equals((short[]) t, (short[]) t2)) {
                            return false;
                        }
                    } else if (!Arrays.equals((byte[]) t, (byte[]) t2)) {
                        return false;
                    }
                } else if (!a((Object[]) t, (Object[]) t2)) {
                    return false;
                }
            }
        }
        return true;
    }
}
