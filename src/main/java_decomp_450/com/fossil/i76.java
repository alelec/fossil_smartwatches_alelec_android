package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.dashboard.activetime.overview.ActiveTimeOverviewFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i76 extends go5 implements h76, rp5, ro5 {
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public qw6<e05> f;
    @DexIgnore
    public g76 g;
    @DexIgnore
    public lp5 h;
    @DexIgnore
    public ActiveTimeOverviewFragment i;
    @DexIgnore
    public pz6 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final i76 a() {
            return new i76();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends pz6 {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView e;
        @DexIgnore
        public /* final */ /* synthetic */ i76 f;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager, i76 i76, LinearLayoutManager linearLayoutManager2) {
            super(linearLayoutManager);
            this.e = recyclerView;
            this.f = i76;
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i) {
            i76.a(this.f).j();
        }

        @DexIgnore
        @Override // com.fossil.pz6
        public void a(int i, int i2) {
        }
    }

    @DexIgnore
    public static final /* synthetic */ g76 a(i76 i76) {
        g76 g76 = i76.g;
        if (g76 != null) {
            return g76;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.rp5
    public void b(Date date, Date date2) {
        ee7.b(date, "startWeekDate");
        ee7.b(date2, "endWeekDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onWeekClicked - startWeekDate=" + date + ", endWeekDate=" + date2);
    }

    @DexIgnore
    @Override // com.fossil.h76
    public void d() {
        pz6 pz6 = this.j;
        if (pz6 != null) {
            pz6.a();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "DashboardActiveTimeFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final e05 f1() {
        qw6<e05> qw6 = this.f;
        if (qw6 != null) {
            return qw6.a();
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<e05> qw6 = new qw6<>(this, (e05) qb.a(layoutInflater, 2131558541, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            e05 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimeFragment", "onDestroy");
        super.onDestroy();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onDestroyView() {
        g76 g76 = this.g;
        if (g76 != null) {
            g76.i();
            super.onDestroyView();
            Z0();
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        g76 g76 = this.g;
        if (g76 != null) {
            g76.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        g76 g76 = this.g;
        if (g76 != null) {
            g76.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        RecyclerView recyclerView;
        RecyclerView recyclerView2;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        ActiveTimeOverviewFragment activeTimeOverviewFragment = (ActiveTimeOverviewFragment) getChildFragmentManager().b("ActiveTimeOverviewFragment");
        this.i = activeTimeOverviewFragment;
        if (activeTimeOverviewFragment == null) {
            this.i = new ActiveTimeOverviewFragment();
        }
        kp5 kp5 = new kp5();
        PortfolioApp c = PortfolioApp.g0.c();
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        ActiveTimeOverviewFragment activeTimeOverviewFragment2 = this.i;
        if (activeTimeOverviewFragment2 != null) {
            this.h = new lp5(kp5, c, this, childFragmentManager, activeTimeOverviewFragment2);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), 1, false);
            e05 f1 = f1();
            if (!(f1 == null || (recyclerView2 = f1.q) == null)) {
                ee7.a((Object) recyclerView2, "it");
                recyclerView2.setLayoutManager(linearLayoutManager);
                lp5 lp5 = this.h;
                if (lp5 != null) {
                    recyclerView2.setAdapter(lp5);
                    RecyclerView.m layoutManager = recyclerView2.getLayoutManager();
                    if (layoutManager != null) {
                        b bVar = new b(recyclerView2, (LinearLayoutManager) layoutManager, this, linearLayoutManager);
                        this.j = bVar;
                        if (bVar != null) {
                            recyclerView2.addOnScrollListener(bVar);
                            recyclerView2.setItemViewCacheSize(0);
                            s66 s66 = new s66(linearLayoutManager.Q());
                            Drawable c2 = v6.c(recyclerView2.getContext(), 2131230855);
                            if (c2 != null) {
                                ee7.a((Object) c2, "ContextCompat.getDrawabl\u2026tion_dashboard_line_1w)!!");
                                s66.a(c2);
                                recyclerView2.addItemDecoration(s66);
                                g76 g76 = this.g;
                                if (g76 != null) {
                                    g76.h();
                                } else {
                                    ee7.d("mPresenter");
                                    throw null;
                                }
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        throw new x87("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                    }
                } else {
                    ee7.d("mDashboardActiveTimeAdapter");
                    throw null;
                }
            }
            e05 f12 = f1();
            if (!(f12 == null || (recyclerView = f12.q) == null)) {
                ee7.a((Object) recyclerView, "recyclerView");
                RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
                if (itemAnimator instanceof dh) {
                    ((dh) itemAnimator).setSupportsChangeAnimations(false);
                }
            }
            V("active_minutes_view");
            FragmentActivity activity = getActivity();
            if (activity != null) {
                he a2 = je.a(activity).a(xz6.class);
                ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ardViewModel::class.java)");
                xz6 xz6 = (xz6) a2;
                return;
            }
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        e05 f1;
        RecyclerView recyclerView;
        View view;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("DashboardActiveTimeFragment visible=");
        sb.append(z);
        sb.append(", tracer=");
        sb.append(c1());
        sb.append(", isRunning=");
        jf5 c1 = c1();
        sb.append(c1 != null ? Boolean.valueOf(c1.b()) : null);
        local.d("onVisibleChanged", sb.toString());
        if (z) {
            jf5 c12 = c1();
            if (c12 != null) {
                c12.d();
            }
            if (isVisible() && this.f != null && (f1 = f1()) != null && (recyclerView = f1.q) != null) {
                RecyclerView.ViewHolder findViewHolderForAdapterPosition = recyclerView.findViewHolderForAdapterPosition(0);
                if (findViewHolderForAdapterPosition == null || (view = findViewHolderForAdapterPosition.itemView) == null || view.getY() != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    recyclerView.smoothScrollToPosition(0);
                    pz6 pz6 = this.j;
                    if (pz6 != null) {
                        pz6.a();
                        return;
                    }
                    return;
                }
                return;
            }
            return;
        }
        jf5 c13 = c1();
        if (c13 != null) {
            c13.a("");
        }
    }

    @DexIgnore
    @Override // com.fossil.h76
    public void a(qf<ActivitySummary> qfVar) {
        lp5 lp5 = this.h;
        if (lp5 != null) {
            lp5.c(qfVar);
        } else {
            ee7.d("mDashboardActiveTimeAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.rp5
    public void a(Date date) {
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeFragment", "onDayClicked - date=" + date);
        Context context = getContext();
        if (context != null) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.A;
            ee7.a((Object) context, "it");
            aVar.a(date, context);
        }
    }

    @DexIgnore
    public void a(g76 g76) {
        ee7.b(g76, "presenter");
        this.g = g76;
    }
}
