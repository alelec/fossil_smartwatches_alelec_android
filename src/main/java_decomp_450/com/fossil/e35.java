package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ View u;
    @DexIgnore
    public /* final */ RecyclerView v;
    @DexIgnore
    public /* final */ SwipeRefreshLayout w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public e35(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, ImageView imageView2, RTLImageView rTLImageView, View view2, RecyclerView recyclerView, SwipeRefreshLayout swipeRefreshLayout, FlexibleTextView flexibleTextView2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = imageView2;
        this.t = rTLImageView;
        this.u = view2;
        this.v = recyclerView;
        this.w = swipeRefreshLayout;
        this.x = flexibleTextView2;
    }
}
