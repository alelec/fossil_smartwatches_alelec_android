package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vz4 extends uz4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i O; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray P;
    @DexIgnore
    public /* final */ NestedScrollView M;
    @DexIgnore
    public long N;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        P = sparseIntArray;
        sparseIntArray.put(2131362037, 1);
        P.put(2131363223, 2);
        P.put(2131363412, 3);
        P.put(2131362669, 4);
        P.put(2131362036, 5);
        P.put(2131363222, 6);
        P.put(2131363411, 7);
        P.put(2131362668, 8);
        P.put(2131362077, 9);
        P.put(2131363296, 10);
        P.put(2131363419, 11);
        P.put(2131362676, 12);
        P.put(2131362104, 13);
        P.put(2131363332, 14);
        P.put(2131363445, 15);
        P.put(2131362681, 16);
        P.put(2131362117, 17);
        P.put(2131362969, 18);
        P.put(2131362968, 19);
        P.put(2131362970, 20);
        P.put(2131362971, 21);
        P.put(2131362245, 22);
    }
    */

    @DexIgnore
    public vz4(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 23, O, P));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.N = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.N != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.N = 1;
        }
        g();
    }

    @DexIgnore
    public vz4(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[5], (ConstraintLayout) objArr[1], (ConstraintLayout) objArr[9], (ConstraintLayout) objArr[13], (ConstraintLayout) objArr[17], (FlexibleButton) objArr[22], (ImageView) objArr[8], (ImageView) objArr[4], (ImageView) objArr[12], (ImageView) objArr[16], (RingProgressBar) objArr[19], (RingProgressBar) objArr[18], (RingProgressBar) objArr[20], (RingProgressBar) objArr[21], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[2], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[14], (View) objArr[7], (View) objArr[3], (View) objArr[11], (View) objArr[15]);
        this.N = -1;
        NestedScrollView nestedScrollView = (NestedScrollView) objArr[0];
        this.M = nestedScrollView;
        nestedScrollView.setTag(null);
        a(view);
        f();
    }
}
