package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nf7 extends ka7 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public long c;
    @DexIgnore
    public /* final */ long d;

    @DexIgnore
    public nf7(long j, long j2, long j3) {
        this.d = j3;
        this.a = j2;
        boolean z = true;
        int i = (j3 > 0 ? 1 : (j3 == 0 ? 0 : -1));
        int i2 = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        if (i <= 0 ? i2 < 0 : i2 > 0) {
            z = false;
        }
        this.b = z;
        this.c = !z ? this.a : j;
    }

    @DexIgnore
    @Override // com.fossil.ka7
    public long a() {
        long j = this.c;
        if (j != this.a) {
            this.c = this.d + j;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return j;
    }

    @DexIgnore
    public boolean hasNext() {
        return this.b;
    }
}
