package com.fossil;

import com.zendesk.sdk.network.Constants;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g84 extends m34 implements h84 {
    @DexIgnore
    public z24 f;

    @DexIgnore
    public g84(String str, String str2, m64 m64) {
        this(str, str2, m64, k64.GET, z24.a());
    }

    @DexIgnore
    @Override // com.fossil.h84
    public JSONObject a(d84 d84, boolean z) {
        if (z) {
            try {
                Map<String, String> a = a(d84);
                l64 a2 = a(a);
                a(a2, d84);
                z24 z24 = this.f;
                z24.a("Requesting settings from " + b());
                z24 z242 = this.f;
                z242.a("Settings query params were: " + a);
                n64 b = a2.b();
                z24 z243 = this.f;
                z243.a("Settings request ID: " + b.a("X-REQUEST-ID"));
                return a(b);
            } catch (IOException e) {
                this.f.b("Settings request failed.", e);
                return null;
            }
        } else {
            throw new RuntimeException("An invalid data collection token was used.");
        }
    }

    @DexIgnore
    public boolean a(int i) {
        return i == 200 || i == 201 || i == 202 || i == 203;
    }

    @DexIgnore
    public final JSONObject b(String str) {
        try {
            return new JSONObject(str);
        } catch (Exception e) {
            z24 z24 = this.f;
            z24.a("Failed to parse settings JSON from " + b(), e);
            z24 z242 = this.f;
            z242.a("Settings response " + str);
            return null;
        }
    }

    @DexIgnore
    public g84(String str, String str2, m64 m64, k64 k64, z24 z24) {
        super(str, str2, m64, k64);
        this.f = z24;
    }

    @DexIgnore
    public JSONObject a(n64 n64) {
        int b = n64.b();
        z24 z24 = this.f;
        z24.a("Settings result was: " + b);
        if (a(b)) {
            return b(n64.a());
        }
        z24 z242 = this.f;
        z242.b("Failed to retrieve settings from " + b());
        return null;
    }

    @DexIgnore
    public final Map<String, String> a(d84 d84) {
        HashMap hashMap = new HashMap();
        hashMap.put("build_version", d84.h);
        hashMap.put("display_version", d84.g);
        hashMap.put("source", Integer.toString(d84.i));
        String str = d84.f;
        if (!t34.b(str)) {
            hashMap.put("instance", str);
        }
        return hashMap;
    }

    @DexIgnore
    public final l64 a(l64 l64, d84 d84) {
        a(l64, "X-CRASHLYTICS-GOOGLE-APP-ID", d84.a);
        a(l64, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
        a(l64, "X-CRASHLYTICS-API-CLIENT-VERSION", y34.e());
        a(l64, Constants.ACCEPT_HEADER, Constants.APPLICATION_JSON);
        a(l64, "X-CRASHLYTICS-DEVICE-MODEL", d84.b);
        a(l64, "X-CRASHLYTICS-OS-BUILD-VERSION", d84.c);
        a(l64, "X-CRASHLYTICS-OS-DISPLAY-VERSION", d84.d);
        a(l64, "X-CRASHLYTICS-INSTALLATION-ID", d84.e.a());
        return l64;
    }

    @DexIgnore
    public final void a(l64 l64, String str, String str2) {
        if (str2 != null) {
            l64.a(str, str2);
        }
    }
}
