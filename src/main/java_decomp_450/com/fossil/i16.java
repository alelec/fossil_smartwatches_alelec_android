package com.fossil;

import android.text.TextUtils;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i16 extends v06 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public Gson f; // = new Gson();
    @DexIgnore
    public CommuteTimeSetting g;
    @DexIgnore
    public ArrayList<String> h; // = new ArrayList<>();
    @DexIgnore
    public MFUser i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public /* final */ w06 k;
    @DexIgnore
    public /* final */ ch5 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2", f = "CommuteTimeSettingsPresenter.kt", l = {154, 161}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address;
        @DexIgnore
        public /* final */ /* synthetic */ String $displayInfo;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ i16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ se7 $searchedRecent;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, se7 se7, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$searchedRecent = se7;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$searchedRecent, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.l.a((List<String>) this.$searchedRecent.element);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.i16$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$saveCommuteTimeSetting$2$searchedRecent$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.i16$b$b  reason: collision with other inner class name */
        public static final class C0080b extends zb7 implements kd7<yi7, fb7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0080b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0080b bVar = new C0080b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<String>> fb7) {
                return ((C0080b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.l.f();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(i16 i16, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = i16;
            this.$address = str;
            this.$displayInfo = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$address, this.$displayInfo, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            se7 se7;
            se7 se72;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                se72 = new se7();
                ti7 a3 = this.this$0.c();
                C0080b bVar = new C0080b(this, null);
                this.L$0 = yi7;
                this.L$1 = se72;
                this.L$2 = se72;
                this.label = 1;
                obj = vh7.a(a3, bVar, this);
                if (obj == a2) {
                    return a2;
                }
                se7 = se72;
            } else if (i == 1) {
                se72 = (se7) this.L$2;
                se7 = (se7) this.L$1;
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                se7 se73 = (se7) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.k.a();
                this.this$0.k.h(true);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ee7.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            se72.element = (T) ((List) obj);
            if (!se7.element.contains(this.$address)) {
                if (this.$displayInfo.length() > 0) {
                    if (this.$address.length() > 0) {
                        se7.element.add(0, this.$address);
                        if (se7.element.size() > 5) {
                            se7.element = (T) se7.element.subList(0, 5);
                        }
                        ti7 a4 = this.this$0.c();
                        a aVar = new a(this, se7, null);
                        this.L$0 = yi7;
                        this.L$1 = se7;
                        this.label = 2;
                        if (vh7.a(a4, aVar, this) == a2) {
                            return a2;
                        }
                    }
                }
            }
            this.this$0.k.a();
            this.this$0.k.h(true);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2", f = "CommuteTimeSettingsPresenter.kt", l = {66, 67}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ i16 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$1", f = "CommuteTimeSettingsPresenter.kt", l = {66}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository f = this.this$0.this$0.m;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = f.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsPresenter$start$2$recentSearchedAddress$1", f = "CommuteTimeSettingsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<String>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.l.f();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(i16 i16, fb7 fb7) {
            super(2, fb7);
            this.this$0 = i16;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0087  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r8.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002b
                if (r1 == r4) goto L_0x001f
                if (r1 != r3) goto L_0x0017
                java.lang.Object r0 = r8.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r9)
                goto L_0x0066
            L_0x0017:
                java.lang.IllegalStateException r9 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r9.<init>(r0)
                throw r9
            L_0x001f:
                java.lang.Object r1 = r8.L$1
                com.fossil.i16 r1 = (com.fossil.i16) r1
                java.lang.Object r4 = r8.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r9)
                goto L_0x004b
            L_0x002b:
                com.fossil.t87.a(r9)
                com.fossil.yi7 r9 = r8.p$
                com.fossil.i16 r1 = r8.this$0
                com.fossil.ti7 r5 = r1.c()
                com.fossil.i16$c$a r6 = new com.fossil.i16$c$a
                r6.<init>(r8, r2)
                r8.L$0 = r9
                r8.L$1 = r1
                r8.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r5, r6, r8)
                if (r4 != r0) goto L_0x0048
                return r0
            L_0x0048:
                r7 = r4
                r4 = r9
                r9 = r7
            L_0x004b:
                com.portfolio.platform.data.model.MFUser r9 = (com.portfolio.platform.data.model.MFUser) r9
                r1.i = r9
                com.fossil.i16 r9 = r8.this$0
                com.fossil.ti7 r9 = r9.c()
                com.fossil.i16$c$b r1 = new com.fossil.i16$c$b
                r1.<init>(r8, r2)
                r8.L$0 = r4
                r8.label = r3
                java.lang.Object r9 = com.fossil.vh7.a(r9, r1, r8)
                if (r9 != r0) goto L_0x0066
                return r0
            L_0x0066:
                java.lang.String r0 = "withContext(IO) { mShare\u2026r.addressSearchedRecent }"
                com.fossil.ee7.a(r9, r0)
                java.util.List r9 = (java.util.List) r9
                com.fossil.i16 r0 = r8.this$0
                java.util.ArrayList r0 = r0.h
                r0.clear()
                com.fossil.i16 r0 = r8.this$0
                java.util.ArrayList r0 = r0.h
                r0.addAll(r9)
                com.fossil.i16 r9 = r8.this$0
                com.portfolio.platform.data.model.MFUser r9 = r9.i
                if (r9 == 0) goto L_0x00b6
                com.fossil.i16 r0 = r8.this$0
                com.fossil.w06 r0 = r0.k
                com.portfolio.platform.data.model.MFUser$Address r1 = r9.getAddresses()
                java.lang.String r2 = ""
                if (r1 == 0) goto L_0x009c
                java.lang.String r1 = r1.getHome()
                if (r1 == 0) goto L_0x009c
                goto L_0x009d
            L_0x009c:
                r1 = r2
            L_0x009d:
                r0.r(r1)
                com.fossil.i16 r0 = r8.this$0
                com.fossil.w06 r0 = r0.k
                com.portfolio.platform.data.model.MFUser$Address r9 = r9.getAddresses()
                if (r9 == 0) goto L_0x00b3
                java.lang.String r9 = r9.getWork()
                if (r9 == 0) goto L_0x00b3
                r2 = r9
            L_0x00b3:
                r0.G(r2)
            L_0x00b6:
                com.fossil.i16 r9 = r8.this$0
                com.fossil.w06 r9 = r9.k
                com.fossil.i16 r0 = r8.this$0
                java.util.ArrayList r0 = r0.h
                r9.e(r0)
                com.fossil.i16 r9 = r8.this$0
                com.fossil.w06 r9 = r9.k
                com.fossil.i16 r0 = r8.this$0
                com.portfolio.platform.data.model.setting.CommuteTimeSetting r0 = r0.g
                r9.a(r0)
                com.fossil.i97 r9 = com.fossil.i97.a
                return r9
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.i16.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
        String simpleName = i16.class.getSimpleName();
        ee7.a((Object) simpleName, "CommuteTimeSettingsPrese\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public i16(w06 w06, ch5 ch5, UserRepository userRepository) {
        ee7.b(w06, "mView");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(userRepository, "mUserRepository");
        this.k = w06;
        this.l = ch5;
        this.m = userRepository;
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void h() {
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setAddress("");
        }
    }

    @DexIgnore
    @Override // com.fossil.v06
    public CommuteTimeSetting i() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void j() {
        MFUser.Address addresses;
        String home;
        MFUser.Address addresses2;
        MFUser mFUser = this.i;
        String str = "";
        if (TextUtils.isEmpty((mFUser == null || (addresses2 = mFUser.getAddresses()) == null) ? null : addresses2.getHome())) {
            this.j = false;
            this.k.c("Home", str);
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (!(mFUser2 == null || (addresses = mFUser2.getAddresses()) == null || (home = addresses.getHome()) == null)) {
                str = home;
            }
            commuteTimeSetting.setAddress(str);
        }
        this.k.h(true);
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void k() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            w06 w06 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getHome()) == null) {
                str = "";
            }
            w06.c("Home", str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void l() {
        MFUser.Address addresses;
        MFUser mFUser = this.i;
        if (TextUtils.isEmpty((mFUser == null || (addresses = mFUser.getAddresses()) == null) ? null : addresses.getWork())) {
            this.j = false;
            this.k.c("Other", "");
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            MFUser mFUser2 = this.i;
            if (mFUser2 != null) {
                MFUser.Address addresses2 = mFUser2.getAddresses();
                if (addresses2 != null) {
                    String work = addresses2.getWork();
                    if (work != null) {
                        commuteTimeSetting.setAddress(work);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
        this.k.h(true);
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void m() {
        String str;
        MFUser.Address addresses;
        if (this.g != null) {
            this.j = true;
            w06 w06 = this.k;
            MFUser mFUser = this.i;
            if (mFUser == null || (addresses = mFUser.getAddresses()) == null || (str = addresses.getWork()) == null) {
                str = "";
            }
            w06.c("Other", str);
        }
    }

    @DexIgnore
    public void n() {
        this.k.a(this);
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void b(String str) {
        ee7.b(str, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "updateFormatType, format = " + str);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            commuteTimeSetting.setFormat(str);
        }
        this.k.a(this.g);
    }

    @DexIgnore
    public void c(String str) {
        CommuteTimeSetting commuteTimeSetting;
        ee7.b(str, MicroAppSetting.SETTING);
        try {
            commuteTimeSetting = (CommuteTimeSetting) this.f.a(str, CommuteTimeSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = n;
            local.d(str2, "exception when parse commute time setting " + e2);
            commuteTimeSetting = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
        this.g = commuteTimeSetting;
        if (commuteTimeSetting == null) {
            this.g = new CommuteTimeSetting(null, null, false, null, 15, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        PlacesClient createClient = Places.createClient(PortfolioApp.g0.c());
        this.e = createClient;
        this.k.a(createClient);
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            if (!TextUtils.isEmpty(commuteTimeSetting.getAddress())) {
                this.k.t(commuteTimeSetting.getAddress());
            }
            this.k.S(commuteTimeSetting.getAvoidTolls());
        }
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void a(String str) {
        ee7.b(str, "address");
        if (this.j) {
            this.j = false;
            return;
        }
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null && !TextUtils.isEmpty(str)) {
            commuteTimeSetting.setAddress(str);
            this.k.t(str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v06
    public void a(String str, String str2, bb5 bb5, boolean z, String str3) {
        ee7.b(str, "displayInfo");
        ee7.b(str2, "address");
        ee7.b(bb5, "directionBy");
        ee7.b(str3, "format");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = n;
        local.d(str4, "saveCommuteTimeSetting - displayInfo=" + str + ", address=" + str2 + ", directionBy=" + bb5.getType() + ", avoidTolls=" + z + ", format=" + str3);
        this.k.b();
        CommuteTimeSetting commuteTimeSetting = this.g;
        if (commuteTimeSetting != null) {
            boolean z2 = true;
            if (str.length() == 0) {
                if (str2.length() != 0) {
                    z2 = false;
                }
                if (z2) {
                    commuteTimeSetting.setAddress(str);
                }
            } else {
                commuteTimeSetting.setAddress(str2);
            }
            commuteTimeSetting.setAvoidTolls(z);
            commuteTimeSetting.setFormat(str3);
        }
        ik7 unused = xh7.b(e(), null, null, new b(this, str2, str, null), 3, null);
    }
}
