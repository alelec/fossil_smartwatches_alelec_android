package com.fossil;

import com.facebook.stetho.dumpapp.plugins.CrashDumperPlugin;
import com.fossil.o14;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x24 {
    @DexIgnore
    public /* final */ y34 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ c34 a;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService b;
        @DexIgnore
        public /* final */ /* synthetic */ s74 c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;
        @DexIgnore
        public /* final */ /* synthetic */ y34 e;

        @DexIgnore
        public a(c34 c34, ExecutorService executorService, s74 s74, boolean z, y34 y34) {
            this.a = c34;
            this.b = executorService;
            this.c = s74;
            this.d = z;
            this.e = y34;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            this.a.a(this.b, this.c);
            if (!this.d) {
                return null;
            }
            this.e.b(this.c);
            return null;
        }
    }

    @DexIgnore
    public x24(y34 y34) {
        this.a = y34;
    }

    @DexIgnore
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v8, types: [com.fossil.h34] */
    /* JADX WARN: Type inference failed for: r5v2, types: [com.fossil.v24] */
    /* JADX WARN: Type inference failed for: r6v2, types: [com.fossil.e34, com.fossil.f34] */
    /* JADX WARNING: Unknown variable types count: 3 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.x24 a(com.fossil.l14 r16, com.fossil.nb4 r17, com.fossil.y24 r18, com.fossil.o14 r19) {
        /*
            r8 = r16
            r0 = r19
            android.content.Context r9 = r16.b()
            java.lang.String r1 = r9.getPackageName()
            com.fossil.j44 r2 = new com.fossil.j44
            r3 = r17
            r2.<init>(r9, r1, r3)
            com.fossil.e44 r4 = new com.fossil.e44
            r4.<init>(r8)
            if (r18 != 0) goto L_0x0021
            com.fossil.a34 r1 = new com.fossil.a34
            r1.<init>()
            r3 = r1
            goto L_0x0023
        L_0x0021:
            r3 = r18
        L_0x0023:
            com.fossil.c34 r11 = new com.fossil.c34
            r11.<init>(r8, r9, r2, r4)
            if (r0 == 0) goto L_0x0071
            com.fossil.z24 r1 = com.fossil.z24.a()
            java.lang.String r5 = "Firebase Analytics is available."
            r1.a(r5)
            com.fossil.h34 r1 = new com.fossil.h34
            r1.<init>(r0)
            com.fossil.v24 r5 = new com.fossil.v24
            r5.<init>()
            com.fossil.o14$a r0 = a(r0, r5)
            if (r0 == 0) goto L_0x0062
            com.fossil.z24 r0 = com.fossil.z24.a()
            java.lang.String r6 = "Firebase Analytics listener registered successfully."
            r0.a(r6)
            com.fossil.g34 r0 = new com.fossil.g34
            r0.<init>()
            com.fossil.f34 r6 = new com.fossil.f34
            r7 = 500(0x1f4, float:7.0E-43)
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.MILLISECONDS
            r6.<init>(r1, r7, r10)
            r5.a(r0)
            r5.b(r6)
            r1 = r6
            goto L_0x0084
        L_0x0062:
            com.fossil.z24 r0 = com.fossil.z24.a()
            java.lang.String r5 = "Firebase Analytics listener registration failed."
            r0.a(r5)
            com.fossil.l34 r0 = new com.fossil.l34
            r0.<init>()
            goto L_0x0084
        L_0x0071:
            com.fossil.z24 r0 = com.fossil.z24.a()
            java.lang.String r1 = "Firebase Analytics is unavailable."
            r0.a(r1)
            com.fossil.l34 r0 = new com.fossil.l34
            r0.<init>()
            com.fossil.i34 r1 = new com.fossil.i34
            r1.<init>()
        L_0x0084:
            r5 = r0
            r6 = r1
            java.lang.String r0 = "Crashlytics Exception Handler"
            java.util.concurrent.ExecutorService r7 = com.fossil.h44.a(r0)
            com.fossil.y34 r15 = new com.fossil.y34
            r0 = r15
            r1 = r16
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            boolean r0 = r11.d()
            if (r0 != 0) goto L_0x00a5
            com.fossil.z24 r0 = com.fossil.z24.a()
            java.lang.String r1 = "Unable to start Crashlytics."
            r0.b(r1)
            r0 = 0
            return r0
        L_0x00a5:
            java.lang.String r0 = "com.google.firebase.crashlytics.startup"
            java.util.concurrent.ExecutorService r0 = com.fossil.h44.a(r0)
            com.fossil.s74 r13 = r11.a(r9, r8, r0)
            boolean r14 = r15.d(r13)
            com.fossil.x24$a r1 = new com.fossil.x24$a
            r10 = r1
            r12 = r0
            r2 = r15
            r10.<init>(r11, r12, r13, r14, r15)
            com.fossil.qo3.a(r0, r1)
            com.fossil.x24 r0 = new com.fossil.x24
            r0.<init>(r2)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.x24.a(com.fossil.l14, com.fossil.nb4, com.fossil.y24, com.fossil.o14):com.fossil.x24");
    }

    @DexIgnore
    public static o14.a a(o14 o14, v24 v24) {
        o14.a a2 = o14.a("clx", v24);
        if (a2 == null) {
            z24.a().a("Could not register AnalyticsConnectorListener with Crashlytics origin.");
            a2 = o14.a(CrashDumperPlugin.NAME, v24);
            if (a2 != null) {
                z24.a().d("A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version.");
            }
        }
        return a2;
    }

    @DexIgnore
    public static x24 a() {
        x24 x24 = (x24) l14.j().a(x24.class);
        if (x24 != null) {
            return x24;
        }
        throw new NullPointerException("FirebaseCrashlytics component is not present.");
    }

    @DexIgnore
    public void a(String str) {
        this.a.b(str);
    }

    @DexIgnore
    public void a(String str, String str2) {
        this.a.a(str, str2);
    }
}
