package com.fossil;

import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wt7 implements tt7 {
    @DexIgnore
    public String a;
    @DexIgnore
    public eu7 b;
    @DexIgnore
    public Queue<zt7> c;

    @DexIgnore
    public wt7(eu7 eu7, Queue<zt7> queue) {
        this.b = eu7;
        this.a = eu7.c();
        this.c = queue;
    }

    @DexIgnore
    public final void a(xt7 xt7, String str, Object[] objArr, Throwable th) {
        a(xt7, null, str, objArr, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str) {
        a(xt7.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str) {
        a(xt7.ERROR, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str) {
        a(xt7.INFO, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isDebugEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isErrorEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isInfoEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isTraceEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public boolean isWarnEnabled() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str) {
        a(xt7.TRACE, str, null, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str) {
        a(xt7.WARN, str, null, null);
    }

    @DexIgnore
    public final void a(xt7 xt7, vt7 vt7, String str, Object[] objArr, Throwable th) {
        zt7 zt7 = new zt7();
        zt7.a(System.currentTimeMillis());
        zt7.a(xt7);
        zt7.a(this.b);
        zt7.a(this.a);
        zt7.a(vt7);
        zt7.b(str);
        zt7.a(objArr);
        zt7.a(th);
        zt7.c(Thread.currentThread().getName());
        this.c.add(zt7);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object obj) {
        a(xt7.DEBUG, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object obj) {
        a(xt7.ERROR, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object obj) {
        a(xt7.INFO, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object obj) {
        a(xt7.TRACE, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object obj) {
        a(xt7.WARN, str, new Object[]{obj}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object obj, Object obj2) {
        a(xt7.DEBUG, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object obj, Object obj2) {
        a(xt7.ERROR, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object obj, Object obj2) {
        a(xt7.INFO, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object obj, Object obj2) {
        a(xt7.TRACE, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object obj, Object obj2) {
        a(xt7.WARN, str, new Object[]{obj, obj2}, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Object... objArr) {
        a(xt7.DEBUG, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Object... objArr) {
        a(xt7.ERROR, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Object... objArr) {
        a(xt7.INFO, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Object... objArr) {
        a(xt7.TRACE, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Object... objArr) {
        a(xt7.WARN, str, objArr, null);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void debug(String str, Throwable th) {
        a(xt7.DEBUG, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void error(String str, Throwable th) {
        a(xt7.ERROR, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void info(String str, Throwable th) {
        a(xt7.INFO, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void trace(String str, Throwable th) {
        a(xt7.TRACE, str, null, th);
    }

    @DexIgnore
    @Override // com.fossil.tt7
    public void warn(String str, Throwable th) {
        a(xt7.WARN, str, null, th);
    }
}
