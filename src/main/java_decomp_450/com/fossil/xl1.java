package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.f60;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xl1 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Serializable serializableExtra = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE");
            if (serializableExtra != null) {
                f60.c cVar = (f60.c) serializableExtra;
                Serializable serializableExtra2 = intent.getSerializableExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE");
                if (serializableExtra2 != null) {
                    xm0.i.a((f60.c) serializableExtra2);
                    return;
                }
                throw new x87("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
            }
            throw new x87("null cannot be cast to non-null type com.fossil.blesdk.adapter.BluetoothLeAdapter.State");
        }
    }
}
