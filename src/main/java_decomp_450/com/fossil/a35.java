package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class a35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ TabLayout t;
    @DexIgnore
    public /* final */ FlexibleProgressBar u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ ViewPager2 w;

    @DexIgnore
    public a35(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, TabLayout tabLayout, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = flexibleTextView2;
        this.s = flexibleTextView3;
        this.t = tabLayout;
        this.u = flexibleProgressBar;
        this.v = constraintLayout;
        this.w = viewPager2;
    }
}
