package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import java.util.ArrayList;
import java.util.Collection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jv4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<ArrayList<Style>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<ArrayList<Style>> {
    }

    @DexIgnore
    public final String a(ArrayList<Style> arrayList) {
        if (o92.a((Collection<?>) arrayList)) {
            return "";
        }
        try {
            String a2 = new Gson().a(arrayList, new b().getType());
            ee7.a((Object) a2, "Gson().toJson(styles, type)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ThemeConverter", "error when convert fromStyleToJson ex=" + e);
            return "";
        }
    }

    @DexIgnore
    public final ArrayList<Style> a(String str) {
        if (TextUtils.isEmpty(str)) {
            return new ArrayList<>();
        }
        try {
            Object a2 = new Gson().a(str, new a().getType());
            ee7.a(a2, "Gson().fromJson(json, type)");
            return (ArrayList) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ThemeConverter", "error when convert fromJsonToStyle ex=" + e);
            return new ArrayList<>();
        }
    }
}
