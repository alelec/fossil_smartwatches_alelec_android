package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ii3 implements ki3 {
    @DexIgnore
    public /* final */ oh3 a;

    @DexIgnore
    public ii3(oh3 oh3) {
        a72.a(oh3);
        this.a = oh3;
    }

    @DexIgnore
    public void a() {
        this.a.i();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public xm3 b() {
        return this.a.b();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public hh3 c() {
        return this.a.c();
    }

    @DexIgnore
    public void d() {
        this.a.c().d();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public jg3 e() {
        return this.a.e();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public Context f() {
        return this.a.f();
    }

    @DexIgnore
    public void g() {
        this.a.c().g();
    }

    @DexIgnore
    public ob3 h() {
        return this.a.F();
    }

    @DexIgnore
    public hg3 i() {
        return this.a.w();
    }

    @DexIgnore
    public jm3 j() {
        return this.a.v();
    }

    @DexIgnore
    public wg3 k() {
        return this.a.p();
    }

    @DexIgnore
    public ym3 l() {
        return this.a.o();
    }

    @DexIgnore
    @Override // com.fossil.ki3
    public n92 zzm() {
        return this.a.zzm();
    }
}
