package com.fossil;

import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma extends ClickableSpan {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ oa b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public ma(int i, oa oaVar, int i2) {
        this.a = i;
        this.b = oaVar;
        this.c = i2;
    }

    @DexIgnore
    public void onClick(View view) {
        Bundle bundle = new Bundle();
        bundle.putInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", this.a);
        this.b.a(this.c, bundle);
    }
}
