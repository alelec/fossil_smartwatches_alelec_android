package com.fossil;

import com.facebook.internal.Utility;
import com.fossil.lo7;
import com.zendesk.sdk.network.Constants;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yw0 implements Interceptor {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public yw0(String str, String str2) {
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        String str;
        String valueOf = String.valueOf(System.currentTimeMillis() / ((long) 1000));
        lo7.a f = chain.c().f();
        f.b("Content-Type", Constants.APPLICATION_JSON);
        f.b("X-Cyc-Auth-Method", "signature");
        f.b("X-Cyc-Access-Key-Id", this.a);
        f.b("X-Cyc-Timestamp", valueOf);
        StringBuilder b2 = yh0.b("Signature=");
        String str2 = this.b;
        try {
            MessageDigest instance = MessageDigest.getInstance(Utility.HASH_ALGORITHM_SHA1);
            String str3 = valueOf + str2;
            Charset c = b21.x.c();
            if (str3 != null) {
                byte[] bytes = str3.getBytes(c);
                ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                instance.update(bytes);
                byte[] digest = instance.digest();
                StringBuilder sb = new StringBuilder();
                for (byte b3 : digest) {
                    we7 we7 = we7.a;
                    String format = String.format("%02X", Arrays.copyOf(new Object[]{Byte.valueOf(b3)}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    sb.append(format);
                }
                str = sb.toString();
                ee7.a((Object) str, "stringBuilder.toString()");
                b2.append(str);
                f.b("Authorization", b2.toString());
                Response a2 = chain.a(f.a());
                ee7.a((Object) a2, "chain.proceed(request)");
                return a2;
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        } catch (Exception e) {
            wl0.h.a(e);
            str = "";
        }
    }
}
