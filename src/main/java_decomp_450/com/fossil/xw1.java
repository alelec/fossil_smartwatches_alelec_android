package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xw1 implements Factory<tw1> {
    @DexIgnore
    public static /* final */ xw1 a; // = new xw1();

    @DexIgnore
    public static xw1 a() {
        return a;
    }

    @DexIgnore
    public static tw1 b() {
        tw1 c = uw1.c();
        c87.a(c, "Cannot return null from a non-@Nullable @Provides method");
        return c;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public tw1 get() {
        return b();
    }
}
