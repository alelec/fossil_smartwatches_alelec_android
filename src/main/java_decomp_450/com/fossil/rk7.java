package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rk7<T> extends ij7<T> {
    @DexIgnore
    public /* final */ fb7<i97> d;

    @DexIgnore
    public rk7(ib7 ib7, kd7<? super yi7, ? super fb7<? super T>, ? extends Object> kd7) {
        super(ib7, false);
        this.d = mb7.a(kd7, this, this);
    }

    @DexIgnore
    @Override // com.fossil.rh7
    public void o() {
        tm7.a(this.d, this);
    }
}
