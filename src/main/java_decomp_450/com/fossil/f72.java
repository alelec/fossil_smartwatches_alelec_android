package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import com.fossil.v02;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f72<T extends IInterface> extends n62<T> {
    @DexIgnore
    public /* final */ v02.h<T> E;

    @DexIgnore
    public v02.h<T> I() {
        return this.E;
    }

    @DexIgnore
    @Override // com.fossil.h62
    public T a(IBinder iBinder) {
        return this.E.a(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public void b(int i, T t) {
        this.E.a(i, t);
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String i() {
        return this.E.i();
    }

    @DexIgnore
    @Override // com.fossil.h62
    public String p() {
        return this.E.p();
    }
}
