package com.fossil;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Deprecated
public abstract class d50<T extends View, Z> extends v40<Z> {
    @DexIgnore
    public static int f; // = fw.glide_custom_view_target_tag;
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public View.OnAttachStateChangeListener c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public d50(T t) {
        u50.a(t);
        this.a = t;
        this.b = new a(t);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void a(b50 b50) {
        this.b.b(b50);
    }

    @DexIgnore
    @Override // com.fossil.c50, com.fossil.v40
    public void b(Drawable drawable) {
        super.b(drawable);
        c();
    }

    @DexIgnore
    public final void c() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.c;
        if (onAttachStateChangeListener != null && !this.e) {
            this.a.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.e = true;
        }
    }

    @DexIgnore
    public final void d() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.c;
        if (onAttachStateChangeListener != null && this.e) {
            this.a.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.e = false;
        }
    }

    @DexIgnore
    public String toString() {
        return "Target for: " + this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static Integer e;
        @DexIgnore
        public /* final */ View a;
        @DexIgnore
        public /* final */ List<b50> b; // = new ArrayList();
        @DexIgnore
        public boolean c;
        @DexIgnore
        public ViewTreeObserver$OnPreDrawListenerC0035a d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d50$a$a")
        /* renamed from: com.fossil.d50$a$a  reason: collision with other inner class name */
        public static final class ViewTreeObserver$OnPreDrawListenerC0035a implements ViewTreeObserver.OnPreDrawListener {
            @DexIgnore
            public /* final */ WeakReference<a> a;

            @DexIgnore
            public ViewTreeObserver$OnPreDrawListenerC0035a(a aVar) {
                this.a = new WeakReference<>(aVar);
            }

            @DexIgnore
            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                a aVar = this.a.get();
                if (aVar == null) {
                    return true;
                }
                aVar.a();
                return true;
            }
        }

        @DexIgnore
        public a(View view) {
            this.a = view;
        }

        @DexIgnore
        public static int a(Context context) {
            if (e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                u50.a(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return e.intValue();
        }

        @DexIgnore
        public final boolean a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        @DexIgnore
        public final void b(int i, int i2) {
            Iterator it = new ArrayList(this.b).iterator();
            while (it.hasNext()) {
                ((b50) it.next()).a(i, i2);
            }
        }

        @DexIgnore
        public final int c() {
            int paddingTop = this.a.getPaddingTop() + this.a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        @DexIgnore
        public final int d() {
            int paddingLeft = this.a.getPaddingLeft() + this.a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
            return a(this.a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        @DexIgnore
        public void b(b50 b50) {
            this.b.remove(b50);
        }

        @DexIgnore
        public void b() {
            ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.d);
            }
            this.d = null;
            this.b.clear();
        }

        @DexIgnore
        public void a() {
            if (!this.b.isEmpty()) {
                int d2 = d();
                int c2 = c();
                if (a(d2, c2)) {
                    b(d2, c2);
                    b();
                }
            }
        }

        @DexIgnore
        public void a(b50 b50) {
            int d2 = d();
            int c2 = c();
            if (a(d2, c2)) {
                b50.a(d2, c2);
                return;
            }
            if (!this.b.contains(b50)) {
                this.b.add(b50);
            }
            if (this.d == null) {
                ViewTreeObserver viewTreeObserver = this.a.getViewTreeObserver();
                ViewTreeObserver$OnPreDrawListenerC0035a aVar = new ViewTreeObserver$OnPreDrawListenerC0035a(this);
                this.d = aVar;
                viewTreeObserver.addOnPreDrawListener(aVar);
            }
        }

        @DexIgnore
        public final boolean a(int i, int i2) {
            return a(i) && a(i2);
        }

        @DexIgnore
        public final int a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.c && this.a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return a(this.a.getContext());
        }
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void a(n40 n40) {
        a((Object) n40);
    }

    @DexIgnore
    @Override // com.fossil.c50
    public n40 a() {
        Object b2 = b();
        if (b2 == null) {
            return null;
        }
        if (b2 instanceof n40) {
            return (n40) b2;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @DexIgnore
    @Override // com.fossil.c50
    public void b(b50 b50) {
        this.b.a(b50);
    }

    @DexIgnore
    public final Object b() {
        return this.a.getTag(f);
    }

    @DexIgnore
    @Override // com.fossil.c50, com.fossil.v40
    public void c(Drawable drawable) {
        super.c(drawable);
        this.b.b();
        if (!this.d) {
            d();
        }
    }

    @DexIgnore
    public final void a(Object obj) {
        this.a.setTag(f, obj);
    }
}
