package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tb0 extends yb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<tb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public tb0 createFromParcel(Parcel parcel) {
            return new tb0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public tb0[] newArray(int i) {
            return new tb0[i];
        }
    }

    @DexIgnore
    public tb0(byte b, int i) {
        super(cb0.BUDDY_CHALLENGE_SYNC_DATA, b, i);
    }

    @DexIgnore
    public /* synthetic */ tb0(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
