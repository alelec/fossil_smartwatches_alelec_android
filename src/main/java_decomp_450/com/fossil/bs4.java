package com.fossil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.widget.ViewPager2;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.indicator.CustomPageIndicator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs4 extends go5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public qw6<mx4> f;
    @DexIgnore
    public rj4 g;
    @DexIgnore
    public /* final */ List<Fragment> h; // = new ArrayList();
    @DexIgnore
    public int i;
    @DexIgnore
    public String j; // = "";
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return bs4.q;
        }

        @DexIgnore
        public final bs4 b() {
            return new bs4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ mx4 a;
        @DexIgnore
        public /* final */ /* synthetic */ bs4 b;

        @DexIgnore
        public b(mx4 mx4, bs4 bs4, wr4 wr4) {
            this.a = mx4;
            this.b = bs4;
        }

        @DexIgnore
        @Override // androidx.viewpager2.widget.ViewPager2.i
        public void b(int i) {
            super.b(i);
            this.b.i = i;
            this.a.q.setCurrentPage(i);
            this.b.j = i != 0 ? i != 1 ? i != 2 ? "" : "bc_challenge_create" : "bc_challenge_list" : "bc_challenge_tab";
            qd5 c = qd5.f.c();
            String f1 = this.b.f1();
            FragmentActivity activity = this.b.getActivity();
            if (activity != null) {
                c.a(f1, activity);
                return;
            }
            throw new x87("null cannot be cast to non-null type android.app.Activity");
        }
    }

    /*
    static {
        String simpleName = bs4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCChallengeTabFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final String f1() {
        return this.j;
    }

    @DexIgnore
    public final void g1() {
        FLogger.INSTANCE.getLocal().e(q, "Inside .initTabs");
        Fragment b2 = getChildFragmentManager().b(ms4.r.a());
        Fragment b3 = getChildFragmentManager().b(ss4.r.a());
        Fragment b4 = getChildFragmentManager().b(gs4.p.a());
        if (b2 == null) {
            b2 = ms4.r.b();
        }
        if (b3 == null) {
            b3 = ss4.r.b();
        }
        if (b4 == null) {
            b4 = gs4.p.b();
        }
        this.h.clear();
        this.h.add(b2);
        this.h.add(b3);
        this.h.add(b4);
        wr4 wr4 = new wr4(this.h, this);
        qw6<mx4> qw6 = this.f;
        if (qw6 != null) {
            mx4 a2 = qw6.a();
            if (a2 != null) {
                ViewPager2 viewPager2 = a2.s;
                ee7.a((Object) viewPager2, "this");
                viewPager2.setAdapter(wr4);
                viewPager2.setOffscreenPageLimit(this.h.size());
                a2.s.a(new b(a2, this, wr4));
                CustomPageIndicator customPageIndicator = a2.q;
                ee7.a((Object) customPageIndicator, "indicator");
                a(customPageIndicator, this.h.size());
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void n(int i2) {
        ViewPager2 viewPager2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "scroll to position=" + i2 + " fragment " + this.h.get(i2));
        qw6<mx4> qw6 = this.f;
        if (qw6 != null) {
            mx4 a2 = qw6.a();
            if (!(a2 == null || (viewPager2 = a2.s) == null)) {
                viewPager2.a(i2, true);
            }
            this.j = "bc_challenge_tab";
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        T t;
        T t2;
        super.onActivityResult(i2, i3, intent);
        int i4 = -1;
        if (i2 != 11) {
            if (i2 == 13 && i3 == -1) {
                n(0);
                if (intent != null) {
                    i4 = intent.getIntExtra("index_extra", -1);
                }
                Iterator<T> it = this.h.iterator();
                while (true) {
                    t = null;
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it.next();
                    if (t2 instanceof ss4) {
                        break;
                    }
                }
                T t3 = t2;
                if (t3 instanceof ss4) {
                    t = t3;
                }
                T t4 = t;
                if (t4 != null) {
                    t4.n(i4);
                }
            }
        } else if (i3 == -1) {
            n(0);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().t().a(this);
        rj4 rj4 = this.g;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(ds4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            ds4 ds4 = (ds4) a2;
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        mx4 mx4 = (mx4) qb.a(layoutInflater, 2131558504, viewGroup, false, a1());
        this.f = new qw6<>(this, mx4);
        ee7.a((Object) mx4, "binding");
        return mx4.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        qd5 c = qd5.f.c();
        String str = this.j;
        FragmentActivity activity = getActivity();
        if (activity != null) {
            c.a(str, activity);
            return;
        }
        throw new x87("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        g1();
    }

    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public final void a(CustomPageIndicator customPageIndicator, int i2) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        hashMap.put(0, 2131230941);
        hashMap.put(Integer.valueOf(this.h.size() - 1), 2131230817);
        customPageIndicator.a(hashMap);
        customPageIndicator.setNumberOfPage(i2);
    }
}
