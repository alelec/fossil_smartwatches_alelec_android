package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ww4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ RTLImageView F;
    @DexIgnore
    public /* final */ RTLImageView G;
    @DexIgnore
    public /* final */ View H;
    @DexIgnore
    public /* final */ LinearLayout I;
    @DexIgnore
    public /* final */ FlexibleProgressBar J;
    @DexIgnore
    public /* final */ ConstraintLayout K;
    @DexIgnore
    public /* final */ RecyclerView L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ View N;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ OverviewDayChart v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    public ww4(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, View view2, LinearLayout linearLayout, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout5, RecyclerView recyclerView, FlexibleTextView flexibleTextView9, View view3) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = constraintLayout3;
        this.u = constraintLayout4;
        this.v = overviewDayChart;
        this.w = flexibleTextView;
        this.x = flexibleTextView2;
        this.y = flexibleTextView3;
        this.z = flexibleTextView4;
        this.A = flexibleTextView5;
        this.B = flexibleTextView6;
        this.C = flexibleTextView7;
        this.D = flexibleTextView8;
        this.E = rTLImageView;
        this.F = rTLImageView2;
        this.G = rTLImageView3;
        this.H = view2;
        this.I = linearLayout;
        this.J = flexibleProgressBar;
        this.K = constraintLayout5;
        this.L = recyclerView;
        this.M = flexibleTextView9;
        this.N = view3;
    }
}
