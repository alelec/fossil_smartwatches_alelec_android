package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lk0 extends k60 {
    @DexIgnore
    public static /* final */ mo1 d; // = new mo1(null);
    @DexIgnore
    public /* final */ aq0 a;
    @DexIgnore
    public /* final */ oi0 b;
    @DexIgnore
    public /* final */ x71 c;

    @DexIgnore
    public /* synthetic */ lk0(aq0 aq0, oi0 oi0, x71 x71, int i) {
        aq0 = (i & 1) != 0 ? aq0.n : aq0;
        x71 = (i & 4) != 0 ? new x71(z51.SUCCESS, 0, 2) : x71;
        this.a = aq0;
        this.b = oi0;
        this.c = x71;
    }

    @DexIgnore
    public static /* synthetic */ lk0 a(lk0 lk0, aq0 aq0, oi0 oi0, x71 x71, int i) {
        if ((i & 1) != 0) {
            aq0 = lk0.a;
        }
        if ((i & 2) != 0) {
            oi0 = lk0.b;
        }
        if ((i & 4) != 0) {
            x71 = lk0.c;
        }
        return lk0.a(aq0, oi0, x71);
    }

    @DexIgnore
    public final lk0 a(aq0 aq0, oi0 oi0, x71 x71) {
        return new lk0(aq0, oi0, x71);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            yz0.a(yz0.a(jSONObject, r51.C2, yz0.a(this.a)), r51.M0, yz0.a(this.b));
            if (this.c.a != z51.SUCCESS) {
                yz0.a(jSONObject, r51.D2, this.c.a());
            }
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lk0)) {
            return false;
        }
        lk0 lk0 = (lk0) obj;
        return ee7.a(this.a, lk0.a) && ee7.a(this.b, lk0.b) && ee7.a(this.c, lk0.c);
    }

    @DexIgnore
    public int hashCode() {
        aq0 aq0 = this.a;
        int i = 0;
        int hashCode = (aq0 != null ? aq0.hashCode() : 0) * 31;
        oi0 oi0 = this.b;
        int hashCode2 = (hashCode + (oi0 != null ? oi0.hashCode() : 0)) * 31;
        x71 x71 = this.c;
        if (x71 != null) {
            i = x71.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public String toString() {
        StringBuilder b2 = yh0.b("Result(commandId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", gattResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public lk0(aq0 aq0, oi0 oi0, x71 x71) {
        this.a = aq0;
        this.b = oi0;
        this.c = x71;
    }
}
