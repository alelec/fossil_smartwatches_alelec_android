package com.fossil;

import android.database.Cursor;
import android.database.SQLException;
import android.os.CancellationSignal;
import android.util.Pair;
import java.io.Closeable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wi extends Closeable {
    @DexIgnore
    void beginTransaction();

    @DexIgnore
    aj compileStatement(String str);

    @DexIgnore
    void endTransaction();

    @DexIgnore
    void execSQL(String str) throws SQLException;

    @DexIgnore
    void execSQL(String str, Object[] objArr) throws SQLException;

    @DexIgnore
    List<Pair<String, String>> getAttachedDbs();

    @DexIgnore
    String getPath();

    @DexIgnore
    boolean inTransaction();

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    Cursor query(zi ziVar);

    @DexIgnore
    Cursor query(zi ziVar, CancellationSignal cancellationSignal);

    @DexIgnore
    Cursor query(String str);

    @DexIgnore
    void setTransactionSuccessful();
}
