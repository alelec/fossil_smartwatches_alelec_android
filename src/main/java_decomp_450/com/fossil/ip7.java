package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ip7 {
    @DexIgnore
    mo7 a(Response response) throws IOException;

    @DexIgnore
    qr7 a(lo7 lo7, long j);

    @DexIgnore
    Response.a a(boolean z) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(lo7 lo7) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    void cancel();
}
