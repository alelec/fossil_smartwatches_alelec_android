package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i23 implements j23 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;
    @DexIgnore
    public static /* final */ tq2<Boolean> c;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.client.global_params.dev", false);
        b = dr2.a("measurement.service.global_params_in_payload", true);
        c = dr2.a("measurement.service.global_params", false);
        dr2.a("measurement.id.service.global_params", 0L);
    }
    */

    @DexIgnore
    @Override // com.fossil.j23
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final boolean zzb() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final boolean zzc() {
        return b.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.j23
    public final boolean zzd() {
        return c.b().booleanValue();
    }
}
