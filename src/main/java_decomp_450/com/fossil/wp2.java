package com.fossil;

import com.fossil.bw2;
import com.j256.ormlite.stmt.query.SimpleComparison;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wp2 extends bw2<wp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ wp2 zzf;
    @DexIgnore
    public static volatile wx2<wp2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd; // = 1;
    @DexIgnore
    public jw2<sp2> zze; // = bw2.o();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<wp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(wp2.zzf);
        }

        @DexIgnore
        public /* synthetic */ a(op2 op2) {
            this();
        }
    }

    @DexIgnore
    public enum b implements dw2 {
        RADS(1),
        PROVISIONING(2);
        
        @DexIgnore
        public /* final */ int zzd;

        /*
        static {
            new aq2();
        }
        */

        @DexIgnore
        public b(int i) {
            this.zzd = i;
        }

        @DexIgnore
        public static fw2 zzb() {
            return bq2.a;
        }

        @DexIgnore
        public final String toString() {
            return SimpleComparison.LESS_THAN_OPERATION + b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
        }

        @DexIgnore
        @Override // com.fossil.dw2
        public final int zza() {
            return this.zzd;
        }

        @DexIgnore
        public static b zza(int i) {
            if (i == 1) {
                return RADS;
            }
            if (i != 2) {
                return null;
            }
            return PROVISIONING;
        }
    }

    /*
    static {
        wp2 wp2 = new wp2();
        zzf = wp2;
        bw2.a(wp2.class, wp2);
    }
    */

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (op2.a[i - 1]) {
            case 1:
                return new wp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u100c\u0000\u0002\u001b", new Object[]{"zzc", "zzd", b.zzb(), "zze", sp2.class});
            case 4:
                return zzf;
            case 5:
                wx2<wp2> wx2 = zzg;
                if (wx2 == null) {
                    synchronized (wp2.class) {
                        wx2 = zzg;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzf);
                            zzg = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
