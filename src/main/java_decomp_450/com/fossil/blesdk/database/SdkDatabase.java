package com.fossil.blesdk.database;

import android.content.Context;
import android.database.Cursor;
import com.fossil.bi;
import com.fossil.bi1;
import com.fossil.ci;
import com.fossil.ee7;
import com.fossil.ik1;
import com.fossil.li;
import com.fossil.ng1;
import com.fossil.rc1;
import com.fossil.u31;
import com.fossil.wi;
import com.fossil.yh0;
import com.fossil.yz0;
import com.fossil.zd7;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SdkDatabase extends ci {
    @DexIgnore
    public static SdkDatabase a;
    @DexIgnore
    public static /* final */ li b; // = new a(1, 2);
    @DexIgnore
    public static /* final */ li c; // = new b(2, 3);
    @DexIgnore
    public static /* final */ li d; // = new c(3, 4);
    @DexIgnore
    public static /* final */ d e; // = new d(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends li {
        @DexIgnore
        public a(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("DROP TABLE ActivityFile");
            wiVar.execSQL("CREATE TABLE DeviceFile(deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, PRIMARY KEY(deviceMacAddress, fileType, fileIndex))");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends li {
        @DexIgnore
        public b(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
            wiVar.execSQL("CREATE TABLE DeviceFile_New(id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deviceMacAddress TEXT NOT NULL, fileType INTEGER NOT NULL, fileIndex INTEGER NOT NULL, rawData BLOB NOT NULL, fileLength INTEGER NOT NULL, fileCrc INTEGER NOT NULL, createdTimeStamp INTEGER NOT NULL, isCompleted INTEGER NOT NULL)");
            Cursor query = wiVar.query("SELECT * from DeviceFile");
            ee7.a((Object) query, "database.query(\"SELECT * from DeviceFile\")");
            int columnIndex = query.getColumnIndex("deviceMacAddress");
            int columnIndex2 = query.getColumnIndex("fileType");
            int columnIndex3 = query.getColumnIndex("fileIndex");
            int columnIndex4 = query.getColumnIndex("rawData");
            int columnIndex5 = query.getColumnIndex("fileLength");
            int columnIndex6 = query.getColumnIndex("fileCrc");
            int columnIndex7 = query.getColumnIndex("createdTimeStamp");
            while (query.moveToNext()) {
                String string = query.getString(columnIndex);
                ee7.a((Object) string, "oldDataCursor.getString(\u2026iceMacAddressColumnIndex)");
                byte b = (byte) query.getShort(columnIndex2);
                byte b2 = (byte) query.getShort(columnIndex3);
                byte[] blob = query.getBlob(columnIndex4);
                ee7.a((Object) blob, "oldDataCursor.getBlob(rawDataColumnIndex)");
                long j = query.getLong(columnIndex5);
                long j2 = query.getLong(columnIndex6);
                long j3 = query.getLong(columnIndex7);
                int i = j2 == ik1.a.a(blob, ng1.CRC32) ? 1 : 0;
                wiVar.execSQL("Insert into DeviceFile_New(deviceMacAddress, fileType, fileIndex, rawData, fileLength, fileCrc, createdTimeStamp, isCompleted) values ('" + string + "', " + ((int) b) + ", " + ((int) b2) + ", X'" + yz0.a(blob, (String) null, 1) + "', " + j + ", " + j2 + ", " + j3 + ", " + i + ')');
                columnIndex = columnIndex;
                columnIndex2 = columnIndex2;
                columnIndex3 = columnIndex3;
                columnIndex4 = columnIndex4;
                query = query;
                columnIndex5 = columnIndex5;
                columnIndex6 = columnIndex6;
            }
            wiVar.execSQL("DROP TABLE DeviceFile");
            wiVar.execSQL("ALTER TABLE DeviceFile_New RENAME TO DeviceFile");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends li {
        @DexIgnore
        public c(int i, int i2) {
            super(i, i2);
        }

        @DexIgnore
        @Override // com.fossil.li
        public void migrate(wi wiVar) {
        }
    }

    @DexIgnore
    public abstract rc1 a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* synthetic */ d(zd7 zd7) {
        }

        @DexIgnore
        public final SdkDatabase a() {
            Context a;
            if (SdkDatabase.a == null && (a = u31.g.a()) != null) {
                StringBuilder b = yh0.b("com.fossil.blesdk.database.");
                b.append(u31.g.e());
                ci.a a2 = bi.a(a, SdkDatabase.class, b.toString());
                a2.a(SdkDatabase.e.b(), SdkDatabase.e.c(), SdkDatabase.e.d());
                a2.a();
                ci b2 = a2.b();
                ee7.a((Object) b2, "Room.databaseBuilder(app\u2026                 .build()");
                SdkDatabase sdkDatabase = (SdkDatabase) b2;
                ci.a a3 = bi.a(a, SdkDatabase.class, "device.sdbk");
                a3.a(SdkDatabase.e.b(), SdkDatabase.e.c(), SdkDatabase.e.d());
                a3.a();
                ci b3 = a3.b();
                ee7.a((Object) b3, "Room.databaseBuilder(app\u2026                 .build()");
                SdkDatabase sdkDatabase2 = (SdkDatabase) b3;
                SdkDatabase.e.a(sdkDatabase, sdkDatabase2);
                sdkDatabase.close();
                SdkDatabase.a = sdkDatabase2;
            }
            return SdkDatabase.a;
        }

        @DexIgnore
        public final li b() {
            return SdkDatabase.b;
        }

        @DexIgnore
        public final li c() {
            return SdkDatabase.c;
        }

        @DexIgnore
        public final li d() {
            return SdkDatabase.d;
        }

        @DexIgnore
        public final void a(SdkDatabase sdkDatabase, SdkDatabase sdkDatabase2) {
            List<bi1> a = sdkDatabase.a().a();
            Iterator<T> it = a.iterator();
            while (it.hasNext()) {
                sdkDatabase2.a().a((bi1) it.next());
            }
            Iterator<T> it2 = a.iterator();
            while (it2.hasNext()) {
                sdkDatabase.a().a(((bi1) it2.next()).b);
            }
        }
    }
}
