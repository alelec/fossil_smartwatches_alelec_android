package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nm0 extends wi1<y90[], y90[]> {
    @DexIgnore
    public static /* final */ y71<y90[]>[] b; // = {new ti0(), new qk0()};
    @DexIgnore
    public static /* final */ uk1<y90[]>[] c; // = new uk1[0];
    @DexIgnore
    public static /* final */ nm0 d; // = new nm0();

    @DexIgnore
    @Override // com.fossil.wi1
    public uk1<y90[]>[] b() {
        return c;
    }

    @DexIgnore
    @Override // com.fossil.wi1
    public y71<y90[]>[] a() {
        return b;
    }

    @DexIgnore
    public final byte[] a(y90[] y90Arr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (y90 y90 : y90Arr) {
            byteArrayOutputStream.write(y90.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        ee7.a((Object) byteArray, "entriesData.toByteArray()");
        return byteArray;
    }
}
