package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lb0 extends kb0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ wf0 c;
    @DexIgnore
    public /* final */ vf0 d;
    @DexIgnore
    public /* final */ yf0 e;
    @DexIgnore
    public /* final */ byte[] f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<lb0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public lb0 createFromParcel(Parcel parcel) {
            return new lb0(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public lb0[] newArray(int i) {
            return new lb0[i];
        }
    }

    @DexIgnore
    public lb0(byte b, wf0 wf0, vf0 vf0, yf0 yf0, byte[] bArr, int i, int i2) {
        super(cb0.ENCRYPTED_DATA, b);
        this.c = wf0;
        this.d = vf0;
        this.e = yf0;
        this.f = bArr;
        this.g = i;
        this.h = i2;
    }

    @DexIgnore
    @Override // com.fossil.bb0, com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(super.a(), r51.h0, yz0.a(this.c)), r51.j5, yz0.a(this.d)), r51.k5, yz0.a(this.e)), r51.S0, Long.valueOf(ik1.a.a(this.f, ng1.CRC32))), r51.y5, Integer.valueOf(this.g)), r51.z5, Integer.valueOf(this.h));
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(lb0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            lb0 lb0 = (lb0) obj;
            return this.c == lb0.c && this.d == lb0.d && this.e == lb0.e && Arrays.equals(this.f, lb0.f) && this.g == lb0.g && this.h == lb0.h;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.event.notification.EncryptedDataNotification");
    }

    @DexIgnore
    public final vf0 getEncryptMethod() {
        return this.d;
    }

    @DexIgnore
    public final byte[] getEncryptedData() {
        return this.f;
    }

    @DexIgnore
    public final wf0 getEncryptedDataType() {
        return this.c;
    }

    @DexIgnore
    public final yf0 getKeyType() {
        return this.e;
    }

    @DexIgnore
    public final short getSequence() {
        return yz0.b(b());
    }

    @DexIgnore
    public final int getXorKeyFirstOffset() {
        return this.g;
    }

    @DexIgnore
    public final int getXorKeySecondOffset() {
        return this.h;
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public int hashCode() {
        int hashCode = this.c.hashCode();
        int hashCode2 = this.d.hashCode();
        int hashCode3 = this.e.hashCode();
        int hashCode4 = this.f.hashCode();
        int hashCode5 = Integer.valueOf(this.g).hashCode();
        return Integer.valueOf(this.h).hashCode() + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (super.hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    @Override // com.fossil.bb0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
        if (parcel != null) {
            parcel.writeInt(this.g);
        }
        if (parcel != null) {
            parcel.writeInt(this.h);
        }
    }

    @DexIgnore
    public /* synthetic */ lb0(Parcel parcel, zd7 zd7) {
        super(parcel);
        wf0 a2 = wf0.c.a(parcel.readByte());
        if (a2 != null) {
            this.c = a2;
            vf0 a3 = vf0.c.a(parcel.readByte());
            if (a3 != null) {
                this.d = a3;
                yf0 a4 = yf0.c.a(parcel.readByte());
                if (a4 != null) {
                    this.e = a4;
                    byte[] createByteArray = parcel.createByteArray();
                    this.f = createByteArray == null ? new byte[0] : createByteArray;
                    this.g = parcel.readInt();
                    this.h = parcel.readInt();
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }
}
