package com.fossil;

import android.graphics.Bitmap;
import android.util.Log;
import com.fossil.ns;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs implements ns {
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public /* final */ ds c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends s4<String, ns.b> {
        @DexIgnore
        public /* final */ /* synthetic */ qs i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(qs qsVar, int i2, int i3) {
            super(i3);
            this.i = qsVar;
        }

        @DexIgnore
        public void a(boolean z, String str, ns.b bVar, ns.b bVar2) {
            ee7.b(str, "key");
            ee7.b(bVar, "oldValue");
            this.i.c.a(bVar.a());
        }

        @DexIgnore
        /* renamed from: a */
        public int c(String str, ns.b bVar) {
            ee7.b(str, "key");
            ee7.b(bVar, "value");
            return bVar.b();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public qs(ds dsVar, int i) {
        ee7.b(dsVar, "referenceCounter");
        this.c = dsVar;
        this.b = new b(this, i, i);
    }

    @DexIgnore
    public int b() {
        return this.b.b();
    }

    @DexIgnore
    public int c() {
        return this.b.c();
    }

    @DexIgnore
    @Override // com.fossil.ns
    public ns.b a(String str) {
        ee7.b(str, "key");
        return (ns.b) this.b.b(str);
    }

    @DexIgnore
    @Override // com.fossil.ns
    public void a(String str, Bitmap bitmap, boolean z) {
        ee7.b(str, "key");
        ee7.b(bitmap, "value");
        int a2 = iu.a(bitmap);
        if (a2 > b()) {
            this.b.c(str);
            return;
        }
        this.c.b(bitmap);
        this.b.a((Object) str, (Object) new ns.b(bitmap, z, a2));
    }

    @DexIgnore
    public void a() {
        if (cu.c.a() && cu.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "clearMemory");
        }
        this.b.a(-1);
    }

    @DexIgnore
    @Override // com.fossil.ns
    public void a(int i) {
        if (cu.c.a() && cu.c.b() <= 3) {
            Log.println(3, "RealMemoryCache", "trimMemory, level=" + i);
        }
        if (i >= 40) {
            a();
        } else if (10 <= i && 20 > i) {
            this.b.a(c() / 2);
        }
    }
}
