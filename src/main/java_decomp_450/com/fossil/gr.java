package com.fossil;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr {
    @DexIgnore
    public /* final */ rq a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public gr(rq rqVar) {
        ee7.b(rqVar, "bitmapPool");
        this.a = rqVar;
    }

    @DexIgnore
    public final Bitmap a(Drawable drawable, rt rtVar, Bitmap.Config config) {
        ee7.b(drawable, ResourceManager.DRAWABLE);
        ee7.b(rtVar, "size");
        ee7.b(config, "config");
        Bitmap.Config c = iu.c(config);
        if (drawable instanceof BitmapDrawable) {
            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
            ee7.a((Object) bitmap, "bitmap");
            if (iu.c(bitmap.getConfig()) == c) {
                return bitmap;
            }
        }
        int intrinsicWidth = drawable.getIntrinsicWidth();
        int intrinsicHeight = drawable.getIntrinsicHeight();
        if (intrinsicWidth <= 0) {
            intrinsicWidth = 512;
        }
        if (intrinsicHeight <= 0) {
            intrinsicHeight = 512;
        }
        if (!(rtVar instanceof nt)) {
            if (rtVar instanceof ot) {
                ot otVar = (ot) rtVar;
                double b = er.b(intrinsicWidth, intrinsicHeight, otVar.d(), otVar.c(), qt.FIT);
                intrinsicWidth = af7.a(((double) intrinsicWidth) * b);
                intrinsicHeight = af7.a(b * ((double) intrinsicHeight));
            } else {
                throw new p87();
            }
        }
        Rect bounds = drawable.getBounds();
        int i = bounds.left;
        int i2 = bounds.top;
        int i3 = bounds.right;
        int i4 = bounds.bottom;
        Bitmap a2 = this.a.a(intrinsicWidth, intrinsicHeight, c);
        drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        drawable.draw(new Canvas(a2));
        drawable.setBounds(i, i2, i3, i4);
        return a2;
    }
}
