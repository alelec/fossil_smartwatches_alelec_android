package com.fossil;

import android.content.Context;
import com.facebook.appevents.UserDataStore;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import net.sqlcipher.DatabaseErrorHandler;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteDatabaseHook;
import net.sqlcipher.database.SQLiteStatement;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bh5 {
    @DexIgnore
    public static /* final */ bh5 a; // = new bh5();

    @DexIgnore
    public enum a {
        DOES_NOT_EXIST,
        UNENCRYPTED,
        ENCRYPTED
    }

    @DexIgnore
    public final a a(Context context, String str) {
        ee7.b(context, "ctxt");
        SQLiteDatabase.loadLibs(context);
        File databasePath = context.getDatabasePath(str);
        ee7.a((Object) databasePath, "ctxt.getDatabasePath(dbName)");
        return a(databasePath);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:7|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        if (r0 != null) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002f, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        throw r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0025, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r4 = com.fossil.bh5.a.ENCRYPTED;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0029, code lost:
        if (r0 == null) goto L_0x002c;
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0027 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.bh5.a a(java.io.File r4) {
        /*
            r3 = this;
            java.lang.String r0 = "dbPath"
            com.fossil.ee7.b(r4, r0)
            boolean r0 = r4.exists()
            if (r0 == 0) goto L_0x0033
            r0 = 0
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x0027 }
            java.lang.String r1 = ""
            r2 = 1
            net.sqlcipher.database.SQLiteDatabase r0 = net.sqlcipher.database.SQLiteDatabase.openDatabase(r4, r1, r0, r2)     // Catch:{ Exception -> 0x0027 }
            java.lang.String r4 = "db"
            com.fossil.ee7.a(r0, r4)     // Catch:{ Exception -> 0x0027 }
            r0.getVersion()     // Catch:{ Exception -> 0x0027 }
            com.fossil.bh5$a r4 = com.fossil.bh5.a.UNENCRYPTED     // Catch:{ Exception -> 0x0027 }
        L_0x0021:
            r0.close()
            goto L_0x002c
        L_0x0025:
            r4 = move-exception
            goto L_0x002d
        L_0x0027:
            com.fossil.bh5$a r4 = com.fossil.bh5.a.ENCRYPTED     // Catch:{ all -> 0x0025 }
            if (r0 == 0) goto L_0x002c
            goto L_0x0021
        L_0x002c:
            return r4
        L_0x002d:
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            throw r4
        L_0x0033:
            com.fossil.bh5$a r4 = com.fossil.bh5.a.DOES_NOT_EXIST
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.bh5.a(java.io.File):com.fossil.bh5$a");
    }

    @DexIgnore
    public final void a(Context context, File file, char[] cArr) throws IOException {
        ee7.b(context, "ctxt");
        ee7.b(file, "originalFile");
        a(context, file, SQLiteDatabase.getBytes(cArr));
    }

    @DexIgnore
    public final void a(Context context, File file, byte[] bArr) throws IOException {
        ee7.b(context, "ctxt");
        ee7.b(file, "originalFile");
        SQLiteDatabase.loadLibs(context);
        if (file.exists()) {
            File createTempFile = File.createTempFile("sqlcipherutils", "tmp", context.getCacheDir());
            SQLiteDatabase openDatabase = SQLiteDatabase.openDatabase(file.getAbsolutePath(), "", (SQLiteDatabase.CursorFactory) null, 0);
            ee7.a((Object) openDatabase, UserDataStore.DATE_OF_BIRTH);
            int version = openDatabase.getVersion();
            openDatabase.close();
            ee7.a((Object) createTempFile, "newFile");
            SQLiteDatabase openDatabase2 = SQLiteDatabase.openDatabase(createTempFile.getAbsolutePath(), bArr, (SQLiteDatabase.CursorFactory) null, 0, (SQLiteDatabaseHook) null, (DatabaseErrorHandler) null);
            SQLiteStatement compileStatement = openDatabase2.compileStatement("ATTACH DATABASE ? AS plaintext KEY ''");
            compileStatement.bindString(1, file.getAbsolutePath());
            compileStatement.execute();
            openDatabase2.rawExecSQL("SELECT sqlcipher_export('main', 'plaintext')");
            openDatabase2.rawExecSQL("DETACH DATABASE plaintext");
            ee7.a((Object) openDatabase2, UserDataStore.DATE_OF_BIRTH);
            openDatabase2.setVersion(version);
            compileStatement.close();
            openDatabase2.close();
            file.delete();
            createTempFile.renameTo(file);
            return;
        }
        throw new FileNotFoundException(file.getAbsolutePath() + " not found");
    }
}
