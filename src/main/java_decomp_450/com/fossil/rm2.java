package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rm2 extends fl2 {
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest s;
    @DexIgnore
    public /* final */ /* synthetic */ d53 t;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rm2(qm2 qm2, a12 a12, LocationRequest locationRequest, d53 d53) {
        super(a12);
        this.s = locationRequest;
        this.t = d53;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b] */
    @Override // com.fossil.r12
    public final /* synthetic */ void a(yl2 yl2) throws RemoteException {
        yl2.a(this.s, z12.a(this.t, im2.a(), d53.class.getSimpleName()), new gl2(this));
    }
}
