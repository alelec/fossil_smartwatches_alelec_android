package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr2 implements jq2 {
    @DexIgnore
    public static /* final */ Map<String, fr2> f; // = new n4();
    @DexIgnore
    public /* final */ SharedPreferences a;
    @DexIgnore
    public /* final */ SharedPreferences.OnSharedPreferenceChangeListener b; // = new er2(this);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public volatile Map<String, ?> d;
    @DexIgnore
    public /* final */ List<kq2> e; // = new ArrayList();

    @DexIgnore
    public fr2(SharedPreferences sharedPreferences) {
        this.a = sharedPreferences;
        sharedPreferences.registerOnSharedPreferenceChangeListener(this.b);
    }

    @DexIgnore
    public static fr2 a(Context context, String str) {
        fr2 fr2;
        if (!((!dq2.a() || str.startsWith("direct_boot:")) ? true : dq2.a(context))) {
            return null;
        }
        synchronized (fr2.class) {
            fr2 = f.get(str);
            if (fr2 == null) {
                fr2 = new fr2(b(context, str));
                f.put(str, fr2);
            }
        }
        return fr2;
    }

    @DexIgnore
    public static SharedPreferences b(Context context, String str) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (str.startsWith("direct_boot:")) {
                if (dq2.a()) {
                    context = context.createDeviceProtectedStorageContext();
                }
                return context.getSharedPreferences(str.substring(12), 0);
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return sharedPreferences;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    @Override // com.fossil.jq2
    public final Object zza(String str) {
        Map<String, ?> map = this.d;
        if (map == null) {
            synchronized (this.c) {
                map = this.d;
                if (map == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        Map<String, ?> all = this.a.getAll();
                        this.d = all;
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        map = all;
                    } catch (Throwable th) {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        throw th;
                    }
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    @DexIgnore
    public static synchronized void a() {
        synchronized (fr2.class) {
            for (fr2 fr2 : f.values()) {
                fr2.a.unregisterOnSharedPreferenceChangeListener(fr2.b);
            }
            f.clear();
        }
    }

    @DexIgnore
    public final /* synthetic */ void a(SharedPreferences sharedPreferences, String str) {
        synchronized (this.c) {
            this.d = null;
            tq2.c();
        }
        synchronized (this) {
            for (kq2 kq2 : this.e) {
                kq2.zza();
            }
        }
    }
}
