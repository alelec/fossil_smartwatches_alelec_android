package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.EncryptedData;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCChallengeInfoWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.buddychallenge.BCListChallengeWatchAppInfo;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj5 {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public static /* final */ a h; // = new a(null);
    @DexIgnore
    public yi7 a;
    @DexIgnore
    public kn7 b; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ ro4 d;
    @DexIgnore
    public /* final */ xo4 e;
    @DexIgnore
    public /* final */ ch5 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return uj5.g;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$getChallengeInfo$1", f = "BuddyChallengeManager.kt", l = {192}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uj5 uj5, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$challengeId = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$challengeId, this.$serial, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            Long a;
            Long a2;
            Long a3;
            Long a4;
            Object a5 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 b = this.this$0.d;
                List<String> d = w97.d(this.$challengeId);
                this.L$0 = yi7;
                this.label = 1;
                obj = b.a(d, 1, 3, false, (fb7<? super ko4<List<hn4>>>) this);
                if (obj == a5) {
                    return a5;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list = (List) ((ko4) obj).c();
            if (list != null && (!list.isEmpty())) {
                hn4 hn4 = (hn4) ea7.d(list);
                ArrayList arrayList = new ArrayList();
                List<jn4> d2 = hn4.d();
                List<jn4> d3 = d2 != null ? ea7.d((Collection) d2) : null;
                List<jn4> b2 = hn4.b();
                if (!(b2 == null || d3 == null)) {
                    pb7.a(d3.addAll(b2));
                }
                if (d3 != null) {
                    for (jn4 jn4 : d3) {
                        Integer p = jn4.p();
                        long j = 0;
                        long longValue = (p == null || (a4 = pb7.a((long) p.intValue())) == null) ? 0 : a4.longValue();
                        Integer m = jn4.m();
                        long longValue2 = longValue - ((m == null || (a3 = pb7.a((long) m.intValue())) == null) ? 0 : a3.longValue());
                        Integer b3 = jn4.b();
                        long longValue3 = (b3 == null || (a2 = pb7.a((long) b3.intValue())) == null) ? 0 : a2.longValue();
                        Integer a6 = jn4.a();
                        if (!(a6 == null || (a = pb7.a((long) a6.intValue())) == null)) {
                            j = a.longValue();
                        }
                        long j2 = longValue3 - j;
                        if (ee7.a((Object) PortfolioApp.g0.c().w(), (Object) jn4.d())) {
                            str = ig5.a(PortfolioApp.g0.c(), 2131886246);
                        } else {
                            str = fu4.a.a(jn4.c(), jn4.e(), jn4.i());
                        }
                        ee7.a((Object) str, "name");
                        Integer h = jn4.h();
                        arrayList.add(new qe0(str, h != null ? h.intValue() : -1, new pe0(longValue2, j2)));
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a7 = uj5.h.a();
                local.e(a7, "getChallengeInfo - watchPlayers: " + arrayList);
                String a8 = hn4.a();
                Object[] array = arrayList.toArray(new qe0[0]);
                if (array != null) {
                    PortfolioApp.g0.c().a(new BCChallengeInfoWatchAppInfo(new ne0(a8, "BC", (qe0[]) array)), this.$serial);
                } else {
                    throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$getListChallenge$1", f = "BuddyChallengeManager.kt", l = {133}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(uj5 uj5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$serial, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ro4 b = this.this$0.d;
                this.L$0 = yi7;
                this.label = 1;
                obj = b.c(this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<mn4> list = (List) obj;
            if (list != null) {
                ArrayList arrayList = new ArrayList(x97.a(list, 10));
                for (mn4 mn4 : list) {
                    String f = mn4.f();
                    String g = mn4.g();
                    if (g == null) {
                        g = "";
                    }
                    arrayList.add(new oe0(f, g, ""));
                }
                PortfolioApp.g0.c().a(new BCListChallengeWatchAppInfo(arrayList), this.$serial);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeCompleted$1", f = "BuddyChallengeManager.kt", l = {66, 71, 72, 74}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(uj5 uj5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$challengeId, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00c7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00df A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00e0  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x00fd A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 0
                r3 = 4
                r4 = 3
                r5 = 2
                r6 = 1
                if (r1 == 0) goto L_0x0051
                if (r1 == r6) goto L_0x0045
                if (r1 == r5) goto L_0x0037
                if (r1 == r4) goto L_0x002a
                if (r1 != r3) goto L_0x0022
                java.lang.Object r0 = r12.L$1
                java.lang.Long r0 = (java.lang.Long) r0
                java.lang.Object r0 = r12.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r13)
                goto L_0x00fe
            L_0x0022:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x002a:
                java.lang.Object r1 = r12.L$1
                java.lang.Long r1 = (java.lang.Long) r1
                java.lang.Object r4 = r12.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r13)
                goto L_0x00e1
            L_0x0037:
                java.lang.Object r1 = r12.L$1
                java.lang.Long r1 = (java.lang.Long) r1
                java.lang.Object r5 = r12.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r13)
                r13 = r5
                goto L_0x00c8
            L_0x0045:
                java.lang.Object r1 = r12.L$1
                java.lang.Long r1 = (java.lang.Long) r1
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
                goto L_0x009d
            L_0x0051:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r13 = r12.p$
                com.fossil.uj5 r1 = r12.this$0
                com.fossil.ch5 r1 = r1.f
                java.lang.Long r1 = r1.c()
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.uj5$a r8 = com.fossil.uj5.h
                java.lang.String r8 = r8.a()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "onChallengeCompleted - endTimeOfUnsetChallenge: "
                r9.append(r10)
                r9.append(r1)
                java.lang.String r9 = r9.toString()
                r7.e(r8, r9)
                r7 = 0
                if (r1 != 0) goto L_0x0085
                goto L_0x009f
            L_0x0085:
                long r9 = r1.longValue()
                int r11 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
                if (r11 != 0) goto L_0x009f
                com.fossil.uj5 r7 = r12.this$0
                r12.L$0 = r13
                r12.L$1 = r1
                r12.label = r6
                java.lang.Object r6 = r7.a(r12)
                if (r6 != r0) goto L_0x009c
                return r0
            L_0x009c:
                r6 = r13
            L_0x009d:
                r13 = r6
                goto L_0x00b9
            L_0x009f:
                com.fossil.uj5 r6 = r12.this$0
                com.fossil.ch5 r6 = r6.f
                java.lang.Boolean r9 = com.fossil.pb7.a(r2)
                r6.a(r9)
                com.fossil.uj5 r6 = r12.this$0
                com.fossil.ch5 r6 = r6.f
                java.lang.Long r7 = com.fossil.pb7.a(r7)
                r6.a(r7)
            L_0x00b9:
                r6 = 1000(0x3e8, double:4.94E-321)
                r12.L$0 = r13
                r12.L$1 = r1
                r12.label = r5
                java.lang.Object r5 = com.fossil.kj7.a(r6, r12)
                if (r5 != r0) goto L_0x00c8
                return r0
            L_0x00c8:
                com.fossil.uj5 r5 = r12.this$0
                com.fossil.ro4 r6 = r5.d
                r7 = 5
                r8 = 0
                r10 = 2
                r11 = 0
                r12.L$0 = r13
                r12.L$1 = r1
                r12.label = r4
                r9 = r12
                java.lang.Object r4 = com.fossil.ro4.a(r6, r7, r8, r9, r10, r11)
                if (r4 != r0) goto L_0x00e0
                return r0
            L_0x00e0:
                r4 = r13
            L_0x00e1:
                com.fossil.uj5 r13 = r12.this$0
                com.fossil.ro4 r5 = r13.d
                java.lang.String r13 = r12.$challengeId
                java.util.List r6 = com.fossil.v97.a(r13)
                r7 = 1
                r8 = 1
                r9 = 0
                r12.L$0 = r4
                r12.L$1 = r1
                r12.label = r3
                r10 = r12
                java.lang.Object r13 = r5.a(r6, r7, r8, r9, r10)
                if (r13 != r0) goto L_0x00fe
                return r0
            L_0x00fe:
                com.fossil.ko4 r13 = (com.fossil.ko4) r13
                java.lang.Object r13 = r13.c()
                java.util.List r13 = (java.util.List) r13
                com.fossil.uj5 r0 = r12.this$0
                java.lang.String r1 = r12.$challengeId
                r0.a(r13, r1, r2)
                com.fossil.i97 r13 = com.fossil.i97.a
                return r13
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj5.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeResponse$1", f = "BuddyChallengeManager.kt", l = {177}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(uj5 uj5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$challengeId, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            mn4 a;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                this.L$0 = this.p$;
                this.label = 1;
                obj = ro4.a(this.this$0.d, this.$challengeId, new String[]{"waiting", "running"}, 0, this, 4, null);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            if (!(ko4.c() == null || (a = this.this$0.d.a(this.$challengeId)) == null)) {
                a.a(pb7.a(((List) ko4.c()).size()));
                this.this$0.d.a(a);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onChallengeStarted$1", f = "BuddyChallengeManager.kt", l = {48, 55}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(uj5 uj5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$challengeId, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            boolean z = false;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = uj5.h.a();
                local.e(a2, "onChallengeStarted - challengeId: " + this.$challengeId);
                ro4 b = this.this$0.d;
                List d = w97.d(this.$challengeId);
                this.L$0 = yi7;
                this.label = 1;
                if (ro4.a(b, d, 3, 3, false, (fb7) this, 8, (Object) null) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                String str = (String) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean e = PortfolioApp.g0.c().e();
            String c = PortfolioApp.g0.c().c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = uj5.h.a();
            local2.e(a3, "onChallengeStarted - consider retry sync data for buddy challenge - activeStatus: " + e);
            if (e) {
                if (c.length() > 0) {
                    z = true;
                }
                if (z && !PortfolioApp.g0.c().g(c) && this.this$0.f.c().longValue() > 0) {
                    uj5 uj5 = this.this$0;
                    this.L$0 = yi7;
                    this.Z$0 = e;
                    this.L$1 = c;
                    this.label = 2;
                    if (uj5.a(this) == a) {
                        return a;
                    }
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ tn4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(tn4 tn4, fb7 fb7, uj5 uj5) {
            super(2, fb7);
            this.$this_run = tn4;
            this.this$0 = uj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.$this_run, fb7, this.this$0);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                xo4 a = this.this$0.e;
                io4 d = this.$this_run.d();
                String c = d != null ? d.c() : null;
                if (c != null) {
                    un4 a2 = a.a(c);
                    if (a2 != null && a2.c() == 1) {
                        a2.a(0);
                        long a3 = this.this$0.e.a(a2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a4 = uj5.h.a();
                        local.e(a4, "onFriendResponse - friend: " + a2 + " - rowid: " + a3);
                    }
                    return i97.a;
                }
                ee7.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$onReachGoal$1", f = "BuddyChallengeManager.kt", l = {82, 83}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(uj5 uj5, String str, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, this.$challengeId, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            ko4 ko4;
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ro4 b = this.this$0.d;
                List a2 = v97.a(this.$challengeId);
                this.L$0 = yi7;
                this.label = 1;
                obj = ro4.a(b, a2, 3, 3, false, (fb7) this, 8, (Object) null);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
            } else if (i == 2) {
                ko4 = (ko4) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                this.this$0.a((List) ko4.c(), this.$challengeId, true);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko42 = (ko4) obj;
            ro4 b2 = this.this$0.d;
            this.L$0 = yi7;
            this.L$1 = ko42;
            this.label = 2;
            if (ro4.a(b2, 5, 0, this, 2, (Object) null) == a) {
                return a;
            }
            ko4 = ko42;
            this.this$0.a((List) ko4.c(), this.$challengeId, true);
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ io4 $this_run;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(io4 io4, fb7 fb7, uj5 uj5) {
            super(2, fb7);
            this.$this_run = io4;
            this.this$0 = uj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.$this_run, fb7, this.this$0);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            nb7.a();
            if (this.label == 0) {
                t87.a(obj);
                String c = this.$this_run.c();
                String e = this.$this_run.e();
                String str2 = e != null ? e : "";
                String b = this.$this_run.b();
                String str3 = b != null ? b : "";
                String d = this.$this_run.d();
                String str4 = d != null ? d : "";
                String a = this.$this_run.a();
                if (a != null) {
                    str = a;
                } else {
                    str = "";
                }
                un4 un4 = new un4(c, str2, str3, str4, null, str, false, 0, 2);
                long a2 = this.this$0.e.a(un4);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = uj5.h.a();
                local.e(a3, "onReceivedRequest - friend: " + un4 + " - rowId: " + a2);
                return i97.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager$processEncryptedData$1", f = "BuddyChallengeManager.kt", l = {125}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ EncryptedData $encryptedData;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(uj5 uj5, EncryptedData encryptedData, fb7 fb7) {
            super(2, fb7);
            this.this$0 = uj5;
            this.$encryptedData = encryptedData;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, this.$encryptedData, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.$encryptedData != null) {
                    ro4 b = this.this$0.d;
                    EncryptedData encryptedData = this.$encryptedData;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.a(encryptedData, this);
                    if (obj == a) {
                        return a;
                    }
                }
                return i97.a;
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = uj5.h.a();
            local.e(a2, "processEncryptedData - result - " + ((ko4) obj));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager", f = "BuddyChallengeManager.kt", l = {304, 234, 240}, m = "requestSyncData")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(uj5 uj5, fb7 fb7) {
            super(fb7);
            this.this$0 = uj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, false, (fb7<? super i97>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.buddychallenge.BuddyChallengeManager", f = "BuddyChallengeManager.kt", l = {252}, m = "setSyncData")
    public static final class l extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ uj5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(uj5 uj5, fb7 fb7) {
            super(fb7);
            this.this$0 = uj5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    /*
    static {
        String simpleName = uj5.class.getSimpleName();
        ee7.a((Object) simpleName, "BuddyChallengeManager::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public uj5(PortfolioApp portfolioApp, ro4 ro4, xo4 xo4, ch5 ch5) {
        ee7.b(portfolioApp, "mPortfolioApp");
        ee7.b(ro4, "mChallengeRepository");
        ee7.b(xo4, "friendRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = ro4;
        this.e = xo4;
        this.f = ch5;
    }

    @DexIgnore
    public final void d(String str) {
        ik7 unused = xh7.b(b(), qj7.b(), null, new h(this, str, null), 2, null);
    }

    @DexIgnore
    public final void b(String str, String str2) {
        ik7 unused = xh7.b(b(), qj7.b(), null, new d(this, str, null), 2, null);
    }

    @DexIgnore
    public final void a(tn4 tn4) {
        pn4 b2;
        pn4 b3;
        String a2;
        pn4 b4;
        String a3;
        pn4 b5;
        String a4;
        ee7.b(tn4, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = g;
        local.e(str, "handleNewNotification() - notification = " + tn4);
        String e2 = tn4.e();
        switch (e2.hashCode()) {
            case -1559094286:
                if (e2.equals("Title_Notification_End_Challenge") && (b2 = tn4.b()) != null) {
                    b(b2.a(), b2.d());
                    return;
                }
                return;
            case -764698119:
                if (e2.equals("Title_Notification_Start_Challenge") && (b3 = tn4.b()) != null && (a2 = b3.a()) != null) {
                    c(a2);
                    return;
                }
                return;
            case -473527954:
                if (e2.equals("Title_Respond_Invitation_Challenge") && (b4 = tn4.b()) != null && (a3 = b4.a()) != null) {
                    b(a3);
                    return;
                }
                return;
            case 238453777:
                if (e2.equals("Title_Notification_Reached_Goal_Challenge") && (b5 = tn4.b()) != null && (a4 = b5.a()) != null) {
                    d(a4);
                    return;
                }
                return;
            case 634854495:
                if (e2.equals("Title_Accepted_Friend_Request")) {
                    b(tn4);
                    return;
                }
                return;
            case 1433363166:
                if (e2.equals("Title_Send_Friend_Request")) {
                    c(tn4);
                    return;
                }
                return;
            case 1669546642:
                e2.equals("Title_Notification_Remind_End_Challenge");
                return;
            case 1898363949:
                e2.equals("Title_Send_Invitation_Challenge");
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void b(tn4 tn4) {
        io4 d2 = tn4.d();
        if ((d2 != null ? d2.c() : null) != null) {
            ik7 unused = xh7.b(b(), qj7.b(), null, new g(tn4, null, this), 2, null);
        }
    }

    @DexIgnore
    public final void c(String str) {
        ik7 unused = xh7.b(b(), qj7.b(), null, new f(this, str, null), 2, null);
    }

    @DexIgnore
    public final void c(tn4 tn4) {
        io4 d2 = tn4.d();
        if (d2 != null) {
            if (d2.c().length() > 0) {
                ik7 unused = xh7.b(b(), qj7.b(), null, new i(d2, null, this), 2, null);
            }
        }
    }

    @DexIgnore
    public final void b(String str) {
        ik7 unused = xh7.b(b(), qj7.b(), null, new e(this, str, null), 2, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003f, code lost:
        if (com.fossil.zi7.b(r0) == false) goto L_0x0046;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.yi7 b() {
        /*
            r5 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.uj5.g
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "coroutine - "
            r2.append(r3)
            com.fossil.yi7 r3 = r5.a
            r2.append(r3)
            java.lang.String r3 = " - isActive: "
            r2.append(r3)
            com.fossil.yi7 r3 = r5.a
            r4 = 0
            if (r3 == 0) goto L_0x002a
            boolean r3 = com.fossil.zi7.b(r3)
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)
            goto L_0x002b
        L_0x002a:
            r3 = r4
        L_0x002b:
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.e(r1, r2)
            com.fossil.yi7 r0 = r5.a
            if (r0 == 0) goto L_0x0046
            if (r0 == 0) goto L_0x0042
            boolean r0 = com.fossil.zi7.b(r0)
            if (r0 != 0) goto L_0x0059
            goto L_0x0046
        L_0x0042:
            com.fossil.ee7.a()
            throw r4
        L_0x0046:
            com.fossil.tk7 r0 = com.fossil.qj7.c()
            com.fossil.yi7 r0 = com.fossil.zi7.a(r0)
            r1 = 1
            com.fossil.ki7 r1 = com.fossil.dl7.a(r4, r1, r4)
            com.fossil.yi7 r0 = com.fossil.zi7.a(r0, r1)
            r5.a = r0
        L_0x0059:
            com.fossil.yi7 r0 = r5.a
            if (r0 == 0) goto L_0x005e
            return r0
        L_0x005e:
            com.fossil.ee7.a()
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj5.b():com.fossil.yi7");
    }

    @DexIgnore
    public final void a(String str, EncryptedData encryptedData) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = g;
        local.e(str2, "processEncryptedData - serial: " + str + " - encryptedData: " + encryptedData);
        ik7 unused = xh7.b(b(), qj7.b(), null, new j(this, encryptedData, null), 2, null);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "serial");
        ik7 unused = xh7.b(b(), qj7.b(), null, new c(this, str, null), 2, null);
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ee7.b(str, "serial");
        ee7.b(str2, "challengeId");
        ik7 unused = xh7.b(b(), qj7.b(), null, new b(this, str2, str, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c4 A[Catch:{ all -> 0x0152 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00d7 A[Catch:{ all -> 0x0152 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(java.lang.String r13, boolean r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r12 = this;
            boolean r0 = r15 instanceof com.fossil.uj5.k
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.fossil.uj5$k r0 = (com.fossil.uj5.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.uj5$k r0 = new com.fossil.uj5$k
            r0.<init>(r12, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 3
            r4 = 2
            r5 = 0
            r6 = 1
            if (r2 == 0) goto L_0x0079
            if (r2 == r6) goto L_0x0065
            if (r2 == r4) goto L_0x004b
            if (r2 != r3) goto L_0x0043
            java.lang.Object r13 = r0.L$4
            java.lang.Long r13 = (java.lang.Long) r13
            java.lang.Object r13 = r0.L$3
            java.lang.Boolean r13 = (java.lang.Boolean) r13
            java.lang.Object r13 = r0.L$2
            com.fossil.kn7 r13 = (com.fossil.kn7) r13
            boolean r14 = r0.Z$0
            java.lang.Object r14 = r0.L$1
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r14 = r0.L$0
            com.fossil.uj5 r14 = (com.fossil.uj5) r14
            goto L_0x005d
        L_0x0043:
            java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
            java.lang.String r14 = "call to 'resume' before 'invoke' with coroutine"
            r13.<init>(r14)
            throw r13
        L_0x004b:
            java.lang.Object r13 = r0.L$3
            java.lang.Boolean r13 = (java.lang.Boolean) r13
            java.lang.Object r13 = r0.L$2
            com.fossil.kn7 r13 = (com.fossil.kn7) r13
            boolean r14 = r0.Z$0
            java.lang.Object r14 = r0.L$1
            java.lang.String r14 = (java.lang.String) r14
            java.lang.Object r14 = r0.L$0
            com.fossil.uj5 r14 = (com.fossil.uj5) r14
        L_0x005d:
            com.fossil.t87.a(r15)     // Catch:{ all -> 0x0062 }
            goto L_0x014a
        L_0x0062:
            r14 = move-exception
            goto L_0x0154
        L_0x0065:
            java.lang.Object r13 = r0.L$2
            com.fossil.kn7 r13 = (com.fossil.kn7) r13
            boolean r14 = r0.Z$0
            java.lang.Object r2 = r0.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r7 = r0.L$0
            com.fossil.uj5 r7 = (com.fossil.uj5) r7
            com.fossil.t87.a(r15)
            r15 = r13
            r13 = r2
            goto L_0x0090
        L_0x0079:
            com.fossil.t87.a(r15)
            com.fossil.kn7 r15 = r12.b
            r0.L$0 = r12
            r0.L$1 = r13
            r0.Z$0 = r14
            r0.L$2 = r15
            r0.label = r6
            java.lang.Object r2 = r15.a(r5, r0)
            if (r2 != r1) goto L_0x008f
            return r1
        L_0x008f:
            r7 = r12
        L_0x0090:
            com.fossil.ch5 r2 = r7.f     // Catch:{ all -> 0x0152 }
            java.lang.Boolean r2 = r2.P()     // Catch:{ all -> 0x0152 }
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0152 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ all -> 0x0152 }
            java.lang.String r9 = com.fossil.uj5.g     // Catch:{ all -> 0x0152 }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0152 }
            r10.<init>()     // Catch:{ all -> 0x0152 }
            java.lang.String r11 = "requestSyncData - serial: "
            r10.append(r11)     // Catch:{ all -> 0x0152 }
            r10.append(r13)     // Catch:{ all -> 0x0152 }
            java.lang.String r11 = " - force: "
            r10.append(r11)     // Catch:{ all -> 0x0152 }
            r10.append(r14)     // Catch:{ all -> 0x0152 }
            java.lang.String r11 = " - needSetSyncData: "
            r10.append(r11)     // Catch:{ all -> 0x0152 }
            r10.append(r2)     // Catch:{ all -> 0x0152 }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x0152 }
            r8.e(r9, r10)     // Catch:{ all -> 0x0152 }
            if (r14 == 0) goto L_0x00d7
            r0.L$0 = r7     // Catch:{ all -> 0x0152 }
            r0.L$1 = r13     // Catch:{ all -> 0x0152 }
            r0.Z$0 = r14     // Catch:{ all -> 0x0152 }
            r0.L$2 = r15     // Catch:{ all -> 0x0152 }
            r0.L$3 = r2     // Catch:{ all -> 0x0152 }
            r0.label = r4     // Catch:{ all -> 0x0152 }
            java.lang.Object r13 = r7.a(r0)     // Catch:{ all -> 0x0152 }
            if (r13 != r1) goto L_0x0149
            return r1
        L_0x00d7:
            java.lang.Boolean r4 = com.fossil.pb7.a(r6)     // Catch:{ all -> 0x0152 }
            boolean r4 = com.fossil.ee7.a(r2, r4)     // Catch:{ all -> 0x0152 }
            if (r4 == 0) goto L_0x0149
            com.fossil.ch5 r4 = r7.f     // Catch:{ all -> 0x0152 }
            java.lang.Long r4 = r4.c()     // Catch:{ all -> 0x0152 }
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0152 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ all -> 0x0152 }
            java.lang.String r8 = com.fossil.uj5.g     // Catch:{ all -> 0x0152 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0152 }
            r9.<init>()     // Catch:{ all -> 0x0152 }
            java.lang.String r10 = "requestSyncData - endTimeOfUnsetChallenge: "
            r9.append(r10)     // Catch:{ all -> 0x0152 }
            r9.append(r4)     // Catch:{ all -> 0x0152 }
            java.lang.String r10 = " - exact: "
            r9.append(r10)     // Catch:{ all -> 0x0152 }
            com.fossil.vt4 r10 = com.fossil.vt4.a     // Catch:{ all -> 0x0152 }
            long r10 = r10.b()     // Catch:{ all -> 0x0152 }
            r9.append(r10)     // Catch:{ all -> 0x0152 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0152 }
            r6.e(r8, r9)     // Catch:{ all -> 0x0152 }
            long r8 = r4.longValue()     // Catch:{ all -> 0x0152 }
            com.fossil.vt4 r6 = com.fossil.vt4.a     // Catch:{ all -> 0x0152 }
            long r10 = r6.b()     // Catch:{ all -> 0x0152 }
            int r6 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x0134
            r0.L$0 = r7     // Catch:{ all -> 0x0152 }
            r0.L$1 = r13     // Catch:{ all -> 0x0152 }
            r0.Z$0 = r14     // Catch:{ all -> 0x0152 }
            r0.L$2 = r15     // Catch:{ all -> 0x0152 }
            r0.L$3 = r2     // Catch:{ all -> 0x0152 }
            r0.L$4 = r4     // Catch:{ all -> 0x0152 }
            r0.label = r3     // Catch:{ all -> 0x0152 }
            java.lang.Object r13 = r7.a(r0)     // Catch:{ all -> 0x0152 }
            if (r13 != r1) goto L_0x0149
            return r1
        L_0x0134:
            com.fossil.ch5 r13 = r7.f     // Catch:{ all -> 0x0152 }
            r14 = 0
            java.lang.Boolean r14 = com.fossil.pb7.a(r14)     // Catch:{ all -> 0x0152 }
            r13.a(r14)     // Catch:{ all -> 0x0152 }
            com.fossil.ch5 r13 = r7.f     // Catch:{ all -> 0x0152 }
            r0 = 0
            java.lang.Long r14 = com.fossil.pb7.a(r0)     // Catch:{ all -> 0x0152 }
            r13.a(r14)     // Catch:{ all -> 0x0152 }
        L_0x0149:
            r13 = r15
        L_0x014a:
            com.fossil.i97 r14 = com.fossil.i97.a
            r13.a(r5)
            com.fossil.i97 r13 = com.fossil.i97.a
            return r13
        L_0x0152:
            r14 = move-exception
            r13 = r15
        L_0x0154:
            r13.a(r5)
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj5.a(java.lang.String, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public static /* synthetic */ Object a(uj5 uj5, String str, boolean z, fb7 fb7, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return uj5.a(str, z, fb7);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.uj5.l
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.uj5$l r0 = (com.fossil.uj5.l) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.uj5$l r0 = new com.fossil.uj5$l
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.uj5 r0 = (com.fossil.uj5) r0
            com.fossil.t87.a(r6)
            goto L_0x0053
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r2 = com.fossil.uj5.g
            java.lang.String r4 = "setSyncData"
            r6.e(r2, r4)
            com.fossil.ro4 r6 = r5.d
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.b(r0)
            if (r6 != r1) goto L_0x0052
            return r1
        L_0x0052:
            r0 = r5
        L_0x0053:
            com.fossil.ko4 r6 = (com.fossil.ko4) r6
            java.lang.Object r6 = r6.c()
            com.fossil.no4 r6 = (com.fossil.no4) r6
            if (r6 == 0) goto L_0x0066
            com.portfolio.platform.PortfolioApp r0 = r0.c
            java.lang.String r6 = r6.a()
            r0.n(r6)
        L_0x0066:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:31:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x009e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.util.List<com.fossil.hn4> r11, java.lang.String r12, boolean r13) {
        /*
            r10 = this;
            r0 = 0
            r1 = 1
            if (r11 == 0) goto L_0x000d
            boolean r2 = r11.isEmpty()
            if (r2 == 0) goto L_0x000b
            goto L_0x000d
        L_0x000b:
            r2 = 0
            goto L_0x000e
        L_0x000d:
            r2 = 1
        L_0x000e:
            if (r2 != 0) goto L_0x00af
            java.lang.Object r11 = com.fossil.ea7.d(r11)
            com.fossil.hn4 r11 = (com.fossil.hn4) r11
            java.util.List r2 = r11.d()
            r3 = 0
            if (r2 == 0) goto L_0x0022
            java.util.List r2 = com.fossil.ea7.d(r2)
            goto L_0x0023
        L_0x0022:
            r2 = r3
        L_0x0023:
            java.util.List r11 = r11.b()
            if (r2 == 0) goto L_0x0052
            java.util.Iterator r2 = r2.iterator()
        L_0x002d:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x004b
            java.lang.Object r4 = r2.next()
            r5 = r4
            com.fossil.jn4 r5 = (com.fossil.jn4) r5
            java.lang.String r5 = r5.d()
            com.portfolio.platform.PortfolioApp r6 = r10.c
            java.lang.String r6 = r6.w()
            boolean r5 = com.fossil.ee7.a(r5, r6)
            if (r5 == 0) goto L_0x002d
            goto L_0x004c
        L_0x004b:
            r4 = r3
        L_0x004c:
            com.fossil.jn4 r4 = (com.fossil.jn4) r4
            if (r4 == 0) goto L_0x0052
            r3 = r4
            goto L_0x0078
        L_0x0052:
            if (r11 == 0) goto L_0x0078
            java.util.Iterator r11 = r11.iterator()
        L_0x0058:
            boolean r2 = r11.hasNext()
            if (r2 == 0) goto L_0x0076
            java.lang.Object r2 = r11.next()
            r4 = r2
            com.fossil.jn4 r4 = (com.fossil.jn4) r4
            java.lang.String r4 = r4.d()
            com.portfolio.platform.PortfolioApp r5 = r10.c
            java.lang.String r5 = r5.w()
            boolean r4 = com.fossil.ee7.a(r4, r5)
            if (r4 == 0) goto L_0x0058
            r3 = r2
        L_0x0076:
            com.fossil.jn4 r3 = (com.fossil.jn4) r3
        L_0x0078:
            if (r3 == 0) goto L_0x009e
            java.lang.Integer r11 = r3.h()
            if (r11 == 0) goto L_0x0085
            int r11 = r11.intValue()
            goto L_0x0086
        L_0x0085:
            r11 = 0
        L_0x0086:
            java.lang.Integer r2 = r3.p()
            if (r2 == 0) goto L_0x0091
            int r2 = r2.intValue()
            goto L_0x0092
        L_0x0091:
            r2 = 0
        L_0x0092:
            if (r13 == 0) goto L_0x0098
        L_0x0094:
            r6 = r11
            r7 = r2
            r8 = 1
            goto L_0x00a1
        L_0x0098:
            if (r11 != r1) goto L_0x009b
            goto L_0x0094
        L_0x009b:
            r6 = r11
            r7 = r2
            goto L_0x00a0
        L_0x009e:
            r6 = 1
            r7 = 1
        L_0x00a0:
            r8 = 0
        L_0x00a1:
            com.fossil.um4 r3 = com.fossil.um4.a
            com.portfolio.platform.PortfolioApp r11 = r10.c
            java.lang.String r5 = r11.w()
            com.portfolio.platform.PortfolioApp r9 = r10.c
            r4 = r12
            r3.a(r4, r5, r6, r7, r8, r9)
        L_0x00af:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.uj5.a(java.util.List, java.lang.String, boolean):void");
    }

    @DexIgnore
    public final void a() {
        yi7 yi7 = this.a;
        if (yi7 != null) {
            zi7.a(yi7, null, 1, null);
        }
    }
}
