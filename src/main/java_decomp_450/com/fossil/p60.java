package com.fossil;

import com.fossil.o60;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum p60 implements ke0 {
    BLUETOOTH_OFF(0),
    INVALID_PARAMETERS(1),
    DEVICE_BUSY(2),
    EXECUTION_TIMEOUT(3),
    REQUEST_FAILED(4),
    REQUEST_UNSUPPORTED(5),
    RESPONSE_TIMEOUT(6),
    RESPONSE_FAILED(7),
    CONNECTION_DROPPED(8),
    INTERRUPTED(9),
    SERVICE_CHANGED(10),
    AUTHENTICATION_FAILED(11),
    SECRET_KEY_IS_REQUIRED(12),
    DATA_SIZE_OVER_LIMIT(13),
    UNSUPPORTED_FORMAT(14),
    TARGET_FIRMWARE_NOT_MATCHED(15),
    REQUEST_TIMEOUT(16),
    UNKNOWN_ERROR(255);
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ String a; // = yz0.a(this);
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final p60 a(eu0 eu0, HashMap<o60.b, Object> hashMap) {
            Boolean bool = (Boolean) hashMap.get(o60.b.HAS_SERVICE_CHANGED);
            boolean booleanValue = bool != null ? bool.booleanValue() : false;
            switch (is1.a[eu0.b.ordinal()]) {
                case 1:
                    return p60.INTERRUPTED;
                case 2:
                    if (booleanValue) {
                        return p60.SERVICE_CHANGED;
                    }
                    return p60.CONNECTION_DROPPED;
                case 3:
                    return p60.REQUEST_UNSUPPORTED;
                case 4:
                    sz0 sz0 = eu0.c;
                    ay0 ay0 = sz0.c;
                    if (ay0 == ay0.p) {
                        return p60.REQUEST_TIMEOUT;
                    }
                    if (ay0 == ay0.q) {
                        return p60.RESPONSE_TIMEOUT;
                    }
                    if (ay0 == ay0.d || ay0 == ay0.r) {
                        return p60.RESPONSE_FAILED;
                    }
                    if (sz0.e == dr0.d) {
                        return p60.DATA_SIZE_OVER_LIMIT;
                    }
                    return p60.REQUEST_FAILED;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                    return p60.RESPONSE_FAILED;
                case 16:
                    return p60.EXECUTION_TIMEOUT;
                case 17:
                case 18:
                    return p60.REQUEST_UNSUPPORTED;
                case 19:
                    return p60.BLUETOOTH_OFF;
                case 20:
                case 21:
                    return p60.AUTHENTICATION_FAILED;
                case 22:
                    return p60.SECRET_KEY_IS_REQUIRED;
                case 23:
                    return p60.INVALID_PARAMETERS;
                case 24:
                    return p60.REQUEST_FAILED;
                case 25:
                    return p60.DEVICE_BUSY;
                case 26:
                    return p60.UNSUPPORTED_FORMAT;
                case 27:
                    return p60.TARGET_FIRMWARE_NOT_MATCHED;
                case 28:
                case 29:
                case 30:
                case 31:
                    return p60.UNKNOWN_ERROR;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore
    public p60(int i) {
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.ke0
    public int getCode() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.ke0
    public String getLogName() {
        return this.a;
    }
}
