package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vy1 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<vy1> CREATOR; // = new yy1();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public Bundle c;

    @DexIgnore
    public vy1(int i, int i2, Bundle bundle) {
        this.a = i;
        this.b = i2;
        this.c = bundle;
    }

    @DexIgnore
    public int e() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, e());
        k72.a(parcel, 3, this.c, false);
        k72.a(parcel, a2);
    }
}
