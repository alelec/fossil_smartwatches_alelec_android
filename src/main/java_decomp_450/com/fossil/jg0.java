package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ ng0 a;
    @DexIgnore
    public /* final */ mg0[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<jg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public jg0 createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                ng0 valueOf = ng0.valueOf(readString);
                Object[] createTypedArray = parcel.createTypedArray(mg0.CREATOR);
                if (createTypedArray != null) {
                    ee7.a((Object) createTypedArray, "parcel.createTypedArray(\u2026AppDeclaration.CREATOR)!!");
                    return new jg0(valueOf, (mg0[]) createTypedArray);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public jg0[] newArray(int i) {
            return new jg0[i];
        }
    }

    @DexIgnore
    public jg0(ng0 ng0, mg0[] mg0Arr) {
        this.a = ng0;
        this.b = mg0Arr;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (mg0 mg0 : this.b) {
            jSONArray.put(mg0.a());
        }
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.A3, yz0.a(this.a)), r51.B3, jSONArray), r51.C3, Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(jg0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            jg0 jg0 = (jg0) obj;
            ng0 ng0 = this.a;
            ng0 ng02 = jg0.a;
            return ng0 == ng02 && ng0 == ng02 && Arrays.equals(this.b, jg0.b);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.MicroAppMapping");
    }

    @DexIgnore
    public final ng0 getMicroAppButton() {
        return this.a;
    }

    @DexIgnore
    public final mg0[] getMicroAppDeclarations() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }
}
