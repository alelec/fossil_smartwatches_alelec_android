package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eg0 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ r60 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<eg0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public eg0 createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                ee7.a((Object) createByteArray, "parcel.createByteArray()!!");
                return new eg0(createByteArray);
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public eg0[] newArray(int i) {
            return new eg0[i];
        }
    }

    @DexIgnore
    public eg0(byte[] bArr) throws IllegalArgumentException {
        this.a = bArr;
        this.b = new r60(bArr[2], bArr[3]);
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(new JSONObject(), r51.h2, this.b.toString()), r51.d3, Long.valueOf(ik1.a.a(this.a, ng1.CRC32C))), r51.H, Integer.valueOf(this.a.length));
    }

    @DexIgnore
    public final r60 b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
