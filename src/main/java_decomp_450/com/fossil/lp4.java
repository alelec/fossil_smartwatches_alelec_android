package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lp4 implements Factory<kp4> {
    @DexIgnore
    public /* final */ Provider<ch5> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;

    @DexIgnore
    public lp4(Provider<ch5> provider, Provider<ro4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static lp4 a(Provider<ch5> provider, Provider<ro4> provider2) {
        return new lp4(provider, provider2);
    }

    @DexIgnore
    public static kp4 a(ch5 ch5, ro4 ro4) {
        return new kp4(ch5, ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public kp4 get() {
        return a(this.a.get(), this.b.get());
    }
}
