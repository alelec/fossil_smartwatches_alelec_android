package com.fossil;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr7 extends wq7 {
    @DexIgnore
    public /* final */ Logger l; // = Logger.getLogger("okio.Okio");
    @DexIgnore
    public /* final */ Socket m;

    @DexIgnore
    public rr7(Socket socket) {
        ee7.b(socket, "socket");
        this.m = socket;
    }

    @DexIgnore
    @Override // com.fossil.wq7
    public IOException b(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    @DexIgnore
    @Override // com.fossil.wq7
    public void i() {
        try {
            this.m.close();
        } catch (Exception e) {
            Logger logger = this.l;
            Level level = Level.WARNING;
            logger.log(level, "Failed to close timed out socket " + this.m, (Throwable) e);
        } catch (AssertionError e2) {
            if (ir7.a(e2)) {
                Logger logger2 = this.l;
                Level level2 = Level.WARNING;
                logger2.log(level2, "Failed to close timed out socket " + this.m, (Throwable) e2);
                return;
            }
            throw e2;
        }
    }
}
