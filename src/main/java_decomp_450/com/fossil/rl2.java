package com.fossil;

import android.content.Context;
import android.location.Location;
import android.os.RemoteException;
import com.fossil.y12;
import com.google.android.gms.location.LocationRequest;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rl2 {
    @DexIgnore
    public /* final */ fm2<nl2> a;
    @DexIgnore
    public /* final */ Context b;
    @DexIgnore
    public boolean c; // = false;
    @DexIgnore
    public /* final */ Map<y12.a<d53>, wl2> d; // = new HashMap();
    @DexIgnore
    public /* final */ Map<y12.a<Object>, vl2> e; // = new HashMap();
    @DexIgnore
    public /* final */ Map<y12.a<c53>, sl2> f; // = new HashMap();

    @DexIgnore
    public rl2(Context context, fm2<nl2> fm2) {
        this.b = context;
        this.a = fm2;
    }

    @DexIgnore
    public final Location a() throws RemoteException {
        this.a.a();
        return this.a.getService().zza(this.b.getPackageName());
    }

    @DexIgnore
    public final wl2 a(y12<d53> y12) {
        wl2 wl2;
        synchronized (this.d) {
            wl2 = this.d.get(y12.b());
            if (wl2 == null) {
                wl2 = new wl2(y12);
            }
            this.d.put(y12.b(), wl2);
        }
        return wl2;
    }

    @DexIgnore
    public final void a(bm2 bm2, y12<c53> y12, kl2 kl2) throws RemoteException {
        this.a.a();
        this.a.getService().a(new dm2(1, bm2, null, null, b(y12).asBinder(), kl2 != null ? kl2.asBinder() : null));
    }

    @DexIgnore
    public final void a(y12.a<d53> aVar, kl2 kl2) throws RemoteException {
        this.a.a();
        a72.a(aVar, "Invalid null listener key");
        synchronized (this.d) {
            wl2 remove = this.d.remove(aVar);
            if (remove != null) {
                remove.E();
                this.a.getService().a(dm2.a(remove, kl2));
            }
        }
    }

    @DexIgnore
    public final void a(LocationRequest locationRequest, y12<d53> y12, kl2 kl2) throws RemoteException {
        this.a.a();
        this.a.getService().a(new dm2(1, bm2.a(locationRequest), a(y12).asBinder(), null, null, kl2 != null ? kl2.asBinder() : null));
    }

    @DexIgnore
    public final void a(boolean z) throws RemoteException {
        this.a.a();
        this.a.getService().e(z);
        this.c = z;
    }

    @DexIgnore
    public final sl2 b(y12<c53> y12) {
        sl2 sl2;
        synchronized (this.f) {
            sl2 = this.f.get(y12.b());
            if (sl2 == null) {
                sl2 = new sl2(y12);
            }
            this.f.put(y12.b(), sl2);
        }
        return sl2;
    }

    @DexIgnore
    public final void b() throws RemoteException {
        synchronized (this.d) {
            for (wl2 wl2 : this.d.values()) {
                if (wl2 != null) {
                    this.a.getService().a(dm2.a(wl2, (kl2) null));
                }
            }
            this.d.clear();
        }
        synchronized (this.f) {
            for (sl2 sl2 : this.f.values()) {
                if (sl2 != null) {
                    this.a.getService().a(dm2.a(sl2, (kl2) null));
                }
            }
            this.f.clear();
        }
        synchronized (this.e) {
            for (vl2 vl2 : this.e.values()) {
                if (vl2 != null) {
                    this.a.getService().a(new om2(2, null, vl2.asBinder(), null));
                }
            }
            this.e.clear();
        }
    }

    @DexIgnore
    public final void b(y12.a<c53> aVar, kl2 kl2) throws RemoteException {
        this.a.a();
        a72.a(aVar, "Invalid null listener key");
        synchronized (this.f) {
            sl2 remove = this.f.remove(aVar);
            if (remove != null) {
                remove.E();
                this.a.getService().a(dm2.a(remove, kl2));
            }
        }
    }

    @DexIgnore
    public final void c() throws RemoteException {
        if (this.c) {
            a(false);
        }
    }
}
