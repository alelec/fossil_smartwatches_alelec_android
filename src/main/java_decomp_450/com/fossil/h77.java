package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h77 {
    @DexIgnore
    public static <T> boolean a(T[] tArr) {
        return tArr == null || tArr.length == 0;
    }

    @DexIgnore
    public static <T> boolean b(T[] tArr) {
        return !a((Object[]) tArr);
    }

    @DexIgnore
    public static <T> List<T> c(List<T> list) {
        return Collections.unmodifiableList(b((List) list));
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection) {
        return collection == null || collection.isEmpty();
    }

    @DexIgnore
    public static <T> boolean b(Collection<T> collection) {
        return !a(collection);
    }

    @DexIgnore
    @SafeVarargs
    public static <T> List<T> a(List<T>... listArr) {
        if (listArr == null || listArr.length == 0) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList();
        Iterator it = new CopyOnWriteArrayList(listArr).iterator();
        while (it.hasNext()) {
            List list = (List) it.next();
            if (b((Collection) list)) {
                Iterator it2 = new CopyOnWriteArrayList(list).iterator();
                while (it2.hasNext()) {
                    arrayList.add(it2.next());
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static <T> List<T> b(List<T> list) {
        return a(list) ? new ArrayList() : list;
    }

    @DexIgnore
    public static <T> List<T> a(List<T> list) {
        if (list == null) {
            return new ArrayList();
        }
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList(list);
        ArrayList arrayList = new ArrayList(copyOnWriteArrayList.size());
        Iterator it = copyOnWriteArrayList.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }
        return arrayList;
    }

    @DexIgnore
    public static <K, V> Map<K, V> a(Map<K, V> map) {
        if (map == null) {
            return new HashMap();
        }
        Map<? extends K, ? extends V> synchronizedMap = Collections.synchronizedMap(map);
        HashMap hashMap = new HashMap();
        hashMap.putAll(synchronizedMap);
        return hashMap;
    }
}
