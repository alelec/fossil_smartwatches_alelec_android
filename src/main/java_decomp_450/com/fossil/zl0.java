package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zl0 extends pe1 {
    @DexIgnore
    public static /* final */ ck0 CREATOR; // = new ck0(null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public /* synthetic */ zl0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readInt() != 0;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.pe1
    public JSONObject a() {
        return yz0.a(yz0.a(super.a(), r51.N3, Byte.valueOf(this.b)), r51.R3, Boolean.valueOf(this.c));
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN);
        ee7.a((Object) order, "ByteBuffer.allocate(2)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b);
        order.put(this.c ? (byte) 1 : 0);
        byte[] array = order.array();
        ee7.a((Object) array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(zl0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            zl0 zl0 = (zl0) obj;
            return this.c == zl0.c && this.b == zl0.b;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.SwitchActivityInstr");
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode() + (this.b * 31);
    }

    @DexIgnore
    @Override // com.fossil.pe1
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }
}
