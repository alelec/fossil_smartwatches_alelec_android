package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface vd<T> {
    @DexIgnore
    Object a(LiveData<T> liveData, fb7<? super rj7> fb7);

    @DexIgnore
    Object a(T t, fb7<? super i97> fb7);
}
