package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.blesdk.database.SdkDatabase;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zk1 extends v61 {
    @DexIgnore
    public /* final */ a81 E;
    @DexIgnore
    public /* final */ ArrayList<byte[]> F;
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<bi1> G;
    @DexIgnore
    public /* final */ ArrayList<bi1> H;
    @DexIgnore
    public long I;
    @DexIgnore
    public long J;
    @DexIgnore
    public float K;
    @DexIgnore
    public int L;
    @DexIgnore
    public long M;
    @DexIgnore
    public int N;
    @DexIgnore
    public /* final */ boolean O;
    @DexIgnore
    public /* final */ boolean P;
    @DexIgnore
    public /* final */ boolean Q;
    @DexIgnore
    public /* final */ int R;
    @DexIgnore
    public /* final */ boolean S;
    @DexIgnore
    public /* final */ float T;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ zk1(com.fossil.ri1 r8, com.fossil.en0 r9, com.fossil.wm0 r10, short r11, java.util.HashMap r12, float r13, java.lang.String r14, int r15) {
        /*
            r7 = this;
            r0 = r15 & 4
            if (r0 == 0) goto L_0x0006
            com.fossil.wm0 r10 = com.fossil.wm0.l
        L_0x0006:
            r3 = r10
            r10 = r15 & 16
            if (r10 == 0) goto L_0x0010
            java.util.HashMap r12 = new java.util.HashMap
            r12.<init>()
        L_0x0010:
            r10 = r15 & 32
            if (r10 == 0) goto L_0x0017
            r13 = 981668463(0x3a83126f, float:0.001)
        L_0x0017:
            r10 = r15 & 64
            if (r10 == 0) goto L_0x0021
            java.lang.String r10 = "UUID.randomUUID().toString()"
            java.lang.String r14 = com.fossil.yh0.a(r10)
        L_0x0021:
            r5 = r14
            r6 = 1
            r0 = r7
            r1 = r8
            r2 = r9
            r4 = r11
            r0.<init>(r1, r2, r3, r4, r5, r6)
            r7.T = r13
            com.fossil.a81 r8 = new com.fossil.a81
            r8.<init>(r11)
            r7.E = r8
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            r7.F = r8
            java.util.concurrent.CopyOnWriteArrayList r8 = new java.util.concurrent.CopyOnWriteArrayList
            r8.<init>()
            r7.G = r8
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            r7.H = r8
            r8 = -1
            r7.L = r8
            com.fossil.xf0 r8 = com.fossil.xf0.SKIP_LIST
            java.lang.Object r8 = r12.get(r8)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            r9 = 0
            if (r8 == 0) goto L_0x005b
            boolean r8 = r8.booleanValue()
            goto L_0x005c
        L_0x005b:
            r8 = 0
        L_0x005c:
            r7.O = r8
            com.fossil.xf0 r8 = com.fossil.xf0.SKIP_ERASE
            java.lang.Object r8 = r12.get(r8)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            if (r8 == 0) goto L_0x006d
            boolean r8 = r8.booleanValue()
            goto L_0x006e
        L_0x006d:
            r8 = 0
        L_0x006e:
            r7.P = r8
            com.fossil.xf0 r8 = com.fossil.xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS
            java.lang.Object r8 = r12.get(r8)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            if (r8 == 0) goto L_0x007f
            boolean r8 = r8.booleanValue()
            goto L_0x0080
        L_0x007f:
            r8 = 0
        L_0x0080:
            r7.Q = r8
            com.fossil.xf0 r8 = com.fossil.xf0.NUMBER_OF_FILE_REQUIRED
            java.lang.Object r8 = r12.get(r8)
            java.lang.Integer r8 = (java.lang.Integer) r8
            if (r8 == 0) goto L_0x0091
            int r8 = r8.intValue()
            goto L_0x0092
        L_0x0091:
            r8 = 0
        L_0x0092:
            r7.R = r8
            com.fossil.xf0 r8 = com.fossil.xf0.ERASE_CACHE_FILE_BEFORE_GET
            java.lang.Object r8 = r12.get(r8)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            if (r8 == 0) goto L_0x00a2
            boolean r9 = r8.booleanValue()
        L_0x00a2:
            r7.S = r9
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zk1.<init>(com.fossil.ri1, com.fossil.en0, com.fossil.wm0, short, java.util.HashMap, float, java.lang.String, int):void");
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        if (this.S) {
            m();
        }
        if (!this.Q) {
            c(new bj1(this));
        }
        if (this.O) {
            this.G.add(new bi1(((zk0) this).w.u, ((v61) this).D, 4294967295L, 0));
            p();
            return;
        }
        zk0.a(this, qa1.u, null, 2, null);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61
    public JSONObject i() {
        JSONObject put = super.i().put(yz0.a(xf0.SKIP_LIST), this.O).put(yz0.a(xf0.SKIP_ERASE), this.P).put(yz0.a(xf0.SKIP_ERASE_CACHE_AFTER_SUCCESS), this.Q).put(yz0.a(xf0.NUMBER_OF_FILE_REQUIRED), this.R).put(yz0.a(xf0.ERASE_CACHE_FILE_BEFORE_GET), this.S);
        ee7.a((Object) put, "super.optionDescription(\u2026 eraseCacheFileBeforeGet)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject put = super.k().put(yz0.a(xf0.SKIP_ERASE), this.P);
        ee7.a((Object) put, "super.resultDescription(\u2026lowerCaseName, skipErase)");
        r51 r51 = r51.N2;
        Object[] array = this.H.toArray(new bi1[0]);
        if (array != null) {
            return yz0.a(put, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void m() {
        Integer num;
        rc1 a;
        String str = ((zk0) this).w.u;
        byte b = this.E.a;
        SdkDatabase a2 = SdkDatabase.e.a();
        if (a2 == null || (a = a2.a()) == null) {
            num = null;
        } else {
            a.a.assertNotSuspendingTransaction();
            aj acquire = a.d.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            a.a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a.a.setTransactionSuccessful();
                a.a.endTransaction();
                a.d.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a.a.endTransaction();
                a.d.release(acquire);
                throw th;
            }
        }
        yz0.a(yz0.a(new a81(b, (byte) 255).a(), r51.L2, JSONObject.NULL), r51.w4, num != null ? num : -1);
        s11 s11 = s11.DELETE;
        t11 t11 = t11.a;
        if (num != null) {
            num.intValue();
        }
    }

    @DexIgnore
    public final bi1 n() {
        return (bi1) ea7.a((List) this.G, this.L);
    }

    @DexIgnore
    public final void o() {
        bi1 n = n();
        if (n == null) {
            ee7.a();
            throw null;
        } else if (n.f > 0) {
            a(qa1.v, new eh1(this));
        } else if (this.P) {
            p();
        } else {
            zk0.a(this, qa1.w, null, 2, null);
        }
    }

    @DexIgnore
    public final void p() {
        int i = this.L + 1;
        this.L = i;
        if (i < this.G.size()) {
            bi1 n = n();
            if (n != null) {
                a81 a81 = new a81(n.c());
                bi1 a = a(a81.a, a81.b);
                ri1 ri1 = ((zk0) this).w;
                le0 le0 = le0.DEBUG;
                String str = ri1.u;
                if (a != null) {
                    a.toString();
                }
                bi1 n2 = n();
                if (n2 == null) {
                    ee7.a();
                    throw null;
                } else if (n2.f <= 0) {
                    p();
                } else if (a != null) {
                    bi1 n3 = n();
                    if (n3 != null) {
                        long j = n3.f;
                        bi1 n4 = n();
                        if (n4 != null) {
                            d(bi1.a(a, null, (byte) 0, (byte) 0, null, j, n4.g, 0, false, 207));
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    this.M = 0;
                    o();
                }
            } else {
                ee7.a();
                throw null;
            }
        } else if (this.F.size() < this.R) {
            a(eu0.a(((zk0) this).v, null, is0.NOT_ENOUGH_FILE_TO_PROCESS, null, 5));
        } else {
            this.H.clear();
            this.H.addAll(l31.a.a(((zk0) this).w.u, this.E.a));
            a(this.H);
        }
    }

    @DexIgnore
    public final void q() {
        T t;
        for (T t2 : l31.a.b(((zk0) this).w.u, this.E.a)) {
            Iterator<T> it = this.G.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                boolean z = false;
                if (t.c() == ByteBuffer.allocate(2).put(((bi1) t2).c).put(((bi1) t2).d).getShort(0)) {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (t == null && ik1.a.a(((bi1) t2).e, ng1.CRC32) != ((bi1) t2).g) {
                l31.a.a(((zk0) this).w.u, ((bi1) t2).c, ((bi1) t2).d);
            }
        }
    }

    @DexIgnore
    public final void b(bi1 bi1) {
        zk0.a(this, new vo1(((zk0) this).w, ((zk0) this).x, bi1, ((zk0) this).z), new od1(this), new if1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public void c(bi1 bi1) {
        le0 le0 = le0.DEBUG;
        bi1.a(2);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        Object[] array = this.H.toArray(new bi1[0]);
        if (array != null) {
            return array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void a(vo1 vo1) {
        long j = vo1.D;
        this.M = j;
        bi1 bi1 = vo1.H;
        a(bi1.a(bi1, null, (byte) 0, (byte) 0, s97.a(bi1.e, 0, (int) j), 0, 0, 0, false, 119));
        long j2 = this.I;
        float f = j2 > 0 ? (((float) (this.J + this.M)) * 1.0f) / ((float) j2) : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (Math.abs(f - this.K) > this.T || f == 1.0f) {
            this.K = f;
            a(f);
        }
        o();
    }

    @DexIgnore
    public final void d(bi1 bi1) {
        if (bi1.g != ik1.a.a(bi1.e, ng1.CRC32) || ((int) bi1.f) != bi1.e.length) {
            int i = this.N;
            if (i < 3) {
                this.N = i + 1;
                b(bi1);
                return;
            }
            a(eu0.a(((zk0) this).v, null, is0.INVALID_FILE_CRC, null, 5));
        } else if (a(bi1.a(bi1, null, (byte) 0, (byte) 0, null, 0, 0, 0, true, 127)) < 0) {
            a(eu0.a(((zk0) this).v, null, is0.DATABASE_ERROR, null, 5));
        } else {
            this.J += bi1.f;
            this.F.add(bi1.e);
            c(bi1);
            if (this.P) {
                p();
            } else {
                zk0.a(this, qa1.w, null, 2, null);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public v81 a(qa1 qa1) {
        int i = k41.a[qa1.ordinal()];
        if (i == 1) {
            jp0 jp0 = new jp0(((v61) this).D, ((zk0) this).w, 0, 4);
            jp0.b(new ub1(this));
            return jp0;
        } else if (i == 2) {
            bi1 n = n();
            if (n != null) {
                rl0 rl0 = new rl0(n.c(), this.M, 4294967295L, ((zk0) this).w, 0, 16);
                f81 f81 = new f81(this, n);
                if (!((v81) rl0).t) {
                    ((v81) rl0).n.add(f81);
                } else {
                    f81.invoke(rl0);
                }
                aa1 aa1 = new aa1(this, n);
                if (!((v81) rl0).t) {
                    ((v81) rl0).o.add(aa1);
                }
                ((v81) rl0).s = c();
                return rl0;
            }
            ee7.a();
            throw null;
        } else if (i != 3) {
            return null;
        } else {
            bi1 n2 = n();
            if (n2 != null) {
                sp1 sp1 = new sp1(n2.c(), ((zk0) this).w, 0, 4);
                sp1.b(new h61(this));
                return sp1;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<bi1> arrayList) {
        a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
    }

    @DexIgnore
    public final long a(bi1 bi1) {
        l31.a.a(bi1.b, bi1.c, bi1.d);
        if (!(bi1.e.length == 0)) {
            return l31.a.a(bi1);
        }
        ri1 ri1 = ((zk0) this).w;
        le0 le0 = le0.DEBUG;
        String str = ri1.u;
        byte b = bi1.c;
        byte b2 = bi1.d;
        return -1;
    }

    @DexIgnore
    public final bi1 a(byte b, byte b2) {
        return (bi1) ea7.a((List) l31.a.b(((zk0) this).w.u, b, b2), 0);
    }
}
