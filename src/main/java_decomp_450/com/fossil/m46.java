package com.fossil;

import android.text.TextUtils;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m46 extends i46 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public WeatherWatchAppSetting f;
    @DexIgnore
    public List<WeatherLocationWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ j46 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements jo3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ m46 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LatLng $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.m46$b$a$a")
            /* renamed from: com.fossil.m46$b$a$a  reason: collision with other inner class name */
            public static final class C0120a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0120a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0120a aVar = new C0120a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((C0120a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        this.this$0.this$0.a.h.i(this.this$0.this$0.a.g);
                        return i97.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(LatLng latLng, fb7 fb7, b bVar) {
                super(2, fb7);
                this.$it = latLng;
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$it, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    String a2 = yx6.a(this.this$0.b);
                    List b = this.this$0.a.g;
                    String str = this.this$0.c;
                    LatLng latLng = this.$it;
                    double d = latLng.a;
                    double d2 = latLng.b;
                    ee7.a((Object) a2, "name");
                    b.add(new WeatherLocationWrapper(str, d, d2, a2, this.this$0.b, false, true, 32, null));
                    tk7 c = qj7.c();
                    C0120a aVar = new C0120a(this, null);
                    this.L$0 = yi7;
                    this.L$1 = a2;
                    this.label = 1;
                    if (vh7.a(c, aVar, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        public b(m46 m46, String str, String str2) {
            this.a = m46;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            this.a.h.a();
            ee7.a((Object) fetchPlaceResponse, "response");
            Place place = fetchPlaceResponse.getPlace();
            ee7.a((Object) place, "response.place");
            LatLng latLng = place.getLatLng();
            if (latLng != null) {
                ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new a(latLng, null, this), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements io3 {
        @DexIgnore
        public /* final */ /* synthetic */ m46 a;

        @DexIgnore
        public c(m46 m46) {
            this.a = m46;
        }

        @DexIgnore
        @Override // com.fossil.io3
        public final void onFailure(Exception exc) {
            ee7.b(exc, "exception");
            this.a.h.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = m46.i;
            local.e(k, "FetchPlaceRequest - exception=" + exc);
            this.a.h.D0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1", f = "WeatherSettingPresenter.kt", l = {129}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ m46 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends WeatherLocationWrapper>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, d dVar) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends WeatherLocationWrapper>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    List b = this.this$0.this$0.g;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj2 : b) {
                        if (pb7.a(((WeatherLocationWrapper) obj2).isEnableLocation()).booleanValue()) {
                            arrayList.add(obj2);
                        }
                    }
                    return arrayList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(m46 m46, fb7 fb7) {
            super(2, fb7);
            this.this$0 = m46;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            WeatherWatchAppSetting weatherWatchAppSetting;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                WeatherWatchAppSetting d = this.this$0.f;
                if (d != null) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(null, this);
                    this.L$0 = yi7;
                    this.L$1 = d;
                    this.label = 1;
                    obj = vh7.a(a3, aVar, this);
                    if (obj == a2) {
                        return a2;
                    }
                    weatherWatchAppSetting = d;
                }
                this.this$0.h.a();
                this.this$0.h.h(true);
                return i97.a;
            } else if (i == 1) {
                weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            weatherWatchAppSetting.setLocations(ea7.d((Collection) ((List) obj)));
            this.this$0.h.a();
            this.this$0.h.h(true);
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = m46.class.getSimpleName();
        ee7.a((Object) simpleName, "WeatherSettingPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public m46(j46 j46, GoogleApiService googleApiService) {
        ee7.b(j46, "mView");
        ee7.b(googleApiService, "mGoogleApiService");
        this.h = j46;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        this.e = Places.createClient(PortfolioApp.g0.c());
        this.h.i(this.g);
        this.h.a(this.e);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.i46
    public WeatherWatchAppSetting h() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.i46
    public void i() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "saveWeatherWatchAppSetting - mLocationWrappers=" + this.g);
        this.h.b();
        ik7 unused = xh7.b(e(), null, null, new d(this, null), 3, null);
    }

    @DexIgnore
    public void j() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(String str) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        ee7.b(str, MicroAppSetting.SETTING);
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) new Gson().a(str, WeatherWatchAppSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse weather setting " + e2);
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        this.f = weatherWatchAppSetting;
        if (weatherWatchAppSetting == null) {
            this.f = new WeatherWatchAppSetting();
        }
        WeatherWatchAppSetting weatherWatchAppSetting2 = this.f;
        if (weatherWatchAppSetting2 != null) {
            List<WeatherLocationWrapper> locations = weatherWatchAppSetting2.getLocations();
            ArrayList arrayList = new ArrayList();
            for (T t : locations) {
                T t2 = t;
                if (!TextUtils.isEmpty(t2 != null ? t2.getId() : null)) {
                    arrayList.add(t);
                }
            }
            if (arrayList.isEmpty()) {
                this.f = new WeatherWatchAppSetting();
            }
            this.g.clear();
            WeatherWatchAppSetting weatherWatchAppSetting3 = this.f;
            if (weatherWatchAppSetting3 != null) {
                for (WeatherLocationWrapper weatherLocationWrapper : weatherWatchAppSetting3.getLocations()) {
                    if (weatherLocationWrapper != null) {
                        this.g.add(weatherLocationWrapper);
                    }
                }
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.i46
    public void a(int i2, boolean z) {
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (z && arrayList.size() > 1) {
            this.h.S();
        } else if (i2 < this.g.size()) {
            this.g.get(i2).setEnableLocation(z);
            this.h.i(this.g);
        }
    }

    @DexIgnore
    @Override // com.fossil.i46
    public void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        ee7.b(str, "address");
        ee7.b(str2, "placeId");
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (arrayList.size() > 1) {
            this.h.S();
            return;
        }
        this.h.b();
        if (this.e != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Place.Field.ADDRESS);
            arrayList2.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str2, arrayList2);
            ee7.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.e;
            if (placesClient != null) {
                no3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                ee7.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a(new b(this, str, str2));
                fetchPlace.a(new c(this));
                return;
            }
            ee7.a();
            throw null;
        }
    }
}
