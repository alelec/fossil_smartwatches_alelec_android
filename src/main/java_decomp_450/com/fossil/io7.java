package com.fossil;

import com.facebook.stetho.dumpapp.Framer;
import com.facebook.stetho.server.http.HttpHeaders;
import com.fossil.fo7;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class io7 extends RequestBody {
    @DexIgnore
    public static /* final */ ho7 e; // = ho7.a("multipart/mixed");
    @DexIgnore
    public static /* final */ ho7 f; // = ho7.a("multipart/form-data");
    @DexIgnore
    public static /* final */ byte[] g; // = {58, 32};
    @DexIgnore
    public static /* final */ byte[] h; // = {13, 10};
    @DexIgnore
    public static /* final */ byte[] i; // = {Framer.STDIN_FRAME_PREFIX, Framer.STDIN_FRAME_PREFIX};
    @DexIgnore
    public /* final */ br7 a;
    @DexIgnore
    public /* final */ ho7 b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public long d; // = -1;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ br7 a;
        @DexIgnore
        public ho7 b;
        @DexIgnore
        public /* final */ List<b> c;

        @DexIgnore
        public a() {
            this(UUID.randomUUID().toString());
        }

        @DexIgnore
        public a a(ho7 ho7) {
            if (ho7 == null) {
                throw new NullPointerException("type == null");
            } else if (ho7.c().equals("multipart")) {
                this.b = ho7;
                return this;
            } else {
                throw new IllegalArgumentException("multipart != " + ho7);
            }
        }

        @DexIgnore
        public a(String str) {
            this.b = io7.e;
            this.c = new ArrayList();
            this.a = br7.encodeUtf8(str);
        }

        @DexIgnore
        public a a(fo7 fo7, RequestBody requestBody) {
            a(b.a(fo7, requestBody));
            return this;
        }

        @DexIgnore
        public a a(String str, String str2) {
            a(b.a(str, str2));
            return this;
        }

        @DexIgnore
        public a a(String str, String str2, RequestBody requestBody) {
            a(b.a(str, str2, requestBody));
            return this;
        }

        @DexIgnore
        public a a(b bVar) {
            if (bVar != null) {
                this.c.add(bVar);
                return this;
            }
            throw new NullPointerException("part == null");
        }

        @DexIgnore
        public io7 a() {
            if (!this.c.isEmpty()) {
                return new io7(this.a, this.b, this.c);
            }
            throw new IllegalStateException("Multipart body must have at least one part.");
        }
    }

    /*
    static {
        ho7.a("multipart/alternative");
        ho7.a("multipart/digest");
        ho7.a("multipart/parallel");
    }
    */

    @DexIgnore
    public io7(br7 br7, ho7 ho7, List<b> list) {
        this.a = br7;
        this.b = ho7.a(ho7 + "; boundary=" + br7.utf8());
        this.c = ro7.a(list);
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public long a() throws IOException {
        long j = this.d;
        if (j != -1) {
            return j;
        }
        long a2 = a((zq7) null, true);
        this.d = a2;
        return a2;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public ho7 b() {
        return this.b;
    }

    @DexIgnore
    @Override // okhttp3.RequestBody
    public void a(zq7 zq7) throws IOException {
        a(zq7, false);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r13v0, resolved type: com.fossil.zq7 */
    /* JADX DEBUG: Multi-variable search result rejected for r13v1, resolved type: com.fossil.zq7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: com.fossil.yq7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v1, resolved type: com.fossil.yq7 */
    /* JADX DEBUG: Multi-variable search result rejected for r13v3, resolved type: com.fossil.zq7 */
    /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.fossil.yq7 */
    /* JADX WARN: Multi-variable type inference failed */
    public final long a(zq7 zq7, boolean z) throws IOException {
        yq7 yq7;
        if (z) {
            zq7 = new yq7();
            yq7 = zq7;
        } else {
            yq7 = 0;
        }
        int size = this.c.size();
        long j = 0;
        for (int i2 = 0; i2 < size; i2++) {
            b bVar = this.c.get(i2);
            fo7 fo7 = bVar.a;
            RequestBody requestBody = bVar.b;
            zq7.write(i);
            zq7.a(this.a);
            zq7.write(h);
            if (fo7 != null) {
                int b2 = fo7.b();
                for (int i3 = 0; i3 < b2; i3++) {
                    zq7.a(fo7.a(i3)).write(g).a(fo7.b(i3)).write(h);
                }
            }
            ho7 b3 = requestBody.b();
            if (b3 != null) {
                zq7.a("Content-Type: ").a(b3.toString()).write(h);
            }
            long a2 = requestBody.a();
            if (a2 != -1) {
                zq7.a("Content-Length: ").i(a2).write(h);
            } else if (z) {
                yq7.k();
                return -1;
            }
            zq7.write(h);
            if (z) {
                j += a2;
            } else {
                requestBody.a(zq7);
            }
            zq7.write(h);
        }
        zq7.write(i);
        zq7.a(this.a);
        zq7.write(i);
        zq7.write(h);
        if (!z) {
            return j;
        }
        long x = j + yq7.x();
        yq7.k();
        return x;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ fo7 a;
        @DexIgnore
        public /* final */ RequestBody b;

        @DexIgnore
        public b(fo7 fo7, RequestBody requestBody) {
            this.a = fo7;
            this.b = requestBody;
        }

        @DexIgnore
        public static b a(fo7 fo7, RequestBody requestBody) {
            if (requestBody == null) {
                throw new NullPointerException("body == null");
            } else if (fo7 != null && fo7.a("Content-Type") != null) {
                throw new IllegalArgumentException("Unexpected header: Content-Type");
            } else if (fo7 == null || fo7.a(HttpHeaders.CONTENT_LENGTH) == null) {
                return new b(fo7, requestBody);
            } else {
                throw new IllegalArgumentException("Unexpected header: Content-Length");
            }
        }

        @DexIgnore
        public static b a(String str, String str2) {
            return a(str, null, RequestBody.a((ho7) null, str2));
        }

        @DexIgnore
        public static b a(String str, String str2, RequestBody requestBody) {
            if (str != null) {
                StringBuilder sb = new StringBuilder("form-data; name=");
                io7.a(sb, str);
                if (str2 != null) {
                    sb.append("; filename=");
                    io7.a(sb, str2);
                }
                fo7.a aVar = new fo7.a();
                aVar.c("Content-Disposition", sb.toString());
                return a(aVar.a(), requestBody);
            }
            throw new NullPointerException("name == null");
        }
    }

    @DexIgnore
    public static StringBuilder a(StringBuilder sb, String str) {
        sb.append('\"');
        int length = str.length();
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt == '\n') {
                sb.append("%0A");
            } else if (charAt == '\r') {
                sb.append("%0D");
            } else if (charAt != '\"') {
                sb.append(charAt);
            } else {
                sb.append("%22");
            }
        }
        sb.append('\"');
        return sb;
    }
}
