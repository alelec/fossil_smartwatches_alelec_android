package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ zm1 a;
    @DexIgnore
    public /* final */ /* synthetic */ short b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ca1(zm1 zm1, short s) {
        super(1);
        this.a = zm1;
        this.b = s;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        byte[] bArr = ((wa1) v81).Q;
        if (bArr.length == 0) {
            zm1 zm1 = this.a;
            zm1.a(eu0.a(((zk0) zm1).v, null, is0.INVALID_RESPONSE, null, 5));
        } else {
            zm1 zm12 = this.a;
            zm12.G.add(new bi1(((zk0) zm12).w.u, new a81(this.b).a, new a81(this.b).b, bArr, (long) bArr.length, ik1.a.a(bArr, ng1.CRC32), this.a.g(), true));
            zm1.d(this.a);
        }
        return i97.a;
    }
}
