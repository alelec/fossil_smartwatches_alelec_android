package com.fossil;

import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z24 {
    @DexIgnore
    public static /* final */ z24 c; // = new z24("FirebaseCrashlytics");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public int b; // = 4;

    @DexIgnore
    public z24(String str) {
        this.a = str;
    }

    @DexIgnore
    public static z24 a() {
        return c;
    }

    @DexIgnore
    public void b(String str, Throwable th) {
        if (a(6)) {
            Log.e(this.a, str, th);
        }
    }

    @DexIgnore
    public void c(String str, Throwable th) {
        if (a(4)) {
            Log.i(this.a, str, th);
        }
    }

    @DexIgnore
    public void d(String str, Throwable th) {
        if (a(5)) {
            Log.w(this.a, str, th);
        }
    }

    @DexIgnore
    public final boolean a(int i) {
        return this.b <= i || Log.isLoggable(this.a, i);
    }

    @DexIgnore
    public void a(String str, Throwable th) {
        if (a(3)) {
            Log.d(this.a, str, th);
        }
    }

    @DexIgnore
    public void b(String str) {
        b(str, null);
    }

    @DexIgnore
    public void c(String str) {
        c(str, null);
    }

    @DexIgnore
    public void d(String str) {
        d(str, null);
    }

    @DexIgnore
    public void a(String str) {
        a(str, null);
    }
}
