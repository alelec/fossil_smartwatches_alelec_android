package com.fossil;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.StateSet;
import com.fossil.v0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"RestrictedAPI"})
public class x0 extends v0 {
    @DexIgnore
    public a r;
    @DexIgnore
    public boolean s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends v0.c {
        @DexIgnore
        public int[][] J;

        @DexIgnore
        public a(a aVar, x0 x0Var, Resources resources) {
            super(aVar, x0Var, resources);
            if (aVar != null) {
                this.J = aVar.J;
            } else {
                this.J = new int[d()][];
            }
        }

        @DexIgnore
        public int a(int[] iArr, Drawable drawable) {
            int a = a(drawable);
            this.J[a] = iArr;
            return a;
        }

        @DexIgnore
        @Override // com.fossil.v0.c
        public void n() {
            int[][] iArr = this.J;
            int[][] iArr2 = new int[iArr.length][];
            for (int length = iArr.length - 1; length >= 0; length--) {
                int[][] iArr3 = this.J;
                iArr2[length] = iArr3[length] != null ? (int[]) iArr3[length].clone() : null;
            }
            this.J = iArr2;
        }

        @DexIgnore
        public Drawable newDrawable() {
            return new x0(this, null);
        }

        @DexIgnore
        public Drawable newDrawable(Resources resources) {
            return new x0(this, resources);
        }

        @DexIgnore
        public int a(int[] iArr) {
            int[][] iArr2 = this.J;
            int e = e();
            for (int i = 0; i < e; i++) {
                if (StateSet.stateSetMatches(iArr2[i], iArr)) {
                    return i;
                }
            }
            return -1;
        }

        @DexIgnore
        @Override // com.fossil.v0.c
        public void a(int i, int i2) {
            super.a(i, i2);
            int[][] iArr = new int[i2][];
            System.arraycopy(this.J, 0, iArr, 0, i);
            this.J = iArr;
        }
    }

    @DexIgnore
    public x0(a aVar, Resources resources) {
        a(new a(aVar, this, resources));
        onStateChange(getState());
    }

    @DexIgnore
    @Override // com.fossil.v0
    public void applyTheme(Resources.Theme theme) {
        super.applyTheme(theme);
        onStateChange(getState());
    }

    @DexIgnore
    public boolean isStateful() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.v0
    public Drawable mutate() {
        if (!this.s) {
            super.mutate();
            if (this == this) {
                this.r.n();
                this.s = true;
            }
        }
        return this;
    }

    @DexIgnore
    @Override // com.fossil.v0
    public boolean onStateChange(int[] iArr) {
        boolean onStateChange = super.onStateChange(iArr);
        int a2 = this.r.a(iArr);
        if (a2 < 0) {
            a2 = this.r.a(StateSet.WILD_CARD);
        }
        return a(a2) || onStateChange;
    }

    @DexIgnore
    public int[] a(AttributeSet attributeSet) {
        int attributeCount = attributeSet.getAttributeCount();
        int[] iArr = new int[attributeCount];
        int i = 0;
        for (int i2 = 0; i2 < attributeCount; i2++) {
            int attributeNameResource = attributeSet.getAttributeNameResource(i2);
            if (!(attributeNameResource == 0 || attributeNameResource == 16842960 || attributeNameResource == 16843161)) {
                int i3 = i + 1;
                if (!attributeSet.getAttributeBooleanValue(i2, false)) {
                    attributeNameResource = -attributeNameResource;
                }
                iArr[i] = attributeNameResource;
                i = i3;
            }
        }
        return StateSet.trimStateSet(iArr, i);
    }

    @DexIgnore
    public x0(a aVar) {
        if (aVar != null) {
            a(aVar);
        }
    }

    @DexIgnore
    @Override // com.fossil.v0
    public a a() {
        return new a(this.r, this, null);
    }

    @DexIgnore
    @Override // com.fossil.v0
    public void a(v0.c cVar) {
        super.a(cVar);
        if (cVar instanceof a) {
            this.r = (a) cVar;
        }
    }
}
