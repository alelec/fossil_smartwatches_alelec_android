package com.fossil;

import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cm7<E> {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater a; // = AtomicReferenceFieldUpdater.newUpdater(cm7.class, Object.class, "_cur");
    @DexIgnore
    public volatile Object _cur;

    @DexIgnore
    public cm7(boolean z) {
        this._cur = new dm7(8, z);
    }

    @DexIgnore
    public final void a() {
        while (true) {
            dm7 dm7 = (dm7) this._cur;
            if (!dm7.a()) {
                a.compareAndSet(this, dm7, dm7.e());
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final int b() {
        return ((dm7) this._cur).b();
    }

    @DexIgnore
    public final E c() {
        while (true) {
            dm7 dm7 = (dm7) this._cur;
            E e = (E) dm7.f();
            if (e != dm7.g) {
                return e;
            }
            a.compareAndSet(this, dm7, dm7.e());
        }
    }

    @DexIgnore
    public final boolean a(E e) {
        while (true) {
            dm7 dm7 = (dm7) this._cur;
            int a2 = dm7.a(e);
            if (a2 == 0) {
                return true;
            }
            if (a2 == 1) {
                a.compareAndSet(this, dm7, dm7.e());
            } else if (a2 == 2) {
                return false;
            }
        }
    }
}
