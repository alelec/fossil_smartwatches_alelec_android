package com.fossil;

import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn4 {
    @DexIgnore
    @te4("notificationId")
    public String a;
    @DexIgnore
    @te4("title-loc-key")
    public String b;
    @DexIgnore
    @te4("createdAt")
    public Date c;
    @DexIgnore
    @te4("loc-key")
    public String d;
    @DexIgnore
    @te4("challenge")
    public pn4 e;
    @DexIgnore
    @te4("profile")
    public io4 f;
    @DexIgnore
    @te4("confirm")
    public Boolean g;
    @DexIgnore
    @te4("rank")
    public Integer h;

    @DexIgnore
    public final String a() {
        return this.d;
    }

    @DexIgnore
    public final pn4 b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }

    @DexIgnore
    public final io4 d() {
        return this.f;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof tn4)) {
            return false;
        }
        tn4 tn4 = (tn4) obj;
        return ee7.a(this.a, tn4.a) && ee7.a(this.b, tn4.b) && ee7.a(this.c, tn4.c) && ee7.a(this.d, tn4.d) && ee7.a(this.e, tn4.e) && ee7.a(this.f, tn4.f) && ee7.a(this.g, tn4.g) && ee7.a(this.h, tn4.h);
    }

    @DexIgnore
    public int hashCode() {
        String str = this.a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Date date = this.c;
        int hashCode3 = (hashCode2 + (date != null ? date.hashCode() : 0)) * 31;
        String str3 = this.d;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        pn4 pn4 = this.e;
        int hashCode5 = (hashCode4 + (pn4 != null ? pn4.hashCode() : 0)) * 31;
        io4 io4 = this.f;
        int hashCode6 = (hashCode5 + (io4 != null ? io4.hashCode() : 0)) * 31;
        Boolean bool = this.g;
        int hashCode7 = (hashCode6 + (bool != null ? bool.hashCode() : 0)) * 31;
        Integer num = this.h;
        if (num != null) {
            i = num.hashCode();
        }
        return hashCode7 + i;
    }

    @DexIgnore
    public String toString() {
        return "FCMNotificationData(id=" + this.a + ", titleLoc=" + this.b + ", createdAt=" + this.c + ", bodyLoc=" + this.d + ", challenge=" + this.e + ", profile=" + this.f + ", confirm=" + this.g + ", rank=" + this.h + ")";
    }
}
