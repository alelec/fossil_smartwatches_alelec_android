package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iq1 extends fe7 implements gd7<zk0, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;
    @DexIgnore
    public /* final */ /* synthetic */ zk0 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public iq1(km1 km1, zk0 zk0) {
        super(1);
        this.a = km1;
        this.b = zk0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(zk0 zk0) {
        zk0 zk02 = zk0;
        if (!yp0.f.a(this.a.t, this.b)) {
            zk02.a(is0.REQUEST_UNSUPPORTED);
        }
        return i97.a;
    }
}
