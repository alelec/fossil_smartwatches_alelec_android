package com.fossil;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.chart.HeartRateSleepSessionChart;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rg6 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public ArrayList<a> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ ArrayList<ez6> a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ short c;
        @DexIgnore
        public /* final */ short d;

        @DexIgnore
        public a() {
            this(null, 0, 0, 0, 15, null);
        }

        @DexIgnore
        public a(ArrayList<ez6> arrayList, int i, short s, short s2) {
            ee7.b(arrayList, "heartRateSessionData");
            this.a = arrayList;
            this.b = i;
            this.c = s;
            this.d = s2;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final ArrayList<ez6> b() {
            return this.a;
        }

        @DexIgnore
        public final short c() {
            return this.d;
        }

        @DexIgnore
        public final short d() {
            return this.c;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return ee7.a(this.a, aVar.a) && this.b == aVar.b && this.c == aVar.c && this.d == aVar.d;
        }

        @DexIgnore
        public int hashCode() {
            ArrayList<ez6> arrayList = this.a;
            return ((((((arrayList != null ? arrayList.hashCode() : 0) * 31) + this.b) * 31) + this.c) * 31) + this.d;
        }

        @DexIgnore
        public String toString() {
            return "SleepHeartRateUIData(heartRateSessionData=" + this.a + ", duration=" + this.b + ", minHR=" + ((int) this.c) + ", maxHR=" + ((int) this.d) + ")";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(ArrayList arrayList, int i, short s, short s2, int i2, zd7 zd7) {
            this((i2 & 1) != 0 ? new ArrayList() : arrayList, (i2 & 2) != 0 ? 1 : i, (i2 & 4) != 0 ? 0 : s, (i2 & 8) != 0 ? 0 : s2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ HeartRateSleepSessionChart a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(View view) {
            super(view);
            ee7.b(view, "itemView");
            View findViewById = view.findViewById(2131362560);
            ee7.a((Object) findViewById, "itemView.findViewById(R.id.hrssc)");
            this.a = (HeartRateSleepSessionChart) findViewById;
        }

        @DexIgnore
        public final HeartRateSleepSessionChart a() {
            return this.a;
        }
    }

    @DexIgnore
    public rg6(ArrayList<a> arrayList) {
        ee7.b(arrayList, "data");
        this.d = arrayList;
        String b2 = eh5.l.a().b("awakeSleep");
        String str = "#FFFFFF";
        this.a = Color.parseColor(b2 == null ? str : b2);
        String b3 = eh5.l.a().b("lightSleep");
        this.b = Color.parseColor(b3 == null ? str : b3);
        String b4 = eh5.l.a().b("deepSleep");
        this.c = Color.parseColor(b4 != null ? b4 : str);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        a aVar = this.d.get(i);
        ee7.a((Object) aVar, "data[position]");
        a aVar2 = aVar;
        bVar.a().setMDuration(aVar2.a());
        bVar.a().setMMinHRValue(aVar2.d());
        bVar.a().setMMaxHRValue(aVar2.c());
        bVar.a().a(this.c, this.b, this.a);
        bVar.a().a(aVar2.b());
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.d.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558690, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new b(inflate);
    }

    @DexIgnore
    public final void a(ArrayList<a> arrayList) {
        ee7.b(arrayList, "data");
        this.d = arrayList;
        notifyDataSetChanged();
    }
}
