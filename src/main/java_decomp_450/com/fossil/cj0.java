package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj0 extends BroadcastReceiver {
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        m60 m60;
        String action = intent != null ? intent.getAction() : null;
        if (action != null && action.hashCode() == -343131398 && action.equals("com.fossil.blesdk.device.DeviceImplementation.action.HID_STATE_CHANGED")) {
            km1 km1 = (km1) intent.getParcelableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.DEVICE");
            l60.d dVar = (l60.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.PREVIOUS_HID_STATE");
            l60.d dVar2 = (l60.d) intent.getSerializableExtra("com.fossil.blesdk.device.DeviceImplementation.extra.NEW_HID_STATE");
            t11 t11 = t11.a;
            xm0 xm0 = xm0.i;
            if (!(km1 == null || (m60 = km1.t) == null)) {
                m60.getMacAddress();
            }
            if (km1 != null && dVar != null && dVar2 != null) {
                xm0.i.a(km1, dVar2);
            }
        }
    }
}
