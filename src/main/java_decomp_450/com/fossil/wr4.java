package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr4 extends FragmentStateAdapter {
    @DexIgnore
    public /* final */ List<Fragment> i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wr4(List<Fragment> list, Fragment fragment) {
        super(fragment);
        ee7.b(list, "frags");
        ee7.b(fragment, "fragment");
        this.i = list;
    }

    @DexIgnore
    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment a(int i2) {
        return this.i.get(i2);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.i.size();
    }
}
