package com.fossil;

import android.os.DeadObjectException;
import android.os.RemoteException;
import com.fossil.u12;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class e42<T> extends s42 {
    @DexIgnore
    public /* final */ oo3<T> b;

    @DexIgnore
    public e42(int i, oo3<T> oo3) {
        super(i);
        this.b = oo3;
    }

    @DexIgnore
    @Override // com.fossil.i32
    public void a(Status status) {
        this.b.b(new w02(status));
    }

    @DexIgnore
    public abstract void d(u12.a<?> aVar) throws RemoteException;

    @DexIgnore
    @Override // com.fossil.i32
    public void a(Exception exc) {
        this.b.b(exc);
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(u12.a<?> aVar) throws DeadObjectException {
        try {
            d(aVar);
        } catch (DeadObjectException e) {
            a(i32.a(e));
            throw e;
        } catch (RemoteException e2) {
            a(i32.a(e2));
        } catch (RuntimeException e3) {
            a(e3);
        }
    }
}
