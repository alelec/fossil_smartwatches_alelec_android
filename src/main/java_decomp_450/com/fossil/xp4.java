package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.fossil.oy6;
import com.portfolio.platform.view.FlexibleCheckBox;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xp4 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ List<mo4> a; // = new ArrayList();
    @DexIgnore
    public /* final */ oy6.b b;
    @DexIgnore
    public /* final */ st4 c;
    @DexIgnore
    public /* final */ a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(mo4 mo4);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ oa5 a;
        @DexIgnore
        public /* final */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ xp4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;
            @DexIgnore
            public /* final */ /* synthetic */ mo4 b;

            @DexIgnore
            public a(b bVar, mo4 mo4, oy6.b bVar2, st4 st4) {
                this.a = bVar;
                this.b = mo4;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.getAdapterPosition() != -1) {
                    mo4 mo4 = this.b;
                    if (view != null) {
                        mo4.a(((FlexibleCheckBox) view).isChecked());
                        this.a.c.d.a(this.b);
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.view.FlexibleCheckBox");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xp4 xp4, oa5 oa5, View view) {
            super(view);
            ee7.b(oa5, "binding");
            ee7.b(view, "root");
            this.c = xp4;
            this.a = oa5;
            this.b = view;
        }

        @DexIgnore
        public void a(mo4 mo4, oy6.b bVar, st4 st4) {
            ee7.b(mo4, "friend");
            ee7.b(bVar, "drawableBuilder");
            ee7.b(st4, "colorGenerator");
            oa5 oa5 = this.a;
            String b2 = fu4.a.b(mo4.a(), mo4.c(), mo4.e());
            FlexibleTextView flexibleTextView = oa5.u;
            ee7.a((Object) flexibleTextView, "tvFullName");
            flexibleTextView.setText(b2);
            FlexibleCheckBox flexibleCheckBox = oa5.q;
            ee7.a((Object) flexibleCheckBox, "cbInvite");
            flexibleCheckBox.setChecked(mo4.f());
            ImageView imageView = oa5.s;
            ee7.a((Object) imageView, "ivAvatar");
            rt4.a(imageView, mo4.d(), b2, bVar, st4);
            oa5.q.setOnClickListener(new a(this, mo4, bVar, st4));
        }
    }

    @DexIgnore
    public xp4(a aVar) {
        ee7.b(aVar, "listener");
        this.d = aVar;
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.b = b2;
        this.c = st4.d.a();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        return i;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void setHasStableIds(boolean z) {
        super.setHasStableIds(true);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        ee7.b(bVar, "holder");
        bVar.a(this.a.get(i), this.b, this.c);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        oa5 a2 = oa5.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        ee7.a((Object) a2, "ItemSuggestingFriendLayo\u2026tInflater, parent, false)");
        View d2 = a2.d();
        ee7.a((Object) d2, "itemFriendInviteListBinding.root");
        return new b(this, a2, d2);
    }

    @DexIgnore
    public final void a(List<mo4> list) {
        ee7.b(list, NativeProtocol.AUDIENCE_FRIENDS);
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }
}
