package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q87 extends Error {
    @DexIgnore
    public q87() {
        this(null, 1, null);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public q87(String str) {
        super(str);
        ee7.b(str, "message");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ q87(String str, int i, zd7 zd7) {
        this((i & 1) != 0 ? "An operation is not implemented." : str);
    }
}
