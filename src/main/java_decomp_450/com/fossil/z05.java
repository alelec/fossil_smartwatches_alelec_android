package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z05 extends y05 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;
    @DexIgnore
    public /* final */ ScrollView z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131362962, 1);
        C.put(2131362278, 2);
        C.put(2131362672, 3);
        C.put(2131362394, 4);
        C.put(2131362393, 5);
        C.put(2131362392, 6);
        C.put(2131362954, 7);
        C.put(2131362352, 8);
        C.put(2131362489, 9);
    }
    */

    @DexIgnore
    public z05(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 10, B, C));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public z05(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleEditText) objArr[2], (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[9], (ImageView) objArr[3], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[1]);
        this.A = -1;
        ScrollView scrollView = (ScrollView) objArr[0];
        this.z = scrollView;
        scrollView.setTag(null);
        a(view);
        f();
    }
}
