package com.fossil;

import com.fossil.ql4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sn5 extends ql4<a.b, a.c, a.C0172a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sn5$a$a")
        /* renamed from: com.fossil.sn5$a$a  reason: collision with other inner class name */
        public static final class C0172a implements ql4.a {
            @DexIgnore
            public C0172a(int i) {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements ql4.b {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements ql4.c {
            @DexIgnore
            public c(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return sn5.e;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ sn5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(sn5 sn5) {
            this.a = sn5;
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = sn5.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.a.a().a(new a.C0172a(i));
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.source.ServerSettingDataSource.OnGetServerSettingList
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(sn5.f.a(), "executeUseCase - onSuccess");
            this.a.a().onSuccess(new a.c(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = sn5.class.getSimpleName();
        ee7.a((Object) simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public sn5(ServerSettingRepository serverSettingRepository) {
        ee7.b(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    public void a(a.b bVar) {
        this.d.getServerSettingList(new b(this));
    }
}
