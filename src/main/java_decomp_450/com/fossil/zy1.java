package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zy1 extends oz1 {
    @DexIgnore
    @Override // com.fossil.nz1
    public void a(GoogleSignInAccount googleSignInAccount, Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public void b(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Override // com.fossil.nz1
    public void a(Status status) throws RemoteException {
        throw new UnsupportedOperationException();
    }
}
