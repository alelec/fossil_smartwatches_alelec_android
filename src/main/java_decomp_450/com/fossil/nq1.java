package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq1 {
    @DexIgnore
    public /* synthetic */ nq1(zd7 zd7) {
    }

    @DexIgnore
    public final ms1[] a(int i) {
        ms1[] values = ms1.values();
        ArrayList arrayList = new ArrayList();
        for (ms1 ms1 : values) {
            if ((ms1.a & i) != 0) {
                arrayList.add(ms1);
            }
        }
        Object[] array = arrayList.toArray(new ms1[0]);
        if (array != null) {
            return (ms1[]) array;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
