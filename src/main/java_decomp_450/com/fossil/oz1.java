package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class oz1 extends if2 implements nz1 {
    @DexIgnore
    public oz1() {
        super("com.google.android.gms.auth.api.signin.internal.ISignInCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.if2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        switch (i) {
            case 101:
                a((GoogleSignInAccount) jf2.a(parcel, GoogleSignInAccount.CREATOR), (Status) jf2.a(parcel, Status.CREATOR));
                throw null;
            case 102:
                a((Status) jf2.a(parcel, Status.CREATOR));
                break;
            case 103:
                b((Status) jf2.a(parcel, Status.CREATOR));
                break;
            default:
                return false;
        }
        parcel2.writeNoException();
        return true;
    }
}
