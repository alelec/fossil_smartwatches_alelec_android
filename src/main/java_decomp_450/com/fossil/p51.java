package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p51<T> implements Comparator<T> {
    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return bb7.a(Byte.valueOf(t.getMessageId()), Byte.valueOf(t2.getMessageId()));
    }
}
