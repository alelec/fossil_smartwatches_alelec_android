package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x70 extends s70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public x70 createFromParcel(Parcel parcel) {
            return new x70(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public x70[] newArray(int i) {
            return new x70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public x70 m84createFromParcel(Parcel parcel) {
            return new x70(parcel, (zd7) null);
        }
    }

    @DexIgnore
    public x70() {
        super(u70.HEART_RATE, null, null, null, 14);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ x70(ue0 ue0, ve0 ve0, int i, zd7 zd7) {
        this(ue0, (i & 2) != 0 ? new ve0(ve0.CREATOR.a()) : ve0);
    }

    @DexIgnore
    public x70(ue0 ue0, ve0 ve0) {
        super(u70.HEART_RATE, null, ue0, ve0, 2);
    }

    @DexIgnore
    public /* synthetic */ x70(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
