package com.fossil;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xs4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ List<Object> a; // = new ArrayList();
    @DexIgnore
    public yt4 b; // = new yt4(1);
    @DexIgnore
    public ws4 c; // = new ws4(2);
    @DexIgnore
    public qs4 d; // = new qs4(3);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public final void a(TimerViewObserver timerViewObserver) {
        ee7.b(timerViewObserver, "observer");
        this.c.a(timerViewObserver);
        this.d.a(timerViewObserver);
    }

    @DexIgnore
    public final Object b(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public final void c(int i) {
        int size = this.a.size();
        int i2 = i - 1;
        int i3 = i + 1;
        if (i != -1 && i < size) {
            if (size == 2) {
                try {
                    this.a.clear();
                    notifyDataSetChanged();
                } catch (Exception unused) {
                    FLogger.INSTANCE.getLocal().e("RecommendationChallengeAdapter", "remove : index: " + i + " - size: " + size);
                }
            } else if (i2 == 0 && i3 < size && (this.a.get(i2) instanceof String) && (this.a.get(i3) instanceof String)) {
                this.a.remove(0);
                this.a.remove(0);
                notifyItemRangeRemoved(0, 2);
            } else if (i2 <= -1 || !(this.a.get(i2) instanceof String) || i3 != size) {
                this.a.remove(i);
                notifyItemRemoved(i);
            } else {
                this.a.remove(i);
                notifyItemRemoved(i);
                this.a.remove(i2);
                notifyItemRemoved(i2);
            }
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        if (this.b.a(this.a, i)) {
            return this.b.a();
        }
        if (this.c.a(this.a, i)) {
            return this.c.a();
        }
        if (this.d.a(this.a, i)) {
            return this.d.a();
        }
        throw new IllegalArgumentException("No delegate for this position : " + i);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        int itemViewType = getItemViewType(i);
        if (itemViewType == this.b.a()) {
            this.b.a(this.a, i, viewHolder);
        } else if (itemViewType == this.c.a()) {
            this.c.a(this.a, i, viewHolder);
        } else if (itemViewType == this.d.a()) {
            this.d.a(this.a, i, viewHolder);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == this.b.a()) {
            return this.b.a(viewGroup);
        }
        if (i == this.c.a()) {
            return this.c.a(viewGroup);
        }
        if (i == this.d.a()) {
            return this.d.a(viewGroup);
        }
        throw new IllegalArgumentException("No support for this viewType: " + i);
    }

    @DexIgnore
    public final void a(int i) {
        Object obj = this.a.get(i);
        if (!(obj instanceof jo4)) {
            obj = null;
        }
        jo4 jo4 = (jo4) obj;
        if (jo4 != null) {
            jo4.a(false);
        }
        notifyItemChanged(i);
    }

    @DexIgnore
    public final void a(List<? extends Object> list) {
        ee7.b(list, "newData");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }
}
