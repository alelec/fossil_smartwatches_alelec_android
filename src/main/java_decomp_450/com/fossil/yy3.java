package com.fossil;

import com.fossil.by3;
import com.fossil.ew3;
import com.fossil.yz3;
import com.google.j2objc.annotations.Weak;
import com.j256.ormlite.stmt.query.SimpleComparison;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy3 {
    @DexIgnore
    public static /* final */ ew3.b a; // = cx3.a.b(SimpleComparison.EQUAL_TO_OPERATION);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends uw3<K, V> {
        @DexIgnore
        public /* final */ /* synthetic */ Map.Entry a;

        @DexIgnore
        public a(Map.Entry entry) {
            this.a = entry;
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.uw3
        public K getKey() {
            return (K) this.a.getKey();
        }

        @DexIgnore
        @Override // java.util.Map.Entry, com.fossil.uw3
        public V getValue() {
            return (V) this.a.getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends j04<Map.Entry<K, V>> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public b(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            return yy3.b((Map.Entry) this.a.next());
        }
    }

    @DexIgnore
    public enum c implements cw3<Map.Entry<?, ?>, Object> {
        KEY {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getKey();
            }
        },
        VALUE {
            @DexIgnore
            public Object apply(Map.Entry<?, ?> entry) {
                return entry.getValue();
            }
        };

        @DexIgnore
        public /* synthetic */ c(xy3 xy3) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K, V> extends yz3.a<Map.Entry<K, V>> {
        @DexIgnore
        public abstract Map<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public abstract boolean contains(Object obj);

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.yz3.a, java.util.Collection, java.util.AbstractSet, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            try {
                jw3.a(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException unused) {
                return yz3.a((Set<?>) this, collection.iterator());
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, com.fossil.yz3.a, java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            try {
                jw3.a(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a = yz3.a(collection.size());
                for (Object obj : collection) {
                    if (contains(obj)) {
                        a.add(((Map.Entry) obj).getKey());
                    }
                }
                return a().keySet().retainAll(a);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<K, V> extends yz3.a<K> {
        @DexIgnore
        @Weak
        public /* final */ Map<K, V> a;

        @DexIgnore
        public e(Map<K, V> map) {
            jw3.a(map);
            this.a = map;
        }

        @DexIgnore
        public Map<K, V> a() {
            return this.a;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsKey(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return yy3.a(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!contains(obj)) {
                return false;
            }
            a().remove(obj);
            return true;
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f<K, V> extends AbstractCollection<V> {
        @DexIgnore
        @Weak
        public /* final */ Map<K, V> a;

        @DexIgnore
        public f(Map<K, V> map) {
            jw3.a(map);
            this.a = map;
        }

        @DexIgnore
        public final Map<K, V> a() {
            return this.a;
        }

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return a().containsValue(obj);
        }

        @DexIgnore
        public boolean isEmpty() {
            return a().isEmpty();
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return yy3.c(a().entrySet().iterator());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            try {
                return super.remove(obj);
            } catch (UnsupportedOperationException unused) {
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (gw3.a(obj, entry.getValue())) {
                        a().remove(entry.getKey());
                        return true;
                    }
                }
                return false;
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            try {
                jw3.a(collection);
                return super.removeAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a2 = yz3.a();
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a2.add(entry.getKey());
                    }
                }
                return a().keySet().removeAll(a2);
            }
        }

        @DexIgnore
        @Override // java.util.AbstractCollection, java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            try {
                jw3.a(collection);
                return super.retainAll(collection);
            } catch (UnsupportedOperationException unused) {
                HashSet a2 = yz3.a();
                for (Map.Entry<K, V> entry : a().entrySet()) {
                    if (collection.contains(entry.getValue())) {
                        a2.add(entry.getKey());
                    }
                }
                return a().keySet().retainAll(a2);
            }
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g<K, V> extends AbstractMap<K, V> {
        @DexIgnore
        public transient Set<Map.Entry<K, V>> a;
        @DexIgnore
        public transient Collection<V> b;

        @DexIgnore
        public abstract Set<Map.Entry<K, V>> a();

        @DexIgnore
        public Collection<V> b() {
            return new f(this);
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Set<Map.Entry<K, V>> entrySet() {
            Set<Map.Entry<K, V>> set = this.a;
            if (set != null) {
                return set;
            }
            Set<Map.Entry<K, V>> a2 = a();
            this.a = a2;
            return a2;
        }

        @DexIgnore
        @Override // java.util.AbstractMap, java.util.Map
        public Collection<V> values() {
            Collection<V> collection = this.b;
            if (collection != null) {
                return collection;
            }
            Collection<V> b2 = b();
            this.b = b2;
            return b2;
        }
    }

    @DexIgnore
    public static <K> cw3<Map.Entry<K, ?>, K> a() {
        return c.KEY;
    }

    @DexIgnore
    public static <K, V> HashMap<K, V> b() {
        return new HashMap<>();
    }

    @DexIgnore
    public static <V> cw3<Map.Entry<?, V>, V> c() {
        return c.VALUE;
    }

    @DexIgnore
    public static boolean d(Map<?, ?> map, Object obj) {
        jw3.a(map);
        try {
            return map.containsKey(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return false;
        }
    }

    @DexIgnore
    public static <V> V e(Map<?, V> map, Object obj) {
        jw3.a(map);
        try {
            return map.get(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    @DexIgnore
    public static <V> V f(Map<?, V> map, Object obj) {
        jw3.a(map);
        try {
            return map.remove(obj);
        } catch (ClassCastException | NullPointerException unused) {
            return null;
        }
    }

    @DexIgnore
    public static <K, V> Iterator<K> a(Iterator<Map.Entry<K, V>> it) {
        return qy3.a((Iterator) it, a());
    }

    @DexIgnore
    public static <K, V> LinkedHashMap<K, V> b(int i) {
        return new LinkedHashMap<>(a(i));
    }

    @DexIgnore
    public static <K, V> Iterator<V> c(Iterator<Map.Entry<K, V>> it) {
        return qy3.a((Iterator) it, c());
    }

    @DexIgnore
    public static int a(int i) {
        if (i < 3) {
            bx3.a(i, "expectedSize");
            return i + 1;
        } else if (i < 1073741824) {
            return (int) ((((float) i) / 0.75f) + 1.0f);
        } else {
            return Integer.MAX_VALUE;
        }
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> b(Map.Entry<? extends K, ? extends V> entry) {
        jw3.a(entry);
        return new a(entry);
    }

    @DexIgnore
    public static boolean c(Map<?, ?> map, Object obj) {
        if (map == obj) {
            return true;
        }
        if (obj instanceof Map) {
            return map.entrySet().equals(((Map) obj).entrySet());
        }
        return false;
    }

    @DexIgnore
    public static <K, V> Map.Entry<K, V> a(K k, V v) {
        return new wx3(k, v);
    }

    @DexIgnore
    public static boolean a(Map<?, ?> map, Object obj) {
        return qy3.a(a(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    public static <K, V> j04<Map.Entry<K, V>> b(Iterator<Map.Entry<K, V>> it) {
        return new b(it);
    }

    @DexIgnore
    public static String a(Map<?, ?> map) {
        StringBuilder a2 = cx3.a(map.size());
        a2.append('{');
        a.a(a2, map);
        a2.append('}');
        return a2.toString();
    }

    @DexIgnore
    public static boolean b(Map<?, ?> map, Object obj) {
        return qy3.a(c(map.entrySet().iterator()), obj);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<K, V> */
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public static <K, V> void a(Map<K, V> map, Map<? extends K, ? extends V> map2) {
        for (Map.Entry<? extends K, ? extends V> entry : map2.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }

    @DexIgnore
    public static <K> K a(Map.Entry<K, ?> entry) {
        if (entry == null) {
            return null;
        }
        return entry.getKey();
    }

    @DexIgnore
    public static <E> by3<E, Integer> a(Collection<E> collection) {
        by3.b bVar = new by3.b(collection.size());
        int i = 0;
        for (E e2 : collection) {
            bVar.a(e2, Integer.valueOf(i));
            i++;
        }
        return bVar.a();
    }
}
