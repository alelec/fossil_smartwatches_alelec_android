package com.fossil;

import android.app.Activity;
import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class de5 {
    @DexIgnore
    public static boolean a(Context context) {
        return (context instanceof Activity) && !((Activity) context).isFinishing();
    }
}
