package com.fossil;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dm4 extends jc {
    @DexIgnore
    public /* final */ ArrayList<Fragment> g; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<String> h; // = new ArrayList<>();

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public dm4(FragmentManager fragmentManager) {
        super(fragmentManager);
        ee7.b(fragmentManager, "fm");
    }

    @DexIgnore
    @Override // com.fossil.pl
    public int a() {
        return this.g.size();
    }

    @DexIgnore
    @Override // com.fossil.jc
    public Fragment c(int i) {
        Fragment fragment = this.g.get(i);
        ee7.a((Object) fragment, "mFragmentList.get(position)");
        return fragment;
    }

    @DexIgnore
    public final void a(Fragment fragment, String str) {
        ee7.b(fragment, "fragment");
        ee7.b(str, "title");
        this.g.add(fragment);
        this.h.add(str);
    }

    @DexIgnore
    @Override // com.fossil.pl
    public CharSequence a(int i) {
        String str = this.h.get(i);
        ee7.a((Object) str, "mFragmentTitleList.get(position)");
        return str;
    }
}
