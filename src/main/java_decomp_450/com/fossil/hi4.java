package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi4 extends oi4 {
    @DexIgnore
    @Override // com.fossil.sg4, com.fossil.li4
    public dh4 a(String str, mg4 mg4, int i, int i2, Map<og4, ?> map) throws tg4 {
        if (mg4 == mg4.EAN_8) {
            return super.a(str, mg4, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got " + mg4);
    }

    @DexIgnore
    @Override // com.fossil.li4
    public boolean[] a(String str) {
        if (str.length() == 8) {
            boolean[] zArr = new boolean[67];
            int a = li4.a(zArr, 0, ni4.a, true) + 0;
            int i = 0;
            while (i <= 3) {
                int i2 = i + 1;
                a += li4.a(zArr, a, ni4.d[Integer.parseInt(str.substring(i, i2))], false);
                i = i2;
            }
            int a2 = a + li4.a(zArr, a, ni4.b, false);
            int i3 = 4;
            while (i3 <= 7) {
                int i4 = i3 + 1;
                a2 += li4.a(zArr, a2, ni4.d[Integer.parseInt(str.substring(i3, i4))], true);
                i3 = i4;
            }
            li4.a(zArr, a2, ni4.a, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be 8 digits long, but got " + str.length());
    }
}
