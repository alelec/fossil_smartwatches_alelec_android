package com.fossil;

import com.fossil.jz3;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kx3<T> extends jz3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ by3<T, Integer> rankMap;

    @DexIgnore
    public kx3(List<T> list) {
        this(yy3.a(list));
    }

    @DexIgnore
    public final int a(T t) {
        Integer num = this.rankMap.get(t);
        if (num != null) {
            return num.intValue();
        }
        throw new jz3.c(t);
    }

    @DexIgnore
    @Override // com.fossil.jz3, java.util.Comparator
    public int compare(T t, T t2) {
        return a(t) - a(t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof kx3) {
            return this.rankMap.equals(((kx3) obj).rankMap);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.rankMap.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.explicit(" + this.rankMap.keySet() + ")";
    }

    @DexIgnore
    public kx3(by3<T, Integer> by3) {
        this.rankMap = by3;
    }
}
