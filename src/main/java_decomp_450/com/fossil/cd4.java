package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.internal.ServerProtocol;
import com.fossil.zc4;
import com.google.firebase.iid.FirebaseInstanceId;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cd4 {
    @DexIgnore
    public static /* final */ r84 a;

    /*
    static {
        d94 d94 = new d94();
        d94.a(zc4.b.class, (u84) new zc4.c());
        d94.a(zc4.class, (u84) new zc4.a());
        a = d94.a();
    }
    */

    @DexIgnore
    public static void a(Intent intent, ft1<String> ft1) {
        a("_nr", intent);
        if (ft1 != null) {
            a("MESSAGE_DELIVERED", intent, ft1);
        }
    }

    @DexIgnore
    public static String b(Intent intent) {
        return intent.getStringExtra("google.c.a.c_id");
    }

    @DexIgnore
    public static String c(Intent intent) {
        return intent.getStringExtra("google.c.a.c_l");
    }

    @DexIgnore
    public static String d(Intent intent) {
        return intent.getStringExtra("google.c.a.m_c");
    }

    @DexIgnore
    public static String e(Intent intent) {
        String stringExtra = intent.getStringExtra("google.message_id");
        return stringExtra == null ? intent.getStringExtra("message_id") : stringExtra;
    }

    @DexIgnore
    public static String f(Intent intent) {
        return intent.getStringExtra("google.c.a.m_l");
    }

    @DexIgnore
    public static String g(Intent intent) {
        return intent.getStringExtra("google.c.a.ts");
    }

    @DexIgnore
    public static String h(Intent intent) {
        return (intent.getExtras() == null || !dd4.a(intent.getExtras())) ? "DATA_MESSAGE" : "DISPLAY_NOTIFICATION";
    }

    @DexIgnore
    public static String i(Intent intent) {
        return (intent.getExtras() == null || !dd4.a(intent.getExtras())) ? "data" : ServerProtocol.DIALOG_PARAM_DISPLAY;
    }

    @DexIgnore
    public static int j(Intent intent) {
        String stringExtra = intent.getStringExtra("google.delivered_priority");
        if (stringExtra == null) {
            if ("1".equals(intent.getStringExtra("google.priority_reduced"))) {
                return 2;
            }
            stringExtra = intent.getStringExtra("google.priority");
        }
        return a(stringExtra);
    }

    @DexIgnore
    public static String k(Intent intent) {
        String stringExtra = intent.getStringExtra("from");
        if (stringExtra == null || !stringExtra.startsWith("/topics/")) {
            return null;
        }
        return stringExtra;
    }

    @DexIgnore
    public static int l(Intent intent) {
        Object obj = intent.getExtras().get("google.ttl");
        if (obj instanceof Integer) {
            return ((Integer) obj).intValue();
        }
        if (!(obj instanceof String)) {
            return 0;
        }
        try {
            return Integer.parseInt((String) obj);
        } catch (NumberFormatException unused) {
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
            sb.append("Invalid TTL: ");
            sb.append(valueOf);
            Log.w("FirebaseMessaging", sb.toString());
            return 0;
        }
    }

    @DexIgnore
    public static String m(Intent intent) {
        if (intent.hasExtra("google.c.a.udt")) {
            return intent.getStringExtra("google.c.a.udt");
        }
        return null;
    }

    @DexIgnore
    public static boolean n(Intent intent) {
        return "com.google.firebase.messaging.RECEIVE_DIRECT_BOOT".equals(intent.getAction());
    }

    @DexIgnore
    public static void o(Intent intent) {
        a("_nd", intent);
    }

    @DexIgnore
    public static void p(Intent intent) {
        a("_nf", intent);
    }

    @DexIgnore
    public static void q(Intent intent) {
        r(intent);
        a("_no", intent);
    }

    @DexIgnore
    public static void r(Intent intent) {
        if (intent != null) {
            if ("1".equals(intent.getStringExtra("google.c.a.tc"))) {
                o14 o14 = (o14) l14.j().a(o14.class);
                if (Log.isLoggable("FirebaseMessaging", 3)) {
                    Log.d("FirebaseMessaging", "Received event with track-conversion=true. Setting user property and reengagement event");
                }
                if (o14 != null) {
                    String stringExtra = intent.getStringExtra("google.c.a.c_id");
                    o14.a("fcm", "_ln", stringExtra);
                    Bundle bundle = new Bundle();
                    bundle.putString("source", "Firebase");
                    bundle.putString("medium", "notification");
                    bundle.putString(AppEventsLogger.PUSH_PAYLOAD_CAMPAIGN_KEY, stringExtra);
                    o14.a("fcm", "_cmp", bundle);
                    return;
                }
                Log.w("FirebaseMessaging", "Unable to set user property for conversion tracking:  analytics library is missing");
            } else if (Log.isLoggable("FirebaseMessaging", 3)) {
                Log.d("FirebaseMessaging", "Received event with track-conversion=false. Do not set user property");
            }
        }
    }

    @DexIgnore
    public static boolean s(Intent intent) {
        if (intent == null || n(intent)) {
            return false;
        }
        return a();
    }

    @DexIgnore
    public static boolean t(Intent intent) {
        if (intent == null || n(intent)) {
            return false;
        }
        return "1".equals(intent.getStringExtra("google.c.a.e"));
    }

    @DexIgnore
    public static String b() {
        return FirebaseInstanceId.getInstance(l14.j()).e();
    }

    @DexIgnore
    public static String c() {
        return l14.j().b().getPackageName();
    }

    @DexIgnore
    public static String d() {
        l14 j = l14.j();
        String c = j.d().c();
        if (c != null) {
            return c;
        }
        String b = j.d().b();
        if (!b.startsWith("1:")) {
            return b;
        }
        String[] split = b.split(":");
        if (split.length < 2) {
            return null;
        }
        String str = split[1];
        if (str.isEmpty()) {
            return null;
        }
        return str;
    }

    @DexIgnore
    public static boolean a() {
        ApplicationInfo applicationInfo;
        try {
            l14.j();
            Context b = l14.j().b();
            SharedPreferences sharedPreferences = b.getSharedPreferences("com.google.firebase.messaging", 0);
            if (sharedPreferences.contains("export_to_big_query")) {
                return sharedPreferences.getBoolean("export_to_big_query", false);
            }
            try {
                PackageManager packageManager = b.getPackageManager();
                if (!(packageManager == null || (applicationInfo = packageManager.getApplicationInfo(b.getPackageName(), 128)) == null || applicationInfo.metaData == null || !applicationInfo.metaData.containsKey("delivery_metrics_exported_to_big_query_enabled"))) {
                    return applicationInfo.metaData.getBoolean("delivery_metrics_exported_to_big_query_enabled", false);
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
            return false;
        } catch (IllegalStateException unused2) {
            Log.i("FirebaseMessaging", "FirebaseApp has not being initialized. Device might be in direct boot mode. Skip exporting delivery metrics to Big Query");
            return false;
        }
    }

    @DexIgnore
    public static void a(String str, Intent intent) {
        Bundle bundle = new Bundle();
        String b = b(intent);
        if (b != null) {
            bundle.putString("_nmid", b);
        }
        String c = c(intent);
        if (c != null) {
            bundle.putString("_nmn", c);
        }
        String f = f(intent);
        if (!TextUtils.isEmpty(f)) {
            bundle.putString("label", f);
        }
        String d = d(intent);
        if (!TextUtils.isEmpty(d)) {
            bundle.putString("message_channel", d);
        }
        String k = k(intent);
        if (k != null) {
            bundle.putString("_nt", k);
        }
        String g = g(intent);
        if (g != null) {
            try {
                bundle.putInt("_nmt", Integer.parseInt(g));
            } catch (NumberFormatException e) {
                Log.w("FirebaseMessaging", "Error while parsing timestamp in GCM event", e);
            }
        }
        String m = m(intent);
        if (m != null) {
            try {
                bundle.putInt("_ndt", Integer.parseInt(m));
            } catch (NumberFormatException e2) {
                Log.w("FirebaseMessaging", "Error while parsing use_device_time in GCM event", e2);
            }
        }
        String i = i(intent);
        if ("_nr".equals(str) || "_nf".equals(str)) {
            bundle.putString("_nmc", i);
        }
        if (Log.isLoggable("FirebaseMessaging", 3)) {
            String valueOf = String.valueOf(bundle);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 37 + String.valueOf(valueOf).length());
            sb.append("Logging to scion event=");
            sb.append(str);
            sb.append(" scionPayload=");
            sb.append(valueOf);
            Log.d("FirebaseMessaging", sb.toString());
        }
        o14 o14 = (o14) l14.j().a(o14.class);
        if (o14 != null) {
            o14.a("fcm", str, bundle);
        } else {
            Log.w("FirebaseMessaging", "Unable to log event: analytics library is missing");
        }
    }

    @DexIgnore
    public static void a(String str, Intent intent, ft1<String> ft1) {
        try {
            ft1.a(ct1.b(a.a(new zc4.b(new zc4(str, intent)))));
        } catch (t84 unused) {
            Log.d("FirebaseMessaging", "Failed to encode big query analytics payload. Skip sending");
        }
    }

    @DexIgnore
    public static String a(Intent intent) {
        return intent.getStringExtra("collapse_key");
    }

    @DexIgnore
    public static int a(String str) {
        if ("high".equals(str)) {
            return 1;
        }
        return "normal".equals(str) ? 2 : 0;
    }
}
