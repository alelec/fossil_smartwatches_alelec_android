package com.fossil;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t02 extends ac {
    @DexIgnore
    public Dialog a;
    @DexIgnore
    public DialogInterface.OnCancelListener b;

    @DexIgnore
    public static t02 a(Dialog dialog, DialogInterface.OnCancelListener onCancelListener) {
        t02 t02 = new t02();
        a72.a(dialog, "Cannot display null dialog");
        Dialog dialog2 = dialog;
        dialog2.setOnCancelListener(null);
        dialog2.setOnDismissListener(null);
        t02.a = dialog2;
        if (onCancelListener != null) {
            t02.b = onCancelListener;
        }
        return t02;
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void onCancel(DialogInterface dialogInterface) {
        DialogInterface.OnCancelListener onCancelListener = this.b;
        if (onCancelListener != null) {
            onCancelListener.onCancel(dialogInterface);
        }
    }

    @DexIgnore
    @Override // com.fossil.ac
    public Dialog onCreateDialog(Bundle bundle) {
        if (this.a == null) {
            setShowsDialog(false);
        }
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.ac
    public void show(FragmentManager fragmentManager, String str) {
        super.show(fragmentManager, str);
    }
}
