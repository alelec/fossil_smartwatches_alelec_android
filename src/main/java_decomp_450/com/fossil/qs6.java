package com.fossil;

import com.portfolio.platform.data.model.PermissionData;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qs6 {
    @DexIgnore
    public /* final */ ns6 a;
    @DexIgnore
    public /* final */ List<PermissionData> b;

    @DexIgnore
    public qs6(ns6 ns6, List<PermissionData> list) {
        ee7.b(ns6, "mView");
        ee7.b(list, "mListPerms");
        this.a = ns6;
        this.b = list;
    }

    @DexIgnore
    public final List<PermissionData> a() {
        return this.b;
    }

    @DexIgnore
    public final ns6 b() {
        return this.a;
    }
}
