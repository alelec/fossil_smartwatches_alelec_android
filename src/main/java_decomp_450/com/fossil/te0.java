package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class te0 extends re0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<te0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public te0 createFromParcel(Parcel parcel) {
            return new te0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public te0[] newArray(int i) {
            return new te0[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public te0 m74createFromParcel(Parcel parcel) {
            return new te0(parcel, (zd7) null);
        }
    }

    @DexIgnore
    public /* synthetic */ te0(Parcel parcel, zd7 zd7) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.c = readString;
            this.d = parcel.readInt();
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.re0, com.fossil.k60
    public JSONObject a() {
        JSONObject put = super.a().put("loc", this.c).put("utc", this.d);
        ee7.a((Object) put, "super.toJSONObject()\n   \u2026.UTC, utcOffsetInMinutes)");
        return put;
    }

    @DexIgnore
    @Override // com.fossil.re0
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.re0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(te0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            te0 te0 = (te0) obj;
            return !(ee7.a(this.c, te0.c) ^ true) && this.d == te0.d;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    public final String getLocation() {
        return this.c;
    }

    @DexIgnore
    public final int getUtcOffsetInMinutes() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.re0
    public int hashCode() {
        return ((this.c.hashCode() + (super.hashCode() * 31)) * 31) + this.d;
    }

    @DexIgnore
    @Override // com.fossil.re0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }

    @DexIgnore
    public te0(String str, int i) {
        super(kr0.SECOND_TIMEZONE, false, 2);
        this.c = str;
        this.d = i;
    }
}
