package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba1 extends fe7 implements gd7<v81, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ vv0 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ba1(vv0 vv0) {
        super(1);
        this.a = vv0;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public i97 invoke(v81 v81) {
        vv0 vv0 = this.a;
        long j = ((qc1) v81).L;
        vv0.I = 0;
        vv0.H = 0;
        vv0.J = 0;
        vv0.G = j;
        int i = (j > vv0.D ? 1 : (j == vv0.D ? 0 : -1));
        if (i > 0 || j <= 0) {
            vv0.G = 0;
            vv0.m();
        } else if (i == 0) {
            vv0.n();
        } else {
            vv0.H = j;
            vv0.o();
        }
        return i97.a;
    }
}
