package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm6 implements Factory<ym6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public zm6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static zm6 a(Provider<ThemeRepository> provider) {
        return new zm6(provider);
    }

    @DexIgnore
    public static ym6 a(ThemeRepository themeRepository) {
        return new ym6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ym6 get() {
        return a(this.a.get());
    }
}
