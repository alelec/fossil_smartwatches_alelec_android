package com.fossil;

import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g21 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ri1 a;
    @DexIgnore
    public /* final */ /* synthetic */ x71 b;
    @DexIgnore
    public /* final */ /* synthetic */ UUID[] c;
    @DexIgnore
    public /* final */ /* synthetic */ qk1[] d;

    @DexIgnore
    public g21(ri1 ri1, x71 x71, UUID[] uuidArr, qk1[] qk1Arr) {
        this.a = ri1;
        this.b = x71;
        this.c = uuidArr;
        this.d = qk1Arr;
    }

    @DexIgnore
    public final void run() {
        this.a.w.g.c();
        this.a.w.g.a(new p01(this.b, this.c, this.d));
    }
}
