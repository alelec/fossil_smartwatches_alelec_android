package com.fossil;

import android.os.DeadObjectException;
import com.fossil.r12;
import com.fossil.u12;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u42<A extends r12<? extends i12, v02.b>> extends i32 {
    @DexIgnore
    public /* final */ A b;

    @DexIgnore
    public u42(int i, A a) {
        super(i);
        this.b = a;
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(u12.a<?> aVar) throws DeadObjectException {
        try {
            this.b.b(aVar.r());
        } catch (RuntimeException e) {
            a(e);
        }
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(Status status) {
        this.b.c(status);
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(Exception exc) {
        String simpleName = exc.getClass().getSimpleName();
        String localizedMessage = exc.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.b.c(new Status(10, sb.toString()));
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final void a(j22 j22, boolean z) {
        j22.a(this.b, z);
    }
}
