package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.GraphRequest;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zx6 implements k37 {
    @DexIgnore
    public static volatile zx6 d;
    @DexIgnore
    public a a;
    @DexIgnore
    public j37 b;
    @DexIgnore
    public String c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a();

        @DexIgnore
        void a(String str);

        @DexIgnore
        void b();

        @DexIgnore
        void c();
    }

    @DexIgnore
    public static synchronized zx6 a() {
        zx6 zx6;
        synchronized (zx6.class) {
            if (d == null) {
                synchronized (zx6.class) {
                    if (d == null) {
                        d = new zx6();
                    }
                }
            }
            zx6 = d;
        }
        return zx6;
    }

    @DexIgnore
    @Override // com.fossil.k37
    public void a(u27 u27) {
    }

    @DexIgnore
    public final void b(v27 v27) {
        if (v27.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize canceled!");
            a aVar = this.a;
            if (aVar != null) {
                aVar.c();
            }
        }
    }

    @DexIgnore
    public final void c(v27 v27) {
        if (v27.a() == 1) {
            FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat authorize denied!");
            a aVar = this.a;
            if (aVar != null) {
                aVar.a();
            }
        }
    }

    @DexIgnore
    public final void d(v27 v27) {
        if (v27.a() == 1) {
            e37 e37 = (e37) v27;
            if (e37.d.equals("com.fossil.wearables.fossil.tag_wechat_login")) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = GraphRequest.TAG;
                local.d(str, "Wechat authorize succeed, data = " + e37.c + ", openid: " + ((v27) e37).b);
                a aVar = this.a;
                if (aVar != null) {
                    aVar.a(e37.c);
                }
            }
        }
    }

    @DexIgnore
    public void a(String str) {
        this.c = str;
    }

    @DexIgnore
    public void a(Context context) {
        j37 a2 = m37.a(context, this.c, false);
        this.b = a2;
        a2.a(this.c);
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, initialize");
    }

    @DexIgnore
    public void a(a aVar) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, login...");
        this.a = aVar;
        if (!this.b.a()) {
            this.a.b();
            return;
        }
        d37 d37 = new d37();
        d37.c = "snsapi_userinfo";
        d37.d = "com.fossil.wearables.fossil.tag_wechat_login";
        if (!this.b.a(d37)) {
            this.a.c();
        }
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, start authorize...");
    }

    @DexIgnore
    @Override // com.fossil.k37
    public void a(v27 v27) {
        FLogger.INSTANCE.getLocal().d(GraphRequest.TAG, "Wechat, onResp...");
        int i = v27.a;
        if (i == -4) {
            c(v27);
        } else if (i == -2) {
            b(v27);
        } else if (i != 0) {
            a aVar = this.a;
            if (aVar != null) {
                aVar.a();
            }
        } else {
            d(v27);
        }
    }

    @DexIgnore
    public void a(Intent intent, k37 k37) {
        j37 j37 = this.b;
        if (j37 != null) {
            j37.a(intent, k37);
        }
    }
}
