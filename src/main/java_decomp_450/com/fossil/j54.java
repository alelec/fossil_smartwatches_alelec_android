package com.fossil;

import com.fossil.v54;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j54 extends v54.d.AbstractC0206d {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.a c;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.c d;
    @DexIgnore
    public /* final */ v54.d.AbstractC0206d.AbstractC0217d e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends v54.d.AbstractC0206d.b {
        @DexIgnore
        public Long a;
        @DexIgnore
        public String b;
        @DexIgnore
        public v54.d.AbstractC0206d.a c;
        @DexIgnore
        public v54.d.AbstractC0206d.c d;
        @DexIgnore
        public v54.d.AbstractC0206d.AbstractC0217d e;

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d.b a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d.b a(String str) {
            if (str != null) {
                this.b = str;
                return this;
            }
            throw new NullPointerException("Null type");
        }

        @DexIgnore
        public b(v54.d.AbstractC0206d dVar) {
            this.a = Long.valueOf(dVar.d());
            this.b = dVar.e();
            this.c = dVar.a();
            this.d = dVar.b();
            this.e = dVar.c();
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d.b a(v54.d.AbstractC0206d.a aVar) {
            if (aVar != null) {
                this.c = aVar;
                return this;
            }
            throw new NullPointerException("Null app");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d.b a(v54.d.AbstractC0206d.c cVar) {
            if (cVar != null) {
                this.d = cVar;
                return this;
            }
            throw new NullPointerException("Null device");
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d.b a(v54.d.AbstractC0206d.AbstractC0217d dVar) {
            this.e = dVar;
            return this;
        }

        @DexIgnore
        @Override // com.fossil.v54.d.AbstractC0206d.b
        public v54.d.AbstractC0206d a() {
            String str = "";
            if (this.a == null) {
                str = str + " timestamp";
            }
            if (this.b == null) {
                str = str + " type";
            }
            if (this.c == null) {
                str = str + " app";
            }
            if (this.d == null) {
                str = str + " device";
            }
            if (str.isEmpty()) {
                return new j54(this.a.longValue(), this.b, this.c, this.d, this.e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public v54.d.AbstractC0206d.a a() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public v54.d.AbstractC0206d.c b() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public v54.d.AbstractC0206d.AbstractC0217d c() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public long d() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public String e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof v54.d.AbstractC0206d)) {
            return false;
        }
        v54.d.AbstractC0206d dVar = (v54.d.AbstractC0206d) obj;
        if (this.a == dVar.d() && this.b.equals(dVar.e()) && this.c.equals(dVar.a()) && this.d.equals(dVar.b())) {
            v54.d.AbstractC0206d.AbstractC0217d dVar2 = this.e;
            if (dVar2 == null) {
                if (dVar.c() == null) {
                    return true;
                }
            } else if (dVar2.equals(dVar.c())) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.v54.d.AbstractC0206d
    public v54.d.AbstractC0206d.b f() {
        return new b(this);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        int hashCode = (((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode()) * 1000003) ^ this.d.hashCode()) * 1000003;
        v54.d.AbstractC0206d.AbstractC0217d dVar = this.e;
        return (dVar == null ? 0 : dVar.hashCode()) ^ hashCode;
    }

    @DexIgnore
    public String toString() {
        return "Event{timestamp=" + this.a + ", type=" + this.b + ", app=" + this.c + ", device=" + this.d + ", log=" + this.e + "}";
    }

    @DexIgnore
    public j54(long j, String str, v54.d.AbstractC0206d.a aVar, v54.d.AbstractC0206d.c cVar, v54.d.AbstractC0206d.AbstractC0217d dVar) {
        this.a = j;
        this.b = str;
        this.c = aVar;
        this.d = cVar;
        this.e = dVar;
    }
}
