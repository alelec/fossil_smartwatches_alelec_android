package com.fossil;

import android.os.Bundle;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zf6 extends cl4 {
    @DexIgnore
    public abstract void a(Bundle bundle);

    @DexIgnore
    public abstract void a(Date date);

    @DexIgnore
    public abstract void b(Date date);

    @DexIgnore
    public abstract ob5 h();

    @DexIgnore
    public abstract rd i();

    @DexIgnore
    public abstract void j();

    @DexIgnore
    public abstract void k();

    @DexIgnore
    public abstract void l();
}
