package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo1 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.FILE_CONFIG}));
    @DexIgnore
    public long D;
    @DexIgnore
    public long E;
    @DexIgnore
    public long F;
    @DexIgnore
    public /* final */ long G; // = yz0.b(this.H.e.length);
    @DexIgnore
    public /* final */ bi1 H;

    @DexIgnore
    public vo1(ri1 ri1, en0 en0, bi1 bi1, String str) {
        super(ri1, en0, wm0.M, str, false, 16);
        this.H = bi1;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public v81 a(qa1 qa1) {
        if (yk1.a[qa1.ordinal()] != 1) {
            return null;
        }
        long j = this.E;
        vu0 vu0 = new vu0(j, this.F - j, this.H.c(), ((zk0) this).w, 0, 16);
        vu0.b(new wm1(this));
        return vu0;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return Long.valueOf(this.D);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        this.E = 0;
        long b = yz0.b(this.H.e.length);
        this.F = b;
        if (this.E == b) {
            a(eu0.a(((zk0) this).v, null, is0.SUCCESS, null, 5));
        } else {
            zk0.a(this, qa1.o, null, 2, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(super.i(), r51.g2, this.H.a(false));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        return yz0.a(super.k(), r51.f2, Long.valueOf(this.D));
    }
}
