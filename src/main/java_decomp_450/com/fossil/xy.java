package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xy {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Handler b; // = new Handler(Looper.getMainLooper(), new a());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Handler.Callback {
        @DexIgnore
        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((uy) message.obj).b();
            return true;
        }
    }

    @DexIgnore
    public synchronized void a(uy<?> uyVar, boolean z) {
        if (!this.a) {
            if (!z) {
                this.a = true;
                uyVar.b();
                this.a = false;
            }
        }
        this.b.obtainMessage(1, uyVar).sendToTarget();
    }
}
