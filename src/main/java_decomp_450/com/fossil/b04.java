package com.fossil;

import com.google.errorprone.annotations.concurrent.LazyInit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b04<E> extends iy3<E> {
    @DexIgnore
    @LazyInit
    public transient int b;
    @DexIgnore
    public /* final */ transient E element;

    @DexIgnore
    public b04(E e) {
        jw3.a(e);
        this.element = e;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean contains(Object obj) {
        return this.element.equals(obj);
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public int copyIntoArray(Object[] objArr, int i) {
        objArr[i] = this.element;
        return i + 1;
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public zx3<E> createAsList() {
        return zx3.of(this.element);
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public final int hashCode() {
        int i = this.b;
        if (i != 0) {
            return i;
        }
        int hashCode = this.element.hashCode();
        this.b = hashCode;
        return hashCode;
    }

    @DexIgnore
    @Override // com.fossil.iy3
    public boolean isHashCodeFast() {
        return this.b != 0;
    }

    @DexIgnore
    @Override // com.fossil.vx3
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return 1;
    }

    @DexIgnore
    public String toString() {
        return '[' + this.element.toString() + ']';
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.vx3, com.fossil.vx3, com.fossil.iy3, com.fossil.iy3, java.util.Collection, java.util.Set, java.lang.Iterable
    public j04<E> iterator() {
        return qy3.a((Object) this.element);
    }

    @DexIgnore
    public b04(E e, int i) {
        this.element = e;
        this.b = i;
    }
}
