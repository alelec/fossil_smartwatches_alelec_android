package com.fossil;

import android.media.RemoteController;
import android.view.KeyEvent;
import com.facebook.internal.AnalyticsEvents;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class kk5 extends hk5 {
    @DexIgnore
    public /* final */ String c; // = "OldMusicController";
    @DexIgnore
    public /* final */ RemoteController.OnClientUpdateListener d;
    @DexIgnore
    public /* final */ RemoteController e;
    @DexIgnore
    public int f;
    @DexIgnore
    public ik5 g;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public kk5(String str) {
        super(str, "All apps");
        ee7.b(str, "appName");
        this.d = new a(this, str);
        this.g = new ik5(c(), str, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
        this.e = new RemoteController(PortfolioApp.g0.c(), this.d);
    }

    @DexIgnore
    public final int a(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 4;
            case 5:
                return 5;
            case 6:
                return 10;
            case 7:
                return 9;
            case 8:
                return 6;
            case 9:
                return 7;
            default:
                return 0;
        }
    }

    @DexIgnore
    public void a(int i, int i2, hk5 hk5) {
        ee7.b(hk5, "controller");
    }

    @DexIgnore
    public void a(ik5 ik5, ik5 ik52) {
        ee7.b(ik5, "oldMetadata");
        ee7.b(ik52, "newMetadata");
    }

    @DexIgnore
    public final void b(int i) {
        this.f = i;
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public int d() {
        return this.f;
    }

    @DexIgnore
    public final ik5 e() {
        return this.g;
    }

    @DexIgnore
    public final int f() {
        return this.f;
    }

    @DexIgnore
    public final void a(ik5 ik5) {
        ee7.b(ik5, "<set-?>");
        this.g = ik5;
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public ik5 b() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.hk5
    public boolean a(KeyEvent keyEvent) {
        ee7.b(keyEvent, "keyEvent");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.c;
        local.d(str, ".dispatchMediaButtonEvent of " + c() + " keyEvent " + keyEvent);
        return this.e.sendMediaKeyEvent(keyEvent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements RemoteController.OnClientUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ kk5 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public a(kk5 kk5, String str) {
            this.a = kk5;
            this.b = str;
        }

        @DexIgnore
        public void onClientChange(boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            local.d(a2, "SystemCallback of " + this.b + " - onClientChange clearing=" + z);
        }

        @DexIgnore
        public void onClientMetadataUpdate(RemoteController.MetadataEditor metadataEditor) {
            if (metadataEditor != null) {
                String a2 = vc5.a(metadataEditor.getString(7, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a3 = vc5.a(metadataEditor.getString(2, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                String a4 = vc5.a(metadataEditor.getString(1, AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a5 = this.a.c;
                local.d(a5, "SystemCallback of " + this.b + " - onMetadataChanged, title=" + a2 + ", artist=" + a3 + ", album=" + a4);
                ik5 ik5 = new ik5(this.a.c(), this.b, a2, a3, a4);
                if (true ^ ee7.a(ik5, this.a.e())) {
                    ik5 e = this.a.e();
                    this.a.a(ik5);
                    this.a.a(e, ik5);
                }
            }
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i) {
            int a2 = this.a.a(i);
            if (a2 != this.a.f()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = this.a.c;
                local.d(a3, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i);
                int f = this.a.f();
                this.a.b(a2);
                kk5 kk5 = this.a;
                kk5.a(f, a2, kk5);
            }
        }

        @DexIgnore
        public void onClientTransportControlUpdate(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            local.d(a2, "SystemCallback of " + this.b + " - onClientTransportControlUpdate(), transportControlFlags=" + i);
        }

        @DexIgnore
        public void onClientPlaybackStateUpdate(int i, long j, long j2, float f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = this.a.c;
            local.d(a2, "SystemCallback of " + this.b + " - onClientPlaybackStateUpdate(), state=" + i + ", stateChangeTimeMs=" + j + ", currentPosMs=" + j2 + ", speed=" + f);
        }
    }
}
