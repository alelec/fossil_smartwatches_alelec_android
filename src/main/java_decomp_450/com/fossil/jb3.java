package com.fossil;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.fossil.ap2;
import com.fossil.bp2;
import com.fossil.bw2;
import com.fossil.dp2;
import com.fossil.vp2;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.zo2;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jb3 extends yl3 {
    @DexIgnore
    public static /* final */ String[] f; // = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] g; // = {"origin", "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    @DexIgnore
    public static /* final */ String[] h; // = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;"};
    @DexIgnore
    public static /* final */ String[] i; // = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    @DexIgnore
    public static /* final */ String[] j; // = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    @DexIgnore
    public static /* final */ String[] k; // = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] l; // = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    @DexIgnore
    public static /* final */ String[] m; // = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};
    @DexIgnore
    public /* final */ kb3 d; // = new kb3(this, f(), "google_app_measurement.db");
    @DexIgnore
    public /* final */ ul3 e; // = new ul3(zzm());

    @DexIgnore
    public jb3(xl3 xl3) {
        super(xl3);
    }

    @DexIgnore
    public final boolean A() {
        return b("select count(1) > 0 from queue where has_realtime = 1", null) != 0;
    }

    @DexIgnore
    public final void B() {
        int delete;
        g();
        q();
        if (x()) {
            long a = k().h.a();
            long c = zzm().c();
            if (Math.abs(c - a) > wb3.y.a(null).longValue()) {
                k().h.a(c);
                g();
                q();
                if (x() && (delete = u().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(zzm().b()), String.valueOf(ym3.w())})) > 0) {
                    e().B().a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                }
            }
        }
    }

    @DexIgnore
    public final long C() {
        return a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @DexIgnore
    public final long D() {
        return a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    @DexIgnore
    public final boolean E() {
        return b("select count(1) > 0 from raw_events", null) != 0;
    }

    @DexIgnore
    public final boolean F() {
        return b("select count(1) > 0 from raw_events where realtime = 1", null) != 0;
    }

    @DexIgnore
    public final long a(String str, String[] strArr, long j2) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = u().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j3 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j3;
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            e().t().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final long b(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = u().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j2 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j2;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e2) {
            e().t().a("Database error", str, e2);
            throw e2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.gm3 c(java.lang.String r19, java.lang.String r20) {
        /*
            r18 = this;
            r8 = r20
            com.fossil.a72.b(r19)
            com.fossil.a72.b(r20)
            r18.g()
            r18.q()
            r9 = 0
            android.database.sqlite.SQLiteDatabase r10 = r18.u()     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            java.lang.String r11 = "user_attributes"
            java.lang.String r0 = "set_timestamp"
            java.lang.String r1 = "value"
            java.lang.String r2 = "origin"
            java.lang.String[] r12 = new java.lang.String[]{r0, r1, r2}     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            java.lang.String r13 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r14 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            r1 = 0
            r14[r1] = r19     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            r2 = 1
            r14[r2] = r8     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            r15 = 0
            r16 = 0
            r17 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x0082, all -> 0x007e }
            boolean r3 = r10.moveToFirst()     // Catch:{ SQLiteException -> 0x007a, all -> 0x0076 }
            if (r3 != 0) goto L_0x003f
            if (r10 == 0) goto L_0x003e
            r10.close()
        L_0x003e:
            return r9
        L_0x003f:
            long r5 = r10.getLong(r1)
            r11 = r18
            java.lang.Object r7 = r11.a(r10, r2)     // Catch:{ SQLiteException -> 0x0074 }
            java.lang.String r3 = r10.getString(r0)     // Catch:{ SQLiteException -> 0x0074 }
            com.fossil.gm3 r0 = new com.fossil.gm3     // Catch:{ SQLiteException -> 0x0074 }
            r1 = r0
            r2 = r19
            r4 = r20
            r1.<init>(r2, r3, r4, r5, r7)     // Catch:{ SQLiteException -> 0x0074 }
            boolean r1 = r10.moveToNext()     // Catch:{ SQLiteException -> 0x0074 }
            if (r1 == 0) goto L_0x006e
            com.fossil.jg3 r1 = r18.e()     // Catch:{ SQLiteException -> 0x0074 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ SQLiteException -> 0x0074 }
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r19)     // Catch:{ SQLiteException -> 0x0074 }
            r1.a(r2, r3)     // Catch:{ SQLiteException -> 0x0074 }
        L_0x006e:
            if (r10 == 0) goto L_0x0073
            r10.close()
        L_0x0073:
            return r0
        L_0x0074:
            r0 = move-exception
            goto L_0x0086
        L_0x0076:
            r0 = move-exception
            r11 = r18
            goto L_0x00a6
        L_0x007a:
            r0 = move-exception
            r11 = r18
            goto L_0x0086
        L_0x007e:
            r0 = move-exception
            r11 = r18
            goto L_0x00a7
        L_0x0082:
            r0 = move-exception
            r11 = r18
            r10 = r9
        L_0x0086:
            com.fossil.jg3 r1 = r18.e()     // Catch:{ all -> 0x00a5 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x00a5 }
            java.lang.String r2 = "Error querying user property. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r19)     // Catch:{ all -> 0x00a5 }
            com.fossil.hg3 r4 = r18.i()     // Catch:{ all -> 0x00a5 }
            java.lang.String r4 = r4.c(r8)     // Catch:{ all -> 0x00a5 }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x00a5 }
            if (r10 == 0) goto L_0x00a4
            r10.close()
        L_0x00a4:
            return r9
        L_0x00a5:
            r0 = move-exception
        L_0x00a6:
            r9 = r10
        L_0x00a7:
            if (r9 == 0) goto L_0x00ac
            r9.close()
        L_0x00ac:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.c(java.lang.String, java.lang.String):com.fossil.gm3");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x011d  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0125  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.wm3 d(java.lang.String r30, java.lang.String r31) {
        /*
            r29 = this;
            r7 = r31
            com.fossil.a72.b(r30)
            com.fossil.a72.b(r31)
            r29.g()
            r29.q()
            r8 = 0
            android.database.sqlite.SQLiteDatabase r9 = r29.u()     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            java.lang.String r10 = "conditional_properties"
            java.lang.String r11 = "origin"
            java.lang.String r12 = "value"
            java.lang.String r13 = "active"
            java.lang.String r14 = "trigger_event_name"
            java.lang.String r15 = "trigger_timeout"
            java.lang.String r16 = "timed_out_event"
            java.lang.String r17 = "creation_timestamp"
            java.lang.String r18 = "triggered_event"
            java.lang.String r19 = "triggered_timestamp"
            java.lang.String r20 = "time_to_live"
            java.lang.String r21 = "expired_event"
            java.lang.String[] r11 = new java.lang.String[]{r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21}     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            java.lang.String r12 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r13 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            r1 = 0
            r13[r1] = r30     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            r2 = 1
            r13[r2] = r7     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            r14 = 0
            r15 = 0
            r16 = 0
            android.database.Cursor r9 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ SQLiteException -> 0x00fe, all -> 0x00fa }
            boolean r3 = r9.moveToFirst()     // Catch:{ SQLiteException -> 0x00f6, all -> 0x00f2 }
            if (r3 != 0) goto L_0x004e
            if (r9 == 0) goto L_0x004d
            r9.close()
        L_0x004d:
            return r8
        L_0x004e:
            java.lang.String r16 = r9.getString(r1)
            r10 = r29
            java.lang.Object r5 = r10.a(r9, r2)     // Catch:{ SQLiteException -> 0x00f0 }
            int r0 = r9.getInt(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            if (r0 == 0) goto L_0x0061
            r20 = 1
            goto L_0x0063
        L_0x0061:
            r20 = 0
        L_0x0063:
            r0 = 3
            java.lang.String r21 = r9.getString(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            r0 = 4
            long r23 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.fm3 r0 = r29.m()     // Catch:{ SQLiteException -> 0x00f0 }
            r1 = 5
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable$Creator<com.fossil.ub3> r2 = com.fossil.ub3.CREATOR     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00f0 }
            r22 = r0
            com.fossil.ub3 r22 = (com.fossil.ub3) r22     // Catch:{ SQLiteException -> 0x00f0 }
            r0 = 6
            long r18 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.fm3 r0 = r29.m()     // Catch:{ SQLiteException -> 0x00f0 }
            r1 = 7
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable$Creator<com.fossil.ub3> r2 = com.fossil.ub3.CREATOR     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00f0 }
            r25 = r0
            com.fossil.ub3 r25 = (com.fossil.ub3) r25     // Catch:{ SQLiteException -> 0x00f0 }
            r0 = 8
            long r3 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            r0 = 9
            long r26 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.fm3 r0 = r29.m()     // Catch:{ SQLiteException -> 0x00f0 }
            r1 = 10
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable$Creator<com.fossil.ub3> r2 = com.fossil.ub3.CREATOR     // Catch:{ SQLiteException -> 0x00f0 }
            android.os.Parcelable r0 = r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x00f0 }
            r28 = r0
            com.fossil.ub3 r28 = (com.fossil.ub3) r28     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.em3 r17 = new com.fossil.em3     // Catch:{ SQLiteException -> 0x00f0 }
            r1 = r17
            r2 = r31
            r6 = r16
            r1.<init>(r2, r3, r5, r6)     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.wm3 r0 = new com.fossil.wm3     // Catch:{ SQLiteException -> 0x00f0 }
            r14 = r0
            r15 = r30
            r14.<init>(r15, r16, r17, r18, r20, r21, r22, r23, r25, r26, r28)     // Catch:{ SQLiteException -> 0x00f0 }
            boolean r1 = r9.moveToNext()     // Catch:{ SQLiteException -> 0x00f0 }
            if (r1 == 0) goto L_0x00ea
            com.fossil.jg3 r1 = r29.e()     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ SQLiteException -> 0x00f0 }
            java.lang.String r2 = "Got multiple records for conditional property, expected one"
            java.lang.Object r3 = com.fossil.jg3.a(r30)     // Catch:{ SQLiteException -> 0x00f0 }
            com.fossil.hg3 r4 = r29.i()     // Catch:{ SQLiteException -> 0x00f0 }
            java.lang.String r4 = r4.c(r7)     // Catch:{ SQLiteException -> 0x00f0 }
            r1.a(r2, r3, r4)     // Catch:{ SQLiteException -> 0x00f0 }
        L_0x00ea:
            if (r9 == 0) goto L_0x00ef
            r9.close()
        L_0x00ef:
            return r0
        L_0x00f0:
            r0 = move-exception
            goto L_0x0102
        L_0x00f2:
            r0 = move-exception
            r10 = r29
            goto L_0x0122
        L_0x00f6:
            r0 = move-exception
            r10 = r29
            goto L_0x0102
        L_0x00fa:
            r0 = move-exception
            r10 = r29
            goto L_0x0123
        L_0x00fe:
            r0 = move-exception
            r10 = r29
            r9 = r8
        L_0x0102:
            com.fossil.jg3 r1 = r29.e()     // Catch:{ all -> 0x0121 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x0121 }
            java.lang.String r2 = "Error querying conditional property"
            java.lang.Object r3 = com.fossil.jg3.a(r30)     // Catch:{ all -> 0x0121 }
            com.fossil.hg3 r4 = r29.i()     // Catch:{ all -> 0x0121 }
            java.lang.String r4 = r4.c(r7)     // Catch:{ all -> 0x0121 }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x0121 }
            if (r9 == 0) goto L_0x0120
            r9.close()
        L_0x0120:
            return r8
        L_0x0121:
            r0 = move-exception
        L_0x0122:
            r8 = r9
        L_0x0123:
            if (r8 == 0) goto L_0x0128
            r8.close()
        L_0x0128:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.d(java.lang.String, java.lang.String):com.fossil.wm3");
    }

    @DexIgnore
    public final int e(String str, String str2) {
        a72.b(str);
        a72.b(str2);
        g();
        q();
        try {
            return u().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            e().t().a("Error deleting conditional property", jg3.a(str), i().c(str2), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.fossil.ap2>> f(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.q()
            r12.g()
            com.fossil.a72.b(r13)
            com.fossil.a72.b(r14)
            com.fossil.n4 r0 = new com.fossil.n4
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.u()
            r9 = 0
            java.lang.String r2 = "event_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            java.lang.String r4 = "app_id=? AND event_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0095 }
            if (r14 == 0) goto L_0x0041
            r14.close()
        L_0x0041:
            return r13
        L_0x0042:
            byte[] r1 = r14.getBlob(r11)
            com.fossil.ap2$a r2 = com.fossil.ap2.z()     // Catch:{ IOException -> 0x0077 }
            com.fossil.fm3.a(r2, r1)     // Catch:{ IOException -> 0x0077 }
            com.fossil.ap2$a r2 = (com.fossil.ap2.a) r2     // Catch:{ IOException -> 0x0077 }
            com.fossil.jx2 r1 = r2.g()     // Catch:{ IOException -> 0x0077 }
            com.fossil.bw2 r1 = (com.fossil.bw2) r1     // Catch:{ IOException -> 0x0077 }
            com.fossil.ap2 r1 = (com.fossil.ap2) r1     // Catch:{ IOException -> 0x0077 }
            int r2 = r14.getInt(r10)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.Object r3 = r0.get(r3)
            java.util.List r3 = (java.util.List) r3
            if (r3 != 0) goto L_0x0073
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.put(r2, r3)
        L_0x0073:
            r3.add(r1)
            goto L_0x0089
        L_0x0077:
            r1 = move-exception
            com.fossil.jg3 r2 = r12.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = com.fossil.jg3.a(r13)
            r2.a(r3, r4, r1)
        L_0x0089:
            boolean r1 = r14.moveToNext()
            if (r1 != 0) goto L_0x0042
            if (r14 == 0) goto L_0x0094
            r14.close()
        L_0x0094:
            return r0
        L_0x0095:
            r0 = move-exception
            goto L_0x009b
        L_0x0097:
            r13 = move-exception
            goto L_0x00d0
        L_0x0099:
            r0 = move-exception
            r14 = r9
        L_0x009b:
            com.fossil.jg3 r1 = r12.e()     // Catch:{ all -> 0x00ce }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x00ce }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r13)     // Catch:{ all -> 0x00ce }
            r1.a(r2, r3, r0)     // Catch:{ all -> 0x00ce }
            boolean r0 = com.fossil.n13.a()     // Catch:{ all -> 0x00ce }
            if (r0 == 0) goto L_0x00c8
            com.fossil.ym3 r0 = r12.l()     // Catch:{ all -> 0x00ce }
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.T0     // Catch:{ all -> 0x00ce }
            boolean r13 = r0.e(r13, r1)     // Catch:{ all -> 0x00ce }
            if (r13 == 0) goto L_0x00c8
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00ce }
            if (r14 == 0) goto L_0x00c7
            r14.close()
        L_0x00c7:
            return r13
        L_0x00c8:
            if (r14 == 0) goto L_0x00cd
            r14.close()
        L_0x00cd:
            return r9
        L_0x00ce:
            r13 = move-exception
            r9 = r14
        L_0x00d0:
            if (r9 == 0) goto L_0x00d5
            r9.close()
        L_0x00d5:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.f(java.lang.String, java.lang.String):java.util.Map");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.fossil.dp2>> g(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.q()
            r12.g()
            com.fossil.a72.b(r13)
            com.fossil.a72.b(r14)
            com.fossil.n4 r0 = new com.fossil.n4
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.u()
            r9 = 0
            java.lang.String r2 = "property_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            java.lang.String r4 = "app_id=? AND property_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0099, all -> 0x0097 }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0095 }
            if (r14 == 0) goto L_0x0041
            r14.close()
        L_0x0041:
            return r13
        L_0x0042:
            byte[] r1 = r14.getBlob(r11)
            com.fossil.dp2$a r2 = com.fossil.dp2.w()     // Catch:{ IOException -> 0x0077 }
            com.fossil.fm3.a(r2, r1)     // Catch:{ IOException -> 0x0077 }
            com.fossil.dp2$a r2 = (com.fossil.dp2.a) r2     // Catch:{ IOException -> 0x0077 }
            com.fossil.jx2 r1 = r2.g()     // Catch:{ IOException -> 0x0077 }
            com.fossil.bw2 r1 = (com.fossil.bw2) r1     // Catch:{ IOException -> 0x0077 }
            com.fossil.dp2 r1 = (com.fossil.dp2) r1     // Catch:{ IOException -> 0x0077 }
            int r2 = r14.getInt(r10)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            java.lang.Object r3 = r0.get(r3)
            java.util.List r3 = (java.util.List) r3
            if (r3 != 0) goto L_0x0073
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r0.put(r2, r3)
        L_0x0073:
            r3.add(r1)
            goto L_0x0089
        L_0x0077:
            r1 = move-exception
            com.fossil.jg3 r2 = r12.e()
            com.fossil.mg3 r2 = r2.t()
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = com.fossil.jg3.a(r13)
            r2.a(r3, r4, r1)
        L_0x0089:
            boolean r1 = r14.moveToNext()
            if (r1 != 0) goto L_0x0042
            if (r14 == 0) goto L_0x0094
            r14.close()
        L_0x0094:
            return r0
        L_0x0095:
            r0 = move-exception
            goto L_0x009b
        L_0x0097:
            r13 = move-exception
            goto L_0x00d0
        L_0x0099:
            r0 = move-exception
            r14 = r9
        L_0x009b:
            com.fossil.jg3 r1 = r12.e()     // Catch:{ all -> 0x00ce }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x00ce }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r13)     // Catch:{ all -> 0x00ce }
            r1.a(r2, r3, r0)     // Catch:{ all -> 0x00ce }
            boolean r0 = com.fossil.n13.a()     // Catch:{ all -> 0x00ce }
            if (r0 == 0) goto L_0x00c8
            com.fossil.ym3 r0 = r12.l()     // Catch:{ all -> 0x00ce }
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.T0     // Catch:{ all -> 0x00ce }
            boolean r13 = r0.e(r13, r1)     // Catch:{ all -> 0x00ce }
            if (r13 == 0) goto L_0x00c8
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00ce }
            if (r14 == 0) goto L_0x00c7
            r14.close()
        L_0x00c7:
            return r13
        L_0x00c8:
            if (r14 == 0) goto L_0x00cd
            r14.close()
        L_0x00cd:
            return r9
        L_0x00ce:
            r13 = move-exception
            r9 = r14
        L_0x00d0:
            if (r9 == 0) goto L_0x00d5
            r9.close()
        L_0x00d5:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.g(java.lang.String, java.lang.String):java.util.Map");
    }

    @DexIgnore
    public final long h(String str, String str2) {
        long a;
        a72.b(str);
        a72.b(str2);
        g();
        q();
        SQLiteDatabase u = u();
        u.beginTransaction();
        long j2 = 0;
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str2);
            sb.append(" from app2 where app_id=?");
            try {
                a = a(sb.toString(), new String[]{str}, -1);
                if (a == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str);
                    contentValues.put("first_open_count", (Integer) 0);
                    contentValues.put("previous_install_count", (Integer) 0);
                    if (u.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                        e().t().a("Failed to insert column (got -1). appId", jg3.a(str), str2);
                        u.endTransaction();
                        return -1;
                    }
                    a = 0;
                }
            } catch (SQLiteException e2) {
                e = e2;
                try {
                    e().t().a("Error inserting column. appId", jg3.a(str), str2, e);
                    u.endTransaction();
                    return j2;
                } catch (Throwable th) {
                    th = th;
                    u.endTransaction();
                    throw th;
                }
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str);
                contentValues2.put(str2, Long.valueOf(1 + a));
                if (((long) u.update("app2", contentValues2, "app_id = ?", new String[]{str})) == 0) {
                    e().t().a("Failed to update column (got 0). appId", jg3.a(str), str2);
                    u.endTransaction();
                    return -1;
                }
                u.setTransactionSuccessful();
                u.endTransaction();
                return a;
            } catch (SQLiteException e3) {
                e = e3;
                j2 = a;
                e().t().a("Error inserting column. appId", jg3.a(str), str2, e);
                u.endTransaction();
                return j2;
            }
        } catch (SQLiteException e4) {
            e = e4;
            e().t().a("Error inserting column. appId", jg3.a(str), str2, e);
            u.endTransaction();
            return j2;
        } catch (Throwable th2) {
            th = th2;
            u.endTransaction();
            throw th;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00d0  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle i(java.lang.String r8) {
        /*
            r7 = this;
            r7.g()
            r7.q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.u()     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00bd }
            java.lang.String r2 = "select parameters from default_event_params where app_id=?"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00bd }
            r4 = 0
            r3[r4] = r8     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00bd }
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x00bf, all -> 0x00bd }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00bb }
            if (r2 != 0) goto L_0x0030
            com.fossil.jg3 r8 = r7.e()     // Catch:{ SQLiteException -> 0x00bb }
            com.fossil.mg3 r8 = r8.B()     // Catch:{ SQLiteException -> 0x00bb }
            java.lang.String r2 = "Default event parameters not found"
            r8.a(r2)     // Catch:{ SQLiteException -> 0x00bb }
            if (r1 == 0) goto L_0x002f
            r1.close()
        L_0x002f:
            return r0
        L_0x0030:
            byte[] r2 = r1.getBlob(r4)
            com.fossil.rp2$a r3 = com.fossil.rp2.z()     // Catch:{ IOException -> 0x00a3 }
            com.fossil.fm3.a(r3, r2)     // Catch:{ IOException -> 0x00a3 }
            com.fossil.rp2$a r3 = (com.fossil.rp2.a) r3     // Catch:{ IOException -> 0x00a3 }
            com.fossil.jx2 r2 = r3.g()     // Catch:{ IOException -> 0x00a3 }
            com.fossil.bw2 r2 = (com.fossil.bw2) r2     // Catch:{ IOException -> 0x00a3 }
            com.fossil.rp2 r2 = (com.fossil.rp2) r2     // Catch:{ IOException -> 0x00a3 }
            r7.m()
            java.util.List r8 = r2.zza()
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.util.Iterator r8 = r8.iterator()
        L_0x0055:
            boolean r3 = r8.hasNext()
            if (r3 == 0) goto L_0x009d
            java.lang.Object r3 = r8.next()
            com.fossil.tp2 r3 = (com.fossil.tp2) r3
            java.lang.String r4 = r3.p()
            boolean r5 = r3.w()
            if (r5 == 0) goto L_0x0073
            double r5 = r3.x()
            r2.putDouble(r4, r5)
            goto L_0x0055
        L_0x0073:
            boolean r5 = r3.u()
            if (r5 == 0) goto L_0x0081
            float r3 = r3.v()
            r2.putFloat(r4, r3)
            goto L_0x0055
        L_0x0081:
            boolean r5 = r3.q()
            if (r5 == 0) goto L_0x008f
            java.lang.String r3 = r3.r()
            r2.putString(r4, r3)
            goto L_0x0055
        L_0x008f:
            boolean r5 = r3.s()
            if (r5 == 0) goto L_0x0055
            long r5 = r3.t()
            r2.putLong(r4, r5)
            goto L_0x0055
        L_0x009d:
            if (r1 == 0) goto L_0x00a2
            r1.close()
        L_0x00a2:
            return r2
        L_0x00a3:
            r2 = move-exception
            com.fossil.jg3 r3 = r7.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r4 = "Failed to retrieve default event parameters. appId"
            java.lang.Object r8 = com.fossil.jg3.a(r8)
            r3.a(r4, r8, r2)
            if (r1 == 0) goto L_0x00ba
            r1.close()
        L_0x00ba:
            return r0
        L_0x00bb:
            r8 = move-exception
            goto L_0x00c1
        L_0x00bd:
            r8 = move-exception
            goto L_0x00d6
        L_0x00bf:
            r8 = move-exception
            r1 = r0
        L_0x00c1:
            com.fossil.jg3 r2 = r7.e()     // Catch:{ all -> 0x00d4 }
            com.fossil.mg3 r2 = r2.t()     // Catch:{ all -> 0x00d4 }
            java.lang.String r3 = "Error selecting default event parameters"
            r2.a(r3, r8)     // Catch:{ all -> 0x00d4 }
            if (r1 == 0) goto L_0x00d3
            r1.close()
        L_0x00d3:
            return r0
        L_0x00d4:
            r8 = move-exception
            r0 = r1
        L_0x00d6:
            if (r0 == 0) goto L_0x00db
            r0.close()
        L_0x00db:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.i(java.lang.String):android.os.Bundle");
    }

    @DexIgnore
    @Override // com.fossil.yl3
    public final boolean s() {
        return false;
    }

    @DexIgnore
    public final void t() {
        q();
        u().setTransactionSuccessful();
    }

    @DexIgnore
    public final SQLiteDatabase u() {
        g();
        try {
            return this.d.getWritableDatabase();
        } catch (SQLiteException e2) {
            e().w().a("Error opening database", e2);
            throw e2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String v() {
        /*
            r6 = this;
            android.database.sqlite.SQLiteDatabase r0 = r6.u()
            r1 = 0
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch:{ SQLiteException -> 0x0029, all -> 0x0024 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0022 }
            if (r2 == 0) goto L_0x001c
            r2 = 0
            java.lang.String r1 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0022 }
            if (r0 == 0) goto L_0x001b
            r0.close()
        L_0x001b:
            return r1
        L_0x001c:
            if (r0 == 0) goto L_0x0021
            r0.close()
        L_0x0021:
            return r1
        L_0x0022:
            r2 = move-exception
            goto L_0x002b
        L_0x0024:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003f
        L_0x0029:
            r2 = move-exception
            r0 = r1
        L_0x002b:
            com.fossil.jg3 r3 = r6.e()     // Catch:{ all -> 0x003e }
            com.fossil.mg3 r3 = r3.t()     // Catch:{ all -> 0x003e }
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.a(r4, r2)     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            return r1
        L_0x003e:
            r1 = move-exception
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.v():java.lang.String");
    }

    @DexIgnore
    public final long w() {
        Cursor cursor = null;
        try {
            cursor = u().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            long j2 = cursor.getLong(0);
            if (cursor != null) {
                cursor.close();
            }
            return j2;
        } catch (SQLiteException e2) {
            e().t().a("Error querying raw events", e2);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean x() {
        return f().getDatabasePath("google_app_measurement.db").exists();
    }

    @DexIgnore
    public final void y() {
        q();
        u().beginTransaction();
    }

    @DexIgnore
    public final void z() {
        q();
        u().endTransaction();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0147  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x0150  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.qb3 a(java.lang.String r26, java.lang.String r27) {
        /*
            r25 = this;
            r15 = r27
            com.fossil.a72.b(r26)
            com.fossil.a72.b(r27)
            r25.g()
            r25.q()
            java.util.ArrayList r0 = new java.util.ArrayList
            java.lang.String r1 = "lifetime_count"
            java.lang.String r2 = "current_bundle_count"
            java.lang.String r3 = "last_fire_timestamp"
            java.lang.String r4 = "last_bundled_timestamp"
            java.lang.String r5 = "last_bundled_day"
            java.lang.String r6 = "last_sampled_complex_event_id"
            java.lang.String r7 = "last_sampling_rate"
            java.lang.String r8 = "last_exempt_from_sampling"
            java.lang.String r9 = "current_session_count"
            java.lang.String[] r1 = new java.lang.String[]{r1, r2, r3, r4, r5, r6, r7, r8, r9}
            java.util.List r1 = java.util.Arrays.asList(r1)
            r0.<init>(r1)
            r18 = 0
            android.database.sqlite.SQLiteDatabase r1 = r25.u()     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            java.lang.String r2 = "events"
            r9 = 0
            java.lang.String[] r3 = new java.lang.String[r9]     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            java.lang.Object[] r0 = r0.toArray(r3)     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            r3 = r0
            java.lang.String[] r3 = (java.lang.String[]) r3     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            java.lang.String r4 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r5 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            r5[r9] = r26     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            r10 = 1
            r5[r10] = r15     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0127, all -> 0x0125 }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x0121, all -> 0x011b }
            if (r1 != 0) goto L_0x005c
            if (r14 == 0) goto L_0x005b
            r14.close()
        L_0x005b:
            return r18
        L_0x005c:
            long r4 = r14.getLong(r9)
            long r6 = r14.getLong(r10)
            long r11 = r14.getLong(r0)
            r0 = 3
            boolean r1 = r14.isNull(r0)
            r2 = 0
            if (r1 == 0) goto L_0x0074
            r16 = r2
            goto L_0x007a
        L_0x0074:
            long r0 = r14.getLong(r0)
            r16 = r0
        L_0x007a:
            r0 = 4
            boolean r1 = r14.isNull(r0)
            if (r1 == 0) goto L_0x0084
            r0 = r18
            goto L_0x008c
        L_0x0084:
            long r0 = r14.getLong(r0)
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
        L_0x008c:
            r1 = 5
            boolean r8 = r14.isNull(r1)
            if (r8 == 0) goto L_0x0096
            r19 = r18
            goto L_0x00a0
        L_0x0096:
            long r19 = r14.getLong(r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r19)
            r19 = r1
        L_0x00a0:
            r1 = 6
            boolean r8 = r14.isNull(r1)
            if (r8 == 0) goto L_0x00aa
            r20 = r18
            goto L_0x00b4
        L_0x00aa:
            long r20 = r14.getLong(r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r20)
            r20 = r1
        L_0x00b4:
            r1 = 7
            boolean r8 = r14.isNull(r1)
            if (r8 != 0) goto L_0x00d0
            long r21 = r14.getLong(r1)     // Catch:{ SQLiteException -> 0x00cd }
            r23 = 1
            int r1 = (r21 > r23 ? 1 : (r21 == r23 ? 0 : -1))
            if (r1 != 0) goto L_0x00c6
            r9 = 1
        L_0x00c6:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r9)     // Catch:{ SQLiteException -> 0x00cd }
            r21 = r1
            goto L_0x00d2
        L_0x00cd:
            r0 = move-exception
            goto L_0x012a
        L_0x00d0:
            r21 = r18
        L_0x00d2:
            r1 = 8
            boolean r8 = r14.isNull(r1)
            if (r8 == 0) goto L_0x00dc
            r8 = r2
            goto L_0x00e1
        L_0x00dc:
            long r1 = r14.getLong(r1)
            r8 = r1
        L_0x00e1:
            com.fossil.qb3 r22 = new com.fossil.qb3
            r1 = r22
            r2 = r26
            r3 = r27
            r10 = r11
            r12 = r16
            r23 = r14
            r14 = r0
            r15 = r19
            r16 = r20
            r17 = r21
            r1.<init>(r2, r3, r4, r6, r8, r10, r12, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
            boolean r0 = r23.moveToNext()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
            if (r0 == 0) goto L_0x010f
            com.fossil.jg3 r0 = r25.e()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
            com.fossil.mg3 r0 = r0.t()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
            java.lang.String r1 = "Got multiple records for event aggregates, expected one. appId"
            java.lang.Object r2 = com.fossil.jg3.a(r26)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
            r0.a(r1, r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0115 }
        L_0x010f:
            if (r23 == 0) goto L_0x0114
            r23.close()
        L_0x0114:
            return r22
        L_0x0115:
            r0 = move-exception
            goto L_0x011e
        L_0x0117:
            r0 = move-exception
            r14 = r23
            goto L_0x012a
        L_0x011b:
            r0 = move-exception
            r23 = r14
        L_0x011e:
            r18 = r23
            goto L_0x014e
        L_0x0121:
            r0 = move-exception
            r23 = r14
            goto L_0x012a
        L_0x0125:
            r0 = move-exception
            goto L_0x014e
        L_0x0127:
            r0 = move-exception
            r14 = r18
        L_0x012a:
            com.fossil.jg3 r1 = r25.e()     // Catch:{ all -> 0x014b }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x014b }
            java.lang.String r2 = "Error querying events. appId"
            java.lang.Object r3 = com.fossil.jg3.a(r26)     // Catch:{ all -> 0x014b }
            com.fossil.hg3 r4 = r25.i()     // Catch:{ all -> 0x014b }
            r5 = r27
            java.lang.String r4 = r4.a(r5)     // Catch:{ all -> 0x014b }
            r1.a(r2, r3, r4, r0)     // Catch:{ all -> 0x014b }
            if (r14 == 0) goto L_0x014a
            r14.close()
        L_0x014a:
            return r18
        L_0x014b:
            r0 = move-exception
            r18 = r14
        L_0x014e:
            if (r18 == 0) goto L_0x0153
            r18.close()
        L_0x0153:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.a(java.lang.String, java.lang.String):com.fossil.qb3");
    }

    @DexIgnore
    public final void b(String str, String str2) {
        a72.b(str);
        a72.b(str2);
        g();
        q();
        try {
            u().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e2) {
            e().t().a("Error deleting user property. appId", jg3.a(str), i().c(str2), e2);
        }
    }

    @DexIgnore
    public final Map<Integer, List<ap2>> e(String str) {
        a72.b(str);
        n4 n4Var = new n4();
        Cursor cursor = null;
        try {
            Cursor query = u().query("event_filters", new String[]{"audience_id", "data"}, "app_id=?", new String[]{str}, null, null, null);
            if (!query.moveToFirst()) {
                Map<Integer, List<ap2>> emptyMap = Collections.emptyMap();
                if (query != null) {
                    query.close();
                }
                return emptyMap;
            }
            do {
                byte[] blob = query.getBlob(1);
                try {
                    ap2.a z = ap2.z();
                    fm3.a(z, blob);
                    ap2 ap2 = (ap2) ((bw2) z.g());
                    if (ap2.t()) {
                        int i2 = query.getInt(0);
                        List list = (List) n4Var.get(Integer.valueOf(i2));
                        if (list == null) {
                            list = new ArrayList();
                            n4Var.put(Integer.valueOf(i2), list);
                        }
                        list.add(ap2);
                    }
                } catch (IOException e2) {
                    e().t().a("Failed to merge filter. appId", jg3.a(str), e2);
                }
            } while (query.moveToNext());
            if (query != null) {
                query.close();
            }
            return n4Var;
        } catch (SQLiteException e3) {
            e().t().a("Database error querying filters. appId", jg3.a(str), e3);
            Map<Integer, List<ap2>> emptyMap2 = Collections.emptyMap();
            if (0 != 0) {
                cursor.close();
            }
            return emptyMap2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final List<wm3> b(String str, String str2, String str3) {
        a72.b(str);
        g();
        q();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat(vt7.ANY_MARKER));
            sb.append(" and name glob ?");
        }
        return a(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    @DexIgnore
    public final long c(String str) {
        a72.b(str);
        g();
        q();
        try {
            return (long) u().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, l().b(str, wb3.p))))});
        } catch (SQLiteException e2) {
            e().t().a("Error deleting over the limit events. appId", jg3.a(str), e2);
            return 0;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, com.fossil.xp2> g(java.lang.String r12) {
        /*
            r11 = this;
            r11.q()
            r11.g()
            com.fossil.a72.b(r12)
            android.database.sqlite.SQLiteDatabase r0 = r11.u()
            r8 = 0
            java.lang.String r1 = "audience_filter_values"
            java.lang.String r2 = "audience_id"
            java.lang.String r3 = "current_results"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch:{ SQLiteException -> 0x009b, all -> 0x0099 }
            java.lang.String r3 = "app_id=?"
            r9 = 1
            java.lang.String[] r4 = new java.lang.String[r9]     // Catch:{ SQLiteException -> 0x009b, all -> 0x0099 }
            r10 = 0
            r4[r10] = r12     // Catch:{ SQLiteException -> 0x009b, all -> 0x0099 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x009b, all -> 0x0099 }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0097 }
            if (r1 != 0) goto L_0x004f
            boolean r1 = com.fossil.n13.a()     // Catch:{ SQLiteException -> 0x0097 }
            if (r1 == 0) goto L_0x0049
            com.fossil.ym3 r1 = r11.l()     // Catch:{ SQLiteException -> 0x0097 }
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.T0     // Catch:{ SQLiteException -> 0x0097 }
            boolean r1 = r1.e(r12, r2)     // Catch:{ SQLiteException -> 0x0097 }
            if (r1 == 0) goto L_0x0049
            java.util.Map r12 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0097 }
            if (r0 == 0) goto L_0x0048
            r0.close()
        L_0x0048:
            return r12
        L_0x0049:
            if (r0 == 0) goto L_0x004e
            r0.close()
        L_0x004e:
            return r8
        L_0x004f:
            com.fossil.n4 r1 = new com.fossil.n4
            r1.<init>()
        L_0x0054:
            int r2 = r0.getInt(r10)
            byte[] r3 = r0.getBlob(r9)
            com.fossil.xp2$a r4 = com.fossil.xp2.A()     // Catch:{ IOException -> 0x0075 }
            com.fossil.fm3.a(r4, r3)     // Catch:{ IOException -> 0x0075 }
            com.fossil.xp2$a r4 = (com.fossil.xp2.a) r4     // Catch:{ IOException -> 0x0075 }
            com.fossil.jx2 r3 = r4.g()     // Catch:{ IOException -> 0x0075 }
            com.fossil.bw2 r3 = (com.fossil.bw2) r3     // Catch:{ IOException -> 0x0075 }
            com.fossil.xp2 r3 = (com.fossil.xp2) r3     // Catch:{ IOException -> 0x0075 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.put(r2, r3)
            goto L_0x008b
        L_0x0075:
            r3 = move-exception
            com.fossil.jg3 r4 = r11.e()
            com.fossil.mg3 r4 = r4.t()
            java.lang.String r5 = "Failed to merge filter results. appId, audienceId, error"
            java.lang.Object r6 = com.fossil.jg3.a(r12)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r4.a(r5, r6, r2, r3)
        L_0x008b:
            boolean r2 = r0.moveToNext()
            if (r2 != 0) goto L_0x0054
            if (r0 == 0) goto L_0x0096
            r0.close()
        L_0x0096:
            return r1
        L_0x0097:
            r1 = move-exception
            goto L_0x009d
        L_0x0099:
            r12 = move-exception
            goto L_0x00d2
        L_0x009b:
            r1 = move-exception
            r0 = r8
        L_0x009d:
            com.fossil.jg3 r2 = r11.e()     // Catch:{ all -> 0x00d0 }
            com.fossil.mg3 r2 = r2.t()     // Catch:{ all -> 0x00d0 }
            java.lang.String r3 = "Database error querying filter results. appId"
            java.lang.Object r4 = com.fossil.jg3.a(r12)     // Catch:{ all -> 0x00d0 }
            r2.a(r3, r4, r1)     // Catch:{ all -> 0x00d0 }
            boolean r1 = com.fossil.n13.a()     // Catch:{ all -> 0x00d0 }
            if (r1 == 0) goto L_0x00ca
            com.fossil.ym3 r1 = r11.l()     // Catch:{ all -> 0x00d0 }
            com.fossil.yf3<java.lang.Boolean> r2 = com.fossil.wb3.T0     // Catch:{ all -> 0x00d0 }
            boolean r12 = r1.e(r12, r2)     // Catch:{ all -> 0x00d0 }
            if (r12 == 0) goto L_0x00ca
            java.util.Map r12 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x00d0 }
            if (r0 == 0) goto L_0x00c9
            r0.close()
        L_0x00c9:
            return r12
        L_0x00ca:
            if (r0 == 0) goto L_0x00cf
            r0.close()
        L_0x00cf:
            return r8
        L_0x00d0:
            r12 = move-exception
            r8 = r0
        L_0x00d2:
            if (r8 == 0) goto L_0x00d7
            r8.close()
        L_0x00d7:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.g(java.lang.String):java.util.Map");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00a2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> f(java.lang.String r8) {
        /*
            r7 = this;
            r7.q()
            r7.g()
            com.fossil.a72.b(r8)
            com.fossil.n4 r0 = new com.fossil.n4
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r7.u()
            r2 = 0
            java.lang.String r3 = "select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0069, all -> 0x0067 }
            r5 = 0
            r4[r5] = r8     // Catch:{ SQLiteException -> 0x0069, all -> 0x0067 }
            r6 = 1
            r4[r6] = r8     // Catch:{ SQLiteException -> 0x0069, all -> 0x0067 }
            android.database.Cursor r1 = r1.rawQuery(r3, r4)     // Catch:{ SQLiteException -> 0x0069, all -> 0x0067 }
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0065 }
            if (r3 != 0) goto L_0x0032
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0065 }
            if (r1 == 0) goto L_0x0031
            r1.close()
        L_0x0031:
            return r8
        L_0x0032:
            int r3 = r1.getInt(r5)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)
            java.lang.Object r4 = r0.get(r4)
            java.util.List r4 = (java.util.List) r4
            if (r4 != 0) goto L_0x004e
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r0.put(r3, r4)
        L_0x004e:
            int r3 = r1.getInt(r6)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r4.add(r3)
            boolean r3 = r1.moveToNext()
            if (r3 != 0) goto L_0x0032
            if (r1 == 0) goto L_0x0064
            r1.close()
        L_0x0064:
            return r0
        L_0x0065:
            r0 = move-exception
            goto L_0x006b
        L_0x0067:
            r8 = move-exception
            goto L_0x00a0
        L_0x0069:
            r0 = move-exception
            r1 = r2
        L_0x006b:
            com.fossil.jg3 r3 = r7.e()     // Catch:{ all -> 0x009e }
            com.fossil.mg3 r3 = r3.t()     // Catch:{ all -> 0x009e }
            java.lang.String r4 = "Database error querying scoped filters. appId"
            java.lang.Object r5 = com.fossil.jg3.a(r8)     // Catch:{ all -> 0x009e }
            r3.a(r4, r5, r0)     // Catch:{ all -> 0x009e }
            boolean r0 = com.fossil.n13.a()     // Catch:{ all -> 0x009e }
            if (r0 == 0) goto L_0x0098
            com.fossil.ym3 r0 = r7.l()     // Catch:{ all -> 0x009e }
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.T0     // Catch:{ all -> 0x009e }
            boolean r8 = r0.e(r8, r3)     // Catch:{ all -> 0x009e }
            if (r8 == 0) goto L_0x0098
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0097
            r1.close()
        L_0x0097:
            return r8
        L_0x0098:
            if (r1 == 0) goto L_0x009d
            r1.close()
        L_0x009d:
            return r2
        L_0x009e:
            r8 = move-exception
            r2 = r1
        L_0x00a0:
            if (r2 == 0) goto L_0x00a5
            r2.close()
        L_0x00a5:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.f(java.lang.String):java.util.Map");
    }

    @DexIgnore
    public final long h(String str) {
        a72.b(str);
        return a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x011b A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x011f A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0159 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0172 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0187 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x01a3 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01a4 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01b3 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01e9 A[Catch:{ SQLiteException -> 0x0200 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0225  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x022d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.kg3 b(java.lang.String r35) {
        /*
            r34 = this;
            r1 = r35
            com.fossil.a72.b(r35)
            r34.g()
            r34.q()
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r34.u()     // Catch:{ SQLiteException -> 0x020e, all -> 0x020a }
            java.lang.String r4 = "apps"
            java.lang.String r5 = "app_instance_id"
            java.lang.String r6 = "gmp_app_id"
            java.lang.String r7 = "resettable_device_id_hash"
            java.lang.String r8 = "last_bundle_index"
            java.lang.String r9 = "last_bundle_start_timestamp"
            java.lang.String r10 = "last_bundle_end_timestamp"
            java.lang.String r11 = "app_version"
            java.lang.String r12 = "app_store"
            java.lang.String r13 = "gmp_version"
            java.lang.String r14 = "dev_cert_hash"
            java.lang.String r15 = "measurement_enabled"
            java.lang.String r16 = "day"
            java.lang.String r17 = "daily_public_events_count"
            java.lang.String r18 = "daily_events_count"
            java.lang.String r19 = "daily_conversions_count"
            java.lang.String r20 = "config_fetched_time"
            java.lang.String r21 = "failed_config_fetch_time"
            java.lang.String r22 = "app_version_int"
            java.lang.String r23 = "firebase_instance_id"
            java.lang.String r24 = "daily_error_events_count"
            java.lang.String r25 = "daily_realtime_events_count"
            java.lang.String r26 = "health_monitor_sample"
            java.lang.String r27 = "android_id"
            java.lang.String r28 = "adid_reporting_enabled"
            java.lang.String r29 = "ssaid_reporting_enabled"
            java.lang.String r30 = "admob_app_id"
            java.lang.String r31 = "dynamite_version"
            java.lang.String r32 = "safelisted_events"
            java.lang.String r33 = "ga_app_id"
            java.lang.String[] r5 = new java.lang.String[]{r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33}     // Catch:{ SQLiteException -> 0x020e, all -> 0x020a }
            java.lang.String r6 = "app_id=?"
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x020e, all -> 0x020a }
            r11 = 0
            r7[r11] = r1     // Catch:{ SQLiteException -> 0x020e, all -> 0x020a }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x020e, all -> 0x020a }
            boolean r4 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0206, all -> 0x0202 }
            if (r4 != 0) goto L_0x006b
            if (r3 == 0) goto L_0x006a
            r3.close()
        L_0x006a:
            return r2
        L_0x006b:
            com.fossil.kg3 r4 = new com.fossil.kg3
            r5 = r34
            com.fossil.xl3 r6 = r5.b     // Catch:{ SQLiteException -> 0x0200 }
            com.fossil.oh3 r6 = r6.u()     // Catch:{ SQLiteException -> 0x0200 }
            r4.<init>(r6, r1)     // Catch:{ SQLiteException -> 0x0200 }
            java.lang.String r6 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0200 }
            r4.a(r6)     // Catch:{ SQLiteException -> 0x0200 }
            java.lang.String r6 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r4.b(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 2
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.e(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 3
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.g(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 4
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.a(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 5
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.b(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 6
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.g(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 7
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.h(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 8
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.d(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 9
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.e(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 10
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r7 != 0) goto L_0x00d9
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r6 == 0) goto L_0x00d7
            goto L_0x00d9
        L_0x00d7:
            r6 = 0
            goto L_0x00da
        L_0x00d9:
            r6 = 1
        L_0x00da:
            r4.a(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 11
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.j(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 12
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.k(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 13
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.l(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 14
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.m(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 15
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.h(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 16
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.i(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 17
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r7 == 0) goto L_0x011f
            r6 = -2147483648(0xffffffff80000000, double:NaN)
            goto L_0x0124
        L_0x011f:
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x0200 }
            long r6 = (long) r6     // Catch:{ SQLiteException -> 0x0200 }
        L_0x0124:
            r4.c(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 18
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.f(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 19
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.o(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 20
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.n(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 21
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r4.i(r6)     // Catch:{ SQLiteException -> 0x0200 }
            com.fossil.ym3 r6 = r34.l()     // Catch:{ SQLiteException -> 0x0200 }
            com.fossil.yf3<java.lang.Boolean> r7 = com.fossil.wb3.M0     // Catch:{ SQLiteException -> 0x0200 }
            boolean r6 = r6.a(r7)     // Catch:{ SQLiteException -> 0x0200 }
            r7 = 0
            if (r6 != 0) goto L_0x016a
            r6 = 22
            boolean r9 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r9 == 0) goto L_0x0163
            r9 = r7
            goto L_0x0167
        L_0x0163:
            long r9 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x0167:
            r4.p(r9)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x016a:
            r6 = 23
            boolean r9 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r9 != 0) goto L_0x017b
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r6 == 0) goto L_0x0179
            goto L_0x017b
        L_0x0179:
            r6 = 0
            goto L_0x017c
        L_0x017b:
            r6 = 1
        L_0x017c:
            r4.b(r6)     // Catch:{ SQLiteException -> 0x0200 }
            r6 = 24
            boolean r9 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r9 != 0) goto L_0x018f
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r6 == 0) goto L_0x018e
            goto L_0x018f
        L_0x018e:
            r0 = 0
        L_0x018f:
            r4.c(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r0 = 25
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r4.c(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r0 = 26
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x0200 }
            if (r6 == 0) goto L_0x01a4
            goto L_0x01a8
        L_0x01a4:
            long r7 = r3.getLong(r0)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x01a8:
            r4.f(r7)     // Catch:{ SQLiteException -> 0x0200 }
            r0 = 27
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x0200 }
            if (r6 != 0) goto L_0x01c5
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x0200 }
            java.lang.String r6 = ","
            r7 = -1
            java.lang.String[] r0 = r0.split(r6, r7)     // Catch:{ SQLiteException -> 0x0200 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r4.a(r0)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x01c5:
            boolean r0 = com.fossil.f23.a()     // Catch:{ SQLiteException -> 0x0200 }
            if (r0 == 0) goto L_0x01e0
            com.fossil.ym3 r0 = r34.l()     // Catch:{ SQLiteException -> 0x0200 }
            com.fossil.yf3<java.lang.Boolean> r6 = com.fossil.wb3.o0     // Catch:{ SQLiteException -> 0x0200 }
            boolean r0 = r0.e(r1, r6)     // Catch:{ SQLiteException -> 0x0200 }
            if (r0 == 0) goto L_0x01e0
            r0 = 28
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x0200 }
            r4.d(r0)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x01e0:
            r4.k()     // Catch:{ SQLiteException -> 0x0200 }
            boolean r0 = r3.moveToNext()     // Catch:{ SQLiteException -> 0x0200 }
            if (r0 == 0) goto L_0x01fa
            com.fossil.jg3 r0 = r34.e()     // Catch:{ SQLiteException -> 0x0200 }
            com.fossil.mg3 r0 = r0.t()     // Catch:{ SQLiteException -> 0x0200 }
            java.lang.String r6 = "Got multiple records for app, expected one. appId"
            java.lang.Object r7 = com.fossil.jg3.a(r35)     // Catch:{ SQLiteException -> 0x0200 }
            r0.a(r6, r7)     // Catch:{ SQLiteException -> 0x0200 }
        L_0x01fa:
            if (r3 == 0) goto L_0x01ff
            r3.close()
        L_0x01ff:
            return r4
        L_0x0200:
            r0 = move-exception
            goto L_0x0212
        L_0x0202:
            r0 = move-exception
            r5 = r34
            goto L_0x022a
        L_0x0206:
            r0 = move-exception
            r5 = r34
            goto L_0x0212
        L_0x020a:
            r0 = move-exception
            r5 = r34
            goto L_0x022b
        L_0x020e:
            r0 = move-exception
            r5 = r34
            r3 = r2
        L_0x0212:
            com.fossil.jg3 r4 = r34.e()     // Catch:{ all -> 0x0229 }
            com.fossil.mg3 r4 = r4.t()     // Catch:{ all -> 0x0229 }
            java.lang.String r6 = "Error querying app. appId"
            java.lang.Object r1 = com.fossil.jg3.a(r35)     // Catch:{ all -> 0x0229 }
            r4.a(r6, r1, r0)     // Catch:{ all -> 0x0229 }
            if (r3 == 0) goto L_0x0228
            r3.close()
        L_0x0228:
            return r2
        L_0x0229:
            r0 = move-exception
        L_0x022a:
            r2 = r3
        L_0x022b:
            if (r2 == 0) goto L_0x0230
            r2.close()
        L_0x0230:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.b(java.lang.String):com.fossil.kg3");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0073  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] d(java.lang.String r11) {
        /*
            r10 = this;
            com.fossil.a72.b(r11)
            r10.g()
            r10.q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.u()     // Catch:{ SQLiteException -> 0x0056, all -> 0x0054 }
            java.lang.String r2 = "apps"
            java.lang.String r3 = "remote_config"
            java.lang.String[] r3 = new java.lang.String[]{r3}     // Catch:{ SQLiteException -> 0x0056, all -> 0x0054 }
            java.lang.String r4 = "app_id=?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0056, all -> 0x0054 }
            r9 = 0
            r5[r9] = r11     // Catch:{ SQLiteException -> 0x0056, all -> 0x0054 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0056, all -> 0x0054 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0052 }
            if (r2 != 0) goto L_0x0031
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            return r0
        L_0x0031:
            byte[] r2 = r1.getBlob(r9)
            boolean r3 = r1.moveToNext()
            if (r3 == 0) goto L_0x004c
            com.fossil.jg3 r3 = r10.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r4 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r5 = com.fossil.jg3.a(r11)
            r3.a(r4, r5)
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            return r2
        L_0x0052:
            r2 = move-exception
            goto L_0x0058
        L_0x0054:
            r11 = move-exception
            goto L_0x0071
        L_0x0056:
            r2 = move-exception
            r1 = r0
        L_0x0058:
            com.fossil.jg3 r3 = r10.e()     // Catch:{ all -> 0x006f }
            com.fossil.mg3 r3 = r3.t()     // Catch:{ all -> 0x006f }
            java.lang.String r4 = "Error querying remote config. appId"
            java.lang.Object r11 = com.fossil.jg3.a(r11)     // Catch:{ all -> 0x006f }
            r3.a(r4, r11, r2)     // Catch:{ all -> 0x006f }
            if (r1 == 0) goto L_0x006e
            r1.close()
        L_0x006e:
            return r0
        L_0x006f:
            r11 = move-exception
            r0 = r1
        L_0x0071:
            if (r0 == 0) goto L_0x0076
            r0.close()
        L_0x0076:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.d(java.lang.String):byte[]");
    }

    @DexIgnore
    public final void a(qb3 qb3) {
        a72.a(qb3);
        g();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", qb3.a);
        contentValues.put("name", qb3.b);
        contentValues.put("lifetime_count", Long.valueOf(qb3.c));
        contentValues.put("current_bundle_count", Long.valueOf(qb3.d));
        contentValues.put("last_fire_timestamp", Long.valueOf(qb3.f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(qb3.g));
        contentValues.put("last_bundled_day", qb3.h);
        contentValues.put("last_sampled_complex_event_id", qb3.i);
        contentValues.put("last_sampling_rate", qb3.j);
        contentValues.put("current_session_count", Long.valueOf(qb3.e));
        Boolean bool = qb3.k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (u().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                e().t().a("Failed to insert/update event aggregates (got -1). appId", jg3.a(qb3.a));
            }
        } catch (SQLiteException e2) {
            e().t().a("Error storing event aggregates. appId", jg3.a(qb3.a), e2);
        }
    }

    @DexIgnore
    public final boolean a(gm3 gm3) {
        a72.a(gm3);
        g();
        q();
        if (c(gm3.a, gm3.c) == null) {
            if (jm3.h(gm3.c)) {
                if (b("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{gm3.a}) >= ((long) l().d(gm3.a))) {
                    return false;
                }
            } else if (!"_npa".equals(gm3.c)) {
                if (b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{gm3.a, gm3.b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", gm3.a);
        contentValues.put("origin", gm3.b);
        contentValues.put("name", gm3.c);
        contentValues.put("set_timestamp", Long.valueOf(gm3.d));
        a(contentValues, "value", gm3.e);
        try {
            if (u().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                e().t().a("Failed to insert/update user property (got -1). appId", jg3.a(gm3.a));
            }
        } catch (SQLiteException e2) {
            e().t().a("Error storing user property. appId", jg3.a(gm3.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final boolean b(String str, List<Integer> list) {
        a72.b(str);
        q();
        g();
        SQLiteDatabase u = u();
        try {
            long b = b("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, l().b(str, wb3.F)));
            if (b <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < list.size(); i2++) {
                Integer num = list.get(i2);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return u.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e2) {
            e().t().a("Database error querying filters. appId", jg3.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00bd  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.gm3> a(java.lang.String r14) {
        /*
            r13 = this;
            com.fossil.a72.b(r14)
            r13.g()
            r13.q()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r13.u()     // Catch:{ SQLiteException -> 0x0084, all -> 0x0082 }
            java.lang.String r3 = "user_attributes"
            java.lang.String r4 = "name"
            java.lang.String r5 = "origin"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String[] r4 = new java.lang.String[]{r4, r5, r6, r7}     // Catch:{ SQLiteException -> 0x0084, all -> 0x0082 }
            java.lang.String r5 = "app_id=?"
            r11 = 1
            java.lang.String[] r6 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0084, all -> 0x0082 }
            r12 = 0
            r6[r12] = r14     // Catch:{ SQLiteException -> 0x0084, all -> 0x0082 }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "1000"
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0084, all -> 0x0082 }
            boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0080 }
            if (r3 != 0) goto L_0x003f
            if (r2 == 0) goto L_0x003e
            r2.close()
        L_0x003e:
            return r0
        L_0x003f:
            java.lang.String r7 = r2.getString(r12)
            java.lang.String r3 = r2.getString(r11)
            if (r3 != 0) goto L_0x004b
            java.lang.String r3 = ""
        L_0x004b:
            r6 = r3
            r3 = 2
            long r8 = r2.getLong(r3)
            r3 = 3
            java.lang.Object r10 = r13.a(r2, r3)
            if (r10 != 0) goto L_0x006a
            com.fossil.jg3 r3 = r13.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r4 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r5 = com.fossil.jg3.a(r14)
            r3.a(r4, r5)
            goto L_0x0074
        L_0x006a:
            com.fossil.gm3 r3 = new com.fossil.gm3
            r4 = r3
            r5 = r14
            r4.<init>(r5, r6, r7, r8, r10)
            r0.add(r3)
        L_0x0074:
            boolean r3 = r2.moveToNext()
            if (r3 != 0) goto L_0x003f
            if (r2 == 0) goto L_0x007f
            r2.close()
        L_0x007f:
            return r0
        L_0x0080:
            r0 = move-exception
            goto L_0x0086
        L_0x0082:
            r14 = move-exception
            goto L_0x00bb
        L_0x0084:
            r0 = move-exception
            r2 = r1
        L_0x0086:
            com.fossil.jg3 r3 = r13.e()     // Catch:{ all -> 0x00b9 }
            com.fossil.mg3 r3 = r3.t()     // Catch:{ all -> 0x00b9 }
            java.lang.String r4 = "Error querying user properties. appId"
            java.lang.Object r5 = com.fossil.jg3.a(r14)     // Catch:{ all -> 0x00b9 }
            r3.a(r4, r5, r0)     // Catch:{ all -> 0x00b9 }
            boolean r0 = com.fossil.n13.a()     // Catch:{ all -> 0x00b9 }
            if (r0 == 0) goto L_0x00b3
            com.fossil.ym3 r0 = r13.l()     // Catch:{ all -> 0x00b9 }
            com.fossil.yf3<java.lang.Boolean> r3 = com.fossil.wb3.T0     // Catch:{ all -> 0x00b9 }
            boolean r14 = r0.e(r14, r3)     // Catch:{ all -> 0x00b9 }
            if (r14 == 0) goto L_0x00b3
            java.util.List r14 = java.util.Collections.emptyList()     // Catch:{ all -> 0x00b9 }
            if (r2 == 0) goto L_0x00b2
            r2.close()
        L_0x00b2:
            return r14
        L_0x00b3:
            if (r2 == 0) goto L_0x00b8
            r2.close()
        L_0x00b8:
            return r1
        L_0x00b9:
            r14 = move-exception
            r1 = r2
        L_0x00bb:
            if (r1 == 0) goto L_0x00c0
            r1.close()
        L_0x00c0:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00fb, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00ff, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0100, code lost:
        r3 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x013c, code lost:
        r9.close();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00fb A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x0011] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x011c A[Catch:{ all -> 0x0138 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0134  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x013c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.fossil.gm3> a(java.lang.String r21, java.lang.String r22, java.lang.String r23) {
        /*
            r20 = this;
            r8 = r21
            com.fossil.a72.b(r21)
            r20.g()
            r20.q()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r9 = 0
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            r10 = 3
            r1.<init>(r10)     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            r1.add(r8)     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            java.lang.String r3 = "app_id=?"
            r2.<init>(r3)     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            boolean r3 = android.text.TextUtils.isEmpty(r22)     // Catch:{ SQLiteException -> 0x00ff, all -> 0x00fb }
            if (r3 != 0) goto L_0x0032
            r3 = r22
            r1.add(r3)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String r4 = " and origin=?"
            r2.append(r4)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            goto L_0x0034
        L_0x0032:
            r3 = r22
        L_0x0034:
            boolean r4 = android.text.TextUtils.isEmpty(r23)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            if (r4 != 0) goto L_0x004c
            java.lang.String r4 = java.lang.String.valueOf(r23)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String r5 = "*"
            java.lang.String r4 = r4.concat(r5)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            r1.add(r4)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String r4 = " and name glob ?"
            r2.append(r4)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
        L_0x004c:
            int r4 = r1.size()     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.Object[] r1 = r1.toArray(r4)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            r15 = r1
            java.lang.String[] r15 = (java.lang.String[]) r15     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            android.database.sqlite.SQLiteDatabase r11 = r20.u()     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String r12 = "user_attributes"
            java.lang.String r1 = "name"
            java.lang.String r4 = "set_timestamp"
            java.lang.String r5 = "value"
            java.lang.String r6 = "origin"
            java.lang.String[] r13 = new java.lang.String[]{r1, r4, r5, r6}     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            java.lang.String r14 = r2.toString()     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            r16 = 0
            r17 = 0
            java.lang.String r18 = "rowid"
            java.lang.String r19 = "1001"
            android.database.Cursor r11 = r11.query(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00fb }
            boolean r1 = r11.moveToFirst()     // Catch:{ SQLiteException -> 0x00f3, all -> 0x00ef }
            if (r1 != 0) goto L_0x0087
            if (r11 == 0) goto L_0x0086
            r11.close()
        L_0x0086:
            return r0
        L_0x0087:
            int r1 = r0.size()
            r2 = 1000(0x3e8, float:1.401E-42)
            if (r1 < r2) goto L_0x00a3
            com.fossil.jg3 r1 = r20.e()
            com.fossil.mg3 r1 = r1.t()
            java.lang.String r4 = "Read more than the max allowed user properties, ignoring excess"
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r1.a(r4, r2)
            r12 = r20
            goto L_0x00e2
        L_0x00a3:
            r1 = 0
            java.lang.String r4 = r11.getString(r1)
            r1 = 1
            long r5 = r11.getLong(r1)
            r1 = 2
            r12 = r20
            java.lang.Object r7 = r12.a(r11, r1)     // Catch:{ SQLiteException -> 0x00ed }
            java.lang.String r13 = r11.getString(r10)     // Catch:{ SQLiteException -> 0x00ed }
            if (r7 != 0) goto L_0x00ce
            com.fossil.jg3 r1 = r20.e()     // Catch:{ SQLiteException -> 0x00ea }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ SQLiteException -> 0x00ea }
            java.lang.String r2 = "(2)Read invalid user property value, ignoring it"
            java.lang.Object r3 = com.fossil.jg3.a(r21)     // Catch:{ SQLiteException -> 0x00ea }
            r14 = r23
            r1.a(r2, r3, r13, r14)     // Catch:{ SQLiteException -> 0x00ea }
            goto L_0x00dc
        L_0x00ce:
            r14 = r23
            com.fossil.gm3 r15 = new com.fossil.gm3     // Catch:{ SQLiteException -> 0x00ea }
            r1 = r15
            r2 = r21
            r3 = r13
            r1.<init>(r2, r3, r4, r5, r7)     // Catch:{ SQLiteException -> 0x00ea }
            r0.add(r15)     // Catch:{ SQLiteException -> 0x00ea }
        L_0x00dc:
            boolean r1 = r11.moveToNext()     // Catch:{ SQLiteException -> 0x00ea }
            if (r1 != 0) goto L_0x00e8
        L_0x00e2:
            if (r11 == 0) goto L_0x00e7
            r11.close()
        L_0x00e7:
            return r0
        L_0x00e8:
            r3 = r13
            goto L_0x0087
        L_0x00ea:
            r0 = move-exception
            r3 = r13
            goto L_0x0105
        L_0x00ed:
            r0 = move-exception
            goto L_0x0105
        L_0x00ef:
            r0 = move-exception
            r12 = r20
            goto L_0x0139
        L_0x00f3:
            r0 = move-exception
            r12 = r20
            goto L_0x0105
        L_0x00f7:
            r0 = move-exception
            r12 = r20
            goto L_0x0104
        L_0x00fb:
            r0 = move-exception
            r12 = r20
            goto L_0x013a
        L_0x00ff:
            r0 = move-exception
            r12 = r20
            r3 = r22
        L_0x0104:
            r11 = r9
        L_0x0105:
            com.fossil.jg3 r1 = r20.e()     // Catch:{ all -> 0x0138 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x0138 }
            java.lang.String r2 = "(2)Error querying user properties"
            java.lang.Object r4 = com.fossil.jg3.a(r21)     // Catch:{ all -> 0x0138 }
            r1.a(r2, r4, r3, r0)     // Catch:{ all -> 0x0138 }
            boolean r0 = com.fossil.n13.a()     // Catch:{ all -> 0x0138 }
            if (r0 == 0) goto L_0x0132
            com.fossil.ym3 r0 = r20.l()     // Catch:{ all -> 0x0138 }
            com.fossil.yf3<java.lang.Boolean> r1 = com.fossil.wb3.T0     // Catch:{ all -> 0x0138 }
            boolean r0 = r0.e(r8, r1)     // Catch:{ all -> 0x0138 }
            if (r0 == 0) goto L_0x0132
            java.util.List r0 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0138 }
            if (r11 == 0) goto L_0x0131
            r11.close()
        L_0x0131:
            return r0
        L_0x0132:
            if (r11 == 0) goto L_0x0137
            r11.close()
        L_0x0137:
            return r9
        L_0x0138:
            r0 = move-exception
        L_0x0139:
            r9 = r11
        L_0x013a:
            if (r9 == 0) goto L_0x013f
            r9.close()
        L_0x013f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.a(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    @DexIgnore
    public final boolean a(wm3 wm3) {
        a72.a(wm3);
        g();
        q();
        if (c(wm3.a, wm3.c.b) == null) {
            if (b("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{wm3.a}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", wm3.a);
        contentValues.put("origin", wm3.b);
        contentValues.put("name", wm3.c.b);
        a(contentValues, "value", wm3.c.zza());
        contentValues.put("active", Boolean.valueOf(wm3.e));
        contentValues.put("trigger_event_name", wm3.f);
        contentValues.put("trigger_timeout", Long.valueOf(wm3.h));
        j();
        contentValues.put("timed_out_event", jm3.a((Parcelable) wm3.g));
        contentValues.put("creation_timestamp", Long.valueOf(wm3.d));
        j();
        contentValues.put("triggered_event", jm3.a((Parcelable) wm3.i));
        contentValues.put("triggered_timestamp", Long.valueOf(wm3.c.c));
        contentValues.put("time_to_live", Long.valueOf(wm3.j));
        j();
        contentValues.put("expired_event", jm3.a((Parcelable) wm3.p));
        try {
            if (u().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                e().t().a("Failed to insert/update conditional user property (got -1)", jg3.a(wm3.a));
            }
        } catch (SQLiteException e2) {
            e().t().a("Error storing conditional user property", jg3.a(wm3.a), e2);
        }
        return true;
    }

    @DexIgnore
    public final List<wm3> a(String str, String[] strArr) {
        g();
        q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = u().query("conditional_properties", new String[]{"app_id", "origin", "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, null, null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j2 = cursor.getLong(6);
                    long j3 = cursor.getLong(8);
                    arrayList.add(new wm3(string, string2, new em3(string3, cursor.getLong(10), a, string2), j3, z, string4, (ub3) m().a(cursor.getBlob(7), ub3.CREATOR), j2, (ub3) m().a(cursor.getBlob(9), ub3.CREATOR), cursor.getLong(11), (ub3) m().a(cursor.getBlob(12), ub3.CREATOR)));
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    e().t().a("Read more than the max allowed conditional properties, ignoring extra", 1000);
                    break;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e2) {
            e().t().a("Error querying conditional user property value", e2);
            List<wm3> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(kg3 kg3) {
        a72.a(kg3);
        g();
        q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", kg3.l());
        contentValues.put("app_instance_id", kg3.m());
        contentValues.put("gmp_app_id", kg3.n());
        contentValues.put("resettable_device_id_hash", kg3.q());
        contentValues.put("last_bundle_index", Long.valueOf(kg3.B()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(kg3.s()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(kg3.t()));
        contentValues.put("app_version", kg3.u());
        contentValues.put("app_store", kg3.w());
        contentValues.put("gmp_version", Long.valueOf(kg3.x()));
        contentValues.put("dev_cert_hash", Long.valueOf(kg3.y()));
        contentValues.put("measurement_enabled", Boolean.valueOf(kg3.A()));
        contentValues.put("day", Long.valueOf(kg3.F()));
        contentValues.put("daily_public_events_count", Long.valueOf(kg3.G()));
        contentValues.put("daily_events_count", Long.valueOf(kg3.H()));
        contentValues.put("daily_conversions_count", Long.valueOf(kg3.I()));
        contentValues.put("config_fetched_time", Long.valueOf(kg3.C()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(kg3.D()));
        contentValues.put("app_version_int", Long.valueOf(kg3.v()));
        contentValues.put("firebase_instance_id", kg3.r());
        contentValues.put("daily_error_events_count", Long.valueOf(kg3.c()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(kg3.b()));
        contentValues.put("health_monitor_sample", kg3.d());
        contentValues.put("android_id", Long.valueOf(kg3.f()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(kg3.g()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(kg3.h()));
        contentValues.put("admob_app_id", kg3.o());
        contentValues.put("dynamite_version", Long.valueOf(kg3.z()));
        if (kg3.j() != null) {
            if (kg3.j().size() == 0) {
                e().w().a("Safelisted events should not be an empty list. appId", kg3.l());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", kg3.j()));
            }
        }
        if (f23.a() && l().e(kg3.l(), wb3.o0)) {
            contentValues.put("ga_app_id", kg3.p());
        }
        try {
            SQLiteDatabase u = u();
            if (((long) u.update("apps", contentValues, "app_id = ?", new String[]{kg3.l()})) == 0 && u.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                e().t().a("Failed to insert/update app (got -1). appId", jg3.a(kg3.l()));
            }
        } catch (SQLiteException e2) {
            e().t().a("Error storing app. appId", jg3.a(kg3.l()), e2);
        }
    }

    @DexIgnore
    public final ib3 a(long j2, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        return a(j2, str, 1, false, false, z3, false, z5);
    }

    @DexIgnore
    public final ib3 a(long j2, String str, long j3, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        a72.b(str);
        g();
        q();
        String[] strArr = {str};
        ib3 ib3 = new ib3();
        Cursor cursor = null;
        try {
            SQLiteDatabase u = u();
            Cursor query = u.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            if (!query.moveToFirst()) {
                e().w().a("Not updating daily counts, app is not known. appId", jg3.a(str));
                if (query != null) {
                    query.close();
                }
                return ib3;
            }
            if (query.getLong(0) == j2) {
                ib3.b = query.getLong(1);
                ib3.a = query.getLong(2);
                ib3.c = query.getLong(3);
                ib3.d = query.getLong(4);
                ib3.e = query.getLong(5);
            }
            if (z) {
                ib3.b += j3;
            }
            if (z2) {
                ib3.a += j3;
            }
            if (z3) {
                ib3.c += j3;
            }
            if (z4) {
                ib3.d += j3;
            }
            if (z5) {
                ib3.e += j3;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j2));
            contentValues.put("daily_public_events_count", Long.valueOf(ib3.a));
            contentValues.put("daily_events_count", Long.valueOf(ib3.b));
            contentValues.put("daily_conversions_count", Long.valueOf(ib3.c));
            contentValues.put("daily_error_events_count", Long.valueOf(ib3.d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(ib3.e));
            u.update("apps", contentValues, "app_id=?", strArr);
            if (query != null) {
                query.close();
            }
            return ib3;
        } catch (SQLiteException e2) {
            e().t().a("Error updating daily counts. appId", jg3.a(str), e2);
            if (0 != 0) {
                cursor.close();
            }
            return ib3;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(vp2 vp2, boolean z) {
        g();
        q();
        a72.a(vp2);
        a72.b(vp2.w0());
        a72.b(vp2.l0());
        B();
        long b = zzm().b();
        if (vp2.m0() < b - ym3.w() || vp2.m0() > ym3.w() + b) {
            e().w().a("Storing bundle outside of the max uploading time span. appId, now, timestamp", jg3.a(vp2.w0()), Long.valueOf(b), Long.valueOf(vp2.m0()));
        }
        try {
            byte[] c = m().c(vp2.a());
            e().B().a("Saving bundle, size", Integer.valueOf(c.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", vp2.w0());
            contentValues.put("bundle_end_timestamp", Long.valueOf(vp2.m0()));
            contentValues.put("data", c);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (vp2.O()) {
                contentValues.put("retry_count", Integer.valueOf(vp2.Q()));
            }
            try {
                if (u().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                e().t().a("Failed to insert bundle (got -1). appId", jg3.a(vp2.w0()));
                return false;
            } catch (SQLiteException e2) {
                e().t().a("Error storing bundle. appId", jg3.a(vp2.w0()), e2);
                return false;
            }
        } catch (IOException e3) {
            e().t().a("Data loss. Failed to serialize bundle. appId", jg3.a(vp2.w0()), e3);
            return false;
        }
    }

    @DexIgnore
    public final List<Pair<vp2, Long>> a(String str, int i2, int i3) {
        g();
        q();
        a72.a(i2 > 0);
        a72.a(i3 > 0);
        a72.b(str);
        Cursor cursor = null;
        try {
            Cursor query = u().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i2));
            if (!query.moveToFirst()) {
                List<Pair<vp2, Long>> emptyList = Collections.emptyList();
                if (query != null) {
                    query.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i4 = 0;
            do {
                long j2 = query.getLong(0);
                try {
                    byte[] b = m().b(query.getBlob(1));
                    if (!arrayList.isEmpty() && b.length + i4 > i3) {
                        break;
                    }
                    try {
                        vp2.a z0 = vp2.z0();
                        fm3.a(z0, b);
                        vp2.a aVar = z0;
                        if (!query.isNull(2)) {
                            aVar.i(query.getInt(2));
                        }
                        i4 += b.length;
                        arrayList.add(Pair.create((vp2) ((bw2) aVar.g()), Long.valueOf(j2)));
                    } catch (IOException e2) {
                        e().t().a("Failed to merge queued bundle. appId", jg3.a(str), e2);
                    }
                    if (!query.moveToNext()) {
                        break;
                    }
                } catch (IOException e3) {
                    e().t().a("Failed to unzip queued bundle. appId", jg3.a(str), e3);
                }
            } while (i4 <= i3);
            if (query != null) {
                query.close();
            }
            return arrayList;
        } catch (SQLiteException e4) {
            e().t().a("Error querying bundles. appId", jg3.a(str), e4);
            List<Pair<vp2, Long>> emptyList2 = Collections.emptyList();
            if (0 != 0) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (0 != 0) {
                cursor.close();
            }
            throw th;
        }
    }

    @DexIgnore
    public final void a(List<Long> list) {
        g();
        q();
        a72.a(list);
        a72.a(list.size());
        if (x()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (b(sb3.toString(), (String[]) null) > 0) {
                e().w().a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase u = u();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                u.execSQL(sb4.toString());
            } catch (SQLiteException e2) {
                e().t().a("Error incrementing retry count. error", e2);
            }
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, ap2 ap2) {
        q();
        g();
        a72.b(str);
        a72.a(ap2);
        Integer num = null;
        if (TextUtils.isEmpty(ap2.q())) {
            mg3 w = e().w();
            Object a = jg3.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (ap2.zza()) {
                num = Integer.valueOf(ap2.p());
            }
            w.a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] a2 = ap2.a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", ap2.zza() ? Integer.valueOf(ap2.p()) : null);
        contentValues.put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, ap2.q());
        contentValues.put("session_scoped", ap2.x() ? Boolean.valueOf(ap2.y()) : null);
        contentValues.put("data", a2);
        try {
            if (u().insertWithOnConflict("event_filters", null, contentValues, 5) != -1) {
                return true;
            }
            e().t().a("Failed to insert event filter (got -1). appId", jg3.a(str));
            return true;
        } catch (SQLiteException e2) {
            e().t().a("Error storing event filter. appId", jg3.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, int i2, dp2 dp2) {
        q();
        g();
        a72.b(str);
        a72.a(dp2);
        Integer num = null;
        if (TextUtils.isEmpty(dp2.q())) {
            mg3 w = e().w();
            Object a = jg3.a(str);
            Integer valueOf = Integer.valueOf(i2);
            if (dp2.zza()) {
                num = Integer.valueOf(dp2.p());
            }
            w.a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] a2 = dp2.a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i2));
        contentValues.put("filter_id", dp2.zza() ? Integer.valueOf(dp2.p()) : null);
        contentValues.put("property_name", dp2.q());
        contentValues.put("session_scoped", dp2.u() ? Boolean.valueOf(dp2.v()) : null);
        contentValues.put("data", a2);
        try {
            if (u().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                return true;
            }
            e().t().a("Failed to insert property filter (got -1). appId", jg3.a(str));
            return false;
        } catch (SQLiteException e2) {
            e().t().a("Error storing property filter. appId", jg3.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public static void a(ContentValues contentValues, String str, Object obj) {
        a72.b(str);
        a72.a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @DexIgnore
    public final Object a(Cursor cursor, int i2) {
        int type = cursor.getType(i2);
        if (type == 0) {
            e().t().a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i2));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i2));
            }
            if (type == 3) {
                return cursor.getString(i2);
            }
            if (type != 4) {
                e().t().a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            e().t().a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    @DexIgnore
    public final long a(vp2 vp2) throws IOException {
        g();
        q();
        a72.a(vp2);
        a72.b(vp2.w0());
        byte[] a = vp2.a();
        long a2 = m().a(a);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", vp2.w0());
        contentValues.put("metadata_fingerprint", Long.valueOf(a2));
        contentValues.put("metadata", a);
        try {
            u().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
            return a2;
        } catch (SQLiteException e2) {
            e().t().a("Error storing raw event metadata. appId", jg3.a(vp2.w0()), e2);
            throw e2;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(long r5) {
        /*
            r4 = this;
            r4.g()
            r4.q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r4.u()     // Catch:{ SQLiteException -> 0x0042, all -> 0x0040 }
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0042, all -> 0x0040 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0040 }
            r6 = 0
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x0042, all -> 0x0040 }
            android.database.Cursor r5 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x0042, all -> 0x0040 }
            boolean r1 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x003e }
            if (r1 != 0) goto L_0x0034
            com.fossil.jg3 r6 = r4.e()     // Catch:{ SQLiteException -> 0x003e }
            com.fossil.mg3 r6 = r6.B()     // Catch:{ SQLiteException -> 0x003e }
            java.lang.String r1 = "No expired configs for apps with pending events"
            r6.a(r1)     // Catch:{ SQLiteException -> 0x003e }
            if (r5 == 0) goto L_0x0033
            r5.close()
        L_0x0033:
            return r0
        L_0x0034:
            java.lang.String r6 = r5.getString(r6)
            if (r5 == 0) goto L_0x003d
            r5.close()
        L_0x003d:
            return r6
        L_0x003e:
            r6 = move-exception
            goto L_0x0044
        L_0x0040:
            r6 = move-exception
            goto L_0x0059
        L_0x0042:
            r6 = move-exception
            r5 = r0
        L_0x0044:
            com.fossil.jg3 r1 = r4.e()     // Catch:{ all -> 0x0057 }
            com.fossil.mg3 r1 = r1.t()     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "Error selecting expired configs"
            r1.a(r2, r6)     // Catch:{ all -> 0x0057 }
            if (r5 == 0) goto L_0x0056
            r5.close()
        L_0x0056:
            return r0
        L_0x0057:
            r6 = move-exception
            r0 = r5
        L_0x0059:
            if (r0 == 0) goto L_0x005e
            r0.close()
        L_0x005e:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.a(long):java.lang.String");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0093  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair<com.fossil.rp2, java.lang.Long> a(java.lang.String r8, java.lang.Long r9) {
        /*
            r7 = this;
            r7.g()
            r7.q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.u()     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            java.lang.String r2 = "select main_event, children_to_process from main_event_params where app_id=? and event_id=?"
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            r4 = 0
            r3[r4] = r8     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            java.lang.String r5 = java.lang.String.valueOf(r9)     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            r6 = 1
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x007a, all -> 0x0078 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0076 }
            if (r2 != 0) goto L_0x0037
            com.fossil.jg3 r8 = r7.e()     // Catch:{ SQLiteException -> 0x0076 }
            com.fossil.mg3 r8 = r8.B()     // Catch:{ SQLiteException -> 0x0076 }
            java.lang.String r9 = "Main event not found"
            r8.a(r9)     // Catch:{ SQLiteException -> 0x0076 }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return r0
        L_0x0037:
            byte[] r2 = r1.getBlob(r4)
            long r3 = r1.getLong(r6)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            com.fossil.rp2$a r4 = com.fossil.rp2.z()     // Catch:{ IOException -> 0x005e }
            com.fossil.fm3.a(r4, r2)     // Catch:{ IOException -> 0x005e }
            com.fossil.rp2$a r4 = (com.fossil.rp2.a) r4     // Catch:{ IOException -> 0x005e }
            com.fossil.jx2 r2 = r4.g()     // Catch:{ IOException -> 0x005e }
            com.fossil.bw2 r2 = (com.fossil.bw2) r2     // Catch:{ IOException -> 0x005e }
            com.fossil.rp2 r2 = (com.fossil.rp2) r2     // Catch:{ IOException -> 0x005e }
            android.util.Pair r8 = android.util.Pair.create(r2, r3)
            if (r1 == 0) goto L_0x005d
            r1.close()
        L_0x005d:
            return r8
        L_0x005e:
            r2 = move-exception
            com.fossil.jg3 r3 = r7.e()
            com.fossil.mg3 r3 = r3.t()
            java.lang.String r4 = "Failed to merge main event. appId, eventId"
            java.lang.Object r8 = com.fossil.jg3.a(r8)
            r3.a(r4, r8, r9, r2)
            if (r1 == 0) goto L_0x0075
            r1.close()
        L_0x0075:
            return r0
        L_0x0076:
            r8 = move-exception
            goto L_0x007c
        L_0x0078:
            r8 = move-exception
            goto L_0x0091
        L_0x007a:
            r8 = move-exception
            r1 = r0
        L_0x007c:
            com.fossil.jg3 r9 = r7.e()     // Catch:{ all -> 0x008f }
            com.fossil.mg3 r9 = r9.t()     // Catch:{ all -> 0x008f }
            java.lang.String r2 = "Error selecting main event"
            r9.a(r2, r8)     // Catch:{ all -> 0x008f }
            if (r1 == 0) goto L_0x008e
            r1.close()
        L_0x008e:
            return r0
        L_0x008f:
            r8 = move-exception
            r0 = r1
        L_0x0091:
            if (r0 == 0) goto L_0x0096
            r0.close()
        L_0x0096:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jb3.a(java.lang.String, java.lang.Long):android.util.Pair");
    }

    @DexIgnore
    public final boolean a(String str, Long l2, long j2, rp2 rp2) {
        g();
        q();
        a72.a(rp2);
        a72.b(str);
        a72.a(l2);
        byte[] a = rp2.a();
        e().B().a("Saving complex main event, appId, data size", i().a(str), Integer.valueOf(a.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put(LogBuilder.KEY_EVENT_ID, l2);
        contentValues.put("children_to_process", Long.valueOf(j2));
        contentValues.put("main_event", a);
        try {
            if (u().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            e().t().a("Failed to insert complex main event (got -1). appId", jg3.a(str));
            return false;
        } catch (SQLiteException e2) {
            e().t().a("Error storing complex main event. appId", jg3.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, Bundle bundle) {
        g();
        q();
        byte[] a = m().a(new rb3(((ii3) this).a, "", str, "dep", 0, 0, bundle)).a();
        e().B().a("Saving default event parameters, appId, data size", i().a(str), Integer.valueOf(a.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("parameters", a);
        try {
            if (u().insertWithOnConflict("default_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            e().t().a("Failed to insert default event parameters (got -1). appId", jg3.a(str));
            return false;
        } catch (SQLiteException e2) {
            e().t().a("Error storing default event parameters. appId", jg3.a(str), e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(rb3 rb3, long j2, boolean z) {
        g();
        q();
        a72.a(rb3);
        a72.b(rb3.a);
        byte[] a = m().a(rb3).a();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", rb3.a);
        contentValues.put("name", rb3.b);
        contentValues.put("timestamp", Long.valueOf(rb3.d));
        contentValues.put("metadata_fingerprint", Long.valueOf(j2));
        contentValues.put("data", a);
        contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
        try {
            if (u().insert("raw_events", null, contentValues) != -1) {
                return true;
            }
            e().t().a("Failed to insert raw event (got -1). appId", jg3.a(rb3.a));
            return false;
        } catch (SQLiteException e2) {
            e().t().a("Error storing raw event. appId", jg3.a(rb3.a), e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, List<zo2> list) {
        boolean z;
        boolean z2;
        a72.a(list);
        for (int i2 = 0; i2 < list.size(); i2++) {
            zo2.a aVar = (zo2.a) list.get(i2).l();
            if (aVar.o() != 0) {
                for (int i3 = 0; i3 < aVar.o(); i3++) {
                    ap2.a aVar2 = (ap2.a) aVar.b(i3).l();
                    ap2.a aVar3 = (ap2.a) ((bw2.a) aVar2.clone());
                    String b = ni3.b(aVar2.zza());
                    if (b != null) {
                        aVar3.a(b);
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    for (int i4 = 0; i4 < aVar2.o(); i4++) {
                        bp2 a = aVar2.a(i4);
                        String a2 = mi3.a(a.v());
                        if (a2 != null) {
                            bp2.a aVar4 = (bp2.a) a.l();
                            aVar4.a(a2);
                            aVar3.a(i4, (bp2) ((bw2) aVar4.g()));
                            z2 = true;
                        }
                    }
                    if (z2) {
                        aVar.a(i3, aVar3);
                        list.set(i2, (zo2) ((bw2) aVar.g()));
                    }
                }
            }
            if (aVar.zza() != 0) {
                for (int i5 = 0; i5 < aVar.zza(); i5++) {
                    dp2 a3 = aVar.a(i5);
                    String a4 = pi3.a(a3.q());
                    if (a4 != null) {
                        dp2.a aVar5 = (dp2.a) a3.l();
                        aVar5.a(a4);
                        aVar.a(i5, aVar5);
                        list.set(i2, (zo2) ((bw2) aVar.g()));
                    }
                }
            }
        }
        q();
        g();
        a72.b(str);
        a72.a(list);
        SQLiteDatabase u = u();
        u.beginTransaction();
        try {
            q();
            g();
            a72.b(str);
            SQLiteDatabase u2 = u();
            u2.delete("property_filters", "app_id=?", new String[]{str});
            u2.delete("event_filters", "app_id=?", new String[]{str});
            for (zo2 zo2 : list) {
                q();
                g();
                a72.b(str);
                a72.a(zo2);
                if (!zo2.zza()) {
                    e().w().a("Audience with no ID. appId", jg3.a(str));
                } else {
                    int p = zo2.p();
                    Iterator<ap2> it = zo2.s().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!it.next().zza()) {
                                e().w().a("Event filter with no ID. Audience definition ignored. appId, audienceId", jg3.a(str), Integer.valueOf(p));
                                break;
                            }
                        } else {
                            Iterator<dp2> it2 = zo2.q().iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (!it2.next().zza()) {
                                        e().w().a("Property filter with no ID. Audience definition ignored. appId, audienceId", jg3.a(str), Integer.valueOf(p));
                                        break;
                                    }
                                } else {
                                    Iterator<ap2> it3 = zo2.s().iterator();
                                    while (true) {
                                        if (it3.hasNext()) {
                                            if (!a(str, p, it3.next())) {
                                                z = false;
                                                break;
                                            }
                                        } else {
                                            z = true;
                                            break;
                                        }
                                    }
                                    if (z) {
                                        Iterator<dp2> it4 = zo2.q().iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (!a(str, p, it4.next())) {
                                                    z = false;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        q();
                                        g();
                                        a72.b(str);
                                        SQLiteDatabase u3 = u();
                                        u3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(p)});
                                        u3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str, String.valueOf(p)});
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (zo2 zo22 : list) {
                arrayList.add(zo22.zza() ? Integer.valueOf(zo22.p()) : null);
            }
            b(str, arrayList);
            u.setTransactionSuccessful();
        } finally {
            u.endTransaction();
        }
    }
}
