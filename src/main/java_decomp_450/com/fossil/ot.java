package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot extends rt {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public ot(int i, int i2) {
        super(null);
        this.a = i;
        this.b = i2;
        if (!(i > 0 && i2 > 0)) {
            throw new IllegalArgumentException("Width and height must be > 0.".toString());
        }
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final int c() {
        return this.b;
    }

    @DexIgnore
    public final int d() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ot)) {
            return false;
        }
        ot otVar = (ot) obj;
        return this.a == otVar.a && this.b == otVar.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 31) + this.b;
    }

    @DexIgnore
    public String toString() {
        return "PixelSize(width=" + this.a + ", height=" + this.b + ")";
    }
}
