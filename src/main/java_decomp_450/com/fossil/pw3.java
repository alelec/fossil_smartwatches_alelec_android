package com.fossil;

import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pw3<E> extends k04<E> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public int b;

    @DexIgnore
    public pw3(int i, int i2) {
        jw3.b(i2, i);
        this.a = i;
        this.b = i2;
    }

    @DexIgnore
    public abstract E a(int i);

    @DexIgnore
    public final boolean hasNext() {
        return this.b < this.a;
    }

    @DexIgnore
    public final boolean hasPrevious() {
        return this.b > 0;
    }

    @DexIgnore
    @Override // java.util.Iterator, java.util.ListIterator
    public final E next() {
        if (hasNext()) {
            int i = this.b;
            this.b = i + 1;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int nextIndex() {
        return this.b;
    }

    @DexIgnore
    @Override // java.util.ListIterator
    public final E previous() {
        if (hasPrevious()) {
            int i = this.b - 1;
            this.b = i;
            return a(i);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final int previousIndex() {
        return this.b - 1;
    }
}
