package com.fossil;

import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iw2 extends IOException {
    @DexIgnore
    public jx2 zza; // = null;

    @DexIgnore
    public iw2(String str) {
        super(str);
    }

    @DexIgnore
    public static iw2 zza() {
        return new iw2("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either that the input has been truncated or that an embedded message misreported its own length.");
    }

    @DexIgnore
    public static iw2 zzb() {
        return new iw2("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    @DexIgnore
    public static iw2 zzc() {
        return new iw2("CodedInputStream encountered a malformed varint.");
    }

    @DexIgnore
    public static iw2 zzd() {
        return new iw2("Protocol message contained an invalid tag (zero).");
    }

    @DexIgnore
    public static iw2 zze() {
        return new iw2("Protocol message end-group tag did not match expected tag.");
    }

    @DexIgnore
    public static lw2 zzf() {
        return new lw2("Protocol message tag had invalid wire type.");
    }

    @DexIgnore
    public static iw2 zzg() {
        return new iw2("Failed to parse the message.");
    }

    @DexIgnore
    public static iw2 zzh() {
        return new iw2("Protocol message had invalid UTF-8.");
    }
}
