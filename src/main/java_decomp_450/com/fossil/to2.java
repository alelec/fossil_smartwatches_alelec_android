package com.fossil;

import android.app.Activity;
import android.os.RemoteException;
import com.fossil.sn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class to2 extends sn2.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity e;
    @DexIgnore
    public /* final */ /* synthetic */ sn2.b f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public to2(sn2.b bVar, Activity activity) {
        super(sn2.this);
        this.f = bVar;
        this.e = activity;
    }

    @DexIgnore
    @Override // com.fossil.sn2.a
    public final void a() throws RemoteException {
        sn2.this.h.onActivityPaused(cb2.a(this.e), ((sn2.a) this).b);
    }
}
