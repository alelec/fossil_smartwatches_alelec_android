package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.lifecycle.Lifecycle;
import coil.target.ImageViewTarget;
import com.fossil.fo7;
import com.fossil.ht;
import com.fossil.it;
import com.misfit.frameworks.common.constants.Constants;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ft extends jt<ft> {
    @DexIgnore
    public Drawable A;
    @DexIgnore
    public Drawable B;
    @DexIgnore
    public Drawable C;
    @DexIgnore
    public /* final */ Context t;
    @DexIgnore
    public vt u; // = null;
    @DexIgnore
    public Lifecycle v; // = null;
    @DexIgnore
    public zt w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ft(Context context, kq kqVar) {
        super(kqVar, null);
        ee7.b(context, "context");
        ee7.b(kqVar, Constants.DEFAULTS);
        this.t = context;
        this.w = kqVar.h();
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.A = kqVar.f();
        this.B = kqVar.d();
        this.C = kqVar.e();
    }

    @DexIgnore
    public final ft a(ImageView imageView) {
        ee7.b(imageView, "imageView");
        a((vt) new ImageViewTarget(imageView));
        return this;
    }

    @DexIgnore
    public final ft b(Object obj) {
        a(obj);
        return this;
    }

    @DexIgnore
    public final et t() {
        Context context = this.t;
        Object f = f();
        vt vtVar = this.u;
        Lifecycle lifecycle = this.v;
        zt ztVar = this.w;
        String k = k();
        List<String> a = a();
        it.a l = l();
        st r = r();
        qt q = q();
        pt p = p();
        fr g = g();
        ti7 i = i();
        List<yt> s = s();
        Bitmap.Config d = d();
        ColorSpace e = e();
        fo7.a j = j();
        ht htVar = null;
        fo7 a2 = iu.a(j != null ? j.a() : null);
        ee7.a((Object) a2, "headers?.build().orEmpty()");
        ht.a o = o();
        if (o != null) {
            htVar = o.a();
        }
        return new et(context, f, vtVar, lifecycle, ztVar, k, a, l, r, q, p, g, i, s, d, e, a2, iu.a(htVar), n(), h(), m(), b(), c(), this.x, this.y, this.z, this.A, this.B, this.C);
    }

    @DexIgnore
    public final ft a(vt vtVar) {
        this.u = vtVar;
        return this;
    }

    @DexIgnore
    public final ft a(Drawable drawable) {
        this.A = drawable;
        this.x = 0;
        return this;
    }
}
