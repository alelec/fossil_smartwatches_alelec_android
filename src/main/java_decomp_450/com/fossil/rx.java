package com.fossil;

import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import com.fossil.jx;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rx implements jx<ParcelFileDescriptor> {
    @DexIgnore
    public /* final */ b a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements jx.a<ParcelFileDescriptor> {
        @DexIgnore
        @Override // com.fossil.jx.a
        public Class<ParcelFileDescriptor> getDataClass() {
            return ParcelFileDescriptor.class;
        }

        @DexIgnore
        public jx<ParcelFileDescriptor> a(ParcelFileDescriptor parcelFileDescriptor) {
            return new rx(parcelFileDescriptor);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ ParcelFileDescriptor a;

        @DexIgnore
        public b(ParcelFileDescriptor parcelFileDescriptor) {
            this.a = parcelFileDescriptor;
        }

        @DexIgnore
        public ParcelFileDescriptor a() throws IOException {
            try {
                Os.lseek(this.a.getFileDescriptor(), 0, OsConstants.SEEK_SET);
                return this.a;
            } catch (ErrnoException e) {
                throw new IOException(e);
            }
        }
    }

    @DexIgnore
    public rx(ParcelFileDescriptor parcelFileDescriptor) {
        this.a = new b(parcelFileDescriptor);
    }

    @DexIgnore
    public static boolean c() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    @Override // com.fossil.jx
    public void a() {
    }

    @DexIgnore
    @Override // com.fossil.jx
    public ParcelFileDescriptor b() throws IOException {
        return this.a.a();
    }
}
