package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface az {
    @DexIgnore
    <T> T a(int i, Class<T> cls);

    @DexIgnore
    void a();

    @DexIgnore
    void a(int i);

    @DexIgnore
    <T> void a(T t);

    @DexIgnore
    <T> T b(int i, Class<T> cls);
}
