package com.fossil;

import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.v02;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n42 {
    @DexIgnore
    public static /* final */ Status d; // = new Status(8, "The connection to Google Play services was lost");
    @DexIgnore
    public static /* final */ BasePendingResult<?>[] e; // = new BasePendingResult[0];
    @DexIgnore
    public /* final */ Set<BasePendingResult<?>> a; // = Collections.synchronizedSet(Collections.newSetFromMap(new WeakHashMap()));
    @DexIgnore
    public /* final */ r42 b; // = new q42(this);
    @DexIgnore
    public /* final */ Map<v02.c<?>, v02.f> c;

    @DexIgnore
    public n42(Map<v02.c<?>, v02.f> map) {
        this.c = map;
    }

    @DexIgnore
    public final void a(BasePendingResult<? extends i12> basePendingResult) {
        this.a.add(basePendingResult);
        basePendingResult.a(this.b);
    }

    @DexIgnore
    public final void b() {
        for (BasePendingResult basePendingResult : (BasePendingResult[]) this.a.toArray(e)) {
            basePendingResult.b(d);
        }
    }

    @DexIgnore
    public final void a() {
        BasePendingResult[] basePendingResultArr = (BasePendingResult[]) this.a.toArray(e);
        for (BasePendingResult basePendingResult : basePendingResultArr) {
            v52 v52 = null;
            basePendingResult.a((r42) null);
            if (basePendingResult.e() != null) {
                basePendingResult.a((j12) null);
                IBinder o = this.c.get(((r12) basePendingResult).i()).o();
                if (basePendingResult.d()) {
                    basePendingResult.a(new p42(basePendingResult, null, o, null));
                } else if (o == null || !o.isBinderAlive()) {
                    basePendingResult.a((r42) null);
                    basePendingResult.a();
                    v52.a(basePendingResult.e().intValue());
                } else {
                    p42 p42 = new p42(basePendingResult, null, o, null);
                    basePendingResult.a(p42);
                    try {
                        o.linkToDeath(p42, 0);
                    } catch (RemoteException unused) {
                        basePendingResult.a();
                        v52.a(basePendingResult.e().intValue());
                    }
                }
                this.a.remove(basePendingResult);
            } else if (basePendingResult.f()) {
                this.a.remove(basePendingResult);
            }
        }
    }
}
