package com.fossil;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.be5;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.ActivePreset;
import com.portfolio.platform.data.legacy.threedotzero.DeviceModel;
import com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.room.microapp.ButtonMapping;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.source.DeviceDao;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ll4 {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public /* final */ ch5 a;
    @DexIgnore
    public /* final */ UserRepository b;
    @DexIgnore
    public /* final */ NotificationsRepository c;
    @DexIgnore
    public /* final */ is6 d;
    @DexIgnore
    public /* final */ PortfolioApp e;
    @DexIgnore
    public /* final */ GoalTrackingRepository f;
    @DexIgnore
    public /* final */ DeviceDao g;
    @DexIgnore
    public /* final */ HybridCustomizeDatabase h;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository i;
    @DexIgnore
    public /* final */ ad5 j;
    @DexIgnore
    public /* final */ lm4 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ll4.l;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {324, 370, 375, 425}, m = "migrateFor2Dot0")
    public static final class b extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ll4 ll4, fb7 fb7) {
            super(fb7);
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.MigrationManager$migrateFor2Dot0$4", f = "MigrationManager.kt", l = {461}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ll4 ll4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                is6 d = this.this$0.d;
                es6 es6 = new es6(this.this$0.e.c());
                this.L$0 = yi7;
                this.label = 1;
                if (gl4.a(d, es6, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ LegacyGoalTrackingSettings $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(LegacyGoalTrackingSettings legacyGoalTrackingSettings, fb7 fb7, ll4 ll4) {
            super(2, fb7);
            this.$it = legacyGoalTrackingSettings;
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.$it, fb7, this.this$0);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            yi7 yi7;
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = ll4.m.a();
                local.d(a2, "Migrate goal tracking with target " + this.$it.getTarget() + " value " + this.$it.getValue());
                pg5 pg5 = pg5.i;
                this.L$0 = yi7;
                this.label = 1;
                obj2 = pg5.c(this);
                if (obj2 == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 = (yi7) this.L$0;
                t87.a(obj);
                obj2 = obj;
            } else if (i == 2) {
                ArrayList arrayList = (ArrayList) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
                return i97.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ((GoalTrackingDatabase) obj2).getGoalTrackingDao().upsertGoalSettings(new GoalSetting(this.$it.getTarget()));
            ArrayList arrayList2 = new ArrayList();
            int value = this.$it.getValue();
            for (int i2 = 0; i2 < value; i2++) {
                String uuid = UUID.randomUUID().toString();
                ee7.a((Object) uuid, "UUID.randomUUID().toString()");
                Date date = new Date();
                TimeZone timeZone = TimeZone.getDefault();
                ee7.a((Object) timeZone, "TimeZone.getDefault()");
                DateTime a3 = zd5.a(date, timeZone.getRawOffset() / 1000);
                ee7.a((Object) a3, "DateHelper.createDateTim\u2026fault().rawOffset / 1000)");
                TimeZone timeZone2 = TimeZone.getDefault();
                ee7.a((Object) timeZone2, "TimeZone.getDefault()");
                arrayList2.add(new GoalTrackingData(uuid, a3, timeZone2.getRawOffset() / 1000, new Date(), new Date().getTime(), new Date().getTime()));
            }
            GoalTrackingRepository e = this.this$0.f;
            List<GoalTrackingData> d = ea7.d((Collection) arrayList2);
            this.L$0 = yi7;
            this.L$1 = arrayList2;
            this.label = 2;
            if (e.insertFromDevice(d, this) == a) {
                return a;
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.MigrationManager$migrateFor4Dot0$7", f = "MigrationManager.kt", l = {263}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ll4 ll4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                is6 d = this.this$0.d;
                es6 es6 = new es6(this.this$0.e.c());
                this.L$0 = yi7;
                this.label = 1;
                if (gl4.a(d, es6, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ll4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public f(ll4 ll4, ArrayList arrayList, List list) {
            this.a = ll4;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        public final void run() {
            this.a.h.microAppDao().upsertListMicroApp(this.b);
            this.a.h.presetDao().upsertPresetList(this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.MigrationManager", f = "MigrationManager.kt", l = {124, 127}, m = "processMigration")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(ll4 ll4, fb7 fb7) {
            super(fb7);
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.MigrationManager$upgrade$2", f = "MigrationManager.kt", l = {81, 86, 90, 97, 99, 100}, m = "invokeSuspend")
    public static final class h extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ll4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.MigrationManager$upgrade$2$1", f = "MigrationManager.kt", l = {111}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    yw6 a2 = yw6.h.a();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (a2.a(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(ll4 ll4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ll4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            h hVar = new h(this.this$0, fb7);
            hVar.p$ = (yi7) obj;
            return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
            return ((h) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:37:0x015f A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x01c4 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:53:0x01c9  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x0212 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x0213  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
                r12 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r12.label
                r2 = 0
                r3 = 0
                r4 = 1
                switch(r1) {
                    case 0: goto L_0x008c;
                    case 1: goto L_0x0079;
                    case 2: goto L_0x0064;
                    case 3: goto L_0x0053;
                    case 4: goto L_0x0041;
                    case 5: goto L_0x0029;
                    case 6: goto L_0x0014;
                    default: goto L_0x000c;
                }
            L_0x000c:
                java.lang.IllegalStateException r13 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r13.<init>(r0)
                throw r13
            L_0x0014:
                java.lang.Object r0 = r12.L$3
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
                goto L_0x0215
            L_0x0029:
                java.lang.Object r1 = r12.L$3
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r5 = r12.L$2
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$1
                java.lang.String r6 = (java.lang.String) r6
                java.lang.Object r7 = r12.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r13)
                r13 = r1
                r1 = r5
                r5 = r6
                goto L_0x01fb
            L_0x0041:
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
            L_0x0050:
                r7 = r6
                goto L_0x01c5
            L_0x0053:
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
            L_0x005f:
                com.fossil.t87.a(r13)     // Catch:{ Exception -> 0x0089 }
                goto L_0x01af
            L_0x0064:
                java.lang.Object r1 = r12.L$4
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r1 = r12.L$3
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                goto L_0x005f
            L_0x0079:
                java.lang.Object r1 = r12.L$2
                java.lang.String r1 = (java.lang.String) r1
                java.lang.Object r5 = r12.L$1
                java.lang.String r5 = (java.lang.String) r5
                java.lang.Object r6 = r12.L$0
                com.fossil.yi7 r6 = (com.fossil.yi7) r6
                com.fossil.t87.a(r13)
                goto L_0x00e8
            L_0x0089:
                r13 = move-exception
                goto L_0x017f
            L_0x008c:
                com.fossil.t87.a(r13)
                com.fossil.yi7 r6 = r12.p$
                com.fossil.ll4 r13 = r12.this$0
                com.fossil.ch5 r13 = r13.a
                java.lang.String r5 = r13.p()
                com.fossil.ll4 r13 = r12.this$0
                com.portfolio.platform.PortfolioApp r13 = r13.e
                java.lang.String r1 = r13.g()
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                com.fossil.ll4$a r7 = com.fossil.ll4.m
                java.lang.String r7 = r7.a()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "onUpgrade for appVersion "
                r8.append(r9)
                r8.append(r1)
                java.lang.String r9 = " lastVersion "
                r8.append(r9)
                r8.append(r5)
                java.lang.String r8 = r8.toString()
                r13.d(r7, r8)
                boolean r13 = android.text.TextUtils.isEmpty(r5)
                if (r13 == 0) goto L_0x0160
                com.fossil.ll4 r13 = r12.this$0
                com.portfolio.platform.data.source.UserRepository r13 = r13.b
                r12.L$0 = r6
                r12.L$1 = r5
                r12.L$2 = r1
                r12.label = r4
                java.lang.Object r13 = r13.getCurrentUser(r12)
                if (r13 != r0) goto L_0x00e8
                return r0
            L_0x00e8:
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                com.fossil.jx6 r7 = com.fossil.jx6.a()
                com.fossil.ll4 r8 = r12.this$0
                com.portfolio.platform.PortfolioApp r8 = r8.e
                java.lang.String r7 = r7.f(r8)
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                com.fossil.ll4$a r9 = com.fossil.ll4.m
                java.lang.String r9 = r9.a()
                java.lang.StringBuilder r10 = new java.lang.StringBuilder
                r10.<init>()
                java.lang.String r11 = "userToken "
                r10.append(r11)
                if (r13 == 0) goto L_0x011b
                com.portfolio.platform.data.model.MFUser$Auth r11 = r13.getAuth()
                if (r11 == 0) goto L_0x011b
                java.lang.String r11 = r11.getAccessToken()
                goto L_0x011c
            L_0x011b:
                r11 = r3
            L_0x011c:
                r10.append(r11)
                java.lang.String r11 = " legacyToken "
                r10.append(r11)
                r10.append(r7)
                java.lang.String r10 = r10.toString()
                r8.d(r9, r10)
                if (r13 == 0) goto L_0x013b
                com.portfolio.platform.data.model.MFUser$Auth r8 = r13.getAuth()
                if (r8 == 0) goto L_0x013b
                java.lang.String r8 = r8.getAccessToken()
                goto L_0x013c
            L_0x013b:
                r8 = r3
            L_0x013c:
                boolean r8 = android.text.TextUtils.isEmpty(r8)
                if (r8 == 0) goto L_0x01af
                boolean r8 = android.text.TextUtils.isEmpty(r7)
                if (r8 != 0) goto L_0x01af
                com.fossil.ll4 r8 = r12.this$0
                java.lang.String r9 = "1.14.2"
                r12.L$0 = r6
                r12.L$1 = r5
                r12.L$2 = r1
                r12.L$3 = r13
                r12.L$4 = r7
                r13 = 2
                r12.label = r13
                java.lang.Object r13 = r8.a(r9, r12)
                if (r13 != r0) goto L_0x01af
                return r0
            L_0x0160:
                boolean r13 = com.fossil.ee7.a(r5, r1)
                r13 = r13 ^ r4
                if (r13 == 0) goto L_0x01af
                com.fossil.ll4 r13 = r12.this$0
                if (r5 == 0) goto L_0x017b
                r12.L$0 = r6
                r12.L$1 = r5
                r12.L$2 = r1
                r7 = 3
                r12.label = r7
                java.lang.Object r13 = r13.a(r5, r12)
                if (r13 != r0) goto L_0x01af
                return r0
            L_0x017b:
                com.fossil.ee7.a()
                throw r3
            L_0x017f:
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
                com.fossil.ll4$a r8 = com.fossil.ll4.m
                java.lang.String r8 = r8.a()
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r9.<init>()
                java.lang.String r10 = "Exception when start upgrade from version "
                r9.append(r10)
                r9.append(r5)
                java.lang.String r10 = " to version="
                r9.append(r10)
                r9.append(r1)
                java.lang.String r10 = ", exception="
                r9.append(r10)
                r9.append(r13)
                java.lang.String r13 = r9.toString()
                r7.e(r8, r13)
            L_0x01af:
                com.fossil.ll4 r13 = r12.this$0
                com.portfolio.platform.data.source.UserRepository r13 = r13.b
                r12.L$0 = r6
                r12.L$1 = r5
                r12.L$2 = r1
                r7 = 4
                r12.label = r7
                java.lang.Object r13 = r13.getCurrentUser(r12)
                if (r13 != r0) goto L_0x0050
                return r0
            L_0x01c5:
                com.portfolio.platform.data.model.MFUser r13 = (com.portfolio.platform.data.model.MFUser) r13
                if (r13 == 0) goto L_0x0232
                com.fossil.ll4 r6 = r12.this$0
                com.fossil.lm4 r6 = r6.k
                com.portfolio.platform.PortfolioApp$a r8 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r8 = r8.c()
                java.lang.String r8 = r8.c()
                com.fossil.xe5 r9 = com.fossil.xe5.b
                java.lang.String r9 = r9.a()
                java.lang.String[] r10 = new java.lang.String[r4]
                com.fossil.gm4 r11 = com.fossil.gm4.BUDDY_CHALLENGE
                java.lang.String r11 = r11.getStrType()
                r10[r2] = r11
                r12.L$0 = r7
                r12.L$1 = r5
                r12.L$2 = r1
                r12.L$3 = r13
                r11 = 5
                r12.label = r11
                java.lang.Object r6 = r6.a(r8, r9, r10, r12)
                if (r6 != r0) goto L_0x01fb
                return r0
            L_0x01fb:
                com.fossil.ll4 r6 = r12.this$0
                com.fossil.lm4 r6 = r6.k
                r12.L$0 = r7
                r12.L$1 = r5
                r12.L$2 = r1
                r12.L$3 = r13
                r7 = 6
                r12.label = r7
                java.lang.Object r6 = r6.a(r12)
                if (r6 != r0) goto L_0x0213
                return r0
            L_0x0213:
                r0 = r13
                r13 = r6
            L_0x0215:
                java.lang.Boolean r13 = (java.lang.Boolean) r13
                boolean r13 = r13.booleanValue()
                com.fossil.ll4 r6 = r12.this$0
                com.fossil.ch5 r6 = r6.a
                java.lang.Boolean r7 = com.fossil.pb7.a(r13)
                r6.b(r7)
                com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r6 = r6.c()
                r6.a(r13)
                r13 = r0
            L_0x0232:
                com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r0 = r0.c()
                r0.a(r13)
                com.fossil.ll4 r13 = r12.this$0
                r13.a()
                com.fossil.ll4 r13 = r12.this$0
                com.fossil.ch5 r13 = r13.a
                r13.r(r1)
                com.fossil.ll4 r13 = r12.this$0
                com.fossil.ch5 r13 = r13.a
                r13.a(r4, r1)
                com.fossil.ll4$h$a r13 = new com.fossil.ll4$h$a
                r13.<init>(r3)
                java.lang.Object unused = com.fossil.wh7.a(r3, r13, r4, r3)
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
                com.fossil.ll4$a r0 = com.fossil.ll4.m
                java.lang.String r0 = r0.a()
                java.lang.String r3 = "trigger full sync after upgrade"
                r13.d(r0, r3)
                com.fossil.ll4 r13 = r12.this$0
                com.portfolio.platform.PortfolioApp r13 = r13.e
                com.fossil.ll4 r0 = r12.this$0
                com.fossil.ad5 r0 = r0.j
                r3 = 13
                r13.a(r0, r2, r3)
                com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r6 = r13.getRemote()
                com.misfit.frameworks.buttonservice.log.FLogger$Component r7 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
                com.misfit.frameworks.buttonservice.log.FLogger$Session r8 = com.misfit.frameworks.buttonservice.log.FLogger.Session.MIGRATION
                com.fossil.ll4 r13 = r12.this$0
                com.portfolio.platform.PortfolioApp r13 = r13.e
                java.lang.String r9 = r13.c()
                com.fossil.ll4$a r13 = com.fossil.ll4.m
                java.lang.String r10 = r13.a()
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r0 = "User update app from version "
                r13.append(r0)
                r13.append(r5)
                java.lang.String r0 = " to app version "
                r13.append(r0)
                r13.append(r1)
                java.lang.String r0 = " at "
                r13.append(r0)
                long r0 = java.lang.System.currentTimeMillis()
                r13.append(r0)
                java.lang.String r11 = r13.toString()
                r6.i(r7, r8, r9, r10, r11)
                java.lang.Boolean r13 = com.fossil.pb7.a(r4)
                return r13
                switch-data {0->0x008c, 1->0x0079, 2->0x0064, 3->0x0053, 4->0x0041, 5->0x0029, 6->0x0014, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.h.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = ll4.class.getSimpleName();
        ee7.a((Object) simpleName, "MigrationManager::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public ll4(ch5 ch5, UserRepository userRepository, NotificationsRepository notificationsRepository, is6 is6, PortfolioApp portfolioApp, GoalTrackingRepository goalTrackingRepository, DeviceDao deviceDao, HybridCustomizeDatabase hybridCustomizeDatabase, MicroAppLastSettingRepository microAppLastSettingRepository, ad5 ad5, lm4 lm4) {
        ee7.b(ch5, "mSharedPrefs");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(notificationsRepository, "mNotificationRepository");
        ee7.b(is6, "mGetHybridDeviceSettingUseCase");
        ee7.b(portfolioApp, "mApp");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(deviceDao, "mDeviceDao");
        ee7.b(hybridCustomizeDatabase, "mHybridCustomizeDatabase");
        ee7.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(lm4, "flagRepository");
        this.a = ch5;
        this.b = userRepository;
        this.c = notificationsRepository;
        this.d = is6;
        this.e = portfolioApp;
        this.f = goalTrackingRepository;
        this.g = deviceDao;
        this.h = hybridCustomizeDatabase;
        this.i = microAppLastSettingRepository;
        this.j = ad5;
        this.k = lm4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(java.lang.String r10, com.fossil.fb7<? super com.fossil.i97> r11) {
        /*
            r9 = this;
            boolean r0 = r11 instanceof com.fossil.ll4.g
            if (r0 == 0) goto L_0x0013
            r0 = r11
            com.fossil.ll4$g r0 = (com.fossil.ll4.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.ll4$g r0 = new com.fossil.ll4$g
            r0.<init>(r9, r11)
        L_0x0018:
            java.lang.Object r11 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r3 = "4.0.0"
            r4 = 2
            r5 = 1
            java.lang.String r6 = "2.0.0"
            if (r2 == 0) goto L_0x004d
            if (r2 == r5) goto L_0x0041
            if (r2 != r4) goto L_0x0039
            java.lang.Object r10 = r0.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$0
            com.fossil.ll4 r10 = (com.fossil.ll4) r10
            com.fossil.t87.a(r11)
            goto L_0x00b6
        L_0x0039:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
            java.lang.String r11 = "call to 'resume' before 'invoke' with coroutine"
            r10.<init>(r11)
            throw r10
        L_0x0041:
            java.lang.Object r10 = r0.L$1
            java.lang.String r10 = (java.lang.String) r10
            java.lang.Object r10 = r0.L$0
            com.fossil.ll4 r10 = (com.fossil.ll4) r10
            com.fossil.t87.a(r11)
            goto L_0x009a
        L_0x004d:
            com.fossil.t87.a(r11)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.fossil.ll4.l
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "processMigration from "
            r7.append(r8)
            r7.append(r10)
            java.lang.String r8 = " value "
            r7.append(r8)
            int r8 = com.fossil.he5.a(r10, r6)
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r11.d(r2, r7)
            int r11 = com.fossil.he5.a(r10, r6)
            r2 = -1
            if (r11 != r2) goto L_0x00a0
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = com.fossil.ll4.l
            java.lang.String r3 = "processMigration for 1.5"
            r11.d(r2, r3)
            r0.L$0 = r9
            r0.L$1 = r10
            r0.label = r5
            java.lang.Object r10 = r9.a(r0)
            if (r10 != r1) goto L_0x0099
            return r1
        L_0x0099:
            r10 = r9
        L_0x009a:
            com.fossil.ch5 r10 = r10.a
            r10.r(r6)
            goto L_0x00fd
        L_0x00a0:
            int r11 = com.fossil.he5.a(r10, r3)
            if (r11 != r2) goto L_0x00fd
            com.portfolio.platform.data.source.UserRepository r11 = r9.b
            r0.L$0 = r9
            r0.L$1 = r10
            r0.label = r4
            java.lang.Object r11 = r11.getCurrentUser(r0)
            if (r11 != r1) goto L_0x00b5
            return r1
        L_0x00b5:
            r10 = r9
        L_0x00b6:
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "processMigration for 4.0.0 userId "
            r2.append(r4)
            if (r11 == 0) goto L_0x00d1
            java.lang.String r4 = r11.getUserId()
            goto L_0x00d2
        L_0x00d1:
            r4 = 0
        L_0x00d2:
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            if (r11 == 0) goto L_0x00f3
            java.lang.String r11 = r11.getUserId()
            r10.a(r11)
            com.fossil.ch5 r11 = r10.a
            com.portfolio.platform.PortfolioApp r0 = r10.e
            java.lang.String r0 = r0.c()
            r1 = 0
            r4 = 0
            r11.a(r0, r1, r4)
        L_0x00f3:
            com.fossil.ch5 r10 = r10.a
            r10.r(r3)
            com.portfolio.platform.workers.PushPendingDataWorker$a r10 = com.portfolio.platform.workers.PushPendingDataWorker.G
            r10.a()
        L_0x00fd:
            com.fossil.i97 r10 = com.fossil.i97.a
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.a(java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public Object b(fb7<? super Boolean> fb7) {
        return vh7.a(qj7.b(), new h(this, null), fb7);
    }

    @DexIgnore
    public void b() {
        List<DeviceModel> allDevice = ah5.p.a().c().getAllDevice();
        String c2 = this.e.c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "migrationDeviceData - allDevices=" + allDevice + ", activeSerial=" + c2);
        try {
            boolean z = !TextUtils.isEmpty(c2) && !be5.o.e(c2);
            if (z) {
                this.a.o("");
                this.a.l(true);
            }
            if (allDevice == null) {
                return;
            }
            if (!allDevice.isEmpty()) {
                for (DeviceModel deviceModel : allDevice) {
                    if (deviceModel != null && !TextUtils.isEmpty(deviceModel.getDeviceId())) {
                        String deviceId = deviceModel.getDeviceId();
                        String macAddress = deviceModel.getMacAddress();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = l;
                        local2.d(str2, "migrationDeviceData - step1: deviceId=" + deviceId + ", deviceAddress=" + macAddress);
                        be5.a aVar = be5.o;
                        String deviceId2 = deviceModel.getDeviceId();
                        if (deviceId2 == null) {
                            ee7.a();
                            throw null;
                        } else if (!aVar.e(deviceId2)) {
                            ah5.p.a().c().removeDevice(deviceId);
                        }
                    }
                }
                if (z) {
                    try {
                        IButtonConnectivity b2 = PortfolioApp.g0.b();
                        if (b2 != null) {
                            b2.removeActiveSerial(c2);
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = l;
                        local3.d(str3, "Exception when remove legacy device in button service ,keep going e=" + e2);
                    }
                }
                for (DeviceModel deviceModel2 : allDevice) {
                    if (deviceModel2 != null && !TextUtils.isEmpty(deviceModel2.getDeviceId())) {
                        String deviceId3 = deviceModel2.getDeviceId();
                        String macAddress2 = deviceModel2.getMacAddress();
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str4 = l;
                        local4.d(str4, "migrationDeviceData - step2: - deviceId=" + deviceId3 + ", deviceAddress=" + macAddress2);
                        be5.a aVar2 = be5.o;
                        String deviceId4 = deviceModel2.getDeviceId();
                        if (deviceId4 == null) {
                            ee7.a();
                            throw null;
                        } else if (aVar2.e(deviceId4)) {
                            if (mh7.b(deviceModel2.getDeviceId(), c2, true)) {
                                PortfolioApp portfolioApp = this.e;
                                String deviceId5 = deviceModel2.getDeviceId();
                                if (deviceId5 != null) {
                                    portfolioApp.c(deviceId5, deviceModel2.getMacAddress());
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                            IButtonConnectivity b3 = PortfolioApp.g0.b();
                            if (b3 != null) {
                                b3.setPairedSerial(deviceModel2.getDeviceId(), deviceModel2.getMacAddress());
                            } else {
                                ee7.a();
                                throw null;
                            }
                        } else {
                            IButtonConnectivity b4 = PortfolioApp.g0.b();
                            if (b4 != null) {
                                b4.removePairedSerial(deviceModel2.getDeviceId());
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                    }
                }
                this.a.m(false);
            }
        } catch (Exception e3) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = l;
            local5.e(str5, "migrationDeviceData - Exception when upgrade to 1.14.0 version e=" + e3);
            e3.printStackTrace();
            this.a.m(true);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:41:0x021e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0223  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r29) {
        /*
            r28 = this;
            r7 = r28
            java.lang.String r0 = "it.timezoneId"
            com.google.gson.Gson r1 = new com.google.gson.Gson
            r1.<init>()
            com.portfolio.platform.PortfolioApp r2 = r7.e
            java.lang.String r8 = r2.c()
            com.fossil.ah5$a r2 = com.fossil.ah5.p
            com.fossil.ah5 r2 = r2.a()
            com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r9 = r2.c()
            java.util.List r10 = r9.getAllDevice()
            java.lang.String r2 = "general"
            java.lang.String[] r2 = new java.lang.String[]{r2}
            java.util.ArrayList r20 = com.fossil.w97.a(r2)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            com.fossil.ah5$a r2 = com.fossil.ah5.p
            com.fossil.ah5 r2 = r2.a()
            com.fossil.ci5 r2 = r2.g()
            java.util.List r2 = r2.c()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.lang.String r3 = "allDevices"
            com.fossil.ee7.a(r10, r3)
            boolean r3 = r10.isEmpty()
            r15 = 1
            r3 = r3 ^ r15
            r14 = 0
            if (r3 == 0) goto L_0x041f
            java.util.Iterator r2 = r2.iterator()
        L_0x0056:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0234
            java.lang.Object r3 = r2.next()
            com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting r3 = (com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting) r3
            java.lang.String r11 = "microAppSetting"
            com.fossil.ee7.a(r3, r11)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r11 = r3.getMicroAppId()     // Catch:{ Exception -> 0x0208 }
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r12 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID     // Catch:{ Exception -> 0x0208 }
            java.lang.String r12 = r12.getValue()     // Catch:{ Exception -> 0x0208 }
            boolean r12 = com.fossil.ee7.a(r11, r12)     // Catch:{ Exception -> 0x0208 }
            if (r12 == 0) goto L_0x00a3
            java.lang.String r11 = r3.getSetting()     // Catch:{ Exception -> 0x0208 }
            java.lang.Class<com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings> r12 = com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings.class
            java.lang.Object r11 = r1.a(r11, r12)     // Catch:{ Exception -> 0x0208 }
            com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings r11 = (com.portfolio.platform.data.legacy.threedotzero.LegacyGoalTrackingSettings) r11     // Catch:{ Exception -> 0x0208 }
            if (r11 == 0) goto L_0x009f
            com.fossil.ti7 r12 = com.fossil.qj7.b()     // Catch:{ Exception -> 0x0208 }
            com.fossil.yi7 r21 = com.fossil.zi7.a(r12)     // Catch:{ Exception -> 0x0208 }
            r22 = 0
            r23 = 0
            com.fossil.ll4$d r12 = new com.fossil.ll4$d     // Catch:{ Exception -> 0x0208 }
            r12.<init>(r11, r14, r7)     // Catch:{ Exception -> 0x0208 }
            r25 = 3
            r26 = 0
            r24 = r12
            com.fossil.ik7 unused = com.fossil.xh7.b(r21, r22, r23, r24, r25, r26)     // Catch:{ Exception -> 0x0208 }
        L_0x009f:
            r18 = r2
            goto L_0x022e
        L_0x00a3:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r12 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_RING_PHONE     // Catch:{ Exception -> 0x0208 }
            java.lang.String r12 = r12.getValue()     // Catch:{ Exception -> 0x0208 }
            boolean r12 = com.fossil.ee7.a(r11, r12)     // Catch:{ Exception -> 0x0208 }
            if (r12 == 0) goto L_0x016c
            java.lang.String r11 = r3.getSetting()     // Catch:{ Exception -> 0x0208 }
            java.lang.Class<com.portfolio.platform.data.legacy.threedotzero.LegacyRingtoneSetting> r12 = com.portfolio.platform.data.legacy.threedotzero.LegacyRingtoneSetting.class
            java.lang.Object r11 = r1.a(r11, r12)     // Catch:{ Exception -> 0x0208 }
            com.portfolio.platform.data.legacy.threedotzero.LegacyRingtoneSetting r11 = (com.portfolio.platform.data.legacy.threedotzero.LegacyRingtoneSetting) r11     // Catch:{ Exception -> 0x0208 }
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0208 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()     // Catch:{ Exception -> 0x0208 }
            java.lang.String r13 = com.fossil.ll4.l     // Catch:{ Exception -> 0x0208 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0208 }
            r14.<init>()     // Catch:{ Exception -> 0x0208 }
            java.lang.String r15 = "legacyRingtoneSeting "
            r14.append(r15)     // Catch:{ Exception -> 0x0208 }
            r14.append(r11)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0208 }
            r12.d(r13, r14)     // Catch:{ Exception -> 0x0208 }
            java.lang.String r12 = "microAppSetting.microAppId"
            if (r11 == 0) goto L_0x00fc
            com.portfolio.platform.data.model.Ringtone r13 = new com.portfolio.platform.data.model.Ringtone
            java.lang.String r14 = r11.getName()
            java.lang.String r15 = "legacyRingtoneSetting.name"
            com.fossil.ee7.a(r14, r15)
            java.lang.String r11 = r11.getId()
            r13.<init>(r14, r11)
            java.lang.String r11 = r3.getMicroAppId()
            com.fossil.ee7.a(r11, r12)
            java.lang.String r12 = com.fossil.wc5.a(r13)
            r4.put(r11, r12)
            goto L_0x009f
        L_0x00fc:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r13 = com.fossil.ll4.l
            java.lang.String r14 = "No legacy ringtone setting, set default instead"
            r11.d(r13, r14)
            com.fossil.rd5$a r11 = com.fossil.rd5.g
            java.util.List r11 = r11.d()
            if (r11 == 0) goto L_0x009f
            boolean r13 = r11.isEmpty()
            r15 = 1
            r13 = r13 ^ r15
            if (r13 == 0) goto L_0x009f
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.fossil.ll4.l
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r18 = r2
            java.lang.String r2 = "defaultRingtone "
            r15.append(r2)     // Catch:{ Exception -> 0x0206 }
            r2 = 0
            java.lang.Object r19 = r11.get(r2)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.model.Ringtone r19 = (com.portfolio.platform.data.model.Ringtone) r19     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r19.getRingtoneName()     // Catch:{ Exception -> 0x0206 }
            r15.append(r2)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r15.toString()     // Catch:{ Exception -> 0x0206 }
            r13.d(r14, r2)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.model.Ringtone r2 = new com.portfolio.platform.data.model.Ringtone     // Catch:{ Exception -> 0x0206 }
            r13 = 0
            java.lang.Object r14 = r11.get(r13)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.model.Ringtone r14 = (com.portfolio.platform.data.model.Ringtone) r14     // Catch:{ Exception -> 0x0206 }
            java.lang.String r14 = r14.getRingtoneName()     // Catch:{ Exception -> 0x0206 }
            java.lang.Object r11 = r11.get(r13)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.model.Ringtone r11 = (com.portfolio.platform.data.model.Ringtone) r11     // Catch:{ Exception -> 0x0206 }
            java.lang.String r11 = r11.getRingtoneId()     // Catch:{ Exception -> 0x0206 }
            r2.<init>(r14, r11)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r11 = r3.getMicroAppId()     // Catch:{ Exception -> 0x0206 }
            com.fossil.ee7.a(r11, r12)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = com.fossil.wc5.a(r2)     // Catch:{ Exception -> 0x0206 }
            r4.put(r11, r2)     // Catch:{ Exception -> 0x0206 }
            goto L_0x022e
        L_0x016c:
            r18 = r2
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_TIME2_ID     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r2.getValue()     // Catch:{ Exception -> 0x0206 }
            boolean r2 = com.fossil.ee7.a(r11, r2)     // Catch:{ Exception -> 0x0206 }
            if (r2 == 0) goto L_0x01b9
            java.lang.String r2 = r3.getSetting()     // Catch:{ Exception -> 0x0206 }
            java.lang.Class<com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting> r11 = com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting.class
            java.lang.Object r2 = r1.a(r2, r11)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r2 = (com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting) r2     // Catch:{ Exception -> 0x0206 }
            if (r2 == 0) goto L_0x022e
            com.portfolio.platform.data.model.setting.SecondTimezoneSetting r11 = new com.portfolio.platform.data.model.setting.SecondTimezoneSetting     // Catch:{ Exception -> 0x0206 }
            java.lang.String r12 = r2.getTimezoneCityName()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r13 = "it.timezoneCityName"
            com.fossil.ee7.a(r12, r13)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r13 = r2.getTimezoneId()     // Catch:{ Exception -> 0x0206 }
            com.fossil.ee7.a(r13, r0)     // Catch:{ Exception -> 0x0206 }
            long r14 = r2.getTimezoneOffset()     // Catch:{ Exception -> 0x0206 }
            int r15 = (int) r14     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r2.getTimezoneId()     // Catch:{ Exception -> 0x0206 }
            com.fossil.ee7.a(r2, r0)     // Catch:{ Exception -> 0x0206 }
            r11.<init>(r12, r13, r15, r2)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r3.getMicroAppId()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r11 = com.fossil.wc5.a(r11)     // Catch:{ Exception -> 0x0206 }
            java.lang.Object r2 = r4.put(r2, r11)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0206 }
            goto L_0x022e
        L_0x01b9:
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r2 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r2.getValue()     // Catch:{ Exception -> 0x0206 }
            boolean r2 = com.fossil.ee7.a(r11, r2)     // Catch:{ Exception -> 0x0206 }
            if (r2 == 0) goto L_0x022e
            java.lang.String r2 = r3.getSetting()     // Catch:{ Exception -> 0x0206 }
            java.lang.Class<com.portfolio.platform.data.legacy.threedotzero.LegacyCommuteTimeSettings> r11 = com.portfolio.platform.data.legacy.threedotzero.LegacyCommuteTimeSettings.class
            java.lang.Object r2 = r1.a(r2, r11)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.legacy.threedotzero.LegacyCommuteTimeSettings r2 = (com.portfolio.platform.data.legacy.threedotzero.LegacyCommuteTimeSettings) r2     // Catch:{ Exception -> 0x0206 }
            if (r2 == 0) goto L_0x022e
            com.portfolio.platform.data.model.setting.CommuteTimeSetting r11 = new com.portfolio.platform.data.model.setting.CommuteTimeSetting     // Catch:{ Exception -> 0x0206 }
            java.lang.String r12 = r2.getDestination()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r13 = "it.destination"
            com.fossil.ee7.a(r12, r13)     // Catch:{ Exception -> 0x0206 }
            com.portfolio.platform.data.legacy.threedotzero.LegacyCommuteTimeSettings$TIME_FORMAT r13 = r2.getTimeFormat()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r23 = r13.toString()     // Catch:{ Exception -> 0x0206 }
            boolean r24 = r2.isIsAvoidTolls()     // Catch:{ Exception -> 0x0206 }
            r25 = 0
            r26 = 8
            r27 = 0
            r21 = r11
            r22 = r12
            r21.<init>(r22, r23, r24, r25, r26, r27)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = r3.getMicroAppId()     // Catch:{ Exception -> 0x0206 }
            java.lang.String r11 = com.fossil.wc5.a(r11)     // Catch:{ Exception -> 0x0206 }
            java.lang.Object r2 = r4.put(r2, r11)     // Catch:{ Exception -> 0x0206 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0206 }
            goto L_0x022e
        L_0x0206:
            goto L_0x020a
        L_0x0208:
            r18 = r2
        L_0x020a:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r11 = com.fossil.ll4.l
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "Exception when parsing micro app setting "
            r12.append(r13)
            if (r3 == 0) goto L_0x0223
            java.lang.String r3 = r3.getSetting()
            goto L_0x0224
        L_0x0223:
            r3 = 0
        L_0x0224:
            r12.append(r3)
            java.lang.String r3 = r12.toString()
            r2.d(r11, r3)
        L_0x022e:
            r2 = r18
            r14 = 0
            r15 = 1
            goto L_0x0056
        L_0x0234:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            java.lang.String r2 = "Calendar.getInstance()"
            com.fossil.ee7.a(r1, r2)
            java.util.Date r1 = r1.getTime()
            java.lang.String r1 = com.fossil.zd5.y(r1)
            java.util.Set r2 = r4.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x0252:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0278
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r11 = r3.getKey()
            java.lang.String r11 = (java.lang.String) r11
            java.lang.Object r3 = r3.getValue()
            java.lang.String r3 = (java.lang.String) r3
            com.portfolio.platform.data.model.microapp.MicroAppLastSetting r12 = new com.portfolio.platform.data.model.microapp.MicroAppLastSetting
            java.lang.String r13 = "updatedAt"
            com.fossil.ee7.a(r1, r13)
            r12.<init>(r11, r1, r3)
            r0.add(r12)
            goto L_0x0252
        L_0x0278:
            com.portfolio.platform.data.source.MicroAppLastSettingRepository r1 = r7.i
            r1.upsertMicroAppLastSettingList(r0)
            java.util.Iterator r21 = r10.iterator()
        L_0x0281:
            boolean r0 = r21.hasNext()
            if (r0 == 0) goto L_0x041f
            java.lang.Object r0 = r21.next()
            com.portfolio.platform.data.legacy.threedotzero.DeviceModel r0 = (com.portfolio.platform.data.legacy.threedotzero.DeviceModel) r0
            java.lang.String r1 = r0.getDeviceId()     // Catch:{ Exception -> 0x03f6 }
            java.util.List r3 = r9.getListMicroApp(r1)     // Catch:{ Exception -> 0x03f6 }
            java.lang.String r1 = "legacyMicroApps"
            com.fossil.ee7.a(r3, r1)     // Catch:{ Exception -> 0x03f6 }
            java.util.Iterator r1 = r3.iterator()     // Catch:{ Exception -> 0x03f6 }
        L_0x029e:
            boolean r2 = r1.hasNext()     // Catch:{ Exception -> 0x03f6 }
            if (r2 == 0) goto L_0x0301
            java.lang.Object r2 = r1.next()     // Catch:{ Exception -> 0x02fc }
            com.portfolio.platform.data.legacy.threedotzero.MicroApp r2 = (com.portfolio.platform.data.legacy.threedotzero.MicroApp) r2     // Catch:{ Exception -> 0x02fc }
            com.portfolio.platform.data.model.room.microapp.MicroApp r15 = new com.portfolio.platform.data.model.room.microapp.MicroApp     // Catch:{ Exception -> 0x02fc }
            java.lang.String r11 = "it"
            com.fossil.ee7.a(r2, r11)     // Catch:{ Exception -> 0x02fc }
            java.lang.String r12 = r2.getAppId()     // Catch:{ Exception -> 0x02fc }
            java.lang.String r11 = "it.appId"
            com.fossil.ee7.a(r12, r11)     // Catch:{ Exception -> 0x02fc }
            java.lang.String r13 = r2.getName()     // Catch:{ Exception -> 0x02fc }
            java.lang.String r11 = "it.name"
            com.fossil.ee7.a(r13, r11)     // Catch:{ Exception -> 0x02fc }
            java.lang.String r14 = ""
            java.lang.String r18 = r0.getDeviceId()     // Catch:{ Exception -> 0x02fc }
            if (r18 == 0) goto L_0x02f5
            java.lang.String r11 = r2.getDescription()     // Catch:{ Exception -> 0x02fc }
            r22 = r1
            java.lang.String r1 = "it.description"
            com.fossil.ee7.a(r11, r1)     // Catch:{ Exception -> 0x02fc }
            java.lang.String r1 = ""
            java.lang.String r19 = r2.getIconUrl()     // Catch:{ Exception -> 0x02fc }
            r2 = r11
            r11 = r15
            r23 = r10
            r10 = 0
            r10 = r15
            r15 = r18
            r16 = r20
            r17 = r2
            r18 = r1
            r11.<init>(r12, r13, r14, r15, r16, r17, r18, r19)     // Catch:{ Exception -> 0x03f1 }
            r6.add(r10)     // Catch:{ Exception -> 0x03f1 }
            r1 = r22
            r10 = r23
            goto L_0x029e
        L_0x02f5:
            r23 = r10
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x03f1 }
            r1 = 0
            throw r1
        L_0x02fc:
            r0 = move-exception
            r23 = r10
            goto L_0x03f2
        L_0x0301:
            r23 = r10
            java.lang.String r1 = r0.getDeviceId()
            com.portfolio.platform.data.legacy.threedotzero.ActivePreset r10 = r9.getActivePreset(r1)
            java.util.List r11 = r9.getAllSavedPresets()
            if (r10 == 0) goto L_0x0351
            java.lang.String r12 = r0.getDeviceId()
            if (r12 == 0) goto L_0x0349
            r1 = r28
            r2 = r3
            r13 = r3
            r3 = r29
            r14 = r4
            r4 = r12
            r12 = r5
            r5 = r10
            r15 = r6
            r6 = r14
            com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x03ef }
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x03ef }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ Exception -> 0x03ef }
            java.lang.String r3 = com.fossil.ll4.l     // Catch:{ Exception -> 0x03ef }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03ef }
            r4.<init>()     // Catch:{ Exception -> 0x03ef }
            java.lang.String r5 = "convert active preset success "
            r4.append(r5)     // Catch:{ Exception -> 0x03ef }
            r4.append(r1)     // Catch:{ Exception -> 0x03ef }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x03ef }
            r2.d(r3, r4)     // Catch:{ Exception -> 0x03ef }
            if (r1 == 0) goto L_0x0355
            r12.add(r1)     // Catch:{ Exception -> 0x03ef }
            goto L_0x0355
        L_0x0349:
            r14 = r4
            r12 = r5
            r15 = r6
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x03ef }
            r1 = 0
            throw r1
        L_0x0351:
            r13 = r3
            r14 = r4
            r12 = r5
            r15 = r6
        L_0x0355:
            java.util.Iterator r1 = r11.iterator()
        L_0x0359:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x03af
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.legacy.threedotzero.SavedPreset r2 = (com.portfolio.platform.data.legacy.threedotzero.SavedPreset) r2
            java.lang.String r3 = "preset"
            com.fossil.ee7.a(r2, r3)
            java.lang.String r3 = r2.getId()
            java.lang.String r4 = "legacyActivePreset"
            com.fossil.ee7.a(r10, r4)
            java.lang.String r4 = r10.getOriginalId()
            boolean r3 = com.fossil.ee7.a(r3, r4)
            if (r3 == 0) goto L_0x037e
            goto L_0x0359
        L_0x037e:
            java.lang.String r3 = r0.getDeviceId()
            if (r3 == 0) goto L_0x03aa
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r7.a(r13, r3, r2, r14)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.fossil.ll4.l
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "into "
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            if (r2 == 0) goto L_0x0359
            r12.add(r2)
            goto L_0x0359
        L_0x03aa:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x03af:
            java.lang.String r1 = r0.getDeviceId()
            boolean r1 = com.fossil.ee7.a(r1, r8)
            if (r1 == 0) goto L_0x0418
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.ll4.l
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r0 = r0.getDeviceId()
            r3.append(r0)
            java.lang.String r0 = " is active, download device setting if possible"
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
            com.fossil.ti7 r0 = com.fossil.qj7.b()
            com.fossil.yi7 r1 = com.fossil.zi7.a(r0)
            r2 = 0
            r3 = 0
            com.fossil.ll4$e r4 = new com.fossil.ll4$e
            r5 = 0
            r4.<init>(r7, r5)
            r5 = 3
            r6 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r1, r2, r3, r4, r5, r6)
            goto L_0x0418
        L_0x03ef:
            r0 = move-exception
            goto L_0x03fc
        L_0x03f1:
            r0 = move-exception
        L_0x03f2:
            r14 = r4
            r12 = r5
            r15 = r6
            goto L_0x03fc
        L_0x03f6:
            r0 = move-exception
            r14 = r4
            r12 = r5
            r15 = r6
            r23 = r10
        L_0x03fc:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.ll4.l
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "exception when migrate customize "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r1.d(r2, r0)
        L_0x0418:
            r5 = r12
            r4 = r14
            r6 = r15
            r10 = r23
            goto L_0x0281
        L_0x041f:
            r12 = r5
            r15 = r6
            r23 = r10
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "microAppList "
            r2.append(r3)
            r2.append(r15)
            java.lang.String r3 = " preset "
            r2.append(r3)
            r2.append(r12)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase r0 = r7.h
            com.fossil.ll4$f r1 = new com.fossil.ll4$f
            r1.<init>(r7, r15, r12)
            r0.runInTransaction(r1)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.Iterator r1 = r23.iterator()
        L_0x045a:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x04ca
            java.lang.Object r2 = r1.next()
            com.portfolio.platform.data.legacy.threedotzero.DeviceModel r2 = (com.portfolio.platform.data.legacy.threedotzero.DeviceModel) r2
            com.portfolio.platform.data.model.Device r3 = new com.portfolio.platform.data.model.Device
            java.lang.String r10 = r2.getDeviceId()
            if (r10 == 0) goto L_0x04c5
            java.lang.String r11 = r2.getMacAddress()
            java.lang.String r12 = r2.getSku()
            java.lang.String r13 = r2.getFirmwareRevision()
            int r14 = r2.getBatteryLevel()
            r15 = 0
            r16 = 0
            r17 = 96
            r18 = 0
            r9 = r3
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17, r18)
            java.lang.String r4 = r2.getCreatedAt()
            r3.setCreatedAt(r4)
            java.lang.String r4 = r2.getUpdateAt()
            r3.setUpdatedAt(r4)
            int r4 = r2.getMajor()
            r3.setMajor(r4)
            int r4 = r2.getMinor()
            r3.setMinor(r4)
            com.fossil.ch5 r4 = r7.a
            java.lang.String r5 = r2.getDeviceId()
            int r4 = r4.h(r5)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r3.setVibrationStrength(r4)
            java.lang.String r2 = r2.getDeviceId()
            boolean r2 = com.fossil.ee7.a(r2, r8)
            r3.setActive(r2)
            r0.add(r3)
            goto L_0x045a
        L_0x04c5:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x04ca:
            boolean r1 = r0.isEmpty()
            r2 = 1
            r1 = r1 ^ r2
            if (r1 == 0) goto L_0x04d7
            com.portfolio.platform.data.source.DeviceDao r1 = r7.g
            r1.addAllDevice(r0)
        L_0x04d7:
            com.fossil.ah5$a r0 = com.fossil.ah5.p
            com.fossil.ah5 r0 = r0.a()
            com.fossil.wearables.fsl.contact.ContactProvider r0 = r0.b()
            java.util.List r0 = r0.getAllContactGroups()
            if (r0 == 0) goto L_0x0585
            java.util.Iterator r0 = r0.iterator()
        L_0x04eb:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0583
            java.lang.Object r1 = r0.next()
            com.fossil.wearables.fsl.contact.ContactGroup r1 = (com.fossil.wearables.fsl.contact.ContactGroup) r1
            java.lang.String r3 = "group"
            com.fossil.ee7.a(r1, r3)
            java.util.List r1 = r1.getContacts()
            java.util.Iterator r1 = r1.iterator()
        L_0x0504:
            boolean r3 = r1.hasNext()
            if (r3 == 0) goto L_0x04eb
            java.lang.Object r3 = r1.next()
            com.fossil.wearables.fsl.contact.Contact r3 = (com.fossil.wearables.fsl.contact.Contact) r3
            java.lang.String r4 = "contact"
            com.fossil.ee7.a(r3, r4)
            int r4 = r3.getContactId()
            r5 = -200(0xffffffffffffff38, float:NaN)
            if (r4 != r5) goto L_0x054c
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.ll4.l
            java.lang.String r6 = "Migrate Text from everyone"
            r4.d(r5, r6)
            com.fossil.wearables.fsl.contact.PhoneNumber r4 = new com.fossil.wearables.fsl.contact.PhoneNumber
            r4.<init>()
            com.portfolio.platform.data.AppType r5 = com.portfolio.platform.data.AppType.ALL_SMS
            java.lang.String r5 = r5.packageName
            r3.setFirstName(r5)
            r3.setUseSms(r2)
            java.lang.String r5 = "-5678"
            r4.setNumber(r5)
            com.portfolio.platform.data.source.NotificationsRepository r5 = r7.c
            r5.saveContact(r3)
            r4.setContact(r3)
            com.portfolio.platform.data.source.NotificationsRepository r3 = r7.c
            r3.savePhoneNumber(r4)
            goto L_0x0504
        L_0x054c:
            int r4 = r3.getContactId()
            r5 = -100
            if (r4 != r5) goto L_0x0504
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.ll4.l
            java.lang.String r6 = "Migrate Call from everyone"
            r4.d(r5, r6)
            com.fossil.wearables.fsl.contact.PhoneNumber r4 = new com.fossil.wearables.fsl.contact.PhoneNumber
            r4.<init>()
            com.portfolio.platform.data.AppType r5 = com.portfolio.platform.data.AppType.ALL_CALLS
            java.lang.String r5 = r5.packageName
            r3.setFirstName(r5)
            r3.setUseCall(r2)
            java.lang.String r5 = "-1234"
            r4.setNumber(r5)
            com.portfolio.platform.data.source.NotificationsRepository r5 = r7.c
            r5.saveContact(r3)
            r4.setContact(r3)
            com.portfolio.platform.data.source.NotificationsRepository r3 = r7.c
            r3.savePhoneNumber(r4)
            goto L_0x0504
        L_0x0583:
            com.fossil.i97 r0 = com.fossil.i97.a
        L_0x0585:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.a(java.lang.String):void");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x033a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x033b  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0353  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0364  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x03a9  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x0556  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x055b  */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x0570  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x070f  */
    /* JADX WARNING: Removed duplicated region for block: B:208:0x076a  */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x0864  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r28) {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            boolean r2 = r1 instanceof com.fossil.ll4.b
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.ll4$b r2 = (com.fossil.ll4.b) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.ll4$b r2 = new com.fossil.ll4$b
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r2.label
            r5 = 4
            r6 = 3
            r7 = 2
            java.lang.String r8 = "MFProfileUtils.getInstance().getUserId(mApp)!!"
            java.lang.String r9 = ""
            r11 = 1
            if (r4 == 0) goto L_0x009f
            if (r4 == r11) goto L_0x0097
            if (r4 == r7) goto L_0x0080
            if (r4 == r6) goto L_0x0067
            if (r4 != r5) goto L_0x005f
            java.lang.Object r3 = r2.L$8
            com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r3 = (com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting) r3
            java.lang.Object r4 = r2.L$7
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r5 = r2.L$6
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            java.lang.Object r5 = r2.L$5
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r6 = r2.L$4
            com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r6 = (com.portfolio.platform.data.legacy.threedotzero.DeviceProvider) r6
            java.lang.Object r7 = r2.L$3
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r7 = r2.L$2
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r8 = r2.L$1
            com.portfolio.platform.data.model.MFUser r8 = (com.portfolio.platform.data.model.MFUser) r8
            java.lang.Object r2 = r2.L$0
            com.fossil.ll4 r2 = (com.fossil.ll4) r2
            com.fossil.t87.a(r1)
            goto L_0x0540
        L_0x005f:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x0067:
            java.lang.Object r4 = r2.L$4
            com.fossil.hw6 r4 = (com.fossil.hw6) r4
            java.lang.Object r4 = r2.L$3
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            java.lang.Object r4 = r2.L$2
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r4 = r2.L$1
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            java.lang.Object r6 = r2.L$0
            com.fossil.ll4 r6 = (com.fossil.ll4) r6
            com.fossil.t87.a(r1)
            goto L_0x033c
        L_0x0080:
            java.lang.Object r4 = r2.L$3
            com.portfolio.platform.data.model.MFUser r4 = (com.portfolio.platform.data.model.MFUser) r4
            java.lang.Object r7 = r2.L$2
            java.lang.String r7 = (java.lang.String) r7
            java.lang.Object r12 = r2.L$1
            com.portfolio.platform.data.model.MFUser r12 = (com.portfolio.platform.data.model.MFUser) r12
            java.lang.Object r13 = r2.L$0
            com.fossil.ll4 r13 = (com.fossil.ll4) r13
            com.fossil.t87.a(r1)
            r1 = r12
            r6 = r13
            goto L_0x030c
        L_0x0097:
            java.lang.Object r4 = r2.L$0
            com.fossil.ll4 r4 = (com.fossil.ll4) r4
            com.fossil.t87.a(r1)
            goto L_0x00b0
        L_0x009f:
            com.fossil.t87.a(r1)
            com.portfolio.platform.data.source.UserRepository r1 = r0.b
            r2.L$0 = r0
            r2.label = r11
            java.lang.Object r1 = r1.getCurrentUser(r2)
            if (r1 != r3) goto L_0x00af
            return r3
        L_0x00af:
            r4 = r0
        L_0x00b0:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.fossil.ll4.l
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "migrateFor2Dot0 currentUser "
            r14.append(r15)
            r14.append(r1)
            java.lang.String r14 = r14.toString()
            r12.d(r13, r14)
            if (r1 != 0) goto L_0x0358
            com.fossil.jx6 r12 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r13 = r4.e
            java.lang.String r12 = r12.f(r13)
            boolean r12 = android.text.TextUtils.isEmpty(r12)
            if (r12 != 0) goto L_0x0358
            com.fossil.ch5 r12 = r4.a
            com.fossil.jx6 r13 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r14 = r4.e
            java.lang.String r13 = r13.f(r14)
            r12.y(r13)
            java.util.Date r12 = new java.util.Date
            r12.<init>()
            java.lang.String r12 = com.fossil.zd5.y(r12)
            com.portfolio.platform.data.model.MFUser r13 = new com.portfolio.platform.data.model.MFUser
            com.fossil.jx6 r14 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r14 = r14.i(r15)
            if (r14 == 0) goto L_0x0353
            java.lang.String r15 = "MFProfileUtils.getInstance().getUserEmail(mApp)!!"
            com.fossil.ee7.a(r14, r15)
            com.fossil.jx6 r15 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r5 = r4.e
            java.lang.String r5 = r15.m(r5)
            if (r5 == 0) goto L_0x034e
            com.fossil.ee7.a(r5, r8)
            r13.<init>(r14, r5)
            com.portfolio.platform.data.model.MFUser$Auth r5 = r13.getAuth()
            com.fossil.jx6 r14 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r14 = r14.f(r15)
            if (r14 == 0) goto L_0x012e
            goto L_0x012f
        L_0x012e:
            r14 = r9
        L_0x012f:
            r5.setAccessToken(r14)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r14 = r4.e
            java.lang.String r5 = r5.d(r14)
            java.lang.String r14 = "today"
            if (r5 == 0) goto L_0x0141
            goto L_0x0145
        L_0x0141:
            com.fossil.ee7.a(r12, r14)
            r5 = r12
        L_0x0145:
            r13.setRegisterDate(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r5 = r5.j(r15)
            if (r5 == 0) goto L_0x0155
            goto L_0x0156
        L_0x0155:
            r5 = r9
        L_0x0156:
            r13.setFirstName(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r5 = r5.n(r15)
            if (r5 == 0) goto L_0x0166
            goto L_0x0167
        L_0x0166:
            r5 = r9
        L_0x0167:
            r13.setLastName(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r5 = r5.a(r15)
            if (r5 == 0) goto L_0x0177
            goto L_0x017b
        L_0x0177:
            com.fossil.ee7.a(r12, r14)
            r5 = r12
        L_0x017b:
            r13.setCreatedAt(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r5 = r5.e(r15)
            if (r5 == 0) goto L_0x018b
            goto L_0x018f
        L_0x018b:
            com.fossil.ee7.a(r12, r14)
            r5 = r12
        L_0x018f:
            r13.setUpdatedAt(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r5 = r5.g(r15)
            if (r5 == 0) goto L_0x019f
            goto L_0x01aa
        L_0x019f:
            com.fossil.za5 r5 = com.fossil.za5.EMAIL
            java.lang.String r5 = r5.getValue()
            java.lang.String r15 = "AuthType.EMAIL.value"
            com.fossil.ee7.a(r5, r15)
        L_0x01aa:
            r13.setAuthType(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()     // Catch:{ Exception -> 0x0242 }
            com.portfolio.platform.PortfolioApp r15 = r4.e     // Catch:{ Exception -> 0x0242 }
            java.lang.String r5 = r5.s(r15)     // Catch:{ Exception -> 0x0242 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0242 }
            java.lang.String r15 = "Integer.valueOf(MFProfil\u2026                  ?: \"0\")"
            java.lang.String r16 = "0"
            r10 = 0
            if (r5 != 0) goto L_0x01e0
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.s(r6)
            if (r5 == 0) goto L_0x01cf
            goto L_0x01d1
        L_0x01cf:
            r5 = r16
        L_0x01d1:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            com.fossil.ee7.a(r5, r15)
            int r5 = r5.intValue()
            r13.setWeightInGrams(r5)
            goto L_0x01e3
        L_0x01e0:
            r13.setWeightInGrams(r10)
        L_0x01e3:
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.l(r6)
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 != 0) goto L_0x0210
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.l(r6)
            if (r5 == 0) goto L_0x0201
            r16 = r5
        L_0x0201:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r16)
            com.fossil.ee7.a(r5, r15)
            int r5 = r5.intValue()
            r13.setHeightInCentimeters(r5)
            goto L_0x0213
        L_0x0210:
            r13.setHeightInCentimeters(r10)
        L_0x0213:
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.c(r6)
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 != 0) goto L_0x023e
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.c(r6)
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r5)
            java.lang.String r6 = "java.lang.Boolean.valueO\u2026gistrationComplete(mApp))"
            com.fossil.ee7.a(r5, r6)
            boolean r5 = r5.booleanValue()
            r13.setRegistrationComplete(r5)
            goto L_0x0243
        L_0x023e:
            r13.setRegistrationComplete(r11)
            goto L_0x0243
        L_0x0242:
        L_0x0243:
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r13.getUnitGroup()
            if (r5 == 0) goto L_0x0349
            com.fossil.jx6 r6 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r10 = r4.e
            java.lang.String r6 = r6.p(r10)
            java.lang.String r10 = "com.portfolio.platform.enums.Unit.IMPERIAL.value"
            if (r6 == 0) goto L_0x0258
            goto L_0x0261
        L_0x0258:
            com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
            java.lang.String r6 = r6.getValue()
            com.fossil.ee7.a(r6, r10)
        L_0x0261:
            r5.setDistance(r6)
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r13.getUnitGroup()
            if (r5 == 0) goto L_0x0344
            com.fossil.jx6 r6 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r6 = r6.q(r15)
            if (r6 == 0) goto L_0x0277
            goto L_0x0280
        L_0x0277:
            com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
            java.lang.String r6 = r6.getValue()
            com.fossil.ee7.a(r6, r10)
        L_0x0280:
            r5.setHeight(r6)
            com.portfolio.platform.data.model.MFUser$UnitGroup r5 = r13.getUnitGroup()
            if (r5 == 0) goto L_0x033f
            com.fossil.jx6 r6 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r15 = r4.e
            java.lang.String r6 = r6.r(r15)
            if (r6 == 0) goto L_0x0296
            goto L_0x029f
        L_0x0296:
            com.fossil.ob5 r6 = com.fossil.ob5.IMPERIAL
            java.lang.String r6 = r6.getValue()
            com.fossil.ee7.a(r6, r10)
        L_0x029f:
            r5.setWeight(r6)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.h(r6)
            if (r5 == 0) goto L_0x02af
            goto L_0x02b3
        L_0x02af:
            com.fossil.ee7.a(r12, r14)
            r5 = r12
        L_0x02b3:
            r13.setBirthday(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            com.fossil.fb5 r5 = r5.k(r6)
            java.lang.String r5 = r5.toString()
            r13.setGender(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.o(r6)
            if (r5 == 0) goto L_0x02d4
            goto L_0x02d5
        L_0x02d4:
            r5 = r9
        L_0x02d5:
            r13.setProfilePicture(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.b(r6)
            if (r5 == 0) goto L_0x02e5
            goto L_0x02e6
        L_0x02e5:
            r5 = r9
        L_0x02e6:
            r13.setIntegrations(r5)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            boolean r5 = r5.t(r6)
            r13.setDiagnosticEnabled(r5)
            com.portfolio.platform.data.source.UserRepository r5 = r4.b
            r2.L$0 = r4
            r2.L$1 = r1
            r2.L$2 = r12
            r2.L$3 = r13
            r2.label = r7
            java.lang.Object r5 = r5.insertUser(r13, r2)
            if (r5 != r3) goto L_0x0309
            return r3
        L_0x0309:
            r6 = r4
            r7 = r12
            r4 = r13
        L_0x030c:
            com.fossil.ah5$a r5 = com.fossil.ah5.p
            com.fossil.ah5 r5 = r5.a()
            java.lang.String r10 = r4.getUserId()
            r5.b(r10)
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r10 = r6.e
            r5.b(r10, r9)
            com.fossil.hw6 r5 = new com.fossil.hw6
            r5.<init>()
            r2.L$0 = r6
            r2.L$1 = r1
            r2.L$2 = r7
            r2.L$3 = r4
            r2.L$4 = r5
            r4 = 3
            r2.label = r4
            java.lang.Object r4 = r5.a(r2)
            if (r4 != r3) goto L_0x033b
            return r3
        L_0x033b:
            r4 = r1
        L_0x033c:
            r1 = r4
            r4 = r6
            goto L_0x0358
        L_0x033f:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x0344:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x0349:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x034e:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x0353:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        L_0x0358:
            com.fossil.ch5 r5 = r4.a
            java.lang.String r5 = r5.w()
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 != 0) goto L_0x0397
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.ll4.l
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r10 = "migrateFor2Dot0 legacySerial="
            r7.append(r10)
            com.fossil.ch5 r10 = r4.a
            java.lang.String r10 = r10.w()
            if (r10 == 0) goto L_0x0392
            r7.append(r10)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            com.fossil.ch5 r5 = r4.a
            java.lang.String r6 = r5.w()
            r5.o(r6)
            goto L_0x0397
        L_0x0392:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x0397:
            com.portfolio.platform.PortfolioApp r5 = r4.e
            java.lang.String r7 = r5.c()
            com.fossil.jx6 r5 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r6 = r4.e
            java.lang.String r5 = r5.m(r6)
            if (r5 == 0) goto L_0x0864
            com.fossil.ee7.a(r5, r8)
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r10 = com.fossil.ll4.l
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "migrateFor2Dot0 userID="
            r12.append(r13)
            r12.append(r5)
            java.lang.String r13 = " activeSerial "
            r12.append(r13)
            r12.append(r7)
            java.lang.String r12 = r12.toString()
            r6.d(r10, r12)
            boolean r6 = android.text.TextUtils.isEmpty(r5)
            if (r6 != 0) goto L_0x03fe
            com.fossil.ah5$a r6 = com.fossil.ah5.p
            com.fossil.ah5 r6 = r6.a()
            com.fossil.jx6 r10 = com.fossil.jx6.a()
            com.portfolio.platform.PortfolioApp r12 = r4.e
            java.lang.String r10 = r10.m(r12)
            if (r10 == 0) goto L_0x03f9
            com.fossil.ee7.a(r10, r8)
            com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r6 = r6.a(r10)
            com.fossil.ch5 r8 = r4.a
            java.lang.String r8 = r8.w()
            r6.getDeviceById(r8)
            goto L_0x03fe
        L_0x03f9:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x03fe:
            com.fossil.ah5$a r6 = com.fossil.ah5.p
            com.fossil.ah5 r6 = r6.a()
            com.fossil.wearables.fsl.contact.ContactProvider r6 = r6.b()
            r6.getAllContactGroups()
            com.fossil.ah5$a r6 = com.fossil.ah5.p
            com.fossil.ah5 r6 = r6.a()
            com.fossil.wearables.fsl.appfilter.AppFilterProvider r6 = r6.a()
            r6.getAllAppFilters()
            com.fossil.ch5 r6 = r4.a
            boolean r8 = r6.u()
            r6.f(r8)
            com.fossil.ah5$a r6 = com.fossil.ah5.p
            com.fossil.ah5 r6 = r6.a()
            com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r6 = r6.c()
            java.util.List r8 = r6.getAllDevice()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.lang.String r12 = "deviceList"
            com.fossil.ee7.a(r8, r12)
            java.util.Iterator r12 = r8.iterator()
        L_0x043d:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x04cd
            java.lang.Object r13 = r12.next()
            com.portfolio.platform.data.legacy.threedotzero.DeviceModel r13 = (com.portfolio.platform.data.legacy.threedotzero.DeviceModel) r13
            com.portfolio.platform.data.model.Device r14 = new com.portfolio.platform.data.model.Device
            java.lang.String r18 = r13.getDeviceId()
            if (r18 == 0) goto L_0x04c8
            java.lang.String r19 = r13.getMacAddress()
            java.lang.String r20 = r13.getSku()
            java.lang.String r21 = r13.getFirmwareRevision()
            int r22 = r13.getBatteryLevel()
            r15 = 50
            java.lang.Integer r23 = com.fossil.pb7.a(r15)
            r24 = 0
            r25 = 64
            r26 = 0
            r17 = r14
            r17.<init>(r18, r19, r20, r21, r22, r23, r24, r25, r26)
            java.lang.String r15 = r13.getCreatedAt()
            r14.setCreatedAt(r15)
            java.lang.String r15 = r13.getUpdateAt()
            r14.setUpdatedAt(r15)
            int r15 = r13.getMajor()
            r14.setMajor(r15)
            int r15 = r13.getMinor()
            r14.setMinor(r15)
            int r15 = r13.getVibrationStrength()
            java.lang.Integer r15 = com.fossil.pb7.a(r15)
            r14.setVibrationStrength(r15)
            java.lang.String r13 = r13.getDeviceId()
            boolean r13 = com.fossil.ee7.a(r13, r7)
            r14.setActive(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r15 = com.fossil.ll4.l
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r0 = "migrateFor2Dot0 migrate success device "
            r11.append(r0)
            r11.append(r14)
            java.lang.String r0 = r11.toString()
            r13.d(r15, r0)
            r10.add(r14)
            r11 = 1
            r0 = r27
            goto L_0x043d
        L_0x04c8:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x04cd:
            boolean r0 = r10.isEmpty()
            r11 = 1
            r0 = r0 ^ r11
            if (r0 == 0) goto L_0x04da
            com.portfolio.platform.data.source.DeviceDao r0 = r4.g
            r0.addAllDevice(r10)
        L_0x04da:
            com.portfolio.platform.data.legacy.threedotzero.MappingSet$MappingSetType r0 = com.portfolio.platform.data.legacy.threedotzero.MappingSet.MappingSetType.USER_SAVED
            java.util.List r0 = r6.getMappingSetByType(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.ll4.l
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "migrateFor2Dot0 userSavedMappingSet "
            r13.append(r14)
            if (r0 == 0) goto L_0x04fd
            int r14 = r0.size()
            java.lang.Integer r14 = com.fossil.pb7.a(r14)
            goto L_0x04fe
        L_0x04fd:
            r14 = 0
        L_0x04fe:
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            boolean r11 = r8.isEmpty()
            r12 = 1
            r11 = r11 ^ r12
            if (r11 == 0) goto L_0x06e6
            com.fossil.ah5$a r11 = com.fossil.ah5.p
            com.fossil.ah5 r11 = r11.a()
            com.portfolio.platform.data.legacy.onedotfive.SecondTimezoneProvider r11 = r11.j()
            com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r11 = r11.getActiveSecondTimezone()
            com.portfolio.platform.data.source.UserRepository r12 = r4.b
            r2.L$0 = r4
            r2.L$1 = r1
            r2.L$2 = r7
            r2.L$3 = r5
            r2.L$4 = r6
            r2.L$5 = r8
            r2.L$6 = r10
            r2.L$7 = r0
            r2.L$8 = r11
            r1 = 4
            r2.label = r1
            java.lang.Object r1 = r12.getCurrentUser(r2)
            if (r1 != r3) goto L_0x053c
            return r3
        L_0x053c:
            r2 = r4
            r5 = r8
            r3 = r11
            r4 = r0
        L_0x0540:
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r8 = com.fossil.ll4.l
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "migrateFor2Dot0 2ndTzSetting "
            r10.append(r11)
            if (r3 == 0) goto L_0x055b
            java.lang.String r11 = r3.getTimezoneCityName()
            goto L_0x055c
        L_0x055b:
            r11 = 0
        L_0x055c:
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r0.d(r8, r10)
            java.util.Iterator r0 = r5.iterator()
        L_0x056a:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L_0x06e5
            java.lang.Object r5 = r0.next()
            com.portfolio.platform.data.legacy.threedotzero.DeviceModel r5 = (com.portfolio.platform.data.legacy.threedotzero.DeviceModel) r5
            java.lang.String r8 = r5.getDeviceId()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r8 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.getDeviceFamily(r8)
            com.portfolio.platform.data.legacy.threedotzero.MappingSet r10 = r6.getActiveMappingSetByDeviceFamily(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.ll4.l
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "migrateFor2Dot0 activeMappingSet of "
            r13.append(r14)
            java.lang.String r14 = r5.getDeviceId()
            r13.append(r14)
            java.lang.String r14 = " deviceFamily "
            r13.append(r14)
            r13.append(r8)
            java.lang.String r14 = " name "
            r13.append(r14)
            java.lang.String r14 = "mappingSet"
            com.fossil.ee7.a(r10, r14)
            java.lang.String r14 = r10.getName()
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            java.util.List r11 = r10.getMappingList()
            java.lang.String r12 = "mappingSet.mappingList"
            com.fossil.ee7.a(r11, r12)
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r13 = com.fossil.ll4.l
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r15 = "migrateFor2Dot0 mappingList="
            r14.append(r15)
            r14.append(r11)
            java.lang.String r11 = r14.toString()
            r12.d(r13, r11)
            if (r1 == 0) goto L_0x05e9
            java.lang.String r11 = r1.getUserId()
            if (r11 == 0) goto L_0x05e9
            goto L_0x05ea
        L_0x05e9:
            r11 = r9
        L_0x05ea:
            java.lang.String r12 = r5.getDeviceId()
            if (r12 == 0) goto L_0x06e0
            com.portfolio.platform.data.model.room.microapp.HybridPreset r10 = r2.a(r11, r12, r10, r3)
            if (r10 == 0) goto L_0x061b
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r12 = com.fossil.ll4.l
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r14 = "migrateFor2Dot0 activeHybridPreset "
            r13.append(r14)
            r13.append(r10)
            java.lang.String r13 = r13.toString()
            r11.d(r12, r13)
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase r11 = r2.h
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r11 = r11.presetDao()
            r11.upsertPreset(r10)
        L_0x061b:
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            java.lang.String r11 = "savedMappingSetList"
            com.fossil.ee7.a(r4, r11)
            java.util.Iterator r11 = r4.iterator()
        L_0x0629:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0685
            java.lang.Object r12 = r11.next()
            com.portfolio.platform.data.legacy.threedotzero.MappingSet r12 = (com.portfolio.platform.data.legacy.threedotzero.MappingSet) r12
            java.lang.String r13 = "it"
            com.fossil.ee7.a(r12, r13)
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r13 = r12.getDeviceFamily()
            if (r13 != r8) goto L_0x0680
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r14 = com.fossil.ll4.l
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r28 = r0
            java.lang.String r0 = "migrateFor2Dot0 convert userMappingSet "
            r15.append(r0)
            java.lang.String r0 = r12.getName()
            r15.append(r0)
            java.lang.String r0 = " to hybridPreset"
            r15.append(r0)
            java.lang.String r0 = r15.toString()
            r13.d(r14, r0)
            java.lang.String r0 = r5.getDeviceId()
            if (r0 == 0) goto L_0x067b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r2.a(r0, r12, r6, r3)
            if (r0 == 0) goto L_0x0682
            boolean r0 = r10.add(r0)
            com.fossil.pb7.a(r0)
            goto L_0x0682
        L_0x067b:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x0680:
            r28 = r0
        L_0x0682:
            r0 = r28
            goto L_0x0629
        L_0x0685:
            r28 = r0
            boolean r0 = r10.isEmpty()
            r8 = 1
            r0 = r0 ^ r8
            if (r0 == 0) goto L_0x0698
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase r0 = r2.h
            com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao r0 = r0.presetDao()
            r0.upsertPresetList(r10)
        L_0x0698:
            java.lang.String r0 = r5.getDeviceId()
            boolean r0 = com.fossil.ee7.a(r0, r7)
            if (r0 == 0) goto L_0x06dc
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r8 = com.fossil.ll4.l
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "migrateFor2Dot0 "
            r10.append(r11)
            java.lang.String r5 = r5.getDeviceId()
            r10.append(r5)
            java.lang.String r5 = " is active, download device setting if possible"
            r10.append(r5)
            java.lang.String r5 = r10.toString()
            r0.d(r8, r5)
            com.fossil.ti7 r0 = com.fossil.qj7.b()
            com.fossil.yi7 r10 = com.fossil.zi7.a(r0)
            r11 = 0
            r12 = 0
            com.fossil.ll4$c r13 = new com.fossil.ll4$c
            r0 = 0
            r13.<init>(r2, r0)
            r14 = 3
            r15 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r10, r11, r12, r13, r14, r15)
        L_0x06dc:
            r0 = r28
            goto L_0x056a
        L_0x06e0:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x06e5:
            r4 = r2
        L_0x06e6:
            com.fossil.ah5$a r0 = com.fossil.ah5.p
            com.fossil.ah5 r0 = r0.a()
            com.fossil.wearables.fsl.appfilter.AppFilterProvider r0 = r0.a()
            java.util.List r0 = r0.getAllAppFilters()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.lang.String r2 = "appFilter"
            if (r0 == 0) goto L_0x0762
            boolean r3 = r0.isEmpty()
            r5 = 1
            r3 = r3 ^ r5
            if (r3 == 0) goto L_0x0762
            java.util.Iterator r0 = r0.iterator()
        L_0x0709:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0762
            java.lang.Object r3 = r0.next()
            com.fossil.wearables.fsl.appfilter.AppFilter r3 = (com.fossil.wearables.fsl.appfilter.AppFilter) r3
            com.portfolio.platform.data.AppType r5 = com.portfolio.platform.data.AppType.ALL_CALLS
            java.lang.String r5 = r5.name()
            com.fossil.ee7.a(r3, r2)
            java.lang.String r6 = r3.getType()
            r7 = 1
            boolean r5 = com.fossil.mh7.b(r5, r6, r7)
            if (r5 != 0) goto L_0x0739
            com.portfolio.platform.data.AppType r5 = com.portfolio.platform.data.AppType.ALL_SMS
            java.lang.String r5 = r5.name()
            java.lang.String r6 = r3.getType()
            boolean r5 = com.fossil.mh7.b(r5, r6, r7)
            if (r5 == 0) goto L_0x0709
        L_0x0739:
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.ll4.l
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "migrateFor2Dot0 Add app filter="
            r7.append(r8)
            java.lang.String r8 = r3.getName()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            r1.add(r3)
            com.portfolio.platform.data.source.NotificationsRepository r5 = r4.c
            r5.removeAppFilter(r3)
            goto L_0x0709
        L_0x0762:
            boolean r0 = r1.isEmpty()
            r3 = 1
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x084c
            java.util.Iterator r0 = r1.iterator()
        L_0x076e:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x084c
            java.lang.Object r1 = r0.next()
            com.fossil.wearables.fsl.appfilter.AppFilter r1 = (com.fossil.wearables.fsl.appfilter.AppFilter) r1
            com.fossil.wearables.fsl.contact.ContactGroup r3 = new com.fossil.wearables.fsl.contact.ContactGroup
            r3.<init>()
            com.misfit.frameworks.buttonservice.enums.MFDeviceFamily r5 = com.misfit.frameworks.buttonservice.enums.MFDeviceFamily.DEVICE_FAMILY_SAM
            int r5 = r5.getValue()
            r3.setDeviceFamily(r5)
            com.fossil.ee7.a(r1, r2)
            int r5 = r1.getHour()
            r3.setHour(r5)
            com.portfolio.platform.data.source.NotificationsRepository r5 = r4.c
            r5.saveContactGroup(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
            java.lang.String r6 = com.fossil.ll4.l
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "migrateFor2Dot0 Save contact group="
            r7.append(r8)
            r7.append(r3)
            java.lang.String r7 = r7.toString()
            r5.d(r6, r7)
            com.fossil.wearables.fsl.contact.Contact r5 = new com.fossil.wearables.fsl.contact.Contact
            r5.<init>()
            com.fossil.wearables.fsl.contact.PhoneNumber r6 = new com.fossil.wearables.fsl.contact.PhoneNumber
            r6.<init>()
            com.portfolio.platform.data.AppType r7 = com.portfolio.platform.data.AppType.ALL_CALLS
            java.lang.String r7 = r7.packageName
            java.lang.String r8 = r1.getName()
            r9 = 1
            boolean r7 = com.fossil.mh7.b(r7, r8, r9)
            if (r7 == 0) goto L_0x07e0
            r7 = -100
            r5.setContactId(r7)
            com.portfolio.platform.data.AppType r7 = com.portfolio.platform.data.AppType.ALL_CALLS
            java.lang.String r7 = r7.packageName
            r5.setFirstName(r7)
            r5.setUseCall(r9)
            java.lang.String r7 = "-1234"
            r6.setNumber(r7)
        L_0x07e0:
            com.portfolio.platform.data.AppType r7 = com.portfolio.platform.data.AppType.ALL_SMS
            java.lang.String r7 = r7.packageName
            java.lang.String r1 = r1.getName()
            boolean r1 = com.fossil.mh7.b(r7, r1, r9)
            if (r1 == 0) goto L_0x0802
            r1 = -200(0xffffffffffffff38, float:NaN)
            r5.setContactId(r1)
            com.portfolio.platform.data.AppType r1 = com.portfolio.platform.data.AppType.ALL_SMS
            java.lang.String r1 = r1.packageName
            r5.setFirstName(r1)
            r5.setUseSms(r9)
            java.lang.String r1 = "-5678"
            r6.setNumber(r1)
        L_0x0802:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r7 = com.fossil.ll4.l
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = "migrateFor2Dot0 Save contact="
            r8.append(r10)
            r8.append(r5)
            java.lang.String r8 = r8.toString()
            r1.d(r7, r8)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r7 = com.fossil.ll4.l
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = "migrateFor2Dot0 Save phone number="
            r8.append(r10)
            r8.append(r6)
            java.lang.String r8 = r8.toString()
            r1.d(r7, r8)
            r5.setContactGroup(r3)
            com.portfolio.platform.data.source.NotificationsRepository r1 = r4.c
            r1.saveContact(r5)
            r6.setContact(r5)
            com.portfolio.platform.data.source.NotificationsRepository r1 = r4.c
            r1.savePhoneNumber(r6)
            goto L_0x076e
        L_0x084c:
            com.fossil.ch5 r0 = r4.a
            int r1 = r0.x()
            r0.e(r1)
            com.fossil.ch5 r0 = r4.a
            java.lang.String r1 = r0.v()
            r0.w(r1)
            r4.b()
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x0864:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a() {
        this.a.c(System.currentTimeMillis());
        this.a.c(0);
        this.a.p(true);
    }

    @DexIgnore
    public final HybridPreset a(List<? extends MicroApp> list, String str, SavedPreset savedPreset, HashMap<String, String> hashMap) {
        T t;
        FLogger.INSTANCE.getLocal().d(l, "convert " + savedPreset + " microAppList " + list.size());
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        String y = zd5.y(instance.getTime());
        for (ButtonMapping buttonMapping : savedPreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) t.getAppId(), (Object) component2)) {
                    break;
                }
            }
            if (t == null) {
                FLogger.INSTANCE.getLocal().d(l, "this preset is not support for " + str + " due to " + component2);
                return null;
            }
            String str2 = hashMap.get(component2);
            ee7.a((Object) y, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, y);
            if (str2 != null) {
                hybridPresetAppSetting.setSettings(str2);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + savedPreset.getId(), savedPreset.getName(), str, arrayList, false);
        FLogger.INSTANCE.getLocal().d(l, "legacy createdAt " + savedPreset.getCreateAt() + " updatedAt " + savedPreset.getUpdateAt());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(zd5.y(new Date(savedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(zd5.y(new Date(savedPreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(l, "preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        hybridPreset.setPinType(savedPreset.getPinType());
        return hybridPreset;
    }

    @DexIgnore
    public final HybridPreset a(List<? extends MicroApp> list, String str, String str2, ActivePreset activePreset, HashMap<String, String> hashMap) {
        T t;
        FLogger.INSTANCE.getLocal().d(l, "convert " + activePreset);
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        String y = zd5.y(instance.getTime());
        for (ButtonMapping buttonMapping : activePreset.getButtonMappingList()) {
            String component1 = buttonMapping.component1();
            String component2 = buttonMapping.component2();
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (ee7.a((Object) t.getAppId(), (Object) component2)) {
                    break;
                }
            }
            if (t == null) {
                FLogger.INSTANCE.getLocal().d(l, "this preset is not support for " + str2 + " due to " + component2);
                return null;
            }
            String str3 = hashMap.get(component2);
            ee7.a((Object) y, "localUpdatedAt");
            HybridPresetAppSetting hybridPresetAppSetting = new HybridPresetAppSetting(component1, component2, y);
            if (str3 != null) {
                hybridPresetAppSetting.setSettings(str3);
            }
            arrayList.add(hybridPresetAppSetting);
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + str2, "Active Preset", str2, arrayList, true);
        FLogger.INSTANCE.getLocal().d(l, "legacy active createdAt " + activePreset.getCreateAt() + " updatedAt " + activePreset.getUpdateAt());
        hybridPreset.setPinType(activePreset.getPinType());
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(zd5.y(new Date(activePreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(zd5.y(new Date(activePreset.getUpdateAt() * j2)));
        FLogger.INSTANCE.getLocal().d(l, "active preset createdAt " + hybridPreset.getCreatedAt() + " updatedAt " + hybridPreset.getUpdatedAt());
        return hybridPreset;
    }

    @DexIgnore
    public final HybridPreset a(String str, RecommendedPreset recommendedPreset) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = l;
        StringBuilder sb = new StringBuilder();
        sb.append("convertRecommendPresetToHybridPreset ");
        sb.append(recommendedPreset != null ? recommendedPreset.getName() : null);
        local.d(str2, sb.toString());
        if (recommendedPreset == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "Calendar.getInstance()");
        String y = zd5.y(instance.getTime());
        List<ButtonMapping> buttonMappingList = recommendedPreset.getButtonMappingList();
        ee7.a((Object) buttonMappingList, "recommendedPreset.buttonMappingList");
        for (T t : buttonMappingList) {
            String component1 = t.component1();
            String component2 = t.component2();
            ee7.a((Object) y, "localUpdatedAt");
            arrayList.add(new HybridPresetAppSetting(component1, component2, y));
        }
        HybridPreset hybridPreset = new HybridPreset(str + ':' + recommendedPreset.getId(), recommendedPreset.getName(), str, new ArrayList(), false);
        hybridPreset.setPinType(0);
        long j2 = (long) 1000;
        hybridPreset.setCreatedAt(zd5.y(new Date(recommendedPreset.getCreateAt() * j2)));
        hybridPreset.setUpdatedAt(zd5.y(new Date(recommendedPreset.getUpdateAt() * j2)));
        return hybridPreset;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r10v0, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r10v1, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r10v3, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x00fa, code lost:
        if (r2 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x012c, code lost:
        if (r2 != null) goto L_0x012e;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x014c  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0166  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0040 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0162 A[EDGE_INSN: B:43:0x0162->B:30:0x0162 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset a(java.lang.String r16, com.portfolio.platform.data.legacy.threedotzero.MappingSet r17, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider r18, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r19) {
        /*
            r15 = this;
            r3 = r16
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "convertSavedMappingSetToHybridPreset "
            r2.append(r4)
            java.lang.String r5 = r17.getName()
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r0, r1)
            java.util.Date r0 = r0.getTime()
            java.lang.String r0 = com.fossil.zd5.y(r0)
            java.util.List r1 = r17.getMappingList()
            java.util.Iterator r1 = r1.iterator()
        L_0x0040:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0196
            java.lang.Object r2 = r1.next()
            com.misfit.frameworks.buttonservice.model.Mapping r2 = (com.misfit.frameworks.buttonservice.model.Mapping) r2
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.String r7 = com.fossil.ll4.l
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "convertSavedMappingSetToHybridPreset action "
            r8.append(r9)
            java.lang.String r9 = "mapping"
            com.fossil.ee7.a(r2, r9)
            int r9 = r2.getAction()
            r8.append(r9)
            java.lang.String r9 = " extraInfo "
            r8.append(r9)
            java.lang.String r9 = r2.getExtraInfo()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r6.d(r7, r8)
            com.fossil.ne5 r6 = com.fossil.ne5.a
            int r7 = r2.getAction()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r6 = r6.a(r7)
            com.fossil.ne5 r7 = com.fossil.ne5.a
            com.misfit.frameworks.common.enums.Gesture r8 = r2.getGesture()
            java.lang.String r7 = r7.a(r8)
            if (r6 == 0) goto L_0x016b
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r8 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_ALARM_ID
            if (r6 != r8) goto L_0x009d
            goto L_0x016b
        L_0x009d:
            int[] r8 = com.fossil.ml4.a
            int r9 = r6.ordinal()
            r8 = r8[r9]
            r9 = 1
            r10 = 0
            java.lang.String r11 = ""
            if (r8 == r9) goto L_0x00fd
            r9 = 2
            if (r8 == r9) goto L_0x00b1
            r11 = r10
            goto L_0x012f
        L_0x00b1:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = com.fossil.ll4.l
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "convertSavedMappingSetToHybridPreset 2ndTz setting "
            r12.append(r13)
            java.lang.String r2 = r2.getExtraInfo()
            r12.append(r2)
            java.lang.String r2 = r12.toString()
            r8.d(r9, r2)
            if (r19 == 0) goto L_0x012f
            com.portfolio.platform.data.model.setting.SecondTimezoneSetting r2 = new com.portfolio.platform.data.model.setting.SecondTimezoneSetting
            java.lang.String r8 = r19.getTimezoneCityName()
            java.lang.String r9 = "it.timezoneCityName"
            com.fossil.ee7.a(r8, r9)
            java.lang.String r9 = r19.getTimezoneId()
            java.lang.String r12 = "it.timezoneId"
            com.fossil.ee7.a(r9, r12)
            long r13 = r19.getTimezoneOffset()
            int r14 = (int) r13
            java.lang.String r13 = r19.getTimezoneId()
            com.fossil.ee7.a(r13, r12)
            r2.<init>(r8, r9, r14, r13)
            java.lang.String r2 = com.fossil.wc5.a(r2)
            if (r2 == 0) goto L_0x012f
            goto L_0x012e
        L_0x00fd:
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r9 = com.fossil.ll4.l
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "convertSavedMappingSetToHybridPreset ringphone setting "
            r12.append(r13)
            java.lang.String r13 = r2.getExtraInfo()
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            r8.d(r9, r12)
            java.lang.String r2 = r2.getExtraInfo()
            if (r2 == 0) goto L_0x012f
            com.portfolio.platform.data.model.Ringtone r8 = new com.portfolio.platform.data.model.Ringtone
            r8.<init>(r2, r11)
            java.lang.String r2 = com.fossil.wc5.a(r8)
            if (r2 == 0) goto L_0x012f
        L_0x012e:
            r11 = r2
        L_0x012f:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r2 = new com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting
            java.lang.String r6 = r6.getValue()
            java.lang.String r8 = "localUpdatedAt"
            com.fossil.ee7.a(r0, r8)
            r2.<init>(r7, r6, r0)
            if (r11 == 0) goto L_0x0142
            r2.setSettings(r11)
        L_0x0142:
            java.util.Iterator r6 = r5.iterator()
        L_0x0146:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x0162
            java.lang.Object r7 = r6.next()
            r8 = r7
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r8 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r8
            java.lang.String r8 = r8.getPosition()
            java.lang.String r9 = r2.getPosition()
            boolean r8 = com.fossil.ee7.a(r8, r9)
            if (r8 == 0) goto L_0x0146
            r10 = r7
        L_0x0162:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r10 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r10
            if (r10 != 0) goto L_0x0040
            r5.add(r2)
            goto L_0x0040
        L_0x016b:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r4)
            r2.append(r6)
            java.lang.String r4 = " not supported, revert  to default preset"
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            r0 = r18
            com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset r0 = r0.getDefaultPreset(r3)
            r6 = r15
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r15.a(r3, r0)
            return r0
        L_0x0196:
            r6 = r15
            com.portfolio.platform.data.model.room.microapp.HybridPreset r7 = new com.portfolio.platform.data.model.room.microapp.HybridPreset
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r3)
            r1 = 58
            r0.append(r1)
            java.lang.String r1 = r17.getId()
            r0.append(r1)
            java.lang.String r1 = r0.toString()
            java.lang.String r2 = r17.getName()
            r8 = 0
            r0 = r7
            r3 = r16
            r4 = r5
            r5 = r8
            r0.<init>(r1, r2, r3, r4, r5)
            r0 = 0
            r7.setPinType(r0)
            java.util.Date r0 = new java.util.Date
            long r1 = r17.getCreateAt()
            r3 = 1000(0x3e8, float:1.401E-42)
            long r3 = (long) r3
            long r1 = r1 * r3
            r0.<init>(r1)
            java.lang.String r0 = com.fossil.zd5.y(r0)
            r7.setCreatedAt(r0)
            java.util.Date r0 = new java.util.Date
            long r1 = r17.getUpdateAt()
            long r1 = r1 * r3
            r0.<init>(r1)
            java.lang.String r0 = com.fossil.zd5.y(r0)
            r7.setUpdatedAt(r0)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.a(java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v4, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r6v5, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r6v7, resolved type: com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00d7, code lost:
        if (r2 != null) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00e9, code lost:
        if (r2 != null) goto L_0x00eb;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00fc  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0123  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x003e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x011f A[EDGE_INSN: B:42:0x011f->B:29:0x011f ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.portfolio.platform.data.model.room.microapp.HybridPreset a(java.lang.String r15, java.lang.String r16, com.portfolio.platform.data.legacy.threedotzero.MappingSet r17, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting r18) {
        /*
            r14 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "convertActiveMappingSetToHybridPreset "
            r2.append(r3)
            java.lang.String r4 = r17.getName()
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r0, r1)
            java.util.Date r0 = r0.getTime()
            java.lang.String r0 = com.fossil.zd5.y(r0)
            java.util.List r1 = r17.getMappingList()
            java.util.Iterator r1 = r1.iterator()
        L_0x003e:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0148
            java.lang.Object r2 = r1.next()
            com.misfit.frameworks.buttonservice.model.Mapping r2 = (com.misfit.frameworks.buttonservice.model.Mapping) r2
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.ll4.l
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "convertActiveMappingSetToHybridPreset action "
            r6.append(r7)
            java.lang.String r7 = "mapping"
            com.fossil.ee7.a(r2, r7)
            int r7 = r2.getAction()
            r6.append(r7)
            java.lang.String r7 = " extraInfo "
            r6.append(r7)
            java.lang.String r7 = r2.getExtraInfo()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r4.d(r5, r6)
            com.fossil.ne5 r4 = com.fossil.ne5.a
            int r5 = r2.getAction()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r4 = r4.a(r5)
            com.fossil.ne5 r5 = com.fossil.ne5.a
            com.misfit.frameworks.common.enums.Gesture r6 = r2.getGesture()
            java.lang.String r5 = r5.a(r6)
            r6 = 0
            if (r4 == 0) goto L_0x0128
            com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction$MicroAppID r7 = com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction.MicroAppID.UAPP_ALARM_ID
            if (r4 != r7) goto L_0x009c
            goto L_0x0128
        L_0x009c:
            int[] r7 = com.fossil.ml4.b
            int r9 = r4.ordinal()
            r7 = r7[r9]
            r9 = 1
            java.lang.String r10 = ""
            if (r7 == r9) goto L_0x00da
            r2 = 2
            if (r7 == r2) goto L_0x00ae
            r10 = r6
            goto L_0x00ec
        L_0x00ae:
            if (r18 == 0) goto L_0x00ec
            com.portfolio.platform.data.model.setting.SecondTimezoneSetting r2 = new com.portfolio.platform.data.model.setting.SecondTimezoneSetting
            java.lang.String r7 = r18.getTimezoneCityName()
            java.lang.String r9 = "it.timezoneCityName"
            com.fossil.ee7.a(r7, r9)
            java.lang.String r9 = r18.getTimezoneId()
            java.lang.String r11 = "it.timezoneId"
            com.fossil.ee7.a(r9, r11)
            long r12 = r18.getTimezoneOffset()
            int r13 = (int) r12
            java.lang.String r12 = r18.getTimezoneId()
            com.fossil.ee7.a(r12, r11)
            r2.<init>(r7, r9, r13, r12)
            java.lang.String r2 = com.fossil.wc5.a(r2)
            if (r2 == 0) goto L_0x00ec
            goto L_0x00eb
        L_0x00da:
            java.lang.String r2 = r2.getExtraInfo()
            if (r2 == 0) goto L_0x00ec
            com.portfolio.platform.data.model.Ringtone r7 = new com.portfolio.platform.data.model.Ringtone
            r7.<init>(r2, r10)
            java.lang.String r2 = com.fossil.wc5.a(r7)
            if (r2 == 0) goto L_0x00ec
        L_0x00eb:
            r10 = r2
        L_0x00ec:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r2 = new com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting
            java.lang.String r4 = r4.getValue()
            java.lang.String r7 = "localUpdatedAt"
            com.fossil.ee7.a(r0, r7)
            r2.<init>(r5, r4, r0)
            if (r10 == 0) goto L_0x00ff
            r2.setSettings(r10)
        L_0x00ff:
            java.util.Iterator r4 = r8.iterator()
        L_0x0103:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x011f
            java.lang.Object r5 = r4.next()
            r7 = r5
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r7 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r7
            java.lang.String r7 = r7.getPosition()
            java.lang.String r9 = r2.getPosition()
            boolean r7 = com.fossil.ee7.a(r7, r9)
            if (r7 == 0) goto L_0x0103
            r6 = r5
        L_0x011f:
            com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting r6 = (com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting) r6
            if (r6 != 0) goto L_0x003e
            r8.add(r2)
            goto L_0x003e
        L_0x0128:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.ll4.l
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            r2.append(r4)
            java.lang.String r3 = " not supported"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            return r6
        L_0x0148:
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = new com.portfolio.platform.data.model.room.microapp.HybridPreset
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r2 = r15
            r1.append(r15)
            r2 = 58
            r1.append(r2)
            r2 = r16
            r1.append(r2)
            java.lang.String r5 = r1.toString()
            r9 = 1
            java.lang.String r6 = "Active Preset"
            r4 = r0
            r7 = r16
            r4.<init>(r5, r6, r7, r8, r9)
            r1 = 0
            r0.setPinType(r1)
            java.util.Date r1 = new java.util.Date
            long r2 = r17.getCreateAt()
            r4 = 1000(0x3e8, float:1.401E-42)
            long r4 = (long) r4
            long r2 = r2 * r4
            r1.<init>(r2)
            java.lang.String r1 = com.fossil.zd5.y(r1)
            r0.setCreatedAt(r1)
            java.util.Date r1 = new java.util.Date
            long r2 = r17.getUpdateAt()
            long r2 = r2 * r4
            r1.<init>(r2)
            java.lang.String r1 = com.fossil.zd5.y(r1)
            r0.setUpdatedAt(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ll4.a(java.lang.String, java.lang.String, com.portfolio.platform.data.legacy.threedotzero.MappingSet, com.portfolio.platform.data.legacy.threedotzero.LegacySecondTimezoneSetting):com.portfolio.platform.data.model.room.microapp.HybridPreset");
    }
}
