package com.fossil;

import android.content.Intent;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class we2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Intent a;
    @DexIgnore
    public /* final */ /* synthetic */ ve2 b;

    @DexIgnore
    public we2(ve2 ve2, Intent intent) {
        this.b = ve2;
        this.a = intent;
    }

    @DexIgnore
    public final void run() {
        String action = this.a.getAction();
        StringBuilder sb = new StringBuilder(String.valueOf(action).length() + 61);
        sb.append("Service took too long to process intent: ");
        sb.append(action);
        sb.append(" App may get closed.");
        Log.w("EnhancedIntentService", sb.toString());
        this.b.a();
    }
}
