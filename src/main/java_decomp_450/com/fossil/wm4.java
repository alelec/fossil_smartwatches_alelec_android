package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wm4 implements Factory<vm4> {
    @DexIgnore
    public /* final */ Provider<qd5> a;
    @DexIgnore
    public /* final */ Provider<ro4> b;

    @DexIgnore
    public wm4(Provider<qd5> provider, Provider<ro4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static wm4 a(Provider<qd5> provider, Provider<ro4> provider2) {
        return new wm4(provider, provider2);
    }

    @DexIgnore
    public static vm4 a(qd5 qd5, ro4 ro4) {
        return new vm4(qd5, ro4);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public vm4 get() {
        return a(this.a.get(), this.b.get());
    }
}
