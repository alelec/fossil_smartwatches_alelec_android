package com.fossil;

import com.fossil.fl4;
import com.fossil.on5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn6 extends qn6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ rn6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ on5 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ ob5 b;

        @DexIgnore
        public b(nn6 nn6, ob5 ob5) {
            this.a = nn6;
            this.b = ob5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            UserDisplayUnit a2;
            ee7.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.e(this.b);
            MFUser a3 = this.a.e;
            if (a3 != null && (a2 = vc5.a(a3)) != null) {
                PortfolioApp.g0.c().a(a2, PortfolioApp.g0.c().c());
            }
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ ob5 b;

        @DexIgnore
        public c(nn6 nn6, ob5 ob5) {
            this.a = nn6;
            this.b = ob5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.c(this.b);
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ ob5 b;

        @DexIgnore
        public d(nn6 nn6, ob5 ob5) {
            this.a = nn6;
            this.b = ob5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            UserDisplayUnit a2;
            ee7.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.a(this.b);
            MFUser a3 = this.a.e;
            if (a3 != null && (a2 = vc5.a(a3)) != null) {
                PortfolioApp.g0.c().a(a2, PortfolioApp.g0.c().c());
            }
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.e<on5.d, on5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ nn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ ob5 b;

        @DexIgnore
        public e(nn6 nn6, ob5 ob5) {
            this.a = nn6;
            this.b = ob5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(on5.d dVar) {
            ee7.b(dVar, "responseValue");
            this.a.f.a();
            this.a.f.d(this.b);
        }

        @DexIgnore
        public void a(on5.c cVar) {
            ee7.b(cVar, "errorValue");
            this.a.f.a();
            this.a.f.a(cVar.a(), "");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ nn6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitPresenter$start$1$1", f = "PreferredUnitPresenter.kt", l = {30}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository b = this.this$0.this$0.g;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = b.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(nn6 nn6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = nn6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            nn6 nn6;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                nn6 nn62 = this.this$0;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.L$1 = nn62;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                nn6 = nn62;
            } else if (i == 1) {
                nn6 = (nn6) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            nn6.e = (MFUser) obj;
            rn6 c = this.this$0.f;
            MFUser a3 = this.this$0.e;
            if (a3 != null) {
                MFUser.UnitGroup unitGroup = a3.getUnitGroup();
                if (unitGroup != null) {
                    String height = unitGroup.getHeight();
                    if (height == null) {
                        height = ob5.METRIC.getValue();
                    }
                    c.c(ob5.fromString(height));
                    rn6 c2 = this.this$0.f;
                    MFUser a4 = this.this$0.e;
                    if (a4 != null) {
                        MFUser.UnitGroup unitGroup2 = a4.getUnitGroup();
                        if (unitGroup2 != null) {
                            String weight = unitGroup2.getWeight();
                            if (weight == null) {
                                weight = ob5.METRIC.getValue();
                            }
                            c2.d(ob5.fromString(weight));
                            rn6 c3 = this.this$0.f;
                            MFUser a5 = this.this$0.e;
                            if (a5 != null) {
                                MFUser.UnitGroup unitGroup3 = a5.getUnitGroup();
                                if (unitGroup3 != null) {
                                    String distance = unitGroup3.getDistance();
                                    if (distance == null) {
                                        distance = ob5.METRIC.getValue();
                                    }
                                    c3.e(ob5.fromString(distance));
                                    rn6 c4 = this.this$0.f;
                                    MFUser a6 = this.this$0.e;
                                    if (a6 != null) {
                                        MFUser.UnitGroup unitGroup4 = a6.getUnitGroup();
                                        if (unitGroup4 != null) {
                                            String temperature = unitGroup4.getTemperature();
                                            if (temperature == null) {
                                                temperature = ob5.METRIC.getValue();
                                            }
                                            c4.a(ob5.fromString(temperature));
                                            return i97.a;
                                        }
                                        ee7.a();
                                        throw null;
                                    }
                                    ee7.a();
                                    throw null;
                                }
                                ee7.a();
                                throw null;
                            }
                            ee7.a();
                            throw null;
                        }
                        ee7.a();
                        throw null;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = nn6.class.getSimpleName();
        ee7.a((Object) simpleName, "PreferredUnitPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public nn6(rn6 rn6, UserRepository userRepository, on5 on5) {
        ee7.b(rn6, "mView");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(on5, "mUpdateUser");
        this.f = rn6;
        this.g = userRepository;
        this.h = on5;
    }

    @DexIgnore
    @Override // com.fossil.qn6
    public void d(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setWeightUnit() called with: unit = [" + ob5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save weight with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setWeight(value);
                this.f.b();
                on5 on5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    on5.a(new on5.b(mFUser2), new e(this, ob5));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ik7 unused = xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.f.a(this);
    }

    @DexIgnore
    @Override // com.fossil.qn6
    public void b(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setHeightUnit() called with: unit = [" + ob5 + ']');
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save height with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setHeight(value);
                this.f.b();
                on5 on5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    on5.a(new on5.b(mFUser2), new c(this, ob5));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qn6
    public void c(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setTemperatureUnit() called with: unit = [" + ob5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setTemperatureUnit: unit = " + ob5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save temperature unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setTemperature(value);
                this.f.b();
                on5 on5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    on5.a(new on5.b(mFUser2), new d(this, ob5));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.qn6
    public void a(ob5 ob5) {
        ee7.b(ob5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "setDistanceUnit() called with: unit = [" + ob5 + ']');
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local2.d(str2, "setDistanceUnit: unit = " + ob5);
        MFUser mFUser = this.e;
        if (mFUser == null) {
            FLogger.INSTANCE.getLocal().d(i, "Can't save distance unit with null user");
        } else if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ob5.getValue();
                ee7.a((Object) value, "unit.value");
                unitGroup.setDistance(value);
                this.f.b();
                on5 on5 = this.h;
                MFUser mFUser2 = this.e;
                if (mFUser2 != null) {
                    on5.a(new on5.b(mFUser2), new b(this, ob5));
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }
}
