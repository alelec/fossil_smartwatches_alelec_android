package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z70 extends s70 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<z70> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public z70 createFromParcel(Parcel parcel) {
            return new z70(parcel, null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public z70[] newArray(int i) {
            return new z70[i];
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public z70 m90createFromParcel(Parcel parcel) {
            return new z70(parcel, null);
        }
    }

    @DexIgnore
    public z70(te0 te0) {
        super(u70.SECOND_TIMEZONE, te0, null, null, 12);
    }

    @DexIgnore
    public final te0 getDataConfig() {
        re0 re0 = ((s70) this).b;
        if (re0 != null) {
            return (te0) re0;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.complication.config.data.TimeZoneTwoComplicationDataConfig");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ z70(te0 te0, ue0 ue0, ve0 ve0, int i, zd7 zd7) {
        this(te0, ue0, (i & 4) != 0 ? new ve0(ve0.CREATOR.a()) : ve0);
    }

    @DexIgnore
    public z70(te0 te0, ue0 ue0, ve0 ve0) {
        super(u70.SECOND_TIMEZONE, te0, ue0, ve0);
    }

    @DexIgnore
    public /* synthetic */ z70(Parcel parcel, zd7 zd7) {
        super(parcel);
    }
}
