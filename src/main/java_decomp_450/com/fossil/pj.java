package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class pj extends FrameLayout {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public boolean b; // = true;

    @DexIgnore
    public pj(ViewGroup viewGroup) {
        super(viewGroup.getContext());
        setClipChildren(false);
        this.a = viewGroup;
        viewGroup.setTag(yj.ghost_view_holder, this);
        nk.a(this.a).a(this);
    }

    @DexIgnore
    public static pj a(ViewGroup viewGroup) {
        return (pj) viewGroup.getTag(yj.ghost_view_holder);
    }

    @DexIgnore
    public void onViewAdded(View view) {
        if (this.b) {
            super.onViewAdded(view);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }

    @DexIgnore
    public void onViewRemoved(View view) {
        super.onViewRemoved(view);
        if ((getChildCount() == 1 && getChildAt(0) == view) || getChildCount() == 0) {
            this.a.setTag(yj.ghost_view_holder, null);
            nk.a(this.a).b(this);
            this.b = false;
        }
    }

    @DexIgnore
    public void a() {
        if (this.b) {
            nk.a(this.a).b(this);
            nk.a(this.a).a(this);
            return;
        }
        throw new IllegalStateException("This GhostViewHolder is detached!");
    }

    @DexIgnore
    public void a(rj rjVar) {
        ArrayList<View> arrayList = new ArrayList<>();
        a(rjVar.c, arrayList);
        int a2 = a(arrayList);
        if (a2 < 0 || a2 >= getChildCount()) {
            addView(rjVar);
        } else {
            addView(rjVar, a2);
        }
    }

    @DexIgnore
    public final int a(ArrayList<View> arrayList) {
        ArrayList arrayList2 = new ArrayList();
        int childCount = getChildCount() - 1;
        int i = 0;
        while (i <= childCount) {
            int i2 = (i + childCount) / 2;
            a(((rj) getChildAt(i2)).c, arrayList2);
            if (a(arrayList, arrayList2)) {
                i = i2 + 1;
            } else {
                childCount = i2 - 1;
            }
            arrayList2.clear();
        }
        return i;
    }

    @DexIgnore
    public static boolean a(ArrayList<View> arrayList, ArrayList<View> arrayList2) {
        if (arrayList.isEmpty() || arrayList2.isEmpty() || arrayList.get(0) != arrayList2.get(0)) {
            return true;
        }
        int min = Math.min(arrayList.size(), arrayList2.size());
        for (int i = 1; i < min; i++) {
            View view = arrayList.get(i);
            View view2 = arrayList2.get(i);
            if (view != view2) {
                return a(view, view2);
            }
        }
        if (arrayList2.size() == min) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public static void a(View view, ArrayList<View> arrayList) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            a((View) parent, arrayList);
        }
        arrayList.add(view);
    }

    @DexIgnore
    public static boolean a(View view, View view2) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        int childCount = viewGroup.getChildCount();
        if (Build.VERSION.SDK_INT < 21 || view.getZ() == view2.getZ()) {
            for (int i = 0; i < childCount; i++) {
                View childAt = viewGroup.getChildAt(nk.a(viewGroup, i));
                if (childAt == view) {
                    return false;
                }
                if (childAt == view2) {
                    break;
                }
            }
            return true;
        } else if (view.getZ() > view2.getZ()) {
            return true;
        } else {
            return false;
        }
    }
}
