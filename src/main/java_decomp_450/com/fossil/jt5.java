package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl4;
import com.fossil.mt5;
import com.fossil.nj5;
import com.fossil.ql4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.xg5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt5 extends et5 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a(null);
    @DexIgnore
    public boolean e; // = this.p.F();
    @DexIgnore
    public List<InstalledApp> f; // = new ArrayList();
    @DexIgnore
    public List<at5> g; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> h; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> i; // = new ArrayList();
    @DexIgnore
    public /* final */ b j; // = new b();
    @DexIgnore
    public /* final */ ft5 k;
    @DexIgnore
    public /* final */ rl4 l;
    @DexIgnore
    public /* final */ nw5 m;
    @DexIgnore
    public /* final */ vu5 n;
    @DexIgnore
    public /* final */ mt5 o;
    @DexIgnore
    public /* final */ ch5 p;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jt5.r;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ql4.d<mt5.c, ql4.a> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(mt5.c cVar) {
                FLogger.INSTANCE.getLocal().d(jt5.s.a(), ".Inside mSaveAppsNotification onSuccess");
                jt5.this.k.a();
                jt5.this.k.close();
            }

            @DexIgnore
            public void a(ql4.a aVar) {
                FLogger.INSTANCE.getLocal().d(jt5.s.a(), ".Inside mSaveAppsNotification onError");
                jt5.this.k.a();
                jt5.this.k.close();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(jt5.s.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jt5.s.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(jt5.s.a(), "onReceive - success");
                jt5.this.p.a(jt5.this.j());
                jt5.this.l.a(jt5.this.o, new mt5.b(jt5.this.k()), new a(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(jt5.s.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra2);
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = jt5.s.a();
            local2.d(a3, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
            int size = integerArrayListExtra.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = jt5.s.a();
                local3.d(a4, "error code " + i + " =" + integerArrayListExtra.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    jt5.this.k.l();
                    return;
                } else if (intExtra2 == 8888) {
                    jt5.this.k.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(integerArrayListExtra);
            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            ft5 l = jt5.this.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
            if (array != null) {
                ib5[] ib5Arr = (ib5[]) array;
                l.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                return;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", f = "NotificationAppsPresenter.kt", l = {130, 135, 184, 192, 201, 210}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ jt5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", f = "NotificationAppsPresenter.kt", l = {131}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$6", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $notificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class a implements Runnable {
                @DexIgnore
                public /* final */ /* synthetic */ b a;

                @DexIgnore
                public a(b bVar) {
                    this.a = bVar;
                }

                @DexIgnore
                public final void run() {
                    this.a.this$0.this$0.q.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(c cVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$notificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, this.$notificationSettings, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.q.runInTransaction(new a(this));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jt5$c$c")
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$7", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.jt5$c$c  reason: collision with other inner class name */
        public static final class C0089c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $listNotificationSettings;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0089c(c cVar, List list, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$listNotificationSettings = list;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0089c cVar = new C0089c(this.this$0, this.$listNotificationSettings, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((C0089c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                        int component2 = notificationSettingsModel.component2();
                        if (notificationSettingsModel.component3()) {
                            String a = this.this$0.this$0.a(component2);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = jt5.s.a();
                            local.d(a2, "CALL settingsTypeName=" + a);
                            if (component2 == 0) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String a3 = jt5.s.a();
                                local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                            } else if (component2 == 1) {
                                int size = this.this$0.this$0.i.size();
                                for (int i = 0; i < size; i++) {
                                    ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.i.get(i);
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String a4 = jt5.s.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mListAppNotificationFilter add PHONE item - ");
                                    sb.append(i);
                                    sb.append(" name = ");
                                    Contact contact = contactGroup.getContacts().get(0);
                                    ee7.a((Object) contact, "item.contacts[0]");
                                    sb.append(contact.getDisplayName());
                                    local3.d(a4, sb.toString());
                                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                                    Contact contact2 = contactGroup.getContacts().get(0);
                                    ee7.a((Object) contact2, "item.contacts[0]");
                                    appNotificationFilter.setSender(contact2.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter);
                                }
                            }
                        } else {
                            String a5 = this.this$0.this$0.a(component2);
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String a6 = jt5.s.a();
                            local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                            if (component2 == 0) {
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String a7 = jt5.s.a();
                                local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                this.this$0.this$0.h.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                            } else if (component2 == 1) {
                                int size2 = this.this$0.this$0.i.size();
                                for (int i2 = 0; i2 < size2; i2++) {
                                    ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.i.get(i2);
                                    ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                    String a8 = jt5.s.a();
                                    StringBuilder sb2 = new StringBuilder();
                                    sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                    sb2.append(i2);
                                    sb2.append(" name = ");
                                    Contact contact3 = contactGroup2.getContacts().get(0);
                                    ee7.a((Object) contact3, "item.contacts[0]");
                                    sb2.append(contact3.getDisplayName());
                                    local6.d(a8, sb2.toString());
                                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                                    Contact contact4 = contactGroup2.getContacts().get(0);
                                    ee7.a((Object) contact4, "item.contacts[0]");
                                    appNotificationFilter2.setSender(contact4.getDisplayName());
                                    this.this$0.this$0.h.add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAllContactGroupsResponse$1", f = "NotificationAppsPresenter.kt", l = {186}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super fl4.c>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super fl4.c> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    vu5 c = this.this$0.this$0.n;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gl4.a(c, null, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$getAppResponse$1", f = "NotificationAppsPresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class e extends zb7 implements kd7<yi7, fb7<? super fl4.c>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public e(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                e eVar = new e(this.this$0, fb7);
                eVar.p$ = (yi7) obj;
                return eVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super fl4.c> fb7) {
                return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    nw5 d = this.this$0.this$0.m;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = gl4.a(d, null, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$listNotificationSettings$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class f extends zb7 implements kd7<yi7, fb7<? super List<? extends NotificationSettingsModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public f(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                f fVar = new f(this.this$0, fb7);
                fVar.p$ = (yi7) obj;
                return fVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends NotificationSettingsModel>> fb7) {
                return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.q.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(jt5 jt5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = jt5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:102:0x0275, code lost:
            if (r15 == false) goto L_0x0299;
         */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:111:0x02b1  */
        /* JADX WARNING: Removed duplicated region for block: B:116:0x02e5 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:119:0x02ed  */
        /* JADX WARNING: Removed duplicated region for block: B:124:0x034f  */
        /* JADX WARNING: Removed duplicated region for block: B:127:0x0389  */
        /* JADX WARNING: Removed duplicated region for block: B:130:0x03d1  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0099 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a0  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
                r14 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r14.label
                r2 = 1
                r3 = 0
                r4 = 0
                switch(r1) {
                    case 0: goto L_0x0058;
                    case 1: goto L_0x0050;
                    case 2: goto L_0x0048;
                    case 3: goto L_0x003e;
                    case 4: goto L_0x0031;
                    case 5: goto L_0x0014;
                    case 6: goto L_0x0020;
                    default: goto L_0x000c;
                }
            L_0x000c:
                java.lang.IllegalStateException r15 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r15.<init>(r0)
                throw r15
            L_0x0014:
                java.lang.Object r0 = r14.L$5
                com.portfolio.platform.data.model.NotificationSettingsModel r0 = (com.portfolio.platform.data.model.NotificationSettingsModel) r0
                java.lang.Object r0 = r14.L$4
                com.portfolio.platform.data.model.NotificationSettingsModel r0 = (com.portfolio.platform.data.model.NotificationSettingsModel) r0
                java.lang.Object r0 = r14.L$3
                java.util.List r0 = (java.util.List) r0
            L_0x0020:
                java.lang.Object r0 = r14.L$2
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r14.L$1
                com.fossil.fl4$c r0 = (com.fossil.fl4.c) r0
                java.lang.Object r0 = r14.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r15)
                goto L_0x03e6
            L_0x0031:
                java.lang.Object r1 = r14.L$1
                com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
                java.lang.Object r5 = r14.L$0
                com.fossil.yi7 r5 = (com.fossil.yi7) r5
                com.fossil.t87.a(r15)
                goto L_0x0347
            L_0x003e:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
            L_0x0045:
                r5 = r1
                goto L_0x02e6
            L_0x0048:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x009a
            L_0x0050:
                java.lang.Object r1 = r14.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r15)
                goto L_0x0083
            L_0x0058:
                com.fossil.t87.a(r15)
                com.fossil.yi7 r1 = r14.p$
                com.portfolio.platform.PortfolioApp$a r15 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r15 = r15.c()
                com.fossil.ch5 r15 = r15.v()
                boolean r15 = r15.U()
                if (r15 != 0) goto L_0x0083
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.b()
                com.fossil.jt5$c$a r5 = new com.fossil.jt5$c$a
                r5.<init>(r4)
                r14.L$0 = r1
                r14.label = r2
                java.lang.Object r15 = com.fossil.vh7.a(r15, r5, r14)
                if (r15 != r0) goto L_0x0083
                return r0
            L_0x0083:
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.b()
                com.fossil.jt5$c$e r5 = new com.fossil.jt5$c$e
                r5.<init>(r14, r4)
                r14.L$0 = r1
                r6 = 2
                r14.label = r6
                java.lang.Object r15 = com.fossil.vh7.a(r15, r5, r14)
                if (r15 != r0) goto L_0x009a
                return r0
            L_0x009a:
                com.fossil.fl4$c r15 = (com.fossil.fl4.c) r15
                boolean r5 = r15 instanceof com.fossil.nw5.a
                if (r5 == 0) goto L_0x02b1
                com.fossil.jt5 r5 = r14.this$0
                com.fossil.ft5 r5 = r5.k
                r5.a()
                com.fossil.jt5 r5 = r14.this$0
                java.util.List r5 = r5.k()
                int r5 = r5.size()
                com.fossil.nw5$a r15 = (com.fossil.nw5.a) r15
                java.util.List r6 = r15.a()
                int r6 = r6.size()
                if (r5 <= r6) goto L_0x0136
                com.fossil.jt5 r5 = r14.this$0
                java.util.List r6 = r5.k()
                java.util.ArrayList r7 = new java.util.ArrayList
                r7.<init>()
                java.util.Iterator r6 = r6.iterator()
            L_0x00ce:
                boolean r8 = r6.hasNext()
                if (r8 == 0) goto L_0x012d
                java.lang.Object r8 = r6.next()
                r9 = r8
                com.fossil.at5 r9 = (com.fossil.at5) r9
                java.util.List r10 = r15.a()
                java.util.Iterator r10 = r10.iterator()
            L_0x00e3:
                boolean r11 = r10.hasNext()
                if (r11 == 0) goto L_0x0117
                java.lang.Object r11 = r10.next()
                r12 = r11
                com.fossil.at5 r12 = (com.fossil.at5) r12
                com.portfolio.platform.data.model.InstalledApp r12 = r12.getInstalledApp()
                if (r12 == 0) goto L_0x00fb
                java.lang.String r12 = r12.getIdentifier()
                goto L_0x00fc
            L_0x00fb:
                r12 = r4
            L_0x00fc:
                com.portfolio.platform.data.model.InstalledApp r13 = r9.getInstalledApp()
                if (r13 == 0) goto L_0x0107
                java.lang.String r13 = r13.getIdentifier()
                goto L_0x0108
            L_0x0107:
                r13 = r4
            L_0x0108:
                boolean r12 = com.fossil.ee7.a(r12, r13)
                java.lang.Boolean r12 = com.fossil.pb7.a(r12)
                boolean r12 = r12.booleanValue()
                if (r12 == 0) goto L_0x00e3
                goto L_0x0118
            L_0x0117:
                r11 = r4
            L_0x0118:
                com.fossil.at5 r11 = (com.fossil.at5) r11
                if (r11 == 0) goto L_0x011e
                r9 = 1
                goto L_0x011f
            L_0x011e:
                r9 = 0
            L_0x011f:
                java.lang.Boolean r9 = com.fossil.pb7.a(r9)
                boolean r9 = r9.booleanValue()
                if (r9 == 0) goto L_0x00ce
                r7.add(r8)
                goto L_0x00ce
            L_0x012d:
                java.util.List r6 = com.fossil.ea7.d(r7)
                r5.a(r6)
                goto L_0x01af
            L_0x0136:
                if (r5 >= r6) goto L_0x01af
                java.util.List r5 = r15.a()
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                java.util.Iterator r5 = r5.iterator()
            L_0x0145:
                boolean r7 = r5.hasNext()
                if (r7 == 0) goto L_0x01a6
                java.lang.Object r7 = r5.next()
                r8 = r7
                com.fossil.at5 r8 = (com.fossil.at5) r8
                com.fossil.jt5 r9 = r14.this$0
                java.util.List r9 = r9.k()
                java.util.Iterator r9 = r9.iterator()
            L_0x015c:
                boolean r10 = r9.hasNext()
                if (r10 == 0) goto L_0x0190
                java.lang.Object r10 = r9.next()
                r11 = r10
                com.fossil.at5 r11 = (com.fossil.at5) r11
                com.portfolio.platform.data.model.InstalledApp r12 = r8.getInstalledApp()
                if (r12 == 0) goto L_0x0174
                java.lang.String r12 = r12.getIdentifier()
                goto L_0x0175
            L_0x0174:
                r12 = r4
            L_0x0175:
                com.portfolio.platform.data.model.InstalledApp r11 = r11.getInstalledApp()
                if (r11 == 0) goto L_0x0180
                java.lang.String r11 = r11.getIdentifier()
                goto L_0x0181
            L_0x0180:
                r11 = r4
            L_0x0181:
                boolean r11 = com.fossil.ee7.a(r12, r11)
                java.lang.Boolean r11 = com.fossil.pb7.a(r11)
                boolean r11 = r11.booleanValue()
                if (r11 == 0) goto L_0x015c
                goto L_0x0191
            L_0x0190:
                r10 = r4
            L_0x0191:
                com.fossil.at5 r10 = (com.fossil.at5) r10
                if (r10 != 0) goto L_0x0197
                r8 = 1
                goto L_0x0198
            L_0x0197:
                r8 = 0
            L_0x0198:
                java.lang.Boolean r8 = com.fossil.pb7.a(r8)
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x0145
                r6.add(r7)
                goto L_0x0145
            L_0x01a6:
                com.fossil.jt5 r5 = r14.this$0
                java.util.List r5 = r5.k()
                r5.addAll(r6)
            L_0x01af:
                com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()
                com.fossil.jt5$a r6 = com.fossil.jt5.s
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "GetApps onSuccess size "
                r7.append(r8)
                java.util.List r15 = r15.a()
                int r15 = r15.size()
                r7.append(r15)
                java.lang.String r15 = " isAllAppEnabled "
                r7.append(r15)
                com.fossil.jt5 r15 = r14.this$0
                boolean r15 = r15.j()
                r7.append(r15)
                java.lang.String r15 = r7.toString()
                r5.d(r6, r15)
                com.fossil.jt5 r15 = r14.this$0
                java.util.List r15 = r15.f
                r15.clear()
                com.fossil.jt5 r15 = r14.this$0
                java.util.List r15 = r15.k()
                java.util.Iterator r15 = r15.iterator()
            L_0x01f8:
                boolean r5 = r15.hasNext()
                if (r5 == 0) goto L_0x0223
                java.lang.Object r5 = r15.next()
                com.fossil.at5 r5 = (com.fossil.at5) r5
                com.portfolio.platform.data.model.InstalledApp r5 = r5.getInstalledApp()
                if (r5 == 0) goto L_0x01f8
                java.lang.Boolean r6 = r5.isSelected()
                java.lang.String r7 = "it.isSelected"
                com.fossil.ee7.a(r6, r7)
                boolean r6 = r6.booleanValue()
                if (r6 == 0) goto L_0x01f8
                com.fossil.jt5 r6 = r14.this$0
                java.util.List r6 = r6.f
                r6.add(r5)
                goto L_0x01f8
            L_0x0223:
                com.fossil.jt5 r15 = r14.this$0
                boolean r15 = r15.j()
                if (r15 == 0) goto L_0x0230
                com.fossil.jt5 r15 = r14.this$0
                r15.c(r2)
            L_0x0230:
                com.fossil.jt5 r15 = r14.this$0
                boolean r15 = r15.j()
                if (r15 != 0) goto L_0x0277
                com.fossil.jt5 r15 = r14.this$0
                java.util.List r15 = r15.k()
                boolean r5 = r15 instanceof java.util.Collection
                if (r5 == 0) goto L_0x024a
                boolean r5 = r15.isEmpty()
                if (r5 == 0) goto L_0x024a
            L_0x0248:
                r15 = 0
                goto L_0x0275
            L_0x024a:
                java.util.Iterator r15 = r15.iterator()
            L_0x024e:
                boolean r5 = r15.hasNext()
                if (r5 == 0) goto L_0x0248
                java.lang.Object r5 = r15.next()
                com.fossil.at5 r5 = (com.fossil.at5) r5
                com.portfolio.platform.data.model.InstalledApp r5 = r5.getInstalledApp()
                if (r5 == 0) goto L_0x0271
                java.lang.Boolean r5 = r5.isSelected()
                java.lang.String r6 = "it.installedApp!!.isSelected"
                com.fossil.ee7.a(r5, r6)
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x024e
                r15 = 1
                goto L_0x0275
            L_0x0271:
                com.fossil.ee7.a()
                throw r4
            L_0x0275:
                if (r15 == 0) goto L_0x0299
            L_0x0277:
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ft5 r15 = r15.k
                if (r15 == 0) goto L_0x02a9
                com.fossil.gt5 r15 = (com.fossil.gt5) r15
                android.content.Context r6 = r15.getContext()
                if (r6 == 0) goto L_0x0299
                com.fossil.xg5 r5 = com.fossil.xg5.b
                com.fossil.xg5$a r7 = com.fossil.xg5.a.NOTIFICATION_APPS
                r8 = 0
                r9 = 0
                r10 = 0
                r11 = 0
                r12 = 60
                r13 = 0
                boolean r15 = com.fossil.xg5.a(r5, r6, r7, r8, r9, r10, r11, r12, r13)
                com.fossil.pb7.a(r15)
            L_0x0299:
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ft5 r15 = r15.k
                com.fossil.jt5 r5 = r14.this$0
                java.util.List r5 = r5.k()
                r15.k(r5)
                goto L_0x02cf
            L_0x02a9:
                com.fossil.x87 r15 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment"
                r15.<init>(r0)
                throw r15
            L_0x02b1:
                boolean r15 = r15 instanceof com.fossil.fl4.a
                if (r15 == 0) goto L_0x02cf
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                com.fossil.jt5$a r5 = com.fossil.jt5.s
                java.lang.String r5 = r5.a()
                java.lang.String r6 = "GetApps onError"
                r15.d(r5, r6)
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ft5 r15 = r15.k
                r15.a()
            L_0x02cf:
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.b()
                com.fossil.jt5$c$d r5 = new com.fossil.jt5$c$d
                r5.<init>(r14, r4)
                r14.L$0 = r1
                r6 = 3
                r14.label = r6
                java.lang.Object r15 = com.fossil.vh7.a(r15, r5, r14)
                if (r15 != r0) goto L_0x0045
                return r0
            L_0x02e6:
                r1 = r15
                com.fossil.fl4$c r1 = (com.fossil.fl4.c) r1
                boolean r15 = r1 instanceof com.fossil.vu5.d
                if (r15 == 0) goto L_0x03d1
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                com.fossil.jt5$a r6 = com.fossil.jt5.s
                java.lang.String r6 = r6.a()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "GetAllContactGroup onSuccess, size = "
                r7.append(r8)
                r8 = r1
                com.fossil.vu5$d r8 = (com.fossil.vu5.d) r8
                java.util.List r9 = r8.a()
                int r9 = r9.size()
                r7.append(r9)
                java.lang.String r7 = r7.toString()
                r15.d(r6, r7)
                com.fossil.jt5 r15 = r14.this$0
                java.util.List r15 = r15.i
                r15.clear()
                com.fossil.jt5 r15 = r14.this$0
                java.util.List r6 = r8.a()
                java.util.List r6 = com.fossil.ea7.d(r6)
                r15.i = r6
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ti7 r15 = r15.c()
                com.fossil.jt5$c$f r6 = new com.fossil.jt5$c$f
                r6.<init>(r14, r4)
                r14.L$0 = r5
                r14.L$1 = r1
                r7 = 4
                r14.label = r7
                java.lang.Object r15 = com.fossil.vh7.a(r15, r6, r14)
                if (r15 != r0) goto L_0x0347
                return r0
            L_0x0347:
                java.util.List r15 = (java.util.List) r15
                boolean r6 = r15.isEmpty()
                if (r6 == 0) goto L_0x0389
                java.util.ArrayList r6 = new java.util.ArrayList
                r6.<init>()
                com.portfolio.platform.data.model.NotificationSettingsModel r7 = new com.portfolio.platform.data.model.NotificationSettingsModel
                java.lang.String r8 = "AllowCallsFrom"
                r7.<init>(r8, r3, r2)
                com.portfolio.platform.data.model.NotificationSettingsModel r2 = new com.portfolio.platform.data.model.NotificationSettingsModel
                java.lang.String r8 = "AllowMessagesFrom"
                r2.<init>(r8, r3, r3)
                r6.add(r7)
                r6.add(r2)
                com.fossil.jt5 r3 = r14.this$0
                com.fossil.ti7 r3 = r3.c()
                com.fossil.jt5$c$b r8 = new com.fossil.jt5$c$b
                r8.<init>(r14, r6, r4)
                r14.L$0 = r5
                r14.L$1 = r1
                r14.L$2 = r15
                r14.L$3 = r6
                r14.L$4 = r7
                r14.L$5 = r2
                r15 = 5
                r14.label = r15
                java.lang.Object r15 = com.fossil.vh7.a(r3, r8, r14)
                if (r15 != r0) goto L_0x03e6
                return r0
            L_0x0389:
                com.fossil.jt5 r2 = r14.this$0
                java.util.List r2 = r2.h
                r2.clear()
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.jt5$a r3 = com.fossil.jt5.s
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r6 = new java.lang.StringBuilder
                r6.<init>()
                java.lang.String r7 = "listNotificationSettings.size = "
                r6.append(r7)
                int r7 = r15.size()
                r6.append(r7)
                java.lang.String r6 = r6.toString()
                r2.d(r3, r6)
                com.fossil.jt5 r2 = r14.this$0
                com.fossil.ti7 r2 = r2.b()
                com.fossil.jt5$c$c r3 = new com.fossil.jt5$c$c
                r3.<init>(r14, r15, r4)
                r14.L$0 = r5
                r14.L$1 = r1
                r14.L$2 = r15
                r15 = 6
                r14.label = r15
                java.lang.Object r15 = com.fossil.vh7.a(r2, r3, r14)
                if (r15 != r0) goto L_0x03e6
                return r0
            L_0x03d1:
                boolean r15 = r1 instanceof com.fossil.vu5.b
                if (r15 == 0) goto L_0x03e6
                com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
                com.fossil.jt5$a r0 = com.fossil.jt5.s
                java.lang.String r0 = r0.a()
                java.lang.String r1 = "GetAllContactGroup onError"
                r15.d(r0, r1)
            L_0x03e6:
                com.fossil.jt5 r15 = r14.this$0
                com.fossil.ft5 r15 = r15.k
                com.fossil.jt5 r0 = r14.this$0
                boolean r0 = r0.j()
                r15.j(r0)
                com.fossil.i97 r15 = com.fossil.i97.a
                return r15
                switch-data {0->0x0058, 1->0x0050, 2->0x0048, 3->0x003e, 4->0x0031, 5->0x0014, 6->0x0020, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt5.c.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = jt5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationAppsPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public jt5(ft5 ft5, rl4 rl4, nw5 nw5, vu5 vu5, mt5 mt5, ch5 ch5, NotificationSettingsDatabase notificationSettingsDatabase) {
        ee7.b(ft5, "mView");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(nw5, "mGetApps");
        ee7.b(vu5, "mGetAllContactGroup");
        ee7.b(mt5, "mSaveAppsNotification");
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = ft5;
        this.l = rl4;
        this.m = nw5;
        this.n = vu5;
        this.o = mt5;
        this.p = ch5;
        this.q = notificationSettingsDatabase;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "init mIsAllAppToggleEnable " + this.e);
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(r, "registerBroadcastReceiver");
        nj5.d.a(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void n() {
        this.k.a(this);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(r, "unregisterBroadcastReceiver");
        nj5.d.b(this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final boolean b(boolean z) {
        int i2;
        int i3;
        List<at5> list = this.g;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            i2 = 0;
            for (T t : list) {
                InstalledApp installedApp = t.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected != null) {
                    if ((isSelected.booleanValue() == z && t.getUri() != null) && (i2 = i2 + 1) < 0) {
                        w97.b();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else {
            i2 = 0;
        }
        List<at5> list2 = this.g;
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            Iterator<T> it = list2.iterator();
            i3 = 0;
            while (it.hasNext()) {
                if ((it.next().getUri() != null) && (i3 = i3 + 1) < 0) {
                    w97.b();
                    throw null;
                }
            }
        } else {
            i3 = 0;
        }
        if (i2 == i3) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void c(boolean z) {
        InstalledApp installedApp;
        for (at5 at5 : this.g) {
            if (!(at5.getUri() == null || (installedApp = at5.getInstalledApp()) == null)) {
                installedApp.setSelected(z);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        m();
        this.k.b();
        ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        o();
        this.k.a();
    }

    @DexIgnore
    @Override // com.fossil.et5
    public void h() {
        if (!l()) {
            this.p.a(this.e);
            FLogger.INSTANCE.getLocal().d(r, "closeAndSave, nothing changed");
            this.k.close();
            return;
        }
        xg5 xg5 = xg5.b;
        ft5 ft5 = this.k;
        if (ft5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        } else if (xg5.a(xg5, ((gt5) ft5).getContext(), xg5.a.SET_BLE_COMMAND, false, false, false, (Integer) null, 60, (Object) null)) {
            FLogger.INSTANCE.getLocal().d(r, "setRuleToDevice, showProgressDialog");
            this.k.b();
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "filter notification, time start=" + currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "mListAppWrapper.size = " + this.g.size());
            this.h.addAll(mx6.a(this.g, false, 1, null));
            long b2 = PortfolioApp.g0.c().b(new AppNotificationFilterSettings(this.h, System.currentTimeMillis()), PortfolioApp.g0.c().c());
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = r;
            local3.d(str3, "filter notification, time end= " + b2);
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = r;
            local4.d(str4, "delayTime =" + (b2 - currentTimeMillis) + " mili seconds");
        }
    }

    @DexIgnore
    @Override // com.fossil.et5
    public void i() {
        this.k.a();
        this.k.close();
    }

    @DexIgnore
    public final boolean j() {
        return this.e;
    }

    @DexIgnore
    public final List<at5> k() {
        return this.g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean l() {
        /*
            r7 = this;
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.util.List<com.fossil.at5> r1 = r7.g
            java.util.Iterator r1 = r1.iterator()
        L_0x000b:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0030
            java.lang.Object r2 = r1.next()
            com.fossil.at5 r2 = (com.fossil.at5) r2
            com.portfolio.platform.data.model.InstalledApp r2 = r2.getInstalledApp()
            if (r2 == 0) goto L_0x000b
            java.lang.Boolean r3 = r2.isSelected()
            java.lang.String r4 = "it.isSelected"
            com.fossil.ee7.a(r3, r4)
            boolean r3 = r3.booleanValue()
            if (r3 == 0) goto L_0x000b
            r0.add(r2)
            goto L_0x000b
        L_0x0030:
            int r1 = r0.size()
            java.util.List<com.portfolio.platform.data.model.InstalledApp> r2 = r7.f
            int r2 = r2.size()
            r3 = 1
            if (r1 == r2) goto L_0x003e
            return r3
        L_0x003e:
            java.util.Iterator r0 = r0.iterator()
        L_0x0042:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0085
            java.lang.Object r1 = r0.next()
            com.portfolio.platform.data.model.InstalledApp r1 = (com.portfolio.platform.data.model.InstalledApp) r1
            java.util.List<com.portfolio.platform.data.model.InstalledApp> r2 = r7.f
            java.util.Iterator r2 = r2.iterator()
        L_0x0054:
            boolean r4 = r2.hasNext()
            if (r4 == 0) goto L_0x0070
            java.lang.Object r4 = r2.next()
            r5 = r4
            com.portfolio.platform.data.model.InstalledApp r5 = (com.portfolio.platform.data.model.InstalledApp) r5
            java.lang.String r5 = r5.getIdentifier()
            java.lang.String r6 = r1.getIdentifier()
            boolean r5 = com.fossil.ee7.a(r5, r6)
            if (r5 == 0) goto L_0x0054
            goto L_0x0071
        L_0x0070:
            r4 = 0
        L_0x0071:
            com.portfolio.platform.data.model.InstalledApp r4 = (com.portfolio.platform.data.model.InstalledApp) r4
            if (r4 == 0) goto L_0x0084
            java.lang.Boolean r2 = r4.isSelected()
            java.lang.Boolean r1 = r1.isSelected()
            boolean r1 = com.fossil.ee7.a(r2, r1)
            r1 = r1 ^ r3
            if (r1 == 0) goto L_0x0042
        L_0x0084:
            return r3
        L_0x0085:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt5.l():boolean");
    }

    @DexIgnore
    public final void a(List<at5> list) {
        ee7.b(list, "<set-?>");
        this.g = list;
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886089);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886091);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886090);
            ee7.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0034  */
    @Override // com.fossil.et5
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(boolean r10) {
        /*
            r9 = this;
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.jt5.r
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "setEnableAllAppsToggle: enable = "
            r2.append(r3)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            r9.e = r10
            r0 = 0
            r1 = 1
            if (r10 != 0) goto L_0x002c
            boolean r2 = r9.b(r1)
            if (r2 == 0) goto L_0x002c
            r9.c(r0)
            goto L_0x0031
        L_0x002c:
            if (r10 == 0) goto L_0x0032
            r9.c(r1)
        L_0x0031:
            r0 = 1
        L_0x0032:
            if (r0 == 0) goto L_0x003b
            com.fossil.ft5 r10 = r9.k
            java.util.List<com.fossil.at5> r0 = r9.g
            r10.k(r0)
        L_0x003b:
            boolean r10 = r9.e
            if (r10 == 0) goto L_0x0060
            com.fossil.xg5 r0 = com.fossil.xg5.b
            com.fossil.ft5 r10 = r9.k
            if (r10 == 0) goto L_0x0058
            com.fossil.gt5 r10 = (com.fossil.gt5) r10
            android.content.Context r1 = r10.getContext()
            com.fossil.xg5$a r2 = com.fossil.xg5.a.NOTIFICATION_APPS
            r3 = 0
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 60
            r8 = 0
            com.fossil.xg5.a(r0, r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x0060
        L_0x0058:
            com.fossil.x87 r10 = new com.fossil.x87
            java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment"
            r10.<init>(r0)
            throw r10
        L_0x0060:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jt5.a(boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.et5
    public void a(at5 at5, boolean z) {
        T t;
        InstalledApp installedApp;
        ee7.b(at5, "appWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        StringBuilder sb = new StringBuilder();
        sb.append("setAppState: appName = ");
        InstalledApp installedApp2 = at5.getInstalledApp();
        sb.append(installedApp2 != null ? installedApp2.getTitle() : null);
        sb.append(", selected = ");
        sb.append(z);
        local.d(str, sb.toString());
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (ee7.a(t.getUri(), at5.getUri())) {
                break;
            }
        }
        T t2 = t;
        if (!(t2 == null || (installedApp = t2.getInstalledApp()) == null)) {
            installedApp.setSelected(z);
        }
        boolean z2 = false;
        if (b(z) && this.e != z) {
            this.e = z;
            this.k.j(z);
        } else if (!z && this.e) {
            this.e = false;
            this.k.j(false);
        }
        if (!this.e) {
            List<at5> list = this.g;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    InstalledApp installedApp3 = it2.next().getInstalledApp();
                    if (installedApp3 != null) {
                        Boolean isSelected = installedApp3.isSelected();
                        ee7.a((Object) isSelected, "it.installedApp!!.isSelected");
                        if (isSelected.booleanValue()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
            if (!z2) {
                return;
            }
        }
        ft5 ft5 = this.k;
        if (ft5 != null) {
            Context context = ((gt5) ft5).getContext();
            if (context != null) {
                xg5.a(xg5.b, context, xg5.a.NOTIFICATION_APPS, false, false, false, (Integer) null, 60, (Object) null);
                return;
            }
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }
}
