package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw7 {
    @DexIgnore
    public /* final */ b a;
    @DexIgnore
    public /* final */ a b;
    @DexIgnore
    public /* final */ MethodChannel c; // = new MethodChannel(this.d.messenger(), "top.kikt/photo_manager/notify");
    @DexIgnore
    public /* final */ PluginRegistry.Registrar d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        public /* final */ /* synthetic */ uw7 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(uw7 uw7, Handler handler) {
            super(handler);
            ee7.b(handler, "handler");
            this.a = uw7;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.a.a(z, uri);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends ContentObserver {
        @DexIgnore
        public /* final */ /* synthetic */ uw7 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(uw7 uw7, Handler handler) {
            super(handler);
            ee7.b(handler, "handler");
            this.a = uw7;
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            super.onChange(z, uri);
            this.a.a(z, uri);
        }
    }

    @DexIgnore
    public uw7(PluginRegistry.Registrar registrar, Handler handler) {
        ee7.b(registrar, "registry");
        ee7.b(handler, "handler");
        this.d = registrar;
        this.a = new b(this, handler);
        this.b = new a(this, handler);
    }

    @DexIgnore
    public final Context a() {
        Context context = this.d.context();
        ee7.a((Object) context, "registry.context()");
        return context.getApplicationContext();
    }

    @DexIgnore
    public final void b() {
        Uri uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Uri uri2 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
        Context a2 = a();
        ee7.a((Object) a2, "context");
        a2.getContentResolver().registerContentObserver(uri, false, this.b);
        Context a3 = a();
        ee7.a((Object) a3, "context");
        a3.getContentResolver().registerContentObserver(uri2, false, this.a);
    }

    @DexIgnore
    public final void c() {
        Context a2 = a();
        ee7.a((Object) a2, "context");
        a2.getContentResolver().unregisterContentObserver(this.b);
        Context a3 = a();
        ee7.a((Object) a3, "context");
        a3.getContentResolver().unregisterContentObserver(this.a);
    }

    @DexIgnore
    public final void a(boolean z, Uri uri) {
        this.c.invokeMethod("change", oa7.c(w87.a("android-self", Boolean.valueOf(z)), w87.a("android-uri", String.valueOf(uri))));
    }

    @DexIgnore
    public final void a(boolean z) {
        this.c.invokeMethod("setAndroidQExperimental", na7.a(w87.a("open", Boolean.valueOf(z))));
    }
}
