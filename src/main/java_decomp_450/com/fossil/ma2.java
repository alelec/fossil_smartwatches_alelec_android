package com.fossil;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ma2 {
    @DexIgnore
    public static volatile w82 a;
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static Context c;

    @DexIgnore
    public static synchronized void a(Context context) {
        synchronized (ma2.class) {
            if (c != null) {
                Log.w("GoogleCertificates", "GoogleCertificates has been initialized already");
            } else if (context != null) {
                c = context.getApplicationContext();
            }
        }
    }

    @DexIgnore
    public static va2 b(String str, na2 na2, boolean z, boolean z2) {
        try {
            if (a == null) {
                a72.a(c);
                synchronized (b) {
                    if (a == null) {
                        a = y82.a(DynamiteModule.a(c, DynamiteModule.k, "com.google.android.gms.googlecertificates").a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            a72.a(c);
            try {
                if (a.a(new ta2(str, na2, z, z2), cb2.a(c.getPackageManager()))) {
                    return va2.c();
                }
                return va2.a(new oa2(z, str, na2));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return va2.a("module call", e);
            }
        } catch (DynamiteModule.a e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return va2.a(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }

    @DexIgnore
    public static va2 a(String str, na2 na2, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return b(str, na2, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    @DexIgnore
    public static final /* synthetic */ String a(boolean z, String str, na2 na2) throws Exception {
        boolean z2 = true;
        if (z || !b(str, na2, true, false).a) {
            z2 = false;
        }
        return va2.a(str, na2, z, z2);
    }
}
