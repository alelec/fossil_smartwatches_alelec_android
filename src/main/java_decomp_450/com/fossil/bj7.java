package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bj7 {
    DEFAULT,
    LAZY,
    ATOMIC,
    UNDISPATCHED;

    @DexIgnore
    public static /* synthetic */ void isLazy$annotations() {
    }

    @DexIgnore
    public final <T> void invoke(gd7<? super fb7<? super T>, ? extends Object> gd7, fb7<? super T> fb7) {
        int i = aj7.a[ordinal()];
        if (i == 1) {
            tm7.a(gd7, fb7);
        } else if (i == 2) {
            hb7.a(gd7, fb7);
        } else if (i == 3) {
            um7.a(gd7, fb7);
        } else if (i != 4) {
            throw new p87();
        }
    }

    @DexIgnore
    public final boolean isLazy() {
        return this == LAZY;
    }

    @DexIgnore
    public final <R, T> void invoke(kd7<? super R, ? super fb7<? super T>, ? extends Object> kd7, R r, fb7<? super T> fb7) {
        int i = aj7.b[ordinal()];
        if (i == 1) {
            tm7.a(kd7, r, fb7);
        } else if (i == 2) {
            hb7.a(kd7, r, fb7);
        } else if (i == 3) {
            um7.a(kd7, r, fb7);
        } else if (i != 4) {
            throw new p87();
        }
    }
}
