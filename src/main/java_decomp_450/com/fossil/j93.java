package com.fossil;

import android.os.RemoteException;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j93 {
    @DexIgnore
    public /* final */ en2 a;

    @DexIgnore
    public j93(en2 en2) {
        a72.a(en2);
        this.a = en2;
    }

    @DexIgnore
    public final String a() {
        try {
            return this.a.getId();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final LatLng b() {
        try {
            return this.a.getPosition();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void c(float f) {
        try {
            this.a.setZIndex(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean d() {
        try {
            return this.a.v();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void e() {
        try {
            this.a.remove();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof j93)) {
            return false;
        }
        try {
            return this.a.b(((j93) obj).a);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void f() {
        try {
            this.a.f();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final int hashCode() {
        try {
            return this.a.a();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(LatLng latLng) {
        if (latLng != null) {
            try {
                this.a.setPosition(latLng);
            } catch (RemoteException e) {
                throw new r93(e);
            }
        } else {
            throw new IllegalArgumentException("latlng cannot be null - a position is required.");
        }
    }

    @DexIgnore
    public final void b(float f, float f2) {
        try {
            this.a.setInfoWindowAnchor(f, f2);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void c() {
        try {
            this.a.r();
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(String str) {
        try {
            this.a.setTitle(str);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void c(boolean z) {
        try {
            this.a.setVisible(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(y83 y83) {
        if (y83 == null) {
            try {
                this.a.b((ab2) null);
            } catch (RemoteException e) {
                throw new r93(e);
            }
        } else {
            this.a.b(y83.a());
        }
    }

    @DexIgnore
    public final void b(boolean z) {
        try {
            this.a.setFlat(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void b(float f) {
        try {
            this.a.setRotation(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(float f, float f2) {
        try {
            this.a.setAnchor(f, f2);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(String str) {
        try {
            this.a.b(str);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        try {
            this.a.setDraggable(z);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }

    @DexIgnore
    public final void a(float f) {
        try {
            this.a.setAlpha(f);
        } catch (RemoteException e) {
            throw new r93(e);
        }
    }
}
