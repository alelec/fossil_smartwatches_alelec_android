package com.fossil;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.view.animation.Animation;
import android.widget.ImageView;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ar5 extends ImageView {
    @DexIgnore
    public Animation.AnimationListener a;
    @DexIgnore
    public int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends OvalShape {
        @DexIgnore
        public RadialGradient a;
        @DexIgnore
        public Paint b; // = new Paint();

        @DexIgnore
        public a(int i) {
            ar5.this.b = i;
            a((int) rect().width());
        }

        @DexIgnore
        public final void a(int i) {
            float f = (float) (i / 2);
            RadialGradient radialGradient = new RadialGradient(f, f, (float) ar5.this.b, new int[]{1023410176, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.a = radialGradient;
            this.b.setShader(radialGradient);
        }

        @DexIgnore
        public void draw(Canvas canvas, Paint paint) {
            int width = ar5.this.getWidth() / 2;
            float f = (float) width;
            float height = (float) (ar5.this.getHeight() / 2);
            canvas.drawCircle(f, height, f, this.b);
            canvas.drawCircle(f, height, (float) (width - ar5.this.b), paint);
        }

        @DexIgnore
        public void onResize(float f, float f2) {
            super.onResize(f, f2);
            a((int) f);
        }
    }

    @DexIgnore
    public ar5(Context context, int i) {
        super(context);
        ShapeDrawable shapeDrawable;
        float f = getContext().getResources().getDisplayMetrics().density;
        int i2 = (int) (1.75f * f);
        int i3 = (int) (LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES * f);
        this.b = (int) (3.5f * f);
        if (a()) {
            shapeDrawable = new ShapeDrawable(new OvalShape());
            da.a(this, f * 4.0f);
        } else {
            ShapeDrawable shapeDrawable2 = new ShapeDrawable(new a(this.b));
            setLayerType(1, shapeDrawable2.getPaint());
            shapeDrawable2.getPaint().setShadowLayer((float) this.b, (float) i3, (float) i2, 503316480);
            int i4 = this.b;
            setPadding(i4, i4, i4, i4);
            shapeDrawable = shapeDrawable2;
        }
        shapeDrawable.getPaint().setColor(i);
        da.a(this, shapeDrawable);
    }

    @DexIgnore
    public final boolean a() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public void onAnimationEnd() {
        super.onAnimationEnd();
        Animation.AnimationListener animationListener = this.a;
        if (animationListener != null) {
            animationListener.onAnimationEnd(getAnimation());
        }
    }

    @DexIgnore
    public void onAnimationStart() {
        super.onAnimationStart();
        Animation.AnimationListener animationListener = this.a;
        if (animationListener != null) {
            animationListener.onAnimationStart(getAnimation());
        }
    }

    @DexIgnore
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!a()) {
            setMeasuredDimension(getMeasuredWidth() + (this.b * 2), getMeasuredHeight() + (this.b * 2));
        }
    }

    @DexIgnore
    public void setBackgroundColor(int i) {
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i);
        }
    }

    @DexIgnore
    public void a(Animation.AnimationListener animationListener) {
        this.a = animationListener;
    }
}
