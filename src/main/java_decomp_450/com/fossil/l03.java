package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l03 implements i03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a;
    @DexIgnore
    public static /* final */ tq2<Boolean> b;

    /*
    static {
        dr2 dr2 = new dr2(uq2.a("com.google.android.gms.measurement"));
        a = dr2.a("measurement.client.consent_state_v1.dev", false);
        b = dr2.a("measurement.service.consent_state_v1", false);
    }
    */

    @DexIgnore
    @Override // com.fossil.i03
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    @Override // com.fossil.i03
    public final boolean zzb() {
        return b.b().booleanValue();
    }
}
