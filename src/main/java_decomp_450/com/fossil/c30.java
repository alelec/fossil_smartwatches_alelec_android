package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c30 implements f30<Bitmap, BitmapDrawable> {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public c30(Resources resources) {
        u50.a(resources);
        this.a = resources;
    }

    @DexIgnore
    @Override // com.fossil.f30
    public uy<BitmapDrawable> a(uy<Bitmap> uyVar, ax axVar) {
        return a20.a(this.a, uyVar);
    }
}
