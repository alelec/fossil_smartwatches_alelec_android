package com.fossil;

import com.fossil.pu6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface fv6 extends dl4<ev6> {
    @DexIgnore
    void M();

    @DexIgnore
    void X0();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(long j);

    @DexIgnore
    void a(pu6.d dVar);

    @DexIgnore
    void a(Double d, Double d2);

    @DexIgnore
    void a(boolean z, boolean z2);

    @DexIgnore
    void g(int i);

    @DexIgnore
    void v(String str);
}
