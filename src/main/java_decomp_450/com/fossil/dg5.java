package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dg5 extends sf5 {
    @DexIgnore
    public dg5(xf5 xf5, vf5 vf5) {
        super(xf5, vf5);
    }

    @DexIgnore
    @Override // com.fossil.rf5
    public qf5 a() {
        return ((rf5) this).a.a(137);
    }

    @DexIgnore
    public byte[] b() {
        return ((rf5) this).a.e(132);
    }
}
