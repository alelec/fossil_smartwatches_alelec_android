package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx6 {
    @DexIgnore
    public static /* final */ n87 b; // = o87.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ b d; // = new b(null);
    @DexIgnore
    public ch5 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fe7 implements vc7<wx6> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final wx6 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final wx6 a() {
            n87 b = wx6.b;
            b bVar = wx6.d;
            return (wx6) b.getValue();
        }

        @DexIgnore
        public /* synthetic */ b(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ wx6 a; // = new wx6();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final wx6 a() {
            return a;
        }
    }

    /*
    static {
        String simpleName = wx6.class.getSimpleName();
        ee7.a((Object) simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public wx6() {
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        ch5 ch5 = this.a;
        if (ch5 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - ch5.q());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", " + "successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                ch5 ch52 = this.a;
                if (ch52 == null) {
                    ee7.d("mSharePrefs");
                    throw null;
                } else if (ch52.i() >= i2) {
                    return true;
                }
            }
            return false;
        }
        ee7.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        ch5 ch5 = this.a;
        if (ch5 != null) {
            return ch5.Q();
        }
        ee7.d("mSharePrefs");
        throw null;
    }
}
