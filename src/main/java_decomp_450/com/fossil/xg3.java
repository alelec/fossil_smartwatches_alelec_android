package com.fossil;

import android.content.SharedPreferences;
import android.os.Bundle;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xg3 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Bundle b; // = new Bundle();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public Bundle d;
    @DexIgnore
    public /* final */ /* synthetic */ wg3 e;

    @DexIgnore
    public xg3(wg3 wg3, String str, Bundle bundle) {
        this.e = wg3;
        a72.b(str);
        this.a = str;
    }

    @DexIgnore
    public final Bundle a() {
        if (!this.c) {
            this.c = true;
            String string = this.e.s().getString(this.a, null);
            if (string != null) {
                try {
                    Bundle bundle = new Bundle();
                    JSONArray jSONArray = new JSONArray(string);
                    for (int i = 0; i < jSONArray.length(); i++) {
                        JSONObject jSONObject = jSONArray.getJSONObject(i);
                        String string2 = jSONObject.getString("n");
                        String string3 = jSONObject.getString("t");
                        char c2 = '\uffff';
                        int hashCode = string3.hashCode();
                        if (hashCode != 100) {
                            if (hashCode != 108) {
                                if (hashCode == 115 && string3.equals("s")) {
                                    c2 = 0;
                                }
                            } else if (string3.equals("l")) {
                                c2 = 2;
                            }
                        } else if (string3.equals("d")) {
                            c2 = 1;
                        }
                        if (c2 == 0) {
                            bundle.putString(string2, jSONObject.getString("v"));
                        } else if (c2 == 1) {
                            bundle.putDouble(string2, Double.parseDouble(jSONObject.getString("v")));
                        } else if (c2 != 2) {
                            try {
                                this.e.e().t().a("Unrecognized persisted bundle type. Type", string3);
                            } catch (NumberFormatException | JSONException unused) {
                                this.e.e().t().a("Error reading value from SharedPreferences. Value dropped");
                            }
                        } else {
                            bundle.putLong(string2, Long.parseLong(jSONObject.getString("v")));
                        }
                    }
                    this.d = bundle;
                } catch (JSONException unused2) {
                    this.e.e().t().a("Error loading bundle from SharedPreferences. Values will be lost");
                }
            }
            if (this.d == null) {
                this.d = this.b;
            }
        }
        return this.d;
    }

    @DexIgnore
    public final String b(Bundle bundle) {
        JSONArray jSONArray = new JSONArray();
        for (String str : bundle.keySet()) {
            Object obj = bundle.get(str);
            if (obj != null) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("n", str);
                    jSONObject.put("v", String.valueOf(obj));
                    if (obj instanceof String) {
                        jSONObject.put("t", "s");
                    } else if (obj instanceof Long) {
                        jSONObject.put("t", "l");
                    } else if (obj instanceof Double) {
                        jSONObject.put("t", "d");
                    } else {
                        this.e.e().t().a("Cannot serialize bundle value to SharedPreferences. Type", obj.getClass());
                    }
                    jSONArray.put(jSONObject);
                } catch (JSONException e2) {
                    this.e.e().t().a("Cannot serialize bundle value to SharedPreferences", e2);
                }
            }
        }
        return jSONArray.toString();
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        if (bundle == null) {
            bundle = new Bundle();
        }
        SharedPreferences.Editor edit = this.e.s().edit();
        if (bundle.size() == 0) {
            edit.remove(this.a);
        } else {
            edit.putString(this.a, b(bundle));
        }
        edit.apply();
        this.d = bundle;
    }
}
