package com.fossil;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.fossil.nj5;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pq5 extends fl4<d, e, c> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public d e;
    @DexIgnore
    public /* final */ b f; // = new b();
    @DexIgnore
    public /* final */ AlarmsRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return pq5.h;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$1", f = "DeleteAlarm.kt", l = {39}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Alarm $requestAlarm;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Alarm alarm, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$requestAlarm = alarm;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$requestAlarm, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    AlarmsRepository a2 = pq5.this.g;
                    Alarm alarm = this.$requestAlarm;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (a2.deleteAlarm(alarm, this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && pq5.this.d()) {
                pq5.this.a(false);
                FLogger.INSTANCE.getLocal().d(pq5.i.a(), "onReceive");
                Alarm a2 = pq5.this.e().a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(pq5.i.a(), "onReceive success");
                    ik7 unused = xh7.b(pq5.this.b(), null, null, new a(this, a2, null), 3, null);
                    pq5.this.a(new e(a2));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = pq5.i.a();
                local.d(a3, "onReceive error - errorCode=" + intExtra);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                pq5.this.a(new c(a2, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            ee7.b(arrayList, "errorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public d(String str, List<Alarm> list, Alarm alarm) {
            ee7.b(str, "deviceId");
            ee7.b(list, "alarms");
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements fl4.d {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public e(Alarm alarm) {
            ee7.b(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm", f = "DeleteAlarm.kt", l = {68, 87}, m = "run")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ pq5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(pq5 pq5, fb7 fb7) {
            super(fb7);
            this.this$0 = pq5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((d) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        String simpleName = pq5.class.getSimpleName();
        ee7.a((Object) simpleName, "DeleteAlarm::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public pq5(AlarmsRepository alarmsRepository) {
        ee7.b(alarmsRepository, "mAlarmsRepository");
        this.g = alarmsRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return h;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }

    @DexIgnore
    public final d e() {
        d dVar = this.e;
        if (dVar != null) {
            return dVar;
        }
        ee7.d("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void f() {
        nj5.d.a(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    public final void g() {
        nj5.d.b(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(d dVar, fb7 fb7) {
        return a(dVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.pq5.d r12, com.fossil.fb7<java.lang.Object> r13) {
        /*
            r11 = this;
            boolean r0 = r13 instanceof com.fossil.pq5.f
            if (r0 == 0) goto L_0x0013
            r0 = r13
            com.fossil.pq5$f r0 = (com.fossil.pq5.f) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.pq5$f r0 = new com.fossil.pq5$f
            r0.<init>(r11, r13)
        L_0x0018:
            java.lang.Object r13 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 2
            r4 = 1
            if (r2 == 0) goto L_0x005d
            if (r2 == r4) goto L_0x0041
            if (r2 != r3) goto L_0x0039
            java.lang.Object r12 = r0.L$2
            com.portfolio.platform.data.source.local.alarm.Alarm r12 = (com.portfolio.platform.data.source.local.alarm.Alarm) r12
            java.lang.Object r1 = r0.L$1
            com.fossil.pq5$d r1 = (com.fossil.pq5.d) r1
            java.lang.Object r0 = r0.L$0
            com.fossil.pq5 r0 = (com.fossil.pq5) r0
            com.fossil.t87.a(r13)
            goto L_0x011b
        L_0x0039:
            java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
            java.lang.String r13 = "call to 'resume' before 'invoke' with coroutine"
            r12.<init>(r13)
            throw r12
        L_0x0041:
            java.lang.Object r12 = r0.L$5
            java.util.Iterator r12 = (java.util.Iterator) r12
            java.lang.Object r2 = r0.L$4
            com.portfolio.platform.data.source.local.alarm.Alarm r2 = (com.portfolio.platform.data.source.local.alarm.Alarm) r2
            java.lang.Object r3 = r0.L$3
            java.util.ArrayList r3 = (java.util.ArrayList) r3
            java.lang.Object r5 = r0.L$2
            com.portfolio.platform.data.source.local.alarm.Alarm r5 = (com.portfolio.platform.data.source.local.alarm.Alarm) r5
            java.lang.Object r6 = r0.L$1
            com.fossil.pq5$d r6 = (com.fossil.pq5.d) r6
            java.lang.Object r7 = r0.L$0
            com.fossil.pq5 r7 = (com.fossil.pq5) r7
            com.fossil.t87.a(r13)
            goto L_0x00b9
        L_0x005d:
            com.fossil.t87.a(r13)
            com.misfit.frameworks.buttonservice.log.FLogger r13 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r13 = r13.getLocal()
            java.lang.String r2 = com.fossil.pq5.h
            java.lang.String r5 = "executeUseCase"
            r13.d(r2, r5)
            if (r12 == 0) goto L_0x0121
            r11.e = r12
            r11.d = r4
            com.portfolio.platform.data.source.local.alarm.Alarm r13 = r12.a()
            boolean r2 = r13.isActive()
            if (r2 == 0) goto L_0x0109
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            java.util.List r3 = r12.b()
            java.util.Iterator r3 = r3.iterator()
            r7 = r11
            r5 = r13
            r13 = r12
            r12 = r3
            r3 = r2
        L_0x008f:
            boolean r2 = r12.hasNext()
            if (r2 == 0) goto L_0x00ff
            java.lang.Object r2 = r12.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r2 = (com.portfolio.platform.data.source.local.alarm.Alarm) r2
            com.portfolio.platform.data.source.AlarmsRepository r6 = r7.g
            java.lang.String r8 = r2.getUri()
            r0.L$0 = r7
            r0.L$1 = r13
            r0.L$2 = r5
            r0.L$3 = r3
            r0.L$4 = r2
            r0.L$5 = r12
            r0.label = r4
            java.lang.Object r6 = r6.getAlarmById(r8, r0)
            if (r6 != r1) goto L_0x00b6
            return r1
        L_0x00b6:
            r10 = r6
            r6 = r13
            r13 = r10
        L_0x00b9:
            com.portfolio.platform.data.source.local.alarm.Alarm r13 = (com.portfolio.platform.data.source.local.alarm.Alarm) r13
            if (r13 == 0) goto L_0x00fa
            java.lang.String r8 = r13.getUri()
            java.lang.String r9 = r5.getUri()
            boolean r8 = com.fossil.ee7.a(r8, r9)
            r8 = r8 ^ r4
            if (r8 == 0) goto L_0x00fd
            int r8 = r2.getMinute()
            r13.setMinute(r8)
            int[] r8 = r2.getDays()
            r13.setDays(r8)
            int r8 = r2.getHour()
            r13.setHour(r8)
            boolean r8 = r2.isActive()
            r13.setActive(r8)
            java.lang.String r8 = r2.getCreatedAt()
            r13.setCreatedAt(r8)
            boolean r2 = r2.isRepeated()
            r13.setRepeated(r2)
            r3.add(r13)
            goto L_0x00fd
        L_0x00fa:
            r3.add(r2)
        L_0x00fd:
            r13 = r6
            goto L_0x008f
        L_0x00ff:
            java.lang.String r12 = r13.c()
            r7.a(r3, r12)
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        L_0x0109:
            com.portfolio.platform.data.source.AlarmsRepository r2 = r11.g
            r0.L$0 = r11
            r0.L$1 = r12
            r0.L$2 = r13
            r0.label = r3
            java.lang.Object r12 = r2.deleteAlarm(r13, r0)
            if (r12 != r1) goto L_0x011a
            return r1
        L_0x011a:
            r12 = r13
        L_0x011b:
            com.fossil.pq5$e r13 = new com.fossil.pq5$e
            r13.<init>(r12)
            return r13
        L_0x0121:
            com.fossil.ee7.a()
            r12 = 0
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.pq5.a(com.fossil.pq5$d, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(List<Alarm> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "setAlarms - alarms=" + list);
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isActive()) {
                arrayList.add(t);
            }
        }
        PortfolioApp.g0.c().a(str, rc5.a(arrayList));
    }
}
