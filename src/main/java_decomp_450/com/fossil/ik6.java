package com.fossil;

import com.fossil.en5;
import com.fossil.ql4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ik6 extends dk6 {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public /* final */ ek6 e;
    @DexIgnore
    public /* final */ en5 f;
    @DexIgnore
    public /* final */ rl4 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ik6.h;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.d<en5.d, en5.c> {
        @DexIgnore
        public /* final */ /* synthetic */ ik6 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(ik6 ik6) {
            this.a = ik6;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(en5.d dVar) {
            ee7.b(dVar, "successResponse");
            if (this.a.e.isActive()) {
                this.a.e.h();
                this.a.e.V0();
            }
        }

        @DexIgnore
        public void a(en5.c cVar) {
            ee7.b(cVar, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ik6.i.a();
            local.e(a2, "updateUserPassword onFailure: errorValue = " + cVar);
            if (this.a.e.isActive()) {
                this.a.e.h();
                int a3 = cVar.a();
                if (a3 == 400004) {
                    this.a.e.i0();
                } else if (a3 != 403005) {
                    this.a.e.c(cVar.a(), cVar.b());
                } else {
                    this.a.e.x0();
                }
            }
        }
    }

    /*
    static {
        String simpleName = ik6.class.getSimpleName();
        ee7.a((Object) simpleName, "ProfileChangePasswordPre\u2026er::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public ik6(ek6 ek6, en5 en5, rl4 rl4) {
        ee7.b(ek6, "mView");
        ee7.b(en5, "mChangePasswordUseCase");
        ee7.b(rl4, "mUseCaseHandler");
        this.e = ek6;
        this.f = en5;
        this.g = rl4;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(h, "presenter starts");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(h, "presenter stop");
    }

    @DexIgnore
    public void h() {
        this.e.a(this);
    }

    @DexIgnore
    @Override // com.fossil.dk6
    public void a(String str, String str2) {
        ee7.b(str, "oldPass");
        ee7.b(str2, "newPass");
        if (!xw6.b(PortfolioApp.g0.c())) {
            this.e.c(601, null);
            return;
        }
        this.e.j();
        this.g.a(this.f, new en5.b(str, str2), new b(this));
    }
}
