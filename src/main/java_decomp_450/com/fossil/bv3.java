package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bv3 extends av3 {
    @DexIgnore
    public float a; // = -1.0f;

    @DexIgnore
    @Override // com.fossil.av3
    public void a(jv3 jv3, float f, float f2, float f3) {
        jv3.a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3 * f2, 180.0f, 180.0f - f);
        double d = (double) f3;
        double d2 = (double) f2;
        jv3.a((float) (Math.sin(Math.toRadians((double) f)) * d * d2), (float) (Math.sin(Math.toRadians((double) (90.0f - f))) * d * d2));
    }
}
