package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uz4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RingProgressBar A;
    @DexIgnore
    public /* final */ RingProgressBar B;
    @DexIgnore
    public /* final */ RingProgressBar C;
    @DexIgnore
    public /* final */ RingProgressBar D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ View I;
    @DexIgnore
    public /* final */ View J;
    @DexIgnore
    public /* final */ View K;
    @DexIgnore
    public /* final */ View L;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ FlexibleButton v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ ImageView x;
    @DexIgnore
    public /* final */ ImageView y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexIgnore
    public uz4(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, FlexibleButton flexibleButton, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, RingProgressBar ringProgressBar, RingProgressBar ringProgressBar2, RingProgressBar ringProgressBar3, RingProgressBar ringProgressBar4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view2, View view3, View view4, View view5) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = constraintLayout2;
        this.s = constraintLayout3;
        this.t = constraintLayout4;
        this.u = constraintLayout5;
        this.v = flexibleButton;
        this.w = imageView;
        this.x = imageView2;
        this.y = imageView3;
        this.z = imageView4;
        this.A = ringProgressBar;
        this.B = ringProgressBar2;
        this.C = ringProgressBar3;
        this.D = ringProgressBar4;
        this.E = flexibleTextView;
        this.F = flexibleTextView2;
        this.G = flexibleTextView3;
        this.H = flexibleTextView4;
        this.I = view2;
        this.J = view3;
        this.K = view4;
        this.L = view5;
    }
}
