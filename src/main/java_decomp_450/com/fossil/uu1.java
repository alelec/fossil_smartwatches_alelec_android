package com.fossil;

import android.content.Context;
import com.fossil.ku1;
import com.fossil.pu1;
import com.fossil.vu1;
import java.util.Collections;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class uu1 implements tu1 {
    @DexIgnore
    public static volatile vu1 e;
    @DexIgnore
    public /* final */ by1 a;
    @DexIgnore
    public /* final */ by1 b;
    @DexIgnore
    public /* final */ sv1 c;
    @DexIgnore
    public /* final */ jw1 d;

    @DexIgnore
    public uu1(by1 by1, by1 by12, sv1 sv1, jw1 jw1, nw1 nw1) {
        this.a = by1;
        this.b = by12;
        this.c = sv1;
        this.d = jw1;
        nw1.a();
    }

    @DexIgnore
    public static void a(Context context) {
        if (e == null) {
            synchronized (uu1.class) {
                if (e == null) {
                    vu1.a c2 = gu1.c();
                    c2.a(context);
                    e = c2.build();
                }
            }
        }
    }

    @DexIgnore
    public static uu1 b() {
        vu1 vu1 = e;
        if (vu1 != null) {
            return vu1.b();
        }
        throw new IllegalStateException("Not initialized!");
    }

    @DexIgnore
    public static Set<bt1> b(hu1 hu1) {
        if (hu1 instanceof iu1) {
            return Collections.unmodifiableSet(((iu1) hu1).a());
        }
        return Collections.singleton(bt1.a("proto"));
    }

    @DexIgnore
    @Deprecated
    public gt1 a(String str) {
        Set<bt1> b2 = b(null);
        pu1.a d2 = pu1.d();
        d2.a(str);
        return new qu1(b2, d2.a(), this);
    }

    @DexIgnore
    public gt1 a(hu1 hu1) {
        Set<bt1> b2 = b(hu1);
        pu1.a d2 = pu1.d();
        d2.a(hu1.getName());
        d2.a(hu1.getExtras());
        return new qu1(b2, d2.a(), this);
    }

    @DexIgnore
    public jw1 a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.tu1
    public void a(ou1 ou1, ht1 ht1) {
        this.c.a(ou1.e().a(ou1.b().c()), a(ou1), ht1);
    }

    @DexIgnore
    public final ku1 a(ou1 ou1) {
        ku1.a i = ku1.i();
        i.a(this.a.a());
        i.b(this.b.a());
        i.a(ou1.f());
        i.a(new ju1(ou1.a(), ou1.c()));
        i.a(ou1.b().a());
        return i.a();
    }
}
