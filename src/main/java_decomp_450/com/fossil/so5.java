package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class so5 extends RecyclerView.g<a> {
    @DexIgnore
    public List<String> a;
    @DexIgnore
    public b b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ so5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.so5$a$a")
        /* renamed from: com.fossil.so5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0174a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0174a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b b;
                int adapterPosition = this.a.getAdapterPosition();
                if (this.a.getAdapterPosition() != -1 && (b = this.a.b.b) != null) {
                    List a2 = this.a.b.a;
                    if (a2 != null) {
                        b.a((String) a2.get(adapterPosition));
                    } else {
                        ee7.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(so5 so5, View view) {
            super(view);
            ee7.b(view, "view");
            this.b = so5;
            view.setOnClickListener(new View$OnClickListenerC0174a(this));
            View findViewById = view.findViewById(2131362751);
            if (findViewById != null) {
                View findViewById2 = view.findViewById(2131362096);
                if (findViewById2 != null) {
                    String b2 = eh5.l.a().b("nonBrandSeparatorLine");
                    String b3 = eh5.l.a().b(Explore.COLUMN_BACKGROUND);
                    if (!TextUtils.isEmpty(b2)) {
                        findViewById.setBackgroundColor(Color.parseColor(b2));
                    }
                    if (!TextUtils.isEmpty(b3)) {
                        findViewById2.setBackgroundColor(Color.parseColor(b3));
                    }
                    View findViewById3 = view.findViewById(2131363340);
                    if (findViewById3 != null) {
                        this.a = (TextView) findViewById3;
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<String> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        ee7.b(aVar, "holder");
        TextView a2 = aVar.a();
        List<String> list = this.a;
        if (list != null) {
            a2.setText(list.get(i));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558655, viewGroup, false);
        ee7.a((Object) inflate, "view");
        return new a(this, inflate);
    }

    @DexIgnore
    public final void a(List<String> list) {
        ee7.b(list, "addressSearchList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        ee7.b(bVar, "listener");
        this.b = bVar;
    }
}
