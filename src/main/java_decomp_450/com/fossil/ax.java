package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ax implements yw {
    @DexIgnore
    public /* final */ n4<zw<?>, Object> b; // = new m50();

    @DexIgnore
    public void a(ax axVar) {
        this.b.a(axVar.b);
    }

    @DexIgnore
    @Override // com.fossil.yw
    public boolean equals(Object obj) {
        if (obj instanceof ax) {
            return this.b.equals(((ax) obj).b);
        }
        return false;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Options{values=" + this.b + '}';
    }

    @DexIgnore
    public <T> ax a(zw<T> zwVar, T t) {
        this.b.put(zwVar, t);
        return this;
    }

    @DexIgnore
    public <T> T a(zw<T> zwVar) {
        return this.b.containsKey(zwVar) ? (T) this.b.get(zwVar) : zwVar.a();
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        for (int i = 0; i < this.b.size(); i++) {
            a(this.b.c(i), this.b.e(i), messageDigest);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.lang.Object */
    /* JADX WARN: Multi-variable type inference failed */
    public static <T> void a(zw<T> zwVar, Object obj, MessageDigest messageDigest) {
        zwVar.a(obj, messageDigest);
    }
}
