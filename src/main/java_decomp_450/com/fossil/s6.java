package com.fossil;

import android.app.RemoteInput;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s6 {
    @DexIgnore
    public static RemoteInput[] a(s6[] s6VarArr) {
        if (s6VarArr == null) {
            return null;
        }
        RemoteInput[] remoteInputArr = new RemoteInput[s6VarArr.length];
        if (s6VarArr.length <= 0) {
            return remoteInputArr;
        }
        a(s6VarArr[0]);
        throw null;
    }

    @DexIgnore
    public String a() {
        throw null;
    }

    @DexIgnore
    public static RemoteInput a(s6 s6Var) {
        s6Var.a();
        throw null;
    }
}
