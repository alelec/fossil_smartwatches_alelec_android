package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yj3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Bundle a;
    @DexIgnore
    public /* final */ /* synthetic */ wj3 b;
    @DexIgnore
    public /* final */ /* synthetic */ wj3 c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ zj3 e;

    @DexIgnore
    public yj3(zj3 zj3, Bundle bundle, wj3 wj3, wj3 wj32, long j) {
        this.e = zj3;
        this.a = bundle;
        this.b = wj3;
        this.c = wj32;
        this.d = j;
    }

    @DexIgnore
    public final void run() {
        this.e.a(this.a, this.b, this.c, this.d);
    }
}
