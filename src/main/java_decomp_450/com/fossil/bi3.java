package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bi3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ em3 a;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 b;
    @DexIgnore
    public /* final */ /* synthetic */ ph3 c;

    @DexIgnore
    public bi3(ph3 ph3, em3 em3, nm3 nm3) {
        this.c = ph3;
        this.a = em3;
        this.b = nm3;
    }

    @DexIgnore
    public final void run() {
        this.c.a.s();
        if (this.a.zza() == null) {
            this.c.a.b(this.a, this.b);
        } else {
            this.c.a.a(this.a, this.b);
        }
    }
}
