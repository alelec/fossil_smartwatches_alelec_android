package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.br6;
import com.fossil.cy6;
import com.fossil.xq6;
import com.fossil.yq6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.ShineDevice;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq6 extends ho5 implements ur6, cy6.g, cy6.h {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public rr6 g;
    @DexIgnore
    public mr6 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            String simpleName = zq6.class.getSimpleName();
            ee7.a((Object) simpleName, "PairingFragment::class.java.simpleName");
            return simpleName;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final zq6 a(boolean z) {
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            zq6 zq6 = new zq6();
            zq6.setArguments(bundle);
            return zq6;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void A(String str) {
        ee7.b(str, "serial");
        TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
        Context context = getContext();
        if (context != null) {
            ee7.a((Object) context, "context!!");
            rr6 rr6 = this.g;
            if (rr6 != null) {
                aVar.a(context, str, true, rr6.j());
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void B() {
        if (isActive()) {
            cy6.f fVar = new cy6.f(2131558481);
            fVar.a(2131363342, ig5.a(PortfolioApp.g0.c(), 2131886744));
            fVar.a(2131363255, ig5.a(PortfolioApp.g0.c(), 2131886743));
            fVar.a(2131363307, ig5.a(PortfolioApp.g0.c(), 2131886746));
            fVar.a(2131363307);
            fVar.a(false);
            fVar.a(getChildFragmentManager(), "NETWORK_ERROR");
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void B0() {
        if (getActivity() != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context context = getContext();
            if (context != null) {
                ee7.a((Object) context, "context!!");
                String c = PortfolioApp.g0.c().c();
                rr6 rr6 = this.g;
                if (rr6 != null) {
                    aVar.a(context, c, true, rr6.j());
                } else {
                    ee7.d("mPairingPresenter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void F(boolean z) {
        mr6 mr6 = this.h;
        if (mr6 instanceof xq6) {
            ((xq6) mr6).T(z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0062, code lost:
        if (r6 != null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0064, code lost:
        r6.startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("market://details?id=com.google.android.wearable.app")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0073, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.zq6.j.a(), "GG play not found, open in browser");
        r6 = getActivity();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0089, code lost:
        if (r6 != null) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x008b, code lost:
        r6.startActivity(new android.content.Intent("android.intent.action.VIEW", android.net.Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.wearable.app")));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.zq6.j.a(), "WearOS app not found, open in GG play");
        r6 = getActivity();
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x004d */
    @Override // com.fossil.ur6
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void J(boolean r6) {
        /*
            r5 = this;
            java.lang.String r0 = "android.intent.action.VIEW"
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.fossil.zq6$a r2 = com.fossil.zq6.j
            java.lang.String r2 = r2.a()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "openWearOSApp(), isChineseUser="
            r3.append(r4)
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            if (r6 == 0) goto L_0x0033
            com.fossil.bx6 r6 = com.fossil.bx6.c
            androidx.fragment.app.FragmentManager r0 = r5.getChildFragmentManager()
            java.lang.String r1 = "childFragmentManager"
            com.fossil.ee7.a(r0, r1)
            r6.m(r0)
            return
        L_0x0033:
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0     // Catch:{ Exception -> 0x004d }
            com.portfolio.platform.PortfolioApp r6 = r6.c()     // Catch:{ Exception -> 0x004d }
            android.content.pm.PackageManager r6 = r6.getPackageManager()     // Catch:{ Exception -> 0x004d }
            java.lang.String r1 = "com.google.android.wearable.app"
            android.content.Intent r6 = r6.getLaunchIntentForPackage(r1)     // Catch:{ Exception -> 0x004d }
            androidx.fragment.app.FragmentActivity r1 = r5.getActivity()     // Catch:{ Exception -> 0x004d }
            if (r1 == 0) goto L_0x0099
            r1.startActivity(r6)     // Catch:{ Exception -> 0x004d }
            goto L_0x0099
        L_0x004d:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ ActivityNotFoundException -> 0x0073 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ ActivityNotFoundException -> 0x0073 }
            com.fossil.zq6$a r1 = com.fossil.zq6.j     // Catch:{ ActivityNotFoundException -> 0x0073 }
            java.lang.String r1 = r1.a()     // Catch:{ ActivityNotFoundException -> 0x0073 }
            java.lang.String r2 = "WearOS app not found, open in GG play"
            r6.d(r1, r2)     // Catch:{ ActivityNotFoundException -> 0x0073 }
            androidx.fragment.app.FragmentActivity r6 = r5.getActivity()     // Catch:{ ActivityNotFoundException -> 0x0073 }
            if (r6 == 0) goto L_0x0099
            android.content.Intent r1 = new android.content.Intent     // Catch:{ ActivityNotFoundException -> 0x0073 }
            java.lang.String r2 = "market://details?id=com.google.android.wearable.app"
            android.net.Uri r2 = android.net.Uri.parse(r2)     // Catch:{ ActivityNotFoundException -> 0x0073 }
            r1.<init>(r0, r2)     // Catch:{ ActivityNotFoundException -> 0x0073 }
            r6.startActivity(r1)     // Catch:{ ActivityNotFoundException -> 0x0073 }
            goto L_0x0099
        L_0x0073:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            com.fossil.zq6$a r1 = com.fossil.zq6.j
            java.lang.String r1 = r1.a()
            java.lang.String r2 = "GG play not found, open in browser"
            r6.d(r1, r2)
            androidx.fragment.app.FragmentActivity r6 = r5.getActivity()
            if (r6 == 0) goto L_0x0099
            android.content.Intent r1 = new android.content.Intent
            java.lang.String r2 = "https://play.google.com/store/apps/details?id=com.google.android.wearable.app"
            android.net.Uri r2 = android.net.Uri.parse(r2)
            r1.<init>(r0, r2)
            r6.startActivity(r1)
        L_0x0099:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zq6.J(boolean):void");
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void K() {
        mr6 mr6 = this.h;
        if (mr6 == null || !(mr6 instanceof br6)) {
            br6.a aVar = br6.j;
            rr6 rr6 = this.g;
            if (rr6 != null) {
                br6 a2 = aVar.a(rr6.j());
                this.h = a2;
                if (a2 != null) {
                    b(a2, "", 2131362149);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
        mr6 mr62 = this.h;
        if (mr62 != null) {
            rr6 rr62 = this.g;
            if (rr62 != null) {
                mr62.a(rr62);
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void U(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            String c = PortfolioApp.g0.c().c();
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.e(c, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void b(String str, boolean z) {
        ee7.b(str, "serial");
        mr6 mr6 = this.h;
        if (mr6 instanceof cr6) {
            ((cr6) mr6).t0();
            return;
        }
        cr6 a2 = cr6.w.a(str, z, 1);
        this.h = a2;
        if (a2 != null) {
            b(a2, "", 2131362149);
            mr6 mr62 = this.h;
            if (mr62 != null) {
                rr6 rr6 = this.g;
                if (rr6 != null) {
                    mr62.a(rr6);
                } else {
                    ee7.d("mPairingPresenter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void d(int i2, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = j.a();
        local.d(a2, "showDeviceConnectFail() - errorCode = " + i2);
        if (!isActive() || getActivity() == null) {
            return;
        }
        if (str != null) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = j.a();
            StringBuilder sb = new StringBuilder();
            sb.append("finish showDeviceConnectFail isOnboardingFlow ");
            rr6 rr6 = this.g;
            if (rr6 != null) {
                sb.append(rr6.j());
                local2.d(a3, sb.toString());
                v();
                TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    ee7.a((Object) activity, "activity!!");
                    rr6 rr62 = this.g;
                    if (rr62 != null) {
                        aVar.a(activity, str, true, rr62.j());
                    } else {
                        ee7.d("mPairingPresenter");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        } else {
            v();
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String a4 = j.a();
            StringBuilder sb2 = new StringBuilder();
            sb2.append("finish serial empty showDeviceConnectFail isOnboardingFlow ");
            rr6 rr63 = this.g;
            if (rr63 != null) {
                sb2.append(rr63.j());
                local3.d(a4, sb2.toString());
                TroubleshootingActivity.a aVar2 = TroubleshootingActivity.z;
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    ee7.a((Object) activity2, "activity!!");
                    rr6 rr64 = this.g;
                    if (rr64 != null) {
                        TroubleshootingActivity.a.a(aVar2, activity2, null, true, rr64.j(), 2, null);
                    } else {
                        ee7.d("mPairingPresenter");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void i() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) activity, "it");
            aVar.a(activity, 2);
        }
    }

    @DexIgnore
    @Override // com.fossil.cy6.h
    public void k(String str) {
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void l(List<r87<ShineDevice, String>> list) {
        ee7.b(list, "deviceList");
        mr6 mr6 = this.h;
        if (mr6 != null && (mr6 instanceof yq6)) {
            ((yq6) mr6).l(list);
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void o(List<String> list) {
        ee7.b(list, "instructions");
        mr6 mr6 = this.h;
        if (mr6 instanceof xq6) {
            ((xq6) mr6).x(list);
            return;
        }
        xq6.a aVar = xq6.y;
        rr6 rr6 = this.g;
        if (rr6 != null) {
            xq6 a2 = aVar.a(list, rr6.j());
            this.h = a2;
            if (a2 != null) {
                b(a2, "PairingAuthorizeFragment", 2131362149);
                mr6 mr62 = this.h;
                if (mr62 != null) {
                    rr6 rr62 = this.g;
                    if (rr62 != null) {
                        mr62.a(rr62);
                    } else {
                        ee7.d("mPairingPresenter");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        return layoutInflater.inflate(2131558600, viewGroup, false);
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        rr6 rr6 = this.g;
        if (rr6 != null) {
            rr6.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        rr6 rr6 = this.g;
        if (rr6 != null) {
            rr6.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        ee7.b(bundle, "outState");
        rr6 rr6 = this.g;
        if (rr6 != null) {
            if (rr6.i() != null) {
                rr6 rr62 = this.g;
                if (rr62 != null) {
                    ShineDevice i2 = rr62.i();
                    if (i2 != null) {
                        bundle.putParcelable("PAIRING_DEVICE", i2);
                    }
                } else {
                    ee7.d("mPairingPresenter");
                    throw null;
                }
            }
            super.onSaveInstanceState(bundle);
            return;
        }
        ee7.d("mPairingPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        if (bundle != null && bundle.containsKey("PAIRING_DEVICE")) {
            rr6 rr6 = this.g;
            if (rr6 != null) {
                Parcelable parcelable = bundle.getParcelable("PAIRING_DEVICE");
                if (parcelable != null) {
                    rr6.b((ShineDevice) parcelable);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
        Bundle arguments = getArguments();
        if (arguments != null) {
            rr6 rr62 = this.g;
            if (rr62 != null) {
                rr62.b(arguments.getBoolean("IS_ONBOARDING_FLOW"));
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
        mr6 mr6 = (mr6) getChildFragmentManager().b(2131362149);
        this.h = mr6;
        if (mr6 == null) {
            br6.a aVar = br6.j;
            rr6 rr63 = this.g;
            if (rr63 != null) {
                br6 a2 = aVar.a(rr63.j());
                this.h = a2;
                if (a2 != null) {
                    b(a2, "", 2131362149);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
        mr6 mr62 = this.h;
        if (mr62 != null) {
            rr6 rr64 = this.g;
            if (rr64 != null) {
                mr62.a(rr64);
                rr6 rr65 = this.g;
                if (rr65 != null) {
                    mr6 mr63 = this.h;
                    rr65.a((mr63 instanceof br6) || (mr63 instanceof yq6));
                    V("scan_device_view");
                    return;
                }
                ee7.d("mPairingPresenter");
                throw null;
            }
            ee7.d("mPairingPresenter");
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void q(List<r87<ShineDevice, String>> list) {
        ee7.b(list, "listDevices");
        mr6 mr6 = this.h;
        if (mr6 == null || !(mr6 instanceof yq6)) {
            yq6.a aVar = yq6.r;
            rr6 rr6 = this.g;
            if (rr6 != null) {
                yq6 a2 = aVar.a(rr6.j());
                a2.l(list);
                this.h = a2;
                if (a2 != null) {
                    b(a2, "", 2131362149);
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
        mr6 mr62 = this.h;
        if (mr62 != null) {
            rr6 rr62 = this.g;
            if (rr62 != null) {
                mr62.a(rr62);
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void u() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ExploreWatchActivity.a aVar = ExploreWatchActivity.z;
            ee7.a((Object) activity, "it");
            rr6 rr6 = this.g;
            if (rr6 != null) {
                aVar.a(activity, rr6.j());
            } else {
                ee7.d("mPairingPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void v() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, "activity!!");
            if (!activity.isFinishing()) {
                FragmentActivity activity2 = getActivity();
                if (activity2 != null) {
                    ee7.a((Object) activity2, "activity!!");
                    if (!activity2.isDestroyed()) {
                        FragmentActivity activity3 = getActivity();
                        if (activity3 != null) {
                            activity3.finish();
                        } else {
                            ee7.a();
                            throw null;
                        }
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void x(String str) {
        ee7.b(str, "serial");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            String c = PortfolioApp.g0.c().c();
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.f(c, childFragmentManager);
        }
    }

    @DexIgnore
    public void a(rr6 rr6) {
        ee7.b(rr6, "presenter");
        this.g = rr6;
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void a(String str, boolean z) {
        ee7.b(str, "serial");
        mr6 mr6 = this.h;
        if (mr6 instanceof cr6) {
            ((cr6) mr6).k0();
            return;
        }
        cr6 a2 = cr6.w.a(str, z, 0);
        this.h = a2;
        if (a2 != null) {
            b(a2, "", 2131362149);
            mr6 mr62 = this.h;
            if (mr62 != null) {
                rr6 rr6 = this.g;
                if (rr6 != null) {
                    mr62.a(rr6);
                } else {
                    ee7.d("mPairingPresenter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void q(String str) {
        ee7.b(str, "serial");
        mr6 mr6 = this.h;
        if (mr6 instanceof xq6) {
            ((xq6) mr6).Y(str);
            return;
        }
        xq6.a aVar = xq6.y;
        rr6 rr6 = this.g;
        if (rr6 != null) {
            xq6 a2 = aVar.a(str, rr6.j());
            this.h = a2;
            if (a2 != null) {
                b(a2, "PairingAuthorizeFragment", 2131362149);
                mr6 mr62 = this.h;
                if (mr62 != null) {
                    rr6 rr62 = this.g;
                    if (rr62 != null) {
                        mr62.a(rr62);
                    } else {
                        ee7.d("mPairingPresenter");
                        throw null;
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mPairingPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void a(String str, boolean z, boolean z2) {
        ee7.b(str, "serial");
        mr6 mr6 = this.h;
        if (mr6 instanceof cr6) {
            ((cr6) mr6).c(z2);
            return;
        }
        cr6 a2 = cr6.w.a(str, z, z2 ? 2 : 3);
        this.h = a2;
        if (a2 != null) {
            b(a2, "", 2131362149);
            mr6 mr62 = this.h;
            if (mr62 != null) {
                rr6 rr6 = this.g;
                if (rr6 != null) {
                    mr62.a(rr6);
                } else {
                    ee7.d("mPairingPresenter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x001c, code lost:
        if (r6.equals("SERVER_ERROR") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00f2, code lost:
        if (r6.equals("SERVER_MAINTENANCE") != false) goto L_0x00f4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00f4, code lost:
        v();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:?, code lost:
        return;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0104  */
    @Override // com.fossil.ho5, com.fossil.cy6.g
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r6, int r7, android.content.Intent r8) {
        /*
            r5 = this;
            java.lang.String r0 = "tag"
            com.fossil.ee7.b(r6, r0)
            int r0 = r6.hashCode()
            java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity"
            java.lang.String r2 = "mPairingPresenter"
            r3 = 2131363307(0x7f0a05eb, float:1.834642E38)
            r4 = 0
            switch(r0) {
                case -1636680713: goto L_0x00ec;
                case -879828873: goto L_0x00dd;
                case 39550276: goto L_0x00c7;
                case 603997695: goto L_0x0093;
                case 927511079: goto L_0x004c;
                case 1008390942: goto L_0x0020;
                case 1178575340: goto L_0x0016;
                default: goto L_0x0014;
            }
        L_0x0014:
            goto L_0x00f8
        L_0x0016:
            java.lang.String r0 = "SERVER_ERROR"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            goto L_0x00f4
        L_0x0020:
            java.lang.String r0 = "NO_INTERNET_CONNECTION"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            r6 = 2131362413(0x7f0a026d, float:1.8344606E38)
            if (r7 == r6) goto L_0x0036
            if (r7 == r3) goto L_0x0031
            goto L_0x0103
        L_0x0031:
            r5.v()
            goto L_0x0103
        L_0x0036:
            androidx.fragment.app.FragmentActivity r6 = r5.getActivity()
            if (r6 == 0) goto L_0x0046
            com.fossil.cl5 r6 = (com.fossil.cl5) r6
            r6.l()
            r5.v()
            goto L_0x0103
        L_0x0046:
            com.fossil.x87 r6 = new com.fossil.x87
            r6.<init>(r1)
            throw r6
        L_0x004c:
            java.lang.String r0 = "UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            r6 = 2131362268(0x7f0a01dc, float:1.8344312E38)
            if (r7 == r6) goto L_0x0086
            r6 = 2131362359(0x7f0a0237, float:1.8344496E38)
            if (r7 == r6) goto L_0x006a
            r6 = 2131362656(0x7f0a0360, float:1.8345099E38)
            if (r7 == r6) goto L_0x0065
            goto L_0x0103
        L_0x0065:
            r5.v()
            goto L_0x0103
        L_0x006a:
            androidx.fragment.app.FragmentActivity r6 = r5.getActivity()
            if (r6 == 0) goto L_0x0103
            com.portfolio.platform.uirenew.home.profile.help.HelpActivity$a r6 = com.portfolio.platform.uirenew.home.profile.help.HelpActivity.z
            androidx.fragment.app.FragmentActivity r7 = r5.getActivity()
            if (r7 == 0) goto L_0x0082
            java.lang.String r8 = "activity!!"
            com.fossil.ee7.a(r7, r8)
            r6.a(r7)
            goto L_0x0103
        L_0x0082:
            com.fossil.ee7.a()
            throw r4
        L_0x0086:
            com.fossil.rr6 r6 = r5.g
            if (r6 == 0) goto L_0x008f
            r6.n()
            goto L_0x0103
        L_0x008f:
            com.fossil.ee7.d(r2)
            throw r4
        L_0x0093:
            java.lang.String r0 = "SWITCH_DEVICE_WORKOUT"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            if (r8 == 0) goto L_0x00a4
            java.lang.String r6 = "SERIAL"
            java.lang.String r6 = r8.getStringExtra(r6)
            goto L_0x00a5
        L_0x00a4:
            r6 = r4
        L_0x00a5:
            if (r6 == 0) goto L_0x0103
            r8 = 2131363229(0x7f0a059d, float:1.834626E38)
            if (r7 == r8) goto L_0x00bb
            if (r7 == r3) goto L_0x00af
            goto L_0x0103
        L_0x00af:
            com.fossil.rr6 r7 = r5.g
            if (r7 == 0) goto L_0x00b7
            r7.a(r6)
            goto L_0x0103
        L_0x00b7:
            com.fossil.ee7.d(r2)
            throw r4
        L_0x00bb:
            com.fossil.rr6 r7 = r5.g
            if (r7 == 0) goto L_0x00c3
            r7.b(r6)
            goto L_0x0103
        L_0x00c3:
            com.fossil.ee7.d(r2)
            throw r4
        L_0x00c7:
            java.lang.String r0 = "SWITCH_DEVICE_SYNC_FAIL"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            if (r7 != r3) goto L_0x0103
            com.fossil.rr6 r6 = r5.g
            if (r6 == 0) goto L_0x00d9
            r6.m()
            goto L_0x0103
        L_0x00d9:
            com.fossil.ee7.d(r2)
            throw r4
        L_0x00dd:
            java.lang.String r0 = "NETWORK_ERROR"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
            if (r7 == r3) goto L_0x00e8
            goto L_0x0103
        L_0x00e8:
            r5.v()
            goto L_0x0103
        L_0x00ec:
            java.lang.String r0 = "SERVER_MAINTENANCE"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x00f8
        L_0x00f4:
            r5.v()
            goto L_0x0103
        L_0x00f8:
            androidx.fragment.app.FragmentActivity r0 = r5.getActivity()
            if (r0 == 0) goto L_0x0104
            com.fossil.cl5 r0 = (com.fossil.cl5) r0
            r0.a(r6, r7, r8)
        L_0x0103:
            return
        L_0x0104:
            com.fossil.x87 r6 = new com.fossil.x87
            r6.<init>(r1)
            throw r6
            switch-data {-1636680713->0x00ec, -879828873->0x00dd, 39550276->0x00c7, 603997695->0x0093, 927511079->0x004c, 1008390942->0x0020, 1178575340->0x0016, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.zq6.a(java.lang.String, int, android.content.Intent):void");
    }

    @DexIgnore
    @Override // com.fossil.ur6
    public void a(int i2, String str, String str2) {
        ee7.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = j.a();
        local.d(a2, "showDeviceConnectFail due to network - errorCode = " + i2);
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(childFragmentManager, i2, str2);
        }
    }
}
