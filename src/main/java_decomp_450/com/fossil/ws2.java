package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ws2<E> extends ps2<E> implements Set<E> {
    @DexIgnore
    @NullableDecl
    public transient os2<E> b;

    @DexIgnore
    public static int zza(int i) {
        int max = Math.max(i, 2);
        boolean z = true;
        if (max < 751619276) {
            int highestOneBit = Integer.highestOneBit(max - 1) << 1;
            while (((double) highestOneBit) * 0.7d < ((double) max)) {
                highestOneBit <<= 1;
            }
            return highestOneBit;
        }
        if (max >= 1073741824) {
            z = false;
        }
        or2.a(z, "collection too large");
        return 1073741824;
    }

    @DexIgnore
    public boolean equals(@NullableDecl Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ws2) || !zza() || !((ws2) obj).zza() || hashCode() == obj.hashCode()) {
            return ut2.a(this, obj);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return ut2.a(this);
    }

    @DexIgnore
    @Override // com.fossil.ps2, java.util.AbstractCollection, java.util.Collection, java.util.Set, java.lang.Iterable
    public /* synthetic */ Iterator iterator() {
        return iterator();
    }

    @DexIgnore
    public boolean zza() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public os2<E> zzc() {
        os2<E> os2 = this.b;
        if (os2 != null) {
            return os2;
        }
        os2<E> zzd = zzd();
        this.b = zzd;
        return zzd;
    }

    @DexIgnore
    public os2<E> zzd() {
        return os2.zza(toArray());
    }

    @DexIgnore
    public static <E> ws2<E> zza(Collection<? extends E> collection) {
        if ((collection instanceof ws2) && !(collection instanceof SortedSet)) {
            ws2<E> ws2 = (ws2) collection;
            if (!ws2.zzh()) {
                return ws2;
            }
        }
        Object[] array = collection.toArray();
        int length = array.length;
        while (length != 0) {
            if (length == 1) {
                return new tt2(array[0]);
            }
            int zza = zza(length);
            Object[] objArr = new Object[zza];
            int i = zza - 1;
            int i2 = 0;
            int i3 = 0;
            for (int i4 = 0; i4 < length; i4++) {
                Object obj = array[i4];
                kt2.a(obj, i4);
                int hashCode = obj.hashCode();
                int a = ms2.a(hashCode);
                while (true) {
                    int i5 = a & i;
                    Object obj2 = objArr[i5];
                    if (obj2 != null) {
                        if (obj2.equals(obj)) {
                            break;
                        }
                        a++;
                    } else {
                        array[i3] = obj;
                        objArr[i5] = obj;
                        i2 += hashCode;
                        i3++;
                        break;
                    }
                }
            }
            Arrays.fill(array, i3, length, (Object) null);
            if (i3 == 1) {
                return new tt2(array[0], i2);
            }
            if (zza(i3) < zza / 2) {
                length = i3;
            } else {
                int length2 = array.length;
                if (i3 < (length2 >> 1) + (length2 >> 2)) {
                    array = Arrays.copyOf(array, i3);
                }
                return new st2(array, i2, objArr, i, i3);
            }
        }
        return st2.zza;
    }
}
