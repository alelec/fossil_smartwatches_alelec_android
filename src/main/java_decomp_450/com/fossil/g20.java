package com.fossil;

import android.graphics.Bitmap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g20 implements cx<Bitmap, Bitmap> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements uy<Bitmap> {
        @DexIgnore
        public /* final */ Bitmap a;

        @DexIgnore
        public a(Bitmap bitmap) {
            this.a = bitmap;
        }

        @DexIgnore
        @Override // com.fossil.uy
        public void b() {
        }

        @DexIgnore
        @Override // com.fossil.uy
        public int c() {
            return v50.a(this.a);
        }

        @DexIgnore
        @Override // com.fossil.uy
        public Class<Bitmap> d() {
            return Bitmap.class;
        }

        @DexIgnore
        @Override // com.fossil.uy
        public Bitmap get() {
            return this.a;
        }
    }

    @DexIgnore
    public boolean a(Bitmap bitmap, ax axVar) {
        return true;
    }

    @DexIgnore
    public uy<Bitmap> a(Bitmap bitmap, int i, int i2, ax axVar) {
        return new a(bitmap);
    }
}
