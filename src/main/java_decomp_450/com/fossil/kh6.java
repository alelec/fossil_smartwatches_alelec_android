package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kh6 extends go5 implements uq5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public qw6<uy4> f;
    @DexIgnore
    public tq5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kh6.i;
        }

        @DexIgnore
        public final kh6 b() {
            return new kh6();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ kh6 a;

        @DexIgnore
        public b(kh6 kh6) {
            this.a = kh6;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            ee7.b(compoundButton, "button");
            if (compoundButton.isPressed()) {
                kh6.a(this.a).h();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ kh6 a;

        @DexIgnore
        public c(kh6 kh6) {
            this.a = kh6;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (this.a.getActivity() != null) {
                this.a.requireActivity().finish();
            }
        }
    }

    /*
    static {
        String simpleName = kh6.class.getSimpleName();
        if (simpleName != null) {
            ee7.a((Object) simpleName, "ConnectedAppsFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ tq5 a(kh6 kh6) {
        tq5 tq5 = kh6.g;
        if (tq5 != null) {
            return tq5;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return i;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    @Override // com.fossil.uq5
    public void l(int i2) {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, "", childFragmentManager);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1 && i3 == -1) {
            tq5 tq5 = this.g;
            if (tq5 != null) {
                tq5.i();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<uy4> qw6 = new qw6<>(this, (uy4) qb.a(layoutInflater, 2131558522, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            uy4 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        tq5 tq5 = this.g;
        if (tq5 != null) {
            tq5.g();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        tq5 tq5 = this.g;
        if (tq5 != null) {
            tq5.f();
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<uy4> qw6 = this.f;
        if (qw6 != null) {
            uy4 a2 = qw6.a();
            if (a2 != null) {
                a2.q.setOnCheckedChangeListener(new b(this));
                a2.w.setOnClickListener(new c(this));
            }
            V("connected_app_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.uq5
    public void w(boolean z) {
        if (isActive()) {
            qw6<uy4> qw6 = this.f;
            if (qw6 != null) {
                uy4 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleSwitchCompat flexibleSwitchCompat = a2.q;
                    ee7.a((Object) flexibleSwitchCompat, "it.cbGooglefit");
                    flexibleSwitchCompat.setChecked(z);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(tq5 tq5) {
        ee7.b(tq5, "presenter");
        this.g = tq5;
    }

    @DexIgnore
    @Override // com.fossil.uq5
    public void a(i02 i02) {
        ee7.b(i02, "connectionResult");
        i02.a(getActivity(), 1);
    }
}
