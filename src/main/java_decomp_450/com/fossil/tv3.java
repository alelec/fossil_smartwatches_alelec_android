package com.fossil;

import android.content.Context;
import com.google.android.material.internal.CheckableImageButton;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tv3 {
    @DexIgnore
    public TextInputLayout a;
    @DexIgnore
    public Context b;
    @DexIgnore
    public CheckableImageButton c;

    @DexIgnore
    public tv3(TextInputLayout textInputLayout) {
        this.a = textInputLayout;
        this.b = textInputLayout.getContext();
        this.c = textInputLayout.getEndIconView();
    }

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public boolean a(int i) {
        return true;
    }

    @DexIgnore
    public boolean b() {
        return false;
    }
}
