package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn3 implements Parcelable.Creator<rn3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ rn3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        b72 b72 = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 1) {
                i = j72.q(parcel, a);
            } else if (a2 != 2) {
                j72.v(parcel, a);
            } else {
                b72 = (b72) j72.a(parcel, a, b72.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new rn3(i, b72);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ rn3[] newArray(int i) {
        return new rn3[i];
    }
}
