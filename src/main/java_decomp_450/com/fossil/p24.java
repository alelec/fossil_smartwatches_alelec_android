package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class p24 implements Runnable {
    @DexIgnore
    public /* final */ Map.Entry a;
    @DexIgnore
    public /* final */ f94 b;

    @DexIgnore
    public p24(Map.Entry entry, f94 f94) {
        this.a = entry;
        this.b = f94;
    }

    @DexIgnore
    public static Runnable a(Map.Entry entry, f94 f94) {
        return new p24(entry, f94);
    }

    @DexIgnore
    public void run() {
        ((g94) this.a.getKey()).a(this.b);
    }
}
