package com.fossil;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wv2 extends nu2<Float> implements jw2<Float>, vx2, RandomAccess {
    @DexIgnore
    public float[] b;
    @DexIgnore
    public int c;

    /*
    static {
        new wv2(new float[0], 0).zzb();
    }
    */

    @DexIgnore
    public wv2() {
        this(new float[10], 0);
    }

    @DexIgnore
    public final void a(float f) {
        a();
        int i = this.c;
        float[] fArr = this.b;
        if (i == fArr.length) {
            float[] fArr2 = new float[(((i * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.b = fArr2;
        }
        float[] fArr3 = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        fArr3[i2] = f;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        float floatValue = ((Float) obj).floatValue();
        a();
        if (i < 0 || i > (i2 = this.c)) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
        float[] fArr = this.b;
        if (i2 < fArr.length) {
            System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
        } else {
            float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.b, i, fArr2, i + 1, this.c - i);
            this.b = fArr2;
        }
        this.b[i] = floatValue;
        this.c++;
        ((AbstractList) this).modCount++;
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, com.fossil.nu2
    public final boolean addAll(Collection<? extends Float> collection) {
        a();
        ew2.a(collection);
        if (!(collection instanceof wv2)) {
            return super.addAll(collection);
        }
        wv2 wv2 = (wv2) collection;
        int i = wv2.c;
        if (i == 0) {
            return false;
        }
        int i2 = this.c;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.b;
            if (i3 > fArr.length) {
                this.b = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(wv2.b, 0, this.b, this.c, wv2.c);
            this.c = i3;
            ((AbstractList) this).modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    @DexIgnore
    public final boolean contains(Object obj) {
        return indexOf(obj) != -1;
    }

    @DexIgnore
    @Override // com.fossil.nu2
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof wv2)) {
            return super.equals(obj);
        }
        wv2 wv2 = (wv2) obj;
        if (this.c != wv2.c) {
            return false;
        }
        float[] fArr = wv2.b;
        for (int i = 0; i < this.c; i++) {
            if (Float.floatToIntBits(this.b[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object get(int i) {
        zzb(i);
        return Float.valueOf(this.b[i]);
    }

    @DexIgnore
    @Override // com.fossil.nu2
    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.c; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.b[i2]);
        }
        return i;
    }

    @DexIgnore
    public final int indexOf(Object obj) {
        if (!(obj instanceof Float)) {
            return -1;
        }
        float floatValue = ((Float) obj).floatValue();
        int size = size();
        for (int i = 0; i < size; i++) {
            if (this.b[i] == floatValue) {
                return i;
            }
        }
        return -1;
    }

    @DexIgnore
    @Override // java.util.List, com.fossil.nu2
    public final boolean remove(Object obj) {
        a();
        for (int i = 0; i < this.c; i++) {
            if (obj.equals(Float.valueOf(this.b[i]))) {
                float[] fArr = this.b;
                System.arraycopy(fArr, i + 1, fArr, i, (this.c - i) - 1);
                this.c--;
                ((AbstractList) this).modCount++;
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final void removeRange(int i, int i2) {
        a();
        if (i2 >= i) {
            float[] fArr = this.b;
            System.arraycopy(fArr, i2, fArr, i, this.c - i2);
            this.c -= i2 - i;
            ((AbstractList) this).modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        a();
        zzb(i);
        float[] fArr = this.b;
        float f = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f);
    }

    @DexIgnore
    public final int size() {
        return this.c;
    }

    @DexIgnore
    /* Return type fixed from 'com.fossil.jw2' to match base method */
    @Override // com.fossil.jw2
    public final /* synthetic */ jw2<Float> zza(int i) {
        if (i >= this.c) {
            return new wv2(Arrays.copyOf(this.b, i), this.c);
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public final void zzb(int i) {
        if (i < 0 || i >= this.c) {
            throw new IndexOutOfBoundsException(zzc(i));
        }
    }

    @DexIgnore
    public final String zzc(int i) {
        int i2 = this.c;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    @DexIgnore
    public wv2(float[] fArr, int i) {
        this.b = fArr;
        this.c = i;
    }

    @DexIgnore
    @Override // java.util.List, java.util.AbstractList
    public final /* synthetic */ Object remove(int i) {
        a();
        zzb(i);
        float[] fArr = this.b;
        float f = fArr[i];
        int i2 = this.c;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.c--;
        ((AbstractList) this).modCount++;
        return Float.valueOf(f);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // java.util.AbstractCollection, java.util.List, java.util.Collection, java.util.AbstractList, com.fossil.nu2
    public final /* synthetic */ boolean add(Float f) {
        a(f.floatValue());
        return true;
    }
}
