package com.fossil;

import com.fossil.lf;
import com.fossil.of;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf<K, A, B> extends of<K, B> {
    @DexIgnore
    public /* final */ of<K, A> a;
    @DexIgnore
    public /* final */ t3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends of.c<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ of.c a;

        @DexIgnore
        public a(of.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        @Override // com.fossil.of.c
        public void a(List<A> list, K k, K k2) {
            this.a.a(lf.convert(yf.this.b, list), k, k2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends of.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ of.a a;

        @DexIgnore
        public b(of.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.of.a
        public void a(List<A> list, K k) {
            this.a.a(lf.convert(yf.this.b, list), k);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends of.a<K, A> {
        @DexIgnore
        public /* final */ /* synthetic */ of.a a;

        @DexIgnore
        public c(of.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        @Override // com.fossil.of.a
        public void a(List<A> list, K k) {
            this.a.a(lf.convert(yf.this.b, list), k);
        }
    }

    @DexIgnore
    public yf(of<K, A> ofVar, t3<List<A>, List<B>> t3Var) {
        this.a = ofVar;
        this.b = t3Var;
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void addInvalidatedCallback(lf.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadAfter(of.f<K> fVar, of.a<K, B> aVar) {
        this.a.loadAfter(fVar, new c(aVar));
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadBefore(of.f<K> fVar, of.a<K, B> aVar) {
        this.a.loadBefore(fVar, new b(aVar));
    }

    @DexIgnore
    @Override // com.fossil.of
    public void loadInitial(of.e<K> eVar, of.c<K, B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    @Override // com.fossil.lf
    public void removeInvalidatedCallback(lf.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
