package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ce7 extends vd7 implements be7, vf7 {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public ce7(int i) {
        this.arity = i;
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public sf7 computeReflected() {
        te7.a(this);
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ce7) {
            ce7 ce7 = (ce7) obj;
            if (getOwner() != null ? getOwner().equals(ce7.getOwner()) : ce7.getOwner() == null) {
                if (!getName().equals(ce7.getName()) || !getSignature().equals(ce7.getSignature()) || !ee7.a(getBoundReceiver(), ce7.getBoundReceiver())) {
                    return false;
                }
                return true;
            }
            return false;
        } else if (obj instanceof vf7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.be7
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner() == null ? 0 : getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.vf7
    public boolean isExternal() {
        return getReflected().isExternal();
    }

    @DexIgnore
    @Override // com.fossil.vf7
    public boolean isInfix() {
        return getReflected().isInfix();
    }

    @DexIgnore
    @Override // com.fossil.vf7
    public boolean isInline() {
        return getReflected().isInline();
    }

    @DexIgnore
    @Override // com.fossil.vf7
    public boolean isOperator() {
        return getReflected().isOperator();
    }

    @DexIgnore
    @Override // com.fossil.sf7, com.fossil.vf7, com.fossil.vd7
    public boolean isSuspend() {
        return getReflected().isSuspend();
    }

    @DexIgnore
    public String toString() {
        sf7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        if ("<init>".equals(getName())) {
            return "constructor (Kotlin reflection is not available)";
        }
        return "function " + getName() + " (Kotlin reflection is not available)";
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public vf7 getReflected() {
        return (vf7) super.getReflected();
    }

    @DexIgnore
    public ce7(int i, Object obj) {
        super(obj);
        this.arity = i;
    }
}
