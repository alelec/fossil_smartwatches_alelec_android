package com.fossil;

import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.LinearGradient;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import com.facebook.places.internal.LocationScannerImpl;
import java.io.IOException;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a7 {
    @DexIgnore
    public static Shader a(Resources resources, XmlPullParser xmlPullParser, AttributeSet attributeSet, Resources.Theme theme) throws IOException, XmlPullParserException {
        String name = xmlPullParser.getName();
        if (name.equals("gradient")) {
            TypedArray a2 = d7.a(resources, theme, attributeSet, f6.GradientColor);
            float a3 = d7.a(a2, xmlPullParser, "startX", f6.GradientColor_android_startX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a4 = d7.a(a2, xmlPullParser, "startY", f6.GradientColor_android_startY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a5 = d7.a(a2, xmlPullParser, "endX", f6.GradientColor_android_endX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a6 = d7.a(a2, xmlPullParser, "endY", f6.GradientColor_android_endY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a7 = d7.a(a2, xmlPullParser, "centerX", f6.GradientColor_android_centerX, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float a8 = d7.a(a2, xmlPullParser, "centerY", f6.GradientColor_android_centerY, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            int b = d7.b(a2, xmlPullParser, "type", f6.GradientColor_android_type, 0);
            int a9 = d7.a(a2, xmlPullParser, "startColor", f6.GradientColor_android_startColor, 0);
            boolean a10 = d7.a(xmlPullParser, "centerColor");
            int a11 = d7.a(a2, xmlPullParser, "centerColor", f6.GradientColor_android_centerColor, 0);
            int a12 = d7.a(a2, xmlPullParser, "endColor", f6.GradientColor_android_endColor, 0);
            int b2 = d7.b(a2, xmlPullParser, "tileMode", f6.GradientColor_android_tileMode, 0);
            float a13 = d7.a(a2, xmlPullParser, "gradientRadius", f6.GradientColor_android_gradientRadius, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            a2.recycle();
            a a14 = a(b(resources, xmlPullParser, attributeSet, theme), a9, a12, a10, a11);
            if (b != 1) {
                if (b != 2) {
                    return new LinearGradient(a3, a4, a5, a6, a14.a, a14.b, a(b2));
                }
                return new SweepGradient(a7, a8, a14.a, a14.b);
            } else if (a13 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                return new RadialGradient(a7, a8, a13, a14.a, a14.b, a(b2));
            } else {
                throw new XmlPullParserException("<gradient> tag requires 'gradientRadius' attribute with radial type");
            }
        } else {
            throw new XmlPullParserException(xmlPullParser.getPositionDescription() + ": invalid gradient color tag " + name);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0084, code lost:
        throw new org.xmlpull.v1.XmlPullParserException(r9.getPositionDescription() + ": <item> tag requires a 'color' attribute and a 'offset' attribute!");
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.a7.a b(android.content.res.Resources r8, org.xmlpull.v1.XmlPullParser r9, android.util.AttributeSet r10, android.content.res.Resources.Theme r11) throws org.xmlpull.v1.XmlPullParserException, java.io.IOException {
        /*
            int r0 = r9.getDepth()
            r1 = 1
            int r0 = r0 + r1
            java.util.ArrayList r2 = new java.util.ArrayList
            r3 = 20
            r2.<init>(r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>(r3)
        L_0x0012:
            int r3 = r9.next()
            if (r3 == r1) goto L_0x0085
            int r5 = r9.getDepth()
            if (r5 >= r0) goto L_0x0021
            r6 = 3
            if (r3 == r6) goto L_0x0085
        L_0x0021:
            r6 = 2
            if (r3 == r6) goto L_0x0025
            goto L_0x0012
        L_0x0025:
            if (r5 > r0) goto L_0x0012
            java.lang.String r3 = r9.getName()
            java.lang.String r5 = "item"
            boolean r3 = r3.equals(r5)
            if (r3 != 0) goto L_0x0034
            goto L_0x0012
        L_0x0034:
            int[] r3 = com.fossil.f6.GradientColorItem
            android.content.res.TypedArray r3 = com.fossil.d7.a(r8, r11, r10, r3)
            int r5 = com.fossil.f6.GradientColorItem_android_color
            boolean r5 = r3.hasValue(r5)
            int r6 = com.fossil.f6.GradientColorItem_android_offset
            boolean r6 = r3.hasValue(r6)
            if (r5 == 0) goto L_0x006a
            if (r6 == 0) goto L_0x006a
            int r5 = com.fossil.f6.GradientColorItem_android_color
            r6 = 0
            int r5 = r3.getColor(r5, r6)
            int r6 = com.fossil.f6.GradientColorItem_android_offset
            r7 = 0
            float r6 = r3.getFloat(r6, r7)
            r3.recycle()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)
            r4.add(r3)
            java.lang.Float r3 = java.lang.Float.valueOf(r6)
            r2.add(r3)
            goto L_0x0012
        L_0x006a:
            org.xmlpull.v1.XmlPullParserException r8 = new org.xmlpull.v1.XmlPullParserException
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r9 = r9.getPositionDescription()
            r10.append(r9)
            java.lang.String r9 = ": <item> tag requires a 'color' attribute and a 'offset' attribute!"
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            r8.<init>(r9)
            throw r8
        L_0x0085:
            int r8 = r4.size()
            if (r8 <= 0) goto L_0x0091
            com.fossil.a7$a r8 = new com.fossil.a7$a
            r8.<init>(r4, r2)
            return r8
        L_0x0091:
            r8 = 0
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.a7.b(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):com.fossil.a7$a");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ float[] b;

        @DexIgnore
        public a(List<Integer> list, List<Float> list2) {
            int size = list.size();
            this.a = new int[size];
            this.b = new float[size];
            for (int i = 0; i < size; i++) {
                this.a[i] = list.get(i).intValue();
                this.b[i] = list2.get(i).floatValue();
            }
        }

        @DexIgnore
        public a(int i, int i2) {
            this.a = new int[]{i, i2};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f};
        }

        @DexIgnore
        public a(int i, int i2, int i3) {
            this.a = new int[]{i, i2, i3};
            this.b = new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f};
        }
    }

    @DexIgnore
    public static a a(a aVar, int i, int i2, boolean z, int i3) {
        if (aVar != null) {
            return aVar;
        }
        if (z) {
            return new a(i, i3, i2);
        }
        return new a(i, i2);
    }

    @DexIgnore
    public static Shader.TileMode a(int i) {
        if (i == 1) {
            return Shader.TileMode.REPEAT;
        }
        if (i != 2) {
            return Shader.TileMode.CLAMP;
        }
        return Shader.TileMode.MIRROR;
    }
}
