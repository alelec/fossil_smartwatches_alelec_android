package com.fossil;

import android.database.Cursor;
import com.fossil.uf;
import com.fossil.zh;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class mi<T> extends uf<T> {
    @DexIgnore
    public /* final */ String mCountQuery;
    @DexIgnore
    public /* final */ ci mDb;
    @DexIgnore
    public /* final */ boolean mInTransaction;
    @DexIgnore
    public /* final */ String mLimitOffsetQuery;
    @DexIgnore
    public /* final */ zh.c mObserver;
    @DexIgnore
    public /* final */ fi mSourceQuery;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends zh.c {
        @DexIgnore
        public a(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        @Override // com.fossil.zh.c
        public void onInvalidated(Set<String> set) {
            mi.this.invalidate();
        }
    }

    @DexIgnore
    public mi(ci ciVar, zi ziVar, boolean z, String... strArr) {
        this(ciVar, fi.a(ziVar), z, strArr);
    }

    @DexIgnore
    private fi getSQLiteQuery(int i, int i2) {
        fi b = fi.b(this.mLimitOffsetQuery, this.mSourceQuery.a() + 2);
        b.a(this.mSourceQuery);
        b.bindLong(b.a() - 1, (long) i2);
        b.bindLong(b.a(), (long) i);
        return b;
    }

    @DexIgnore
    public abstract List<T> convertRows(Cursor cursor);

    @DexIgnore
    public int countItems() {
        fi b = fi.b(this.mCountQuery, this.mSourceQuery.a());
        b.a(this.mSourceQuery);
        Cursor query = this.mDb.query(b);
        try {
            if (query.moveToFirst()) {
                return query.getInt(0);
            }
            query.close();
            b.c();
            return 0;
        } finally {
            query.close();
            b.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.lf
    public boolean isInvalid() {
        this.mDb.getInvalidationTracker().c();
        return super.isInvalid();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0055  */
    @Override // com.fossil.uf
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadInitial(com.fossil.uf.d r7, com.fossil.uf.b<T> r8) {
        /*
            r6 = this;
            java.util.List r0 = java.util.Collections.emptyList()
            com.fossil.ci r1 = r6.mDb
            r1.beginTransaction()
            r1 = 0
            int r2 = r6.countItems()     // Catch:{ all -> 0x0047 }
            if (r2 == 0) goto L_0x0032
            int r0 = com.fossil.uf.computeInitialLoadPosition(r7, r2)     // Catch:{ all -> 0x0047 }
            int r7 = com.fossil.uf.computeInitialLoadSize(r7, r0, r2)     // Catch:{ all -> 0x0047 }
            com.fossil.fi r7 = r6.getSQLiteQuery(r0, r7)     // Catch:{ all -> 0x0047 }
            com.fossil.ci r3 = r6.mDb     // Catch:{ all -> 0x0030 }
            android.database.Cursor r1 = r3.query(r7)     // Catch:{ all -> 0x0030 }
            java.util.List r3 = r6.convertRows(r1)     // Catch:{ all -> 0x0030 }
            com.fossil.ci r4 = r6.mDb     // Catch:{ all -> 0x0030 }
            r4.setTransactionSuccessful()     // Catch:{ all -> 0x0030 }
            r5 = r3
            r3 = r7
            r7 = r0
            r0 = r5
            goto L_0x0034
        L_0x0030:
            r8 = move-exception
            goto L_0x0049
        L_0x0032:
            r7 = 0
            r3 = r1
        L_0x0034:
            if (r1 == 0) goto L_0x0039
            r1.close()
        L_0x0039:
            com.fossil.ci r1 = r6.mDb
            r1.endTransaction()
            if (r3 == 0) goto L_0x0043
            r3.c()
        L_0x0043:
            r8.a(r0, r7, r2)
            return
        L_0x0047:
            r8 = move-exception
            r7 = r1
        L_0x0049:
            if (r1 == 0) goto L_0x004e
            r1.close()
        L_0x004e:
            com.fossil.ci r0 = r6.mDb
            r0.endTransaction()
            if (r7 == 0) goto L_0x0058
            r7.c()
        L_0x0058:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.mi.loadInitial(com.fossil.uf$d, com.fossil.uf$b):void");
    }

    @DexIgnore
    @Override // com.fossil.uf
    public void loadRange(uf.g gVar, uf.e<T> eVar) {
        eVar.a(loadRange(gVar.a, gVar.b));
    }

    @DexIgnore
    public mi(ci ciVar, fi fiVar, boolean z, String... strArr) {
        this.mDb = ciVar;
        this.mSourceQuery = fiVar;
        this.mInTransaction = z;
        this.mCountQuery = "SELECT COUNT(*) FROM ( " + this.mSourceQuery.b() + " )";
        this.mLimitOffsetQuery = "SELECT * FROM ( " + this.mSourceQuery.b() + " ) LIMIT ? OFFSET ?";
        this.mObserver = new a(strArr);
        ciVar.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    public List<T> loadRange(int i, int i2) {
        fi sQLiteQuery = getSQLiteQuery(i, i2);
        if (this.mInTransaction) {
            this.mDb.beginTransaction();
            Cursor cursor = null;
            try {
                cursor = this.mDb.query(sQLiteQuery);
                List<T> convertRows = convertRows(cursor);
                this.mDb.setTransactionSuccessful();
                return convertRows;
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                this.mDb.endTransaction();
                sQLiteQuery.c();
            }
        } else {
            Cursor query = this.mDb.query(sQLiteQuery);
            try {
                return convertRows(query);
            } finally {
                query.close();
                sQLiteQuery.c();
            }
        }
    }
}
