package com.fossil;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ r43 a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ AppMeasurementDynamiteService d;

    @DexIgnore
    public lm3(AppMeasurementDynamiteService appMeasurementDynamiteService, r43 r43, String str, String str2) {
        this.d = appMeasurementDynamiteService;
        this.a = r43;
        this.b = str;
        this.c = str2;
    }

    @DexIgnore
    public final void run() {
        this.d.a.E().a(this.a, this.b, this.c);
    }
}
