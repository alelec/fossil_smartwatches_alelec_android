package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import org.joda.time.DateTimeFieldType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum j90 {
    SUNDAY((byte) 1, "Sun"),
    MONDAY((byte) 2, "Mon"),
    TUESDAY((byte) 4, "Tue"),
    WEDNESDAY((byte) 8, "Wed"),
    THURSDAY(DateTimeFieldType.CLOCKHOUR_OF_DAY, "Thu"),
    FRIDAY((byte) 32, "Fri"),
    SATURDAY((byte) 64, "Sat");
    
    @DexIgnore
    public static /* final */ a d; // = new a(null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public j90(byte b2, String str) {
        this.a = b2;
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final byte b() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        public final Set<j90> a(byte b) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            j90[] values = j90.values();
            for (j90 j90 : values) {
                if (((byte) (j90.b() & b)) == j90.b()) {
                    linkedHashSet.add(j90);
                }
            }
            return linkedHashSet;
        }

        @DexIgnore
        public final j90[] a(String[] strArr) {
            j90 j90;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                j90[] values = j90.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        j90 = null;
                        break;
                    }
                    j90 = values[i];
                    if (ee7.a(yz0.a(j90), str) || ee7.a(j90.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (j90 != null) {
                    arrayList.add(j90);
                }
            }
            Object[] array = arrayList.toArray(new j90[0]);
            if (array != null) {
                return (j90[]) array;
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
