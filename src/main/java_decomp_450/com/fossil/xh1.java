package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xh1 extends ey0 {
    @DexIgnore
    public h41 L; // = new h41(0, 0, 0);
    @DexIgnore
    public long M; // = ButtonService.CONNECT_TIMEOUT;
    @DexIgnore
    public x91 N;

    @DexIgnore
    public xh1(x91 x91, ri1 ri1) {
        super(uh0.h, qa1.s, ri1, 0, 8);
        this.N = x91;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public void a(long j) {
        this.M = j;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public long e() {
        return this.M;
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject g() {
        return yz0.a(super.g(), this.N.a());
    }

    @DexIgnore
    @Override // com.fossil.v81
    public JSONObject h() {
        return yz0.a(super.h(), this.L.a());
    }

    @DexIgnore
    @Override // com.fossil.uh1, com.fossil.ey0
    public byte[] m() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.N.a).putShort((short) this.N.b).putShort((short) this.N.c).putShort((short) this.N.d).array();
        ee7.a((Object) array, "ByteBuffer.allocate(8).o\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final h41 r() {
        return this.L;
    }

    @DexIgnore
    @Override // com.fossil.uh1
    public JSONObject a(byte[] bArr) {
        ((uh1) this).E = true;
        JSONObject jSONObject = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        h41 h41 = new h41(yz0.b(order.getShort(0)), yz0.b(order.getShort(2)), yz0.b(order.getShort(4)));
        this.L = h41;
        return yz0.a(jSONObject, h41.a());
    }
}
