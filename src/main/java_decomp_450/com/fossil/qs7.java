package com.fossil;

import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qs7 {
    @DexIgnore
    public static /* final */ char a; // = File.separatorChar;

    /*
    static {
        Character.toString('.');
        a();
    }
    */

    @DexIgnore
    public static boolean a() {
        return a == '\\';
    }

    @DexIgnore
    public static int b(String str) {
        if (str == null) {
            return -1;
        }
        return Math.max(str.lastIndexOf(47), str.lastIndexOf(92));
    }

    @DexIgnore
    public static String a(String str) {
        if (str == null) {
            return null;
        }
        return str.substring(b(str) + 1);
    }
}
