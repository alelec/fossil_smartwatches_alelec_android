package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class yk0 extends Enum<yk0> {
    @DexIgnore
    public static /* final */ yk0 a;
    @DexIgnore
    public static /* final */ /* synthetic */ yk0[] b;

    /*
    static {
        yk0 yk0 = new yk0("NOT_AUTHENTICATED", 0);
        a = yk0;
        b = new yk0[]{yk0, new yk0("AUTHENTICATING", 1), new yk0("AUTHENTICATED", 2)};
    }
    */

    @DexIgnore
    public yk0(String str, int i) {
    }

    @DexIgnore
    public static yk0 valueOf(String str) {
        return (yk0) Enum.valueOf(yk0.class, str);
    }

    @DexIgnore
    public static yk0[] values() {
        return (yk0[]) b.clone();
    }
}
