package com.fossil;

import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class md5 implements yw {
    @DexIgnore
    public /* final */ List<BaseFeatureModel> b;

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.List<? extends com.fossil.wearables.fsl.shared.BaseFeatureModel> */
    /* JADX WARN: Multi-variable type inference failed */
    public md5(List<? extends BaseFeatureModel> list) {
        this.b = list;
    }

    @DexIgnore
    public final List<BaseFeatureModel> a() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.yw
    public void a(MessageDigest messageDigest) {
        ee7.b(messageDigest, "messageDigest");
        List<BaseFeatureModel> list = this.b;
        StringBuilder sb = new StringBuilder();
        if (list != null) {
            for (BaseFeatureModel baseFeatureModel : list) {
                if (baseFeatureModel instanceof AppFilter) {
                    sb.append(((AppFilter) baseFeatureModel).getType());
                    ee7.a((Object) sb, "id.append(item.type)");
                } else if (baseFeatureModel != null) {
                    List<Contact> contacts = ((ContactGroup) baseFeatureModel).getContacts();
                    if (contacts != null && (!contacts.isEmpty())) {
                        Contact contact = contacts.get(0);
                        ee7.a((Object) contact, "contactList[0]");
                        sb.append(contact.getFirstName());
                        Contact contact2 = contacts.get(0);
                        ee7.a((Object) contact2, "contactList[0]");
                        sb.append(contact2.getLastName());
                        Contact contact3 = contacts.get(0);
                        ee7.a((Object) contact3, "contactList[0]");
                        sb.append(contact3.getPhotoThumbUri());
                    }
                } else {
                    throw new x87("null cannot be cast to non-null type com.fossil.wearables.fsl.contact.ContactGroup");
                }
            }
        }
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "id.toString()");
        Charset charset = yw.a;
        ee7.a((Object) charset, "Key.CHARSET");
        if (sb2 != null) {
            byte[] bytes = sb2.getBytes(charset);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            messageDigest.update(bytes);
            return;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }
}
