package com.fossil;

import android.location.Location;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.facebook.places.model.PlaceFields;
import com.facebook.share.internal.ShareConstants;
import com.facebook.share.internal.VideoUploader;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.diana.commutetime.Address;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Arrays;
import java.util.Iterator;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk5 {
    @DexIgnore
    public static tk5 q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public ch5 e;
    @DexIgnore
    public DianaPresetRepository f;
    @DexIgnore
    public /* final */ Gson g;
    @DexIgnore
    public String h;
    @DexIgnore
    public TrafficResponse i;
    @DexIgnore
    public Address j;
    @DexIgnore
    public AddressWrapper k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public AddressWrapper m;
    @DexIgnore
    public b n;
    @DexIgnore
    public b o;
    @DexIgnore
    public boolean p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final synchronized tk5 a() {
            tk5 h;
            if (tk5.q == null) {
                tk5.q = new tk5(null);
            }
            h = tk5.q;
            if (h == null) {
                ee7.a();
                throw null;
            }
            return h;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements LocationSource.LocationListener {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$Listener$onLocationResult$1", f = "WatchAppCommuteTimeManager.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Location $location;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Location location, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$location = location;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$location, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    tk5.this.a(this.$location.getLatitude(), this.$location.getLongitude());
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.data.LocationSource.LocationListener
        public void onLocationResult(Location location) {
            ee7.b(location, PlaceFields.LOCATION);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchAppCommuteTimeManager", "onLocationResult lastLocation=" + location);
            ik7 unused = xh7.b(zi7.a(qj7.a()), qj7.c(), null, new a(this, location, null), 2, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1", f = "WatchAppCommuteTimeManager.kt", l = {115}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $destinationAlias;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ tk5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager$getCommuteTimeForWatchApp$1$1", f = "WatchAppCommuteTimeManager.kt", l = {142}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                T t;
                Object a = nb7.a();
                int i = this.label;
                String str = null;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    DianaPresetRepository b = this.this$0.this$0.b();
                    String c = this.this$0.this$0.h;
                    if (c != null) {
                        DianaPreset activePresetBySerial = b.getActivePresetBySerial(c);
                        if (activePresetBySerial == null) {
                            return null;
                        }
                        Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it.next();
                            if (pb7.a(ee7.a((Object) t.getId(), (Object) "commute-time")).booleanValue()) {
                                break;
                            }
                        }
                        T t2 = t;
                        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp activePreset " + activePresetBySerial);
                        if (t2 == null) {
                            return null;
                        }
                        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp json=" + t2.getSettings());
                        if (!sc5.a(t2.getSettings())) {
                            CommuteTimeWatchAppSetting commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) this.this$0.this$0.g.a(t2.getSettings(), CommuteTimeWatchAppSetting.class);
                            AddressWrapper addressByName = commuteTimeWatchAppSetting.getAddressByName(this.this$0.$destinationAlias);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("getCommuteTimeForWatchApp destinationAlias ");
                            sb.append(this.this$0.$destinationAlias);
                            sb.append(" destinationAddress ");
                            sb.append(addressByName != null ? addressByName.getAddress() : null);
                            local.d("WatchAppCommuteTimeManager", sb.toString());
                            JSONObject jSONObject = new JSONObject();
                            String id = addressByName != null ? addressByName.getId() : null;
                            AddressWrapper a2 = this.this$0.this$0.m;
                            if (ee7.a((Object) id, (Object) (a2 != null ? a2.getId() : null))) {
                                AddressWrapper a3 = this.this$0.this$0.m;
                                jSONObject.put("des", a3 != null ? a3.getId() : null);
                                jSONObject.put("type", "refresh");
                            } else {
                                AddressWrapper a4 = this.this$0.this$0.m;
                                jSONObject.put("des", a4 != null ? a4.getId() : null);
                                jSONObject.put("type", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                                FLogger.INSTANCE.getRemote().startSession(FLogger.Session.DIANA_COMMUTE_TIME, this.this$0.$serial, "WatchAppCommuteTimeManager");
                            }
                            this.this$0.this$0.m = addressByName;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("getDurationTimeBaseOnLocation currentDestinationAddress ");
                            AddressWrapper a5 = this.this$0.this$0.m;
                            if (a5 != null) {
                                str = a5.getAddress();
                            }
                            sb2.append(str);
                            local2.d("WatchAppCommuteTimeManager", sb2.toString());
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                            String str2 = this.this$0.$serial;
                            String jSONObject2 = jSONObject.toString();
                            ee7.a((Object) jSONObject2, "jsonObject.toString()");
                            remote.i(component, session, str2, "WatchAppCommuteTimeManager", jSONObject2);
                            tk5 tk5 = this.this$0.this$0;
                            this.L$0 = yi7;
                            this.L$1 = activePresetBySerial;
                            this.L$2 = activePresetBySerial;
                            this.L$3 = t2;
                            this.L$4 = commuteTimeWatchAppSetting;
                            this.L$5 = addressByName;
                            this.L$6 = jSONObject;
                            this.L$7 = t2;
                            this.label = 1;
                            if (tk5.a(this) == a) {
                                return a;
                            }
                        }
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else if (i == 1) {
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) this.L$7;
                    JSONObject jSONObject3 = (JSONObject) this.L$6;
                    AddressWrapper addressWrapper = (AddressWrapper) this.L$5;
                    CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.L$4;
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$3;
                    DianaPreset dianaPreset = (DianaPreset) this.L$2;
                    DianaPreset dianaPreset2 = (DianaPreset) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(tk5 tk5, String str, String str2, fb7 fb7) {
            super(2, fb7);
            this.this$0 = tk5;
            this.$destinationAlias = str;
            this.$serial = str2;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, this.$destinationAlias, this.$serial, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (kl7.a(DeviceAppResponse.LIFE_TIME, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zb7 implements gd7<fb7<? super fv7<TrafficResponse>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ fb7 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $location$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ TrafficRequest $trafficRequest;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ tk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(TrafficRequest trafficRequest, fb7 fb7, tk5 tk5, Location location, fb7 fb72) {
            super(1, fb7);
            this.$trafficRequest = trafficRequest;
            this.this$0 = tk5;
            this.$location$inlined = location;
            this.$continuation$inlined = fb72;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new d(this.$trafficRequest, fb7, this.this$0, this.$location$inlined, this.$continuation$inlined);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<TrafficResponse>> fb7) {
            return ((d) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                ApiServiceV2 a2 = this.this$0.a();
                TrafficRequest trafficRequest = this.$trafficRequest;
                this.label = 1;
                obj = a2.getTrafficStatus(trafficRequest, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {201}, m = "getDurationTime")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(tk5 tk5, fb7 fb7) {
            super(fb7);
            this.this$0 = tk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Location) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager", f = "WatchAppCommuteTimeManager.kt", l = {154, 174}, m = "getDurationTimeBaseOnLocation")
    public static final class f extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ tk5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(tk5 tk5, fb7 fb7) {
            super(fb7);
            this.this$0 = tk5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore
    public tk5() {
        this.g = new Gson();
        this.n = new b();
        this.o = new b();
        PortfolioApp.g0.c().f().a(this);
    }

    @DexIgnore
    public final void e() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.n, true, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                ee7.d("mPortfolioApp");
                throw null;
            }
        } else {
            ee7.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void f() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.o, false);
        } else {
            ee7.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final void g() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "stopSignificantLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            locationSource.unObserverLocation(this.n, true);
        } else {
            ee7.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final DianaPresetRepository b() {
        DianaPresetRepository dianaPresetRepository = this.f;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        ee7.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final void c() {
        String str;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse");
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper != null) {
            try {
                this.p = true;
                int i2 = uk5.a[addressWrapper.getType().ordinal()];
                if (i2 == 1) {
                    PortfolioApp portfolioApp = this.a;
                    if (portfolioApp != null) {
                        str = ig5.a(portfolioApp, 2131886347);
                    } else {
                        ee7.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 2) {
                    PortfolioApp portfolioApp2 = this.a;
                    if (portfolioApp2 != null) {
                        str = ig5.a(portfolioApp2, 2131886345);
                    } else {
                        ee7.d("mPortfolioApp");
                        throw null;
                    }
                } else if (i2 == 3) {
                    we7 we7 = we7.a;
                    PortfolioApp portfolioApp3 = this.a;
                    if (portfolioApp3 != null) {
                        String a2 = ig5.a(portfolioApp3, 2131886344);
                        ee7.a((Object) a2, "LanguageHelper.getString\u2026Popup___ArrivedToAddress)");
                        str = String.format(a2, Arrays.copyOf(new Object[]{addressWrapper.getName()}, 1));
                        ee7.a((Object) str, "java.lang.String.format(format, *args)");
                    } else {
                        ee7.d("mPortfolioApp");
                        throw null;
                    }
                } else {
                    throw new p87();
                }
                ee7.a((Object) str, "message");
                CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(str, uf0.END);
                PortfolioApp c2 = PortfolioApp.g0.c();
                String str2 = this.h;
                if (str2 != null) {
                    c2.a(commuteTimeWatchAppMessage, str2);
                    JSONObject jSONObject = new JSONObject();
                    AddressWrapper addressWrapper2 = this.m;
                    jSONObject.put("des", addressWrapper2 != null ? addressWrapper2.getId() : null);
                    jSONObject.put("type", "end");
                    jSONObject.put("end_type", "arrived");
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
                    String str3 = this.h;
                    if (str3 != null) {
                        String jSONObject2 = jSONObject.toString();
                        ee7.a((Object) jSONObject2, "jsonObject.toString()");
                        remote.i(component, session, str3, "WatchAppCommuteTimeManager", jSONObject2);
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                ee7.a();
                throw null;
            } catch (IllegalArgumentException e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WatchAppCommuteTimeManager", "sendArriveToDestinationResponse exception exception=" + e2.getMessage());
            }
        }
    }

    @DexIgnore
    public final void d() {
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "startMonitoringLocationObserver");
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            PortfolioApp portfolioApp = this.a;
            if (portfolioApp != null) {
                LocationSource.observerLocation$default(locationSource, portfolioApp, this.o, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 8, null);
            } else {
                ee7.d("mPortfolioApp");
                throw null;
            }
        } else {
            ee7.d("mLocationSource");
            throw null;
        }
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        ee7.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        ee7.b(str, "serial");
        ee7.b(str2, ShareConstants.DESTINATION);
        if (i2 == tf0.START.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process start commute time");
            a(str, str2);
        } else if (i2 == tf0.STOP.ordinal()) {
            FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "process stop commute time");
            JSONObject jSONObject = new JSONObject();
            AddressWrapper addressWrapper = this.m;
            jSONObject.put("des", addressWrapper != null ? addressWrapper.getId() : null);
            jSONObject.put("type", "end");
            jSONObject.put("end_type", "user_exit");
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
            String jSONObject2 = jSONObject.toString();
            ee7.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            FLogger.INSTANCE.getRemote().summary(this.p ? 0 : FailureCode.USER_CANCELLED, FLogger.Component.APP, FLogger.Session.DIANA_COMMUTE_TIME, str, "WatchAppCommuteTimeManager");
            this.m = null;
            f();
            g();
        }
    }

    @DexIgnore
    public /* synthetic */ tk5(zd7 zd7) {
        this();
    }

    @DexIgnore
    public final void a(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getCommuteTimeForWatchApp serial " + str + " destinationAlias " + str2);
        this.h = str;
        this.p = false;
        ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new c(this, str2, str, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f9  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x010b  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0126  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0200  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            boolean r2 = r1 instanceof com.fossil.tk5.f
            if (r2 == 0) goto L_0x0017
            r2 = r1
            com.fossil.tk5$f r2 = (com.fossil.tk5.f) r2
            int r3 = r2.label
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = r3 & r4
            if (r5 == 0) goto L_0x0017
            int r3 = r3 - r4
            r2.label = r3
            goto L_0x001c
        L_0x0017:
            com.fossil.tk5$f r2 = new com.fossil.tk5$f
            r2.<init>(r0, r1)
        L_0x001c:
            java.lang.Object r1 = r2.result
            java.lang.Object r12 = com.fossil.nb7.a()
            int r3 = r2.label
            r13 = 2
            r4 = 1
            if (r3 == 0) goto L_0x005d
            if (r3 == r4) goto L_0x004d
            if (r3 != r13) goto L_0x0045
            java.lang.Object r3 = r2.L$4
            android.location.Location r3 = (android.location.Location) r3
            java.lang.Object r3 = r2.L$3
            com.portfolio.platform.data.LocationSource$Result r3 = (com.portfolio.platform.data.LocationSource.Result) r3
            java.lang.Object r3 = r2.L$2
            com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage r3 = (com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage) r3
            java.lang.Object r3 = r2.L$1
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r2 = r2.L$0
            com.fossil.tk5 r2 = (com.fossil.tk5) r2
            com.fossil.t87.a(r1)
            goto L_0x0230
        L_0x0045:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x004d:
            java.lang.Object r3 = r2.L$2
            com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage r3 = (com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage) r3
            java.lang.Object r4 = r2.L$1
            java.lang.String r4 = (java.lang.String) r4
            java.lang.Object r5 = r2.L$0
            com.fossil.tk5 r5 = (com.fossil.tk5) r5
            com.fossil.t87.a(r1)
            goto L_0x00ae
        L_0x005d:
            com.fossil.t87.a(r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            r3 = 2131886346(0x7f12010a, float:1.9407268E38)
            java.lang.String r1 = com.fossil.ig5.a(r1, r3)
            com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage r15 = new com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage
            java.lang.String r3 = "message"
            com.fossil.ee7.a(r1, r3)
            com.fossil.uf0 r3 = com.fossil.uf0.IN_PROGRESS
            r15.<init>(r1, r3)
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            java.lang.String r5 = r0.h
            if (r5 == 0) goto L_0x0241
            r3.a(r15, r5)
            com.portfolio.platform.data.LocationSource r3 = r0.c
            if (r3 == 0) goto L_0x023a
            com.portfolio.platform.PortfolioApp r5 = r0.a
            if (r5 == 0) goto L_0x0233
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 28
            r11 = 0
            r2.L$0 = r0
            r2.L$1 = r1
            r2.L$2 = r15
            r2.label = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r2
            java.lang.Object r3 = com.portfolio.platform.data.LocationSource.getLocation$default(r3, r4, r5, r6, r7, r8, r9, r10, r11)
            if (r3 != r12) goto L_0x00aa
            return r12
        L_0x00aa:
            r5 = r0
            r4 = r1
            r1 = r3
            r3 = r15
        L_0x00ae:
            com.portfolio.platform.data.LocationSource$Result r1 = (com.portfolio.platform.data.LocationSource.Result) r1
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "getDurationTimeBaseOnLocation currentUserLocation "
            r7.append(r8)
            android.location.Location r8 = r1.getLocation()
            if (r8 == 0) goto L_0x00cf
            double r8 = r8.getLatitude()
            java.lang.Double r8 = com.fossil.pb7.a(r8)
            goto L_0x00d0
        L_0x00cf:
            r8 = 0
        L_0x00d0:
            r7.append(r8)
            r8 = 58
            r7.append(r8)
            android.location.Location r8 = r1.getLocation()
            if (r8 == 0) goto L_0x00e7
            double r8 = r8.getLongitude()
            java.lang.Double r8 = com.fossil.pb7.a(r8)
            goto L_0x00e8
        L_0x00e7:
            r8 = 0
        L_0x00e8:
            r7.append(r8)
            java.lang.String r8 = " cacheDestination "
            r7.append(r8)
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r8 = r5.k
            if (r8 == 0) goto L_0x00f9
            java.lang.String r8 = r8.getAddress()
            goto L_0x00fa
        L_0x00f9:
            r8 = 0
        L_0x00fa:
            r7.append(r8)
            java.lang.String r8 = " currentDestination "
            r7.append(r8)
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r8 = r5.m
            if (r8 == 0) goto L_0x010b
            java.lang.String r8 = r8.getAddress()
            goto L_0x010c
        L_0x010b:
            r8 = 0
        L_0x010c:
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            java.lang.String r8 = "WatchAppCommuteTimeManager"
            r6.d(r8, r7)
            boolean r6 = r1.getFromCache()
            r5.l = r6
            com.portfolio.platform.data.LocationSource$ErrorState r6 = r1.getErrorState()
            com.portfolio.platform.data.LocationSource$ErrorState r7 = com.portfolio.platform.data.LocationSource.ErrorState.SUCCESS
            if (r6 != r7) goto L_0x0200
            android.location.Location r6 = r1.getLocation()
            if (r6 == 0) goto L_0x01fb
            com.portfolio.platform.data.model.diana.commutetime.Address r7 = r5.j
            if (r7 == 0) goto L_0x01dc
            com.portfolio.platform.data.model.diana.commutetime.TrafficResponse r7 = r5.i
            if (r7 == 0) goto L_0x01dc
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r7 = r5.k
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r9 = r5.m
            boolean r7 = com.fossil.ee7.a(r7, r9)
            if (r7 == 0) goto L_0x01dc
            com.portfolio.platform.data.model.diana.commutetime.Address r7 = new com.portfolio.platform.data.model.diana.commutetime.Address
            double r9 = r6.getLatitude()
            double r13 = r6.getLongitude()
            r7.<init>(r9, r13)
            com.portfolio.platform.data.model.diana.commutetime.Address r9 = r5.j
            if (r9 == 0) goto L_0x01d7
            java.lang.Float r7 = r5.a(r7, r9)
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "Cache and current destination is the same, distance is "
            r10.append(r13)
            r10.append(r7)
            java.lang.String r10 = r10.toString()
            r9.d(r8, r10)
            if (r7 == 0) goto L_0x01dc
            r7.floatValue()
            float r7 = r7.floatValue()
            r9 = 300(0x12c, float:4.2E-43)
            float r9 = (float) r9
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 >= 0) goto L_0x01dc
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "getDurationTimeBaseOnLocation fromCache"
            r1.d(r8, r2)
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r1 = r5.m
            if (r1 == 0) goto L_0x01d2
            java.lang.String r1 = r1.getName()
            com.portfolio.platform.data.model.diana.commutetime.TrafficResponse r2 = r5.i
            if (r2 == 0) goto L_0x01cd
            long r2 = r2.getDurationInTraffic()
            com.portfolio.platform.data.model.diana.commutetime.TrafficResponse r4 = r5.i
            if (r4 == 0) goto L_0x01c8
            java.lang.String r4 = r4.getStatus()
            if (r4 == 0) goto L_0x01a3
            goto L_0x01a5
        L_0x01a3:
            java.lang.String r4 = "unknown"
        L_0x01a5:
            r5.a(r1, r2, r4)
            com.portfolio.platform.data.model.diana.commutetime.Address r1 = r5.j
            if (r1 == 0) goto L_0x01c3
            double r1 = r1.getLat()
            com.portfolio.platform.data.model.diana.commutetime.Address r3 = r5.j
            if (r3 == 0) goto L_0x01be
            double r3 = r3.getLng()
            r5.a(r1, r3)
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x01be:
            com.fossil.ee7.a()
            r7 = 0
            throw r7
        L_0x01c3:
            r7 = 0
            com.fossil.ee7.a()
            throw r7
        L_0x01c8:
            r7 = 0
            com.fossil.ee7.a()
            throw r7
        L_0x01cd:
            r7 = 0
            com.fossil.ee7.a()
            throw r7
        L_0x01d2:
            r7 = 0
            com.fossil.ee7.a()
            throw r7
        L_0x01d7:
            r7 = 0
            com.fossil.ee7.a()
            throw r7
        L_0x01dc:
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r9 = "Cache and current destination is the different, start get duration"
            r7.d(r8, r9)
            r2.L$0 = r5
            r2.L$1 = r4
            r2.L$2 = r3
            r2.L$3 = r1
            r2.L$4 = r6
            r1 = 2
            r2.label = r1
            java.lang.Object r1 = r5.a(r6, r2)
            if (r1 != r12) goto L_0x0230
            return r12
        L_0x01fb:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x0200:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getDurationTimeBaseOnLocation getLocation error="
            r3.append(r4)
            com.portfolio.platform.data.LocationSource$ErrorState r4 = r1.getErrorState()
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r2.d(r8, r3)
            com.portfolio.platform.data.LocationSource$ErrorState r1 = r1.getErrorState()
            r5.a(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = "getDurationTimeBaseOnLocation - current location is null, stop service."
            r1.d(r8, r2)
        L_0x0230:
            com.fossil.i97 r1 = com.fossil.i97.a
            return r1
        L_0x0233:
            java.lang.String r1 = "mPortfolioApp"
            com.fossil.ee7.d(r1)
            r1 = 0
            throw r1
        L_0x023a:
            r1 = 0
            java.lang.String r2 = "mLocationSource"
            com.fossil.ee7.d(r2)
            throw r1
        L_0x0241:
            r1 = 0
            com.fossil.ee7.a()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tk5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x019c A[Catch:{ Exception -> 0x0273 }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x01a1 A[Catch:{ Exception -> 0x0273 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x01b8 A[Catch:{ Exception -> 0x0273 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x026a A[Catch:{ Exception -> 0x0271 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(android.location.Location r26, com.fossil.fb7<? super com.fossil.i97> r27) {
        /*
            r25 = this;
            r7 = r25
            r0 = r27
            boolean r1 = r0 instanceof com.fossil.tk5.e
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.fossil.tk5$e r1 = (com.fossil.tk5.e) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            goto L_0x001c
        L_0x0017:
            com.fossil.tk5$e r1 = new com.fossil.tk5$e
            r1.<init>(r7, r0)
        L_0x001c:
            r0 = r1
            java.lang.Object r1 = r0.result
            java.lang.Object r8 = com.fossil.nb7.a()
            int r2 = r0.label
            java.lang.String r9 = "type"
            java.lang.String r10 = "des"
            r11 = 1
            java.lang.String r13 = "WatchAppCommuteTimeManager"
            if (r2 == 0) goto L_0x0062
            if (r2 != r11) goto L_0x005a
            java.lang.Object r2 = r0.L$6
            org.json.JSONObject r2 = (org.json.JSONObject) r2
            java.lang.Object r2 = r0.L$5
            com.portfolio.platform.data.model.diana.commutetime.TrafficRequest r2 = (com.portfolio.platform.data.model.diana.commutetime.TrafficRequest) r2
            java.lang.Object r2 = r0.L$4
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r2 = (com.portfolio.platform.data.model.diana.commutetime.AddressWrapper) r2
            java.lang.Object r3 = r0.L$3
            com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage r3 = (com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage) r3
            java.lang.Object r3 = r0.L$2
            java.lang.String r3 = (java.lang.String) r3
            java.lang.Object r3 = r0.L$1
            android.location.Location r3 = (android.location.Location) r3
            java.lang.Object r0 = r0.L$0
            com.fossil.tk5 r0 = (com.fossil.tk5) r0
            com.fossil.t87.a(r1)     // Catch:{ Exception -> 0x0056 }
            r15 = r2
            r16 = r13
            r2 = r1
            r1 = r3
            goto L_0x0193
        L_0x0056:
            r0 = move-exception
            r5 = r13
            goto L_0x0276
        L_0x005a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0062:
            com.fossil.t87.a(r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            r2 = 2131886343(0x7f120107, float:1.9407262E38)
            java.lang.String r14 = com.fossil.ig5.a(r1, r2)
            com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage r15 = new com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime.CommuteTimeWatchAppMessage
            java.lang.String r1 = "message"
            com.fossil.ee7.a(r14, r1)
            com.fossil.uf0 r1 = com.fossil.uf0.IN_PROGRESS
            r15.<init>(r14, r1)
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r2 = r7.h
            if (r2 == 0) goto L_0x029e
            r1.a(r15, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDurationTime location lng="
            r2.append(r3)
            double r3 = r26.getLongitude()
            r2.append(r3)
            java.lang.String r3 = " lat="
            r2.append(r3)
            double r3 = r26.getLatitude()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r1.d(r13, r2)
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r6 = r7.m
            if (r6 == 0) goto L_0x029b
            com.portfolio.platform.data.model.diana.commutetime.TrafficRequest r5 = new com.portfolio.platform.data.model.diana.commutetime.TrafficRequest
            boolean r1 = r6.getAvoidTolls()
            com.portfolio.platform.data.model.diana.commutetime.Address r2 = new com.portfolio.platform.data.model.diana.commutetime.Address
            double r3 = r6.getLat()
            r16 = r13
            double r12 = r6.getLng()
            r2.<init>(r3, r12)
            com.portfolio.platform.data.model.diana.commutetime.Address r3 = new com.portfolio.platform.data.model.diana.commutetime.Address
            double r12 = r26.getLatitude()
            r17 = r14
            r18 = r15
            double r14 = r26.getLongitude()
            r3.<init>(r12, r14)
            r5.<init>(r1, r2, r3)
            org.json.JSONObject r12 = new org.json.JSONObject
            r12.<init>()
            java.lang.String r1 = r6.getId()
            r12.put(r10, r1)
            java.lang.String r1 = "begin_request"
            r12.put(r9, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            double r2 = r26.getLatitude()
            r1.append(r2)
            java.lang.String r2 = ", "
            r1.append(r2)
            double r3 = r26.getLongitude()
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            java.lang.String r3 = "origin_coordinate"
            r12.put(r3, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            double r3 = r6.getLat()
            r1.append(r3)
            r1.append(r2)
            double r2 = r6.getLng()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "des_coordinate"
            r12.put(r2, r1)
            java.util.Calendar r1 = java.util.Calendar.getInstance()
            java.lang.String r2 = "Calendar.getInstance()"
            com.fossil.ee7.a(r1, r2)
            long r1 = r1.getTimeInMillis()
            java.lang.String r3 = "timestamp"
            r12.put(r3, r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r19 = r1.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r20 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r21 = com.misfit.frameworks.buttonservice.log.FLogger.Session.DIANA_COMMUTE_TIME
            java.lang.String r1 = r7.h
            if (r1 == 0) goto L_0x0296
            java.lang.String r2 = r12.toString()
            java.lang.String r3 = "jsonObject.toString()"
            com.fossil.ee7.a(r2, r3)
            java.lang.String r23 = "WatchAppCommuteTimeManager"
            r22 = r1
            r24 = r2
            r19.i(r20, r21, r22, r23, r24)
            com.fossil.tk5$d r13 = new com.fossil.tk5$d     // Catch:{ Exception -> 0x0273 }
            r3 = 0
            r1 = r13
            r2 = r5
            r4 = r25
            r14 = r5
            r5 = r26
            r15 = r6
            r6 = r0
            r1.<init>(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0273 }
            r0.L$0 = r7     // Catch:{ Exception -> 0x0273 }
            r1 = r26
            r0.L$1 = r1     // Catch:{ Exception -> 0x0273 }
            r2 = r17
            r0.L$2 = r2     // Catch:{ Exception -> 0x0273 }
            r2 = r18
            r0.L$3 = r2     // Catch:{ Exception -> 0x0273 }
            r0.L$4 = r15     // Catch:{ Exception -> 0x0273 }
            r0.L$5 = r14     // Catch:{ Exception -> 0x0273 }
            r0.L$6 = r12     // Catch:{ Exception -> 0x0273 }
            r0.label = r11     // Catch:{ Exception -> 0x0273 }
            java.lang.Object r0 = com.fossil.aj5.a(r13, r0)     // Catch:{ Exception -> 0x0273 }
            if (r0 != r8) goto L_0x0191
            return r8
        L_0x0191:
            r2 = r0
            r0 = r7
        L_0x0193:
            com.fossil.zi5 r2 = (com.fossil.zi5) r2     // Catch:{ Exception -> 0x0273 }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x0273 }
            r3.<init>()     // Catch:{ Exception -> 0x0273 }
            if (r15 == 0) goto L_0x01a1
            java.lang.String r4 = r15.getId()     // Catch:{ Exception -> 0x0273 }
            goto L_0x01a2
        L_0x01a1:
            r4 = 0
        L_0x01a2:
            r3.put(r10, r4)     // Catch:{ Exception -> 0x0273 }
            java.lang.String r4 = "response_received"
            r3.put(r9, r4)     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r8 = r4.getRemote()     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.FLogger$Component r9 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.FLogger$Session r10 = com.misfit.frameworks.buttonservice.log.FLogger.Session.DIANA_COMMUTE_TIME     // Catch:{ Exception -> 0x0273 }
            java.lang.String r11 = r0.h     // Catch:{ Exception -> 0x0273 }
            if (r11 == 0) goto L_0x026a
            java.lang.String r12 = "WatchAppCommuteTimeManager"
            java.lang.String r13 = r3.toString()     // Catch:{ Exception -> 0x0273 }
            java.lang.String r3 = "json.toString()"
            com.fossil.ee7.a(r13, r3)     // Catch:{ Exception -> 0x0273 }
            r8.i(r9, r10, r11, r12, r13)     // Catch:{ Exception -> 0x0273 }
            boolean r3 = r2 instanceof com.fossil.bj5     // Catch:{ Exception -> 0x0273 }
            if (r3 == 0) goto L_0x0241
            com.fossil.bj5 r2 = (com.fossil.bj5) r2     // Catch:{ Exception -> 0x0273 }
            java.lang.Object r2 = r2.a()     // Catch:{ Exception -> 0x0273 }
            com.portfolio.platform.data.model.diana.commutetime.TrafficResponse r2 = (com.portfolio.platform.data.model.diana.commutetime.TrafficResponse) r2     // Catch:{ Exception -> 0x0273 }
            if (r2 == 0) goto L_0x029b
            r0.k = r15     // Catch:{ Exception -> 0x0273 }
            r0.i = r2     // Catch:{ Exception -> 0x0273 }
            com.portfolio.platform.data.model.diana.commutetime.Address r3 = new com.portfolio.platform.data.model.diana.commutetime.Address     // Catch:{ Exception -> 0x0273 }
            double r4 = r1.getLatitude()     // Catch:{ Exception -> 0x0273 }
            double r8 = r1.getLongitude()     // Catch:{ Exception -> 0x0273 }
            r3.<init>(r4, r8)     // Catch:{ Exception -> 0x0273 }
            r0.j = r3     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0273 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x0273 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0273 }
            r4.<init>()     // Catch:{ Exception -> 0x0273 }
            java.lang.String r5 = "getDurationTime trafficResponse distance "
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            long r5 = r2.getDistance()     // Catch:{ Exception -> 0x0273 }
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            java.lang.String r5 = "  status "
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            java.lang.String r5 = r2.getStatus()     // Catch:{ Exception -> 0x0273 }
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            java.lang.String r5 = " durationInTraffic "
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            long r5 = r2.getDurationInTraffic()     // Catch:{ Exception -> 0x0273 }
            r4.append(r5)     // Catch:{ Exception -> 0x0273 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0273 }
            r5 = r16
            r3.d(r5, r4)     // Catch:{ Exception -> 0x0271 }
            java.lang.String r3 = r15.getName()     // Catch:{ Exception -> 0x0271 }
            long r8 = r2.getDurationInTraffic()     // Catch:{ Exception -> 0x0271 }
            java.lang.String r2 = r2.getStatus()     // Catch:{ Exception -> 0x0271 }
            if (r2 == 0) goto L_0x022e
            goto L_0x0230
        L_0x022e:
            java.lang.String r2 = "unknown"
        L_0x0230:
            r0.a(r3, r8, r2)     // Catch:{ Exception -> 0x0271 }
            double r2 = r1.getLatitude()     // Catch:{ Exception -> 0x0271 }
            double r8 = r1.getLongitude()     // Catch:{ Exception -> 0x0271 }
            r0.a(r2, r8)     // Catch:{ Exception -> 0x0271 }
            com.fossil.i97 r0 = com.fossil.i97.a     // Catch:{ Exception -> 0x0271 }
            goto L_0x029b
        L_0x0241:
            r5 = r16
            boolean r0 = r2 instanceof com.fossil.yi5     // Catch:{ Exception -> 0x0271 }
            if (r0 == 0) goto L_0x0264
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0271 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x0271 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0271 }
            r1.<init>()     // Catch:{ Exception -> 0x0271 }
            java.lang.String r3 = "getDurationTime failed: "
            r1.append(r3)     // Catch:{ Exception -> 0x0271 }
            r1.append(r2)     // Catch:{ Exception -> 0x0271 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0271 }
            r0.d(r5, r1)     // Catch:{ Exception -> 0x0271 }
            com.fossil.i97 r0 = com.fossil.i97.a     // Catch:{ Exception -> 0x0271 }
            goto L_0x029b
        L_0x0264:
            com.fossil.p87 r0 = new com.fossil.p87     // Catch:{ Exception -> 0x0271 }
            r0.<init>()     // Catch:{ Exception -> 0x0271 }
            throw r0     // Catch:{ Exception -> 0x0271 }
        L_0x026a:
            r5 = r16
            com.fossil.ee7.a()     // Catch:{ Exception -> 0x0271 }
            r0 = 0
            throw r0
        L_0x0271:
            r0 = move-exception
            goto L_0x0276
        L_0x0273:
            r0 = move-exception
            r5 = r16
        L_0x0276:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "getDurationTime exception="
            r2.append(r3)
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.e(r5, r2)
            r0.printStackTrace()
            com.fossil.i97 r0 = com.fossil.i97.a
            goto L_0x029b
        L_0x0296:
            com.fossil.ee7.a()
            r0 = 0
            throw r0
        L_0x029b:
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x029e:
            r0 = 0
            com.fossil.ee7.a()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.tk5.a(android.location.Location, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(String str, long j2, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse destination " + str + ", durationInTraffic " + j2 + ", trafficStatus " + str2);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            return;
        }
        if (addressWrapper == null) {
            ee7.a();
            throw null;
        } else if (!TextUtils.isEmpty(addressWrapper.getName())) {
            int a2 = af7.a(((float) j2) / 60.0f);
            if (mh7.b(str2, "not_found", true)) {
                PortfolioApp portfolioApp = this.a;
                if (portfolioApp != null) {
                    String a3 = ig5.a(portfolioApp, 2131886342);
                    ee7.a((Object) a3, "message");
                    CommuteTimeWatchAppMessage commuteTimeWatchAppMessage = new CommuteTimeWatchAppMessage(a3, uf0.END);
                    PortfolioApp c2 = PortfolioApp.g0.c();
                    String str3 = this.h;
                    if (str3 != null) {
                        c2.a(commuteTimeWatchAppMessage, str3);
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    ee7.d("mPortfolioApp");
                    throw null;
                }
            } else {
                try {
                    CommuteTimeWatchAppInfo commuteTimeWatchAppInfo = new CommuteTimeWatchAppInfo(str, a2, a(str2));
                    PortfolioApp c3 = PortfolioApp.g0.c();
                    String str4 = this.h;
                    if (str4 != null) {
                        c3.a(commuteTimeWatchAppInfo, str4);
                        if5 c4 = qd5.f.c("commute-time");
                        if (c4 != null) {
                            String str5 = this.h;
                            if (str5 != null) {
                                c4.a(str5, this.l, "");
                            } else {
                                ee7.a();
                                throw null;
                            }
                        }
                        qd5.f.e("commute-time");
                        return;
                    }
                    ee7.a();
                    throw null;
                } catch (IllegalArgumentException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("WatchAppCommuteTimeManager", "sendCommuteTimeAppResponse exception exception=" + e2.getMessage());
                }
            }
        }
    }

    @DexIgnore
    public final String a(String str) {
        switch (str.hashCode()) {
            case -1078030475:
                if (str.equals("medium")) {
                    PortfolioApp portfolioApp = this.a;
                    if (portfolioApp != null) {
                        String a2 = ig5.a(portfolioApp, 2131887547);
                        ee7.a((Object) a2, "LanguageHelper.getString\u2026ng.traffic_status_medium)");
                        return a2;
                    }
                    ee7.d("mPortfolioApp");
                    throw null;
                }
                break;
            case -284840886:
                if (str.equals("unknown")) {
                    PortfolioApp portfolioApp2 = this.a;
                    if (portfolioApp2 != null) {
                        String a3 = ig5.a(portfolioApp2, 2131887548);
                        ee7.a((Object) a3, "LanguageHelper.getString\u2026g.traffic_status_unknown)");
                        return a3;
                    }
                    ee7.d("mPortfolioApp");
                    throw null;
                }
                break;
            case 99152071:
                if (str.equals("heavy")) {
                    PortfolioApp portfolioApp3 = this.a;
                    if (portfolioApp3 != null) {
                        String a4 = ig5.a(portfolioApp3, 2131887545);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026ing.traffic_status_heavy)");
                        return a4;
                    }
                    ee7.d("mPortfolioApp");
                    throw null;
                }
                break;
            case 102970646:
                if (str.equals("light")) {
                    PortfolioApp portfolioApp4 = this.a;
                    if (portfolioApp4 != null) {
                        String a5 = ig5.a(portfolioApp4, 2131887546);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026ing.traffic_status_light)");
                        return a5;
                    }
                    ee7.d("mPortfolioApp");
                    throw null;
                }
                break;
        }
        return "--";
    }

    @DexIgnore
    public final Float a(Address address, Address address2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchAppCommuteTimeManager", "getSimpleDistance between " + address + " and " + address2);
        float[] fArr = new float[1];
        Location.distanceBetween(address2.getLat(), address2.getLng(), address.getLat(), address.getLng(), fArr);
        return Float.valueOf(fArr[0]);
    }

    @DexIgnore
    public final void a(double d2, double d3) {
        Float f2;
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver updateObserver=" + d2 + ',' + d3);
        AddressWrapper addressWrapper = this.m;
        if (addressWrapper == null) {
            f2 = null;
        } else if (addressWrapper != null) {
            double lat = addressWrapper.getLat();
            AddressWrapper addressWrapper2 = this.m;
            if (addressWrapper2 != null) {
                f2 = a(new Address(lat, addressWrapper2.getLng()), new Address(d2, d3));
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.a();
            throw null;
        }
        FLogger.INSTANCE.getLocal().d("WatchAppCommuteTimeManager", "updateObserver distance=" + f2);
        JSONObject jSONObject = new JSONObject();
        AddressWrapper addressWrapper3 = this.m;
        jSONObject.put("des", addressWrapper3 != null ? addressWrapper3.getId() : null);
        jSONObject.put("type", "loc_update");
        jSONObject.put("dis_to_des", f2);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.APP;
        FLogger.Session session = FLogger.Session.DIANA_COMMUTE_TIME;
        String str = this.h;
        if (str != null) {
            String jSONObject2 = jSONObject.toString();
            ee7.a((Object) jSONObject2, "jsonObject.toString()");
            remote.i(component, session, str, "WatchAppCommuteTimeManager", jSONObject2);
            if (f2 != null) {
                if (f2 == null) {
                    ee7.a();
                    throw null;
                } else if (f2.floatValue() < ((float) 2000)) {
                    if (f2 == null) {
                        ee7.a();
                        throw null;
                    } else if (f2.floatValue() < ((float) 100)) {
                        f();
                        g();
                        c();
                        return;
                    } else {
                        g();
                        d();
                        return;
                    }
                }
            }
            f();
            e();
            return;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final void a(LocationSource.ErrorState errorState) {
        if5 c2 = qd5.f.c("commute-time");
        if (c2 != null) {
            String str = this.h;
            if (str != null) {
                c2.a(str, false, ErrorCodeBuilder.AppError.NOT_FETCH_CURRENT_LOCATION.getValue());
            } else {
                ee7.a();
                throw null;
            }
        }
        qd5.f.e("commute-time");
    }
}
