package com.fossil;

import androidx.lifecycle.LiveData;
import com.fossil.te5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j76 extends g76 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<ActivitySummary> f;
    @DexIgnore
    public /* final */ h76 g;
    @DexIgnore
    public /* final */ SummariesRepository h;
    @DexIgnore
    public /* final */ FitnessDataRepository i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ pj4 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1", f = "DashboardActiveTimePresenter.kt", l = {58, 66}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a<T> implements zd<qf<ActivitySummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ h76 a;

            @DexIgnore
            public a(h76 h76) {
                this.a = h76;
            }

            @DexIgnore
            /* renamed from: a */
            public final void onChanged(qf<ActivitySummary> qfVar) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(qfVar != null ? Integer.valueOf(qfVar.size()) : null);
                local.d("DashboardActiveTimePresenter", sb.toString());
                if (qfVar != null) {
                    this.a.a(qfVar);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.j76$b$b")
        /* renamed from: com.fossil.j76$b$b  reason: collision with other inner class name */
        public static final class C0084b implements te5.a {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0084b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            @Override // com.fossil.te5.a
            public final void a(te5.g gVar) {
                ee7.b(gVar, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardActiveTimePresenter", "onStatusChange status=" + gVar);
                if (gVar.a()) {
                    this.a.this$0.g.d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimePresenter$initDataSource$1$user$1", f = "DashboardActiveTimePresenter.kt", l = {58}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFUser> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    UserRepository f = this.this$0.this$0.j;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = f.getCurrentUser(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(j76 j76, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00aa  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00b6  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r11) {
            /*
                r10 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r10.label
                r2 = 2
                r3 = 1
                if (r1 == 0) goto L_0x0036
                if (r1 == r3) goto L_0x002e
                if (r1 != r2) goto L_0x0026
                java.lang.Object r0 = r10.L$4
                com.fossil.j76 r0 = (com.fossil.j76) r0
                java.lang.Object r1 = r10.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r10.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r10.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r11)
                goto L_0x008f
            L_0x0026:
                java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r11.<init>(r0)
                throw r11
            L_0x002e:
                java.lang.Object r1 = r10.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r11)
                goto L_0x0052
            L_0x0036:
                com.fossil.t87.a(r11)
                com.fossil.yi7 r1 = r10.p$
                com.fossil.j76 r11 = r10.this$0
                com.fossil.ti7 r11 = r11.c()
                com.fossil.j76$b$c r4 = new com.fossil.j76$b$c
                r5 = 0
                r4.<init>(r10, r5)
                r10.L$0 = r1
                r10.label = r3
                java.lang.Object r11 = com.fossil.vh7.a(r11, r4, r10)
                if (r11 != r0) goto L_0x0052
                return r0
            L_0x0052:
                com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
                if (r11 == 0) goto L_0x00be
                java.lang.String r3 = r11.getCreatedAt()
                java.util.Date r6 = com.fossil.zd5.d(r3)
                com.fossil.j76 r3 = r10.this$0
                com.portfolio.platform.data.source.SummariesRepository r4 = r3.h
                com.fossil.j76 r5 = r10.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r5 = r5.i
                java.lang.String r7 = "createdDate"
                com.fossil.ee7.a(r6, r7)
                com.fossil.j76 r7 = r10.this$0
                com.fossil.pj4 r7 = r7.k
                com.fossil.j76$b$b r8 = new com.fossil.j76$b$b
                r8.<init>(r10)
                r10.L$0 = r1
                r10.L$1 = r11
                r10.L$2 = r11
                r10.L$3 = r6
                r10.L$4 = r3
                r10.label = r2
                r9 = r10
                java.lang.Object r11 = r4.getSummariesPaging(r5, r6, r7, r8, r9)
                if (r11 != r0) goto L_0x008e
                return r0
            L_0x008e:
                r0 = r3
            L_0x008f:
                com.portfolio.platform.data.Listing r11 = (com.portfolio.platform.data.Listing) r11
                r0.f = r11
                com.fossil.j76 r11 = r10.this$0
                com.fossil.h76 r11 = r11.g
                com.fossil.j76 r0 = r10.this$0
                com.portfolio.platform.data.Listing r0 = r0.f
                if (r0 == 0) goto L_0x00be
                androidx.lifecycle.LiveData r0 = r0.getPagedList()
                if (r0 == 0) goto L_0x00be
                if (r11 == 0) goto L_0x00b6
                r1 = r11
                com.fossil.i76 r1 = (com.fossil.i76) r1
                com.fossil.j76$b$a r2 = new com.fossil.j76$b$a
                r2.<init>(r11)
                r0.a(r1, r2)
                goto L_0x00be
            L_0x00b6:
                com.fossil.x87 r11 = new com.fossil.x87
                java.lang.String r0 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment"
                r11.<init>(r0)
                throw r11
            L_0x00be:
                com.fossil.i97 r11 = com.fossil.i97.a
                return r11
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.j76.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public j76(h76 h76, SummariesRepository summariesRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository, pj4 pj4) {
        ee7.b(h76, "mView");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(userRepository, "mUserRepository");
        ee7.b(pj4, "mAppExecutors");
        this.g = h76;
        this.h = summariesRepository;
        this.i = fitnessDataRepository;
        this.j = userRepository;
        this.k = pj4;
        FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
    }

    @DexIgnore
    @Override // com.fossil.g76
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.g76
    public void i() {
        LiveData<qf<ActivitySummary>> pagedList;
        try {
            h76 h76 = this.g;
            Listing<ActivitySummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (h76 != null) {
                    pagedList.a((i76) h76);
                } else {
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.activetime.DashboardActiveTimeFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(i97.a);
            local.e("DashboardActiveTimePresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.g76
    public void j() {
        vc7<i97> retry;
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "retry all failed request");
        Listing<ActivitySummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public void k() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        Boolean w = zd5.w(this.e);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimePresenter", "start isDateTodayDate " + w + " listingPage " + this.f);
        if (!w.booleanValue()) {
            this.e = new Date();
            Listing<ActivitySummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("DashboardActiveTimePresenter", "stop");
    }
}
