package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.data.DataHolder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class up3 extends c62<Object> implements i12 {
    @DexIgnore
    public /* final */ Status d;

    @DexIgnore
    public up3(DataHolder dataHolder) {
        super(dataHolder);
        this.d = new Status(dataHolder.g());
    }

    @DexIgnore
    @Override // com.fossil.i12
    public Status a() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.c62
    public String c() {
        return "path";
    }

    @DexIgnore
    @Override // com.fossil.c62
    public /* synthetic */ Object a(int i, int i2) {
        return new iq3(((y52) this).a, i, i2);
    }
}
