package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class fm0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a;

    /*
    static {
        int[] iArr = new int[ru0.values().length];
        a = iArr;
        iArr[ru0.JSON_FILE_EVENT.ordinal()] = 1;
        a[ru0.HEARTBEAT_EVENT.ordinal()] = 2;
        a[ru0.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 3;
        a[ru0.APP_NOTIFICATION_EVENT.ordinal()] = 4;
        a[ru0.MUSIC_EVENT.ordinal()] = 5;
        a[ru0.BACKGROUND_SYNC_EVENT.ordinal()] = 6;
        a[ru0.SERVICE_CHANGE_EVENT.ordinal()] = 7;
        a[ru0.MICRO_APP_EVENT.ordinal()] = 8;
        a[ru0.TIME_SYNC_EVENT.ordinal()] = 9;
        a[ru0.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        a[ru0.BATTERY_EVENT.ordinal()] = 11;
        a[ru0.ENCRYPTED_DATA.ordinal()] = 12;
    }
    */
}
