package com.fossil;

import com.misfit.frameworks.common.enums.Action;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dg4 extends jg4 {
    @DexIgnore
    public static volatile dg4[] x;
    @DexIgnore
    public fg4 a;
    @DexIgnore
    public fg4 b;
    @DexIgnore
    public fg4 c;
    @DexIgnore
    public fg4 d;
    @DexIgnore
    public fg4 e;
    @DexIgnore
    public fg4 f;
    @DexIgnore
    public fg4 g;
    @DexIgnore
    public fg4 h;
    @DexIgnore
    public fg4 i;
    @DexIgnore
    public fg4 j;
    @DexIgnore
    public fg4 k;
    @DexIgnore
    public fg4 l;
    @DexIgnore
    public fg4 m;
    @DexIgnore
    public fg4 n;
    @DexIgnore
    public fg4 o;
    @DexIgnore
    public fg4 p;
    @DexIgnore
    public int q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public String t;
    @DexIgnore
    public String u;
    @DexIgnore
    public cg4[] v;
    @DexIgnore
    public cg4[] w;

    @DexIgnore
    public dg4() {
        a();
    }

    @DexIgnore
    public static dg4[] b() {
        if (x == null) {
            synchronized (hg4.a) {
                if (x == null) {
                    x = new dg4[0];
                }
            }
        }
        return x;
    }

    @DexIgnore
    public dg4 a() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;
        this.k = null;
        this.l = null;
        this.m = null;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = 0;
        this.r = "";
        this.s = "";
        this.t = "";
        this.u = "";
        this.v = cg4.b();
        this.w = cg4.b();
        return this;
    }

    @DexIgnore
    @Override // com.fossil.jg4
    public dg4 a(gg4 gg4) throws IOException {
        while (true) {
            int j2 = gg4.j();
            switch (j2) {
                case 0:
                    return this;
                case 10:
                    if (this.a == null) {
                        this.a = new fg4();
                    }
                    gg4.a(this.a);
                    break;
                case 18:
                    if (this.b == null) {
                        this.b = new fg4();
                    }
                    gg4.a(this.b);
                    break;
                case 26:
                    if (this.c == null) {
                        this.c = new fg4();
                    }
                    gg4.a(this.c);
                    break;
                case 34:
                    if (this.d == null) {
                        this.d = new fg4();
                    }
                    gg4.a(this.d);
                    break;
                case 42:
                    if (this.e == null) {
                        this.e = new fg4();
                    }
                    gg4.a(this.e);
                    break;
                case 50:
                    if (this.f == null) {
                        this.f = new fg4();
                    }
                    gg4.a(this.f);
                    break;
                case 58:
                    if (this.g == null) {
                        this.g = new fg4();
                    }
                    gg4.a(this.g);
                    break;
                case 66:
                    if (this.h == null) {
                        this.h = new fg4();
                    }
                    gg4.a(this.h);
                    break;
                case 74:
                    gg4.i();
                    break;
                case 80:
                    this.q = gg4.d();
                    break;
                case 90:
                    this.r = gg4.i();
                    break;
                case 98:
                    gg4.i();
                    break;
                case 106:
                    this.s = gg4.i();
                    break;
                case 122:
                    this.t = gg4.i();
                    break;
                case 130:
                    this.u = gg4.i();
                    break;
                case 138:
                    gg4.i();
                    break;
                case 144:
                    gg4.c();
                    break;
                case 154:
                    int a2 = lg4.a(gg4, 154);
                    cg4[] cg4Arr = this.v;
                    int length = cg4Arr == null ? 0 : cg4Arr.length;
                    int i2 = a2 + length;
                    cg4[] cg4Arr2 = new cg4[i2];
                    if (length != 0) {
                        System.arraycopy(this.v, 0, cg4Arr2, 0, length);
                    }
                    while (length < i2 - 1) {
                        cg4Arr2[length] = new cg4();
                        gg4.a(cg4Arr2[length]);
                        gg4.j();
                        length++;
                    }
                    cg4Arr2[length] = new cg4();
                    gg4.a(cg4Arr2[length]);
                    this.v = cg4Arr2;
                    break;
                case 162:
                    int a3 = lg4.a(gg4, 162);
                    cg4[] cg4Arr3 = this.w;
                    int length2 = cg4Arr3 == null ? 0 : cg4Arr3.length;
                    int i3 = a3 + length2;
                    cg4[] cg4Arr4 = new cg4[i3];
                    if (length2 != 0) {
                        System.arraycopy(this.w, 0, cg4Arr4, 0, length2);
                    }
                    while (length2 < i3 - 1) {
                        cg4Arr4[length2] = new cg4();
                        gg4.a(cg4Arr4[length2]);
                        gg4.j();
                        length2++;
                    }
                    cg4Arr4[length2] = new cg4();
                    gg4.a(cg4Arr4[length2]);
                    this.w = cg4Arr4;
                    break;
                case 170:
                    if (this.i == null) {
                        this.i = new fg4();
                    }
                    gg4.a(this.i);
                    break;
                case 176:
                    gg4.c();
                    break;
                case 186:
                    gg4.i();
                    break;
                case 194:
                    if (this.p == null) {
                        this.p = new fg4();
                    }
                    gg4.a(this.p);
                    break;
                case Action.Selfie.TAKE_BURST /*{ENCODED_INT: 202}*/:
                    if (this.j == null) {
                        this.j = new fg4();
                    }
                    gg4.a(this.j);
                    break;
                case 208:
                    gg4.c();
                    break;
                case 218:
                    if (this.k == null) {
                        this.k = new fg4();
                    }
                    gg4.a(this.k);
                    break;
                case 226:
                    if (this.l == null) {
                        this.l = new fg4();
                    }
                    gg4.a(this.l);
                    break;
                case 234:
                    if (this.m == null) {
                        this.m = new fg4();
                    }
                    gg4.a(this.m);
                    break;
                case 242:
                    if (this.n == null) {
                        this.n = new fg4();
                    }
                    gg4.a(this.n);
                    break;
                case 250:
                    if (this.o == null) {
                        this.o = new fg4();
                    }
                    gg4.a(this.o);
                    break;
                case 256:
                    gg4.c();
                    break;
                default:
                    if (lg4.b(gg4, j2)) {
                        break;
                    } else {
                        return this;
                    }
            }
        }
    }
}
