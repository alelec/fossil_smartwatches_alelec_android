package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.List;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class co7 {
    @DexIgnore
    public static /* final */ co7 a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends co7 {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements c {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        @Override // com.fossil.co7.c
        public co7 a(qn7 qn7) {
            return co7.this;
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        co7 a(qn7 qn7);
    }

    @DexIgnore
    public static c a(co7 co7) {
        return new b();
    }

    @DexIgnore
    public void a(qn7 qn7) {
    }

    @DexIgnore
    public void a(qn7 qn7, long j) {
    }

    @DexIgnore
    public void a(qn7 qn7, eo7 eo7) {
    }

    @DexIgnore
    public void a(qn7 qn7, lo7 lo7) {
    }

    @DexIgnore
    public void a(qn7 qn7, un7 un7) {
    }

    @DexIgnore
    public void a(qn7 qn7, IOException iOException) {
    }

    @DexIgnore
    public void a(qn7 qn7, String str) {
    }

    @DexIgnore
    public void a(qn7 qn7, String str, List<InetAddress> list) {
    }

    @DexIgnore
    public void a(qn7 qn7, InetSocketAddress inetSocketAddress, Proxy proxy) {
    }

    @DexIgnore
    public void a(qn7 qn7, InetSocketAddress inetSocketAddress, Proxy proxy, jo7 jo7) {
    }

    @DexIgnore
    public void a(qn7 qn7, InetSocketAddress inetSocketAddress, Proxy proxy, jo7 jo7, IOException iOException) {
    }

    @DexIgnore
    public void a(qn7 qn7, Response response) {
    }

    @DexIgnore
    public void b(qn7 qn7) {
    }

    @DexIgnore
    public void b(qn7 qn7, long j) {
    }

    @DexIgnore
    public void b(qn7 qn7, un7 un7) {
    }

    @DexIgnore
    public void c(qn7 qn7) {
    }

    @DexIgnore
    public void d(qn7 qn7) {
    }

    @DexIgnore
    public void e(qn7 qn7) {
    }

    @DexIgnore
    public void f(qn7 qn7) {
    }

    @DexIgnore
    public void g(qn7 qn7) {
    }
}
