package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dq extends RuntimeException {
    @DexIgnore
    public dq(Exception exc) {
        super("An exception was thrown by an Executor", exc);
    }
}
