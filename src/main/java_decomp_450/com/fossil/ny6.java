package com.fossil;

import android.content.Context;
import android.view.ViewConfiguration;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ny6 {
    @DexIgnore
    public static /* final */ float w; // = ((float) (Math.log(0.75d) / Math.log(0.9d)));
    @DexIgnore
    public static /* final */ float[] x; // = new float[101];
    @DexIgnore
    public static /* final */ float y; // = 8.0f;
    @DexIgnore
    public static float z;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int k;
    @DexIgnore
    public long l;
    @DexIgnore
    public int m;
    @DexIgnore
    public float n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public Interpolator r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public /* final */ float v;

    /*
    static {
        float f2;
        float f3;
        float f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        for (int i2 = 0; i2 <= 100; i2++) {
            float f5 = ((float) i2) / 100.0f;
            float f6 = 1.0f;
            while (true) {
                float f7 = ((f6 - f4) / 2.0f) + f4;
                float f8 = 1.0f - f7;
                f2 = 3.0f * f7 * f8;
                f3 = f7 * f7 * f7;
                float f9 = (((f8 * 0.4f) + (0.6f * f7)) * f2) + f3;
                if (((double) Math.abs(f9 - f5)) < 1.0E-5d) {
                    break;
                } else if (f9 > f5) {
                    f6 = f7;
                } else {
                    f4 = f7;
                }
            }
            x[i2] = f2 + f3;
        }
        x[100] = 1.0f;
        z = 1.0f;
        z = 1.0f / b(1.0f);
    }
    */

    @DexIgnore
    public ny6(Context context, Interpolator interpolator) {
        this(context, interpolator, true);
    }

    @DexIgnore
    public final float a(float f2) {
        return this.v * 386.0878f * f2;
    }

    @DexIgnore
    public final float b() {
        return this.t - ((this.u * ((float) g())) / 2000.0f);
    }

    @DexIgnore
    public final int c() {
        return this.k;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public final int e() {
        return this.c;
    }

    @DexIgnore
    public final boolean f() {
        return this.q;
    }

    @DexIgnore
    public final int g() {
        return (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
    }

    @DexIgnore
    public ny6(Context context, Interpolator interpolator, boolean z2) {
        this.q = true;
        this.r = interpolator;
        this.v = context.getResources().getDisplayMetrics().density * 160.0f;
        this.u = a(ViewConfiguration.getScrollFriction());
        this.s = z2;
    }

    @DexIgnore
    public static float b(float f2) {
        float f3;
        float f4 = f2 * y;
        if (f4 < 1.0f) {
            f3 = f4 - (1.0f - ((float) Math.exp((double) (-f4))));
        } else {
            f3 = ((1.0f - ((float) Math.exp((double) (1.0f - f4)))) * 0.63212055f) + 0.36787945f;
        }
        return f3 * z;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.q = z2;
    }

    @DexIgnore
    public boolean a() {
        float f2;
        if (this.q) {
            return false;
        }
        int currentAnimationTimeMillis = (int) (AnimationUtils.currentAnimationTimeMillis() - this.l);
        int i2 = this.m;
        if (currentAnimationTimeMillis < i2) {
            int i3 = this.a;
            if (i3 == 0) {
                float f3 = ((float) currentAnimationTimeMillis) * this.n;
                Interpolator interpolator = this.r;
                if (interpolator == null) {
                    f2 = b(f3);
                } else {
                    f2 = interpolator.getInterpolation(f3);
                }
                this.j = this.b + Math.round(this.o * f2);
                this.k = this.c + Math.round(f2 * this.p);
            } else if (i3 == 1) {
                float f4 = ((float) currentAnimationTimeMillis) / ((float) i2);
                int i4 = (int) (f4 * 100.0f);
                float f5 = ((float) i4) / 100.0f;
                int i5 = i4 + 1;
                float[] fArr = x;
                float f6 = fArr[i4];
                float f7 = f6 + (((f4 - f5) / ((((float) i5) / 100.0f) - f5)) * (fArr[i5] - f6));
                int i6 = this.b;
                int round = i6 + Math.round(((float) (this.d - i6)) * f7);
                this.j = round;
                int min = Math.min(round, this.g);
                this.j = min;
                this.j = Math.max(min, this.f);
                int i7 = this.c;
                int round2 = i7 + Math.round(f7 * ((float) (this.e - i7)));
                this.k = round2;
                int min2 = Math.min(round2, this.i);
                this.k = min2;
                int max = Math.max(min2, this.h);
                this.k = max;
                if (this.j == this.d && max == this.e) {
                    this.q = true;
                }
            }
        } else {
            this.j = this.d;
            this.k = this.e;
            this.q = true;
        }
        return true;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5, int i6) {
        this.a = 0;
        this.q = false;
        this.m = i6;
        this.l = AnimationUtils.currentAnimationTimeMillis();
        this.b = i2;
        this.c = i3;
        this.d = i2 + i4;
        this.e = i3 + i5;
        this.o = (float) i4;
        this.p = (float) i5;
        this.n = 1.0f / ((float) this.m);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00a6  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(int r17, int r18, int r19, int r20, int r21, int r22, int r23, int r24) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            boolean r3 = r0.s
            if (r3 == 0) goto L_0x0053
            boolean r3 = r0.q
            if (r3 != 0) goto L_0x0053
            float r3 = r16.b()
            int r4 = r0.d
            int r5 = r0.b
            int r4 = r4 - r5
            float r4 = (float) r4
            int r5 = r0.e
            int r6 = r0.c
            int r5 = r5 - r6
            float r5 = (float) r5
            float r6 = r4 * r4
            float r7 = r5 * r5
            float r6 = r6 + r7
            double r6 = (double) r6
            double r6 = java.lang.Math.sqrt(r6)
            float r6 = (float) r6
            float r4 = r4 / r6
            float r5 = r5 / r6
            float r4 = r4 * r3
            float r5 = r5 * r3
            r3 = r19
            float r6 = (float) r3
            float r7 = java.lang.Math.signum(r6)
            float r8 = java.lang.Math.signum(r4)
            int r7 = (r7 > r8 ? 1 : (r7 == r8 ? 0 : -1))
            if (r7 != 0) goto L_0x0055
            r7 = r20
            float r8 = (float) r7
            float r9 = java.lang.Math.signum(r8)
            float r10 = java.lang.Math.signum(r5)
            int r9 = (r9 > r10 ? 1 : (r9 == r10 ? 0 : -1))
            if (r9 != 0) goto L_0x0057
            float r6 = r6 + r4
            int r3 = (int) r6
            float r8 = r8 + r5
            int r4 = (int) r8
            r7 = r4
            goto L_0x0057
        L_0x0053:
            r3 = r19
        L_0x0055:
            r7 = r20
        L_0x0057:
            r4 = 1
            r0.a = r4
            r4 = 0
            r0.q = r4
            int r4 = r3 * r3
            int r5 = r7 * r7
            int r4 = r4 + r5
            double r4 = (double) r4
            double r4 = java.lang.Math.sqrt(r4)
            float r4 = (float) r4
            r0.t = r4
            r5 = 1053609165(0x3ecccccd, float:0.4)
            float r5 = r5 * r4
            r6 = 1145569280(0x44480000, float:800.0)
            float r5 = r5 / r6
            double r5 = (double) r5
            double r5 = java.lang.Math.log(r5)
            r8 = 4652007308841189376(0x408f400000000000, double:1000.0)
            float r10 = com.fossil.ny6.w
            double r10 = (double) r10
            r12 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            double r10 = r10 - r12
            double r10 = r5 / r10
            double r10 = java.lang.Math.exp(r10)
            double r10 = r10 * r8
            int r8 = (int) r10
            r0.m = r8
            long r8 = android.view.animation.AnimationUtils.currentAnimationTimeMillis()
            r0.l = r8
            r0.b = r1
            r0.c = r2
            r8 = 0
            r9 = 1065353216(0x3f800000, float:1.0)
            int r8 = (r4 > r8 ? 1 : (r4 == r8 ? 0 : -1))
            if (r8 != 0) goto L_0x00a1
            r3 = 1065353216(0x3f800000, float:1.0)
            goto L_0x00a3
        L_0x00a1:
            float r3 = (float) r3
            float r3 = r3 / r4
        L_0x00a3:
            if (r8 != 0) goto L_0x00a6
            goto L_0x00a9
        L_0x00a6:
            float r7 = (float) r7
            float r9 = r7 / r4
        L_0x00a9:
            r7 = 4650248090236747776(0x4089000000000000, double:800.0)
            float r4 = com.fossil.ny6.w
            double r10 = (double) r4
            double r14 = (double) r4
            double r14 = r14 - r12
            double r10 = r10 / r14
            double r10 = r10 * r5
            double r4 = java.lang.Math.exp(r10)
            double r4 = r4 * r7
            int r4 = (int) r4
            r5 = r21
            r0.f = r5
            r5 = r22
            r0.g = r5
            r5 = r23
            r0.h = r5
            r5 = r24
            r0.i = r5
            float r4 = (float) r4
            float r3 = r3 * r4
            int r3 = java.lang.Math.round(r3)
            int r1 = r1 + r3
            r0.d = r1
            int r3 = r0.g
            int r1 = java.lang.Math.min(r1, r3)
            r0.d = r1
            int r3 = r0.f
            int r1 = java.lang.Math.max(r1, r3)
            r0.d = r1
            float r4 = r4 * r9
            int r1 = java.lang.Math.round(r4)
            int r1 = r1 + r2
            r0.e = r1
            int r2 = r0.i
            int r1 = java.lang.Math.min(r1, r2)
            r0.e = r1
            int r2 = r0.h
            int r1 = java.lang.Math.max(r1, r2)
            r0.e = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ny6.a(int, int, int, int, int, int, int, int):void");
    }
}
