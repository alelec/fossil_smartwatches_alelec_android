package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i6 {
    @DexIgnore
    public static /* final */ Class<?> a; // = a();
    @DexIgnore
    public static /* final */ Field b; // = b();
    @DexIgnore
    public static /* final */ Field c; // = c();
    @DexIgnore
    public static /* final */ Method d; // = b(a);
    @DexIgnore
    public static /* final */ Method e; // = a(a);
    @DexIgnore
    public static /* final */ Method f; // = c(a);
    @DexIgnore
    public static /* final */ Handler g; // = new Handler(Looper.getMainLooper());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ d a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public a(d dVar, Object obj) {
            this.a = dVar;
            this.b = obj;
        }

        @DexIgnore
        public void run() {
            this.a.a = this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Application a;
        @DexIgnore
        public /* final */ /* synthetic */ d b;

        @DexIgnore
        public b(Application application, d dVar) {
            this.a = application;
            this.b = dVar;
        }

        @DexIgnore
        public void run() {
            this.a.unregisterActivityLifecycleCallbacks(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public c(Object obj, Object obj2) {
            this.a = obj;
            this.b = obj2;
        }

        @DexIgnore
        public void run() {
            try {
                if (i6.d != null) {
                    i6.d.invoke(this.a, this.b, false, "AppCompat recreation");
                    return;
                }
                i6.e.invoke(this.a, this.b, false);
            } catch (RuntimeException e) {
                if (e.getClass() == RuntimeException.class && e.getMessage() != null && e.getMessage().startsWith("Unable to stop")) {
                    throw e;
                }
            } catch (Throwable th) {
                Log.e("ActivityRecreator", "Exception while invoking performStopActivity", th);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public Object a;
        @DexIgnore
        public Activity b;
        @DexIgnore
        public boolean c; // = false;
        @DexIgnore
        public boolean d; // = false;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public d(Activity activity) {
            this.b = activity;
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            if (this.b == activity) {
                this.b = null;
                this.d = true;
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            if (this.d && !this.e && !this.c && i6.a(this.a, activity)) {
                this.e = true;
                this.a = null;
            }
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            if (this.b == activity) {
                this.c = true;
            }
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
        }
    }

    @DexIgnore
    public static boolean a(Activity activity) {
        Object obj;
        if (Build.VERSION.SDK_INT >= 28) {
            activity.recreate();
            return true;
        } else if (d() && f == null) {
            return false;
        } else {
            if (e == null && d == null) {
                return false;
            }
            try {
                Object obj2 = c.get(activity);
                if (obj2 == null || (obj = b.get(activity)) == null) {
                    return false;
                }
                Application application = activity.getApplication();
                d dVar = new d(activity);
                application.registerActivityLifecycleCallbacks(dVar);
                g.post(new a(dVar, obj2));
                try {
                    if (d()) {
                        f.invoke(obj, obj2, null, null, 0, false, null, null, false, false);
                    } else {
                        activity.recreate();
                    }
                    return true;
                } finally {
                    g.post(new b(application, dVar));
                }
            } catch (Throwable unused) {
                return false;
            }
        }
    }

    @DexIgnore
    public static Method b(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE, String.class);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Method c(Class<?> cls) {
        if (d() && cls != null) {
            try {
                Method declaredMethod = cls.getDeclaredMethod("requestRelaunchActivity", IBinder.class, List.class, List.class, Integer.TYPE, Boolean.TYPE, Configuration.class, Configuration.class, Boolean.TYPE, Boolean.TYPE);
                declaredMethod.setAccessible(true);
                return declaredMethod;
            } catch (Throwable unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public static boolean d() {
        int i = Build.VERSION.SDK_INT;
        return i == 26 || i == 27;
    }

    @DexIgnore
    public static Field b() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mMainThread");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Field c() {
        try {
            Field declaredField = Activity.class.getDeclaredField("mToken");
            declaredField.setAccessible(true);
            return declaredField;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static boolean a(Object obj, Activity activity) {
        try {
            Object obj2 = c.get(activity);
            if (obj2 != obj) {
                return false;
            }
            g.postAtFrontOfQueue(new c(b.get(activity), obj2));
            return true;
        } catch (Throwable th) {
            Log.e("ActivityRecreator", "Exception while fetching field values", th);
            return false;
        }
    }

    @DexIgnore
    public static Method a(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        try {
            Method declaredMethod = cls.getDeclaredMethod("performStopActivity", IBinder.class, Boolean.TYPE);
            declaredMethod.setAccessible(true);
            return declaredMethod;
        } catch (Throwable unused) {
            return null;
        }
    }

    @DexIgnore
    public static Class<?> a() {
        try {
            return Class.forName("android.app.ActivityThread");
        } catch (Throwable unused) {
            return null;
        }
    }
}
