package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum fe1 {
    REQUEST_DISCOVER_SERVICE(new byte[]{1}),
    CLEAN_UP_DEVICE(new byte[]{35, 0, 1, 0, 0, 0});
    
    @DexIgnore
    public /* final */ byte[] a;

    @DexIgnore
    public fe1(byte[] bArr) {
        this.a = bArr;
    }
}
