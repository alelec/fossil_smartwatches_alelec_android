package com.fossil;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gs2<T> implements Iterator<T> {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ /* synthetic */ ds2 d;

    @DexIgnore
    public gs2(ds2 ds2) {
        this.d = ds2;
        this.a = this.d.b;
        this.b = this.d.zzd();
        this.c = -1;
    }

    @DexIgnore
    public abstract T a(int i);

    @DexIgnore
    public boolean hasNext() {
        return this.b >= 0;
    }

    @DexIgnore
    @Override // java.util.Iterator
    public T next() {
        zza();
        if (hasNext()) {
            int i = this.b;
            this.c = i;
            T a2 = a(i);
            this.b = this.d.zza(this.b);
            return a2;
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public void remove() {
        zza();
        or2.b(this.c >= 0, "no calls to next() since the last call to remove()");
        this.a += 32;
        ds2 ds2 = this.d;
        ds2.remove(ds2.zzb[this.c]);
        this.b = ds2.zzb(this.b, this.c);
        this.c = -1;
    }

    @DexIgnore
    public final void zza() {
        if (this.d.b != this.a) {
            throw new ConcurrentModificationException();
        }
    }

    @DexIgnore
    public /* synthetic */ gs2(ds2 ds2, cs2 cs2) {
        this(ds2);
    }
}
