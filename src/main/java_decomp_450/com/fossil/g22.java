package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g22 {
    @DexIgnore
    public static void a(Status status, oo3<Void> oo3) {
        a(status, null, oo3);
    }

    @DexIgnore
    public static <TResult> void a(Status status, TResult tresult, oo3<TResult> oo3) {
        if (status.y()) {
            oo3.a((Object) tresult);
        } else {
            oo3.a((Exception) new w02(status));
        }
    }

    @DexIgnore
    @Deprecated
    public static no3<Void> a(no3<Boolean> no3) {
        return no3.a(new k42());
    }
}
