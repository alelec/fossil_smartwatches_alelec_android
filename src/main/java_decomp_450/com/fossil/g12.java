package com.fossil;

import android.app.Activity;
import android.content.IntentSender;
import android.util.Log;
import com.fossil.i12;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g12<R extends i12> extends k12<R> {
    @DexIgnore
    public /* final */ Activity a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public g12(Activity activity, int i) {
        a72.a(activity, "Activity must not be null");
        this.a = activity;
        this.b = i;
    }

    @DexIgnore
    @Override // com.fossil.k12
    public final void a(Status status) {
        if (status.w()) {
            try {
                status.a(this.a, this.b);
            } catch (IntentSender.SendIntentException e) {
                Log.e("ResolvingResultCallback", "Failed to start resolution", e);
                b(new Status(8));
            }
        } else {
            b(status);
        }
    }

    @DexIgnore
    public abstract void b(Status status);
}
