package com.fossil;

import android.database.Cursor;
import com.fossil.blesdk.database.SdkDatabase;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l31 {
    @DexIgnore
    public static /* final */ l31 a; // = new l31();

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final List<bi1> a(String str, byte b) {
        JSONArray jSONArray;
        rc1 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        ArrayList arrayList = null;
        if (!(a3 == null || (a2 = a3.a()) == null)) {
            fi b2 = fi.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ?", 2);
            if (str == null) {
                b2.bindNull(1);
            } else {
                b2.bindString(1, str);
            }
            b2.bindLong(2, (long) b);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = pi.a(a2.a, b2, false, null);
            try {
                int b3 = oi.b(a4, "id");
                int b4 = oi.b(a4, "deviceMacAddress");
                int b5 = oi.b(a4, "fileType");
                int b6 = oi.b(a4, "fileIndex");
                int b7 = oi.b(a4, "rawData");
                int b8 = oi.b(a4, "fileLength");
                int b9 = oi.b(a4, "fileCrc");
                int b10 = oi.b(a4, "createdTimeStamp");
                int b11 = oi.b(a4, "isCompleted");
                ArrayList arrayList2 = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    bi1 bi1 = new bi1(a4.getString(b4), (byte) a4.getShort(b5), (byte) a4.getShort(b6), a4.getBlob(b7), a4.getLong(b8), a4.getLong(b9), a4.getLong(b10), a4.getInt(b11) != 0);
                    bi1.a = a4.getInt(b3);
                    arrayList2.add(bi1);
                }
                a4.close();
                b2.c();
                arrayList = arrayList2;
            } catch (Throwable th) {
                a4.close();
                b2.c();
                throw th;
            }
        }
        JSONObject a5 = yz0.a(new a81(b, (byte) 255).a(), r51.L2, JSONObject.NULL);
        r51 r51 = r51.N2;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new bi1[0]);
            if (array != null) {
                jSONArray = yz0.a((bi1[]) array);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        yz0.a(a5, r51, jSONArray);
        s11 s11 = s11.QUERY;
        t11 t11 = t11.a;
        if (arrayList != null) {
            arrayList.size();
        }
        return arrayList != null ? arrayList : w97.a();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final List<bi1> b(String str, byte b) {
        JSONArray jSONArray;
        rc1 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        ArrayList arrayList = null;
        if (!(a3 == null || (a2 = a3.a()) == null)) {
            fi b2 = fi.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ? and isCompleted = 0", 2);
            if (str == null) {
                b2.bindNull(1);
            } else {
                b2.bindString(1, str);
            }
            b2.bindLong(2, (long) b);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = pi.a(a2.a, b2, false, null);
            try {
                int b3 = oi.b(a4, "id");
                int b4 = oi.b(a4, "deviceMacAddress");
                int b5 = oi.b(a4, "fileType");
                int b6 = oi.b(a4, "fileIndex");
                int b7 = oi.b(a4, "rawData");
                int b8 = oi.b(a4, "fileLength");
                int b9 = oi.b(a4, "fileCrc");
                int b10 = oi.b(a4, "createdTimeStamp");
                int b11 = oi.b(a4, "isCompleted");
                ArrayList arrayList2 = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    bi1 bi1 = new bi1(a4.getString(b4), (byte) a4.getShort(b5), (byte) a4.getShort(b6), a4.getBlob(b7), a4.getLong(b8), a4.getLong(b9), a4.getLong(b10), a4.getInt(b11) != 0);
                    bi1.a = a4.getInt(b3);
                    arrayList2.add(bi1);
                }
                a4.close();
                b2.c();
                arrayList = arrayList2;
            } catch (Throwable th) {
                a4.close();
                b2.c();
                throw th;
            }
        }
        JSONObject a5 = yz0.a(new a81(b, (byte) 255).a(), r51.L2, (Object) false);
        r51 r51 = r51.N2;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new bi1[0]);
            if (array != null) {
                jSONArray = yz0.a((bi1[]) array);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        yz0.a(a5, r51, jSONArray);
        s11 s11 = s11.QUERY;
        t11 t11 = t11.a;
        if (arrayList != null) {
            arrayList.size();
        }
        return arrayList != null ? arrayList : w97.a();
    }

    @DexIgnore
    public final long a(bi1 bi1) {
        rc1 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        Long valueOf = (a3 == null || (a2 = a3.a()) == null) ? null : Long.valueOf(a2.a(bi1));
        of7 a4 = qf7.a(Long.MIN_VALUE, 0);
        if (valueOf != null) {
            a4.a(valueOf.longValue());
        }
        yz0.a(yz0.a(new JSONObject(), r51.u4, bi1.a(true)), r51.v4, valueOf != null ? valueOf : -1);
        s11 s11 = s11.INSERT;
        String str = bi1.b;
        t11 t11 = t11.a;
        byte b = bi1.c;
        byte b2 = bi1.d;
        long j = bi1.g;
        int length = bi1.e.length;
        long j2 = bi1.f;
        boolean z = bi1.i;
        if (valueOf != null) {
            return valueOf.longValue();
        }
        return -1;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final List<bi1> b(String str, byte b, byte b2) {
        JSONArray jSONArray;
        rc1 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        ArrayList arrayList = null;
        if (!(a3 == null || (a2 = a3.a()) == null)) {
            fi b3 = fi.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0", 3);
            if (str == null) {
                b3.bindNull(1);
            } else {
                b3.bindString(1, str);
            }
            b3.bindLong(2, (long) b);
            b3.bindLong(3, (long) b2);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = pi.a(a2.a, b3, false, null);
            try {
                int b4 = oi.b(a4, "id");
                int b5 = oi.b(a4, "deviceMacAddress");
                int b6 = oi.b(a4, "fileType");
                int b7 = oi.b(a4, "fileIndex");
                int b8 = oi.b(a4, "rawData");
                int b9 = oi.b(a4, "fileLength");
                int b10 = oi.b(a4, "fileCrc");
                int b11 = oi.b(a4, "createdTimeStamp");
                int b12 = oi.b(a4, "isCompleted");
                ArrayList arrayList2 = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    bi1 bi1 = new bi1(a4.getString(b5), (byte) a4.getShort(b6), (byte) a4.getShort(b7), a4.getBlob(b8), a4.getLong(b9), a4.getLong(b10), a4.getLong(b11), a4.getInt(b12) != 0);
                    bi1.a = a4.getInt(b4);
                    arrayList2.add(bi1);
                    b5 = b5;
                }
                a4.close();
                b3.c();
                arrayList = arrayList2;
            } catch (Throwable th) {
                a4.close();
                b3.c();
                throw th;
            }
        }
        JSONObject a5 = yz0.a(new a81(b, b2).a(), r51.L2, (Object) false);
        r51 r51 = r51.N2;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new bi1[0]);
            if (array != null) {
                jSONArray = yz0.a((bi1[]) array);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        yz0.a(a5, r51, jSONArray);
        s11 s11 = s11.QUERY;
        t11 t11 = t11.a;
        if (arrayList != null) {
            arrayList.size();
        }
        return arrayList != null ? arrayList : w97.a();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int a(String str, byte b, byte b2) {
        Integer num;
        rc1 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        if (a3 == null || (a2 = a3.a()) == null) {
            num = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            aj acquire = a2.c.acquire();
            if (str == null) {
                acquire.bindNull(1);
            } else {
                acquire.bindString(1, str);
            }
            acquire.bindLong(2, (long) b);
            acquire.bindLong(3, (long) b2);
            a2.a.beginTransaction();
            try {
                int executeUpdateDelete = acquire.executeUpdateDelete();
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                a2.c.release(acquire);
                num = Integer.valueOf(executeUpdateDelete);
            } catch (Throwable th) {
                a2.a.endTransaction();
                a2.c.release(acquire);
                throw th;
            }
        }
        yz0.a(yz0.a(new a81(b, b2).a(), r51.L2, (Object) false), r51.w4, num != null ? num : -1);
        s11 s11 = s11.DELETE;
        t11 t11 = t11.a;
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }
}
