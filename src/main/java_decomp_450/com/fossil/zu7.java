package com.fossil;

import com.fossil.qn7;
import java.io.IOException;
import okhttp3.Response;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zu7<T> implements Call<T> {
    @DexIgnore
    public /* final */ ev7 a;
    @DexIgnore
    public /* final */ Object[] b;
    @DexIgnore
    public /* final */ qn7.a c;
    @DexIgnore
    public /* final */ tu7<mo7, T> d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public qn7 f;
    @DexIgnore
    public Throwable g;
    @DexIgnore
    public boolean h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements rn7 {
        @DexIgnore
        public /* final */ /* synthetic */ ru7 a;

        @DexIgnore
        public a(ru7 ru7) {
            this.a = ru7;
        }

        @DexIgnore
        public final void a(Throwable th) {
            try {
                this.a.onFailure(zu7.this, th);
            } catch (Throwable th2) {
                jv7.a(th2);
                th2.printStackTrace();
            }
        }

        @DexIgnore
        @Override // com.fossil.rn7
        public void onFailure(qn7 qn7, IOException iOException) {
            a(iOException);
        }

        @DexIgnore
        @Override // com.fossil.rn7
        public void onResponse(qn7 qn7, Response response) {
            try {
                try {
                    this.a.onResponse(zu7.this, zu7.this.a(response));
                } catch (Throwable th) {
                    jv7.a(th);
                    th.printStackTrace();
                }
            } catch (Throwable th2) {
                jv7.a(th2);
                a(th2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends mo7 {
        @DexIgnore
        public /* final */ mo7 a;
        @DexIgnore
        public /* final */ ar7 b;
        @DexIgnore
        public IOException c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends dr7 {
            @DexIgnore
            public a(sr7 sr7) {
                super(sr7);
            }

            @DexIgnore
            @Override // com.fossil.sr7, com.fossil.dr7
            public long b(yq7 yq7, long j) throws IOException {
                try {
                    return super.b(yq7, j);
                } catch (IOException e) {
                    b.this.c = e;
                    throw e;
                }
            }
        }

        @DexIgnore
        public b(mo7 mo7) {
            this.a = mo7;
            this.b = ir7.a(new a(mo7.source()));
        }

        @DexIgnore
        public void a() throws IOException {
            IOException iOException = this.c;
            if (iOException != null) {
                throw iOException;
            }
        }

        @DexIgnore
        @Override // java.io.Closeable, com.fossil.mo7, java.lang.AutoCloseable
        public void close() {
            this.a.close();
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public long contentLength() {
            return this.a.contentLength();
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ho7 contentType() {
            return this.a.contentType();
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ar7 source() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends mo7 {
        @DexIgnore
        public /* final */ ho7 a;
        @DexIgnore
        public /* final */ long b;

        @DexIgnore
        public c(ho7 ho7, long j) {
            this.a = ho7;
            this.b = j;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public long contentLength() {
            return this.b;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ho7 contentType() {
            return this.a;
        }

        @DexIgnore
        @Override // com.fossil.mo7
        public ar7 source() {
            throw new IllegalStateException("Cannot read raw response body of a converted body.");
        }
    }

    @DexIgnore
    public zu7(ev7 ev7, Object[] objArr, qn7.a aVar, tu7<mo7, T> tu7) {
        this.a = ev7;
        this.b = objArr;
        this.c = aVar;
        this.d = tu7;
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void a(ru7<T> ru7) {
        qn7 qn7;
        Throwable th;
        jv7.a(ru7, "callback == null");
        synchronized (this) {
            if (!this.h) {
                this.h = true;
                qn7 = this.f;
                th = this.g;
                if (qn7 == null && th == null) {
                    try {
                        qn7 d2 = d();
                        this.f = d2;
                        qn7 = d2;
                    } catch (Throwable th2) {
                        th = th2;
                        jv7.a(th);
                        this.g = th;
                    }
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (th != null) {
            ru7.onFailure(this, th);
            return;
        }
        if (this.e) {
            qn7.cancel();
        }
        qn7.a(new a(ru7));
    }

    @DexIgnore
    @Override // retrofit2.Call
    public synchronized lo7 c() {
        qn7 qn7 = this.f;
        if (qn7 != null) {
            return qn7.c();
        } else if (this.g == null) {
            try {
                qn7 d2 = d();
                this.f = d2;
                return d2.c();
            } catch (Error | RuntimeException e2) {
                jv7.a(e2);
                this.g = e2;
                throw e2;
            } catch (IOException e3) {
                this.g = e3;
                throw new RuntimeException("Unable to create request.", e3);
            }
        } else if (this.g instanceof IOException) {
            throw new RuntimeException("Unable to create request.", this.g);
        } else if (this.g instanceof RuntimeException) {
            throw ((RuntimeException) this.g);
        } else {
            throw ((Error) this.g);
        }
    }

    @DexIgnore
    @Override // retrofit2.Call
    public void cancel() {
        qn7 qn7;
        this.e = true;
        synchronized (this) {
            qn7 = this.f;
        }
        if (qn7 != null) {
            qn7.cancel();
        }
    }

    @DexIgnore
    public final qn7 d() throws IOException {
        qn7 a2 = this.c.a(this.a.a(this.b));
        if (a2 != null) {
            return a2;
        }
        throw new NullPointerException("Call.Factory returned null.");
    }

    @DexIgnore
    @Override // retrofit2.Call
    public boolean e() {
        boolean z = true;
        if (this.e) {
            return true;
        }
        synchronized (this) {
            if (this.f == null || !this.f.e()) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    @Override // java.lang.Object, retrofit2.Call
    public zu7<T> clone() {
        return new zu7<>(this.a, this.b, this.c, this.d);
    }

    @DexIgnore
    @Override // retrofit2.Call
    public fv7<T> a() throws IOException {
        qn7 qn7;
        synchronized (this) {
            if (!this.h) {
                this.h = true;
                if (this.g == null) {
                    qn7 = this.f;
                    if (qn7 == null) {
                        try {
                            qn7 = d();
                            this.f = qn7;
                        } catch (IOException | Error | RuntimeException e2) {
                            jv7.a(e2);
                            this.g = e2;
                            throw e2;
                        }
                    }
                } else if (this.g instanceof IOException) {
                    throw ((IOException) this.g);
                } else if (this.g instanceof RuntimeException) {
                    throw ((RuntimeException) this.g);
                } else {
                    throw ((Error) this.g);
                }
            } else {
                throw new IllegalStateException("Already executed.");
            }
        }
        if (this.e) {
            qn7.cancel();
        }
        return a(qn7.a());
    }

    @DexIgnore
    public fv7<T> a(Response response) throws IOException {
        mo7 a2 = response.a();
        Response.a q = response.q();
        q.a(new c(a2.contentType(), a2.contentLength()));
        Response a3 = q.a();
        int e2 = a3.e();
        if (e2 < 200 || e2 >= 300) {
            try {
                return fv7.a(jv7.a(a2), a3);
            } finally {
                a2.close();
            }
        } else if (e2 == 204 || e2 == 205) {
            a2.close();
            return fv7.a((Object) null, a3);
        } else {
            b bVar = new b(a2);
            try {
                return fv7.a((Object) this.d.a(bVar), a3);
            } catch (RuntimeException e3) {
                bVar.a();
                throw e3;
            }
        }
    }
}
