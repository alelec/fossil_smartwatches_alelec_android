package com.fossil;

import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr0 {
    @DexIgnore
    public static /* final */ HashMap<String, up0> a; // = new HashMap<>();
    @DexIgnore
    public static /* final */ qr0 b; // = new qr0();

    @DexIgnore
    public final long a(String str) {
        long j;
        synchronized (a) {
            up0 up0 = a.get(str);
            if (up0 == null) {
                up0 = new up0(0, 128);
            }
            long min = Math.min(128L, af7.b(Math.pow((double) 2, (double) (up0.a + 1))));
            if (min < up0.b) {
                up0.a++;
            }
            a.put(str, up0);
            t11 t11 = t11.a;
            j = min * 1000;
        }
        return j;
    }

    @DexIgnore
    public final void b(String str) {
        synchronized (a) {
            up0 up0 = a.get(str);
            if (up0 == null) {
                up0 = new up0(0, 128);
            }
            a.put(str, up0.a(0, up0.b));
            i97 i97 = i97.a;
        }
    }
}
