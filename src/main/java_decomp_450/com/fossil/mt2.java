package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mt2<E> extends os2<E> {
    @DexIgnore
    public static /* final */ os2<Object> zza; // = new mt2(new Object[0], 0);
    @DexIgnore
    public /* final */ transient Object[] c;
    @DexIgnore
    public /* final */ transient int d;

    @DexIgnore
    public mt2(Object[] objArr, int i) {
        this.c = objArr;
        this.d = i;
    }

    @DexIgnore
    @Override // java.util.List
    public final E get(int i) {
        or2.a(i, this.d);
        return (E) this.c[i];
    }

    @DexIgnore
    public final int size() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.os2, com.fossil.ps2
    public final int zzb(Object[] objArr, int i) {
        System.arraycopy(this.c, 0, objArr, i, this.d);
        return i + this.d;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final Object[] zze() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzf() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final int zzg() {
        return this.d;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return false;
    }
}
