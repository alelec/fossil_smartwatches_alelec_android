package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class aw4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ RecyclerView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;

    @DexIgnore
    public aw4(Object obj, View view, int i, RTLImageView rTLImageView, ConstraintLayout constraintLayout, RecyclerView recyclerView, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = constraintLayout;
        this.s = recyclerView;
        this.t = flexibleTextView;
    }
}
