package com.fossil;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.customview.CircleProgressView;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.InterceptSwipe;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms4 extends go5 {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ a r; // = new a(null);
    @DexIgnore
    public /* final */ k f; // = new k(this);
    @DexIgnore
    public rj4 g;
    @DexIgnore
    public qw6<cz4> h;
    @DexIgnore
    public os4 i;
    @DexIgnore
    public /* final */ TimerViewObserver j; // = new TimerViewObserver();
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ms4.q;
        }

        @DexIgnore
        public final ms4 b() {
            return new ms4();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ms4 ms4) {
            super(1);
            this.this$0 = ms4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            ms4.b(this.this$0).n();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements InterceptSwipe.j {
        @DexIgnore
        public /* final */ /* synthetic */ cz4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ms4 b;

        @DexIgnore
        public c(cz4 cz4, ms4 ms4) {
            this.a = cz4;
            this.b = ms4;
        }

        @DexIgnore
        @Override // com.portfolio.platform.uirenew.customview.InterceptSwipe.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.w;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            FlexibleTextView flexibleTextView2 = this.a.v;
            ee7.a((Object) flexibleTextView2, "ftvEmpty");
            flexibleTextView2.setVisibility(8);
            ms4.b(this.b).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public d(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            cz4 cz4 = (cz4) ms4.a(this.a).a();
            if (cz4 != null) {
                InterceptSwipe interceptSwipe = cz4.B;
                ee7.a((Object) interceptSwipe, "swipeRefresh");
                ee7.a((Object) bool, "it");
                interceptSwipe.setRefreshing(bool.booleanValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public e(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            ms4 ms4 = this.a;
            ee7.a((Object) bool, "it");
            ms4.T(bool.booleanValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public f(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mn4 mn4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ms4.r.a();
            local.e(a2, "challengeLive - challenge: " + mn4 + " - " + System.currentTimeMillis());
            if (mn4 != null) {
                this.a.d(mn4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends Boolean, ? extends mn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public g(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, mn4> r87) {
            if (r87.getFirst().booleanValue()) {
                this.a.c(r87.getSecond());
            } else {
                this.a.b(r87.getSecond());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<v87<? extends hn4, ? extends String, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public h(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<hn4, String, Integer> v87) {
            ms4 ms4 = this.a;
            ee7.a((Object) v87, "triple");
            ms4.a(v87);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public i(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            cz4 cz4 = (cz4) ms4.a(this.a).a();
            if (cz4 == null) {
                return;
            }
            if (r87.getFirst().booleanValue()) {
                FlexibleTextView flexibleTextView = cz4.w;
                ee7.a((Object) flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(0);
                return;
            }
            FlexibleTextView flexibleTextView2 = cz4.w;
            ee7.a((Object) flexibleTextView2, "ftvError");
            flexibleTextView2.setVisibility(8);
            FlexibleTextView flexibleTextView3 = cz4.w;
            ee7.a((Object) flexibleTextView3, "ftvError");
            String a2 = ig5.a(flexibleTextView3.getContext(), 2131886227);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                Toast.makeText(activity, a2, 1).show();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        public j(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(Boolean bool) {
            FlexibleTextView flexibleTextView;
            cz4 cz4 = (cz4) ms4.a(this.a).a();
            if (cz4 != null && (flexibleTextView = cz4.z) != null) {
                int i = 0;
                if (!ee7.a((Object) bool, (Object) false)) {
                    i = 8;
                }
                flexibleTextView.setVisibility(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ ms4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public k(ms4 ms4) {
            this.a = ms4;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            boolean z = true;
            Boolean valueOf = intent != null ? Boolean.valueOf(intent.getBooleanExtra("set_sync_data_extra", true)) : null;
            os4 b = ms4.b(this.a);
            if (valueOf != null) {
                z = valueOf.booleanValue();
            }
            b.a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ cz4 a;

        @DexIgnore
        public l(cz4 cz4) {
            this.a = cz4;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            TimerTextView timerTextView = this.a.t;
            ee7.a((Object) timerTextView, "ftvCountDown");
            ee7.a((Object) valueAnimator, "it");
            timerTextView.setText(valueAnimator.getAnimatedValue().toString());
        }
    }

    /*
    static {
        String simpleName = ms4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCCurrentChallengeSubTab\u2026nt::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ qw6 a(ms4 ms4) {
        qw6<cz4> qw6 = ms4.h;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ os4 b(ms4 ms4) {
        os4 os4 = ms4.i;
        if (os4 != null) {
            return os4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void T(boolean z) {
        qw6<cz4> qw6 = this.h;
        if (qw6 != null) {
            cz4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                if (z) {
                    qe5.c.b();
                    FlexibleTextView flexibleTextView2 = a2.v;
                    ee7.a((Object) flexibleTextView2, "ftvEmpty");
                    flexibleTextView2.setVisibility(0);
                    TimerTextView timerTextView = a2.r;
                    ee7.a((Object) timerTextView, "ftvChallengeInfo");
                    timerTextView.setVisibility(8);
                    FlexibleTextView flexibleTextView3 = a2.s;
                    ee7.a((Object) flexibleTextView3, "ftvChallengeName");
                    flexibleTextView3.setVisibility(8);
                    FlexibleTextView flexibleTextView4 = a2.y;
                    ee7.a((Object) flexibleTextView4, "ftvRanking");
                    flexibleTextView4.setVisibility(8);
                    TimerTextView timerTextView2 = a2.t;
                    ee7.a((Object) timerTextView2, "ftvCountDown");
                    timerTextView2.setVisibility(8);
                    CircleProgressView circleProgressView = a2.q;
                    ee7.a((Object) circleProgressView, "circleProgress");
                    circleProgressView.setVisibility(8);
                    FlexibleTextView flexibleTextView5 = a2.x;
                    ee7.a((Object) flexibleTextView5, "ftvPlayerInfo");
                    flexibleTextView5.setVisibility(8);
                    FlexibleTextView flexibleTextView6 = a2.u;
                    ee7.a((Object) flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setVisibility(8);
                    FlexibleTextView flexibleTextView7 = a2.z;
                    ee7.a((Object) flexibleTextView7, "ftvSetSyncData");
                    flexibleTextView7.setVisibility(8);
                    TimerTextView timerTextView3 = a2.t;
                    ee7.a((Object) timerTextView3, "ftvCountDown");
                    timerTextView3.setTag(null);
                    a2.t.f();
                    a2.r.f();
                    TimerTextView timerTextView4 = a2.r;
                    ee7.a((Object) timerTextView4, "ftvChallengeInfo");
                    timerTextView4.setText("");
                    a2.q.b();
                    return;
                }
                FlexibleTextView flexibleTextView8 = a2.v;
                ee7.a((Object) flexibleTextView8, "ftvEmpty");
                flexibleTextView8.setVisibility(8);
                TimerTextView timerTextView5 = a2.r;
                ee7.a((Object) timerTextView5, "ftvChallengeInfo");
                timerTextView5.setVisibility(0);
                FlexibleTextView flexibleTextView9 = a2.s;
                ee7.a((Object) flexibleTextView9, "ftvChallengeName");
                flexibleTextView9.setVisibility(0);
                FlexibleTextView flexibleTextView10 = a2.y;
                ee7.a((Object) flexibleTextView10, "ftvRanking");
                flexibleTextView10.setVisibility(0);
                TimerTextView timerTextView6 = a2.t;
                ee7.a((Object) timerTextView6, "ftvCountDown");
                timerTextView6.setVisibility(0);
                CircleProgressView circleProgressView2 = a2.q;
                ee7.a((Object) circleProgressView2, "circleProgress");
                circleProgressView2.setVisibility(0);
                FlexibleTextView flexibleTextView11 = a2.x;
                ee7.a((Object) flexibleTextView11, "ftvPlayerInfo");
                flexibleTextView11.setVisibility(0);
                FlexibleTextView flexibleTextView12 = a2.u;
                ee7.a((Object) flexibleTextView12, "ftvCountPlayers");
                flexibleTextView12.setVisibility(0);
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void d(mn4 mn4) {
        String str;
        String str2;
        qw6<cz4> qw6 = this.h;
        if (qw6 != null) {
            cz4 a2 = qw6.a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "ftvError");
                flexibleTextView.setVisibility(8);
                FlexibleTextView flexibleTextView2 = a2.s;
                ee7.a((Object) flexibleTextView2, "ftvChallengeName");
                flexibleTextView2.setText(mn4.g());
                Integer h2 = mn4.h();
                int intValue = h2 != null ? h2.intValue() : 1;
                FlexibleTextView flexibleTextView3 = a2.u;
                ee7.a((Object) flexibleTextView3, "ftvCountPlayers");
                Object tag = flexibleTextView3.getTag();
                if (!(tag instanceof Integer)) {
                    tag = null;
                }
                Integer num = (Integer) tag;
                int intValue2 = num != null ? num.intValue() : 0;
                if (ee7.a((Object) mn4.p(), (Object) "pending-invitation")) {
                    FlexibleTextView flexibleTextView4 = a2.u;
                    ee7.a((Object) flexibleTextView4, "ftvCountPlayers");
                    flexibleTextView4.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView5 = a2.u;
                    ee7.a((Object) flexibleTextView5, "ftvCountPlayers");
                    flexibleTextView5.setText(String.valueOf(intValue));
                } else if (intValue > intValue2) {
                    FlexibleTextView flexibleTextView6 = a2.u;
                    ee7.a((Object) flexibleTextView6, "ftvCountPlayers");
                    flexibleTextView6.setTag(Integer.valueOf(intValue));
                    FlexibleTextView flexibleTextView7 = a2.u;
                    ee7.a((Object) flexibleTextView7, "ftvCountPlayers");
                    flexibleTextView7.setText(String.valueOf(intValue));
                }
                String j2 = mn4.j();
                if (j2 != null && j2.hashCode() == -314497661 && j2.equals("private")) {
                    a2.u.setCompoundDrawablesWithIntrinsicBounds(2131231024, 0, 0, 0);
                } else {
                    a2.u.setCompoundDrawablesWithIntrinsicBounds(2131231025, 0, 0, 0);
                }
                String u = mn4.u();
                if (u != null) {
                    int hashCode = u.hashCode();
                    if (hashCode != -1348781656) {
                        if (hashCode == -637042289 && u.equals("activity_reach_goal")) {
                            TimerTextView timerTextView = a2.t;
                            ee7.a((Object) timerTextView, "ftvCountDown");
                            timerTextView.setTextColor(v6.a(timerTextView.getContext(), 2131099689));
                            a2.r.setHighlightColour(2131099689);
                        }
                    } else if (u.equals("activity_best_result")) {
                        TimerTextView timerTextView2 = a2.t;
                        ee7.a((Object) timerTextView2, "ftvCountDown");
                        xe5 xe5 = xe5.b;
                        Integer d2 = mn4.d();
                        timerTextView2.setText(xe5.h(d2 != null ? d2.intValue() : 0));
                        TimerTextView timerTextView3 = a2.t;
                        ee7.a((Object) timerTextView3, "ftvCountDown");
                        timerTextView3.setTextColor(v6.a(timerTextView3.getContext(), 2131099677));
                        a2.r.setHighlightColour(2131099677);
                    }
                }
                String p2 = mn4.p();
                if (p2 != null) {
                    int hashCode2 = p2.hashCode();
                    if (hashCode2 != 1116313165) {
                        if (hashCode2 == 1550783935 && p2.equals("running")) {
                            a2.r.f();
                            String u2 = mn4.u();
                            if (u2 != null) {
                                int hashCode3 = u2.hashCode();
                                if (hashCode3 != -1348781656) {
                                    if (hashCode3 == -637042289 && u2.equals("activity_reach_goal")) {
                                        TimerTextView timerTextView4 = a2.t;
                                        ee7.a((Object) timerTextView4, "ftvCountDown");
                                        Object tag2 = timerTextView4.getTag();
                                        if (!(tag2 instanceof Integer)) {
                                            tag2 = null;
                                        }
                                        if (((Integer) tag2) == null) {
                                            TimerTextView timerTextView5 = a2.t;
                                            ee7.a((Object) timerTextView5, "ftvCountDown");
                                            Integer t = mn4.t();
                                            timerTextView5.setText(t != null ? String.valueOf(t.intValue()) : null);
                                            a2.q.f(1, 1);
                                        }
                                        CircleProgressView circleProgressView = a2.q;
                                        ee7.a((Object) circleProgressView, "circleProgress");
                                        circleProgressView.setProgressColour(v6.a(circleProgressView.getContext(), 2131099689));
                                    }
                                } else if (u2.equals("activity_best_result")) {
                                    TimerTextView timerTextView6 = a2.t;
                                    Date e2 = mn4.e();
                                    timerTextView6.setTime(e2 != null ? e2.getTime() : 0);
                                    a2.t.setDisplayType(ut4.HOUR_MIN_SEC);
                                    a2.t.setObserver(this.j);
                                    CircleProgressView circleProgressView2 = a2.q;
                                    ee7.a((Object) circleProgressView2, "circleProgress");
                                    circleProgressView2.setProgressColour(v6.a(circleProgressView2.getContext(), 2131099677));
                                    a2.q.f(1, 1);
                                    Integer d3 = mn4.d();
                                    int intValue3 = d3 != null ? d3.intValue() : 0;
                                    CircleProgressView circleProgressView3 = a2.q;
                                    long j3 = ((long) intValue3) * 1000;
                                    Date e3 = mn4.e();
                                    circleProgressView3.d(j3, e3 != null ? e3.getTime() : 0);
                                    CircleProgressView circleProgressView4 = a2.q;
                                    circleProgressView4.a(this.j, circleProgressView4.getIdentity());
                                }
                            }
                            TimerTextView timerTextView7 = a2.t;
                            ee7.a((Object) timerTextView7, "ftvCountDown");
                            Object tag3 = timerTextView7.getTag();
                            if (!(tag3 instanceof Integer)) {
                                tag3 = null;
                            }
                            if (((Integer) tag3) == null) {
                                FlexibleTextView flexibleTextView8 = a2.x;
                                ee7.a((Object) flexibleTextView8, "ftvPlayerInfo");
                                flexibleTextView8.setText("--");
                                TimerTextView timerTextView8 = a2.r;
                                ee7.a((Object) timerTextView8, "ftvChallengeInfo");
                                timerTextView8.setText("--");
                            }
                        }
                    } else if (p2.equals("waiting")) {
                        TimerTextView timerTextView9 = a2.t;
                        ee7.a((Object) timerTextView9, "ftvCountDown");
                        timerTextView9.setTag(null);
                        String w = PortfolioApp.g0.c().w();
                        eo4 i2 = mn4.i();
                        if (ee7.a((Object) w, (Object) (i2 != null ? i2.b() : null))) {
                            str = ig5.a(PortfolioApp.g0.c(), 2131886246);
                        } else {
                            fu4 fu4 = fu4.a;
                            eo4 i3 = mn4.i();
                            String a3 = i3 != null ? i3.a() : null;
                            eo4 i4 = mn4.i();
                            String c2 = i4 != null ? i4.c() : null;
                            eo4 i5 = mn4.i();
                            if (i5 == null || (str2 = i5.d()) == null) {
                                str2 = ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR;
                            }
                            str = fu4.a(a3, c2, str2);
                        }
                        FlexibleTextView flexibleTextView9 = a2.x;
                        ee7.a((Object) flexibleTextView9, "ftvPlayerInfo");
                        xe5 xe52 = xe5.b;
                        ee7.a((Object) str, "owner");
                        we7 we7 = we7.a;
                        FlexibleTextView flexibleTextView10 = a2.y;
                        ee7.a((Object) flexibleTextView10, "ftvRanking");
                        String a4 = ig5.a(flexibleTextView10.getContext(), 2131886251);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026ding_Text__CreatedByName)");
                        String format = String.format(a4, Arrays.copyOf(new Object[]{str}, 1));
                        ee7.a((Object) format, "java.lang.String.format(format, *args)");
                        flexibleTextView9.setText(xe5.a(xe52, str, format, 0, 4, null));
                        FlexibleTextView flexibleTextView11 = a2.y;
                        ee7.a((Object) flexibleTextView11, "ftvRanking");
                        we7 we72 = we7.a;
                        FlexibleTextView flexibleTextView12 = a2.y;
                        ee7.a((Object) flexibleTextView12, "ftvRanking");
                        String a5 = ig5.a(flexibleTextView12.getContext(), 2131887272);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026.buddy_challenge_ranking)");
                        String format2 = String.format(a5, Arrays.copyOf(new Object[]{0, Integer.valueOf(intValue)}, 2));
                        ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                        flexibleTextView11.setText(format2);
                        CircleProgressView circleProgressView5 = a2.q;
                        ee7.a((Object) circleProgressView5, "circleProgress");
                        circleProgressView5.setProgressColour(v6.a(circleProgressView5.getContext(), (int) R.color.transparent));
                        a2.q.f(0, 1);
                        a2.r.setDisplayType(ut4.START_LEFT);
                        TimerTextView timerTextView10 = a2.r;
                        Date m = mn4.m();
                        timerTextView10.setTime(m != null ? m.getTime() : 0);
                        a2.r.setObserver(this.j);
                        if (ee7.a((Object) "activity_reach_goal", (Object) mn4.u())) {
                            TimerTextView timerTextView11 = a2.t;
                            ee7.a((Object) timerTextView11, "ftvCountDown");
                            timerTextView11.setText(String.valueOf(0));
                        }
                    }
                }
            }
        } else {
            ee7.d("binding");
            throw null;
        }
    }

    @DexIgnore
    public final void f1() {
        qw6<cz4> qw6 = this.h;
        if (qw6 != null) {
            cz4 a2 = qw6.a();
            if (a2 != null) {
                InterceptSwipe interceptSwipe = a2.B;
                ee7.a((Object) interceptSwipe, "swipeRefresh");
                interceptSwipe.setRefreshing(true);
                CircleProgressView circleProgressView = a2.q;
                ee7.a((Object) circleProgressView, "circleProgress");
                du4.a(circleProgressView, new b(this));
                a2.B.setOnRefreshListener(new c(a2, this));
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final IntentFilter g1() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("set_sync_data_event");
        return intentFilter;
    }

    @DexIgnore
    public final void h1() {
        os4 os4 = this.i;
        if (os4 != null) {
            os4.g().a(getViewLifecycleOwner(), new d(this));
            os4 os42 = this.i;
            if (os42 != null) {
                os42.e().a(getViewLifecycleOwner(), new e(this));
                os4 os43 = this.i;
                if (os43 != null) {
                    os43.c().a(getViewLifecycleOwner(), new f(this));
                    os4 os44 = this.i;
                    if (os44 != null) {
                        os44.i().a(getViewLifecycleOwner(), new g(this));
                        os4 os45 = this.i;
                        if (os45 != null) {
                            os45.d().a(getViewLifecycleOwner(), new h(this));
                            os4 os46 = this.i;
                            if (os46 != null) {
                                os46.f().a(getViewLifecycleOwner(), new i(this));
                                os4 os47 = this.i;
                                if (os47 != null) {
                                    os47.h().a(getViewLifecycleOwner(), new j(this));
                                } else {
                                    ee7.d("viewModel");
                                    throw null;
                                }
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.g0.c().f().b().a(this);
        rj4 rj4 = this.g;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(os4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026TabViewModel::class.java)");
            this.i = (os4) a2;
            getLifecycle().a(this.j);
            os4 os4 = this.i;
            if (os4 != null) {
                os4.a();
                qe.a(requireContext()).a(this.f, g1());
                return;
            }
            ee7.d("viewModel");
            throw null;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        cz4 cz4 = (cz4) qb.a(layoutInflater, 2131558527, viewGroup, false, a1());
        this.h = new qw6<>(this, cz4);
        ee7.a((Object) cz4, "binding");
        return cz4.d();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        getLifecycle().b(this.j);
        qe.a(requireContext()).a(this.f);
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        os4 os4 = this.i;
        if (os4 != null) {
            os4.j();
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        os4 os4 = this.i;
        if (os4 != null) {
            os4.l();
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        f1();
        h1();
    }

    @DexIgnore
    public final void c(mn4 mn4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "startOverviewLeaderBoard - challenge: " + mn4);
        BCOverviewLeaderBoardActivity.y.a(this, mn4, null);
    }

    @DexIgnore
    public final void b(mn4 mn4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = q;
        local.e(str, "startDetailScreen - challenge: " + mn4);
        BCWaitingChallengeDetailActivity.a aVar = BCWaitingChallengeDetailActivity.z;
        Fragment requireParentFragment = requireParentFragment();
        ee7.a((Object) requireParentFragment, "requireParentFragment()");
        aVar.a(requireParentFragment, mn4, "joined_challenge", -1, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00c2, code lost:
        if (r11 != null) goto L_0x00f3;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.fossil.v87<com.fossil.hn4, java.lang.String, java.lang.Integer> r25) {
        /*
            r24 = this;
            r0 = r24
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.fossil.ms4.q
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "updateViewOnTopPlayers - triple: "
            r3.append(r4)
            r4 = r25
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r1.e(r2, r3)
            java.lang.Object r1 = r25.getFirst()
            com.fossil.hn4 r1 = (com.fossil.hn4) r1
            java.lang.Object r2 = r25.getSecond()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r25.getThird()
            java.lang.Number r3 = (java.lang.Number) r3
            int r3 = r3.intValue()
            java.lang.Integer r4 = r1.c()
            r5 = 1
            if (r4 == 0) goto L_0x0042
            int r4 = r4.intValue()
            goto L_0x0043
        L_0x0042:
            r4 = 1
        L_0x0043:
            java.util.List r6 = r1.d()
            java.util.List r7 = r1.b()
            java.util.List r1 = r1.d()
            if (r1 == 0) goto L_0x0079
            java.util.Iterator r1 = r1.iterator()
        L_0x0055:
            boolean r10 = r1.hasNext()
            if (r10 == 0) goto L_0x0075
            java.lang.Object r10 = r1.next()
            r11 = r10
            com.fossil.jn4 r11 = (com.fossil.jn4) r11
            java.lang.Integer r11 = r11.h()
            if (r11 != 0) goto L_0x0069
            goto L_0x0071
        L_0x0069:
            int r11 = r11.intValue()
            if (r11 != r5) goto L_0x0071
            r11 = 1
            goto L_0x0072
        L_0x0071:
            r11 = 0
        L_0x0072:
            if (r11 == 0) goto L_0x0055
            goto L_0x0076
        L_0x0075:
            r10 = 0
        L_0x0076:
            com.fossil.jn4 r10 = (com.fossil.jn4) r10
            goto L_0x007a
        L_0x0079:
            r10 = 0
        L_0x007a:
            if (r10 == 0) goto L_0x0087
            java.lang.Integer r1 = r10.p()
            if (r1 == 0) goto L_0x0087
            int r1 = r1.intValue()
            goto L_0x0088
        L_0x0087:
            r1 = 0
        L_0x0088:
            if (r10 == 0) goto L_0x0095
            java.lang.Integer r11 = r10.m()
            if (r11 == 0) goto L_0x0095
            int r11 = r11.intValue()
            goto L_0x0096
        L_0x0095:
            r11 = 0
        L_0x0096:
            int r1 = r1 - r11
            if (r6 == 0) goto L_0x00c5
            java.util.Iterator r6 = r6.iterator()
        L_0x009d:
            boolean r11 = r6.hasNext()
            if (r11 == 0) goto L_0x00bf
            java.lang.Object r11 = r6.next()
            r12 = r11
            com.fossil.jn4 r12 = (com.fossil.jn4) r12
            java.lang.String r12 = r12.d()
            com.portfolio.platform.PortfolioApp$a r13 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r13 = r13.c()
            java.lang.String r13 = r13.w()
            boolean r12 = com.fossil.ee7.a(r12, r13)
            if (r12 == 0) goto L_0x009d
            goto L_0x00c0
        L_0x00bf:
            r11 = 0
        L_0x00c0:
            com.fossil.jn4 r11 = (com.fossil.jn4) r11
            if (r11 == 0) goto L_0x00c5
            goto L_0x00f3
        L_0x00c5:
            if (r7 == 0) goto L_0x00f2
            java.util.Iterator r6 = r7.iterator()
        L_0x00cb:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x00ed
            java.lang.Object r7 = r6.next()
            r11 = r7
            com.fossil.jn4 r11 = (com.fossil.jn4) r11
            java.lang.String r11 = r11.d()
            com.portfolio.platform.PortfolioApp$a r12 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r12 = r12.c()
            java.lang.String r12 = r12.w()
            boolean r11 = com.fossil.ee7.a(r11, r12)
            if (r11 == 0) goto L_0x00cb
            goto L_0x00ee
        L_0x00ed:
            r7 = 0
        L_0x00ee:
            r11 = r7
            com.fossil.jn4 r11 = (com.fossil.jn4) r11
            goto L_0x00f3
        L_0x00f2:
            r11 = 0
        L_0x00f3:
            if (r11 == 0) goto L_0x0100
            java.lang.Integer r6 = r11.p()
            if (r6 == 0) goto L_0x0100
            int r6 = r6.intValue()
            goto L_0x0101
        L_0x0100:
            r6 = 0
        L_0x0101:
            if (r11 == 0) goto L_0x010e
            java.lang.Integer r7 = r11.m()
            if (r7 == 0) goto L_0x010e
            int r7 = r7.intValue()
            goto L_0x010f
        L_0x010e:
            r7 = 0
        L_0x010f:
            int r6 = r6 - r7
            com.fossil.qw6<com.fossil.cz4> r7 = r0.h
            if (r7 == 0) goto L_0x03f1
            java.lang.Object r7 = r7.a()
            com.fossil.cz4 r7 = (com.fossil.cz4) r7
            if (r7 == 0) goto L_0x03f0
            com.portfolio.platform.uirenew.customview.TimerTextView r12 = r7.t
            java.lang.String r13 = "ftvCountDown"
            com.fossil.ee7.a(r12, r13)
            java.lang.Object r12 = r12.getTag()
            boolean r14 = r12 instanceof java.lang.Integer
            if (r14 != 0) goto L_0x012c
            r12 = 0
        L_0x012c:
            java.lang.Integer r12 = (java.lang.Integer) r12
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r15 = com.fossil.ms4.q
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r5 = "updateViewOnTopPlayers - cachedStep: "
            r9.append(r5)
            r9.append(r12)
            java.lang.String r5 = " - ownerStep: "
            r9.append(r5)
            r9.append(r6)
            java.lang.String r5 = r9.toString()
            r14.e(r15, r5)
            int r5 = r2.hashCode()
            r9 = -1348781656(0xffffffffaf9b39a8, float:-2.8235303E-10)
            r14 = 2131099677(0x7f06001d, float:1.7811714E38)
            r15 = 2
            if (r5 == r9) goto L_0x01eb
            r9 = -637042289(0xffffffffda07818f, float:-9.5353933E15)
            if (r5 == r9) goto L_0x016a
        L_0x0164:
            r16 = r10
            r17 = r11
            goto L_0x0203
        L_0x016a:
            java.lang.String r5 = "activity_reach_goal"
            boolean r2 = r2.equals(r5)
            if (r2 == 0) goto L_0x0164
            int r2 = r3 - r6
            com.portfolio.platform.buddy_challenge.customview.CircleProgressView r5 = r7.q
            long r8 = (long) r2
            r16 = r10
            r17 = r11
            long r10 = (long) r3
            r5.e(r8, r10)
            r14 = 2131099689(0x7f060029, float:1.7811738E38)
            int[] r5 = new int[r15]
            if (r12 == 0) goto L_0x018b
            int r8 = r12.intValue()
            goto L_0x018c
        L_0x018b:
            r8 = r3
        L_0x018c:
            r9 = 0
            r5[r9] = r8
            r8 = 1
            r5[r8] = r2
            android.animation.ValueAnimator r5 = android.animation.ValueAnimator.ofInt(r5)
            java.lang.String r8 = "animator"
            com.fossil.ee7.a(r5, r8)
            android.view.animation.AccelerateDecelerateInterpolator r8 = new android.view.animation.AccelerateDecelerateInterpolator
            r8.<init>()
            r5.setInterpolator(r8)
            r8 = 800(0x320, double:3.953E-321)
            r5.setDuration(r8)
            com.fossil.ms4$l r8 = new com.fossil.ms4$l
            r8.<init>(r7)
            r5.addUpdateListener(r8)
            r5.start()
            com.portfolio.platform.uirenew.customview.TimerTextView r5 = r7.t
            com.fossil.ee7.a(r5, r13)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            r5.setTag(r2)
            if (r6 < r3) goto L_0x0203
            com.fossil.os4 r2 = r0.i
            if (r2 == 0) goto L_0x01e4
            r2.b()
            androidx.fragment.app.FragmentActivity r2 = r24.getActivity()
            if (r2 == 0) goto L_0x0203
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r5 = 2131886191(0x7f12006f, float:1.9406954E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r5)
            r5 = 1
            android.widget.Toast r2 = android.widget.Toast.makeText(r2, r3, r5)
            r2.show()
            goto L_0x0203
        L_0x01e4:
            java.lang.String r1 = "viewModel"
            com.fossil.ee7.d(r1)
            r1 = 0
            throw r1
        L_0x01eb:
            r16 = r10
            r17 = r11
            java.lang.String r3 = "activity_best_result"
            boolean r2 = r2.equals(r3)
            if (r2 == 0) goto L_0x0203
            com.portfolio.platform.uirenew.customview.TimerTextView r2 = r7.t
            com.fossil.ee7.a(r2, r13)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r6)
            r2.setTag(r3)
        L_0x0203:
            com.portfolio.platform.view.FlexibleTextView r2 = r7.u
            java.lang.String r3 = "ftvCountPlayers"
            com.fossil.ee7.a(r2, r3)
            java.lang.String r5 = java.lang.String.valueOf(r4)
            r2.setText(r5)
            com.portfolio.platform.view.FlexibleTextView r2 = r7.u
            com.fossil.ee7.a(r2, r3)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r2.setTag(r3)
            if (r16 == 0) goto L_0x0224
            java.lang.String r2 = r16.d()
            goto L_0x0225
        L_0x0224:
            r2 = 0
        L_0x0225:
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            java.lang.String r3 = r3.w()
            boolean r2 = com.fossil.ee7.a(r2, r3)
            java.lang.String r3 = "ftvRanking"
            java.lang.String r5 = "LanguageHelper.getString\u2026.buddy_challenge_ranking)"
            r8 = 2131887272(0x7f1204a8, float:1.9409146E38)
            java.lang.String r9 = "ftvChallengeInfo"
            java.lang.String r10 = "ftvPlayerInfo"
            java.lang.String r11 = "java.lang.String.format(format, *args)"
            if (r2 == 0) goto L_0x02c6
            com.portfolio.platform.view.FlexibleTextView r2 = r7.x
            com.fossil.ee7.a(r2, r10)
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r10 = 2131886249(0x7f1200a9, float:1.9407072E38)
            java.lang.String r6 = com.fossil.ig5.a(r6, r10)
            r2.setText(r6)
            com.fossil.we7 r2 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            r6 = 2131886247(0x7f1200a7, float:1.9407067E38)
            java.lang.String r2 = com.fossil.ig5.a(r2, r6)
            java.lang.String r6 = "LanguageHelper.getString\u2026bel__YouJustHitStepsKeep)"
            com.fossil.ee7.a(r2, r6)
            r6 = 1
            java.lang.Object[] r10 = new java.lang.Object[r6]
            java.lang.String r12 = java.lang.String.valueOf(r1)
            r13 = 0
            r10[r13] = r12
            java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r6)
            java.lang.String r2 = java.lang.String.format(r2, r10)
            com.fossil.ee7.a(r2, r11)
            com.portfolio.platform.uirenew.customview.TimerTextView r6 = r7.r
            com.fossil.ee7.a(r6, r9)
            com.fossil.xe5 r9 = com.fossil.xe5.b
            java.lang.String r1 = java.lang.String.valueOf(r1)
            android.text.Spannable r1 = r9.a(r1, r2, r14)
            r6.setText(r1)
            com.fossil.we7 r1 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r1 = com.fossil.ig5.a(r1, r8)
            com.fossil.ee7.a(r1, r5)
            java.lang.Object[] r2 = new java.lang.Object[r15]
            r5 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            r8 = 0
            r2[r8] = r6
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r2[r5] = r4
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r2, r15)
            java.lang.String r1 = java.lang.String.format(r1, r2)
            com.fossil.ee7.a(r1, r11)
            com.portfolio.platform.view.FlexibleTextView r2 = r7.y
            com.fossil.ee7.a(r2, r3)
            r2.setText(r1)
            goto L_0x03f0
        L_0x02c6:
            if (r16 == 0) goto L_0x02cd
            java.lang.Boolean r2 = r16.s()
            goto L_0x02ce
        L_0x02cd:
            r2 = 0
        L_0x02ce:
            r12 = 1
            java.lang.Boolean r13 = java.lang.Boolean.valueOf(r12)
            boolean r2 = com.fossil.ee7.a(r2, r13)
            if (r2 == 0) goto L_0x02ea
            com.portfolio.platform.view.FlexibleTextView r2 = r7.x
            com.fossil.ee7.a(r2, r10)
            android.content.Context r2 = r2.getContext()
            r12 = 2131887275(0x7f1204ab, float:1.9409152E38)
            java.lang.String r2 = com.fossil.ig5.a(r2, r12)
            goto L_0x030d
        L_0x02ea:
            com.fossil.fu4 r2 = com.fossil.fu4.a
            if (r16 == 0) goto L_0x02f3
            java.lang.String r12 = r16.c()
            goto L_0x02f4
        L_0x02f3:
            r12 = 0
        L_0x02f4:
            if (r16 == 0) goto L_0x02fb
            java.lang.String r13 = r16.e()
            goto L_0x02fc
        L_0x02fb:
            r13 = 0
        L_0x02fc:
            if (r16 == 0) goto L_0x0305
            java.lang.String r16 = r16.i()
            if (r16 == 0) goto L_0x0305
            goto L_0x0307
        L_0x0305:
            java.lang.String r16 = "-"
        L_0x0307:
            r15 = r16
            java.lang.String r2 = r2.a(r12, r13, r15)
        L_0x030d:
            int r1 = r1 - r6
            com.fossil.we7 r6 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r12 = 2131886250(0x7f1200aa, float:1.9407074E38)
            java.lang.String r6 = com.fossil.ig5.a(r6, r12)
            java.lang.String r12 = "LanguageHelper.getString\u2026oing_Text__NameIsLeading)"
            com.fossil.ee7.a(r6, r12)
            r12 = 1
            java.lang.Object[] r13 = new java.lang.Object[r12]
            r15 = 0
            r13[r15] = r2
            java.lang.Object[] r13 = java.util.Arrays.copyOf(r13, r12)
            java.lang.String r6 = java.lang.String.format(r6, r13)
            com.fossil.ee7.a(r6, r11)
            com.portfolio.platform.view.FlexibleTextView r12 = r7.x
            com.fossil.ee7.a(r12, r10)
            com.fossil.xe5 r18 = com.fossil.xe5.b
            java.lang.String r10 = "topLastName"
            com.fossil.ee7.a(r2, r10)
            r21 = 0
            r22 = 4
            r23 = 0
            r19 = r2
            r20 = r6
            android.text.Spannable r2 = com.fossil.xe5.a(r18, r19, r20, r21, r22, r23)
            r12.setText(r2)
            com.fossil.we7 r2 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            r6 = 2131886306(0x7f1200e2, float:1.9407187E38)
            java.lang.String r2 = com.fossil.ig5.a(r2, r6)
            java.lang.String r6 = "LanguageHelper.getString\u2026rd_FullView_Label__Steps)"
            com.fossil.ee7.a(r2, r6)
            r6 = 1
            java.lang.Object[] r10 = new java.lang.Object[r6]
            java.lang.String r12 = java.lang.String.valueOf(r1)
            r13 = 0
            r10[r13] = r12
            java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r6)
            java.lang.String r2 = java.lang.String.format(r2, r10)
            com.fossil.ee7.a(r2, r11)
            com.fossil.we7 r2 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r2 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r2 = r2.c()
            r10 = 2131886248(0x7f1200a8, float:1.940707E38)
            java.lang.String r2 = com.fossil.ig5.a(r2, r10)
            java.lang.String r10 = "LanguageHelper.getString\u2026el__YouNeedMoreThanSteps)"
            com.fossil.ee7.a(r2, r10)
            java.lang.Object[] r10 = new java.lang.Object[r6]
            java.lang.String r12 = java.lang.String.valueOf(r1)
            r13 = 0
            r10[r13] = r12
            java.lang.Object[] r10 = java.util.Arrays.copyOf(r10, r6)
            java.lang.String r2 = java.lang.String.format(r2, r10)
            com.fossil.ee7.a(r2, r11)
            com.portfolio.platform.uirenew.customview.TimerTextView r6 = r7.r
            com.fossil.ee7.a(r6, r9)
            com.fossil.xe5 r9 = com.fossil.xe5.b
            java.lang.String r1 = java.lang.String.valueOf(r1)
            android.text.Spannable r1 = r9.a(r1, r2, r14)
            r6.setText(r1)
            com.fossil.we7 r1 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r1 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r1 = r1.c()
            java.lang.String r1 = com.fossil.ig5.a(r1, r8)
            com.fossil.ee7.a(r1, r5)
            r2 = 2
            java.lang.Object[] r5 = new java.lang.Object[r2]
            if (r17 == 0) goto L_0x03ce
            java.lang.Integer r2 = r17.h()
            if (r2 == 0) goto L_0x03ce
            goto L_0x03d2
        L_0x03ce:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)
        L_0x03d2:
            r6 = 0
            r5[r6] = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)
            r4 = 1
            r5[r4] = r2
            r2 = 2
            java.lang.Object[] r2 = java.util.Arrays.copyOf(r5, r2)
            java.lang.String r1 = java.lang.String.format(r1, r2)
            com.fossil.ee7.a(r1, r11)
            com.portfolio.platform.view.FlexibleTextView r2 = r7.y
            com.fossil.ee7.a(r2, r3)
            r2.setText(r1)
        L_0x03f0:
            return
        L_0x03f1:
            java.lang.String r1 = "binding"
            com.fossil.ee7.d(r1)
            r1 = 0
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ms4.a(com.fossil.v87):void");
    }
}
