package com.fossil;

import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wa4 implements fo3 {
    @DexIgnore
    public /* final */ xa4 a;
    @DexIgnore
    public /* final */ Pair b;

    @DexIgnore
    public wa4(xa4 xa4, Pair pair) {
        this.a = xa4;
        this.b = pair;
    }

    @DexIgnore
    @Override // com.fossil.fo3
    public final Object then(no3 no3) {
        this.a.a(this.b, no3);
        return no3;
    }
}
