package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o3 extends q3 {
    @DexIgnore
    public static volatile o3 c;
    @DexIgnore
    public static /* final */ Executor d; // = new a();
    @DexIgnore
    public static /* final */ Executor e; // = new b();
    @DexIgnore
    public q3 a;
    @DexIgnore
    public q3 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            o3.c().c(runnable);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public void execute(Runnable runnable) {
            o3.c().a(runnable);
        }
    }

    @DexIgnore
    public o3() {
        p3 p3Var = new p3();
        this.b = p3Var;
        this.a = p3Var;
    }

    @DexIgnore
    public static Executor b() {
        return e;
    }

    @DexIgnore
    public static o3 c() {
        if (c != null) {
            return c;
        }
        synchronized (o3.class) {
            if (c == null) {
                c = new o3();
            }
        }
        return c;
    }

    @DexIgnore
    public static Executor d() {
        return d;
    }

    @DexIgnore
    @Override // com.fossil.q3
    public void a(Runnable runnable) {
        this.a.a(runnable);
    }

    @DexIgnore
    @Override // com.fossil.q3
    public boolean a() {
        return this.a.a();
    }

    @DexIgnore
    @Override // com.fossil.q3
    public void c(Runnable runnable) {
        this.a.c(runnable);
    }
}
