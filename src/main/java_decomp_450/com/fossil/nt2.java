package com.fossil;

import java.util.AbstractMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nt2 extends os2<Map.Entry<K, V>> {
    @DexIgnore
    public /* final */ /* synthetic */ ot2 zza;

    @DexIgnore
    public nt2(ot2 ot2) {
        this.zza = ot2;
    }

    @DexIgnore
    @Override // java.util.List
    public final /* synthetic */ Object get(int i) {
        or2.a(i, this.zza.e);
        int i2 = i * 2;
        return new AbstractMap.SimpleImmutableEntry(this.zza.d[i2], this.zza.d[i2 + 1]);
    }

    @DexIgnore
    public final int size() {
        return this.zza.e;
    }

    @DexIgnore
    @Override // com.fossil.ps2
    public final boolean zzh() {
        return true;
    }
}
