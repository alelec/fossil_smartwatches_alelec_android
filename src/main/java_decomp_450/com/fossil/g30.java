package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g30 {
    @DexIgnore
    public /* final */ List<a<?, ?>> a; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<Z, R> {
        @DexIgnore
        public /* final */ Class<Z> a;
        @DexIgnore
        public /* final */ Class<R> b;
        @DexIgnore
        public /* final */ f30<Z, R> c;

        @DexIgnore
        public a(Class<Z> cls, Class<R> cls2, f30<Z, R> f30) {
            this.a = cls;
            this.b = cls2;
            this.c = f30;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return this.a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.b);
        }
    }

    @DexIgnore
    public synchronized <Z, R> void a(Class<Z> cls, Class<R> cls2, f30<Z, R> f30) {
        this.a.add(new a<>(cls, cls2, f30));
    }

    @DexIgnore
    public synchronized <Z, R> List<Class<R>> b(Class<Z> cls, Class<R> cls2) {
        ArrayList arrayList = new ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList.add(cls2);
            return arrayList;
        }
        for (a<?, ?> aVar : this.a) {
            if (aVar.a(cls, cls2)) {
                arrayList.add(cls2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Z, R> f30<Z, R> a(Class<Z> cls, Class<R> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return h30.a();
        }
        for (a<?, ?> aVar : this.a) {
            if (aVar.a(cls, cls2)) {
                return aVar.c;
            }
        }
        throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
    }
}
