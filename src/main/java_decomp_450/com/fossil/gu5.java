package com.fossil;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.fl4;
import com.fossil.ql4;
import com.fossil.vu5;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.xg5;
import com.fossil.yu5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gu5 extends au5 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ a m; // = new a(null);
    @DexIgnore
    public /* final */ List<bt5> e; // = new ArrayList();
    @DexIgnore
    public /* final */ List<bt5> f; // = new ArrayList();
    @DexIgnore
    public /* final */ bu5 g;
    @DexIgnore
    public /* final */ rl4 h;
    @DexIgnore
    public /* final */ vu5 i;
    @DexIgnore
    public /* final */ yu5 j;
    @DexIgnore
    public /* final */ LoaderManager k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return gu5.l;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ql4.d<yu5.c, ql4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ gu5 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b(gu5 gu5) {
            this.a = gu5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(yu5.c cVar) {
            FLogger.INSTANCE.getLocal().d(gu5.m.a(), ".Inside mSaveContactGroupsNotification onSuccess");
            this.a.g.close();
        }

        @DexIgnore
        public void a(ql4.a aVar) {
            FLogger.INSTANCE.getLocal().d(gu5.m.a(), ".Inside mSaveContactGroupsNotification onError");
            this.a.g.close();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1", f = "NotificationContactsPresenter.kt", l = {49}, m = "invokeSuspend")
    public static final class c extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gu5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$1", f = "NotificationContactsPresenter.kt", l = {50}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    if (c.k(this) == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements fl4.e<vu5.d, vu5.b> {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1", f = "NotificationContactsPresenter.kt", l = {60}, m = "invokeSuspend")
            public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ vu5.d $responseValue;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ b this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gu5$c$b$a$a")
                @tb7(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter$start$1$2$onSuccess$1$1", f = "NotificationContactsPresenter.kt", l = {}, m = "invokeSuspend")
                /* renamed from: com.fossil.gu5$c$b$a$a  reason: collision with other inner class name */
                public static final class C0070a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public yi7 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0070a(a aVar, fb7 fb7) {
                        super(2, fb7);
                        this.this$0 = aVar;
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final fb7<i97> create(Object obj, fb7<?> fb7) {
                        ee7.b(fb7, "completion");
                        C0070a aVar = new C0070a(this.this$0, fb7);
                        aVar.p$ = (yi7) obj;
                        return aVar;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.fossil.kd7
                    public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                        return ((C0070a) create(yi7, fb7)).invokeSuspend(i97.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.ob7
                    public final Object invokeSuspend(Object obj) {
                        nb7.a();
                        if (this.label == 0) {
                            t87.a(obj);
                            for (T t : this.this$0.$responseValue.a()) {
                                List<Contact> contacts = t.getContacts();
                                ee7.a((Object) contacts, "it.contacts");
                                if (!contacts.isEmpty()) {
                                    Contact contact = t.getContacts().get(0);
                                    bt5 bt5 = new bt5(contact, null, 2, null);
                                    bt5.setAdded(true);
                                    Contact contact2 = bt5.getContact();
                                    if (contact2 != null) {
                                        ee7.a((Object) contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                    }
                                    Contact contact3 = bt5.getContact();
                                    if (contact3 != null) {
                                        ee7.a((Object) contact, "contact");
                                        contact3.setUseSms(contact.isUseSms());
                                    }
                                    Contact contact4 = bt5.getContact();
                                    if (contact4 != null) {
                                        ee7.a((Object) contact, "contact");
                                        contact4.setUseCall(contact.isUseCall());
                                    }
                                    ee7.a((Object) contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    ee7.a((Object) phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        ee7.a((Object) phoneNumber, "contact.phoneNumbers[0]");
                                        if (!TextUtils.isEmpty(phoneNumber.getNumber())) {
                                            bt5.setHasPhoneNumber(true);
                                            PhoneNumber phoneNumber2 = contact.getPhoneNumbers().get(0);
                                            ee7.a((Object) phoneNumber2, "contact.phoneNumbers[0]");
                                            bt5.setPhoneNumber(phoneNumber2.getNumber());
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a = gu5.m.a();
                                            StringBuilder sb = new StringBuilder();
                                            sb.append(".Inside loadContactData filter selected contact, phoneNumber=");
                                            PhoneNumber phoneNumber3 = contact.getPhoneNumbers().get(0);
                                            ee7.a((Object) phoneNumber3, "contact.phoneNumbers[0]");
                                            sb.append(phoneNumber3.getNumber());
                                            local.d(a, sb.toString());
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a2 = gu5.m.a();
                                    local2.d(a2, ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.a.this$0.j().add(bt5);
                                }
                            }
                            return pb7.a(this.this$0.this$0.a.this$0.k().addAll(ea7.d((Collection) this.this$0.this$0.a.this$0.j())));
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public a(b bVar, vu5.d dVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = bVar;
                    this.$responseValue = dVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    a aVar = new a(this.this$0, this.$responseValue, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                    return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = nb7.a();
                    int i = this.label;
                    if (i == 0) {
                        t87.a(obj);
                        yi7 yi7 = this.p$;
                        ti7 a2 = this.this$0.a.this$0.b();
                        C0070a aVar = new C0070a(this, null);
                        this.L$0 = yi7;
                        this.label = 1;
                        if (vh7.a(a2, aVar, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        yi7 yi72 = (yi7) this.L$0;
                        t87.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    this.this$0.a.this$0.g.a(this.this$0.a.this$0.j(), fx6.a.a());
                    this.this$0.a.this$0.k.a(0, new Bundle(), this.this$0.a.this$0);
                    return i97.a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public b(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            /* renamed from: a */
            public void onSuccess(vu5.d dVar) {
                ee7.b(dVar, "responseValue");
                FLogger.INSTANCE.getLocal().d(gu5.m.a(), "GetAllContactGroup onSuccess");
                ik7 unused = xh7.b(this.a.this$0.e(), null, null, new a(this, dVar, null), 3, null);
            }

            @DexIgnore
            public void a(vu5.b bVar) {
                ee7.b(bVar, "errorValue");
                FLogger.INSTANCE.getLocal().d(gu5.m.a(), "GetAllContactGroup onError");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(gu5 gu5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = gu5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            c cVar = new c(this.this$0, fb7);
            cVar.p$ = (yi7) obj;
            return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (!PortfolioApp.g0.c().v().U()) {
                    ti7 a3 = this.this$0.b();
                    a aVar = new a(null);
                    this.L$0 = yi7;
                    this.label = 1;
                    if (vh7.a(a3, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.j().isEmpty()) {
                this.this$0.i.a((fl4.b) null, new b(this));
            }
            return i97.a;
        }
    }

    /*
    static {
        String simpleName = gu5.class.getSimpleName();
        ee7.a((Object) simpleName, "NotificationContactsPres\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public gu5(bu5 bu5, rl4 rl4, vu5 vu5, yu5 yu5, LoaderManager loaderManager) {
        ee7.b(bu5, "mView");
        ee7.b(rl4, "mUseCaseHandler");
        ee7.b(vu5, "mGetAllContactGroup");
        ee7.b(yu5, "mSaveContactGroupsNotification");
        ee7.b(loaderManager, "mLoaderManager");
        this.g = bu5;
        this.h = rl4;
        this.i = vu5;
        this.j = yu5;
        this.k = loaderManager;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        xg5 xg5 = xg5.b;
        bu5 bu5 = this.g;
        if (bu5 == null) {
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsFragment");
        } else if (xg5.a(xg5, ((cu5) bu5).getContext(), xg5.a.NOTIFICATION_GET_CONTACTS, false, true, false, (Integer) null, 52, (Object) null)) {
            ik7 unused = xh7.b(e(), null, null, new c(this, null), 3, null);
        }
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.au5
    public void h() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        List b2 = ea7.b((Collection) this.f, (Iterable) this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Iterator it = b2.iterator();
        while (true) {
            Integer num = null;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            Contact contact = ((bt5) next).getContact();
            if (contact != null) {
                num = Integer.valueOf(contact.getContactId());
            }
            Object obj = linkedHashMap.get(num);
            if (obj == null) {
                obj = new ArrayList();
                linkedHashMap.put(num, obj);
            }
            ((List) obj).add(next);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it2 = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it2.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it2.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList<bt5> arrayList3 = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            ba7.a((Collection) arrayList3, (Iterable) ((List) entry2.getValue()));
        }
        for (bt5 bt5 : arrayList3) {
            Iterator<T> it3 = this.f.iterator();
            boolean z2 = false;
            while (it3.hasNext()) {
                Contact contact2 = it3.next().getContact();
                Integer valueOf = contact2 != null ? Integer.valueOf(contact2.getContactId()) : null;
                Contact contact3 = bt5.getContact();
                if (ee7.a(valueOf, contact3 != null ? Integer.valueOf(contact3.getContactId()) : null)) {
                    arrayList.add(bt5);
                    z2 = true;
                }
            }
            if (!z2) {
                arrayList2.add(bt5);
            }
        }
        this.h.a(this.j, new yu5.b(arrayList, arrayList2), new b(this));
    }

    @DexIgnore
    @Override // com.fossil.au5
    public void i() {
        FLogger.INSTANCE.getLocal().d(l, "onListContactWrapperChanged");
        List b2 = ea7.b((Collection) this.f, (Iterable) this.e);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Object obj : b2) {
            Contact contact = ((bt5) obj).getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Object obj2 = linkedHashMap.get(valueOf);
            if (obj2 == null) {
                obj2 = new ArrayList();
                linkedHashMap.put(valueOf, obj2);
            }
            ((List) obj2).add(obj);
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        Iterator it = linkedHashMap.entrySet().iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Map.Entry entry = (Map.Entry) it.next();
            if (((List) entry.getValue()).size() == 1) {
                z = true;
            }
            if (z) {
                linkedHashMap2.put(entry.getKey(), entry.getValue());
            }
        }
        ArrayList arrayList = new ArrayList();
        for (Map.Entry entry2 : linkedHashMap2.entrySet()) {
            ba7.a((Collection) arrayList, (Iterable) ((List) entry2.getValue()));
        }
        if (arrayList.isEmpty()) {
            this.g.R(false);
        } else {
            this.g.R(true);
        }
    }

    @DexIgnore
    public final List<bt5> j() {
        return this.e;
    }

    @DexIgnore
    public final List<bt5> k() {
        return this.f;
    }

    @DexIgnore
    public void l() {
        this.g.a(this);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public oe<Cursor> a(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = " + "has_phone_number!=0 AND mimetype=?");
        String[] strArr = {"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"};
        return new ne(PortfolioApp.g0.c(), ContactsContract.Data.CONTENT_URI, strArr, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    public void a(oe<Cursor> oeVar, Cursor cursor) {
        ee7.b(oeVar, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.g.a(cursor);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void a(oe<Cursor> oeVar) {
        ee7.b(oeVar, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.g.s();
    }
}
