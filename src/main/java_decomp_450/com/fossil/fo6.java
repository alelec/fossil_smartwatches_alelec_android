package com.fossil;

import com.portfolio.platform.uirenew.login.LoginActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fo6 implements MembersInjector<LoginActivity> {
    @DexIgnore
    public static void a(LoginActivity loginActivity, ih5 ih5) {
        loginActivity.y = ih5;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, jh5 jh5) {
        loginActivity.z = jh5;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, lh5 lh5) {
        loginActivity.A = lh5;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, kh5 kh5) {
        loginActivity.B = kh5;
    }

    @DexIgnore
    public static void a(LoginActivity loginActivity, no6 no6) {
        loginActivity.C = no6;
    }
}
