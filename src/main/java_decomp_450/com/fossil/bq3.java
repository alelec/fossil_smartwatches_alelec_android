package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bq3 implements Parcelable.Creator<aq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ aq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        String str = null;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                str = j72.e(parcel, a);
            } else if (a2 != 3) {
                j72.v(parcel, a);
            } else {
                arrayList = j72.c(parcel, a, pq3.CREATOR);
            }
        }
        j72.h(parcel, b);
        return new aq3(str, arrayList);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ aq3[] newArray(int i) {
        return new aq3[i];
    }
}
