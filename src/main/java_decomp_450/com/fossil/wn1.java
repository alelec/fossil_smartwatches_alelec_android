package com.fossil;

import com.fossil.f60;
import com.fossil.l60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ km1 a;

    @DexIgnore
    public wn1(km1 km1) {
        this.a = km1;
    }

    @DexIgnore
    public final void run() {
        if (this.a.u == l60.c.DISCONNECTED && f60.k.d() == f60.c.ENABLED) {
            yp0 yp0 = yp0.f;
            zk0[] b = this.a.f.b();
            int length = b.length;
            int i = 0;
            while (i < length && !(!(b[i] instanceof ec1))) {
                i++;
            }
            yp0.f();
            xm0.i.a(false);
            this.a.b(oa7.b(w87.a(yo1.AUTO_CONNECT, false), w87.a(yo1.CONNECTION_TIME_OUT, 0L)));
        }
    }
}
