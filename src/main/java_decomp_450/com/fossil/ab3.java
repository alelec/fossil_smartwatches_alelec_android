package com.fossil;

import com.fossil.n63;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ab3 extends j73 {
    @DexIgnore
    public /* final */ /* synthetic */ n63.g a;

    @DexIgnore
    public ab3(n63 n63, n63.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    @Override // com.fossil.i73
    public final void onMapClick(LatLng latLng) {
        this.a.onMapClick(latLng);
    }
}
