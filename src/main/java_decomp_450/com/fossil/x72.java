package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x72 extends tf2 implements u72 {
    @DexIgnore
    public x72() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    @DexIgnore
    @Override // com.fossil.tf2
    public boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        d(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
