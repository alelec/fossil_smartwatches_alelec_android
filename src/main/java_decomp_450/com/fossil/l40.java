package com.fossil;

import com.fossil.o40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l40 implements o40, n40 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ o40 b;
    @DexIgnore
    public volatile n40 c;
    @DexIgnore
    public volatile n40 d;
    @DexIgnore
    public o40.a e;
    @DexIgnore
    public o40.a f;

    @DexIgnore
    public l40(Object obj, o40 o40) {
        o40.a aVar = o40.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.a = obj;
        this.b = o40;
    }

    @DexIgnore
    public void a(n40 n40, n40 n402) {
        this.c = n40;
        this.d = n402;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean b(n40 n40) {
        if (!(n40 instanceof l40)) {
            return false;
        }
        l40 l40 = (l40) n40;
        if (!this.c.b(l40.c) || !this.d.b(l40.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void c() {
        synchronized (this.a) {
            if (this.e != o40.a.RUNNING) {
                this.e = o40.a.RUNNING;
                this.c.c();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void clear() {
        synchronized (this.a) {
            this.e = o40.a.CLEARED;
            this.c.clear();
            if (this.f != o40.a.CLEARED) {
                this.f = o40.a.CLEARED;
                this.d.clear();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public void d() {
        synchronized (this.a) {
            if (this.e == o40.a.RUNNING) {
                this.e = o40.a.PAUSED;
                this.c.d();
            }
            if (this.f == o40.a.RUNNING) {
                this.f = o40.a.PAUSED;
                this.d.d();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.e == o40.a.CLEARED && this.f == o40.a.CLEARED;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean f() {
        boolean z;
        synchronized (this.a) {
            if (this.e != o40.a.SUCCESS) {
                if (this.f != o40.a.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean g() {
        o40 o40 = this.b;
        return o40 == null || o40.f(this);
    }

    @DexIgnore
    public final boolean h() {
        o40 o40 = this.b;
        return o40 == null || o40.c(this);
    }

    @DexIgnore
    public final boolean i() {
        o40 o40 = this.b;
        return o40 == null || o40.d(this);
    }

    @DexIgnore
    @Override // com.fossil.n40
    public boolean isRunning() {
        boolean z;
        synchronized (this.a) {
            if (this.e != o40.a.RUNNING) {
                if (this.f != o40.a.RUNNING) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean g(n40 n40) {
        return n40.equals(this.c) || (this.e == o40.a.FAILED && n40.equals(this.d));
    }

    @DexIgnore
    @Override // com.fossil.n40, com.fossil.o40
    public boolean a() {
        boolean z;
        synchronized (this.a) {
            if (!this.c.a()) {
                if (!this.d.a()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public o40 b() {
        o40 b2;
        synchronized (this.a) {
            b2 = this.b != null ? this.b.b() : this;
        }
        return b2;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public void e(n40 n40) {
        synchronized (this.a) {
            if (n40.equals(this.c)) {
                this.e = o40.a.SUCCESS;
            } else if (n40.equals(this.d)) {
                this.f = o40.a.SUCCESS;
            }
            if (this.b != null) {
                this.b.e(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean f(n40 n40) {
        boolean z;
        synchronized (this.a) {
            z = g() && g(n40);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public void a(n40 n40) {
        synchronized (this.a) {
            if (!n40.equals(this.d)) {
                this.e = o40.a.FAILED;
                if (this.f != o40.a.RUNNING) {
                    this.f = o40.a.RUNNING;
                    this.d.c();
                }
                return;
            }
            this.f = o40.a.FAILED;
            if (this.b != null) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean c(n40 n40) {
        boolean z;
        synchronized (this.a) {
            z = h() && g(n40);
        }
        return z;
    }

    @DexIgnore
    @Override // com.fossil.o40
    public boolean d(n40 n40) {
        boolean z;
        synchronized (this.a) {
            z = i() && g(n40);
        }
        return z;
    }
}
