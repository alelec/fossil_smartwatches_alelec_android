package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i43 implements tr2<h43> {
    @DexIgnore
    public static i43 b; // = new i43();
    @DexIgnore
    public /* final */ tr2<h43> a;

    @DexIgnore
    public i43(tr2<h43> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((h43) b.zza()).zza();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ h43 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public i43() {
        this(sr2.a(new k43()));
    }
}
