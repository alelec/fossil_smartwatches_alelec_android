package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentProvider;
import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l77 {
    @DexIgnore
    public static void a(Activity activity) {
        c87.a(activity, Constants.ACTIVITY);
        Application application = activity.getApplication();
        if (application instanceof u77) {
            a(activity, (u77) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), u77.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void a(Service service) {
        c87.a(service, Constants.SERVICE);
        Application application = service.getApplication();
        if (application instanceof u77) {
            a(service, (u77) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), u77.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void a(BroadcastReceiver broadcastReceiver, Context context) {
        c87.a(broadcastReceiver, "broadcastReceiver");
        c87.a(context, "context");
        Application application = (Application) context.getApplicationContext();
        if (application instanceof u77) {
            a(broadcastReceiver, (u77) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), u77.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void a(ContentProvider contentProvider) {
        c87.a(contentProvider, "contentProvider");
        Application application = (Application) contentProvider.getContext().getApplicationContext();
        if (application instanceof u77) {
            a(contentProvider, (u77) application);
        } else {
            throw new RuntimeException(String.format("%s does not implement %s", application.getClass().getCanonicalName(), u77.class.getCanonicalName()));
        }
    }

    @DexIgnore
    public static void a(Object obj, u77 u77) {
        m77<Object> a = u77.a();
        c87.a(a, "%s.androidInjector() returned null", u77.getClass());
        a.a(obj);
    }
}
