package com.fossil;

import com.fossil.fitness.WorkoutType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ac5 {
    UNKNOWN("unknown"),
    RUNNING("running"),
    CYCLING("cycling"),
    TREADMILL("treadmill"),
    ELLIPTICAL("elliptical"),
    WEIGHTS("weights"),
    WORKOUT("workout"),
    WALKING("walking"),
    ROWING("rowing"),
    HIKING("hiking"),
    YOGA("yoga"),
    SWIMMING("swimming"),
    AEROBIC("aerobic"),
    SPINNING("spinning");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final ac5 a(String str) {
            ac5 ac5;
            ee7.b(str, "value");
            ac5[] values = ac5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    ac5 = null;
                    break;
                }
                ac5 = values[i];
                String mValue = ac5.getMValue();
                String lowerCase = str.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (ee7.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return ac5 != null ? ac5 : ac5.UNKNOWN;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final ac5 a(int i) {
            if (i == WorkoutType.RUNNING.ordinal()) {
                return ac5.RUNNING;
            }
            if (i == WorkoutType.CYCLING.ordinal()) {
                return ac5.CYCLING;
            }
            if (i == WorkoutType.TREADMILL.ordinal()) {
                return ac5.TREADMILL;
            }
            if (i == WorkoutType.ELLIPTICAL.ordinal()) {
                return ac5.ELLIPTICAL;
            }
            if (i == WorkoutType.WEIGHTS.ordinal()) {
                return ac5.WEIGHTS;
            }
            if (i == WorkoutType.WORKOUT.ordinal()) {
                return ac5.WORKOUT;
            }
            if (i == WorkoutType.WALKING.ordinal()) {
                return ac5.WALKING;
            }
            if (i == WorkoutType.ROWING.ordinal()) {
                return ac5.ROWING;
            }
            if (i == WorkoutType.HIKING.ordinal()) {
                return ac5.HIKING;
            }
            if (i == WorkoutType.YOGA.ordinal()) {
                return ac5.YOGA;
            }
            if (i == WorkoutType.SWIMMING.ordinal()) {
                return ac5.SWIMMING;
            }
            if (i == WorkoutType.AEROBIC.ordinal()) {
                return ac5.AEROBIC;
            }
            if (i == WorkoutType.SPINNING.ordinal()) {
                return ac5.SPINNING;
            }
            return ac5.UNKNOWN;
        }

        @DexIgnore
        public final WorkoutType a(ac5 ac5) {
            if (ac5 != null) {
                switch (zb5.a[ac5.ordinal()]) {
                    case 1:
                        return WorkoutType.RUNNING;
                    case 2:
                        return WorkoutType.CYCLING;
                    case 3:
                        return WorkoutType.TREADMILL;
                    case 4:
                        return WorkoutType.ELLIPTICAL;
                    case 5:
                        return WorkoutType.WEIGHTS;
                    case 6:
                        return WorkoutType.WORKOUT;
                    case 7:
                        return WorkoutType.WALKING;
                    case 8:
                        return WorkoutType.ROWING;
                    case 9:
                        return WorkoutType.HIKING;
                    case 10:
                        return WorkoutType.YOGA;
                    case 11:
                        return WorkoutType.SWIMMING;
                    case 12:
                        return WorkoutType.AEROBIC;
                    case 13:
                        return WorkoutType.SPINNING;
                }
            }
            return WorkoutType.UNKNOWN;
        }
    }

    @DexIgnore
    public ac5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
