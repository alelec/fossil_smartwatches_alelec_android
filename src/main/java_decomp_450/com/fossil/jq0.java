package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jq0 extends zk1 {
    @DexIgnore
    public /* final */ boolean U; // = true;
    @DexIgnore
    public HashMap<o80, n80> V;

    @DexIgnore
    public jq0(ri1 ri1, en0 en0, short s) {
        super(ri1, en0, wm0.v, s, oa7.b(w87.a(xf0.SKIP_ERASE, true), w87.a(xf0.NUMBER_OF_FILE_REQUIRED, 1), w87.a(xf0.ERASE_CACHE_FILE_BEFORE_GET, true)), 1.0f, null, 64);
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void a(ArrayList<bi1> arrayList) {
        a(((zk0) this).v);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.U;
    }

    @DexIgnore
    @Override // com.fossil.zk1
    public void c(bi1 bi1) {
        is0 is0;
        super.c(bi1);
        try {
            this.V = (HashMap) wt0.f.a(bi1.e);
            is0 = is0.SUCCESS;
        } catch (f41 e) {
            wl0.h.a(e);
            is0 = is0.UNSUPPORTED_FORMAT;
        }
        ((zk0) this).v = eu0.a(((zk0) this).v, null, is0, null, 5);
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public Object d() {
        HashMap<o80, n80> hashMap = this.V;
        return hashMap != null ? hashMap : new HashMap();
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.zk1
    public JSONObject k() {
        JSONArray jSONArray;
        Collection<n80> values;
        JSONObject k = super.k();
        r51 r51 = r51.Q;
        HashMap<o80, n80> hashMap = this.V;
        if (hashMap == null || (values = hashMap.values()) == null) {
            jSONArray = null;
        } else {
            Object[] array = values.toArray(new n80[0]);
            if (array != null) {
                jSONArray = yz0.a((n80[]) array);
            } else {
                throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return yz0.a(k, r51, jSONArray);
    }
}
