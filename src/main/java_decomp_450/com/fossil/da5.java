package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.uirenew.customview.TimerTextView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class da5 extends ca5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public /* final */ CardView y;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131362594, 1);
        B.put(2131362515, 2);
        B.put(2131362356, 3);
        B.put(2131362459, 4);
        B.put(2131362390, 5);
        B.put(2131362505, 6);
        B.put(2131362363, 7);
        B.put(2131362354, 8);
    }
    */

    @DexIgnore
    public da5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public da5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleTextView) objArr[8], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[5], (FlexibleTextView) objArr[4], (FlexibleTextView) objArr[6], (TimerTextView) objArr[2], (ImageView) objArr[1]);
        this.z = -1;
        CardView cardView = (CardView) objArr[0];
        this.y = cardView;
        cardView.setTag(null);
        a(view);
        f();
    }
}
