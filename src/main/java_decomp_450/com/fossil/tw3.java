package com.fossil;

import com.fossil.dz3;
import com.fossil.ez3;
import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.Serializable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tw3<E> extends ww3<E> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -2250766705698539974L;
    @DexIgnore
    public transient Map<E, fx3> c;
    @DexIgnore
    public transient long d; // = ((long) super.size());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Iterator<dz3.a<E>> {
        @DexIgnore
        public Map.Entry<E, fx3> a;
        @DexIgnore
        public /* final */ /* synthetic */ Iterator b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.tw3$a$a")
        /* renamed from: com.fossil.tw3$a$a  reason: collision with other inner class name */
        public class C0190a extends ez3.b<E> {
            @DexIgnore
            public /* final */ /* synthetic */ Map.Entry a;

            @DexIgnore
            public C0190a(Map.Entry entry) {
                this.a = entry;
            }

            @DexIgnore
            @Override // com.fossil.dz3.a
            public int getCount() {
                fx3 fx3;
                fx3 fx32 = (fx3) this.a.getValue();
                if ((fx32 == null || fx32.get() == 0) && (fx3 = (fx3) tw3.this.c.get(getElement())) != null) {
                    return fx3.get();
                }
                if (fx32 == null) {
                    return 0;
                }
                return fx32.get();
            }

            @DexIgnore
            @Override // com.fossil.dz3.a
            public E getElement() {
                return (E) this.a.getKey();
            }
        }

        @DexIgnore
        public a(Iterator it) {
            this.b = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b.hasNext();
        }

        @DexIgnore
        public void remove() {
            bx3.a(this.a != null);
            tw3.access$122(tw3.this, (long) this.a.getValue().getAndSet(0));
            this.b.remove();
            this.a = null;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public dz3.a<E> next() {
            Map.Entry<E, fx3> entry = (Map.Entry) this.b.next();
            this.a = entry;
            return new C0190a(entry);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Iterator<E> {
        @DexIgnore
        public /* final */ Iterator<Map.Entry<E, fx3>> a;
        @DexIgnore
        public Map.Entry<E, fx3> b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b() {
            this.a = tw3.this.c.entrySet().iterator();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c > 0 || this.a.hasNext();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public E next() {
            if (this.c == 0) {
                Map.Entry<E, fx3> next = this.a.next();
                this.b = next;
                this.c = next.getValue().get();
            }
            this.c--;
            this.d = true;
            return this.b.getKey();
        }

        @DexIgnore
        public void remove() {
            bx3.a(this.d);
            if (this.b.getValue().get() > 0) {
                if (this.b.getValue().addAndGet(-1) == 0) {
                    this.a.remove();
                }
                tw3.access$110(tw3.this);
                this.d = false;
                return;
            }
            throw new ConcurrentModificationException();
        }
    }

    @DexIgnore
    public tw3(Map<E, fx3> map) {
        jw3.a(map);
        this.c = map;
    }

    @DexIgnore
    public static int a(fx3 fx3, int i) {
        if (fx3 == null) {
            return 0;
        }
        return fx3.getAndSet(i);
    }

    @DexIgnore
    public static /* synthetic */ long access$110(tw3 tw3) {
        long j = tw3.d;
        tw3.d = j - 1;
        return j;
    }

    @DexIgnore
    public static /* synthetic */ long access$122(tw3 tw3, long j) {
        long j2 = tw3.d - j;
        tw3.d = j2;
        return j2;
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    @CanIgnoreReturnValue
    public int add(E e, int i) {
        if (i == 0) {
            return count(e);
        }
        boolean z = true;
        int i2 = 0;
        jw3.a(i > 0, "occurrences cannot be negative: %s", i);
        fx3 fx3 = this.c.get(e);
        if (fx3 == null) {
            this.c.put(e, new fx3(i));
        } else {
            int i3 = fx3.get();
            long j = ((long) i3) + ((long) i);
            if (j > 2147483647L) {
                z = false;
            }
            jw3.a(z, "too many occurrences: %s", j);
            fx3.add(i);
            i2 = i3;
        }
        this.d += (long) i;
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public void clear() {
        for (fx3 fx3 : this.c.values()) {
            fx3.set(0);
        }
        this.c.clear();
        this.d = 0;
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    public int count(Object obj) {
        fx3 fx3 = (fx3) yy3.e(this.c, obj);
        if (fx3 == null) {
            return 0;
        }
        return fx3.get();
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public int distinctElements() {
        return this.c.size();
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public Iterator<dz3.a<E>> entryIterator() {
        return new a(this.c.entrySet().iterator());
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    public Set<dz3.a<E>> entrySet() {
        return super.entrySet();
    }

    @DexIgnore
    @Override // java.util.AbstractCollection, com.fossil.ww3, java.util.Collection, java.lang.Iterable
    public Iterator<E> iterator() {
        return new b();
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    @CanIgnoreReturnValue
    public int remove(Object obj, int i) {
        if (i == 0) {
            return count(obj);
        }
        jw3.a(i > 0, "occurrences cannot be negative: %s", i);
        fx3 fx3 = this.c.get(obj);
        if (fx3 == null) {
            return 0;
        }
        int i2 = fx3.get();
        if (i2 <= i) {
            this.c.remove(obj);
            i = i2;
        }
        fx3.add(-i);
        this.d -= (long) i;
        return i2;
    }

    @DexIgnore
    public void setBackingMap(Map<E, fx3> map) {
        this.c = map;
    }

    @DexIgnore
    @Override // com.fossil.dz3, com.fossil.ww3
    @CanIgnoreReturnValue
    public int setCount(E e, int i) {
        int i2;
        bx3.a(i, "count");
        if (i == 0) {
            i2 = a(this.c.remove(e), i);
        } else {
            fx3 fx3 = this.c.get(e);
            int a2 = a(fx3, i);
            if (fx3 == null) {
                this.c.put(e, new fx3(i));
            }
            i2 = a2;
        }
        this.d += (long) (i - i2);
        return i2;
    }

    @DexIgnore
    @Override // com.fossil.ww3
    public int size() {
        return x04.a(this.d);
    }
}
