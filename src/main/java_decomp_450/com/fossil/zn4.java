package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn4 {
    @DexIgnore
    public /* final */ Gson a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends TypeToken<List<? extends jn4>> {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends jn4>> {
    }

    @DexIgnore
    public zn4() {
        Gson a2 = new be4().a();
        ee7.a((Object) a2, "GsonBuilder().create()");
        this.a = a2;
    }

    @DexIgnore
    public final String a(jn4 jn4) {
        try {
            String a2 = this.a.a(jn4, jn4.class);
            ee7.a((Object) a2, "gson.toJson(player, BCPlayer::class.java)");
            return a2;
        } catch (Exception unused) {
            return "";
        }
    }

    @DexIgnore
    public final jn4 b(String str) {
        ee7.b(str, "value");
        try {
            return (jn4) this.a.a(str, jn4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final List<jn4> c(String str) {
        List<jn4> list;
        try {
            Object a2 = this.a.a(str, new a().getType());
            ee7.a(a2, "gson.fromJson(value, obj\u2026ist<BCPlayer>>() {}.type)");
            return (List) a2;
        } catch (Exception e) {
            if (e instanceof oe4) {
                try {
                    FLogger.INSTANCE.getLocal().e("HistoryChallengeConverter", "stringToPlayers - apply date format for special case");
                    be4 be4 = new be4();
                    be4.a("MM dd, yyyy HH:mm:ss");
                    list = (List) be4.a().a(str, new b().getType());
                } catch (Exception e2) {
                    e2.printStackTrace();
                    list = w97.a();
                }
                ee7.a((Object) list, "try {\n                  \u2026ayer>()\n                }");
            } else {
                list = w97.a();
            }
            return list;
        }
    }

    @DexIgnore
    public final String a(List<jn4> list) {
        ee7.b(list, "players");
        String a2 = new Gson().a(list);
        ee7.a((Object) a2, "Gson().toJson(players)");
        return a2;
    }

    @DexIgnore
    public final eo4 a(String str) {
        ee7.b(str, "value");
        try {
            return (eo4) this.a.a(str, eo4.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public final String a(eo4 eo4) {
        ee7.b(eo4, "owner");
        try {
            return this.a.a(eo4, eo4.class);
        } catch (Exception unused) {
            return null;
        }
    }
}
