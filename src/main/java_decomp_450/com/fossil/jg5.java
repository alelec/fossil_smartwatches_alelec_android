package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.NativeProtocol;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import io.flutter.plugin.common.StandardMessageCodec;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jg5 extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a(null);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public WeakReference<Activity> b;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public /* final */ Application.ActivityLifecycleCallbacks e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ GuestApiService g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jg5.h;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:23:0x007a  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0080  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final boolean a(java.lang.String r6, java.lang.String r7) {
            /*
                r5 = this;
                java.lang.String r0 = "filePath"
                com.fossil.ee7.b(r6, r0)
                if (r7 != 0) goto L_0x0009
                r6 = 1
                return r6
            L_0x0009:
                r0 = 0
                r1 = 0
                java.lang.String r2 = "MD5"
                java.security.MessageDigest r2 = java.security.MessageDigest.getInstance(r2)     // Catch:{ Exception -> 0x004d }
                r3 = 2014(0x7de, float:2.822E-42)
                byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x004d }
                java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x004d }
                r4.<init>(r6)     // Catch:{ Exception -> 0x004d }
            L_0x001a:
                int r6 = r4.read(r3)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                r0 = -1
                if (r6 != r0) goto L_0x003f
                byte[] r6 = r2.digest()     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                java.lang.String r0 = "digest.digest()"
                com.fossil.ee7.a(r6, r0)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                java.lang.String r6 = r5.a(r6)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                java.lang.String r7 = r7.toLowerCase()     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                java.lang.String r0 = "(this as java.lang.String).toLowerCase()"
                com.fossil.ee7.a(r7, r0)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                boolean r6 = com.fossil.ee7.a(r7, r6)     // Catch:{ Exception -> 0x0048, all -> 0x0045 }
                r4.close()
                return r6
            L_0x003f:
                if (r6 <= 0) goto L_0x001a
                r2.update(r3, r1, r6)
                goto L_0x001a
            L_0x0045:
                r6 = move-exception
                r0 = r4
                goto L_0x007e
            L_0x0048:
                r6 = move-exception
                r0 = r4
                goto L_0x004e
            L_0x004b:
                r6 = move-exception
                goto L_0x007e
            L_0x004d:
                r6 = move-exception
            L_0x004e:
                com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x004b }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()     // Catch:{ all -> 0x004b }
                java.lang.String r2 = r5.a()     // Catch:{ all -> 0x004b }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x004b }
                r3.<init>()     // Catch:{ all -> 0x004b }
                java.lang.String r4 = "Error inside "
                r3.append(r4)     // Catch:{ all -> 0x004b }
                java.lang.String r4 = r5.a()     // Catch:{ all -> 0x004b }
                r3.append(r4)     // Catch:{ all -> 0x004b }
                java.lang.String r4 = ".verifyDownloadFile - e="
                r3.append(r4)     // Catch:{ all -> 0x004b }
                r3.append(r6)     // Catch:{ all -> 0x004b }
                java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x004b }
                r7.e(r2, r6)     // Catch:{ all -> 0x004b }
                if (r0 == 0) goto L_0x007d
                r0.close()
            L_0x007d:
                return r1
            L_0x007e:
                if (r0 == 0) goto L_0x0083
                r0.close()
            L_0x0083:
                throw r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.jg5.a.a(java.lang.String, java.lang.String):boolean");
        }

        @DexIgnore
        public final String a(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & 255) + StandardMessageCodec.NULL, 16);
                ee7.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    ee7.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new x87("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            ee7.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ jg5 c;

        @DexIgnore
        public c(jg5 jg5, String str, boolean z) {
            ee7.b(str, "mFilePath");
            this.c = jg5;
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            ee7.b(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            boolean z = true;
            try {
                if (ee7.a((Object) ig5.b(), (Object) "en_US")) {
                    mg5.a(this.a, this.b);
                    mg5.a(this.c.d() + "/strings.json", true);
                } else {
                    mg5.a(this.a, this.b);
                }
                mg5.b();
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(jg5.i.a(), "load cache failed e=" + e);
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public /* final */ /* synthetic */ jg5 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(fb7 fb7, f fVar) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.this$0.a.g().getFilesDir();
                    ee7.a((Object) filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    File file = new File(sb2);
                    if (!file.exists()) {
                        jg5 jg5 = this.this$0.a;
                        this.L$0 = yi7;
                        this.L$1 = sb2;
                        this.L$2 = file;
                        this.label = 1;
                        if (jg5.a(this) == a) {
                            return a;
                        }
                    }
                } else if (i == 1) {
                    File file2 = (File) this.L$2;
                    String str = (String) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(jg5 jg5) {
            this.a = jg5;
        }

        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Class<?> cls;
            if (activity != null) {
                this.a.a(new WeakReference<>(activity));
                WeakReference<Activity> h = this.a.h();
                if (h != null) {
                    Activity activity2 = h.get();
                    if (ee7.a((Object) ((activity2 == null || (cls = activity2.getClass()) == null) ? null : cls.getSimpleName()), (Object) this.a.f())) {
                        ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new a(null, this), 3, null);
                        return;
                    }
                    return;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            ee7.b(activity, Constants.ACTIVITY);
            if (this.a.h() != null) {
                WeakReference<Activity> h = this.a.h();
                if (h == null) {
                    ee7.a();
                    throw null;
                } else if (ee7.a(h.get(), activity)) {
                    this.a.a((WeakReference<Activity>) null);
                }
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            ee7.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
            ee7.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            ee7.b(activity, Constants.ACTIVITY);
            ee7.b(bundle, "bundle");
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            ee7.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            ee7.b(activity, Constants.ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.localization.LocalizationManager", f = "LocalizationManager.kt", l = {194}, m = "downloadLanguagePack")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ jg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(jg5 jg5, fb7 fb7) {
            super(fb7);
            this.this$0 = jg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.localization.LocalizationManager$downloadLanguagePack$response$1", f = "LocalizationManager.kt", l = {194}, m = "invokeSuspend")
    public static final class h extends zb7 implements gd7<fb7<? super fv7<ApiResponse<ie4>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ jg5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(jg5 jg5, fb7 fb7) {
            super(1, fb7);
            this.this$0 = jg5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(fb7<?> fb7) {
            ee7.b(fb7, "completion");
            return new h(this.this$0, fb7);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public final Object invoke(fb7<? super fv7<ApiResponse<ie4>>> fb7) {
            return ((h) create(fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                GuestApiService b = this.this$0.g;
                String a2 = this.this$0.f;
                this.label = 1;
                obj = b.getLocalizations(a2, "android", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        String simpleName = jg5.class.getSimpleName();
        ee7.a((Object) simpleName, "LocalizationManager::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public jg5(Application application, String str, GuestApiService guestApiService) {
        ee7.b(application, "app");
        ee7.b(str, "mAppVersion");
        ee7.b(guestApiService, "mGuestApiService");
        this.f = str;
        this.g = guestApiService;
        ee7.a((Object) application.getSharedPreferences(application.getPackageName() + ".language", 0), "app.getSharedPreferences\u2026e\", Context.MODE_PRIVATE)");
        this.a = application;
        this.e = new f(this);
    }

    @DexIgnore
    public final String c() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        ee7.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append(e());
        return sb.toString();
    }

    @DexIgnore
    public final String d() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        ee7.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append("/");
        sb.append("language");
        sb.append("/values");
        return sb.toString();
    }

    @DexIgnore
    public final String e() {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        ee7.a((Object) filesDir, "mContext.filesDir");
        sb2.append(filesDir.getAbsolutePath());
        sb2.append("/");
        sb2.append("language");
        File file = new File(sb2.toString());
        if (file.exists()) {
            String[] list = file.list();
            ee7.a((Object) list, "file.list()");
            Locale a2 = ig5.a();
            int length = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = list[i2];
                ee7.a((Object) a2, "l");
                String language = a2.getLanguage();
                ee7.a((Object) language, "l.language");
                if (nh7.a((CharSequence) str, (CharSequence) language, false, 2, (Object) null)) {
                    String sb3 = sb.toString();
                    ee7.a((Object) sb3, "path.toString()");
                    String language2 = a2.getLanguage();
                    ee7.a((Object) language2, "l.language");
                    if (!nh7.a((CharSequence) sb3, (CharSequence) language2, false, 2, (Object) null)) {
                        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb.append(a2.getLanguage());
                    }
                    String country = a2.getCountry();
                    ee7.a((Object) country, "l.country");
                    if (nh7.a((CharSequence) str, (CharSequence) country, false, 2, (Object) null)) {
                        sb.append("-r");
                        sb.append(a2.getCountry());
                        break;
                    }
                }
                i2++;
            }
        }
        sb.insert(0, "language/values");
        sb.append("/strings.json");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "path=" + sb.toString());
        String sb4 = sb.toString();
        ee7.a((Object) sb4, "path.toString()");
        return sb4;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final Context g() {
        return this.a;
    }

    @DexIgnore
    public final WeakReference<Activity> h() {
        return this.b;
    }

    @DexIgnore
    public final void i() {
        mg5.a();
        String c2 = c();
        if (new File(c2).exists()) {
            a(c2, true);
        } else {
            a(e(), false);
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Activity activity;
        ee7.b(context, "context");
        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && ee7.a((Object) intent.getAction(), (Object) "android.intent.action.LOCALE_CHANGED")) {
            String c2 = PortfolioApp.g0.c().c();
            if (be5.o.f(c2)) {
                PortfolioApp.g0.c().p(c2);
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "onReceive locale=" + ig5.b());
            WeakReference<Activity> weakReference = this.b;
            if (!(weakReference == null || (activity = weakReference.get()) == null)) {
                ee7.a((Object) activity, "it");
                if (!activity.isFinishing()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = h;
                    local2.d(str2, "onReceive finish activity=" + activity.getLocalClassName());
                    activity.setResult(0);
                    activity.finishAffinity();
                }
            }
            PortfolioApp.g0.c().B();
            i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            ee7.b(strArr, NativeProtocol.WEB_DIALOG_PARAMS);
            String str = strArr[0];
            jg5 jg5 = jg5.this;
            return Boolean.valueOf(jg5.a(jg5.g()));
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    jg5.this.a(new File(jg5.this.g().getFilesDir().toString() + ""));
                    jg5 jg5 = jg5.this;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = jg5.this.g().getFilesDir();
                    ee7.a((Object) filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append(jg5.this.e());
                    jg5.a(sb.toString(), true);
                }
                d b = jg5.this.b();
                if (b != null) {
                    b.a(bool.booleanValue());
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        this.b = weakReference;
    }

    @DexIgnore
    public final d b() {
        return this.d;
    }

    @DexIgnore
    public final Application.ActivityLifecycleCallbacks a() {
        return this.e;
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        ee7.b(str, "path");
        new c(this, str, z).execute(new String[0]);
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "splashScreen");
        this.c = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r8) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.fossil.jg5.g
            if (r0 == 0) goto L_0x0013
            r0 = r8
            com.fossil.jg5$g r0 = (com.fossil.jg5.g) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.jg5$g r0 = new com.fossil.jg5$g
            r0.<init>(r7, r8)
        L_0x0018:
            java.lang.Object r8 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.jg5 r0 = (com.fossil.jg5) r0
            com.fossil.t87.a(r8)
            goto L_0x0057
        L_0x002d:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r8.<init>(r0)
            throw r8
        L_0x0035:
            com.fossil.t87.a(r8)
            com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
            java.lang.String r2 = com.fossil.jg5.h
            java.lang.String r4 = "downloadLanguagePack() called"
            r8.d(r2, r4)
            com.fossil.jg5$h r8 = new com.fossil.jg5$h
            r2 = 0
            r8.<init>(r7, r2)
            r0.L$0 = r7
            r0.label = r3
            java.lang.Object r8 = com.fossil.aj5.a(r8, r0)
            if (r8 != r1) goto L_0x0056
            return r1
        L_0x0056:
            r0 = r7
        L_0x0057:
            com.fossil.zi5 r8 = (com.fossil.zi5) r8
            boolean r1 = r8 instanceof com.fossil.bj5
            r2 = 0
            if (r1 == 0) goto L_0x00b4
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r4 = com.fossil.jg5.h
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "download language success isFromCache "
            r5.append(r6)
            com.fossil.bj5 r8 = (com.fossil.bj5) r8
            boolean r6 = r8.b()
            r5.append(r6)
            java.lang.String r5 = r5.toString()
            r1.d(r4, r5)
            boolean r1 = r8.b()
            if (r1 != 0) goto L_0x00ac
            java.lang.Object r8 = r8.a()
            com.portfolio.platform.data.source.remote.ApiResponse r8 = (com.portfolio.platform.data.source.remote.ApiResponse) r8
            if (r8 == 0) goto L_0x00ac
            java.util.List r8 = r8.get_items()
            if (r8 == 0) goto L_0x00ac
            boolean r1 = r8.isEmpty()
            r1 = r1 ^ r3
            if (r1 == 0) goto L_0x00ac
            com.fossil.kg5 r1 = new com.fossil.kg5
            r1.<init>()
            java.lang.Object r8 = r8.get(r2)
            com.fossil.ie4 r8 = (com.fossil.ie4) r8
            r1.a(r8)
            r0.a(r1)
        L_0x00ac:
            com.fossil.jg5$d r8 = r0.d
            if (r8 == 0) goto L_0x00bf
            r8.a(r2)
            goto L_0x00bf
        L_0x00b4:
            boolean r8 = r8 instanceof com.fossil.yi5
            if (r8 == 0) goto L_0x00bf
            com.fossil.jg5$d r8 = r0.d
            if (r8 == 0) goto L_0x00bf
            r8.a(r2)
        L_0x00bf:
            com.fossil.i97 r8 = com.fossil.i97.a
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jg5.a(com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(kg5 kg5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "downloadFile response=" + kg5);
        new b(this, this.a, kg5).execute(new String[0]);
    }

    @DexIgnore
    public final void a(File file) {
        ee7.b(file, "folder");
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles();
            for (File file2 : listFiles) {
                ee7.a((Object) file2, "f");
                if (file2.isDirectory()) {
                    a(file2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ kg5 b;
        @DexIgnore
        public /* final */ /* synthetic */ jg5 c;

        @DexIgnore
        public b(jg5 jg5, Context context, kg5 kg5) {
            ee7.b(context, "context");
            ee7.b(kg5, Firmware.COLUMN_DOWNLOAD_URL);
            this.c = jg5;
            this.a = context;
            this.b = kg5;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            ee7.b(strArr, "links");
            if (!TextUtils.isEmpty(this.b.b())) {
                try {
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.a.getFilesDir();
                    ee7.a((Object) filesDir, "context.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append("/");
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    if (new File(sb2).exists() && jg5.i.a(sb2, this.b.a())) {
                        return true;
                    }
                    URLConnection openConnection = new URL(this.b.b()).openConnection();
                    openConnection.connect();
                    BufferedInputStream bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                    FileOutputStream fileOutputStream = new FileOutputStream(sb2);
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                break;
                            } else if (isCancelled()) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                bufferedInputStream.close();
                                return false;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    } catch (Throwable th) {
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        bufferedInputStream.close();
                        throw th;
                    }
                    bufferedInputStream.close();
                    if (TextUtils.isEmpty(this.b.a())) {
                        FLogger.INSTANCE.getLocal().e(jg5.i.a(), "Download complete with risk cause by empty checksum");
                        return true;
                    } else if (jg5.i.a(sb2, this.b.a())) {
                        return true;
                    } else {
                        FLogger.INSTANCE.getLocal().e(jg5.i.a(), "Inconsistent checksum, retry download?");
                        return true;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = jg5.i.a();
                    local.e(a2, "Error inside " + jg5.i.a() + ".onHandleIntent - e=" + e2);
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    new e().execute(this.b.c());
                }
                d b2 = this.c.b();
                if (b2 != null) {
                    b2.a(false);
                    return;
                }
                return;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Context r10) {
        /*
            r9 = this;
            java.lang.String r0 = "/"
            java.lang.String r1 = "ctx.filesDir"
            java.lang.String r2 = "ctx"
            com.fossil.ee7.b(r10, r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.jg5.h
            java.lang.String r4 = "unzipFile"
            r2.d(r3, r4)
            r2 = 2048(0x800, float:2.87E-42)
            byte[] r2 = new byte[r2]
            r3 = 0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe }
            r4.<init>()     // Catch:{ Exception -> 0x00fe }
            java.io.File r5 = r10.getFilesDir()     // Catch:{ Exception -> 0x00fe }
            com.fossil.ee7.a(r5, r1)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x00fe }
            r4.append(r5)     // Catch:{ Exception -> 0x00fe }
            r4.append(r0)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00fe }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe }
            r5.<init>()     // Catch:{ Exception -> 0x00fe }
            java.io.File r10 = r10.getFilesDir()     // Catch:{ Exception -> 0x00fe }
            com.fossil.ee7.a(r10, r1)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r10 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x00fe }
            r5.append(r10)     // Catch:{ Exception -> 0x00fe }
            r5.append(r0)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r10 = "language.zip"
            r5.append(r10)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r10 = r5.toString()     // Catch:{ Exception -> 0x00fe }
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x00fe }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ Exception -> 0x00fe }
            java.lang.String r1 = com.fossil.jg5.h     // Catch:{ Exception -> 0x00fe }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00fe }
            r5.<init>()     // Catch:{ Exception -> 0x00fe }
            java.lang.String r6 = "filePath="
            r5.append(r6)     // Catch:{ Exception -> 0x00fe }
            r5.append(r10)     // Catch:{ Exception -> 0x00fe }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x00fe }
            r0.d(r1, r5)     // Catch:{ Exception -> 0x00fe }
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x00fe }
            r0.<init>(r10)     // Catch:{ Exception -> 0x00fe }
            boolean r0 = r0.exists()     // Catch:{ Exception -> 0x00fe }
            if (r0 != 0) goto L_0x007c
            return r3
        L_0x007c:
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00fe }
            r0.<init>(r10)     // Catch:{ Exception -> 0x00fe }
            java.util.zip.ZipInputStream r10 = new java.util.zip.ZipInputStream     // Catch:{ Exception -> 0x00fe }
            r10.<init>(r0)     // Catch:{ Exception -> 0x00fe }
            r1 = 0
            r5 = r1
        L_0x0088:
            java.util.zip.ZipEntry r5 = r10.getNextEntry()     // Catch:{ Exception -> 0x00d9 }
            if (r5 != 0) goto L_0x009a
            r10.close()
            r0.close()
            if (r1 == 0) goto L_0x00eb
        L_0x0096:
            r1.close()
            goto L_0x00eb
        L_0x009a:
            boolean r6 = r5.isDirectory()
            if (r6 == 0) goto L_0x00ad
            java.lang.String r6 = r5.getName()
            java.lang.String r7 = "ze.name"
            com.fossil.ee7.a(r6, r7)
            r9.a(r4, r6)
            goto L_0x0088
        L_0x00ad:
            java.io.FileOutputStream r6 = new java.io.FileOutputStream
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r4)
            java.lang.String r8 = r5.getName()
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
        L_0x00c5:
            int r1 = r10.read(r2)     // Catch:{ Exception -> 0x00d4, all -> 0x00d1 }
            if (r1 > 0) goto L_0x00cd
            r1 = r6
            goto L_0x0088
        L_0x00cd:
            r6.write(r2, r3, r1)     // Catch:{ Exception -> 0x00d4, all -> 0x00d1 }
            goto L_0x00c5
        L_0x00d1:
            r2 = move-exception
            r1 = r6
            goto L_0x00ed
        L_0x00d4:
            r2 = move-exception
            r1 = r6
            goto L_0x00da
        L_0x00d7:
            r2 = move-exception
            goto L_0x00ed
        L_0x00d9:
            r2 = move-exception
        L_0x00da:
            r2.printStackTrace()     // Catch:{ all -> 0x00d7 }
            r10.close()
            r0.close()
            if (r5 == 0) goto L_0x00e8
            r10.closeEntry()
        L_0x00e8:
            if (r1 == 0) goto L_0x00eb
            goto L_0x0096
        L_0x00eb:
            r10 = 1
            return r10
        L_0x00ed:
            r10.close()
            r0.close()
            if (r5 == 0) goto L_0x00f8
            r10.closeEntry()
        L_0x00f8:
            if (r1 == 0) goto L_0x00fd
            r1.close()
        L_0x00fd:
            throw r2
        L_0x00fe:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.fossil.jg5.h
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = "Unzipping failed ex="
            r2.append(r4)
            r2.append(r10)
            java.lang.String r10 = r2.toString()
            r0.e(r1, r10)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jg5.a(android.content.Context):boolean");
    }

    @DexIgnore
    public final void a(String str, String str2) {
        File file = new File(str + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    @DexIgnore
    public final jg5 a(d dVar) {
        this.d = dVar;
        return this;
    }
}
