package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Member;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class z04 extends AccessibleObject implements Member {
    @DexIgnore
    public /* final */ AccessibleObject a;
    @DexIgnore
    public /* final */ Member b;

    @DexIgnore
    public <M extends AccessibleObject & Member> z04(M m) {
        jw3.a(m);
        this.a = m;
        this.b = m;
    }

    @DexIgnore
    public f14<?> a() {
        return f14.of((Class) getDeclaringClass());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof z04)) {
            return false;
        }
        z04 z04 = (z04) obj;
        if (!a().equals(z04.a()) || !this.b.equals(z04.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject, java.lang.reflect.AnnotatedElement
    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return (A) this.a.getAnnotation(cls);
    }

    @DexIgnore
    public final Annotation[] getAnnotations() {
        return this.a.getAnnotations();
    }

    @DexIgnore
    public final Annotation[] getDeclaredAnnotations() {
        return this.a.getDeclaredAnnotations();
    }

    @DexIgnore
    @Override // java.lang.reflect.Member
    public Class<?> getDeclaringClass() {
        return this.b.getDeclaringClass();
    }

    @DexIgnore
    public final int getModifiers() {
        return this.b.getModifiers();
    }

    @DexIgnore
    public final String getName() {
        return this.b.getName();
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public final boolean isAccessible() {
        return this.a.isAccessible();
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject, java.lang.reflect.AnnotatedElement
    public final boolean isAnnotationPresent(Class<? extends Annotation> cls) {
        return this.a.isAnnotationPresent(cls);
    }

    @DexIgnore
    public final boolean isSynthetic() {
        return this.b.isSynthetic();
    }

    @DexIgnore
    @Override // java.lang.reflect.AccessibleObject
    public final void setAccessible(boolean z) throws SecurityException {
        this.a.setAccessible(z);
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }
}
