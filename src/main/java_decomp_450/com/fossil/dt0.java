package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt0 {
    @DexIgnore
    public /* final */ qk1 a;
    @DexIgnore
    public byte[] b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public dt0(qk1 qk1, byte[] bArr, byte[] bArr2, int i, int i2) {
        this.a = qk1;
        this.b = bArr;
        this.c = bArr2;
        this.d = i;
        this.e = i2;
    }

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = new byte[8];
        byte[] bArr2 = new byte[8];
        bArr[0] = this.a.b;
        System.arraycopy(this.b, 0, bArr, 2, 6);
        System.arraycopy(this.c, 0, bArr2, 1, 7);
        yz0.b(bArr, this.d);
        yz0.b(bArr2, this.e);
        return s97.a(bArr, bArr2);
    }
}
