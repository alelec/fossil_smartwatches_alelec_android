package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.fossil.cy6;
import com.fossil.oy6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputActivity;
import com.portfolio.platform.buddy_challenge.screens.create_invite.BCInviteFriendActivity;
import com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeActivity;
import com.portfolio.platform.buddy_challenge.util.TimerViewObserver;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.uirenew.customview.FriendsInView;
import com.portfolio.platform.uirenew.home.HomeActivity;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr4 extends go5 implements cy6.g {
    @DexIgnore
    public static /* final */ int A; // = v6.a(PortfolioApp.g0.c(), 2131099677);
    @DexIgnore
    public static /* final */ String B; // = eh5.l.a().b("nonBrandPlaceholderBackground");
    @DexIgnore
    public static /* final */ a C; // = new a(null);
    @DexIgnore
    public static /* final */ String y;
    @DexIgnore
    public static /* final */ int z; // = v6.a(PortfolioApp.g0.c(), 2131099689);
    @DexIgnore
    public rj4 f;
    @DexIgnore
    public mr4 g;
    @DexIgnore
    public mn4 h;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j; // = -1;
    @DexIgnore
    public String p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public qw6<w65> r;
    @DexIgnore
    public vc7<i97> s; // = b.INSTANCE;
    @DexIgnore
    public vc7<i97> t; // = c.INSTANCE;
    @DexIgnore
    public /* final */ TimerViewObserver u; // = new TimerViewObserver();
    @DexIgnore
    public /* final */ oy6.b v;
    @DexIgnore
    public /* final */ st4 w;
    @DexIgnore
    public HashMap x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jr4.y;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final jr4 a(mn4 mn4, String str, int i, String str2, boolean z) {
            jr4 jr4 = new jr4();
            Bundle bundle = new Bundle();
            bundle.putParcelable("challenge_extra", mn4);
            bundle.putString("category_extra", str);
            bundle.putInt("index_extra", i);
            bundle.putString("challenge_id_extra", str2);
            bundle.putBoolean("about_extra", z);
            jr4.setArguments(bundle);
            return jr4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements vc7<i97> {
        @DexIgnore
        public static /* final */ b INSTANCE; // = new b();

        @DexIgnore
        public b() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends fe7 implements vc7<i97> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements zd<v87<? extends String, ? extends String, ? extends String>> {
        @DexIgnore
        public static /* final */ d a; // = new d();

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<String, String, String> v87) {
            if (ee7.a((Object) v87.getFirst(), (Object) "bc_left_challenge_before_start")) {
                um4.a.a(v87.getFirst(), v87.getSecond(), v87.getThird(), 0, PortfolioApp.g0.c());
            } else {
                um4.a.a(v87.getFirst(), v87.getSecond(), v87.getThird(), PortfolioApp.g0.c());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements zd<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public e(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mn4 mn4) {
            if (mn4 != null) {
                this.a.b(mn4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<mn4> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public f(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(mn4 mn4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jr4.C.a();
            local.e(a2, "pendingChallenge - data: " + mn4);
            if (mn4 != null) {
                this.a.b(mn4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public g(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            w65 w65 = (w65) jr4.c(this.a).a();
            if (w65 != null) {
                boolean booleanValue = r87.getFirst().booleanValue();
                ServerError serverError = (ServerError) r87.getSecond();
                if (booleanValue) {
                    FlexibleTextView flexibleTextView = w65.y;
                    ee7.a((Object) flexibleTextView, "ftvError");
                    flexibleTextView.setVisibility(0);
                    return;
                }
                FlexibleTextView flexibleTextView2 = w65.y;
                ee7.a((Object) flexibleTextView2, "ftvError");
                String a2 = ig5.a(flexibleTextView2.getContext(), 2131886227);
                FragmentActivity activity = this.a.getActivity();
                if (activity != null) {
                    Toast.makeText(activity, a2, 1).show();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<r87<? extends Boolean, ? extends Boolean>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public h(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, Boolean> r87) {
            w65 w65;
            Boolean first = r87.getFirst();
            Boolean second = r87.getSecond();
            if (!(first == null || (w65 = (w65) jr4.c(this.a).a()) == null)) {
                SwipeRefreshLayout swipeRefreshLayout = w65.G;
                ee7.a((Object) swipeRefreshLayout, "swipeRefresh");
                swipeRefreshLayout.setRefreshing(first.booleanValue());
            }
            if (second == null) {
                return;
            }
            if (ee7.a((Object) second, (Object) true)) {
                this.a.b();
            } else if (ee7.a((Object) second, (Object) false)) {
                this.a.a();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements zd<r87<? extends Boolean, ? extends ServerError>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public i(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<Boolean, ? extends ServerError> r87) {
            String str = null;
            Intent intent = null;
            if (r87.getFirst().booleanValue()) {
                if (this.a.j != -1) {
                    intent = new Intent();
                    intent.putExtra("index_extra", this.a.j);
                }
                this.a.b(intent);
                return;
            }
            ServerError serverError = (ServerError) r87.getSecond();
            this.a.a(serverError != null ? serverError.getCode() : null, serverError != null ? serverError.getMessage() : null);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jr4.C.a();
            StringBuilder sb = new StringBuilder();
            sb.append("code: ");
            sb.append(serverError != null ? serverError.getCode() : null);
            sb.append(" - message: ");
            if (serverError != null) {
                str = serverError.getMessage();
            }
            sb.append(str);
            local.e(a2, sb.toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<T> implements zd<v87<? extends List<? extends xn4>, ? extends ServerError, ? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public j(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(v87<? extends List<xn4>, ? extends ServerError, Integer> v87) {
            w65 w65 = (w65) jr4.c(this.a).a();
            if (w65 != null) {
                List<xn4> list = (List) v87.getFirst();
                if (list != null && (!list.isEmpty())) {
                    FriendsInView friendsInView = w65.t;
                    ee7.a((Object) friendsInView, "friendsInView");
                    friendsInView.setVisibility(0);
                    w65.t.setData(list);
                }
                Integer third = v87.getThird();
                if (third != null) {
                    we7 we7 = we7.a;
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886215);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026_Subtitle__NumberMembers)");
                    String format = String.format(a2, Arrays.copyOf(new Object[]{third}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    FlexibleTextView flexibleTextView = w65.w;
                    ee7.a((Object) flexibleTextView, "ftvCountMember");
                    flexibleTextView.setText(xe5.a(xe5.b, String.valueOf(third.intValue()), format, 0, 4, null));
                    FlexibleTextView flexibleTextView2 = w65.w;
                    ee7.a((Object) flexibleTextView2, "ftvCountMember");
                    flexibleTextView2.setTag(third);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<T> implements zd<List<? extends dn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public k(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<dn4> list) {
            jr4 jr4 = this.a;
            ee7.a((Object) list, "it");
            jr4.x(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements zd<r87<? extends tt4, ? extends mn4>> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public l(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(r87<? extends tt4, mn4> r87) {
            int i = kr4.a[((tt4) r87.getFirst()).ordinal()];
            if (i == 1) {
                this.a.c(r87.getSecond());
            } else if (i == 2) {
                this.a.d(r87.getSecond());
            } else if (i != 3) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = jr4.C.a();
                local.e(a2, "optionActionLive - wrong option: " + r87);
            } else {
                this.a.i1();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<gu4> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public m(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(gu4 gu4) {
            if (gu4 != null) {
                int i = kr4.b[gu4.ordinal()];
                boolean z = true;
                if (i == 1) {
                    bx6 bx6 = bx6.c;
                    FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager, "childFragmentManager");
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886229);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026uCanOnlyJoinOneChallenge)");
                    bx6.a(childFragmentManager, a2, a3);
                    return;
                } else if (i == 2) {
                    if (PortfolioApp.g0.c().c().length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        bx6 bx62 = bx6.c;
                        FragmentManager childFragmentManager2 = this.a.getChildFragmentManager();
                        ee7.a((Object) childFragmentManager2, "childFragmentManager");
                        String a4 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                        ee7.a((Object) a4, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                        String a5 = ig5.a(PortfolioApp.g0.c(), 2131886230);
                        ee7.a((Object) a5, "LanguageHelper.getString\u2026IsDisconnectedPleaseSync)");
                        bx62.a(childFragmentManager2, a4, a5);
                        return;
                    }
                    bx6 bx63 = bx6.c;
                    FragmentManager childFragmentManager3 = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager3, "childFragmentManager");
                    String a6 = ig5.a(PortfolioApp.g0.c(), 2131886231);
                    ee7.a((Object) a6, "LanguageHelper.getString\u2026lenge_Error_Title__Error)");
                    String a7 = ig5.a(PortfolioApp.g0.c(), 2131886223);
                    ee7.a((Object) a7, "LanguageHelper.getString\u2026ctivateYourDeviceToJoinA)");
                    bx63.a(childFragmentManager3, a6, a7);
                    return;
                } else if (i == 3) {
                    bx6 bx64 = bx6.c;
                    FragmentManager childFragmentManager4 = this.a.getChildFragmentManager();
                    ee7.a((Object) childFragmentManager4, "childFragmentManager");
                    bx64.X(childFragmentManager4);
                    return;
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a8 = jr4.C.a();
            local.e(a8, "warningTypeDialogLive - wrong type: " + gu4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ mn4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ w65 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(w65 w65, String str, jr4 jr4, mn4 mn4) {
            super(0);
            this.$this_run$inlined = w65;
            this.$subject$inlined = str;
            this.this$0 = jr4;
            this.$challenge$inlined = mn4;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            jr4.g(this.this$0).a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ mn4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $subject$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ w65 $this_run$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(w65 w65, String str, jr4 jr4, mn4 mn4) {
            super(0);
            this.$this_run$inlined = w65;
            this.$subject$inlined = str;
            this.this$0 = jr4;
            this.$challenge$inlined = mn4;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            jr4.g(this.this$0).a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p extends fe7 implements vc7<i97> {
        @DexIgnore
        public /* final */ /* synthetic */ mn4 $challenge$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(jr4 jr4, mn4 mn4) {
            super(0);
            this.this$0 = jr4;
            this.$challenge$inlined = mn4;
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
            Date m = this.$challenge$inlined.m();
            this.this$0.c(this.$challenge$inlined.f(), (m != null ? m.getTime() : 0) > vt4.a.b());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q extends fe7 implements vc7<i97> {
        @DexIgnore
        public static /* final */ q INSTANCE; // = new q();

        @DexIgnore
        public q() {
            super(0);
        }

        @DexIgnore
        @Override // com.fossil.vc7
        public final void invoke() {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        public r(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.A();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class s extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(jr4 jr4) {
            super(1);
            this.this$0 = jr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.s.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class t extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(jr4 jr4) {
            super(1);
            this.this$0 = jr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            jr4.g(this.this$0).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class u implements SwipeRefreshLayout.j {
        @DexIgnore
        public /* final */ /* synthetic */ w65 a;
        @DexIgnore
        public /* final */ /* synthetic */ jr4 b;

        @DexIgnore
        public u(w65 w65, jr4 jr4) {
            this.a = w65;
            this.b = jr4;
        }

        @DexIgnore
        @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.j
        public final void a() {
            FlexibleTextView flexibleTextView = this.a.y;
            ee7.a((Object) flexibleTextView, "ftvError");
            flexibleTextView.setVisibility(8);
            jr4.g(this.b).o();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class v extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public v(jr4 jr4) {
            super(1);
            this.this$0 = jr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            this.this$0.t.invoke();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class w extends fe7 implements gd7<View, i97> {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(jr4 jr4) {
            super(1);
            this.this$0 = jr4;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ i97 invoke(View view) {
            invoke(view);
            return i97.a;
        }

        @DexIgnore
        public final void invoke(View view) {
            mn4 d = this.this$0.h;
            if (d != null) {
                Date m = d.m();
                this.this$0.c(d.f(), (m != null ? m.getTime() : 0) > vt4.a.b());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class x implements er5 {
        @DexIgnore
        public /* final */ /* synthetic */ jr4 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public x(jr4 jr4) {
            this.a = jr4;
        }

        @DexIgnore
        @Override // com.fossil.er5
        public void a(dn4 dn4) {
            ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
            mr4 g = jr4.g(this.a);
            Object a2 = dn4.a();
            if (a2 != null) {
                g.a((tt4) a2);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.buddy_challenge.util.DetailOptionDialog");
        }
    }

    /*
    static {
        String simpleName = jr4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCWaitingChallengeDetail\u2026nt::class.java.simpleName");
        y = simpleName;
    }
    */

    @DexIgnore
    public jr4() {
        oy6.b b2 = oy6.a().b();
        ee7.a((Object) b2, "TextDrawable.builder().round()");
        this.v = b2;
        this.w = st4.d.a();
    }

    @DexIgnore
    public static final /* synthetic */ qw6 c(jr4 jr4) {
        qw6<w65> qw6 = jr4.r;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ mr4 g(jr4 jr4) {
        mr4 mr4 = jr4.g;
        if (mr4 != null) {
            return mr4;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void A() {
        Context context;
        if (!(this.p == null || (context = getContext()) == null)) {
            HomeActivity.a aVar = HomeActivity.z;
            ee7.a((Object) context, "it");
            aVar.a(context, 1);
        }
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.x;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void f1() {
        mn4 mn4 = this.h;
        if (mn4 != null) {
            qe5.c.a(PortfolioApp.g0.c(), mn4.f().hashCode());
        }
        String str = this.p;
        if (str != null) {
            qe5.c.a(PortfolioApp.g0.c(), str.hashCode());
        }
    }

    @DexIgnore
    public final void g1() {
        mr4 mr4 = this.g;
        if (mr4 != null) {
            mr4.b().a(getViewLifecycleOwner(), new e(this));
            mr4 mr42 = this.g;
            if (mr42 != null) {
                LiveData<mn4> e2 = mr42.e();
                if (e2 != null) {
                    e2.a(getViewLifecycleOwner(), new f(this));
                }
                mr4 mr43 = this.g;
                if (mr43 != null) {
                    mr43.c().a(getViewLifecycleOwner(), new g(this));
                    mr4 mr44 = this.g;
                    if (mr44 != null) {
                        mr44.f().a(getViewLifecycleOwner(), new h(this));
                        mr4 mr45 = this.g;
                        if (mr45 != null) {
                            mr45.g().a(getViewLifecycleOwner(), new i(this));
                            mr4 mr46 = this.g;
                            if (mr46 != null) {
                                mr46.d().a(getViewLifecycleOwner(), new j(this));
                                mr4 mr47 = this.g;
                                if (mr47 != null) {
                                    mr47.i().a(getViewLifecycleOwner(), new k(this));
                                    mr4 mr48 = this.g;
                                    if (mr48 != null) {
                                        mr48.h().a(getViewLifecycleOwner(), new l(this));
                                        mr4 mr49 = this.g;
                                        if (mr49 != null) {
                                            mr49.j().a(getViewLifecycleOwner(), new m(this));
                                            mr4 mr410 = this.g;
                                            if (mr410 != null) {
                                                mr410.a().a(getViewLifecycleOwner(), d.a);
                                            } else {
                                                ee7.d("viewModel");
                                                throw null;
                                            }
                                        } else {
                                            ee7.d("viewModel");
                                            throw null;
                                        }
                                    } else {
                                        ee7.d("viewModel");
                                        throw null;
                                    }
                                } else {
                                    ee7.d("viewModel");
                                    throw null;
                                }
                            } else {
                                ee7.d("viewModel");
                                throw null;
                            }
                        } else {
                            ee7.d("viewModel");
                            throw null;
                        }
                    } else {
                        ee7.d("viewModel");
                        throw null;
                    }
                } else {
                    ee7.d("viewModel");
                    throw null;
                }
            } else {
                ee7.d("viewModel");
                throw null;
            }
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void h1() {
        qw6<w65> qw6 = this.r;
        if (qw6 != null) {
            w65 a2 = qw6.a();
            if (a2 != null) {
                a2.C.setOnClickListener(new r(this));
                String str = B;
                if (str != null) {
                    a2.w.setBackgroundColor(Color.parseColor(str));
                }
                FlexibleButton flexibleButton = a2.q;
                ee7.a((Object) flexibleButton, "btnAction");
                du4.a(flexibleButton, new s(this));
                RTLImageView rTLImageView = a2.B;
                ee7.a((Object) rTLImageView, "imgOption");
                du4.a(rTLImageView, new t(this));
                a2.G.setOnRefreshListener(new u(a2, this));
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "ftvCountMember");
                du4.a(flexibleTextView, new v(this));
                FriendsInView friendsInView = a2.t;
                ee7.a((Object) friendsInView, "friendsInView");
                du4.a(friendsInView, new w(this));
                return;
            }
            return;
        }
        ee7.d("binding");
        throw null;
    }

    @DexIgnore
    public final void i1() {
        bx6 bx6 = bx6.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        bx6.d(childFragmentManager);
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        this.h = arguments != null ? (mn4) arguments.getParcelable("challenge_extra") : null;
        Bundle arguments2 = getArguments();
        this.i = arguments2 != null ? arguments2.getString("category_extra") : null;
        Bundle arguments3 = getArguments();
        this.j = arguments3 != null ? arguments3.getInt("index_extra") : -1;
        Bundle arguments4 = getArguments();
        this.p = arguments4 != null ? arguments4.getString("challenge_id_extra") : null;
        Bundle arguments5 = getArguments();
        this.q = arguments5 != null ? arguments5.getBoolean("about_extra") : false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = y;
        local.e(str, "onCreate - challenge: " + this.h + " - category: " + this.i + " - index: " + this.j + " - visitId: " + this.p + " - about: " + this.q);
        f1();
        PortfolioApp.g0.c().f().q().a(this);
        rj4 rj4 = this.f;
        if (rj4 != null) {
            he a2 = je.a(this, rj4).a(mr4.class);
            ee7.a((Object) a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.g = (mr4) a2;
            getLifecycle().a(this.u);
            return;
        }
        ee7.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        w65 w65 = (w65) qb.a(layoutInflater, 2131558634, viewGroup, false, a1());
        this.r = new qw6<>(this, w65);
        ee7.a((Object) w65, "binding");
        return w65.d();
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        mr4 mr4 = this.g;
        if (mr4 != null) {
            mr4.n();
        } else {
            ee7.d("viewModel");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        mr4 mr4 = this.g;
        if (mr4 != null) {
            mr4.p();
            qd5 c2 = qd5.f.c();
            FragmentActivity activity = getActivity();
            if (activity != null) {
                c2.a("bc_challenge_detail", activity);
                return;
            }
            throw new x87("null cannot be cast to non-null type android.app.Activity");
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        mr4 mr4 = this.g;
        if (mr4 != null) {
            mr4.a(this.h, this.p);
            h1();
            g1();
            mn4 mn4 = this.h;
            if (mn4 != null) {
                b(mn4);
                return;
            }
            return;
        }
        ee7.d("viewModel");
        throw null;
    }

    @DexIgnore
    public final void x(List<dn4> list) {
        bn4 b2 = bn4.z.b();
        String a2 = ig5.a(requireContext(), 2131886318);
        ee7.a((Object) a2, "LanguageHelper.getString\u2026rBoard_Menu_Title__Allow)");
        b2.setTitle(a2);
        b2.x(list);
        b2.a(new x(this));
        FragmentManager childFragmentManager = getChildFragmentManager();
        ee7.a((Object) childFragmentManager, "childFragmentManager");
        b2.show(childFragmentManager, bn4.z.a());
    }

    @DexIgnore
    public final void d(mn4 mn4) {
        BCInviteFriendActivity.y.a(this, nt4.b(mn4));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0440  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x04f0  */
    /* JADX WARNING: Removed duplicated region for block: B:125:0x04fe  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0519  */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x051e  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x056f  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x0575  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x057a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(com.fossil.mn4 r26) {
        /*
            r25 = this;
            r0 = r25
            r1 = r26
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.fossil.jr4.y
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "setData - challenge: "
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.e(r3, r4)
            com.fossil.qw6<com.fossil.w65> r2 = r0.r
            if (r2 == 0) goto L_0x0580
            java.lang.Object r2 = r2.a()
            com.fossil.w65 r2 = (com.fossil.w65) r2
            if (r2 == 0) goto L_0x057f
            androidx.constraintlayout.widget.ConstraintLayout r4 = r2.r
            java.lang.String r5 = "clChallengeInformation"
            com.fossil.ee7.a(r4, r5)
            r5 = 0
            r4.setVisibility(r5)
            android.widget.ImageView r4 = r2.A
            java.lang.String r6 = "imgAvatar"
            com.fossil.ee7.a(r4, r6)
            com.fossil.eo4 r6 = r26.i()
            if (r6 == 0) goto L_0x0049
            java.lang.String r6 = r6.e()
            goto L_0x004a
        L_0x0049:
            r6 = 0
        L_0x004a:
            com.fossil.eo4 r7 = r26.i()
            if (r7 == 0) goto L_0x0055
            java.lang.String r7 = r7.c()
            goto L_0x0056
        L_0x0055:
            r7 = 0
        L_0x0056:
            com.fossil.oy6$b r8 = r0.v
            com.fossil.st4 r9 = r0.w
            com.fossil.rt4.a(r4, r6, r7, r8, r9)
            java.lang.String r4 = r26.c()
            if (r4 == 0) goto L_0x006d
            com.portfolio.platform.view.FlexibleTextView r6 = r2.x
            java.lang.String r7 = "ftvDes"
            com.fossil.ee7.a(r6, r7)
            r6.setText(r4)
        L_0x006d:
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            java.lang.String r4 = r4.w()
            com.fossil.eo4 r6 = r26.i()
            if (r6 == 0) goto L_0x0082
            java.lang.String r6 = r6.b()
            goto L_0x0083
        L_0x0082:
            r6 = 0
        L_0x0083:
            boolean r4 = com.fossil.ee7.a(r4, r6)
            if (r4 == 0) goto L_0x0097
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r6 = 2131886246(0x7f1200a6, float:1.9407065E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r6)
            goto L_0x00c4
        L_0x0097:
            com.fossil.fu4 r4 = com.fossil.fu4.a
            com.fossil.eo4 r6 = r26.i()
            if (r6 == 0) goto L_0x00a4
            java.lang.String r6 = r6.a()
            goto L_0x00a5
        L_0x00a4:
            r6 = 0
        L_0x00a5:
            com.fossil.eo4 r7 = r26.i()
            if (r7 == 0) goto L_0x00b0
            java.lang.String r7 = r7.c()
            goto L_0x00b1
        L_0x00b0:
            r7 = 0
        L_0x00b1:
            com.fossil.eo4 r8 = r26.i()
            if (r8 == 0) goto L_0x00be
            java.lang.String r8 = r8.d()
            if (r8 == 0) goto L_0x00be
            goto L_0x00c0
        L_0x00be:
            java.lang.String r8 = "-"
        L_0x00c0:
            java.lang.String r4 = r4.a(r6, r7, r8)
        L_0x00c4:
            java.lang.String r6 = r0.i
            java.lang.String r12 = "running"
            java.lang.String r13 = "waiting"
            java.lang.String r14 = "completed"
            java.lang.String r9 = "pending-invitation"
            java.lang.String r8 = "btnAction"
            java.lang.String r7 = "java.lang.String.format(format, *args)"
            java.lang.String r15 = "imgOption"
            if (r6 == 0) goto L_0x0336
            int r10 = r6.hashCode()
            r11 = -2056567409(0xffffffff856b458f, float:-1.1062423E-35)
            java.lang.String r5 = "LanguageHelper.getString\u2026ding_Text__CreatedByName)"
            java.lang.String r3 = "subject"
            r18 = r8
            java.lang.String r8 = "ftvBy"
            r19 = r9
            java.lang.String r9 = "tvTitle"
            if (r10 == r11) goto L_0x02ae
            r11 = -1441466643(0xffffffffaa14f6ed, float:-1.323071E-13)
            if (r10 == r11) goto L_0x018b
            r11 = -1418673569(0xffffffffab70c25f, float:-8.553487E-13)
            if (r10 == r11) goto L_0x00fe
            r22 = r7
            r3 = r14
        L_0x00f8:
            r6 = r18
            r24 = r19
            goto L_0x033c
        L_0x00fe:
            java.lang.String r10 = "available-challenge"
            boolean r6 = r6.equals(r10)
            if (r6 == 0) goto L_0x0181
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r9)
            com.portfolio.platform.PortfolioApp$a r9 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r9 = r9.c()
            r10 = 2131886216(0x7f120088, float:1.9407005E38)
            java.lang.String r9 = com.fossil.ig5.a(r9, r10)
            r6.setText(r9)
            com.portfolio.platform.view.FlexibleTextView r11 = r2.u
            com.fossil.ee7.a(r11, r8)
            com.fossil.xe5 r6 = com.fossil.xe5.b
            com.fossil.ee7.a(r4, r3)
            com.fossil.we7 r3 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r8 = 2131886251(0x7f1200ab, float:1.9407076E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r8)
            com.fossil.ee7.a(r3, r5)
            r5 = 1
            java.lang.Object[] r8 = new java.lang.Object[r5]
            r9 = 0
            r8[r9] = r4
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r5)
            java.lang.String r8 = java.lang.String.format(r3, r8)
            com.fossil.ee7.a(r8, r7)
            r9 = 0
            r10 = 4
            r3 = 0
            r5 = r7
            r7 = r4
            r20 = r18
            r21 = r19
            r17 = r5
            r5 = r11
            r16 = r14
            r14 = 8
            r11 = r3
            android.text.Spannable r3 = com.fossil.xe5.a(r6, r7, r8, r9, r10, r11)
            r5.setText(r3)
            com.portfolio.platform.view.FlexibleButton r3 = r2.q
            r11 = r20
            com.fossil.ee7.a(r3, r11)
            r5 = 0
            r3.setVisibility(r5)
            com.portfolio.platform.view.RTLImageView r3 = r2.B
            com.fossil.ee7.a(r3, r15)
            r3.setVisibility(r14)
            com.fossil.jr4$o r3 = new com.fossil.jr4$o
            r3.<init>(r2, r4, r0, r1)
            r0.s = r3
            r6 = r11
            r3 = r16
            r22 = r17
            goto L_0x02aa
        L_0x0181:
            r16 = r14
            r14 = 8
            r22 = r7
            r3 = r16
            goto L_0x00f8
        L_0x018b:
            r10 = r7
            r16 = r14
            r11 = r18
            r21 = r19
            r14 = 8
            java.lang.String r7 = "joined_challenge"
            boolean r6 = r6.equals(r7)
            if (r6 == 0) goto L_0x02a5
            com.portfolio.platform.view.FlexibleTextView r7 = r2.u
            com.fossil.ee7.a(r7, r8)
            com.fossil.xe5 r6 = com.fossil.xe5.b
            com.fossil.ee7.a(r4, r3)
            com.fossil.we7 r3 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r8 = 2131886251(0x7f1200ab, float:1.9407076E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r8)
            com.fossil.ee7.a(r3, r5)
            r5 = 1
            java.lang.Object[] r8 = new java.lang.Object[r5]
            r17 = 0
            r8[r17] = r4
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r5)
            java.lang.String r8 = java.lang.String.format(r3, r8)
            com.fossil.ee7.a(r8, r10)
            r3 = 0
            r5 = 4
            r17 = 0
            r14 = r7
            r7 = r4
            r4 = r9
            r9 = r3
            r3 = r10
            r10 = r5
            r5 = r11
            r11 = r17
            android.text.Spannable r6 = com.fossil.xe5.a(r6, r7, r8, r9, r10, r11)
            r14.setText(r6)
            com.portfolio.platform.view.FlexibleButton r6 = r2.q
            com.fossil.ee7.a(r6, r5)
            r7 = 8
            r6.setVisibility(r7)
            java.lang.String r6 = r26.p()
            if (r6 != 0) goto L_0x01ef
            goto L_0x0202
        L_0x01ef:
            int r7 = r6.hashCode()
            r14 = -1402931637(0xffffffffac60f64b, float:-3.1969035E-12)
            if (r7 == r14) goto L_0x026a
            r8 = 1116313165(0x4289964d, float:68.79356)
            if (r7 == r8) goto L_0x0245
            r8 = 1550783935(0x5c6f15bf, float:2.69185718E17)
            if (r7 == r8) goto L_0x0207
        L_0x0202:
            r22 = r3
            r6 = r5
            goto L_0x02a8
        L_0x0207:
            boolean r6 = r6.equals(r12)
            if (r6 == 0) goto L_0x0202
            boolean r6 = r0.q
            if (r6 == 0) goto L_0x0226
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r4)
            java.lang.String r4 = ""
            r6.setText(r4)
            com.portfolio.platform.view.RTLImageView r4 = r2.B
            com.fossil.ee7.a(r4, r15)
            r6 = 8
            r4.setVisibility(r6)
            goto L_0x0202
        L_0x0226:
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r4)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r7 = 2131886217(0x7f120089, float:1.9407007E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r7)
            r6.setText(r4)
            com.portfolio.platform.view.RTLImageView r4 = r2.B
            com.fossil.ee7.a(r4, r15)
            r6 = 0
            r4.setVisibility(r6)
            goto L_0x0202
        L_0x0245:
            boolean r6 = r6.equals(r13)
            if (r6 == 0) goto L_0x0202
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r4)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r7 = 2131886303(0x7f1200df, float:1.9407181E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r7)
            r6.setText(r4)
            com.portfolio.platform.view.RTLImageView r4 = r2.B
            com.fossil.ee7.a(r4, r15)
            r6 = 0
            r4.setVisibility(r6)
            goto L_0x0202
        L_0x026a:
            r11 = r16
            boolean r6 = r6.equals(r11)
            if (r6 == 0) goto L_0x02a0
            com.portfolio.platform.view.RTLImageView r6 = r2.B
            com.fossil.ee7.a(r6, r15)
            r7 = 8
            r6.setVisibility(r7)
            boolean r6 = r0.q
            if (r6 == 0) goto L_0x028b
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r4)
            java.lang.String r4 = ""
            r6.setText(r4)
            goto L_0x02a0
        L_0x028b:
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r4)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r7 = 2131886217(0x7f120089, float:1.9407007E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r7)
            r6.setText(r4)
        L_0x02a0:
            r22 = r3
            r6 = r5
            r3 = r11
            goto L_0x02aa
        L_0x02a5:
            r22 = r10
            r6 = r11
        L_0x02a8:
            r3 = r16
        L_0x02aa:
            r24 = r21
            goto L_0x033c
        L_0x02ae:
            r11 = r14
            r10 = r19
            r14 = r9
            r9 = r18
            boolean r6 = r6.equals(r10)
            if (r6 == 0) goto L_0x032f
            com.portfolio.platform.view.FlexibleTextView r6 = r2.H
            com.fossil.ee7.a(r6, r14)
            com.portfolio.platform.PortfolioApp$a r14 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r14 = r14.c()
            r20 = r9
            r9 = 2131886241(0x7f1200a1, float:1.9407055E38)
            java.lang.String r9 = com.fossil.ig5.a(r14, r9)
            r6.setText(r9)
            com.portfolio.platform.view.FlexibleTextView r14 = r2.u
            com.fossil.ee7.a(r14, r8)
            com.fossil.xe5 r6 = com.fossil.xe5.b
            com.fossil.ee7.a(r4, r3)
            com.fossil.we7 r3 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r8 = 2131886251(0x7f1200ab, float:1.9407076E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r8)
            com.fossil.ee7.a(r3, r5)
            r5 = 1
            java.lang.Object[] r8 = new java.lang.Object[r5]
            r9 = 0
            r8[r9] = r4
            java.lang.Object[] r8 = java.util.Arrays.copyOf(r8, r5)
            java.lang.String r8 = java.lang.String.format(r3, r8)
            com.fossil.ee7.a(r8, r7)
            r9 = 0
            r3 = 4
            r5 = 0
            r22 = r7
            r7 = r4
            r23 = r20
            r24 = r10
            r10 = r3
            r3 = r11
            r11 = r5
            android.text.Spannable r5 = com.fossil.xe5.a(r6, r7, r8, r9, r10, r11)
            r14.setText(r5)
            com.portfolio.platform.view.FlexibleButton r5 = r2.q
            r6 = r23
            com.fossil.ee7.a(r5, r6)
            r7 = 0
            r5.setVisibility(r7)
            com.portfolio.platform.view.RTLImageView r5 = r2.B
            com.fossil.ee7.a(r5, r15)
            r7 = 8
            r5.setVisibility(r7)
            com.fossil.jr4$n r5 = new com.fossil.jr4$n
            r5.<init>(r2, r4, r0, r1)
            r0.s = r5
            goto L_0x033c
        L_0x032f:
            r22 = r7
            r6 = r9
            r24 = r10
            r3 = r11
            goto L_0x033c
        L_0x0336:
            r22 = r7
            r6 = r8
            r24 = r9
            r3 = r14
        L_0x033c:
            java.lang.String r4 = r26.p()
            java.lang.String r5 = "private"
            if (r4 != 0) goto L_0x0346
            goto L_0x040a
        L_0x0346:
            int r7 = r4.hashCode()
            java.lang.String r8 = "ftvTimeLeft"
            r9 = -1402931637(0xffffffffac60f64b, float:-3.1969035E-12)
            if (r7 == r9) goto L_0x03d7
            r3 = 2131886302(0x7f1200de, float:1.940718E38)
            r9 = 1116313165(0x4289964d, float:68.79356)
            if (r7 == r9) goto L_0x03a1
            r9 = 1550783935(0x5c6f15bf, float:2.69185718E17)
            if (r7 == r9) goto L_0x0360
            goto L_0x040a
        L_0x0360:
            boolean r4 = r4.equals(r12)
            if (r4 == 0) goto L_0x040a
            com.portfolio.platform.uirenew.customview.TimerTextView r4 = r2.z
            r4.f()
            com.portfolio.platform.uirenew.customview.TimerTextView r4 = r2.z
            com.fossil.ee7.a(r4, r8)
            com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r7 = r7.c()
            java.lang.String r3 = com.fossil.ig5.a(r7, r3)
            r4.setText(r3)
            java.lang.String r3 = r0.i
            r4 = r24
            boolean r3 = com.fossil.ee7.a(r3, r4)
            if (r3 == 0) goto L_0x042a
            java.lang.String r3 = r26.j()
            boolean r3 = com.fossil.ee7.a(r3, r5)
            if (r3 == 0) goto L_0x042a
            boolean r3 = r0.q
            if (r3 != 0) goto L_0x042a
            com.portfolio.platform.view.FlexibleButton r3 = r2.q
            com.fossil.ee7.a(r3, r6)
            r4 = 8
            r3.setVisibility(r4)
            goto L_0x042a
        L_0x03a1:
            boolean r4 = r4.equals(r13)
            if (r4 == 0) goto L_0x040a
            com.portfolio.platform.uirenew.customview.TimerTextView r4 = r2.z
            com.fossil.ut4 r6 = com.fossil.ut4.START_LEFT
            r4.setDisplayType(r6)
            com.portfolio.platform.uirenew.customview.TimerTextView r4 = r2.z
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            java.lang.String r3 = com.fossil.ig5.a(r6, r3)
            r4.setEndingText(r3)
            com.portfolio.platform.uirenew.customview.TimerTextView r3 = r2.z
            java.util.Date r4 = r26.m()
            if (r4 == 0) goto L_0x03ca
            long r6 = r4.getTime()
            goto L_0x03cc
        L_0x03ca:
            r6 = 0
        L_0x03cc:
            r3.setTime(r6)
            com.portfolio.platform.uirenew.customview.TimerTextView r3 = r2.z
            com.portfolio.platform.buddy_challenge.util.TimerViewObserver r4 = r0.u
            r3.setObserver(r4)
            goto L_0x042a
        L_0x03d7:
            boolean r3 = r4.equals(r3)
            if (r3 == 0) goto L_0x040a
            com.portfolio.platform.uirenew.customview.TimerTextView r3 = r2.z
            r3.f()
            com.portfolio.platform.uirenew.customview.TimerTextView r3 = r2.z
            com.fossil.ee7.a(r3, r8)
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r7 = 2131886301(0x7f1200dd, float:1.9407177E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r7)
            r3.setText(r4)
            com.portfolio.platform.view.RTLImageView r3 = r2.B
            com.fossil.ee7.a(r3, r15)
            r4 = 8
            r3.setVisibility(r4)
            com.portfolio.platform.view.FlexibleButton r3 = r2.q
            com.fossil.ee7.a(r3, r6)
            r3.setVisibility(r4)
            goto L_0x042a
        L_0x040a:
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.fossil.jr4.y
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "setData - status: "
            r6.append(r7)
            java.lang.String r7 = r26.p()
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            r3.e(r4, r6)
        L_0x042a:
            com.portfolio.platform.view.FlexibleTextView r3 = r2.v
            java.lang.String r4 = "ftvChallengeName"
            com.fossil.ee7.a(r3, r4)
            java.lang.String r4 = r26.g()
            r3.setText(r4)
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.s
            java.lang.Integer r4 = r26.d()
            if (r4 == 0) goto L_0x057a
            int r4 = r4.intValue()
            java.lang.String r4 = com.fossil.ye5.a(r4)
            java.lang.String r6 = "TimeHelper.durationHrsMin(challenge.duration!!)"
            com.fossil.ee7.a(r4, r6)
            r3.setValue(r4)
            java.lang.String r3 = r26.u()
            if (r3 == 0) goto L_0x04e2
            java.lang.String r4 = "activity_best_result"
            boolean r3 = com.fossil.ee7.a(r3, r4)
            if (r3 == 0) goto L_0x0489
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.F
            java.lang.String r4 = "stepsCell"
            com.fossil.ee7.a(r3, r4)
            r4 = 8
            r3.setVisibility(r4)
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.I
            com.portfolio.platform.PortfolioApp$a r4 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r4 = r4.c()
            r6 = 2131886305(0x7f1200e1, float:1.9407185E38)
            java.lang.String r4 = com.fossil.ig5.a(r4, r6)
            java.lang.String r6 = "LanguageHelper.getString\u2026l__TheTortoiseAndTheHare)"
            com.fossil.ee7.a(r4, r6)
            r3.setValue(r4)
            com.portfolio.platform.view.FlexibleTextView r3 = r2.v
            int r4 = com.fossil.jr4.A
            r3.setTextColor(r4)
            goto L_0x04e2
        L_0x0489:
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.F
            java.lang.String r4 = "stepsCell"
            com.fossil.ee7.a(r3, r4)
            r4 = 0
            r3.setVisibility(r4)
            com.fossil.we7 r3 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r6 = 2131886306(0x7f1200e2, float:1.9407187E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r6)
            java.lang.String r6 = "LanguageHelper.getString\u2026rd_FullView_Label__Steps)"
            com.fossil.ee7.a(r3, r6)
            r6 = 1
            java.lang.Object[] r7 = new java.lang.Object[r6]
            java.lang.Integer r8 = r26.t()
            r7[r4] = r8
            java.lang.Object[] r4 = java.util.Arrays.copyOf(r7, r6)
            java.lang.String r3 = java.lang.String.format(r3, r4)
            r4 = r22
            com.fossil.ee7.a(r3, r4)
            com.portfolio.platform.uirenew.customview.TitleValueCell r6 = r2.F
            r6.setValue(r3)
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.I
            com.portfolio.platform.PortfolioApp$a r6 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r6 = r6.c()
            r7 = 2131886304(0x7f1200e0, float:1.9407183E38)
            java.lang.String r6 = com.fossil.ig5.a(r6, r7)
            java.lang.String r7 = "LanguageHelper.getString\u2026nge_Label__FirstToFinish)"
            com.fossil.ee7.a(r6, r7)
            r3.setValue(r6)
            com.portfolio.platform.view.FlexibleTextView r3 = r2.v
            int r6 = com.fossil.jr4.z
            r3.setTextColor(r6)
            goto L_0x04e4
        L_0x04e2:
            r4 = r22
        L_0x04e4:
            com.portfolio.platform.uirenew.customview.TitleValueCell r3 = r2.D
            java.lang.String r6 = r26.j()
            boolean r5 = com.fossil.ee7.a(r6, r5)
            if (r5 == 0) goto L_0x04fe
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            r6 = 2131886275(0x7f1200c3, float:1.9407124E38)
            java.lang.String r5 = com.fossil.ig5.a(r5, r6)
            goto L_0x050b
        L_0x04fe:
            com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r5 = r5.c()
            r6 = 2131886274(0x7f1200c2, float:1.9407122E38)
            java.lang.String r5 = com.fossil.ig5.a(r5, r6)
        L_0x050b:
            java.lang.String r6 = "if (challenge.privacy ==\u2026t__Friends)\n            }"
            com.fossil.ee7.a(r5, r6)
            r3.setValue(r5)
            java.lang.Integer r3 = r26.h()
            if (r3 == 0) goto L_0x051e
            int r9 = r3.intValue()
            goto L_0x051f
        L_0x051e:
            r9 = 0
        L_0x051f:
            com.fossil.we7 r3 = com.fossil.we7.a
            com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r3 = r3.c()
            r5 = 2131886215(0x7f120087, float:1.9407003E38)
            java.lang.String r3 = com.fossil.ig5.a(r3, r5)
            java.lang.String r5 = "LanguageHelper.getString\u2026_Subtitle__NumberMembers)"
            com.fossil.ee7.a(r3, r5)
            r5 = 1
            java.lang.Object[] r6 = new java.lang.Object[r5]
            java.lang.Integer r7 = java.lang.Integer.valueOf(r9)
            r8 = 0
            r6[r8] = r7
            java.lang.Object[] r5 = java.util.Arrays.copyOf(r6, r5)
            java.lang.String r12 = java.lang.String.format(r3, r5)
            com.fossil.ee7.a(r12, r4)
            com.portfolio.platform.view.FlexibleTextView r3 = r2.w
            java.lang.String r4 = "ftvCountMember"
            com.fossil.ee7.a(r3, r4)
            com.fossil.xe5 r10 = com.fossil.xe5.b
            java.lang.String r11 = java.lang.String.valueOf(r9)
            r13 = 0
            r14 = 4
            r15 = 0
            android.text.Spannable r4 = com.fossil.xe5.a(r10, r11, r12, r13, r14, r15)
            r3.setText(r4)
            com.portfolio.platform.view.FlexibleTextView r2 = r2.w
            java.lang.String r3 = "ftvCountMember"
            com.fossil.ee7.a(r2, r3)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r9)
            r2.setTag(r3)
            if (r9 <= 0) goto L_0x0575
            com.fossil.jr4$p r2 = new com.fossil.jr4$p
            r2.<init>(r0, r1)
            goto L_0x0577
        L_0x0575:
            com.fossil.jr4$q r2 = com.fossil.jr4.q.INSTANCE
        L_0x0577:
            r0.t = r2
            goto L_0x057f
        L_0x057a:
            com.fossil.ee7.a()
            r1 = 0
            throw r1
        L_0x057f:
            return
        L_0x0580:
            r1 = 0
            java.lang.String r2 = "binding"
            com.fossil.ee7.d(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.jr4.b(com.fossil.mn4):void");
    }

    @DexIgnore
    public final void c(mn4 mn4) {
        BCCreateChallengeInputActivity.y.a(this, nt4.b(mn4));
    }

    @DexIgnore
    public final void c(String str, boolean z2) {
        BCMemberInChallengeActivity.y.a(this, str, z2);
    }

    @DexIgnore
    @Override // com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (str.hashCode() == 1970588827 && str.equals("LEAVE_CHALLENGE") && i2 == 2131363307) {
            mr4 mr4 = this.g;
            if (mr4 != null) {
                mr4.k();
            } else {
                ee7.d("viewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(Integer num, String str) {
        if (num == null) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.q(childFragmentManager);
            return;
        }
        bx6 bx62 = bx6.c;
        int intValue = num.intValue();
        FragmentManager childFragmentManager2 = getChildFragmentManager();
        ee7.a((Object) childFragmentManager2, "childFragmentManager");
        bx62.a(intValue, str, childFragmentManager2);
    }

    @DexIgnore
    public final void b(Intent intent) {
        if (this.p != null) {
            Context context = getContext();
            if (context != null) {
                HomeActivity.a aVar = HomeActivity.z;
                ee7.a((Object) context, "it");
                aVar.a(context, 1);
            }
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.finish();
                return;
            }
            return;
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.setResult(-1, intent);
        }
        FragmentActivity activity3 = getActivity();
        if (activity3 != null) {
            activity3.finish();
        }
    }
}
