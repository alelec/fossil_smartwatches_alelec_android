package com.fossil;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dx2<K, V> extends LinkedHashMap<K, V> {
    @DexIgnore
    public static /* final */ dx2 a;
    @DexIgnore
    public boolean zza; // = true;

    /*
    static {
        dx2 dx2 = new dx2();
        a = dx2;
        dx2.zza = false;
    }
    */

    @DexIgnore
    public dx2() {
    }

    @DexIgnore
    public static int b(Object obj) {
        if (obj instanceof byte[]) {
            return ew2.c((byte[]) obj);
        }
        if (!(obj instanceof dw2)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <K, V> dx2<K, V> zza() {
        return a;
    }

    @DexIgnore
    public final void clear() {
        zze();
        super.clear();
    }

    @DexIgnore
    @Override // java.util.LinkedHashMap, java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof java.util.Map
            r1 = 0
            if (r0 == 0) goto L_0x005d
            java.util.Map r7 = (java.util.Map) r7
            r0 = 1
            if (r6 == r7) goto L_0x0059
            int r2 = r6.size()
            int r3 = r7.size()
            if (r2 == r3) goto L_0x0016
        L_0x0014:
            r7 = 0
            goto L_0x005a
        L_0x0016:
            java.util.Set r2 = r6.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x001e:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0059
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            boolean r4 = r7.containsKey(r4)
            if (r4 != 0) goto L_0x0035
            goto L_0x0014
        L_0x0035:
            java.lang.Object r4 = r3.getValue()
            java.lang.Object r3 = r3.getKey()
            java.lang.Object r3 = r7.get(r3)
            boolean r5 = r4 instanceof byte[]
            if (r5 == 0) goto L_0x0052
            boolean r5 = r3 instanceof byte[]
            if (r5 == 0) goto L_0x0052
            byte[] r4 = (byte[]) r4
            byte[] r3 = (byte[]) r3
            boolean r3 = java.util.Arrays.equals(r4, r3)
            goto L_0x0056
        L_0x0052:
            boolean r3 = r4.equals(r3)
        L_0x0056:
            if (r3 != 0) goto L_0x001e
            goto L_0x0014
        L_0x0059:
            r7 = 1
        L_0x005a:
            if (r7 == 0) goto L_0x005d
            return r0
        L_0x005d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.dx2.equals(java.lang.Object):boolean");
    }

    @DexIgnore
    public final int hashCode() {
        int i = 0;
        for (Map.Entry<K, V> entry : entrySet()) {
            i += b(entry.getValue()) ^ b(entry.getKey());
        }
        return i;
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V put(K k, V v) {
        zze();
        ew2.a((Object) k);
        ew2.a((Object) v);
        return (V) super.put(k, v);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final void putAll(Map<? extends K, ? extends V> map) {
        zze();
        for (Object obj : map.keySet()) {
            ew2.a(obj);
            ew2.a(map.get(obj));
        }
        super.putAll(map);
    }

    @DexIgnore
    @Override // java.util.AbstractMap, java.util.Map, java.util.HashMap
    public final V remove(Object obj) {
        zze();
        return (V) super.remove(obj);
    }

    @DexIgnore
    public final dx2<K, V> zzb() {
        return isEmpty() ? new dx2<>() : new dx2<>(this);
    }

    @DexIgnore
    public final void zzc() {
        this.zza = false;
    }

    @DexIgnore
    public final boolean zzd() {
        return this.zza;
    }

    @DexIgnore
    public final void zze() {
        if (!this.zza) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public final void zza(dx2<K, V> dx2) {
        zze();
        if (!dx2.isEmpty()) {
            putAll(dx2);
        }
    }

    @DexIgnore
    public dx2(Map<K, V> map) {
        super(map);
    }
}
