package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface s73 extends IInterface {
    @DexIgnore
    void c(en2 en2) throws RemoteException;

    @DexIgnore
    void d(en2 en2) throws RemoteException;

    @DexIgnore
    void e(en2 en2) throws RemoteException;
}
