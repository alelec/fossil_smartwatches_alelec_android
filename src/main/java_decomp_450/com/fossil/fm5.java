package com.fossil;

import android.content.Intent;
import com.fossil.fl4;
import com.fossil.nj5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public int e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public /* final */ e g; // = new e();
    @DexIgnore
    public /* final */ DeviceRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return fm5.i;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(String str, int i) {
            ee7.b(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
            this.a = str;
            this.b = i;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            ee7.b(arrayList, "errorCodes");
            this.a = i;
            this.b = i2;
            this.c = arrayList;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements nj5.b {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase$SetVibrationStrengthBroadcastReceiver$receive$1", f = "SetVibrationStrengthUseCase.kt", l = {49}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    Device deviceBySerial = fm5.this.h.getDeviceBySerial(fm5.this.e());
                    if (deviceBySerial != null) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = fm5.j.a();
                        local.d(a2, "Update vibration stregnth " + fm5.this.d() + " to db");
                        deviceBySerial.setVibrationStrength(pb7.a(fm5.this.d()));
                        DeviceRepository a3 = fm5.this.h;
                        this.L$0 = yi7;
                        this.L$1 = deviceBySerial;
                        this.L$2 = deviceBySerial;
                        this.label = 1;
                        obj = a3.updateDevice(deviceBySerial, false, this);
                        if (obj == a) {
                            return a;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d(fm5.j.a(), "onReceive #getDeviceBySerial success");
                    fm5.this.a(new d());
                    return i97.a;
                } else if (i == 1) {
                    Device device = (Device) this.L$2;
                    Device device2 = (Device) this.L$1;
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                zi5 zi5 = (zi5) obj;
                FLogger.INSTANCE.getLocal().d(fm5.j.a(), "onReceive #getDeviceBySerial success");
                fm5.this.a(new d());
                return i97.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e() {
        }

        @DexIgnore
        @Override // com.fossil.nj5.b
        public void a(CommunicateMode communicateMode, Intent intent) {
            ee7.b(communicateMode, "communicateMode");
            ee7.b(intent, "intent");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = fm5.j.a();
            local.d(a2, "Inside .bleReceiver communicateMode=" + communicateMode + ", isExecuted=" + fm5.this.f() + ", isSuccess=" + intExtra);
            if (communicateMode == CommunicateMode.SET_VIBRATION_STRENGTH && fm5.this.f()) {
                boolean z = false;
                fm5.this.a(false);
                if (intExtra == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                if (z) {
                    ik7 unused = xh7.b(fm5.this.b(), null, null, new a(this, null), 3, null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d(fm5.j.a(), "onReceive failed");
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                fm5.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra2, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        String simpleName = fm5.class.getSimpleName();
        ee7.a((Object) simpleName, "SetVibrationStrengthUseCase::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public fm5(DeviceRepository deviceRepository, ch5 ch5) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ch5, "mSharedPreferencesManager");
        this.h = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return i;
    }

    @DexIgnore
    public final int d() {
        return this.e;
    }

    @DexIgnore
    public final String e() {
        return this.f;
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final void g() {
        nj5.d.a(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    public final void h() {
        nj5.d.b(this.g, CommunicateMode.SET_VIBRATION_STRENGTH);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public Object a(b bVar, fb7<Object> fb7) {
        try {
            FLogger.INSTANCE.getLocal().d(i, "running UseCase");
            this.d = true;
            Integer a2 = bVar != null ? pb7.a(bVar.b()) : null;
            if (a2 != null) {
                this.e = a2.intValue();
                this.f = bVar.a();
                PortfolioApp.g0.c().a(bVar.a(), new VibrationStrengthObj(he5.a(bVar.b()), false, 2, null));
                return new Object();
            }
            ee7.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            local.e(str, "Error inside " + i + ".connectDevice - e=" + e2);
            return new c(600, -1, new ArrayList());
        }
    }
}
