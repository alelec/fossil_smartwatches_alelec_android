package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p67 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ Context b;
    @DexIgnore
    public /* final */ /* synthetic */ a47 c;

    @DexIgnore
    public p67(String str, Context context, a47 a47) {
        this.a = str;
        this.b = context;
        this.c = a47;
    }

    @DexIgnore
    public final void run() {
        try {
            synchronized (z37.k) {
                if (z37.k.size() >= w37.h()) {
                    k57 f = z37.m;
                    f.d("The number of page events exceeds the maximum value " + Integer.toString(w37.h()));
                    return;
                }
                String unused = z37.i = this.a;
                if (z37.k.containsKey(z37.i)) {
                    k57 f2 = z37.m;
                    f2.c("Duplicate PageID : " + z37.i + ", onResume() repeated?");
                    return;
                }
                z37.k.put(z37.i, Long.valueOf(System.currentTimeMillis()));
                z37.a(this.b, true, this.c);
            }
        } catch (Throwable th) {
            z37.m.a(th);
            z37.a(this.b, th);
        }
    }
}
