package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.fossil.jr5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ir5 extends AsyncTask<Void, Void, a> {
    @DexIgnore
    public /* final */ WeakReference<CropImageView> a;
    @DexIgnore
    public /* final */ Uri b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public FilterType f;

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public ir5(CropImageView cropImageView, Uri uri, FilterType filterType) {
        this.b = uri;
        this.f = filterType;
        this.a = new WeakReference<>(cropImageView);
        this.c = cropImageView.getContext();
        DisplayMetrics displayMetrics = cropImageView.getResources().getDisplayMetrics();
        float f2 = displayMetrics.density;
        double d2 = f2 > 1.0f ? (double) (1.0f / f2) : 1.0d;
        this.d = (int) (((double) displayMetrics.widthPixels) * d2);
        this.e = (int) (((double) displayMetrics.heightPixels) * d2);
    }

    @DexIgnore
    public Uri a() {
        return this.b;
    }

    @DexIgnore
    /* renamed from: a */
    public a doInBackground(Void... voidArr) {
        try {
            if (isCancelled()) {
                return null;
            }
            jr5.a a2 = jr5.a(this.c, this.b, this.d, this.e);
            if (isCancelled()) {
                return null;
            }
            jr5.b a3 = jr5.a(a2.a, this.c, this.b);
            return new a(this.b, a3.a, a2.b, a3.b, EInkImageFilter.create().apply(a3.a, this.f, false, false, new OutputSettings(a3.a.getWidth(), a3.a.getHeight(), Format.RAW, false, false)).getPreview());
        } catch (Exception e2) {
            return new a(this.b, e2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Uri a;
        @DexIgnore
        public /* final */ Bitmap b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ int d;
        @DexIgnore
        public /* final */ Exception e;
        @DexIgnore
        public /* final */ Bitmap f;

        @DexIgnore
        public a(Uri uri, Bitmap bitmap, int i, int i2, Bitmap bitmap2) {
            this.a = uri;
            this.b = bitmap;
            this.c = i;
            this.d = i2;
            this.e = null;
            this.f = bitmap2;
        }

        @DexIgnore
        public a(Uri uri, Exception exc) {
            this.a = uri;
            this.b = null;
            this.c = 0;
            this.d = 0;
            this.e = exc;
            this.f = null;
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onPostExecute(a aVar) {
        Bitmap bitmap;
        CropImageView cropImageView;
        if (aVar != null) {
            boolean z = false;
            if (!isCancelled() && (cropImageView = this.a.get()) != null) {
                z = true;
                cropImageView.a(aVar);
            }
            if (!z && (bitmap = aVar.b) != null) {
                bitmap.recycle();
            }
        }
    }
}
