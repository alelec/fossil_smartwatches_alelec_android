package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq implements rq {
    @DexIgnore
    public static /* final */ a j; // = new a(null);
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ Set<Bitmap.Config> h;
    @DexIgnore
    public /* final */ uq i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final Set<Bitmap.Config> a() {
            o4 a = p4.a(Bitmap.Config.ALPHA_8, Bitmap.Config.RGB_565, Bitmap.Config.ARGB_4444, Bitmap.Config.ARGB_8888);
            if (Build.VERSION.SDK_INT >= 26) {
                a.add(Bitmap.Config.RGBA_F16);
            }
            return a;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends android.graphics.Bitmap$Config> */
    /* JADX WARN: Multi-variable type inference failed */
    public sq(long j2, Set<? extends Bitmap.Config> set, uq uqVar) {
        ee7.b(set, "allowedConfigs");
        ee7.b(uqVar, "strategy");
        this.g = j2;
        this.h = set;
        this.i = uqVar;
        if (!(j2 >= 0)) {
            throw new IllegalArgumentException("maxSize must be >= 0.".toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.rq
    public synchronized void a(Bitmap bitmap) {
        ee7.b(bitmap, "bitmap");
        boolean z = true;
        if (!bitmap.isRecycled()) {
            int a2 = iu.a(bitmap);
            if (bitmap.isMutable()) {
                long j2 = (long) a2;
                if (j2 <= this.g) {
                    if (this.h.contains(bitmap.getConfig())) {
                        this.i.a(bitmap);
                        this.e++;
                        this.b += j2;
                        if (cu.c.a() && cu.c.b() <= 2) {
                            Log.println(2, "RealBitmapPool", "Put bitmap in pool=" + this.i.b(bitmap));
                        }
                        c();
                        a(this.g);
                        return;
                    }
                }
            }
            if (cu.c.a() && cu.c.b() <= 2) {
                StringBuilder sb = new StringBuilder();
                sb.append("Rejected bitmap from pool: bitmap: ");
                sb.append(this.i.b(bitmap));
                sb.append(", ");
                sb.append("is mutable: ");
                sb.append(bitmap.isMutable());
                sb.append(", ");
                sb.append("is greater than max size: ");
                if (((long) a2) <= this.g) {
                    z = false;
                }
                sb.append(z);
                sb.append("is allowed config: ");
                sb.append(this.h.contains(bitmap.getConfig()));
                Log.println(2, "RealBitmapPool", sb.toString());
            }
            bitmap.recycle();
            return;
        }
        throw new IllegalArgumentException("Cannot pool recycled bitmap!".toString());
    }

    @DexIgnore
    @Override // com.fossil.rq
    public synchronized Bitmap b(int i2, int i3, Bitmap.Config config) {
        Bitmap a2;
        ee7.b(config, "config");
        a(config);
        a2 = this.i.a(i2, i3, config);
        if (a2 == null) {
            if (cu.c.a() && cu.c.b() <= 3) {
                Log.println(3, "RealBitmapPool", "Missing bitmap=" + this.i.b(i2, i3, config));
            }
            this.d++;
        } else {
            this.c++;
            this.b -= (long) iu.a(a2);
            b(a2);
        }
        if (cu.c.a() && cu.c.b() <= 2) {
            Log.println(2, "RealBitmapPool", "Get bitmap=" + this.i.b(i2, i3, config));
        }
        c();
        return a2;
    }

    @DexIgnore
    public Bitmap c(int i2, int i3, Bitmap.Config config) {
        ee7.b(config, "config");
        Bitmap b2 = b(i2, i3, config);
        if (b2 != null) {
            b2.eraseColor(0);
        }
        return b2;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ sq(long j2, Set set, uq uqVar, int i2, zd7 zd7) {
        this(j2, (i2 & 2) != 0 ? j.a() : set, (i2 & 4) != 0 ? uq.a.a() : uqVar);
    }

    @DexIgnore
    public final void c() {
        if (cu.c.a() && cu.c.b() <= 2) {
            Log.println(2, "RealBitmapPool", b());
        }
    }

    @DexIgnore
    public final void b(Bitmap bitmap) {
        bitmap.setDensity(0);
        bitmap.setHasAlpha(true);
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    @DexIgnore
    public final String b() {
        return "Hits=" + this.c + ", misses=" + this.d + ", puts=" + this.e + ", evictions=" + this.f + ", " + "currentSize=" + this.b + ", maxSize=" + this.g + ", strategy=" + this.i;
    }

    @DexIgnore
    @Override // com.fossil.rq
    public Bitmap a(int i2, int i3, Bitmap.Config config) {
        ee7.b(config, "config");
        Bitmap c2 = c(i2, i3, config);
        if (c2 != null) {
            return c2;
        }
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, config);
        ee7.a((Object) createBitmap, "Bitmap.createBitmap(width, height, config)");
        return createBitmap;
    }

    @DexIgnore
    public final synchronized void a(long j2) {
        while (this.b > j2) {
            Bitmap removeLast = this.i.removeLast();
            if (removeLast == null) {
                if (cu.c.a() && cu.c.b() <= 5) {
                    Log.println(5, "RealBitmapPool", "Size mismatch, resetting.\n" + b());
                }
                this.b = 0;
                return;
            }
            this.b -= (long) iu.a(removeLast);
            this.f++;
            if (cu.c.a() && cu.c.b() <= 3) {
                Log.println(3, "RealBitmapPool", "Evicting bitmap=" + this.i.b(removeLast));
            }
            c();
            removeLast.recycle();
        }
    }

    @DexIgnore
    public final void a(Bitmap.Config config) {
        if (!(Build.VERSION.SDK_INT < 26 || config != Bitmap.Config.HARDWARE)) {
            throw new IllegalArgumentException("Cannot create a mutable hardware Bitmap.".toString());
        }
    }

    @DexIgnore
    public final void a() {
        if (cu.c.a() && cu.c.b() <= 3) {
            Log.println(3, "RealBitmapPool", "clearMemory");
        }
        a(-1L);
    }

    @DexIgnore
    @Override // com.fossil.rq
    public synchronized void a(int i2) {
        if (cu.c.a() && cu.c.b() <= 3) {
            Log.println(3, "RealBitmapPool", "trimMemory, level=" + i2);
        }
        if (i2 >= 40) {
            a();
        } else if (10 <= i2) {
            if (20 > i2) {
                a(this.b / ((long) 2));
            }
        }
    }
}
