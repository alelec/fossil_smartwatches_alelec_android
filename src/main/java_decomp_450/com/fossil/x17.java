package com.fossil;

import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x17 extends v17 {
    @DexIgnore
    public /* final */ s17 a;
    @DexIgnore
    public /* final */ MethodChannel.Result b;
    @DexIgnore
    public /* final */ Boolean c;

    @DexIgnore
    public x17(MethodChannel.Result result, s17 s17, Boolean bool) {
        this.b = result;
        this.a = s17;
        this.c = bool;
    }

    @DexIgnore
    @Override // com.fossil.z17, com.fossil.v17
    public s17 a() {
        return this.a;
    }

    @DexIgnore
    @Override // com.fossil.z17
    public <T> T a(String str) {
        return null;
    }

    @DexIgnore
    @Override // com.fossil.z17, com.fossil.v17
    public Boolean c() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.a27
    public void error(String str, String str2, Object obj) {
        this.b.error(str, str2, obj);
    }

    @DexIgnore
    @Override // com.fossil.a27
    public void success(Object obj) {
        this.b.success(obj);
    }
}
