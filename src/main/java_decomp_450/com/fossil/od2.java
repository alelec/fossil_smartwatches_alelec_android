package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.y62;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<od2> CREATOR; // = new nd2();
    @DexIgnore
    public /* final */ DataSet a;
    @DexIgnore
    public /* final */ zi2 b;
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public od2(DataSet dataSet, IBinder iBinder, boolean z) {
        this.a = dataSet;
        this.b = bj2.a(iBinder);
        this.c = z;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj != this) {
            return (obj instanceof od2) && y62.a(this.a, ((od2) obj).a);
        }
        return true;
    }

    @DexIgnore
    public final int hashCode() {
        return y62.a(this.a);
    }

    @DexIgnore
    public final String toString() {
        y62.a a2 = y62.a(this);
        a2.a("dataSet", this.a);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, (Parcelable) this.a, i, false);
        zi2 zi2 = this.b;
        k72.a(parcel, 2, zi2 == null ? null : zi2.asBinder(), false);
        k72.a(parcel, 4, this.c);
        k72.a(parcel, a2);
    }

    @DexIgnore
    public od2(DataSet dataSet, zi2 zi2, boolean z) {
        this.a = dataSet;
        this.b = zi2;
        this.c = z;
    }
}
