package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.io.IOException;
import java.nio.CharBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o04 {
    @DexIgnore
    public static CharBuffer a() {
        return CharBuffer.allocate(2048);
    }

    @DexIgnore
    @CanIgnoreReturnValue
    public static <T> T a(Readable readable, t04<T> t04) throws IOException {
        String a;
        jw3.a(readable);
        jw3.a(t04);
        u04 u04 = new u04(readable);
        do {
            a = u04.a();
            if (a == null) {
                break;
            }
        } while (t04.a(a));
        return t04.getResult();
    }
}
