package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class am1 extends Enum<am1> {
    @DexIgnore
    public static /* final */ /* synthetic */ am1[] c; // = {new am1("PLAY_PAUSE", 0, 205, zn1.b), new am1("VOLUME_UP", 1, 233, zn1.b), new am1("VOLUME_DOWN", 2, 234, zn1.b), new am1("NEXT", 3, 181, zn1.b), new am1("PREVIOUS", 4, 182, zn1.b), new am1("MUTE", 5, 226, zn1.b), new am1("LOW_WHITE", 6, 5, zn1.KEYBOARD), new am1("LOW_BLACK", 7, 26, zn1.KEYBOARD), new am1("RIGHT", 8, 79, zn1.KEYBOARD), new am1("LEFT", 9, 80, zn1.KEYBOARD), new am1("PAGE_UP", 10, 75, zn1.KEYBOARD), new am1("PAGE_DOWN", 11, 78, zn1.KEYBOARD), new am1("ENTER", 12, 88, zn1.KEYBOARD)};
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ zn1 b;

    @DexIgnore
    public am1(String str, int i, short s, zn1 zn1) {
        this.a = s;
        this.b = zn1;
    }

    @DexIgnore
    public static am1 valueOf(String str) {
        return (am1) Enum.valueOf(am1.class, str);
    }

    @DexIgnore
    public static am1[] values() {
        return (am1[]) c.clone();
    }
}
