package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import com.facebook.places.internal.LocationScannerImpl;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lr5 implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<lr5> CREATOR; // = new a();
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public CharSequence I;
    @DexIgnore
    public int J;
    @DexIgnore
    public Uri K;
    @DexIgnore
    public Bitmap.CompressFormat L;
    @DexIgnore
    public int M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public CropImageView.j P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public Rect R;
    @DexIgnore
    public int S;
    @DexIgnore
    public boolean T;
    @DexIgnore
    public boolean U;
    @DexIgnore
    public boolean V;
    @DexIgnore
    public int W;
    @DexIgnore
    public boolean X;
    @DexIgnore
    public boolean Y;
    @DexIgnore
    public CharSequence Z;
    @DexIgnore
    public CropImageView.c a;
    @DexIgnore
    public int a0;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public CropImageView.d d;
    @DexIgnore
    public CropImageView.k e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public float p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public int u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public int y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.Creator<lr5> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public lr5 createFromParcel(Parcel parcel) {
            return new lr5(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public lr5[] newArray(int i) {
            return new lr5[i];
        }
    }

    @DexIgnore
    public lr5() {
        DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        this.a = CropImageView.c.RECTANGLE;
        this.b = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.c = TypedValue.applyDimension(1, 24.0f, displayMetrics);
        this.d = CropImageView.d.ON_TOUCH;
        this.e = CropImageView.k.FIT_CENTER;
        this.f = true;
        this.g = true;
        this.h = true;
        this.i = false;
        this.j = 4;
        this.p = 0.1f;
        this.q = false;
        this.r = 1;
        this.s = 1;
        this.t = TypedValue.applyDimension(1, 3.0f, displayMetrics);
        this.u = Color.argb(170, 255, 255, 255);
        this.v = TypedValue.applyDimension(1, 2.0f, displayMetrics);
        this.w = TypedValue.applyDimension(1, 5.0f, displayMetrics);
        this.x = TypedValue.applyDimension(1, 14.0f, displayMetrics);
        this.y = -1;
        this.z = TypedValue.applyDimension(1, 1.0f, displayMetrics);
        this.A = Color.argb(170, 255, 255, 255);
        this.B = Color.argb(200, 0, 0, 0);
        this.C = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.D = (int) TypedValue.applyDimension(1, 42.0f, displayMetrics);
        this.E = 40;
        this.F = 40;
        this.G = 99999;
        this.H = 99999;
        this.I = "";
        this.J = 0;
        this.K = Uri.EMPTY;
        this.L = Bitmap.CompressFormat.JPEG;
        this.M = 90;
        this.N = 0;
        this.O = 0;
        this.P = CropImageView.j.NONE;
        this.Q = false;
        this.R = null;
        this.S = -1;
        this.T = true;
        this.U = true;
        this.V = false;
        this.W = 90;
        this.X = false;
        this.Y = false;
        this.Z = null;
        this.a0 = 0;
    }

    @DexIgnore
    public void a() {
        if (this.j < 0) {
            throw new IllegalArgumentException("Cannot set max zoom to a number < 1");
        } else if (this.c >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f2 = this.p;
            if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || ((double) f2) >= 0.5d) {
                throw new IllegalArgumentException("Cannot set initial crop window padding value to a number < 0 or >= 0.5");
            } else if (this.r <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.s <= 0) {
                throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
            } else if (this.t < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set line thickness value to a number less than 0.");
            } else if (this.v < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set corner thickness value to a number less than 0.");
            } else if (this.z < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                throw new IllegalArgumentException("Cannot set guidelines thickness value to a number less than 0.");
            } else if (this.D >= 0) {
                int i2 = this.E;
                if (i2 >= 0) {
                    int i3 = this.F;
                    if (i3 < 0) {
                        throw new IllegalArgumentException("Cannot set min crop result height value to a number < 0 ");
                    } else if (this.G < i2) {
                        throw new IllegalArgumentException("Cannot set max crop result width to smaller value than min crop result width");
                    } else if (this.H < i3) {
                        throw new IllegalArgumentException("Cannot set max crop result height to smaller value than min crop result height");
                    } else if (this.N < 0) {
                        throw new IllegalArgumentException("Cannot set request width value to a number < 0 ");
                    } else if (this.O >= 0) {
                        int i4 = this.W;
                        if (i4 < 0 || i4 > 360) {
                            throw new IllegalArgumentException("Cannot set rotation degrees value to a number < 0 or > 360");
                        }
                    } else {
                        throw new IllegalArgumentException("Cannot set request height value to a number < 0 ");
                    }
                } else {
                    throw new IllegalArgumentException("Cannot set min crop result width value to a number < 0 ");
                }
            } else {
                throw new IllegalArgumentException("Cannot set min crop window height value to a number < 0 ");
            }
        } else {
            throw new IllegalArgumentException("Cannot set touch radius value to a number <= 0 ");
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.a.ordinal());
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
        parcel.writeInt(this.d.ordinal());
        parcel.writeInt(this.e.ordinal());
        parcel.writeByte(this.f ? (byte) 1 : 0);
        parcel.writeByte(this.g ? (byte) 1 : 0);
        parcel.writeByte(this.h ? (byte) 1 : 0);
        parcel.writeByte(this.i ? (byte) 1 : 0);
        parcel.writeInt(this.j);
        parcel.writeFloat(this.p);
        parcel.writeByte(this.q ? (byte) 1 : 0);
        parcel.writeInt(this.r);
        parcel.writeInt(this.s);
        parcel.writeFloat(this.t);
        parcel.writeInt(this.u);
        parcel.writeFloat(this.v);
        parcel.writeFloat(this.w);
        parcel.writeFloat(this.x);
        parcel.writeInt(this.y);
        parcel.writeFloat(this.z);
        parcel.writeInt(this.A);
        parcel.writeInt(this.B);
        parcel.writeInt(this.C);
        parcel.writeInt(this.D);
        parcel.writeInt(this.E);
        parcel.writeInt(this.F);
        parcel.writeInt(this.G);
        parcel.writeInt(this.H);
        TextUtils.writeToParcel(this.I, parcel, i2);
        parcel.writeInt(this.J);
        parcel.writeParcelable(this.K, i2);
        parcel.writeString(this.L.name());
        parcel.writeInt(this.M);
        parcel.writeInt(this.N);
        parcel.writeInt(this.O);
        parcel.writeInt(this.P.ordinal());
        parcel.writeInt(this.Q ? 1 : 0);
        parcel.writeParcelable(this.R, i2);
        parcel.writeInt(this.S);
        parcel.writeByte(this.T ? (byte) 1 : 0);
        parcel.writeByte(this.U ? (byte) 1 : 0);
        parcel.writeByte(this.V ? (byte) 1 : 0);
        parcel.writeInt(this.W);
        parcel.writeByte(this.X ? (byte) 1 : 0);
        parcel.writeByte(this.Y ? (byte) 1 : 0);
        TextUtils.writeToParcel(this.Z, parcel, i2);
        parcel.writeInt(this.a0);
    }

    @DexIgnore
    public lr5(Parcel parcel) {
        this.a = CropImageView.c.values()[parcel.readInt()];
        this.b = parcel.readFloat();
        this.c = parcel.readFloat();
        this.d = CropImageView.d.values()[parcel.readInt()];
        this.e = CropImageView.k.values()[parcel.readInt()];
        boolean z2 = true;
        this.f = parcel.readByte() != 0;
        this.g = parcel.readByte() != 0;
        this.h = parcel.readByte() != 0;
        this.i = parcel.readByte() != 0;
        this.j = parcel.readInt();
        this.p = parcel.readFloat();
        this.q = parcel.readByte() != 0;
        this.r = parcel.readInt();
        this.s = parcel.readInt();
        this.t = parcel.readFloat();
        this.u = parcel.readInt();
        this.v = parcel.readFloat();
        this.w = parcel.readFloat();
        this.x = parcel.readFloat();
        this.y = parcel.readInt();
        this.z = parcel.readFloat();
        this.A = parcel.readInt();
        this.B = parcel.readInt();
        this.C = parcel.readInt();
        this.D = parcel.readInt();
        this.E = parcel.readInt();
        this.F = parcel.readInt();
        this.G = parcel.readInt();
        this.H = parcel.readInt();
        this.I = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.J = parcel.readInt();
        this.K = (Uri) parcel.readParcelable(Uri.class.getClassLoader());
        this.L = Bitmap.CompressFormat.valueOf(parcel.readString());
        this.M = parcel.readInt();
        this.N = parcel.readInt();
        this.O = parcel.readInt();
        this.P = CropImageView.j.values()[parcel.readInt()];
        this.Q = parcel.readByte() != 0;
        this.R = (Rect) parcel.readParcelable(Rect.class.getClassLoader());
        this.S = parcel.readInt();
        this.T = parcel.readByte() != 0;
        this.U = parcel.readByte() != 0;
        this.V = parcel.readByte() != 0;
        this.W = parcel.readInt();
        this.X = parcel.readByte() != 0;
        this.Y = parcel.readByte() == 0 ? false : z2;
        this.Z = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.a0 = parcel.readInt();
    }
}
