package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fi7 extends kk7<ik7> {
    @DexIgnore
    public /* final */ bi7<?> e;

    @DexIgnore
    public fi7(ik7 ik7, bi7<?> bi7) {
        super(ik7);
        this.e = bi7;
    }

    @DexIgnore
    @Override // com.fossil.pi7
    public void b(Throwable th) {
        bi7<?> bi7 = this.e;
        bi7.c(bi7.a((ik7) ((ok7) this).d));
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        b(th);
        return i97.a;
    }

    @DexIgnore
    @Override // com.fossil.bm7
    public String toString() {
        return "ChildContinuation[" + this.e + ']';
    }
}
