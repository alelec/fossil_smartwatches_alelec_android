package com.fossil;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u44 {
    @DexIgnore
    public String a; // = null;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, String> b; // = new ConcurrentHashMap<>();

    @DexIgnore
    public void a(String str) {
        this.a = b(str);
    }

    @DexIgnore
    public String b() {
        return this.a;
    }

    @DexIgnore
    public static String b(String str) {
        if (str == null) {
            return str;
        }
        String trim = str.trim();
        return trim.length() > 1024 ? trim.substring(0, 1024) : trim;
    }

    @DexIgnore
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.b);
    }

    @DexIgnore
    public void a(String str, String str2) {
        String str3;
        if (str != null) {
            String b2 = b(str);
            if (this.b.size() < 64 || this.b.containsKey(b2)) {
                if (str2 == null) {
                    str3 = "";
                } else {
                    str3 = b(str2);
                }
                this.b.put(b2, str3);
                return;
            }
            z24.a().a("Exceeded maximum number of custom attributes (64)");
            return;
        }
        throw new IllegalArgumentException("Custom attribute key must not be null.");
    }
}
