package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n83 extends tm2 implements w63 {
    @DexIgnore
    public n83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final float C() throws RemoteException {
        Parcel a = a(2, zza());
        float readFloat = a.readFloat();
        a.recycle();
        return readFloat;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        b(4, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void b(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(93, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void c(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        b(92, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void clear() throws RemoteException {
        b(14, zza());
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void d(ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        b(5, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final float g() throws RemoteException {
        Parcel a = a(3, zza());
        float readFloat = a.readFloat();
        a.recycle();
        return readFloat;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final CameraPosition m() throws RemoteException {
        Parcel a = a(1, zza());
        CameraPosition cameraPosition = (CameraPosition) xm2.a(a, CameraPosition.CREATOR);
        a.recycle();
        return cameraPosition;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final boolean s() throws RemoteException {
        Parcel a = a(17, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void setBuildingsEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(41, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final boolean setIndoorEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        Parcel a = a(20, zza);
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void setMapType(int i) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        b(16, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void setMyLocationEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(22, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void setTrafficEnabled(boolean z) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, z);
        b(18, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void u() throws RemoteException {
        b(94, zza());
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final c73 w() throws RemoteException {
        c73 c73;
        Parcel a = a(25, zza());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            c73 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            if (queryLocalInterface instanceof c73) {
                c73 = (c73) queryLocalInterface;
            } else {
                c73 = new g83(readStrongBinder);
            }
        }
        a.recycle();
        return c73;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final boolean x() throws RemoteException {
        Parcel a = a(40, zza());
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final z63 y() throws RemoteException {
        z63 z63;
        Parcel a = a(26, zza());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            z63 = null;
        } else {
            IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IProjectionDelegate");
            if (queryLocalInterface instanceof z63) {
                z63 = (z63) queryLocalInterface;
            } else {
                z63 = new b83(readStrongBinder);
            }
        }
        a.recycle();
        return z63;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(ab2 ab2, j83 j83) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        xm2.a(zza, j83);
        b(6, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final kn2 a(p93 p93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, p93);
        Parcel a = a(9, zza);
        kn2 a2 = um2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final hn2 a(n93 n93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, n93);
        Parcel a = a(10, zza);
        hn2 a2 = in2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final en2 a(k93 k93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, k93);
        Parcel a = a(11, zza);
        en2 a2 = fn2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(i73 i73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, i73);
        b(28, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(m73 m73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, m73);
        b(29, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(q73 q73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, q73);
        b(30, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(s73 s73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, s73);
        b(31, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(g73 g73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, g73);
        b(32, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final bn2 a(d93 d93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, d93);
        Parcel a = a(35, zza);
        bn2 a2 = cn2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(c83 c83, ab2 ab2) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, c83);
        xm2.a(zza, ab2);
        b(38, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(int i, int i2, int i3, int i4) throws RemoteException {
        Parcel zza = zza();
        zza.writeInt(i);
        zza.writeInt(i2);
        zza.writeInt(i3);
        zza.writeInt(i4);
        b(39, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(k73 k73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, k73);
        b(42, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(v73 v73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, v73);
        b(85, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(x73 x73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, x73);
        b(87, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(w83 w83) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, w83);
        b(89, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(LatLngBounds latLngBounds) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, latLngBounds);
        b(95, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(u83 u83) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, u83);
        b(96, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(s83 s83) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, s83);
        b(97, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final void a(q83 q83) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, q83);
        b(99, zza);
    }

    @DexIgnore
    @Override // com.fossil.w63
    public final boolean a(i93 i93) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, i93);
        Parcel a = a(91, zza);
        boolean a2 = xm2.a(a);
        a.recycle();
        return a2;
    }
}
