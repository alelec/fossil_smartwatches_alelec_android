package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq3 implements Parcelable.Creator<eq3> {
    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ eq3 createFromParcel(Parcel parcel) {
        int b = j72.b(parcel);
        int i = 0;
        gq3 gq3 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = j72.a(parcel);
            int a2 = j72.a(a);
            if (a2 == 2) {
                gq3 = (gq3) j72.a(parcel, a, gq3.CREATOR);
            } else if (a2 == 3) {
                i = j72.q(parcel, a);
            } else if (a2 == 4) {
                i2 = j72.q(parcel, a);
            } else if (a2 != 5) {
                j72.v(parcel, a);
            } else {
                i3 = j72.q(parcel, a);
            }
        }
        j72.h(parcel, b);
        return new eq3(gq3, i, i2, i3);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public final /* synthetic */ eq3[] newArray(int i) {
        return new eq3[i];
    }
}
