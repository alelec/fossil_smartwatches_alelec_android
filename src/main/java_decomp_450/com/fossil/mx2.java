package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mx2 extends lx2, Cloneable {
    @DexIgnore
    mx2 a(jx2 jx2);

    @DexIgnore
    mx2 a(byte[] bArr) throws iw2;

    @DexIgnore
    mx2 a(byte[] bArr, nv2 nv2) throws iw2;

    @DexIgnore
    jx2 b();

    @DexIgnore
    jx2 g();
}
