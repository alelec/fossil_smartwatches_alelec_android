package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq3 extends i72 implements xp3 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nq3> CREATOR; // = new oq3();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public nq3(int i, String str, byte[] bArr, String str2) {
        this.a = i;
        this.b = str;
        this.c = bArr;
        this.d = str2;
    }

    @DexIgnore
    public final byte[] e() {
        return this.c;
    }

    @DexIgnore
    public final int g() {
        return this.a;
    }

    @DexIgnore
    public final String getPath() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        int i = this.a;
        String str = this.b;
        byte[] bArr = this.c;
        String valueOf = String.valueOf(bArr == null ? "null" : Integer.valueOf(bArr.length));
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 43 + String.valueOf(valueOf).length());
        sb.append("MessageEventParcelable[");
        sb.append(i);
        sb.append(",");
        sb.append(str);
        sb.append(", size=");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    @DexIgnore
    public final String v() {
        return this.d;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, g());
        k72.a(parcel, 3, getPath(), false);
        k72.a(parcel, 4, e(), false);
        k72.a(parcel, 5, v(), false);
        k72.a(parcel, a2);
    }
}
