package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<hf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public hf0 createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(bc0.class.getClassLoader());
            if (readParcelable != null) {
                ee7.a((Object) readParcelable, "parcel.readParcelable<Re\u2026class.java.classLoader)!!");
                bc0 bc0 = (bc0) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(df0.class.getClassLoader());
                if (readParcelable2 != null) {
                    return new hf0(bc0, (df0) readParcelable2);
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public hf0[] newArray(int i) {
            return new hf0[i];
        }
    }

    @DexIgnore
    public hf0(bc0 bc0, df0 df0) {
        super(bc0, df0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            df0 deviceMessage = getDeviceMessage();
            if (deviceMessage != null) {
                jSONObject.put("workoutApp._.config.response", deviceMessage.a());
                JSONObject jSONObject2 = new JSONObject();
                String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
                try {
                    JSONObject jSONObject3 = new JSONObject();
                    jSONObject3.put("id", valueOf);
                    jSONObject3.put("set", jSONObject);
                    jSONObject2.put(str, jSONObject3);
                } catch (JSONException e) {
                    wl0.h.a(e);
                }
                String jSONObject4 = jSONObject2.toString();
                ee7.a((Object) jSONObject4, "deviceResponseJSONObject.toString()");
                Charset c = b21.x.c();
                if (jSONObject4 != null) {
                    byte[] bytes = jSONObject4.getBytes(c);
                    ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                    return bytes;
                }
                throw new x87("null cannot be cast to non-null type java.lang.String");
            }
            ee7.a();
            throw null;
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
    }
}
