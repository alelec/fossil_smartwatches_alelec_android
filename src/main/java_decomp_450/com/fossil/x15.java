package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x15 extends w15 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B;
    @DexIgnore
    public long z;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        B = sparseIntArray;
        sparseIntArray.put(2131363377, 1);
        B.put(2131363378, 2);
        B.put(2131362328, 3);
        B.put(2131362039, 4);
        B.put(2131362329, 5);
        B.put(2131362316, 6);
        B.put(2131362322, 7);
        B.put(2131362973, 8);
    }
    */

    @DexIgnore
    public x15(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 9, A, B));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    public x15(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[4], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[7], (FlexibleTextView) objArr[3], (FlexibleTextView) objArr[5], (ConstraintLayout) objArr[0], (RecyclerView) objArr[8], (View) objArr[1], (View) objArr[2]);
        this.z = -1;
        ((w15) this).v.setTag(null);
        a(view);
        f();
    }
}
