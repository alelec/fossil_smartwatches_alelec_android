package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d76 extends b76 {
    @DexIgnore
    public /* final */ ad5 A;
    @DexIgnore
    public /* final */ SummariesRepository B;
    @DexIgnore
    public /* final */ GoalTrackingRepository C;
    @DexIgnore
    public /* final */ SleepSummariesRepository D;
    @DexIgnore
    public /* final */ ch5 E;
    @DexIgnore
    public /* final */ HeartRateSampleRepository F;
    @DexIgnore
    public /* final */ ro4 G;
    @DexIgnore
    public Date e;
    @DexIgnore
    public int f;
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE h;
    @DexIgnore
    public MFSleepDay i;
    @DexIgnore
    public ActivitySummary j;
    @DexIgnore
    public GoalTrackingSummary k;
    @DexIgnore
    public Integer l;
    @DexIgnore
    public List<HeartRateSample> m;
    @DexIgnore
    public HashMap<Integer, Boolean> n; // = new HashMap<>();
    @DexIgnore
    public boolean o;
    @DexIgnore
    public volatile boolean p;
    @DexIgnore
    public /* final */ c q; // = new c();
    @DexIgnore
    public /* final */ MutableLiveData<Date> r;
    @DexIgnore
    public /* final */ LiveData<qx6<ActivitySummary>> s;
    @DexIgnore
    public /* final */ LiveData<qx6<MFSleepDay>> t;
    @DexIgnore
    public /* final */ LiveData<qx6<GoalTrackingSummary>> u;
    @DexIgnore
    public /* final */ LiveData<qx6<Integer>> v;
    @DexIgnore
    public LiveData<qx6<List<HeartRateSample>>> w;
    @DexIgnore
    public /* final */ c76 x;
    @DexIgnore
    public /* final */ PortfolioApp y;
    @DexIgnore
    public /* final */ DeviceRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1", f = "HomeDashboardPresenter.kt", l = {125, 126, 127, 128}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1", f = "HomeDashboardPresenter.kt", l = {125}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ActivityStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ActivityStatistic> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    SummariesRepository n = this.this$0.this$0.B;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = n.getActivityStatisticAwait(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d76$b$b")
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        /* renamed from: com.fossil.d76$b$b  reason: collision with other inner class name */
        public static final class C0038b extends zb7 implements kd7<yi7, fb7<? super String>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0038b(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                C0038b bVar = new C0038b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super String> fb7) {
                return ((C0038b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.z.getDeviceNameBySerial(this.this$0.this$0.g);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$sleepStatistic$1", f = "HomeDashboardPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super SleepStatistic>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super SleepStatistic> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    SleepSummariesRepository m = this.this$0.this$0.D;
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = m.getSleepStatisticAwait(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$totalDevices$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class d extends zb7 implements kd7<yi7, fb7<? super List<? extends Device>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                d dVar = new d(this.this$0, fb7);
                dVar.p$ = (yi7) obj;
                return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends Device>> fb7) {
                return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.z.getAllDevice();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(d76 d76, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00ab A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x00ac  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00cd A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00ce  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00e6  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00e8  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0114  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x011d  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0128  */
        /* JADX WARNING: Removed duplicated region for block: B:47:0x0131  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0153  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0204  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r11.label
                r2 = 4
                r3 = 3
                r4 = 2
                r5 = 1
                r6 = 0
                if (r1 == 0) goto L_0x0057
                if (r1 == r5) goto L_0x004f
                if (r1 == r4) goto L_0x0043
                if (r1 == r3) goto L_0x0032
                if (r1 != r2) goto L_0x002a
                java.lang.Object r0 = r11.L$3
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r1 = r11.L$2
                com.portfolio.platform.data.SleepStatistic r1 = (com.portfolio.platform.data.SleepStatistic) r1
                java.lang.Object r2 = r11.L$1
                com.portfolio.platform.data.ActivityStatistic r2 = (com.portfolio.platform.data.ActivityStatistic) r2
                java.lang.Object r3 = r11.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r12)
                goto L_0x00d1
            L_0x002a:
                java.lang.IllegalStateException r12 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r12.<init>(r0)
                throw r12
            L_0x0032:
                java.lang.Object r1 = r11.L$2
                com.portfolio.platform.data.SleepStatistic r1 = (com.portfolio.platform.data.SleepStatistic) r1
                java.lang.Object r3 = r11.L$1
                com.portfolio.platform.data.ActivityStatistic r3 = (com.portfolio.platform.data.ActivityStatistic) r3
                java.lang.Object r7 = r11.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r12)
                goto L_0x00b0
            L_0x0043:
                java.lang.Object r1 = r11.L$1
                com.portfolio.platform.data.ActivityStatistic r1 = (com.portfolio.platform.data.ActivityStatistic) r1
                java.lang.Object r7 = r11.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r12)
                goto L_0x0090
            L_0x004f:
                java.lang.Object r1 = r11.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r12)
                goto L_0x0072
            L_0x0057:
                com.fossil.t87.a(r12)
                com.fossil.yi7 r1 = r11.p$
                com.fossil.d76 r12 = r11.this$0
                com.fossil.ti7 r12 = r12.c()
                com.fossil.d76$b$a r7 = new com.fossil.d76$b$a
                r7.<init>(r11, r6)
                r11.L$0 = r1
                r11.label = r5
                java.lang.Object r12 = com.fossil.vh7.a(r12, r7, r11)
                if (r12 != r0) goto L_0x0072
                return r0
            L_0x0072:
                com.portfolio.platform.data.ActivityStatistic r12 = (com.portfolio.platform.data.ActivityStatistic) r12
                com.fossil.d76 r7 = r11.this$0
                com.fossil.ti7 r7 = r7.c()
                com.fossil.d76$b$c r8 = new com.fossil.d76$b$c
                r8.<init>(r11, r6)
                r11.L$0 = r1
                r11.L$1 = r12
                r11.label = r4
                java.lang.Object r7 = com.fossil.vh7.a(r7, r8, r11)
                if (r7 != r0) goto L_0x008c
                return r0
            L_0x008c:
                r10 = r1
                r1 = r12
                r12 = r7
                r7 = r10
            L_0x0090:
                com.portfolio.platform.data.SleepStatistic r12 = (com.portfolio.platform.data.SleepStatistic) r12
                com.fossil.d76 r8 = r11.this$0
                com.fossil.ti7 r8 = r8.c()
                com.fossil.d76$b$d r9 = new com.fossil.d76$b$d
                r9.<init>(r11, r6)
                r11.L$0 = r7
                r11.L$1 = r1
                r11.L$2 = r12
                r11.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r8, r9, r11)
                if (r3 != r0) goto L_0x00ac
                return r0
            L_0x00ac:
                r10 = r1
                r1 = r12
                r12 = r3
                r3 = r10
            L_0x00b0:
                java.util.List r12 = (java.util.List) r12
                com.fossil.d76 r8 = r11.this$0
                com.fossil.ti7 r8 = r8.c()
                com.fossil.d76$b$b r9 = new com.fossil.d76$b$b
                r9.<init>(r11, r6)
                r11.L$0 = r7
                r11.L$1 = r3
                r11.L$2 = r1
                r11.L$3 = r12
                r11.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r8, r9, r11)
                if (r2 != r0) goto L_0x00ce
                return r0
            L_0x00ce:
                r0 = r12
                r12 = r2
                r2 = r3
            L_0x00d1:
                java.lang.String r12 = (java.lang.String) r12
                com.portfolio.platform.PortfolioApp$a r3 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r3 = r3.c()
                com.fossil.d76 r7 = r11.this$0
                java.lang.String r7 = r7.g
                int r3 = r3.f(r7)
                r7 = 0
                if (r3 != r4) goto L_0x00e8
                r3 = 1
                goto L_0x00e9
            L_0x00e8:
                r3 = 0
            L_0x00e9:
                com.fossil.d76 r4 = r11.this$0
                if (r2 == 0) goto L_0x00f3
                int r8 = r2.getTotalSteps()
                if (r8 != 0) goto L_0x00fc
            L_0x00f3:
                if (r1 == 0) goto L_0x00fe
                int r8 = r1.getTotalSleeps()
                if (r8 != 0) goto L_0x00fc
                goto L_0x00fe
            L_0x00fc:
                r8 = 0
                goto L_0x00ff
            L_0x00fe:
                r8 = 1
            L_0x00ff:
                r4.o = r8
                com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "checkDeviceStatus activity "
                r8.append(r9)
                if (r2 == 0) goto L_0x011d
                int r2 = r2.getTotalSteps()
                java.lang.Integer r2 = com.fossil.pb7.a(r2)
                goto L_0x011e
            L_0x011d:
                r2 = r6
            L_0x011e:
                r8.append(r2)
                java.lang.String r2 = " sleep "
                r8.append(r2)
                if (r1 == 0) goto L_0x0131
                int r1 = r1.getTotalSleeps()
                java.lang.Integer r1 = com.fossil.pb7.a(r1)
                goto L_0x0132
            L_0x0131:
                r1 = r6
            L_0x0132:
                r8.append(r1)
                java.lang.String r1 = " totalDevices "
                r8.append(r1)
                int r1 = r0.size()
                r8.append(r1)
                java.lang.String r1 = r8.toString()
                java.lang.String r2 = "HomeDashboardPresenter"
                r4.d(r2, r1)
                boolean r1 = r0.isEmpty()
                r1 = r1 ^ r5
                java.lang.String r4 = ""
                if (r1 == 0) goto L_0x0204
                java.util.Iterator r0 = r0.iterator()
            L_0x0157:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x017d
                java.lang.Object r1 = r0.next()
                r8 = r1
                com.portfolio.platform.data.model.Device r8 = (com.portfolio.platform.data.model.Device) r8
                java.lang.String r8 = r8.getDeviceId()
                com.fossil.d76 r9 = r11.this$0
                java.lang.String r9 = r9.g
                boolean r8 = com.fossil.ee7.a(r8, r9)
                java.lang.Boolean r8 = com.fossil.pb7.a(r8)
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x0157
                goto L_0x017e
            L_0x017d:
                r1 = r6
            L_0x017e:
                com.portfolio.platform.data.model.Device r1 = (com.portfolio.platform.data.model.Device) r1
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                java.lang.String r9 = "checkDeviceStatus activeDevice "
                r8.append(r9)
                if (r1 == 0) goto L_0x0197
                java.lang.String r9 = r1.getDeviceId()
                goto L_0x0198
            L_0x0197:
                r9 = r6
            L_0x0198:
                r8.append(r9)
                java.lang.String r9 = " currentSerial "
                r8.append(r9)
                com.fossil.d76 r9 = r11.this$0
                java.lang.String r9 = r9.g
                r8.append(r9)
                java.lang.String r8 = r8.toString()
                r0.d(r2, r8)
                if (r1 == 0) goto L_0x01eb
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r4 = "checkDeviceStatus isActiveDeviceConnected "
                r1.append(r4)
                r1.append(r3)
                java.lang.String r1 = r1.toString()
                r0.d(r2, r1)
                com.fossil.d76 r0 = r11.this$0
                com.fossil.c76 r0 = r0.x
                com.fossil.d76 r1 = r11.this$0
                java.lang.String r1 = r1.g
                r0.b(r1, r12)
                com.fossil.d76 r12 = r11.this$0
                com.fossil.c76 r12 = r12.x
                com.fossil.d76 r0 = r11.this$0
                boolean r0 = r0.o
                r12.a(r5, r5, r0)
                goto L_0x021c
            L_0x01eb:
                com.fossil.d76 r12 = r11.this$0
                com.fossil.c76 r12 = r12.x
                r12.b(r6, r4)
                com.fossil.d76 r12 = r11.this$0
                com.fossil.c76 r12 = r12.x
                com.fossil.d76 r0 = r11.this$0
                boolean r0 = r0.o
                r12.a(r5, r7, r0)
                goto L_0x021c
            L_0x0204:
                com.fossil.d76 r12 = r11.this$0
                com.fossil.c76 r12 = r12.x
                r12.b(r6, r4)
                com.fossil.d76 r12 = r11.this$0
                com.fossil.c76 r12 = r12.x
                com.fossil.d76 r0 = r11.this$0
                boolean r0 = r0.o
                r12.a(r7, r7, r0)
            L_0x021c:
                com.fossil.d76 r12 = r11.this$0
                r12.o()
                com.fossil.d76 r12 = r11.this$0
                r12.p()
                com.fossil.i97 r12 = com.fossil.i97.a
                return r12
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.d76.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ee7.b(context, "context");
            ee7.b(intent, "intent");
            String stringExtra = intent.getStringExtra(Constants.SERIAL_NUMBER);
            int intExtra = intent.getIntExtra(Constants.CONNECTION_STATE, ConnectionStateChange.GATT_OFF.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mConnectionStateChangeReceiver: serial = " + stringExtra + ", state = " + intExtra);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalSettingTargetLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {103, 103}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends Integer>>, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends Integer>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    GoalTrackingRepository f = this.this$0.a.C;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = f.getLastGoalSettingLiveData(this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<Integer>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalSettingTargetLiveData on date changed " + date);
            return ed.a(null, 0, new a(this, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mGoalTrackingSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {97, 97}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends GoalTrackingSummary>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends GoalTrackingSummary>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    GoalTrackingRepository f = this.this$0.a.C;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = f.getSummary(date, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public e(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<GoalTrackingSummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mGoalTrackingSummaryLiveData on date changed " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mHeartRateSampleLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {109, 109}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<HeartRateSample>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<HeartRateSample>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    HeartRateSampleRepository h = this.this$0.a.F;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = h.getHeartRateSamples(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public f(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<HeartRateSample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mHeartRateSampleLiveData on date changed " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSleepSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {91, 91}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends MFSleepDay>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends MFSleepDay>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SleepSummariesRepository m = this.this$0.a.D;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = m.getSleepSummary(date, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public g(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<MFSleepDay>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSleepSummaryLiveData on date changed " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$mSummaryLiveData$1$1", f = "HomeDashboardPresenter.kt", l = {85, 85}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends ActivitySummary>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends ActivitySummary>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SummariesRepository n = this.this$0.a.B;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = n.getSummary(date, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<ActivitySummary>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "mSummaryLiveData on date changed " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$1", f = "HomeDashboardPresenter.kt", l = {371}, m = "invokeSuspend")
    public static final class i extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d76 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(d76 d76, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            i iVar = new i(this.this$0, fb7);
            iVar.p$ = (yi7) obj;
            return iVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((i) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Boolean P = this.this$0.E.P();
                Boolean a2 = this.this$0.E.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync successfully - isNeedGetSyncDataForWatch: " + P);
                if (ee7.a(P, pb7.a(true))) {
                    ee7.a((Object) a2, "isBcOn");
                    if (a2.booleanValue()) {
                        d76 d76 = this.this$0;
                        this.L$0 = yi7;
                        this.L$1 = P;
                        this.L$2 = a2;
                        this.label = 1;
                        if (d76.a(this) == a) {
                            return a;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$onRetrieveSyncEvent$2", f = "HomeDashboardPresenter.kt", l = {404}, m = "invokeSuspend")
    public static final class j extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d76 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(d76 d76, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            j jVar = new j(this.this$0, fb7);
            jVar.p$ = (yi7) obj;
            return jVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((j) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Boolean P = this.this$0.E.P();
                Boolean a2 = this.this$0.E.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("HomeDashboardPresenter", "consider retry sync data after sync failed - isNeedGetSyncDataForWatch: " + P);
                if (ee7.a(P, pb7.a(true))) {
                    ee7.a((Object) a2, "isBcOn");
                    if (a2.booleanValue() && PortfolioApp.g0.c().e()) {
                        d76 d76 = this.this$0;
                        this.L$0 = yi7;
                        this.L$1 = P;
                        this.L$2 = a2;
                        this.label = 1;
                        if (d76.a(this) == a) {
                            return a;
                        }
                    }
                }
            } else if (i == 1) {
                Boolean bool = (Boolean) this.L$2;
                Boolean bool2 = (Boolean) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter", f = "HomeDashboardPresenter.kt", l = {425}, m = "retrySetSyncData")
    public static final class k extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ d76 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(d76 d76, fb7 fb7) {
            super(fb7);
            this.this$0 = d76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1", f = "HomeDashboardPresenter.kt", l = {159}, m = "invokeSuspend")
    public static final class l extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ d76 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$showLayoutBattery$1$activeDevice$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ l this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(l lVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = lVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Device> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return this.this$0.this$0.z.getDeviceBySerial(PortfolioApp.g0.c().c());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(d76 d76, fb7 fb7) {
            super(2, fb7);
            this.this$0 = d76;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            l lVar = new l(this.this$0, fb7);
            lVar.p$ = (yi7) obj;
            return lVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((l) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.c();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "showLayoutBattery device " + device);
            if (device != null) {
                if (ee7.a((Object) "release", (Object) "debug") || ee7.a((Object) "release", (Object) "staging")) {
                    if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= PortfolioApp.g0.c().v().z() || device.getBatteryLevel() <= 0) {
                        this.this$0.x.C(false);
                    } else {
                        this.this$0.x.C(true);
                    }
                } else if (FossilDeviceSerialPatternUtil.isDianaDevice(device.getDeviceId()) || device.getBatteryLevel() >= 25 || device.getBatteryLevel() <= 0) {
                    this.this$0.x.C(false);
                } else {
                    this.this$0.x.C(true);
                }
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements zd<qx6<? extends ActivitySummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore
        public m(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<ActivitySummary> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "XXX- summaryChanged -- summary=" + qx6 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(0)));
            ActivitySummary activitySummary = null;
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                if (qx6 != null) {
                    activitySummary = qx6.c();
                }
                if ((!ee7.a((Object) ((Boolean) this.a.n.get(0)), (Object) true)) || (!ee7.a(this.a.j, activitySummary))) {
                    this.a.j = activitySummary;
                    this.a.p();
                    this.a.n.put(0, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n<T> implements zd<qx6<? extends MFSleepDay>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore
        public n(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<MFSleepDay> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mSleepSummaryLiveData -- summary=" + qx6 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(5)));
            MFSleepDay mFSleepDay = null;
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                if (qx6 != null) {
                    mFSleepDay = qx6.c();
                }
                if ((!ee7.a((Object) ((Boolean) this.a.n.get(5)), (Object) true)) || (!ee7.a(this.a.i, mFSleepDay))) {
                    this.a.i = mFSleepDay;
                    this.a.p();
                    this.a.n.put(5, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o<T> implements zd<qx6<? extends GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore
        public o(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<GoalTrackingSummary> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalTrackingSummaryLiveData -- summary=" + qx6 + ", mFirstDBLoad=" + ((Boolean) this.a.n.get(4)));
            GoalTrackingSummary goalTrackingSummary = null;
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                if (qx6 != null) {
                    goalTrackingSummary = qx6.c();
                }
                if ((!ee7.a((Object) ((Boolean) this.a.n.get(4)), (Object) true)) || (!ee7.a(this.a.k, goalTrackingSummary))) {
                    this.a.k = goalTrackingSummary;
                    this.a.p();
                    this.a.n.put(4, true);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p<T> implements zd<qx6<? extends Integer>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore
        public p(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<Integer> qx6) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- resource=" + qx6);
            Integer num = null;
            if ((qx6 != null ? qx6.d() : null) != lb5.DATABASE_LOADING) {
                if (qx6 != null) {
                    num = qx6.c();
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HomeDashboardPresenter", "start - mGoalSettingTargetLiveData -- goal=" + num);
                if (!ee7.a(this.a.l, num)) {
                    this.a.l = num;
                    this.a.p();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q<T> implements zd<qx6<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ d76 a;

        @DexIgnore
        public q(d76 d76) {
            this.a = d76;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<HeartRateSample>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSampleLiveData -- resource.status=");
            sb.append(a2);
            sb.append(", ");
            sb.append("data.size=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", mFirstDBLoad=");
            sb.append((Boolean) this.a.n.get(3));
            local.d("HomeDashboardPresenter", sb.toString());
            if (a2 == lb5.DATABASE_LOADING) {
                return;
            }
            if ((!ee7.a((Object) ((Boolean) this.a.n.get(3)), (Object) true)) || (!ee7.a(this.a.m, list))) {
                this.a.m = list;
                this.a.p();
                this.a.n.put(3, true);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public d76(c76 c76, PortfolioApp portfolioApp, DeviceRepository deviceRepository, ad5 ad5, SummariesRepository summariesRepository, GoalTrackingRepository goalTrackingRepository, SleepSummariesRepository sleepSummariesRepository, ch5 ch5, HeartRateSampleRepository heartRateSampleRepository, ro4 ro4) {
        ee7.b(c76, "mView");
        ee7.b(portfolioApp, "mApp");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(ad5, "mDeviceSettingFactory");
        ee7.b(summariesRepository, "mSummaryRepository");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(sleepSummariesRepository, "mSleepSummaryRepository");
        ee7.b(ch5, "sharedPreferencesManager");
        ee7.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        ee7.b(ro4, "challengeRepository");
        this.x = c76;
        this.y = portfolioApp;
        this.z = deviceRepository;
        this.A = ad5;
        this.B = summariesRepository;
        this.C = goalTrackingRepository;
        this.D = sleepSummariesRepository;
        this.E = ch5;
        this.F = heartRateSampleRepository;
        this.G = ro4;
        String c2 = portfolioApp.c();
        this.g = c2;
        this.h = FossilDeviceSerialPatternUtil.getDeviceBySerial(c2);
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.r = mutableLiveData;
        LiveData<qx6<ActivitySummary>> b2 = ge.b(mutableLiveData, new h(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.s = b2;
        LiveData<qx6<MFSleepDay>> b3 = ge.b(this.r, new g(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.t = b3;
        LiveData<qx6<GoalTrackingSummary>> b4 = ge.b(this.r, new e(this));
        ee7.a((Object) b4, "Transformations.switchMa\u2026mary(it))\n        }\n    }");
        this.u = b4;
        LiveData<qx6<Integer>> b5 = ge.b(this.r, new d(this));
        ee7.a((Object) b5, "Transformations.switchMa\u2026veData())\n        }\n    }");
        this.v = b5;
        LiveData<qx6<List<HeartRateSample>>> b6 = ge.b(this.r, new f(this));
        ee7.a((Object) b6, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.w = b6;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.x.c(z2);
    }

    @DexIgnore
    public final void c(int i2) {
        this.x.f(i2);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "start - this=" + hashCode());
        Date date = new Date();
        Date date2 = this.e;
        if (date2 == null || !zd5.d(date, date2)) {
            this.e = date;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("HomeDashboardPresenter", "date change " + this.e);
            this.r.a(this.e);
        }
        PortfolioApp portfolioApp = this.y;
        c cVar = this.q;
        portfolioApp.registerReceiver(cVar, new IntentFilter(this.y.getPackageName() + ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE()));
        k();
        LiveData<qx6<ActivitySummary>> liveData = this.s;
        c76 c76 = this.x;
        if (c76 != null) {
            liveData.a(((sr5) c76).getViewLifecycleOwner(), new m(this));
            this.t.a(((sr5) this.x).getViewLifecycleOwner(), new n(this));
            this.u.a(((sr5) this.x).getViewLifecycleOwner(), new o(this));
            this.v.a(((sr5) this.x).getViewLifecycleOwner(), new p(this));
            this.w.a(((sr5) this.x).getViewLifecycleOwner(), new q(this));
            l();
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "stop");
        try {
            this.y.unregisterReceiver(this.q);
            c76 c76 = this.x;
            if (c76 != null) {
                LifecycleOwner viewLifecycleOwner = ((sr5) c76).getViewLifecycleOwner();
                this.s.a(viewLifecycleOwner);
                this.t.a(viewLifecycleOwner);
                this.u.a(viewLifecycleOwner);
                this.v.a(viewLifecycleOwner);
                this.w.a(viewLifecycleOwner);
                PortfolioApp.g0.c().d().a(viewLifecycleOwner);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeDashboardFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("HomeDashboardPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.b76
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.h;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.b76
    public int i() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.b76
    public void j() {
        String c2 = this.y.c();
        int e2 = this.y.e(c2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeDashboardPresenter", "Inside .onRefresh currentDeviceSession=" + e2);
        if (TextUtils.isEmpty(c2) || e2 == CommunicateMode.OTA.getValue()) {
            this.x.q(false);
            return;
        }
        this.p = true;
        this.x.Y0();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.y.c(), "HomeDashboardPresenter", "[Sync Start] PULL TO SYNC");
        this.y.a(this.A, false, 12);
    }

    @DexIgnore
    public final ik7 k() {
        return xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    public final void l() {
        PortfolioApp portfolioApp = this.y;
        if (portfolioApp.g(portfolioApp.c()) && !this.p) {
            this.p = true;
            this.x.Y0();
        }
    }

    @DexIgnore
    public void m() {
        this.x.Q0();
    }

    @DexIgnore
    public void n() {
        this.x.a(this);
    }

    @DexIgnore
    public final void o() {
        ik7 unused = xh7.b(e(), null, null, new l(this, null), 3, null);
    }

    @DexIgnore
    public final void p() {
        Integer num;
        HeartRateSample heartRateSample;
        Resting resting;
        boolean z2;
        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "updateHomeInfo");
        this.x.a(this.j, this.i, this.k);
        List<HeartRateSample> list = this.m;
        if (list != null) {
            ListIterator<HeartRateSample> listIterator = list.listIterator(list.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    heartRateSample = null;
                    break;
                }
                heartRateSample = listIterator.previous();
                if (heartRateSample.getResting() != null) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            HeartRateSample heartRateSample2 = heartRateSample;
            num = Integer.valueOf((heartRateSample2 == null || (resting = heartRateSample2.getResting()) == null) ? 0 : resting.getValue());
        } else {
            num = null;
        }
        this.x.a(this.j, this.i, this.k, this.l, num, (this.g.length() == 0) && this.o);
        c76 c76 = this.x;
        Date date = this.e;
        if (date != null) {
            c76.b(date);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.b76
    public void b(int i2) {
        if (i2 == this.f) {
            this.x.D();
        }
    }

    @DexIgnore
    @Override // com.fossil.b76
    public void a(int i2) {
        this.f = i2;
    }

    @DexIgnore
    @Override // com.fossil.b76
    public void a(String str, boolean z2) {
        ee7.b(str, "activeId");
        this.y.a(str, z2);
    }

    @DexIgnore
    public void a(Intent intent) {
        ee7.b(intent, "intent");
        if (this.x.isActive()) {
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && mh7.b(stringExtra, this.g, true)) {
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeDashboardPresenter", "mSyncReceiver - syncStatus: " + intExtra + " - mIntendingSync: " + this.p);
                if (intExtra != 0) {
                    if (intExtra == 1) {
                        this.p = false;
                        this.x.q(true);
                        FLogger.INSTANCE.getLocal().d("HomeDashboardPresenter", "sync complete, check for low battery");
                        o();
                        ik7 unused = xh7.b(e(), qj7.b(), null, new i(this, null), 2, null);
                    } else if (intExtra == 2 || intExtra == 4) {
                        this.p = false;
                        this.x.q(false);
                        if (intExtra2 == 12) {
                            int intExtra3 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                            if (integerArrayListExtra == null) {
                                integerArrayListExtra = new ArrayList<>();
                            }
                            if (intExtra3 != 1101) {
                                if (intExtra3 == 1603) {
                                    this.x.q(false);
                                    return;
                                } else if (!(intExtra3 == 1112 || intExtra3 == 1113)) {
                                    this.x.W0();
                                    ik7 unused2 = xh7.b(e(), qj7.b(), null, new j(this, null), 2, null);
                                    return;
                                }
                            }
                            List<ib5> convertBLEPermissionErrorCode = ib5.convertBLEPermissionErrorCode(integerArrayListExtra);
                            ee7.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                            c76 c76 = this.x;
                            Object[] array = convertBLEPermissionErrorCode.toArray(new ib5[0]);
                            if (array != null) {
                                ib5[] ib5Arr = (ib5[]) array;
                                c76.a((ib5[]) Arrays.copyOf(ib5Arr, ib5Arr.length));
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                    } else if (intExtra == 5) {
                        this.x.P();
                    } else if (intExtra == 6) {
                        this.p = false;
                        this.x.q(false);
                        this.x.J();
                    }
                } else if (!this.p) {
                    this.p = true;
                    this.x.Y0();
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.b76
    public void a(boolean z2) {
        this.p = z2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a(com.fossil.fb7<? super com.fossil.i97> r6) {
        /*
            r5 = this;
            boolean r0 = r6 instanceof com.fossil.d76.k
            if (r0 == 0) goto L_0x0013
            r0 = r6
            com.fossil.d76$k r0 = (com.fossil.d76.k) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.d76$k r0 = new com.fossil.d76$k
            r0.<init>(r5, r6)
        L_0x0018:
            java.lang.Object r6 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0035
            if (r2 != r3) goto L_0x002d
            java.lang.Object r0 = r0.L$0
            com.fossil.d76 r0 = (com.fossil.d76) r0
            com.fossil.t87.a(r6)
            goto L_0x0046
        L_0x002d:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r0)
            throw r6
        L_0x0035:
            com.fossil.t87.a(r6)
            com.fossil.ro4 r6 = r5.G
            r0.L$0 = r5
            r0.label = r3
            java.lang.Object r6 = r6.b(r0)
            if (r6 != r1) goto L_0x0045
            return r1
        L_0x0045:
            r0 = r5
        L_0x0046:
            com.fossil.ko4 r6 = (com.fossil.ko4) r6
            java.lang.Object r6 = r6.c()
            com.fossil.no4 r6 = (com.fossil.no4) r6
            if (r6 == 0) goto L_0x005e
            com.portfolio.platform.PortfolioApp$a r0 = com.portfolio.platform.PortfolioApp.g0
            com.portfolio.platform.PortfolioApp r0 = r0.c()
            java.lang.String r6 = r6.a()
            r0.n(r6)
            goto L_0x00b7
        L_0x005e:
            com.fossil.ch5 r6 = r0.E
            java.lang.Long r6 = r6.c()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "retrySetSyncData - endTimeOfUnsetChallenge: "
            r2.append(r3)
            r2.append(r6)
            java.lang.String r3 = " - exact: "
            r2.append(r3)
            com.fossil.vt4 r3 = com.fossil.vt4.a
            long r3 = r3.b()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "HomeDashboardPresenter"
            r1.e(r3, r2)
            long r1 = r6.longValue()
            com.fossil.vt4 r6 = com.fossil.vt4.a
            long r3 = r6.b()
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 <= 0) goto L_0x00a2
            com.fossil.qe5 r6 = com.fossil.qe5.c
            r6.d()
            goto L_0x00b7
        L_0x00a2:
            com.fossil.ch5 r6 = r0.E
            r1 = 0
            java.lang.Long r1 = com.fossil.pb7.a(r1)
            r6.a(r1)
            com.fossil.ch5 r6 = r0.E
            r0 = 0
            java.lang.Boolean r0 = com.fossil.pb7.a(r0)
            r6.a(r0)
        L_0x00b7:
            com.fossil.i97 r6 = com.fossil.i97.a
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.d76.a(com.fossil.fb7):java.lang.Object");
    }
}
