package com.fossil;

import android.os.Bundle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.be5;
import com.fossil.rg6;
import com.fossil.sg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class og6 extends jg6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<r87<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE k; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(PortfolioApp.g0.c().c());
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public List<MFSleepSession> m; // = new ArrayList();
    @DexIgnore
    public MFSleepDay n;
    @DexIgnore
    public List<MFSleepSession> o;
    @DexIgnore
    public LiveData<qx6<List<MFSleepDay>>> p;
    @DexIgnore
    public LiveData<qx6<List<MFSleepSession>>> q;
    @DexIgnore
    public /* final */ kg6 r;
    @DexIgnore
    public /* final */ SleepSummariesRepository s;
    @DexIgnore
    public /* final */ SleepSessionsRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fe7 implements gd7<MFSleepSession, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.gd7
        public /* bridge */ /* synthetic */ Boolean invoke(MFSleepSession mFSleepSession) {
            return Boolean.valueOf(invoke(mFSleepSession));
        }

        @DexIgnore
        public final boolean invoke(MFSleepSession mFSleepSession) {
            ee7.b(mFSleepSession, "it");
            return zd5.d(mFSleepSession.getDay(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ og6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$sessionTransformations$1$1", f = "SleepDetailPresenter.kt", l = {65, 65}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<MFSleepSession>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<MFSleepSession>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SleepSessionsRepository f = this.this$0.a.t;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = f.getSleepSessionList(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(og6 og6) {
            this.a = og6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<MFSleepSession>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1", f = "SleepDetailPresenter.kt", l = {150, 174, 175}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ og6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$1", f = "SleepDetailPresenter.kt", l = {150}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Date>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Date> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    PortfolioApp c = PortfolioApp.g0.c();
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = c.g(this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$sessions$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super List<MFSleepSession>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.this$0, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<MFSleepSession>> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    og6 og6 = this.this$0.this$0;
                    return og6.a(og6.f, this.this$0.this$0.m);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$setDate$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super MFSleepDay>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super MFSleepDay> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    og6 og6 = this.this$0.this$0;
                    return og6.b(og6.f, this.this$0.this$0.l);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(og6 og6, Date date, fb7 fb7) {
            super(2, fb7);
            this.this$0 = og6;
            this.$date = date;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, this.$date, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:28:0x019d A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x019e  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x01af  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x01c1  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r17) {
            /*
                r16 = this;
                r0 = r16
                java.lang.Object r1 = com.fossil.nb7.a()
                int r2 = r0.label
                r3 = 3
                r4 = 2
                r5 = 0
                r6 = 1
                if (r2 == 0) goto L_0x0060
                if (r2 == r6) goto L_0x0052
                if (r2 == r4) goto L_0x0037
                if (r2 != r3) goto L_0x002f
                java.lang.Object r1 = r0.L$4
                com.portfolio.platform.data.model.room.sleep.MFSleepDay r1 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r1
                java.lang.Object r2 = r0.L$3
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r2 = r0.L$2
                android.util.Pair r2 = (android.util.Pair) r2
                java.lang.Object r2 = r0.L$1
                java.lang.Boolean r2 = (java.lang.Boolean) r2
                java.lang.Object r2 = r0.L$0
                com.fossil.yi7 r2 = (com.fossil.yi7) r2
                com.fossil.t87.a(r17)
                r2 = r17
                goto L_0x019f
            L_0x002f:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
                r1.<init>(r2)
                throw r1
            L_0x0037:
                java.lang.Object r2 = r0.L$3
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r4 = r0.L$2
                android.util.Pair r4 = (android.util.Pair) r4
                java.lang.Object r7 = r0.L$1
                java.lang.Boolean r7 = (java.lang.Boolean) r7
                boolean r8 = r0.Z$0
                java.lang.Object r9 = r0.L$0
                com.fossil.yi7 r9 = (com.fossil.yi7) r9
                com.fossil.t87.a(r17)
                r10 = r7
                r7 = r4
                r4 = r17
                goto L_0x017c
            L_0x0052:
                java.lang.Object r2 = r0.L$1
                com.fossil.og6 r2 = (com.fossil.og6) r2
                java.lang.Object r7 = r0.L$0
                com.fossil.yi7 r7 = (com.fossil.yi7) r7
                com.fossil.t87.a(r17)
                r8 = r17
                goto L_0x0085
            L_0x0060:
                com.fossil.t87.a(r17)
                com.fossil.yi7 r7 = r0.p$
                com.fossil.og6 r2 = r0.this$0
                java.util.Date r2 = r2.e
                if (r2 != 0) goto L_0x008a
                com.fossil.og6 r2 = r0.this$0
                com.fossil.ti7 r8 = r2.b()
                com.fossil.og6$d$a r9 = new com.fossil.og6$d$a
                r9.<init>(r5)
                r0.L$0 = r7
                r0.L$1 = r2
                r0.label = r6
                java.lang.Object r8 = com.fossil.vh7.a(r8, r9, r0)
                if (r8 != r1) goto L_0x0085
                return r1
            L_0x0085:
                java.util.Date r8 = (java.util.Date) r8
                r2.e = r8
            L_0x008a:
                r9 = r7
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                java.lang.StringBuilder r7 = new java.lang.StringBuilder
                r7.<init>()
                java.lang.String r8 = "setDate - date="
                r7.append(r8)
                java.util.Date r8 = r0.$date
                r7.append(r8)
                java.lang.String r8 = ", createdAt="
                r7.append(r8)
                com.fossil.og6 r8 = r0.this$0
                java.util.Date r8 = r8.e
                r7.append(r8)
                java.lang.String r7 = r7.toString()
                java.lang.String r8 = "SleepDetailPresenter"
                r2.d(r8, r7)
                com.fossil.og6 r2 = r0.this$0
                java.util.Date r7 = r0.$date
                r2.f = r7
                com.fossil.og6 r2 = r0.this$0
                java.util.Date r2 = r2.e
                java.util.Date r7 = r0.$date
                boolean r2 = com.fossil.zd5.c(r2, r7)
                java.util.Date r7 = new java.util.Date
                r7.<init>()
                java.util.Date r10 = r0.$date
                boolean r7 = com.fossil.zd5.c(r7, r10)
                r7 = r7 ^ r6
                java.util.Date r10 = r0.$date
                java.lang.Boolean r10 = com.fossil.zd5.w(r10)
                com.fossil.og6 r11 = r0.this$0
                com.fossil.kg6 r11 = r11.r
                java.util.Date r12 = r0.$date
                java.lang.String r13 = "isToday"
                com.fossil.ee7.a(r10, r13)
                boolean r13 = r10.booleanValue()
                r11.a(r12, r2, r13, r7)
                java.util.Date r7 = r0.$date
                com.fossil.og6 r11 = r0.this$0
                java.util.Date r11 = r11.e
                android.util.Pair r7 = com.fossil.zd5.a(r7, r11)
                java.lang.String r11 = "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)"
                com.fossil.ee7.a(r7, r11)
                com.fossil.og6 r11 = r0.this$0
                androidx.lifecycle.MutableLiveData r11 = r11.g
                java.lang.Object r11 = r11.a()
                com.fossil.r87 r11 = (com.fossil.r87) r11
                com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
                java.lang.StringBuilder r13 = new java.lang.StringBuilder
                r13.<init>()
                java.lang.String r14 = "setDate - rangeDateValue="
                r13.append(r14)
                r13.append(r11)
                java.lang.String r14 = ", newRange="
                r13.append(r14)
                com.fossil.r87 r14 = new com.fossil.r87
                java.lang.Object r15 = r7.first
                java.lang.Object r6 = r7.second
                r14.<init>(r15, r6)
                r13.append(r14)
                java.lang.String r6 = r13.toString()
                r12.d(r8, r6)
                if (r11 == 0) goto L_0x01ea
                java.lang.Object r6 = r11.getFirst()
                java.util.Date r6 = (java.util.Date) r6
                java.lang.Object r8 = r7.first
                java.util.Date r8 = (java.util.Date) r8
                boolean r6 = com.fossil.zd5.d(r6, r8)
                if (r6 == 0) goto L_0x01ea
                java.lang.Object r6 = r11.getSecond()
                java.util.Date r6 = (java.util.Date) r6
                java.lang.Object r8 = r7.second
                java.util.Date r8 = (java.util.Date) r8
                boolean r6 = com.fossil.zd5.d(r6, r8)
                if (r6 != 0) goto L_0x015c
                goto L_0x01ea
            L_0x015c:
                com.fossil.og6 r6 = r0.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.og6$d$c r8 = new com.fossil.og6$d$c
                r8.<init>(r0, r5)
                r0.L$0 = r9
                r0.Z$0 = r2
                r0.L$1 = r10
                r0.L$2 = r7
                r0.L$3 = r11
                r0.label = r4
                java.lang.Object r4 = com.fossil.vh7.a(r6, r8, r0)
                if (r4 != r1) goto L_0x017a
                return r1
            L_0x017a:
                r8 = r2
                r2 = r11
            L_0x017c:
                com.portfolio.platform.data.model.room.sleep.MFSleepDay r4 = (com.portfolio.platform.data.model.room.sleep.MFSleepDay) r4
                com.fossil.og6 r6 = r0.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.og6$d$b r11 = new com.fossil.og6$d$b
                r11.<init>(r0, r5)
                r0.L$0 = r9
                r0.Z$0 = r8
                r0.L$1 = r10
                r0.L$2 = r7
                r0.L$3 = r2
                r0.L$4 = r4
                r0.label = r3
                java.lang.Object r2 = com.fossil.vh7.a(r6, r11, r0)
                if (r2 != r1) goto L_0x019e
                return r1
            L_0x019e:
                r1 = r4
            L_0x019f:
                java.util.List r2 = (java.util.List) r2
                com.fossil.og6 r3 = r0.this$0
                com.portfolio.platform.data.model.room.sleep.MFSleepDay r3 = r3.n
                boolean r3 = com.fossil.ee7.a(r3, r1)
                r4 = 1
                r3 = r3 ^ r4
                if (r3 == 0) goto L_0x01b4
                com.fossil.og6 r3 = r0.this$0
                r3.n = r1
            L_0x01b4:
                com.fossil.og6 r3 = r0.this$0
                java.util.List r3 = r3.o
                boolean r3 = com.fossil.ee7.a(r3, r2)
                r3 = r3 ^ r4
                if (r3 == 0) goto L_0x01c6
                com.fossil.og6 r3 = r0.this$0
                r3.o = r2
            L_0x01c6:
                com.fossil.og6 r2 = r0.this$0
                com.fossil.kg6 r2 = r2.r
                r2.a(r1)
                com.fossil.og6 r1 = r0.this$0
                boolean r1 = r1.i
                if (r1 == 0) goto L_0x0207
                com.fossil.og6 r1 = r0.this$0
                boolean r1 = r1.j
                if (r1 == 0) goto L_0x0207
                com.fossil.og6 r1 = r0.this$0
                com.fossil.ik7 unused = r1.l()
                com.fossil.og6 r1 = r0.this$0
                com.fossil.ik7 unused = r1.m()
                goto L_0x0207
            L_0x01ea:
                com.fossil.og6 r1 = r0.this$0
                r2 = 0
                r1.i = r2
                com.fossil.og6 r1 = r0.this$0
                r1.j = r2
                com.fossil.og6 r1 = r0.this$0
                androidx.lifecycle.MutableLiveData r1 = r1.g
                com.fossil.r87 r2 = new com.fossil.r87
                java.lang.Object r3 = r7.first
                java.lang.Object r4 = r7.second
                r2.<init>(r3, r4)
                r1.a(r2)
            L_0x0207:
                com.fossil.i97 r1 = com.fossil.i97.a
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.og6.d.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$1", f = "SleepDetailPresenter.kt", l = {328}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ og6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showDetailChart$1$data$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super List<? extends sg6.b>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super List<? extends sg6.b>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    og6 og6 = this.this$0.this$0;
                    return og6.b(og6.o);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(og6 og6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = og6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List<sg6.b> list = (List) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepDetailPresenter", "showDetailChart - data=" + list);
            if (!list.isEmpty()) {
                this.this$0.r.j(list);
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1", f = "SleepDetailPresenter.kt", l = {125}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ og6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1$data$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ArrayList<rg6.a>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ArrayList<rg6.a>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    og6 og6 = this.this$0.this$0;
                    return og6.a(og6.o);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(og6 og6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = og6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 a3 = this.this$0.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ArrayList<rg6.a> arrayList = (ArrayList) obj;
            be5.a aVar2 = be5.o;
            String b = this.this$0.h;
            if (b != null) {
                if (aVar2.f(b)) {
                    if (arrayList.isEmpty()) {
                        arrayList.add(new rg6.a(null, 0, 0, 0, 15, null));
                    }
                    this.this$0.r.c(arrayList);
                } else if (arrayList.isEmpty()) {
                    this.this$0.r.E();
                } else {
                    this.this$0.r.c(arrayList);
                }
                return i97.a;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<qx6<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ og6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1", f = "SleepDetailPresenter.kt", l = {86}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.og6$g$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$1$1$summary$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.og6$g$a$a  reason: collision with other inner class name */
            public static final class C0149a extends zb7 implements kd7<yi7, fb7<? super MFSleepDay>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0149a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0149a aVar = new C0149a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super MFSleepDay> fb7) {
                    return ((C0149a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        og6 og6 = this.this$0.this$0.a;
                        return og6.b(og6.f, this.this$0.this$0.a.l);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(g gVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = this.this$0.a.b();
                    C0149a aVar = new C0149a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MFSleepDay mFSleepDay = (MFSleepDay) obj;
                if (this.this$0.a.n == null || (!ee7.a(this.this$0.a.n, mFSleepDay))) {
                    this.this$0.a.n = mFSleepDay;
                    this.this$0.a.r.a(mFSleepDay);
                    if (this.this$0.a.i && this.this$0.a.j) {
                        ik7 unused = this.this$0.a.l();
                        ik7 unused2 = this.this$0.a.m();
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore
        public g(og6 og6) {
            this.a = og6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<MFSleepDay>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - summaryTransformations -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == lb5.NETWORK_LOADING || a2 == lb5.SUCCESS) {
                this.a.l = list;
                this.a.i = true;
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<qx6<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ og6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1", f = "SleepDetailPresenter.kt", l = {110}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ h this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.og6$h$a$a")
            @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$start$2$1$sessions$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
            /* renamed from: com.fossil.og6$h$a$a  reason: collision with other inner class name */
            public static final class C0150a extends zb7 implements kd7<yi7, fb7<? super List<MFSleepSession>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public yi7 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0150a(a aVar, fb7 fb7) {
                    super(2, fb7);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final fb7<i97> create(Object obj, fb7<?> fb7) {
                    ee7.b(fb7, "completion");
                    C0150a aVar = new C0150a(this.this$0, fb7);
                    aVar.p$ = (yi7) obj;
                    return aVar;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.fossil.kd7
                public final Object invoke(yi7 yi7, fb7<? super List<MFSleepSession>> fb7) {
                    return ((C0150a) create(yi7, fb7)).invokeSuspend(i97.a);
                }

                @DexIgnore
                @Override // com.fossil.ob7
                public final Object invokeSuspend(Object obj) {
                    nb7.a();
                    if (this.label == 0) {
                        t87.a(obj);
                        og6 og6 = this.this$0.this$0.a;
                        return og6.a(og6.f, this.this$0.this$0.a.m);
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(h hVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = hVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    ti7 a2 = this.this$0.a.b();
                    C0150a aVar = new C0150a(this, null);
                    this.L$0 = yi7;
                    this.label = 1;
                    obj = vh7.a(a2, aVar, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) obj;
                if (this.this$0.a.o == null || (!ee7.a(this.this$0.a.o, list))) {
                    this.this$0.a.o = list;
                    if (this.this$0.a.i && this.this$0.a.j) {
                        ik7 unused = this.this$0.a.l();
                        ik7 unused2 = this.this$0.a.m();
                    }
                }
                return i97.a;
            }
        }

        @DexIgnore
        public h(og6 og6) {
            this.a = og6;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<MFSleepSession>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - sessionTransformations -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepDetailPresenter", sb.toString());
            if (a2 == lb5.NETWORK_LOADING || a2 == lb5.SUCCESS) {
                this.a.m = list;
                this.a.j = true;
                ik7 unused = xh7.b(this.a.e(), null, null, new a(this, null), 3, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ og6 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$summaryTransformations$1$1", f = "SleepDetailPresenter.kt", l = {61, 61}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<MFSleepDay>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $first;
            @DexIgnore
            public /* final */ /* synthetic */ Date $second;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ i this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(i iVar, Date date, Date date2, fb7 fb7) {
                super(2, fb7);
                this.this$0 = iVar;
                this.$first = date;
                this.$second = date2;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$first, this.$second, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<MFSleepDay>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SleepSummariesRepository k = this.this$0.a.s;
                    Date date = this.$first;
                    Date date2 = this.$second;
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = k.getSleepSummaries(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public i(og6 og6) {
            this.a = og6;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<MFSleepDay>>> apply(r87<? extends Date, ? extends Date> r87) {
            return ed.a(null, 0, new a(this, (Date) r87.component1(), (Date) r87.component2(), null), 3, null);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public og6(kg6 kg6, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository) {
        ee7.b(kg6, "mView");
        ee7.b(sleepSummariesRepository, "mSummariesRepository");
        ee7.b(sleepSessionsRepository, "mSessionsRepository");
        this.r = kg6;
        this.s = sleepSummariesRepository;
        this.t = sleepSessionsRepository;
        LiveData<qx6<List<MFSleepDay>>> b2 = ge.b(this.g, new i(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.p = b2;
        LiveData<qx6<List<MFSleepSession>>> b3 = ge.b(this.g, new c(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026t, second, true)) }\n    }");
        this.q = b3;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.h = PortfolioApp.g0.c().c();
        LiveData<qx6<List<MFSleepDay>>> liveData = this.p;
        kg6 kg6 = this.r;
        if (kg6 != null) {
            liveData.a((lg6) kg6, new g(this));
            this.q.a((LifecycleOwner) this.r, new h(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepDetailPresenter", "stop");
        LiveData<qx6<List<MFSleepDay>>> liveData = this.p;
        kg6 kg6 = this.r;
        if (kg6 != null) {
            liveData.a((lg6) kg6);
            this.q.a((LifecycleOwner) this.r);
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.sleep.SleepDetailFragment");
    }

    @DexIgnore
    @Override // com.fossil.jg6
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.k;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    @Override // com.fossil.jg6
    public void i() {
        Date o2 = zd5.o(this.f);
        ee7.a((Object) o2, "DateHelper.getNextDate(mDate)");
        a(o2);
    }

    @DexIgnore
    @Override // com.fossil.jg6
    public void j() {
        Date p2 = zd5.p(this.f);
        ee7.a((Object) p2, "DateHelper.getPrevDate(mDate)");
        a(p2);
    }

    @DexIgnore
    public void k() {
        this.r.a(this);
    }

    @DexIgnore
    public final ik7 l() {
        return xh7.b(e(), null, null, new e(this, null), 3, null);
    }

    @DexIgnore
    public final ik7 m() {
        return xh7.b(e(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    public final List<sg6.b> b(List<MFSleepSession> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSleepSessionsToDetailChart - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        int a2 = ge5.c.a(this.n);
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                T next = it.next();
                BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
                ArrayList arrayList2 = new ArrayList();
                List<WrapperSleepStateChange> sleepStateChange = next.getSleepStateChange();
                ArrayList arrayList3 = new ArrayList();
                SleepDistribution sleepState = next.getSleepState();
                int realStartTime = next.getRealStartTime();
                int totalMinuteBySleepDistribution = sleepState.getTotalMinuteBySleepDistribution();
                if (sleepStateChange != null) {
                    for (T t2 : sleepStateChange) {
                        BarChart.b bVar = new BarChart.b(0, null, 0, 0, null, 31, null);
                        bVar.a((int) ((WrapperSleepStateChange) t2).index);
                        bVar.c(realStartTime);
                        bVar.b(totalMinuteBySleepDistribution);
                        int i2 = ((WrapperSleepStateChange) t2).state;
                        if (i2 == 0) {
                            bVar.a(BarChart.f.LOWEST);
                        } else if (i2 == 1) {
                            bVar.a(BarChart.f.DEFAULT);
                        } else if (i2 == 2) {
                            bVar.a(BarChart.f.HIGHEST);
                        }
                        arrayList3.add(bVar);
                        it = it;
                    }
                }
                arrayList2.add(arrayList3);
                cVar.a().add(new BarChart.a(a2, arrayList2.size() != 0 ? arrayList2 : w97.a((Object[]) new ArrayList[]{w97.a((Object[]) new BarChart.b[]{new BarChart.b(0, null, 0, 0, null, 23, null)})}), 0, false, 12, null));
                cVar.b(a2);
                cVar.a(a2);
                int awake = sleepState.getAwake();
                int light = sleepState.getLight();
                int deep = sleepState.getDeep();
                if (totalMinuteBySleepDistribution > 0) {
                    float f2 = (float) totalMinuteBySleepDistribution;
                    float f3 = ((float) awake) / f2;
                    float f4 = ((float) light) / f2;
                    arrayList.add(new sg6.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, next.getTimezoneOffset()));
                }
                it = it;
            }
        }
        return arrayList;
    }

    @DexIgnore
    @Override // com.fossil.jg6
    public void a(Date date) {
        ee7.b(date, "date");
        ik7 unused = xh7.b(e(), null, null, new d(this, date, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.jg6
    public void a(Bundle bundle) {
        ee7.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    public final ArrayList<rg6.a> a(List<MFSleepSession> list) {
        short s2;
        short s3;
        short s4;
        short s5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("extractHeartRateDataFromSleepSession - sleepSessions.size=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepDetailPresenter", sb.toString());
        ArrayList<rg6.a> arrayList = new ArrayList<>();
        if (list != null) {
            for (T t2 : list) {
                List<WrapperSleepStateChange> sleepStateChange = t2.getSleepStateChange();
                try {
                    SleepSessionHeartRate heartRate = t2.getHeartRate();
                    if (heartRate != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("extractHeartRateDataFromSleepSession - sleepStates.size=");
                        sb2.append(sleepStateChange != null ? Integer.valueOf(sleepStateChange.size()) : null);
                        sb2.append(", heartRateData.size=");
                        sb2.append(heartRate.getValues().size());
                        local2.d("SleepDetailPresenter", sb2.toString());
                        ArrayList arrayList2 = new ArrayList();
                        int resolutionInSecond = heartRate.getResolutionInSecond();
                        int i2 = 0;
                        short s6 = Short.MAX_VALUE;
                        short s7 = Short.MIN_VALUE;
                        int i3 = 0;
                        for (T t3 : heartRate.getValues()) {
                            int i4 = i3 + 1;
                            if (i3 >= 0) {
                                short shortValue = t3.shortValue();
                                if (s6 > shortValue && shortValue != ((short) i2)) {
                                    s6 = shortValue;
                                }
                                if (s7 < shortValue) {
                                    s7 = shortValue;
                                }
                                ez6 ez6 = new ez6(0, 0, 0, 7, null);
                                ez6.b((i3 * resolutionInSecond) / 60);
                                ez6.c(shortValue);
                                if (sleepStateChange != null) {
                                    int size = sleepStateChange.size();
                                    int i5 = 0;
                                    while (true) {
                                        if (i5 >= size) {
                                            break;
                                        }
                                        if (i5 < w97.a((List) sleepStateChange)) {
                                            s5 = s6;
                                            s4 = s7;
                                            if (sleepStateChange.get(i5).index <= ((long) ez6.e()) && ((long) ez6.e()) < sleepStateChange.get(i5 + 1).index) {
                                                ez6.a(sleepStateChange.get(i5).state);
                                                break;
                                            }
                                        } else {
                                            s5 = s6;
                                            s4 = s7;
                                            ez6.a(sleepStateChange.get(i5).state);
                                        }
                                        i5++;
                                        s6 = s5;
                                        s7 = s4;
                                    }
                                }
                                s5 = s6;
                                s4 = s7;
                                arrayList2.add(ez6);
                                i3 = i4;
                                s6 = s5;
                                s7 = s4;
                                i2 = 0;
                            } else {
                                w97.c();
                                throw null;
                            }
                        }
                        if (s6 == Short.MAX_VALUE) {
                            s3 = Short.MIN_VALUE;
                            s2 = 0;
                        } else {
                            s2 = s6;
                            s3 = Short.MIN_VALUE;
                        }
                        if (s7 == s3) {
                            s7 = 100;
                        }
                        try {
                            arrayList.add(new rg6.a(arrayList2, t2.getRealSleepStateDistInMinute().getTotalMinuteBySleepDistribution(), s2, s7));
                        } catch (Exception e2) {
                            e = e2;
                        }
                    }
                } catch (Exception e3) {
                    e = e3;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("SleepDetailPresenter", "extractHeartRateDataFromSleepSession - e=" + e);
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<MFSleepSession> a(Date date, List<MFSleepSession> list) {
        hg7 b2;
        hg7 a2;
        if (list == null || (b2 = ea7.b((Iterable) list)) == null || (a2 = og7.a(b2, new b(date))) == null) {
            return null;
        }
        return og7.g(a2);
    }

    @DexIgnore
    public final MFSleepDay b(Date date, List<MFSleepDay> list) {
        Calendar instance = Calendar.getInstance();
        ee7.a((Object) instance, "calendar");
        instance.setTime(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepDetailPresenter", "findSleepSummary - date=" + date);
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (zd5.d(instance.getTime(), next.getDate())) {
                t2 = next;
                break;
            }
        }
        return t2;
    }
}
