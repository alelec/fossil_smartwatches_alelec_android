package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qk4 implements Factory<nj4> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public qk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static qk4 a(wj4 wj4) {
        return new qk4(wj4);
    }

    @DexIgnore
    public static nj4 b(wj4 wj4) {
        nj4 l = wj4.l();
        c87.a(l, "Cannot return null from a non-@Nullable @Provides method");
        return l;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public nj4 get() {
        return b(this.a);
    }
}
