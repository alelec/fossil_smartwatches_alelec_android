package com.fossil;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nz6 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public static String o; // = "CalendarAdapter";
    @DexIgnore
    public Calendar a;
    @DexIgnore
    public Calendar b;
    @DexIgnore
    public Calendar c;
    @DexIgnore
    public Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ Map<Long, Float> e; // = new TreeMap();
    @DexIgnore
    public /* final */ int[] f; // = new int[49];
    @DexIgnore
    public int g;
    @DexIgnore
    public /* final */ Context h;
    @DexIgnore
    public RecyclerViewCalendar.b i;
    @DexIgnore
    public int j; // = 0;
    @DexIgnore
    public int k; // = 0;
    @DexIgnore
    public int l; // = 0;
    @DexIgnore
    public int m; // = 0;
    @DexIgnore
    public int n; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        public b(View view) {
            super(view);
            this.a = (FlexibleTextView) view;
        }

        @DexIgnore
        public void a(int i) {
            String str;
            switch ((i / 7) % 7) {
                case 0:
                    str = ig5.a(nz6.this.h, 2131886720);
                    break;
                case 1:
                    str = ig5.a(nz6.this.h, 2131886717);
                    break;
                case 2:
                    str = ig5.a(nz6.this.h, 2131886722);
                    break;
                case 3:
                    str = ig5.a(nz6.this.h, 2131886723);
                    break;
                case 4:
                    str = ig5.a(nz6.this.h, 2131886721);
                    break;
                case 5:
                    str = ig5.a(nz6.this.h, 2131886718);
                    break;
                case 6:
                    str = ig5.a(nz6.this.h, 2131886719);
                    break;
                default:
                    str = "";
                    break;
            }
            this.a.setText(str);
        }
    }

    @DexIgnore
    public nz6(Context context) {
        this.h = context;
        int[][] iArr = (int[][]) Array.newInstance(int.class, 7, 7);
        for (int i2 = 0; i2 < 7; i2++) {
            for (int i3 = 0; i3 < 7; i3++) {
                iArr[i2][i3] = (i2 * 7) + i3;
            }
        }
        int[][] iArr2 = (int[][]) Array.newInstance(int.class, 7, 7);
        for (int i4 = 0; i4 < 7; i4++) {
            for (int i5 = 0; i5 < 7; i5++) {
                iArr2[i4][i5] = iArr[i5][6 - i4];
            }
        }
        for (int i6 = 0; i6 < 7; i6++) {
            System.arraycopy(iArr2[i6], 0, this.f, (i6 * 7) + 1, 6);
        }
        this.d = zd5.a(0, this.d);
    }

    @DexIgnore
    public void a(RecyclerViewCalendar.b bVar) {
        this.i = bVar;
    }

    @DexIgnore
    public void b(Calendar calendar) {
        this.c = calendar;
        if (this.a == null) {
            this.a = calendar;
        }
        if (this.b == null) {
            this.b = this.c;
        }
    }

    @DexIgnore
    public void c(Calendar calendar) {
        this.a = calendar;
    }

    @DexIgnore
    public Calendar d() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        Calendar calendar;
        if (this.a == null || (calendar = this.b) == null) {
            return 0;
        }
        return (((((calendar.get(1) - this.a.get(1)) * 12) + this.b.get(2)) - this.a.get(2)) + 1) * 49;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i2) {
        return i2 % 7 == 0 ? 0 : 1;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == 0) {
            ((b) viewHolder).a(i2);
        } else if (itemViewType == 1) {
            ((a) viewHolder).a(i2);
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        if (i2 != 0) {
            View inflate = LayoutInflater.from(this.h).inflate(2131558660, viewGroup, false);
            inflate.getLayoutParams().width = this.g;
            return new a(inflate);
        }
        View inflate2 = LayoutInflater.from(this.h).inflate(2131558661, viewGroup, false);
        inflate2.getLayoutParams().width = this.g;
        return new b(inflate2);
    }

    @DexIgnore
    public Calendar a(int i2) {
        Calendar calendar = (Calendar) this.d.clone();
        int i3 = this.f[i2 % 49];
        int i4 = calendar.get(7) - 1;
        if (i3 < i4 || i3 >= calendar.getActualMaximum(5) + i4) {
            return null;
        }
        calendar.add(5, i3 - i4);
        FLogger.INSTANCE.getLocal().d(o, "getCalendarItem day=" + calendar.get(5) + " month=" + calendar.get(2));
        return calendar;
    }

    @DexIgnore
    public Calendar c() {
        return this.b;
    }

    @DexIgnore
    public void b(int i2) {
        this.g = i2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder implements View.OnClickListener {
        @DexIgnore
        public /* final */ FlexibleTextView a;
        @DexIgnore
        public /* final */ ProgressBar b;

        @DexIgnore
        public a(View view) {
            super(view);
            this.a = (FlexibleTextView) view.findViewById(2131362317);
            ProgressBar progressBar = (ProgressBar) view.findViewById(2131362883);
            this.b = progressBar;
            progressBar.setMax(100);
            a(this.b, nz6.this.n);
            if (Build.VERSION.SDK_INT > 21) {
                this.b.setBackgroundTintList(ColorStateList.valueOf(nz6.this.n));
            } else {
                Drawable c2 = v6.c(view.getContext(), 2131230860);
                c2.setColorFilter(nz6.this.n, PorterDuff.Mode.SRC_IN);
                this.b.setBackground(c2);
            }
            view.setOnClickListener(this);
        }

        @DexIgnore
        public final void a(ProgressBar progressBar, int i) {
            Drawable c2 = v6.c(((RecyclerView.ViewHolder) this).itemView.getContext(), 2131230859);
            try {
                if (c2 instanceof LayerDrawable) {
                    LayerDrawable layerDrawable = (LayerDrawable) c2;
                    if (layerDrawable.getNumberOfLayers() > 1) {
                        ((GradientDrawable) layerDrawable.getDrawable(1)).setColor(i);
                        progressBar.setProgressDrawable(c2);
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = nz6.o;
                local.d(str, "DayViewHolder - e=" + e);
            }
        }

        @DexIgnore
        public void onClick(View view) {
            Calendar a2;
            int adapterPosition = getAdapterPosition();
            nz6 nz6 = nz6.this;
            if (nz6.i != null && adapterPosition != -1 && (a2 = nz6.a(adapterPosition)) != null && !a2.before(nz6.this.a) && !a2.after(nz6.this.b)) {
                nz6.this.i.a(adapterPosition, a2);
            }
        }

        @DexIgnore
        public void a(int i) {
            if (i != -1) {
                Calendar calendar = (Calendar) nz6.this.d.clone();
                int i2 = calendar.get(7) - 1;
                int i3 = nz6.this.f[i % 49];
                if (i3 < i2 || i3 >= calendar.getActualMaximum(5) + i2) {
                    this.a.setVisibility(4);
                    this.b.setVisibility(4);
                    return;
                }
                calendar.add(5, i3 - i2);
                boolean d = zd5.d(nz6.this.c.getTime(), calendar.getTime());
                this.a.setVisibility(0);
                this.b.setVisibility(0);
                this.a.setText(String.valueOf(calendar.get(5)));
                Map<Long, Float> map = nz6.this.e;
                zd5.d(calendar);
                Float f = map.get(Long.valueOf(calendar.getTimeInMillis()));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = nz6.o;
                local.d(str, "build - position=" + i + ", progress=" + f + ", date=" + calendar.getTime().toString());
                if (f == null) {
                    FlexibleTextView flexibleTextView = this.a;
                    nz6 nz6 = nz6.this;
                    flexibleTextView.setTextColor(d ? nz6.m : nz6.l);
                    this.b.setVisibility(4);
                } else if (f.floatValue() < 1.0f) {
                    if (f.floatValue() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        FlexibleTextView flexibleTextView2 = this.a;
                        nz6 nz62 = nz6.this;
                        flexibleTextView2.setTextColor(d ? nz62.m : nz62.k);
                        this.b.setVisibility(0);
                        this.b.setMax(100);
                        ObjectAnimator ofInt = ObjectAnimator.ofInt(this.b, "progress", 0, (int) (f.floatValue() * 100.0f));
                        ofInt.setDuration(300L);
                        ofInt.setAutoCancel(true);
                        ofInt.start();
                    } else if (f.floatValue() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        FlexibleTextView flexibleTextView3 = this.a;
                        nz6 nz63 = nz6.this;
                        flexibleTextView3.setTextColor(d ? nz63.m : nz63.k);
                        this.b.setVisibility(0);
                        this.b.setMax(100);
                        ObjectAnimator ofInt2 = ObjectAnimator.ofInt(this.b, "progress", 0, 0);
                        ofInt2.setDuration(300L);
                        ofInt2.setAutoCancel(true);
                        ofInt2.start();
                    } else {
                        FlexibleTextView flexibleTextView4 = this.a;
                        nz6 nz64 = nz6.this;
                        flexibleTextView4.setTextColor(d ? nz64.m : nz64.l);
                        this.b.setVisibility(4);
                    }
                    this.b.setSelected(false);
                } else {
                    this.b.setSelected(true);
                    this.b.setProgressTintList(ColorStateList.valueOf(0));
                    this.a.setTextColor(nz6.this.j);
                }
                FlexibleTextView flexibleTextView5 = this.a;
                flexibleTextView5.setTypeface(flexibleTextView5.getTypeface(), d ? 1 : 0);
            }
        }
    }

    @DexIgnore
    public void a(Calendar calendar) {
        this.b = calendar;
    }

    @DexIgnore
    public void a(Map<Long, Float> map, Calendar calendar) {
        FLogger.INSTANCE.getLocal().d(o, "setData");
        this.e.putAll(map);
        this.d = calendar;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5, int i6) {
        this.j = i2;
        this.k = i3;
        this.l = i4;
        this.m = i5;
        this.n = i6;
    }
}
