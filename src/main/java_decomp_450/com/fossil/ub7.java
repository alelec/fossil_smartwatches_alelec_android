package com.fossil;

import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ub7 {
    @DexIgnore
    public static final tb7 a(ob7 ob7) {
        return (tb7) ob7.getClass().getAnnotation(tb7.class);
    }

    @DexIgnore
    public static final int b(ob7 ob7) {
        try {
            Field declaredField = ob7.getClass().getDeclaredField("label");
            ee7.a((Object) declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(ob7);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    public static final StackTraceElement c(ob7 ob7) {
        int i;
        String str;
        ee7.b(ob7, "$this$getStackTraceElementImpl");
        tb7 a = a(ob7);
        if (a == null) {
            return null;
        }
        a(1, a.v());
        int b = b(ob7);
        if (b < 0) {
            i = -1;
        } else {
            i = a.l()[b];
        }
        String b2 = wb7.c.b(ob7);
        if (b2 == null) {
            str = a.c();
        } else {
            str = b2 + '/' + a.c();
        }
        return new StackTraceElement(str, a.m(), a.f(), i);
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        if (i2 > i) {
            throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
        }
    }
}
