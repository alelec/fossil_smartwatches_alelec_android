package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class xl0 extends Enum<xl0> {
    @DexIgnore
    public static /* final */ xl0 b;
    @DexIgnore
    public static /* final */ /* synthetic */ xl0[] c;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        xl0 xl0 = new xl0("FULL", 0, (byte) 0);
        b = xl0;
        c = new xl0[]{xl0, new xl0("HALF", 1, (byte) 1), new xl0("QUARTER", 2, (byte) 2), new xl0("EIGHTH", 3, (byte) 3), new xl0("SIXTEENTH", 4, (byte) 4)};
    }
    */

    @DexIgnore
    public xl0(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static xl0 valueOf(String str) {
        return (xl0) Enum.valueOf(xl0.class, str);
    }

    @DexIgnore
    public static xl0[] values() {
        return (xl0[]) c.clone();
    }
}
