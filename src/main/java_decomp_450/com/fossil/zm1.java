package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zm1 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C;
    @DexIgnore
    public short D;
    @DexIgnore
    public long E;
    @DexIgnore
    public int F;
    @DexIgnore
    public /* final */ ArrayList<bi1> G;
    @DexIgnore
    public /* final */ x91 H;
    @DexIgnore
    public /* final */ short I;
    @DexIgnore
    public float J;
    @DexIgnore
    public /* final */ c80 K;
    @DexIgnore
    public /* final */ boolean L;
    @DexIgnore
    public /* final */ boolean M;
    @DexIgnore
    public /* final */ float N;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ zm1(ri1 ri1, en0 en0, c80 c80, boolean z, boolean z2, float f, String str, int i) {
        this(ri1, en0, c80, z, z2, (i & 32) != 0 ? 0.001f : f, (i & 64) != 0 ? yh0.a("UUID.randomUUID().toString()") : str);
    }

    @DexIgnore
    public static final /* synthetic */ void d(zm1 zm1) {
        int i = zm1.F + 1;
        zm1.F = i;
        if (i < zm1.D) {
            zm1.n();
        } else if (zm1.M) {
            zm1.a(eu0.a(((zk0) zm1).v, null, is0.SUCCESS, null, 5));
        } else {
            zm1.m();
        }
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public void h() {
        zk0.a(this, new mh1(((zk0) this).w, ((zk0) this).x, this.H, ((zk0) this).z), new dj1(this), new bl1(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        JSONObject put = super.i().put(o80.BIOMETRIC_PROFILE.b(), this.K.d());
        ee7.a((Object) put, "super.optionDescription(\u2026ofile.valueDescription())");
        return yz0.a(yz0.a(yz0.a(put, r51.h3, Boolean.valueOf(this.L)), r51.i3, Boolean.valueOf(this.M)), r51.y0, yz0.a(this.I));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.N2;
        Object[] array = this.G.toArray(new bi1[0]);
        if (array != null) {
            return yz0.a(k, r51, yz0.a((k60[]) array));
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void m() {
        zk0.a(this, new q11(this.I, ((zk0) this).w, 0, 4), new j61(this), new h81(this), (kd7) null, (gd7) null, (gd7) null, 56, (Object) null);
    }

    @DexIgnore
    public final void n() {
        short s = (short) (this.I + this.F);
        zk0.a(this, new wa1(s, this.E, ((zk0) this).w, 0, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 24), new ca1(this, s), new wb1(this), new qd1(this), (gd7) null, (gd7) null, 48, (Object) null);
    }

    @DexIgnore
    public zm1(ri1 ri1, en0 en0, c80 c80, boolean z, boolean z2, float f, String str) {
        super(ri1, en0, wm0.k0, str, false, 16);
        this.K = c80;
        this.L = z;
        this.M = z2;
        this.N = f;
        this.C = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.FILE_CONFIG, ul0.TRANSFER_DATA}));
        this.G = new ArrayList<>();
        this.H = b21.x.m();
        this.I = 256;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        return this.G;
    }
}
