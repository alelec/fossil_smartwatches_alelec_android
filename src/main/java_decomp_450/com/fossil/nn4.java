package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface nn4 {
    @DexIgnore
    int a(yn4 yn4);

    @DexIgnore
    int a(String str);

    @DexIgnore
    long a(in4 in4);

    @DexIgnore
    long a(mn4 mn4);

    @DexIgnore
    LiveData<mn4> a(String[] strArr, Date date);

    @DexIgnore
    List<jn4> a();

    @DexIgnore
    void a(String[] strArr);

    @DexIgnore
    Long[] a(List<mn4> list);

    @DexIgnore
    hn4 b(String str);

    @DexIgnore
    mn4 b(String[] strArr, Date date);

    @DexIgnore
    void b();

    @DexIgnore
    Long[] b(List<yn4> list);

    @DexIgnore
    LiveData<List<yn4>> c();

    @DexIgnore
    yn4 c(String str);

    @DexIgnore
    Long[] c(List<jn4> list);

    @DexIgnore
    LiveData<yn4> d(String str);

    @DexIgnore
    List<in4> d();

    @DexIgnore
    Long[] d(List<hn4> list);

    @DexIgnore
    mn4 e(String str);

    @DexIgnore
    void e();

    @DexIgnore
    int f(String str);

    @DexIgnore
    void f();

    @DexIgnore
    LiveData<mn4> g(String str);

    @DexIgnore
    void g();

    @DexIgnore
    void h();

    @DexIgnore
    void i();
}
