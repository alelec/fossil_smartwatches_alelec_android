package com.fossil;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.ip5;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.customview.RecyclerViewEmptySupport;
import com.portfolio.platform.view.FlexibleTextView;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t46 extends go5 implements s46 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a p; // = new a(null);
    @DexIgnore
    public qw6<y65> f;
    @DexIgnore
    public ip5 g;
    @DexIgnore
    public r46 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return t46.j;
        }

        @DexIgnore
        public final t46 b() {
            return new t46();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t46 a;

        @DexIgnore
        public b(t46 t46) {
            this.a = t46;
        }

        @DexIgnore
        public final void onClick(View view) {
            y65 a2 = this.a.g1().a();
            if (a2 != null) {
                a2.t.setText("");
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ t46 a;

        @DexIgnore
        public c(t46 t46) {
            this.a = t46;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView;
            ImageView imageView2;
            if (TextUtils.isEmpty(charSequence)) {
                y65 a2 = this.a.g1().a();
                if (!(a2 == null || (imageView2 = a2.r) == null)) {
                    imageView2.setVisibility(8);
                }
                this.a.b("");
                this.a.h1().h();
                return;
            }
            y65 a3 = this.a.g1().a();
            if (!(a3 == null || (imageView = a3.r) == null)) {
                imageView.setVisibility(0);
            }
            this.a.b(String.valueOf(charSequence));
            this.a.h1().a(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t46 a;

        @DexIgnore
        public d(t46 t46) {
            this.a = t46;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements ip5.d {
        @DexIgnore
        public /* final */ /* synthetic */ t46 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public e(t46 t46) {
            this.a = t46;
        }

        @DexIgnore
        @Override // com.fossil.ip5.d
        public void a(String str) {
            ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
            y65 a2 = this.a.g1().a();
            if (a2 != null) {
                FlexibleTextView flexibleTextView = a2.w;
                ee7.a((Object) flexibleTextView, "it.tvNotFound");
                flexibleTextView.setVisibility(0);
                FlexibleTextView flexibleTextView2 = a2.w;
                ee7.a((Object) flexibleTextView2, "it.tvNotFound");
                we7 we7 = we7.a;
                String a3 = ig5.a(PortfolioApp.g0.c(), 2131886760);
                ee7.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                String format = String.format(a3, Arrays.copyOf(new Object[]{str}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView2.setText(format);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements ip5.e {
        @DexIgnore
        public /* final */ /* synthetic */ t46 a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public f(t46 t46) {
            this.a = t46;
        }

        @DexIgnore
        @Override // com.fossil.ip5.e
        public void a(WatchApp watchApp) {
            ee7.b(watchApp, "item");
            this.a.h1().a(watchApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Transition.TransitionListener {
        @DexIgnore
        public /* final */ /* synthetic */ y65 a;
        @DexIgnore
        public /* final */ /* synthetic */ long b;

        @DexIgnore
        public g(y65 y65, long j) {
            this.a = y65;
            this.b = j;
        }

        @DexIgnore
        public void onTransitionCancel(Transition transition) {
        }

        @DexIgnore
        public void onTransitionEnd(Transition transition) {
        }

        @DexIgnore
        public void onTransitionPause(Transition transition) {
        }

        @DexIgnore
        public void onTransitionResume(Transition transition) {
        }

        @DexIgnore
        public void onTransitionStart(Transition transition) {
            FlexibleTextView flexibleTextView = this.a.q;
            ee7.a((Object) flexibleTextView, "binding.btnCancel");
            if (flexibleTextView.getAlpha() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                this.a.q.animate().setDuration(this.b).alpha(1.0f);
            } else {
                this.a.q.animate().setDuration(this.b).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }
    }

    /*
    static {
        String simpleName = t46.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchAppSearchFragment::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    @Override // com.fossil.go5
    public void Z0() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void b(String str) {
        ee7.b(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        ip5 ip5 = this.g;
        if (ip5 != null) {
            ip5.a(str);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void c(List<r87<WatchApp, String>> list) {
        ee7.b(list, "recentSearchResult");
        ip5 ip5 = this.g;
        if (ip5 != null) {
            ip5.a(list);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return j;
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            return true;
        }
        activity.supportFinishAfterTransition();
        return true;
    }

    @DexIgnore
    public void f1() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            le5 le5 = le5.a;
            qw6<y65> qw6 = this.f;
            FlexibleTextView flexibleTextView = null;
            if (qw6 != null) {
                y65 a2 = qw6.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    ee7.a((Object) activity, "it");
                    le5.a(flexibleTextView, activity);
                    activity.setResult(0);
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.View");
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final qw6<y65> g1() {
        qw6<y65> qw6 = this.f;
        if (qw6 != null) {
            return qw6;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final r46 h1() {
        r46 r46 = this.h;
        if (r46 != null) {
            return r46;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void n() {
        ip5 ip5 = this.g;
        if (ip5 != null) {
            ip5.b(null);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        qw6<y65> qw6 = new qw6<>(this, (y65) qb.a(layoutInflater, 2131558635, viewGroup, false, a1()));
        this.f = qw6;
        if (qw6 != null) {
            y65 a2 = qw6.a();
            if (a2 != null) {
                ee7.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            ee7.a();
            throw null;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        r46 r46 = this.h;
        if (r46 != null) {
            r46.f();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        r46 r46 = this.h;
        if (r46 != null) {
            r46.g();
        } else {
            ee7.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        V("set_watch_apps_view");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            ee7.a((Object) activity, "it");
            a(activity, 550);
        }
        this.g = new ip5();
        qw6<y65> qw6 = this.f;
        if (qw6 != null) {
            y65 a2 = qw6.a();
            if (a2 != null) {
                y65 y65 = a2;
                RecyclerViewEmptySupport recyclerViewEmptySupport = y65.v;
                ee7.a((Object) recyclerViewEmptySupport, "this.rvResults");
                ip5 ip5 = this.g;
                if (ip5 != null) {
                    recyclerViewEmptySupport.setAdapter(ip5);
                    RecyclerViewEmptySupport recyclerViewEmptySupport2 = y65.v;
                    ee7.a((Object) recyclerViewEmptySupport2, "this.rvResults");
                    recyclerViewEmptySupport2.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerViewEmptySupport recyclerViewEmptySupport3 = y65.v;
                    FlexibleTextView flexibleTextView = y65.w;
                    ee7.a((Object) flexibleTextView, "this.tvNotFound");
                    recyclerViewEmptySupport3.setEmptyView(flexibleTextView);
                    ImageView imageView = y65.r;
                    ee7.a((Object) imageView, "this.btnSearchClear");
                    imageView.setVisibility(8);
                    y65.r.setOnClickListener(new b(this));
                    y65.t.addTextChangedListener(new c(this));
                    y65.q.setOnClickListener(new d(this));
                    ip5 ip52 = this.g;
                    if (ip52 != null) {
                        ip52.a(new e(this));
                        ip5 ip53 = this.g;
                        if (ip53 != null) {
                            ip53.a(new f(this));
                        } else {
                            ee7.d("mAdapter");
                            throw null;
                        }
                    } else {
                        ee7.d("mAdapter");
                        throw null;
                    }
                } else {
                    ee7.d("mAdapter");
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void a(FragmentActivity fragmentActivity, long j2) {
        TransitionSet a2 = zk5.a.a(j2);
        Window window = fragmentActivity.getWindow();
        ee7.a((Object) window, "context.window");
        window.setEnterTransition(a2);
        qw6<y65> qw6 = this.f;
        if (qw6 != null) {
            y65 a3 = qw6.a();
            if (a3 != null) {
                ee7.a((Object) a3, "binding");
                a(a2, j2, a3);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void b(List<r87<WatchApp, String>> list) {
        ee7.b(list, "results");
        ip5 ip5 = this.g;
        if (ip5 != null) {
            ip5.b(list);
        } else {
            ee7.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public final TransitionSet a(TransitionSet transitionSet, long j2, y65 y65) {
        FlexibleTextView flexibleTextView = y65.q;
        ee7.a((Object) flexibleTextView, "binding.btnCancel");
        flexibleTextView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        return transitionSet.addListener((Transition.TransitionListener) new g(y65, j2));
    }

    @DexIgnore
    public void a(r46 r46) {
        ee7.b(r46, "presenter");
        this.h = r46;
    }

    @DexIgnore
    @Override // com.fossil.s46
    public void a(WatchApp watchApp) {
        ee7.b(watchApp, "selectedWatchApp");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            le5 le5 = le5.a;
            qw6<y65> qw6 = this.f;
            FlexibleTextView flexibleTextView = null;
            if (qw6 != null) {
                y65 a2 = qw6.a();
                if (a2 != null) {
                    flexibleTextView = a2.q;
                }
                if (flexibleTextView != null) {
                    ee7.a((Object) activity, "it");
                    le5.a(flexibleTextView, activity);
                    activity.setResult(-1, new Intent().putExtra("SEARCH_WATCH_APP_RESULT_ID", watchApp.getWatchappId()));
                    activity.supportFinishAfterTransition();
                    return;
                }
                throw new x87("null cannot be cast to non-null type android.view.View");
            }
            ee7.d("mBinding");
            throw null;
        }
    }
}
