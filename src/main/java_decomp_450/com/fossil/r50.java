package com.fossil;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class r50<T, Y> {
    @DexIgnore
    public /* final */ Map<T, Y> a; // = new LinkedHashMap(100, 0.75f, true);
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;

    @DexIgnore
    public r50(long j) {
        this.b = j;
    }

    @DexIgnore
    public synchronized Y a(T t) {
        return this.a.get(t);
    }

    @DexIgnore
    public void a(T t, Y y) {
    }

    @DexIgnore
    public int b(Y y) {
        return 1;
    }

    @DexIgnore
    public synchronized Y b(T t, Y y) {
        long b2 = (long) b(y);
        if (b2 >= this.b) {
            a(t, y);
            return null;
        }
        if (y != null) {
            this.c += b2;
        }
        Y put = this.a.put(t, y);
        if (put != null) {
            this.c -= (long) b(put);
            if (!put.equals(y)) {
                a(t, put);
            }
        }
        b();
        return put;
    }

    @DexIgnore
    public synchronized long c() {
        return this.b;
    }

    @DexIgnore
    public void a() {
        a(0);
    }

    @DexIgnore
    public synchronized Y c(T t) {
        Y remove;
        remove = this.a.remove(t);
        if (remove != null) {
            this.c -= (long) b(remove);
        }
        return remove;
    }

    @DexIgnore
    public synchronized void a(long j) {
        while (this.c > j) {
            Iterator<Map.Entry<T, Y>> it = this.a.entrySet().iterator();
            Map.Entry<T, Y> next = it.next();
            Y value = next.getValue();
            this.c -= (long) b(value);
            T key = next.getKey();
            it.remove();
            a(key, value);
        }
    }

    @DexIgnore
    public final void b() {
        a(this.b);
    }
}
