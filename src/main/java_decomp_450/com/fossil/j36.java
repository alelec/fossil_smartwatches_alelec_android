package com.fossil;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.MutableLiveData;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.BitmapUtils;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.preset.Data;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.MetaData;
import com.portfolio.platform.data.model.diana.preset.RingStyle;
import com.portfolio.platform.data.model.diana.preset.RingStyleItem;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.sina.weibo.sdk.utils.ResourceManager;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j36 extends he {
    @DexIgnore
    public MutableLiveData<b> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<d> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<c> c; // = new MutableLiveData<>();
    @DexIgnore
    public FilterType d; // = FilterType.ORDERED_DITHERING;
    @DexIgnore
    public ArrayList<fz5> e;
    @DexIgnore
    public Bitmap f;
    @DexIgnore
    public /* final */ WatchFaceRepository g;
    @DexIgnore
    public /* final */ RingStyleRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ BitmapDrawable a;

        @DexIgnore
        public b(BitmapDrawable bitmapDrawable) {
            ee7.b(bitmapDrawable, ResourceManager.DRAWABLE);
            this.a = bitmapDrawable;
        }

        @DexIgnore
        public final BitmapDrawable a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ Drawable a;
        @DexIgnore
        public /* final */ String b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public c(Drawable drawable, String str, boolean z) {
            this.a = drawable;
            this.b = str;
            this.c = z;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final Drawable b() {
            return this.a;
        }

        @DexIgnore
        public final boolean c() {
            return this.c;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(Drawable drawable, String str, boolean z, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : drawable, (i & 2) != 0 ? null : str, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public d(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(String str, boolean z, int i, zd7 zd7) {
            this((i & 1) != 0 ? null : str, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1", f = "PreviewViewModel.kt", l = {62}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadBackgroundPhoto$1$drawable$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super BitmapDrawable>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $previewPhotoDir;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, String str, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
                this.$previewPhotoDir = str;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$previewPhotoDir, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super BitmapDrawable> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    BitmapUtils bitmapUtils = BitmapUtils.INSTANCE;
                    String str = this.$previewPhotoDir;
                    int i = this.this$0.$size;
                    Bitmap decodeBitmapFromDirectory = bitmapUtils.decodeBitmapFromDirectory(str, i, i);
                    if (decodeBitmapFromDirectory == null) {
                        return null;
                    }
                    this.this$0.this$0.f = decodeBitmapFromDirectory;
                    EInkImageFilter create = EInkImageFilter.create();
                    ee7.a((Object) create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(decodeBitmapFromDirectory, this.this$0.this$0.b(), false, false, new OutputSettings(480, 480, Format.RAW, false, false));
                    ee7.a((Object) apply, "algorithm.apply(bitmap, \u2026e, false, outputSettings)");
                    Bitmap preview = apply.getPreview();
                    int i2 = this.this$0.$size;
                    return new BitmapDrawable(PortfolioApp.g0.c().getResources(), Bitmap.createScaledBitmap(preview, i2, i2, false));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(j36 j36, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j36;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, this.$size, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                String str2 = FileUtils.getDirectory(applicationContext, FileType.WATCH_FACE) + File.separator + "previewPhoto";
                ti7 b = qj7.b();
                a aVar = new a(this, str2, null);
                this.L$0 = yi7;
                this.L$1 = applicationContext;
                this.L$2 = str2;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                str = str2;
            } else if (i == 1) {
                str = (String) this.L$2;
                Context context = (Context) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            BitmapDrawable bitmapDrawable = (BitmapDrawable) obj;
            if (bitmapDrawable != null) {
                FLogger.INSTANCE.getLocal().d("PreviewViewModel", "load photo success");
                hg5.b().a(str + this.$size + this.$size, bitmapDrawable);
                this.this$0.c().b(new b(bitmapDrawable));
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1", f = "PreviewViewModel.kt", l = {121}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$loadComplicationSetting$1$1", f = "PreviewViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    DianaComplicationRingStyle ringStylesBySerial = this.this$0.this$0.h.getRingStylesBySerial(PortfolioApp.g0.c().c());
                    if (ringStylesBySerial != null) {
                        ee5 ee5 = ee5.a;
                        String previewUrl = ringStylesBySerial.getData().getPreviewUrl();
                        int i = this.this$0.$size;
                        Drawable b = ee5.b(previewUrl, i, i, FileType.WATCH_FACE);
                        String unselectedForegroundColor = ringStylesBySerial.getMetaData().getUnselectedForegroundColor();
                        if (unselectedForegroundColor == null || b == null) {
                            this.this$0.this$0.d().a(new c(null, null, false, 3, null));
                        } else {
                            FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadComplicationSetting() success");
                            this.this$0.this$0.d().a(new c(b, unselectedForegroundColor, true));
                        }
                    } else {
                        this.this$0.this$0.d().a(new c(null, null, false, 3, null));
                    }
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(j36 j36, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j36;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, this.$size, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.preview.PreviewViewModel$saveTheme$1", f = "PreviewViewModel.kt", l = {94}, m = "invokeSuspend")
    public static final class g extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $size;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ j36 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Bitmap $srcBitmap;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ g this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(Bitmap bitmap, fb7 fb7, g gVar) {
                super(2, fb7);
                this.$srcBitmap = bitmap;
                this.this$0 = gVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.$srcBitmap, fb7, this.this$0);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    String valueOf = String.valueOf(System.currentTimeMillis());
                    String str = valueOf + Constants.PHOTO_IMAGE_NAME_SUFFIX;
                    String str2 = valueOf + Constants.PHOTO_BINARY_NAME_SUFFIX;
                    String str3 = FileUtils.getDirectory(PortfolioApp.g0.c().getApplicationContext(), FileType.WATCH_FACE) + File.separator;
                    EInkImageFilter create = EInkImageFilter.create();
                    ee7.a((Object) create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(this.$srcBitmap, this.this$0.this$0.b(), false, false, OutputSettings.BACKGROUND);
                    ee7.a((Object) apply, "algorithm.apply(srcBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap bitmap = this.$srcBitmap;
                    FilterResult apply2 = create.apply(bitmap.copy(bitmap.getConfig(), true), this.this$0.this$0.b(), false, false, OutputSettings.BACKGROUND);
                    ee7.a((Object) apply2, "algorithm.apply(srcBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap a = jr5.a(apply2.getPreview());
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(PortfolioApp.g0.c().getResources(), a);
                    hg5.b().a(str3 + valueOf + this.this$0.$size + this.this$0.$size, bitmapDrawable);
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "save theme to repository");
                    WatchFaceRepository c = this.this$0.this$0.g;
                    Context applicationContext = PortfolioApp.g0.c().getApplicationContext();
                    ee7.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    ee7.a((Object) a, "bitmap");
                    byte[] data = apply.getData();
                    ee7.a((Object) data, "thumbnailResult.data");
                    c.saveBackgroundPhoto(applicationContext, str3, str, a, str2, data);
                    j36 j36 = this.this$0.this$0;
                    String str4 = File.separator;
                    ee7.a((Object) str4, "File.separator");
                    j36.a(nh7.c(str3, str4, (String) null, 2, (Object) null), str, str2);
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(j36 j36, int i, fb7 fb7) {
            super(2, fb7);
            this.this$0 = j36;
            this.$size = i;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            g gVar = new g(this.this$0, this.$size, fb7);
            gVar.p$ = (yi7) obj;
            return gVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((g) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                if (this.this$0.f == null) {
                    FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme() error mBitmap == null");
                    this.this$0.e().a(new d(null, false, 1, null));
                    return i97.a;
                }
                Bitmap a3 = this.this$0.f;
                if (a3 != null) {
                    if (a3.isRecycled()) {
                        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle fail bitmap is recycledObject?");
                        this.this$0.e().a(new d(null, false, 1, null));
                    } else {
                        ti7 b = qj7.b();
                        a aVar = new a(a3, null, this);
                        this.L$0 = yi7;
                        this.L$1 = a3;
                        this.label = 1;
                        if (vh7.a(b, aVar, this) == a2) {
                            return a2;
                        }
                    }
                }
            } else if (i == 1) {
                Bitmap bitmap = (Bitmap) this.L$1;
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public j36(WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository) {
        ee7.b(watchFaceRepository, "watchFaceRepository");
        ee7.b(ringStyleRepository, "ringStyleRepository");
        this.g = watchFaceRepository;
        this.h = ringStyleRepository;
    }

    @DexIgnore
    public final MutableLiveData<c> d() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<d> e() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.he
    public void onCleared() {
        zi7.a(ie.a(this), null, 1, null);
        Bitmap bitmap = this.f;
        if (bitmap != null) {
            bitmap.recycle();
        }
        super.onCleared();
    }

    @DexIgnore
    public final FilterType b() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<b> c() {
        return this.a;
    }

    @DexIgnore
    public final void b(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PreviewViewModel", "loadComplicationSetting() size=" + i);
        ik7 unused = xh7.b(ie.a(this), null, null, new f(this, i, null), 3, null);
    }

    @DexIgnore
    public final void c(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveTheme()");
        ik7 unused = xh7.b(ie.a(this), null, null, new g(this, i, null), 3, null);
    }

    @DexIgnore
    public final void a(FilterType filterType) {
        ee7.b(filterType, "<set-?>");
        this.d = filterType;
    }

    @DexIgnore
    public final ArrayList<fz5> a() {
        return this.e;
    }

    @DexIgnore
    public final void a(ArrayList<fz5> arrayList) {
        this.e = arrayList;
    }

    @DexIgnore
    public final void a(int i) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "loadBackgroundPhoto()");
        ik7 unused = xh7.b(ie.a(this), null, null, new e(this, i, null), 3, null);
    }

    @DexIgnore
    public final ArrayList<RingStyleItem> a(DianaComplicationRingStyle dianaComplicationRingStyle) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "createPhotoRingStyleList()");
        String url = dianaComplicationRingStyle.getData().getUrl();
        String previewUrl = dianaComplicationRingStyle.getData().getPreviewUrl();
        String selectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getSelectedBackgroundColor();
        String unselectedBackgroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedBackgroundColor();
        String selectedForegroundColor = dianaComplicationRingStyle.getMetaData().getSelectedForegroundColor();
        String unselectedForegroundColor = dianaComplicationRingStyle.getMetaData().getUnselectedForegroundColor();
        ArrayList<fz5> arrayList = this.e;
        ArrayList<RingStyleItem> arrayList2 = null;
        if (arrayList != null) {
            if (arrayList == null) {
                ee7.a();
                throw null;
            } else if (!arrayList.isEmpty()) {
                arrayList2 = new ArrayList<>();
                ArrayList<fz5> arrayList3 = this.e;
                if (arrayList3 != null) {
                    Iterator<T> it = arrayList3.iterator();
                    while (it.hasNext()) {
                        String c2 = it.next().c();
                        if (c2 != null) {
                            switch (c2.hashCode()) {
                                case -1383228885:
                                    if (c2.equals("bottom")) {
                                        arrayList2.add(new RingStyleItem("bottom", new RingStyle("bottomRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 115029:
                                    if (c2.equals(ViewHierarchy.DIMENSION_TOP_KEY)) {
                                        arrayList2.add(new RingStyleItem(ViewHierarchy.DIMENSION_TOP_KEY, new RingStyle("topRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 3317767:
                                    if (c2.equals(ViewHierarchy.DIMENSION_LEFT_KEY)) {
                                        arrayList2.add(new RingStyleItem(ViewHierarchy.DIMENSION_LEFT_KEY, new RingStyle("leftRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                        break;
                                    } else {
                                        continue;
                                    }
                                case 108511772:
                                    if (c2.equals("right")) {
                                        arrayList2.add(new RingStyleItem("right", new RingStyle("rightRing", new Data(previewUrl, url), new MetaData(selectedForegroundColor, selectedBackgroundColor, unselectedForegroundColor, unselectedBackgroundColor))));
                                        break;
                                    } else {
                                        continue;
                                    }
                            }
                        }
                    }
                }
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public final void a(String str, String str2, String str3) {
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle");
        DianaComplicationRingStyle ringStylesBySerial = this.h.getRingStylesBySerial(PortfolioApp.g0.c().c());
        if (ringStylesBySerial != null) {
            FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle success");
            this.g.insertWatchFace(str, str2, str3, a(ringStylesBySerial));
            this.b.a(new d(str2, true));
            return;
        }
        FLogger.INSTANCE.getLocal().d("PreviewViewModel", "saveRingStyle fail");
        this.b.a(new d(null, false, 1, null));
    }
}
