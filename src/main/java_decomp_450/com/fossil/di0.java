package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class di0 extends Enum<di0> {
    @DexIgnore
    public static /* final */ di0 b;
    @DexIgnore
    public static /* final */ di0 c;
    @DexIgnore
    public static /* final */ /* synthetic */ di0[] d;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        di0 di0 = new di0("HOUR", 0, (byte) 1);
        b = di0;
        di0 di02 = new di0("MINUTE", 1, (byte) 2);
        c = di02;
        d = new di0[]{di0, di02, new di0("SUB_EYE", 2, (byte) 4)};
    }
    */

    @DexIgnore
    public di0(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static di0 valueOf(String str) {
        return (di0) Enum.valueOf(di0.class, str);
    }

    @DexIgnore
    public static di0[] values() {
        return (di0[]) d.clone();
    }
}
