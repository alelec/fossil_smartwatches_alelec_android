package com.fossil;

import com.fossil.z84;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface z84<T extends z84<T>> {
    @DexIgnore
    <U> T a(Class<U> cls, u84<? super U> u84);
}
