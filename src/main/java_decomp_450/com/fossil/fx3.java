package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fx3 implements Serializable {
    @DexIgnore
    public int value;

    @DexIgnore
    public fx3(int i) {
        this.value = i;
    }

    @DexIgnore
    public void add(int i) {
        this.value += i;
    }

    @DexIgnore
    public int addAndGet(int i) {
        int i2 = this.value + i;
        this.value = i2;
        return i2;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof fx3) && ((fx3) obj).value == this.value;
    }

    @DexIgnore
    public int get() {
        return this.value;
    }

    @DexIgnore
    public int getAndSet(int i) {
        int i2 = this.value;
        this.value = i;
        return i2;
    }

    @DexIgnore
    public int hashCode() {
        return this.value;
    }

    @DexIgnore
    public void set(int i) {
        this.value = i;
    }

    @DexIgnore
    public String toString() {
        return Integer.toString(this.value);
    }
}
