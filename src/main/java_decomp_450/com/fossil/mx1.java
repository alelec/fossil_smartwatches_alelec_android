package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rx1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class mx1 implements rx1.b {
    @DexIgnore
    public /* final */ rx1 a;
    @DexIgnore
    public /* final */ pu1 b;
    @DexIgnore
    public /* final */ ku1 c;

    @DexIgnore
    public mx1(rx1 rx1, pu1 pu1, ku1 ku1) {
        this.a = rx1;
        this.b = pu1;
        this.c = ku1;
    }

    @DexIgnore
    public static rx1.b a(rx1 rx1, pu1 pu1, ku1 ku1) {
        return new mx1(rx1, pu1, ku1);
    }

    @DexIgnore
    @Override // com.fossil.rx1.b
    public Object apply(Object obj) {
        return rx1.a(this.a, this.b, this.c, (SQLiteDatabase) obj);
    }
}
