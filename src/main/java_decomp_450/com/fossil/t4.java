package com.fossil;

import com.j256.ormlite.stmt.query.SimpleComparison;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class t4<K, V> {
    @DexIgnore
    public t4<K, V>.b a;
    @DexIgnore
    public t4<K, V>.c b;
    @DexIgnore
    public t4<K, V>.e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a<T> implements Iterator<T> {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public a(int i) {
            this.a = i;
            this.b = t4.this.c();
        }

        @DexIgnore
        public boolean hasNext() {
            return this.c < this.b;
        }

        @DexIgnore
        @Override // java.util.Iterator
        public T next() {
            if (hasNext()) {
                T t = (T) t4.this.a(this.c, this.a);
                this.c++;
                this.d = true;
                return t;
            }
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            if (this.d) {
                int i = this.c - 1;
                this.c = i;
                this.b--;
                this.d = false;
                t4.this.a(i);
                return;
            }
            throw new IllegalStateException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements Set<Map.Entry<K, V>> {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean a(Map.Entry<K, V> entry) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public /* bridge */ /* synthetic */ boolean add(Object obj) {
            a((Map.Entry) obj);
            throw null;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: com.fossil.t4 */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // java.util.Collection, java.util.Set
        public boolean addAll(Collection<? extends Map.Entry<K, V>> collection) {
            int c = t4.this.c();
            Iterator<? extends Map.Entry<K, V>> it = collection.iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                t4.this.a(entry.getKey(), entry.getValue());
            }
            return c != t4.this.c();
        }

        @DexIgnore
        public void clear() {
            t4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            int a2 = t4.this.a(entry.getKey());
            if (a2 < 0) {
                return false;
            }
            return q4.a(t4.this.a(a2, 1), entry.getValue());
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            Iterator<?> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return t4.a((Set) this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2;
            int i3 = 0;
            for (int c = t4.this.c() - 1; c >= 0; c--) {
                Object a2 = t4.this.a(c, 0);
                Object a3 = t4.this.a(c, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 == null) {
                    i2 = 0;
                } else {
                    i2 = a3.hashCode();
                }
                i3 += i ^ i2;
            }
            return i3;
        }

        @DexIgnore
        public boolean isEmpty() {
            return t4.this.c() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<Map.Entry<K, V>> iterator() {
            return new d();
        }

        @DexIgnore
        public boolean remove(Object obj) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public int size() {
            return t4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public <T> T[] toArray(T[] tArr) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements Set<K> {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean add(K k) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean addAll(Collection<? extends K> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            t4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return t4.this.a(obj) >= 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean containsAll(Collection<?> collection) {
            return t4.a((Map) t4.this.b(), collection);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return t4.a((Set) this, obj);
        }

        @DexIgnore
        public int hashCode() {
            int i;
            int i2 = 0;
            for (int c = t4.this.c() - 1; c >= 0; c--) {
                Object a2 = t4.this.a(c, 0);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                i2 += i;
            }
            return i2;
        }

        @DexIgnore
        public boolean isEmpty() {
            return t4.this.c() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set, java.lang.Iterable
        public Iterator<K> iterator() {
            return new a(0);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int a2 = t4.this.a(obj);
            if (a2 < 0) {
                return false;
            }
            t4.this.a(a2);
            return true;
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean removeAll(Collection<?> collection) {
            return t4.b(t4.this.b(), collection);
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public boolean retainAll(Collection<?> collection) {
            return t4.c(t4.this.b(), collection);
        }

        @DexIgnore
        public int size() {
            return t4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return t4.this.b(0);
        }

        @DexIgnore
        @Override // java.util.Collection, java.util.Set
        public <T> T[] toArray(T[] tArr) {
            return (T[]) t4.this.a(tArr, 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d implements Iterator<Map.Entry<K, V>>, Map.Entry<K, V> {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c; // = false;

        @DexIgnore
        public d() {
            this.a = t4.this.c() - 1;
            this.b = -1;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!this.c) {
                throw new IllegalStateException("This container does not support retaining Map.Entry objects");
            } else if (!(obj instanceof Map.Entry)) {
                return false;
            } else {
                Map.Entry entry = (Map.Entry) obj;
                if (!q4.a(entry.getKey(), t4.this.a(this.b, 0)) || !q4.a(entry.getValue(), t4.this.a(this.b, 1))) {
                    return false;
                }
                return true;
            }
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public K getKey() {
            if (this.c) {
                return (K) t4.this.a(this.b, 0);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V getValue() {
            if (this.c) {
                return (V) t4.this.a(this.b, 1);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b < this.a;
        }

        @DexIgnore
        public int hashCode() {
            int i;
            if (this.c) {
                int i2 = 0;
                Object a2 = t4.this.a(this.b, 0);
                Object a3 = t4.this.a(this.b, 1);
                if (a2 == null) {
                    i = 0;
                } else {
                    i = a2.hashCode();
                }
                if (a3 != null) {
                    i2 = a3.hashCode();
                }
                return i ^ i2;
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public void remove() {
            if (this.c) {
                t4.this.a(this.b);
                this.b--;
                this.a--;
                this.c = false;
                return;
            }
            throw new IllegalStateException();
        }

        @DexIgnore
        @Override // java.util.Map.Entry
        public V setValue(V v) {
            if (this.c) {
                return (V) t4.this.a(this.b, v);
            }
            throw new IllegalStateException("This container does not support retaining Map.Entry objects");
        }

        @DexIgnore
        public String toString() {
            return getKey() + SimpleComparison.EQUAL_TO_OPERATION + getValue();
        }

        @DexIgnore
        @Override // java.util.Iterator
        public Map.Entry<K, V> next() {
            if (hasNext()) {
                this.b++;
                this.c = true;
                return this;
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements Collection<V> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean add(V v) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean addAll(Collection<? extends V> collection) {
            throw new UnsupportedOperationException();
        }

        @DexIgnore
        public void clear() {
            t4.this.a();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return t4.this.b(obj) >= 0;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean containsAll(Collection<?> collection) {
            Iterator<?> it = collection.iterator();
            while (it.hasNext()) {
                if (!contains(it.next())) {
                    return false;
                }
            }
            return true;
        }

        @DexIgnore
        public boolean isEmpty() {
            return t4.this.c() == 0;
        }

        @DexIgnore
        @Override // java.util.Collection, java.lang.Iterable
        public Iterator<V> iterator() {
            return new a(1);
        }

        @DexIgnore
        public boolean remove(Object obj) {
            int b = t4.this.b(obj);
            if (b < 0) {
                return false;
            }
            t4.this.a(b);
            return true;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean removeAll(Collection<?> collection) {
            int c = t4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (collection.contains(t4.this.a(i, 1))) {
                    t4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        @Override // java.util.Collection
        public boolean retainAll(Collection<?> collection) {
            int c = t4.this.c();
            int i = 0;
            boolean z = false;
            while (i < c) {
                if (!collection.contains(t4.this.a(i, 1))) {
                    t4.this.a(i);
                    i--;
                    c--;
                    z = true;
                }
                i++;
            }
            return z;
        }

        @DexIgnore
        public int size() {
            return t4.this.c();
        }

        @DexIgnore
        public Object[] toArray() {
            return t4.this.b(1);
        }

        @DexIgnore
        @Override // java.util.Collection
        public <T> T[] toArray(T[] tArr) {
            return (T[]) t4.this.a(tArr, 1);
        }
    }

    @DexIgnore
    public static <K, V> boolean a(Map<K, V> map, Collection<?> collection) {
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            if (!map.containsKey(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <K, V> boolean b(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<?> it = collection.iterator();
        while (it.hasNext()) {
            map.remove(it.next());
        }
        return size != map.size();
    }

    @DexIgnore
    public static <K, V> boolean c(Map<K, V> map, Collection<?> collection) {
        int size = map.size();
        Iterator<K> it = map.keySet().iterator();
        while (it.hasNext()) {
            if (!collection.contains(it.next())) {
                it.remove();
            }
        }
        return size != map.size();
    }

    @DexIgnore
    public abstract int a(Object obj);

    @DexIgnore
    public abstract Object a(int i, int i2);

    @DexIgnore
    public abstract V a(int i, V v);

    @DexIgnore
    public abstract void a();

    @DexIgnore
    public abstract void a(int i);

    @DexIgnore
    public abstract void a(K k, V v);

    @DexIgnore
    public abstract int b(Object obj);

    @DexIgnore
    public abstract Map<K, V> b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public Set<Map.Entry<K, V>> d() {
        if (this.a == null) {
            this.a = new b();
        }
        return this.a;
    }

    @DexIgnore
    public Set<K> e() {
        if (this.b == null) {
            this.b = new c();
        }
        return this.b;
    }

    @DexIgnore
    public Collection<V> f() {
        if (this.c == null) {
            this.c = new e();
        }
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r4v1, resolved type: T[] */
    /* JADX WARN: Multi-variable type inference failed */
    public <T> T[] a(T[] tArr, int i) {
        T[] tArr2;
        int c2 = c();
        if (tArr.length < c2) {
            tArr = (T[]) ((Object[]) Array.newInstance(tArr.getClass().getComponentType(), c2));
        }
        for (int i2 = 0; i2 < c2; i2++) {
            tArr2[i2] = a(i2, i);
        }
        if (tArr2.length > c2) {
            tArr2[c2] = 0;
        }
        return tArr2;
    }

    @DexIgnore
    public Object[] b(int i) {
        int c2 = c();
        Object[] objArr = new Object[c2];
        for (int i2 = 0; i2 < c2; i2++) {
            objArr[i2] = a(i2, i);
        }
        return objArr;
    }

    @DexIgnore
    public static <T> boolean a(Set<T> set, Object obj) {
        if (set == obj) {
            return true;
        }
        if (obj instanceof Set) {
            Set set2 = (Set) obj;
            try {
                if (set.size() != set2.size() || !set.containsAll(set2)) {
                    return false;
                }
                return true;
            } catch (ClassCastException | NullPointerException unused) {
            }
        }
        return false;
    }
}
