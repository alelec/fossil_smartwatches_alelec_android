package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.SignUpSocialAuth;
import java.util.Arrays;
import java.util.StringTokenizer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ih5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public CallbackManager a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ih5.b;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final String[] a(String str) {
            ee7.b(str, "fullName");
            String[] strArr = {"", ""};
            StringTokenizer stringTokenizer = new StringTokenizer(str);
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken = stringTokenizer.nextToken();
                ee7.a((Object) nextToken, "tokenizer.nextToken()");
                strArr[0] = nextToken;
            }
            if (stringTokenizer.hasMoreTokens()) {
                String nextToken2 = stringTokenizer.nextToken();
                ee7.a((Object) nextToken2, "tokenizer.nextToken()");
                strArr[1] = nextToken2;
            }
            return strArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements GraphRequest.GraphJSONObjectCallback {
        @DexIgnore
        public /* final */ /* synthetic */ mh5 a;
        @DexIgnore
        public /* final */ /* synthetic */ AccessToken b;

        @DexIgnore
        public b(mh5 mh5, AccessToken accessToken) {
            this.a = mh5;
            this.b = accessToken;
        }

        @DexIgnore
        @Override // com.facebook.GraphRequest.GraphJSONObjectCallback
        public final void onCompleted(JSONObject jSONObject, GraphResponse graphResponse) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ih5.c.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .fetchGraphDataFacebook response=");
            sb.append(graphResponse);
            sb.append(", error=");
            ee7.a((Object) graphResponse, "response");
            sb.append(graphResponse.getError());
            local.d(a2, sb.toString());
            if (graphResponse.getError() != null) {
                this.a.a(600, null, "");
            } else if (jSONObject != null) {
                SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
                String optString = jSONObject.optString(Constants.EMAIL);
                ee7.a((Object) optString, "me.optString(\"email\")");
                signUpSocialAuth.setEmail(optString);
                String optString2 = jSONObject.optString("name");
                String optString3 = jSONObject.optString("first_name");
                String optString4 = jSONObject.optString("last_name");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = ih5.c.a();
                local2.d(a3, "Facebook email is " + jSONObject.optString(Constants.EMAIL) + " facebook name " + jSONObject.optString("name"));
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = ih5.c.a();
                local3.d(a4, "Facebook first name = " + optString3 + " last name = " + optString4);
                if (!TextUtils.isEmpty(optString3) && !TextUtils.isEmpty(optString4)) {
                    ee7.a((Object) optString3, Constants.PROFILE_KEY_FIRST_NAME);
                    signUpSocialAuth.setFirstName(optString3);
                    ee7.a((Object) optString4, Constants.PROFILE_KEY_LAST_NAME);
                    signUpSocialAuth.setLastName(optString4);
                } else if (!TextUtils.isEmpty(optString2)) {
                    a aVar = ih5.c;
                    ee7.a((Object) optString2, "name");
                    String[] a5 = aVar.a(optString2);
                    signUpSocialAuth.setFirstName(a5[0]);
                    signUpSocialAuth.setLastName(a5[1]);
                }
                String token = this.b.getToken();
                ee7.a((Object) token, "token.token");
                signUpSocialAuth.setToken(token);
                signUpSocialAuth.setService(Constants.FACEBOOK);
                this.a.a(signUpSocialAuth);
            } else {
                this.a.a(600, null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements FacebookCallback<LoginResult> {
        @DexIgnore
        public /* final */ /* synthetic */ ih5 a;
        @DexIgnore
        public /* final */ /* synthetic */ mh5 b;

        @DexIgnore
        public c(ih5 ih5, mh5 mh5) {
            this.a = ih5;
            this.b = mh5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(LoginResult loginResult) {
            ee7.b(loginResult, "loginResult");
            AccessToken accessToken = loginResult.getAccessToken();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ih5.c.a();
            local.d(a2, "Step 1: Login using facebook success token=" + accessToken);
            ih5 ih5 = this.a;
            ee7.a((Object) accessToken, Constants.PROFILE_KEY_ACCESS_TOKEN);
            ih5.a(accessToken, this.b);
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onCancel() {
            FLogger.INSTANCE.getLocal().e(ih5.c.a(), "loginWithEmail facebook is cancel");
            this.b.a(2, null, "");
        }

        @DexIgnore
        @Override // com.facebook.FacebookCallback
        public void onError(FacebookException facebookException) {
            ee7.b(facebookException, "e");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ih5.c.a();
            local.e(a2, "loginWithEmail facebook fail " + facebookException.getMessage());
            this.b.a(600, null, "");
        }
    }

    /*
    static {
        String canonicalName = ih5.class.getCanonicalName();
        if (canonicalName != null) {
            ee7.a((Object) canonicalName, "MFLoginFacebookManager::class.java.canonicalName!!");
            b = canonicalName;
            return;
        }
        ee7.a();
        throw null;
    }
    */

    @DexIgnore
    public final void a(Activity activity, mh5 mh5) {
        ee7.b(activity, "activityContext");
        ee7.b(mh5, Constants.CALLBACK);
        this.a = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList(Constants.EMAIL, "user_photos", "public_profile"));
        LoginManager instance = LoginManager.getInstance();
        CallbackManager callbackManager = this.a;
        if (callbackManager != null) {
            instance.registerCallback(callbackManager, new c(this, mh5));
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(AccessToken accessToken, mh5 mh5) {
        ee7.b(accessToken, "token");
        ee7.b(mh5, Constants.CALLBACK);
        GraphRequest newMeRequest = GraphRequest.newMeRequest(accessToken, new b(mh5, accessToken));
        Bundle bundle = new Bundle();
        bundle.putString("fields", "id, name, email, first_name, last_name");
        ee7.a((Object) newMeRequest, "request");
        newMeRequest.setParameters(bundle);
        newMeRequest.executeAsync();
    }

    @DexIgnore
    public final boolean a(int i, int i2, Intent intent) {
        ee7.b(intent, "data");
        CallbackManager callbackManager = this.a;
        if (callbackManager == null) {
            return false;
        }
        if (callbackManager != null) {
            boolean onActivityResult = callbackManager.onActivityResult(i, i2, intent);
            this.a = null;
            return onActivityResult;
        }
        ee7.a();
        throw null;
    }
}
