package com.fossil;

import android.text.Html;
import android.text.Spanned;
import com.fossil.vx6;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sv6 extends pv6 {
    @DexIgnore
    public /* final */ qv6 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1", f = "WelcomePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sv6 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1$isAA$1", f = "WelcomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            public a(fb7 fb7) {
                super(2, fb7);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Boolean> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    return pb7.a(ng5.a().a(PortfolioApp.g0.c()) != null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sv6 sv6, fb7 fb7) {
            super(2, fb7);
            this.this$0 = sv6;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) obj).booleanValue()) {
                this.this$0.j().R0();
            } else {
                this.this$0.j().Z();
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        ee7.a((Object) sv6.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public sv6(qv6 qv6) {
        ee7.b(qv6, "mView");
        this.e = qv6;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        qv6 qv6 = this.e;
        Spanned fromHtml = Html.fromHtml(k());
        ee7.a((Object) fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        qv6.a(fromHtml);
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
    }

    @DexIgnore
    @Override // com.fossil.pv6
    public void h() {
        ik7 unused = xh7.b(e(), null, null, new b(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.pv6
    public void i() {
        this.e.K0();
    }

    @DexIgnore
    public final qv6 j() {
        return this.e;
    }

    @DexIgnore
    public final String k() {
        String a2 = vx6.a(vx6.c.TERMS, null);
        String a3 = vx6.a(vx6.c.PRIVACY, null);
        String str = ig5.a(PortfolioApp.g0.c(), 2131886923).toString();
        ee7.a((Object) a2, "termOfUseUrl");
        String a4 = mh7.a(str, "term_of_use_url", a2, false, 4, (Object) null);
        ee7.a((Object) a3, "privacyPolicyUrl");
        return mh7.a(a4, "privacy_policy", a3, false, 4, (Object) null);
    }

    @DexIgnore
    public void l() {
        this.e.a(this);
    }
}
