package com.fossil;

import android.content.SharedPreferences;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class er2 implements SharedPreferences.OnSharedPreferenceChangeListener {
    @DexIgnore
    public /* final */ fr2 a;

    @DexIgnore
    public er2(fr2 fr2) {
        this.a = fr2;
    }

    @DexIgnore
    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.a.a(sharedPreferences, str);
    }
}
