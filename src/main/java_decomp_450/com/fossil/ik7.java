package com.fossil;

import com.fossil.ib7;
import java.util.concurrent.CancellationException;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ik7 extends ib7.b {
    @DexIgnore
    public static final b o = b.a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ib7.c<ik7> {
        @DexIgnore
        public static /* final */ /* synthetic */ b a; // = new b();

        /*
        static {
            CoroutineExceptionHandler.a aVar = CoroutineExceptionHandler.n;
        }
        */
    }

    @DexIgnore
    gi7 a(ii7 ii7);

    @DexIgnore
    rj7 a(boolean z, boolean z2, gd7<? super Throwable, i97> gd7);

    @DexIgnore
    void a(CancellationException cancellationException);

    @DexIgnore
    rj7 b(gd7<? super Throwable, i97> gd7);

    @DexIgnore
    CancellationException b();

    @DexIgnore
    boolean isActive();

    @DexIgnore
    boolean start();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <E extends ib7.b> E a(ik7 ik7, ib7.c<E> cVar) {
            return (E) ib7.b.a.a(ik7, cVar);
        }

        @DexIgnore
        public static ib7 a(ik7 ik7, ib7 ib7) {
            return ib7.b.a.a(ik7, ib7);
        }

        @DexIgnore
        public static <R> R a(ik7 ik7, R r, kd7<? super R, ? super ib7.b, ? extends R> kd7) {
            return (R) ib7.b.a.a(ik7, r, kd7);
        }

        @DexIgnore
        public static /* synthetic */ void a(ik7 ik7, CancellationException cancellationException, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    cancellationException = null;
                }
                ik7.a(cancellationException);
                return;
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: cancel");
        }

        @DexIgnore
        public static ib7 b(ik7 ik7, ib7.c<?> cVar) {
            return ib7.b.a.b(ik7, cVar);
        }

        @DexIgnore
        public static /* synthetic */ rj7 a(ik7 ik7, boolean z, boolean z2, gd7 gd7, int i, Object obj) {
            if (obj == null) {
                if ((i & 1) != 0) {
                    z = false;
                }
                if ((i & 2) != 0) {
                    z2 = true;
                }
                return ik7.a(z, z2, gd7);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: invokeOnCompletion");
        }
    }
}
