package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;
import com.google.android.gms.maps.GoogleMapOptions;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o83 extends tm2 implements x63 {
    @DexIgnore
    public o83(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IMapFragmentDelegate");
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void a(ab2 ab2, GoogleMapOptions googleMapOptions, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        xm2.a(zza, googleMapOptions);
        xm2.a(zza, bundle);
        b(2, zza);
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void e() throws RemoteException {
        b(7, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onCreate(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bundle);
        b(3, zza);
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onDestroy() throws RemoteException {
        b(8, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onLowMemory() throws RemoteException {
        b(9, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onPause() throws RemoteException {
        b(6, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onResume() throws RemoteException {
        b(5, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onSaveInstanceState(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, bundle);
        Parcel a = a(10, zza);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onStart() throws RemoteException {
        b(15, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void onStop() throws RemoteException {
        b(16, zza());
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final ab2 a(ab2 ab2, ab2 ab22, Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, ab2);
        xm2.a(zza, ab22);
        xm2.a(zza, bundle);
        Parcel a = a(4, zza);
        ab2 a2 = ab2.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    @Override // com.fossil.x63
    public final void a(o73 o73) throws RemoteException {
        Parcel zza = zza();
        xm2.a(zza, o73);
        b(12, zza);
    }
}
