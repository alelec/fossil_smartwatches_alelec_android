package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x21 extends fe7 implements kd7<zk0, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ rf1 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public x21(rf1 rf1) {
        super(2);
        this.a = rf1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(zk0 zk0, Float f) {
        float floatValue = f.floatValue();
        rf1 rf1 = this.a;
        rf1.a(((((float) rf1.F) + floatValue) * rf1.G) + 0.1f);
        return i97.a;
    }
}
