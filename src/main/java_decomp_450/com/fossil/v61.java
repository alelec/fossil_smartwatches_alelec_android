package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v61 extends zk0 {
    @DexIgnore
    public /* final */ ArrayList<ul0> C; // = yz0.a(((zk0) this).i, w97.a((Object[]) new ul0[]{ul0.FILE_CONFIG, ul0.TRANSFER_DATA}));
    @DexIgnore
    public /* final */ short D;

    @DexIgnore
    public v61(ri1 ri1, en0 en0, wm0 wm0, short s, String str, boolean z) {
        super(ri1, en0, wm0, str, z);
        this.D = s;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean a(zk0 zk0) {
        return !(zk0 instanceof v61) || ((v61) zk0).D != this.D;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public ArrayList<ul0> f() {
        return this.C;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public JSONObject i() {
        return yz0.a(yz0.a(super.i(), r51.y0, yz0.a(this.D)), r51.e4, a81.d.a(this.D));
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean a(v81 v81) {
        qa1 qa1 = v81 != null ? v81.x : null;
        if (qa1 != null && y41.a[qa1.ordinal()] == 1) {
            return true;
        }
        return false;
    }
}
