package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceArray;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm7 implements Executor, Closeable {
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater h; // = AtomicLongFieldUpdater.newUpdater(vm7.class, "parkedWorkersStack");
    @DexIgnore
    public static /* final */ AtomicLongFieldUpdater i; // = AtomicLongFieldUpdater.newUpdater(vm7.class, "controlState");
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater j; // = AtomicIntegerFieldUpdater.newUpdater(vm7.class, "_isTerminated");
    @DexIgnore
    public static /* final */ lm7 p; // = new lm7("NOT_IN_STACK");
    @DexIgnore
    public volatile int _isTerminated;
    @DexIgnore
    public /* final */ zm7 a;
    @DexIgnore
    public /* final */ zm7 b;
    @DexIgnore
    public /* final */ AtomicReferenceArray<b> c;
    @DexIgnore
    public volatile long controlState;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public volatile long parkedWorkersStack;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Thread {
        @DexIgnore
        public static /* final */ AtomicIntegerFieldUpdater h; // = AtomicIntegerFieldUpdater.newUpdater(b.class, "workerCtl");
        @DexIgnore
        public /* final */ in7 a;
        @DexIgnore
        public c b;
        @DexIgnore
        public long c;
        @DexIgnore
        public long d;
        @DexIgnore
        public int e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public volatile int indexInArray;
        @DexIgnore
        public volatile Object nextParkedWorker;
        @DexIgnore
        public volatile int workerCtl;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public b() {
            setDaemon(true);
            this.a = new in7();
            this.b = c.DORMANT;
            this.workerCtl = 0;
            this.nextParkedWorker = vm7.p;
            this.e = gf7.b.a();
        }

        @DexIgnore
        public final int a() {
            return this.indexInArray;
        }

        @DexIgnore
        public final Object b() {
            return this.nextParkedWorker;
        }

        @DexIgnore
        public final boolean c() {
            return this.nextParkedWorker != vm7.p;
        }

        @DexIgnore
        public final int d(int i) {
            int i2 = this.e;
            int i3 = i2 ^ (i2 << 13);
            int i4 = i3 ^ (i3 >> 17);
            int i5 = i4 ^ (i4 << 5);
            this.e = i5;
            int i6 = i - 1;
            if ((i6 & i) == 0) {
                return i5 & i6;
            }
            return (i5 & Integer.MAX_VALUE) % i;
        }

        @DexIgnore
        public final void e(int i) {
            StringBuilder sb = new StringBuilder();
            sb.append(vm7.this.g);
            sb.append("-worker-");
            sb.append(i == 0 ? "TERMINATED" : String.valueOf(i));
            setName(sb.toString());
            this.indexInArray = i;
        }

        @DexIgnore
        public final void f() {
            loop0:
            while (true) {
                boolean z = false;
                while (!vm7.this.isTerminated() && this.b != c.TERMINATED) {
                    dn7 b2 = b(this.f);
                    if (b2 != null) {
                        this.d = 0;
                        a(b2);
                    } else {
                        this.f = false;
                        if (this.d == 0) {
                            h();
                        } else if (!z) {
                            z = true;
                        } else {
                            a(c.PARKING);
                            Thread.interrupted();
                            LockSupport.parkNanos(this.d);
                            this.d = 0;
                        }
                    }
                }
            }
            a(c.TERMINATED);
        }

        @DexIgnore
        public final boolean g() {
            boolean z;
            if (this.b != c.CPU_ACQUIRED) {
                vm7 vm7 = vm7.this;
                while (true) {
                    long j = vm7.controlState;
                    if (((int) ((9223367638808264704L & j) >> 42)) != 0) {
                        if (vm7.i.compareAndSet(vm7, j, j - 4398046511104L)) {
                            z = true;
                            break;
                        }
                    } else {
                        z = false;
                        break;
                    }
                }
                if (!z) {
                    return false;
                }
                this.b = c.CPU_ACQUIRED;
            }
            return true;
        }

        @DexIgnore
        public final void h() {
            if (!c()) {
                vm7.this.b(this);
                return;
            }
            if (dj7.a()) {
                if (!(this.a.b() == 0)) {
                    throw new AssertionError();
                }
            }
            this.workerCtl = -1;
            while (c() && !vm7.this.isTerminated() && this.b != c.TERMINATED) {
                a(c.PARKING);
                Thread.interrupted();
                d();
            }
        }

        @DexIgnore
        public final void i() {
            synchronized (vm7.this.c) {
                if (!vm7.this.isTerminated()) {
                    if (vm7.this.c() > vm7.this.d) {
                        if (h.compareAndSet(this, -1, 1)) {
                            int i = this.indexInArray;
                            e(0);
                            vm7.this.a(this, i, 0);
                            int andDecrement = (int) (vm7.i.getAndDecrement(vm7.this) & 2097151);
                            if (andDecrement != i) {
                                b bVar = vm7.this.c.get(andDecrement);
                                if (bVar != null) {
                                    b bVar2 = bVar;
                                    vm7.this.c.set(i, bVar2);
                                    bVar2.e(i);
                                    vm7.this.a(bVar2, andDecrement, i);
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                            vm7.this.c.set(andDecrement, null);
                            i97 i97 = i97.a;
                            this.b = c.TERMINATED;
                        }
                    }
                }
            }
        }

        @DexIgnore
        public void run() {
            f();
        }

        @DexIgnore
        public final void a(Object obj) {
            this.nextParkedWorker = obj;
        }

        @DexIgnore
        public final void b(int i) {
            if (i != 0 && a(c.BLOCKING)) {
                vm7.this.g();
            }
        }

        @DexIgnore
        public final void c(int i) {
            this.c = 0;
            if (this.b == c.PARKING) {
                if (dj7.a()) {
                    boolean z = true;
                    if (i != 1) {
                        z = false;
                    }
                    if (!z) {
                        throw new AssertionError();
                    }
                }
                this.b = c.BLOCKING;
            }
        }

        @DexIgnore
        public final boolean a(c cVar) {
            c cVar2 = this.b;
            boolean z = cVar2 == c.CPU_ACQUIRED;
            if (z) {
                vm7.i.addAndGet(vm7.this, 4398046511104L);
            }
            if (cVar2 != cVar) {
                this.b = cVar;
            }
            return z;
        }

        @DexIgnore
        public final dn7 e() {
            if (d(2) == 0) {
                dn7 dn7 = (dn7) vm7.this.a.c();
                if (dn7 != null) {
                    return dn7;
                }
                return (dn7) vm7.this.b.c();
            }
            dn7 dn72 = (dn7) vm7.this.b.c();
            if (dn72 != null) {
                return dn72;
            }
            return (dn7) vm7.this.a.c();
        }

        @DexIgnore
        public final dn7 b(boolean z) {
            dn7 dn7;
            if (g()) {
                return a(z);
            }
            if (z) {
                dn7 = this.a.c();
                if (dn7 == null) {
                    dn7 = (dn7) vm7.this.b.c();
                }
            } else {
                dn7 = (dn7) vm7.this.b.c();
            }
            return dn7 != null ? dn7 : c(true);
        }

        @DexIgnore
        public final void d() {
            if (this.c == 0) {
                this.c = System.nanoTime() + vm7.this.f;
            }
            LockSupport.parkNanos(vm7.this.f);
            if (System.nanoTime() - this.c >= 0) {
                this.c = 0;
                i();
            }
        }

        @DexIgnore
        public final dn7 c(boolean z) {
            long j;
            if (dj7.a()) {
                if (!(this.a.b() == 0)) {
                    throw new AssertionError();
                }
            }
            int a2 = vm7.this.c();
            if (a2 < 2) {
                return null;
            }
            int d2 = d(a2);
            long j2 = Long.MAX_VALUE;
            for (int i = 0; i < a2; i++) {
                d2++;
                if (d2 > a2) {
                    d2 = 1;
                }
                b bVar = vm7.this.c.get(d2);
                if (!(bVar == null || bVar == this)) {
                    if (dj7.a()) {
                        if (!(this.a.b() == 0)) {
                            throw new AssertionError();
                        }
                    }
                    if (z) {
                        j = this.a.a(bVar.a);
                    } else {
                        j = this.a.b(bVar.a);
                    }
                    if (j == -1) {
                        return this.a.c();
                    }
                    if (j > 0) {
                        j2 = Math.min(j2, j);
                    }
                }
            }
            if (j2 == Long.MAX_VALUE) {
                j2 = 0;
            }
            this.d = j2;
            return null;
        }

        @DexIgnore
        public b(vm7 vm7, int i) {
            this();
            e(i);
        }

        @DexIgnore
        public final void a(int i) {
            if (i != 0) {
                vm7.i.addAndGet(vm7.this, -2097152);
                c cVar = this.b;
                if (cVar != c.TERMINATED) {
                    if (dj7.a()) {
                        if (!(cVar == c.BLOCKING)) {
                            throw new AssertionError();
                        }
                    }
                    this.b = c.DORMANT;
                }
            }
        }

        @DexIgnore
        public final dn7 a(boolean z) {
            dn7 e2;
            dn7 e3;
            if (z) {
                boolean z2 = d(vm7.this.d * 2) == 0;
                if (z2 && (e3 = e()) != null) {
                    return e3;
                }
                dn7 c2 = this.a.c();
                if (c2 != null) {
                    return c2;
                }
                if (!z2 && (e2 = e()) != null) {
                    return e2;
                }
            } else {
                dn7 e4 = e();
                if (e4 != null) {
                    return e4;
                }
            }
            return c(false);
        }

        @DexIgnore
        public final void a(dn7 dn7) {
            int c2 = dn7.b.c();
            c(c2);
            b(c2);
            vm7.this.b(dn7);
            a(c2);
        }
    }

    @DexIgnore
    public enum c {
        CPU_ACQUIRED,
        BLOCKING,
        PARKING,
        DORMANT,
        TERMINATED
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public vm7(int i2, int i3, long j2, String str) {
        this.d = i2;
        this.e = i3;
        this.f = j2;
        this.g = str;
        if (i2 >= 1) {
            if (this.e >= this.d) {
                if (this.e <= 2097150) {
                    if (this.f > 0) {
                        this.a = new zm7();
                        this.b = new zm7();
                        this.parkedWorkersStack = 0;
                        this.c = new AtomicReferenceArray<>(this.e + 1);
                        this.controlState = ((long) this.d) << 42;
                        this._isTerminated = 0;
                        return;
                    }
                    throw new IllegalArgumentException(("Idle worker keep alive time " + this.f + " must be positive").toString());
                }
                throw new IllegalArgumentException(("Max pool size " + this.e + " should not exceed maximal supported number of threads 2097150").toString());
            }
            throw new IllegalArgumentException(("Max pool size " + this.e + " should be greater than or equals to core pool size " + this.d).toString());
        }
        throw new IllegalArgumentException(("Core pool size " + this.d + " should be at least 1").toString());
    }

    @DexIgnore
    public final boolean b(b bVar) {
        long j2;
        long j3;
        int a2;
        if (bVar.b() != p) {
            return false;
        }
        do {
            j2 = this.parkedWorkersStack;
            int i2 = (int) (2097151 & j2);
            j3 = (2097152 + j2) & -2097152;
            a2 = bVar.a();
            if (dj7.a()) {
                if (!(a2 != 0)) {
                    throw new AssertionError();
                }
            }
            bVar.a(this.c.get(i2));
        } while (!h.compareAndSet(this, j2, ((long) a2) | j3));
        return true;
    }

    @DexIgnore
    public final int c() {
        return (int) (this.controlState & 2097151);
    }

    @DexIgnore
    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
        e(ButtonService.CONNECT_TIMEOUT);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x006f, code lost:
        if (r9 != null) goto L_0x007a;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e(long r9) {
        /*
            r8 = this;
            java.util.concurrent.atomic.AtomicIntegerFieldUpdater r0 = com.fossil.vm7.j
            r1 = 0
            r2 = 1
            boolean r0 = r0.compareAndSet(r8, r1, r2)
            if (r0 != 0) goto L_0x000b
            return
        L_0x000b:
            com.fossil.vm7$b r0 = r8.b()
            java.util.concurrent.atomic.AtomicReferenceArray<com.fossil.vm7$b> r3 = r8.c
            monitor-enter(r3)
            long r4 = r8.controlState     // Catch:{ all -> 0x00b9 }
            r6 = 2097151(0x1fffff, double:1.0361303E-317)
            long r4 = r4 & r6
            int r5 = (int) r4
            monitor-exit(r3)
            if (r2 > r5) goto L_0x005f
            r3 = 1
        L_0x001d:
            java.util.concurrent.atomic.AtomicReferenceArray<com.fossil.vm7$b> r4 = r8.c
            java.lang.Object r4 = r4.get(r3)
            if (r4 == 0) goto L_0x005a
            com.fossil.vm7$b r4 = (com.fossil.vm7.b) r4
            if (r4 == r0) goto L_0x0055
        L_0x0029:
            boolean r6 = r4.isAlive()
            if (r6 == 0) goto L_0x0036
            java.util.concurrent.locks.LockSupport.unpark(r4)
            r4.join(r9)
            goto L_0x0029
        L_0x0036:
            com.fossil.vm7$c r6 = r4.b
            boolean r7 = com.fossil.dj7.a()
            if (r7 == 0) goto L_0x004e
            com.fossil.vm7$c r7 = com.fossil.vm7.c.TERMINATED
            if (r6 != r7) goto L_0x0044
            r6 = 1
            goto L_0x0045
        L_0x0044:
            r6 = 0
        L_0x0045:
            if (r6 == 0) goto L_0x0048
            goto L_0x004e
        L_0x0048:
            java.lang.AssertionError r9 = new java.lang.AssertionError
            r9.<init>()
            throw r9
        L_0x004e:
            com.fossil.in7 r4 = r4.a
            com.fossil.zm7 r6 = r8.b
            r4.a(r6)
        L_0x0055:
            if (r3 == r5) goto L_0x005f
            int r3 = r3 + 1
            goto L_0x001d
        L_0x005a:
            com.fossil.ee7.a()
            r9 = 0
            throw r9
        L_0x005f:
            com.fossil.zm7 r9 = r8.b
            r9.a()
            com.fossil.zm7 r9 = r8.a
            r9.a()
        L_0x0069:
            if (r0 == 0) goto L_0x0072
            com.fossil.dn7 r9 = r0.b(r2)
            if (r9 == 0) goto L_0x0072
            goto L_0x007a
        L_0x0072:
            com.fossil.zm7 r9 = r8.a
            java.lang.Object r9 = r9.c()
            com.fossil.dn7 r9 = (com.fossil.dn7) r9
        L_0x007a:
            if (r9 == 0) goto L_0x007d
            goto L_0x0085
        L_0x007d:
            com.fossil.zm7 r9 = r8.b
            java.lang.Object r9 = r9.c()
            com.fossil.dn7 r9 = (com.fossil.dn7) r9
        L_0x0085:
            if (r9 == 0) goto L_0x008b
            r8.b(r9)
            goto L_0x0069
        L_0x008b:
            if (r0 == 0) goto L_0x0092
            com.fossil.vm7$c r9 = com.fossil.vm7.c.TERMINATED
            r0.a(r9)
        L_0x0092:
            boolean r9 = com.fossil.dj7.a()
            if (r9 == 0) goto L_0x00b2
            long r9 = r8.controlState
            r3 = 9223367638808264704(0x7ffffc0000000000, double:NaN)
            long r9 = r9 & r3
            r0 = 42
            long r9 = r9 >> r0
            int r10 = (int) r9
            int r9 = r8.d
            if (r10 != r9) goto L_0x00a9
            r1 = 1
        L_0x00a9:
            if (r1 == 0) goto L_0x00ac
            goto L_0x00b2
        L_0x00ac:
            java.lang.AssertionError r9 = new java.lang.AssertionError
            r9.<init>()
            throw r9
        L_0x00b2:
            r9 = 0
            r8.parkedWorkersStack = r9
            r8.controlState = r9
            return
        L_0x00b9:
            r9 = move-exception
            monitor-exit(r3)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.vm7.e(long):void");
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        a(this, runnable, null, false, 6, null);
    }

    @DexIgnore
    public final void g() {
        if (!k() && !a(this, 0, 1, null)) {
            k();
        }
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r0v0, types: [int, boolean] */
    public final boolean isTerminated() {
        return this._isTerminated;
    }

    @DexIgnore
    public final boolean k() {
        b e2;
        do {
            e2 = e();
            if (e2 == null) {
                return false;
            }
        } while (!b.h.compareAndSet(e2, -1, 0));
        LockSupport.unpark(e2);
        return true;
    }

    @DexIgnore
    public String toString() {
        ArrayList arrayList = new ArrayList();
        int length = this.c.length();
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        for (int i7 = 1; i7 < length; i7++) {
            b bVar = this.c.get(i7);
            if (bVar != null) {
                int b2 = bVar.a.b();
                int i8 = wm7.a[bVar.b.ordinal()];
                if (i8 == 1) {
                    i4++;
                } else if (i8 == 2) {
                    i3++;
                    arrayList.add(String.valueOf(b2) + "b");
                } else if (i8 == 3) {
                    i2++;
                    arrayList.add(String.valueOf(b2) + "c");
                } else if (i8 == 4) {
                    i5++;
                    if (b2 > 0) {
                        arrayList.add(String.valueOf(b2) + "d");
                    }
                } else if (i8 == 5) {
                    i6++;
                }
            }
        }
        long j2 = this.controlState;
        return this.g + '@' + ej7.b(this) + '[' + "Pool Size {" + "core = " + this.d + ", " + "max = " + this.e + "}, " + "Worker States {" + "CPU = " + i2 + ", " + "blocking = " + i3 + ", " + "parked = " + i4 + ", " + "dormant = " + i5 + ", " + "terminated = " + i6 + "}, " + "running workers queues = " + arrayList + ", " + "global CPU queue size = " + this.a.b() + ", " + "global blocking queue size = " + this.b.b() + ", " + "Control State {" + "created workers= " + ((int) (2097151 & j2)) + ", " + "blocking tasks = " + ((int) ((4398044413952L & j2) >> 21)) + ", " + "CPUs acquired = " + (this.d - ((int) ((9223367638808264704L & j2) >> 42))) + "}]";
    }

    @DexIgnore
    public final int a(b bVar) {
        Object b2 = bVar.b();
        while (b2 != p) {
            if (b2 == null) {
                return 0;
            }
            b bVar2 = (b) b2;
            int a2 = bVar2.a();
            if (a2 != 0) {
                return a2;
            }
            b2 = bVar2.b();
        }
        return -1;
    }

    @DexIgnore
    public final boolean g(long j2) {
        if (qf7.a(((int) (2097151 & j2)) - ((int) ((j2 & 4398044413952L) >> 21)), 0) < this.d) {
            int a2 = a();
            if (a2 == 1 && this.d > 1) {
                a();
            }
            if (a2 > 0) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static /* synthetic */ void a(vm7 vm7, Runnable runnable, en7 en7, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            en7 = cn7.b;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        vm7.a(runnable, en7, z);
    }

    @DexIgnore
    public final b b() {
        Thread currentThread = Thread.currentThread();
        if (!(currentThread instanceof b)) {
            currentThread = null;
        }
        b bVar = (b) currentThread;
        if (bVar == null || !ee7.a(vm7.this, this)) {
            return null;
        }
        return bVar;
    }

    @DexIgnore
    public final void a(Runnable runnable, en7 en7, boolean z) {
        gl7 a2 = hl7.a();
        if (a2 != null) {
            a2.e();
        }
        dn7 a3 = a(runnable, en7);
        b b2 = b();
        dn7 a4 = a(b2, a3, z);
        if (a4 == null || a(a4)) {
            boolean z2 = z && b2 != null;
            if (a3.b.c() != 0) {
                a(z2);
            } else if (!z2) {
                g();
            }
        } else {
            throw new RejectedExecutionException(this.g + " was terminated");
        }
    }

    @DexIgnore
    public final void b(dn7 dn7) {
        gl7 a2;
        try {
            dn7.run();
            a2 = hl7.a();
            if (a2 == null) {
                return;
            }
        } catch (Throwable th) {
            gl7 a3 = hl7.a();
            if (a3 != null) {
                a3.c();
            }
            throw th;
        }
        a2.c();
    }

    @DexIgnore
    public final dn7 a(Runnable runnable, en7 en7) {
        long a2 = gn7.e.a();
        if (!(runnable instanceof dn7)) {
            return new fn7(runnable, a2, en7);
        }
        dn7 dn7 = (dn7) runnable;
        dn7.a = a2;
        dn7.b = en7;
        return dn7;
    }

    @DexIgnore
    public static /* synthetic */ boolean a(vm7 vm7, long j2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            j2 = vm7.controlState;
        }
        return vm7.g(j2);
    }

    @DexIgnore
    public final int a() {
        synchronized (this.c) {
            if (isTerminated()) {
                return -1;
            }
            long j2 = this.controlState;
            int i2 = (int) (j2 & 2097151);
            boolean z = false;
            int a2 = qf7.a(i2 - ((int) ((j2 & 4398044413952L) >> 21)), 0);
            if (a2 >= this.d) {
                return 0;
            }
            if (i2 >= this.e) {
                return 0;
            }
            int i3 = ((int) (this.controlState & 2097151)) + 1;
            if (i3 > 0 && this.c.get(i3) == null) {
                b bVar = new b(this, i3);
                this.c.set(i3, bVar);
                if (i3 == ((int) (2097151 & i.incrementAndGet(this)))) {
                    z = true;
                }
                if (z) {
                    bVar.start();
                    return a2 + 1;
                }
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }
    }

    @DexIgnore
    public final b e() {
        while (true) {
            long j2 = this.parkedWorkersStack;
            b bVar = this.c.get((int) (2097151 & j2));
            if (bVar == null) {
                return null;
            }
            long j3 = (2097152 + j2) & -2097152;
            int a2 = a(bVar);
            if (a2 >= 0 && h.compareAndSet(this, j2, ((long) a2) | j3)) {
                bVar.a(p);
                return bVar;
            }
        }
    }

    @DexIgnore
    public final dn7 a(b bVar, dn7 dn7, boolean z) {
        if (bVar == null || bVar.b == c.TERMINATED) {
            return dn7;
        }
        if (dn7.b.c() == 0 && bVar.b == c.BLOCKING) {
            return dn7;
        }
        bVar.f = true;
        return bVar.a.a(dn7, z);
    }

    @DexIgnore
    public final boolean a(dn7 dn7) {
        boolean z = true;
        if (dn7.b.c() != 1) {
            z = false;
        }
        if (z) {
            return this.b.a(dn7);
        }
        return this.a.a(dn7);
    }

    @DexIgnore
    public final void a(b bVar, int i2, int i3) {
        while (true) {
            long j2 = this.parkedWorkersStack;
            int i4 = (int) (2097151 & j2);
            long j3 = (2097152 + j2) & -2097152;
            if (i4 == i2) {
                i4 = i3 == 0 ? a(bVar) : i3;
            }
            if (i4 >= 0 && h.compareAndSet(this, j2, j3 | ((long) i4))) {
                return;
            }
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        long addAndGet = i.addAndGet(this, 2097152);
        if (!z && !k() && !g(addAndGet)) {
            k();
        }
    }
}
