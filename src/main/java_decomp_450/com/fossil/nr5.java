package com.fossil;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;

public final class nr5 {
    public final float a;
    public final float b;
    public final float c;
    public final float d;
    public final b e;
    public final PointF f = new PointF();

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|(3:17|18|20)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.fossil.nr5$b[] r0 = com.fossil.nr5.b.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.nr5.a.a = r0
                com.fossil.nr5$b r1 = com.fossil.nr5.b.TOP_LEFT     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.TOP_RIGHT     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.BOTTOM_LEFT     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.BOTTOM_RIGHT     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.LEFT     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.TOP     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.RIGHT     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.fossil.nr5.a.a     // Catch:{ NoSuchFieldError -> 0x006c }
                com.fossil.nr5$b r1 = com.fossil.nr5.b.CENTER     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.nr5.a.<clinit>():void");
        }
        */
    }

    public enum b {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        CENTER
    }

    /*
    static {
        new Matrix();
    }
    */

    public nr5(b bVar, mr5 mr5, float f2, float f3) {
        this.e = bVar;
        this.a = mr5.e();
        this.b = mr5.d();
        this.c = mr5.c();
        this.d = mr5.b();
        a(mr5.f(), f2, f3);
    }

    public static float a(float f2, float f3, float f4, float f5) {
        return (f4 - f2) / (f5 - f3);
    }

    public void a(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, boolean z, float f5) {
        PointF pointF = this.f;
        float f6 = f2 + pointF.x;
        float f7 = f3 + pointF.y;
        if (this.e == b.CENTER) {
            a(rectF, f6, f7, rectF2, i, i2, f4);
        } else if (z) {
            a(rectF, f6, f7, rectF2, i, i2, f4, f5);
        } else {
            b(rectF, f6, f7, rectF2, i, i2, f4);
        }
    }

    public final void b(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4) {
        switch (a.a[this.e.ordinal()]) {
            case 1:
                b(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                a(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 2:
                b(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                b(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 3:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                a(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 4:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                b(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 5:
                a(rectF, f2, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 6:
                b(rectF, f3, rectF2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 7:
                b(rectF, f2, rectF2, i, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, false, false);
                return;
            default:
                return;
        }
    }

    public final void c(RectF rectF, RectF rectF2, float f2) {
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4 + f2) {
            rectF.offset(f4 - f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f5 = rectF.top;
        float f6 = rectF2.top;
        if (f5 < f6 + f2) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6 - f5);
        }
        float f7 = rectF.right;
        float f8 = rectF2.right;
        if (f7 > f8 - f2) {
            rectF.offset(f8 - f7, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f9 = rectF.bottom;
        float f10 = rectF2.bottom;
        if (f9 > f10 - f2) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10 - f9);
        }
    }

    public final void d(RectF rectF, float f2) {
        rectF.top = rectF.bottom - (rectF.width() / f2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void a(RectF rectF, float f2, float f3) {
        float f4;
        float f5;
        float f6;
        int i = a.a[this.e.ordinal()];
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        switch (i) {
            case 1:
                f7 = rectF.left - f2;
                f5 = rectF.top;
                f4 = f5 - f3;
                break;
            case 2:
                f7 = rectF.right - f2;
                f5 = rectF.top;
                f4 = f5 - f3;
                break;
            case 3:
                f7 = rectF.left - f2;
                f5 = rectF.bottom;
                f4 = f5 - f3;
                break;
            case 4:
                f7 = rectF.right - f2;
                f5 = rectF.bottom;
                f4 = f5 - f3;
                break;
            case 5:
                f6 = rectF.left;
                f7 = f6 - f2;
                f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                break;
            case 6:
                f5 = rectF.top;
                f4 = f5 - f3;
                break;
            case 7:
                f6 = rectF.right;
                f7 = f6 - f2;
                f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                break;
            case 8:
                f5 = rectF.bottom;
                f4 = f5 - f3;
                break;
            case 9:
                f7 = rectF.centerX() - f2;
                f5 = rectF.centerY();
                f4 = f5 - f3;
                break;
            default:
                f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                break;
        }
        PointF pointF = this.f;
        pointF.x = f7;
        pointF.y = f4;
    }

    public final void c(RectF rectF, float f2) {
        rectF.right = rectF.left + (rectF.height() * f2);
    }

    public final void b(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.x -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.right;
        if (f2 > f6) {
            this.f.x -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.right;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.left;
        float f9 = this.a;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.left;
        float f11 = this.c;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.right;
        if (f12 - f2 < f3) {
            f2 = f12;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f13 = rectF.left;
            float f14 = (f2 - f13) / f4;
            float f15 = this.b;
            if (f14 < f15) {
                f2 = Math.min(rectF2.right, f13 + (f15 * f4));
                f14 = (f2 - rectF.left) / f4;
            }
            float f16 = this.d;
            if (f14 > f16) {
                f2 = Math.min(rectF2.right, rectF.left + (f16 * f4));
                f14 = (f2 - rectF.left) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.bottom;
                    float f18 = rectF2.top;
                    if (f17 - f14 < f18) {
                        f2 = Math.min(rectF2.right, rectF.left + ((f17 - f18) * f4));
                        f14 = (f2 - rectF.left) / f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.top;
                    float f20 = rectF2.bottom;
                    if (f14 + f19 > f20) {
                        f2 = Math.min(f2, Math.min(rectF2.right, rectF.left + ((f20 - f19) * f4)));
                    }
                }
            } else {
                f2 = Math.min(f2, Math.min(rectF2.right, rectF.left + (rectF2.height() * f4)));
            }
        }
        rectF.right = f2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0056, code lost:
        if ((r0 + r9) <= r10.bottom) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002d, code lost:
        if ((r1 + r8) <= r10.right) goto L_0x0039;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.graphics.RectF r7, float r8, float r9, android.graphics.RectF r10, int r11, int r12, float r13) {
        /*
            r6 = this;
            float r0 = r7.centerX()
            float r8 = r8 - r0
            float r0 = r7.centerY()
            float r9 = r9 - r0
            float r0 = r7.left
            float r1 = r0 + r8
            r2 = 1073741824(0x40000000, float:2.0)
            r3 = 1065772646(0x3f866666, float:1.05)
            r4 = 0
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 < 0) goto L_0x002f
            float r1 = r7.right
            float r5 = r1 + r8
            float r11 = (float) r11
            int r11 = (r5 > r11 ? 1 : (r5 == r11 ? 0 : -1))
            if (r11 > 0) goto L_0x002f
            float r0 = r0 + r8
            float r11 = r10.left
            int r11 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r11 < 0) goto L_0x002f
            float r1 = r1 + r8
            float r11 = r10.right
            int r11 = (r1 > r11 ? 1 : (r1 == r11 ? 0 : -1))
            if (r11 <= 0) goto L_0x0039
        L_0x002f:
            float r8 = r8 / r3
            android.graphics.PointF r11 = r6.f
            float r0 = r11.x
            float r1 = r8 / r2
            float r0 = r0 - r1
            r11.x = r0
        L_0x0039:
            float r11 = r7.top
            float r0 = r11 + r9
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x0058
            float r0 = r7.bottom
            float r1 = r0 + r9
            float r12 = (float) r12
            int r12 = (r1 > r12 ? 1 : (r1 == r12 ? 0 : -1))
            if (r12 > 0) goto L_0x0058
            float r11 = r11 + r9
            float r12 = r10.top
            int r11 = (r11 > r12 ? 1 : (r11 == r12 ? 0 : -1))
            if (r11 < 0) goto L_0x0058
            float r0 = r0 + r9
            float r11 = r10.bottom
            int r11 = (r0 > r11 ? 1 : (r0 == r11 ? 0 : -1))
            if (r11 <= 0) goto L_0x0062
        L_0x0058:
            float r9 = r9 / r3
            android.graphics.PointF r11 = r6.f
            float r12 = r11.y
            float r0 = r9 / r2
            float r12 = r12 - r0
            r11.y = r12
        L_0x0062:
            r7.offset(r8, r9)
            r6.c(r7, r10, r13)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.nr5.a(android.graphics.RectF, float, float, android.graphics.RectF, int, int, float):void");
    }

    public final void a(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, float f5) {
        switch (a.a[this.e.ordinal()]) {
            case 1:
                if (a(f2, f3, rectF.right, rectF.bottom) < f5) {
                    b(rectF, f3, rectF2, f4, f5, true, false);
                    b(rectF, f5);
                    return;
                }
                a(rectF, f2, rectF2, f4, f5, true, false);
                d(rectF, f5);
                return;
            case 2:
                if (a(rectF.left, f3, f2, rectF.bottom) < f5) {
                    b(rectF, f3, rectF2, f4, f5, false, true);
                    c(rectF, f5);
                    return;
                }
                b(rectF, f2, rectF2, i, f4, f5, true, false);
                d(rectF, f5);
                return;
            case 3:
                if (a(f2, rectF.top, rectF.right, f3) < f5) {
                    a(rectF, f3, rectF2, i2, f4, f5, true, false);
                    b(rectF, f5);
                    return;
                }
                a(rectF, f2, rectF2, f4, f5, false, true);
                a(rectF, f5);
                return;
            case 4:
                if (a(rectF.left, rectF.top, f2, f3) < f5) {
                    a(rectF, f3, rectF2, i2, f4, f5, false, true);
                    c(rectF, f5);
                    return;
                }
                b(rectF, f2, rectF2, i, f4, f5, false, true);
                a(rectF, f5);
                return;
            case 5:
                a(rectF, f2, rectF2, f4, f5, true, true);
                b(rectF, rectF2, f5);
                return;
            case 6:
                b(rectF, f3, rectF2, f4, f5, true, true);
                a(rectF, rectF2, f5);
                return;
            case 7:
                b(rectF, f2, rectF2, i, f4, f5, true, true);
                b(rectF, rectF2, f5);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, f5, true, true);
                a(rectF, rectF2, f5);
                return;
            default:
                return;
        }
    }

    public final void b(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f2 /= 1.05f;
            this.f.y -= f2 / 1.1f;
        }
        float f5 = rectF2.top;
        if (f2 < f5) {
            this.f.y -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.top;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.bottom;
        float f8 = this.b;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.bottom;
        float f10 = this.d;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.top;
        if (f2 - f11 < f3) {
            f2 = f11;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f12 = rectF.bottom;
            float f13 = (f12 - f2) * f4;
            float f14 = this.a;
            if (f13 < f14) {
                f2 = Math.max(rectF2.top, f12 - (f14 / f4));
                f13 = (rectF.bottom - f2) * f4;
            }
            float f15 = this.c;
            if (f13 > f15) {
                f2 = Math.max(rectF2.top, rectF.bottom - (f15 / f4));
                f13 = (rectF.bottom - f2) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.right;
                    float f17 = rectF2.left;
                    if (f16 - f13 < f17) {
                        f2 = Math.max(rectF2.top, rectF.bottom - ((f16 - f17) / f4));
                        f13 = (rectF.bottom - f2) * f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.left;
                    float f19 = rectF2.right;
                    if (f13 + f18 > f19) {
                        f2 = Math.max(f2, Math.max(rectF2.top, rectF.bottom - ((f19 - f18) / f4)));
                    }
                }
            } else {
                f2 = Math.max(f2, Math.max(rectF2.top, rectF.bottom - (rectF2.width() / f4)));
            }
        }
        rectF.top = f2;
    }

    public final void a(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f2 /= 1.05f;
            this.f.x -= f2 / 1.1f;
        }
        float f5 = rectF2.left;
        if (f2 < f5) {
            this.f.x -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.left;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.right;
        float f8 = this.a;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.right;
        float f10 = this.c;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.left;
        if (f2 - f11 < f3) {
            f2 = f11;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f12 = rectF.right;
            float f13 = (f12 - f2) / f4;
            float f14 = this.b;
            if (f13 < f14) {
                f2 = Math.max(rectF2.left, f12 - (f14 * f4));
                f13 = (rectF.right - f2) / f4;
            }
            float f15 = this.d;
            if (f13 > f15) {
                f2 = Math.max(rectF2.left, rectF.right - (f15 * f4));
                f13 = (rectF.right - f2) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.bottom;
                    float f17 = rectF2.top;
                    if (f16 - f13 < f17) {
                        f2 = Math.max(rectF2.left, rectF.right - ((f16 - f17) * f4));
                        f13 = (rectF.right - f2) / f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.top;
                    float f19 = rectF2.bottom;
                    if (f13 + f18 > f19) {
                        f2 = Math.max(f2, Math.max(rectF2.left, rectF.right - ((f19 - f18) * f4)));
                    }
                }
            } else {
                f2 = Math.max(f2, Math.max(rectF2.left, rectF.right - (rectF2.height() * f4)));
            }
        }
        rectF.left = f2;
    }

    public final void b(RectF rectF, float f2) {
        rectF.left = rectF.right - (rectF.height() * f2);
    }

    public final void b(RectF rectF, RectF rectF2, float f2) {
        rectF.inset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (rectF.height() - (rectF.width() / f2)) / 2.0f);
        float f3 = rectF.top;
        float f4 = rectF2.top;
        if (f3 < f4) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f4 - f3);
        }
        float f5 = rectF.bottom;
        float f6 = rectF2.bottom;
        if (f5 > f6) {
            rectF.offset(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6 - f5);
        }
    }

    public final void a(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.y -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.bottom;
        if (f2 > f6) {
            this.f.y -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.bottom;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.top;
        float f9 = this.b;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.top;
        float f11 = this.d;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.bottom;
        if (f12 - f2 < f3) {
            f2 = f12;
        }
        if (f4 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f13 = rectF.top;
            float f14 = (f2 - f13) * f4;
            float f15 = this.a;
            if (f14 < f15) {
                f2 = Math.min(rectF2.bottom, f13 + (f15 / f4));
                f14 = (f2 - rectF.top) * f4;
            }
            float f16 = this.c;
            if (f14 > f16) {
                f2 = Math.min(rectF2.bottom, rectF.top + (f16 / f4));
                f14 = (f2 - rectF.top) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.right;
                    float f18 = rectF2.left;
                    if (f17 - f14 < f18) {
                        f2 = Math.min(rectF2.bottom, rectF.top + ((f17 - f18) / f4));
                        f14 = (f2 - rectF.top) * f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.left;
                    float f20 = rectF2.right;
                    if (f14 + f19 > f20) {
                        f2 = Math.min(f2, Math.min(rectF2.bottom, rectF.top + ((f20 - f19) / f4)));
                    }
                }
            } else {
                f2 = Math.min(f2, Math.min(rectF2.bottom, rectF.top + (rectF2.width() / f4)));
            }
        }
        rectF.bottom = f2;
    }

    public final void a(RectF rectF, float f2) {
        rectF.bottom = rectF.top + (rectF.width() / f2);
    }

    public final void a(RectF rectF, RectF rectF2, float f2) {
        rectF.inset((rectF.width() - (rectF.height() * f2)) / 2.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4) {
            rectF.offset(f4 - f3, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        float f5 = rectF.right;
        float f6 = rectF2.right;
        if (f5 > f6) {
            rectF.offset(f6 - f5, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
    }
}
