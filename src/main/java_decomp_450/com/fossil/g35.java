package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleImageButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class g35 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleImageButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;

    @DexIgnore
    public g35(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleImageButton flexibleImageButton, ImageView imageView, RTLImageView rTLImageView, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleImageButton;
        this.s = imageView;
        this.t = rTLImageView;
        this.u = flexibleTextView;
    }
}
