package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class jx0 extends Enum<jx0> {
    @DexIgnore
    public static /* final */ jx0 b;
    @DexIgnore
    public static /* final */ /* synthetic */ jx0[] c;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        jx0 jx0 = new jx0("FOSSIL", 0, (byte) 1);
        b = jx0;
        c = new jx0[]{jx0, new jx0("GOOGLE", 1, (byte) 2)};
    }
    */

    @DexIgnore
    public jx0(String str, int i, byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public static jx0 valueOf(String str) {
        return (jx0) Enum.valueOf(jx0.class, str);
    }

    @DexIgnore
    public static jx0[] values() {
        return (jx0[]) c.clone();
    }
}
