package com.fossil;

import android.content.Context;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m57 {
    @DexIgnore
    public static o57 c;
    @DexIgnore
    public static k57 d; // = v57.b();
    @DexIgnore
    public static JSONObject e; // = new JSONObject();
    @DexIgnore
    public Integer a; // = null;
    @DexIgnore
    public String b; // = null;

    @DexIgnore
    public m57(Context context) {
        try {
            a(context);
            this.a = v57.p(context.getApplicationContext());
            this.b = k47.a(context).b();
        } catch (Throwable th) {
            d.a(th);
        }
    }

    @DexIgnore
    public static synchronized o57 a(Context context) {
        o57 o57;
        synchronized (m57.class) {
            if (c == null) {
                c = new o57(context.getApplicationContext());
            }
            o57 = c;
        }
        return o57;
    }

    @DexIgnore
    public void a(JSONObject jSONObject, Thread thread) {
        String str;
        String str2;
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (c != null) {
                c.a(jSONObject2, thread);
            }
            a67.a(jSONObject2, "cn", this.b);
            if (this.a != null) {
                jSONObject2.put("tn", this.a);
            }
            if (thread == null) {
                str = "ev";
                str2 = jSONObject2;
            } else {
                str = "errkv";
                str2 = jSONObject2.toString();
            }
            jSONObject.put(str, str2);
            if (e != null && e.length() > 0) {
                jSONObject.put("eva", e);
            }
        } catch (Throwable th) {
            d.a(th);
        }
    }
}
