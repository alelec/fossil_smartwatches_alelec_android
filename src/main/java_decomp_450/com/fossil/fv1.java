package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fv1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ by1 b;
    @DexIgnore
    public /* final */ by1 c;

    @DexIgnore
    public fv1(Context context, by1 by1, by1 by12) {
        this.a = context;
        this.b = by1;
        this.c = by12;
    }

    @DexIgnore
    public ev1 a(String str) {
        return ev1.a(this.a, this.b, this.c, str);
    }
}
