package com.fossil;

import android.location.Location;
import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface nl2 extends IInterface {
    @DexIgnore
    void a(dm2 dm2) throws RemoteException;

    @DexIgnore
    void a(f53 f53, pl2 pl2, String str) throws RemoteException;

    @DexIgnore
    void a(om2 om2) throws RemoteException;

    @DexIgnore
    void e(boolean z) throws RemoteException;

    @DexIgnore
    Location zza(String str) throws RemoteException;
}
