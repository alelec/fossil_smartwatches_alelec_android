package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class zv4 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository a;
    @DexIgnore
    private /* final */ /* synthetic */ RecommendedPreset b;

    @DexIgnore
    public /* synthetic */ zv4(PresetRepository presetRepository, RecommendedPreset recommendedPreset) {
        this.a = presetRepository;
        this.b = recommendedPreset;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
