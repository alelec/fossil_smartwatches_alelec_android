package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.tp3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gq3 extends i72 implements rp3, tp3.a {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gq3> CREATOR; // = new hq3();
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public gq3(String str, String str2, String str3) {
        a72.a((Object) str);
        this.a = str;
        a72.a((Object) str2);
        this.b = str2;
        a72.a((Object) str3);
        this.c = str3;
    }

    @DexIgnore
    public final String e() {
        return this.b;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gq3)) {
            return false;
        }
        gq3 gq3 = (gq3) obj;
        return this.a.equals(gq3.a) && y62.a(gq3.b, this.b) && y62.a(gq3.c, this.c);
    }

    @DexIgnore
    public final String getPath() {
        return this.c;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final String toString() {
        int i = 0;
        for (char c2 : this.a.toCharArray()) {
            i += c2;
        }
        String trim = this.a.trim();
        int length = trim.length();
        if (length > 25) {
            String substring = trim.substring(0, 10);
            String substring2 = trim.substring(length - 10, length);
            StringBuilder sb = new StringBuilder(String.valueOf(substring).length() + 16 + String.valueOf(substring2).length());
            sb.append(substring);
            sb.append("...");
            sb.append(substring2);
            sb.append("::");
            sb.append(i);
            trim = sb.toString();
        }
        String str = this.b;
        String str2 = this.c;
        StringBuilder sb2 = new StringBuilder(String.valueOf(trim).length() + 31 + String.valueOf(str).length() + String.valueOf(str2).length());
        sb2.append("Channel{token=");
        sb2.append(trim);
        sb2.append(", nodeId=");
        sb2.append(str);
        sb2.append(", path=");
        sb2.append(str2);
        sb2.append("}");
        return sb2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a, false);
        k72.a(parcel, 3, e(), false);
        k72.a(parcel, 4, getPath(), false);
        k72.a(parcel, a2);
    }
}
