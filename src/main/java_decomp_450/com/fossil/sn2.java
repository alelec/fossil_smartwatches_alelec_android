package com.fossil;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.dynamite.DynamiteModule;
import com.google.android.gms.dynamite.descriptors.com.google.android.gms.measurement.dynamite.ModuleDescriptor;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sn2 {
    @DexIgnore
    public static volatile sn2 i; // = null;
    @DexIgnore
    public static Boolean j; // = null;
    @DexIgnore
    public static String k; // = "allow_remote_dynamite";
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ n92 b;
    @DexIgnore
    public /* final */ ExecutorService c;
    @DexIgnore
    public /* final */ eb3 d;
    @DexIgnore
    public List<Pair<ri3, c>> e;
    @DexIgnore
    public int f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public q43 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class a implements Runnable {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ long b;
        @DexIgnore
        public /* final */ boolean c;

        @DexIgnore
        public a(sn2 sn2) {
            this(true);
        }

        @DexIgnore
        public abstract void a() throws RemoteException;

        @DexIgnore
        public void b() {
        }

        @DexIgnore
        public void run() {
            if (sn2.this.g) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e) {
                sn2.this.a(e, false, this.c);
                b();
            }
        }

        @DexIgnore
        public a(boolean z) {
            this.a = sn2.this.b.b();
            this.b = sn2.this.b.c();
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final void onActivityCreated(Activity activity, Bundle bundle) {
            sn2.this.a(new so2(this, activity, bundle));
        }

        @DexIgnore
        public final void onActivityDestroyed(Activity activity) {
            sn2.this.a(new xo2(this, activity));
        }

        @DexIgnore
        public final void onActivityPaused(Activity activity) {
            sn2.this.a(new to2(this, activity));
        }

        @DexIgnore
        public final void onActivityResumed(Activity activity) {
            sn2.this.a(new uo2(this, activity));
        }

        @DexIgnore
        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            o43 o43 = new o43();
            sn2.this.a(new vo2(this, activity, o43));
            Bundle c = o43.c(50);
            if (c != null) {
                bundle.putAll(c);
            }
        }

        @DexIgnore
        public final void onActivityStarted(Activity activity) {
            sn2.this.a(new ro2(this, activity));
        }

        @DexIgnore
        public final void onActivityStopped(Activity activity) {
            sn2.this.a(new wo2(this, activity));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends mn2 {
        @DexIgnore
        public /* final */ ri3 a;

        @DexIgnore
        public c(ri3 ri3) {
            this.a = ri3;
        }

        @DexIgnore
        @Override // com.fossil.nn2
        public final void a(String str, String str2, Bundle bundle, long j) {
            this.a.a(str, str2, bundle, j);
        }

        @DexIgnore
        @Override // com.fossil.nn2
        public final int zza() {
            return System.identityHashCode(this.a);
        }
    }

    @DexIgnore
    public sn2(Context context, String str, String str2, String str3, Bundle bundle) {
        if (str == null || !c(str2, str3)) {
            this.a = "FA";
        } else {
            this.a = str;
        }
        this.b = q92.d();
        this.c = zv2.a().a(new bo2(this), n43.a);
        this.d = new eb3(this);
        boolean z = false;
        if (!(!e(context) || h())) {
            this.g = true;
            Log.w(this.a, "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection.");
            return;
        }
        if (!c(str2, str3)) {
            if (str2 == null || str3 == null) {
                if ((str2 == null) ^ (str3 == null ? true : z)) {
                    Log.w(this.a, "Specified origin or custom app id is null. Both parameters will be ignored.");
                }
            } else {
                Log.v(this.a, "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy");
            }
        }
        a(new vn2(this, str2, str3, context, bundle));
        Application application = (Application) context.getApplicationContext();
        if (application == null) {
            Log.w(this.a, "Unable to register lifecycle notifications. Application null.");
        } else {
            application.registerActivityLifecycleCallbacks(new b());
        }
    }

    @DexIgnore
    public static sn2 a(Context context) {
        return a(context, (String) null, (String) null, (String) null, (Bundle) null);
    }

    @DexIgnore
    public static boolean c(String str, String str2) {
        return (str2 == null || str == null || h()) ? false : true;
    }

    @DexIgnore
    public static boolean e(Context context) {
        try {
            return xj3.a(context, "google_app_id") != null;
        } catch (IllegalStateException unused) {
        }
    }

    @DexIgnore
    public static int f(Context context) {
        return DynamiteModule.b(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static int g(Context context) {
        return DynamiteModule.a(context, ModuleDescriptor.MODULE_ID);
    }

    @DexIgnore
    public static boolean h() {
        try {
            Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    @DexIgnore
    public final void b(String str, String str2, Bundle bundle) {
        a(new xn2(this, str, str2, bundle));
    }

    @DexIgnore
    public final long d() {
        o43 o43 = new o43();
        a(new fo2(this, o43));
        Long l = (Long) o43.a(o43.c(500), Long.class);
        if (l != null) {
            return l.longValue();
        }
        long nextLong = new Random(System.nanoTime() ^ this.b.b()).nextLong();
        int i2 = this.f + 1;
        this.f = i2;
        return nextLong + ((long) i2);
    }

    @DexIgnore
    public static sn2 a(Context context, String str, String str2, String str3, Bundle bundle) {
        a72.a(context);
        if (i == null) {
            synchronized (sn2.class) {
                if (i == null) {
                    i = new sn2(context, str, str2, str3, bundle);
                }
            }
        }
        return i;
    }

    @DexIgnore
    public static void h(Context context) {
        synchronized (sn2.class) {
            try {
                if (j == null) {
                    if (a(context, "app_measurement_internal_disable_startup_flags")) {
                        j = false;
                        return;
                    }
                    SharedPreferences sharedPreferences = context.getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
                    j = Boolean.valueOf(sharedPreferences.getBoolean(k, false));
                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.remove(k);
                    edit.apply();
                }
            } catch (Exception e2) {
                Log.e("FA", "Exception reading flag from SharedPreferences.", e2);
                j = false;
            }
        }
    }

    @DexIgnore
    public final List<Bundle> b(String str, String str2) {
        o43 o43 = new o43();
        a(new wn2(this, str, str2, o43));
        List<Bundle> list = (List) o43.a(o43.c(5000), List.class);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public final void c(String str) {
        a(new eo2(this, str));
    }

    @DexIgnore
    public final String e() {
        o43 o43 = new o43();
        a(new io2(this, o43));
        return o43.b(500);
    }

    @DexIgnore
    public final String f() {
        o43 o43 = new o43();
        a(new ho2(this, o43));
        return o43.b(500);
    }

    @DexIgnore
    public final String c() {
        o43 o43 = new o43();
        a(new go2(this, o43));
        return o43.b(50);
    }

    @DexIgnore
    public final void b(String str) {
        a(new co2(this, str));
    }

    @DexIgnore
    public final int d(String str) {
        o43 o43 = new o43();
        a(new mo2(this, str, o43));
        Integer num = (Integer) o43.a(o43.c(ButtonService.CONNECT_TIMEOUT), Integer.class);
        if (num == null) {
            return 25;
        }
        return num.intValue();
    }

    @DexIgnore
    public final String b() {
        o43 o43 = new o43();
        a(new do2(this, o43));
        return o43.b(500);
    }

    @DexIgnore
    public final eb3 a() {
        return this.d;
    }

    @DexIgnore
    public final void a(a aVar) {
        this.c.execute(aVar);
    }

    @DexIgnore
    public final void b(boolean z) {
        a(new no2(this, z));
    }

    @DexIgnore
    public final q43 a(Context context, boolean z) {
        DynamiteModule.b bVar;
        if (z) {
            try {
                bVar = DynamiteModule.l;
            } catch (DynamiteModule.a e2) {
                a((Exception) e2, true, false);
                return null;
            }
        } else {
            bVar = DynamiteModule.j;
        }
        return p43.asInterface(DynamiteModule.a(context, bVar, ModuleDescriptor.MODULE_ID).a("com.google.android.gms.measurement.internal.AppMeasurementDynamiteService"));
    }

    @DexIgnore
    public final void a(Exception exc, boolean z, boolean z2) {
        this.g |= z;
        if (z) {
            Log.w(this.a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            a(5, "Error with data collection. Data lost.", exc, (Object) null, (Object) null);
        }
        Log.w(this.a, "Error with data collection. Data lost.", exc);
    }

    @DexIgnore
    public final void a(ri3 ri3) {
        a72.a(ri3);
        a(new oo2(this, ri3));
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        a(null, str, bundle, false, true, null);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle) {
        a(str, str2, bundle, true, true, null);
    }

    @DexIgnore
    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, Long l) {
        a(new qo2(this, l, str, str2, bundle, z, z2));
    }

    @DexIgnore
    public final void a(String str, String str2) {
        a((String) null, str, (Object) str2, false);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj) {
        a(str, str2, obj, true);
    }

    @DexIgnore
    public final void a(String str, String str2, Object obj, boolean z) {
        a(new po2(this, str, str2, obj, z));
    }

    @DexIgnore
    public final void a(Bundle bundle) {
        a(new un2(this, bundle));
    }

    @DexIgnore
    public final void a(String str) {
        a(new zn2(this, str));
    }

    @DexIgnore
    public final void a(Activity activity, String str, String str2) {
        a(new yn2(this, activity, str, str2));
    }

    @DexIgnore
    public final void a(boolean z) {
        a(new ao2(this, z));
    }

    @DexIgnore
    public final Map<String, Object> a(String str, String str2, boolean z) {
        o43 o43 = new o43();
        a(new lo2(this, str, str2, z, o43));
        Bundle c2 = o43.c(5000);
        if (c2 == null || c2.size() == 0) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(c2.size());
        for (String str3 : c2.keySet()) {
            Object obj = c2.get(str3);
            if ((obj instanceof Double) || (obj instanceof Long) || (obj instanceof String)) {
                hashMap.put(str3, obj);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final void a(int i2, String str, Object obj, Object obj2, Object obj3) {
        a(new ko2(this, false, 5, str, obj, null, null));
    }

    @DexIgnore
    public static boolean a(Context context, String str) {
        a72.b(str);
        try {
            ApplicationInfo a2 = ja2.b(context).a(context.getPackageName(), 128);
            if (a2 != null) {
                if (a2.metaData != null) {
                    return a2.metaData.getBoolean(str);
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }
}
