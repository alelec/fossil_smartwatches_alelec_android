package com.fossil;

import com.fossil.bw2;
import java.io.IOException;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ov2 extends pv2<bw2.e> {
    @DexIgnore
    @Override // com.fossil.pv2
    public final boolean a(jx2 jx2) {
        return jx2 instanceof bw2.b;
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final qv2<bw2.e> b(Object obj) {
        return ((bw2.b) obj).zza();
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final void c(Object obj) {
        a(obj).a();
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final qv2<bw2.e> a(Object obj) {
        return ((bw2.b) obj).zzc;
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final int a(Map.Entry<?, ?> entry) {
        bw2.e eVar = (bw2.e) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final void a(oz2 oz2, Map.Entry<?, ?> entry) throws IOException {
        bw2.e eVar = (bw2.e) entry.getKey();
        throw new NoSuchMethodError();
    }

    @DexIgnore
    @Override // com.fossil.pv2
    public final Object a(nv2 nv2, jx2 jx2, int i) {
        return nv2.a(jx2, i);
    }
}
