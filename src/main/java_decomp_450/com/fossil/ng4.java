package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ng4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof ng4) {
            ng4 ng4 = (ng4) obj;
            if (this.a == ng4.a && this.b == ng4.b) {
                return true;
            }
            return false;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a * 32713) + this.b;
    }

    @DexIgnore
    public String toString() {
        return this.a + "x" + this.b;
    }
}
