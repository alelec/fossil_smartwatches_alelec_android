package com.fossil;

import android.graphics.Rect;
import android.view.ViewGroup;
import androidx.transition.Transition;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lj extends zk {
    @DexIgnore
    public float b; // = 3.0f;

    @DexIgnore
    @Override // com.fossil.fk
    public long a(ViewGroup viewGroup, Transition transition, hk hkVar, hk hkVar2) {
        int i;
        int i2;
        int i3;
        if (hkVar == null && hkVar2 == null) {
            return 0;
        }
        if (hkVar2 == null || b(hkVar) == 0) {
            i = -1;
        } else {
            hkVar = hkVar2;
            i = 1;
        }
        int c = c(hkVar);
        int d = d(hkVar);
        Rect h = transition.h();
        if (h != null) {
            i3 = h.centerX();
            i2 = h.centerY();
        } else {
            int[] iArr = new int[2];
            viewGroup.getLocationOnScreen(iArr);
            int round = Math.round(((float) (iArr[0] + (viewGroup.getWidth() / 2))) + viewGroup.getTranslationX());
            i2 = Math.round(((float) (iArr[1] + (viewGroup.getHeight() / 2))) + viewGroup.getTranslationY());
            i3 = round;
        }
        float a = a((float) c, (float) d, (float) i3, (float) i2) / a((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) viewGroup.getWidth(), (float) viewGroup.getHeight());
        long f = transition.f();
        if (f < 0) {
            f = 300;
        }
        return (long) Math.round((((float) (f * ((long) i))) / this.b) * a);
    }

    @DexIgnore
    public static float a(float f, float f2, float f3, float f4) {
        float f5 = f3 - f;
        float f6 = f4 - f2;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }
}
