package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.DeviceRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ff5 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public /* final */ DeviceRepository a;
    @DexIgnore
    public /* final */ PortfolioApp b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ff5.class.getSimpleName();
        ee7.a((Object) simpleName, "WatchParamHelper::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public ff5(DeviceRepository deviceRepository, PortfolioApp portfolioApp) {
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(portfolioApp, "mApp");
        this.a = deviceRepository;
        this.b = portfolioApp;
    }

    @DexIgnore
    public final Object a(String str, float f, WatchParameterResponse watchParameterResponse, fb7<? super i97> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local.d(str2, "handleSuccessResponseWatchParam, response=" + watchParameterResponse + ", currentWPVersion=" + f);
        WatchParam watchParamModel = watchParameterResponse.toWatchParamModel(str);
        this.a.saveWatchParamModel(watchParamModel);
        String versionMajor = watchParamModel.getVersionMajor();
        if (versionMajor != null) {
            int parseInt = Integer.parseInt(versionMajor);
            String versionMinor = watchParamModel.getVersionMinor();
            if (versionMinor != null) {
                if (f < a(parseInt, Integer.parseInt(versionMinor))) {
                    FLogger.INSTANCE.getLocal().d(c, "Need to update newer WP version from response");
                    String data = watchParamModel.getData();
                    PortfolioApp portfolioApp = this.b;
                    if (data != null) {
                        portfolioApp.a(str, true, new WatchParamsFileMapping(data));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "No need to update WP version in device, it's the latest one");
                    this.b.a(str, true, (WatchParamsFileMapping) null);
                }
                return i97.a;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final Object a(String str, float f, fb7<? super i97> fb7) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = c;
        local.d(str2, "handleFailureResponseWatchParam, currentWPVersion=" + f);
        WatchParam watchParamBySerialId = this.a.getWatchParamBySerialId(str);
        if (watchParamBySerialId != null) {
            String versionMajor = watchParamBySerialId.getVersionMajor();
            if (versionMajor != null) {
                int parseInt = Integer.parseInt(versionMajor);
                String versionMinor = watchParamBySerialId.getVersionMinor();
                if (versionMinor == null) {
                    ee7.a();
                    throw null;
                } else if (f < a(parseInt, Integer.parseInt(versionMinor))) {
                    FLogger.INSTANCE.getLocal().d(c, "Newer version is available in database, set to device");
                    String data = watchParamBySerialId.getData();
                    PortfolioApp portfolioApp = this.b;
                    if (data != null) {
                        portfolioApp.a(str, true, new WatchParamsFileMapping(data));
                    } else {
                        ee7.a();
                        throw null;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(c, "The saved version in database is older than the current one, skip it");
                    this.b.a(str, true, (WatchParamsFileMapping) null);
                }
            } else {
                ee7.a();
                throw null;
            }
        } else if (f != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database, but keep going because the current version is not empty");
            this.b.a(str, true, (WatchParamsFileMapping) null);
        } else {
            FLogger.INSTANCE.getLocal().d(c, "Can't get WP version from database and the current version is empty -> notify failed");
            this.b.a(str, false, (WatchParamsFileMapping) null);
        }
        return i97.a;
    }

    @DexIgnore
    public final float a(int i, int i2) {
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append('.');
        sb.append(i2);
        return Float.parseFloat(sb.toString());
    }
}
