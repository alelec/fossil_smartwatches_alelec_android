package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y03 implements z03 {
    @DexIgnore
    public static /* final */ tq2<Boolean> a; // = new dr2(uq2.a("com.google.android.gms.measurement")).a("measurement.client.firebase_feature_rollout.v1.enable", true);

    @DexIgnore
    @Override // com.fossil.z03
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    @Override // com.fossil.z03
    public final boolean zzb() {
        return a.b().booleanValue();
    }
}
