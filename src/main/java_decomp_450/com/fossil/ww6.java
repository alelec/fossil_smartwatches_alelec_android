package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww6 implements MembersInjector<uw6> {
    @DexIgnore
    public static void a(uw6 uw6, DeviceRepository deviceRepository) {
        uw6.h = deviceRepository;
    }

    @DexIgnore
    public static void a(uw6 uw6, NotificationSettingsDatabase notificationSettingsDatabase) {
        uw6.i = notificationSettingsDatabase;
    }
}
