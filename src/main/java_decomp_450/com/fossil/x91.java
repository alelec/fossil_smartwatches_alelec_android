package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x91 extends k60 implements Parcelable {
    @DexIgnore
    public static /* final */ c81 CREATOR; // = new c81(null);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public x91(int i, int i2, int i3, int i4) {
        boolean z = false;
        if (true == (i < 6)) {
            i = 6;
        } else {
            if (true == (i > 3200)) {
                i = 3200;
            }
        }
        this.a = i;
        this.b = (i > i2 || 3200 < i2) ? this.a : i2;
        if (true == (i3 < 0)) {
            i3 = 0;
        } else {
            if (true == (i3 > 499)) {
                i3 = 499;
            }
        }
        this.c = i3;
        if (true == (i4 < 10)) {
            i4 = 10;
        } else {
            if (true == (i4 > 3200 ? true : z)) {
                i4 = 3200;
            }
        }
        this.d = i4;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.M, Integer.valueOf(this.a)), r51.L, Integer.valueOf(this.b)), r51.N, Integer.valueOf(this.c)), r51.O, Integer.valueOf(this.d));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeInt(this.d);
    }
}
