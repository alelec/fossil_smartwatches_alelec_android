package com.fossil;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z96 extends w96 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e;
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public /* final */ MutableLiveData<Date> i;
    @DexIgnore
    public /* final */ LiveData<qx6<List<ActivitySummary>>> j;
    @DexIgnore
    public /* final */ LiveData<qx6<List<ActivitySample>>> k;
    @DexIgnore
    public /* final */ LiveData<qx6<List<WorkoutSession>>> l;
    @DexIgnore
    public /* final */ x96 m;
    @DexIgnore
    public /* final */ SummariesRepository n;
    @DexIgnore
    public /* final */ ActivitiesRepository o;
    @DexIgnore
    public /* final */ WorkoutSessionRepository p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$mActivitySamples$1$1", f = "CaloriesOverviewDayPresenter.kt", l = {53, 53}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySample>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(b bVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = bVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySample>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    ActivitiesRepository b = this.this$0.a.o;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = b.getActivityList(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public b(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySample>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewDayPresenter", "mActivitySamples onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$mActivitySummaries$1$1", f = "CaloriesOverviewDayPresenter.kt", l = {47, 47}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<ActivitySummary>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ c this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(c cVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = cVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<ActivitySummary>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    SummariesRepository g = this.this$0.a.n;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = g.getSummaries(date, date2, false, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public c(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<ActivitySummary>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewDayPresenter", "mActivitySummaries onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements t3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$mWorkoutSessions$1$1", f = "CaloriesOverviewDayPresenter.kt", l = {59, 59}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<vd<qx6<? extends List<WorkoutSession>>>, fb7<? super i97>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public vd p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Date date, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, this.$it, fb7);
                aVar.p$ = (vd) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(vd<qx6<? extends List<WorkoutSession>>> vdVar, fb7<? super i97> fb7) {
                return ((a) create(vdVar, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                vd vdVar;
                vd vdVar2;
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    vdVar2 = this.p$;
                    WorkoutSessionRepository j = this.this$0.a.p;
                    Date date = this.$it;
                    ee7.a((Object) date, "it");
                    Date date2 = this.$it;
                    ee7.a((Object) date2, "it");
                    this.L$0 = vdVar2;
                    this.L$1 = vdVar2;
                    this.label = 1;
                    obj = j.getWorkoutSessions(date, date2, true, this);
                    if (obj == a) {
                        return a;
                    }
                    vdVar = vdVar2;
                } else if (i == 1) {
                    vdVar2 = (vd) this.L$1;
                    vdVar = (vd) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    vd vdVar3 = (vd) this.L$0;
                    t87.a(obj);
                    return i97.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.L$0 = vdVar;
                this.label = 2;
                if (vdVar2.a((LiveData) obj, (fb7<? super rj7>) this) == a) {
                    return a;
                }
                return i97.a;
            }
        }

        @DexIgnore
        public d(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<qx6<List<WorkoutSession>>> apply(Date date) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewDayPresenter", "mWorkoutSessions onDateChange " + date);
            return ed.a(null, 0, new a(this, date, null), 3, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1", f = "CaloriesOverviewDayPresenter.kt", l = {127, 129, 130}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ z96 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1$activitySummary$1", f = "CaloriesOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ActivitySummary>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ActivitySummary> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            /* JADX DEBUG: Multi-variable search result rejected for r0v2, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary */
            /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary */
            /* JADX DEBUG: Multi-variable search result rejected for r0v5, resolved type: com.portfolio.platform.data.model.room.fitness.ActivitySummary */
            /* JADX WARN: Multi-variable type inference failed */
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                List list;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    qx6 qx6 = (qx6) this.this$0.this$0.j.a();
                    Object obj2 = null;
                    if (qx6 == null || (list = (List) qx6.c()) == null) {
                        return null;
                    }
                    Iterator it = list.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Object next = it.next();
                        if (pb7.a(zd5.d(((ActivitySummary) next).getDate(), this.this$0.this$0.f)).booleanValue()) {
                            obj2 = next;
                            break;
                        }
                    }
                    return (ActivitySummary) obj2;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1$maxValue$1", f = "CaloriesOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class b extends zb7 implements kd7<yi7, fb7<? super Integer>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ ArrayList $data;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(ArrayList arrayList, fb7 fb7) {
                super(2, fb7);
                this.$data = arrayList;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                b bVar = new b(this.$data, fb7);
                bVar.p$ = (yi7) obj;
                return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super Integer> fb7) {
                return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object obj2;
                ArrayList<ArrayList<BarChart.b>> c;
                ArrayList<BarChart.b> arrayList;
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    Iterator it = this.$data.iterator();
                    int i = 0;
                    if (!it.hasNext()) {
                        obj2 = null;
                    } else {
                        obj2 = it.next();
                        if (it.hasNext()) {
                            ArrayList<BarChart.b> arrayList2 = ((BarChart.a) obj2).c().get(0);
                            ee7.a((Object) arrayList2, "it.mListOfBarPoints[0]");
                            Iterator<T> it2 = arrayList2.iterator();
                            int i2 = 0;
                            while (it2.hasNext()) {
                                i2 += pb7.a(it2.next().e()).intValue();
                            }
                            Integer a = pb7.a(i2);
                            do {
                                Object next = it.next();
                                ArrayList<BarChart.b> arrayList3 = ((BarChart.a) next).c().get(0);
                                ee7.a((Object) arrayList3, "it.mListOfBarPoints[0]");
                                Iterator<T> it3 = arrayList3.iterator();
                                int i3 = 0;
                                while (it3.hasNext()) {
                                    i3 += pb7.a(it3.next().e()).intValue();
                                }
                                Integer a2 = pb7.a(i3);
                                if (a.compareTo((Object) a2) < 0) {
                                    obj2 = next;
                                    a = a2;
                                }
                            } while (it.hasNext());
                        }
                    }
                    BarChart.a aVar = (BarChart.a) obj2;
                    if (aVar == null || (c = aVar.c()) == null || (arrayList = c.get(0)) == null) {
                        return null;
                    }
                    Iterator<T> it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        i += pb7.a(it4.next().e()).intValue();
                    }
                    return pb7.a(i);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayPresenter$showDetailChart$1$pair$1", f = "CaloriesOverviewDayPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class c extends zb7 implements kd7<yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(e eVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = eVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                c cVar = new c(this.this$0, fb7);
                cVar.p$ = (yi7) obj;
                return cVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super r87<? extends ArrayList<BarChart.a>, ? extends ArrayList<String>>> fb7) {
                return ((c) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    tg6 tg6 = tg6.a;
                    Date e = this.this$0.this$0.f;
                    List<ActivitySample> list = null;
                    if (e != null) {
                        qx6 qx6 = (qx6) this.this$0.this$0.k.a();
                        if (qx6 != null) {
                            list = (List) qx6.c();
                        }
                        return tg6.a(e, list, 2);
                    }
                    ee7.a();
                    throw null;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(z96 z96, fb7 fb7) {
            super(2, fb7);
            this.this$0 = z96;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00a7 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00a8  */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x00bf  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r9.label
                r2 = 3
                r3 = 2
                r4 = 1
                r5 = 0
                if (r1 == 0) goto L_0x0048
                if (r1 == r4) goto L_0x003f
                if (r1 == r3) goto L_0x002f
                if (r1 != r2) goto L_0x0027
                java.lang.Object r0 = r9.L$3
                java.lang.Integer r0 = (java.lang.Integer) r0
                java.lang.Object r1 = r9.L$2
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                java.lang.Object r2 = r9.L$1
                com.fossil.r87 r2 = (com.fossil.r87) r2
                java.lang.Object r3 = r9.L$0
                com.fossil.yi7 r3 = (com.fossil.yi7) r3
                com.fossil.t87.a(r10)
                goto L_0x00ab
            L_0x0027:
                java.lang.IllegalStateException r10 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r10.<init>(r0)
                throw r10
            L_0x002f:
                java.lang.Object r1 = r9.L$2
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                java.lang.Object r3 = r9.L$1
                com.fossil.r87 r3 = (com.fossil.r87) r3
                java.lang.Object r4 = r9.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r10)
                goto L_0x008a
            L_0x003f:
                java.lang.Object r1 = r9.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r10)
                r4 = r1
                goto L_0x0065
            L_0x0048:
                com.fossil.t87.a(r10)
                com.fossil.yi7 r10 = r9.p$
                com.fossil.z96 r1 = r9.this$0
                com.fossil.ti7 r1 = r1.b()
                com.fossil.z96$e$c r6 = new com.fossil.z96$e$c
                r6.<init>(r9, r5)
                r9.L$0 = r10
                r9.label = r4
                java.lang.Object r1 = com.fossil.vh7.a(r1, r6, r9)
                if (r1 != r0) goto L_0x0063
                return r0
            L_0x0063:
                r4 = r10
                r10 = r1
            L_0x0065:
                com.fossil.r87 r10 = (com.fossil.r87) r10
                java.lang.Object r1 = r10.getFirst()
                java.util.ArrayList r1 = (java.util.ArrayList) r1
                com.fossil.z96 r6 = r9.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.z96$e$b r7 = new com.fossil.z96$e$b
                r7.<init>(r1, r5)
                r9.L$0 = r4
                r9.L$1 = r10
                r9.L$2 = r1
                r9.label = r3
                java.lang.Object r3 = com.fossil.vh7.a(r6, r7, r9)
                if (r3 != r0) goto L_0x0087
                return r0
            L_0x0087:
                r8 = r3
                r3 = r10
                r10 = r8
            L_0x008a:
                java.lang.Integer r10 = (java.lang.Integer) r10
                com.fossil.z96 r6 = r9.this$0
                com.fossil.ti7 r6 = r6.b()
                com.fossil.z96$e$a r7 = new com.fossil.z96$e$a
                r7.<init>(r9, r5)
                r9.L$0 = r4
                r9.L$1 = r3
                r9.L$2 = r1
                r9.L$3 = r10
                r9.label = r2
                java.lang.Object r2 = com.fossil.vh7.a(r6, r7, r9)
                if (r2 != r0) goto L_0x00a8
                return r0
            L_0x00a8:
                r0 = r10
                r10 = r2
                r2 = r3
            L_0x00ab:
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r10 = (com.portfolio.platform.data.model.room.fitness.ActivitySummary) r10
                com.fossil.ge5$a r3 = com.fossil.ge5.c
                com.fossil.gb5 r4 = com.fossil.gb5.CALORIES
                int r10 = r3.a(r10, r4)
                com.fossil.z96 r3 = r9.this$0
                com.fossil.x96 r3 = r3.m
                com.portfolio.platform.ui.view.chart.base.BarChart$c r4 = new com.portfolio.platform.ui.view.chart.base.BarChart$c
                if (r0 == 0) goto L_0x00c4
                int r0 = r0.intValue()
                goto L_0x00c5
            L_0x00c4:
                r0 = 0
            L_0x00c5:
                int r5 = r10 / 16
                int r0 = java.lang.Math.max(r0, r5)
                r4.<init>(r0, r10, r1)
                java.lang.Object r10 = r2.getSecond()
                java.util.ArrayList r10 = (java.util.ArrayList) r10
                r3.b(r4, r10)
                com.fossil.i97 r10 = com.fossil.i97.a
                return r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.z96.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f<T> implements zd<qx6<? extends List<ActivitySummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore
        public f(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<ActivitySummary>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySummaries -- activitySummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    ik7 unused = this.a.j();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements zd<qx6<? extends List<ActivitySample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore
        public g(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<ActivitySample>> qx6) {
            lb5 a2 = qx6.a();
            List list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mActivitySamples -- activitySamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 != lb5.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    ik7 unused = this.a.j();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements zd<qx6<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ z96 a;

        @DexIgnore
        public h(z96 z96) {
            this.a = z96;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(qx6<? extends List<WorkoutSession>> qx6) {
            lb5 a2 = qx6.a();
            List<WorkoutSession> list = (List) qx6.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("CaloriesOverviewDayPresenter", sb.toString());
            if (a2 == lb5.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.m.a(false, new ArrayList());
            } else {
                this.a.m.a(true, list);
            }
        }
    }

    /*
    static {
        new a(null);
    }
    */

    @DexIgnore
    public z96(x96 x96, SummariesRepository summariesRepository, ActivitiesRepository activitiesRepository, WorkoutSessionRepository workoutSessionRepository, PortfolioApp portfolioApp) {
        ee7.b(x96, "mView");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(activitiesRepository, "mActivitiesRepository");
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        ee7.b(portfolioApp, "mApp");
        this.m = x96;
        this.n = summariesRepository;
        this.o = activitiesRepository;
        this.p = workoutSessionRepository;
        this.e = FossilDeviceSerialPatternUtil.getDeviceBySerial(portfolioApp.c());
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.i = mutableLiveData;
        LiveData<qx6<List<ActivitySummary>>> b2 = ge.b(mutableLiveData, new c(this));
        ee7.a((Object) b2, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.j = b2;
        LiveData<qx6<List<ActivitySample>>> b3 = ge.b(this.i, new b(this));
        ee7.a((Object) b3, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.k = b3;
        LiveData<qx6<List<WorkoutSession>>> b4 = ge.b(this.i, new d(this));
        ee7.a((Object) b4, "Transformations.switchMa\u2026t, true))\n        }\n    }");
        this.l = b4;
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void f() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.f;
        if (date == null || !zd5.w(date).booleanValue()) {
            this.g = false;
            this.h = false;
            Date date2 = new Date();
            this.f = date2;
            this.i.a(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("CaloriesOverviewDayPresenter", "loadData - mDate=" + this.f);
        LiveData<qx6<List<ActivitySummary>>> liveData = this.j;
        x96 x96 = this.m;
        if (x96 != null) {
            liveData.a((y96) x96, new f(this));
            this.k.a((LifecycleOwner) this.m, new g(this));
            this.l.a((LifecycleOwner) this.m, new h(this));
            return;
        }
        throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayFragment");
    }

    @DexIgnore
    @Override // com.fossil.cl4
    public void g() {
        FLogger.INSTANCE.getLocal().d("CaloriesOverviewDayPresenter", "stop");
        try {
            LiveData<qx6<List<ActivitySample>>> liveData = this.k;
            x96 x96 = this.m;
            if (x96 != null) {
                liveData.a((y96) x96);
                this.j.a((LifecycleOwner) this.m);
                this.l.a((LifecycleOwner) this.m);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.calories.overview.CaloriesOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("CaloriesOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.w96
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        ee7.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        this.m.a(this);
    }

    @DexIgnore
    public final ik7 j() {
        return xh7.b(e(), null, null, new e(this, null), 3, null);
    }
}
