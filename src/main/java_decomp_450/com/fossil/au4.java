package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements zd<T> {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public T b;
        @DexIgnore
        public /* final */ /* synthetic */ xd c;

        @DexIgnore
        public a(xd xdVar) {
            this.c = xdVar;
        }

        @DexIgnore
        @Override // com.fossil.zd
        public void onChanged(T t) {
            if (!this.a) {
                this.a = true;
                this.b = t;
                this.c.a((Object) t);
            } else if ((t == null && this.b != null) || (!ee7.a((Object) t, (Object) this.b))) {
                this.b = t;
                this.c.a((Object) t);
            }
        }
    }

    @DexIgnore
    public static final <T> LiveData<T> a(LiveData<T> liveData) {
        ee7.b(liveData, "$this$distinct");
        xd xdVar = new xd();
        xdVar.a(liveData, new a(xdVar));
        return xdVar;
    }
}
