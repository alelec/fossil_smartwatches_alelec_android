package com.fossil;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ch {
    @DexIgnore
    public static int a(RecyclerView.State state, zg zgVar, View view, View view2, RecyclerView.m mVar, boolean z, boolean z2) {
        int i;
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        int min = Math.min(mVar.l(view), mVar.l(view2));
        int max = Math.max(mVar.l(view), mVar.l(view2));
        if (z2) {
            i = Math.max(0, (state.a() - max) - 1);
        } else {
            i = Math.max(0, min);
        }
        if (!z) {
            return i;
        }
        return Math.round((((float) i) * (((float) Math.abs(zgVar.a(view2) - zgVar.d(view))) / ((float) (Math.abs(mVar.l(view) - mVar.l(view2)) + 1)))) + ((float) (zgVar.f() - zgVar.d(view))));
    }

    @DexIgnore
    public static int b(RecyclerView.State state, zg zgVar, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return state.a();
        }
        return (int) ((((float) (zgVar.a(view2) - zgVar.d(view))) / ((float) (Math.abs(mVar.l(view) - mVar.l(view2)) + 1))) * ((float) state.a()));
    }

    @DexIgnore
    public static int a(RecyclerView.State state, zg zgVar, View view, View view2, RecyclerView.m mVar, boolean z) {
        if (mVar.e() == 0 || state.a() == 0 || view == null || view2 == null) {
            return 0;
        }
        if (!z) {
            return Math.abs(mVar.l(view) - mVar.l(view2)) + 1;
        }
        return Math.min(zgVar.g(), zgVar.a(view2) - zgVar.d(view));
    }
}
