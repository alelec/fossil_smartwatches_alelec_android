package com.fossil;

import android.text.TextUtils;
import com.fossil.wearables.fsl.contact.Contact;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt5 implements Serializable {
    @DexIgnore
    public Contact contact;
    @DexIgnore
    public int currentHandGroup;
    @DexIgnore
    public boolean isAdded;
    @DexIgnore
    public boolean isFavorites;
    @DexIgnore
    public boolean mHasPhoneNumber;
    @DexIgnore
    public String phoneNumber;
    @DexIgnore
    public String sortKey;

    @DexIgnore
    public bt5(Contact contact2, String str) {
        ee7.b(str, "sortKey");
        this.contact = contact2;
        this.sortKey = str;
    }

    @DexIgnore
    public final char a(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    public final Contact getContact() {
        return this.contact;
    }

    @DexIgnore
    public final int getCurrentHandGroup() {
        return this.currentHandGroup;
    }

    @DexIgnore
    public final String getPhoneNumber() {
        return this.phoneNumber;
    }

    @DexIgnore
    public final String getSortKey() {
        return this.sortKey;
    }

    @DexIgnore
    public final char getSortKeyInRightFormat() {
        return a(this.sortKey);
    }

    @DexIgnore
    public final boolean hasPhoneNumber() {
        return this.mHasPhoneNumber;
    }

    @DexIgnore
    public final boolean isAdded() {
        return this.isAdded;
    }

    @DexIgnore
    public final boolean isFavorites() {
        return this.isFavorites;
    }

    @DexIgnore
    public final void setAdded(boolean z) {
        this.isAdded = z;
    }

    @DexIgnore
    public final void setContact(Contact contact2) {
        this.contact = contact2;
    }

    @DexIgnore
    public final void setCurrentHandGroup(int i) {
        this.currentHandGroup = i;
    }

    @DexIgnore
    public final void setFavorites(boolean z) {
        this.isFavorites = z;
    }

    @DexIgnore
    public final void setHasPhoneNumber(boolean z) {
        this.mHasPhoneNumber = z;
    }

    @DexIgnore
    public final void setPhoneNumber(String str) {
        this.phoneNumber = str;
    }

    @DexIgnore
    public final void setSortKey(String str) {
        ee7.b(str, "<set-?>");
        this.sortKey = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ bt5(Contact contact2, String str, int i, zd7 zd7) {
        this(contact2, (i & 2) != 0 ? "" : str);
    }
}
