package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nv3 extends hb {
    @DexIgnore
    public static /* final */ Parcelable.Creator<nv3> CREATOR; // = new a();
    @DexIgnore
    public /* final */ SimpleArrayMap<String, Bundle> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.ClassLoaderCreator<nv3> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public nv3[] newArray(int i) {
            return new nv3[i];
        }

        @DexIgnore
        @Override // android.os.Parcelable.ClassLoaderCreator
        public nv3 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new nv3(parcel, classLoader, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public nv3 createFromParcel(Parcel parcel) {
            return new nv3(parcel, null, null);
        }
    }

    @DexIgnore
    public /* synthetic */ nv3(Parcel parcel, ClassLoader classLoader, a aVar) {
        this(parcel, classLoader);
    }

    @DexIgnore
    public String toString() {
        return "ExtendableSavedState{" + Integer.toHexString(System.identityHashCode(this)) + " states=" + this.c + "}";
    }

    @DexIgnore
    @Override // com.fossil.hb
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        int size = this.c.size();
        parcel.writeInt(size);
        String[] strArr = new String[size];
        Bundle[] bundleArr = new Bundle[size];
        for (int i2 = 0; i2 < size; i2++) {
            strArr[i2] = this.c.c(i2);
            bundleArr[i2] = this.c.e(i2);
        }
        parcel.writeStringArray(strArr);
        parcel.writeTypedArray(bundleArr, 0);
    }

    @DexIgnore
    public nv3(Parcelable parcelable) {
        super(parcelable);
        this.c = new SimpleArrayMap<>();
    }

    @DexIgnore
    public nv3(Parcel parcel, ClassLoader classLoader) {
        super(parcel, classLoader);
        int readInt = parcel.readInt();
        String[] strArr = new String[readInt];
        parcel.readStringArray(strArr);
        Bundle[] bundleArr = new Bundle[readInt];
        parcel.readTypedArray(bundleArr, Bundle.CREATOR);
        this.c = new SimpleArrayMap<>(readInt);
        for (int i = 0; i < readInt; i++) {
            this.c.put(strArr[i], bundleArr[i]);
        }
    }
}
