package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk7 extends yh7 {
    @DexIgnore
    public /* final */ bm7 a;

    @DexIgnore
    public yk7(bm7 bm7) {
        this.a = bm7;
    }

    @DexIgnore
    @Override // com.fossil.zh7
    public void a(Throwable th) {
        this.a.g();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Throwable th) {
        a(th);
        return i97.a;
    }

    @DexIgnore
    public String toString() {
        return "RemoveOnCancel[" + this.a + ']';
    }
}
