package com.fossil;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp4 extends he {
    @DexIgnore
    public static /* final */ String t;
    @DexIgnore
    public /* final */ MutableLiveData<qn4> a; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<dn4>> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<c> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<b> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<v87<Boolean, ServerError, qn4>> m; // = new MutableLiveData<>();
    @DexIgnore
    public qn4 n;
    @DexIgnore
    public qn4 o;
    @DexIgnore
    public List<dn4> p; // = ot4.a.g();
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public /* final */ ro4 s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ int b;

        @DexIgnore
        public b(boolean z, int i) {
            this.a = z;
            this.b = i;
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return this.a == bVar.a && this.b == bVar.b;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            return (i * 31) + this.b;
        }

        @DexIgnore
        public String toString() {
            return "NameRule(isValid=" + this.a + ", length=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ qn4 a;

        @DexIgnore
        public c(qn4 qn4) {
            ee7.b(qn4, "challengeDraft");
            this.a = qn4;
        }

        @DexIgnore
        public final qn4 a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && ee7.a(this.a, ((c) obj).a);
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            qn4 qn4 = this.a;
            if (qn4 != null) {
                return qn4.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "NextState(challengeDraft=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1", f = "BCCreateChallengeInputViewModel.kt", l = {259}, m = "invokeSuspend")
    public static final class d extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ kp4 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.buddy_challenge.screens.create_input.BCCreateChallengeInputViewModel$onDone$1$result$1", f = "BCCreateChallengeInputViewModel.kt", l = {261, 263}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super ko4<mn4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = dVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super ko4<mn4>> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                Object a = nb7.a();
                int i = this.label;
                if (i == 0) {
                    t87.a(obj);
                    yi7 yi7 = this.p$;
                    String str = null;
                    if (ee7.a((Object) kp4.b(this.this$0.this$0).j(), (Object) "activity_best_result")) {
                        ro4 j = this.this$0.this$0.j();
                        qn4 a2 = this.this$0.this$0.n;
                        String c = a2 != null ? a2.c() : null;
                        if (c != null) {
                            qn4 a3 = this.this$0.this$0.n;
                            String e = a3 != null ? a3.e() : null;
                            qn4 a4 = this.this$0.this$0.n;
                            if (a4 != null) {
                                str = a4.a();
                            }
                            this.L$0 = yi7;
                            this.label = 1;
                            obj = ro4.a(j, c, e, str, (Integer) null, this, 8, (Object) null);
                            if (obj == a) {
                                return a;
                            }
                        } else {
                            ee7.a();
                            throw null;
                        }
                    } else {
                        ro4 j2 = this.this$0.this$0.j();
                        qn4 a5 = this.this$0.this$0.n;
                        String c2 = a5 != null ? a5.c() : null;
                        if (c2 != null) {
                            qn4 a6 = this.this$0.this$0.n;
                            String e2 = a6 != null ? a6.e() : null;
                            qn4 a7 = this.this$0.this$0.n;
                            if (a7 != null) {
                                str = a7.a();
                            }
                            this.L$0 = yi7;
                            this.label = 2;
                            obj = ro4.a(j2, c2, e2, str, (Integer) null, this, 8, (Object) null);
                            if (obj == a) {
                                return a;
                            }
                            return (ko4) obj;
                        }
                        ee7.a();
                        throw null;
                    }
                } else if (i == 1) {
                    yi7 yi72 = (yi7) this.L$0;
                    t87.a(obj);
                } else if (i == 2) {
                    yi7 yi73 = (yi7) this.L$0;
                    t87.a(obj);
                    return (ko4) obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return (ko4) obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kp4 kp4, fb7 fb7) {
            super(2, fb7);
            this.this$0 = kp4;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            d dVar = new d(this.this$0, fb7);
            dVar.p$ = (yi7) obj;
            return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((d) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                this.this$0.l.a(pb7.a(true));
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                obj = vh7.a(b, aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ko4 ko4 = (ko4) obj;
            this.this$0.l.a(pb7.a(false));
            if (ko4.c() != null) {
                this.this$0.m.a(new v87(pb7.a(true), null, this.this$0.n));
            } else {
                this.this$0.m.a(new v87(pb7.a(false), ko4.a(), null));
            }
            return i97.a;
        }
    }

    /*
    static {
        new a(null);
        String simpleName = ct4.class.getSimpleName();
        ee7.a((Object) simpleName, "BCFriendTabViewModel::class.java.simpleName");
        t = simpleName;
    }
    */

    @DexIgnore
    public kp4(ch5 ch5, ro4 ro4) {
        ee7.b(ch5, "mSharedPreferencesManager");
        ee7.b(ro4, "mChallengeRepository");
        this.s = ro4;
    }

    @DexIgnore
    public static final /* synthetic */ qn4 b(kp4 kp4) {
        qn4 qn4 = kp4.o;
        if (qn4 != null) {
            return qn4;
        }
        ee7.d("originalDraft");
        throw null;
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.h;
    }

    @DexIgnore
    public final LiveData<qn4> f() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<v87<Boolean, ServerError, qn4>> g() {
        return this.m;
    }

    @DexIgnore
    public final LiveData<Integer> h() {
        return this.i;
    }

    @DexIgnore
    public final LiveData<Boolean> i() {
        return this.l;
    }

    @DexIgnore
    public final ro4 j() {
        return this.s;
    }

    @DexIgnore
    public final LiveData<Integer> k() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<b> l() {
        return this.g;
    }

    @DexIgnore
    public final LiveData<c> m() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> n() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<List<dn4>> o() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Integer> p() {
        return this.k;
    }

    @DexIgnore
    public final LiveData<Boolean> q() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Boolean> r() {
        return this.e;
    }

    @DexIgnore
    public final void s() {
        qn4 qn4 = this.o;
        if (qn4 != null) {
            qn4 qn42 = this.n;
            if (qn42 == null) {
                ee7.a();
                throw null;
            } else if (nt4.a(qn4, qn42)) {
                this.m.a(new v87<>(true, null, null));
            } else {
                ik7 unused = xh7.b(ie.a(this), null, null, new d(this, null), 3, null);
            }
        } else {
            ee7.d("originalDraft");
            throw null;
        }
    }

    @DexIgnore
    public final void t() {
        qn4 qn4 = this.n;
        if (qn4 != null) {
            a(qn4);
        }
    }

    @DexIgnore
    public final void a(sn4 sn4) {
        if (sn4 != null) {
            String d2 = sn4.d();
            this.n = new qn4(null, null, null, sn4.g(), d2, sn4.e(), sn4.b(), null, null, false, null, FailureCode.FAILED_TO_CLEAR_DATA, null);
            a();
        }
        c();
    }

    @DexIgnore
    public final void b(qn4 qn4) {
        ee7.b(qn4, "draft");
        this.n = qn4;
        this.o = new qn4(null, qn4.e(), qn4.a(), null, null, qn4.i(), qn4.b(), null, null, false, null, 1945, null);
        a();
    }

    @DexIgnore
    public final void c(CharSequence charSequence) {
        if (charSequence != null) {
            int i2 = 0;
            if (!(charSequence.length() > 0)) {
                qn4 qn4 = this.n;
                if (qn4 != null) {
                    qn4.b(0);
                }
                d(0);
                b();
                c(0);
            } else if (charSequence.length() > 7) {
                qn4 qn42 = this.n;
                if (qn42 != null) {
                    i2 = qn42.i();
                }
                d(i2);
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                qn4 qn43 = this.n;
                if (qn43 == null || parseInt != qn43.i()) {
                    qn4 qn44 = this.n;
                    if (qn44 != null) {
                        qn44.b(parseInt);
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        d(parseInt);
                    }
                    c(parseInt);
                    b();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    d(0);
                }
            }
        }
    }

    @DexIgnore
    public final void d() {
        int i2 = (this.q * 60 * 60) + (this.r * 60);
        if (3600 <= i2 && 259200 >= i2) {
            if (!ee7.a((Object) this.e.a(), (Object) true)) {
                this.e.a((Boolean) true);
            }
        } else if (!ee7.a((Object) this.e.a(), (Object) false)) {
            this.e.a((Boolean) false);
        }
    }

    @DexIgnore
    public final void e(int i2) {
        if (1 <= i2 && 32 >= i2) {
            b a2 = this.g.a();
            if (a2 == null || !a2.b()) {
                a(true, i2);
                return;
            }
            return;
        }
        b a3 = this.g.a();
        if (a3 == null || a3.b()) {
            a(false, i2);
        }
    }

    @DexIgnore
    public final void b(String str) {
        ee7.b(str, "newName");
        e(str.length());
        qn4 qn4 = this.n;
        if (qn4 != null) {
            qn4.b(str);
            b();
        }
    }

    @DexIgnore
    public final void a(String str) {
        ee7.b(str, "newDes");
        qn4 qn4 = this.n;
        if (qn4 != null) {
            qn4.a(str);
        }
        if (str.length() > 0) {
            if (str.length() > 500) {
                this.h.a((Boolean) false);
            } else {
                this.h.a((Boolean) true);
            }
        }
        b();
    }

    @DexIgnore
    public final void d(int i2) {
        this.k.a(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void b(CharSequence charSequence) {
        if (charSequence != null) {
            if (charSequence.length() > 0) {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.r != parseInt) {
                    this.r = parseInt;
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        b(parseInt);
                    } else if (parseInt > 59) {
                        this.r = 59;
                        b(59);
                    }
                    qn4 qn4 = this.n;
                    if (qn4 != null) {
                        qn4.a((this.q * 60 * 60) + (this.r * 60));
                    }
                    b();
                    d();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    b(parseInt);
                }
            } else {
                this.r = 0;
                b(0);
                qn4 qn42 = this.n;
                if (qn42 != null) {
                    qn42.a((this.q * 60 * 60) + (this.r * 60));
                }
                b();
                d();
            }
        }
    }

    @DexIgnore
    public final void a(CharSequence charSequence) {
        if (charSequence != null) {
            if (!(charSequence.length() > 0)) {
                this.q = 0;
                a(0);
                qn4 qn4 = this.n;
                if (qn4 != null) {
                    qn4.a((this.q * 60 * 60) + (this.r * 60));
                }
                b();
            } else if (charSequence.length() > 3) {
                a(this.q);
                return;
            } else {
                int parseInt = Integer.parseInt(charSequence.toString());
                if (this.q != parseInt) {
                    this.q = parseInt;
                    qn4 qn42 = this.n;
                    if (qn42 != null) {
                        qn42.a((parseInt * 60 * 60) + (this.r * 60));
                    }
                    if (parseInt <= 9 && charSequence.length() > 1) {
                        a(this.q);
                    }
                    b();
                } else if (parseInt == 0 && charSequence.length() > 1) {
                    a(0);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final void c() {
        this.b.a(this.p);
    }

    @DexIgnore
    public final void c(int i2) {
        if (1000 <= i2 && 300000 >= i2) {
            if (!ee7.a((Object) this.f.a(), (Object) true)) {
                this.f.a((Boolean) true);
            }
        } else if (!ee7.a((Object) this.f.a(), (Object) false)) {
            this.f.a((Boolean) false);
        }
    }

    @DexIgnore
    public final void b() {
        String str;
        qn4 qn4 = this.n;
        if (qn4 == null || (str = qn4.a()) == null) {
            str = "";
        }
        boolean z = true;
        if (str.length() > 0) {
            MutableLiveData<Boolean> mutableLiveData = this.d;
            qn4 qn42 = this.n;
            if (!(qn42 != null ? rn4.a(qn42) : false) || str.length() > 500) {
                z = false;
            }
            mutableLiveData.a(Boolean.valueOf(z));
            return;
        }
        MutableLiveData<Boolean> mutableLiveData2 = this.d;
        qn4 qn43 = this.n;
        mutableLiveData2.a(qn43 != null ? Boolean.valueOf(rn4.a(qn43)) : null);
    }

    @DexIgnore
    public final void a(dn4 dn4) {
        ee7.b(dn4, DeviceRequestsHelper.DEVICE_INFO_MODEL);
        qn4 qn4 = this.n;
        if (qn4 != null) {
            Object a2 = dn4.a();
            if (!(a2 instanceof String)) {
                a2 = null;
            }
            String str = (String) a2;
            if (str == null) {
                str = "public_with_friend";
            }
            qn4.c(str);
        }
        c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = t;
        local.e(str2, "onPrivacyChanged - privacyModels: " + this.p);
    }

    @DexIgnore
    public final void b(int i2) {
        this.j.a(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void a() {
        this.a.a(this.n);
    }

    @DexIgnore
    public final void a(int i2) {
        this.i.a(Integer.valueOf(i2));
    }

    @DexIgnore
    public final void a(qn4 qn4) {
        this.c.a(new c(qn4));
    }

    @DexIgnore
    public final void a(boolean z, int i2) {
        this.g.a(new b(z, i2));
    }
}
