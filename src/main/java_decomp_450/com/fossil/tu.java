package com.fossil;

import android.annotation.TargetApi;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Process;
import android.os.SystemClock;
import java.util.concurrent.BlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tu extends Thread {
    @DexIgnore
    public /* final */ BlockingQueue<yu<?>> a;
    @DexIgnore
    public /* final */ su b;
    @DexIgnore
    public /* final */ mu c;
    @DexIgnore
    public /* final */ bv d;
    @DexIgnore
    public volatile boolean e; // = false;

    @DexIgnore
    public tu(BlockingQueue<yu<?>> blockingQueue, su suVar, mu muVar, bv bvVar) {
        this.a = blockingQueue;
        this.b = suVar;
        this.c = muVar;
        this.d = bvVar;
    }

    @DexIgnore
    @TargetApi(14)
    public final void a(yu<?> yuVar) {
        if (Build.VERSION.SDK_INT >= 14) {
            TrafficStats.setThreadStatsTag(yuVar.getTrafficStatsTag());
        }
    }

    @DexIgnore
    public void b() {
        this.e = true;
        interrupt();
    }

    @DexIgnore
    public void run() {
        Process.setThreadPriority(10);
        while (true) {
            try {
                a();
            } catch (InterruptedException unused) {
                if (this.e) {
                    Thread.currentThread().interrupt();
                    return;
                }
                gv.c("Ignoring spurious interrupt of NetworkDispatcher thread; use quit() to terminate it", new Object[0]);
            }
        }
    }

    @DexIgnore
    public final void a() throws InterruptedException {
        b(this.a.take());
    }

    @DexIgnore
    public void b(yu<?> yuVar) {
        long elapsedRealtime = SystemClock.elapsedRealtime();
        try {
            yuVar.addMarker("network-queue-take");
            if (yuVar.isCanceled()) {
                yuVar.finish("network-discard-cancelled");
                yuVar.notifyListenerResponseNotUsable();
                return;
            }
            a(yuVar);
            vu a2 = this.b.a(yuVar);
            yuVar.addMarker("network-http-complete");
            if (!a2.e || !yuVar.hasHadResponseDelivered()) {
                av<?> parseNetworkResponse = yuVar.parseNetworkResponse(a2);
                yuVar.addMarker("network-parse-complete");
                if (yuVar.shouldCache() && parseNetworkResponse.b != null) {
                    this.c.a(yuVar.getCacheKey(), parseNetworkResponse.b);
                    yuVar.addMarker("network-cache-written");
                }
                yuVar.markDelivered();
                this.d.a(yuVar, parseNetworkResponse);
                yuVar.notifyListenerResponseReceived(parseNetworkResponse);
                return;
            }
            yuVar.finish("not-modified");
            yuVar.notifyListenerResponseNotUsable();
        } catch (fv e2) {
            e2.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            a(yuVar, e2);
            yuVar.notifyListenerResponseNotUsable();
        } catch (Exception e3) {
            gv.a(e3, "Unhandled exception %s", e3.toString());
            fv fvVar = new fv(e3);
            fvVar.setNetworkTimeMs(SystemClock.elapsedRealtime() - elapsedRealtime);
            this.d.a(yuVar, fvVar);
            yuVar.notifyListenerResponseNotUsable();
        }
    }

    @DexIgnore
    public final void a(yu<?> yuVar, fv fvVar) {
        this.d.a(yuVar, yuVar.parseNetworkError(fvVar));
    }
}
