package com.fossil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ng {
    @DexIgnore
    public static /* final */ Comparator<g> a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Comparator<g> {
        @DexIgnore
        /* renamed from: a */
        public int compare(g gVar, g gVar2) {
            int i = gVar.a - gVar2.a;
            return i == 0 ? gVar.b - gVar2.b : i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class b {
        @DexIgnore
        public abstract int a();

        @DexIgnore
        public abstract boolean a(int i, int i2);

        @DexIgnore
        public abstract int b();

        @DexIgnore
        public abstract boolean b(int i, int i2);

        @DexIgnore
        public abstract Object c(int i, int i2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<T> {
        @DexIgnore
        public abstract boolean areContentsTheSame(T t, T t2);

        @DexIgnore
        public abstract boolean areItemsTheSame(T t, T t2);

        @DexIgnore
        public Object getChangePayload(T t, T t2) {
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public boolean c;

        @DexIgnore
        public e(int i, int i2, boolean z) {
            this.a = i;
            this.b = i2;
            this.c = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;

        @DexIgnore
        public f() {
        }

        @DexIgnore
        public f(int i, int i2, int i3, int i4) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.d = i4;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g {
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;
        @DexIgnore
        public boolean e;
    }

    @DexIgnore
    public static c a(b bVar) {
        return a(bVar, true);
    }

    @DexIgnore
    public static c a(b bVar, boolean z) {
        f fVar;
        int b2 = bVar.b();
        int a2 = bVar.a();
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add(new f(0, b2, 0, a2));
        int abs = b2 + a2 + Math.abs(b2 - a2);
        int i = abs * 2;
        int[] iArr = new int[i];
        int[] iArr2 = new int[i];
        ArrayList arrayList3 = new ArrayList();
        while (!arrayList2.isEmpty()) {
            f fVar2 = (f) arrayList2.remove(arrayList2.size() - 1);
            g a3 = a(bVar, fVar2.a, fVar2.b, fVar2.c, fVar2.d, iArr, iArr2, abs);
            if (a3 != null) {
                if (a3.c > 0) {
                    arrayList.add(a3);
                }
                a3.a += fVar2.a;
                a3.b += fVar2.c;
                if (arrayList3.isEmpty()) {
                    fVar = new f();
                } else {
                    fVar = (f) arrayList3.remove(arrayList3.size() - 1);
                }
                fVar.a = fVar2.a;
                fVar.c = fVar2.c;
                if (a3.e) {
                    fVar.b = a3.a;
                    fVar.d = a3.b;
                } else if (a3.d) {
                    fVar.b = a3.a - 1;
                    fVar.d = a3.b;
                } else {
                    fVar.b = a3.a;
                    fVar.d = a3.b - 1;
                }
                arrayList2.add(fVar);
                if (!a3.e) {
                    int i2 = a3.a;
                    int i3 = a3.c;
                    fVar2.a = i2 + i3;
                    fVar2.c = a3.b + i3;
                } else if (a3.d) {
                    int i4 = a3.a;
                    int i5 = a3.c;
                    fVar2.a = i4 + i5 + 1;
                    fVar2.c = a3.b + i5;
                } else {
                    int i6 = a3.a;
                    int i7 = a3.c;
                    fVar2.a = i6 + i7;
                    fVar2.c = a3.b + i7 + 1;
                }
                arrayList2.add(fVar2);
            } else {
                arrayList3.add(fVar2);
            }
        }
        Collections.sort(arrayList, a);
        return new c(bVar, arrayList, iArr, iArr2, z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ List<g> a;
        @DexIgnore
        public /* final */ int[] b;
        @DexIgnore
        public /* final */ int[] c;
        @DexIgnore
        public /* final */ b d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;

        @DexIgnore
        public c(b bVar, List<g> list, int[] iArr, int[] iArr2, boolean z) {
            this.a = list;
            this.b = iArr;
            this.c = iArr2;
            Arrays.fill(iArr, 0);
            Arrays.fill(this.c, 0);
            this.d = bVar;
            this.e = bVar.b();
            this.f = bVar.a();
            this.g = z;
            a();
            b();
        }

        @DexIgnore
        public final void a() {
            g gVar = this.a.isEmpty() ? null : this.a.get(0);
            if (gVar == null || gVar.a != 0 || gVar.b != 0) {
                g gVar2 = new g();
                gVar2.a = 0;
                gVar2.b = 0;
                gVar2.d = false;
                gVar2.c = 0;
                gVar2.e = false;
                this.a.add(0, gVar2);
            }
        }

        @DexIgnore
        public final void b() {
            int i = this.e;
            int i2 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                g gVar = this.a.get(size);
                int i3 = gVar.a;
                int i4 = gVar.c;
                int i5 = i3 + i4;
                int i6 = gVar.b + i4;
                if (this.g) {
                    while (i > i5) {
                        a(i, i2, size);
                        i--;
                    }
                    while (i2 > i6) {
                        b(i, i2, size);
                        i2--;
                    }
                }
                for (int i7 = 0; i7 < gVar.c; i7++) {
                    int i8 = gVar.a + i7;
                    int i9 = gVar.b + i7;
                    int i10 = this.d.a(i8, i9) ? 1 : 2;
                    this.b[i8] = (i9 << 5) | i10;
                    this.c[i9] = (i8 << 5) | i10;
                }
                i = gVar.a;
                i2 = gVar.b;
            }
        }

        @DexIgnore
        public final void a(int i, int i2, int i3) {
            if (this.b[i - 1] == 0) {
                a(i, i2, i3, false);
            }
        }

        @DexIgnore
        public int a(int i) {
            if (i < 0 || i >= this.e) {
                throw new IndexOutOfBoundsException("Index out of bounds - passed position = " + i + ", old list size = " + this.e);
            }
            int i2 = this.b[i];
            if ((i2 & 31) == 0) {
                return -1;
            }
            return i2 >> 5;
        }

        @DexIgnore
        public final boolean a(int i, int i2, int i3, boolean z) {
            int i4;
            int i5;
            if (z) {
                i2--;
                i4 = i;
                i5 = i2;
            } else {
                i5 = i - 1;
                i4 = i5;
            }
            while (i3 >= 0) {
                g gVar = this.a.get(i3);
                int i6 = gVar.a;
                int i7 = gVar.c;
                int i8 = i6 + i7;
                int i9 = gVar.b + i7;
                int i10 = 8;
                if (z) {
                    for (int i11 = i4 - 1; i11 >= i8; i11--) {
                        if (this.d.b(i11, i5)) {
                            if (!this.d.a(i11, i5)) {
                                i10 = 4;
                            }
                            this.c[i5] = (i11 << 5) | 16;
                            this.b[i11] = (i5 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                } else {
                    for (int i12 = i2 - 1; i12 >= i9; i12--) {
                        if (this.d.b(i5, i12)) {
                            if (!this.d.a(i5, i12)) {
                                i10 = 4;
                            }
                            int i13 = i - 1;
                            this.b[i13] = (i12 << 5) | 16;
                            this.c[i12] = (i13 << 5) | i10;
                            return true;
                        }
                    }
                    continue;
                }
                i4 = gVar.a;
                i2 = gVar.b;
                i3--;
            }
            return false;
        }

        @DexIgnore
        public final void b(int i, int i2, int i3) {
            if (this.c[i2 - 1] == 0) {
                a(i, i2, i3, true);
            }
        }

        @DexIgnore
        public final void b(List<e> list, xg xgVar, int i, int i2, int i3) {
            if (!this.g) {
                xgVar.c(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.b[i5] & 31;
                if (i6 == 0) {
                    xgVar.c(i + i4, 1);
                    for (e eVar : list) {
                        eVar.b--;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.b[i5] >> 5;
                    e a2 = a(list, i7, false);
                    xgVar.a(i + i4, a2.b - 1);
                    if (i6 == 4) {
                        xgVar.a(a2.b - 1, 1, this.d.c(i5, i7));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i + i4, true));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }

        @DexIgnore
        public void a(xg xgVar) {
            kg kgVar;
            if (xgVar instanceof kg) {
                kgVar = (kg) xgVar;
            } else {
                kgVar = new kg(xgVar);
            }
            ArrayList arrayList = new ArrayList();
            int i = this.e;
            int i2 = this.f;
            for (int size = this.a.size() - 1; size >= 0; size--) {
                g gVar = this.a.get(size);
                int i3 = gVar.c;
                int i4 = gVar.a + i3;
                int i5 = gVar.b + i3;
                if (i4 < i) {
                    b(arrayList, kgVar, i4, i - i4, i4);
                }
                if (i5 < i2) {
                    a(arrayList, kgVar, i4, i2 - i5, i5);
                }
                for (int i6 = i3 - 1; i6 >= 0; i6--) {
                    int[] iArr = this.b;
                    int i7 = gVar.a;
                    if ((iArr[i7 + i6] & 31) == 2) {
                        kgVar.a(i7 + i6, 1, this.d.c(i7 + i6, gVar.b + i6));
                    }
                }
                i = gVar.a;
                i2 = gVar.b;
            }
            kgVar.a();
        }

        @DexIgnore
        public static e a(List<e> list, int i, boolean z) {
            int size = list.size() - 1;
            while (size >= 0) {
                e eVar = list.get(size);
                if (eVar.a == i && eVar.c == z) {
                    list.remove(size);
                    while (size < list.size()) {
                        list.get(size).b += z ? 1 : -1;
                        size++;
                    }
                    return eVar;
                }
                size--;
            }
            return null;
        }

        @DexIgnore
        public final void a(List<e> list, xg xgVar, int i, int i2, int i3) {
            if (!this.g) {
                xgVar.b(i, i2);
                return;
            }
            for (int i4 = i2 - 1; i4 >= 0; i4--) {
                int i5 = i3 + i4;
                int i6 = this.c[i5] & 31;
                if (i6 == 0) {
                    xgVar.b(i, 1);
                    for (e eVar : list) {
                        eVar.b++;
                    }
                } else if (i6 == 4 || i6 == 8) {
                    int i7 = this.c[i5] >> 5;
                    xgVar.a(a(list, i7, true).b, i);
                    if (i6 == 4) {
                        xgVar.a(i, 1, this.d.c(i7, i5));
                    }
                } else if (i6 == 16) {
                    list.add(new e(i5, i, false));
                } else {
                    throw new IllegalStateException("unknown flag for pos " + i5 + " " + Long.toBinaryString((long) i6));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0042, code lost:
        if (r24[r13 - 1] < r24[r13 + r5]) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b8, code lost:
        if (r25[r12 - 1] < r25[r12 + 1]) goto L_0x00c5;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.fossil.ng.g a(com.fossil.ng.b r19, int r20, int r21, int r22, int r23, int[] r24, int[] r25, int r26) {
        /*
            r0 = r19
            r1 = r24
            r2 = r25
            int r3 = r21 - r20
            int r4 = r23 - r22
            r5 = 1
            if (r3 < r5) goto L_0x012f
            if (r4 >= r5) goto L_0x0011
            goto L_0x012f
        L_0x0011:
            int r6 = r3 - r4
            int r7 = r3 + r4
            int r7 = r7 + r5
            int r7 = r7 / 2
            int r8 = r26 - r7
            int r8 = r8 - r5
            int r9 = r26 + r7
            int r9 = r9 + r5
            r10 = 0
            java.util.Arrays.fill(r1, r8, r9, r10)
            int r8 = r8 + r6
            int r9 = r9 + r6
            java.util.Arrays.fill(r2, r8, r9, r3)
            int r8 = r6 % 2
            if (r8 == 0) goto L_0x002d
            r8 = 1
            goto L_0x002e
        L_0x002d:
            r8 = 0
        L_0x002e:
            r9 = 0
        L_0x002f:
            if (r9 > r7) goto L_0x0127
            int r11 = -r9
            r12 = r11
        L_0x0033:
            if (r12 > r9) goto L_0x00a0
            if (r12 == r11) goto L_0x004d
            if (r12 == r9) goto L_0x0045
            int r13 = r26 + r12
            int r14 = r13 + -1
            r14 = r1[r14]
            int r13 = r13 + r5
            r13 = r1[r13]
            if (r14 >= r13) goto L_0x0045
            goto L_0x004d
        L_0x0045:
            int r13 = r26 + r12
            int r13 = r13 - r5
            r13 = r1[r13]
            int r13 = r13 + r5
            r14 = 1
            goto L_0x0053
        L_0x004d:
            int r13 = r26 + r12
            int r13 = r13 + r5
            r13 = r1[r13]
            r14 = 0
        L_0x0053:
            int r15 = r13 - r12
        L_0x0055:
            if (r13 >= r3) goto L_0x006a
            if (r15 >= r4) goto L_0x006a
            int r10 = r20 + r13
            int r5 = r22 + r15
            boolean r5 = r0.b(r10, r5)
            if (r5 == 0) goto L_0x006a
            int r13 = r13 + 1
            int r15 = r15 + 1
            r5 = 1
            r10 = 0
            goto L_0x0055
        L_0x006a:
            int r5 = r26 + r12
            r1[r5] = r13
            if (r8 == 0) goto L_0x009a
            int r10 = r6 - r9
            r13 = 1
            int r10 = r10 + r13
            if (r12 < r10) goto L_0x009a
            int r10 = r6 + r9
            int r10 = r10 - r13
            if (r12 > r10) goto L_0x009a
            r10 = r1[r5]
            r13 = r2[r5]
            if (r10 < r13) goto L_0x009a
            com.fossil.ng$g r0 = new com.fossil.ng$g
            r0.<init>()
            r3 = r2[r5]
            r0.a = r3
            int r3 = r3 - r12
            r0.b = r3
            r1 = r1[r5]
            r2 = r2[r5]
            int r1 = r1 - r2
            r0.c = r1
            r0.d = r14
            r13 = 0
            r0.e = r13
            return r0
        L_0x009a:
            r13 = 0
            int r12 = r12 + 2
            r5 = 1
            r10 = 0
            goto L_0x0033
        L_0x00a0:
            r13 = 0
            r5 = r11
        L_0x00a2:
            if (r5 > r9) goto L_0x011c
            int r10 = r5 + r6
            int r12 = r9 + r6
            if (r10 == r12) goto L_0x00c4
            int r12 = r11 + r6
            if (r10 == r12) goto L_0x00bb
            int r12 = r26 + r10
            int r14 = r12 + -1
            r14 = r2[r14]
            r15 = 1
            int r12 = r12 + r15
            r12 = r2[r12]
            if (r14 >= r12) goto L_0x00bc
            goto L_0x00c5
        L_0x00bb:
            r15 = 1
        L_0x00bc:
            int r12 = r26 + r10
            int r12 = r12 + r15
            r12 = r2[r12]
            int r12 = r12 - r15
            r14 = 1
            goto L_0x00cb
        L_0x00c4:
            r15 = 1
        L_0x00c5:
            int r12 = r26 + r10
            int r12 = r12 - r15
            r12 = r2[r12]
            r14 = 0
        L_0x00cb:
            int r16 = r12 - r10
        L_0x00cd:
            if (r12 <= 0) goto L_0x00ea
            if (r16 <= 0) goto L_0x00ea
            int r17 = r20 + r12
            int r13 = r17 + -1
            int r17 = r22 + r16
            r18 = r3
            int r3 = r17 + -1
            boolean r3 = r0.b(r13, r3)
            if (r3 == 0) goto L_0x00ec
            int r12 = r12 + -1
            int r16 = r16 + -1
            r3 = r18
            r13 = 0
            r15 = 1
            goto L_0x00cd
        L_0x00ea:
            r18 = r3
        L_0x00ec:
            int r3 = r26 + r10
            r2[r3] = r12
            if (r8 != 0) goto L_0x0115
            if (r10 < r11) goto L_0x0115
            if (r10 > r9) goto L_0x0115
            r12 = r1[r3]
            r13 = r2[r3]
            if (r12 < r13) goto L_0x0115
            com.fossil.ng$g r0 = new com.fossil.ng$g
            r0.<init>()
            r4 = r2[r3]
            r0.a = r4
            int r4 = r4 - r10
            r0.b = r4
            r1 = r1[r3]
            r2 = r2[r3]
            int r1 = r1 - r2
            r0.c = r1
            r0.d = r14
            r3 = 1
            r0.e = r3
            return r0
        L_0x0115:
            r3 = 1
            int r5 = r5 + 2
            r3 = r18
            r13 = 0
            goto L_0x00a2
        L_0x011c:
            r18 = r3
            r3 = 1
            int r9 = r9 + 1
            r3 = r18
            r5 = 1
            r10 = 0
            goto L_0x002f
        L_0x0127:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "DiffUtil hit an unexpected case while trying to calculate the optimal path. Please make sure your data is not changing during the diff calculation."
            r0.<init>(r1)
            throw r0
        L_0x012f:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.ng.a(com.fossil.ng$b, int, int, int, int, int[], int[], int):com.fossil.ng$g");
    }
}
