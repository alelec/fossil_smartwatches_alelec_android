package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class ms1 extends Enum<ms1> {
    @DexIgnore
    public static /* final */ ms1 b;
    @DexIgnore
    public static /* final */ ms1 c;
    @DexIgnore
    public static /* final */ /* synthetic */ ms1[] d;
    @DexIgnore
    public static /* final */ nq1 e; // = new nq1(null);
    @DexIgnore
    public /* final */ int a;

    /*
    static {
        ms1 ms1 = new ms1("PROPERTY_NOTIFY", 4, 16);
        b = ms1;
        ms1 ms12 = new ms1("PROPERTY_INDICATE", 5, 32);
        c = ms12;
        d = new ms1[]{new ms1("PROPERTY_BROADCAST", 0, 1), new ms1("PROPERTY_READ", 1, 2), new ms1("PROPERTY_WRITE_NO_RESPONSE", 2, 4), new ms1("PROPERTY_WRITE", 3, 8), ms1, ms12, new ms1("PROPERTY_SIGNED_WRITE", 6, 64), new ms1("PROPERTY_EXTENDED_PROPS", 7, 128)};
    }
    */

    @DexIgnore
    public ms1(String str, int i, int i2) {
        this.a = i2;
    }

    @DexIgnore
    public static ms1 valueOf(String str) {
        return (ms1) Enum.valueOf(ms1.class, str);
    }

    @DexIgnore
    public static ms1[] values() {
        return (ms1[]) d.clone();
    }
}
