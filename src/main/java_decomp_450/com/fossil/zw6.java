package com.fossil;

import com.misfit.frameworks.buttonservice.source.FirmwareFileRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.GuestApiService;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zw6 implements MembersInjector<yw6> {
    @DexIgnore
    public static void a(yw6 yw6, DeviceRepository deviceRepository) {
        yw6.a = deviceRepository;
    }

    @DexIgnore
    public static void a(yw6 yw6, ch5 ch5) {
        yw6.b = ch5;
    }

    @DexIgnore
    public static void a(yw6 yw6, UserRepository userRepository) {
        yw6.c = userRepository;
    }

    @DexIgnore
    public static void a(yw6 yw6, GuestApiService guestApiService) {
        yw6.d = guestApiService;
    }

    @DexIgnore
    public static void a(yw6 yw6, FirmwareFileRepository firmwareFileRepository) {
        yw6.e = firmwareFileRepository;
    }
}
