package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gi3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ String c;
    @DexIgnore
    public /* final */ /* synthetic */ long d;
    @DexIgnore
    public /* final */ /* synthetic */ ph3 e;

    @DexIgnore
    public gi3(ph3 ph3, String str, String str2, String str3, long j) {
        this.e = ph3;
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
    }

    @DexIgnore
    public final void run() {
        String str = this.a;
        if (str == null) {
            this.e.a.u().D().a(this.b, (wj3) null);
            return;
        }
        this.e.a.u().D().a(this.b, new wj3(this.c, str, this.d));
    }
}
