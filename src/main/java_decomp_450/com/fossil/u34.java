package com.fossil;

import java.util.concurrent.Callable;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u34 {
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public no3<Void> b; // = qo3.a((Object) null);
    @DexIgnore
    public /* final */ Object c; // = new Object();
    @DexIgnore
    public ThreadLocal<Boolean> d; // = new ThreadLocal<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            u34.this.d.set(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Callable<Void> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable a;

        @DexIgnore
        public b(u34 u34, Runnable runnable) {
            this.a = runnable;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Void call() throws Exception {
            this.a.run();
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements fo3<Void, T> {
        @DexIgnore
        public /* final */ /* synthetic */ Callable a;

        @DexIgnore
        public c(u34 u34, Callable callable) {
            this.a = callable;
        }

        @DexIgnore
        @Override // com.fossil.fo3
        public T then(no3<Void> no3) throws Exception {
            return (T) this.a.call();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements fo3<T, Void> {
        @DexIgnore
        public d(u34 u34) {
        }

        @DexIgnore
        @Override // com.fossil.fo3
        public Void then(no3<T> no3) throws Exception {
            return null;
        }
    }

    @DexIgnore
    public u34(ExecutorService executorService) {
        this.a = executorService;
        executorService.submit(new a());
    }

    @DexIgnore
    public Executor b() {
        return this.a;
    }

    @DexIgnore
    public final boolean c() {
        return Boolean.TRUE.equals(this.d.get());
    }

    @DexIgnore
    public void a() {
        if (!c()) {
            throw new IllegalStateException("Not running on background worker thread as intended.");
        }
    }

    @DexIgnore
    public <T> no3<T> b(Callable<T> callable) {
        no3<T> a2;
        synchronized (this.c) {
            a2 = this.b.a(this.a, a(callable));
            this.b = a(a2);
        }
        return a2;
    }

    @DexIgnore
    public <T> no3<T> c(Callable<no3<T>> callable) {
        no3<T> b2;
        synchronized (this.c) {
            b2 = this.b.b(this.a, a(callable));
            this.b = a(b2);
        }
        return b2;
    }

    @DexIgnore
    public no3<Void> a(Runnable runnable) {
        return b(new b(this, runnable));
    }

    @DexIgnore
    public final <T> fo3<Void, T> a(Callable<T> callable) {
        return new c(this, callable);
    }

    @DexIgnore
    public final <T> no3<Void> a(no3<T> no3) {
        return no3.a(this.a, new d(this));
    }
}
