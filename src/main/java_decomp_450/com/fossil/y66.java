package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y66 implements Factory<ua6> {
    @DexIgnore
    public static ua6 a(u66 u66) {
        ua6 d = u66.d();
        c87.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
