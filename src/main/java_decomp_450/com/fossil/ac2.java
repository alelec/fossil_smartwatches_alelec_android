package com.fossil;

import android.content.Context;
import android.os.Build;
import com.fossil.v02;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ac2 {
    @DexIgnore
    @Deprecated
    public static /* final */ v02<v02.d.C0203d> a; // = sh2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ v02<v02.d.C0203d> b; // = mh2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ v02<v02.d.C0203d> c; // = gk2.G;
    @DexIgnore
    @Deprecated
    public static /* final */ zb2 d; // = new dj2();

    /*
    static {
        v02<v02.d.C0203d> v02 = xh2.G;
        new kj2();
        new jj2();
        v02<v02.d.C0203d> v022 = bi2.G;
        new lj2();
        new ij2();
        v02<v02.d.C0203d> v023 = mk2.G;
        new gj2();
        v02<v02.d.C0203d> v024 = ck2.G;
        if (Build.VERSION.SDK_INT >= 18) {
            new cj2();
        } else {
            new oj2();
        }
        new Scope("https://www.googleapis.com/auth/fitness.activity.read");
        new Scope("https://www.googleapis.com/auth/fitness.activity.write");
        new Scope("https://www.googleapis.com/auth/fitness.location.read");
        new Scope("https://www.googleapis.com/auth/fitness.location.write");
        new Scope("https://www.googleapis.com/auth/fitness.body.read");
        new Scope("https://www.googleapis.com/auth/fitness.body.write");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.read");
        new Scope("https://www.googleapis.com/auth/fitness.nutrition.write");
    }
    */

    @DexIgnore
    public static dc2 a(Context context, GoogleSignInAccount googleSignInAccount) {
        a72.a(googleSignInAccount);
        return new dc2(context, new wd2(context, googleSignInAccount));
    }

    @DexIgnore
    public static fc2 b(Context context, GoogleSignInAccount googleSignInAccount) {
        a72.a(googleSignInAccount);
        return new fc2(context, new wd2(context, googleSignInAccount));
    }
}
