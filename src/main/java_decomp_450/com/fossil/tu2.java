package com.fossil;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tu2 implements Serializable, Iterable<Byte> {
    @DexIgnore
    public static /* final */ zu2 a; // = (mu2.a() ? new dv2(null) : new xu2(null));
    @DexIgnore
    public static /* final */ tu2 zza; // = new ev2(ew2.b);
    @DexIgnore
    public int zzc; // = 0;

    /*
    static {
        new vu2();
    }
    */

    @DexIgnore
    public static int a(byte b) {
        return b & 255;
    }

    @DexIgnore
    public static tu2 zza(byte[] bArr, int i, int i2) {
        zzb(i, i + i2, bArr.length);
        return new ev2(a.a(bArr, i, i2));
    }

    @DexIgnore
    public static cv2 zzc(int i) {
        return new cv2(i, null);
    }

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public final int hashCode() {
        int i = this.zzc;
        if (i == 0) {
            int zza2 = zza();
            i = zza(zza2, 0, zza2);
            if (i == 0) {
                i = 1;
            }
            this.zzc = i;
        }
        return i;
    }

    @DexIgnore
    /* Return type fixed from 'java.util.Iterator' to match base method */
    @Override // java.lang.Iterable
    public /* synthetic */ Iterator<Byte> iterator() {
        return new su2(this);
    }

    @DexIgnore
    public final String toString() {
        Locale locale = Locale.ROOT;
        Object[] objArr = new Object[3];
        objArr[0] = Integer.toHexString(System.identityHashCode(this));
        objArr[1] = Integer.valueOf(zza());
        objArr[2] = zza() <= 50 ? qy2.a(this) : String.valueOf(qy2.a(zza(0, 47))).concat("...");
        return String.format(locale, "<ByteString@%s size=%d contents=\"%s\">", objArr);
    }

    @DexIgnore
    public abstract byte zza(int i);

    @DexIgnore
    public abstract int zza();

    @DexIgnore
    public abstract int zza(int i, int i2, int i3);

    @DexIgnore
    public abstract tu2 zza(int i, int i2);

    @DexIgnore
    public abstract String zza(Charset charset);

    @DexIgnore
    public abstract void zza(qu2 qu2) throws IOException;

    @DexIgnore
    public abstract byte zzb(int i);

    @DexIgnore
    public final String zzb() {
        return zza() == 0 ? "" : zza(ew2.a);
    }

    @DexIgnore
    public abstract boolean zzc();

    @DexIgnore
    public final int zzd() {
        return this.zzc;
    }

    @DexIgnore
    public static tu2 zza(byte[] bArr) {
        return new ev2(bArr);
    }

    @DexIgnore
    public static int zzb(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    @DexIgnore
    public static tu2 zza(String str) {
        return new ev2(str.getBytes(ew2.a));
    }
}
