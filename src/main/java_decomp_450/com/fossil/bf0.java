package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bf0 extends cf0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ n70 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<bf0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public bf0 createFromParcel(Parcel parcel) {
            return new bf0(parcel, (zd7) null);
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public bf0[] newArray(int i) {
            return new bf0[i];
        }
    }

    @DexIgnore
    public bf0(xb0 xb0, n70 n70) throws IllegalArgumentException {
        super(xb0, null);
        this.c = n70;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public byte[] a(short s, r60 r60) {
        yb0 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (this.c != null) {
                jSONObject2.put("dest", this.c.getDestination()).put("commute", this.c.getCommuteTimeInMinute()).put("traffic", this.c.getTraffic());
            } else {
                df0 deviceMessage = getDeviceMessage();
                if (deviceMessage != null) {
                    jSONObject2.put("message", deviceMessage.getMessage()).put("type", deviceMessage.getType().a());
                }
            }
            jSONObject.put("commuteApp._.config.commute_info", jSONObject2);
        } catch (JSONException e) {
            wl0.h.a(e);
        }
        JSONObject jSONObject3 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject4 = new JSONObject();
            jSONObject4.put("id", valueOf);
            jSONObject4.put("set", jSONObject);
            jSONObject3.put(str, jSONObject4);
        } catch (JSONException e2) {
            wl0.h.a(e2);
        }
        String jSONObject5 = jSONObject3.toString();
        ee7.a((Object) jSONObject5, "deviceResponseJSONObject.toString()");
        Charset c2 = b21.x.c();
        if (jSONObject5 != null) {
            byte[] bytes = jSONObject5.getBytes(c2);
            ee7.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new x87("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public JSONObject b() {
        Object obj;
        JSONObject b = super.b();
        r51 r51 = r51.y4;
        n70 n70 = this.c;
        if (n70 == null || (obj = n70.a()) == null) {
            obj = JSONObject.NULL;
        }
        return yz0.a(b, r51, obj);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!ee7.a(bf0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(ee7.a(this.c, ((bf0) obj).c) ^ true);
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.CommuteTimeWatchAppData");
    }

    @DexIgnore
    public final n70 getCommuteTimeInfo() {
        return this.c;
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        n70 n70 = this.c;
        return hashCode + (n70 != null ? n70.hashCode() : 0);
    }

    @DexIgnore
    @Override // com.fossil.cf0
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            n70 n70 = this.c;
            if (n70 != null) {
                parcel.writeParcelable(n70, i);
                return;
            }
            throw new x87("null cannot be cast to non-null type android.os.Parcelable");
        }
    }

    @DexIgnore
    public bf0(xb0 xb0, df0 df0) throws IllegalArgumentException {
        super(xb0, df0);
        this.c = null;
    }

    @DexIgnore
    public bf0(xb0 xb0, df0 df0, n70 n70) {
        super(xb0, df0);
        this.c = n70;
    }

    @DexIgnore
    public bf0(n70 n70) throws IllegalArgumentException {
        this(null, null, n70);
    }

    @DexIgnore
    public bf0(df0 df0) throws IllegalArgumentException {
        this(null, df0, null);
    }

    @DexIgnore
    public /* synthetic */ bf0(Parcel parcel, zd7 zd7) {
        super((yb0) parcel.readParcelable(xb0.class.getClassLoader()), (df0) parcel.readParcelable(df0.class.getClassLoader()));
        this.c = (n70) parcel.readParcelable(n70.class.getClassLoader());
    }
}
