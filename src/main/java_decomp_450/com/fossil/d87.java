package com.fossil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import io.flutter.embedding.android.SplashScreen;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: SplashScreen */
public final /* synthetic */ class d87 {
    @DexIgnore
    @SuppressLint({"NewApi"})
    public static boolean $default$doesSplashViewRememberItsTransition(SplashScreen splashScreen) {
        return false;
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static Bundle $default$saveSplashScreenState(SplashScreen splashScreen) {
        return null;
    }
}
