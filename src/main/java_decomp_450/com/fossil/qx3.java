package com.fossil;

import com.google.errorprone.annotations.CanIgnoreReturnValue;
import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.Queue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qx3<E> extends mx3<E> implements Queue<E> {
    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract /* bridge */ /* synthetic */ Object delegate();

    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract /* bridge */ /* synthetic */ Collection delegate();

    @DexIgnore
    @Override // com.fossil.mx3, com.fossil.mx3, com.fossil.px3
    public abstract Queue<E> delegate();

    @DexIgnore
    @Override // java.util.Queue
    public E element() {
        return delegate().element();
    }

    @DexIgnore
    @Override // java.util.Queue
    @CanIgnoreReturnValue
    public abstract boolean offer(E e);

    @DexIgnore
    @Override // java.util.Queue
    public E peek() {
        return delegate().peek();
    }

    @DexIgnore
    @Override // java.util.Queue
    @CanIgnoreReturnValue
    public E poll() {
        return delegate().poll();
    }

    @DexIgnore
    @Override // java.util.Queue
    @CanIgnoreReturnValue
    public E remove() {
        return delegate().remove();
    }

    @DexIgnore
    public boolean standardOffer(E e) {
        try {
            return add(e);
        } catch (IllegalStateException unused) {
            return false;
        }
    }

    @DexIgnore
    public E standardPeek() {
        try {
            return element();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }

    @DexIgnore
    public E standardPoll() {
        try {
            return remove();
        } catch (NoSuchElementException unused) {
            return null;
        }
    }
}
