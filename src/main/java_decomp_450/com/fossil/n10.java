package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n10 implements cx<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ j10 a; // = new j10();

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, ax axVar) throws IOException {
        return true;
    }

    @DexIgnore
    public uy<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, ax axVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(byteBuffer), i, i2, axVar);
    }
}
