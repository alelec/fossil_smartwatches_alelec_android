package com.fossil;

import android.os.RemoteException;
import com.fossil.u12;
import com.fossil.y12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v42 extends e42<Boolean> {
    @DexIgnore
    public /* final */ y12.a<?> c;

    @DexIgnore
    public v42(y12.a<?> aVar, oo3<Boolean> oo3) {
        super(4, oo3);
        this.c = aVar;
    }

    @DexIgnore
    @Override // com.fossil.i32
    public final /* bridge */ /* synthetic */ void a(j22 j22, boolean z) {
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final k02[] b(u12.a<?> aVar) {
        d42 d42 = aVar.k().get(this.c);
        if (d42 == null) {
            return null;
        }
        return d42.a.c();
    }

    @DexIgnore
    @Override // com.fossil.s42
    public final boolean c(u12.a<?> aVar) {
        d42 d42 = aVar.k().get(this.c);
        return d42 != null && d42.a.d();
    }

    @DexIgnore
    @Override // com.fossil.e42
    public final void d(u12.a<?> aVar) throws RemoteException {
        d42 remove = aVar.k().remove(this.c);
        if (remove != null) {
            remove.b.a(aVar.r(), ((e42) this).b);
            remove.a.a();
            return;
        }
        ((e42) this).b.b((T) false);
    }
}
