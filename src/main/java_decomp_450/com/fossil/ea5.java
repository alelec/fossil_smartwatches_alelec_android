package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ea5 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;

    @DexIgnore
    public ea5(Object obj, View view, int i, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
        this.q = flexibleTextView;
    }

    @DexIgnore
    public static ea5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, qb.a());
    }

    @DexIgnore
    @Deprecated
    public static ea5 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return (ea5) ViewDataBinding.a(layoutInflater, 2131558705, viewGroup, z, obj);
    }
}
