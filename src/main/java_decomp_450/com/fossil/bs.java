package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bs implements yr<Uri, Uri> {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public bs(Context context) {
        ee7.b(context, "context");
        this.a = context;
    }

    @DexIgnore
    public boolean a(Uri uri) {
        ee7.b(uri, "data");
        if (ee7.a((Object) uri.getScheme(), (Object) "android.resource")) {
            String authority = uri.getAuthority();
            if (!(authority == null || mh7.a(authority))) {
                List<String> pathSegments = uri.getPathSegments();
                ee7.a((Object) pathSegments, "data.pathSegments");
                if (pathSegments.size() == 2) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public Uri b(Uri uri) {
        ee7.b(uri, "data");
        String authority = uri.getAuthority();
        if (authority == null) {
            authority = "";
        }
        Resources resourcesForApplication = this.a.getPackageManager().getResourcesForApplication(authority);
        List<String> pathSegments = uri.getPathSegments();
        boolean z = false;
        int identifier = resourcesForApplication.getIdentifier(pathSegments.get(1), pathSegments.get(0), authority);
        if (identifier != 0) {
            z = true;
        }
        if (z) {
            Uri parse = Uri.parse("android.resource://" + authority + '/' + identifier);
            ee7.a((Object) parse, "Uri.parse(this)");
            return parse;
        }
        throw new IllegalStateException(("Invalid android.resource URI: " + uri).toString());
    }
}
