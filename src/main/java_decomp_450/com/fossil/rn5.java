package com.fossil;

import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn5 extends fl4<c, d, b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            ee7.b(str, "errorMessage");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public d(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase", f = "DownloadUserInfoUseCase.kt", l = {22}, m = "run")
    public static final class e extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ rn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(rn5 rn5, fb7 fb7) {
            super(fb7);
            this.this$0 = rn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((c) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        new a(null);
        String simpleName = rn5.class.getSimpleName();
        ee7.a((Object) simpleName, "DownloadUserInfoUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public rn5(UserRepository userRepository) {
        ee7.b(userRepository, "userRepository");
        this.d = userRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(c cVar, fb7 fb7) {
        return a(cVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.rn5.c r6, com.fossil.fb7<java.lang.Object> r7) {
        /*
            r5 = this;
            boolean r0 = r7 instanceof com.fossil.rn5.e
            if (r0 == 0) goto L_0x0013
            r0 = r7
            com.fossil.rn5$e r0 = (com.fossil.rn5.e) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.fossil.rn5$e r0 = new com.fossil.rn5$e
            r0.<init>(r5, r7)
        L_0x0018:
            java.lang.Object r7 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            if (r2 == 0) goto L_0x0039
            if (r2 != r3) goto L_0x0031
            java.lang.Object r6 = r0.L$1
            com.fossil.rn5$c r6 = (com.fossil.rn5.c) r6
            java.lang.Object r6 = r0.L$0
            com.fossil.rn5 r6 = (com.fossil.rn5) r6
            com.fossil.t87.a(r7)
            goto L_0x0058
        L_0x0031:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException
            java.lang.String r7 = "call to 'resume' before 'invoke' with coroutine"
            r6.<init>(r7)
            throw r6
        L_0x0039:
            com.fossil.t87.a(r7)
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r2 = com.fossil.rn5.e
            java.lang.String r4 = "running UseCase"
            r7.d(r2, r4)
            com.portfolio.platform.data.source.UserRepository r7 = r5.d
            r0.L$0 = r5
            r0.L$1 = r6
            r0.label = r3
            java.lang.Object r7 = r7.loadUserInfo(r0)
            if (r7 != r1) goto L_0x0058
            return r1
        L_0x0058:
            com.fossil.zi5 r7 = (com.fossil.zi5) r7
            boolean r6 = r7 instanceof com.fossil.bj5
            if (r6 == 0) goto L_0x006c
            com.fossil.rn5$d r6 = new com.fossil.rn5$d
            com.fossil.bj5 r7 = (com.fossil.bj5) r7
            java.lang.Object r7 = r7.a()
            com.portfolio.platform.data.model.MFUser r7 = (com.portfolio.platform.data.model.MFUser) r7
            r6.<init>(r7)
            goto L_0x008d
        L_0x006c:
            boolean r6 = r7 instanceof com.fossil.yi5
            if (r6 == 0) goto L_0x008b
            com.fossil.rn5$b r6 = new com.fossil.rn5$b
            com.fossil.yi5 r7 = (com.fossil.yi5) r7
            int r0 = r7.a()
            com.portfolio.platform.data.model.ServerError r7 = r7.c()
            if (r7 == 0) goto L_0x0085
            java.lang.String r7 = r7.getMessage()
            if (r7 == 0) goto L_0x0085
            goto L_0x0087
        L_0x0085:
            java.lang.String r7 = ""
        L_0x0087:
            r6.<init>(r0, r7)
            goto L_0x008d
        L_0x008b:
            com.fossil.i97 r6 = com.fossil.i97.a
        L_0x008d:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.rn5.a(com.fossil.rn5$c, com.fossil.fb7):java.lang.Object");
    }
}
