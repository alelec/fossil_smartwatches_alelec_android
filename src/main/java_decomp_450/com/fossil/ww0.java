package com.fossil;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ww0 extends f01 {
    @DexIgnore
    public static /* final */ ev0 CREATOR; // = new ev0(null);
    @DexIgnore
    public /* final */ boolean c;

    @DexIgnore
    public ww0(rg0 rg0, r60 r60, boolean z) {
        super(rg0, r60);
        this.c = z;
    }

    @DexIgnore
    @Override // com.fossil.k60, com.fossil.f01
    public JSONObject a() {
        return yz0.a(super.a(), r51.u3, Integer.valueOf(this.c ? 1 : 0));
    }

    @DexIgnore
    @Override // com.fossil.f01
    public List<pe1> b() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new cm1());
        if (this.c) {
            arrayList.add(new lr0(new e91[]{new e91(di0.b, ei1.c, ak0.POSITION, xl0.b, 0), new e91(di0.b, ei1.c, ak0.POSITION, xl0.b, 0)}));
        }
        arrayList.add(new rp0(tn0.b));
        arrayList.add(new cv0());
        return arrayList;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    @Override // com.fossil.f01
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(ww0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.c == ((ww0) obj).c;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.response.MicroAppErrorResponse");
    }

    @DexIgnore
    @Override // com.fossil.f01
    public int hashCode() {
        return Boolean.valueOf(this.c).hashCode();
    }

    @DexIgnore
    @Override // com.fossil.f01
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.c ? 1 : 0);
        }
    }

    @DexIgnore
    public /* synthetic */ ww0(Parcel parcel, zd7 zd7) {
        super(parcel);
        this.c = parcel.readInt() != 0;
    }
}
