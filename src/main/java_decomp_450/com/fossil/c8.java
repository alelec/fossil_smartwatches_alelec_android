package com.fossil;

import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcelable;
import android.util.Size;
import android.util.SizeF;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c8 {
    @DexIgnore
    public static final Bundle a(r87<String, ? extends Object>... r87Arr) {
        ee7.b(r87Arr, "pairs");
        Bundle bundle = new Bundle(r87Arr.length);
        for (r87<String, ? extends Object> r87 : r87Arr) {
            String component1 = r87.component1();
            Object component2 = r87.component2();
            if (component2 == null) {
                bundle.putString(component1, null);
            } else if (component2 instanceof Boolean) {
                bundle.putBoolean(component1, ((Boolean) component2).booleanValue());
            } else if (component2 instanceof Byte) {
                bundle.putByte(component1, ((Number) component2).byteValue());
            } else if (component2 instanceof Character) {
                bundle.putChar(component1, ((Character) component2).charValue());
            } else if (component2 instanceof Double) {
                bundle.putDouble(component1, ((Number) component2).doubleValue());
            } else if (component2 instanceof Float) {
                bundle.putFloat(component1, ((Number) component2).floatValue());
            } else if (component2 instanceof Integer) {
                bundle.putInt(component1, ((Number) component2).intValue());
            } else if (component2 instanceof Long) {
                bundle.putLong(component1, ((Number) component2).longValue());
            } else if (component2 instanceof Short) {
                bundle.putShort(component1, ((Number) component2).shortValue());
            } else if (component2 instanceof Bundle) {
                bundle.putBundle(component1, (Bundle) component2);
            } else if (component2 instanceof CharSequence) {
                bundle.putCharSequence(component1, (CharSequence) component2);
            } else if (component2 instanceof Parcelable) {
                bundle.putParcelable(component1, (Parcelable) component2);
            } else if (component2 instanceof boolean[]) {
                bundle.putBooleanArray(component1, (boolean[]) component2);
            } else if (component2 instanceof byte[]) {
                bundle.putByteArray(component1, (byte[]) component2);
            } else if (component2 instanceof char[]) {
                bundle.putCharArray(component1, (char[]) component2);
            } else if (component2 instanceof double[]) {
                bundle.putDoubleArray(component1, (double[]) component2);
            } else if (component2 instanceof float[]) {
                bundle.putFloatArray(component1, (float[]) component2);
            } else if (component2 instanceof int[]) {
                bundle.putIntArray(component1, (int[]) component2);
            } else if (component2 instanceof long[]) {
                bundle.putLongArray(component1, (long[]) component2);
            } else if (component2 instanceof short[]) {
                bundle.putShortArray(component1, (short[]) component2);
            } else if (component2 instanceof Object[]) {
                Class<?> componentType = component2.getClass().getComponentType();
                if (componentType != null) {
                    ee7.a((Object) componentType, "value::class.java.componentType!!");
                    if (Parcelable.class.isAssignableFrom(componentType)) {
                        if (component2 != null) {
                            bundle.putParcelableArray(component1, (Parcelable[]) component2);
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<android.os.Parcelable>");
                        }
                    } else if (String.class.isAssignableFrom(componentType)) {
                        if (component2 != null) {
                            bundle.putStringArray(component1, (String[]) component2);
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                        }
                    } else if (CharSequence.class.isAssignableFrom(componentType)) {
                        if (component2 != null) {
                            bundle.putCharSequenceArray(component1, (CharSequence[]) component2);
                        } else {
                            throw new x87("null cannot be cast to non-null type kotlin.Array<kotlin.CharSequence>");
                        }
                    } else if (Serializable.class.isAssignableFrom(componentType)) {
                        bundle.putSerializable(component1, (Serializable) component2);
                    } else {
                        throw new IllegalArgumentException("Illegal value array type " + componentType.getCanonicalName() + " for key \"" + component1 + '\"');
                    }
                } else {
                    ee7.a();
                    throw null;
                }
            } else if (component2 instanceof Serializable) {
                bundle.putSerializable(component1, (Serializable) component2);
            } else if (Build.VERSION.SDK_INT >= 18 && (component2 instanceof Binder)) {
                bundle.putBinder(component1, (IBinder) component2);
            } else if (Build.VERSION.SDK_INT >= 21 && (component2 instanceof Size)) {
                bundle.putSize(component1, (Size) component2);
            } else if (Build.VERSION.SDK_INT < 21 || !(component2 instanceof SizeF)) {
                throw new IllegalArgumentException("Illegal value type " + component2.getClass().getCanonicalName() + " for key \"" + component1 + '\"');
            } else {
                bundle.putSizeF(component1, (SizeF) component2);
            }
        }
        return bundle;
    }
}
