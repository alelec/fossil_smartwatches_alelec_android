package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.ab2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p43 extends kp2 implements q43 {
    @DexIgnore
    public p43() {
        super("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    @DexIgnore
    public static q43 asInterface(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
        if (queryLocalInterface instanceof q43) {
            return (q43) queryLocalInterface;
        }
        return new s43(iBinder);
    }

    @DexIgnore
    @Override // com.fossil.kp2
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        r43 r43;
        r43 r432;
        r43 r433 = null;
        r43 r434 = null;
        r43 r435 = null;
        nn2 nn2 = null;
        nn2 nn22 = null;
        nn2 nn23 = null;
        r43 r436 = null;
        r43 r437 = null;
        r43 r438 = null;
        r43 r439 = null;
        r43 r4310 = null;
        r43 r4311 = null;
        on2 on2 = null;
        r43 r4312 = null;
        r43 r4313 = null;
        r43 r4314 = null;
        r43 r4315 = null;
        switch (i) {
            case 1:
                initialize(ab2.a.a(parcel.readStrongBinder()), (qn2) jo2.a(parcel, qn2.CREATOR), parcel.readLong());
                break;
            case 2:
                logEvent(parcel.readString(), parcel.readString(), (Bundle) jo2.a(parcel, Bundle.CREATOR), jo2.a(parcel), jo2.a(parcel), parcel.readLong());
                break;
            case 3:
                String readString = parcel.readString();
                String readString2 = parcel.readString();
                Bundle bundle = (Bundle) jo2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder = parcel.readStrongBinder();
                if (readStrongBinder == null) {
                    r43 = null;
                } else {
                    IInterface queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface instanceof r43) {
                        r432 = (r43) queryLocalInterface;
                    } else {
                        r432 = new t43(readStrongBinder);
                    }
                    r43 = r432;
                }
                logEventAndBundle(readString, readString2, bundle, r43, parcel.readLong());
                break;
            case 4:
                setUserProperty(parcel.readString(), parcel.readString(), ab2.a.a(parcel.readStrongBinder()), jo2.a(parcel), parcel.readLong());
                break;
            case 5:
                String readString3 = parcel.readString();
                String readString4 = parcel.readString();
                boolean a = jo2.a(parcel);
                IBinder readStrongBinder2 = parcel.readStrongBinder();
                if (readStrongBinder2 != null) {
                    IInterface queryLocalInterface2 = readStrongBinder2.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface2 instanceof r43) {
                        r433 = (r43) queryLocalInterface2;
                    } else {
                        r433 = new t43(readStrongBinder2);
                    }
                }
                getUserProperties(readString3, readString4, a, r433);
                break;
            case 6:
                String readString5 = parcel.readString();
                IBinder readStrongBinder3 = parcel.readStrongBinder();
                if (readStrongBinder3 != null) {
                    IInterface queryLocalInterface3 = readStrongBinder3.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface3 instanceof r43) {
                        r4315 = (r43) queryLocalInterface3;
                    } else {
                        r4315 = new t43(readStrongBinder3);
                    }
                }
                getMaxUserProperties(readString5, r4315);
                break;
            case 7:
                setUserId(parcel.readString(), parcel.readLong());
                break;
            case 8:
                setConditionalUserProperty((Bundle) jo2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 9:
                clearConditionalUserProperty(parcel.readString(), parcel.readString(), (Bundle) jo2.a(parcel, Bundle.CREATOR));
                break;
            case 10:
                String readString6 = parcel.readString();
                String readString7 = parcel.readString();
                IBinder readStrongBinder4 = parcel.readStrongBinder();
                if (readStrongBinder4 != null) {
                    IInterface queryLocalInterface4 = readStrongBinder4.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface4 instanceof r43) {
                        r4314 = (r43) queryLocalInterface4;
                    } else {
                        r4314 = new t43(readStrongBinder4);
                    }
                }
                getConditionalUserProperties(readString6, readString7, r4314);
                break;
            case 11:
                setMeasurementEnabled(jo2.a(parcel), parcel.readLong());
                break;
            case 12:
                resetAnalyticsData(parcel.readLong());
                break;
            case 13:
                setMinimumSessionDuration(parcel.readLong());
                break;
            case 14:
                setSessionTimeoutDuration(parcel.readLong());
                break;
            case 15:
                setCurrentScreen(ab2.a.a(parcel.readStrongBinder()), parcel.readString(), parcel.readString(), parcel.readLong());
                break;
            case 16:
                IBinder readStrongBinder5 = parcel.readStrongBinder();
                if (readStrongBinder5 != null) {
                    IInterface queryLocalInterface5 = readStrongBinder5.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface5 instanceof r43) {
                        r4313 = (r43) queryLocalInterface5;
                    } else {
                        r4313 = new t43(readStrongBinder5);
                    }
                }
                getCurrentScreenName(r4313);
                break;
            case 17:
                IBinder readStrongBinder6 = parcel.readStrongBinder();
                if (readStrongBinder6 != null) {
                    IInterface queryLocalInterface6 = readStrongBinder6.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface6 instanceof r43) {
                        r4312 = (r43) queryLocalInterface6;
                    } else {
                        r4312 = new t43(readStrongBinder6);
                    }
                }
                getCurrentScreenClass(r4312);
                break;
            case 18:
                IBinder readStrongBinder7 = parcel.readStrongBinder();
                if (readStrongBinder7 != null) {
                    IInterface queryLocalInterface7 = readStrongBinder7.queryLocalInterface("com.google.android.gms.measurement.api.internal.IStringProvider");
                    if (queryLocalInterface7 instanceof on2) {
                        on2 = (on2) queryLocalInterface7;
                    } else {
                        on2 = new rn2(readStrongBinder7);
                    }
                }
                setInstanceIdProvider(on2);
                break;
            case 19:
                IBinder readStrongBinder8 = parcel.readStrongBinder();
                if (readStrongBinder8 != null) {
                    IInterface queryLocalInterface8 = readStrongBinder8.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface8 instanceof r43) {
                        r4311 = (r43) queryLocalInterface8;
                    } else {
                        r4311 = new t43(readStrongBinder8);
                    }
                }
                getCachedAppInstanceId(r4311);
                break;
            case 20:
                IBinder readStrongBinder9 = parcel.readStrongBinder();
                if (readStrongBinder9 != null) {
                    IInterface queryLocalInterface9 = readStrongBinder9.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface9 instanceof r43) {
                        r4310 = (r43) queryLocalInterface9;
                    } else {
                        r4310 = new t43(readStrongBinder9);
                    }
                }
                getAppInstanceId(r4310);
                break;
            case 21:
                IBinder readStrongBinder10 = parcel.readStrongBinder();
                if (readStrongBinder10 != null) {
                    IInterface queryLocalInterface10 = readStrongBinder10.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface10 instanceof r43) {
                        r439 = (r43) queryLocalInterface10;
                    } else {
                        r439 = new t43(readStrongBinder10);
                    }
                }
                getGmpAppId(r439);
                break;
            case 22:
                IBinder readStrongBinder11 = parcel.readStrongBinder();
                if (readStrongBinder11 != null) {
                    IInterface queryLocalInterface11 = readStrongBinder11.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface11 instanceof r43) {
                        r438 = (r43) queryLocalInterface11;
                    } else {
                        r438 = new t43(readStrongBinder11);
                    }
                }
                generateEventId(r438);
                break;
            case 23:
                beginAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 24:
                endAdUnitExposure(parcel.readString(), parcel.readLong());
                break;
            case 25:
                onActivityStarted(ab2.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 26:
                onActivityStopped(ab2.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 27:
                onActivityCreated(ab2.a.a(parcel.readStrongBinder()), (Bundle) jo2.a(parcel, Bundle.CREATOR), parcel.readLong());
                break;
            case 28:
                onActivityDestroyed(ab2.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 29:
                onActivityPaused(ab2.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 30:
                onActivityResumed(ab2.a.a(parcel.readStrongBinder()), parcel.readLong());
                break;
            case 31:
                ab2 a2 = ab2.a.a(parcel.readStrongBinder());
                IBinder readStrongBinder12 = parcel.readStrongBinder();
                if (readStrongBinder12 != null) {
                    IInterface queryLocalInterface12 = readStrongBinder12.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface12 instanceof r43) {
                        r437 = (r43) queryLocalInterface12;
                    } else {
                        r437 = new t43(readStrongBinder12);
                    }
                }
                onActivitySaveInstanceState(a2, r437, parcel.readLong());
                break;
            case 32:
                Bundle bundle2 = (Bundle) jo2.a(parcel, Bundle.CREATOR);
                IBinder readStrongBinder13 = parcel.readStrongBinder();
                if (readStrongBinder13 != null) {
                    IInterface queryLocalInterface13 = readStrongBinder13.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface13 instanceof r43) {
                        r436 = (r43) queryLocalInterface13;
                    } else {
                        r436 = new t43(readStrongBinder13);
                    }
                }
                performAction(bundle2, r436, parcel.readLong());
                break;
            case 33:
                logHealthData(parcel.readInt(), parcel.readString(), ab2.a.a(parcel.readStrongBinder()), ab2.a.a(parcel.readStrongBinder()), ab2.a.a(parcel.readStrongBinder()));
                break;
            case 34:
                IBinder readStrongBinder14 = parcel.readStrongBinder();
                if (readStrongBinder14 != null) {
                    IInterface queryLocalInterface14 = readStrongBinder14.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface14 instanceof nn2) {
                        nn23 = (nn2) queryLocalInterface14;
                    } else {
                        nn23 = new pn2(readStrongBinder14);
                    }
                }
                setEventInterceptor(nn23);
                break;
            case 35:
                IBinder readStrongBinder15 = parcel.readStrongBinder();
                if (readStrongBinder15 != null) {
                    IInterface queryLocalInterface15 = readStrongBinder15.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface15 instanceof nn2) {
                        nn22 = (nn2) queryLocalInterface15;
                    } else {
                        nn22 = new pn2(readStrongBinder15);
                    }
                }
                registerOnMeasurementEventListener(nn22);
                break;
            case 36:
                IBinder readStrongBinder16 = parcel.readStrongBinder();
                if (readStrongBinder16 != null) {
                    IInterface queryLocalInterface16 = readStrongBinder16.queryLocalInterface("com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
                    if (queryLocalInterface16 instanceof nn2) {
                        nn2 = (nn2) queryLocalInterface16;
                    } else {
                        nn2 = new pn2(readStrongBinder16);
                    }
                }
                unregisterOnMeasurementEventListener(nn2);
                break;
            case 37:
                initForTests(jo2.b(parcel));
                break;
            case 38:
                IBinder readStrongBinder17 = parcel.readStrongBinder();
                if (readStrongBinder17 != null) {
                    IInterface queryLocalInterface17 = readStrongBinder17.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface17 instanceof r43) {
                        r435 = (r43) queryLocalInterface17;
                    } else {
                        r435 = new t43(readStrongBinder17);
                    }
                }
                getTestFlag(r435, parcel.readInt());
                break;
            case 39:
                setDataCollectionEnabled(jo2.a(parcel));
                break;
            case 40:
                IBinder readStrongBinder18 = parcel.readStrongBinder();
                if (readStrongBinder18 != null) {
                    IInterface queryLocalInterface18 = readStrongBinder18.queryLocalInterface("com.google.android.gms.measurement.api.internal.IBundleReceiver");
                    if (queryLocalInterface18 instanceof r43) {
                        r434 = (r43) queryLocalInterface18;
                    } else {
                        r434 = new t43(readStrongBinder18);
                    }
                }
                isDataCollectionEnabled(r434);
                break;
            case 41:
            default:
                return false;
            case 42:
                setDefaultEventParameters((Bundle) jo2.a(parcel, Bundle.CREATOR));
                break;
        }
        parcel2.writeNoException();
        return true;
    }
}
