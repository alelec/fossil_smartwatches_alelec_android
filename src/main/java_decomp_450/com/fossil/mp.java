package com.fossil;

import androidx.work.WorkerParameters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mp implements Runnable {
    @DexIgnore
    public dn a;
    @DexIgnore
    public String b;
    @DexIgnore
    public WorkerParameters.a c;

    @DexIgnore
    public mp(dn dnVar, String str, WorkerParameters.a aVar) {
        this.a = dnVar;
        this.b = str;
        this.c = aVar;
    }

    @DexIgnore
    public void run() {
        this.a.d().a(this.b, this.c);
    }
}
