package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zq0 extends k60 {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public qk1 b;
    @DexIgnore
    public byte[] c;
    @DexIgnore
    public /* final */ JSONObject d;

    @DexIgnore
    public /* synthetic */ zq0(long j, qk1 qk1, byte[] bArr, JSONObject jSONObject, int i) {
        j = (i & 1) != 0 ? System.currentTimeMillis() : j;
        qk1 = (i & 2) != 0 ? null : qk1;
        bArr = (i & 4) != 0 ? new byte[0] : bArr;
        jSONObject = (i & 8) != 0 ? new JSONObject() : jSONObject;
        this.a = j;
        this.b = qk1;
        this.c = bArr;
        this.d = jSONObject;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        JSONObject a2 = yz0.a(new JSONObject(), r51.O0, Double.valueOf(yz0.a(this.a)));
        r51 r51 = r51.P0;
        qk1 qk1 = this.b;
        return yz0.a(yz0.a(yz0.a(a2, r51, qk1 != null ? qk1.a : null), r51.Q0, yz0.a(this.c, (String) null, 1)), this.d);
    }
}
