package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.y62;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t93 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<t93> CREATOR; // = new ha3();
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public float a;
        @DexIgnore
        public float b;

        @DexIgnore
        public final a a(float f) {
            this.a = f;
            return this;
        }

        @DexIgnore
        public final a b(float f) {
            this.b = f;
            return this;
        }

        @DexIgnore
        public final t93 a() {
            return new t93(this.b, this.a);
        }
    }

    @DexIgnore
    public t93(float f, float f2) {
        boolean z = -90.0f <= f && f <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f);
        a72.a(z, sb.toString());
        this.a = f + LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.b = (((double) f2) <= 0.0d ? (f2 % 360.0f) + 360.0f : f2) % 360.0f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof t93)) {
            return false;
        }
        t93 t93 = (t93) obj;
        return Float.floatToIntBits(this.a) == Float.floatToIntBits(t93.a) && Float.floatToIntBits(this.b) == Float.floatToIntBits(t93.b);
    }

    @DexIgnore
    public int hashCode() {
        return y62.a(Float.valueOf(this.a), Float.valueOf(this.b));
    }

    @DexIgnore
    public String toString() {
        y62.a a2 = y62.a(this);
        a2.a("tilt", Float.valueOf(this.a));
        a2.a("bearing", Float.valueOf(this.b));
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 2, this.a);
        k72.a(parcel, 3, this.b);
        k72.a(parcel, a2);
    }
}
