package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import com.facebook.LegacyTokenHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g72 {
    @DexIgnore
    public /* final */ Resources a;
    @DexIgnore
    public /* final */ String b;

    @DexIgnore
    public g72(Context context) {
        a72.a(context);
        Resources resources = context.getResources();
        this.a = resources;
        this.b = resources.getResourcePackageName(s02.common_google_play_services_unknown_issue);
    }

    @DexIgnore
    public String a(String str) {
        int identifier = this.a.getIdentifier(str, LegacyTokenHelper.TYPE_STRING, this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
