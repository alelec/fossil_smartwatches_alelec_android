package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import com.fossil.p1;
import com.fossil.v1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface p2 {
    @DexIgnore
    ha a(int i, long j);

    @DexIgnore
    void a(int i);

    @DexIgnore
    void a(Menu menu, v1.a aVar);

    @DexIgnore
    void a(v1.a aVar, p1.a aVar2);

    @DexIgnore
    void a(z2 z2Var);

    @DexIgnore
    void a(boolean z);

    @DexIgnore
    boolean a();

    @DexIgnore
    void b();

    @DexIgnore
    void b(int i);

    @DexIgnore
    void b(boolean z);

    @DexIgnore
    boolean c();

    @DexIgnore
    void collapseActionView();

    @DexIgnore
    boolean d();

    @DexIgnore
    boolean e();

    @DexIgnore
    boolean f();

    @DexIgnore
    void g();

    @DexIgnore
    Context getContext();

    @DexIgnore
    CharSequence getTitle();

    @DexIgnore
    boolean h();

    @DexIgnore
    Menu i();

    @DexIgnore
    int j();

    @DexIgnore
    ViewGroup k();

    @DexIgnore
    int l();

    @DexIgnore
    void m();

    @DexIgnore
    void n();

    @DexIgnore
    void setIcon(int i);

    @DexIgnore
    void setIcon(Drawable drawable);

    @DexIgnore
    void setTitle(CharSequence charSequence);

    @DexIgnore
    void setVisibility(int i);

    @DexIgnore
    void setWindowCallback(Window.Callback callback);

    @DexIgnore
    void setWindowTitle(CharSequence charSequence);
}
