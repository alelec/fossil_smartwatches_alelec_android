package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum x37 {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    
    @DexIgnore
    public int a;

    @DexIgnore
    public x37(int i) {
        this.a = i;
    }

    @DexIgnore
    public static x37 getStatReportStrategy(int i) {
        x37[] values = values();
        for (x37 x37 : values) {
            if (i == x37.a()) {
                return x37;
            }
        }
        return null;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }
}
