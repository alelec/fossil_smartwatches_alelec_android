package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.fossil.ql4;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tl4 implements sl4 {
    @DexIgnore
    public static /* final */ int c;
    @DexIgnore
    public static /* final */ int d;
    @DexIgnore
    public static /* final */ int e; // = ((c * 2) + 1);
    @DexIgnore
    public static /* final */ ThreadFactory f; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> g; // = new LinkedBlockingQueue(Integer.MAX_VALUE);
    @DexIgnore
    public /* final */ Handler a; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ ThreadPoolExecutor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ql4.d a;
        @DexIgnore
        public /* final */ /* synthetic */ ql4.a b;

        @DexIgnore
        public b(tl4 tl4, ql4.d dVar, ql4.a aVar) {
            this.a = dVar;
            this.b = aVar;
        }

        @DexIgnore
        public void run() {
            ql4.d dVar = this.a;
            if (dVar != null) {
                dVar.a(this.b);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ql4.d a;
        @DexIgnore
        public /* final */ /* synthetic */ ql4.c b;

        @DexIgnore
        public c(tl4 tl4, ql4.d dVar, ql4.c cVar) {
            this.a = dVar;
            this.b = cVar;
        }

        @DexIgnore
        public void run() {
            ql4.d dVar = this.a;
            if (dVar != null) {
                dVar.onSuccess(this.b);
            }
        }
    }

    /*
    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        c = availableProcessors;
        d = Math.max(2, Math.min(availableProcessors - 1, 4));
    }
    */

    @DexIgnore
    public tl4() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(d, e, 30, TimeUnit.SECONDS, g, f);
        this.b = threadPoolExecutor;
        threadPoolExecutor.allowCoreThreadTimeOut(true);
    }

    @DexIgnore
    @Override // com.fossil.sl4
    public <P extends ql4.c, E extends ql4.a> void a(E e2, ql4.d<P, E> dVar) {
        this.a.post(new b(this, dVar, e2));
    }

    @DexIgnore
    @Override // com.fossil.sl4
    public void execute(Runnable runnable) {
        this.b.execute(runnable);
    }

    @DexIgnore
    @Override // com.fossil.sl4
    public <P extends ql4.c, E extends ql4.a> void a(P p, ql4.d<P, E> dVar) {
        this.a.post(new c(this, dVar, p));
    }
}
