package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bm6 implements Factory<am6> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public bm6(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static bm6 a(Provider<ThemeRepository> provider) {
        return new bm6(provider);
    }

    @DexIgnore
    public static am6 a(ThemeRepository themeRepository) {
        return new am6(themeRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public am6 get() {
        return a(this.a.get());
    }
}
