package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vk4 implements Factory<rl4> {
    @DexIgnore
    public /* final */ wj4 a;

    @DexIgnore
    public vk4(wj4 wj4) {
        this.a = wj4;
    }

    @DexIgnore
    public static vk4 a(wj4 wj4) {
        return new vk4(wj4);
    }

    @DexIgnore
    public static rl4 b(wj4 wj4) {
        rl4 m = wj4.m();
        c87.a(m, "Cannot return null from a non-@Nullable @Provides method");
        return m;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rl4 get() {
        return b(this.a);
    }
}
