package com.fossil;

import androidx.collection.SimpleArrayMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m50<K, V> extends n4<K, V> {
    @DexIgnore
    public int i;

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public V a(int i2, V v) {
        this.i = 0;
        return (V) super.a(i2, v);
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public void clear() {
        this.i = 0;
        super.clear();
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public V d(int i2) {
        this.i = 0;
        return (V) super.d(i2);
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public int hashCode() {
        if (this.i == 0) {
            this.i = super.hashCode();
        }
        return this.i;
    }

    @DexIgnore
    @Override // java.util.Map, androidx.collection.SimpleArrayMap
    public V put(K k, V v) {
        this.i = 0;
        return (V) super.put(k, v);
    }

    @DexIgnore
    @Override // androidx.collection.SimpleArrayMap
    public void a(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.i = 0;
        super.a(simpleArrayMap);
    }
}
