package com.fossil;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl5 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public int a;
    @DexIgnore
    public List<? extends ll5> b;
    @DexIgnore
    public e c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public /* final */ /* synthetic */ kl5 b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.kl5$a$a")
        /* renamed from: com.fossil.kl5$a$a  reason: collision with other inner class name */
        public static final class View$OnClickListenerC0098a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public View$OnClickListenerC0098a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<ll5> d = this.a.b.d();
                    if (d != null) {
                        ll5 ll5 = d.get(adapterPosition);
                        e c = this.a.b.c();
                        if (c != null) {
                            c.a(ll5.a(), this.a.b.e(), adapterPosition, ll5.b(), null);
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public b(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<ll5> d = this.a.b.d();
                if (d != null) {
                    ll5 ll5 = d.get(adapterPosition);
                    e c = this.a.b.c();
                    if (c == null) {
                        return true;
                    }
                    c.b(ll5.a(), this.a.b.e(), adapterPosition, ll5.b(), null);
                    return true;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(kl5 kl5, View view) {
            super(view);
            ee7.b(view, "itemView");
            this.b = kl5;
            view.setOnClickListener(new View$OnClickListenerC0098a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363241);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public List<String> a; // = new ArrayList();
        @DexIgnore
        public TextView b;
        @DexIgnore
        public Spinner c;
        @DexIgnore
        public Button d;
        @DexIgnore
        public /* final */ /* synthetic */ kl5 e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<ll5> d = this.a.e.d();
                    if (d != null) {
                        ll5 ll5 = d.get(adapterPosition);
                        e c = this.a.e.c();
                        if (c != null) {
                            String a2 = ll5.a();
                            int e = this.a.e.e();
                            Object b = ll5.b();
                            Bundle bundle = new Bundle();
                            bundle.putInt("DEBUG_BUNDLE_SPINNER_SELECTED_POS", this.a.c.getSelectedItemPosition());
                            c.a(a2, e, adapterPosition, b, bundle);
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(kl5 kl5, View view) {
            super(view);
            ee7.b(view, "itemView");
            this.e = kl5;
            View findViewById = view.findViewById(2131363343);
            if (findViewById != null) {
                this.b = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363075);
                if (findViewById2 != null) {
                    this.c = (Spinner) findViewById2;
                    View findViewById3 = view.findViewById(2131361964);
                    if (findViewById3 != null) {
                        Button button = (Button) findViewById3;
                        button.setOnClickListener(new a(this));
                        this.d = button;
                        return;
                    }
                    ee7.a();
                    throw null;
                }
                throw new x87("null cannot be cast to non-null type android.widget.Spinner");
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public final TextView b() {
            return this.b;
        }

        @DexIgnore
        public final void c() {
            View view = ((RecyclerView.ViewHolder) this).itemView;
            ee7.a((Object) view, "itemView");
            ArrayAdapter arrayAdapter = new ArrayAdapter(view.getContext(), 17367048, this.a);
            arrayAdapter.setDropDownViewResource(17367049);
            this.c.setAdapter((SpinnerAdapter) arrayAdapter);
        }

        @DexIgnore
        public final void a(List<String> list) {
            ee7.b(list, "value");
            this.a = list;
            c();
        }

        @DexIgnore
        public final Button a() {
            return this.d;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public SwitchCompat b;
        @DexIgnore
        public /* final */ /* synthetic */ kl5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ SwitchCompat a;
            @DexIgnore
            public /* final */ /* synthetic */ c b;

            @DexIgnore
            public a(SwitchCompat switchCompat, c cVar) {
                this.a = switchCompat;
                this.b = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.b.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<ll5> d = this.b.c.d();
                    if (d != null) {
                        ll5 ll5 = d.get(adapterPosition);
                        if (ll5 != null) {
                            ((nl5) ll5).a(this.a.isChecked());
                            e c = this.b.c.c();
                            if (c != null) {
                                String a2 = ll5.a();
                                int e = this.b.c.e();
                                Object b2 = ll5.b();
                                Bundle bundle = new Bundle();
                                List<ll5> d2 = this.b.c.d();
                                if (d2 != null) {
                                    ll5 ll52 = d2.get(adapterPosition);
                                    if (ll52 != null) {
                                        bundle.putBoolean("DEBUG_BUNDLE_IS_CHECKED", ((nl5) ll52).d());
                                        c.a(a2, e, adapterPosition, b2, bundle);
                                    } else {
                                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                                    }
                                } else {
                                    ee7.a();
                                    throw null;
                                }
                            }
                            this.b.c.notifyItemChanged(adapterPosition);
                            return;
                        }
                        throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(kl5 kl5, View view) {
            super(view);
            ee7.b(view, "itemView");
            this.c = kl5;
            View findViewById = view.findViewById(2131363242);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363020);
                if (findViewById2 != null) {
                    SwitchCompat switchCompat = (SwitchCompat) findViewById2;
                    switchCompat.setOnClickListener(new a(switchCompat, this));
                    this.b = switchCompat;
                    return;
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        public final SwitchCompat a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public TextView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ kl5 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List<ll5> d = this.a.c.d();
                    if (d != null) {
                        ll5 ll5 = d.get(adapterPosition);
                        e c = this.a.c.c();
                        if (c != null) {
                            c.a(ll5.a(), this.a.c.e(), adapterPosition, ll5.b(), null);
                            return;
                        }
                        return;
                    }
                    ee7.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnLongClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public b(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final boolean onLongClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition == -1) {
                    return true;
                }
                List<ll5> d = this.a.c.d();
                if (d != null) {
                    ll5 ll5 = d.get(adapterPosition);
                    e c = this.a.c.c();
                    if (c == null) {
                        return true;
                    }
                    c.b(ll5.a(), this.a.c.e(), adapterPosition, ll5.b(), null);
                    return true;
                }
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(kl5 kl5, View view) {
            super(view);
            ee7.b(view, "itemView");
            this.c = kl5;
            view.setOnClickListener(new a(this));
            view.setOnLongClickListener(new b(this));
            View findViewById = view.findViewById(2131363243);
            if (findViewById != null) {
                this.a = (TextView) findViewById;
                View findViewById2 = view.findViewById(2131363244);
                if (findViewById2 != null) {
                    this.b = (TextView) findViewById2;
                } else {
                    ee7.a();
                    throw null;
                }
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final TextView a() {
            return this.b;
        }

        @DexIgnore
        public final TextView b() {
            return this.a;
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(String str, int i, int i2, Object obj, Bundle bundle);

        @DexIgnore
        void b(String str, int i, int i2, Object obj, Bundle bundle);
    }

    @DexIgnore
    public final void a(int i) {
        this.a = i;
    }

    @DexIgnore
    public final e c() {
        return this.c;
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: java.util.List<? extends com.fossil.ll5>, java.util.List<com.fossil.ll5> */
    public final List<ll5> d() {
        return this.b;
    }

    @DexIgnore
    public final int e() {
        return this.a;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemCount() {
        List<? extends ll5> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public int getItemViewType(int i) {
        List<? extends ll5> list = this.b;
        ll5 ll5 = null;
        if ((list != null ? (ll5) list.get(i) : null) instanceof nl5) {
            return ql5.CHILD_ITEM_WITH_SWITCH.ordinal();
        }
        List<? extends ll5> list2 = this.b;
        if ((list2 != null ? (ll5) list2.get(i) : null) instanceof ol5) {
            return ql5.CHILD_ITEM_WITH_TEXT.ordinal();
        }
        List<? extends ll5> list3 = this.b;
        if (list3 != null) {
            ll5 = (ll5) list3.get(i);
        }
        if (ll5 instanceof ml5) {
            return ql5.CHILD_ITEM_WITH_SPINNER.ordinal();
        }
        return ql5.CHILD.ordinal();
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        ee7.b(viewHolder, "holder");
        if (viewHolder instanceof a) {
            TextView a2 = ((a) viewHolder).a();
            List<? extends ll5> list = this.b;
            if (list != null) {
                a2.setText(((ll5) list.get(i)).c());
            } else {
                ee7.a();
                throw null;
            }
        } else if (viewHolder instanceof c) {
            c cVar = (c) viewHolder;
            TextView b2 = cVar.b();
            List<? extends ll5> list2 = this.b;
            if (list2 != null) {
                b2.setText(((ll5) list2.get(i)).c());
                SwitchCompat a3 = cVar.a();
                List<? extends ll5> list3 = this.b;
                if (list3 != null) {
                    Object obj = list3.get(i);
                    if (obj != null) {
                        a3.setChecked(((nl5) obj).d());
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSwitch");
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        } else if (viewHolder instanceof d) {
            d dVar = (d) viewHolder;
            TextView b3 = dVar.b();
            List<? extends ll5> list4 = this.b;
            if (list4 != null) {
                b3.setText(((ll5) list4.get(i)).c());
                TextView a4 = dVar.a();
                List<? extends ll5> list5 = this.b;
                if (list5 != null) {
                    Object obj2 = list5.get(i);
                    if (obj2 != null) {
                        a4.setText(((ol5) obj2).d());
                        return;
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithText");
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        } else if (viewHolder instanceof b) {
            b bVar = (b) viewHolder;
            TextView b4 = bVar.b();
            List<? extends ll5> list6 = this.b;
            if (list6 != null) {
                b4.setText(((ll5) list6.get(i)).c());
                Button a5 = bVar.a();
                List<? extends ll5> list7 = this.b;
                if (list7 != null) {
                    Object obj3 = list7.get(i);
                    if (obj3 != null) {
                        a5.setText(((ml5) obj3).d());
                        List<? extends ll5> list8 = this.b;
                        if (list8 != null) {
                            Object obj4 = list8.get(i);
                            if (obj4 != null) {
                                bVar.a(((ml5) obj4).e());
                                return;
                            }
                            throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                        }
                        ee7.a();
                        throw null;
                    }
                    throw new x87("null cannot be cast to non-null type com.portfolio.platform.ui.debug.DebugChildItemWithSpinner");
                }
                ee7.a();
                throw null;
            }
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.recyclerview.widget.RecyclerView.g
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ee7.b(viewGroup, "parent");
        if (i == ql5.CHILD.ordinal()) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558673, viewGroup, false);
            ee7.a((Object) inflate, "view");
            return new a(this, inflate);
        } else if (i == ql5.CHILD_ITEM_WITH_SWITCH.ordinal()) {
            View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558675, viewGroup, false);
            ee7.a((Object) inflate2, "view");
            return new c(this, inflate2);
        } else if (i == ql5.CHILD_ITEM_WITH_TEXT.ordinal()) {
            View inflate3 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558676, viewGroup, false);
            ee7.a((Object) inflate3, "view");
            return new d(this, inflate3);
        } else if (i == ql5.CHILD_ITEM_WITH_SPINNER.ordinal()) {
            View inflate4 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558674, viewGroup, false);
            ee7.a((Object) inflate4, "view");
            return new b(this, inflate4);
        } else {
            throw new IllegalArgumentException("viewType is not appropriate");
        }
    }

    @DexIgnore
    public final void a(List<? extends ll5> list) {
        this.b = list;
    }

    @DexIgnore
    public final void a(e eVar) {
        ee7.b(eVar, "itemClickListener");
        this.c = eVar;
    }
}
