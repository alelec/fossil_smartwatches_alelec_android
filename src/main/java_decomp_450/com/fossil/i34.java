package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i34 implements d34 {
    @DexIgnore
    @Override // com.fossil.d34
    public void b(String str, Bundle bundle) {
        z24.a().a("Skipping logging Crashlytics event to Firebase, no Firebase Analytics");
    }
}
