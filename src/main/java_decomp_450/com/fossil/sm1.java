package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sm1<T> extends uk1<T> {
    @DexIgnore
    public /* final */ ng1 d;

    @DexIgnore
    public sm1(pb1 pb1, r60 r60) {
        super(pb1, r60);
        this.d = ng1.CRC32C;
    }

    @DexIgnore
    @Override // com.fossil.uk1
    public ng1 a() {
        return this.d;
    }

    @DexIgnore
    public sm1(pb1 pb1) {
        this(pb1, new r60((byte) 2, (byte) 0));
    }
}
