package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qc5 extends e07 {
    @DexIgnore
    public /* final */ Handler i; // = new Handler(Looper.getMainLooper());

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;

        @DexIgnore
        public a(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public void run() {
            qc5.super.a(this.a);
        }
    }

    @DexIgnore
    public qc5(l07 l07) {
        super(l07);
    }

    @DexIgnore
    @Override // com.fossil.e07
    public void a(Object obj) {
        if (Objects.equals(Looper.myLooper(), Looper.getMainLooper())) {
            super.a(obj);
        } else {
            this.i.post(new a(obj));
        }
    }
}
