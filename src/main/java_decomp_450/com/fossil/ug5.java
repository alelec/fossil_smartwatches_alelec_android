package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ug5 implements MembersInjector<sg5> {
    @DexIgnore
    public static void a(sg5 sg5, DeviceRepository deviceRepository) {
        sg5.a = deviceRepository;
    }

    @DexIgnore
    public static void a(sg5 sg5, ch5 ch5) {
        sg5.b = ch5;
    }
}
