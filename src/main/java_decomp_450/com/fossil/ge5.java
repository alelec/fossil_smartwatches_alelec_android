package com.fossil;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.facebook.share.internal.VideoUploader;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ge5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public static /* final */ a c; // = new a(null);
    @DexIgnore
    public /* final */ ch5 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a(long j) {
            if (j < ((long) 70)) {
                return 0;
            }
            return j < ((long) ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL) ? 1 : 2;
        }

        @DexIgnore
        public final int a(ActivitySummary activitySummary, gb5 gb5) {
            ee7.b(gb5, "goalType");
            int i = fe5.a[gb5.ordinal()];
            if (i != 1) {
                if (i != 2) {
                    if (i != 3) {
                        return VideoUploader.RETRY_DELAY_UNIT_MS;
                    }
                    return activitySummary != null ? activitySummary.getCaloriesGoal() : ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL;
                } else if (activitySummary != null) {
                    return activitySummary.getActiveTimeGoal();
                } else {
                    return 30;
                }
            } else if (activitySummary != null) {
                return activitySummary.getStepGoal();
            } else {
                return VideoUploader.RETRY_DELAY_UNIT_MS;
            }
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final int a(MFSleepDay mFSleepDay) {
            if (mFSleepDay != null) {
                return mFSleepDay.getGoalMinutes();
            }
            return 480;
        }

        @DexIgnore
        public final int a(GoalTrackingSummary goalTrackingSummary) {
            if (goalTrackingSummary != null) {
                return goalTrackingSummary.getGoalTarget();
            }
            return 8;
        }

        @DexIgnore
        public final String a(String str, MFSleepSession mFSleepSession) {
            ee7.b(str, ButtonService.USER_ID);
            ee7.b(mFSleepSession, "sleepSession");
            String value = db5.values()[mFSleepSession.getSource()].getValue();
            ee7.a((Object) value, "FitnessSourceType.values\u2026leepSession.source].value");
            if (value != null) {
                String lowerCase = value.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return str + ":" + lowerCase + ":" + mFSleepSession.getRealEndTime();
            }
            throw new x87("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.helper.FitnessHelper$getCurrentSteps$2", f = "FitnessHelper.kt", l = {69, 80}, m = "invokeSuspend")
    public static final class b extends zb7 implements kd7<yi7, fb7<? super Float>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $includeRealTimeStep;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ge5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ge5 ge5, Date date, boolean z, fb7 fb7) {
            super(2, fb7);
            this.this$0 = ge5;
            this.$date = date;
            this.$includeRealTimeStep = z;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            b bVar = new b(this.this$0, this.$date, this.$includeRealTimeStep, fb7);
            bVar.p$ = (yi7) obj;
            return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super Float> fb7) {
            return ((b) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00af  */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r7.label
                r2 = 0
                r3 = 2
                r4 = 1
                if (r1 == 0) goto L_0x002e
                if (r1 == r4) goto L_0x0022
                if (r1 != r3) goto L_0x001a
                float r2 = r7.F$0
                java.lang.Object r0 = r7.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r8)
                goto L_0x00a1
            L_0x001a:
                java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r8.<init>(r0)
                throw r8
            L_0x0022:
                java.lang.Object r1 = r7.L$1
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r4 = r7.L$0
                com.fossil.yi7 r4 = (com.fossil.yi7) r4
                com.fossil.t87.a(r8)
                goto L_0x004f
            L_0x002e:
                com.fossil.t87.a(r8)
                com.fossil.yi7 r8 = r7.p$
                java.util.Date r1 = r7.$date
                java.util.Date r1 = com.fossil.ye5.a(r1)
                com.portfolio.platform.PortfolioApp$a r5 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r5 = r5.c()
                r7.L$0 = r8
                r7.L$1 = r1
                r7.label = r4
                java.lang.Object r4 = r5.g(r7)
                if (r4 != r0) goto L_0x004c
                return r0
            L_0x004c:
                r6 = r4
                r4 = r8
                r8 = r6
            L_0x004f:
                java.util.Date r8 = (java.util.Date) r8
                boolean r8 = r1.before(r8)
                if (r8 == 0) goto L_0x0092
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0089 }
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()     // Catch:{ Exception -> 0x0089 }
                java.lang.String r0 = com.fossil.ge5.b     // Catch:{ Exception -> 0x0089 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0089 }
                r1.<init>()     // Catch:{ Exception -> 0x0089 }
                java.lang.String r3 = "Inside "
                r1.append(r3)     // Catch:{ Exception -> 0x0089 }
                java.lang.String r3 = com.fossil.ge5.b     // Catch:{ Exception -> 0x0089 }
                r1.append(r3)     // Catch:{ Exception -> 0x0089 }
                java.lang.String r3 = ".getCurrentSteps, this date -"
                r1.append(r3)     // Catch:{ Exception -> 0x0089 }
                java.util.Date r3 = r7.$date     // Catch:{ Exception -> 0x0089 }
                r1.append(r3)     // Catch:{ Exception -> 0x0089 }
                java.lang.String r3 = "- before user signing up date, return null."
                r1.append(r3)     // Catch:{ Exception -> 0x0089 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0089 }
                r8.d(r0, r1)     // Catch:{ Exception -> 0x0089 }
                goto L_0x008d
            L_0x0089:
                r8 = move-exception
                r8.printStackTrace()
            L_0x008d:
                java.lang.Float r8 = com.fossil.pb7.a(r2)
                return r8
            L_0x0092:
                com.fossil.pg5 r8 = com.fossil.pg5.i
                r7.L$0 = r4
                r7.F$0 = r2
                r7.label = r3
                java.lang.Object r8 = r8.b(r7)
                if (r8 != r0) goto L_0x00a1
                return r0
            L_0x00a1:
                com.portfolio.platform.data.source.local.fitness.FitnessDatabase r8 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r8
                com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao r8 = r8.activitySummaryDao()
                java.util.Date r0 = r7.$date
                com.portfolio.platform.data.model.room.fitness.ActivitySummary r8 = r8.getActivitySummary(r0)
                if (r8 == 0) goto L_0x00b4
                double r0 = r8.getSteps()
                float r2 = (float) r0
            L_0x00b4:
                java.util.Date r8 = r7.$date
                java.lang.Boolean r8 = com.fossil.zd5.w(r8)
                java.lang.String r0 = "DateHelper.isToday(date)"
                com.fossil.ee7.a(r8, r0)
                boolean r8 = r8.booleanValue()
                if (r8 == 0) goto L_0x00fc
                boolean r8 = r7.$includeRealTimeStep
                if (r8 == 0) goto L_0x00fc
                com.fossil.ge5 r8 = r7.this$0
                java.util.Date r0 = r7.$date
                long r0 = r8.a(r0)
                com.misfit.frameworks.buttonservice.log.FLogger r8 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r8 = r8.getLocal()
                java.lang.String r3 = com.fossil.ge5.b
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "getCurrentSteps - steps="
                r4.append(r5)
                r4.append(r2)
                java.lang.String r5 = ", realTimeSteps="
                r4.append(r5)
                r4.append(r0)
                java.lang.String r4 = r4.toString()
                r8.d(r3, r4)
                float r8 = (float) r0
                float r2 = com.fossil.qf7.a(r8, r2)
            L_0x00fc:
                java.lang.Float r8 = com.fossil.pb7.a(r2)
                return r8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.ge5.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = ge5.class.getSimpleName();
        ee7.a((Object) simpleName, "FitnessHelper::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public ge5(ch5 ch5) {
        ee7.b(ch5, "mSharedPreferencesManager");
        this.a = ch5;
    }

    @DexIgnore
    public final List<ActivitySample> a(List<ActivitySample> list, String str) {
        ee7.b(list, "activitySamples");
        ee7.b(str, ButtonService.USER_ID);
        long a2 = a(new Date());
        long j = 0;
        long j2 = 0;
        for (ActivitySample activitySample : list) {
            DateTime component4 = activitySample.component4();
            j += (long) activitySample.component5();
            j2 = component4.getMillis();
        }
        FLogger.INSTANCE.getLocal().d(b, "addRealTimeStepToActivitySample - steps=" + j + ", realTimeSteps=" + a2);
        if (j < a2) {
            try {
                ActivityIntensities activityIntensities = new ActivityIntensities();
                double d = (double) (a2 - j);
                activityIntensities.setIntensity(d);
                long j3 = j2 + 1;
                Date date = new Date(j3);
                DateTime dateTime = new DateTime(j3);
                DateTime dateTime2 = new DateTime(j2 + ((long) 100));
                int a3 = zd5.a();
                String value = db5.Device.getValue();
                ee7.a((Object) value, "FitnessSourceType.Device.value");
                list.add(new ActivitySample(str, date, dateTime, dateTime2, d, 0.0d, 0.0d, 0, activityIntensities, a3, value, j3, j3, j3));
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(b, "addRealTimeStepToActivitySample - e=" + e);
                e.printStackTrace();
            }
        }
        return list;
    }

    @DexIgnore
    public final Object a(Date date, boolean z, fb7<? super Float> fb7) {
        return vh7.a(qj7.b(), new b(this, date, z, null), fb7);
    }

    @DexIgnore
    public final void a(Date date, long j) {
        ee7.b(date, "date");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "saveRealTimeStep - date=" + date + ", steps=" + j);
        if (j >= 0) {
            String y = this.a.y();
            if (!TextUtils.isEmpty(y)) {
                String[] a2 = xs7.a(y, LocaleConverter.LOCALE_DELIMITER);
                if (a2 != null && a2.length == 2) {
                    try {
                        long parseLong = Long.parseLong(a2[0]);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = b;
                        local2.d(str2, "saveRealTimeStep - lastSampleRawTimeStamp=" + date + ", savedRealTimeStepTimeStamp=" + new Date(parseLong));
                        if (zd5.d(new Date(parseLong), date)) {
                            ch5 ch5 = this.a;
                            ch5.x(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                            return;
                        }
                        FLogger.INSTANCE.getLocal().d(b, "saveRealTimeStep - Different date, clear realTimeStepStamp");
                        ch5 ch52 = this.a;
                        ch52.x(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
                    } catch (Exception e) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str3 = b;
                        local3.e(str3, "saveRealTimeStep - e=" + e);
                        e.printStackTrace();
                    }
                }
            } else {
                ch5 ch53 = this.a;
                ch53.x(String.valueOf(date.getTime()) + LocaleConverter.LOCALE_DELIMITER + j);
            }
        }
    }

    @DexIgnore
    public final long a(Date date) {
        long j;
        String[] a2;
        ee7.b(date, "date");
        String y = this.a.y();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = b;
        local.d(str, "getRealTimeStep - data=" + y + ", date=" + zd5.k(date));
        if (!TextUtils.isEmpty(y) && (a2 = xs7.a(y, LocaleConverter.LOCALE_DELIMITER)) != null && a2.length == 2) {
            try {
                if (zd5.d(date, new Date(Long.parseLong(a2[0])))) {
                    j = Long.parseLong(a2[1]);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = b;
                    local2.d(str2, "XXX- getRealTimeStep - date=" + zd5.k(date) + " steps " + j);
                    return j;
                }
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = b;
                local3.e(str3, "getRealTimeStep - e=" + e);
                e.printStackTrace();
            }
        }
        j = 0;
        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
        String str22 = b;
        local22.d(str22, "XXX- getRealTimeStep - date=" + zd5.k(date) + " steps " + j);
        return j;
    }
}
