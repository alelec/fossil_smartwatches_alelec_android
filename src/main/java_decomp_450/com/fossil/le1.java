package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le1 implements Parcelable.Creator<bi1> {
    @DexIgnore
    public /* synthetic */ le1(zd7 zd7) {
    }

    @DexIgnore
    public final byte a(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(1);
    }

    @DexIgnore
    public final byte b(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public bi1 createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString == null) {
            readString = "";
        }
        byte readByte = parcel.readByte();
        byte readByte2 = parcel.readByte();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray == null) {
            createByteArray = new byte[0];
        }
        return new bi1(readString, readByte, readByte2, createByteArray, parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readInt() != 0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public bi1[] newArray(int i) {
        return new bi1[i];
    }
}
