package com.fossil;

import android.content.BroadcastReceiver;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class da4 implements ho3 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ BroadcastReceiver.PendingResult b;

    @DexIgnore
    public da4(boolean z, BroadcastReceiver.PendingResult pendingResult) {
        this.a = z;
        this.b = pendingResult;
    }

    @DexIgnore
    @Override // com.fossil.ho3
    public final void onComplete(no3 no3) {
        FirebaseInstanceIdReceiver.a(this.a, this.b, no3);
    }
}
