package com.fossil;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i27 {
    @DexIgnore
    public static i27 c;
    @DexIgnore
    public Map<Integer, h27> a; // = null;
    @DexIgnore
    public Context b; // = null;

    @DexIgnore
    public i27(Context context) {
        this.b = context.getApplicationContext();
        HashMap hashMap = new HashMap(3);
        this.a = hashMap;
        hashMap.put(1, new g27(context));
        this.a.put(2, new d27(context));
        this.a.put(4, new f27(context));
    }

    @DexIgnore
    public static synchronized i27 a(Context context) {
        i27 i27;
        synchronized (i27.class) {
            if (c == null) {
                c = new i27(context);
            }
            i27 = c;
        }
        return i27;
    }

    @DexIgnore
    public final e27 a() {
        return a(new ArrayList(Arrays.asList(1, 2, 4)));
    }

    @DexIgnore
    public final e27 a(List<Integer> list) {
        e27 c2;
        if (list.size() >= 0) {
            for (Integer num : list) {
                h27 h27 = this.a.get(num);
                if (h27 != null && (c2 = h27.c()) != null && j27.b(c2.c)) {
                    return c2;
                }
            }
        }
        return new e27();
    }

    @DexIgnore
    public final void a(String str) {
        e27 a2 = a();
        a2.c = str;
        if (!j27.a(a2.a)) {
            a2.a = j27.a(this.b);
        }
        if (!j27.a(a2.b)) {
            a2.b = j27.b(this.b);
        }
        a2.d = System.currentTimeMillis();
        for (Map.Entry<Integer, h27> entry : this.a.entrySet()) {
            entry.getValue().a(a2);
        }
    }
}
