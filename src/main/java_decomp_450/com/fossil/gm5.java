package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gm5 implements Factory<fm5> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;

    @DexIgnore
    public gm5(Provider<DeviceRepository> provider, Provider<ch5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static gm5 a(Provider<DeviceRepository> provider, Provider<ch5> provider2) {
        return new gm5(provider, provider2);
    }

    @DexIgnore
    public static fm5 a(DeviceRepository deviceRepository, ch5 ch5) {
        return new fm5(deviceRepository, ch5);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public fm5 get() {
        return a(this.a.get(), this.b.get());
    }
}
