package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jm1 extends fe7 implements kd7<zk0, Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ he0 a;
    @DexIgnore
    public /* final */ /* synthetic */ km1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jm1(he0 he0, km1 km1, zk0 zk0) {
        super(2);
        this.a = he0;
        this.b = km1;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public i97 invoke(zk0 zk0, Float f) {
        this.b.b.post(new j01(this, zk0, f.floatValue()));
        return i97.a;
    }
}
