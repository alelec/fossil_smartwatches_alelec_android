package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class om2 extends i72 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<om2> CREATOR; // = new pm2();
    @DexIgnore
    public int a;
    @DexIgnore
    public mm2 b;
    @DexIgnore
    public c63 c;
    @DexIgnore
    public kl2 d;

    @DexIgnore
    public om2(int i, mm2 mm2, IBinder iBinder, IBinder iBinder2) {
        this.a = i;
        this.b = mm2;
        kl2 kl2 = null;
        this.c = iBinder == null ? null : d63.a(iBinder);
        if (!(iBinder2 == null || iBinder2 == null)) {
            IInterface queryLocalInterface = iBinder2.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            kl2 = queryLocalInterface instanceof kl2 ? (kl2) queryLocalInterface : new ml2(iBinder2);
        }
        this.d = kl2;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = k72.a(parcel);
        k72.a(parcel, 1, this.a);
        k72.a(parcel, 2, (Parcelable) this.b, i, false);
        c63 c63 = this.c;
        IBinder iBinder = null;
        k72.a(parcel, 3, c63 == null ? null : c63.asBinder(), false);
        kl2 kl2 = this.d;
        if (kl2 != null) {
            iBinder = kl2.asBinder();
        }
        k72.a(parcel, 4, iBinder, false);
        k72.a(parcel, a2);
    }
}
