package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
public final class aj4 extends Enum<aj4> {
    @DexIgnore
    public static /* final */ aj4 H;
    @DexIgnore
    public static /* final */ aj4 L; // = new aj4("L", 0, 1);
    @DexIgnore
    public static /* final */ aj4 M; // = new aj4("M", 1, 0);
    @DexIgnore
    public static /* final */ aj4 Q; // = new aj4("Q", 2, 3);
    @DexIgnore
    public static /* final */ aj4[] a;
    @DexIgnore
    public static /* final */ /* synthetic */ aj4[] b;
    @DexIgnore
    public /* final */ int bits;

    /*
    static {
        aj4 aj4 = new aj4("H", 3, 2);
        H = aj4;
        aj4 aj42 = L;
        aj4 aj43 = M;
        aj4 aj44 = Q;
        b = new aj4[]{aj42, aj43, aj44, aj4};
        a = new aj4[]{aj43, aj42, aj4, aj44};
    }
    */

    @DexIgnore
    public aj4(String str, int i, int i2) {
        this.bits = i2;
    }

    @DexIgnore
    public static aj4 forBits(int i) {
        if (i >= 0) {
            aj4[] aj4Arr = a;
            if (i < aj4Arr.length) {
                return aj4Arr[i];
            }
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static aj4 valueOf(String str) {
        return (aj4) Enum.valueOf(aj4.class, str);
    }

    @DexIgnore
    public static aj4[] values() {
        return (aj4[]) b.clone();
    }

    @DexIgnore
    public int getBits() {
        return this.bits;
    }
}
