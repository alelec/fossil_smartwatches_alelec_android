package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fw {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int bottom; // = 2131361915;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131362019;
    @DexIgnore
    public static /* final */ int end; // = 2131362215;
    @DexIgnore
    public static /* final */ int forever; // = 2131362303;
    @DexIgnore
    public static /* final */ int glide_custom_view_target_tag; // = 2131362538;
    @DexIgnore
    public static /* final */ int icon; // = 2131362579;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362580;
    @DexIgnore
    public static /* final */ int info; // = 2131362603;
    @DexIgnore
    public static /* final */ int italic; // = 2131362619;
    @DexIgnore
    public static /* final */ int left; // = 2131362749;
    @DexIgnore
    public static /* final */ int line1; // = 2131362752;
    @DexIgnore
    public static /* final */ int line3; // = 2131362754;
    @DexIgnore
    public static /* final */ int none; // = 2131362843;
    @DexIgnore
    public static /* final */ int normal; // = 2131362844;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362845;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362846;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362847;
    @DexIgnore
    public static /* final */ int right; // = 2131362948;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362949;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362950;
    @DexIgnore
    public static /* final */ int start; // = 2131363091;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363124;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363125;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363126;
    @DexIgnore
    public static /* final */ int text; // = 2131363132;
    @DexIgnore
    public static /* final */ int text2; // = 2131363133;
    @DexIgnore
    public static /* final */ int time; // = 2131363154;
    @DexIgnore
    public static /* final */ int title; // = 2131363156;
    @DexIgnore
    public static /* final */ int top; // = 2131363165;
}
