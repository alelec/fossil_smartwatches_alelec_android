package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w64 implements v64 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public w64(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.v64
    public String a() {
        return new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics").getPath();
    }

    @DexIgnore
    @Override // com.fossil.v64
    public File b() {
        return a(new File(this.a.getFilesDir(), ".com.google.firebase.crashlytics"));
    }

    @DexIgnore
    public File a(File file) {
        if (file == null) {
            z24.a().a("Null File");
            return null;
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            z24.a().d("Couldn't create file");
            return null;
        }
    }
}
