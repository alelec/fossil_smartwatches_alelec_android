package com.fossil;

import com.fossil.gw;
import com.fossil.m00;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q00 {
    @DexIgnore
    public static /* final */ c e; // = new c();
    @DexIgnore
    public static /* final */ m00<Object, Object> f; // = new a();
    @DexIgnore
    public /* final */ List<b<?, ?>> a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public /* final */ Set<b<?, ?>> c;
    @DexIgnore
    public /* final */ b9<List<Throwable>> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements m00<Object, Object> {
        @DexIgnore
        @Override // com.fossil.m00
        public m00.a<Object> a(Object obj, int i, int i2, ax axVar) {
            return null;
        }

        @DexIgnore
        @Override // com.fossil.m00
        public boolean a(Object obj) {
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<Model, Data> {
        @DexIgnore
        public /* final */ Class<Model> a;
        @DexIgnore
        public /* final */ Class<Data> b;
        @DexIgnore
        public /* final */ n00<? extends Model, ? extends Data> c;

        @DexIgnore
        public b(Class<Model> cls, Class<Data> cls2, n00<? extends Model, ? extends Data> n00) {
            this.a = cls;
            this.b = cls2;
            this.c = n00;
        }

        @DexIgnore
        public boolean a(Class<?> cls, Class<?> cls2) {
            return a(cls) && this.b.isAssignableFrom(cls2);
        }

        @DexIgnore
        public boolean a(Class<?> cls) {
            return this.a.isAssignableFrom(cls);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public <Model, Data> p00<Model, Data> a(List<m00<Model, Data>> list, b9<List<Throwable>> b9Var) {
            return new p00<>(list, b9Var);
        }
    }

    @DexIgnore
    public q00(b9<List<Throwable>> b9Var) {
        this(b9Var, e);
    }

    @DexIgnore
    public synchronized <Model, Data> void a(Class<Model> cls, Class<Data> cls2, n00<? extends Model, ? extends Data> n00) {
        a(cls, cls2, n00, true);
    }

    @DexIgnore
    public synchronized List<Class<?>> b(Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (b<?, ?> bVar : this.a) {
            if (!arrayList.contains(bVar.b) && bVar.a(cls)) {
                arrayList.add(bVar.b);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public q00(b9<List<Throwable>> b9Var, c cVar) {
        this.a = new ArrayList();
        this.c = new HashSet();
        this.d = b9Var;
        this.b = cVar;
    }

    @DexIgnore
    public final <Model, Data> void a(Class<Model> cls, Class<Data> cls2, n00<? extends Model, ? extends Data> n00, boolean z) {
        b<?, ?> bVar = new b<>(cls, cls2, n00);
        List<b<?, ?>> list = this.a;
        list.add(z ? list.size() : 0, bVar);
    }

    @DexIgnore
    public synchronized <Model> List<m00<Model, ?>> a(Class<Model> cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (b<?, ?> bVar : this.a) {
                if (!this.c.contains(bVar)) {
                    if (bVar.a(cls)) {
                        this.c.add(bVar);
                        arrayList.add(a(bVar));
                        this.c.remove(bVar);
                    }
                }
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
        return arrayList;
    }

    @DexIgnore
    public synchronized <Model, Data> m00<Model, Data> a(Class<Model> cls, Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (b<?, ?> bVar : this.a) {
                if (this.c.contains(bVar)) {
                    z = true;
                } else if (bVar.a(cls, cls2)) {
                    this.c.add(bVar);
                    arrayList.add(a(bVar));
                    this.c.remove(bVar);
                }
            }
            if (arrayList.size() > 1) {
                return this.b.a(arrayList, this.d);
            } else if (arrayList.size() == 1) {
                return (m00) arrayList.get(0);
            } else if (z) {
                return a();
            } else {
                throw new gw.c((Class<?>) cls, (Class<?>) cls2);
            }
        } catch (Throwable th) {
            this.c.clear();
            throw th;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Type inference failed for r1v3. Raw type applied. Possible types: com.fossil.m00<? extends Model, ? extends Data>, com.fossil.m00<Model, Data> */
    public final <Model, Data> m00<Model, Data> a(b<?, ?> bVar) {
        m00 a2 = bVar.c.a(this);
        u50.a(a2);
        return (m00<? extends Model, ? extends Data>) a2;
    }

    @DexIgnore
    public static <Model, Data> m00<Model, Data> a() {
        return (m00<Model, Data>) f;
    }
}
