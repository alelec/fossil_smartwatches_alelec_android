package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl7 {
    @DexIgnore
    public static /* final */ ThreadLocal<vj7> a; // = new ThreadLocal<>();
    @DexIgnore
    public static /* final */ fl7 b; // = new fl7();

    @DexIgnore
    public final vj7 a() {
        return a.get();
    }

    @DexIgnore
    public final vj7 b() {
        vj7 vj7 = a.get();
        if (vj7 != null) {
            return vj7;
        }
        vj7 a2 = yj7.a();
        a.set(a2);
        return a2;
    }

    @DexIgnore
    public final void c() {
        a.set(null);
    }

    @DexIgnore
    public final void a(vj7 vj7) {
        a.set(vj7);
    }
}
