package com.fossil;

import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z10 implements cx<InputStream, Bitmap> {
    @DexIgnore
    public /* final */ j10 a; // = new j10();

    @DexIgnore
    public boolean a(InputStream inputStream, ax axVar) throws IOException {
        return true;
    }

    @DexIgnore
    public uy<Bitmap> a(InputStream inputStream, int i, int i2, ax axVar) throws IOException {
        return this.a.a(ImageDecoder.createSource(l50.a(inputStream)), i, i2, axVar);
    }
}
