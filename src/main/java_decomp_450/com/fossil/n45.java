package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n45 extends m45 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C;
    @DexIgnore
    public long A;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        C = sparseIntArray;
        sparseIntArray.put(2131362163, 1);
        C.put(2131362636, 2);
        C.put(2131362106, 3);
        C.put(2131362506, 4);
        C.put(2131363021, 5);
        C.put(2131362384, 6);
        C.put(2131362100, 7);
        C.put(2131363158, 8);
        C.put(2131361888, 9);
    }
    */

    @DexIgnore
    public n45(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 10, B, C));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public n45(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (FlexibleSwitchCompat) objArr[9], (ConstraintLayout) objArr[7], (ConstraintLayout) objArr[3], (ConstraintLayout) objArr[1], (FlexibleTextView) objArr[6], (FlexibleTextView) objArr[4], (RTLImageView) objArr[2], (ConstraintLayout) objArr[0], (FlexibleSwitchCompat) objArr[5], (FlexibleTextView) objArr[8]);
        this.A = -1;
        ((m45) this).x.setTag(null);
        a(view);
        f();
    }
}
