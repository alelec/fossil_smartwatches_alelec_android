package com.fossil;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b20 implements cx<ParcelFileDescriptor, Bitmap> {
    @DexIgnore
    public /* final */ s10 a;

    @DexIgnore
    public b20(s10 s10) {
        this.a = s10;
    }

    @DexIgnore
    public boolean a(ParcelFileDescriptor parcelFileDescriptor, ax axVar) {
        return this.a.a(parcelFileDescriptor);
    }

    @DexIgnore
    public uy<Bitmap> a(ParcelFileDescriptor parcelFileDescriptor, int i, int i2, ax axVar) throws IOException {
        return this.a.a(parcelFileDescriptor, i, i2, axVar);
    }
}
