package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tk3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ ub3 c;
    @DexIgnore
    public /* final */ /* synthetic */ nm3 d;
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ ek3 f;

    @DexIgnore
    public tk3(ek3 ek3, boolean z, boolean z2, ub3 ub3, nm3 nm3, String str) {
        this.f = ek3;
        this.a = z;
        this.b = z2;
        this.c = ub3;
        this.d = nm3;
        this.e = str;
    }

    @DexIgnore
    public final void run() {
        bg3 d2 = this.f.d;
        if (d2 == null) {
            this.f.e().t().a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.a) {
            this.f.a(d2, this.b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c, this.e, this.f.e().C());
                }
            } catch (RemoteException e2) {
                this.f.e().t().a("Failed to send event to the service", e2);
            }
        }
        this.f.J();
    }
}
