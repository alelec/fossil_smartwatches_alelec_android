package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.be5;
import com.fossil.cy6;
import com.fossil.oh6;
import com.fossil.xg5;
import com.fossil.zo5;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.buddy_challenge.screens.overview_leaderboard.BCOverviewLeaderBoardActivity;
import com.portfolio.platform.buddy_challenge.screens.pending.BCWaitingChallengeDetailActivity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.uirenew.connectedapps.ConnectedAppsActivity;
import com.portfolio.platform.uirenew.home.profile.about.AboutActivity;
import com.portfolio.platform.uirenew.home.profile.battery.ReplaceBatteryActivity;
import com.portfolio.platform.uirenew.home.profile.edit.ProfileEditActivity;
import com.portfolio.platform.uirenew.home.profile.goal.ProfileGoalEditActivity;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.home.profile.opt.ProfileOptInActivity;
import com.portfolio.platform.uirenew.home.profile.password.ProfileChangePasswordActivity;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesActivity;
import com.portfolio.platform.uirenew.home.profile.unit.PreferredUnitActivity;
import com.portfolio.platform.uirenew.home.profile.workout.WorkoutSettingActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.watchsetting.WatchSettingActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.view.AutoResizeTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.FossilCircleImageView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class es5 extends ho5 implements nh6, zo5.b, cy6.g, ro5 {
    @DexIgnore
    public static /* final */ a q; // = new a(null);
    @DexIgnore
    public mh6 g;
    @DexIgnore
    public qw6<w25> h;
    @DexIgnore
    public kd5 i;
    @DexIgnore
    public zo5 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final es5 a() {
            return new es5();
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public b(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                HelpActivity.a aVar = HelpActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public c(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                AboutActivity.a aVar = AboutActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public d(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ConnectedAppsActivity.a aVar = ConnectedAppsActivity.A;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public e(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileOptInActivity.a aVar = ProfileOptInActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public f(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ReplaceBatteryActivity.a aVar = ReplaceBatteryActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public g(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ThemesActivity.a aVar = ThemesActivity.y;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public h(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                String a2 = rw6.b.a(6);
                BaseWebViewActivity.a aVar = BaseWebViewActivity.B;
                ee7.a((Object) activity, "it");
                aVar.a(activity, "", a2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public i(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.doCameraTask();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public j(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public k(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileEditActivity.a aVar = ProfileEditActivity.y;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public l(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.f1().a(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public m(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                ee7.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public n(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileGoalEditActivity.a aVar = ProfileGoalEditActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public o(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                WorkoutSettingActivity.a aVar = WorkoutSettingActivity.y;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public p(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                ProfileChangePasswordActivity.a aVar = ProfileChangePasswordActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class q implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ es5 a;

        @DexIgnore
        public q(es5 es5) {
            this.a = es5;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                PreferredUnitActivity.a aVar = PreferredUnitActivity.z;
                ee7.a((Object) activity, "it");
                aVar.a(activity);
            }
        }
    }

    @DexIgnore
    @iu7(122)
    public final void doCameraTask() {
        if (xg5.a(xg5.b, (Context) getActivity(), xg5.a.EDIT_AVATAR, false, false, false, (Integer) null, 60, (Object) null)) {
            mh6 mh6 = this.g;
            if (mh6 != null) {
                mh6.k();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void C() {
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.G(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void J(String str) {
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                if (str == null || str.length() == 0) {
                    FlexibleTextView flexibleTextView = a2.A0;
                    ee7.a((Object) flexibleTextView, "tvSocialId");
                    flexibleTextView.setVisibility(8);
                    return;
                }
                FlexibleTextView flexibleTextView2 = a2.A0;
                ee7.a((Object) flexibleTextView2, "tvSocialId");
                flexibleTextView2.setVisibility(0);
                FlexibleTextView flexibleTextView3 = a2.A0;
                ee7.a((Object) flexibleTextView3, "tvSocialId");
                we7 we7 = we7.a;
                String string = getString(2131887271);
                ee7.a((Object) string, "getString(R.string.buddy_challenge_profile_social)");
                String format = String.format(string, Arrays.copyOf(new Object[]{str}, 1));
                ee7.a((Object) format, "java.lang.String.format(format, *args)");
                flexibleTextView3.setText(format);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void b(ArrayList<oh6.b> arrayList) {
        ee7.b(arrayList, Constants.DEVICES);
        zo5 zo5 = this.j;
        if (zo5 != null) {
            zo5.a(arrayList);
        }
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (arrayList.isEmpty()) {
                FlexibleTextView flexibleTextView = a2.u0;
                ee7.a((Object) flexibleTextView, "it.tvDevice");
                flexibleTextView.setText(ig5.a(PortfolioApp.g0.c(), 2131887020));
                FlexibleButton flexibleButton = a2.H;
                ee7.a((Object) flexibleButton, "it.cvPairFirstWatch");
                flexibleButton.setVisibility(0);
                FlexibleButton flexibleButton2 = a2.I;
                ee7.a((Object) flexibleButton2, "it.fbtAddDevice");
                flexibleButton2.setVisibility(8);
                return;
            }
            FlexibleTextView flexibleTextView2 = a2.u0;
            ee7.a((Object) flexibleTextView2, "it.tvDevice");
            flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131887123));
            FlexibleButton flexibleButton3 = a2.H;
            ee7.a((Object) flexibleButton3, "it.cvPairFirstWatch");
            flexibleButton3.setVisibility(8);
            FlexibleButton flexibleButton4 = a2.I;
            ee7.a((Object) flexibleButton4, "it.fbtAddDevice");
            flexibleButton4.setVisibility(0);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void c(boolean z, boolean z2) {
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                if (z2) {
                    FlexibleTextView flexibleTextView = a2.L;
                    ee7.a((Object) flexibleTextView, "it.ftvTitleLowBattery");
                    we7 we7 = we7.a;
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886784);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format = String.format(a3, Arrays.copyOf(new Object[]{"10%"}, 1));
                    ee7.a((Object) format, "java.lang.String.format(format, *args)");
                    flexibleTextView.setText(format);
                    FlexibleTextView flexibleTextView2 = a2.K;
                    ee7.a((Object) flexibleTextView2, "it.ftvDescriptionLowBattery");
                    flexibleTextView2.setText(ig5.a(PortfolioApp.g0.c(), 2131886783));
                    FlexibleButton flexibleButton = a2.x;
                    ee7.a((Object) flexibleButton, "it.btReplaceBattery");
                    flexibleButton.setVisibility(8);
                } else {
                    FlexibleTextView flexibleTextView3 = a2.L;
                    ee7.a((Object) flexibleTextView3, "it.ftvTitleLowBattery");
                    we7 we72 = we7.a;
                    String a4 = ig5.a(PortfolioApp.g0.c(), 2131886794);
                    ee7.a((Object) a4, "LanguageHelper.getString\u2026YourBatteryIsBelowNumber)");
                    String format2 = String.format(a4, Arrays.copyOf(new Object[]{"25%"}, 1));
                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    flexibleTextView3.setText(format2);
                    FlexibleTextView flexibleTextView4 = a2.K;
                    ee7.a((Object) flexibleTextView4, "it.ftvDescriptionLowBattery");
                    flexibleTextView4.setText(ig5.a(PortfolioApp.g0.c(), 2131886793));
                    FlexibleButton flexibleButton2 = a2.x;
                    ee7.a((Object) flexibleButton2, "it.btReplaceBattery");
                    flexibleButton2.setVisibility(0);
                }
                if (tm4.a.a().g()) {
                    FlexibleButton flexibleButton3 = a2.x;
                    ee7.a((Object) flexibleButton3, "it.btReplaceBattery");
                    flexibleButton3.setVisibility(8);
                }
                ConstraintLayout constraintLayout = a2.G;
                ee7.a((Object) constraintLayout, "it.clLowBattery");
                constraintLayout.setVisibility(0);
                return;
            }
            ConstraintLayout constraintLayout2 = a2.G;
            ee7.a((Object) constraintLayout2, "it.clLowBattery");
            constraintLayout2.setVisibility(8);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void d0() {
        zo5 zo5 = this.j;
        if (zo5 != null) {
            zo5.c();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5
    public String d1() {
        return "HomeProfileFragment";
    }

    @DexIgnore
    @Override // com.fossil.go5
    public boolean e1() {
        return false;
    }

    @DexIgnore
    public final mh6 f1() {
        mh6 mh6 = this.g;
        if (mh6 != null) {
            return mh6;
        }
        ee7.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.zo5.b
    public void g(String str) {
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "user select " + str);
        WatchSettingActivity.a aVar = WatchSettingActivity.z;
        FragmentActivity requireActivity = requireActivity();
        ee7.a((Object) requireActivity, "requireActivity()");
        aVar.a(requireActivity, str);
    }

    @DexIgnore
    public final void g1() {
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                ee7.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(0);
                u5 u5Var = new u5();
                u5Var.c(a2.B);
                u5Var.a(2131362031, 6, 0, 6);
                u5Var.a(2131362031, 7, 2131362026, 6);
                u5Var.a(2131362031, 3, 0, 3);
                u5Var.a(2131362031, 4, 2131362030, 3);
                u5Var.a(2131362026, 6, 2131362031, 7);
                u5Var.a(2131362026, 7, 0, 7);
                u5Var.a(2131362026, 3, 2131362031, 3);
                u5Var.a(2131362026, 4, 2131362031, 4);
                u5Var.a(2131362041, 6, 2131362031, 6);
                u5Var.a(2131362041, 7, 2131362031, 7);
                u5Var.a(2131362041, 3, 2131362031, 4);
                u5Var.a(2131362041, 4, 0, 4);
                u5Var.a(2131362030, 6, 2131362026, 6);
                u5Var.a(2131362030, 7, 2131362026, 7);
                u5Var.a(2131362030, 3, 2131362041, 3);
                u5Var.a(2131362030, 4, 2131362041, 4);
                u5Var.a(a2.B);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void h() {
        a();
    }

    @DexIgnore
    public final void h1() {
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.C;
                ee7.a((Object) constraintLayout, "clActiveTime");
                constraintLayout.setVisibility(8);
                u5 u5Var = new u5();
                u5Var.c(a2.B);
                u5Var.a(2131362031, 6, 0, 6);
                u5Var.a(2131362031, 7, 2131362041, 6);
                u5Var.a(2131362031, 3, 0, 3);
                u5Var.a(2131362031, 4, 2131362030, 3);
                u5Var.a(2131362041, 6, 2131362031, 7);
                u5Var.a(2131362041, 7, 0, 7);
                u5Var.a(2131362041, 3, 2131362031, 3);
                u5Var.a(2131362041, 4, 2131362031, 4);
                u5Var.a(2131362030, 6, 2131362031, 6);
                u5Var.a(2131362030, 7, 2131362031, 7);
                u5Var.a(2131362030, 3, 2131362031, 4);
                u5Var.a(2131362030, 4, 0, 4);
                u5Var.a(a2.B);
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void j() {
        b();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == -1 && i2 == 1234) {
            mh6 mh6 = this.g;
            if (mh6 != null) {
                mh6.a(intent);
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        ee7.b(layoutInflater, "inflater");
        super.onCreateView(layoutInflater, viewGroup, bundle);
        w25 w25 = (w25) qb.a(layoutInflater, 2131558576, viewGroup, false, a1());
        kd5 a2 = hd5.a(this);
        ee7.a((Object) a2, "GlideApp.with(this)");
        this.i = a2;
        ArrayList arrayList = new ArrayList();
        kd5 kd5 = this.i;
        if (kd5 != null) {
            this.j = new zo5(arrayList, kd5, this, PortfolioApp.g0.c());
            qw6<w25> qw6 = new qw6<>(this, w25);
            this.h = qw6;
            if (qw6 != null) {
                w25 a3 = qw6.a();
                if (a3 != null) {
                    ee7.a((Object) a3, "mBinding.get()!!");
                    return a3.d();
                }
                ee7.a();
                throw null;
            }
            ee7.d("mBinding");
            throw null;
        }
        ee7.d("mGlideRequests");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.go5, com.fossil.ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        mh6 mh6 = this.g;
        if (mh6 != null) {
            if (mh6 != null) {
                mh6.g();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.a("");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        mh6 mh6 = this.g;
        if (mh6 != null) {
            if (mh6 != null) {
                mh6.f();
            } else {
                ee7.d("mPresenter");
                throw null;
            }
        }
        jf5 c1 = c1();
        if (c1 != null) {
            c1.d();
        }
    }

    @DexIgnore
    @Override // com.fossil.go5, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String b2;
        ee7.b(view, "view");
        super.onViewCreated(view, bundle);
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                String b3 = eh5.l.a().b("onPrimaryButton");
                if (!TextUtils.isEmpty(b3)) {
                    int parseColor = Color.parseColor(b3);
                    Drawable drawable = PortfolioApp.g0.c().getDrawable(2131230982);
                    if (drawable != null) {
                        drawable.setTint(parseColor);
                        a2.I.setCompoundDrawablesWithIntrinsicBounds(drawable, (Drawable) null, (Drawable) null, (Drawable) null);
                    }
                }
                FossilCircleImageView fossilCircleImageView = a2.c0;
                ee7.a((Object) fossilCircleImageView, "binding.ivUserAvatar");
                bf5.a(fossilCircleImageView, new i(this));
                a2.w0.setOnClickListener(new j(this));
                a2.W.setOnClickListener(new k(this));
                a2.I.setOnClickListener(new l(this));
                a2.H.setOnClickListener(new m(this));
                a2.y.setOnClickListener(new n(this));
                a2.s.setOnClickListener(new o(this));
                a2.t.setOnClickListener(new p(this));
                a2.A.setOnClickListener(new q(this));
                a2.v.setOnClickListener(new b(this));
                a2.r.setOnClickListener(new c(this));
                a2.u.setOnClickListener(new d(this));
                a2.w.setOnClickListener(new e(this));
                a2.x.setOnClickListener(new f(this));
                a2.z.setOnClickListener(new g(this));
                ConstraintLayout constraintLayout = a2.B;
                if (constraintLayout != null) {
                    String b4 = eh5.l.a().b("nonBrandSurface");
                    if (!TextUtils.isEmpty(b4)) {
                        constraintLayout.setBackgroundColor(Color.parseColor(b4));
                    }
                }
                ConstraintLayout constraintLayout2 = a2.G;
                if (!(constraintLayout2 == null || (b2 = eh5.l.a().b("nonBrandSurface")) == null)) {
                    constraintLayout2.setBackgroundColor(Color.parseColor(b2));
                }
                a2.J.setOnClickListener(new h(this));
                mh6 mh6 = this.g;
                if (mh6 != null) {
                    be5.a aVar = be5.o;
                    if (mh6 == null) {
                        ee7.d("mPresenter");
                        throw null;
                    } else if (aVar.a(mh6.h())) {
                        String b5 = eh5.l.a().b("dianaActiveMinutesTab");
                        String b6 = eh5.l.a().b("dianaStepsTab");
                        String b7 = eh5.l.a().b("dianaActiveCaloriesTab");
                        String b8 = eh5.l.a().b("dianaSleepTab");
                        if (!TextUtils.isEmpty(b5)) {
                            a2.O.setColorFilter(Color.parseColor(b5));
                        }
                        if (!TextUtils.isEmpty(b8)) {
                            a2.P.setColorFilter(Color.parseColor(b8));
                        }
                        if (!TextUtils.isEmpty(b6)) {
                            a2.Q.setColorFilter(Color.parseColor(b6));
                        }
                        if (!TextUtils.isEmpty(b7)) {
                            a2.R.setColorFilter(Color.parseColor(b7));
                        }
                        RecyclerView recyclerView = a2.h0;
                        ee7.a((Object) recyclerView, "binding.rvDevices");
                        recyclerView.setAdapter(this.j);
                        RecyclerView recyclerView2 = a2.h0;
                        ee7.a((Object) recyclerView2, "binding.rvDevices");
                        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                        RecyclerView recyclerView3 = a2.h0;
                        ee7.a((Object) recyclerView3, "binding.rvDevices");
                        recyclerView3.setNestedScrollingEnabled(false);
                    }
                }
                String b9 = eh5.l.a().b("hybridStepsTab");
                String b10 = eh5.l.a().b("hybridActiveCaloriesTab");
                String b11 = eh5.l.a().b("hybridSleepTab");
                if (!TextUtils.isEmpty(b11)) {
                    a2.P.setColorFilter(Color.parseColor(b11));
                }
                if (!TextUtils.isEmpty(b9)) {
                    a2.Q.setColorFilter(Color.parseColor(b9));
                }
                if (!TextUtils.isEmpty(b10)) {
                    a2.R.setColorFilter(Color.parseColor(b10));
                }
                RecyclerView recyclerView4 = a2.h0;
                ee7.a((Object) recyclerView4, "binding.rvDevices");
                recyclerView4.setAdapter(this.j);
                RecyclerView recyclerView22 = a2.h0;
                ee7.a((Object) recyclerView22, "binding.rvDevices");
                recyclerView22.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));
                RecyclerView recyclerView32 = a2.h0;
                ee7.a((Object) recyclerView32, "binding.rvDevices");
                recyclerView32.setNestedScrollingEnabled(false);
            }
            V("profile_view");
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void p0() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            WelcomeActivity.a aVar = WelcomeActivity.z;
            ee7.a((Object) activity, "it");
            aVar.b(activity);
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.fossil.ro5
    public void r(boolean z) {
        if (z) {
            jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
            }
            if (this.h != null) {
                mh6 mh6 = this.g;
                if (mh6 != null) {
                    mh6.i();
                } else {
                    ee7.d("mPresenter");
                    throw null;
                }
            }
        } else {
            jf5 c12 = c1();
            if (c12 != null) {
                c12.a("");
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void updateUser(MFUser mFUser) {
        String str;
        String str2;
        ee7.b(mFUser, "user");
        FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "updateUser");
        if (isActive()) {
            qw6<w25> qw6 = this.h;
            if (qw6 != null) {
                w25 a2 = qw6.a();
                if (a2 != null) {
                    FlexibleTextView flexibleTextView = a2.B0;
                    ee7.a((Object) flexibleTextView, "it.tvUserName");
                    flexibleTextView.setText(mFUser.getFirstName() + " " + mFUser.getLastName());
                    if (!TextUtils.isEmpty(mFUser.getRegisterDate())) {
                        try {
                            String j2 = zd5.j(zd5.b(mFUser.getRegisterDate()).toDate());
                            FlexibleTextView flexibleTextView2 = a2.x0;
                            ee7.a((Object) flexibleTextView2, "it.tvMemberSince");
                            we7 we7 = we7.a;
                            String a3 = ig5.a(PortfolioApp.g0.c(), 2131887121);
                            ee7.a((Object) a3, "LanguageHelper.getString\u2026_Text__JoinedInMonthYear)");
                            String format = String.format(a3, Arrays.copyOf(new Object[]{j2}, 1));
                            ee7.a((Object) format, "java.lang.String.format(format, *args)");
                            flexibleTextView2.setText(format);
                        } catch (Exception e2) {
                            FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "Exception when parse register date. " + e2);
                        }
                    }
                    String profilePicture = mFUser.getProfilePicture();
                    String str3 = mFUser.getFirstName() + " " + mFUser.getLastName();
                    if (TextUtils.isEmpty(profilePicture) || (!URLUtil.isHttpUrl(profilePicture) && !URLUtil.isHttpsUrl(profilePicture))) {
                        kd5 kd5 = this.i;
                        if (kd5 != null) {
                            kd5.a((Object) new gd5(mFUser.getProfilePicture(), str3)).a(new r40().a((ex<Bitmap>) new vd5())).a((ImageView) a2.c0);
                            FossilCircleImageView fossilCircleImageView = a2.c0;
                            ee7.a((Object) fossilCircleImageView, "it.ivUserAvatar");
                            fossilCircleImageView.setBorderColor(v6.a(requireContext(), 2131099830));
                            FossilCircleImageView fossilCircleImageView2 = a2.c0;
                            ee7.a((Object) fossilCircleImageView2, "it.ivUserAvatar");
                            fossilCircleImageView2.setBorderWidth(3);
                            FossilCircleImageView fossilCircleImageView3 = a2.c0;
                            ee7.a((Object) fossilCircleImageView3, "it.ivUserAvatar");
                            fossilCircleImageView3.setBackground(v6.c(requireContext(), 2131231270));
                        } else {
                            ee7.d("mGlideRequests");
                            throw null;
                        }
                    } else {
                        FossilCircleImageView fossilCircleImageView4 = a2.c0;
                        kd5 kd52 = this.i;
                        if (kd52 != null) {
                            fossilCircleImageView4.a(kd52, profilePicture, str3);
                            FossilCircleImageView fossilCircleImageView5 = a2.c0;
                            ee7.a((Object) fossilCircleImageView5, "it.ivUserAvatar");
                            fossilCircleImageView5.setBorderColor(v6.a(requireContext(), (int) R.color.transparent));
                        } else {
                            ee7.d("mGlideRequests");
                            throw null;
                        }
                    }
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "useDefaultBiometric = " + mFUser.getUseDefaultBiometric());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "weightInGrams = " + mFUser.getWeightInGrams());
                    FLogger.INSTANCE.getLocal().d("HomeProfileFragment", "heightInCentimeters = " + mFUser.getHeightInCentimeters());
                    String string = PortfolioApp.g0.c().getString(2131887288);
                    ee7.a((Object) string, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                    if (ee7.a((Object) (unitGroup != null ? unitGroup.getWeight() : null), (Object) ob5.IMPERIAL.getValue())) {
                        if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                            we7 we72 = we7.a;
                            Object[] objArr = new Object[2];
                            objArr[0] = string;
                            String a4 = ig5.a(PortfolioApp.g0.c(), 2131886902);
                            ee7.a((Object) a4, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a4 != null) {
                                String lowerCase = a4.toLowerCase();
                                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                                objArr[1] = lowerCase;
                                str = String.format("%s %s", Arrays.copyOf(objArr, 2));
                                ee7.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new x87("null cannot be cast to non-null type java.lang.String");
                            }
                        } else {
                            we7 we73 = we7.a;
                            Object[] objArr2 = new Object[2];
                            objArr2[0] = re5.a(xd5.f((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                            String a5 = ig5.a(PortfolioApp.g0.c(), 2131886902);
                            ee7.a((Object) a5, "LanguageHelper.getString\u2026alInformation_Label__Lbs)");
                            if (a5 != null) {
                                String lowerCase2 = a5.toLowerCase();
                                ee7.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase()");
                                objArr2[1] = lowerCase2;
                                str = String.format("%s %s", Arrays.copyOf(objArr2, 2));
                                ee7.a((Object) str, "java.lang.String.format(format, *args)");
                            } else {
                                throw new x87("null cannot be cast to non-null type java.lang.String");
                            }
                        }
                    } else if (mFUser.getWeightInGrams() == 0 || mFUser.getUseDefaultBiometric()) {
                        we7 we74 = we7.a;
                        Object[] objArr3 = new Object[2];
                        objArr3[0] = string;
                        String a6 = ig5.a(PortfolioApp.g0.c(), 2131886901);
                        ee7.a((Object) a6, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a6 != null) {
                            String lowerCase3 = a6.toLowerCase();
                            ee7.a((Object) lowerCase3, "(this as java.lang.String).toLowerCase()");
                            objArr3[1] = lowerCase3;
                            str = String.format("%s %s", Arrays.copyOf(objArr3, 2));
                            ee7.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        we7 we75 = we7.a;
                        Object[] objArr4 = new Object[2];
                        objArr4[0] = re5.a(xd5.e((float) (mFUser.getWeightInGrams() + 1)), 1).toString();
                        String a7 = ig5.a(PortfolioApp.g0.c(), 2131886901);
                        ee7.a((Object) a7, "LanguageHelper.getString\u2026nalInformation_Label__Kg)");
                        if (a7 != null) {
                            String lowerCase4 = a7.toLowerCase();
                            ee7.a((Object) lowerCase4, "(this as java.lang.String).toLowerCase()");
                            objArr4[1] = lowerCase4;
                            str = String.format("%s %s", Arrays.copyOf(objArr4, 2));
                            ee7.a((Object) str, "java.lang.String.format(format, *args)");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String string2 = PortfolioApp.g0.c().getString(2131887288);
                    ee7.a((Object) string2, "PortfolioApp.instance.ge\u2026ng.character_dash_double)");
                    MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                    if (ee7.a((Object) (unitGroup2 != null ? unitGroup2.getHeight() : null), (Object) ob5.IMPERIAL.getValue())) {
                        if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                            we7 we76 = we7.a;
                            String a8 = ig5.a(PortfolioApp.g0.c(), 2131887530);
                            ee7.a((Object) a8, "LanguageHelper.getString\u2026_height_single_character)");
                            str2 = String.format(a8, Arrays.copyOf(new Object[]{string2}, 1));
                            ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            r87<Integer, Integer> b2 = xd5.b((float) mFUser.getHeightInCentimeters());
                            we7 we77 = we7.a;
                            String a9 = ig5.a(PortfolioApp.g0.c(), 2131887529);
                            ee7.a((Object) a9, "LanguageHelper.getString\u2026_height_double_character)");
                            str2 = String.format(a9, Arrays.copyOf(new Object[]{String.valueOf(b2.getFirst().intValue()), String.valueOf(b2.getSecond().intValue())}, 2));
                            ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                        }
                    } else if (mFUser.getHeightInCentimeters() == 0 || mFUser.getUseDefaultBiometric()) {
                        we7 we78 = we7.a;
                        Object[] objArr5 = new Object[2];
                        objArr5[0] = string2;
                        String a10 = ig5.a(PortfolioApp.g0.c(), 2131886899);
                        ee7.a((Object) a10, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a10 != null) {
                            String lowerCase5 = a10.toLowerCase();
                            ee7.a((Object) lowerCase5, "(this as java.lang.String).toLowerCase()");
                            objArr5[1] = lowerCase5;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr5, 2));
                            ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    } else {
                        we7 we79 = we7.a;
                        Object[] objArr6 = new Object[2];
                        objArr6[0] = String.valueOf(mFUser.getHeightInCentimeters());
                        String a11 = ig5.a(PortfolioApp.g0.c(), 2131886899);
                        ee7.a((Object) a11, "LanguageHelper.getString\u2026nalInformation_Label__Cm)");
                        if (a11 != null) {
                            String lowerCase6 = a11.toLowerCase();
                            ee7.a((Object) lowerCase6, "(this as java.lang.String).toLowerCase()");
                            objArr6[1] = lowerCase6;
                            str2 = String.format("%s %s", Arrays.copyOf(objArr6, 2));
                            ee7.a((Object) str2, "java.lang.String.format(format, *args)");
                        } else {
                            throw new x87("null cannot be cast to non-null type java.lang.String");
                        }
                    }
                    String str4 = str + ", " + str2;
                    FlexibleTextView flexibleTextView3 = a2.v0;
                    ee7.a((Object) flexibleTextView3, "it.tvHeightWeight");
                    flexibleTextView3.setText(str4);
                    if (ee7.a((Object) mFUser.getAuthType(), (Object) za5.EMAIL.getValue())) {
                        RelativeLayout relativeLayout = a2.t;
                        ee7.a((Object) relativeLayout, "it.btChangePassword");
                        relativeLayout.setVisibility(0);
                        View view = a2.D0;
                        ee7.a((Object) view, "it.vChangePasswordSeparatorLine");
                        view.setVisibility(0);
                        return;
                    }
                    RelativeLayout relativeLayout2 = a2.t;
                    ee7.a((Object) relativeLayout2, "it.btChangePassword");
                    relativeLayout2.setVisibility(8);
                    View view2 = a2.D0;
                    ee7.a((Object) view2, "it.vChangePasswordSeparatorLine");
                    view2.setVisibility(8);
                    return;
                }
                return;
            }
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void x(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeProfileFragment", "hideWorkoutSettings, value = " + z);
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (z) {
                RelativeLayout relativeLayout = a2.s;
                ee7.a((Object) relativeLayout, "it.btAutoWorkout");
                relativeLayout.setVisibility(8);
                View view = a2.C0;
                ee7.a((Object) view, "it.vAutoWorkoutSeparatorLine");
                view.setVisibility(8);
                return;
            }
            RelativeLayout relativeLayout2 = a2.s;
            ee7.a((Object) relativeLayout2, "it.btAutoWorkout");
            relativeLayout2.setVisibility(0);
            View view2 = a2.C0;
            ee7.a((Object) view2, "it.vAutoWorkoutSeparatorLine");
            view2.setVisibility(0);
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.ho5, com.fossil.cy6.g
    public void a(String str, int i2, Intent intent) {
        ee7.b(str, "tag");
        if (!(str.length() == 0) && getActivity() != null) {
            int hashCode = str.hashCode();
            mn4 mn4 = null;
            if (hashCode != -292748329) {
                if (hashCode == 1970588827 && str.equals("LEAVE_CHALLENGE")) {
                    if (i2 == 2131363307) {
                        if (intent != null) {
                            mn4 = (mn4) intent.getParcelableExtra("CHALLENGE");
                        }
                        if (mn4 != null) {
                            b(mn4);
                            return;
                        }
                        return;
                    }
                    return;
                }
            } else if (str.equals("CONFIRM_LOGOUT_ACCOUNT")) {
                if (i2 == 2131363307) {
                    mh6 mh6 = this.g;
                    if (mh6 != null) {
                        mh6.j();
                        return;
                    } else {
                        ee7.d("mPresenter");
                        throw null;
                    }
                } else {
                    return;
                }
            }
            super.a(str, i2, intent);
        }
    }

    @DexIgnore
    public final void b(mn4 mn4) {
        long b2 = vt4.a.b();
        Date m2 = mn4.m();
        if (b2 > (m2 != null ? m2.getTime() : 0)) {
            BCOverviewLeaderBoardActivity.y.a(this, mn4, null);
        } else {
            BCWaitingChallengeDetailActivity.z.a(this, mn4, "joined_challenge", -1, false);
        }
    }

    @DexIgnore
    public void a(mh6 mh6) {
        ee7.b(mh6, "presenter");
        this.g = mh6;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void a(ActivityStatistic activityStatistic) {
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                if (activityStatistic != null) {
                    ActivityStatistic.ActivityDailyBest stepsBestDay = activityStatistic.getStepsBestDay();
                    ActivityStatistic.ActivityDailyBest activeTimeBestDay = activityStatistic.getActiveTimeBestDay();
                    ActivityStatistic.CaloriesBestDay caloriesBestDay = activityStatistic.getCaloriesBestDay();
                    String a3 = ig5.a(getContext(), 2131887288);
                    if (stepsBestDay != null) {
                        AutoResizeTextView autoResizeTextView = a2.l0;
                        ee7.a((Object) autoResizeTextView, "it.tvAvgActivity");
                        autoResizeTextView.setText(re5.b(stepsBestDay.getValue()));
                        FlexibleTextView flexibleTextView = a2.m0;
                        ee7.a((Object) flexibleTextView, "it.tvAvgActivityDate");
                        flexibleTextView.setText(zd5.c(stepsBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView2 = a2.l0;
                        ee7.a((Object) autoResizeTextView2, "it.tvAvgActivity");
                        autoResizeTextView2.setText(a3);
                        FlexibleTextView flexibleTextView2 = a2.m0;
                        ee7.a((Object) flexibleTextView2, "it.tvAvgActivityDate");
                        flexibleTextView2.setText("");
                    }
                    if (activeTimeBestDay != null) {
                        AutoResizeTextView autoResizeTextView3 = a2.i0;
                        ee7.a((Object) autoResizeTextView3, "it.tvAvgActiveTime");
                        autoResizeTextView3.setText(re5.b(activeTimeBestDay.getValue()));
                        FlexibleTextView flexibleTextView3 = a2.j0;
                        ee7.a((Object) flexibleTextView3, "it.tvAvgActiveTimeDate");
                        flexibleTextView3.setText(zd5.c(activeTimeBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView4 = a2.i0;
                        ee7.a((Object) autoResizeTextView4, "it.tvAvgActiveTime");
                        autoResizeTextView4.setText(a3);
                        FlexibleTextView flexibleTextView4 = a2.j0;
                        ee7.a((Object) flexibleTextView4, "it.tvAvgActiveTimeDate");
                        flexibleTextView4.setText("");
                    }
                    if (caloriesBestDay != null) {
                        AutoResizeTextView autoResizeTextView5 = a2.o0;
                        ee7.a((Object) autoResizeTextView5, "it.tvAvgCalories");
                        autoResizeTextView5.setText(re5.b(af7.a(caloriesBestDay.getValue())));
                        FlexibleTextView flexibleTextView5 = a2.p0;
                        ee7.a((Object) flexibleTextView5, "it.tvAvgCaloriesDate");
                        flexibleTextView5.setText(zd5.c(caloriesBestDay.getDate()));
                    } else {
                        AutoResizeTextView autoResizeTextView6 = a2.o0;
                        ee7.a((Object) autoResizeTextView6, "it.tvAvgCalories");
                        autoResizeTextView6.setText(a3);
                        FlexibleTextView flexibleTextView6 = a2.p0;
                        ee7.a((Object) flexibleTextView6, "it.tvAvgCaloriesDate");
                        flexibleTextView6.setText("");
                    }
                } else {
                    String a4 = ig5.a(getContext(), 2131887288);
                    AutoResizeTextView autoResizeTextView7 = a2.l0;
                    ee7.a((Object) autoResizeTextView7, "it.tvAvgActivity");
                    autoResizeTextView7.setText(a4);
                    AutoResizeTextView autoResizeTextView8 = a2.o0;
                    ee7.a((Object) autoResizeTextView8, "it.tvAvgCalories");
                    autoResizeTextView8.setText(a4);
                    AutoResizeTextView autoResizeTextView9 = a2.i0;
                    ee7.a((Object) autoResizeTextView9, "it.tvAvgActiveTime");
                    autoResizeTextView9.setText(a4);
                    FlexibleTextView flexibleTextView7 = a2.m0;
                    ee7.a((Object) flexibleTextView7, "it.tvAvgActivityDate");
                    flexibleTextView7.setText("");
                    FlexibleTextView flexibleTextView8 = a2.p0;
                    ee7.a((Object) flexibleTextView8, "it.tvAvgCaloriesDate");
                    flexibleTextView8.setText("");
                    FlexibleTextView flexibleTextView9 = a2.j0;
                    ee7.a((Object) flexibleTextView9, "it.tvAvgActiveTimeDate");
                    flexibleTextView9.setText("");
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeProfileFragment", "active serial =" + PortfolioApp.g0.c().c());
            if (DeviceIdentityUtils.isDianaDevice(PortfolioApp.g0.c().c())) {
                g1();
            } else {
                h1();
            }
        } else {
            ee7.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void a(SleepStatistic sleepStatistic) {
        SleepStatistic.SleepDailyBest sleepTimeBestDay;
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 == null) {
                return;
            }
            if (sleepStatistic == null || (sleepTimeBestDay = sleepStatistic.getSleepTimeBestDay()) == null) {
                FlexibleTextView flexibleTextView = a2.r0;
                ee7.a((Object) flexibleTextView, "it.tvAvgSleep");
                flexibleTextView.setText(ig5.a(getContext(), 2131887289));
                FlexibleTextView flexibleTextView2 = a2.s0;
                ee7.a((Object) flexibleTextView2, "it.tvAvgSleepDate");
                flexibleTextView2.setText("");
                return;
            }
            FlexibleTextView flexibleTextView3 = a2.r0;
            ee7.a((Object) flexibleTextView3, "it.tvAvgSleep");
            flexibleTextView3.setText(xe5.b.d(sleepTimeBestDay.getValue()));
            FlexibleTextView flexibleTextView4 = a2.s0;
            ee7.a((Object) flexibleTextView4, "it.tvAvgSleepDate");
            flexibleTextView4.setText(zd5.c(sleepTimeBestDay.getDate()));
            return;
        }
        ee7.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void a(int i2, String str) {
        ee7.b(str, "message");
        if (isActive()) {
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            bx6.a(i2, str, childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void a(boolean z, mn4 mn4, boolean z2) {
        String str;
        if (z) {
            if (z2) {
                str = ig5.a(PortfolioApp.g0.c(), 2131886218);
            } else {
                str = ig5.a(PortfolioApp.g0.c(), 2131886242);
            }
            bx6 bx6 = bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            ee7.a((Object) childFragmentManager, "childFragmentManager");
            ee7.a((Object) str, "title");
            String a2 = ig5.a(PortfolioApp.g0.c(), 2131886238);
            ee7.a((Object) a2, "LanguageHelper.getString\u2026ourCurrentChallengeFirst)");
            bx6.a(childFragmentManager, str, a2, mn4);
        } else if (z2) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                PairingInstructionsActivity.a aVar = PairingInstructionsActivity.z;
                ee7.a((Object) activity, "it");
                PairingInstructionsActivity.a.a(aVar, activity, false, false, 4, null);
            }
        } else {
            bx6 bx62 = bx6.c;
            FragmentManager childFragmentManager2 = getChildFragmentManager();
            ee7.a((Object) childFragmentManager2, "childFragmentManager");
            bx62.y(childFragmentManager2);
        }
    }

    @DexIgnore
    @Override // com.fossil.nh6
    public void a(lh6 lh6) {
        ee7.b(lh6, "activityDailyBest");
        qw6<w25> qw6 = this.h;
        if (qw6 != null) {
            w25 a2 = qw6.a();
            if (a2 != null) {
                AutoResizeTextView autoResizeTextView = a2.l0;
                ee7.a((Object) autoResizeTextView, "it.tvAvgActivity");
                autoResizeTextView.setText(re5.b((int) lh6.b()));
                FlexibleTextView flexibleTextView = a2.m0;
                ee7.a((Object) flexibleTextView, "it.tvAvgActivityDate");
                flexibleTextView.setText(zd5.c(lh6.a()));
                return;
            }
            return;
        }
        ee7.d("mBinding");
        throw null;
    }
}
