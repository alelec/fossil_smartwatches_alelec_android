package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h21 extends eo0 {
    @DexIgnore
    public /* final */ nm1 j; // = nm1.HIGH;
    @DexIgnore
    public p91 k; // = p91.DISCONNECTED;

    @DexIgnore
    public h21(cx0 cx0) {
        super(aq0.m, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        c(s91);
        x71 x71 = s91.a;
        if (x71.a != z51.SUCCESS) {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        } else if (this.k == p91.DISCONNECTED) {
            lk0 = lk0.a(((eo0) this).d, null, oi0.a, null, 5);
        } else {
            lk0 = lk0.a(((eo0) this).d, null, oi0.d, null, 5);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public nm1 b() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.l;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return (s91 instanceof tt0) && ((tt0) s91).b == p91.DISCONNECTED;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.k = ((tt0) s91).b;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        int i = xe1.d[ri1.e().ordinal()];
        if (i == 1) {
            x71 x71 = new x71(z51.SUCCESS, 0, 2);
            p91 p91 = p91.DISCONNECTED;
            ri1.a.post(new n01(ri1, x71, p91, p91));
        } else if (i == 2 || i == 3) {
            x71 a = x71.c.a(zz0.d.b(ri1.x));
            if (a.a != z51.SUCCESS) {
                ri1.a.post(new n01(ri1, a, p91.CONNECTED, p91.DISCONNECTED));
            }
        }
    }
}
