package com.fossil;

import com.fossil.mu;
import com.fossil.wearables.fsl.dial.ConfigItem;
import com.j256.ormlite.stmt.query.SimpleComparison;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nv {
    @DexIgnore
    public static mu.a a(vu vuVar) {
        long j;
        long j2;
        boolean z;
        long j3;
        long j4;
        long j5;
        long currentTimeMillis = System.currentTimeMillis();
        Map<String, String> map = vuVar.c;
        String str = map.get(ConfigItem.KEY_DATE);
        long a = str != null ? a(str) : 0;
        String str2 = map.get(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
        int i = 0;
        if (str2 != null) {
            String[] split = str2.split(",", 0);
            int i2 = 0;
            j2 = 0;
            j = 0;
            while (i < split.length) {
                String trim = split[i].trim();
                if (trim.equals("no-cache") || trim.equals("no-store")) {
                    return null;
                }
                if (trim.startsWith("max-age=")) {
                    try {
                        j2 = Long.parseLong(trim.substring(8));
                    } catch (Exception unused) {
                    }
                } else if (trim.startsWith("stale-while-revalidate=")) {
                    j = Long.parseLong(trim.substring(23));
                } else if (trim.equals("must-revalidate") || trim.equals("proxy-revalidate")) {
                    i2 = 1;
                }
                i++;
            }
            i = i2;
            z = true;
        } else {
            z = false;
            j2 = 0;
            j = 0;
        }
        String str3 = map.get("Expires");
        long a2 = str3 != null ? a(str3) : 0;
        String str4 = map.get("Last-Modified");
        long a3 = str4 != null ? a(str4) : 0;
        String str5 = map.get("ETag");
        if (z) {
            j4 = currentTimeMillis + (j2 * 1000);
            if (i != 0) {
                j5 = j4;
            } else {
                Long.signum(j);
                j5 = (j * 1000) + j4;
            }
            j3 = j5;
        } else {
            j3 = 0;
            if (a <= 0 || a2 < a) {
                j4 = 0;
            } else {
                j4 = currentTimeMillis + (a2 - a);
                j3 = j4;
            }
        }
        mu.a aVar = new mu.a();
        aVar.a = vuVar.b;
        aVar.b = str5;
        aVar.f = j4;
        aVar.e = j3;
        aVar.c = a;
        aVar.d = a3;
        aVar.g = map;
        aVar.h = vuVar.d;
        return aVar;
    }

    @DexIgnore
    public static long a(String str) {
        try {
            return a().parse(str).getTime();
        } catch (ParseException e) {
            gv.a(e, "Unable to parse dateStr: %s, falling back to 0", str);
            return 0;
        }
    }

    @DexIgnore
    public static String a(long j) {
        return a().format(new Date(j));
    }

    @DexIgnore
    public static SimpleDateFormat a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return simpleDateFormat;
    }

    @DexIgnore
    public static String a(Map<String, String> map, String str) {
        String str2 = map.get("Content-Type");
        if (str2 != null) {
            String[] split = str2.split(";", 0);
            for (int i = 1; i < split.length; i++) {
                String[] split2 = split[i].trim().split(SimpleComparison.EQUAL_TO_OPERATION, 0);
                if (split2.length == 2 && split2[0].equals("charset")) {
                    return split2[1];
                }
            }
        }
        return str;
    }

    @DexIgnore
    public static Map<String, String> a(List<ru> list) {
        TreeMap treeMap = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (ru ruVar : list) {
            treeMap.put(ruVar.a(), ruVar.b());
        }
        return treeMap;
    }

    @DexIgnore
    public static List<ru> a(Map<String, String> map) {
        ArrayList arrayList = new ArrayList(map.size());
        for (Map.Entry<String, String> entry : map.entrySet()) {
            arrayList.add(new ru(entry.getKey(), entry.getValue()));
        }
        return arrayList;
    }
}
