package com.fossil;

import android.graphics.drawable.Drawable;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ev3 {
    @DexIgnore
    public static av3 a(int i) {
        if (i == 0) {
            return new gv3();
        }
        if (i != 1) {
            return a();
        }
        return new bv3();
    }

    @DexIgnore
    public static cv3 b() {
        return new cv3();
    }

    @DexIgnore
    public static av3 a() {
        return new gv3();
    }

    @DexIgnore
    public static void a(View view, float f) {
        Drawable background = view.getBackground();
        if (background instanceof dv3) {
            ((dv3) background).b(f);
        }
    }

    @DexIgnore
    public static void a(View view) {
        Drawable background = view.getBackground();
        if (background instanceof dv3) {
            a(view, (dv3) background);
        }
    }

    @DexIgnore
    public static void a(View view, dv3 dv3) {
        if (dv3.y()) {
            dv3.d(lu3.a(view));
        }
    }
}
