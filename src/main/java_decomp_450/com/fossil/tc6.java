package com.fossil;

import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc6 implements Factory<sc6> {
    @DexIgnore
    public static sc6 a(qc6 qc6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        return new sc6(qc6, userRepository, heartRateSummaryRepository);
    }
}
