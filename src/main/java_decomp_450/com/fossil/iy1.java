package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import com.facebook.places.internal.LocationScannerImpl;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class iy1 {
    @DexIgnore
    public h02 a;
    @DexIgnore
    public ef2 b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ long h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(String str, boolean z) {
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public final String toString() {
            String str = this.a;
            boolean z = this.b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z);
            return sb.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Thread {
        @DexIgnore
        public WeakReference<iy1> a;
        @DexIgnore
        public long b;
        @DexIgnore
        public CountDownLatch c; // = new CountDownLatch(1);
        @DexIgnore
        public boolean d; // = false;

        @DexIgnore
        public b(iy1 iy1, long j) {
            this.a = new WeakReference<>(iy1);
            this.b = j;
            start();
        }

        @DexIgnore
        public final void a() {
            iy1 iy1 = this.a.get();
            if (iy1 != null) {
                iy1.a();
                this.d = true;
            }
        }

        @DexIgnore
        public final void run() {
            try {
                if (!this.c.await(this.b, TimeUnit.MILLISECONDS)) {
                    a();
                }
            } catch (InterruptedException unused) {
                a();
            }
        }
    }

    @DexIgnore
    public iy1(Context context, long j, boolean z, boolean z2) {
        Context applicationContext;
        a72.a(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f = context;
        this.c = false;
        this.h = j;
        this.g = z2;
    }

    @DexIgnore
    public static ef2 a(Context context, h02 h02) throws IOException {
        try {
            return ff2.a(h02.a(ButtonService.CONNECT_TIMEOUT, TimeUnit.MILLISECONDS));
        } catch (InterruptedException unused) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    @DexIgnore
    public static h02 a(Context context, boolean z) throws IOException, n02, o02 {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int a2 = m02.a().a(context, q02.a);
            if (a2 == 0 || a2 == 2) {
                String str = z ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                h02 h02 = new h02();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (e92.a().a(context, intent, h02, 1)) {
                        return h02;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            } else {
                throw new IOException("Google Play services not available");
            }
        } catch (PackageManager.NameNotFoundException unused) {
            throw new n02(9);
        }
    }

    @DexIgnore
    public static a a(Context context) throws IOException, IllegalStateException, n02, o02 {
        ky1 ky1 = new ky1(context);
        boolean a2 = ky1.a("gads:ad_id_app_context:enabled", false);
        float a3 = ky1.a("gads:ad_id_app_context:ping_ratio", LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        String a4 = ky1.a("gads:ad_id_use_shared_preference:experiment_id", "");
        iy1 iy1 = new iy1(context, -1, a2, ky1.a("gads:ad_id_use_persistent_service:enabled", false));
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            iy1.a(false);
            a b2 = iy1.b();
            iy1.a(b2, a2, a3, SystemClock.elapsedRealtime() - elapsedRealtime, a4, null);
            iy1.a();
            return b2;
        } catch (Throwable th) {
            iy1.a();
            throw th;
        }
    }

    @DexIgnore
    public static void b(boolean z) {
    }

    @DexIgnore
    public final void a() {
        a72.c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.f != null && this.a != null) {
                try {
                    if (this.c) {
                        e92.a().a(this.f, this.a);
                    }
                } catch (Throwable th) {
                    Log.i("AdvertisingIdClient", "AdvertisingIdClient unbindService failed.", th);
                }
                this.c = false;
                this.b = null;
                this.a = null;
            }
        }
    }

    @DexIgnore
    public final void a(boolean z) throws IOException, IllegalStateException, n02, o02 {
        a72.c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.c) {
                a();
            }
            h02 a2 = a(this.f, this.g);
            this.a = a2;
            this.b = a(this.f, a2);
            this.c = true;
            if (z) {
                c();
            }
        }
    }

    @DexIgnore
    public final boolean a(a aVar, boolean z, float f2, long j, String str, Throwable th) {
        if (Math.random() > ((double) f2)) {
            return false;
        }
        HashMap hashMap = new HashMap();
        String str2 = "1";
        hashMap.put("app_context", z ? str2 : "0");
        if (aVar != null) {
            if (!aVar.b()) {
                str2 = "0";
            }
            hashMap.put("limit_ad_tracking", str2);
        }
        if (!(aVar == null || aVar.a() == null)) {
            hashMap.put("ad_id_size", Integer.toString(aVar.a().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put("tag", "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new jy1(this, hashMap).start();
        return true;
    }

    @DexIgnore
    public a b() throws IOException {
        a aVar;
        a72.c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.c) {
                synchronized (this.d) {
                    if (this.e == null || !this.e.d) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    a(false);
                    if (!this.c) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (Exception e2) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e2);
                }
            }
            a72.a(this.a);
            a72.a(this.b);
            try {
                aVar = new a(this.b.getId(), this.b.b(true));
            } catch (RemoteException e3) {
                Log.i("AdvertisingIdClient", "GMS remote exception ", e3);
                throw new IOException("Remote exception");
            }
        }
        c();
        return aVar;
    }

    @DexIgnore
    public final void c() {
        synchronized (this.d) {
            if (this.e != null) {
                this.e.c.countDown();
                try {
                    this.e.join();
                } catch (InterruptedException unused) {
                }
            }
            if (this.h > 0) {
                this.e = new b(this, this.h);
            }
        }
    }

    @DexIgnore
    public void finalize() throws Throwable {
        a();
        super.finalize();
    }
}
