package com.fossil;

import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface sf7<R> extends rf7 {
    @DexIgnore
    R call(Object... objArr);

    @DexIgnore
    R callBy(Map<Object, ? extends Object> map);

    @DexIgnore
    String getName();

    @DexIgnore
    List<Object> getParameters();

    @DexIgnore
    cg7 getReturnType();

    @DexIgnore
    List<Object> getTypeParameters();

    @DexIgnore
    dg7 getVisibility();

    @DexIgnore
    boolean isAbstract();

    @DexIgnore
    boolean isFinal();

    @DexIgnore
    boolean isOpen();

    @DexIgnore
    boolean isSuspend();
}
