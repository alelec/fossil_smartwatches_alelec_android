package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ra5 extends qa5 {
    @DexIgnore
    public static /* final */ ViewDataBinding.i N; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray O;
    @DexIgnore
    public long M;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        O = sparseIntArray;
        sparseIntArray.put(2131362741, 1);
        O.put(2131362703, 2);
        O.put(2131362624, 3);
        O.put(2131363396, 4);
        O.put(2131363388, 5);
        O.put(2131363384, 6);
        O.put(2131362929, 7);
        O.put(2131362533, 8);
        O.put(2131362532, 9);
        O.put(2131362332, 10);
        O.put(2131362387, 11);
        O.put(2131362505, 12);
        O.put(2131362351, 13);
        O.put(2131362419, 14);
        O.put(2131362420, 15);
        O.put(2131362627, 16);
        O.put(2131362663, 17);
        O.put(2131362724, 18);
        O.put(2131362645, 19);
        O.put(2131362691, 20);
        O.put(2131362692, 21);
    }
    */

    @DexIgnore
    public ra5(pb pbVar, View view) {
        this(pbVar, view, ViewDataBinding.a(pbVar, view, 22, N, O));
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.M = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            if (this.M != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.M = 1;
        }
        g();
    }

    @DexIgnore
    public ra5(pb pbVar, View view, Object[] objArr) {
        super(pbVar, view, 0, (ConstraintLayout) objArr[0], (FlexibleTextView) objArr[10], (FlexibleTextView) objArr[13], (FlexibleTextView) objArr[11], (FlexibleTextView) objArr[14], (FlexibleTextView) objArr[15], (FlexibleTextView) objArr[12], (FlexibleTextView) objArr[9], (FlexibleTextView) objArr[8], (RTLImageView) objArr[16], (RTLImageView) objArr[19], (RTLImageView) objArr[17], (RTLImageView) objArr[20], (RTLImageView) objArr[21], (ImageView) objArr[3], (ImageView) objArr[2], (RTLImageView) objArr[18], (ImageView) objArr[1], (FlexibleProgressBar) objArr[7], (View) objArr[6], (View) objArr[5], (View) objArr[4]);
        this.M = -1;
        ((qa5) this).q.setTag(null);
        a(view);
        f();
    }
}
