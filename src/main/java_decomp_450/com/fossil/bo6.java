package com.fossil;

import android.app.Activity;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.uirenew.customview.ExpandableTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bo6 extends FrameLayout implements View.OnClickListener {
    @DexIgnore
    public a a;
    @DexIgnore
    public ExpandableTextView b;
    @DexIgnore
    public InAppNotification c;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(InAppNotification inAppNotification);
    }

    @DexIgnore
    public void a() {
        ((ViewGroup) ((Activity) getContext()).findViewById(16908290)).removeView(this);
        a aVar = this.a;
        if (aVar != null) {
            aVar.a(this.c);
        }
    }

    @DexIgnore
    public void onClick(View view) {
        int id = view.getId();
        if (id == 2131362149) {
            this.b.e();
        } else if (id == 2131362200 || id == 2131363413) {
            a();
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
