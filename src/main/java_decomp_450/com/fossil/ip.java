package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.work.impl.WorkDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ip {
    @DexIgnore
    public /* final */ WorkDatabase a;

    @DexIgnore
    public ip(WorkDatabase workDatabase) {
        this.a = workDatabase;
    }

    @DexIgnore
    public int a(int i, int i2) {
        synchronized (ip.class) {
            int a2 = a("next_job_scheduler_id");
            if (a2 >= i) {
                if (a2 <= i2) {
                    i = a2;
                }
            }
            a("next_job_scheduler_id", i + 1);
        }
        return i;
    }

    @DexIgnore
    public int a() {
        int a2;
        synchronized (ip.class) {
            a2 = a("next_alarm_manager_id");
        }
        return a2;
    }

    @DexIgnore
    public final int a(String str) {
        this.a.beginTransaction();
        try {
            Long a2 = this.a.b().a(str);
            int i = 0;
            int intValue = a2 != null ? a2.intValue() : 0;
            if (intValue != Integer.MAX_VALUE) {
                i = intValue + 1;
            }
            a(str, i);
            this.a.setTransactionSuccessful();
            return intValue;
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public final void a(String str, int i) {
        this.a.b().a(new no(str, (long) i));
    }

    @DexIgnore
    public static void a(Context context, wi wiVar) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("androidx.work.util.id", 0);
        if (sharedPreferences.contains("next_job_scheduler_id") || sharedPreferences.contains("next_job_scheduler_id")) {
            int i = sharedPreferences.getInt("next_job_scheduler_id", 0);
            int i2 = sharedPreferences.getInt("next_alarm_manager_id", 0);
            wiVar.beginTransaction();
            try {
                wiVar.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_job_scheduler_id", Integer.valueOf(i)});
                wiVar.execSQL("INSERT OR REPLACE INTO `Preference` (`key`, `long_value`) VALUES (@key, @long_value)", new Object[]{"next_alarm_manager_id", Integer.valueOf(i2)});
                sharedPreferences.edit().clear().apply();
                wiVar.setTransactionSuccessful();
            } finally {
                wiVar.endTransaction();
            }
        }
    }
}
