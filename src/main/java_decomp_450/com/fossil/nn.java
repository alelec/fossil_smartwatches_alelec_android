package com.fossil;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.work.impl.background.systemjob.SystemJobService;
import com.fossil.bm;

public class nn {
    public static final String b = im.a("SystemJobInfoConverter");
    public final ComponentName a;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.fossil.jm[] r0 = com.fossil.jm.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.fossil.nn.a.a = r0
                com.fossil.jm r1 = com.fossil.jm.NOT_REQUIRED     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.fossil.nn.a.a     // Catch:{ NoSuchFieldError -> 0x001d }
                com.fossil.jm r1 = com.fossil.jm.CONNECTED     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.fossil.nn.a.a     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.fossil.jm r1 = com.fossil.jm.UNMETERED     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.fossil.nn.a.a     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.fossil.jm r1 = com.fossil.jm.NOT_ROAMING     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.fossil.nn.a.a     // Catch:{ NoSuchFieldError -> 0x003e }
                com.fossil.jm r1 = com.fossil.jm.METERED     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.nn.a.<clinit>():void");
        }
        */
    }

    public nn(Context context) {
        this.a = new ComponentName(context.getApplicationContext(), SystemJobService.class);
    }

    public JobInfo a(zo zoVar, int i) {
        am amVar = zoVar.j;
        int a2 = a(amVar.b());
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putString("EXTRA_WORK_SPEC_ID", zoVar.a);
        persistableBundle.putBoolean("EXTRA_IS_PERIODIC", zoVar.d());
        JobInfo.Builder extras = new JobInfo.Builder(i, this.a).setRequiredNetworkType(a2).setRequiresCharging(amVar.g()).setRequiresDeviceIdle(amVar.h()).setExtras(persistableBundle);
        if (!amVar.h()) {
            extras.setBackoffCriteria(zoVar.m, zoVar.l == yl.LINEAR ? 0 : 1);
        }
        long max = Math.max(zoVar.a() - System.currentTimeMillis(), 0L);
        if (Build.VERSION.SDK_INT <= 28) {
            extras.setMinimumLatency(max);
        } else if (max > 0) {
            extras.setMinimumLatency(max);
        } else {
            extras.setImportantWhileForeground(true);
        }
        if (Build.VERSION.SDK_INT >= 24 && amVar.e()) {
            for (bm.a aVar : amVar.a().a()) {
                extras.addTriggerContentUri(a(aVar));
            }
            extras.setTriggerContentUpdateDelay(amVar.c());
            extras.setTriggerContentMaxDelay(amVar.d());
        }
        extras.setPersisted(false);
        if (Build.VERSION.SDK_INT >= 26) {
            extras.setRequiresBatteryNotLow(amVar.f());
            extras.setRequiresStorageNotLow(amVar.i());
        }
        return extras.build();
    }

    public static JobInfo.TriggerContentUri a(bm.a aVar) {
        return new JobInfo.TriggerContentUri(aVar.a(), aVar.b() ? 1 : 0);
    }

    public static int a(jm jmVar) {
        int i = a.a[jmVar.ordinal()];
        if (i == 1) {
            return 0;
        }
        if (i == 2) {
            return 1;
        }
        if (i == 3) {
            return 2;
        }
        if (i != 4) {
            if (i == 5 && Build.VERSION.SDK_INT >= 26) {
                return 4;
            }
        } else if (Build.VERSION.SDK_INT >= 24) {
            return 3;
        }
        im.a().a(b, String.format("API version too low. Cannot convert network type value %s", jmVar), new Throwable[0]);
        return 1;
    }
}
