package com.fossil;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.fl4;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qn5 extends fl4<b, d, c> {
    @DexIgnore
    public static /* final */ String Q;
    @DexIgnore
    public static /* final */ a R; // = new a(null);
    @DexIgnore
    public /* final */ FitnessDataRepository A;
    @DexIgnore
    public /* final */ HeartRateSampleRepository B;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository C;
    @DexIgnore
    public /* final */ WorkoutSessionRepository D;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase E;
    @DexIgnore
    public /* final */ fp4 F;
    @DexIgnore
    public /* final */ xo4 G;
    @DexIgnore
    public /* final */ ro4 H;
    @DexIgnore
    public /* final */ bp4 I;
    @DexIgnore
    public /* final */ FileRepository J;
    @DexIgnore
    public /* final */ QuickResponseRepository K;
    @DexIgnore
    public /* final */ WatchFaceRepository L;
    @DexIgnore
    public /* final */ RingStyleRepository M;
    @DexIgnore
    public /* final */ to4 N;
    @DexIgnore
    public /* final */ WorkoutSettingRepository O;
    @DexIgnore
    public /* final */ WatchAppDataRepository P;
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ AlarmsRepository e;
    @DexIgnore
    public /* final */ ch5 f;
    @DexIgnore
    public /* final */ HybridPresetRepository g;
    @DexIgnore
    public /* final */ ActivitiesRepository h;
    @DexIgnore
    public /* final */ SummariesRepository i;
    @DexIgnore
    public /* final */ MicroAppSettingRepository j;
    @DexIgnore
    public /* final */ NotificationsRepository k;
    @DexIgnore
    public /* final */ DeviceRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ GoalTrackingRepository n;
    @DexIgnore
    public /* final */ jh5 o;
    @DexIgnore
    public /* final */ ie5 p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ DianaPresetRepository r;
    @DexIgnore
    public /* final */ WatchAppRepository s;
    @DexIgnore
    public /* final */ ComplicationRepository t;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase u;
    @DexIgnore
    public /* final */ DNDSettingsDatabase v;
    @DexIgnore
    public /* final */ MicroAppRepository w;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository x;
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository y;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return qn5.Q;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements fl4.b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ WeakReference<Activity> b;

        @DexIgnore
        public b(int i, WeakReference<Activity> weakReference) {
            this.a = i;
            this.b = weakReference;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final WeakReference<Activity> b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements fl4.a {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements fl4.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$clearUserData$1", f = "DeleteLogoutUserUseCase.kt", l = {178, 179, 180, 184, 186, 195, 196, 197, Action.Selfie.TAKE_BURST, 209, 210, 211, 212, 213, 223}, m = "invokeSuspend")
    public static final class e extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(qn5 qn5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = qn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            e eVar = new e(this.this$0, fb7);
            eVar.p$ = (yi7) obj;
            return eVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((e) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:30:0x012a A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x013c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0157 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0169 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x01ad A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x01bf A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01d2 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:51:0x0209 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x025e A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x0271 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:60:0x0284 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x0297 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:66:0x02aa A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0303 A[RETURN] */
        @Override // com.fossil.ob7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r7) {
            /*
                r6 = this;
                java.lang.Object r0 = com.fossil.nb7.a()
                int r1 = r6.label
                switch(r1) {
                    case 0: goto L_0x0098;
                    case 1: goto L_0x008f;
                    case 2: goto L_0x0086;
                    case 3: goto L_0x007d;
                    case 4: goto L_0x0074;
                    case 5: goto L_0x006b;
                    case 6: goto L_0x0062;
                    case 7: goto L_0x0059;
                    case 8: goto L_0x0050;
                    case 9: goto L_0x0047;
                    case 10: goto L_0x003e;
                    case 11: goto L_0x0035;
                    case 12: goto L_0x002c;
                    case 13: goto L_0x0023;
                    case 14: goto L_0x001a;
                    case 15: goto L_0x0011;
                    default: goto L_0x0009;
                }
            L_0x0009:
                java.lang.IllegalStateException r7 = new java.lang.IllegalStateException
                java.lang.String r0 = "call to 'resume' before 'invoke' with coroutine"
                r7.<init>(r0)
                throw r7
            L_0x0011:
                java.lang.Object r0 = r6.L$0
                com.fossil.yi7 r0 = (com.fossil.yi7) r0
                com.fossil.t87.a(r7)
                goto L_0x0304
            L_0x001a:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x02ab
            L_0x0023:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0298
            L_0x002c:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0285
            L_0x0035:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0272
            L_0x003e:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x025f
            L_0x0047:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x020a
            L_0x0050:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x01d3
            L_0x0059:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x01c0
            L_0x0062:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x01ae
            L_0x006b:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x016a
            L_0x0074:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0158
            L_0x007d:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x013d
            L_0x0086:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x012b
            L_0x008f:
                java.lang.Object r1 = r6.L$0
                com.fossil.yi7 r1 = (com.fossil.yi7) r1
                com.fossil.t87.a(r7)
                goto L_0x0119
            L_0x0098:
                com.fossil.t87.a(r7)
                com.fossil.yi7 r7 = r6.p$
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.IRemoteFLogger r1 = r1.getRemote()
                r1.flush()
                com.fossil.ix6 r1 = com.fossil.ix6.b     // Catch:{ Exception -> 0x00ae }
                java.lang.String r2 = "deviceSecretKey"
                r1.f(r2)     // Catch:{ Exception -> 0x00ae }
                goto L_0x00cf
            L_0x00ae:
                r1 = move-exception
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.fossil.qn5$a r3 = com.fossil.qn5.R
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "exception when remove alias in keystore "
                r4.append(r5)
                r4.append(r1)
                java.lang.String r1 = r4.toString()
                r2.e(r3, r1)
            L_0x00cf:
                com.fossil.qn5 r1 = r6.this$0
                com.fossil.ch5 r1 = r1.f
                r2 = 0
                r1.a(r2)
                com.fossil.qn5 r1 = r6.this$0
                com.fossil.fp4 r1 = r1.F
                r1.a()
                com.fossil.qn5 r1 = r6.this$0
                com.fossil.xo4 r1 = r1.G
                r1.a()
                com.fossil.qn5 r1 = r6.this$0
                com.fossil.bp4 r1 = r1.I
                r1.a()
                com.fossil.qn5 r1 = r6.this$0
                com.fossil.ro4 r1 = r1.H
                r1.a()
                com.fossil.qn5 r1 = r6.this$0
                com.portfolio.platform.data.source.NotificationsRepository r1 = r1.k
                r1.clearAllNotificationSetting()
                com.fossil.qn5 r1 = r6.this$0
                com.portfolio.platform.data.source.SleepSessionsRepository r1 = r1.m
                r6.L$0 = r7
                r2 = 1
                r6.label = r2
                java.lang.Object r1 = r1.cleanUp(r6)
                if (r1 != r0) goto L_0x0118
                return r0
            L_0x0118:
                r1 = r7
            L_0x0119:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.SleepSummariesRepository r7 = r7.q
                r6.L$0 = r1
                r2 = 2
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x012b
                return r0
            L_0x012b:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.FitnessDataRepository r7 = r7.A
                r6.L$0 = r1
                r2 = 3
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x013d
                return r0
            L_0x013d:
                com.fossil.qn5 r7 = r6.this$0
                com.fossil.ch5 r7 = r7.f
                r7.c0()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.UserRepository r7 = r7.d
                r6.L$0 = r1
                r2 = 4
                r6.label = r2
                java.lang.Object r7 = r7.clearAllUser(r6)
                if (r7 != r0) goto L_0x0158
                return r0
            L_0x0158:
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                r6.L$0 = r1
                r2 = 5
                r6.label = r2
                java.lang.Object r7 = r7.m(r6)
                if (r7 != r0) goto L_0x016a
                return r0
            L_0x016a:
                com.fossil.qn5 r7 = r6.this$0
                r7.d()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.MicroAppLastSettingRepository r7 = r7.x
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.ComplicationLastSettingRepository r7 = r7.y
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WatchAppLastSettingRepository r7 = r7.z
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.HybridPresetRepository r7 = r7.g
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.MicroAppRepository r7 = r7.w
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.ActivitiesRepository r7 = r7.h
                r6.L$0 = r1
                r2 = 6
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x01ae
                return r0
            L_0x01ae:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.SummariesRepository r7 = r7.i
                r6.L$0 = r1
                r2 = 7
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x01c0
                return r0
            L_0x01c0:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.GoalTrackingRepository r7 = r7.n
                r6.L$0 = r1
                r2 = 8
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x01d3
                return r0
            L_0x01d3:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.DianaPresetRepository r7 = r7.r
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WatchAppRepository r7 = r7.s
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.ComplicationRepository r7 = r7.t
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository r7 = r7.j
                r7.clearData()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.AlarmsRepository r7 = r7.e
                r6.L$0 = r1
                r2 = 9
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x020a
                return r0
            L_0x020a:
                com.fossil.kj5 r7 = com.fossil.kj5.g
                r7.a()
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                r7.I()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase r7 = r7.u
                com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao r7 = r7.getNotificationSettingsDao()
                r7.delete()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase r7 = r7.v
                com.portfolio.platform.data.source.local.dnd.DNDScheduledTimeDao r7 = r7.getDNDScheduledTimeDao()
                r7.delete()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase r7 = r7.E
                com.portfolio.platform.data.source.local.reminders.InactivityNudgeTimeDao r7 = r7.getInactivityNudgeTimeDao()
                r7.delete()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase r7 = r7.E
                com.portfolio.platform.data.source.local.reminders.RemindTimeDao r7 = r7.getRemindTimeDao()
                r7.delete()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WorkoutSettingRepository r7 = r7.O
                r6.L$0 = r1
                r2 = 10
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x025f
                return r0
            L_0x025f:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WatchAppDataRepository r7 = r7.P
                r6.L$0 = r1
                r2 = 11
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x0272
                return r0
            L_0x0272:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.HeartRateSampleRepository r7 = r7.B
                r6.L$0 = r1
                r2 = 12
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x0285
                return r0
            L_0x0285:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.HeartRateSummaryRepository r7 = r7.C
                r6.L$0 = r1
                r2 = 13
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x0298
                return r0
            L_0x0298:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WorkoutSessionRepository r7 = r7.D
                r6.L$0 = r1
                r2 = 14
                r6.label = r2
                java.lang.Object r7 = r7.cleanUp(r6)
                if (r7 != r0) goto L_0x02ab
                return r0
            L_0x02ab:
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.RingStyleRepository r7 = r7.M
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.FileRepository r7 = r7.J
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.WatchFaceRepository r7 = r7.L
                r7.cleanUp()
                com.fossil.qn5 r7 = r6.this$0
                r7.e()
                com.fossil.qn5 r7 = r6.this$0
                com.portfolio.platform.data.source.DeviceRepository r7 = r7.l
                r7.cleanUp()
                com.fossil.qe5 r7 = com.fossil.qe5.c
                r7.a()
                com.fossil.pg5 r7 = com.fossil.pg5.i
                r7.c()
                com.fossil.ah5$a r7 = com.fossil.ah5.p
                com.fossil.ah5 r7 = r7.a()
                r7.m()
                com.fossil.qn5 r7 = r6.this$0
                com.fossil.qn5$d r2 = new com.fossil.qn5$d
                r2.<init>()
                r7.a(r2)
                com.portfolio.platform.PortfolioApp$a r7 = com.portfolio.platform.PortfolioApp.g0
                com.portfolio.platform.PortfolioApp r7 = r7.c()
                r6.L$0 = r1
                r1 = 15
                r6.label = r1
                java.lang.Object r7 = r7.b(r6)
                if (r7 != r0) goto L_0x0304
                return r0
            L_0x0304:
                com.fossil.i97 r7 = com.fossil.i97.a
                return r7
                switch-data {0->0x0098, 1->0x008f, 2->0x0086, 3->0x007d, 4->0x0074, 5->0x006b, 6->0x0062, 7->0x0059, 8->0x0050, 9->0x0047, 10->0x003e, 11->0x0035, 12->0x002c, 13->0x0023, 14->0x001a, 15->0x0011, }
            */
            throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn5.e.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1", f = "DeleteLogoutUserUseCase.kt", l = {146}, m = "invokeSuspend")
    public static final class f extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public yi7 p$;
        @DexIgnore
        public /* final */ /* synthetic */ qn5 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @tb7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase$resetQuickResponseMessage$1$1", f = "DeleteLogoutUserUseCase.kt", l = {}, m = "invokeSuspend")
        public static final class a extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yi7 p$;
            @DexIgnore
            public /* final */ /* synthetic */ f this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(f fVar, fb7 fb7) {
                super(2, fb7);
                this.this$0 = fVar;
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final fb7<i97> create(Object obj, fb7<?> fb7) {
                ee7.b(fb7, "completion");
                a aVar = new a(this.this$0, fb7);
                aVar.p$ = (yi7) obj;
                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.fossil.kd7
            public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
                return ((a) create(yi7, fb7)).invokeSuspend(i97.a);
            }

            @DexIgnore
            @Override // com.fossil.ob7
            public final Object invokeSuspend(Object obj) {
                nb7.a();
                if (this.label == 0) {
                    t87.a(obj);
                    this.this$0.this$0.K.removeAll();
                    String a = ig5.a(PortfolioApp.g0.c(), 2131886131);
                    ee7.a((Object) a, "LanguageHelper.getString\u2026an_Text__CanICallYouBack)");
                    String a2 = ig5.a(PortfolioApp.g0.c(), 2131886132);
                    ee7.a((Object) a2, "LanguageHelper.getString\u2026_MessageOn_Text__OnMyWay)");
                    String a3 = ig5.a(PortfolioApp.g0.c(), 2131886133);
                    ee7.a((Object) a3, "LanguageHelper.getString\u2026y_Text__SorryCantTalkNow)");
                    this.this$0.this$0.K.insertQRs(w97.c(new QuickResponseMessage(a), new QuickResponseMessage(a2), new QuickResponseMessage(a3)));
                    return i97.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(qn5 qn5, fb7 fb7) {
            super(2, fb7);
            this.this$0 = qn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final fb7<i97> create(Object obj, fb7<?> fb7) {
            ee7.b(fb7, "completion");
            f fVar = new f(this.this$0, fb7);
            fVar.p$ = (yi7) obj;
            return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.fossil.kd7
        public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
            return ((f) create(yi7, fb7)).invokeSuspend(i97.a);
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            Object a2 = nb7.a();
            int i = this.label;
            if (i == 0) {
                t87.a(obj);
                yi7 yi7 = this.p$;
                ti7 b = qj7.b();
                a aVar = new a(this, null);
                this.L$0 = yi7;
                this.label = 1;
                if (vh7.a(b, aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                yi7 yi72 = (yi7) this.L$0;
                t87.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.d(pb7.a(false));
            return i97.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @tb7(c = "com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase", f = "DeleteLogoutUserUseCase.kt", l = {87, 96, 98, 117, 132}, m = "run")
    public static final class g extends rb7 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ qn5 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(qn5 qn5, fb7 fb7) {
            super(fb7);
            this.this$0 = qn5;
        }

        @DexIgnore
        @Override // com.fossil.ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((b) null, (fb7<Object>) this);
        }
    }

    /*
    static {
        String simpleName = qn5.class.getSimpleName();
        ee7.a((Object) simpleName, "DeleteLogoutUserUseCase::class.java.simpleName");
        Q = simpleName;
    }
    */

    @DexIgnore
    public qn5(UserRepository userRepository, AlarmsRepository alarmsRepository, ch5 ch5, HybridPresetRepository hybridPresetRepository, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, MicroAppSettingRepository microAppSettingRepository, NotificationsRepository notificationsRepository, DeviceRepository deviceRepository, SleepSessionsRepository sleepSessionsRepository, GoalTrackingRepository goalTrackingRepository, jh5 jh5, ie5 ie5, SleepSummariesRepository sleepSummariesRepository, DianaPresetRepository dianaPresetRepository, WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, NotificationSettingsDatabase notificationSettingsDatabase, DNDSettingsDatabase dNDSettingsDatabase, MicroAppRepository microAppRepository, MicroAppLastSettingRepository microAppLastSettingRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, FitnessDataRepository fitnessDataRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, RemindersSettingsDatabase remindersSettingsDatabase, fp4 fp4, xo4 xo4, ro4 ro4, bp4 bp4, FileRepository fileRepository, QuickResponseRepository quickResponseRepository, WatchFaceRepository watchFaceRepository, RingStyleRepository ringStyleRepository, to4 to4, WorkoutSettingRepository workoutSettingRepository, WatchAppDataRepository watchAppDataRepository) {
        ee7.b(userRepository, "mUserRepository");
        ee7.b(alarmsRepository, "mAlarmRepository");
        ee7.b(ch5, "mSharedPreferences");
        ee7.b(hybridPresetRepository, "mPresetRepository");
        ee7.b(activitiesRepository, "mActivitiesRepository");
        ee7.b(summariesRepository, "mSummariesRepository");
        ee7.b(microAppSettingRepository, "mMicroAppSettingRepository");
        ee7.b(notificationsRepository, "mNotificationRepository");
        ee7.b(deviceRepository, "mDeviceRepository");
        ee7.b(sleepSessionsRepository, "mSleepSessionsRepository");
        ee7.b(goalTrackingRepository, "mGoalTrackingRepository");
        ee7.b(jh5, "mLoginGoogleManager");
        ee7.b(ie5, "mGoogleFitHelper");
        ee7.b(sleepSummariesRepository, "mSleepSummariesRepository");
        ee7.b(dianaPresetRepository, "mDianaPresetRepository");
        ee7.b(watchAppRepository, "mWatchAppRepository");
        ee7.b(complicationRepository, "mComplicationRepository");
        ee7.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        ee7.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        ee7.b(microAppRepository, "mMicroAppRepository");
        ee7.b(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        ee7.b(complicationLastSettingRepository, "mComplicationLastSettingRepository");
        ee7.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        ee7.b(fitnessDataRepository, "mFitnessDataRepository");
        ee7.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        ee7.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        ee7.b(workoutSessionRepository, "mWorkoutSessionRepository");
        ee7.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        ee7.b(fp4, "mSocialProfileRepository");
        ee7.b(xo4, "mSocialFriendRepository");
        ee7.b(ro4, "challengeRepository");
        ee7.b(bp4, "notificationRepository");
        ee7.b(fileRepository, "mFileRepository");
        ee7.b(quickResponseRepository, "mQuickResponseRepository");
        ee7.b(watchFaceRepository, "mWatchFaceRepository");
        ee7.b(ringStyleRepository, "mRingStyleRepository");
        ee7.b(to4, "fcmRepository");
        ee7.b(workoutSettingRepository, "mWorkoutSettingRepository");
        ee7.b(watchAppDataRepository, "mWatchAppDataRepository");
        this.d = userRepository;
        this.e = alarmsRepository;
        this.f = ch5;
        this.g = hybridPresetRepository;
        this.h = activitiesRepository;
        this.i = summariesRepository;
        this.j = microAppSettingRepository;
        this.k = notificationsRepository;
        this.l = deviceRepository;
        this.m = sleepSessionsRepository;
        this.n = goalTrackingRepository;
        this.o = jh5;
        this.p = ie5;
        this.q = sleepSummariesRepository;
        this.r = dianaPresetRepository;
        this.s = watchAppRepository;
        this.t = complicationRepository;
        this.u = notificationSettingsDatabase;
        this.v = dNDSettingsDatabase;
        this.w = microAppRepository;
        this.x = microAppLastSettingRepository;
        this.y = complicationLastSettingRepository;
        this.z = watchAppLastSettingRepository;
        this.A = fitnessDataRepository;
        this.B = heartRateSampleRepository;
        this.C = heartRateSummaryRepository;
        this.D = workoutSessionRepository;
        this.E = remindersSettingsDatabase;
        this.F = fp4;
        this.G = xo4;
        this.H = ro4;
        this.I = bp4;
        this.J = fileRepository;
        this.K = quickResponseRepository;
        this.L = watchFaceRepository;
        this.M = ringStyleRepository;
        this.N = to4;
        this.O = workoutSettingRepository;
        this.P = watchAppDataRepository;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.fl4$b, com.fossil.fb7] */
    @Override // com.fossil.fl4
    public /* bridge */ /* synthetic */ Object a(b bVar, fb7 fb7) {
        return a(bVar, (fb7<Object>) fb7);
    }

    @DexIgnore
    @Override // com.fossil.fl4
    public String c() {
        return Q;
    }

    @DexIgnore
    public final void d() {
        if (this.p.e()) {
            this.p.h();
        }
    }

    @DexIgnore
    public final void e() {
        ik7 unused = xh7.b(b(), null, null, new f(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0164  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01c3  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01c7  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0213  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0217  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(com.fossil.qn5.b r18, com.fossil.fb7<java.lang.Object> r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r2 instanceof com.fossil.qn5.g
            if (r3 == 0) goto L_0x0019
            r3 = r2
            com.fossil.qn5$g r3 = (com.fossil.qn5.g) r3
            int r4 = r3.label
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = r4 & r5
            if (r6 == 0) goto L_0x0019
            int r4 = r4 - r5
            r3.label = r4
            goto L_0x001e
        L_0x0019:
            com.fossil.qn5$g r3 = new com.fossil.qn5$g
            r3.<init>(r0, r2)
        L_0x001e:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.nb7.a()
            int r5 = r3.label
            r6 = 404(0x194, float:5.66E-43)
            r7 = 401(0x191, float:5.62E-43)
            java.lang.String r8 = "Inside .run failed with http code="
            java.lang.String r9 = ""
            r10 = 200(0xc8, float:2.8E-43)
            r12 = 5
            r13 = 4
            r14 = 3
            r15 = 2
            r11 = 1
            if (r5 == 0) goto L_0x00a4
            if (r5 == r11) goto L_0x0098
            if (r5 == r15) goto L_0x0080
            if (r5 == r14) goto L_0x006b
            if (r5 == r13) goto L_0x005a
            if (r5 != r12) goto L_0x0052
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r3.L$1
            com.fossil.qn5$b r1 = (com.fossil.qn5.b) r1
            java.lang.Object r3 = r3.L$0
            com.fossil.qn5 r3 = (com.fossil.qn5) r3
            com.fossil.t87.a(r2)
            goto L_0x020f
        L_0x0052:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r1.<init>(r2)
            throw r1
        L_0x005a:
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r3.L$1
            com.fossil.qn5$b r1 = (com.fossil.qn5.b) r1
            java.lang.Object r3 = r3.L$0
            com.fossil.qn5 r3 = (com.fossil.qn5) r3
            com.fossil.t87.a(r2)
            goto L_0x01bb
        L_0x006b:
            java.lang.Object r1 = r3.L$3
            com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r1 = r3.L$1
            com.fossil.qn5$b r1 = (com.fossil.qn5.b) r1
            java.lang.Object r3 = r3.L$0
            com.fossil.qn5 r3 = (com.fossil.qn5) r3
            com.fossil.t87.a(r2)
            goto L_0x014a
        L_0x0080:
            java.lang.Object r1 = r3.L$2
            com.fossil.ko4 r1 = (com.fossil.ko4) r1
            java.lang.Object r5 = r3.L$1
            com.fossil.qn5$b r5 = (com.fossil.qn5.b) r5
            java.lang.Object r11 = r3.L$0
            com.fossil.qn5 r11 = (com.fossil.qn5) r11
            com.fossil.t87.a(r2)
            r16 = r2
            r2 = r1
            r1 = r5
            r5 = r11
            r11 = r16
            goto L_0x0132
        L_0x0098:
            java.lang.Object r1 = r3.L$1
            com.fossil.qn5$b r1 = (com.fossil.qn5.b) r1
            java.lang.Object r5 = r3.L$0
            com.fossil.qn5 r5 = (com.fossil.qn5) r5
            com.fossil.t87.a(r2)
            goto L_0x00df
        L_0x00a4:
            com.fossil.t87.a(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r5 = com.fossil.qn5.Q
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = "running UseCase with mode="
            r12.append(r13)
            if (r1 == 0) goto L_0x00c4
            int r13 = r18.a()
            java.lang.Integer r13 = com.fossil.pb7.a(r13)
            goto L_0x00c5
        L_0x00c4:
            r13 = 0
        L_0x00c5:
            r12.append(r13)
            java.lang.String r12 = r12.toString()
            r2.d(r5, r12)
            com.fossil.to4 r2 = r0.N
            r3.L$0 = r0
            r3.L$1 = r1
            r3.label = r11
            java.lang.Object r2 = r2.b(r3)
            if (r2 != r4) goto L_0x00de
            return r4
        L_0x00de:
            r5 = r0
        L_0x00df:
            com.fossil.ko4 r2 = (com.fossil.ko4) r2
            com.portfolio.platform.data.model.ServerError r12 = r2.a()
            if (r12 == 0) goto L_0x0106
            com.fossil.qn5$c r1 = new com.fossil.qn5$c
            com.portfolio.platform.data.model.ServerError r2 = r2.a()
            java.lang.Integer r2 = r2.getCode()
            java.lang.String r3 = "wrapper.error.code"
            com.fossil.ee7.a(r2, r3)
            int r2 = r2.intValue()
            r1.<init>(r2)
            r5.a(r1)
            java.lang.Object r1 = new java.lang.Object
            r1.<init>()
            return r1
        L_0x0106:
            com.fossil.ch5 r12 = r5.f
            r12.s(r9)
            if (r1 == 0) goto L_0x0116
            int r12 = r1.a()
            java.lang.Integer r12 = com.fossil.pb7.a(r12)
            goto L_0x0117
        L_0x0116:
            r12 = 0
        L_0x0117:
            if (r12 != 0) goto L_0x011b
            goto L_0x019f
        L_0x011b:
            int r13 = r12.intValue()
            if (r13 != 0) goto L_0x019f
            com.portfolio.platform.data.source.UserRepository r11 = r5.d
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r3.label = r15
            java.lang.Object r11 = r11.getCurrentUser(r3)
            if (r11 != r4) goto L_0x0132
            return r4
        L_0x0132:
            com.portfolio.platform.data.model.MFUser r11 = (com.portfolio.platform.data.model.MFUser) r11
            if (r11 == 0) goto L_0x0221
            com.portfolio.platform.data.source.UserRepository r12 = r5.d
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r3.L$3 = r11
            r3.label = r14
            java.lang.Object r2 = r12.deleteUser(r3)
            if (r2 != r4) goto L_0x0149
            return r4
        L_0x0149:
            r3 = r5
        L_0x014a:
            java.lang.Number r2 = (java.lang.Number) r2
            int r2 = r2.intValue()
            java.lang.String r4 = "result"
            java.lang.String r5 = "remove_user"
            if (r2 != r10) goto L_0x0164
            com.fossil.qd5$a r2 = com.fossil.qd5.f
            com.fossil.qd5 r2 = r2.c()
            r2.b(r5, r4, r9)
            r3.a(r1)
            goto L_0x0221
        L_0x0164:
            com.misfit.frameworks.buttonservice.log.FLogger r9 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r9.getLocal()
            java.lang.String r10 = com.fossil.qn5.Q
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r8)
            r11.append(r2)
            java.lang.String r8 = r11.toString()
            r9.d(r10, r8)
            com.fossil.qd5$a r8 = com.fossil.qd5.f
            com.fossil.qd5 r8 = r8.c()
            java.lang.String r9 = java.lang.String.valueOf(r2)
            r8.b(r5, r4, r9)
            if (r2 == r7) goto L_0x019a
            if (r2 != r6) goto L_0x0190
            goto L_0x019a
        L_0x0190:
            com.fossil.qn5$c r1 = new com.fossil.qn5$c
            r1.<init>(r2)
            r3.a(r1)
            goto L_0x0221
        L_0x019a:
            r3.a(r1)
            goto L_0x0221
        L_0x019f:
            if (r12 != 0) goto L_0x01a2
            goto L_0x01f3
        L_0x01a2:
            int r9 = r12.intValue()
            if (r9 != r11) goto L_0x01f3
            com.portfolio.platform.data.source.UserRepository r9 = r5.d
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r2 = 4
            r3.label = r2
            java.lang.Object r2 = r9.logoutUser(r3)
            if (r2 != r4) goto L_0x01ba
            return r4
        L_0x01ba:
            r3 = r5
        L_0x01bb:
            java.lang.Number r2 = (java.lang.Number) r2
            int r2 = r2.intValue()
            if (r2 != r10) goto L_0x01c7
            r3.a(r1)
            goto L_0x0221
        L_0x01c7:
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.String r5 = com.fossil.qn5.Q
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r8)
            r9.append(r2)
            java.lang.String r8 = r9.toString()
            r4.d(r5, r8)
            if (r2 == r7) goto L_0x01ef
            if (r2 != r6) goto L_0x01e6
            goto L_0x01ef
        L_0x01e6:
            com.fossil.qn5$c r1 = new com.fossil.qn5$c
            r1.<init>(r2)
            r3.a(r1)
            goto L_0x0221
        L_0x01ef:
            r3.a(r1)
            goto L_0x0221
        L_0x01f3:
            if (r12 != 0) goto L_0x01f6
            goto L_0x0221
        L_0x01f6:
            int r6 = r12.intValue()
            if (r6 != r15) goto L_0x0221
            com.portfolio.platform.data.source.UserRepository r6 = r5.d
            r3.L$0 = r5
            r3.L$1 = r1
            r3.L$2 = r2
            r2 = 5
            r3.label = r2
            java.lang.Object r2 = r6.getCurrentUser(r3)
            if (r2 != r4) goto L_0x020e
            return r4
        L_0x020e:
            r3 = r5
        L_0x020f:
            com.portfolio.platform.data.model.MFUser r2 = (com.portfolio.platform.data.model.MFUser) r2
            if (r2 == 0) goto L_0x0217
            r3.a(r1)
            goto L_0x0221
        L_0x0217:
            com.fossil.qn5$c r1 = new com.fossil.qn5$c
            r2 = 600(0x258, float:8.41E-43)
            r1.<init>(r2)
            r3.a(r1)
        L_0x0221:
            java.lang.Object r1 = new java.lang.Object
            r1.<init>()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fossil.qn5.a(com.fossil.qn5$b, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void a(b bVar) {
        this.o.a(bVar.b());
        try {
            IButtonConnectivity b2 = PortfolioApp.g0.b();
            if (b2 != null) {
                b2.deviceUnlink(PortfolioApp.g0.c().c());
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        ik7 unused = xh7.b(b(), null, null, new e(this, null), 3, null);
    }
}
