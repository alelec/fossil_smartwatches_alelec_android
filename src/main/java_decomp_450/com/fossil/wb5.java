package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wb5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public float d;

    @DexIgnore
    public wb5(int i, int i2, int i3, float f) {
        this.a = i;
        this.b = i2;
        this.c = i3;
        this.d = f;
    }

    @DexIgnore
    public final int a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.a;
    }

    @DexIgnore
    public final float c() {
        return this.d;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof wb5)) {
            return false;
        }
        wb5 wb5 = (wb5) obj;
        return this.a == wb5.a && this.b == wb5.b && this.c == wb5.c && Float.compare(this.d, wb5.d) == 0;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + Float.floatToIntBits(this.d);
    }

    @DexIgnore
    public String toString() {
        return "WorkoutPickerDataWrapper(minValue=" + this.a + ", maxValue=" + this.b + ", value=" + this.c + ", step=" + this.d + ")";
    }
}
