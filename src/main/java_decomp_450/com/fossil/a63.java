package com.fossil;

import android.os.RemoteException;
import com.fossil.y12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a63 extends h22<yl2, c53> {
    @DexIgnore
    public /* final */ /* synthetic */ b53 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public a63(b53 b53, y12.a aVar) {
        super(aVar);
        this.b = b53;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.v02$b, com.fossil.oo3] */
    @Override // com.fossil.h22
    public final /* synthetic */ void a(yl2 yl2, oo3 oo3) throws RemoteException {
        try {
            yl2.b(a(), this.b.a((oo3<Boolean>) oo3));
        } catch (RuntimeException e) {
            oo3.b((Exception) e);
        }
    }
}
