package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jj1 extends jr1 {
    @DexIgnore
    public /* final */ boolean S;
    @DexIgnore
    public o80[] T;
    @DexIgnore
    public /* final */ n80[] U;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ jj1(ri1 ri1, en0 en0, n80[] n80Arr, short s, String str, int i) {
        super(ri1, en0, wm0.u, true, (i & 8) != 0 ? gq0.b.b(ri1.u, pb1.DEVICE_CONFIG) : s, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (i & 16) != 0 ? yh0.a("UUID.randomUUID().toString()") : str, false, 160);
        this.U = n80Arr;
        this.S = true;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public boolean b() {
        return this.S;
    }

    @DexIgnore
    @Override // com.fossil.zk0
    public Object d() {
        o80[] o80Arr = this.T;
        return o80Arr != null ? o80Arr : new o80[0];
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.v61, com.fossil.jr1
    public JSONObject i() {
        return yz0.a(super.i(), r51.Q, yz0.a(this.U));
    }

    @DexIgnore
    @Override // com.fossil.zk0, com.fossil.jr1
    public JSONObject k() {
        JSONObject k = super.k();
        r51 r51 = r51.R;
        o80[] o80Arr = this.T;
        return yz0.a(k, r51, o80Arr != null ? yz0.a(o80Arr) : null);
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public byte[] n() {
        wt0 wt0 = wt0.f;
        short s = ((v61) this).D;
        r60 r60 = ((zk0) this).x.a().i().get(Short.valueOf(pb1.DEVICE_CONFIG.a));
        if (r60 == null) {
            r60 = b21.x.d();
        }
        return wt0.a(s, r60, this.U);
    }

    @DexIgnore
    @Override // com.fossil.jr1
    public void q() {
        n80[] n80Arr = this.U;
        ArrayList arrayList = new ArrayList(n80Arr.length);
        for (n80 n80 : n80Arr) {
            arrayList.add(n80.getKey());
        }
        Object[] array = arrayList.toArray(new o80[0]);
        if (array != null) {
            o80[] o80Arr = (o80[]) array;
            this.T = o80Arr;
            le0 le0 = le0.DEBUG;
            if (o80Arr != null) {
                ee7.a((Object) Arrays.toString(o80Arr), "java.util.Arrays.toString(this)");
            }
            super.q();
            return;
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
