package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum iz2 {
    DOUBLE(pz2.DOUBLE, 1),
    FLOAT(pz2.FLOAT, 5),
    INT64(pz2.LONG, 0),
    UINT64(pz2.LONG, 0),
    INT32(pz2.INT, 0),
    FIXED64(pz2.LONG, 1),
    FIXED32(pz2.INT, 5),
    BOOL(pz2.BOOLEAN, 0),
    STRING(pz2.STRING, 2) {
    },
    GROUP(pz2.MESSAGE, 3) {
    },
    MESSAGE(pz2.MESSAGE, 2) {
    },
    BYTES(pz2.BYTE_STRING, 2) {
    },
    UINT32(pz2.INT, 0),
    ENUM(pz2.ENUM, 0),
    SFIXED32(pz2.INT, 5),
    SFIXED64(pz2.LONG, 1),
    SINT32(pz2.INT, 0),
    SINT64(pz2.LONG, 0);
    
    @DexIgnore
    public /* final */ pz2 zzs;
    @DexIgnore
    public /* final */ int zzt;

    @DexIgnore
    public iz2(pz2 pz2, int i) {
        this.zzs = pz2;
        this.zzt = i;
    }

    @DexIgnore
    public final pz2 zza() {
        return this.zzs;
    }

    @DexIgnore
    public final int zzb() {
        return this.zzt;
    }

    @DexIgnore
    public /* synthetic */ iz2(pz2 pz2, int i, jz2 jz2) {
        this(pz2, i);
    }
}
