package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi5 implements Factory<ui5> {
    @DexIgnore
    public /* final */ Provider<mk5> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;

    @DexIgnore
    public vi5(Provider<mk5> provider, Provider<ch5> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static vi5 a(Provider<mk5> provider, Provider<ch5> provider2) {
        return new vi5(provider, provider2);
    }

    @DexIgnore
    public static ui5 a() {
        return new ui5();
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ui5 get() {
        ui5 a2 = a();
        wi5.a(a2, this.a.get());
        wi5.a(a2, this.b.get());
        return a2;
    }
}
