package com.fossil;

import com.facebook.share.internal.VideoUploader;
import com.fossil.fitness.WorkoutState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yb5 {
    START(VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE),
    STOP("stop"),
    PAUSE("pause"),
    RESUME("resume"),
    IDLE("idle");
    
    @DexIgnore
    public static /* final */ a Companion; // = new a(null);
    @DexIgnore
    public String mValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final yb5 a(String str) {
            yb5 yb5;
            ee7.b(str, "value");
            yb5[] values = yb5.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    yb5 = null;
                    break;
                }
                yb5 = values[i];
                String mValue = yb5.getMValue();
                String lowerCase = str.toLowerCase();
                ee7.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                if (ee7.a((Object) mValue, (Object) lowerCase)) {
                    break;
                }
                i++;
            }
            return yb5 != null ? yb5 : yb5.IDLE;
        }

        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
            this();
        }

        @DexIgnore
        public final yb5 a(int i) {
            if (i == WorkoutState.START.ordinal()) {
                return yb5.START;
            }
            if (i == WorkoutState.END.ordinal()) {
                return yb5.STOP;
            }
            if (i == WorkoutState.PAUSE.ordinal()) {
                return yb5.PAUSE;
            }
            if (i == WorkoutState.RESUME.ordinal()) {
                return yb5.RESUME;
            }
            return yb5.IDLE;
        }
    }

    @DexIgnore
    public yb5(String str) {
        this.mValue = str;
    }

    @DexIgnore
    public final String getMValue() {
        return this.mValue;
    }

    @DexIgnore
    public final void setMValue(String str) {
        ee7.b(str, "<set-?>");
        this.mValue = str;
    }
}
