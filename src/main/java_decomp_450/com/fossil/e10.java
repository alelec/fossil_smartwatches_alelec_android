package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e10<T> implements uy<T> {
    @DexIgnore
    public /* final */ T a;

    @DexIgnore
    public e10(T t) {
        u50.a((Object) t);
        this.a = t;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public void b() {
    }

    @DexIgnore
    @Override // com.fossil.uy
    public final int c() {
        return 1;
    }

    @DexIgnore
    @Override // com.fossil.uy
    public Class<T> d() {
        return (Class<T>) this.a.getClass();
    }

    @DexIgnore
    @Override // com.fossil.uy
    public final T get() {
        return this.a;
    }
}
