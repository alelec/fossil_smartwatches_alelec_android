package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vh5 extends AppFilterProviderImpl {
    @DexIgnore
    public static /* final */ String a; // = "vh5";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements UpgradeCommand {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(vh5.a, " ---- UPGRADE DB APPFILTER, table APPFILTER");
            sQLiteDatabase.execSQL("ALTER TABLE appfilter ADD COLUMN deviceFamily int");
            FLogger.INSTANCE.getLocal().d(vh5.a, " ---- UPGRADE DB APPFILTER, table APPFILTER SUCCESS");
            StringBuilder sb = new StringBuilder();
            String c = PortfolioApp.c0.c();
            if (!TextUtils.isEmpty(c)) {
                sb.append("UPDATE ");
                sb.append("appfilter");
                sb.append(" SET deviceFamily = ");
                sb.append(DeviceIdentityUtils.getDeviceFamily(c).ordinal());
                sQLiteDatabase.execSQL(sb.toString());
                return;
            }
            vh5.this.removeAllAppFilters();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements UpgradeCommand {
        @DexIgnore
        public b(vh5 vh5) {
        }

        @DexIgnore
        @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
        public void execute(SQLiteDatabase sQLiteDatabase) {
            String str;
            String str2;
            String str3;
            String str4;
            int i;
            try {
                FLogger.INSTANCE.getLocal().d(vh5.a, "Inside .doInBackground upgrade appfilter");
                Cursor query = sQLiteDatabase.query(true, "appfilter", new String[]{"id", "type", BaseFeatureModel.COLUMN_COLOR, "name", BaseFeatureModel.COLUMN_HAPTIC, "timestamp", "enabled", "deviceFamily"}, null, null, null, null, null, null);
                ArrayList<AppFilter> arrayList = new ArrayList();
                int i2 = -1;
                String c = PortfolioApp.c0.c();
                if (!TextUtils.isEmpty(c)) {
                    i2 = be5.o.a(c).getValue();
                }
                String str5 = "deviceFamily";
                String str6 = "enabled";
                String str7 = "name";
                String str8 = "type";
                if (query == null || i2 <= 0 || !be5.o.g(c)) {
                    str = str5;
                    str2 = str6;
                    str3 = str7;
                    str4 = str8;
                } else {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        String string = query.getString(query.getColumnIndex(str8));
                        String string2 = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_COLOR));
                        String string3 = query.getString(query.getColumnIndex(str7));
                        String string4 = query.getString(query.getColumnIndex(BaseFeatureModel.COLUMN_HAPTIC));
                        int i3 = query.getInt(query.getColumnIndex("timestamp"));
                        int i4 = query.getInt(query.getColumnIndex(str6));
                        int i5 = query.getInt(query.getColumnIndex(str5));
                        int i6 = query.getInt(query.getColumnIndex("id"));
                        if (i5 == i2) {
                            i = i2;
                            AppFilter appFilter = new AppFilter();
                            appFilter.setType(string);
                            appFilter.setColor(string2);
                            appFilter.setName(string3);
                            appFilter.setHaptic(string4);
                            appFilter.setTimestamp((long) i3);
                            boolean z = true;
                            if (i4 != 1) {
                                z = false;
                            }
                            appFilter.setEnabled(z);
                            appFilter.setDeviceFamily(i5);
                            appFilter.setDbRowId(i6);
                            xh5 a = ah5.p.a().e().a(string, MFDeviceFamily.fromInt(appFilter.getDeviceFamily()).toString());
                            if (a != null) {
                                appFilter.setHour(a.d());
                                appFilter.setVibrationOnly(a.f());
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str9 = vh5.a;
                            local.d(str9, "Add appfiler=" + appFilter);
                            if (!appFilter.isVibrationOnly()) {
                                arrayList.add(appFilter);
                            }
                        } else {
                            i = i2;
                            FLogger.INSTANCE.getLocal().d(vh5.a, "Skip this app filter");
                        }
                        query.moveToNext();
                        str8 = str8;
                        str7 = str7;
                        str6 = str6;
                        str5 = str5;
                        i2 = i;
                    }
                    str = str5;
                    str2 = str6;
                    str3 = str7;
                    str4 = str8;
                    query.close();
                }
                sQLiteDatabase.execSQL("CREATE TABLE appfilter_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, type VARCHAR, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, isVibrationOnly INTEGER, deviceFamily INTEGER, hour INTEGER);");
                if (!arrayList.isEmpty()) {
                    for (AppFilter appFilter2 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(BaseFeatureModel.COLUMN_COLOR, appFilter2.getColor());
                        contentValues.put(BaseFeatureModel.COLUMN_HAPTIC, appFilter2.getHaptic());
                        contentValues.put("timestamp", Long.valueOf(appFilter2.getTimestamp()));
                        contentValues.put(str3, appFilter2.getName());
                        contentValues.put(str2, Boolean.valueOf(appFilter2.isEnabled()));
                        contentValues.put(str4, appFilter2.getType());
                        contentValues.put("id", Integer.valueOf(appFilter2.getDbRowId()));
                        contentValues.put(AppFilter.COLUMN_IS_VIBRATION_ONLY, Boolean.valueOf(appFilter2.isVibrationOnly()));
                        contentValues.put(str, Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                        contentValues.put(AppFilter.COLUMN_HOUR, Integer.valueOf(appFilter2.getHour()));
                        sQLiteDatabase.insert("appfilter_copy", null, contentValues);
                        str3 = str3;
                        str2 = str2;
                        str4 = str4;
                        str = str;
                    }
                }
                sQLiteDatabase.execSQL("DROP TABLE appfilter;");
                sQLiteDatabase.execSQL("ALTER TABLE appfilter_copy RENAME TO appfilter;");
                FLogger.INSTANCE.getLocal().d(vh5.a, "Migration complete");
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str10 = vh5.a;
                local2.e(str10, "Error inside " + vh5.a + ".upgrade - e=" + e);
            }
        }
    }

    @DexIgnore
    public vh5(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider, com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl
    public String getDbPath() {
        return ((BaseDbProvider) this).databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.appfilter.AppFilterProviderImpl, com.fossil.wearables.fsl.shared.BaseDbProvider
    @SuppressLint({"UseSparseArrays"})
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        Map<Integer, UpgradeCommand> dbUpgrades = super.getDbUpgrades();
        if (dbUpgrades == null) {
            dbUpgrades = new HashMap<>();
        }
        dbUpgrades.put(2, new a());
        dbUpgrades.put(4, new b(this));
        return dbUpgrades;
    }
}
