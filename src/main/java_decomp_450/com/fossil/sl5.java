package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sl5 implements Factory<rl5> {
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<ch5> b;
    @DexIgnore
    public /* final */ Provider<nw5> c;
    @DexIgnore
    public /* final */ Provider<PortfolioApp> d;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> e;
    @DexIgnore
    public /* final */ Provider<NotificationSettingsDatabase> f;
    @DexIgnore
    public /* final */ Provider<vu5> g;
    @DexIgnore
    public /* final */ Provider<nw6> h;
    @DexIgnore
    public /* final */ Provider<pm5> i;
    @DexIgnore
    public /* final */ Provider<qd5> j;
    @DexIgnore
    public /* final */ Provider<WatchFaceRepository> k;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> l;
    @DexIgnore
    public /* final */ Provider<WorkoutSettingRepository> m;
    @DexIgnore
    public /* final */ Provider<DianaPresetRepository> n;

    @DexIgnore
    public sl5(Provider<DianaPresetRepository> provider, Provider<ch5> provider2, Provider<nw5> provider3, Provider<PortfolioApp> provider4, Provider<DeviceRepository> provider5, Provider<NotificationSettingsDatabase> provider6, Provider<vu5> provider7, Provider<nw6> provider8, Provider<pm5> provider9, Provider<qd5> provider10, Provider<WatchFaceRepository> provider11, Provider<AlarmsRepository> provider12, Provider<WorkoutSettingRepository> provider13, Provider<DianaPresetRepository> provider14) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
        this.g = provider7;
        this.h = provider8;
        this.i = provider9;
        this.j = provider10;
        this.k = provider11;
        this.l = provider12;
        this.m = provider13;
        this.n = provider14;
    }

    @DexIgnore
    public static sl5 a(Provider<DianaPresetRepository> provider, Provider<ch5> provider2, Provider<nw5> provider3, Provider<PortfolioApp> provider4, Provider<DeviceRepository> provider5, Provider<NotificationSettingsDatabase> provider6, Provider<vu5> provider7, Provider<nw6> provider8, Provider<pm5> provider9, Provider<qd5> provider10, Provider<WatchFaceRepository> provider11, Provider<AlarmsRepository> provider12, Provider<WorkoutSettingRepository> provider13, Provider<DianaPresetRepository> provider14) {
        return new sl5(provider, provider2, provider3, provider4, provider5, provider6, provider7, provider8, provider9, provider10, provider11, provider12, provider13, provider14);
    }

    @DexIgnore
    public static rl5 a(DianaPresetRepository dianaPresetRepository, ch5 ch5, nw5 nw5, PortfolioApp portfolioApp, DeviceRepository deviceRepository, NotificationSettingsDatabase notificationSettingsDatabase, vu5 vu5, nw6 nw6, pm5 pm5, qd5 qd5, WatchFaceRepository watchFaceRepository, AlarmsRepository alarmsRepository, WorkoutSettingRepository workoutSettingRepository, DianaPresetRepository dianaPresetRepository2) {
        return new rl5(dianaPresetRepository, ch5, nw5, portfolioApp, deviceRepository, notificationSettingsDatabase, vu5, nw6, pm5, qd5, watchFaceRepository, alarmsRepository, workoutSettingRepository, dianaPresetRepository2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public rl5 get() {
        return a(this.a.get(), this.b.get(), this.c.get(), this.d.get(), this.e.get(), this.f.get(), this.g.get(), this.h.get(), this.i.get(), this.j.get(), this.k.get(), this.l.get(), this.m.get(), this.n.get());
    }
}
