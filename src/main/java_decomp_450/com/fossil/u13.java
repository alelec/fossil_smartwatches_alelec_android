package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u13 implements tr2<x13> {
    @DexIgnore
    public static u13 b; // = new u13();
    @DexIgnore
    public /* final */ tr2<x13> a;

    @DexIgnore
    public u13(tr2<x13> tr2) {
        this.a = sr2.a((tr2) tr2);
    }

    @DexIgnore
    public static boolean a() {
        return ((x13) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((x13) b.zza()).zzb();
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.fossil.tr2
    public final /* synthetic */ x13 zza() {
        return this.a.zza();
    }

    @DexIgnore
    public u13() {
        this(sr2.a(new w13()));
    }
}
