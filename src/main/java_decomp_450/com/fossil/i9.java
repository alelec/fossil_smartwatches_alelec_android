package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i9 {
    @DexIgnore
    public /* final */ Object a;

    @DexIgnore
    public i9(Object obj) {
        this.a = obj;
    }

    @DexIgnore
    public static i9 a(Object obj) {
        if (obj == null) {
            return null;
        }
        return new i9(obj);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || i9.class != obj.getClass()) {
            return false;
        }
        i9 i9Var = (i9) obj;
        Object obj2 = this.a;
        if (obj2 != null) {
            return obj2.equals(i9Var.a);
        }
        if (i9Var.a == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Object obj = this.a;
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "DisplayCutoutCompat{" + this.a + "}";
    }
}
