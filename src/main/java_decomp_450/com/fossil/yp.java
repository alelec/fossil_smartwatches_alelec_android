package com.fossil;

import android.net.Uri;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yp {
    @DexIgnore
    public Uri a;
    @DexIgnore
    public Uri b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Uri a;

        @DexIgnore
        public a(String str, String str2, Uri uri, String str3) {
            this.a = uri;
        }
    }

    @DexIgnore
    public yp(Uri uri, List<a> list, Uri uri2) {
        this.a = uri;
        if (list == null) {
            Collections.emptyList();
        }
        this.b = uri2;
    }
}
