package com.fossil;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hh4 {
    @DexIgnore
    public /* final */ fh4 a;
    @DexIgnore
    public /* final */ List<gh4> b;

    @DexIgnore
    public hh4(fh4 fh4) {
        this.a = fh4;
        ArrayList arrayList = new ArrayList();
        this.b = arrayList;
        arrayList.add(new gh4(fh4, new int[]{1}));
    }

    @DexIgnore
    public final gh4 a(int i) {
        if (i >= this.b.size()) {
            List<gh4> list = this.b;
            gh4 gh4 = list.get(list.size() - 1);
            for (int size = this.b.size(); size <= i; size++) {
                fh4 fh4 = this.a;
                gh4 = gh4.c(new gh4(fh4, new int[]{1, fh4.a((size - 1) + fh4.a())}));
                this.b.add(gh4);
            }
        }
        return this.b.get(i);
    }

    @DexIgnore
    public void a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                gh4 a2 = a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] a3 = new gh4(this.a, iArr2).a(i, 1).b(a2)[1].a();
                int length2 = i - a3.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(a3, 0, iArr, length + length2, a3.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
