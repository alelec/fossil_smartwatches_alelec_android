package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.i17;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j17 extends i17 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public j17(Context context) {
        this.a = context;
    }

    @DexIgnore
    @Override // com.fossil.i17
    public boolean a(g17 g17) {
        if (g17.e != 0) {
            return true;
        }
        return "android.resource".equals(g17.d.getScheme());
    }

    @DexIgnore
    @Override // com.fossil.i17
    public i17.a a(g17 g17, int i) throws IOException {
        Resources a2 = o17.a(this.a, g17);
        return new i17.a(a(a2, o17.a(a2, g17), g17), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public static Bitmap a(Resources resources, int i, g17 g17) {
        BitmapFactory.Options b = i17.b(g17);
        if (i17.a(b)) {
            BitmapFactory.decodeResource(resources, i, b);
            i17.a(g17.h, g17.i, b, g17);
        }
        return BitmapFactory.decodeResource(resources, i, b);
    }
}
