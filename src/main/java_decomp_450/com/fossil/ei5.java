package com.fossil;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.PhoneFavoritesContact;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ei5 extends BaseProvider {
    @DexIgnore
    void a(PhoneFavoritesContact phoneFavoritesContact);

    @DexIgnore
    void d();

    @DexIgnore
    void removePhoneFavoritesContact(String str);
}
