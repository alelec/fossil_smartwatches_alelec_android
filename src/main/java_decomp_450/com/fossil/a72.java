package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.facebook.appevents.codeless.CodelessMatcher;
import org.checkerframework.checker.nullness.qual.EnsuresNonNull;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a72 {
    @DexIgnore
    @EnsuresNonNull({"#1"})
    public static <T> T a(T t) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException("null reference");
    }

    @DexIgnore
    public static String b(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }

    @DexIgnore
    public static void c(String str) {
        if (ga2.a()) {
            throw new IllegalStateException(str);
        }
    }

    @DexIgnore
    public static String a(String str, Object obj) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }

    @DexIgnore
    public static void b(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    @DexIgnore
    @EnsuresNonNull({"#1"})
    public static <T> T a(T t, Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    @DexIgnore
    public static void b(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    @DexIgnore
    public static int a(int i) {
        if (i != 0) {
            return i;
        }
        throw new IllegalArgumentException("Given Integer is zero");
    }

    @DexIgnore
    public static void b(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalStateException(String.format(str, objArr));
        }
    }

    @DexIgnore
    public static void a(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    @DexIgnore
    public static void a(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalArgumentException(String.format(str, objArr));
        }
    }

    @DexIgnore
    public static void a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    @DexIgnore
    public static void a(String str) {
        if (!ga2.a()) {
            throw new IllegalStateException(str);
        }
    }

    @DexIgnore
    public static void a() {
        c("Must not be called on the main application thread");
    }

    @DexIgnore
    public static void a(Handler handler) {
        String name = Looper.myLooper() != null ? Looper.myLooper().getThread().getName() : "null current looper";
        String name2 = handler.getLooper().getThread().getName();
        StringBuilder sb = new StringBuilder(String.valueOf(name2).length() + 36 + String.valueOf(name).length());
        sb.append("Must be called on ");
        sb.append(name2);
        sb.append(" thread, but got ");
        sb.append(name);
        sb.append(CodelessMatcher.CURRENT_CLASS_NAME);
        a(handler, sb.toString());
    }

    @DexIgnore
    public static void a(Handler handler, String str) {
        if (Looper.myLooper() != handler.getLooper()) {
            throw new IllegalStateException(str);
        }
    }
}
