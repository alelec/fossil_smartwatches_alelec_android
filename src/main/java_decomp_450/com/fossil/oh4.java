package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh4 implements ph4 {
    @DexIgnore
    public int a() {
        return 4;
    }

    @DexIgnore
    @Override // com.fossil.ph4
    public void a(qh4 qh4) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!qh4.i()) {
                break;
            }
            a(qh4.c(), sb);
            qh4.f++;
            if (sb.length() >= 4) {
                qh4.a(a(sb, 0));
                sb.delete(0, 4);
                if (sh4.a(qh4.d(), qh4.f, a()) != a()) {
                    qh4.b(0);
                    break;
                }
            }
        }
        sb.append((char) 31);
        a(qh4, sb);
    }

    @DexIgnore
    public static void a(qh4 qh4, CharSequence charSequence) {
        try {
            int length = charSequence.length();
            if (length != 0) {
                boolean z = true;
                if (length == 1) {
                    qh4.l();
                    int a = qh4.g().a() - qh4.a();
                    if (qh4.f() == 0 && a <= 2) {
                        qh4.b(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a2 = a(charSequence, 0);
                    if (!(!qh4.i()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        qh4.c(qh4.a() + i);
                        if (qh4.g().a() - qh4.a() >= 3) {
                            qh4.c(qh4.a() + a2.length());
                            z = false;
                        }
                    }
                    if (z) {
                        qh4.k();
                        qh4.f -= i;
                    } else {
                        qh4.a(a2);
                    }
                    qh4.b(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            qh4.b(0);
        }
    }

    @DexIgnore
    public static void a(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            sh4.a(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    @DexIgnore
    public static String a(CharSequence charSequence, int i) {
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char c = 0;
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = (charAt << 18) + (charAt2 << '\f') + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
