package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl5 extends ll5 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nl5(String str, String str2, boolean z) {
        super(str, str2);
        ee7.b(str, "tagName");
        ee7.b(str2, "title");
        this.d = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
