package com.fossil;

import android.content.res.AssetFileDescriptor;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x2 extends Resources {
    @DexIgnore
    public /* final */ Resources a;

    @DexIgnore
    public x2(Resources resources) {
        super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        this.a = resources;
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getAnimation(int i) throws Resources.NotFoundException {
        return this.a.getAnimation(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public boolean getBoolean(int i) throws Resources.NotFoundException {
        return this.a.getBoolean(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getColor(int i) throws Resources.NotFoundException {
        return this.a.getColor(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public ColorStateList getColorStateList(int i) throws Resources.NotFoundException {
        return this.a.getColorStateList(i);
    }

    @DexIgnore
    public Configuration getConfiguration() {
        return this.a.getConfiguration();
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public float getDimension(int i) throws Resources.NotFoundException {
        return this.a.getDimension(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getDimensionPixelOffset(int i) throws Resources.NotFoundException {
        return this.a.getDimensionPixelOffset(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getDimensionPixelSize(int i) throws Resources.NotFoundException {
        return this.a.getDimensionPixelSize(i);
    }

    @DexIgnore
    public DisplayMetrics getDisplayMetrics() {
        return this.a.getDisplayMetrics();
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i) throws Resources.NotFoundException {
        return this.a.getDrawable(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawableForDensity(int i, int i2) throws Resources.NotFoundException {
        return this.a.getDrawableForDensity(i, i2);
    }

    @DexIgnore
    public float getFraction(int i, int i2, int i3) {
        return this.a.getFraction(i, i2, i3);
    }

    @DexIgnore
    public int getIdentifier(String str, String str2, String str3) {
        return this.a.getIdentifier(str, str2, str3);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int[] getIntArray(int i) throws Resources.NotFoundException {
        return this.a.getIntArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public int getInteger(int i) throws Resources.NotFoundException {
        return this.a.getInteger(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getLayout(int i) throws Resources.NotFoundException {
        return this.a.getLayout(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Movie getMovie(int i) throws Resources.NotFoundException {
        return this.a.getMovie(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getQuantityString(int i, int i2, Object... objArr) throws Resources.NotFoundException {
        return this.a.getQuantityString(i, i2, objArr);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence getQuantityText(int i, int i2) throws Resources.NotFoundException {
        return this.a.getQuantityText(i, i2);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceEntryName(int i) throws Resources.NotFoundException {
        return this.a.getResourceEntryName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceName(int i) throws Resources.NotFoundException {
        return this.a.getResourceName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourcePackageName(int i) throws Resources.NotFoundException {
        return this.a.getResourcePackageName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getResourceTypeName(int i) throws Resources.NotFoundException {
        return this.a.getResourceTypeName(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getString(int i) throws Resources.NotFoundException {
        return this.a.getString(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String[] getStringArray(int i) throws Resources.NotFoundException {
        return this.a.getStringArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence getText(int i) throws Resources.NotFoundException {
        return this.a.getText(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public CharSequence[] getTextArray(int i) throws Resources.NotFoundException {
        return this.a.getTextArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValue(int i, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.a.getValue(i, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValueForDensity(int i, int i2, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.a.getValueForDensity(i, i2, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public XmlResourceParser getXml(int i) throws Resources.NotFoundException {
        return this.a.getXml(i);
    }

    @DexIgnore
    public TypedArray obtainAttributes(AttributeSet attributeSet, int[] iArr) {
        return this.a.obtainAttributes(attributeSet, iArr);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public TypedArray obtainTypedArray(int i) throws Resources.NotFoundException {
        return this.a.obtainTypedArray(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public InputStream openRawResource(int i) throws Resources.NotFoundException {
        return this.a.openRawResource(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public AssetFileDescriptor openRawResourceFd(int i) throws Resources.NotFoundException {
        return this.a.openRawResourceFd(i);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void parseBundleExtra(String str, AttributeSet attributeSet, Bundle bundle) throws XmlPullParserException {
        this.a.parseBundleExtra(str, attributeSet, bundle);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void parseBundleExtras(XmlResourceParser xmlResourceParser, Bundle bundle) throws XmlPullParserException, IOException {
        this.a.parseBundleExtras(xmlResourceParser, bundle);
    }

    @DexIgnore
    public void updateConfiguration(Configuration configuration, DisplayMetrics displayMetrics) {
        super.updateConfiguration(configuration, displayMetrics);
        Resources resources = this.a;
        if (resources != null) {
            resources.updateConfiguration(configuration, displayMetrics);
        }
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public Drawable getDrawable(int i, Resources.Theme theme) throws Resources.NotFoundException {
        return this.a.getDrawable(i, theme);
    }

    @DexIgnore
    public Drawable getDrawableForDensity(int i, int i2, Resources.Theme theme) {
        return this.a.getDrawableForDensity(i, i2, theme);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getQuantityString(int i, int i2) throws Resources.NotFoundException {
        return this.a.getQuantityString(i, i2);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public String getString(int i, Object... objArr) throws Resources.NotFoundException {
        return this.a.getString(i, objArr);
    }

    @DexIgnore
    public CharSequence getText(int i, CharSequence charSequence) {
        return this.a.getText(i, charSequence);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public void getValue(String str, TypedValue typedValue, boolean z) throws Resources.NotFoundException {
        this.a.getValue(str, typedValue, z);
    }

    @DexIgnore
    @Override // android.content.res.Resources
    public InputStream openRawResource(int i, TypedValue typedValue) throws Resources.NotFoundException {
        return this.a.openRawResource(i, typedValue);
    }
}
