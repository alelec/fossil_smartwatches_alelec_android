package com.fossil;

import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h44 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ThreadFactory {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ AtomicLong b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.h44$a$a")
        /* renamed from: com.fossil.h44$a$a  reason: collision with other inner class name */
        public class C0073a extends p34 {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable a;

            @DexIgnore
            public C0073a(a aVar, Runnable runnable) {
                this.a = runnable;
            }

            @DexIgnore
            @Override // com.fossil.p34
            public void a() {
                this.a.run();
            }
        }

        @DexIgnore
        public a(String str, AtomicLong atomicLong) {
            this.a = str;
            this.b = atomicLong;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread newThread = Executors.defaultThreadFactory().newThread(new C0073a(this, runnable));
            newThread.setName(this.a + this.b.getAndIncrement());
            return newThread;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends p34 {
        @DexIgnore
        public /* final */ /* synthetic */ String a;
        @DexIgnore
        public /* final */ /* synthetic */ ExecutorService b;
        @DexIgnore
        public /* final */ /* synthetic */ long c;
        @DexIgnore
        public /* final */ /* synthetic */ TimeUnit d;

        @DexIgnore
        public b(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
            this.a = str;
            this.b = executorService;
            this.c = j;
            this.d = timeUnit;
        }

        @DexIgnore
        @Override // com.fossil.p34
        public void a() {
            try {
                z24 a2 = z24.a();
                a2.a("Executing shutdown hook for " + this.a);
                this.b.shutdown();
                if (!this.b.awaitTermination(this.c, this.d)) {
                    z24 a3 = z24.a();
                    a3.a(this.a + " did not shut down in the allocated time. Requesting immediate shutdown.");
                    this.b.shutdownNow();
                }
            } catch (InterruptedException unused) {
                z24.a().a(String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.a));
                this.b.shutdownNow();
            }
        }
    }

    @DexIgnore
    public static ExecutorService a(String str) {
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor(b(str));
        a(str, newSingleThreadExecutor);
        return newSingleThreadExecutor;
    }

    @DexIgnore
    public static final ThreadFactory b(String str) {
        return new a(str, new AtomicLong(1));
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService) {
        a(str, executorService, 2, TimeUnit.SECONDS);
    }

    @DexIgnore
    public static final void a(String str, ExecutorService executorService, long j, TimeUnit timeUnit) {
        Runtime runtime = Runtime.getRuntime();
        b bVar = new b(str, executorService, j, timeUnit);
        runtime.addShutdownHook(new Thread(bVar, "Crashlytics Shutdown Hook for " + str));
    }
}
