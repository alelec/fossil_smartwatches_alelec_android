package com.fossil;

import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class sb4 implements Callable {
    @DexIgnore
    public /* final */ vb4 a;

    @DexIgnore
    public sb4(vb4 vb4) {
        this.a = vb4;
    }

    @DexIgnore
    public static Callable a(vb4 vb4) {
        return new sb4(vb4);
    }

    @DexIgnore
    @Override // java.util.concurrent.Callable
    public Object call() {
        return this.a.b();
    }
}
