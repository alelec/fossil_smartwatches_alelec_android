package com.fossil;

import android.app.Notification;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class em {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Notification c;

    @DexIgnore
    public em(int i, Notification notification, int i2) {
        this.a = i;
        this.c = notification;
        this.b = i2;
    }

    @DexIgnore
    public int a() {
        return this.b;
    }

    @DexIgnore
    public Notification b() {
        return this.c;
    }

    @DexIgnore
    public int c() {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || em.class != obj.getClass()) {
            return false;
        }
        em emVar = (em) obj;
        if (this.a == emVar.a && this.b == emVar.b) {
            return this.c.equals(emVar.c);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.a * 31) + this.b) * 31) + this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "ForegroundInfo{" + "mNotificationId=" + this.a + ", mForegroundServiceType=" + this.b + ", mNotification=" + this.c + '}';
    }
}
