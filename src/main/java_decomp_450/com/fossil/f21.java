package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f21 implements Parcelable.Creator<ri1> {
    @DexIgnore
    public /* synthetic */ f21(zd7 zd7) {
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // android.os.Parcelable.Creator
    public ri1 createFromParcel(Parcel parcel) {
        w51 w51 = w51.b;
        Parcelable readParcelable = parcel.readParcelable(BluetoothDevice.class.getClassLoader());
        if (readParcelable != null) {
            return w51.a((BluetoothDevice) readParcelable);
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object[]' to match base method */
    @Override // android.os.Parcelable.Creator
    public ri1[] newArray(int i) {
        return new ri1[i];
    }
}
