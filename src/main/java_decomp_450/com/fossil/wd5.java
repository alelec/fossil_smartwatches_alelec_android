package com.fossil;

import android.os.Build;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wd5 {
    @DexIgnore
    public static /* final */ ArrayList<String> a; // = w97.a((Object[]) new String[]{"commute-time", "second-timezone"});
    @DexIgnore
    public static /* final */ wd5 b; // = new wd5();

    /*
    static {
        w97.a((Object[]) new String[]{"commute-time", "weather", "chance-of-rain"});
    }
    */

    @DexIgnore
    public final String a(String str) {
        ee7.b(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode != -829740640) {
            if (hashCode == 134170930 && str.equals("second-timezone")) {
                String a2 = ig5.a(PortfolioApp.g0.c(), 2131886527);
                ee7.a((Object) a2, "LanguageHelper.getString\u2026tCity_Title__ChooseACity)");
                return a2;
            }
        } else if (str.equals("commute-time")) {
            String a3 = ig5.a(PortfolioApp.g0.c(), 2131886363);
            ee7.a((Object) a3, "LanguageHelper.getString\u2026Time_CTA__SetDestination)");
            return a3;
        }
        return "";
    }

    @DexIgnore
    public final List<String> b(String str) {
        ee7.b(str, "complicationId");
        int hashCode = str.hashCode();
        if (hashCode == -829740640 ? !str.equals("commute-time") : hashCode == -48173007 ? !str.equals("chance-of-rain") : hashCode != 1223440372 || !str.equals("weather")) {
            return new ArrayList();
        }
        int i = Build.VERSION.SDK_INT;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "android.os.Build.VERSION.SDK_INT=" + i);
        if (i >= 29) {
            return w97.a((Object[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE, InAppPermission.ACCESS_BACKGROUND_LOCATION});
        }
        return w97.a((Object[]) new String[]{InAppPermission.ACCESS_FINE_LOCATION, InAppPermission.LOCATION_SERVICE});
    }

    @DexIgnore
    public final boolean c(String str) {
        ee7.b(str, "complicationId");
        return a.contains(str);
    }

    @DexIgnore
    public final boolean d(String str) {
        ee7.b(str, "complicationId");
        List<String> b2 = b(str);
        String[] a2 = px6.a.a();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ComplicationHelper", "isPermissionGrantedForComplication " + str + " granted=" + a2 + " required=" + b2);
        Iterator<T> it = b2.iterator();
        while (it.hasNext()) {
            if (!t97.a((Object[]) a2, (Object) it.next())) {
                return false;
            }
        }
        return true;
    }
}
