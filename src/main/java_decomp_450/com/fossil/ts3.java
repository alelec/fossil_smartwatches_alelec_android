package com.fossil;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.us3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ts3 {
    @DexIgnore
    public static /* final */ int j;
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ View b;
    @DexIgnore
    public /* final */ Path c; // = new Path();
    @DexIgnore
    public /* final */ Paint d; // = new Paint(7);
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public us3.e f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Canvas canvas);

        @DexIgnore
        boolean c();
    }

    /*
    static {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 21) {
            j = 2;
        } else if (i2 >= 18) {
            j = 1;
        } else {
            j = 0;
        }
    }
    */

    @DexIgnore
    public ts3(a aVar) {
        this.a = aVar;
        View view = (View) aVar;
        this.b = view;
        view.setWillNotDraw(false);
        Paint paint = new Paint(1);
        this.e = paint;
        paint.setColor(0);
    }

    @DexIgnore
    public void a() {
        if (j == 0) {
            this.h = true;
            this.i = false;
            this.b.buildDrawingCache();
            Bitmap drawingCache = this.b.getDrawingCache();
            if (!(drawingCache != null || this.b.getWidth() == 0 || this.b.getHeight() == 0)) {
                drawingCache = Bitmap.createBitmap(this.b.getWidth(), this.b.getHeight(), Bitmap.Config.ARGB_8888);
                this.b.draw(new Canvas(drawingCache));
            }
            if (drawingCache != null) {
                Paint paint = this.d;
                Shader.TileMode tileMode = Shader.TileMode.CLAMP;
                paint.setShader(new BitmapShader(drawingCache, tileMode, tileMode));
            }
            this.h = false;
            this.i = true;
        }
    }

    @DexIgnore
    public void b() {
        if (j == 0) {
            this.i = false;
            this.b.destroyDrawingCache();
            this.d.setShader(null);
            this.b.invalidate();
        }
    }

    @DexIgnore
    public Drawable c() {
        return this.g;
    }

    @DexIgnore
    public int d() {
        return this.e.getColor();
    }

    @DexIgnore
    public us3.e e() {
        us3.e eVar = this.f;
        if (eVar == null) {
            return null;
        }
        us3.e eVar2 = new us3.e(eVar);
        if (eVar2.a()) {
            eVar2.c = a(eVar2);
        }
        return eVar2;
    }

    @DexIgnore
    public final void f() {
        if (j == 1) {
            this.c.rewind();
            us3.e eVar = this.f;
            if (eVar != null) {
                this.c.addCircle(eVar.a, eVar.b, eVar.c, Path.Direction.CW);
            }
        }
        this.b.invalidate();
    }

    @DexIgnore
    public boolean g() {
        return this.a.c() && !h();
    }

    @DexIgnore
    public final boolean h() {
        us3.e eVar = this.f;
        boolean z = eVar == null || eVar.a();
        if (j != 0) {
            return !z;
        }
        if (z || !this.i) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean i() {
        return (this.h || this.g == null || this.f == null) ? false : true;
    }

    @DexIgnore
    public final boolean j() {
        return !this.h && Color.alpha(this.e.getColor()) != 0;
    }

    @DexIgnore
    public void b(us3.e eVar) {
        if (eVar == null) {
            this.f = null;
        } else {
            us3.e eVar2 = this.f;
            if (eVar2 == null) {
                this.f = new us3.e(eVar);
            } else {
                eVar2.a(eVar);
            }
            if (mu3.a(eVar.c, a(eVar), 1.0E-4f)) {
                this.f.c = Float.MAX_VALUE;
            }
        }
        f();
    }

    @DexIgnore
    public void a(int i2) {
        this.e.setColor(i2);
        this.b.invalidate();
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.g = drawable;
        this.b.invalidate();
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        if (i()) {
            Rect bounds = this.g.getBounds();
            float width = this.f.a - (((float) bounds.width()) / 2.0f);
            float height = this.f.b - (((float) bounds.height()) / 2.0f);
            canvas.translate(width, height);
            this.g.draw(canvas);
            canvas.translate(-width, -height);
        }
    }

    @DexIgnore
    public final float a(us3.e eVar) {
        return mu3.a(eVar.a, eVar.b, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight());
    }

    @DexIgnore
    public void a(Canvas canvas) {
        if (h()) {
            int i2 = j;
            if (i2 == 0) {
                us3.e eVar = this.f;
                canvas.drawCircle(eVar.a, eVar.b, eVar.c, this.d);
                if (j()) {
                    us3.e eVar2 = this.f;
                    canvas.drawCircle(eVar2.a, eVar2.b, eVar2.c, this.e);
                }
            } else if (i2 == 1) {
                int save = canvas.save();
                canvas.clipPath(this.c);
                this.a.a(canvas);
                if (j()) {
                    canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
                }
                canvas.restoreToCount(save);
            } else if (i2 == 2) {
                this.a.a(canvas);
                if (j()) {
                    canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
                }
            } else {
                throw new IllegalStateException("Unsupported strategy " + j);
            }
        } else {
            this.a.a(canvas);
            if (j()) {
                canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) this.b.getWidth(), (float) this.b.getHeight(), this.e);
            }
        }
        b(canvas);
    }
}
