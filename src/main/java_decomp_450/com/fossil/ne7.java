package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ne7 extends vd7 implements zf7 {
    @DexIgnore
    public ne7() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof ne7) {
            ne7 ne7 = (ne7) obj;
            if (!getOwner().equals(ne7.getOwner()) || !getName().equals(ne7.getName()) || !getSignature().equals(ne7.getSignature()) || !ee7.a(getBoundReceiver(), ne7.getBoundReceiver())) {
                return false;
            }
            return true;
        } else if (obj instanceof zf7) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    @Override // com.fossil.zf7
    public boolean isConst() {
        return getReflected().isConst();
    }

    @DexIgnore
    @Override // com.fossil.zf7
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    @DexIgnore
    public String toString() {
        sf7 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }

    @DexIgnore
    public ne7(Object obj) {
        super(obj);
    }

    @DexIgnore
    @Override // com.fossil.vd7
    public zf7 getReflected() {
        return (zf7) super.getReflected();
    }
}
