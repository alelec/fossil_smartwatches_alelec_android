package com.fossil;

import android.graphics.Bitmap;
import android.net.Uri;
import com.facebook.places.internal.LocationScannerImpl;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g17 {
    @DexIgnore
    public static /* final */ long s; // = TimeUnit.SECONDS.toNanos(5);
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ Uri d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ List<Transformation> g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public /* final */ float m;
    @DexIgnore
    public /* final */ float n;
    @DexIgnore
    public /* final */ float o;
    @DexIgnore
    public /* final */ boolean p;
    @DexIgnore
    public /* final */ Bitmap.Config q;
    @DexIgnore
    public /* final */ Picasso.e r;

    @DexIgnore
    public String a() {
        Uri uri = this.d;
        if (uri != null) {
            return String.valueOf(uri.getPath());
        }
        return Integer.toHexString(this.e);
    }

    @DexIgnore
    public boolean b() {
        return this.g != null;
    }

    @DexIgnore
    public boolean c() {
        return (this.h == 0 && this.i == 0) ? false : true;
    }

    @DexIgnore
    public String d() {
        long nanoTime = System.nanoTime() - this.b;
        if (nanoTime > s) {
            return g() + '+' + TimeUnit.NANOSECONDS.toSeconds(nanoTime) + 's';
        }
        return g() + '+' + TimeUnit.NANOSECONDS.toMillis(nanoTime) + "ms";
    }

    @DexIgnore
    public boolean e() {
        return c() || this.m != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public boolean f() {
        return e() || b();
    }

    @DexIgnore
    public String g() {
        return "[R" + this.a + ']';
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder("Request{");
        int i2 = this.e;
        if (i2 > 0) {
            sb.append(i2);
        } else {
            sb.append(this.d);
        }
        List<Transformation> list = this.g;
        if (list != null && !list.isEmpty()) {
            for (Transformation transformation : this.g) {
                sb.append(' ');
                sb.append(transformation.key());
            }
        }
        if (this.f != null) {
            sb.append(" stableKey(");
            sb.append(this.f);
            sb.append(')');
        }
        if (this.h > 0) {
            sb.append(" resize(");
            sb.append(this.h);
            sb.append(',');
            sb.append(this.i);
            sb.append(')');
        }
        if (this.j) {
            sb.append(" centerCrop");
        }
        if (this.k) {
            sb.append(" centerInside");
        }
        if (this.m != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            sb.append(" rotation(");
            sb.append(this.m);
            if (this.p) {
                sb.append(" @ ");
                sb.append(this.n);
                sb.append(',');
                sb.append(this.o);
            }
            sb.append(')');
        }
        if (this.q != null) {
            sb.append(' ');
            sb.append(this.q);
        }
        sb.append('}');
        return sb.toString();
    }

    @DexIgnore
    public g17(Uri uri, int i2, String str, List<Transformation> list, int i3, int i4, boolean z, boolean z2, boolean z3, float f2, float f3, float f4, boolean z4, Bitmap.Config config, Picasso.e eVar) {
        this.d = uri;
        this.e = i2;
        this.f = str;
        if (list == null) {
            this.g = null;
        } else {
            this.g = Collections.unmodifiableList(list);
        }
        this.h = i3;
        this.i = i4;
        this.j = z;
        this.k = z2;
        this.l = z3;
        this.m = f2;
        this.n = f3;
        this.o = f4;
        this.p = z4;
        this.q = config;
        this.r = eVar;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public Uri a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public float i;
        @DexIgnore
        public float j;
        @DexIgnore
        public float k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public List<Transformation> m;
        @DexIgnore
        public Bitmap.Config n;
        @DexIgnore
        public Picasso.e o;

        @DexIgnore
        public b(Uri uri, int i2, Bitmap.Config config) {
            this.a = uri;
            this.b = i2;
            this.n = config;
        }

        @DexIgnore
        public b a(int i2, int i3) {
            if (i2 < 0) {
                throw new IllegalArgumentException("Width must be positive number or 0.");
            } else if (i3 < 0) {
                throw new IllegalArgumentException("Height must be positive number or 0.");
            } else if (i3 == 0 && i2 == 0) {
                throw new IllegalArgumentException("At least one dimension has to be positive number.");
            } else {
                this.d = i2;
                this.e = i3;
                return this;
            }
        }

        @DexIgnore
        public boolean b() {
            return (this.a == null && this.b == 0) ? false : true;
        }

        @DexIgnore
        public boolean c() {
            return (this.d == 0 && this.e == 0) ? false : true;
        }

        @DexIgnore
        public b a(Transformation transformation) {
            if (transformation == null) {
                throw new IllegalArgumentException("Transformation must not be null.");
            } else if (transformation.key() != null) {
                if (this.m == null) {
                    this.m = new ArrayList(2);
                }
                this.m.add(transformation);
                return this;
            } else {
                throw new IllegalArgumentException("Transformation key must not be null.");
            }
        }

        @DexIgnore
        public g17 a() {
            if (this.g && this.f) {
                throw new IllegalStateException("Center crop and center inside can not be used together.");
            } else if (this.f && this.d == 0 && this.e == 0) {
                throw new IllegalStateException("Center crop requires calling resize with positive width and height.");
            } else if (this.g && this.d == 0 && this.e == 0) {
                throw new IllegalStateException("Center inside requires calling resize with positive width and height.");
            } else {
                if (this.o == null) {
                    this.o = Picasso.e.NORMAL;
                }
                return new g17(this.a, this.b, this.c, this.m, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.n, this.o);
            }
        }
    }
}
