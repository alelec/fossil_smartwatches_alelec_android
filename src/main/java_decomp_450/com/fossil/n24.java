package com.fossil;

import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n24 extends o24 {
    @DexIgnore
    public /* final */ List<c24<?>> componentsInCycle;

    @DexIgnore
    public n24(List<c24<?>> list) {
        super("Dependency cycle detected: " + Arrays.toString(list.toArray()));
        this.componentsInCycle = list;
    }

    @DexIgnore
    public List<c24<?>> getComponentsInCycle() {
        return this.componentsInCycle;
    }
}
