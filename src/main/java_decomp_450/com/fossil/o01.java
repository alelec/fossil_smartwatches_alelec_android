package com.fossil;

import android.bluetooth.BluetoothGatt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class o01 extends eo0 {
    @DexIgnore
    public /* final */ nm1 j; // = nm1.VERY_HIGH;
    @DexIgnore
    public dd1 k; // = dd1.DISCONNECTED;

    @DexIgnore
    public o01(cx0 cx0) {
        super(aq0.c, cx0);
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(s91 s91) {
        lk0 lk0;
        c(s91);
        x71 x71 = s91.a;
        if (x71.a != z51.SUCCESS) {
            lk0 a = lk0.d.a(x71);
            lk0 = lk0.a(((eo0) this).d, null, a.b, a.c, 1);
        } else if (this.k == dd1.DISCONNECTED) {
            lk0 = lk0.a(((eo0) this).d, null, oi0.a, null, 5);
        } else {
            lk0 = lk0.a(((eo0) this).d, null, oi0.d, null, 5);
        }
        ((eo0) this).d = lk0;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public nm1 b() {
        return this.j;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public qy0<s91> c() {
        return ((eo0) this).i.f;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public boolean b(s91 s91) {
        return (s91 instanceof nv0) && ((nv0) s91).b == dd1.DISCONNECTED;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void c(s91 s91) {
        this.k = ((nv0) s91).b;
    }

    @DexIgnore
    @Override // com.fossil.eo0
    public void a(ri1 ri1) {
        int i = xe1.b[ri1.t.ordinal()];
        if (i == 1) {
            ri1.a.post(new wy0(ri1, new x71(z51.SUCCESS, 0, 2), dd1.DISCONNECTED));
        } else if (i == 2) {
            ri1.a(3, 0);
            BluetoothGatt bluetoothGatt = ri1.b;
            if (bluetoothGatt != null) {
                le0 le0 = le0.DEBUG;
                bluetoothGatt.disconnect();
            }
            ri1.a(0, 0);
        } else if (i == 3) {
            ri1.a(3, 0);
            BluetoothGatt bluetoothGatt2 = ri1.b;
            if (bluetoothGatt2 != null) {
                le0 le02 = le0.DEBUG;
                bluetoothGatt2.disconnect();
            } else {
                le0 le03 = le0.DEBUG;
            }
            ri1.a(0, 0);
        }
    }
}
