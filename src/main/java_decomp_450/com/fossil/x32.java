package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x32 extends BroadcastReceiver {
    @DexIgnore
    public Context a;
    @DexIgnore
    public /* final */ z32 b;

    @DexIgnore
    public x32(z32 z32) {
        this.b = z32;
    }

    @DexIgnore
    public final void a(Context context) {
        this.a = context;
    }

    @DexIgnore
    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.b.a();
            a();
        }
    }

    @DexIgnore
    public final synchronized void a() {
        if (this.a != null) {
            this.a.unregisterReceiver(this);
        }
        this.a = null;
    }
}
