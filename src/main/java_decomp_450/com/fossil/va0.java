package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class va0 extends k60 implements Parcelable, Serializable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ i90 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<va0> {
        @DexIgnore
        public /* synthetic */ a(zd7 zd7) {
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        @Override // android.os.Parcelable.Creator
        public va0 createFromParcel(Parcel parcel) {
            float readFloat = parcel.readFloat();
            float readFloat2 = parcel.readFloat();
            float readFloat3 = parcel.readFloat();
            int readInt = parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                ee7.a((Object) readString, "parcel.readString()!!");
                return new va0(readFloat, readFloat2, readFloat3, readInt, i90.valueOf(readString));
            }
            ee7.a();
            throw null;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object[]' to match base method */
        @Override // android.os.Parcelable.Creator
        public va0[] newArray(int i) {
            return new va0[i];
        }
    }

    @DexIgnore
    public va0(float f, float f2, float f3, int i, i90 i90) {
        this.a = yz0.a(f, 2);
        this.b = yz0.a(f2, 2);
        this.c = yz0.a(f3, 2);
        this.d = i;
        this.e = i90;
    }

    @DexIgnore
    @Override // com.fossil.k60
    public JSONObject a() {
        return yz0.a(yz0.a(yz0.a(yz0.a(yz0.a(new JSONObject(), r51.W1, Float.valueOf(this.a)), r51.X1, Float.valueOf(this.b)), r51.Y1, Float.valueOf(this.c)), r51.q, Integer.valueOf(this.d)), r51.s, yz0.a(this.e));
    }

    @DexIgnore
    public final JSONObject b() {
        JSONObject put = new JSONObject().put("temp", Float.valueOf(this.a)).put("high", Float.valueOf(this.b)).put("low", Float.valueOf(this.c)).put("rain", this.d).put("cond_id", this.e.a());
        ee7.a((Object) put, "JSONObject()\n           \u2026rrentWeatherCondition.id)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!ee7.a(va0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            va0 va0 = (va0) obj;
            return this.a == va0.a && this.b == va0.b && this.c == va0.c && this.d == va0.d && this.e == va0.e;
        }
        throw new x87("null cannot be cast to non-null type com.fossil.blesdk.device.data.weather.CurrentWeatherInfo");
    }

    @DexIgnore
    public final int getChanceOfRain() {
        return this.d;
    }

    @DexIgnore
    public final float getCurrentTemperature() {
        return this.a;
    }

    @DexIgnore
    public final i90 getCurrentWeatherCondition() {
        return this.e;
    }

    @DexIgnore
    public final float getHighTemperature() {
        return this.b;
    }

    @DexIgnore
    public final float getLowTemperature() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = Float.valueOf(this.b).hashCode();
        int hashCode2 = Float.valueOf(this.c).hashCode();
        return this.e.hashCode() + ((((hashCode2 + ((hashCode + (Float.valueOf(this.a).hashCode() * 31)) * 31)) * 31) + this.d) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeFloat(this.a);
        }
        if (parcel != null) {
            parcel.writeFloat(this.b);
        }
        if (parcel != null) {
            parcel.writeFloat(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeString(this.e.name());
        }
    }
}
