package com.fossil;

import com.fossil.bw2;
import com.fossil.lp2;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mp2 extends bw2<mp2, a> implements lx2 {
    @DexIgnore
    public static /* final */ mp2 zzl;
    @DexIgnore
    public static volatile wx2<mp2> zzm;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public long zzd;
    @DexIgnore
    public String zze; // = "";
    @DexIgnore
    public int zzf;
    @DexIgnore
    public jw2<np2> zzg; // = bw2.o();
    @DexIgnore
    public jw2<lp2> zzh; // = bw2.o();
    @DexIgnore
    public jw2<zo2> zzi; // = bw2.o();
    @DexIgnore
    public String zzj; // = "";
    @DexIgnore
    public boolean zzk;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends bw2.a<mp2, a> implements lx2 {
        @DexIgnore
        public a() {
            super(mp2.zzl);
        }

        @DexIgnore
        public final lp2 a(int i) {
            return ((mp2) ((bw2.a) this).b).b(i);
        }

        @DexIgnore
        public final List<zo2> o() {
            return Collections.unmodifiableList(((mp2) ((bw2.a) this).b).u());
        }

        @DexIgnore
        public final a p() {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((mp2) ((bw2.a) this).b).w();
            return this;
        }

        @DexIgnore
        public final int zza() {
            return ((mp2) ((bw2.a) this).b).t();
        }

        @DexIgnore
        public /* synthetic */ a(jp2 jp2) {
            this();
        }

        @DexIgnore
        public final a a(int i, lp2.a aVar) {
            if (((bw2.a) this).c) {
                l();
                ((bw2.a) this).c = false;
            }
            ((mp2) ((bw2.a) this).b).a(i, (lp2) ((bw2) aVar.g()));
            return this;
        }
    }

    /*
    static {
        mp2 mp2 = new mp2();
        zzl = mp2;
        bw2.a(mp2.class, mp2);
    }
    */

    @DexIgnore
    public static a x() {
        return (a) zzl.e();
    }

    @DexIgnore
    public static mp2 y() {
        return zzl;
    }

    @DexIgnore
    public final void a(int i, lp2 lp2) {
        lp2.getClass();
        jw2<lp2> jw2 = this.zzh;
        if (!jw2.zza()) {
            this.zzh = bw2.a(jw2);
        }
        this.zzh.set(i, lp2);
    }

    @DexIgnore
    public final lp2 b(int i) {
        return this.zzh.get(i);
    }

    @DexIgnore
    public final long p() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean q() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final String r() {
        return this.zze;
    }

    @DexIgnore
    public final List<np2> s() {
        return this.zzg;
    }

    @DexIgnore
    public final int t() {
        return this.zzh.size();
    }

    @DexIgnore
    public final List<zo2> u() {
        return this.zzi;
    }

    @DexIgnore
    public final boolean v() {
        return this.zzk;
    }

    @DexIgnore
    public final void w() {
        this.zzi = bw2.o();
    }

    @DexIgnore
    public final boolean zza() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    @Override // com.fossil.bw2
    public final Object a(int i, Object obj, Object obj2) {
        switch (jp2.a[i - 1]) {
            case 1:
                return new mp2();
            case 2:
                return new a(null);
            case 3:
                return bw2.a(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0003\u0000\u0001\u1002\u0000\u0002\u1008\u0001\u0003\u1004\u0002\u0004\u001b\u0005\u001b\u0006\u001b\u0007\u1008\u0003\b\u1007\u0004", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", np2.class, "zzh", lp2.class, "zzi", zo2.class, "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                wx2<mp2> wx2 = zzm;
                if (wx2 == null) {
                    synchronized (mp2.class) {
                        wx2 = zzm;
                        if (wx2 == null) {
                            wx2 = new bw2.c<>(zzl);
                            zzm = wx2;
                        }
                    }
                }
                return wx2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
