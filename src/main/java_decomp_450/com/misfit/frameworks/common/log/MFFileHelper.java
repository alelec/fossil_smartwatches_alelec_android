package com.misfit.frameworks.common.log;

import android.content.Context;
import android.util.Log;
import com.fossil.rw;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFFileHelper extends Thread {
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "log_%s.txt";
    @DexIgnore
    public static /* final */ int FILE_SIZE; // = 3145728;
    @DexIgnore
    public static /* final */ String LOG_PATTERN; // = "%s %s: %s\n";
    @DexIgnore
    public static /* final */ int MAX_FILE_NUMBER; // = 3;
    @DexIgnore
    public static /* final */ String TAG; // = MFFileHelper.class.getSimpleName();
    @DexIgnore
    public static MFFileHelper sInstance;
    @DexIgnore
    public String directoryPath;
    @DexIgnore
    public boolean isInitialized;
    @DexIgnore
    public boolean isRunning;
    @DexIgnore
    public /* final */ Object locker; // = new Object();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<String> pendingQueue; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public SimpleDateFormat sdf; // = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss.SSS", Locale.US);

    @DexIgnore
    private void flush() {
        if (!this.isRunning) {
            synchronized (this.locker) {
                this.locker.notify();
            }
        }
    }

    @DexIgnore
    private String getFilePath(String str) {
        return this.directoryPath + str;
    }

    @DexIgnore
    public static synchronized MFFileHelper getInstance() {
        MFFileHelper mFFileHelper;
        synchronized (MFFileHelper.class) {
            if (sInstance == null) {
                sInstance = new MFFileHelper();
            }
            mFFileHelper = sInstance;
        }
        return mFFileHelper;
    }

    @DexIgnore
    private void queue(String str) {
        synchronized (this.pendingQueue) {
            this.pendingQueue.add(str);
            flush();
        }
    }

    @DexIgnore
    private synchronized void rotateFiles() throws Exception {
        Log.d(TAG, "Inside rotateFiles - Start rotating files...");
        for (int i = 2; i >= 0; i--) {
            File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, Integer.valueOf(i))));
            if (file.exists()) {
                FileChannel channel = new RandomAccessFile(file, rw.u).getChannel();
                FileLock lock = channel.lock();
                Log.d(TAG, "Inside rotateFiles - Locking " + file.getName());
                if (i >= 2) {
                    Log.d(TAG, "Inside rotateFiles - Deleting " + String.format(FILE_NAME_PATTERN, Integer.valueOf(i)));
                    file.delete();
                } else {
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Inside rotateFiles - Renaming ");
                    sb.append(String.format(FILE_NAME_PATTERN, Integer.valueOf(i)));
                    sb.append(" to ");
                    int i2 = i + 1;
                    sb.append(String.format(FILE_NAME_PATTERN, Integer.valueOf(i2)));
                    Log.d(str, sb.toString());
                    file.renameTo(new File(getFilePath(String.format(FILE_NAME_PATTERN, Integer.valueOf(i2)))));
                }
                Log.d(TAG, "Inside rotateFiles - Unlocking " + file.getName());
                lock.release();
                channel.close();
            }
        }
        Log.d(TAG, "Inside rotateFiles - Creating new " + getFilePath(String.format(FILE_NAME_PATTERN, 0)));
        new File(getFilePath(String.format(FILE_NAME_PATTERN, 0))).createNewFile();
        Log.d(TAG, "Inside rotateFiles - End Rotating files");
    }

    @DexIgnore
    private void writeLog(String str) {
        if (!this.isInitialized) {
            String str2 = TAG;
            Log.e(str2, TAG + " is not initialized");
            return;
        }
        File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, 0)));
        try {
            if (!file.exists()) {
                Log.d(TAG, "Inside writeLog - Creating file...");
                file.createNewFile();
            } else if (file.length() >= 3145728) {
                Log.d(TAG, "Inside writeLog - Rotating files...");
                rotateFiles();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file, true);
            fileOutputStream.write(str.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            String str3 = TAG;
            Log.e(str3, "Error inside writeLog - e=" + e + ", fileName=" + file.getAbsolutePath());
        }
    }

    @DexIgnore
    public void d(String str, String str2) {
        Locale locale = Locale.US;
        queue(String.format(locale, "%s %s: %s\n", this.sdf.format(new Date()), str + " /D", str2));
    }

    @DexIgnore
    public void e(String str, String str2) {
        Locale locale = Locale.US;
        queue(String.format(locale, "%s %s: %s\n", this.sdf.format(new Date()), str + " /E", str2));
    }

    @DexIgnore
    public List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < 3; i++) {
            File file = new File(getFilePath(String.format(FILE_NAME_PATTERN, Integer.valueOf(i))));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void i(String str, String str2) {
        Locale locale = Locale.US;
        queue(String.format(locale, "%s %s: %s\n", this.sdf.format(new Date()), str + " /I", str2));
    }

    @DexIgnore
    public synchronized void init(Context context) {
        if (!this.isInitialized) {
            this.directoryPath = context.getFilesDir().toString();
            this.isInitialized = true;
            sInstance.start();
        }
    }

    @DexIgnore
    public void run() {
        while (true) {
            try {
                if (this.pendingQueue.size() > 0) {
                    this.isRunning = true;
                    writeLog(this.pendingQueue.remove(0));
                } else {
                    synchronized (this.locker) {
                        this.isRunning = false;
                        this.locker.wait();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Thread.currentThread().interrupt();
                return;
            }
        }
    }
}
