package com.misfit.frameworks.common.model;

import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HarmonyHub {
    @DexIgnore
    public String id;
    @DexIgnore
    public List<HarmonyActivityHub> listHarmonyActivity;
    @DexIgnore
    public String name;

    @DexIgnore
    public HarmonyHub() {
        this.id = "";
        this.name = "";
        this.listHarmonyActivity = new ArrayList();
    }

    @DexIgnore
    public String getID() {
        return this.id;
    }

    @DexIgnore
    public List<HarmonyActivityHub> getListHarmonyActivity() {
        return this.listHarmonyActivity;
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public void setID(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setListHarmonyActivity(List<HarmonyActivityHub> list) {
        this.listHarmonyActivity = list;
    }

    @DexIgnore
    public void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public HarmonyHub(String str, String str2) {
        this.id = str;
        this.name = str2;
        this.listHarmonyActivity = new ArrayList();
    }
}
