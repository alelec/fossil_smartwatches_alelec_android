package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum OperationResult implements Parcelable {
    STATE_SUCCESSFUL(0),
    STATE_FAILED(1),
    STATE_REMOTE_UNREACHABLE(2),
    STATE_REMOTE_CONNECTION_LOST(3),
    STATE_NOT_REGISTERED(4),
    STATE_NOT_LOGGED_IN(5),
    STATE_UNKNOWN(-1);
    
    @DexIgnore
    public static /* final */ Parcelable.Creator<OperationResult> CREATOR; // = new Anon1();
    @DexIgnore
    public int mId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<OperationResult> {
        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OperationResult createFromParcel(Parcel parcel) {
            return OperationResult.getValue(parcel.readInt());
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OperationResult[] newArray(int i) {
            return new OperationResult[i];
        }
    }

    @DexIgnore
    public OperationResult(int i) {
        this.mId = i;
    }

    @DexIgnore
    public static OperationResult getValue(int i) {
        OperationResult[] values = values();
        for (OperationResult operationResult : values) {
            if (operationResult.mId == i) {
                return operationResult;
            }
        }
        return STATE_UNKNOWN;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
    }
}
