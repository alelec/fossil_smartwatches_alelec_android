package com.misfit.frameworks.buttonservice.source;

import android.content.Context;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.qs7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileRepository implements FirmwareFileSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static FirmwareFileRepository INSTANCE;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public String applicationFileDir;
    @DexIgnore
    public /* final */ FirmwareFileLocalSource mLocalFirmwareFileSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final FirmwareFileRepository getInstance(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
            ee7.b(context, "context");
            ee7.b(firmwareFileLocalSource, "firmwareFileLocalSource");
            FirmwareFileRepository access$getINSTANCE$cp = FirmwareFileRepository.INSTANCE;
            return access$getINSTANCE$cp != null ? access$getINSTANCE$cp : new FirmwareFileRepository(context, firmwareFileLocalSource);
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public FirmwareFileRepository(Context context, FirmwareFileLocalSource firmwareFileLocalSource) {
        ee7.b(context, "applicationContext");
        ee7.b(firmwareFileLocalSource, "mLocalFirmwareFileSource");
        this.mLocalFirmwareFileSource = firmwareFileLocalSource;
        String name = FirmwareFileRepository.class.getName();
        ee7.a((Object) name, "FirmwareFileRepository::class.java.name");
        this.TAG = name;
        String file = context.getFilesDir().toString();
        ee7.a((Object) file, "applicationContext.filesDir.toString()");
        this.applicationFileDir = file;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00bb  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x011e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ java.lang.Object downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, com.fossil.fb7 r14) {
        /*
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r0 = r10.TAG
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "downloadFirmware version "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r2 = " fileUrl "
            r1.append(r2)
            r1.append(r12)
            java.lang.String r2 = " checksum "
            r1.append(r2)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            r14.d(r0, r1)
            boolean r14 = android.text.TextUtils.isEmpty(r12)
            r0 = 1
            java.lang.String r1 = ", checkSum="
            r2 = 0
            r3 = 0
            if (r14 != 0) goto L_0x00d1
            boolean r14 = android.text.TextUtils.isEmpty(r13)
            if (r14 != 0) goto L_0x00d1
            java.lang.String r14 = r10.getFirmwareFilePath(r11)
            com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource r4 = r10.mLocalFirmwareFileSource     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            boolean r4 = r4.verify(r14, r13)     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            if (r4 != 0) goto L_0x008b
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            java.lang.String r5 = r10.TAG     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            java.lang.String r6 = "downloadFirmware version local fw is not found or invalid, download from server"
            r4.d(r5, r6)     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            java.io.BufferedInputStream r4 = r10.openConnectURL$buttonservice_release(r12)     // Catch:{ Exception -> 0x0095, all -> 0x0093 }
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0089 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r6 = r10.TAG     // Catch:{ Exception -> 0x0089 }
            java.lang.String r7 = "downloadFirmware open stream success"
            r5.d(r6, r7)     // Catch:{ Exception -> 0x0089 }
            if (r4 == 0) goto L_0x007d
            com.misfit.frameworks.buttonservice.log.FLogger r5 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0089 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r5 = r5.getLocal()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r6 = r10.TAG     // Catch:{ Exception -> 0x0089 }
            java.lang.String r7 = "downloadFirmware success, save to local"
            r5.d(r6, r7)     // Catch:{ Exception -> 0x0089 }
            com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource r5 = r10.mLocalFirmwareFileSource     // Catch:{ Exception -> 0x0089 }
            boolean r5 = r5.saveFile(r4, r14)     // Catch:{ Exception -> 0x0089 }
            goto L_0x007e
        L_0x007d:
            r5 = 0
        L_0x007e:
            if (r5 == 0) goto L_0x0087
            com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource r5 = r10.mLocalFirmwareFileSource     // Catch:{ Exception -> 0x0089 }
            boolean r5 = r5.verify(r14, r13)     // Catch:{ Exception -> 0x0089 }
            goto L_0x008d
        L_0x0087:
            r5 = 0
            goto L_0x008d
        L_0x0089:
            r5 = move-exception
            goto L_0x0097
        L_0x008b:
            r4 = r2
            r5 = 1
        L_0x008d:
            if (r4 == 0) goto L_0x00b9
            r4.close()
            goto L_0x00b9
        L_0x0093:
            r10 = move-exception
            goto L_0x00cb
        L_0x0095:
            r5 = move-exception
            r4 = r2
        L_0x0097:
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x00c9 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ all -> 0x00c9 }
            java.lang.String r7 = r10.TAG     // Catch:{ all -> 0x00c9 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c9 }
            r8.<init>()     // Catch:{ all -> 0x00c9 }
            java.lang.String r9 = "downloadFirmware() - e="
            r8.append(r9)     // Catch:{ all -> 0x00c9 }
            r8.append(r5)     // Catch:{ all -> 0x00c9 }
            java.lang.String r5 = r8.toString()     // Catch:{ all -> 0x00c9 }
            r6.e(r7, r5)     // Catch:{ all -> 0x00c9 }
            if (r4 == 0) goto L_0x00b8
            r4.close()
        L_0x00b8:
            r5 = 0
        L_0x00b9:
            if (r5 != 0) goto L_0x00c1
            com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource r4 = r10.mLocalFirmwareFileSource
            r4.deleteFile(r14)
            goto L_0x00f3
        L_0x00c1:
            com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource r2 = r10.mLocalFirmwareFileSource
            java.io.File r14 = r2.getFile(r14)
            r2 = r14
            goto L_0x00f3
        L_0x00c9:
            r10 = move-exception
            r2 = r4
        L_0x00cb:
            if (r2 == 0) goto L_0x00d0
            r2.close()
        L_0x00d0:
            throw r10
        L_0x00d1:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r4 = r10.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "downloadFirmware() failed - fileUrl="
            r5.append(r6)
            r5.append(r12)
            r5.append(r1)
            r5.append(r13)
            java.lang.String r5 = r5.toString()
            r14.d(r4, r5)
        L_0x00f3:
            com.misfit.frameworks.buttonservice.log.FLogger r14 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r14 = r14.getLocal()
            java.lang.String r10 = r10.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "downloadFirmware() - firmwareVersion="
            r4.append(r5)
            r4.append(r11)
            java.lang.String r11 = ", fileUrl="
            r4.append(r11)
            r4.append(r12)
            r4.append(r1)
            r4.append(r13)
            java.lang.String r11 = ", download success ="
            r4.append(r11)
            if (r2 == 0) goto L_0x011e
            goto L_0x011f
        L_0x011e:
            r0 = 0
        L_0x011f:
            r4.append(r0)
            java.lang.String r11 = r4.toString()
            r14.d(r10, r11)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.source.FirmwareFileRepository.downloadFirmware$suspendImpl(com.misfit.frameworks.buttonservice.source.FirmwareFileRepository, java.lang.String, java.lang.String, java.lang.String, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public Object downloadFirmware(String str, String str2, String str3, fb7<? super File> fb7) {
        return downloadFirmware$suspendImpl(this, str, str2, str3, fb7);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public String getFirmwareFilePath(String str) {
        ee7.b(str, "firmwareVersion");
        return this.applicationFileDir + "/" + qs7.a(str);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public boolean isDownloaded(String str, String str2) {
        ee7.b(str, "firmwareVersion");
        ee7.b(str2, "checkSum");
        String firmwareFilePath = getFirmwareFilePath(str);
        boolean z = true;
        if (!(str2.length() == 0)) {
            return this.mLocalFirmwareFileSource.verify(firmwareFilePath, str2);
        }
        if (this.mLocalFirmwareFileSource.getFile(firmwareFilePath) == null) {
            z = false;
        }
        return z;
    }

    @DexIgnore
    public BufferedInputStream openConnectURL$buttonservice_release(String str) {
        ee7.b(str, "fileUrl");
        try {
            URL url = new URL(str);
            URLConnection openConnection = url.openConnection();
            openConnection.connect();
            ee7.a((Object) openConnection, "connection");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, "openConnectURL(), filePath=" + url + ", size=" + ((long) openConnection.getContentLength()));
            return new BufferedInputStream(openConnection.getInputStream());
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, "openConnectURL(), ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.source.FirmwareFileSource
    public byte[] readFirmware(String str) {
        ee7.b(str, "firmwareVersion");
        FileInputStream readFile = this.mLocalFirmwareFileSource.readFile(getFirmwareFilePath(str));
        byte[] bArr = null;
        if (readFile != null) {
            try {
                int size = (int) readFile.getChannel().size();
                byte[] bArr2 = new byte[size];
                int read = readFile.read(bArr2);
                if (read != size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.TAG;
                    local.e(str2, "getOtaData() - expectedSize=" + size + ", readSize=" + read);
                } else {
                    bArr = bArr2;
                }
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = this.TAG;
                local2.e(str3, "getOtaData() - e=" + e);
            } catch (Throwable th) {
                readFile.close();
                throw th;
            }
            readFile.close();
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("readFirmware() - firmwareVersion=");
        sb.append(str);
        sb.append(", dataExist=");
        sb.append(bArr != null);
        local3.d(str4, sb.toString());
        return bArr;
    }
}
