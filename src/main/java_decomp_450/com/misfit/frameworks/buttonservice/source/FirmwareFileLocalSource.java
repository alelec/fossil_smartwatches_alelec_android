package com.misfit.frameworks.buttonservice.source;

import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileLocalSource {
    @DexIgnore
    public /* final */ String TAG;

    @DexIgnore
    public FirmwareFileLocalSource() {
        String name = FirmwareFileLocalSource.class.getName();
        ee7.a((Object) name, "FirmwareFileLocalSource::class.java.name");
        this.TAG = name;
    }

    @DexIgnore
    public boolean deleteFile(String str) {
        ee7.b(str, "filePath");
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, ".saveFile(), success=" + true + ", filePath=" + str);
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".deleteFile(), cannot delete file, ex=" + e);
            return false;
        }
    }

    @DexIgnore
    public File getFile(String str) {
        ee7.b(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".getFile(), filePath=" + str);
        try {
            return new File(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".getFile(), cannot get file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    public FileInputStream readFile(String str) {
        ee7.b(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".readFile(), filePath=" + str);
        try {
            return new FileInputStream(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".readFile(), cannot read file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009a A[SYNTHETIC, Splitter:B:24:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bc A[SYNTHETIC, Splitter:B:29:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean saveFile(java.io.InputStream r10, java.lang.String r11) {
        /*
            r9 = this;
            java.lang.String r0 = ".saveFile(), close output stream, ex="
            java.lang.String r1 = "inputStream"
            com.fossil.ee7.b(r10, r1)
            java.lang.String r1 = "filePath"
            com.fossil.ee7.b(r11, r1)
            r1 = 0
            r2 = 1
            r3 = 0
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x007b }
            r4.<init>(r11)     // Catch:{ Exception -> 0x007b }
            r11 = 1024(0x400, float:1.435E-42)
            byte[] r11 = new byte[r11]     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r5 = 0
            int r3 = r10.read(r11)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
        L_0x001e:
            r7 = -1
            if (r3 == r7) goto L_0x002b
            long r7 = (long) r3     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            long r5 = r5 + r7
            r4.write(r11, r1, r3)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            int r3 = r10.read(r11)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            goto L_0x001e
        L_0x002b:
            r4.flush()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            com.misfit.frameworks.buttonservice.log.FLogger r10 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r10 = r10.getLocal()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.String r11 = r9.TAG     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r3.<init>()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.String r7 = ".saveFile(), success="
            r3.append(r7)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r3.append(r2)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.String r7 = ", total size="
            r3.append(r7)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r3.append(r5)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r10.d(r11, r3)     // Catch:{ Exception -> 0x0076, all -> 0x0073 }
            r4.close()     // Catch:{ Exception -> 0x0056 }
            goto L_0x0071
        L_0x0056:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r1 = r9.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            r11.e(r1, r10)
        L_0x0071:
            r1 = 1
            goto L_0x00b9
        L_0x0073:
            r10 = move-exception
            r3 = r4
            goto L_0x00ba
        L_0x0076:
            r10 = move-exception
            r3 = r4
            goto L_0x007c
        L_0x0079:
            r10 = move-exception
            goto L_0x00ba
        L_0x007b:
            r10 = move-exception
        L_0x007c:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0079 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = r9.TAG     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            r4.<init>()     // Catch:{ all -> 0x0079 }
            java.lang.String r5 = ".saveFile(), cannot save file, ex="
            r4.append(r5)     // Catch:{ all -> 0x0079 }
            r4.append(r10)     // Catch:{ all -> 0x0079 }
            java.lang.String r10 = r4.toString()     // Catch:{ all -> 0x0079 }
            r11.e(r2, r10)     // Catch:{ all -> 0x0079 }
            if (r3 == 0) goto L_0x00b9
            r3.close()     // Catch:{ Exception -> 0x009e }
            goto L_0x00b9
        L_0x009e:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()
            java.lang.String r2 = r9.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            r11.e(r2, r10)
        L_0x00b9:
            return r1
        L_0x00ba:
            if (r3 == 0) goto L_0x00db
            r3.close()     // Catch:{ Exception -> 0x00c0 }
            goto L_0x00db
        L_0x00c0:
            r11 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = r9.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r3.append(r11)
            java.lang.String r11 = r3.toString()
            r1.e(r2, r11)
        L_0x00db:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.source.FirmwareFileLocalSource.saveFile(java.io.InputStream, java.lang.String):boolean");
    }

    @DexIgnore
    public boolean verify(String str, String str2) {
        ee7.b(str, "filePath");
        ee7.b(str2, "checkSum");
        boolean verifyDownloadFile = FileUtils.verifyDownloadFile(str, str2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, ".verify(), isValid=" + verifyDownloadFile + ", filePath=" + str + ", checkSum=" + str2);
        return verifyDownloadFile;
    }
}
