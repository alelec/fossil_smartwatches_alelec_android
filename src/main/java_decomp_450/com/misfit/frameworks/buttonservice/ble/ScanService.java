package com.misfit.frameworks.buttonservice.ble;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.ee7;
import com.fossil.f60;
import com.fossil.g60;
import com.fossil.j60;
import com.fossil.l60;
import com.fossil.n60;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ScanService implements f60.b {
    @DexIgnore
    public static /* final */ long BLE_SCAN_TIMEOUT; // = 120000;
    @DexIgnore
    public static /* final */ int CONNECT_TIMEOUT; // = 10000;
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_BOND_RSSI_MARK; // = -999999;
    @DexIgnore
    public static /* final */ int RETRIEVE_DEVICE_RSSI_MARK; // = 0;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public Handler autoStopHandler;
    @DexIgnore
    public /* final */ Runnable autoStopTask; // = new ScanService$autoStopTask$Anon1(this);
    @DexIgnore
    public Callback callback;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public boolean isScanning; // = false;
    @DexIgnore
    public MFLog mfLog;
    @DexIgnore
    public long startScanTimestamp;
    @DexIgnore
    public /* final */ long tagTime;

    @DexIgnore
    public interface Callback {
        @DexIgnore
        void onDeviceFound(l60 l60, int i);

        @DexIgnore
        void onScanFail(g60 g60);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = ScanService.class.getSimpleName();
        ee7.a((Object) simpleName, "ScanService::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ScanService(Context context2, Callback callback2, long j) {
        ee7.b(context2, "context");
        this.callback = callback2;
        this.tagTime = j;
        Context applicationContext = context2.getApplicationContext();
        ee7.a((Object) applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    @DexIgnore
    private final String getCollectionTagByActiveLog() {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            String l = Long.toString(this.tagTime);
            ee7.a((Object) l, "java.lang.Long.toString(tagTime)");
            return l;
        } else if (mFLog != null) {
            return String.valueOf(mFLog.getStartTimeEpoch());
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    private final void log(String str) {
        MFLog mFLog = this.mfLog;
        if (mFLog == null) {
            return;
        }
        if (mFLog != null) {
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            MFLog mFLog2 = this.mfLog;
            if (mFLog2 != null) {
                sb.append(mFLog2.getSerial());
                sb.append(" - Scanning] ");
                sb.append(str);
                mFLog.log(sb.toString());
                return;
            }
            ee7.a();
            throw null;
        }
        ee7.a();
        throw null;
    }

    @DexIgnore
    private final void startAutoStopTimer() {
        stopAutoStopTimer();
        Handler handler = new Handler(Looper.getMainLooper());
        this.autoStopHandler = handler;
        if (handler != null) {
            handler.postDelayed(this.autoStopTask, 120000);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    private final void stopAutoStopTimer() {
        Handler handler = this.autoStopHandler;
        if (handler != null) {
            handler.removeCallbacks(this.autoStopTask);
        }
    }

    @DexIgnore
    public final l60 buildDeviceBySerial(String str, String str2) {
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        try {
            return f60.k.a(str, str2);
        } catch (Exception e) {
            log("BuildDeviceBySerrial, error:" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.fossil.f60.b
    public void onDeviceFound(l60 l60, int i) {
        ee7.b(l60, "device");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, ".onDeviceFound, serialNumber=" + l60.n().getSerialNumber() + ", fastPairIdHex=" + l60.n().getFastPairIdInHexString() + ", hashcode=" + hashCode());
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onDeviceFound, callback is null");
        } else if (callback2 != null) {
            callback2.onDeviceFound(l60, i);
        } else {
            ee7.a();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.f60.b
    public void onStartScanFailed(g60 g60) {
        ee7.b(g60, "scanError");
        stopScan();
        Callback callback2 = this.callback;
        if (callback2 == null) {
            FLogger.INSTANCE.getLocal().e(TAG, ".onStartScanFail, callback is null");
        } else if (callback2 != null) {
            callback2.onScanFail(g60);
        } else {
            ee7.a();
            throw null;
        }
        this.callback = null;
    }

    @DexIgnore
    public final void setActiveDeviceLog(String str) {
        ee7.b(str, "serial");
        this.mfLog = MFLogManager.getInstance(this.context).getActiveLog(str);
    }

    @DexIgnore
    public final synchronized void startScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScan");
        this.isScanning = true;
        f60.k.a(new j60().setDeviceTypes(new n60[]{n60.DIANA, n60.MINI, n60.SLIM, n60.SE1, n60.WEAR_OS, n60.IVY}), this);
        log("Called start scan api v2.");
    }

    @DexIgnore
    public final synchronized void startScanWithAutoStopTimer() {
        FLogger.INSTANCE.getLocal().d(TAG, ".startScanWithAutoStopTimer()");
        startAutoStopTimer();
        this.isScanning = true;
        f60.k.a(new j60().setDeviceTypes(new n60[]{n60.DIANA, n60.MINI, n60.SLIM, n60.SE1, n60.WEAR_OS, n60.IVY}), this);
        this.startScanTimestamp = System.currentTimeMillis();
    }

    @DexIgnore
    public final synchronized void stopScan() {
        FLogger.INSTANCE.getLocal().d(TAG, ".stopScan");
        this.startScanTimestamp = -1;
        this.isScanning = false;
        stopAutoStopTimer();
        f60.k.b(this);
        log("Called stop scan api v2.");
    }
}
