package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import com.fossil.ee7;
import com.fossil.he0;
import com.fossil.hg0;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.jg0;
import com.fossil.ke0;
import com.fossil.o80;
import com.fossil.r60;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.SharePreferencesUtils;
import com.misfit.frameworks.buttonservice.utils.WatchAppUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseTransferSettingsSubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BackgroundConfig backgroundConfig;
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public /* final */ boolean isFullSync;
    @DexIgnore
    public /* final */ LocalizationData localizationData;
    @DexIgnore
    public /* final */ List<MicroAppMapping> microAppMappings;
    @DexIgnore
    public /* final */ List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public /* final */ AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public /* final */ int secondTimezoneOffset;
    @DexIgnore
    public /* final */ UserProfile userProfile;
    @DexIgnore
    public /* final */ WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBackgroundImageConfig extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBackgroundImageConfig() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                BackgroundConfig backgroundConfig = BaseTransferSettingsSubFlow.this.getBackgroundConfig();
                if (backgroundConfig != null) {
                    ie0<i97> backgroundImage = BaseTransferSettingsSubFlow.this.getBleAdapter().setBackgroundImage(BaseTransferSettingsSubFlow.this.getLogSession(), backgroundConfig, this);
                    this.task = backgroundImage;
                    if (backgroundImage == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                    } else {
                        startTimeout();
                        obj = i97.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No background image config");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Background Image Config step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetBackgroundConfig=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetBackgroundImageSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.BACKGROUND_IMAGE);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Background Image timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_BACKGROUND_IMAGE_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LOCALIZATION_STATE));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplications extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetComplications() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                ComplicationAppMappingSettings complicationAppMappingSettings = BaseTransferSettingsSubFlow.this.getComplicationAppMappingSettings();
                if (complicationAppMappingSettings != null) {
                    ie0<i97> complications = BaseTransferSettingsSubFlow.this.getBleAdapter().setComplications(BaseTransferSettingsSubFlow.this.getLogSession(), complicationAppMappingSettings, this);
                    this.task = complications;
                    if (complications == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                    } else {
                        startTimeout();
                        obj = i97.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Complication step. No complication settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Complication step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(1920);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Complication timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(1920);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APPS_STATE));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class SetDeviceConfigState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDeviceConfigState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:40:0x01fe, code lost:
            if (r0 == null) goto L_0x0201;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x0242, code lost:
            if (r0 == null) goto L_0x0245;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.fossil.n80[] prepareConfigData() {
            /*
                r14 = this;
                long r0 = java.lang.System.currentTimeMillis()
                r2 = 1000(0x3e8, float:1.401E-42)
                long r3 = (long) r2
                long r5 = r0 / r3
                long r3 = r3 * r5
                long r3 = r0 - r3
                int r4 = (int) r3
                short r3 = (short) r4
                java.util.TimeZone r4 = java.util.TimeZone.getDefault()
                int r0 = r4.getOffset(r0)
                int r0 = r0 / r2
                int r0 = r0 / 60
                short r0 = (short) r0
                com.fossil.a90 r1 = new com.fossil.a90
                r1.<init>()
                r1.a(r5, r3, r0)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r0.getBleSession()
                boolean r0 = r0 instanceof com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession
                if (r0 == 0) goto L_0x0042
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = r14.getTAG()
                java.lang.String r3 = "Pair new device, set vibe strength medium as default"
                r0.d(r2, r3)
                com.fossil.w80$a r0 = com.fossil.w80.a.MEDIUM
                r1.a(r0)
                goto L_0x006b
            L_0x0042:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r0 = r0.getVibrationStrength()
                boolean r0 = r0.isDefaultValue()
                if (r0 != 0) goto L_0x0064
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj r0 = r0.getVibrationStrength()
                com.fossil.w80$a r0 = r0.toSDKVibrationStrengthLevel()
                r1.a(r0)
                goto L_0x006b
            L_0x0064:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set Device Config: VibeStrength is default value, do not set it to device."
                r0.log(r2)
            L_0x006b:
                com.misfit.frameworks.buttonservice.utils.SettingsUtils r0 = com.misfit.frameworks.buttonservice.utils.SettingsUtils.INSTANCE
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                int r2 = r2.getSecondTimezoneOffset()
                short r2 = (short) r2
                boolean r0 = r0.isSecondTimezoneInRange(r2)
                if (r0 == 0) goto L_0x0085
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                int r0 = r0.getSecondTimezoneOffset()
                short r0 = (short) r0
                r1.a(r0)
                goto L_0x00a1
            L_0x0085:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "Set Device Config: Timezone is out of range: "
                r2.append(r3)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r3 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                int r3 = r3.getSecondTimezoneOffset()
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                r0.log(r2)
            L_0x00a1:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                if (r0 == 0) goto L_0x0130
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                long r2 = r0.getCurrentSteps()
                r1.d(r2)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                long r2 = r0.getGoalSteps()
                r1.e(r2)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                int r0 = r0.getActiveMinute()
                r1.b(r0)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                int r0 = r0.getActiveMinuteGoal()
                r1.a(r0)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                long r2 = r0.getCalories()
                r1.a(r2)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                long r2 = r0.getCaloriesGoal()
                r1.b(r2)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                long r2 = r0.getDistanceInCentimeter()
                r1.c(r2)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                int r0 = r0.getTotalSleepInMinute()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r2 = r2.getUserProfile()
                int r2 = r2.getAwakeInMinute()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r3 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r3 = r3.getUserProfile()
                int r3 = r3.getLightSleepInMinute()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r4 = r4.getUserProfile()
                int r4 = r4.getDeepSleepInMinute()
                r1.b(r0, r2, r3, r4)
                goto L_0x0139
            L_0x0130:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set Device Config: No user profile data."
                r0.log(r2)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x0139:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                if (r0 == 0) goto L_0x0170
                com.misfit.frameworks.buttonservice.model.UserDisplayUnit r0 = r0.getDisplayUnit()
                if (r0 == 0) goto L_0x0170
                com.misfit.frameworks.buttonservice.model.UserDisplayUnit$TemperatureUnit r2 = r0.getTemperatureUnit()
                com.fossil.g90 r8 = r2.toSDKTemperatureUnit()
                com.fossil.b90 r9 = com.fossil.b90.KCAL
                com.misfit.frameworks.buttonservice.model.UserDisplayUnit$DistanceUnit r0 = r0.getDistanceUnit()
                com.fossil.e90 r10 = r0.toSDKDistanceUnit()
                com.misfit.frameworks.buttonservice.utils.ConversionUtils r0 = com.misfit.frameworks.buttonservice.utils.ConversionUtils.INSTANCE
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r2 = r2.getBleAdapter()
                android.content.Context r2 = r2.getContext()
                com.fossil.h90 r11 = r0.getTimeFormat(r2)
                com.fossil.d90 r12 = com.fossil.d90.MONTH_DAY_YEAR
                r7 = r1
                r7.a(r8, r9, r10, r11, r12)
                goto L_0x0179
            L_0x0170:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set Device Config: No user display unit."
                r0.log(r2)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x0179:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                if (r0 == 0) goto L_0x01ac
                com.misfit.frameworks.buttonservice.model.InactiveNudgeData r0 = r0.getInactiveNudgeData()
                if (r0 == 0) goto L_0x01ac
                byte r8 = r0.getStartHour()
                byte r9 = r0.getStartMinute()
                byte r10 = r0.getStopHour()
                byte r11 = r0.getStopMinute()
                short r12 = r0.getRepeatInterval()
                boolean r0 = r0.isEnable()
                if (r0 == 0) goto L_0x01a4
                com.fossil.t80$a r0 = com.fossil.t80.a.ENABLE
                goto L_0x01a6
            L_0x01a4:
                com.fossil.t80$a r0 = com.fossil.t80.a.DISABLE
            L_0x01a6:
                r13 = r0
                r7 = r1
                r7.a(r8, r9, r10, r11, r12, r13)
                goto L_0x01b5
            L_0x01ac:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set Device Config: No inactive nudge config."
                r0.log(r2)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x01b5:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                if (r0 == 0) goto L_0x0201
                com.misfit.frameworks.buttonservice.model.UserBiometricData r0 = r0.getUserBiometricData()
                if (r0 == 0) goto L_0x0201
                com.fossil.c80 r0 = r0.toSDKBiometricProfile()     // Catch:{ Exception -> 0x01e1 }
                byte r8 = r0.getAge()     // Catch:{ Exception -> 0x01e1 }
                com.fossil.c80$a r9 = r0.getGender()     // Catch:{ Exception -> 0x01e1 }
                short r10 = r0.getHeightInCentimeter()     // Catch:{ Exception -> 0x01e1 }
                short r11 = r0.getWeightInKilogram()     // Catch:{ Exception -> 0x01e1 }
                com.fossil.c80$b r12 = r0.getWearingPosition()     // Catch:{ Exception -> 0x01e1 }
                r7 = r1
                r7.a(r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x01e1 }
                r0 = r1
                goto L_0x01fe
            L_0x01e1:
                r0 = move-exception
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Set Device Config: exception="
                r3.append(r4)
                java.lang.String r0 = r0.getMessage()
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                r2.log(r0)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x01fe:
                if (r0 == 0) goto L_0x0201
                goto L_0x020a
            L_0x0201:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set Device Config: No biometric data."
                r0.log(r2)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x020a:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.model.UserProfile r0 = r0.getUserProfile()
                if (r0 == 0) goto L_0x0245
                com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting r0 = r0.getWorkoutDetectionSetting()
                if (r0 == 0) goto L_0x0245
                java.util.List r0 = r0.getWorkoutDetectionList()     // Catch:{ Exception -> 0x0225 }
                com.fossil.z80[] r0 = com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionItemExtentionKt.toAutoWorkoutDetectionItemConfig(r0)     // Catch:{ Exception -> 0x0225 }
                r1.a(r0)     // Catch:{ Exception -> 0x0225 }
                r0 = r1
                goto L_0x0242
            L_0x0225:
                r0 = move-exception
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Set AutoWorkoutDetection Config: exception="
                r3.append(r4)
                java.lang.String r0 = r0.getMessage()
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                r2.log(r0)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x0242:
                if (r0 == 0) goto L_0x0245
                goto L_0x024e
            L_0x0245:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r2 = "Set AutoWorkoutDetection Config: No data"
                r0.log(r2)
                com.fossil.i97 r0 = com.fossil.i97.a
            L_0x024e:
                com.fossil.n80[] r0 = r1.a()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetDeviceConfigState.prepareConfigData():com.fossil.n80[]");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<o80[]> deviceConfig = BaseTransferSettingsSubFlow.this.getBleAdapter().setDeviceConfig(BaseTransferSettingsSubFlow.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Device Config timeout. Cancel.");
            ie0<o80[]> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_LIST_ALARMS_STATE));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetListAlarmsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                List<AlarmSetting> multiAlarmSettings = BaseTransferSettingsSubFlow.this.getMultiAlarmSettings();
                if (multiAlarmSettings != null) {
                    ie0<i97> alarms = BaseTransferSettingsSubFlow.this.getBleAdapter().setAlarms(BaseTransferSettingsSubFlow.this.getLogSession(), multiAlarmSettings, this);
                    this.task = alarms;
                    if (alarms == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
                    } else {
                        startTimeout();
                        obj = i97.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Alarm step. Alarm settings null");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Alarm step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetMultiAlarmSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MULTI_ALARM);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set List Alarms timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_ALARM);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_WATCH_APP_FILES));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalization extends BleStateAbs {
        @DexIgnore
        public he0<String> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetLocalization() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                LocalizationData localizationData = BaseTransferSettingsSubFlow.this.getLocalizationData();
                if (localizationData != null) {
                    if (localizationData.isDataValid()) {
                        he0<String> localizationData2 = BaseTransferSettingsSubFlow.this.getBleAdapter().setLocalizationData(localizationData, BaseTransferSettingsSubFlow.this.getLogSession(), this);
                        this.task = localizationData2;
                        if (localizationData2 == null) {
                            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                            obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                        } else {
                            startTimeout();
                            obj = i97.a;
                        }
                    } else {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                        baseTransferSettingsSubFlow2.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow3.enterSubStateAsync(baseTransferSettingsSubFlow3.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Localization step. No localization settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow5 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow5.log("Skip Set Localization step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetLocalizationSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow6 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow6.enterSubStateAsync(baseTransferSettingsSubFlow6.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Localization timeout. Cancel.");
            he0<String> he0 = this.task;
            if (he0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE));
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public he0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetMicroAppMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onConfigureMicroAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (BaseTransferSettingsSubFlow.this.getMicroAppMappings() == null || !(!BaseTransferSettingsSubFlow.this.getMicroAppMappings().isEmpty())) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
            } else {
                BleAdapterImpl bleAdapter = BaseTransferSettingsSubFlow.this.getBleAdapter();
                FLogger.Session logSession = BaseTransferSettingsSubFlow.this.getLogSession();
                List<jg0> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(BaseTransferSettingsSubFlow.this.getMicroAppMappings());
                ee7.a((Object) convertToSDKMapping, "MicroAppMapping.convertT\u2026Mapping(microAppMappings)");
                he0<i97> configureMicroApp = bleAdapter.configureMicroApp(logSession, convertToSDKMapping, this);
                this.task = configureMicroApp;
                if (configureMicroApp == null) {
                    BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                    baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
                } else {
                    startTimeout();
                }
            }
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Micro Apps timeout. Cancel.");
            he0<i97> he0 = this.task;
            if (he0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE));
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetNotificationFilterSettings extends BleStateAbs {
        @DexIgnore
        public he0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetNotificationFilterSettings() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                AppNotificationFilterSettings notificationFilterSettings = BaseTransferSettingsSubFlow.this.getNotificationFilterSettings();
                if (notificationFilterSettings != null) {
                    he0<i97> notificationFilters = BaseTransferSettingsSubFlow.this.getBleAdapter().setNotificationFilters(BaseTransferSettingsSubFlow.this.getLogSession(), notificationFilterSettings.getNotificationFilters(), this);
                    this.task = notificationFilters;
                    if (notificationFilters == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                    } else {
                        startTimeout();
                        obj = i97.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip this step. No notification filter settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Notification Filter Settings step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetNotificationFilterSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterProgressChanged(float f) {
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetNotificationFilterSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.NOTIFICATION_FILTERS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Notification Filter Settings timeout. Cancel.");
            he0<i97> he0 = this.task;
            if (he0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_NOTIFICATION_FILTERS_CONFIG);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE));
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetReplyMessageMappingState extends BleStateAbs {
        @DexIgnore
        public he0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetReplyMessageMappingState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (!FossilDeviceSerialPatternUtil.isDianaDevice(BaseTransferSettingsSubFlow.this.getSerial()) || BaseTransferSettingsSubFlow.this.getUserProfile() == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
                return true;
            }
            he0<i97> replyMessageMapping = BaseTransferSettingsSubFlow.this.getBleAdapter().setReplyMessageMapping(BaseTransferSettingsSubFlow.this.getLogSession(), BaseTransferSettingsSubFlow.this.getUserProfile().getReplyMessageMapping(), this);
            this.task = replyMessageMapping;
            if (replyMessageMapping == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(10000);
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingError(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("setReplyMessageMappingError");
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Reply Message Mapping success");
            BaseTransferSettingsSubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            he0<i97> he0 = this.task;
            if (he0 == null) {
                BaseTransferSettingsSubFlow.this.stopSubFlow(0);
            } else if (he0 != null) {
                he0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppFiles extends BleStateAbs {
        @DexIgnore
        public he0<hg0[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppFiles() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle bundleUiVersion(r60 r60) {
            Bundle bundle = new Bundle();
            bundle.putByte(ButtonService.Companion.getWATCH_APP_MAJOR_VERSION(), r60.getMajor());
            bundle.putByte(ButtonService.Companion.getWATCH_APP_MINOR_VERSION(), r60.getMinor());
            return bundle;
        }

        @DexIgnore
        private final hg0[] prepareWatchAppFiles() {
            return WatchAppUtils.INSTANCE.getWatchAppFiles(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0024, code lost:
            if ((r0.length == 0) != false) goto L_0x0026;
         */
        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onEnter() {
            /*
                r5 = this;
                super.onEnter()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r1 = "Enter Set Watch App Files"
                r0.log(r1)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r0 = r0.getSerial()
                boolean r0 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.isDianaDevice(r0)
                r1 = 1
                if (r0 == 0) goto L_0x00b7
                com.fossil.hg0[] r0 = r5.prepareWatchAppFiles()
                r2 = 0
                if (r0 == 0) goto L_0x0026
                int r3 = r0.length
                if (r3 != 0) goto L_0x0023
                r3 = 1
                goto L_0x0024
            L_0x0023:
                r3 = 0
            L_0x0024:
                if (r3 == 0) goto L_0x0027
            L_0x0026:
                r2 = 1
            L_0x0027:
                if (r2 == 0) goto L_0x00b3
                com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
                java.lang.String r2 = r5.getTAG()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "Start fetch watchAppData from network, version "
                r3.append(r4)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r4 = r4.getBleAdapter()
                com.fossil.r60 r4 = r4.getUiPackageOSVersion()
                r3.append(r4)
                java.lang.String r4 = " callback "
                r3.append(r4)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r4 = r4.getBleSessionCallback()
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                r0.d(r2, r3)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.fossil.r60 r0 = r0.getUiPackageOSVersion()
                if (r0 == 0) goto L_0x00a7
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r0 = r0.getBleAdapter()
                com.fossil.r60 r0 = r0.getUiPackageOSVersion()
                if (r0 == 0) goto L_0x00a2
                android.os.Bundle r0 = r5.bundleUiVersion(r0)
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r2 = r2.getBleSessionCallback()
                if (r2 == 0) goto L_0x0096
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r2 = r2.getBleSessionCallback()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r3 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.String r3 = r3.getSerial()
                r2.onAskForWatchAppFiles(r3, r0)
                r5.startTimeout()
                goto L_0x00c2
            L_0x0096:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_COMPLICATIONS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
                goto L_0x00c2
            L_0x00a2:
                com.fossil.ee7.a()
                r0 = 0
                throw r0
            L_0x00a7:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_COMPLICATIONS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
                goto L_0x00c2
            L_0x00b3:
                r5.setWatchAppFilesToDevice(r0)
                goto L_0x00c2
            L_0x00b7:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r2 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_COMPLICATIONS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r2 = r0.createConcreteState(r2)
                r0.enterSubStateAsync(r2)
            L_0x00c2:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles.onEnter():boolean");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileFailed() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files Failed");
            SharePreferencesUtils instance = SharePreferencesUtils.getInstance(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
            instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), false);
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileProgressChanged(float f) {
            setTimeout(30000);
            startTimeout();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFileSuccess() {
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files success");
            SharePreferencesUtils instance = SharePreferencesUtils.getInstance(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext());
            Boolean bool = instance.getBoolean(SharePreferencesUtils.BC_STATUS, false);
            ee7.a((Object) bool, "isBCOn");
            if (bool.booleanValue()) {
                instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), true);
            } else {
                instance.setBoolean(SharePreferencesUtils.BC_APP_READY + BaseTransferSettingsSubFlow.this.getSerial(), false);
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch App Files Timeout");
            he0<hg0[]> he0 = this.task;
            if (he0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APP_FILES);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0048, code lost:
            if ((r4.length == 0) != false) goto L_0x004a;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void resumeSetWatchAppFilesState(boolean r4) {
            /*
                r3 = this;
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "resumeSetWatchAppFilesState, isSuccess = "
                r1.append(r2)
                r1.append(r4)
                java.lang.String r1 = r1.toString()
                r0.log(r1)
                r3.stopTimeout()
                if (r4 == 0) goto L_0x005d
                com.fossil.hg0[] r4 = r3.prepareWatchAppFiles()
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "resumeSetWatchAppFilesState, watchAppFile size after downloaded "
                r1.append(r2)
                if (r4 == 0) goto L_0x0033
                int r2 = r4.length
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
                goto L_0x0034
            L_0x0033:
                r2 = 0
            L_0x0034:
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.log(r1)
                r0 = 0
                r1 = 1
                if (r4 == 0) goto L_0x004a
                int r2 = r4.length
                if (r2 != 0) goto L_0x0047
                r2 = 1
                goto L_0x0048
            L_0x0047:
                r2 = 0
            L_0x0048:
                if (r2 == 0) goto L_0x004b
            L_0x004a:
                r0 = 1
            L_0x004b:
                if (r0 != 0) goto L_0x0051
                r3.setWatchAppFilesToDevice(r4)
                goto L_0x0068
            L_0x0051:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_COMPLICATIONS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r0 = r4.createConcreteState(r0)
                r4.enterSubStateAsync(r0)
                goto L_0x0068
            L_0x005d:
                com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow r4 = com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.this
                com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$SessionState r0 = com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow.SessionState.SET_COMPLICATIONS_STATE
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs r0 = r4.createConcreteState(r0)
                r4.enterSubStateAsync(r0)
            L_0x0068:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles.resumeSetWatchAppFilesState(boolean):void");
        }

        @DexIgnore
        public final void setWatchAppFilesToDevice(hg0[] hg0Arr) {
            ee7.b(hg0Arr, "watchAppFiles");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "setWatchAppFilesToDevice, size = " + hg0Arr.length);
            he0<hg0[]> watchAppFiles = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchAppFiles(BaseTransferSettingsSubFlow.this.getLogSession(), hg0Arr, this);
            this.task = watchAppFiles;
            if (watchAppFiles == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(10000);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_COMPLICATIONS_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchApps extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchApps() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            Object obj;
            super.onEnter();
            boolean isNeedToSetSetting = DevicePreferenceUtils.isNeedToSetSetting(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            if (BaseTransferSettingsSubFlow.this.isFullSync() || isNeedToSetSetting) {
                WatchAppMappingSettings watchAppMappingSettings = BaseTransferSettingsSubFlow.this.getWatchAppMappingSettings();
                if (watchAppMappingSettings != null) {
                    ie0<i97> watchApps = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchApps(BaseTransferSettingsSubFlow.this.getLogSession(), watchAppMappingSettings, this);
                    this.task = watchApps;
                    if (watchApps == null) {
                        BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                        obj = baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                    } else {
                        startTimeout();
                        obj = i97.a;
                    }
                    if (obj != null) {
                        return true;
                    }
                }
                BaseTransferSettingsSubFlow.this.log("Skip Set Watch App step. No watch apps settings");
                DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow2 = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow2.enterSubStateAsync(baseTransferSettingsSubFlow2.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
                return true;
            }
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow3 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow3.log("Skip Set Watch App step. isFullSync=" + BaseTransferSettingsSubFlow.this.isFullSync() + ", isNeedToSetComplicationAppSettings=" + isNeedToSetSetting);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow4 = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow4.enterSubStateAsync(baseTransferSettingsSubFlow4.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.removeSettingFlag(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), DeviceSettings.WATCH_APPS);
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Apps timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_APPS);
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE));
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchParamsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchParamsState() {
            super(BaseTransferSettingsSubFlow.this.getTAG());
        }

        @DexIgnore
        private final Bundle exportFirmwareVersion() {
            Bundle bundle = new Bundle();
            bundle.putInt(ButtonService.WATCH_PARAMS_MAJOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMajor());
            bundle.putInt(ButtonService.WATCH_PARAMS_MINOR, BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamMinor());
            bundle.putFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION, Float.parseFloat(BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion()));
            return bundle;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            startTimeout();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "supportedWatchParamVersion = " + BaseTransferSettingsSubFlow.this.getBleAdapter().getSupportedWatchParamVersion() + ", currentWatchParamVersion=" + BaseTransferSettingsSubFlow.this.getBleAdapter().getCurrentWatchParamVersion());
            BleSession.BleSessionCallback bleSessionCallback = BaseTransferSettingsSubFlow.this.getBleSessionCallback();
            if (bleSessionCallback == null) {
                return true;
            }
            bleSessionCallback.onRequestLatestWatchParams(BaseTransferSettingsSubFlow.this.getSerial(), exportFirmwareVersion());
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onGetWatchParamsFail() {
            stopTimeout();
            if (!retry(BaseTransferSettingsSubFlow.this.getBleAdapter().getContext(), BaseTransferSettingsSubFlow.this.getSerial())) {
                BaseTransferSettingsSubFlow.this.log("Reach the limit retry. Stop.");
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchParamsFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Param failed");
            BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchParamsSuccess() {
            BaseTransferSettingsSubFlow.this.log("Set Watch Param success");
            stopTimeout();
            BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
            baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            BaseTransferSettingsSubFlow.this.log("Set Watch Params timeout. Cancel.");
            ie0<i97> ie0 = this.task;
            if (ie0 == null) {
                BaseTransferSettingsSubFlow.this.addFailureCode(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
                BaseTransferSettingsSubFlow.this.stopSubFlow(FailureCode.FAILED_TO_SET_WATCH_PARAMS);
            } else if (ie0 != null) {
                ie0.e();
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final void setWatchParamToDevice(String str, WatchParamsFileMapping watchParamsFileMapping) {
            ee7.b(str, "serial");
            ee7.b(watchParamsFileMapping, "watchParamsFileMapping");
            BaseTransferSettingsSubFlow.this.log("Set WatchParam to device");
            ie0<i97> watchParams = BaseTransferSettingsSubFlow.this.getBleAdapter().setWatchParams(BaseTransferSettingsSubFlow.this.getLogSession(), watchParamsFileMapping, this);
            this.task = watchParams;
            if (watchParams == null) {
                BaseTransferSettingsSubFlow baseTransferSettingsSubFlow = BaseTransferSettingsSubFlow.this;
                baseTransferSettingsSubFlow.enterSubStateAsync(baseTransferSettingsSubFlow.createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
                return;
            }
            startTimeout();
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r23v0, resolved type: java.util.List<? extends com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseTransferSettingsSubFlow(String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, boolean z, String str2, BleAdapterImpl bleAdapterImpl, UserProfile userProfile2, List<AlarmSetting> list, ComplicationAppMappingSettings complicationAppMappingSettings2, WatchAppMappingSettings watchAppMappingSettings2, BackgroundConfig backgroundConfig2, AppNotificationFilterSettings appNotificationFilterSettings, LocalizationData localizationData2, List<? extends MicroAppMapping> list2, int i, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        ee7.b(str, "tagName");
        ee7.b(bleSession, "bleSession");
        ee7.b(session, "logSession");
        ee7.b(str2, "serial");
        ee7.b(bleAdapterImpl, "mBleAdapterV2");
        this.isFullSync = z;
        this.userProfile = userProfile2;
        this.multiAlarmSettings = list;
        this.complicationAppMappingSettings = complicationAppMappingSettings2;
        this.watchAppMappingSettings = watchAppMappingSettings2;
        this.backgroundConfig = backgroundConfig2;
        this.notificationFilterSettings = appNotificationFilterSettings;
        this.localizationData = localizationData2;
        this.microAppMappings = list2;
        this.secondTimezoneOffset = i;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final void doNextState() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_DEVICE_CONFIG_STATE));
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, failed because the current sub state is not an instance of SetWatchParamsState");
        }
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final ComplicationAppMappingSettings getComplicationAppMappingSettings() {
        return this.complicationAppMappingSettings;
    }

    @DexIgnore
    public final LocalizationData getLocalizationData() {
        return this.localizationData;
    }

    @DexIgnore
    public final List<MicroAppMapping> getMicroAppMappings() {
        return this.microAppMappings;
    }

    @DexIgnore
    public final List<AlarmSetting> getMultiAlarmSettings() {
        return this.multiAlarmSettings;
    }

    @DexIgnore
    public final AppNotificationFilterSettings getNotificationFilterSettings() {
        return this.notificationFilterSettings;
    }

    @DexIgnore
    public final int getSecondTimezoneOffset() {
        return this.secondTimezoneOffset;
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public final WatchAppMappingSettings getWatchAppMappingSettings() {
        return this.watchAppMappingSettings;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.SET_WATCH_PARAMS;
        String name = SetWatchParamsState.class.getName();
        ee7.a((Object) name, "SetWatchParamsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.SET_DEVICE_CONFIG_STATE;
        String name2 = SetDeviceConfigState.class.getName();
        ee7.a((Object) name2, "SetDeviceConfigState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.SET_LIST_ALARMS_STATE;
        String name3 = SetListAlarmsState.class.getName();
        ee7.a((Object) name3, "SetListAlarmsState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<SubFlow.SessionState, String> sessionStateMap4 = getSessionStateMap();
        SubFlow.SessionState sessionState4 = SubFlow.SessionState.SET_WATCH_APP_FILES;
        String name4 = SetWatchAppFiles.class.getName();
        ee7.a((Object) name4, "SetWatchAppFiles::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<SubFlow.SessionState, String> sessionStateMap5 = getSessionStateMap();
        SubFlow.SessionState sessionState5 = SubFlow.SessionState.SET_COMPLICATIONS_STATE;
        String name5 = SetComplications.class.getName();
        ee7.a((Object) name5, "SetComplications::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<SubFlow.SessionState, String> sessionStateMap6 = getSessionStateMap();
        SubFlow.SessionState sessionState6 = SubFlow.SessionState.SET_WATCH_APPS_STATE;
        String name6 = SetWatchApps.class.getName();
        ee7.a((Object) name6, "SetWatchApps::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<SubFlow.SessionState, String> sessionStateMap7 = getSessionStateMap();
        SubFlow.SessionState sessionState7 = SubFlow.SessionState.SET_BACKGROUND_IMAGE_CONFIG_STATE;
        String name7 = SetBackgroundImageConfig.class.getName();
        ee7.a((Object) name7, "SetBackgroundImageConfig::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<SubFlow.SessionState, String> sessionStateMap8 = getSessionStateMap();
        SubFlow.SessionState sessionState8 = SubFlow.SessionState.SET_LOCALIZATION_STATE;
        String name8 = SetLocalization.class.getName();
        ee7.a((Object) name8, "SetLocalization::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<SubFlow.SessionState, String> sessionStateMap9 = getSessionStateMap();
        SubFlow.SessionState sessionState9 = SubFlow.SessionState.SET_NOTIFICATION_FILTERS_STATE;
        String name9 = SetNotificationFilterSettings.class.getName();
        ee7.a((Object) name9, "SetNotificationFilterSettings::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
        HashMap<SubFlow.SessionState, String> sessionStateMap10 = getSessionStateMap();
        SubFlow.SessionState sessionState10 = SubFlow.SessionState.SET_MICRO_APP_MAPPING_AFTER_OTA_STATE;
        String name10 = SetMicroAppMappingState.class.getName();
        ee7.a((Object) name10, "SetMicroAppMappingState::class.java.name");
        sessionStateMap10.put(sessionState10, name10);
        HashMap<SubFlow.SessionState, String> sessionStateMap11 = getSessionStateMap();
        SubFlow.SessionState sessionState11 = SubFlow.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE;
        String name11 = SetReplyMessageMappingState.class.getName();
        ee7.a((Object) name11, "SetReplyMessageMappingState::class.java.name");
        sessionStateMap11.put(sessionState11, name11);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return this.isFullSync;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.SET_WATCH_PARAMS));
        return true;
    }

    @DexIgnore
    public final void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "getWatchParamFailed");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).onGetWatchParamsFail();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        ee7.b(str, "serial");
        ee7.b(watchParamsFileMapping, "watchParamsData");
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam...");
        if (getMCurrentState() instanceof SetWatchParamsState) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchParamsState) mCurrentState).setWatchParamToDevice(str, watchParamsFileMapping);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchParamsState");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, failed because the current sub state is not an instance of SetWatchParamsState");
    }

    @DexIgnore
    public final void setWatchAppFiles(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setWatchAppFiles, isSuccess = " + z);
        if (getMCurrentState() instanceof SetWatchAppFiles) {
            BleStateAbs mCurrentState = getMCurrentState();
            if (mCurrentState != null) {
                ((SetWatchAppFiles) mCurrentState).resumeSetWatchAppFilesState(z);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow.SetWatchAppFiles");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }
}
