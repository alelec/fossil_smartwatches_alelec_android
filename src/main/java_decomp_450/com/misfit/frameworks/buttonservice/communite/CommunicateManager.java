package com.misfit.frameworks.buttonservice.communite;

import android.content.Context;
import com.fossil.ce7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.gd7;
import com.fossil.te7;
import com.fossil.uf7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommunicateManager {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, BleCommunicator> bleCommunicators;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public BleCommunicator currentCommunicator;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion extends SingletonHolder<CommunicateManager, Context> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class Anon1 extends ce7 implements gd7<Context, CommunicateManager> {
            @DexIgnore
            public static /* final */ Anon1 INSTANCE; // = new Anon1();

            @DexIgnore
            public Anon1() {
                super(1);
            }

            @DexIgnore
            @Override // com.fossil.sf7, com.fossil.vd7
            public final String getName() {
                return "<init>";
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public final uf7 getOwner() {
                return te7.a(CommunicateManager.class);
            }

            @DexIgnore
            @Override // com.fossil.vd7
            public final String getSignature() {
                return "<init>(Landroid/content/Context;)V";
            }

            @DexIgnore
            public final CommunicateManager invoke(Context context) {
                ee7.b(context, "p1");
                return new CommunicateManager(context, null);
            }
        }

        @DexIgnore
        public Companion() {
            super(Anon1.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String simpleName = CommunicateManager.class.getSimpleName();
        ee7.a((Object) simpleName, "CommunicateManager::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CommunicateManager(Context context2) {
        this.context = context2;
        this.bleCommunicators = new ConcurrentHashMap<>();
    }

    @DexIgnore
    private final BleCommunicator createDeviceCommunicator(Context context2, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        return new DeviceCommunicator(context2, str, str2, communicationResultCallback);
    }

    @DexIgnore
    public final void clearCommunicatorSessionQueue(String str) {
        ee7.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator != null) {
            bleCommunicator.clearSessionQueue();
        }
    }

    @DexIgnore
    public final void clearCurrentCommunicatorSessionQueueIfNot(String str) {
        ee7.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        BleCommunicator bleCommunicator2 = this.currentCommunicator;
        if (bleCommunicator != bleCommunicator2 && bleCommunicator2 != null) {
            if (bleCommunicator2 != null) {
                bleCommunicator2.clearSessionQueue();
            } else {
                ee7.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int getBatteryLevel(String str) {
        ee7.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return -1;
        }
        ee7.a((Object) bleCommunicator, "bleCommunicators[serial] ?: return -1");
        return bleCommunicator.getBleAdapter().getBatteryLevel();
    }

    @DexIgnore
    public final ConcurrentHashMap<String, BleCommunicator> getBleCommunicators() {
        return this.bleCommunicators;
    }

    @DexIgnore
    public final synchronized BleCommunicator getCommunicator(String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        BleCommunicator bleCommunicator;
        ee7.b(str, "serial");
        ee7.b(str2, "macAddress");
        ee7.b(communicationResultCallback, "communicationResultCallback");
        bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            bleCommunicator = createDeviceCommunicator(this.context, str, str2, communicationResultCallback);
            this.bleCommunicators.put(str, bleCommunicator);
        }
        this.currentCommunicator = bleCommunicator;
        return bleCommunicator;
    }

    @DexIgnore
    public final String getLocale(String str) {
        ee7.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return "";
        }
        ee7.a((Object) bleCommunicator, "bleCommunicators[serial] ?: return \"\"");
        return bleCommunicator.getBleAdapter().getLocale();
    }

    @DexIgnore
    public final List<BleCommunicator> getRunningCommunicator() {
        Collection<BleCommunicator> values = this.bleCommunicators.values();
        ee7.a((Object) values, "bleCommunicators.values");
        ArrayList arrayList = new ArrayList();
        for (T t : values) {
            if (t.isRunning()) {
                arrayList.add(t);
            }
        }
        return ea7.d((Collection) arrayList);
    }

    @DexIgnore
    public final void removeCommunicator(String str) {
        ee7.b(str, "serial");
        this.bleCommunicators.remove(str);
    }

    @DexIgnore
    public /* synthetic */ CommunicateManager(Context context2, zd7 zd7) {
        this(context2);
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        ee7.b(str, "serial");
        ee7.b(communicationResultCallback, "communicationResultCallback");
        String macAddress = DeviceUtils.getInstance(this.context).getMacAddress(this.context, str);
        ee7.a((Object) macAddress, "DeviceUtils.getInstance(\u2026cAddress(context, serial)");
        return getCommunicator(str, macAddress, communicationResultCallback);
    }
}
