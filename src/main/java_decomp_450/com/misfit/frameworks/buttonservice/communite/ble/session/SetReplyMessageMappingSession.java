package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.he0;
import com.fossil.i97;
import com.fossil.ke0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetReplyMessageMappingSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ ReplyMessageMappingGroup mReplyMessageMappingList;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetReplyMessageMappingState extends BleStateAbs {
        @DexIgnore
        public he0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetReplyMessageMappingState() {
            super(SetReplyMessageMappingSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            he0<i97> replyMessageMapping = SetReplyMessageMappingSession.this.getBleAdapter().setReplyMessageMapping(SetReplyMessageMappingSession.this.getLogSession(), SetReplyMessageMappingSession.this.mReplyMessageMappingList, this);
            this.task = replyMessageMapping;
            if (replyMessageMapping == null) {
                SetReplyMessageMappingSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingError(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetReplyMessageMappingSession.this.getContext(), SetReplyMessageMappingSession.this.getSerial())) {
                SetReplyMessageMappingSession.this.log("Reach the limit retry. Stop.");
                SetReplyMessageMappingSession.this.stop(FailureCode.FAILED_TO_REPLY_MESSAGE_MAPPING);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetReplyMessageMappingSuccess() {
            stopTimeout();
            SetReplyMessageMappingSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            he0<i97> he0 = this.task;
            if (he0 != null) {
                he0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetReplyMessageMappingSession(ReplyMessageMappingGroup replyMessageMappingGroup, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_REPLY_MESSAGE_MAPPING, bleAdapterImpl, bleSessionCallback);
        ee7.b(replyMessageMappingGroup, "mReplyMessageMappingList");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mReplyMessageMappingList = replyMessageMappingGroup;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_REPLY_MESSAGE_MAPPING) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetReplyMessageMappingSession setReplyMessageMappingSession = new SetReplyMessageMappingSession(this.mReplyMessageMappingList, getBleAdapter(), getBleSessionCallback());
        setReplyMessageMappingSession.setDevice(getDevice());
        return setReplyMessageMappingSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_REPLY_MESSAGE_MAPPING_STATE;
        String name = SetReplyMessageMappingState.class.getName();
        ee7.a((Object) name, "SetReplyMessageMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
