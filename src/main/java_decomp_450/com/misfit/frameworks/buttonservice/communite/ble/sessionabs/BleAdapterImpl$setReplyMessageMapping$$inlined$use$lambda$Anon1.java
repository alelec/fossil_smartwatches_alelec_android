package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.ee7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.se7;
import com.fossil.vg0;
import com.fossil.yd0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1 extends fe7 implements gd7<i97, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ yd0 $replyMessageFeature$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ReplyMessageMappingGroup $replyMessageGroup$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ se7 $replyMessageIcon$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ vg0[] $replyMessageMapping$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1(vg0[] vg0Arr, BleAdapterImpl bleAdapterImpl, se7 se7, ReplyMessageMappingGroup replyMessageMappingGroup, yd0 yd0, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.$replyMessageMapping$inlined = vg0Arr;
        this.this$0 = bleAdapterImpl;
        this.$replyMessageIcon$inlined = se7;
        this.$replyMessageGroup$inlined = replyMessageMappingGroup;
        this.$replyMessageFeature$inlined = yd0;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(i97 i97) {
        invoke(i97);
        return i97.a;
    }

    @DexIgnore
    public final void invoke(i97 i97) {
        ee7.b(i97, "it");
        this.this$0.log(this.$logSession$inlined, "setReplyMessageMapping Success");
        this.$callback$inlined.onSetReplyMessageMappingSuccess();
    }
}
