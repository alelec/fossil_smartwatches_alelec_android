package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.c80;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1 extends fe7 implements gd7<Float, i97> {
    @DexIgnore
    public /* final */ /* synthetic */ c80 $biometricProfile$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$readDataFile$$inlined$let$lambda$Anon1(BleAdapterImpl bleAdapterImpl, c80 c80, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.this$0 = bleAdapterImpl;
        this.$biometricProfile$inlined = c80;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.fossil.gd7
    public /* bridge */ /* synthetic */ i97 invoke(Float f) {
        invoke(f.floatValue());
        return i97.a;
    }

    @DexIgnore
    public final void invoke(float f) {
        BleAdapterImpl bleAdapterImpl = this.this$0;
        FLogger.Session session = this.$logSession$inlined;
        bleAdapterImpl.log(session, "Read Data Files: progress=" + f);
        this.$callback$inlined.onReadDataFilesProgressChanged(f);
    }
}
