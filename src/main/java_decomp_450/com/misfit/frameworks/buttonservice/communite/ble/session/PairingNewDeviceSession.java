package com.misfit.frameworks.buttonservice.communite.ble.session;

import android.os.Bundle;
import com.fossil.ee7;
import com.fossil.eg0;
import com.fossil.he0;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.mh7;
import com.fossil.p60;
import com.fossil.r60;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseExchangeSecretKeySubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseOTASubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.SkipFirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.pairing.LabelResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.Constants;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.FirmwareUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingNewDeviceSession extends BasePairingNewDeviceSession implements IPairDeviceSession, IExchangeKeySession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class AuthorizeDeviceState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public AuthorizeDeviceState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final void authorizeDevice(long j) {
            BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                access$getBleSessionCallback$p.onNeedStartTimer(PairingNewDeviceSession.this.getSerial());
            }
            ie0<i97> confirmAuthorization = PairingNewDeviceSession.this.getBleAdapter().confirmAuthorization(PairingNewDeviceSession.this.getLogSession(), j, this);
            this.task = confirmAuthorization;
            if (confirmAuthorization == null) {
                PairingNewDeviceSession.this.stop(10000);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void cancelCurrentBleTask() {
            super.cancelCurrentBleTask();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }

        @DexIgnore
        public final ie0<i97> getTask() {
            return this.task;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthorizeDeviceFailed() {
            FLogger.INSTANCE.getLocal().e(getTAG(), "onAuthorizeDeviceState() failed");
            PairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_AUTHORIZE_DEVICE);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthorizeDeviceSuccess() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onAuthorizeDeviceState() success");
            BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p != null) {
                access$getBleSessionCallback$p.onAuthorizeDeviceSuccess(PairingNewDeviceSession.this.getSerial());
            }
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
        }

        @DexIgnore
        public final void setTask(ie0<i97> ie0) {
            this.task = ie0;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class CloseConnectionState extends BleStateAbs {
        @DexIgnore
        public int failureCode;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public CloseConnectionState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final int getFailureCode() {
            return this.failureCode;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            PairingNewDeviceSession.this.getBleAdapter().closeConnection(PairingNewDeviceSession.this.getLogSession(), true);
            PairingNewDeviceSession.this.stop(this.failureCode);
            return true;
        }

        @DexIgnore
        public final void setFailureCode(int i) {
            this.failureCode = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySubFlow extends BaseExchangeSecretKeySubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ExchangeSecretKeySubFlow() {
            super(CommunicateMode.LINK, PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i == p60.REQUEST_UNSUPPORTED.getCode() || i == p60.UNSUPPORTED_FORMAT.getCode() || i == 0) {
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.SET_LABEL_STATE));
                return;
            }
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (access$createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) access$createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class LinkServerState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public LinkServerState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
            if (access$getBleSessionCallback$p == null) {
                return true;
            }
            CommunicateMode communicateMode = CommunicateMode.LINK;
            Bundle bundle = new Bundle();
            bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(PairingNewDeviceSession.this.getBleAdapter()));
            bundle.putBoolean(Constants.IS_JUST_OTA, PairingNewDeviceSession.this.isJustUpdateFW());
            access$getBleSessionCallback$p.onAskForLinkServer(communicateMode, bundle);
            return true;
        }

        @DexIgnore
        public final void onLinkServerCompleted(boolean z, int i) {
            if (z) {
                PairingNewDeviceSession.this.stop(0);
                return;
            }
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE);
            if (access$createConcreteState instanceof CloseConnectionState) {
                ((CloseConnectionState) access$createConcreteState).setFailureCode(i);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class OTASubFlow extends BaseOTASubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public OTASubFlow() {
            /*
                r11 = this;
                com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.this = r12
                java.lang.String r1 = r12.getTAG()
                com.misfit.frameworks.buttonservice.log.MFLog r3 = r12.getMfLog()
                com.misfit.frameworks.buttonservice.log.FLogger$Session r4 = r12.getLogSession()
                java.lang.String r5 = r12.getSerial()
                com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl r6 = r12.getBleAdapter()
                com.misfit.frameworks.buttonservice.model.FirmwareData r7 = r12.getFirmwareData()
                r0 = 0
                if (r7 == 0) goto L_0x0035
                byte[] r8 = r12.getFirmwareBytes()
                if (r8 == 0) goto L_0x0031
                com.misfit.frameworks.buttonservice.communite.ble.BleSession$BleSessionCallback r9 = r12.getBleSessionCallback()
                com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator$CommunicationResultCallback r10 = r12.getCommunicationResultCallback()
                r0 = r11
                r2 = r12
                r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10)
                return
            L_0x0031:
                com.fossil.ee7.a()
                throw r0
            L_0x0035:
                com.fossil.ee7.a()
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.OTASubFlow.<init>(com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession):void");
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i == 0) {
                PairingNewDeviceSession.this.setJustUpdateFW(true);
                BleSession.BleSessionCallback bleSessionCallback = getBleSessionCallback();
                if (bleSessionCallback != null) {
                    bleSessionCallback.onUpdateFirmwareSuccess();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.AUTHORIZE_DEVICE));
                return;
            }
            BleSession.BleSessionCallback bleSessionCallback2 = getBleSessionCallback();
            if (bleSessionCallback2 != null) {
                bleSessionCallback2.onUpdateFirmwareFailed();
            }
            addFailureCode(FailureCode.FAILED_TO_OTA);
            BleState access$createConcreteState = PairingNewDeviceSession.this.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE);
            if (access$createConcreteState instanceof PairingCheckFirmware) {
                ((PairingCheckFirmware) access$createConcreteState).setFromOTAFailed(true);
            }
            PairingNewDeviceSession.this.enterStateAsync(access$createConcreteState);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PairingCheckFirmware extends BleStateAbs {
        @DexIgnore
        public boolean isFromOTAFailed;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public PairingCheckFirmware() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        public final boolean isFromOTAFailed() {
            return this.isFromOTAFailed;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            BleSession.BleSessionCallback access$getBleSessionCallback$p;
            super.onEnter();
            if (this.isFromOTAFailed || (access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback()) == null) {
                return true;
            }
            Bundle bundle = new Bundle();
            bundle.putString("device_model", PairingNewDeviceSession.this.getBleAdapter().getDeviceModel());
            access$getBleSessionCallback$p.onRequestLatestFirmware(bundle);
            return true;
        }

        @DexIgnore
        public final void onReceiveLatestFirmwareData(FirmwareData firmwareData) {
            ee7.b(firmwareData, "firmwareData");
            if ((firmwareData instanceof SkipFirmwareData) || mh7.b(PairingNewDeviceSession.this.getBleAdapter().getFirmwareVersion(), firmwareData.getFirmwareVersion(), true)) {
                BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
                if (access$getBleSessionCallback$p != null) {
                    access$getBleSessionCallback$p.onFirmwareLatest();
                }
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.AUTHORIZE_DEVICE));
                return;
            }
            String firmwareVersion = firmwareData.getFirmwareVersion();
            String checkSum = firmwareData.getCheckSum();
            boolean isEmbedded = firmwareData.isEmbedded();
            byte[] readFirmware = FirmwareUtils.INSTANCE.readFirmware(firmwareData, PairingNewDeviceSession.this.getContext());
            PairingNewDeviceSession.this.log("Verifying firmware...");
            PairingNewDeviceSession pairingNewDeviceSession2 = PairingNewDeviceSession.this;
            pairingNewDeviceSession2.log("- Version: " + firmwareVersion);
            PairingNewDeviceSession pairingNewDeviceSession3 = PairingNewDeviceSession.this;
            pairingNewDeviceSession3.log("- Checksum: " + checkSum);
            PairingNewDeviceSession pairingNewDeviceSession4 = PairingNewDeviceSession.this;
            pairingNewDeviceSession4.log("- Length: " + readFirmware.length);
            PairingNewDeviceSession pairingNewDeviceSession5 = PairingNewDeviceSession.this;
            pairingNewDeviceSession5.log("- In-app Bundled: " + isEmbedded);
            if (isEmbedded) {
                PairingNewDeviceSession.this.log("In-app bundled firmware, skip verifying!");
            } else if (FirmwareUtils.INSTANCE.verifyFirmware(readFirmware, checkSum)) {
                PairingNewDeviceSession.this.log("Verified: OK");
            } else {
                PairingNewDeviceSession.this.log("Verified: FAILED. OTA: FAILED");
                PairingNewDeviceSession.this.stop(FailureCode.FAILED_TO_OTA_FILE_NOT_READY);
            }
            PairingNewDeviceSession.this.setFirmwareData(firmwareData);
            PairingNewDeviceSession.this.setFirmwareBytes(readFirmware);
            PairingNewDeviceSession pairingNewDeviceSession6 = PairingNewDeviceSession.this;
            pairingNewDeviceSession6.enterStateAsync(pairingNewDeviceSession6.createConcreteState(BleSessionAbs.SessionState.OTA_STATE));
        }

        @DexIgnore
        public final void setFromOTAFailed(boolean z) {
            this.isFromOTAFailed = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLabelState extends BleStateAbs {
        @DexIgnore
        public he0<r60> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetLabelState() {
            super(PairingNewDeviceSession.this.getTAG());
        }

        @DexIgnore
        private final void closeSession(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "closeSession, errorCode = " + i);
            PairingNewDeviceSession.this.stop(i);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0042, code lost:
            com.fossil.hc7.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x0045, code lost:
            throw r2;
         */
        @DexIgnore
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private final com.fossil.eg0 getLabelFile() {
            /*
                r4 = this;
                com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession r0 = com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.this
                android.content.Context r0 = r0.getContext()
                com.misfit.frameworks.buttonservice.model.FileType r1 = com.misfit.frameworks.buttonservice.model.FileType.LABEL
                java.lang.String r0 = com.misfit.frameworks.buttonservice.utils.FileUtils.getDirectory(r0, r1)
                java.io.File r1 = new java.io.File
                r1.<init>(r0)
                java.io.File[] r0 = r1.listFiles()
                r1 = 1
                r2 = 0
                if (r0 == 0) goto L_0x0023
                int r3 = r0.length
                if (r3 != 0) goto L_0x001e
                r3 = 1
                goto L_0x001f
            L_0x001e:
                r3 = 0
            L_0x001f:
                if (r3 == 0) goto L_0x0022
                goto L_0x0023
            L_0x0022:
                r1 = 0
            L_0x0023:
                r3 = 0
                if (r1 != 0) goto L_0x0046
                r0 = r0[r2]
                java.lang.String r1 = "files[0]"
                com.fossil.ee7.a(r0, r1)
                java.io.FileInputStream r1 = new java.io.FileInputStream
                r1.<init>(r0)
                com.fossil.eg0 r0 = new com.fossil.eg0     // Catch:{ all -> 0x003f }
                byte[] r2 = com.fossil.gc7.a(r1)     // Catch:{ all -> 0x003f }
                r0.<init>(r2)     // Catch:{ all -> 0x003f }
                com.fossil.hc7.a(r1, r3)
                return r0
            L_0x003f:
                r0 = move-exception
                throw r0     // Catch:{ all -> 0x0041 }
            L_0x0041:
                r2 = move-exception
                com.fossil.hc7.a(r1, r0)
                throw r2
            L_0x0046:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.SetLabelState.getLabelFile():com.fossil.eg0");
        }

        @DexIgnore
        private final void skipStepForNotSupportedDevice() {
            FLogger.INSTANCE.getLocal().d(getTAG(), "skipStepForNotSupportedDevice, FEATURE_IS_NOT_SUPPORTED");
            PairingNewDeviceSession.this.addFailureCode(10000);
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            if (PairingNewDeviceSession.this.getBleSessionCallback() != null) {
                BleSession.BleSessionCallback access$getBleSessionCallback$p = PairingNewDeviceSession.this.getBleSessionCallback();
                if (access$getBleSessionCallback$p != null) {
                    access$getBleSessionCallback$p.onAskForLabelFile(PairingNewDeviceSession.this.getSerial(), CommunicateMode.LINK);
                    startTimeout();
                    return true;
                }
                ee7.a();
                throw null;
            }
            closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLabelFileFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.log("onSetLabelFileFailed, error = " + ke0);
            stopTimeout();
            if (ke0 == p60.REQUEST_UNSUPPORTED || ke0 == p60.UNSUPPORTED_FORMAT) {
                skipStepForNotSupportedDevice();
            } else if (!retry(PairingNewDeviceSession.this.getContext(), PairingNewDeviceSession.this.getSerial())) {
                PairingNewDeviceSession.this.log("Reach the limit retry. Stop.");
                closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetLabelFileSuccess() {
            stopTimeout();
            PairingNewDeviceSession.this.log("onSetLabelFileSuccess");
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            PairingNewDeviceSession.this.log("onSetLabelFile Timeout");
            he0<r60> he0 = this.task;
            if (he0 == null) {
                closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
            } else if (he0 != null) {
                he0.e();
            } else {
                ee7.a();
                throw null;
            }
        }

        @DexIgnore
        public final void setLabelToDevice(LabelResponse labelResponse) {
            ee7.b(labelResponse, "response");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "setLabelToDevice, response = " + labelResponse + " for currentVersion = " + PairingNewDeviceSession.this.getBleAdapter().getCurrentLabelVersion());
            int code = labelResponse.getCode();
            if (code != 0) {
                if (code != 10000) {
                    closeSession(labelResponse.getCode());
                } else {
                    skipStepForNotSupportedDevice();
                }
            } else if (ee7.a((Object) labelResponse.getVersion(), (Object) "0.0") || (!ee7.a((Object) labelResponse.getVersion(), (Object) PairingNewDeviceSession.this.getBleAdapter().getCurrentLabelVersion()))) {
                eg0 labelFile = getLabelFile();
                if (labelFile != null) {
                    FLogger.INSTANCE.getLocal().d(getTAG(), "setLabelToDevice, start install label");
                    he0<r60> labelFile2 = PairingNewDeviceSession.this.getBleAdapter().setLabelFile(PairingNewDeviceSession.this.getLogSession(), labelFile, this);
                    this.task = labelFile2;
                    if (labelFile2 == null) {
                        skipStepForNotSupportedDevice();
                    } else {
                        startTimeout();
                    }
                } else {
                    closeSession(FailureCode.FAILED_TO_SET_LABEL_FILE);
                }
            } else {
                FLogger.INSTANCE.getLocal().d(getTAG(), "setLabelToDevice, label is up-to-date, skip this step");
                PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
                pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferDataSubFlow() {
            super(PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getUserProfile(), PairingNewDeviceSession.this.getBleSessionCallback(), true);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            if (i != 0) {
                PairingNewDeviceSession.this.stop(i);
                return;
            }
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TransferSettingsSubFlow() {
            super(PairingNewDeviceSession.this.getTAG(), PairingNewDeviceSession.this, PairingNewDeviceSession.this.getMfLog(), PairingNewDeviceSession.this.getLogSession(), true, PairingNewDeviceSession.this.getSerial(), PairingNewDeviceSession.this.getBleAdapter(), PairingNewDeviceSession.this.getUserProfile(), PairingNewDeviceSession.this.multiAlarmSettings, PairingNewDeviceSession.this.complicationAppMappingSettings, PairingNewDeviceSession.this.watchAppMappingSettings, PairingNewDeviceSession.this.backgroundConfig, PairingNewDeviceSession.this.notificationFilterSettings, PairingNewDeviceSession.this.localizationData, PairingNewDeviceSession.this.microAppMappings, PairingNewDeviceSession.this.secondTimezoneOffset, PairingNewDeviceSession.this.getBleSessionCallback());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
        public void onStop(int i) {
            PairingNewDeviceSession pairingNewDeviceSession = PairingNewDeviceSession.this;
            pairingNewDeviceSession.enterStateAsync(pairingNewDeviceSession.createConcreteState(BleSessionAbs.SessionState.LINK_SERVER));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public PairingNewDeviceSession(UserProfile userProfile, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        super(userProfile, bleAdapterImpl, bleSessionCallback, communicationResultCallback);
        ee7.b(userProfile, "userProfile");
        ee7.b(bleAdapterImpl, "bleAdapterV2");
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.PAIR);
        userProfile.setNewDevice(true);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        PairingNewDeviceSession pairingNewDeviceSession = new PairingNewDeviceSession(getUserProfile(), getBleAdapter(), getBleSessionCallback(), getCommunicationResultCallback());
        pairingNewDeviceSession.setDevice(getDevice());
        return pairingNewDeviceSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BasePairingNewDeviceSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.EXCHANGE_SECRET_KEY_SUB_FLOW;
        String name = ExchangeSecretKeySubFlow.class.getName();
        ee7.a((Object) name, "ExchangeSecretKeySubFlow::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_LABEL_STATE;
        String name2 = SetLabelState.class.getName();
        ee7.a((Object) name2, "SetLabelState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.PAIRING_CHECK_FIRMWARE;
        String name3 = PairingCheckFirmware.class.getName();
        ee7.a((Object) name3, "PairingCheckFirmware::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.OTA_STATE;
        String name4 = OTASubFlow.class.getName();
        ee7.a((Object) name4, "OTASubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.AUTHORIZE_DEVICE;
        String name5 = AuthorizeDeviceState.class.getName();
        ee7.a((Object) name5, "AuthorizeDeviceState::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap6 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState6 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name6 = TransferDataSubFlow.class.getName();
        ee7.a((Object) name6, "TransferDataSubFlow::class.java.name");
        sessionStateMap6.put(sessionState6, name6);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap7 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState7 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name7 = TransferSettingsSubFlow.class.getName();
        ee7.a((Object) name7, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap7.put(sessionState7, name7);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap8 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState8 = BleSessionAbs.SessionState.LINK_SERVER;
        String name8 = LinkServerState.class.getName();
        ee7.a((Object) name8, "LinkServerState::class.java.name");
        sessionStateMap8.put(sessionState8, name8);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap9 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState9 = BleSessionAbs.SessionState.CLOSE_CONNECTION_STATE;
        String name9 = CloseConnectionState.class.getName();
        ee7.a((Object) name9, "CloseConnectionState::class.java.name");
        sessionStateMap9.put(sessionState9, name9);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onAuthorizeDevice(long j) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onAuthorizeDevice(), timeout=" + j);
        BleState currentState = getCurrentState();
        if (currentState instanceof AuthorizeDeviceState) {
            ((AuthorizeDeviceState) currentState).authorizeDevice(j);
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onAuthorizeDevice() failed, can't execute because currentState is not an instance of AuthorizeDeviceState");
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onDownloadLabelResponse(LabelResponse labelResponse) {
        ee7.b(labelResponse, "response");
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onDownloadLabelResponse response=" + labelResponse + " state=" + currentState);
        if (currentState instanceof SetLabelState) {
            ((SetLabelState) currentState).setLabelToDevice(labelResponse);
        } else {
            FLogger.INSTANCE.getLocal().d(getTAG(), "onDownloadLabelResponse, can't set Label because currentState is not an instance of SetLabelState");
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).onGetWatchParamFailed();
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void onLinkServerSuccess(boolean z, int i) {
        BleState currentState = getCurrentState();
        if (currentState instanceof LinkServerState) {
            ((LinkServerState) currentState).onLinkServerCompleted(z, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof ExchangeSecretKeySubFlow) {
            ((ExchangeSecretKeySubFlow) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchAppFileSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void onWatchAppFilesReady(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onWatchAppFilesReady, isSuccess = " + z);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setWatchAppFiles(z);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setWatchAppFiles FAILED. It is not in SetWatchAppFilesSession");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.ISetWatchParamStateSession, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        ee7.b(str, "serial");
        ee7.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.PairingNewDeviceSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.IPairDeviceSession
    public void updateFirmware(FirmwareData firmwareData) {
        ee7.b(firmwareData, "firmwareData");
        BleState currentState = getCurrentState();
        if (currentState instanceof PairingCheckFirmware) {
            ((PairingCheckFirmware) currentState).onReceiveLatestFirmwareData(firmwareData);
        }
    }
}
