package com.misfit.frameworks.buttonservice.communite.ble;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.fossil.ee7;
import com.fossil.fitness.FitnessData;
import com.fossil.i97;
import com.fossil.od0;
import com.fossil.sd0;
import com.fossil.vd0;
import com.fossil.x87;
import com.fossil.xd0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.LabelResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingAuthorizeResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingLinkServerResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.pairing.PairingUpdateFWResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleCommunicator {
    @DexIgnore
    public /* final */ int NOTIFICATION_THRESHOLD; // = 20;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public BleSession.BleSessionCallback bleSessionCallback; // = new BleCommunicator$bleSessionCallback$Anon1(this);
    @DexIgnore
    public /* final */ CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ HandlerThread handlerThread;
    @DexIgnore
    public PriorityBlockingQueue<BleSession> highSessionQueue;
    @DexIgnore
    public PriorityBlockingQueue<BleSession> lowSessionQueue;
    @DexIgnore
    public /* final */ ArrayList<DianaNotificationObj> mNotificationQueue; // = new ArrayList<>();
    @DexIgnore
    public /* final */ String serial;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo((Enum) bleSession.getSessionType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2<T> implements Comparator<BleSession> {
        @DexIgnore
        public static /* final */ Anon2 INSTANCE; // = new Anon2();

        @DexIgnore
        public final int compare(BleSession bleSession, BleSession bleSession2) {
            return bleSession2.getSessionType().compareTo((Enum) bleSession.getSessionType());
        }
    }

    @DexIgnore
    public interface CommunicationResultCallback {
        @DexIgnore
        void onAskForCurrentSecretKey(String str);

        @DexIgnore
        void onAskForLabelFile(String str, CommunicateMode communicateMode);

        @DexIgnore
        void onAskForLinkServer(String str, CommunicateMode communicateMode, Bundle bundle);

        @DexIgnore
        void onAskForRandomKey(String str);

        @DexIgnore
        void onAskForServerSecretKey(String str, Bundle bundle);

        @DexIgnore
        void onAskForStopWorkout(String str);

        @DexIgnore
        void onAskForWatchAppFiles(String str, Bundle bundle);

        @DexIgnore
        void onAuthorizeDeviceSuccess(String str);

        @DexIgnore
        void onCommunicatorResult(CommunicateMode communicateMode, String str, int i, List<Integer> list, Bundle bundle);

        @DexIgnore
        void onDeviceAppsRequest(int i, Bundle bundle, String str);

        @DexIgnore
        void onExchangeSecretKeySuccess(String str, String str2);

        @DexIgnore
        void onFirmwareLatest(String str);

        @DexIgnore
        void onGattConnectionStateChanged(String str, int i);

        @DexIgnore
        void onHeartBeatDataReceived(int i, int i2, String str);

        @DexIgnore
        void onHeartRateNotification(short s, String str);

        @DexIgnore
        void onHidConnectionStateChanged(String str, int i);

        @DexIgnore
        void onNeedStartTimer(String str);

        @DexIgnore
        void onNotificationSent(int i, boolean z);

        @DexIgnore
        void onOtaProgressUpdated(String str, float f);

        @DexIgnore
        void onPreparationCompleted(boolean z, String str);

        @DexIgnore
        void onReadCurrentWorkoutSuccess(String str, Bundle bundle);

        @DexIgnore
        void onReceivedSyncData(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestFirmware(String str, Bundle bundle);

        @DexIgnore
        void onRequestLatestWatchParams(String str, Bundle bundle);

        @DexIgnore
        void onRequestPushSecretKeyToServer(String str, String str2);

        @DexIgnore
        void onUpdateFirmwareFailed(String str);

        @DexIgnore
        void onUpdateFirmwareSuccess(String str);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[SessionType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[SessionType.SPECIAL.ordinal()] = 1;
            $EnumSwitchMapping$0[SessionType.SYNC.ordinal()] = 2;
            $EnumSwitchMapping$0[SessionType.CONNECT.ordinal()] = 3;
            $EnumSwitchMapping$0[SessionType.UI.ordinal()] = 4;
            $EnumSwitchMapping$0[SessionType.DEVICE_SETTING.ordinal()] = 5;
            $EnumSwitchMapping$0[SessionType.URGENT.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public BleCommunicator(String str, CommunicationResultCallback communicationResultCallback2) {
        ee7.b(str, "serial");
        ee7.b(communicationResultCallback2, "communicationResultCallback");
        this.serial = str;
        this.communicationResultCallback = communicationResultCallback2;
        String simpleName = BleCommunicator.class.getSimpleName();
        ee7.a((Object) simpleName, "BleCommunicator::class.java.simpleName");
        this.TAG = simpleName;
        HandlerThread handlerThread2 = new HandlerThread(this.TAG);
        this.handlerThread = handlerThread2;
        handlerThread2.start();
        this.highSessionQueue = new PriorityBlockingQueue<>(11, Anon1.INSTANCE);
        this.lowSessionQueue = new PriorityBlockingQueue<>(11, Anon2.INSTANCE);
    }

    @DexIgnore
    public static /* synthetic */ boolean startConnectionDeviceSession$default(BleCommunicator bleCommunicator, boolean z, UserProfile userProfile, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                userProfile = null;
            }
            return bleCommunicator.startConnectionDeviceSession(z, userProfile);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startConnectionDeviceSession");
    }

    @DexIgnore
    public static /* synthetic */ void startSendDeviceAppResponse$default(BleCommunicator bleCommunicator, DeviceAppResponse deviceAppResponse, boolean z, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                z = false;
            }
            bleCommunicator.startSendDeviceAppResponse(deviceAppResponse, z);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: startSendDeviceAppResponse");
    }

    @DexIgnore
    public final synchronized void cancelCalibrationSession() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".cancelCalibrationSession() - current session=" + getCurrentSession());
        if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof ICalibrationSession)) {
            BleSession currentSession = getCurrentSession();
            if (currentSession != null) {
                ((ICalibrationSession) currentSession).handleReleaseHandControl();
            } else {
                throw new x87("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.ICalibrationSession");
            }
        }
        ArrayList arrayList = new ArrayList();
        printQueue();
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            BleSession next = it.next();
            if (next instanceof ICalibrationSession) {
                arrayList.add(next);
                next.stop(0);
            }
        }
        this.highSessionQueue.removeAll(arrayList);
    }

    @DexIgnore
    public final void cancelPairDevice() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".cancelPairDevice() - current session=" + getCurrentSession());
        if (!BleSession.Companion.isNull(getCurrentSession()) && (getCurrentSession() instanceof IPairDeviceSession)) {
            FLogger.INSTANCE.getRemote().d(FLogger.Component.BLE, FLogger.Session.PAIR, this.serial, this.TAG, "App called cancel Pair Device.");
            getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
        }
        ArrayList arrayList = new ArrayList();
        printQueue();
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            BleSession next = it.next();
            if (next instanceof IPairDeviceSession) {
                arrayList.add(next);
                next.stop(0);
            }
        }
        this.highSessionQueue.removeAll(arrayList);
    }

    @DexIgnore
    public void cleanUp() {
        DevicePreferenceUtils.clearAutoListAlarm(getBleAdapter().getContext());
        DevicePreferenceUtils.clearAutoSetMapping(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoComplicationAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoWatchAppSettings(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoBackgroundImageConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAutoNotificationFiltersConfig(getBleAdapter().getContext(), this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(getBleAdapter().getContext());
    }

    @DexIgnore
    public abstract void clearQuickCommandQueue();

    @DexIgnore
    public final synchronized void clearSessionQueue() {
        synchronized (this.lowSessionQueue) {
            synchronized (this.highSessionQueue) {
                Iterator<BleSession> it = this.highSessionQueue.iterator();
                while (it.hasNext()) {
                    it.next().stop(FailureCode.SESSION_INTERRUPTED);
                }
                this.highSessionQueue.clear();
                Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
                while (it2.hasNext()) {
                    it2.next().stop(FailureCode.SESSION_INTERRUPTED);
                }
                this.lowSessionQueue.clear();
                if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
                    interruptCurrentSession();
                }
                i97 i97 = i97.a;
            }
            i97 i972 = i97.a;
        }
    }

    @DexIgnore
    public final void closeConnection() {
        clearSessionQueue();
        if (getCurrentSession().getCommunicateMode() != CommunicateMode.UNLINK) {
            getBleAdapter().closeConnection(FLogger.Session.OTHER, true);
        }
    }

    @DexIgnore
    public abstract void confirmStopWorkout(String str, boolean z);

    @DexIgnore
    public final boolean containSyncMode() {
        CommunicateMode communicateMode = getCommunicateMode();
        if (communicateMode != CommunicateMode.SYNC && !this.highSessionQueue.isEmpty()) {
            Iterator<BleSession> it = this.highSessionQueue.iterator();
            while (true) {
                if (it.hasNext()) {
                    CommunicateMode communicateMode2 = it.next().getCommunicateMode();
                    CommunicateMode communicateMode3 = CommunicateMode.SYNC;
                    if (communicateMode2 == communicateMode3) {
                        communicateMode = communicateMode3;
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        return communicateMode == CommunicateMode.SYNC;
    }

    @DexIgnore
    public abstract boolean disableHeartRateNotification();

    @DexIgnore
    public abstract boolean enableHeartRateNotification();

    @DexIgnore
    public abstract BleAdapter getBleAdapter();

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return CommunicateMode.IDLE;
        }
        return getCurrentSession().getCommunicateMode();
    }

    @DexIgnore
    public final CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public abstract BleSession getCurrentSession();

    @DexIgnore
    public final String getCurrentSessionName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL";
        }
        String simpleName = getCurrentSession().getClass().getSimpleName();
        ee7.a((Object) simpleName, "currentSession.javaClass.simpleName");
        return simpleName;
    }

    @DexIgnore
    public final String getCurrentStateName() {
        if (BleSession.Companion.isNull(getCurrentSession())) {
            return "NULL Session";
        }
        if (BleState.Companion.isNull(getCurrentSession().getCurrentState())) {
            return "NULL State";
        }
        String name = getCurrentSession().getCurrentState().getClass().getName();
        ee7.a((Object) name, "currentSession.currentState.javaClass.name");
        return name;
    }

    @DexIgnore
    public final int getGattState() {
        return getBleAdapter().getGattState();
    }

    @DexIgnore
    public final HandlerThread getHandlerThread() {
        return this.handlerThread;
    }

    @DexIgnore
    public final int getHidState() {
        return getBleAdapter().getHidState();
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getHighSessionQueue() {
        return this.highSessionQueue;
    }

    @DexIgnore
    public final PriorityBlockingQueue<BleSession> getLowSessionQueue() {
        return this.lowSessionQueue;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public abstract List<FitnessData> getSyncData();

    @DexIgnore
    public final String getTAG() {
        return this.TAG;
    }

    @DexIgnore
    public final synchronized void interruptCurrentSession() {
        if (!BleSession.Companion.isNull(getCurrentSession()) && getCurrentSession().getSessionType() != SessionType.CONNECT_WITHOUT_TIMEOUT) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.d(str, "Inside " + this.TAG + ".interruptCurrentSession - currentSession=" + getCurrentSession());
            getCurrentSession().stop(FailureCode.SESSION_INTERRUPTED);
            setNullCurrentSession();
        }
    }

    @DexIgnore
    public abstract boolean isDeviceReady();

    @DexIgnore
    public final synchronized boolean isQueueEmpty() {
        return this.highSessionQueue.isEmpty() && this.lowSessionQueue.isEmpty();
    }

    @DexIgnore
    public final boolean isRunning() {
        return !BleSession.Companion.isNull(getCurrentSession()) || !isQueueEmpty();
    }

    @DexIgnore
    public final void log(String str) {
        MFLog mfLog;
        ee7.b(str, "message");
        if (!BleSession.Companion.isNull(getCurrentSession()) && (mfLog = getCurrentSession().getMfLog()) != null) {
            mfLog.log('[' + this.serial + "] " + str);
        }
    }

    @DexIgnore
    public final void logNoCurrentSession(String str) {
        ee7.b(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No running session to handle " + str);
        log("No running session to handle " + str);
    }

    @DexIgnore
    public final void logNoCurrentState(String str) {
        ee7.b(str, "handle");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.i(str2, "No current state to handle " + str);
        log("No current state to handle " + str);
    }

    @DexIgnore
    public abstract boolean onPing();

    @DexIgnore
    public abstract boolean onReceiveCurrentSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean onReceivePushSecretKeyResponse(boolean z);

    @DexIgnore
    public abstract boolean onReceiveServerRandomKey(byte[] bArr, int i);

    @DexIgnore
    public abstract boolean onReceiveServerSecretKey(byte[] bArr, int i);

    @DexIgnore
    public abstract void onSetWatchParamResponse(String str, boolean z, WatchParamsFileMapping watchParamsFileMapping);

    @DexIgnore
    public final boolean pairDeviceResponse(PairingResponse pairingResponse) {
        ee7.b(pairingResponse, "response");
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof IPairDeviceSession) {
            if (pairingResponse instanceof PairingUpdateFWResponse) {
                ((IPairDeviceSession) currentSession).updateFirmware(((PairingUpdateFWResponse) pairingResponse).getFirmwareData());
            } else if (pairingResponse instanceof PairingLinkServerResponse) {
                PairingLinkServerResponse pairingLinkServerResponse = (PairingLinkServerResponse) pairingResponse;
                ((IPairDeviceSession) currentSession).onLinkServerSuccess(pairingLinkServerResponse.isSuccess(), pairingLinkServerResponse.getFailureCode());
            } else if (pairingResponse instanceof PairingAuthorizeResponse) {
                ((IPairDeviceSession) currentSession).onAuthorizeDevice(((PairingAuthorizeResponse) pairingResponse).getTimeOutDuration());
            } else if (pairingResponse instanceof LabelResponse) {
                ((IPairDeviceSession) currentSession).onDownloadLabelResponse((LabelResponse) pairingResponse);
            } else {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, ".pairDeviceResponse() FAILED, response=" + pairingResponse);
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.BLE;
                FLogger.Session session = FLogger.Session.PAIR;
                String str2 = this.serial;
                String str3 = this.TAG;
                remote.i(component, session, str2, str3, "pairDeviceResponse FAILED, response=" + pairingResponse);
                return false;
            }
            return true;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = this.TAG;
        local2.d(str4, ".pairDeviceResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
        FLogger.Component component2 = FLogger.Component.BLE;
        FLogger.Session session2 = FLogger.Session.OTHER;
        String str5 = this.serial;
        String str6 = this.TAG;
        remote2.i(component2, session2, str5, str6, "pairDeviceResponse FAILED, currentSession=" + currentSession);
        return false;
    }

    @DexIgnore
    public final void printQueue() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".printQueue - current session=" + getCurrentSession().getClass().getSimpleName());
        Iterator<BleSession> it = this.highSessionQueue.iterator();
        while (it.hasNext()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.d(str2, "Inside " + this.TAG + ".printQueue - high session=" + it.next().getClass().getSimpleName());
        }
        Iterator<BleSession> it2 = this.lowSessionQueue.iterator();
        while (it2.hasNext()) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local3.d(str3, "Inside " + this.TAG + ".printQueue - low session=" + it2.next().getClass().getSimpleName());
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:158:0x0685  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean queueSessionAndStart(com.misfit.frameworks.buttonservice.communite.ble.BleSession r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.lang.String r0 = "session"
            com.fossil.ee7.b(r11, r0)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r2.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside queueSessionAndStart - session="
            r2.append(r3)     // Catch:{ all -> 0x068a }
            r2.append(r11)     // Catch:{ all -> 0x068a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r2)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r11.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT_WITHOUT_TIMEOUT     // Catch:{ all -> 0x068a }
            r2 = 0
            r3 = 1
            if (r0 != r1) goto L_0x0075
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 == 0) goto L_0x0050
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x068a }
            if (r0 == 0) goto L_0x0050
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.lowSessionQueue     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x068a }
            if (r0 == 0) goto L_0x0050
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
        L_0x004d:
            r2 = 1
            goto L_0x0680
        L_0x0050:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Still have session in queue. Not queue ConnectWithoutTimeout Session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x0075:
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r1 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r4 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r1 = r1.isNull(r4)     // Catch:{ all -> 0x068a }
            if (r1 != 0) goto L_0x00b0
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r1 = r1.accept(r11)     // Catch:{ all -> 0x068a }
            if (r1 == 0) goto L_0x008c
            goto L_0x00b0
        L_0x008c:
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r2 = "Inside "
            r1.append(r2)     // Catch:{ all -> 0x068a }
            java.lang.String r2 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r2)     // Catch:{ all -> 0x068a }
            java.lang.String r2 = ".queueSessionAndStart -Input session is same as current session. Do nothing."
            r1.append(r2)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x00b0:
            int[] r1 = com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator.WhenMappings.$EnumSwitchMapping$0     // Catch:{ all -> 0x068a }
            int r4 = r0.ordinal()     // Catch:{ all -> 0x068a }
            r1 = r1[r4]     // Catch:{ all -> 0x068a }
            switch(r1) {
                case 1: goto L_0x0599;
                case 2: goto L_0x0444;
                case 3: goto L_0x0398;
                case 4: goto L_0x0243;
                case 5: goto L_0x0149;
                case 6: goto L_0x00bf;
                default: goto L_0x00bb;
            }     // Catch:{ all -> 0x068a }
        L_0x00bb:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            goto L_0x05dd
        L_0x00bf:
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x010c
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x010c
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Urgent Mode, current session is "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r3 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r3 = r3.getSessionType()     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ", reject this session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x010c:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r0.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x068a }
        L_0x0117:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x068a }
            if (r2 == 0) goto L_0x013d
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r2 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r2     // Catch:{ all -> 0x068a }
            java.lang.Class r4 = r11.getClass()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x068a }
            java.lang.Class r5 = r2.getClass()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r5.getName()     // Catch:{ all -> 0x068a }
            boolean r4 = com.fossil.mh7.b(r4, r5, r3)     // Catch:{ all -> 0x068a }
            if (r4 == 0) goto L_0x0117
            r0.add(r2)     // Catch:{ all -> 0x068a }
            goto L_0x0117
        L_0x013d:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.removeAll(r0)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0149:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart - Micro App Set Up Mode"
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r4)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x01b9
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x01b9
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Micro App Set Up Mode, current session is "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r3 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r3 = r3.getSessionType()     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ", reject this session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x01b9:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r0.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x068a }
        L_0x01c4:
            boolean r4 = r1.hasNext()     // Catch:{ all -> 0x068a }
            if (r4 == 0) goto L_0x0234
            java.lang.Object r4 = r1.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r4 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r4     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r5 = r4.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r6 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x01dc
            r0.add(r4)     // Catch:{ all -> 0x068a }
            goto L_0x01c4
        L_0x01dc:
            boolean r5 = r4.accept(r11)     // Catch:{ all -> 0x068a }
            if (r5 != 0) goto L_0x01c4
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r5 = r4.getCommunicateMode()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r6 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x01f0
            r0.add(r4)     // Catch:{ all -> 0x068a }
            goto L_0x01c4
        L_0x01f0:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r5.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r6 = "Inside "
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = r10.TAG     // Catch:{ all -> 0x068a }
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = ".queueSessionAndStart - Micro App Set Up Mode, "
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = "session in queue is "
            r5.append(r6)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r4 = r4.getCommunicateMode()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.name()     // Catch:{ all -> 0x068a }
            r5.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = ", reject this session, communicateMode="
            r5.append(r4)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r4 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.name()     // Catch:{ all -> 0x068a }
            r5.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x068a }
            r1.d(r3, r4)     // Catch:{ all -> 0x068a }
            goto L_0x0235
        L_0x0234:
            r2 = 1
        L_0x0235:
            if (r2 == 0) goto L_0x0680
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.removeAll(r0)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x0243:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r5.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r6 = "Inside "
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = r10.TAG     // Catch:{ all -> 0x068a }
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = ".queueSessionAndStart - type:"
            r5.append(r6)     // Catch:{ all -> 0x068a }
            r5.append(r0)     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x068a }
            r1.d(r4, r0)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x0369
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x02ae
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r3.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = "Inside "
            r3.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r10.TAG     // Catch:{ all -> 0x068a }
            r3.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = ".queueSessionAndStart - Sync Mode, current session is "
            r3.append(r4)     // Catch:{ all -> 0x068a }
            r3.append(r0)     // Catch:{ all -> 0x068a }
            java.lang.String r0 = ", reject this session."
            r3.append(r0)     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x068a }
            r11.d(r1, r0)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x02ae:
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SYNC     // Catch:{ all -> 0x068a }
            if (r0 == r1) goto L_0x0314
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT     // Catch:{ all -> 0x068a }
            if (r0 == r1) goto L_0x0314
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.DEVICE_SETTING     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x02bb
            goto L_0x0314
        L_0x02bb:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r2 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x068a }
        L_0x02c6:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x068a }
            if (r4 == 0) goto L_0x02de
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r4 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r4     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r5 = r4.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r6 = com.misfit.frameworks.buttonservice.communite.SessionType.UI     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x02c6
            r1.add(r4)     // Catch:{ all -> 0x068a }
            goto L_0x02c6
        L_0x02de:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r2 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r2.removeAll(r1)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT_WITHOUT_TIMEOUT     // Catch:{ all -> 0x068a }
            if (r0 == r1) goto L_0x030d
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.UI     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x02ef
            r10.interruptCurrentSession()     // Catch:{ all -> 0x068a }
            goto L_0x030d
        L_0x02ef:
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r0.copyObject()     // Catch:{ all -> 0x068a }
            r10.interruptCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r2 = com.misfit.frameworks.buttonservice.communite.SessionType.BACK_GROUND     // Catch:{ all -> 0x068a }
            if (r1 != r2) goto L_0x0308
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.lowSessionQueue     // Catch:{ all -> 0x068a }
            r1.offer(r0)     // Catch:{ all -> 0x068a }
            goto L_0x030d
        L_0x0308:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.offer(r0)     // Catch:{ all -> 0x068a }
        L_0x030d:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0314:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r2 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart - this UI session will be put in queue. CurrentSessionType: "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            r4.append(r0)     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x068a }
            r1.d(r2, r0)     // Catch:{ all -> 0x068a }
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r0.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x068a }
        L_0x0345:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x068a }
            if (r2 == 0) goto L_0x035d
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r2 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r2     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r4 = r2.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r5 = com.misfit.frameworks.buttonservice.communite.SessionType.UI     // Catch:{ all -> 0x068a }
            if (r4 != r5) goto L_0x0345
            r0.add(r2)     // Catch:{ all -> 0x068a }
            goto L_0x0345
        L_0x035d:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.removeAll(r0)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0369:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r0.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x068a }
        L_0x0374:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x068a }
            if (r2 == 0) goto L_0x038c
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r2 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r2     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r4 = r2.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r5 = com.misfit.frameworks.buttonservice.communite.SessionType.UI     // Catch:{ all -> 0x068a }
            if (r4 != r5) goto L_0x0374
            r0.add(r2)     // Catch:{ all -> 0x068a }
            goto L_0x0374
        L_0x038c:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.removeAll(r0)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0398:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart - Connect Mode"
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r4)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x0408
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x0408
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Connect Mode, current session is "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r3 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r3 = r3.getSessionType()     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ", reject this session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x0408:
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x0420
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT     // Catch:{ all -> 0x068a }
            if (r0 == r1) goto L_0x004d
        L_0x0420:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x068a }
        L_0x0426:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x068a }
            if (r1 == 0) goto L_0x043b
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r1     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = r1.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r4 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT     // Catch:{ all -> 0x068a }
            if (r1 != r4) goto L_0x0426
            r2 = 1
        L_0x043b:
            if (r2 != 0) goto L_0x004d
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0444:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart - Sync Mode"
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r4)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x04b4
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x04b4
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Sync Mode, current session is "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r3 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r3 = r3.getSessionType()     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ", reject this session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x04b4:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r0.<init>()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r5.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r6 = "Inside "
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = r10.TAG     // Catch:{ all -> 0x068a }
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r6 = ".queueSessionAndStart - Sync Mode, highSessionQueueSize="
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r6 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            int r6 = r6.size()     // Catch:{ all -> 0x068a }
            r5.append(r6)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x068a }
            r1.d(r4, r5)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x068a }
        L_0x04eb:
            boolean r4 = r1.hasNext()     // Catch:{ all -> 0x068a }
            if (r4 == 0) goto L_0x058a
            java.lang.Object r4 = r1.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r4 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r4     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r5 = r4.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.FLogger r6 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r6 = r6.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r7 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r8.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r9 = "Inside "
            r8.append(r9)     // Catch:{ all -> 0x068a }
            java.lang.String r9 = r10.TAG     // Catch:{ all -> 0x068a }
            r8.append(r9)     // Catch:{ all -> 0x068a }
            java.lang.String r9 = ".queueSessionAndStart, SessionType="
            r8.append(r9)     // Catch:{ all -> 0x068a }
            r8.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r9 = ", communicateMode="
            r8.append(r9)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r9 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            r8.append(r9)     // Catch:{ all -> 0x068a }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x068a }
            r6.d(r7, r8)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r6 = com.misfit.frameworks.buttonservice.communite.SessionType.CONNECT     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x0535
            r0.add(r4)     // Catch:{ all -> 0x068a }
            goto L_0x04eb
        L_0x0535:
            com.misfit.frameworks.buttonservice.communite.SessionType r6 = com.misfit.frameworks.buttonservice.communite.SessionType.SYNC     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x04eb
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r5 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r6 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC     // Catch:{ all -> 0x068a }
            if (r5 != r6) goto L_0x058b
            r5 = r11
            com.misfit.frameworks.buttonservice.communite.ble.ISyncSession r5 = (com.misfit.frameworks.buttonservice.communite.ble.ISyncSession) r5     // Catch:{ all -> 0x068a }
            int r5 = r5.getSyncMode()     // Catch:{ all -> 0x068a }
            r6 = 13
            if (r5 != r6) goto L_0x0550
            r0.add(r4)     // Catch:{ all -> 0x068a }
            goto L_0x04eb
        L_0x0550:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart, communicateMode="
            r4.append(r5)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r5 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ", syncMode="
            r4.append(r5)     // Catch:{ all -> 0x068a }
            r5 = r11
            com.misfit.frameworks.buttonservice.communite.ble.ISyncSession r5 = (com.misfit.frameworks.buttonservice.communite.ble.ISyncSession) r5     // Catch:{ all -> 0x068a }
            int r5 = r5.getSyncMode()     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x068a }
            r1.d(r3, r4)     // Catch:{ all -> 0x068a }
            goto L_0x058b
        L_0x058a:
            r2 = 1
        L_0x058b:
            if (r2 == 0) goto L_0x0680
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r1 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r1.removeAll(r0)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x0599:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r2.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r4 = "Inside "
            r2.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r10.TAG     // Catch:{ all -> 0x068a }
            r2.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = ".queueSessionAndStart - Clean up all sessions and start input session."
            r2.append(r4)     // Catch:{ all -> 0x068a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r2)     // Catch:{ all -> 0x068a }
            java.lang.String r0 = "Input session is a SPECIAL session, interrupt all other ones."
            r10.log(r0)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = r11.getSessionType()     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x05d3
            r10.setNullCurrentSession()     // Catch:{ all -> 0x068a }
            goto L_0x05d6
        L_0x05d3:
            r10.clearSessionQueue()     // Catch:{ all -> 0x068a }
        L_0x05d6:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.highSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x05dd:
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r4.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r5 = "Inside "
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = r10.TAG     // Catch:{ all -> 0x068a }
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r5 = ".queueSessionAndStart - Other Mode"
            r4.append(r5)     // Catch:{ all -> 0x068a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x068a }
            r0.d(r1, r4)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession$Companion r0 = com.misfit.frameworks.buttonservice.communite.ble.BleSession.Companion     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r1 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            boolean r0 = r0.isNull(r1)     // Catch:{ all -> 0x068a }
            if (r0 != 0) goto L_0x064a
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r0 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r0 = r0.getSessionType()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r1 = com.misfit.frameworks.buttonservice.communite.SessionType.SPECIAL     // Catch:{ all -> 0x068a }
            if (r0 != r1) goto L_0x064a
            com.misfit.frameworks.buttonservice.log.FLogger r11 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r11 = r11.getLocal()     // Catch:{ all -> 0x068a }
            java.lang.String r0 = r10.TAG     // Catch:{ all -> 0x068a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.lang.String r3 = "Inside "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = r10.TAG     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ".queueSessionAndStart - Other Mode, current session is "
            r1.append(r3)     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r3 = r10.getCurrentSession()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.SessionType r3 = r3.getSessionType()     // Catch:{ all -> 0x068a }
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r3 = ", reject this session."
            r1.append(r3)     // Catch:{ all -> 0x068a }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x068a }
            r11.d(r0, r1)     // Catch:{ all -> 0x068a }
            goto L_0x0680
        L_0x064a:
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r0 = r11.getCommunicateMode()     // Catch:{ all -> 0x068a }
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x068a }
            r1.<init>()     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r4 = r10.lowSessionQueue     // Catch:{ all -> 0x068a }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x068a }
        L_0x0659:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x068a }
            if (r5 == 0) goto L_0x0674
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.ble.BleSession r5 = (com.misfit.frameworks.buttonservice.communite.ble.BleSession) r5     // Catch:{ all -> 0x068a }
            com.misfit.frameworks.buttonservice.communite.CommunicateMode r6 = r5.getCommunicateMode()     // Catch:{ all -> 0x068a }
            if (r0 != r6) goto L_0x066d
            r6 = 1
            goto L_0x066e
        L_0x066d:
            r6 = 0
        L_0x066e:
            if (r6 == 0) goto L_0x0659
            r1.add(r5)     // Catch:{ all -> 0x068a }
            goto L_0x0659
        L_0x0674:
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.lowSessionQueue     // Catch:{ all -> 0x068a }
            r0.removeAll(r1)     // Catch:{ all -> 0x068a }
            java.util.concurrent.PriorityBlockingQueue<com.misfit.frameworks.buttonservice.communite.ble.BleSession> r0 = r10.lowSessionQueue     // Catch:{ all -> 0x068a }
            r0.offer(r11)     // Catch:{ all -> 0x068a }
            goto L_0x004d
        L_0x0680:
            r10.printQueue()     // Catch:{ all -> 0x068a }
            if (r2 == 0) goto L_0x0688
            r10.startSessionInQueue()     // Catch:{ all -> 0x068a }
        L_0x0688:
            monitor-exit(r10)
            return r2
        L_0x068a:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
            switch-data {1->0x0599, 2->0x0444, 3->0x0398, 4->0x0243, 5->0x0149, 6->0x00bf, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator.queueSessionAndStart(com.misfit.frameworks.buttonservice.communite.ble.BleSession):boolean");
    }

    @DexIgnore
    public final void resetSettingFlagsToDefault() {
        Context context = getBleAdapter().getContext();
        List<AlarmSetting> autoListAlarm = DevicePreferenceUtils.getAutoListAlarm(context);
        String autoSecondTimezoneId = DevicePreferenceUtils.getAutoSecondTimezoneId(context);
        List<BLEMapping> autoMapping = DevicePreferenceUtils.getAutoMapping(context, this.serial);
        ee7.a((Object) autoMapping, "DevicePreferenceUtils.ge\u2026oMapping(context, serial)");
        ComplicationAppMappingSettings autoComplicationAppSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(context, this.serial);
        WatchAppMappingSettings autoWatchAppSettings = DevicePreferenceUtils.getAutoWatchAppSettings(context, this.serial);
        LocalizationData autoLocalizationDataSettings = DevicePreferenceUtils.getAutoLocalizationDataSettings(context, this.serial);
        DevicePreferenceUtils.clearAllSettingFlag(context);
        if (!(autoListAlarm == null || !(!autoListAlarm.isEmpty()) || getBleAdapter().isSupportedFeature(od0.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MULTI_ALARM);
        }
        if (!TextUtils.isEmpty(autoSecondTimezoneId) && getBleAdapter().isSupportedFeature(sd0.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.SECOND_TIMEZONE);
        }
        if (!autoMapping.isEmpty()) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.MAPPINGS);
        }
        if (!(autoComplicationAppSettings == null || getBleAdapter().isSupportedFeature(xd0.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.COMPLICATION_APPS);
        }
        if (!(autoWatchAppSettings == null || getBleAdapter().isSupportedFeature(xd0.class) == null)) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.WATCH_APPS);
        }
        if (autoLocalizationDataSettings != null && getBleAdapter().isSupportedFeature(vd0.class) != null) {
            DevicePreferenceUtils.setSettingFlag(context, DeviceSettings.LOCALIZATION_DATA);
        }
    }

    @DexIgnore
    public abstract void sendCustomCommand(CustomRequest customRequest);

    @DexIgnore
    public abstract boolean sendingEncryptedDataSession(byte[] bArr, boolean z);

    @DexIgnore
    public final void setBleSessionCallback(BleSession.BleSessionCallback bleSessionCallback2) {
        ee7.b(bleSessionCallback2, "<set-?>");
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public abstract void setCurrentSession(BleSession bleSession);

    @DexIgnore
    public final void setHighSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        ee7.b(priorityBlockingQueue, "<set-?>");
        this.highSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public final void setLowSessionQueue(PriorityBlockingQueue<BleSession> priorityBlockingQueue) {
        ee7.b(priorityBlockingQueue, "<set-?>");
        this.lowSessionQueue = priorityBlockingQueue;
    }

    @DexIgnore
    public abstract boolean setMinimumStepThresholdSession(long j);

    @DexIgnore
    public abstract void setNullCurrentSession();

    @DexIgnore
    public abstract void setSecretKey(byte[] bArr);

    @DexIgnore
    public abstract boolean setWorkoutConfigSession(WorkoutConfigData workoutConfigData);

    @DexIgnore
    public abstract boolean setWorkoutGPSData(Location location);

    @DexIgnore
    public abstract boolean startCalibrationSession();

    @DexIgnore
    public abstract boolean startCleanLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startConnectionDeviceSession(boolean z, UserProfile userProfile);

    @DexIgnore
    public abstract boolean startGetBatteryLevelSession();

    @DexIgnore
    public abstract boolean startGetRssiSession();

    @DexIgnore
    public abstract boolean startGetVibrationStrengthSession();

    @DexIgnore
    public abstract boolean startInstallWatchApps(boolean z);

    @DexIgnore
    public abstract boolean startNotifyNotificationEvent(NotificationBaseObj notificationBaseObj);

    @DexIgnore
    public abstract boolean startOtaSession(FirmwareData firmwareData, UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPairingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startPlayAnimationSession();

    @DexIgnore
    public abstract boolean startReadCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startReadRealTimeStepSession();

    @DexIgnore
    public abstract void startSendDeviceAppResponse(DeviceAppResponse deviceAppResponse, boolean z);

    @DexIgnore
    public abstract void startSendMusicAppResponse(MusicResponse musicResponse);

    @DexIgnore
    public abstract boolean startSendNotification(NotificationBaseObj notificationBaseObj);

    @DexIgnore
    public final synchronized void startSessionInQueue() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Inside " + this.TAG + ".startSessionInQueue - Starting currentSession=" + getCurrentSession());
        startSessionInQueueProcess();
    }

    @DexIgnore
    public abstract void startSessionInQueueProcess();

    @DexIgnore
    public abstract boolean startSetActivityGoals(int i, int i2, int i3, boolean z);

    @DexIgnore
    public abstract boolean startSetAutoBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetAutoBiometricData(UserBiometricData userBiometricData);

    @DexIgnore
    public abstract boolean startSetAutoComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetAutoMapping(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetAutoMultiAlarms(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetAutoNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetAutoSecondTimezone(String str);

    @DexIgnore
    public abstract boolean startSetAutoWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetBackgroundImageConfig(BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetComplicationApps(ComplicationAppMappingSettings complicationAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetFrontLightEnable(boolean z);

    @DexIgnore
    public abstract boolean startSetHeartRateMode(HeartRateMode heartRateMode);

    @DexIgnore
    public abstract boolean startSetImplicitDeviceConfig(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSetImplicitDisplayUnitSettings(UserDisplayUnit userDisplayUnit);

    @DexIgnore
    public abstract boolean startSetInactiveNudgeConfigSession(InactiveNudgeData inactiveNudgeData);

    @DexIgnore
    public abstract boolean startSetLinkMappingSession(List<? extends BLEMapping> list);

    @DexIgnore
    public abstract boolean startSetLocalizationData(LocalizationData localizationData);

    @DexIgnore
    public abstract boolean startSetMultipleAlarmsSession(List<AlarmSetting> list);

    @DexIgnore
    public abstract boolean startSetNotificationFilterSettings(AppNotificationFilterSettings appNotificationFilterSettings);

    @DexIgnore
    public abstract boolean startSetPresetApps(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig);

    @DexIgnore
    public abstract boolean startSetReplyMessageMappingSettings(ReplyMessageMappingGroup replyMessageMappingGroup);

    @DexIgnore
    public abstract boolean startSetSecondTimezoneSession(String str);

    @DexIgnore
    public abstract boolean startSetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj);

    @DexIgnore
    public abstract boolean startSetWatchApps(WatchAppMappingSettings watchAppMappingSettings);

    @DexIgnore
    public abstract boolean startSetWorkoutDetectionSetting(WorkoutDetectionSetting workoutDetectionSetting);

    @DexIgnore
    public abstract boolean startStopCurrentWorkoutSession();

    @DexIgnore
    public abstract boolean startSwitchDeviceSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startSyncingSession(UserProfile userProfile);

    @DexIgnore
    public abstract boolean startUnlinkSession();

    @DexIgnore
    public abstract boolean startUpdateCurrentTime();

    @DexIgnore
    public abstract boolean startVerifySecretKeySession();

    @DexIgnore
    public final boolean switchDeviceResponse(boolean z, int i) {
        BleSession currentSession = getCurrentSession();
        if (currentSession instanceof ISwitchDeviceSession) {
            ((ISwitchDeviceSession) currentSession).onLinkServerSuccess(z, i);
            return true;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".switchDeviceResponse() FAILED, current session=" + currentSession);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String str2 = this.serial;
        String str3 = this.TAG;
        remote.i(component, session, str2, str3, "switchDeviceResponse FAILED, currentSession=" + currentSession);
        return false;
    }
}
