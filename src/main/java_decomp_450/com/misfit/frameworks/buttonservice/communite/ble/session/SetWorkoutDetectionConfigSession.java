package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionItemExtentionKt;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutDetectionConfigSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ WorkoutDetectionSetting workoutDetectionSetting;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWorkoutDetectionState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWorkoutDetectionState() {
            super(SetWorkoutDetectionConfigSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareData() {
            a90 a90 = new a90();
            a90.a(WorkoutDetectionItemExtentionKt.toAutoWorkoutDetectionItemConfig(SetWorkoutDetectionConfigSession.this.workoutDetectionSetting.getWorkoutDetectionList()));
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<o80[]> deviceConfig = SetWorkoutDetectionConfigSession.this.getBleAdapter().setDeviceConfig(SetWorkoutDetectionConfigSession.this.getLogSession(), prepareData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetWorkoutDetectionConfigSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SetWorkoutDetectionConfigSession.this.stop(FailureCode.FAILED_TO_SET_WORKOUT_DETECTION_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetWorkoutDetectionConfigSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWorkoutDetectionConfigSession(WorkoutDetectionSetting workoutDetectionSetting2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_WORKOUT_DETECTION, bleAdapterImpl, bleSessionCallback);
        ee7.b(workoutDetectionSetting2, "workoutDetectionSetting");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.workoutDetectionSetting = workoutDetectionSetting2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWorkoutDetectionConfigSession setWorkoutDetectionConfigSession = new SetWorkoutDetectionConfigSession(this.workoutDetectionSetting, getBleAdapter(), getBleSessionCallback());
        setWorkoutDetectionConfigSession.setDevice(getDevice());
        return setWorkoutDetectionConfigSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WORKOUT_DETECTION_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WORKOUT_DETECTION_STATE;
        String name = SetWorkoutDetectionState.class.getName();
        ee7.a((Object) name, "SetWorkoutDetectionState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
