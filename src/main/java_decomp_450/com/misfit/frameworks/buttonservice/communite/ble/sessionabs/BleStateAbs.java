package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.ab0;
import com.fossil.ee7;
import com.fossil.fitness.FitnessData;
import com.fossil.g60;
import com.fossil.ke0;
import com.fossil.l60;
import com.fossil.m60;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleStateAbs extends BleState implements ISessionSdkCallback {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int MAKE_DEVICE_READY_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int OTA_SUCCESS_TIMEOUT; // = 60000;
    @DexIgnore
    public static /* final */ int PROGRESS_TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public static /* final */ int PROGRESS_UPDATE_TIMEOUT; // = 30000;
    @DexIgnore
    public static /* final */ int TASK_MAXIMUM_RETRY; // = 3;
    @DexIgnore
    public int maxRetries; // = 3;
    @DexIgnore
    public int retryCounter;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleStateAbs(String str) {
        super(str);
        ee7.b(str, "tagName");
        setTimeout(30000);
    }

    @DexIgnore
    public final int getMaxRetries() {
        return this.maxRetries;
    }

    @DexIgnore
    public final int getRetryCounter() {
        return this.retryCounter;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyHandPositionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onApplyHandPositionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthenticateDeviceFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthenticateDeviceSuccess(byte[] bArr) {
        ee7.b(bArr, "randomKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthorizeDeviceFailed() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onAuthorizeDeviceSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onConfigureMicroAppFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onConfigureMicroAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferCompleted() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDataTransferProgressChange(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onDeviceFound(l60 l60, int i) {
        ee7.b(l60, "device");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseDataFilesFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseDataFilesSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseHWLogFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onEraseHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onExchangeSecretKeyFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onExchangeSecretKeySuccess(byte[] bArr) {
        ee7.b(bArr, "secretKey");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onFetchDeviceInfoFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onFetchDeviceInfoSuccess(m60 m60) {
        ee7.b(m60, "deviceInformation");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetDeviceConfigFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetDeviceConfigSuccess(HashMap<o80, n80> hashMap) {
        ee7.b(hashMap, "deviceConfiguration");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onGetWatchParamsFail() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onMoveHandFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onMoveHandSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNextSession() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNotifyNotificationEventFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onNotifyNotificationEventSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayAnimationFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayAnimationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onPlayDeviceAnimation(boolean z, ke0 ke0) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadCurrentWorkoutSessionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadCurrentWorkoutSessionSuccess(ab0 ab0) {
        ee7.b(ab0, "workoutSession");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadDataFilesSuccess(FitnessData[] fitnessDataArr) {
        ee7.b(fitnessDataArr, "data");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadHWLogSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadRssiFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReadRssiSuccess(int i) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReleaseHandControlFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onReleaseHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onRequestHandControlFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onRequestHandControlSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onResetHandsFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onResetHandsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onScanFail(g60 g60) {
        ee7.b(g60, "scanError");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendMicroAppDataFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendMicroAppDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendingEncryptedDataSessionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSendingEncryptedDataSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetAlarmFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetAlarmSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetBackgroundImageFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetBackgroundImageSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetComplicationFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetComplicationSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetDeviceConfigFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetDeviceConfigSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetFrontLightFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetFrontLightSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLabelFileFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLabelFileSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLocalizationDataFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetLocalizationDataSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetMinimumStepThresholdSessionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetMinimumStepThresholdSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetNotificationFilterSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetReplyMessageMappingError(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetReplyMessageMappingSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileFailed() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileProgressChanged(float f) {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppFileSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchAppSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchParamsFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWatchParamsSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWorkoutGPSDataSessionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onSetWorkoutGPSDataSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onStopCurrentWorkoutSessionFailed(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onStopCurrentWorkoutSessionSuccess() {
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onVerifySecretKeyFail(ke0 ke0) {
        ee7.b(ke0, "error");
        logUnexpectedCallback();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback
    public void onVerifySecretKeySuccess(boolean z) {
        logUnexpectedCallback();
    }

    @DexIgnore
    public final boolean retry(Context context, String str) {
        ee7.b(context, "context");
        ee7.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        MFLogManager instance = MFLogManager.getInstance(context);
        instance.addLogForActiveLog(str, "Retry state: " + getTAG() + ", counter=" + this.retryCounter + ", maxRetries=" + this.maxRetries);
        int i = this.retryCounter;
        if (i >= this.maxRetries) {
            return false;
        }
        this.retryCounter = i + 1;
        onEnter();
        return true;
    }

    @DexIgnore
    public final void setMaxRetries(int i) {
        this.maxRetries = i;
    }

    @DexIgnore
    public final void setRetryCounter(int i) {
        this.retryCounter = i;
    }
}
