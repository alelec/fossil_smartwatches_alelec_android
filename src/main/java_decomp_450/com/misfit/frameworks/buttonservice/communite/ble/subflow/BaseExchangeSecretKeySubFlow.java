package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import android.os.Bundle;
import android.util.Base64;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.p60;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseExchangeSecretKeySubFlow extends SubFlow {
    @DexIgnore
    public /* final */ BleSession.BleSessionCallback bleSessionCallback;
    @DexIgnore
    public /* final */ CommunicateMode communicateMode;
    @DexIgnore
    public /* final */ BleAdapterImpl mBleAdapterV2;
    @DexIgnore
    public byte[] mRandomKey;
    @DexIgnore
    public /* final */ MFLog mflog;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class AuthenticateDeviceSessionState extends BleStateAbs {
        @DexIgnore
        public ie0<byte[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public AuthenticateDeviceSessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthenticateDeviceFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            int code = ke0.getCode();
            if (code == p60.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(p60.REQUEST_UNSUPPORTED.getCode());
            } else if (code == p60.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(p60.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1217);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onAuthenticateDeviceSuccess(byte[] bArr) {
            ee7.b(bArr, "randomKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                Bundle bundle = new Bundle();
                bundle.putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(BaseExchangeSecretKeySubFlow.this.getBleAdapter()));
                bundle.putString(ButtonService.DEVICE_RANDOM_KEY, Base64.encodeToString(bArr, 2));
                bleSessionCallback.onAskForSecretKey(bundle);
            }
            BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
            baseExchangeSecretKeySubFlow.enterSubStateAsync(baseExchangeSecretKeySubFlow.createConcreteState(SubFlow.SessionState.EXCHANGE_SECRET_KEY));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "onEnter AuthenticateDeviceSessionState " + BaseExchangeSecretKeySubFlow.this.mRandomKey);
            if (BaseExchangeSecretKeySubFlow.this.mRandomKey != null) {
                BleAdapterImpl mBleAdapterV2 = BaseExchangeSecretKeySubFlow.this.getMBleAdapterV2();
                FLogger.Session logSession = BaseExchangeSecretKeySubFlow.this.getLogSession();
                byte[] access$getMRandomKey$p = BaseExchangeSecretKeySubFlow.this.mRandomKey;
                if (access$getMRandomKey$p != null) {
                    ie0<byte[]> startAuthenticate = mBleAdapterV2.startAuthenticate(logSession, access$getMRandomKey$p, this);
                    this.task = startAuthenticate;
                    if (startAuthenticate == null) {
                        BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
                        return true;
                    }
                    startTimeout();
                    return true;
                }
                ee7.a();
                throw null;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("AuthenticateDeviceSession: no random key", FLogger.Component.BLE, ErrorCodeBuilder.Step.START_AUTHEN, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout AuthenticateDeviceSessionState");
            ie0<byte[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ExchangeSecretKeySessionState extends BleStateAbs {
        @DexIgnore
        public ie0<byte[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public ExchangeSecretKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onExchangeSecretKeyFail(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            int code = ke0.getCode();
            if (code == p60.REQUEST_UNSUPPORTED.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(p60.REQUEST_UNSUPPORTED.getCode());
            } else if (code == p60.UNSUPPORTED_FORMAT.getCode()) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(p60.UNSUPPORTED_FORMAT.getCode());
            } else {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(1218);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onExchangeSecretKeySuccess(byte[] bArr) {
            ee7.b(bArr, "secretKey");
            stopTimeout();
            BleSession.BleSessionCallback bleSessionCallback = BaseExchangeSecretKeySubFlow.this.getBleSessionCallback();
            if (bleSessionCallback != null) {
                String serial = BaseExchangeSecretKeySubFlow.this.getSerial();
                String encodeToString = Base64.encodeToString(bArr, 2);
                ee7.a((Object) encodeToString, "Base64.encodeToString(secretKey, Base64.NO_WRAP)");
                bleSessionCallback.broadcastExchangeSecretKeySuccess(serial, encodeToString);
            }
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(0);
        }

        @DexIgnore
        public final void onReceiveSecretKey(byte[] bArr, int i) {
            if (i != 0 || bArr == null) {
                BaseExchangeSecretKeySubFlow baseExchangeSecretKeySubFlow = BaseExchangeSecretKeySubFlow.this;
                baseExchangeSecretKeySubFlow.errorLog("ExchangeSecretKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.EXCHANGE_SECRET_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(i);
                return;
            }
            ie0<byte[]> exchangeSecretKey = BaseExchangeSecretKeySubFlow.this.getBleAdapter().exchangeSecretKey(BaseExchangeSecretKeySubFlow.this.getLogSession(), bArr, this);
            this.task = exchangeSecretKey;
            if (exchangeSecretKey == null) {
                BaseExchangeSecretKeySubFlow.this.stopSubFlow(10000);
            } else {
                startTimeout();
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout ExchangeSecretKeySessionState");
            ie0<byte[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GenerateRandomKeySessionState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public GenerateRandomKeySessionState() {
            super(BaseExchangeSecretKeySubFlow.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onEnter GenerateRandomKeySessionState");
            if (BaseExchangeSecretKeySubFlow.this.getBleSessionCallback() != null) {
                BaseExchangeSecretKeySubFlow.this.getBleSessionCallback().onAskForRandomKey(BaseExchangeSecretKeySubFlow.this.getSerial());
                return true;
            }
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: No callback", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            FLogger.INSTANCE.getLocal().d(getTAG(), "onTimeout GenerateRandomKeySessionState");
            BaseExchangeSecretKeySubFlow.this.errorLog("GenerateRandomKeySession: timeout", FLogger.Component.BLE, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
            BaseExchangeSecretKeySubFlow.this.stopSubFlow(FailureCode.FAIL_TO_GET_RANDOM_KEY);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseExchangeSecretKeySubFlow(CommunicateMode communicateMode2, String str, BleSession bleSession, MFLog mFLog, FLogger.Session session, String str2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback2) {
        super(str, bleSession, mFLog, session, str2, bleAdapterImpl);
        ee7.b(communicateMode2, "communicateMode");
        ee7.b(str, "tagName");
        ee7.b(bleSession, "bleSession");
        ee7.b(session, "logSession");
        ee7.b(str2, "serial");
        ee7.b(bleAdapterImpl, "mBleAdapterV2");
        this.communicateMode = communicateMode2;
        this.mflog = mFLog;
        this.mBleAdapterV2 = bleAdapterImpl;
        this.bleSessionCallback = bleSessionCallback2;
    }

    @DexIgnore
    public final BleSession.BleSessionCallback getBleSessionCallback() {
        return this.bleSessionCallback;
    }

    @DexIgnore
    public final CommunicateMode getCommunicateMode() {
        return this.communicateMode;
    }

    @DexIgnore
    public final BleAdapterImpl getMBleAdapterV2() {
        return this.mBleAdapterV2;
    }

    @DexIgnore
    public final MFLog getMflog() {
        return this.mflog;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow
    public void initStateMap() {
        HashMap<SubFlow.SessionState, String> sessionStateMap = getSessionStateMap();
        SubFlow.SessionState sessionState = SubFlow.SessionState.GENERATE_RANDOM_KEY;
        String name = GenerateRandomKeySessionState.class.getName();
        ee7.a((Object) name, "GenerateRandomKeySessionState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<SubFlow.SessionState, String> sessionStateMap2 = getSessionStateMap();
        SubFlow.SessionState sessionState2 = SubFlow.SessionState.AUTHENTICATE_DEVICE;
        String name2 = AuthenticateDeviceSessionState.class.getName();
        ee7.a((Object) name2, "AuthenticateDeviceSessionState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<SubFlow.SessionState, String> sessionStateMap3 = getSessionStateMap();
        SubFlow.SessionState sessionState3 = SubFlow.SessionState.EXCHANGE_SECRET_KEY;
        String name3 = ExchangeSecretKeySessionState.class.getName();
        ee7.a((Object) name3, "ExchangeSecretKeySessionState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
    public boolean onEnter() {
        super.onEnter();
        enterSubStateAsync(createConcreteState(SubFlow.SessionState.GENERATE_RANDOM_KEY));
        return true;
    }

    @DexIgnore
    public final void onReceiveRandomKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey failureCode " + i + " randomKey " + bArr + " state " + getMCurrentState());
        if (i == 0) {
            this.mRandomKey = bArr;
            enterSubStateAsync(createConcreteState(SubFlow.SessionState.AUTHENTICATE_DEVICE));
            return;
        }
        errorLog("GenerateRandomKeySession: server errorCode=" + i, FLogger.Component.API, ErrorCodeBuilder.Step.GENERATE_PAIRING_KEY, ErrorCodeBuilder.AppError.UNKNOWN);
        stopSubFlow(i);
    }

    @DexIgnore
    public final void onReceiveServerSecretKey(byte[] bArr, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey failureCode " + i + " state " + getMCurrentState());
        BleStateAbs mCurrentState = getMCurrentState();
        if (mCurrentState instanceof ExchangeSecretKeySessionState) {
            ((ExchangeSecretKeySessionState) mCurrentState).onReceiveSecretKey(bArr, i);
        }
    }
}
