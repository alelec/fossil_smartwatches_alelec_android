package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetMultiAlarmsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ List<AlarmSetting> mAlarmSettingList;
    @DexIgnore
    public List<AlarmSetting> mOldMultiAlarmSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetListAlarmsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetListAlarmsState() {
            super(SetMultiAlarmsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<i97> alarms = SetMultiAlarmsSession.this.getBleAdapter().setAlarms(SetMultiAlarmsSession.this.getLogSession(), SetMultiAlarmsSession.this.mAlarmSettingList, this);
            this.task = alarms;
            if (alarms == null) {
                SetMultiAlarmsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetMultiAlarmsSession.this.getContext(), SetMultiAlarmsSession.this.getSerial())) {
                SetMultiAlarmsSession.this.log("Reach the limit retry. Stop.");
                SetMultiAlarmsSession.this.stop(FailureCode.FAILED_TO_SET_ALARM);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetAlarmSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoListAlarm(SetMultiAlarmsSession.this.getBleAdapter().getContext(), SetMultiAlarmsSession.this.mAlarmSettingList);
            SetMultiAlarmsSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetMultiAlarmsSession(BleAdapterImpl bleAdapterImpl, List<AlarmSetting> list, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_LIST_ALARM, bleAdapterImpl, bleSessionCallback);
        ee7.b(bleAdapterImpl, "bleAdapter");
        ee7.b(list, "mAlarmSettingList");
        this.mAlarmSettingList = list;
        setLogSession(FLogger.Session.SET_ALARM);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_MULTI_ALARM) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetMultiAlarmsSession setMultiAlarmsSession = new SetMultiAlarmsSession(getBleAdapter(), this.mAlarmSettingList, getBleSessionCallback());
        setMultiAlarmsSession.setDevice(getDevice());
        return setMultiAlarmsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initSettings() {
        super.initSettings();
        this.mOldMultiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getBleAdapter().getContext());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LIST_ALARMS_STATE;
        String name = SetListAlarmsState.class.getName();
        ee7.a((Object) name, "SetListAlarmsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
