package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.ee7;
import com.fossil.i97;
import com.fossil.ie0;
import com.fossil.ke0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWatchAppsSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ WatchAppMappingSettings mWatchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetWatchAppsState extends BleStateAbs {
        @DexIgnore
        public ie0<i97> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetWatchAppsState() {
            super(SetWatchAppsSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<i97> watchApps = SetWatchAppsSession.this.getBleAdapter().setWatchApps(SetWatchAppsSession.this.getLogSession(), SetWatchAppsSession.this.mWatchAppMappingSettings, this);
            this.task = watchApps;
            if (watchApps == null) {
                SetWatchAppsSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            if (!retry(SetWatchAppsSession.this.getContext(), SetWatchAppsSession.this.getSerial())) {
                SetWatchAppsSession.this.log("Reach the limit retry. Stop.");
                SetWatchAppsSession.this.stop(FailureCode.FAILED_TO_SET_WATCH_APPS);
            }
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetWatchAppSuccess() {
            stopTimeout();
            DevicePreferenceUtils.setAutoWatchAppSettings(SetWatchAppsSession.this.getBleAdapter().getContext(), SetWatchAppsSession.this.getBleAdapter().getSerial(), new Gson().a(SetWatchAppsSession.this.mWatchAppMappingSettings));
            SetWatchAppsSession.this.stop(0);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<i97> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetWatchAppsSession(WatchAppMappingSettings watchAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_WATCH_APPS, bleAdapterImpl, bleSessionCallback);
        ee7.b(watchAppMappingSettings, "mWatchAppMappingSettings");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mWatchAppMappingSettings = watchAppMappingSettings;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public boolean accept(BleSession bleSession) {
        ee7.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_WATCH_APPS) ? false : true;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetWatchAppsSession setWatchAppsSession = new SetWatchAppsSession(this.mWatchAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setWatchAppsSession.setDevice(getDevice());
        return setWatchAppsSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_WATCH_APPS_STATE);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_WATCH_APPS_STATE;
        String name = SetWatchAppsState.class.getName();
        ee7.a((Object) name, "SetWatchAppsState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
