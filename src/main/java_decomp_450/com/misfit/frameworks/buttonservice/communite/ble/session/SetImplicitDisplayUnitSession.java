package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.b90;
import com.fossil.d90;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetImplicitDisplayUnitSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ UserDisplayUnit mUserDisplayUnit;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetImplicitDisplayUnitSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetDisplayUnitState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetDisplayUnitState() {
            super(SetImplicitDisplayUnitSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            a90 a90 = new a90();
            a90.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            a90.a(SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getTemperatureUnit().toSDKTemperatureUnit(), b90.KCAL, SetImplicitDisplayUnitSession.this.mUserDisplayUnit.getDistanceUnit().toSDKDistanceUnit(), ConversionUtils.INSTANCE.getTimeFormat(SetImplicitDisplayUnitSession.this.getBleAdapter().getContext()), d90.MONTH_DAY_YEAR);
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            ie0<o80[]> deviceConfig = SetImplicitDisplayUnitSession.this.getBleAdapter().setDeviceConfig(SetImplicitDisplayUnitSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetImplicitDisplayUnitSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SetImplicitDisplayUnitSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = SetImplicitDisplayUnitSession.this;
            setImplicitDisplayUnitSession.enterStateAsync(setImplicitDisplayUnitSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            SetImplicitDisplayUnitSession.this.log("Set Display Unit timeout. Cancel.");
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetImplicitDisplayUnitSession(UserDisplayUnit userDisplayUnit, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_IMPLICIT_DISPLAY_UNIT, bleAdapterImpl, bleSessionCallback);
        ee7.b(userDisplayUnit, "mUserDisplayUnit");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mUserDisplayUnit = userDisplayUnit;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetImplicitDisplayUnitSession setImplicitDisplayUnitSession = new SetImplicitDisplayUnitSession(this.mUserDisplayUnit, getBleAdapter(), getBleSessionCallback());
        setImplicitDisplayUnitSession.setDevice(getDevice());
        return setImplicitDisplayUnitSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_DISPLAY_UNIT_STATE;
        String name = SetDisplayUnitState.class.getName();
        ee7.a((Object) name, "SetDisplayUnitState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        ee7.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        ee7.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
