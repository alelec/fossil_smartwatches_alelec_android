package com.misfit.frameworks.buttonservice.communite;

import com.fossil.ee7;
import com.fossil.gd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SingletonHolder<T, A> {
    @DexIgnore
    public gd7<? super A, ? extends T> creator;
    @DexIgnore
    public volatile T instance;

    @DexIgnore
    public SingletonHolder(gd7<? super A, ? extends T> gd7) {
        ee7.b(gd7, "creator");
        this.creator = gd7;
    }

    @DexIgnore
    public final T getInstance(A a) {
        T t;
        T t2 = this.instance;
        if (t2 != null) {
            return t2;
        }
        synchronized (this) {
            T t3 = this.instance;
            if (t3 == null) {
                gd7<? super A, ? extends T> gd7 = this.creator;
                if (gd7 != null) {
                    t3 = (T) gd7.invoke(a);
                    this.instance = t3;
                    this.creator = null;
                } else {
                    ee7.a();
                    throw null;
                }
            }
        }
        return t;
    }
}
