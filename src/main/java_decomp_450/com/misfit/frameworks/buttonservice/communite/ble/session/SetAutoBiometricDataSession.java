package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a90;
import com.fossil.c80;
import com.fossil.ee7;
import com.fossil.ie0;
import com.fossil.ke0;
import com.fossil.n80;
import com.fossil.o80;
import com.fossil.xd0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoBiometricDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public UserBiometricData mNewBiometricData;
    @DexIgnore
    public UserBiometricData mOldBiometricData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public DoneState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoBiometricDataSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetBiometricDataState extends BleStateAbs {
        @DexIgnore
        public ie0<o80[]> task;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public SetBiometricDataState() {
            super(SetAutoBiometricDataSession.this.getTAG());
        }

        @DexIgnore
        private final n80[] prepareConfigData() {
            a90 a90 = new a90();
            try {
                c80 sDKBiometricProfile = SetAutoBiometricDataSession.this.mNewBiometricData.toSDKBiometricProfile();
                a90.a(sDKBiometricProfile.getAge(), sDKBiometricProfile.getGender(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getHeightInCentimeter(), sDKBiometricProfile.getWearingPosition());
            } catch (Exception e) {
                SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
                setAutoBiometricDataSession.log("Set Biometric Data: exception=" + e.getMessage());
            }
            return a90.a();
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public boolean onEnter() {
            super.onEnter();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.log("Set Biometric Data, " + SetAutoBiometricDataSession.this.mNewBiometricData);
            ie0<o80[]> deviceConfig = SetAutoBiometricDataSession.this.getBleAdapter().setDeviceConfig(SetAutoBiometricDataSession.this.getLogSession(), prepareConfigData(), this);
            this.task = deviceConfig;
            if (deviceConfig == null) {
                SetAutoBiometricDataSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetComplicationFailed(ke0 ke0) {
            ee7.b(ke0, "error");
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, true);
            SetAutoBiometricDataSession.this.stop(FailureCode.FAILED_TO_SET_BIOMETRIC_DATA);
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ISessionSdkCallback, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetAutoBiometricDataSession setAutoBiometricDataSession = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession.storeBiometricData(setAutoBiometricDataSession.mNewBiometricData, false);
            SetAutoBiometricDataSession setAutoBiometricDataSession2 = SetAutoBiometricDataSession.this;
            setAutoBiometricDataSession2.enterStateAsync(setAutoBiometricDataSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        @Override // com.misfit.frameworks.buttonservice.communite.ble.BleState
        public void onTimeout() {
            super.onTimeout();
            ie0<o80[]> ie0 = this.task;
            if (ie0 != null) {
                ie0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoBiometricDataSession(UserBiometricData userBiometricData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_BIOMETRIC_DATA, bleAdapterImpl, bleSessionCallback);
        ee7.b(userBiometricData, "mNewBiometricData");
        ee7.b(bleAdapterImpl, "bleAdapter");
        this.mNewBiometricData = userBiometricData;
    }

    @DexIgnore
    private final void storeBiometricData(UserBiometricData userBiometricData, boolean z) {
        DevicePreferenceUtils.setAutoBiometricSettings(getBleAdapter().getContext(), new Gson().a(userBiometricData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.BIOMETRIC);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.BleSession
    public BleSession copyObject() {
        SetAutoBiometricDataSession setAutoBiometricDataSession = new SetAutoBiometricDataSession(this.mNewBiometricData, getBleAdapter(), getBleSessionCallback());
        setAutoBiometricDataSession.setDevice(getDevice());
        return setAutoBiometricDataSession;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldBiometricData = DevicePreferenceUtils.getAutoBiometricSettings(getContext());
        if (getBleAdapter().isSupportedFeature(xd0.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (UserBiometricData.CREATOR.isSame(this.mOldBiometricData, this.mNewBiometricData)) {
            log("New biometric data and old biometric data are the same. No need to set again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else {
            storeBiometricData(this.mNewBiometricData, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs, com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_BIOMETRIC_DATA_STATE;
        String name = SetBiometricDataState.class.getName();
        ee7.a((Object) name, "SetBiometricDataState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        ee7.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        ee7.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
