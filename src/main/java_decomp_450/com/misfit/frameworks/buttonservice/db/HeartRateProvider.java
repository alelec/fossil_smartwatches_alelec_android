package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HeartRateProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "heart_rate_file.db";
    @DexIgnore
    public static /* final */ String TAG; // = "HeartRateProvider";
    @DexIgnore
    public static HeartRateProvider sInstance;

    @DexIgnore
    public HeartRateProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static synchronized HeartRateProvider getInstance(Context context) {
        HeartRateProvider heartRateProvider;
        synchronized (HeartRateProvider.class) {
            if (sInstance == null) {
                sInstance = new HeartRateProvider(context, DB_NAME);
            }
            heartRateProvider = sInstance;
        }
        return heartRateProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.i(str, "Inside " + TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "Error inside " + TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public void deleteDataFile(DataFile dataFile) {
        try {
            getDataFileDao().delete(dataFile);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".deleteDataFile - e=" + e);
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<DataFile, String> queryBuilder = getDataFileDao().queryBuilder();
            queryBuilder.where().lt("syncTime", Long.valueOf(j));
            List<DataFile> query = getDataFileDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error inside " + TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x012c, code lost:
        if (r2 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x012e, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x015c, code lost:
        if (r2 != null) goto L_0x012e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x015f, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0162  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.buttonservice.db.DataFile getDataFile(java.lang.String r21, java.lang.String r22) {
        /*
            r20 = this;
            r0 = r21
            r7 = r20
            com.misfit.frameworks.buttonservice.db.DatabaseHelper r1 = r7.databaseHelper
            android.database.sqlite.SQLiteDatabase r1 = r1.getReadableDatabase()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "SELECT length(dataFile), serial, syncTime FROM dataFile WHERE `key` = ?"
            r2.append(r3)
            java.lang.String r3 = " AND serial = ?"
            java.lang.String r4 = ""
            if (r22 != 0) goto L_0x001c
            r5 = r4
            goto L_0x001d
        L_0x001c:
            r5 = r3
        L_0x001d:
            r2.append(r5)
            java.lang.String r2 = r2.toString()
            r5 = 2
            r6 = 1
            r8 = 0
            if (r22 != 0) goto L_0x002e
            java.lang.String[] r9 = new java.lang.String[r6]
            r9[r8] = r0
            goto L_0x0034
        L_0x002e:
            java.lang.String[] r9 = new java.lang.String[r5]
            r9[r8] = r0
            r9[r6] = r22
        L_0x0034:
            android.database.Cursor r2 = r1.rawQuery(r2, r9)
            com.j256.ormlite.dao.Dao r10 = r20.getDataFileDao()     // Catch:{ Exception -> 0x0134 }
            com.j256.ormlite.stmt.QueryBuilder r10 = r10.queryBuilder()     // Catch:{ Exception -> 0x0134 }
            com.j256.ormlite.stmt.Where r11 = r10.where()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r12 = "key"
            r11.eq(r12, r0)     // Catch:{ Exception -> 0x0134 }
            if (r2 == 0) goto L_0x0064
            int r13 = r2.getCount()     // Catch:{ Exception -> 0x0134 }
            if (r13 <= 0) goto L_0x0064
            r2.moveToFirst()     // Catch:{ Exception -> 0x0134 }
            int r13 = r2.getInt(r8)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r14 = r2.getString(r6)     // Catch:{ Exception -> 0x0134 }
            long r15 = r2.getLong(r5)     // Catch:{ Exception -> 0x0134 }
            r2.close()     // Catch:{ Exception -> 0x0134 }
            goto L_0x0068
        L_0x0064:
            r14 = r4
            r13 = 0
            r15 = 0
        L_0x0068:
            com.misfit.frameworks.buttonservice.log.FLogger r17 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x0134 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r17.getLocal()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r11 = com.misfit.frameworks.buttonservice.db.HeartRateProvider.TAG     // Catch:{ Exception -> 0x0134 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0134 }
            r12.<init>()     // Catch:{ Exception -> 0x0134 }
            java.lang.String r5 = "Inside .getDataFile - fileData len="
            r12.append(r5)     // Catch:{ Exception -> 0x0134 }
            r12.append(r13)     // Catch:{ Exception -> 0x0134 }
            java.lang.String r5 = r12.toString()     // Catch:{ Exception -> 0x0134 }
            r9.d(r11, r5)     // Catch:{ Exception -> 0x0134 }
            r5 = 1000000(0xf4240, float:1.401298E-39)
            if (r13 <= r5) goto L_0x0115
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0134 }
            r5.<init>()     // Catch:{ Exception -> 0x0134 }
            r9 = r2
            r11 = 0
        L_0x0091:
            long r6 = (long) r13
            int r2 = (r11 > r6 ? 1 : (r11 == r6 ? 0 : -1))
            if (r2 >= 0) goto L_0x00fe
            r6 = 1
            long r6 = r6 + r11
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r2.<init>()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r10 = "SELECT substr(dataFile, ?, ? ) FROM dataFile WHERE `key` = ?"
            r2.append(r10)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            if (r22 != 0) goto L_0x00a7
            r10 = r4
            goto L_0x00a8
        L_0x00a7:
            r10 = r3
        L_0x00a8:
            r2.append(r10)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r18 = 1000000(0xf4240, double:4.940656E-318)
            r10 = 3
            if (r22 != 0) goto L_0x00ca
            java.lang.String[] r10 = new java.lang.String[r10]     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r10[r8] = r6     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r6 = java.lang.Long.toString(r18)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r7 = 1
            r10[r7] = r6     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r6 = 2
            r10[r6] = r0     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r6 = 2
            r7 = 1
            goto L_0x00e1
        L_0x00ca:
            r10 = 4
            java.lang.String[] r10 = new java.lang.String[r10]     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r6 = java.lang.Long.toString(r6)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r10[r8] = r6     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r6 = java.lang.Long.toString(r18)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r7 = 1
            r10[r7] = r6     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r6 = 2
            r10[r6] = r0     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r17 = 3
            r10[r17] = r22     // Catch:{ Exception -> 0x0112, all -> 0x010f }
        L_0x00e1:
            android.database.Cursor r9 = r1.rawQuery(r2, r10)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            if (r9 == 0) goto L_0x0091
            int r2 = r9.getCount()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            if (r2 <= 0) goto L_0x0091
            r9.moveToFirst()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r2 = r9.getString(r8)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r5.append(r2)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            int r2 = r2.length()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            long r6 = (long) r2     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            long r11 = r11 + r6
            goto L_0x0091
        L_0x00fe:
            com.misfit.frameworks.buttonservice.db.DataFile r7 = new com.misfit.frameworks.buttonservice.db.DataFile     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r1 = r7
            r2 = r21
            r4 = r14
            r5 = r15
            r1.<init>(r2, r3, r4, r5)     // Catch:{ Exception -> 0x0112, all -> 0x010f }
            r2 = r9
            r9 = r7
            goto L_0x0124
        L_0x010f:
            r0 = move-exception
            r2 = r9
            goto L_0x0160
        L_0x0112:
            r0 = move-exception
            r2 = r9
            goto L_0x0135
        L_0x0115:
            com.j256.ormlite.dao.Dao r0 = r20.getDataFileDao()
            com.j256.ormlite.stmt.PreparedQuery r1 = r10.prepare()
            java.lang.Object r0 = r0.queryForFirst(r1)
            com.misfit.frameworks.buttonservice.db.DataFile r0 = (com.misfit.frameworks.buttonservice.db.DataFile) r0
            r9 = r0
        L_0x0124:
            if (r2 == 0) goto L_0x012c
            r2.close()     // Catch:{ Exception -> 0x012a }
            goto L_0x012c
        L_0x012a:
            r0 = move-exception
            goto L_0x0136
        L_0x012c:
            if (r2 == 0) goto L_0x015f
        L_0x012e:
            r2.close()
            goto L_0x015f
        L_0x0132:
            r0 = move-exception
            goto L_0x0160
        L_0x0134:
            r0 = move-exception
        L_0x0135:
            r9 = 0
        L_0x0136:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x0132 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()     // Catch:{ all -> 0x0132 }
            java.lang.String r3 = com.misfit.frameworks.buttonservice.db.HeartRateProvider.TAG     // Catch:{ all -> 0x0132 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0132 }
            r4.<init>()     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = "Error inside "
            r4.append(r5)     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = com.misfit.frameworks.buttonservice.db.HeartRateProvider.TAG     // Catch:{ all -> 0x0132 }
            r4.append(r5)     // Catch:{ all -> 0x0132 }
            java.lang.String r5 = ".getDataFile - e="
            r4.append(r5)     // Catch:{ all -> 0x0132 }
            r4.append(r0)     // Catch:{ all -> 0x0132 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0132 }
            r1.e(r3, r0)     // Catch:{ all -> 0x0132 }
            if (r2 == 0) goto L_0x015f
            goto L_0x012e
        L_0x015f:
            return r9
        L_0x0160:
            if (r2 == 0) goto L_0x0165
            r2.close()
        L_0x0165:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.db.HeartRateProvider.getDataFile(java.lang.String, java.lang.String):com.misfit.frameworks.buttonservice.db.DataFile");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "Inside " + TAG + ".saveDataFile - Saving dataFile=" + dataFile);
                getDataFileDao().create(dataFile);
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.e(str3, "Error inside " + TAG + ".saveDataFile -e=Data file existed");
            }
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local4.e(str4, "Error inside " + TAG + ".saveDataFile - e=" + e);
        }
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = TAG;
        local5.d(str5, "Inside " + TAG + ".saveDataFile - dataFile=" + dataFile + " - DONE");
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, null);
    }
}
