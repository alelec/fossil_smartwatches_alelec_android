package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DataFileProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ int BUFFER_SIZE; // = 1000000;
    @DexIgnore
    public static /* final */ String DB_NAME; // = "data_file.db";
    @DexIgnore
    public static DataFileProvider sInstance;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.misfit.frameworks.buttonservice.db.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE datafile ADD COLUMN syncTime BIGINT");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
        }
    }

    @DexIgnore
    public DataFileProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<DataFile, String> getDataFileDao() throws SQLException {
        return ((BaseDbProvider) this).databaseHelper.getDao(DataFile.class);
    }

    @DexIgnore
    public static synchronized DataFileProvider getInstance(Context context) {
        DataFileProvider dataFileProvider;
        synchronized (DataFileProvider.class) {
            if (sInstance == null) {
                sInstance = new DataFileProvider(context, DB_NAME);
            }
            dataFileProvider = sInstance;
        }
        return dataFileProvider;
    }

    @DexIgnore
    public void clearFiles() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = ((BaseDbProvider) this).TAG;
            local.i(str, "Inside " + ((BaseDbProvider) this).TAG + ".clearHwLog");
            getDataFileDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            local2.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public int deleteDataFiles(String str) {
        try {
            DeleteBuilder<DataFile, String> deleteBuilder = getDataFileDao().deleteBuilder();
            deleteBuilder.where().eq("serial", str);
            return deleteBuilder.delete();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = ((BaseDbProvider) this).TAG;
            local.e(str2, "Error inside " + ((BaseDbProvider) this).TAG + ".deleteDataFile - e=" + e);
            return 0;
        }
    }

    @DexIgnore
    public List<DataFile> getAllDataFiles(long j, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            String str2 = "SELECT key FROM dataFile WHERE serial = '" + str + "' AND " + "syncTime" + " <= " + j;
            GenericRawResults<String[]> queryRaw = getDataFileDao().queryRaw(str2, new String[0]);
            FLogger.INSTANCE.getLocal().d(((BaseDbProvider) this).TAG, str2);
            ArrayList arrayList2 = new ArrayList();
            for (String[] strArr : queryRaw) {
                if (strArr.length > 0) {
                    arrayList2.add(strArr[0]);
                }
            }
            queryRaw.close();
            if (arrayList2.size() > 0) {
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    arrayList.add(getDataFile((String) it.next()));
                }
            }
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().e(((BaseDbProvider) this).TAG, "Error inside " + ((BaseDbProvider) this).TAG + ".getAllDataFiles - e=" + e);
        }
        return arrayList;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0136, code lost:
        if (r3 != null) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0138, code lost:
        r3.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0166, code lost:
        if (r3 != null) goto L_0x0138;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0169, code lost:
        return r9;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:60:0x016c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.misfit.frameworks.buttonservice.db.DataFile getDataFile(java.lang.String r21, java.lang.String r22) {
        /*
            r20 = this;
            r1 = r20
            r0 = r21
            com.misfit.frameworks.buttonservice.db.DatabaseHelper r2 = r1.databaseHelper
            android.database.sqlite.SQLiteDatabase r2 = r2.getReadableDatabase()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "SELECT length(dataFile), serial, syncTime FROM dataFile WHERE `key` = ?"
            r3.append(r4)
            java.lang.String r4 = " AND serial = ?"
            java.lang.String r5 = ""
            if (r22 != 0) goto L_0x001c
            r6 = r5
            goto L_0x001d
        L_0x001c:
            r6 = r4
        L_0x001d:
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r6 = 2
            r7 = 1
            r8 = 0
            if (r22 != 0) goto L_0x002e
            java.lang.String[] r9 = new java.lang.String[r7]
            r9[r8] = r0
            goto L_0x0034
        L_0x002e:
            java.lang.String[] r9 = new java.lang.String[r6]
            r9[r8] = r0
            r9[r7] = r22
        L_0x0034:
            android.database.Cursor r3 = r2.rawQuery(r3, r9)
            com.j256.ormlite.dao.Dao r10 = r20.getDataFileDao()     // Catch:{ Exception -> 0x013e }
            com.j256.ormlite.stmt.QueryBuilder r10 = r10.queryBuilder()     // Catch:{ Exception -> 0x013e }
            com.j256.ormlite.stmt.Where r11 = r10.where()     // Catch:{ Exception -> 0x013e }
            java.lang.String r12 = "key"
            r11.eq(r12, r0)     // Catch:{ Exception -> 0x013e }
            if (r3 == 0) goto L_0x0064
            int r13 = r3.getCount()     // Catch:{ Exception -> 0x013e }
            if (r13 <= 0) goto L_0x0064
            r3.moveToFirst()     // Catch:{ Exception -> 0x013e }
            int r13 = r3.getInt(r8)     // Catch:{ Exception -> 0x013e }
            java.lang.String r14 = r3.getString(r7)     // Catch:{ Exception -> 0x013e }
            long r15 = r3.getLong(r6)     // Catch:{ Exception -> 0x013e }
            r3.close()     // Catch:{ Exception -> 0x013e }
            goto L_0x0068
        L_0x0064:
            r14 = r5
            r13 = 0
            r15 = 0
        L_0x0068:
            com.misfit.frameworks.buttonservice.log.FLogger r17 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x013e }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r9 = r17.getLocal()     // Catch:{ Exception -> 0x013e }
            java.lang.String r11 = r1.TAG     // Catch:{ Exception -> 0x013e }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x013e }
            r12.<init>()     // Catch:{ Exception -> 0x013e }
            java.lang.String r6 = "Inside .getDataFile - fileData len="
            r12.append(r6)     // Catch:{ Exception -> 0x013e }
            r12.append(r13)     // Catch:{ Exception -> 0x013e }
            java.lang.String r6 = r12.toString()     // Catch:{ Exception -> 0x013e }
            r9.d(r11, r6)     // Catch:{ Exception -> 0x013e }
            r6 = 1000000(0xf4240, float:1.401298E-39)
            if (r13 <= r6) goto L_0x011f
            java.lang.StringBuffer r6 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x013e }
            r6.<init>()     // Catch:{ Exception -> 0x013e }
            r9 = r3
            r11 = 0
        L_0x0091:
            long r7 = (long) r13
            int r3 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r3 >= 0) goto L_0x0108
            r7 = 1
            long r7 = r7 + r11
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r3.<init>()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r10 = "SELECT substr(dataFile, ?, ? ) FROM dataFile WHERE `key` = ?"
            r3.append(r10)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            if (r22 != 0) goto L_0x00a7
            r10 = r5
            goto L_0x00a8
        L_0x00a7:
            r10 = r4
        L_0x00a8:
            r3.append(r10)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r18 = 1000000(0xf4240, double:4.940656E-318)
            r10 = 3
            if (r22 != 0) goto L_0x00cb
            java.lang.String[] r10 = new java.lang.String[r10]     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r7 = java.lang.Long.toString(r7)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r8 = 0
            r10[r8] = r7     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r7 = java.lang.Long.toString(r18)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r8 = 1
            r10[r8] = r7     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r7 = 2
            r10[r7] = r0     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r7 = 2
            r8 = 1
            goto L_0x00e3
        L_0x00cb:
            r10 = 4
            java.lang.String[] r10 = new java.lang.String[r10]     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r7 = java.lang.Long.toString(r7)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r8 = 0
            r10[r8] = r7     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r7 = java.lang.Long.toString(r18)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r8 = 1
            r10[r8] = r7     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r7 = 2
            r10[r7] = r0     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r17 = 3
            r10[r17] = r22     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
        L_0x00e3:
            android.database.Cursor r9 = r2.rawQuery(r3, r10)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            if (r9 == 0) goto L_0x0103
            int r3 = r9.getCount()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            if (r3 <= 0) goto L_0x0103
            r9.moveToFirst()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r3 = 0
            java.lang.String r10 = r9.getString(r3)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r6.append(r10)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            int r10 = r10.length()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r17 = r4
            long r3 = (long) r10     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            long r11 = r11 + r3
            goto L_0x0105
        L_0x0103:
            r17 = r4
        L_0x0105:
            r4 = r17
            goto L_0x0091
        L_0x0108:
            com.misfit.frameworks.buttonservice.db.DataFile r8 = new com.misfit.frameworks.buttonservice.db.DataFile     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            java.lang.String r4 = r6.toString()     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r2 = r8
            r3 = r21
            r5 = r14
            r6 = r15
            r2.<init>(r3, r4, r5, r6)     // Catch:{ Exception -> 0x011c, all -> 0x0119 }
            r3 = r9
            r9 = r8
            goto L_0x012e
        L_0x0119:
            r0 = move-exception
            r3 = r9
            goto L_0x016a
        L_0x011c:
            r0 = move-exception
            r3 = r9
            goto L_0x013f
        L_0x011f:
            com.j256.ormlite.dao.Dao r0 = r20.getDataFileDao()
            com.j256.ormlite.stmt.PreparedQuery r2 = r10.prepare()
            java.lang.Object r0 = r0.queryForFirst(r2)
            com.misfit.frameworks.buttonservice.db.DataFile r0 = (com.misfit.frameworks.buttonservice.db.DataFile) r0
            r9 = r0
        L_0x012e:
            if (r3 == 0) goto L_0x0136
            r3.close()     // Catch:{ Exception -> 0x0134 }
            goto L_0x0136
        L_0x0134:
            r0 = move-exception
            goto L_0x0140
        L_0x0136:
            if (r3 == 0) goto L_0x0169
        L_0x0138:
            r3.close()
            goto L_0x0169
        L_0x013c:
            r0 = move-exception
            goto L_0x016a
        L_0x013e:
            r0 = move-exception
        L_0x013f:
            r9 = 0
        L_0x0140:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ all -> 0x013c }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()     // Catch:{ all -> 0x013c }
            java.lang.String r4 = r1.TAG     // Catch:{ all -> 0x013c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x013c }
            r5.<init>()     // Catch:{ all -> 0x013c }
            java.lang.String r6 = "Error inside "
            r5.append(r6)     // Catch:{ all -> 0x013c }
            java.lang.String r6 = r1.TAG     // Catch:{ all -> 0x013c }
            r5.append(r6)     // Catch:{ all -> 0x013c }
            java.lang.String r6 = ".getDataFile - e="
            r5.append(r6)     // Catch:{ all -> 0x013c }
            r5.append(r0)     // Catch:{ all -> 0x013c }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x013c }
            r2.e(r4, r0)     // Catch:{ all -> 0x013c }
            if (r3 == 0) goto L_0x0169
            goto L_0x0138
        L_0x0169:
            return r9
        L_0x016a:
            if (r3 == 0) goto L_0x016f
            r3.close()
        L_0x016f:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.db.DataFileProvider.getDataFile(java.lang.String, java.lang.String):com.misfit.frameworks.buttonservice.db.DataFile");
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DataFile.class};
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.db.BaseDbProvider
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public void saveDataFile(DataFile dataFile) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = ((BaseDbProvider) this).TAG;
        local.d(str, "Inside " + ((BaseDbProvider) this).TAG + ".saveDataFile - dataFile=" + dataFile);
        try {
            if (getDataFile(dataFile.key, dataFile.serial) == null) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = ((BaseDbProvider) this).TAG;
                local2.d(str2, "Inside " + ((BaseDbProvider) this).TAG + ".saveDataFile - Save dataFile=" + dataFile);
                getDataFileDao().create(dataFile);
                return;
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = ((BaseDbProvider) this).TAG;
            local3.e(str3, "Error inside " + ((BaseDbProvider) this).TAG + ".saveDataFile -e=Data file existed");
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = ((BaseDbProvider) this).TAG;
            local4.e(str4, "Error inside " + ((BaseDbProvider) this).TAG + ".saveDataFile - e=" + e);
        }
    }

    @DexIgnore
    public DataFile getDataFile(String str) {
        return getDataFile(str, null);
    }
}
