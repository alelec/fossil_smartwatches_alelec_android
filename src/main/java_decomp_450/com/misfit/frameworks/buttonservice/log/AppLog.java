package com.misfit.frameworks.buttonservice.log;

import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.te4;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppLog {
    @DexIgnore
    @te4("app_log")
    public float appLogSize;
    @DexIgnore
    @te4("cache")
    public float cache;
    @DexIgnore
    @te4(UserDataStore.DATE_OF_BIRTH)
    public float db;

    @DexIgnore
    public AppLog() {
        this(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 7, null);
    }

    @DexIgnore
    public AppLog(float f, float f2, float f3) {
        this.db = f;
        this.cache = f2;
        this.appLogSize = f3;
    }

    @DexIgnore
    public static /* synthetic */ AppLog copy$default(AppLog appLog, float f, float f2, float f3, int i, Object obj) {
        if ((i & 1) != 0) {
            f = appLog.db;
        }
        if ((i & 2) != 0) {
            f2 = appLog.cache;
        }
        if ((i & 4) != 0) {
            f3 = appLog.appLogSize;
        }
        return appLog.copy(f, f2, f3);
    }

    @DexIgnore
    public final float component1() {
        return this.db;
    }

    @DexIgnore
    public final float component2() {
        return this.cache;
    }

    @DexIgnore
    public final float component3() {
        return this.appLogSize;
    }

    @DexIgnore
    public final AppLog copy(float f, float f2, float f3) {
        return new AppLog(f, f2, f3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AppLog)) {
            return false;
        }
        AppLog appLog = (AppLog) obj;
        return Float.compare(this.db, appLog.db) == 0 && Float.compare(this.cache, appLog.cache) == 0 && Float.compare(this.appLogSize, appLog.appLogSize) == 0;
    }

    @DexIgnore
    public final float getAppLogSize() {
        return this.appLogSize;
    }

    @DexIgnore
    public final float getCache() {
        return this.cache;
    }

    @DexIgnore
    public final float getDb() {
        return this.db;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.db) * 31) + Float.floatToIntBits(this.cache)) * 31) + Float.floatToIntBits(this.appLogSize);
    }

    @DexIgnore
    public final void setAppLogSize(float f) {
        this.appLogSize = f;
    }

    @DexIgnore
    public final void setCache(float f) {
        this.cache = f;
    }

    @DexIgnore
    public final void setDb(float f) {
        this.db = f;
    }

    @DexIgnore
    public String toString() {
        return "AppLog(db=" + this.db + ", cache=" + this.cache + ", appLogSize=" + this.appLogSize + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AppLog(float f, float f2, float f3, int i, zd7 zd7) {
        this((i & 1) != 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f, (i & 2) != 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f2, (i & 4) != 0 ? LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES : f3);
    }
}
