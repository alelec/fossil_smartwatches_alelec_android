package com.misfit.frameworks.buttonservice.log;

import com.fossil.ie4;
import com.fossil.iw7;
import com.fossil.xv7;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface LogApiService {
    @DexIgnore
    @iw7("app_log/event")
    Call<ie4> sendLogs(@xv7 ie4 ie4);
}
