package com.misfit.frameworks.buttonservice.log;

import com.fossil.dl7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.kn7;
import com.fossil.mn7;
import com.fossil.qj7;
import com.fossil.rw;
import com.fossil.we7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int FILE_LOG_SIZE_THRESHOLD; // = 5242880;
    @DexIgnore
    public static /* final */ String FILE_NAME_PATTERN; // = "app_log_%s.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public FileDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ SynchronizeQueue<String> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public int mCount;
    @DexIgnore
    public /* final */ kn7 mFileLogWriterMutex; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ yi7 mFileLogWriterScope; // = zi7.a(qj7.b().plus(dl7.a(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    /*
    static {
        String name = FileLogWriter.class.getName();
        ee7.a((Object) name, "FileLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    private final String getFilePath(String str) {
        return this.directoryPath + File.separatorChar + LOG_FOLDER + File.separatorChar + str;
    }

    @DexIgnore
    private final ik7 pollLogEvent() {
        return xh7.b(this.mFileLogWriterScope, null, null, new FileLogWriter$pollLogEvent$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final synchronized void rotateFiles() throws Exception {
        for (int i = 2; i >= 0; i--) {
            we7 we7 = we7.a;
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                FileChannel channel = new RandomAccessFile(file, rw.u).getChannel();
                FileLock lock = channel.lock();
                if (i >= 2) {
                    file.delete();
                } else {
                    we7 we72 = we7.a;
                    String format2 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i + 1)}, 1));
                    ee7.a((Object) format2, "java.lang.String.format(format, *args)");
                    file.renameTo(new File(getFilePath(format2)));
                }
                lock.release();
                channel.close();
            }
        }
        we7 we73 = we7.a;
        String format3 = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{0}, 1));
        ee7.a((Object) format3, "java.lang.String.format(format, *args)");
        new File(getFilePath(format3)).createNewFile();
    }

    @DexIgnore
    public final List<File> exportLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i <= 2; i++) {
            we7 we7 = we7.a;
            String format = String.format(FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{Integer.valueOf(i)}, 1));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            File file = new File(getFilePath(format));
            if (file.exists()) {
                arrayList.add(file);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void startWriter(String str) {
        ee7.b(str, "directoryPath");
        this.directoryPath = str;
        pollLogEvent();
    }

    @DexIgnore
    public final void writeLog(String str) {
        ee7.b(str, "logMessage");
        this.logEventQueue.add(str);
        pollLogEvent();
    }

    @DexIgnore
    public final void startWriter(String str, FileDebugOption fileDebugOption) {
        ee7.b(str, "directoryPath");
        this.debugOption = fileDebugOption;
        this.mCount = 0;
        startWriter(str);
    }
}
