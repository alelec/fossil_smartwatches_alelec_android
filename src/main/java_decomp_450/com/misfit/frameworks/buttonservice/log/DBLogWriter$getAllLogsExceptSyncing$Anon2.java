package com.misfit.frameworks.buttonservice.log;

import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.fe7;
import com.fossil.gd7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.og7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$getAllLogsExceptSyncing$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$getAllLogsExceptSyncing$Anon2 extends zb7 implements kd7<yi7, fb7<? super List<LogEvent>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends fe7 implements gd7<Log, LogEvent> {
        @DexIgnore
        public /* final */ /* synthetic */ Gson $gson;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(Gson gson) {
            super(1);
            this.$gson = gson;
        }

        @DexIgnore
        public final LogEvent invoke(Log log) {
            ee7.b(log, "it");
            try {
                LogEvent logEvent = (LogEvent) this.$gson.a(log.getContent(), LogEvent.class);
                logEvent.setTag(Integer.valueOf(log.getId()));
                return logEvent;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String access$getTAG$cp = DBLogWriter.TAG;
                local.e(access$getTAG$cp, ", getAllLogs(), ex: " + e.getMessage());
                return null;
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$getAllLogsExceptSyncing$Anon2(DBLogWriter dBLogWriter, fb7 fb7) {
        super(2, fb7);
        this.this$0 = dBLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        DBLogWriter$getAllLogsExceptSyncing$Anon2 dBLogWriter$getAllLogsExceptSyncing$Anon2 = new DBLogWriter$getAllLogsExceptSyncing$Anon2(this.this$0, fb7);
        dBLogWriter$getAllLogsExceptSyncing$Anon2.p$ = (yi7) obj;
        return dBLogWriter$getAllLogsExceptSyncing$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super List<LogEvent>> fb7) {
        return ((DBLogWriter$getAllLogsExceptSyncing$Anon2) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            return og7.g(og7.d(ea7.b((Iterable) this.this$0.logDao.getAllLogEventsExcept(Log.Flag.SYNCING)), new Anon1_Level2(new Gson())));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
