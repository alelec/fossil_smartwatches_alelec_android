package com.misfit.frameworks.buttonservice.log;

import android.content.BroadcastReceiver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemoteFLogger$mMessageBroadcastReceiver$Anon1 extends BroadcastReceiver {
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public RemoteFLogger$mMessageBroadcastReceiver$Anon1(RemoteFLogger remoteFLogger) {
        this.this$0 = remoteFLogger;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0089, code lost:
        if ((!com.fossil.ee7.a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00b2, code lost:
        if ((!com.fossil.ee7.a((java.lang.Object) r6.this$0.floggerName, (java.lang.Object) r1)) != false) goto L_0x00b4;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b7 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:53:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceive(android.content.Context r7, android.content.Intent r8) {
        /*
            r6 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r7, r0)
            java.lang.String r7 = "intent"
            com.fossil.ee7.b(r8, r7)
            java.lang.String r7 = "action"
            java.lang.String r7 = r8.getStringExtra(r7)
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$MessageTarget$Companion r0 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.Companion
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$MessageTarget r1 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.MessageTarget.TO_ALL_FLOGGER
            int r1 = r1.getValue()
            java.lang.String r2 = "target"
            int r1 = r8.getIntExtra(r2, r1)
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$MessageTarget r0 = r0.fromValue(r1)
            java.lang.String r1 = "sender"
            boolean r2 = r8.hasExtra(r1)
            if (r2 == 0) goto L_0x0034
            java.lang.String r1 = r8.getStringExtra(r1)
            java.lang.String r2 = "intent.getStringExtra(MESSAGE_SENDER_KEY)"
            com.fossil.ee7.a(r1, r2)
            goto L_0x0036
        L_0x0034:
            java.lang.String r1 = ""
        L_0x0036:
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "mMessageBroadcastReceiver.onReceive(), logAction="
            r4.append(r5)
            r4.append(r7)
            java.lang.String r5 = ", target="
            r4.append(r5)
            r4.append(r0)
            java.lang.String r5 = ", sender="
            r4.append(r5)
            r4.append(r1)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            int[] r2 = com.misfit.frameworks.buttonservice.log.RemoteFLogger.WhenMappings.$EnumSwitchMapping$0
            int r0 = r0.ordinal()
            r0 = r2[r0]
            r2 = 0
            r3 = 1
            if (r0 == r3) goto L_0x009f
            r4 = 2
            if (r0 == r4) goto L_0x0092
            r4 = 3
            if (r0 != r4) goto L_0x008c
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            boolean r0 = r0.isMainFLogger
            if (r0 != 0) goto L_0x00b5
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            java.lang.String r0 = r0.floggerName
            boolean r0 = com.fossil.ee7.a(r0, r1)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x00b5
            goto L_0x00b4
        L_0x008c:
            com.fossil.p87 r7 = new com.fossil.p87
            r7.<init>()
            throw r7
        L_0x0092:
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            java.lang.String r0 = r0.floggerName
            boolean r0 = com.fossil.ee7.a(r0, r1)
            r2 = r0 ^ 1
            goto L_0x00b5
        L_0x009f:
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            boolean r0 = r0.isMainFLogger
            if (r0 == 0) goto L_0x00b5
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            java.lang.String r0 = r0.floggerName
            boolean r0 = com.fossil.ee7.a(r0, r1)
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x00b5
        L_0x00b4:
            r2 = 1
        L_0x00b5:
            if (r2 == 0) goto L_0x015e
            if (r7 != 0) goto L_0x00bb
            goto L_0x015e
        L_0x00bb:
            int r0 = r7.hashCode()
            java.lang.String r1 = "param:summary_key"
            switch(r0) {
                case -1960182802: goto L_0x0144;
                case -1885432660: goto L_0x0130;
                case 308052341: goto L_0x0111;
                case 1513386699: goto L_0x00e5;
                case 1920521161: goto L_0x00c6;
                default: goto L_0x00c4;
            }
        L_0x00c4:
            goto L_0x015e
        L_0x00c6:
            java.lang.String r8 = "action:flush_data"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x015e
            com.fossil.ti7 r7 = com.fossil.qj7.a()
            com.fossil.yi7 r0 = com.fossil.zi7.a(r7)
            r1 = 0
            r2 = 0
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2 r3 = new com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$Anon1$onReceive$Anon4_Level2
            r7 = 0
            r3.<init>(r6, r7)
            r4 = 3
            r5 = 0
            com.fossil.ik7 unused = com.fossil.xh7.b(r0, r1, r2, r3, r4, r5)
            goto L_0x015e
        L_0x00e5:
            java.lang.String r0 = "action:error_recorded"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x015e
            java.lang.String r7 = r8.getStringExtra(r1)
            java.lang.String r0 = "param:error"
            java.lang.String r8 = r8.getStringExtra(r0)
            if (r7 == 0) goto L_0x015e
            if (r8 == 0) goto L_0x015e
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            java.util.HashMap r0 = r0.summarySessionMap
            java.lang.Object r7 = r0.get(r7)
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$SessionSummary r7 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary) r7
            if (r7 == 0) goto L_0x015e
            java.util.List r7 = r7.getErrors()
            r7.add(r8)
            goto L_0x015e
        L_0x0111:
            java.lang.String r0 = "action:start_session"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x015e
            java.lang.String r7 = r8.getStringExtra(r1)
            java.lang.String r0 = "param:serial"
            java.lang.String r8 = r8.getStringExtra(r0)
            if (r7 == 0) goto L_0x015e
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r0 = r6.this$0
            java.lang.String r1 = "serial"
            com.fossil.ee7.a(r8, r1)
            r0.startSession(r7, r8)
            goto L_0x015e
        L_0x0130:
            java.lang.String r8 = "action:full_buffer"
            boolean r7 = r7.equals(r8)
            if (r7 == 0) goto L_0x015e
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r7 = r6.this$0
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r7 = r7.bufferLogWriter
            if (r7 == 0) goto L_0x015e
            r7.forceFlushBuffer()
            goto L_0x015e
        L_0x0144:
            java.lang.String r0 = "action:end_session"
            boolean r7 = r7.equals(r0)
            if (r7 == 0) goto L_0x015e
            java.lang.String r7 = r8.getStringExtra(r1)
            if (r7 == 0) goto L_0x015e
            com.misfit.frameworks.buttonservice.log.RemoteFLogger r8 = r6.this$0
            java.util.HashMap r8 = r8.summarySessionMap
            java.lang.Object r7 = r8.remove(r7)
            com.misfit.frameworks.buttonservice.log.RemoteFLogger$SessionSummary r7 = (com.misfit.frameworks.buttonservice.log.RemoteFLogger.SessionSummary) r7
        L_0x015e:
            return
            switch-data {-1960182802->0x0144, -1885432660->0x0130, 308052341->0x0111, 1513386699->0x00e5, 1920521161->0x00c6, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.RemoteFLogger$mMessageBroadcastReceiver$Anon1.onReceive(android.content.Context, android.content.Intent):void");
    }
}
