package com.misfit.frameworks.buttonservice.log;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$1", f = "BufferLogWriter.kt", l = {191, 80, 99, 104, 109}, m = "invokeSuspend")
public final class BufferLogWriter$pollEventQueue$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public boolean Z$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ BufferLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BufferLogWriter$pollEventQueue$Anon1(BufferLogWriter bufferLogWriter, fb7 fb7) {
        super(2, fb7);
        this.this$0 = bufferLogWriter;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        BufferLogWriter$pollEventQueue$Anon1 bufferLogWriter$pollEventQueue$Anon1 = new BufferLogWriter$pollEventQueue$Anon1(this.this$0, fb7);
        bufferLogWriter$pollEventQueue$Anon1.p$ = (yi7) obj;
        return bufferLogWriter$pollEventQueue$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((BufferLogWriter$pollEventQueue$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:89:0x01e6, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x01e7, code lost:
        com.fossil.hc7.a(r13, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x01eb, code lost:
        throw r0;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:103:0x021c  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0192 A[Catch:{ all -> 0x01e6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x020d A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x020e  */
    @Override // com.fossil.ob7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r17) {
        /*
            r16 = this;
            r1 = r16
            java.lang.Object r0 = com.fossil.nb7.a()
            int r2 = r1.label
            r3 = 5
            r4 = 4
            r5 = 3
            r6 = 2
            r7 = 0
            r8 = 1
            r9 = 0
            if (r2 == 0) goto L_0x00b9
            if (r2 == r8) goto L_0x00ac
            if (r2 == r6) goto L_0x008b
            if (r2 == r5) goto L_0x0064
            if (r2 == r4) goto L_0x0043
            if (r2 != r3) goto L_0x003b
            java.lang.Object r0 = r1.L$4
            com.misfit.frameworks.buttonservice.log.LogEvent r0 = (com.misfit.frameworks.buttonservice.log.LogEvent) r0
            java.lang.Object r0 = r1.L$3
            com.fossil.qe7 r0 = (com.fossil.qe7) r0
            java.lang.Object r0 = r1.L$2
            java.io.File r0 = (java.io.File) r0
            java.lang.Object r0 = r1.L$1
            r2 = r0
            com.fossil.kn7 r2 = (com.fossil.kn7) r2
            java.lang.Object r0 = r1.L$0
            com.fossil.yi7 r0 = (com.fossil.yi7) r0
            com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x0038 }
            goto L_0x0245
        L_0x0035:
            r0 = move-exception
            goto L_0x024d
        L_0x0038:
            r0 = move-exception
            goto L_0x023c
        L_0x003b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r2)
            throw r0
        L_0x0043:
            java.lang.Object r2 = r1.L$4
            com.misfit.frameworks.buttonservice.log.LogEvent r2 = (com.misfit.frameworks.buttonservice.log.LogEvent) r2
            java.lang.Object r4 = r1.L$3
            com.fossil.qe7 r4 = (com.fossil.qe7) r4
            java.lang.Object r5 = r1.L$2
            java.io.File r5 = (java.io.File) r5
            java.lang.Object r6 = r1.L$1
            com.fossil.kn7 r6 = (com.fossil.kn7) r6
            java.lang.Object r7 = r1.L$0
            com.fossil.yi7 r7 = (com.fossil.yi7) r7
            com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x0060, all -> 0x005c }
            goto L_0x0213
        L_0x005c:
            r0 = move-exception
            r2 = r6
            goto L_0x024d
        L_0x0060:
            r0 = move-exception
            r2 = r6
            goto L_0x023c
        L_0x0064:
            java.lang.Object r2 = r1.L$5
            java.io.FileOutputStream r2 = (java.io.FileOutputStream) r2
            boolean r2 = r1.Z$0
            java.lang.Object r5 = r1.L$4
            com.misfit.frameworks.buttonservice.log.LogEvent r5 = (com.misfit.frameworks.buttonservice.log.LogEvent) r5
            java.lang.Object r6 = r1.L$3
            com.fossil.qe7 r6 = (com.fossil.qe7) r6
            java.lang.Object r7 = r1.L$2
            java.io.File r7 = (java.io.File) r7
            java.lang.Object r8 = r1.L$1
            com.fossil.kn7 r8 = (com.fossil.kn7) r8
            java.lang.Object r10 = r1.L$0
            com.fossil.yi7 r10 = (com.fossil.yi7) r10
            com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x0087, all -> 0x0083 }
            goto L_0x01d8
        L_0x0083:
            r0 = move-exception
            r2 = r8
            goto L_0x024d
        L_0x0087:
            r0 = move-exception
            r2 = r8
            goto L_0x023c
        L_0x008b:
            java.lang.Object r2 = r1.L$4
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r2 = r1.L$3
            com.fossil.qe7 r2 = (com.fossil.qe7) r2
            java.lang.Object r6 = r1.L$2
            java.io.File r6 = (java.io.File) r6
            java.lang.Object r10 = r1.L$1
            com.fossil.kn7 r10 = (com.fossil.kn7) r10
            java.lang.Object r11 = r1.L$0
            com.fossil.yi7 r11 = (com.fossil.yi7) r11
            com.fossil.t87.a(r17)     // Catch:{ Exception -> 0x00a8, all -> 0x00a4 }
            goto L_0x0154
        L_0x00a4:
            r0 = move-exception
            r2 = r10
            goto L_0x024d
        L_0x00a8:
            r0 = move-exception
            r2 = r10
            goto L_0x023c
        L_0x00ac:
            java.lang.Object r2 = r1.L$1
            com.fossil.kn7 r2 = (com.fossil.kn7) r2
            java.lang.Object r10 = r1.L$0
            com.fossil.yi7 r10 = (com.fossil.yi7) r10
            com.fossil.t87.a(r17)
            r11 = r10
            goto L_0x00d3
        L_0x00b9:
            com.fossil.t87.a(r17)
            com.fossil.yi7 r2 = r1.p$
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r10 = r1.this$0
            com.fossil.kn7 r10 = r10.mBufferLogMutex
            r1.L$0 = r2
            r1.L$1 = r10
            r1.label = r8
            java.lang.Object r11 = r10.a(r9, r1)
            if (r11 != r0) goto L_0x00d1
            return r0
        L_0x00d1:
            r11 = r2
            r2 = r10
        L_0x00d3:
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r10 = r1.this$0     // Catch:{ all -> 0x0035 }
            java.lang.String r10 = r10.logFilePath     // Catch:{ all -> 0x0035 }
            boolean r10 = com.fossil.mh7.a(r10)     // Catch:{ all -> 0x0035 }
            if (r10 != 0) goto L_0x0245
            java.io.File r10 = new java.io.File
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r12 = r1.this$0
            java.lang.String r12 = r12.logFilePath
            r10.<init>(r12)
            com.fossil.qe7 r12 = new com.fossil.qe7
            r12.<init>()
            java.io.File r13 = r10.getParentFile()
            if (r13 == 0) goto L_0x00fe
            boolean r13 = r13.exists()
            java.lang.Boolean r13 = com.fossil.pb7.a(r13)
            goto L_0x00ff
        L_0x00fe:
            r13 = r9
        L_0x00ff:
            if (r13 == 0) goto L_0x0238
            boolean r13 = r13.booleanValue()
            if (r13 != 0) goto L_0x0115
            java.io.File r13 = r10.getParentFile()
            if (r13 == 0) goto L_0x0111
            r13.mkdirs()
            goto L_0x0115
        L_0x0111:
            com.fossil.ee7.a()
            throw r9
        L_0x0115:
            boolean r13 = r10.exists()
            if (r13 != 0) goto L_0x0121
            r10.createNewFile()
            r12.element = r7
            goto L_0x015b
        L_0x0121:
            java.nio.charset.Charset r13 = java.nio.charset.Charset.defaultCharset()
            java.util.List r13 = com.fossil.r04.b(r10, r13)
            java.lang.String r14 = "logLines"
            com.fossil.ee7.a(r13, r14)
            int r14 = r13.size()
            r12.element = r14
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r15 = r1.this$0
            int r15 = r15.threshold
            if (r14 < r15) goto L_0x015b
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r14 = r1.this$0
            r1.L$0 = r11
            r1.L$1 = r2
            r1.L$2 = r10
            r1.L$3 = r12
            r1.L$4 = r13
            r1.label = r6
            java.lang.Object r6 = r14.renameToFullBufferFile(r10, r7, r1)
            if (r6 != r0) goto L_0x0151
            return r0
        L_0x0151:
            r6 = r10
            r10 = r2
            r2 = r12
        L_0x0154:
            r2.element = r7
            r7 = r6
            r6 = r2
            r2 = r10
            r10 = r11
            goto L_0x015e
        L_0x015b:
            r7 = r10
            r10 = r11
            r6 = r12
        L_0x015e:
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r11 = r1.this$0
            com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue r11 = r11.logEventQueue
            java.lang.Object r11 = r11.poll()
            com.misfit.frameworks.buttonservice.log.LogEvent r11 = (com.misfit.frameworks.buttonservice.log.LogEvent) r11
            boolean r12 = r11 instanceof com.misfit.frameworks.buttonservice.log.FlushLogEvent
            if (r11 == 0) goto L_0x01ec
            boolean r13 = r11 instanceof com.misfit.frameworks.buttonservice.log.FinishLogEvent
            if (r13 != 0) goto L_0x01ec
            if (r12 != 0) goto L_0x01ec
            java.io.FileOutputStream r13 = new java.io.FileOutputStream
            r13.<init>(r7, r8)
            java.lang.String r8 = r11.toString()     // Catch:{ all -> 0x01e3 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e3 }
            r14.<init>()     // Catch:{ all -> 0x01e3 }
            r14.append(r8)     // Catch:{ all -> 0x01e3 }
            java.lang.String r8 = "\n"
            r14.append(r8)     // Catch:{ all -> 0x01e3 }
            java.lang.String r8 = r14.toString()     // Catch:{ all -> 0x01e3 }
            java.nio.charset.Charset r14 = com.fossil.sg7.a     // Catch:{ all -> 0x01e3 }
            if (r8 == 0) goto L_0x01db
            byte[] r8 = r8.getBytes(r14)     // Catch:{ all -> 0x01e3 }
            java.lang.String r14 = "(this as java.lang.String).getBytes(charset)"
            com.fossil.ee7.a(r8, r14)     // Catch:{ all -> 0x01e3 }
            r13.write(r8)     // Catch:{ all -> 0x01e3 }
            r13.flush()     // Catch:{ all -> 0x01e3 }
            int r8 = r6.element     // Catch:{ all -> 0x01e3 }
            int r14 = r8 + 1
            r6.element = r14     // Catch:{ all -> 0x01e3 }
            com.fossil.pb7.a(r8)     // Catch:{ all -> 0x01e3 }
            com.fossil.hc7.a(r13, r9)
            com.misfit.frameworks.buttonservice.log.FLogger$LogLevel r8 = r11.getLogLevel()
            com.misfit.frameworks.buttonservice.log.FLogger$LogLevel r14 = com.misfit.frameworks.buttonservice.log.FLogger.LogLevel.SUMMARY
            if (r8 != r14) goto L_0x01ec
            com.fossil.ti7 r8 = com.fossil.qj7.a()
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2 r14 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon1_Level2
            r14.<init>(r11, r9, r1)
            r1.L$0 = r10
            r1.L$1 = r2
            r1.L$2 = r7
            r1.L$3 = r6
            r1.L$4 = r11
            r1.Z$0 = r12
            r1.L$5 = r13
            r1.label = r5
            java.lang.Object r5 = com.fossil.vh7.a(r8, r14, r1)
            if (r5 != r0) goto L_0x01d5
            return r0
        L_0x01d5:
            r8 = r2
            r5 = r11
            r2 = r12
        L_0x01d8:
            r12 = r2
            r2 = r8
            goto L_0x01ed
        L_0x01db:
            com.fossil.x87 r0 = new com.fossil.x87
            java.lang.String r3 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r3)
            throw r0
        L_0x01e3:
            r0 = move-exception
            r3 = r0
            throw r3     // Catch:{ all -> 0x01e6 }
        L_0x01e6:
            r0 = move-exception
            r4 = r0
            com.fossil.hc7.a(r13, r3)
            throw r4
        L_0x01ec:
            r5 = r11
        L_0x01ed:
            int r8 = r6.element
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r11 = r1.this$0
            int r11 = r11.threshold
            if (r8 >= r11) goto L_0x01f9
            if (r12 == 0) goto L_0x0218
        L_0x01f9:
            com.misfit.frameworks.buttonservice.log.BufferLogWriter r8 = r1.this$0
            r1.L$0 = r10
            r1.L$1 = r2
            r1.L$2 = r7
            r1.L$3 = r6
            r1.L$4 = r5
            r1.label = r4
            java.lang.Object r4 = r8.renameToFullBufferFile(r7, r12, r1)
            if (r4 != r0) goto L_0x020e
            return r0
        L_0x020e:
            r4 = r6
            r6 = r2
            r2 = r5
            r5 = r7
            r7 = r10
        L_0x0213:
            r10 = r7
            r7 = r5
            r5 = r2
            r2 = r6
            r6 = r4
        L_0x0218:
            boolean r4 = r5 instanceof com.misfit.frameworks.buttonservice.log.FinishLogEvent
            if (r4 == 0) goto L_0x0245
            com.fossil.ti7 r4 = com.fossil.qj7.a()
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2 r8 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1$invokeSuspend$$inlined$withLock$lambda$Anon2_Level2
            r8.<init>(r9, r1)
            r1.L$0 = r10
            r1.L$1 = r2
            r1.L$2 = r7
            r1.L$3 = r6
            r1.L$4 = r5
            r1.label = r3
            java.lang.Object r3 = com.fossil.vh7.a(r4, r8, r1)
            if (r3 != r0) goto L_0x0245
            return r0
        L_0x0238:
            com.fossil.ee7.a()
            throw r9
        L_0x023c:
            java.lang.String r0 = r0.toString()
            java.io.PrintStream r3 = java.lang.System.out
            r3.print(r0)
        L_0x0245:
            com.fossil.i97 r0 = com.fossil.i97.a
            r2.a(r9)
            com.fossil.i97 r0 = com.fossil.i97.a
            return r0
        L_0x024d:
            r2.a(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.BufferLogWriter$pollEventQueue$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
