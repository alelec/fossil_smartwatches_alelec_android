package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.fossil.bi;
import com.fossil.ci;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.ik7;
import com.fossil.kn7;
import com.fossil.mn7;
import com.fossil.nb7;
import com.fossil.og7;
import com.fossil.pb7;
import com.fossil.qj7;
import com.fossil.vh7;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.db.DBConstants;
import com.misfit.frameworks.buttonservice.log.db.Log;
import com.misfit.frameworks.buttonservice.log.db.LogDao;
import com.misfit.frameworks.buttonservice.log.db.LogDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int LIMIT_INPUT_IDS_PARAM; // = 500;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ IDBLogWriterCallback callback;
    @DexIgnore
    public /* final */ LogDao logDao;
    @DexIgnore
    public /* final */ kn7 mMutex; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ int thresholdValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface IDBLogWriterCallback {
        @DexIgnore
        void onDeleteLogs();

        @DexIgnore
        void onReachDBThreshold();
    }

    /*
    static {
        String simpleName = DBLogWriter.class.getSimpleName();
        ee7.a((Object) simpleName, "DBLogWriter::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DBLogWriter(Context context, int i, IDBLogWriterCallback iDBLogWriterCallback) {
        ee7.b(context, "context");
        this.thresholdValue = i;
        this.callback = iDBLogWriterCallback;
        ci.a a = bi.a(context, LogDatabase.class, DBConstants.LOG_DB_NAME);
        a.d();
        this.logDao = ((LogDatabase) a.b()).getLogDao();
    }

    @DexIgnore
    public final /* synthetic */ Object deleteLogs(List<Integer> list, fb7<? super Integer> fb7) {
        if (!list.isEmpty()) {
            return vh7.a(qj7.a(), new DBLogWriter$deleteLogs$Anon2(this, list, null), fb7);
        }
        return pb7.a(0);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00fc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0139  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0162 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0163  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x01b1 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01f0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01f1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r14, com.fossil.fb7<? super com.fossil.i97> r15) {
        /*
            r13 = this;
            boolean r0 = r15 instanceof com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon1
            if (r0 == 0) goto L_0x0013
            r0 = r15
            com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon1 r0 = (com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon1) r0
            int r1 = r0.label
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = r1 & r2
            if (r3 == 0) goto L_0x0013
            int r1 = r1 - r2
            r0.label = r1
            goto L_0x0018
        L_0x0013:
            com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon1 r0 = new com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon1
            r0.<init>(r13, r15)
        L_0x0018:
            java.lang.Object r15 = r0.result
            java.lang.Object r1 = com.fossil.nb7.a()
            int r2 = r0.label
            r3 = 1
            r4 = 0
            switch(r2) {
                case 0: goto L_0x00d8;
                case 1: goto L_0x00c6;
                case 2: goto L_0x00b1;
                case 3: goto L_0x0092;
                case 4: goto L_0x0073;
                case 5: goto L_0x004e;
                case 6: goto L_0x002d;
                default: goto L_0x0025;
            }
        L_0x0025:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r15 = "call to 'resume' before 'invoke' with coroutine"
            r14.<init>(r15)
            throw r14
        L_0x002d:
            java.lang.Object r14 = r0.L$6
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r14 = r0.L$5
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r14 = r0.L$4
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r14 = r0.L$3
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r14 = r0.L$2
            com.fossil.kn7 r14 = (com.fossil.kn7) r14
            java.lang.Object r1 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r1 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r1
            java.lang.Object r0 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r0 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r0
            com.fossil.t87.a(r15)     // Catch:{ all -> 0x00c3 }
            goto L_0x01f2
        L_0x004e:
            java.lang.Object r14 = r0.L$6
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r2 = r0.L$5
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r0.L$4
            java.util.List r3 = (java.util.List) r3
            java.lang.Object r5 = r0.L$3
            java.util.List r5 = (java.util.List) r5
            java.lang.Object r6 = r0.L$2
            com.fossil.kn7 r6 = (com.fossil.kn7) r6
            java.lang.Object r7 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r7 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r7
            java.lang.Object r8 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r8 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r8
            com.fossil.t87.a(r15)     // Catch:{ all -> 0x006f }
            goto L_0x01b4
        L_0x006f:
            r15 = move-exception
            r14 = r6
            goto L_0x01fe
        L_0x0073:
            java.lang.Object r14 = r0.L$4
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r2 = r0.L$3
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r0.L$2
            com.fossil.kn7 r3 = (com.fossil.kn7) r3
            java.lang.Object r5 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r5 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r5
            java.lang.Object r6 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r6 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r6
            com.fossil.t87.a(r15)     // Catch:{ all -> 0x00ad }
            r7 = r5
            r8 = r6
            r5 = r2
            r12 = r3
            r3 = r14
            r14 = r12
            goto L_0x0169
        L_0x0092:
            java.lang.Object r14 = r0.L$4
            java.util.List r14 = (java.util.List) r14
            java.lang.Object r2 = r0.L$3
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r0.L$2
            com.fossil.kn7 r3 = (com.fossil.kn7) r3
            java.lang.Object r5 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r5 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r5
            java.lang.Object r6 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r6 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r6
            com.fossil.t87.a(r15)
            r15 = r14
            r14 = r3
            goto L_0x014f
        L_0x00ad:
            r15 = move-exception
            r14 = r3
            goto L_0x01fe
        L_0x00b1:
            java.lang.Object r14 = r0.L$2
            com.fossil.kn7 r14 = (com.fossil.kn7) r14
            java.lang.Object r2 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r2 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r2
            java.lang.Object r5 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r5 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r5
            com.fossil.t87.a(r15)
            r6 = r5
            r5 = r2
            goto L_0x0101
        L_0x00c3:
            r15 = move-exception
            goto L_0x01fe
        L_0x00c6:
            java.lang.Object r14 = r0.L$2
            com.fossil.kn7 r14 = (com.fossil.kn7) r14
            java.lang.Object r2 = r0.L$1
            com.misfit.frameworks.buttonservice.log.IRemoteLogWriter r2 = (com.misfit.frameworks.buttonservice.log.IRemoteLogWriter) r2
            java.lang.Object r5 = r0.L$0
            com.misfit.frameworks.buttonservice.log.DBLogWriter r5 = (com.misfit.frameworks.buttonservice.log.DBLogWriter) r5
            com.fossil.t87.a(r15)
            r15 = r14
            r14 = r2
            goto L_0x00ed
        L_0x00d8:
            com.fossil.t87.a(r15)
            com.fossil.kn7 r15 = r13.mMutex
            r0.L$0 = r13
            r0.L$1 = r14
            r0.L$2 = r15
            r0.label = r3
            java.lang.Object r2 = r15.a(r4, r0)
            if (r2 != r1) goto L_0x00ec
            return r1
        L_0x00ec:
            r5 = r13
        L_0x00ed:
            r0.L$0 = r5     // Catch:{ all -> 0x01fa }
            r0.L$1 = r14     // Catch:{ all -> 0x01fa }
            r0.L$2 = r15     // Catch:{ all -> 0x01fa }
            r2 = 2
            r0.label = r2     // Catch:{ all -> 0x01fa }
            java.lang.Object r2 = r5.getAllLogsExceptSyncing(r0)     // Catch:{ all -> 0x01fa }
            if (r2 != r1) goto L_0x00fd
            return r1
        L_0x00fd:
            r6 = r5
            r5 = r14
            r14 = r15
            r15 = r2
        L_0x0101:
            r2 = r15
            java.util.List r2 = (java.util.List) r2
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r7 = com.misfit.frameworks.buttonservice.log.DBLogWriter.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = ".flush(), logEventSize="
            r8.append(r9)
            int r9 = r2.size()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r15.d(r7, r8)
            com.fossil.hg7 r15 = com.fossil.ea7.b(r2)
            com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2 r7 = com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$syncingLogIds$Anon1_Level2.INSTANCE
            com.fossil.hg7 r15 = com.fossil.og7.d(r15, r7)
            java.util.List r15 = com.fossil.og7.g(r15)
            boolean r7 = r15.isEmpty()
            r3 = r3 ^ r7
            if (r3 == 0) goto L_0x01f2
            com.misfit.frameworks.buttonservice.log.db.Log$Flag r3 = com.misfit.frameworks.buttonservice.log.db.Log.Flag.SYNCING
            r0.L$0 = r6
            r0.L$1 = r5
            r0.L$2 = r14
            r0.L$3 = r2
            r0.L$4 = r15
            r7 = 3
            r0.label = r7
            java.lang.Object r3 = r6.updateCloudFlag(r15, r3, r0)
            if (r3 != r1) goto L_0x014f
            return r1
        L_0x014f:
            r0.L$0 = r6
            r0.L$1 = r5
            r0.L$2 = r14
            r0.L$3 = r2
            r0.L$4 = r15
            r3 = 4
            r0.label = r3
            java.lang.Object r3 = r5.sendLog(r2, r0)
            if (r3 != r1) goto L_0x0163
            return r1
        L_0x0163:
            r7 = r5
            r8 = r6
            r5 = r2
            r12 = r3
            r3 = r15
            r15 = r12
        L_0x0169:
            r2 = r15
            java.util.List r2 = (java.util.List) r2
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r6 = com.misfit.frameworks.buttonservice.log.DBLogWriter.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = ".flush(), sentLog="
            r9.append(r10)
            int r10 = r2.size()
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            r15.d(r6, r9)
            com.fossil.hg7 r15 = com.fossil.ea7.b(r2)
            com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2 r6 = com.misfit.frameworks.buttonservice.log.DBLogWriter$flushTo$Anon2$sentLogIds$Anon1_Level2.INSTANCE
            com.fossil.hg7 r15 = com.fossil.og7.d(r15, r6)
            java.util.List r15 = com.fossil.og7.g(r15)
            r0.L$0 = r8
            r0.L$1 = r7
            r0.L$2 = r14
            r0.L$3 = r5
            r0.L$4 = r3
            r0.L$5 = r2
            r0.L$6 = r15
            r6 = 5
            r0.label = r6
            java.lang.Object r6 = r8.deleteLogs(r15, r0)
            if (r6 != r1) goto L_0x01b2
            return r1
        L_0x01b2:
            r6 = r14
            r14 = r15
        L_0x01b4:
            r3.removeAll(r14)
            com.misfit.frameworks.buttonservice.log.FLogger r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r15 = r15.getLocal()
            java.lang.String r9 = com.misfit.frameworks.buttonservice.log.DBLogWriter.TAG
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = ".flush(), syncingLogs="
            r10.append(r11)
            int r11 = r3.size()
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            r15.d(r9, r10)
            com.misfit.frameworks.buttonservice.log.db.Log$Flag r15 = com.misfit.frameworks.buttonservice.log.db.Log.Flag.ADD
            r0.L$0 = r8
            r0.L$1 = r7
            r0.L$2 = r6
            r0.L$3 = r5
            r0.L$4 = r3
            r0.L$5 = r2
            r0.L$6 = r14
            r14 = 6
            r0.label = r14
            java.lang.Object r14 = r8.updateCloudFlag(r3, r15, r0)
            if (r14 != r1) goto L_0x01f1
            return r1
        L_0x01f1:
            r14 = r6
        L_0x01f2:
            com.fossil.i97 r15 = com.fossil.i97.a
            r14.a(r4)
            com.fossil.i97 r14 = com.fossil.i97.a
            return r14
        L_0x01fa:
            r14 = move-exception
            r12 = r15
            r15 = r14
            r14 = r12
        L_0x01fe:
            r14.a(r4)
            throw r15
            switch-data {0->0x00d8, 1->0x00c6, 2->0x00b1, 3->0x0092, 4->0x0073, 5->0x004e, 6->0x002d, }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.DBLogWriter.flushTo(com.misfit.frameworks.buttonservice.log.IRemoteLogWriter, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object getAllLogsExceptSyncing(fb7<? super List<LogEvent>> fb7) {
        return vh7.a(qj7.a(), new DBLogWriter$getAllLogsExceptSyncing$Anon2(this, null), fb7);
    }

    @DexIgnore
    public final /* synthetic */ Object updateCloudFlag(List<Integer> list, Log.Flag flag, fb7<? super i97> fb7) {
        Object a = vh7.a(qj7.a(), new DBLogWriter$updateCloudFlag$Anon2(this, list, flag, null), fb7);
        return a == nb7.a() ? a : i97.a;
    }

    @DexIgnore
    public final synchronized void writeLog(List<LogEvent> list) {
        ee7.b(list, "logEvents");
        this.logDao.insertLogEvent(og7.g(og7.c(ea7.b((Iterable) list), DBLogWriter$writeLog$Anon1.INSTANCE)));
        ik7 unused = xh7.b(zi7.a(qj7.a()), null, null, new DBLogWriter$writeLog$Anon2(this, null), 3, null);
    }
}
