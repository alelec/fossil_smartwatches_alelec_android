package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$onFullBuffer$1", f = "RemoteFLogger.kt", l = {157}, m = "invokeSuspend")
public final class RemoteFLogger$onFullBuffer$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $forceFlush;
    @DexIgnore
    public /* final */ /* synthetic */ List $logLines;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$onFullBuffer$Anon1(RemoteFLogger remoteFLogger, List list, boolean z, fb7 fb7) {
        super(2, fb7);
        this.this$0 = remoteFLogger;
        this.$logLines = list;
        this.$forceFlush = z;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        RemoteFLogger$onFullBuffer$Anon1 remoteFLogger$onFullBuffer$Anon1 = new RemoteFLogger$onFullBuffer$Anon1(this.this$0, this.$logLines, this.$forceFlush, fb7);
        remoteFLogger$onFullBuffer$Anon1.p$ = (yi7) obj;
        return remoteFLogger$onFullBuffer$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((RemoteFLogger$onFullBuffer$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        Object a = nb7.a();
        int i = this.label;
        if (i == 0) {
            t87.a(obj);
            yi7 yi7 = this.p$;
            if (this.this$0.isMainFLogger) {
                DBLogWriter access$getDbLogWriter$p = this.this$0.dbLogWriter;
                if (access$getDbLogWriter$p != null) {
                    access$getDbLogWriter$p.writeLog(this.$logLines);
                }
                if (this.$forceFlush) {
                    RemoteFLogger remoteFLogger = this.this$0;
                    this.L$0 = yi7;
                    this.label = 1;
                    if (remoteFLogger.flushDB(this) == a) {
                        return a;
                    }
                }
            } else if (!this.$forceFlush) {
                FLogger.INSTANCE.getLocal().d(RemoteFLogger.TAG, ".onFullBuffer(), broadcast");
                RemoteFLogger remoteFLogger2 = this.this$0;
                String access$getFloggerName$p = remoteFLogger2.floggerName;
                RemoteFLogger.MessageTarget messageTarget = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle = Bundle.EMPTY;
                ee7.a((Object) bundle, "Bundle.EMPTY");
                remoteFLogger2.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FULL_BUFFER, access$getFloggerName$p, messageTarget, bundle);
            } else {
                RemoteFLogger remoteFLogger3 = this.this$0;
                String access$getFloggerName$p2 = remoteFLogger3.floggerName;
                RemoteFLogger.MessageTarget messageTarget2 = RemoteFLogger.MessageTarget.TO_MAIN_FLOGGER;
                Bundle bundle2 = Bundle.EMPTY;
                ee7.a((Object) bundle2, "Bundle.EMPTY");
                remoteFLogger3.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, access$getFloggerName$p2, messageTarget2, bundle2);
            }
        } else if (i == 1) {
            yi7 yi72 = (yi7) this.L$0;
            t87.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return i97.a;
    }
}
