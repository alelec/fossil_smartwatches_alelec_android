package com.misfit.frameworks.buttonservice.log.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.ButtonService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppLogInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ String appVersion;
    @DexIgnore
    public /* final */ String phoneID;
    @DexIgnore
    public /* final */ String phoneModel;
    @DexIgnore
    public /* final */ String platform;
    @DexIgnore
    public /* final */ String platformVersion;
    @DexIgnore
    public /* final */ String sdkVersion;
    @DexIgnore
    public /* final */ String userId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppLogInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppLogInfo createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new AppLogInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppLogInfo[] newArray(int i) {
            return new AppLogInfo[i];
        }
    }

    @DexIgnore
    public AppLogInfo(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        ee7.b(str, ButtonService.USER_ID);
        ee7.b(str2, "appVersion");
        ee7.b(str3, "platform");
        ee7.b(str4, "platformVersion");
        ee7.b(str5, "phoneID");
        ee7.b(str6, "sdkVersion");
        ee7.b(str7, "phoneModel");
        this.userId = str;
        this.appVersion = str2;
        this.platform = str3;
        this.platformVersion = str4;
        this.phoneID = str5;
        this.sdkVersion = str6;
        this.phoneModel = str7;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getAppVersion() {
        return this.appVersion;
    }

    @DexIgnore
    public final String getPhoneID() {
        return this.phoneID;
    }

    @DexIgnore
    public final String getPhoneModel() {
        return this.phoneModel;
    }

    @DexIgnore
    public final String getPlatform() {
        return this.platform;
    }

    @DexIgnore
    public final String getPlatformVersion() {
        return this.platformVersion;
    }

    @DexIgnore
    public final String getSdkVersion() {
        return this.sdkVersion;
    }

    @DexIgnore
    public final String getUserId() {
        return this.userId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.userId);
        parcel.writeString(this.appVersion);
        parcel.writeString(this.platform);
        parcel.writeString(this.platformVersion);
        parcel.writeString(this.phoneID);
        parcel.writeString(this.sdkVersion);
        parcel.writeString(this.phoneModel);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppLogInfo(android.os.Parcel r11) {
        /*
            r10 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r11, r0)
            java.lang.String r0 = r11.readString()
            java.lang.String r1 = ""
            if (r0 == 0) goto L_0x000f
            r3 = r0
            goto L_0x0010
        L_0x000f:
            r3 = r1
        L_0x0010:
            java.lang.String r0 = r11.readString()
            if (r0 == 0) goto L_0x0018
            r4 = r0
            goto L_0x0019
        L_0x0018:
            r4 = r1
        L_0x0019:
            java.lang.String r0 = r11.readString()
            if (r0 == 0) goto L_0x0021
            r5 = r0
            goto L_0x0022
        L_0x0021:
            r5 = r1
        L_0x0022:
            java.lang.String r0 = r11.readString()
            if (r0 == 0) goto L_0x002a
            r6 = r0
            goto L_0x002b
        L_0x002a:
            r6 = r1
        L_0x002b:
            java.lang.String r0 = r11.readString()
            if (r0 == 0) goto L_0x0033
            r7 = r0
            goto L_0x0034
        L_0x0033:
            r7 = r1
        L_0x0034:
            java.lang.String r0 = r11.readString()
            if (r0 == 0) goto L_0x003c
            r8 = r0
            goto L_0x003d
        L_0x003c:
            r8 = r1
        L_0x003d:
            java.lang.String r11 = r11.readString()
            if (r11 == 0) goto L_0x0045
            r9 = r11
            goto L_0x0046
        L_0x0045:
            r9 = r1
        L_0x0046:
            r2 = r10
            r2.<init>(r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.model.AppLogInfo.<init>(android.os.Parcel):void");
    }
}
