package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.yi7;
import com.fossil.zb7;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1", f = "RemoteFLogger.kt", l = {}, m = "invokeSuspend")
public final class RemoteFLogger$flush$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$Anon1(RemoteFLogger remoteFLogger, fb7 fb7) {
        super(2, fb7);
        this.this$0 = remoteFLogger;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        RemoteFLogger$flush$Anon1 remoteFLogger$flush$Anon1 = new RemoteFLogger$flush$Anon1(this.this$0, fb7);
        remoteFLogger$flush$Anon1.p$ = (yi7) obj;
        return remoteFLogger$flush$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((RemoteFLogger$flush$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            if (this.this$0.isMainFLogger && !this.this$0.isFlushing) {
                this.this$0.isFlushing = true;
                RemoteFLogger remoteFLogger = this.this$0;
                remoteFLogger.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new Bundle());
                this.this$0.isFlushing = false;
            }
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
