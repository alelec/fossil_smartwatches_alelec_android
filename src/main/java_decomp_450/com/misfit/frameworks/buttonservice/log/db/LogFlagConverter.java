package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogFlagConverter {
    @DexIgnore
    public final String logFlagEnumToString(Log.Flag flag) {
        ee7.b(flag, "value");
        return flag.getValue();
    }

    @DexIgnore
    public final Log.Flag stringToLogFlag(String str) {
        Log.Flag fromValue;
        return (str == null || (fromValue = Log.Flag.Companion.fromValue(str)) == null) ? Log.Flag.ADD : fromValue;
    }
}
