package com.misfit.frameworks.buttonservice.log;

import com.fossil.dl7;
import com.fossil.ea7;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.kn7;
import com.fossil.mh7;
import com.fossil.mn7;
import com.fossil.og7;
import com.fossil.qj7;
import com.fossil.t97;
import com.fossil.we7;
import com.fossil.xh7;
import com.fossil.yi7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ kn7 mBufferLogMutex; // = mn7.a(false, 1, null);
    @DexIgnore
    public /* final */ yi7 mBufferLogScope; // = zi7.a(qj7.b().plus(dl7.a(null, 1, null)));
    @DexIgnore
    public /* final */ int threshold;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        ee7.a((Object) name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        ee7.b(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        we7 we7 = we7.a;
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(new Object[]{this.floggerName, Long.valueOf(j), Locale.US}, 3));
        ee7.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final ik7 pollEventQueue() {
        return xh7.b(this.mBufferLogScope, null, null, new BufferLogWriter$pollEventQueue$Anon1(this, null), 3, null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return og7.g(og7.d(ea7.b((Iterable) list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    @DexIgnore
    public final List<File> exportLogs() {
        List<File> i;
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        return (listFiles == null || (i = t97.i(listFiles)) == null) ? new ArrayList() : i;
    }

    @DexIgnore
    public final void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    @DexIgnore
    public final float getLogFileSize() {
        return ConversionUtils.INSTANCE.convertByteToMegaBytes(new File(this.logFilePath).length());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0088, code lost:
        if (r3 != null) goto L_0x0090;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object renameToFullBufferFile(java.io.File r12, boolean r13, com.fossil.fb7<? super com.fossil.i97> r14) {
        /*
            r11 = this;
            java.io.File r14 = new java.io.File
            java.util.Calendar r0 = java.util.Calendar.getInstance()
            java.lang.String r1 = "Calendar.getInstance()"
            com.fossil.ee7.a(r0, r1)
            long r0 = r0.getTimeInMillis()
            java.lang.String r0 = r11.getFullBufferLogFileName(r0)
            r14.<init>(r0)
            r12.renameTo(r14)
            java.io.File r12 = new java.io.File
            java.lang.String r14 = r11.directoryPath
            r12.<init>(r14)
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$founds$Anon1 r14 = com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$founds$Anon1.INSTANCE
            java.io.File[] r12 = r12.listFiles(r14)
            boolean r14 = r11.isMainFLogger
            java.lang.String r0 = "onFullBuffer delegate callback"
            r1 = 0
            r2 = 1
            if (r14 == 0) goto L_0x00d9
            if (r12 == 0) goto L_0x003b
            int r14 = r12.length
            if (r14 <= r2) goto L_0x003b
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$$inlined$sortBy$Anon1 r14 = new com.misfit.frameworks.buttonservice.log.BufferLogWriter$renameToFullBufferFile$$inlined$sortBy$Anon1
            r14.<init>()
            com.fossil.s97.a(r12, r14)
        L_0x003b:
            java.lang.String r14 = ", ex="
            java.lang.String r2 = "it"
            if (r12 == 0) goto L_0x008b
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            int r4 = r12.length
            r5 = 0
        L_0x0048:
            if (r5 >= r4) goto L_0x0084
            r6 = r12[r5]
            java.nio.charset.Charset r7 = java.nio.charset.Charset.defaultCharset()     // Catch:{ Exception -> 0x0055 }
            java.util.List r6 = com.fossil.r04.b(r6, r7)     // Catch:{ Exception -> 0x0055 }
            goto L_0x007e
        L_0x0055:
            r7 = move-exception
            java.lang.String r8 = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = ".renameToFullBufferFile(), read buffer file, name="
            r9.append(r10)
            com.fossil.ee7.a(r6, r2)
            java.lang.String r6 = r6.getName()
            r9.append(r6)
            r9.append(r14)
            r9.append(r7)
            java.lang.String r6 = r9.toString()
            android.util.Log.e(r8, r6)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
        L_0x007e:
            com.fossil.ba7.a(r3, r6)
            int r5 = r5 + 1
            goto L_0x0048
        L_0x0084:
            java.util.List r3 = com.fossil.ea7.d(r3)
            if (r3 == 0) goto L_0x008b
            goto L_0x0090
        L_0x008b:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
        L_0x0090:
            if (r12 == 0) goto L_0x00c2
            int r4 = r12.length
        L_0x0093:
            if (r1 >= r4) goto L_0x00c2
            r5 = r12[r1]
            r5.delete()     // Catch:{ Exception -> 0x009b }
            goto L_0x00bf
        L_0x009b:
            r6 = move-exception
            java.lang.String r7 = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = ".renameToFullBufferFile(), delete buffer file, name="
            r8.append(r9)
            com.fossil.ee7.a(r5, r2)
            java.lang.String r5 = r5.getName()
            r8.append(r5)
            r8.append(r14)
            r8.append(r6)
            java.lang.String r5 = r8.toString()
            android.util.Log.e(r7, r5)
        L_0x00bf:
            int r1 = r1 + 1
            goto L_0x0093
        L_0x00c2:
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r14 = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG
            r12.d(r14, r0)
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$IBufferLogCallback r12 = r11.callback
            if (r12 == 0) goto L_0x0107
            java.util.List r14 = r11.toLogEvents(r3)
            r12.onFullBuffer(r14, r13)
            goto L_0x0107
        L_0x00d9:
            r14 = 0
            if (r12 == 0) goto L_0x00e7
            int r12 = r12.length
            if (r12 != 0) goto L_0x00e0
            r1 = 1
        L_0x00e0:
            r12 = r1 ^ 1
            java.lang.Boolean r12 = com.fossil.pb7.a(r12)
            goto L_0x00e8
        L_0x00e7:
            r12 = r14
        L_0x00e8:
            if (r12 == 0) goto L_0x010a
            boolean r12 = r12.booleanValue()
            if (r12 == 0) goto L_0x0107
            com.misfit.frameworks.buttonservice.log.FLogger r12 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r12 = r12.getLocal()
            java.lang.String r14 = com.misfit.frameworks.buttonservice.log.BufferLogWriter.TAG
            r12.d(r14, r0)
            com.misfit.frameworks.buttonservice.log.BufferLogWriter$IBufferLogCallback r12 = r11.callback
            if (r12 == 0) goto L_0x0107
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            r12.onFullBuffer(r14, r13)
        L_0x0107:
            com.fossil.i97 r12 = com.fossil.i97.a
            return r12
        L_0x010a:
            com.fossil.ee7.a()
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.BufferLogWriter.renameToFullBufferFile(java.io.File, boolean, com.fossil.fb7):java.lang.Object");
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        ee7.b(str, "directoryPath");
        ee7.b(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        String str3 = str + File.separatorChar + LOG_FOLDER;
        this.directoryPath = str3;
        if (!mh7.a((CharSequence) str3)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.directoryPath);
            sb.append(File.separatorChar);
            we7 we7 = we7.a;
            String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(new Object[]{this.floggerName, Locale.US}, 2));
            ee7.a((Object) format, "java.lang.String.format(format, *args)");
            sb.append(format);
            str2 = sb.toString();
        } else {
            str2 = "";
        }
        this.logFilePath = str2;
        this.isMainFLogger = z;
        pollEventQueue();
    }

    @DexIgnore
    public final void writeLog(LogEvent logEvent) {
        ee7.b(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        pollEventQueue();
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        ee7.b(str, "directoryPath");
        ee7.b(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }
}
