package com.misfit.frameworks.buttonservice.log.cloud;

import com.fossil.ee7;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.log.IRemoteLogWriter;
import com.misfit.frameworks.buttonservice.log.LogApiService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudLogWriter implements IRemoteLogWriter {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String ITEMS_PARAM; // = "_items";
    @DexIgnore
    public static /* final */ int REQUEST_SIZE; // = 500;
    @DexIgnore
    public /* final */ LogApiService mApi;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public CloudLogWriter(LogApiService logApiService) {
        ee7.b(logApiService, "mApi");
        this.mApi = logApiService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    @Override // com.misfit.frameworks.buttonservice.log.IRemoteLogWriter
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object sendLog(java.util.List<com.misfit.frameworks.buttonservice.log.LogEvent> r18, com.fossil.fb7<? super java.util.List<com.misfit.frameworks.buttonservice.log.LogEvent>> r19) {
        /*
            r17 = this;
            r0 = r19
            boolean r1 = r0 instanceof com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon1
            if (r1 == 0) goto L_0x0017
            r1 = r0
            com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon1 r1 = (com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon1) r1
            int r2 = r1.label
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = r2 & r3
            if (r4 == 0) goto L_0x0017
            int r2 = r2 - r3
            r1.label = r2
            r2 = r17
            goto L_0x001e
        L_0x0017:
            com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon1 r1 = new com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon1
            r2 = r17
            r1.<init>(r2, r0)
        L_0x001e:
            java.lang.Object r0 = r1.result
            java.lang.Object r3 = com.fossil.nb7.a()
            int r4 = r1.label
            r5 = 1
            if (r4 == 0) goto L_0x0069
            if (r4 != r5) goto L_0x0061
            java.lang.Object r4 = r1.L$10
            com.fossil.ie4 r4 = (com.fossil.ie4) r4
            java.lang.Object r4 = r1.L$9
            com.google.gson.JsonElement r4 = (com.google.gson.JsonElement) r4
            java.lang.Object r4 = r1.L$8
            java.util.List r4 = (java.util.List) r4
            java.lang.Object r6 = r1.L$7
            java.lang.Object r6 = r1.L$6
            java.util.Iterator r6 = (java.util.Iterator) r6
            java.lang.Object r7 = r1.L$5
            java.lang.Iterable r7 = (java.lang.Iterable) r7
            java.lang.Object r8 = r1.L$4
            java.util.List r8 = (java.util.List) r8
            java.lang.Object r9 = r1.L$3
            java.util.List r9 = (java.util.List) r9
            java.lang.Object r10 = r1.L$2
            com.google.gson.Gson r10 = (com.google.gson.Gson) r10
            java.lang.Object r11 = r1.L$1
            java.util.List r11 = (java.util.List) r11
            java.lang.Object r12 = r1.L$0
            com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter r12 = (com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter) r12
            com.fossil.t87.a(r0)
            r16 = r9
            r9 = r6
            r6 = r11
            r11 = r10
            r10 = r16
            goto L_0x00da
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0069:
            com.fossil.t87.a(r0)
            com.misfit.frameworks.buttonservice.log.FLogUtils r0 = com.misfit.frameworks.buttonservice.log.FLogUtils.INSTANCE
            com.google.gson.Gson r0 = r0.getGsonForLogEvent()
            r4 = 500(0x1f4, float:7.0E-43)
            r6 = r18
            java.util.List r4 = com.fossil.ea7.b(r6, r4)
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.Iterator r8 = r4.iterator()
            r10 = r0
            r12 = r2
            r9 = r4
            r0 = r7
            r7 = r9
        L_0x0088:
            boolean r4 = r8.hasNext()
            if (r4 == 0) goto L_0x00e8
            java.lang.Object r4 = r8.next()
            r11 = r4
            java.util.List r11 = (java.util.List) r11
            com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon2$logEventArrays$Anon1_Level2 r13 = new com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter$sendLog$Anon2$logEventArrays$Anon1_Level2
            r13.<init>()
            java.lang.reflect.Type r13 = r13.getType()
            com.google.gson.JsonElement r13 = r10.b(r11, r13)
            com.fossil.ie4 r14 = new com.fossil.ie4
            r14.<init>()
            java.lang.String r15 = "_items"
            r14.a(r15, r13)
            com.misfit.frameworks.buttonservice.log.LogApiService r15 = r12.mApi
            retrofit2.Call r15 = r15.sendLogs(r14)
            r1.L$0 = r12
            r1.L$1 = r6
            r1.L$2 = r10
            r1.L$3 = r9
            r1.L$4 = r0
            r1.L$5 = r7
            r1.L$6 = r8
            r1.L$7 = r4
            r1.L$8 = r11
            r1.L$9 = r13
            r1.L$10 = r14
            r1.label = r5
            java.lang.Object r4 = com.misfit.frameworks.buttonservice.log.LogEndPointKt.await(r15, r1)
            if (r4 != r3) goto L_0x00d1
            return r3
        L_0x00d1:
            r16 = r8
            r8 = r0
            r0 = r4
            r4 = r11
            r11 = r10
            r10 = r9
            r9 = r16
        L_0x00da:
            com.misfit.frameworks.buttonservice.log.RepoResponse r0 = (com.misfit.frameworks.buttonservice.log.RepoResponse) r0
            boolean r0 = r0 instanceof com.misfit.frameworks.buttonservice.log.Success
            if (r0 == 0) goto L_0x00e3
            r8.addAll(r4)
        L_0x00e3:
            r0 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            goto L_0x0088
        L_0x00e8:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter.sendLog(java.util.List, com.fossil.fb7):java.lang.Object");
    }
}
