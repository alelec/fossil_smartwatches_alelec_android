package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.ee7;
import com.fossil.fb7;
import com.fossil.i97;
import com.fossil.kd7;
import com.fossil.nb7;
import com.fossil.t87;
import com.fossil.tb7;
import com.fossil.xi;
import com.fossil.yi7;
import com.fossil.zb7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@tb7(c = "com.misfit.frameworks.buttonservice.log.db.LogDatabase$init$1", f = "LogDatabase.kt", l = {}, m = "invokeSuspend")
public final class LogDatabase$init$Anon1 extends zb7 implements kd7<yi7, fb7<? super i97>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public yi7 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LogDatabase this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ LogDatabase$init$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(LogDatabase$init$Anon1 logDatabase$init$Anon1) {
            this.this$0 = logDatabase$init$Anon1;
        }

        @DexIgnore
        public final void run() {
            xi openHelper = this.this$0.this$0.getOpenHelper();
            ee7.a((Object) openHelper, "openHelper");
            openHelper.getWritableDatabase().execSQL("CREATE TRIGGER IF NOT EXISTS delete_keep_2000 after insert on log WHEN (select count(*) from log) >= 2000 BEGIN DELETE FROM log WHERE id NOT IN  (SELECT id FROM log ORDER BY timeStamp desc limit 2000); END");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LogDatabase$init$Anon1(LogDatabase logDatabase, fb7 fb7) {
        super(2, fb7);
        this.this$0 = logDatabase;
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final fb7<i97> create(Object obj, fb7<?> fb7) {
        ee7.b(fb7, "completion");
        LogDatabase$init$Anon1 logDatabase$init$Anon1 = new LogDatabase$init$Anon1(this.this$0, fb7);
        logDatabase$init$Anon1.p$ = (yi7) obj;
        return logDatabase$init$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.fossil.kd7
    public final Object invoke(yi7 yi7, fb7<? super i97> fb7) {
        return ((LogDatabase$init$Anon1) create(yi7, fb7)).invokeSuspend(i97.a);
    }

    @DexIgnore
    @Override // com.fossil.ob7
    public final Object invokeSuspend(Object obj) {
        nb7.a();
        if (this.label == 0) {
            t87.a(obj);
            this.this$0.runInTransaction(new Anon1_Level2(this));
            return i97.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
