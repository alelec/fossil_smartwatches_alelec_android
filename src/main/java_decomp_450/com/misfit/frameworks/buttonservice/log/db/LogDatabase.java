package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.ci;
import com.fossil.ee7;
import com.fossil.ik7;
import com.fossil.qj7;
import com.fossil.th;
import com.fossil.xh7;
import com.fossil.zd7;
import com.fossil.zi7;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class LogDatabase extends ci {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "LogDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public abstract LogDao getLogDao();

    @DexIgnore
    @Override // com.fossil.ci
    public void init(th thVar) {
        ee7.b(thVar, "configuration");
        super.init(thVar);
        try {
            ik7 unused = xh7.b(zi7.a(qj7.b()), null, null, new LogDatabase$init$Anon1(this, null), 3, null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when init database " + e);
        }
    }
}
