package com.misfit.frameworks.buttonservice.model;

import com.fossil.bb7;
import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickCommandQueue$addForNotificationResponse$$inlined$compareBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return bb7.a(Integer.valueOf(t.getLifeCountDownObject().getCount()), Integer.valueOf(t2.getLifeCountDownObject().getCount()));
    }
}
