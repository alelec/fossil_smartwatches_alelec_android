package com.misfit.frameworks.buttonservice.model.watchapp.response;

import com.fossil.ee7;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicResponseFactory {
    @DexIgnore
    public static /* final */ MusicResponseFactory INSTANCE; // = new MusicResponseFactory();

    @DexIgnore
    public final NotifyMusicEventResponse createMusicEventResponse(NotifyMusicEventResponse.MusicMediaAction musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus) {
        ee7.b(musicMediaAction, "musicAction");
        ee7.b(musicMediaStatus, "status");
        return new NotifyMusicEventResponse(musicMediaAction, musicMediaStatus);
    }

    @DexIgnore
    public final MusicTrackInfoResponse createMusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        ee7.b(str, "appName");
        ee7.b(str2, "trackTitle");
        ee7.b(str3, "artistName");
        ee7.b(str4, "albumName");
        return new MusicTrackInfoResponse(str, b, str2, str3, str4);
    }
}
