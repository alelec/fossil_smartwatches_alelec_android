package com.misfit.frameworks.buttonservice.model.background;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;
import com.fossil.ee7;
import com.fossil.g70;
import com.fossil.zd7;
import com.misfit.frameworks.common.log.MFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BackgroundImgData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public String imgData;
    @DexIgnore
    public String imgName;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<BackgroundImgData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundImgData createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new BackgroundImgData(parcel, (zd7) null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public BackgroundImgData[] newArray(int i) {
            return new BackgroundImgData[i];
        }
    }

    /*
    static {
        String simpleName = BackgroundImgData.class.getSimpleName();
        ee7.a((Object) simpleName, "BackgroundImgData::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public /* synthetic */ BackgroundImgData(Parcel parcel, zd7 zd7) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getHash() {
        return this.imgName + ':' + this.imgData;
    }

    @DexIgnore
    public final String getImgData() {
        return this.imgData;
    }

    @DexIgnore
    public final String getImgName() {
        return this.imgName;
    }

    @DexIgnore
    public final void setImgData(String str) {
        ee7.b(str, "<set-?>");
        this.imgData = str;
    }

    @DexIgnore
    public final void setImgName(String str) {
        ee7.b(str, "<set-?>");
        this.imgName = str;
    }

    @DexIgnore
    public final g70 toSDKBackgroundImage() {
        byte[] bArr;
        try {
            bArr = Base64.decode(this.imgData, 0);
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, ".toSDKBackgroundImage(), ex: " + e);
            bArr = new byte[0];
        }
        String str2 = this.imgName;
        ee7.a((Object) bArr, "byteData");
        return new g70(str2, bArr, null, 4, null);
    }

    @DexIgnore
    public String toString() {
        return "{imgName: " + this.imgName + ", imgData: BASE64_DATA_SIZE_" + this.imgData.length() + '}';
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.imgName);
        parcel.writeString(this.imgData);
    }

    @DexIgnore
    public BackgroundImgData(String str, String str2) {
        ee7.b(str, "imgName");
        ee7.b(str2, "imgData");
        this.imgName = str;
        this.imgData = str2;
    }

    @DexIgnore
    public BackgroundImgData(Parcel parcel) {
        String readString = parcel.readString();
        String str = "";
        this.imgName = readString == null ? str : readString;
        String readString2 = parcel.readString();
        this.imgData = readString2 != null ? readString2 : str;
    }
}
