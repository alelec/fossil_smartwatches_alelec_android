package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.n90;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicTrackInfoResponse extends MusicResponse {
    @DexIgnore
    public /* final */ String albumName;
    @DexIgnore
    public /* final */ String appName;
    @DexIgnore
    public /* final */ String artistName;
    @DexIgnore
    public /* final */ String trackTitle;
    @DexIgnore
    public /* final */ byte volume;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicTrackInfoResponse(String str, byte b, String str2, String str3, String str4) {
        super(MusicResponse.TYPE_MUSIC_TRACK_INFO);
        ee7.b(str, "appName");
        ee7.b(str2, "trackTitle");
        ee7.b(str3, "artistName");
        ee7.b(str4, "albumName");
        this.appName = str;
        this.volume = b;
        this.trackTitle = str2;
        this.artistName = str3;
        this.albumName = str4;
    }

    @DexIgnore
    public final String getAlbumName() {
        return this.albumName;
    }

    @DexIgnore
    public final String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public final String getArtistName() {
        return this.artistName;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String getHash() {
        String str = this.appName + ":" + Byte.valueOf(this.volume) + ":" + this.trackTitle + ":" + this.artistName + ":" + this.albumName;
        ee7.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final String getTrackTitle() {
        return this.trackTitle;
    }

    @DexIgnore
    public final byte getVolume() {
        return this.volume;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", appName=" + this.appName;
    }

    @DexIgnore
    public final n90 toSDKTrackInfo() {
        return new n90(this.appName, this.volume, this.trackTitle, this.artistName, this.albumName);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.appName);
        parcel.writeByte(this.volume);
        parcel.writeString(this.trackTitle);
        parcel.writeString(this.artistName);
        parcel.writeString(this.albumName);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MusicTrackInfoResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String readString = parcel.readString();
        String str = "";
        this.appName = readString == null ? str : readString;
        this.volume = parcel.readByte();
        String readString2 = parcel.readString();
        this.trackTitle = readString2 == null ? str : readString2;
        String readString3 = parcel.readString();
        this.artistName = readString3 == null ? str : readString3;
        String readString4 = parcel.readString();
        this.albumName = readString4 != null ? readString4 : str;
    }
}
