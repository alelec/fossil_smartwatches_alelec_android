package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OtaEvent implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public float process;
    @DexIgnore
    public String serial;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<OtaEvent> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OtaEvent createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new OtaEvent(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public OtaEvent[] newArray(int i) {
            return new OtaEvent[i];
        }
    }

    @DexIgnore
    public OtaEvent(String str, float f) {
        ee7.b(str, "serial");
        this.serial = str;
        this.process = f;
    }

    @DexIgnore
    public static /* synthetic */ OtaEvent copy$default(OtaEvent otaEvent, String str, float f, int i, Object obj) {
        if ((i & 1) != 0) {
            str = otaEvent.serial;
        }
        if ((i & 2) != 0) {
            f = otaEvent.process;
        }
        return otaEvent.copy(str, f);
    }

    @DexIgnore
    public final String component1() {
        return this.serial;
    }

    @DexIgnore
    public final float component2() {
        return this.process;
    }

    @DexIgnore
    public final OtaEvent copy(String str, float f) {
        ee7.b(str, "serial");
        return new OtaEvent(str, f);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof OtaEvent)) {
            return false;
        }
        OtaEvent otaEvent = (OtaEvent) obj;
        return ee7.a(this.serial, otaEvent.serial) && Float.compare(this.process, otaEvent.process) == 0;
    }

    @DexIgnore
    public final float getProcess() {
        return this.process;
    }

    @DexIgnore
    public final String getSerial() {
        return this.serial;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.serial;
        return ((str != null ? str.hashCode() : 0) * 31) + Float.floatToIntBits(this.process);
    }

    @DexIgnore
    public final void setProcess(float f) {
        this.process = f;
    }

    @DexIgnore
    public final void setSerial(String str) {
        ee7.b(str, "<set-?>");
        this.serial = str;
    }

    @DexIgnore
    public String toString() {
        return "OtaEvent(serial=" + this.serial + ", process=" + this.process + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.serial);
        parcel.writeFloat(this.process);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public OtaEvent(android.os.Parcel r2) {
        /*
            r1 = this;
            java.lang.String r0 = "parcel"
            com.fossil.ee7.b(r2, r0)
            java.lang.String r0 = r2.readString()
            if (r0 == 0) goto L_0x000c
            goto L_0x000e
        L_0x000c:
            java.lang.String r0 = ""
        L_0x000e:
            float r2 = r2.readFloat()
            r1.<init>(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.OtaEvent.<init>(android.os.Parcel):void");
    }
}
