package com.misfit.frameworks.buttonservice.model.calibration;

import com.fossil.ee7;
import com.fossil.i70;
import com.fossil.k70;
import com.fossil.l70;
import com.fossil.m70;
import com.fossil.p87;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.CalibrationEnums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCalibrationObj {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int degree;
    @DexIgnore
    public /* final */ i70 handId;
    @DexIgnore
    public /* final */ k70 handMovingDirection;
    @DexIgnore
    public /* final */ l70 handMovingSpeed;
    @DexIgnore
    public /* final */ m70 handMovingType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$1;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$2;
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$3;

            /*
            static {
                int[] iArr = new int[CalibrationEnums.MovingType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[CalibrationEnums.MovingType.DISTANCE.ordinal()] = 1;
                $EnumSwitchMapping$0[CalibrationEnums.MovingType.POSITION.ordinal()] = 2;
                int[] iArr2 = new int[CalibrationEnums.HandId.values().length];
                $EnumSwitchMapping$1 = iArr2;
                iArr2[CalibrationEnums.HandId.HOUR.ordinal()] = 1;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.MINUTE.ordinal()] = 2;
                $EnumSwitchMapping$1[CalibrationEnums.HandId.SUB_EYE.ordinal()] = 3;
                int[] iArr3 = new int[CalibrationEnums.Direction.values().length];
                $EnumSwitchMapping$2 = iArr3;
                iArr3[CalibrationEnums.Direction.CLOCKWISE.ordinal()] = 1;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.COUNTER_CLOCKWISE.ordinal()] = 2;
                $EnumSwitchMapping$2[CalibrationEnums.Direction.SHORTEST_PATH.ordinal()] = 3;
                int[] iArr4 = new int[CalibrationEnums.Speed.values().length];
                $EnumSwitchMapping$3 = iArr4;
                iArr4[CalibrationEnums.Speed.SIXTEENTH.ordinal()] = 1;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.EIGHTH.ordinal()] = 2;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.QUARTER.ordinal()] = 3;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.HALF.ordinal()] = 4;
                $EnumSwitchMapping$3[CalibrationEnums.Speed.FULL.ordinal()] = 5;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final DianaCalibrationObj consume(HandCalibrationObj handCalibrationObj) {
            m70 m70;
            i70 i70;
            k70 k70;
            l70 l70;
            ee7.b(handCalibrationObj, "handCalibrationObj");
            int i = WhenMappings.$EnumSwitchMapping$0[handCalibrationObj.getMovingType().ordinal()];
            if (i == 1) {
                m70 = m70.DISTANCE;
            } else if (i == 2) {
                m70 = m70.POSITION;
            } else {
                throw new p87();
            }
            int i2 = WhenMappings.$EnumSwitchMapping$1[handCalibrationObj.getHandId().ordinal()];
            if (i2 == 1) {
                i70 = i70.HOUR;
            } else if (i2 == 2) {
                i70 = i70.MINUTE;
            } else if (i2 == 3) {
                i70 = i70.SUB_EYE;
            } else {
                throw new p87();
            }
            int i3 = WhenMappings.$EnumSwitchMapping$2[handCalibrationObj.getDirection().ordinal()];
            if (i3 == 1) {
                k70 = k70.CLOCKWISE;
            } else if (i3 == 2) {
                k70 = k70.COUNTER_CLOCKWISE;
            } else if (i3 == 3) {
                k70 = k70.SHORTEST_PATH;
            } else {
                throw new p87();
            }
            int i4 = WhenMappings.$EnumSwitchMapping$3[handCalibrationObj.getSpeed().ordinal()];
            if (i4 == 1) {
                l70 = l70.SIXTEENTH;
            } else if (i4 == 2) {
                l70 = l70.EIGHTH;
            } else if (i4 == 3) {
                l70 = l70.QUARTER;
            } else if (i4 == 4) {
                l70 = l70.HALF;
            } else if (i4 == 5) {
                l70 = l70.FULL;
            } else {
                throw new p87();
            }
            return new DianaCalibrationObj(m70, i70, k70, l70, handCalibrationObj.getDegree());
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public DianaCalibrationObj(m70 m70, i70 i70, k70 k70, l70 l70, int i) {
        ee7.b(m70, "handMovingType");
        ee7.b(i70, "handId");
        ee7.b(k70, "handMovingDirection");
        ee7.b(l70, "handMovingSpeed");
        this.handMovingType = m70;
        this.handId = i70;
        this.handMovingDirection = k70;
        this.handMovingSpeed = l70;
        this.degree = i;
    }

    @DexIgnore
    public final int getDegree() {
        return this.degree;
    }

    @DexIgnore
    public final i70 getHandId() {
        return this.handId;
    }

    @DexIgnore
    public final k70 getHandMovingDirection() {
        return this.handMovingDirection;
    }

    @DexIgnore
    public final l70 getHandMovingSpeed() {
        return this.handMovingSpeed;
    }

    @DexIgnore
    public final m70 getHandMovingType() {
        return this.handMovingType;
    }
}
