package com.misfit.frameworks.buttonservice.model.watchapp.response.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.model.PlaceFields;
import com.fossil.ee7;
import com.fossil.g90;
import com.fossil.va0;
import com.fossil.wa0;
import com.fossil.x87;
import com.fossil.x97;
import com.fossil.xa0;
import com.fossil.ya0;
import com.fossil.zd7;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherWatchAppInfo implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public /* final */ CurrentWeatherInfo currentWeatherInfo;
    @DexIgnore
    public /* final */ List<WeatherDayForecast> dayForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public /* final */ List<WeatherHourForecast> hourForecast; // = new ArrayList();
    @DexIgnore
    public /* final */ String location;
    @DexIgnore
    public /* final */ UserDisplayUnit.TemperatureUnit temperatureUnit;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherWatchAppInfo> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppInfo createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new WeatherWatchAppInfo(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public WeatherWatchAppInfo[] newArray(int i) {
            return new WeatherWatchAppInfo[i];
        }
    }

    @DexIgnore
    public WeatherWatchAppInfo(String str, UserDisplayUnit.TemperatureUnit temperatureUnit2, CurrentWeatherInfo currentWeatherInfo2, List<WeatherDayForecast> list, List<WeatherHourForecast> list2, long j) {
        ee7.b(str, PlaceFields.LOCATION);
        ee7.b(temperatureUnit2, "temperatureUnit");
        ee7.b(currentWeatherInfo2, "currentWeatherInfo");
        ee7.b(list, "dayForecast");
        ee7.b(list2, "hourForecast");
        this.location = str;
        this.temperatureUnit = temperatureUnit2;
        this.currentWeatherInfo = currentWeatherInfo2;
        this.dayForecast.addAll(list);
        this.hourForecast.addAll(list2);
        this.expiredAt = j;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final ya0 toSDKWeatherAppInfo() {
        List<WeatherDayForecast> list = this.dayForecast;
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKWeatherDayForecast());
        }
        List<WeatherHourForecast> list2 = this.hourForecast;
        ArrayList arrayList2 = new ArrayList(x97.a(list2, 10));
        Iterator<T> it2 = list2.iterator();
        while (it2.hasNext()) {
            arrayList2.add(it2.next().toSDKWeatherHourForecast());
        }
        long j = this.expiredAt;
        String str = this.location;
        g90 sDKTemperatureUnit = this.temperatureUnit.toSDKTemperatureUnit();
        va0 sDKCurrentWeatherInfo = this.currentWeatherInfo.toSDKCurrentWeatherInfo();
        Object[] array = arrayList2.toArray(new xa0[0]);
        if (array != null) {
            xa0[] xa0Arr = (xa0[]) array;
            Object[] array2 = arrayList.toArray(new wa0[0]);
            if (array2 != null) {
                return new ya0(j, str, sDKTemperatureUnit, sDKCurrentWeatherInfo, xa0Arr, (wa0[]) array2);
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.temperatureUnit.getValue());
        parcel.writeParcelable(this.currentWeatherInfo, i);
        parcel.writeTypedList(this.dayForecast);
        parcel.writeTypedList(this.hourForecast);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    public WeatherWatchAppInfo(Parcel parcel) {
        ee7.b(parcel, "parcel");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        UserDisplayUnit.TemperatureUnit.Companion companion = UserDisplayUnit.TemperatureUnit.Companion;
        String readString2 = parcel.readString();
        this.temperatureUnit = companion.fromValue(readString2 == null ? "C" : readString2);
        CurrentWeatherInfo currentWeatherInfo2 = (CurrentWeatherInfo) parcel.readParcelable(CurrentWeatherInfo.class.getClassLoader());
        this.currentWeatherInfo = currentWeatherInfo2 == null ? new CurrentWeatherInfo() : currentWeatherInfo2;
        parcel.readTypedList(this.dayForecast, WeatherDayForecast.CREATOR);
        parcel.readTypedList(this.hourForecast, WeatherHourForecast.CREATOR);
        this.expiredAt = parcel.readLong();
    }
}
