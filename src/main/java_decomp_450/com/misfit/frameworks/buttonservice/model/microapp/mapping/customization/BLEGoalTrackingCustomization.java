package com.misfit.frameworks.buttonservice.model.microapp.mapping.customization;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.kg0;
import com.fossil.lg0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BLEGoalTrackingCustomization extends BLECustomization {
    @DexIgnore
    public int goalId;

    @DexIgnore
    public BLEGoalTrackingCustomization(int i) {
        super(1);
        this.goalId = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public lg0 getCustomizationFrame() {
        return new kg0((short) this.goalId);
    }

    @DexIgnore
    public final int getGoalId() {
        return this.goalId;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getType());
        sb.append(':');
        sb.append(this.goalId);
        return sb.toString();
    }

    @DexIgnore
    public final void setGoalId(int i) {
        this.goalId = i;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.microapp.mapping.customization.BLECustomization
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.goalId);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BLEGoalTrackingCustomization(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "in");
        this.goalId = parcel.readInt();
    }
}
