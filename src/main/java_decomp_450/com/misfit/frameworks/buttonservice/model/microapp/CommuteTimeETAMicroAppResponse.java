package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.r60;
import com.fossil.vb0;
import com.fossil.yb0;
import com.fossil.ze0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeETAMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mHour;
    @DexIgnore
    public int mMinute;

    @DexIgnore
    public CommuteTimeETAMicroAppResponse(int i, int i2) {
        super(cb0.COMMUTE_TIME_ETA_MICRO_APP);
        this.mHour = i;
        this.mMinute = i2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof vb0) || r60 == null) {
            return null;
        }
        return new ze0((vb0) yb0, r60, this.mHour, this.mMinute);
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mHour);
        parcel.writeInt(this.mMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.mHour = parcel.readInt();
        this.mMinute = parcel.readInt();
    }
}
