package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.common.enums.Action;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum MicroAppMode {
    UNKNOWN,
    HID,
    STREAMING,
    GOAL_TRACKING,
    DISPLAY_ACTION;

    @DexIgnore
    public static MicroAppMode fromAction(int i) {
        MicroAppMode microAppMode = UNKNOWN;
        if (Action.HIDAction.isActionBelongToThisType(i)) {
            return HID;
        }
        if (Action.StreamingAction.isActionBelongToThisType(i)) {
            return STREAMING;
        }
        if (Action.GoalTracking.isActionBelongToThisType(i)) {
            return GOAL_TRACKING;
        }
        return Action.DisplayAction.isActionBelongToThisType(i) ? DISPLAY_ACTION : microAppMode;
    }

    @DexIgnore
    public static MicroAppMode fromMappings(List<Mapping> list) {
        if (list == null || list.size() == 0) {
            return UNKNOWN;
        }
        return fromAction(list.get(0).getAction());
    }
}
