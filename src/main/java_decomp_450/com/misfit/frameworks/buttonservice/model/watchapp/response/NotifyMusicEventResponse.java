package com.misfit.frameworks.buttonservice.model.watchapp.response;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.k90;
import com.fossil.l90;
import com.fossil.m90;
import com.fossil.p87;
import com.fossil.zd7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotifyMusicEventResponse extends MusicResponse {
    @DexIgnore
    public /* final */ MusicMediaAction musicAction;
    @DexIgnore
    public /* final */ MusicMediaStatus status;

    @DexIgnore
    public enum MusicMediaAction {
        PLAY,
        PAUSE,
        TOGGLE_PLAY_PAUSE,
        NEXT,
        PREVIOUS,
        VOLUME_UP,
        VOLUME_DOWN,
        NONE;
        
        @DexIgnore
        public static /* final */ Companion Companion; // = new Companion(null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public final /* synthetic */ class WhenMappings {
                @DexIgnore
                public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

                /*
                static {
                    int[] iArr = new int[k90.values().length];
                    $EnumSwitchMapping$0 = iArr;
                    iArr[k90.PLAY.ordinal()] = 1;
                    $EnumSwitchMapping$0[k90.PAUSE.ordinal()] = 2;
                    $EnumSwitchMapping$0[k90.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                    $EnumSwitchMapping$0[k90.NEXT.ordinal()] = 4;
                    $EnumSwitchMapping$0[k90.PREVIOUS.ordinal()] = 5;
                    $EnumSwitchMapping$0[k90.VOLUME_UP.ordinal()] = 6;
                    $EnumSwitchMapping$0[k90.VOLUME_DOWN.ordinal()] = 7;
                }
                */
            }

            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final MusicMediaAction fromSDKMusicAction(k90 k90) {
                ee7.b(k90, "sdkMusicAction");
                switch (WhenMappings.$EnumSwitchMapping$0[k90.ordinal()]) {
                    case 1:
                        return MusicMediaAction.PLAY;
                    case 2:
                        return MusicMediaAction.PAUSE;
                    case 3:
                        return MusicMediaAction.TOGGLE_PLAY_PAUSE;
                    case 4:
                        return MusicMediaAction.NEXT;
                    case 5:
                        return MusicMediaAction.PREVIOUS;
                    case 6:
                        return MusicMediaAction.VOLUME_UP;
                    case 7:
                        return MusicMediaAction.VOLUME_DOWN;
                    default:
                        return MusicMediaAction.NONE;
                }
            }

            @DexIgnore
            public /* synthetic */ Companion(zd7 zd7) {
                this();
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaAction.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaAction.PLAY.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaAction.PAUSE.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaAction.TOGGLE_PLAY_PAUSE.ordinal()] = 3;
                $EnumSwitchMapping$0[MusicMediaAction.NEXT.ordinal()] = 4;
                $EnumSwitchMapping$0[MusicMediaAction.PREVIOUS.ordinal()] = 5;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_UP.ordinal()] = 6;
                $EnumSwitchMapping$0[MusicMediaAction.VOLUME_DOWN.ordinal()] = 7;
                $EnumSwitchMapping$0[MusicMediaAction.NONE.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final k90 toSDKMusicAction() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return k90.PLAY;
                case 2:
                    return k90.PAUSE;
                case 3:
                    return k90.TOGGLE_PLAY_PAUSE;
                case 4:
                    return k90.NEXT;
                case 5:
                    return k90.PREVIOUS;
                case 6:
                    return k90.VOLUME_UP;
                case 7:
                    return k90.VOLUME_DOWN;
                case 8:
                    return null;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore
    public enum MusicMediaStatus {
        SUCCESS,
        NO_MUSIC_PLAYER,
        FAIL_TO_TRIGGER;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[MusicMediaStatus.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[MusicMediaStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[MusicMediaStatus.NO_MUSIC_PLAYER.ordinal()] = 2;
                $EnumSwitchMapping$0[MusicMediaStatus.FAIL_TO_TRIGGER.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final l90 toSDKMusicActionStatus() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return l90.SUCCESS;
            }
            if (i == 2) {
                return l90.NO_MUSIC_PLAYER;
            }
            if (i == 3) {
                return l90.FAIL_TO_TRIGGER;
            }
            throw new p87();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(MusicMediaAction musicMediaAction, MusicMediaStatus musicMediaStatus) {
        super("Music Event_" + musicMediaAction);
        ee7.b(musicMediaAction, "musicAction");
        ee7.b(musicMediaStatus, "status");
        this.musicAction = musicMediaAction;
        this.status = musicMediaStatus;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String getHash() {
        String str = this.musicAction + ":" + this.status;
        ee7.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final MusicMediaAction getMusicAction() {
        return this.musicAction;
    }

    @DexIgnore
    public final MusicMediaStatus getStatus() {
        return this.status;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public String toRemoteLogString() {
        return super.toRemoteLogString() + ", musicAction=" + this.musicAction + ", status=" + this.status;
    }

    @DexIgnore
    public final m90 toSDKMusicEvent() {
        k90 sDKMusicAction = this.musicAction.toSDKMusicAction();
        if (sDKMusicAction != null) {
            return new m90(sDKMusicAction, this.status.toSDKMusicActionStatus());
        }
        return null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.musicAction.ordinal());
        parcel.writeInt(this.status.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public NotifyMusicEventResponse(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.musicAction = MusicMediaAction.values()[parcel.readInt()];
        this.status = MusicMediaStatus.values()[parcel.readInt()];
    }
}
