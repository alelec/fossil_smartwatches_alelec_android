package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.ee7;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShineDevice extends Device implements Parcelable, Comparable<Object> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ShineDevice> CREATOR; // = new ShineDevice$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final ShineDevice clone(ScannedDevice scannedDevice) {
            ee7.b(scannedDevice, "device");
            return new ShineDevice(scannedDevice.getDeviceSerial(), scannedDevice.getDeviceName(), scannedDevice.getDeviceMACAddress(), scannedDevice.getRssi(), scannedDevice.getFastPairIdInHex());
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShineDevice(String str, String str2, String str3, int i, String str4) {
        super(str, str2, str3, i, str4);
        ee7.b(str, "serial");
        ee7.b(str2, "name");
        ee7.b(str3, "macAddress");
        ee7.b(str4, "fastPairId");
    }

    @DexIgnore
    @Override // java.lang.Comparable
    public int compareTo(Object obj) {
        ee7.b(obj, FacebookRequestErrorClassification.KEY_OTHER);
        if (!(obj instanceof ShineDevice)) {
            return 1;
        }
        ShineDevice shineDevice = (ShineDevice) obj;
        if (shineDevice.getRssi() == getRssi()) {
            return 0;
        }
        if (shineDevice.getRssi() > getRssi()) {
            return 1;
        }
        return -1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "dest");
        parcel.writeString(((Device) this).serial);
        parcel.writeString(((Device) this).name);
        parcel.writeString(((Device) this).macAddress);
        parcel.writeInt(((Device) this).rssi);
        parcel.writeString(((Device) this).fastPairId);
    }

    @DexIgnore
    public ShineDevice(Parcel parcel) {
        ee7.b(parcel, "parcel");
        ((Device) this).serial = parcel.readString();
        ((Device) this).name = parcel.readString();
        ((Device) this).macAddress = parcel.readString();
        ((Device) this).rssi = parcel.readInt();
        ((Device) this).fastPairId = parcel.readString();
    }
}
