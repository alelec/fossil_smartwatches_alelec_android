package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.ah0;
import com.fossil.ee7;
import com.fossil.ga0;
import com.fossil.na0;
import com.fossil.x87;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public List<String> destinations;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(List<String> list) {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getCOMMUTE_TIME());
        ee7.b(list, "destinations");
        this.destinations = list;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public na0 toSDKSetting() {
        List<String> list = this.destinations;
        if (list != null) {
            Object[] array = list.toArray(new String[0]);
            if (array != null) {
                return new ga0(new ah0((String[]) array));
            }
            throw new x87("null cannot be cast to non-null type kotlin.Array<T>");
        }
        ee7.d("destinations");
        throw null;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        List<String> list = this.destinations;
        if (list != null) {
            parcel.writeStringList(list);
        } else {
            ee7.d("destinations");
            throw null;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMapping(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        ArrayList arrayList = new ArrayList();
        parcel.readStringList(arrayList);
        this.destinations = arrayList;
    }
}
