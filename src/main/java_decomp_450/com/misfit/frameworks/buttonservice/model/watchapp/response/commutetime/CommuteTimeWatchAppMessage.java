package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.bf0;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.df0;
import com.fossil.ee7;
import com.fossil.r60;
import com.fossil.uf0;
import com.fossil.xb0;
import com.fossil.yb0;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMessage extends DeviceAppResponse {
    @DexIgnore
    public uf0 deviceMessageType;
    @DexIgnore
    public String message;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[uf0.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[uf0.END.ordinal()] = 1;
            $EnumSwitchMapping$0[uf0.IN_PROGRESS.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(String str, uf0 uf0) {
        super(cb0.COMMUTE_TIME_WATCH_APP);
        ee7.b(str, "message");
        ee7.b(uf0, "deviceMessageType");
        this.message = "";
        this.deviceMessageType = uf0.END;
        this.message = str;
        this.deviceMessageType = uf0;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return new bf0(new df0(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof xb0)) {
            return null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[this.deviceMessageType.ordinal()];
        if (i == 1) {
            return new bf0((xb0) yb0, new df0(this.message, this.deviceMessageType));
        }
        if (i != 2) {
            return new bf0(new df0(this.message, this.deviceMessageType));
        }
        return new bf0(new df0(this.message, this.deviceMessageType));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        String str = "";
        this.message = str;
        this.deviceMessageType = uf0.END;
        String readString = parcel.readString();
        this.message = readString != null ? readString : str;
        this.deviceMessageType = uf0.values()[parcel.readInt()];
    }
}
