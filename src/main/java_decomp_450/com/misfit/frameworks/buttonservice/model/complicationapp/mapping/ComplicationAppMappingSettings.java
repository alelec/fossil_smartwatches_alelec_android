package com.misfit.frameworks.buttonservice.model.complicationapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ee7;
import com.fossil.t70;
import com.fossil.wg0;
import com.fossil.xg0;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationAppMappingSettings implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<ComplicationAppMappingSettings> CREATOR; // = new ComplicationAppMappingSettings$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public ComplicationAppMapping bottomAppMapping;
    @DexIgnore
    public ComplicationAppMapping leftAppMapping;
    @DexIgnore
    public ComplicationAppMapping rightAppMapping;
    @DexIgnore
    public /* final */ long timeStamp;
    @DexIgnore
    public ComplicationAppMapping topAppMapping;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final long compareTimeStamp(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            long j = 0;
            long timeStamp = complicationAppMappingSettings != null ? complicationAppMappingSettings.getTimeStamp() : 0;
            if (complicationAppMappingSettings2 != null) {
                j = complicationAppMappingSettings2.getTimeStamp();
            }
            return timeStamp - j;
        }

        @DexIgnore
        public final boolean isSettingsSame(ComplicationAppMappingSettings complicationAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings2) {
            if ((complicationAppMappingSettings != null || complicationAppMappingSettings2 == null) && (complicationAppMappingSettings == null || complicationAppMappingSettings2 != null)) {
                return ee7.a(complicationAppMappingSettings, complicationAppMappingSettings2);
            }
            return false;
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public /* synthetic */ ComplicationAppMappingSettings(Parcel parcel, zd7 zd7) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof ComplicationAppMappingSettings)) {
            return false;
        }
        return ee7.a((Object) getHash(), (Object) ((ComplicationAppMappingSettings) obj).getHash());
    }

    @DexIgnore
    public final String getHash() {
        String str = this.topAppMapping.getHash() + ":" + this.bottomAppMapping.getHash() + ":" + this.leftAppMapping + ":" + this.rightAppMapping;
        ee7.a((Object) str, "builder.toString()");
        return str;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public final wg0 toSDKSetting(boolean z) {
        return new wg0(new xg0[]{new t70(this.topAppMapping.toSDKSetting(z), this.rightAppMapping.toSDKSetting(z), this.bottomAppMapping.toSDKSetting(z), this.leftAppMapping.toSDKSetting(z))});
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeLong(this.timeStamp);
        parcel.writeParcelable(this.topAppMapping, i);
        parcel.writeParcelable(this.bottomAppMapping, i);
        parcel.writeParcelable(this.leftAppMapping, i);
        parcel.writeParcelable(this.rightAppMapping, i);
    }

    @DexIgnore
    public ComplicationAppMappingSettings(ComplicationAppMapping complicationAppMapping, ComplicationAppMapping complicationAppMapping2, ComplicationAppMapping complicationAppMapping3, ComplicationAppMapping complicationAppMapping4, long j) {
        ee7.b(complicationAppMapping, "topAppMapping");
        ee7.b(complicationAppMapping2, "bottomAppMapping");
        ee7.b(complicationAppMapping3, "leftAppMapping");
        ee7.b(complicationAppMapping4, "rightAppMapping");
        this.topAppMapping = complicationAppMapping;
        this.bottomAppMapping = complicationAppMapping2;
        this.leftAppMapping = complicationAppMapping3;
        this.rightAppMapping = complicationAppMapping4;
        this.timeStamp = j;
    }

    @DexIgnore
    public ComplicationAppMappingSettings(Parcel parcel) {
        this.timeStamp = parcel.readLong();
        ComplicationAppMapping complicationAppMapping = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.topAppMapping = complicationAppMapping == null ? new CaloriesComplicationAppMapping() : complicationAppMapping;
        ComplicationAppMapping complicationAppMapping2 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.bottomAppMapping = complicationAppMapping2 == null ? new ActiveMinutesComplicationAppMapping() : complicationAppMapping2;
        ComplicationAppMapping complicationAppMapping3 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.leftAppMapping = complicationAppMapping3 == null ? new DateComplicationAppMapping() : complicationAppMapping3;
        ComplicationAppMapping complicationAppMapping4 = (ComplicationAppMapping) parcel.readParcelable(ComplicationAppMapping.class.getClassLoader());
        this.rightAppMapping = complicationAppMapping4 == null ? new ChanceOfRainComplicationAppMapping() : complicationAppMapping4;
    }
}
