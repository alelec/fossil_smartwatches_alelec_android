package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.aa0;
import com.fossil.ca0;
import com.fossil.ee7;
import com.fossil.j70;
import com.fossil.zd7;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilter implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ int IS_FIELD_EXIST; // = 1;
    @DexIgnore
    public static /* final */ int IS_FIELD_NOT_EXIST; // = 0;
    @DexIgnore
    public static /* final */ String TAG; // = "AppNotificationFilter";
    @DexIgnore
    public FNotification fNotification;
    @DexIgnore
    public aa0 handMovingConfig;
    @DexIgnore
    public Short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public ca0 vibePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilter> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(zd7 zd7) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter createFromParcel(Parcel parcel) {
            ee7.b(parcel, "parcel");
            return new AppNotificationFilter(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter[] newArray(int i) {
            return new AppNotificationFilter[i];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationFilter(Parcel parcel, zd7 zd7) {
        this(parcel);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AppNotificationFilter)) {
            return false;
        }
        AppNotificationFilter appNotificationFilter = (AppNotificationFilter) obj;
        if (!ee7.a(this.fNotification, appNotificationFilter.fNotification) || !ee7.a((Object) this.sender, (Object) appNotificationFilter.sender) || !ee7.a(this.priority, appNotificationFilter.priority)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final aa0 getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.fNotification.getPackageName();
    }

    @DexIgnore
    public final Short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final ca0 getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setHandMovingConfig(aa0 aa0) {
        this.handMovingConfig = aa0;
    }

    @DexIgnore
    public final void setPriority(Short sh) {
        this.priority = sh;
    }

    @DexIgnore
    public final void setSender(String str) {
        this.sender = str;
    }

    @DexIgnore
    public final void setVibePattern(ca0 ca0) {
        this.vibePattern = ca0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00ec, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00ed, code lost:
        com.fossil.hc7.a(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00f0, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x017e, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x017f, code lost:
        com.fossil.hc7.a(r10, r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0182, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0185, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0186, code lost:
        com.fossil.hc7.a(r3, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0189, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.y90 toSDKNotificationFilter(android.content.Context r10) {
        /*
            r9 = this;
            java.lang.String r0 = "context"
            com.fossil.ee7.b(r10, r0)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = "AppNotificationFilter"
            java.lang.String r2 = "toSDKNotificationFilter() start"
            r0.d(r1, r2)
            com.fossil.aa0 r0 = r9.handMovingConfig
            r2 = 0
            if (r0 == 0) goto L_0x004f
            com.fossil.ca0 r10 = r9.vibePattern
            if (r10 == 0) goto L_0x0038
            com.fossil.y90 r10 = new com.fossil.y90
            com.misfit.frameworks.buttonservice.model.notification.FNotification r0 = r9.fNotification
            java.lang.String r0 = r0.getPackageName()
            com.fossil.aa0 r1 = r9.handMovingConfig
            if (r1 == 0) goto L_0x0034
            com.fossil.ca0 r3 = r9.vibePattern
            if (r3 == 0) goto L_0x0030
            r10.<init>(r0, r1, r3)
            goto L_0x01a9
        L_0x0030:
            com.fossil.ee7.a()
            throw r2
        L_0x0034:
            com.fossil.ee7.a()
            throw r2
        L_0x0038:
            com.fossil.y90 r10 = new com.fossil.y90
            com.misfit.frameworks.buttonservice.model.notification.FNotification r0 = r9.fNotification
            java.lang.String r0 = r0.getPackageName()
            com.fossil.aa0 r1 = r9.handMovingConfig
            if (r1 == 0) goto L_0x004b
            com.fossil.ca0 r2 = com.fossil.ca0.NO_VIBE
            r10.<init>(r0, r1, r2)
            goto L_0x01a9
        L_0x004b:
            com.fossil.ee7.a()
            throw r2
        L_0x004f:
            com.fossil.y90 r0 = new com.fossil.y90
            com.misfit.frameworks.buttonservice.model.notification.FNotification r3 = r9.fNotification
            java.lang.String r3 = r3.getPackageName()
            r0.<init>(r3)
            com.misfit.frameworks.buttonservice.model.notification.FNotification r3 = r9.fNotification
            java.lang.String r3 = r3.getPackageName()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r4 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r4 = r4.getPHONE_INCOMING_CALL()
            java.lang.String r4 = r4.getPackageName()
            boolean r3 = com.fossil.ee7.a(r3, r4)
            java.lang.String r4 = "toSDKNotificationFilter() "
            java.lang.String r5 = "it"
            if (r3 != 0) goto L_0x0111
            com.misfit.frameworks.buttonservice.model.notification.FNotification r3 = r9.fNotification
            java.lang.String r3 = r3.getPackageName()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r6 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r6 = r6.getPHONE_MISSED_CALL()
            java.lang.String r6 = r6.getPackageName()
            boolean r3 = com.fossil.ee7.a(r3, r6)
            if (r3 == 0) goto L_0x008c
            goto L_0x0111
        L_0x008c:
            com.misfit.frameworks.buttonservice.model.notification.FNotification r3 = r9.fNotification
            java.lang.String r3 = r3.getIconFwPath()
            int r3 = r3.length()
            if (r3 <= 0) goto L_0x009a
            r3 = 1
            goto L_0x009b
        L_0x009a:
            r3 = 0
        L_0x009b:
            if (r3 == 0) goto L_0x01a8
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE     // Catch:{ Exception -> 0x00f1 }
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()     // Catch:{ Exception -> 0x00f1 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f1 }
            r6.<init>()     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r7 = "toSDKNotificationFilter() openIconPath "
            r6.append(r7)     // Catch:{ Exception -> 0x00f1 }
            com.misfit.frameworks.buttonservice.model.notification.FNotification r7 = r9.fNotification     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r7 = r7.getIconFwPath()     // Catch:{ Exception -> 0x00f1 }
            r6.append(r7)     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00f1 }
            r3.e(r1, r6)     // Catch:{ Exception -> 0x00f1 }
            android.content.res.AssetManager r10 = r10.getAssets()     // Catch:{ Exception -> 0x00f1 }
            com.misfit.frameworks.buttonservice.model.notification.FNotification r3 = r9.fNotification     // Catch:{ Exception -> 0x00f1 }
            java.lang.String r3 = r3.getIconFwPath()     // Catch:{ Exception -> 0x00f1 }
            java.io.InputStream r10 = r10.open(r3)     // Catch:{ Exception -> 0x00f1 }
            com.fossil.sg0 r3 = new com.fossil.sg0     // Catch:{ all -> 0x00ea }
            com.fossil.gg0 r6 = new com.fossil.gg0     // Catch:{ all -> 0x00ea }
            com.misfit.frameworks.buttonservice.model.notification.FNotification r7 = r9.fNotification     // Catch:{ all -> 0x00ea }
            java.lang.String r7 = r7.getIconFwPath()     // Catch:{ all -> 0x00ea }
            com.fossil.ee7.a(r10, r5)     // Catch:{ all -> 0x00ea }
            byte[] r5 = com.fossil.gc7.a(r10)     // Catch:{ all -> 0x00ea }
            r6.<init>(r7, r5)     // Catch:{ all -> 0x00ea }
            r3.<init>(r6)     // Catch:{ all -> 0x00ea }
            r0.setIconConfig(r3)     // Catch:{ all -> 0x00ea }
            com.fossil.hc7.a(r10, r2)
            goto L_0x01a8
        L_0x00ea:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x00ec }
        L_0x00ec:
            r3 = move-exception
            com.fossil.hc7.a(r10, r2)
            throw r3
        L_0x00f1:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r4)
            r10.printStackTrace()
            com.fossil.i97 r10 = com.fossil.i97.a
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            r2.e(r1, r10)
            goto L_0x01a8
        L_0x0111:
            android.content.res.AssetManager r3 = r10.getAssets()     // Catch:{ Exception -> 0x018a }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r6 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion     // Catch:{ Exception -> 0x018a }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r6 = r6.getPHONE_INCOMING_CALL()     // Catch:{ Exception -> 0x018a }
            java.lang.String r6 = r6.getIconFwPath()     // Catch:{ Exception -> 0x018a }
            java.io.InputStream r3 = r3.open(r6)     // Catch:{ Exception -> 0x018a }
            com.fossil.gg0 r6 = new com.fossil.gg0     // Catch:{ all -> 0x0183 }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r7 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion     // Catch:{ all -> 0x0183 }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r7 = r7.getPHONE_INCOMING_CALL()     // Catch:{ all -> 0x0183 }
            java.lang.String r7 = r7.getIconFwPath()     // Catch:{ all -> 0x0183 }
            com.fossil.ee7.a(r3, r5)     // Catch:{ all -> 0x0183 }
            byte[] r8 = com.fossil.gc7.a(r3)     // Catch:{ all -> 0x0183 }
            r6.<init>(r7, r8)     // Catch:{ all -> 0x0183 }
            com.fossil.hc7.a(r3, r2)
            android.content.res.AssetManager r10 = r10.getAssets()
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r3 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r3 = r3.getPHONE_MISSED_CALL()
            java.lang.String r3 = r3.getIconFwPath()
            java.io.InputStream r10 = r10.open(r3)
            com.fossil.gg0 r3 = new com.fossil.gg0     // Catch:{ all -> 0x017c }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName$Companion r7 = com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj.AApplicationName.Companion     // Catch:{ all -> 0x017c }
            com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj$AApplicationName r7 = r7.getPHONE_MISSED_CALL()     // Catch:{ all -> 0x017c }
            java.lang.String r7 = r7.getIconFwPath()     // Catch:{ all -> 0x017c }
            com.fossil.ee7.a(r10, r5)     // Catch:{ all -> 0x017c }
            byte[] r5 = com.fossil.gc7.a(r10)     // Catch:{ all -> 0x017c }
            r3.<init>(r7, r5)     // Catch:{ all -> 0x017c }
            com.fossil.hc7.a(r10, r2)
            com.fossil.sg0 r10 = new com.fossil.sg0
            r10.<init>(r6)
            com.fossil.ba0 r2 = com.fossil.ba0.INCOMING_CALL
            com.fossil.sg0 r10 = r10.setIconForType(r2, r6)
            com.fossil.ba0 r2 = com.fossil.ba0.MISSED_CALL
            com.fossil.sg0 r10 = r10.setIconForType(r2, r3)
            r0.setIconConfig(r10)
            goto L_0x01a8
        L_0x017c:
            r2 = move-exception
            throw r2     // Catch:{ all -> 0x017e }
        L_0x017e:
            r3 = move-exception
            com.fossil.hc7.a(r10, r2)
            throw r3
        L_0x0183:
            r10 = move-exception
            throw r10     // Catch:{ all -> 0x0185 }
        L_0x0185:
            r2 = move-exception
            com.fossil.hc7.a(r3, r10)
            throw r2
        L_0x018a:
            r10 = move-exception
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r4)
            r10.printStackTrace()
            com.fossil.i97 r10 = com.fossil.i97.a
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            r2.e(r1, r10)
        L_0x01a8:
            r10 = r0
        L_0x01a9:
            java.lang.String r0 = r9.sender
            if (r0 == 0) goto L_0x01b0
            r10.setSender(r0)
        L_0x01b0:
            java.lang.Short r0 = r9.priority
            if (r0 == 0) goto L_0x01bb
            short r0 = r0.shortValue()
            r10.setPriority(r0)
        L_0x01bb:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter.toSDKNotificationFilter(android.content.Context):com.fossil.y90");
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeParcelable(this.fNotification, 0);
        String str = this.sender;
        if (str != null) {
            parcel.writeInt(1);
            parcel.writeString(str);
        } else {
            parcel.writeInt(0);
        }
        Short sh = this.priority;
        if (sh != null) {
            short shortValue = sh.shortValue();
            parcel.writeInt(1);
            parcel.writeInt(shortValue);
        } else {
            parcel.writeInt(0);
        }
        aa0 aa0 = this.handMovingConfig;
        if (aa0 != null) {
            parcel.writeInt(1);
            parcel.writeParcelable(aa0, 0);
        } else {
            parcel.writeInt(0);
        }
        ca0 ca0 = this.vibePattern;
        if (ca0 != null) {
            parcel.writeInt(1);
            parcel.writeInt(ca0.ordinal());
            return;
        }
        parcel.writeInt(0);
    }

    @DexIgnore
    public AppNotificationFilter(FNotification fNotification2) {
        ee7.b(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public AppNotificationFilter(Parcel parcel) {
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        if (parcel.readInt() == 1) {
            this.sender = parcel.readString();
        }
        if (parcel.readInt() == 1) {
            this.priority = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readInt() == 1) {
            this.handMovingConfig = (aa0) parcel.readParcelable(j70.class.getClassLoader());
        }
        if (parcel.readInt() == 1) {
            this.vibePattern = ca0.values()[parcel.readInt()];
        }
    }
}
