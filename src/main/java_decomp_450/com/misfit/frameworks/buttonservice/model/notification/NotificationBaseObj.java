package com.misfit.frameworks.buttonservice.model.notification;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.ba0;
import com.fossil.ee7;
import com.fossil.o90;
import com.fossil.p87;
import com.fossil.r90;
import com.fossil.s90;
import com.fossil.x97;
import com.fossil.z90;
import com.fossil.zd7;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.LifeCountDownObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NotificationBaseObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<NotificationBaseObj> CREATOR; // = new NotificationBaseObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public /* final */ int LIFE_COUNT_DOWN; // = 7;
    @DexIgnore
    public /* final */ LifeCountDownObject lifeCountDownObject; // = new LifeCountDownObject(7);
    @DexIgnore
    public String message;
    @DexIgnore
    public NotificationControlActionStatus notificationControlActionStatus;
    @DexIgnore
    public NotificationControlActionType notificationControlActionType;
    @DexIgnore
    public List<ANotificationFlag> notificationFlags;
    @DexIgnore
    public ANotificationType notificationType;
    @DexIgnore
    public String sender;
    @DexIgnore
    public int senderId; // = -1;
    @DexIgnore
    public String title;
    @DexIgnore
    public int uid;

    @DexIgnore
    public enum ANotificationFlag {
        SILENT,
        IMPORTANT,
        PRE_EXISTING,
        ALLOW_USER_REPLY_MESSAGE,
        ALLOW_USER_ACCEPT_CALL,
        ALLOW_USER_REJECT_CALL;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[ANotificationFlag.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[ANotificationFlag.SILENT.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationFlag.IMPORTANT.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationFlag.PRE_EXISTING.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REPLY_MESSAGE.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_ACCEPT_CALL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationFlag.ALLOW_USER_REJECT_CALL.ordinal()] = 6;
            }
            */
        }

        @DexIgnore
        public final z90 toSDKNotificationFlag() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return z90.SILENT;
                case 2:
                    return z90.IMPORTANT;
                case 3:
                    return z90.PRE_EXISTING;
                case 4:
                    return z90.ALLOW_USER_REPLY_ACTION;
                case 5:
                    return z90.ALLOW_USER_POSITIVE_ACTION;
                case 6:
                    return z90.ALLOW_USER_NEGATIVE_ACTION;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore
    public enum ANotificationType {
        UNSUPPORTED,
        INCOMING_CALL,
        TEXT,
        NOTIFICATION,
        EMAIL,
        CALENDAR,
        MISSED_CALL,
        REMOVED;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[ANotificationType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[ANotificationType.UNSUPPORTED.ordinal()] = 1;
                $EnumSwitchMapping$0[ANotificationType.INCOMING_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[ANotificationType.TEXT.ordinal()] = 3;
                $EnumSwitchMapping$0[ANotificationType.NOTIFICATION.ordinal()] = 4;
                $EnumSwitchMapping$0[ANotificationType.EMAIL.ordinal()] = 5;
                $EnumSwitchMapping$0[ANotificationType.CALENDAR.ordinal()] = 6;
                $EnumSwitchMapping$0[ANotificationType.MISSED_CALL.ordinal()] = 7;
                $EnumSwitchMapping$0[ANotificationType.REMOVED.ordinal()] = 8;
            }
            */
        }

        @DexIgnore
        public final ba0 toSDKNotificationType() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return ba0.UNSUPPORTED;
                case 2:
                    return ba0.INCOMING_CALL;
                case 3:
                    return ba0.TEXT;
                case 4:
                    return ba0.NOTIFICATION;
                case 5:
                    return ba0.EMAIL;
                case 6:
                    return ba0.CALENDAR;
                case 7:
                    return ba0.MISSED_CALL;
                case 8:
                    return ba0.REMOVED;
                default:
                    throw new p87();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(zd7 zd7) {
            this();
        }
    }

    @DexIgnore
    public enum NotificationControlActionStatus {
        SUCCESS,
        FAILED,
        NOT_FOUND;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[NotificationControlActionStatus.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[NotificationControlActionStatus.SUCCESS.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionStatus.FAILED.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionStatus.NOT_FOUND.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public final r90 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return r90.SUCCESS;
            }
            if (i == 2) {
                return r90.FAILED;
            }
            if (i == 3) {
                return r90.NOT_FOUND;
            }
            throw new p87();
        }
    }

    @DexIgnore
    public enum NotificationControlActionType {
        ACCEPT_PHONE_CALL,
        REJECT_PHONE_CALL,
        DISMISS_NOTIFICATION,
        REPLY_MESSAGE;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

            /*
            static {
                int[] iArr = new int[NotificationControlActionType.values().length];
                $EnumSwitchMapping$0 = iArr;
                iArr[NotificationControlActionType.ACCEPT_PHONE_CALL.ordinal()] = 1;
                $EnumSwitchMapping$0[NotificationControlActionType.REJECT_PHONE_CALL.ordinal()] = 2;
                $EnumSwitchMapping$0[NotificationControlActionType.DISMISS_NOTIFICATION.ordinal()] = 3;
                $EnumSwitchMapping$0[NotificationControlActionType.REPLY_MESSAGE.ordinal()] = 4;
            }
            */
        }

        @DexIgnore
        public final s90 toSDKNotificationType() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return s90.ACCEPT_PHONE_CALL;
            }
            if (i == 2) {
                return s90.REJECT_PHONE_CALL;
            }
            if (i == 3) {
                return s90.DISMISS_NOTIFICATION;
            }
            if (i == 4) {
                return s90.REPLY_MESSAGE;
            }
            throw new p87();
        }
    }

    @DexIgnore
    public NotificationBaseObj() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final LifeCountDownObject getLifeCountDownObject() {
        return this.lifeCountDownObject;
    }

    @DexIgnore
    public final String getMessage() {
        String str = this.message;
        if (str != null) {
            return str;
        }
        ee7.d("message");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionStatus getNotificationControlActionStatus() {
        NotificationControlActionStatus notificationControlActionStatus2 = this.notificationControlActionStatus;
        if (notificationControlActionStatus2 != null) {
            return notificationControlActionStatus2;
        }
        ee7.d("notificationControlActionStatus");
        throw null;
    }

    @DexIgnore
    public final NotificationControlActionType getNotificationControlActionType() {
        NotificationControlActionType notificationControlActionType2 = this.notificationControlActionType;
        if (notificationControlActionType2 != null) {
            return notificationControlActionType2;
        }
        ee7.d("notificationControlActionType");
        throw null;
    }

    @DexIgnore
    public final List<ANotificationFlag> getNotificationFlags() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list != null) {
            return list;
        }
        ee7.d("notificationFlags");
        throw null;
    }

    @DexIgnore
    public final ANotificationType getNotificationType() {
        ANotificationType aNotificationType = this.notificationType;
        if (aNotificationType != null) {
            return aNotificationType;
        }
        ee7.d("notificationType");
        throw null;
    }

    @DexIgnore
    public final String getSender() {
        String str = this.sender;
        if (str != null) {
            return str;
        }
        ee7.d(RemoteFLogger.MESSAGE_SENDER_KEY);
        throw null;
    }

    @DexIgnore
    public final int getSenderId() {
        return this.senderId;
    }

    @DexIgnore
    public final String getTitle() {
        String str = this.title;
        if (str != null) {
            return str;
        }
        ee7.d("title");
        throw null;
    }

    @DexIgnore
    public final int getUid() {
        return this.uid;
    }

    @DexIgnore
    public final void setMessage(String str) {
        ee7.b(str, "<set-?>");
        this.message = str;
    }

    @DexIgnore
    public final void setNotificationControlActionStatus(NotificationControlActionStatus notificationControlActionStatus2) {
        ee7.b(notificationControlActionStatus2, "<set-?>");
        this.notificationControlActionStatus = notificationControlActionStatus2;
    }

    @DexIgnore
    public final void setNotificationControlActionType(NotificationControlActionType notificationControlActionType2) {
        ee7.b(notificationControlActionType2, "<set-?>");
        this.notificationControlActionType = notificationControlActionType2;
    }

    @DexIgnore
    public final void setNotificationFlags(List<ANotificationFlag> list) {
        ee7.b(list, "<set-?>");
        this.notificationFlags = list;
    }

    @DexIgnore
    public final void setNotificationType(ANotificationType aNotificationType) {
        ee7.b(aNotificationType, "<set-?>");
        this.notificationType = aNotificationType;
    }

    @DexIgnore
    public final void setSender(String str) {
        ee7.b(str, "<set-?>");
        this.sender = str;
    }

    @DexIgnore
    public final void setSenderId(int i) {
        this.senderId = i;
    }

    @DexIgnore
    public final void setTitle(String str) {
        ee7.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setUid(int i) {
        this.uid = i;
    }

    @DexIgnore
    public final void toExistedNotification() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list == null) {
            return;
        }
        if (list == null) {
            ee7.d("notificationFlags");
            throw null;
        } else if (!list.contains(ANotificationFlag.PRE_EXISTING)) {
            List<ANotificationFlag> list2 = this.notificationFlags;
            if (list2 != null) {
                list2.add(ANotificationFlag.PRE_EXISTING);
            } else {
                ee7.d("notificationFlags");
                throw null;
            }
        }
    }

    @DexIgnore
    public String toRemoteLogString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public abstract o90 toSDKNotification();

    @DexIgnore
    public final List<z90> toSDKNotificationFlags(List<? extends ANotificationFlag> list) {
        ee7.b(list, "$this$toSDKNotificationFlags");
        ArrayList arrayList = new ArrayList(x97.a(list, 10));
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toSDKNotificationFlag());
        }
        return arrayList;
    }

    @DexIgnore
    public final void toSilentNotification() {
        List<ANotificationFlag> list = this.notificationFlags;
        if (list == null) {
            return;
        }
        if (list == null) {
            ee7.d("notificationFlags");
            throw null;
        } else if (!list.contains(ANotificationFlag.SILENT)) {
            List<ANotificationFlag> list2 = this.notificationFlags;
            if (list2 != null) {
                list2.add(ANotificationFlag.SILENT);
            } else {
                ee7.d("notificationFlags");
                throw null;
            }
        }
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        ee7.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
    }

    @DexIgnore
    public NotificationBaseObj(Parcel parcel) {
        ee7.b(parcel, "parcel");
    }
}
