package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import com.fossil.ee7;
import com.fossil.na0;
import com.fossil.ra0;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WellnessWatchAppMapping extends WatchAppMapping {
    @DexIgnore
    public WellnessWatchAppMapping() {
        super(WatchAppMapping.WatchAppMappingType.INSTANCE.getWELLNESS_DASHBOARD());
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public String getHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(getMType());
        String sb2 = sb.toString();
        ee7.a((Object) sb2, "builder.toString()");
        return sb2;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMapping
    public na0 toSDKSetting() {
        return new ra0();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WellnessWatchAppMapping(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
    }
}
