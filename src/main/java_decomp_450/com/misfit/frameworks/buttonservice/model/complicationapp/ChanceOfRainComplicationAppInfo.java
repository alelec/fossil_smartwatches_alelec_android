package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.cb0;
import com.fossil.cf0;
import com.fossil.ee7;
import com.fossil.r60;
import com.fossil.ta0;
import com.fossil.ub0;
import com.fossil.yb0;
import com.fossil.ye0;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ChanceOfRainComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public int probability;

    @DexIgnore
    public ChanceOfRainComplicationAppInfo(int i, long j) {
        super(cb0.CHANCE_OF_RAIN_COMPLICATION);
        this.probability = i;
        this.expiredAt = j;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceData() {
        return new ye0(new ta0(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public cf0 getSDKDeviceResponse(yb0 yb0, r60 r60) {
        ee7.b(yb0, "deviceRequest");
        if (!(yb0 instanceof ub0)) {
            return null;
        }
        return new ye0((ub0) yb0, new ta0(this.expiredAt, this.probability));
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.probability);
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ChanceOfRainComplicationAppInfo(Parcel parcel) {
        super(parcel);
        ee7.b(parcel, "parcel");
        this.probability = parcel.readInt();
        this.expiredAt = parcel.readLong();
    }
}
