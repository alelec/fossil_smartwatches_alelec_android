package com.misfit.frameworks.buttonservice.model.pairing;

import android.os.Parcel;
import com.fossil.ee7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingAuthorizeResponse extends PairingResponse {
    @DexIgnore
    public /* final */ long timeOutDuration;

    @DexIgnore
    public PairingAuthorizeResponse(long j) {
        this.timeOutDuration = j;
    }

    @DexIgnore
    public final long getTimeOutDuration() {
        return this.timeOutDuration;
    }

    @DexIgnore
    @Override // com.misfit.frameworks.buttonservice.model.pairing.PairingResponse
    public void writeToParcel(Parcel parcel, int i) {
        ee7.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeLong(this.timeOutDuration);
    }

    @DexIgnore
    public PairingAuthorizeResponse(Parcel parcel) {
        ee7.b(parcel, "parcel");
        this.timeOutDuration = parcel.readLong();
    }
}
