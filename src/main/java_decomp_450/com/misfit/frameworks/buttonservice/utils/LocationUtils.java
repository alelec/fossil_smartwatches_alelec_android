package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import com.facebook.places.model.PlaceFields;
import com.fossil.v6;
import com.fossil.xs7;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LocationUtils {
    @DexIgnore
    public static /* final */ String HUAWEI_LOCAL_PROVIDER; // = "local_database";
    @DexIgnore
    public static /* final */ String HUAWEI_MODEL; // = "huawei";

    @DexIgnore
    public static boolean isBackgroundLocationPermissionGranted(Context context) {
        return v6.a(context, "android.permission.ACCESS_BACKGROUND_LOCATION") == 0;
    }

    @DexIgnore
    public static boolean isLocationEnable(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(PlaceFields.LOCATION);
        if (locationManager == null) {
            return false;
        }
        String bestProvider = locationManager.getBestProvider(new Criteria(), true);
        if (!(xs7.a(bestProvider) || "passive".equals(bestProvider) || (HUAWEI_MODEL.equalsIgnoreCase(Build.MANUFACTURER) && HUAWEI_LOCAL_PROVIDER.equalsIgnoreCase(bestProvider)))) {
            return true;
        }
        try {
            if (Settings.Secure.getInt(context.getContentResolver(), "location_mode") != 0) {
                return true;
            }
            return false;
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public static boolean isLocationPermissionGranted(Context context) {
        return v6.a(context, "android.permission.ACCESS_FINE_LOCATION") == 0;
    }
}
