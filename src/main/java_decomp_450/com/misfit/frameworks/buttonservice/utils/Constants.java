package com.misfit.frameworks.buttonservice.utils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Constants {
    @DexIgnore
    public static /* final */ String BOTTOM_BACKGROUND_NAME; // = "bottom_bg";
    @DexIgnore
    public static /* final */ String CURRENT_WORKOUT_SESSION; // = "CURRENT_WORKOUT_SESSION";
    @DexIgnore
    public static /* final */ String DEFAULT_VERSION; // = "0.0";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_ACTION; // = "DEVICE_REQUEST_ACTION";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_EVENT; // = "DEVICE_REQUEST_EVENT";
    @DexIgnore
    public static /* final */ String DEVICE_REQUEST_EXTRA; // = "DEVICE_REQUEST_EXTRA";
    @DexIgnore
    public static /* final */ Constants INSTANCE; // = new Constants();
    @DexIgnore
    public static /* final */ String IS_JUST_OTA; // = "IS_JUST_OTA";
    @DexIgnore
    public static /* final */ String LEFT_BACKGROUND_NAME; // = "left_bg";
    @DexIgnore
    public static /* final */ String MAIN_BACKGROUND_NAME; // = "main_bg";
    @DexIgnore
    public static /* final */ int MAXIMUM_SECOND_TIMEZONE_OFFSET; // = 840;
    @DexIgnore
    public static /* final */ float MAXIMUM_USER_BIOMETRIC_HEIGHT; // = 2.5f;
    @DexIgnore
    public static /* final */ float MAXIMUM_USER_BIOMETRIC_WEIGHT; // = 250.0f;
    @DexIgnore
    public static /* final */ int MINIMUM_SECOND_TIMEZONE_OFFSET; // = -720;
    @DexIgnore
    public static /* final */ float MINIMUM_USER_BIOMETRIC_HEIGHT; // = 1.0f;
    @DexIgnore
    public static /* final */ float MINIMUM_USER_BIOMETRIC_WEIGHT; // = 35.0f;
    @DexIgnore
    public static /* final */ String NOTIFICATION_REQUEST_REPLY_MESSAGE_ID; // = "NOTIFICATION_REQUEST_REPLY_MESSAGE_ID";
    @DexIgnore
    public static /* final */ String NOTIFICATION_UID; // = "NOTIFICATION_UID";
    @DexIgnore
    public static /* final */ String PHOTO_BINARY_NAME_SUFFIX; // = "_bin";
    @DexIgnore
    public static /* final */ String PHOTO_IMAGE_NAME_SUFFIX; // = "_img";
    @DexIgnore
    public static /* final */ String RIGHT_BACKGROUND_NAME; // = "right_bg";
    @DexIgnore
    public static /* final */ String START_FOREGROUND_ACTION; // = "START_FOREGROUND_ACTION";
    @DexIgnore
    public static /* final */ String STOP_FOREGROUND_ACTION; // = "STOP_FOREGROUND_ACTION";
    @DexIgnore
    public static /* final */ String TOP_BACKGROUND_NAME; // = "top_bg";
}
