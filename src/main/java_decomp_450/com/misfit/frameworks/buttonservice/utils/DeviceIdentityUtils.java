package com.misfit.frameworks.buttonservice.utils;

import android.text.TextUtils;
import com.fossil.xs7;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.common.constants.Constants;

public class DeviceIdentityUtils {
    public static final String FAKE_SAM_SERIAL_NUMBER_PREFIX = "SAM-FSL";
    public static final String FLASH_SERIAL_NUMBER_PREFIX = "F";
    public static final String[] Q_MOTION_PREFIX = {"BF", "BM", "BK", "BS"};
    public static final String RAY_SERIAL_NUMBER_PREFIX = "B0";
    public static final String RMM_SERIAL_NUMBER_PREFIX = "C0";
    public static final String SAM_DIANA_SERIAL_NUMBER_PREFIX = "D0";
    public static final String SAM_MINI_SERIAL_NUMBER_PREFIX = "M0";
    public static final String SAM_SE0_SERIAL_NUMBER_PREFIX = "Z0";
    public static final String SAM_SERIAL_NUMBER_PREFIX = "W0";
    public static final String SAM_SLIM_SERIAL_NUMBER_PREFIX = "L0";
    public static final String SHINE2_SERIAL_NUMBER_PREFIX = "S2";
    public static final String SHINE_SERIAL_NUMBER_PREFIX = "S";
    public static final String SPEEDO_SERIAL_NUMBER_PREFIX = "SV0EZ";
    public static final String SWAROVSKI_SERIAL_NUMBER_PREFIX = "SC";

    public static /* synthetic */ class Anon1 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE;

        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE[] r0 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE = r0
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.RMM     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x001d }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.Q_MOTION     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.FAKE_SAM     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x003e }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM_SLIM     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SAM_MINI     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.SE0     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.DEVICE.DIANA     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils.Anon1.<clinit>():void");
        }
        */
    }

    public enum QMotionType {
        BLACK("DZ"),
        STAINLESS_SILVER("SZ"),
        GOLD("GZ"),
        ROSE_GOLD("R1"),
        BLUE("BZ"),
        COPPER("PZ");
        
        public String value;

        public QMotionType(String str) {
            this.value = str;
        }

        public static QMotionType fromColorCode(String str) {
            QMotionType[] values = values();
            for (QMotionType qMotionType : values) {
                if (qMotionType.getValue().equalsIgnoreCase(str)) {
                    return qMotionType;
                }
            }
            return ROSE_GOLD;
        }

        public String getValue() {
            return this.value;
        }
    }

    public static MFDeviceFamily getDeviceFamily(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return MFDeviceFamily.DEVICE_FAMILY_RMM;
            case 2:
                return MFDeviceFamily.DEVICE_FAMILY_Q_MOTION;
            case 3:
            case 4:
                return MFDeviceFamily.DEVICE_FAMILY_SAM;
            case 5:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_SLIM;
            case 6:
                return MFDeviceFamily.DEVICE_FAMILY_SAM_MINI;
            case 7:
                return MFDeviceFamily.DEVICE_FAMILY_SE0;
            case 8:
                return MFDeviceFamily.DEVICE_FAMILY_DIANA;
            default:
                return MFDeviceFamily.UNKNOWN;
        }
    }

    public static String getNameBySerial(String str) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()]) {
            case 1:
                return "RMM";
            case 2:
                return "Q Motion";
            case 3:
            case 4:
                return "Hybrid Smartwatch";
            case 5:
                return "SAM Slim";
            case 6:
                return "SAM Mini";
            default:
                return "UNKNOWN";
        }
    }

    public static QMotionType getQMotionTypeBySerial(String str) {
        if (TextUtils.isEmpty(str) || str.length() < 4) {
            return QMotionType.ROSE_GOLD;
        }
        return QMotionType.fromColorCode(xs7.a(str, 3, 5));
    }

    public static boolean isDianaDevice(String str) {
        return str != null && str.startsWith(SAM_DIANA_SERIAL_NUMBER_PREFIX);
    }

    public static boolean isFlash(String str) {
        return str != null && str.startsWith(FLASH_SERIAL_NUMBER_PREFIX);
    }

    public static boolean isFlashButton(String str) {
        return str != null && str.equals(Constants.BUTTON_MODEL);
    }

    public static boolean isMisfitDevice(String str) {
        return FossilDeviceSerialPatternUtil.isRmmDevice(str) || isFlash(str) || isFlashButton(str) || isRay(str) || isShine(str) || isShine2(str) || isSpeedoShine(str) || isSwarovskiShine(str) || isQMotion(str) || FossilDeviceSerialPatternUtil.isHybridSmartWatchDevice(str);
    }

    public static boolean isQMotion(String str) {
        if (!TextUtils.isEmpty(str) && str.length() > 2) {
            String substring = str.substring(0, 2);
            for (String str2 : Q_MOTION_PREFIX) {
                if (substring.equalsIgnoreCase(str2)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isRay(String str) {
        return str != null && str.startsWith(RAY_SERIAL_NUMBER_PREFIX);
    }

    public static boolean isShine(String str) {
        return str != null && str.startsWith(SHINE_SERIAL_NUMBER_PREFIX) && !str.startsWith("S2");
    }

    public static boolean isShine2(String str) {
        return str != null && str.startsWith("S2");
    }

    public static boolean isSpeedoShine(String str) {
        return str != null && str.startsWith(SPEEDO_SERIAL_NUMBER_PREFIX);
    }

    public static boolean isSwarovskiShine(String str) {
        return str != null && str.startsWith(SWAROVSKI_SERIAL_NUMBER_PREFIX);
    }

    public static boolean isWearOSDevice(String str) {
        return str == null || str.isEmpty();
    }
}
