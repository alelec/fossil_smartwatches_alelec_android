package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.ButtonType;

public class DeviceUtils {
    public static final String PREFERENCE_NAME = "com.misfit.frameworks.buttonservice.cacheddevices";
    public static DeviceUtils sInstance;
    public Context context;

    public static /* synthetic */ class Anon1 {
        public static final /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$ButtonType;

        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|(3:21|22|24)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|24) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0049 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0054 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0060 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0078 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0012 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001d */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0028 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0033 */
        /*
        static {
            /*
                com.misfit.frameworks.common.enums.ButtonType[] r0 = com.misfit.frameworks.common.enums.ButtonType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType = r0
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.SELFIE     // Catch:{ NoSuchFieldError -> 0x0012 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0012 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0012 }
            L_0x0012:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x001d }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.MUSIC     // Catch:{ NoSuchFieldError -> 0x001d }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001d }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001d }
            L_0x001d:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0028 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.PRESENTATION     // Catch:{ NoSuchFieldError -> 0x0028 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0028 }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0028 }
            L_0x0028:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0033 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.ACTIVITY     // Catch:{ NoSuchFieldError -> 0x0033 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0033 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0033 }
            L_0x0033:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x003e }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.BOLT_CONTROL     // Catch:{ NoSuchFieldError -> 0x003e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x003e }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x003e }
            L_0x003e:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0049 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.CUSTOM     // Catch:{ NoSuchFieldError -> 0x0049 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0049 }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0049 }
            L_0x0049:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0054 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.PLUTO_TRACKER     // Catch:{ NoSuchFieldError -> 0x0054 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0054 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0054 }
            L_0x0054:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0060 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.SILVRETTA_TRACKER     // Catch:{ NoSuchFieldError -> 0x0060 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0060 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0060 }
            L_0x0060:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x006c }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.BMW_TRACKER     // Catch:{ NoSuchFieldError -> 0x006c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006c }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006c }
            L_0x006c:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0078 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.SWAROVSKI_TRACKER     // Catch:{ NoSuchFieldError -> 0x0078 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0078 }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0078 }
            L_0x0078:
                int[] r0 = com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.misfit.frameworks.common.enums.ButtonType r1 = com.misfit.frameworks.common.enums.ButtonType.ThirdPartyApp     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.buttonservice.utils.DeviceUtils.Anon1.<clinit>():void");
        }
        */
    }

    public DeviceUtils(Context context2) {
        this.context = context2.getApplicationContext();
    }

    public static synchronized DeviceUtils getInstance(Context context2) {
        DeviceUtils deviceUtils;
        synchronized (DeviceUtils.class) {
            if (sInstance == null) {
                sInstance = new DeviceUtils(context2);
            }
            deviceUtils = sInstance;
        }
        return deviceUtils;
    }

    public ButtonType bleCommandToButtonType(int i) {
        if (i == 1) {
            return ButtonType.SELFIE;
        }
        if (i == 2) {
            return ButtonType.MUSIC;
        }
        if (i == 3) {
            return ButtonType.PRESENTATION;
        }
        if (i == 5) {
            return ButtonType.ACTIVITY;
        }
        if (i == 6) {
            return ButtonType.BOLT_CONTROL;
        }
        if (i == 7) {
            return ButtonType.CUSTOM;
        }
        if (i != 50) {
            return ButtonType.NONE;
        }
        return ButtonType.PLUTO_TRACKER;
    }

    public int buttonTypeToBleCommand(ButtonType buttonType) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType[buttonType.ordinal()]) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                return 7;
            default:
                return 0;
        }
    }

    public void clearMacAddress(Context context2, String str) {
        setString(context2, str, "");
    }

    public ButtonType getButtonTypeByAction(int i) {
        if (Action.Music.isActionBelongToThisType(i)) {
            return ButtonType.MUSIC;
        }
        if (Action.Selfie.isActionBelongToThisType(i)) {
            return ButtonType.SELFIE;
        }
        if (Action.Presenter.isActionBelongToThisType(i)) {
            return ButtonType.PRESENTATION;
        }
        if (Action.ActivityTracker.isActionBelongToThisType(i)) {
            return ButtonType.ACTIVITY;
        }
        if (Action.DisplayMode.isActionBelongToThisType(i)) {
            return ButtonType.DISPLAY_MODE;
        }
        if (i == 505) {
            return ButtonType.RING_MY_PHONE;
        }
        if (i == 1000) {
            return ButtonType.GOAL_TRACKING;
        }
        if (i <= 600 || i >= 699) {
            return ButtonType.NONE;
        }
        return ButtonType.BOLT_CONTROL;
    }

    public String getMacAddress(Context context2, String str) {
        return getString(context2, str);
    }

    public SharedPreferences getPreferences(Context context2) {
        return context2.getSharedPreferences(PREFERENCE_NAME, 0);
    }

    public String getSerial(Context context2, String str) {
        return getString(context2, str);
    }

    public String getString(Context context2, String str) {
        SharedPreferences preferences = getPreferences(context2);
        if (preferences != null) {
            return preferences.getString(str, "");
        }
        return "";
    }

    public void saveMacAddress(Context context2, String str, String str2) {
        setString(context2, str, str2);
        setString(context2, str2, str);
    }

    public void setString(Context context2, String str, String str2) {
        SharedPreferences preferences = getPreferences(context2);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(str, str2);
            edit.apply();
        }
    }
}
