package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.gt7;
import com.fossil.kt7;
import com.fossil.lt7;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.HTTPMethod;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFHeader;
import com.misfit.frameworks.network.manager.MFNetwork;
import com.misfit.frameworks.network.responses.MFResponse;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MFMultipartRequest extends MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFMultipartRequest";
    @DexIgnore
    public gt7 multipartEntity;

    @DexIgnore
    public MFMultipartRequest(Context context) {
        super(context);
    }

    @DexIgnore
    private void appendFormDataToEntity(String str, gt7 gt7) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                gt7.a(next, new lt7(jSONObject.get(next).toString()));
            }
        } catch (Exception e) {
            String str2 = TAG;
            MFLogger.d(str2, "Exception when appenFormDataToEntity " + e);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    public void buildHeader(MFHeader mFHeader) {
        HashMap<String, String> headerMap;
        if (mFHeader != null && (headerMap = ((MFBaseRequest) this).configuration.getHeader().getHeaderMap()) != null) {
            for (String str : headerMap.keySet()) {
                String str2 = headerMap.get(str);
                if (!TextUtils.isEmpty(str2)) {
                    ((MFBaseRequest) this).httpURLConnection.setRequestProperty(str, str2);
                }
            }
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    public void buildRequest() {
        ((MFBaseRequest) this).configuration = initConfiguration();
        ((MFBaseRequest) this).jsonData = initJsonData();
        ((MFBaseRequest) this).method = initHttpMethod();
        ((MFBaseRequest) this).apiMethod = initApiMethod();
        ((MFBaseRequest) this).buttonApiResponse = initResponse();
        JSONObject initUploadFileUrl = initUploadFileUrl();
        Uri parse = Uri.parse(((MFBaseRequest) this).configuration.getBaseServerUrl() + ((MFBaseRequest) this).apiMethod);
        gt7 gt7 = new gt7();
        try {
            if (((MFBaseRequest) this).jsonData != null) {
                if (((MFBaseRequest) this).jsonData instanceof JSONObject) {
                    JSONObject jSONObject = (JSONObject) ((MFBaseRequest) this).jsonData;
                    Iterator<String> keys = jSONObject.keys();
                    while (keys.hasNext()) {
                        appendFormDataToEntity(jSONObject.get(keys.next()).toString(), gt7);
                    }
                } else {
                    throw new Exception("For MultipartRequest, jsonData must be an object");
                }
            }
            if (initUploadFileUrl != null) {
                Iterator<String> keys2 = initUploadFileUrl.keys();
                while (keys2.hasNext()) {
                    String next = keys2.next();
                    gt7.a(next, new kt7(new File(initUploadFileUrl.get(next).toString()), "image/jpeg"));
                }
            }
            URL url = new URL(parse.toString());
            ((MFBaseRequest) this).url = url;
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            ((MFBaseRequest) this).httpURLConnection = httpURLConnection;
            httpURLConnection.setDoOutput(true);
            ((MFBaseRequest) this).httpURLConnection.setRequestMethod("POST");
            buildHeader(((MFBaseRequest) this).configuration.getHeader());
        } catch (Exception e) {
            String str = TAG;
            MFLogger.e(str, "Error when build request multipart " + e);
        }
    }

    @DexIgnore
    @Override // com.misfit.frameworks.network.request.MFBaseRequest
    public MFResponse execute() {
        BufferedInputStream bufferedInputStream;
        InputStream inputStream = null;
        try {
            buildRequest();
            ((MFBaseRequest) this).buttonApiResponse.setRequestId(((MFBaseRequest) this).requestId);
            if (((MFBaseRequest) this).httpURLConnection == null) {
                MFLogger.d(TAG, "execute - httpUriRequest == null");
                ((MFBaseRequest) this).buttonApiResponse.setHttpReturnCode(MFNetworkReturnCode.REQUEST_NOT_FOUND);
                MFResponse mFResponse = ((MFBaseRequest) this).buttonApiResponse;
                HttpURLConnection httpURLConnection = ((MFBaseRequest) this).httpURLConnection;
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                return mFResponse;
            }
            if (MFNetwork.isDebug()) {
                for (Map.Entry<String, List<String>> entry : ((MFBaseRequest) this).httpURLConnection.getRequestProperties().entrySet()) {
                    List<String> value = entry.getValue();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Header value--");
                    sb.append(entry.getKey());
                    sb.append(":");
                    sb.append((value == null || value.isEmpty()) ? "" : value.get(0));
                    MFLogger.d(str, sb.toString());
                }
                String str2 = TAG;
                MFLogger.d(str2, "Inside MFBaseRequest.doInBackground - sending REQUEST: command=, requestId=" + ((MFBaseRequest) this).requestId + "\nurl=" + ((MFBaseRequest) this).httpURLConnection.getURL().toString() + "\njsonData =");
                if (((MFBaseRequest) this).jsonData != null) {
                    String str3 = TAG;
                    MFLogger.d(str3, "Inside MFBaseRequest.doInBackground - jsondata : " + ((MFBaseRequest) this).jsonData.toString());
                }
            }
            ((MFBaseRequest) this).httpURLConnection.setUseCaches(false);
            if (((MFBaseRequest) this).method == HTTPMethod.POST || ((MFBaseRequest) this).method == HTTPMethod.PATCH) {
                DataOutputStream dataOutputStream = new DataOutputStream(((MFBaseRequest) this).httpURLConnection.getOutputStream());
                this.multipartEntity.writeTo(dataOutputStream);
                dataOutputStream.flush();
                dataOutputStream.close();
            } else {
                ((MFBaseRequest) this).httpURLConnection.connect();
            }
            if (((MFBaseRequest) this).httpURLConnection.getResponseCode() == 200) {
                bufferedInputStream = new BufferedInputStream(((MFBaseRequest) this).httpURLConnection.getInputStream());
            } else {
                bufferedInputStream = new BufferedInputStream(((MFBaseRequest) this).httpURLConnection.getErrorStream());
            }
            String readStream = readStream(bufferedInputStream);
            String str4 = TAG;
            MFLogger.d(str4, "Inside MFBaseRequest.Worker.doInBackground requestId " + ((MFBaseRequest) this).buttonApiResponse.getRequestId() + "- RESPONSE {httpStatus=" + ((MFBaseRequest) this).httpURLConnection.getResponseCode() + ", jsonData=" + readStream + "}");
            ((MFBaseRequest) this).buttonApiResponse.setHttpReturnCode(((MFBaseRequest) this).httpURLConnection.getResponseCode());
            if (!TextUtils.isEmpty(readStream)) {
                Object nextValue = new JSONTokener(readStream).nextValue();
                if (nextValue instanceof JSONObject) {
                    ((MFBaseRequest) this).buttonApiResponse.parse(new JSONObject(readStream));
                } else if (nextValue instanceof JSONArray) {
                    ((MFBaseRequest) this).buttonApiResponse.parse(new JSONArray(readStream));
                }
            }
            HttpURLConnection httpURLConnection2 = ((MFBaseRequest) this).httpURLConnection;
            if (httpURLConnection2 != null) {
                httpURLConnection2.disconnect();
            }
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ((MFBaseRequest) this).buttonApiResponse;
        } catch (Exception e2) {
            String str5 = TAG;
            MFLogger.e(str5, "Error inside MFBaseRequest.Worker.doInBackground - e=" + e2);
            ((MFBaseRequest) this).exception = e2;
            HttpURLConnection httpURLConnection3 = ((MFBaseRequest) this).httpURLConnection;
            if (httpURLConnection3 != null) {
                httpURLConnection3.disconnect();
            }
            if (0 != 0) {
                inputStream.close();
            }
        } catch (Throwable th) {
            HttpURLConnection httpURLConnection4 = ((MFBaseRequest) this).httpURLConnection;
            if (httpURLConnection4 != null) {
                httpURLConnection4.disconnect();
            }
            if (0 != 0) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public abstract JSONObject initUploadFileUrl();
}
