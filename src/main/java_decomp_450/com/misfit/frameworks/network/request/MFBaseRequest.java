package com.misfit.frameworks.network.request;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.HTTPMethod;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.configuration.MFConfiguration;
import com.misfit.frameworks.network.configuration.MFHeader;
import com.misfit.frameworks.network.manager.MFNetwork;
import com.misfit.frameworks.network.responses.MFResponse;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MFBaseRequest {
    @DexIgnore
    public static /* final */ String TAG; // = "MFBaseRequest";
    @DexIgnore
    public String apiMethod;
    @DexIgnore
    public MFResponse buttonApiResponse; // = new MFResponse();
    @DexIgnore
    public MFConfiguration configuration;
    @DexIgnore
    public Context context;
    @DexIgnore
    public Exception exception;
    @DexIgnore
    public HttpURLConnection httpURLConnection;
    @DexIgnore
    public Object jsonData; // = new JSONObject();
    @DexIgnore
    public HTTPMethod method;
    @DexIgnore
    public int requestId;
    @DexIgnore
    public URL url;

    @DexIgnore
    public MFBaseRequest(Context context2) {
        this.context = context2;
    }

    @DexIgnore
    public void buildHeader(MFHeader mFHeader) {
        HashMap<String, String> headerMap;
        this.httpURLConnection.setRequestProperty("Content-Type", Constants.CONTENT_TYPE);
        this.httpURLConnection.setRequestProperty(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, com.zendesk.sdk.network.Constants.APPLICATION_JSON);
        this.httpURLConnection.setRequestProperty("User-Agent", MFNetwork.getInstance(this.context).getUserAgent());
        this.httpURLConnection.setRequestProperty("Installation-ID", MFNetwork.getInstance(this.context).getInstallationId());
        this.httpURLConnection.setRequestProperty("Locale", MFNetwork.getInstance(this.context).getLocale());
        if (this.method == HTTPMethod.PATCH) {
            this.httpURLConnection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
        }
        if (!(mFHeader == null || (headerMap = this.configuration.getHeader().getHeaderMap()) == null)) {
            for (String str : headerMap.keySet()) {
                String str2 = headerMap.get(str);
                if (!TextUtils.isEmpty(str2)) {
                    this.httpURLConnection.setRequestProperty(str, str2);
                }
            }
        }
        this.httpURLConnection.setConnectTimeout(10000);
        this.httpURLConnection.setReadTimeout(10000);
    }

    @DexIgnore
    public void buildRequest() {
        this.configuration = initConfiguration();
        this.jsonData = initJsonData();
        this.method = initHttpMethod();
        this.apiMethod = initApiMethod();
        this.buttonApiResponse = initResponse();
        Uri parse = Uri.parse(this.configuration.getBaseServerUrl() + this.apiMethod);
        try {
            if (this.method == HTTPMethod.GET) {
                if (this.jsonData != null) {
                    if (this.jsonData instanceof JSONObject) {
                        JSONObject jSONObject = (JSONObject) this.jsonData;
                        Iterator<String> keys = jSONObject.keys();
                        Uri.Builder buildUpon = parse.buildUpon();
                        while (keys.hasNext()) {
                            String next = keys.next();
                            buildUpon.appendQueryParameter(next, String.valueOf(jSONObject.get(next)));
                        }
                        parse = buildUpon.build();
                    } else {
                        throw new Exception("We do not support JSONArray now for GET request.");
                    }
                }
                URL url2 = new URL(parse.toString());
                this.url = url2;
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) url2.openConnection();
                this.httpURLConnection = httpURLConnection2;
                httpURLConnection2.setDoInput(true);
                this.httpURLConnection.setRequestMethod("GET");
            } else {
                if (this.method != HTTPMethod.POST) {
                    if (this.method != HTTPMethod.PATCH) {
                        if (this.method == HTTPMethod.PUT) {
                            URL url3 = new URL(parse.toString());
                            this.url = url3;
                            HttpURLConnection httpURLConnection3 = (HttpURLConnection) url3.openConnection();
                            this.httpURLConnection = httpURLConnection3;
                            httpURLConnection3.setDoOutput(true);
                            this.httpURLConnection.setRequestMethod("PUT");
                        } else if (this.method == HTTPMethod.DELETE) {
                            if (this.jsonData != null) {
                                if (this.jsonData instanceof JSONObject) {
                                    JSONObject jSONObject2 = (JSONObject) this.jsonData;
                                    Iterator<String> keys2 = jSONObject2.keys();
                                    Uri.Builder buildUpon2 = parse.buildUpon();
                                    while (keys2.hasNext()) {
                                        String next2 = keys2.next();
                                        buildUpon2.appendQueryParameter(next2, String.valueOf(jSONObject2.get(next2)));
                                    }
                                    parse = buildUpon2.build();
                                } else {
                                    throw new Exception("We do not support JSONArray now for DELETE request.");
                                }
                            }
                            URL url4 = new URL(parse.toString());
                            this.url = url4;
                            HttpURLConnection httpURLConnection4 = (HttpURLConnection) url4.openConnection();
                            this.httpURLConnection = httpURLConnection4;
                            httpURLConnection4.setDoInput(true);
                            this.httpURLConnection.setRequestMethod("DELETE");
                        }
                    }
                }
                URL url5 = new URL(parse.toString());
                this.url = url5;
                HttpURLConnection httpURLConnection5 = (HttpURLConnection) url5.openConnection();
                this.httpURLConnection = httpURLConnection5;
                httpURLConnection5.setDoOutput(true);
                this.httpURLConnection.setRequestMethod("POST");
            }
        } catch (Exception e) {
            String str = TAG;
            MFLogger.d(str, "Exception when build request " + e);
        }
        buildHeader(this.configuration.getHeader());
    }

    @DexIgnore
    public MFResponse execute() {
        BufferedInputStream bufferedInputStream;
        Map<String, List<String>> requestProperties;
        InputStream inputStream = null;
        try {
            buildRequest();
            this.buttonApiResponse.setRequestId(this.requestId);
            if (this.httpURLConnection == null) {
                MFLogger.d(TAG, "execute - httpUriRequest == null");
                this.buttonApiResponse.setHttpReturnCode(MFNetworkReturnCode.REQUEST_NOT_FOUND);
                return this.buttonApiResponse;
            }
            if (MFNetwork.isDebug() && (requestProperties = this.httpURLConnection.getRequestProperties()) != null) {
                for (Map.Entry<String, List<String>> entry : requestProperties.entrySet()) {
                    List<String> value = entry.getValue();
                    String str = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Header value--");
                    sb.append(entry.getKey());
                    sb.append(":");
                    sb.append((value == null || value.isEmpty()) ? "" : value.get(0));
                    MFLogger.d(str, sb.toString());
                }
                String str2 = TAG;
                MFLogger.d(str2, "Inside MFBaseRequest.doInBackground - sending REQUEST: command=, requestId=" + this.requestId + "\nurl=" + this.httpURLConnection.getURL().toString() + "\njsonData =");
                if (this.jsonData != null) {
                    String str3 = TAG;
                    MFLogger.d(str3, "Inside MFBaseRequest.doInBackground - jsondata : " + this.jsonData.toString());
                }
            }
            this.httpURLConnection.setUseCaches(false);
            if (this.method != HTTPMethod.GET) {
                byte[] initBinaryData = initBinaryData();
                if (initBinaryData != null) {
                    DataOutputStream dataOutputStream = new DataOutputStream(this.httpURLConnection.getOutputStream());
                    dataOutputStream.write(initBinaryData);
                    dataOutputStream.flush();
                    dataOutputStream.close();
                } else if (this.jsonData != null) {
                    BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(this.httpURLConnection.getOutputStream(), "UTF-8"));
                    bufferedWriter.write(this.jsonData.toString());
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } else {
                this.httpURLConnection.connect();
            }
            if (this.httpURLConnection.getResponseCode() < 200 || this.httpURLConnection.getResponseCode() >= 400) {
                bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getErrorStream());
            } else {
                bufferedInputStream = new BufferedInputStream(this.httpURLConnection.getInputStream());
            }
            String readStream = readStream(bufferedInputStream);
            String str4 = TAG;
            MFLogger.d(str4, "Inside MFBaseRequest.Worker.doInBackground requestId " + this.buttonApiResponse.getRequestId() + "- RESPONSE {httpStatus=" + this.httpURLConnection.getResponseCode() + ", jsonData=" + readStream + "}");
            this.buttonApiResponse.setHttpReturnCode(this.httpURLConnection.getResponseCode());
            if (!TextUtils.isEmpty(readStream)) {
                Object nextValue = new JSONTokener(readStream).nextValue();
                if (nextValue instanceof JSONObject) {
                    this.buttonApiResponse.parse(new JSONObject(readStream));
                } else if (nextValue instanceof JSONArray) {
                    this.buttonApiResponse.parse(new JSONArray(readStream));
                }
            }
            try {
                bufferedInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return this.buttonApiResponse;
        } catch (SocketTimeoutException e2) {
            MFLogger.e(TAG, "Error inside MFBaseRequest.Worker.doInBackground - e=SocketTimeoutException");
            this.buttonApiResponse.setHttpReturnCode(601);
            this.exception = e2;
            if (0 != 0) {
                inputStream.close();
            }
        } catch (ConnectException e3) {
            MFLogger.e(TAG, "Error inside MFBaseRequest.Worker.doInBackground - e=ConnectException");
            this.buttonApiResponse.setHttpReturnCode(500);
            this.exception = e3;
            if (0 != 0) {
                inputStream.close();
            }
        } catch (Exception e4) {
            String str5 = TAG;
            MFLogger.e(str5, "Error inside MFBaseRequest.Worker.doInBackground - e=" + e4);
            this.exception = e4;
            if (0 != 0) {
                inputStream.close();
            }
        } catch (Throwable th) {
            if (0 != 0) {
                try {
                    inputStream.close();
                } catch (IOException e5) {
                    e5.printStackTrace();
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public Exception getException() {
        return this.exception;
    }

    @DexIgnore
    public abstract String initApiMethod();

    @DexIgnore
    public byte[] initBinaryData() {
        return null;
    }

    @DexIgnore
    public abstract MFConfiguration initConfiguration();

    @DexIgnore
    public abstract HTTPMethod initHttpMethod();

    @DexIgnore
    public abstract Object initJsonData();

    @DexIgnore
    public abstract MFResponse initResponse();

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x003b A[SYNTHETIC, Splitter:B:24:0x003b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String readStream(java.io.InputStream r5) {
        /*
            r4 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r1 = 0
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0026 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0026 }
            r3.<init>(r5)     // Catch:{ IOException -> 0x0026 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0026 }
        L_0x0010:
            java.lang.String r5 = r2.readLine()     // Catch:{ IOException -> 0x0021, all -> 0x001e }
            if (r5 == 0) goto L_0x001a
            r0.append(r5)     // Catch:{ IOException -> 0x0021, all -> 0x001e }
            goto L_0x0010
        L_0x001a:
            r2.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x0034
        L_0x001e:
            r5 = move-exception
            r1 = r2
            goto L_0x0039
        L_0x0021:
            r5 = move-exception
            r1 = r2
            goto L_0x0027
        L_0x0024:
            r5 = move-exception
            goto L_0x0039
        L_0x0026:
            r5 = move-exception
        L_0x0027:
            r5.printStackTrace()     // Catch:{ all -> 0x0024 }
            if (r1 == 0) goto L_0x0034
            r1.close()
            goto L_0x0034
        L_0x0030:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0034:
            java.lang.String r5 = r0.toString()
            return r5
        L_0x0039:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x003f }
            goto L_0x0043
        L_0x003f:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0043:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.misfit.frameworks.network.request.MFBaseRequest.readStream(java.io.InputStream):java.lang.String");
    }

    @DexIgnore
    public void setRequestId(int i) {
        this.requestId = i;
    }
}
