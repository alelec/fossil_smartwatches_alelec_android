package com.misfit.frameworks.network.manager;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.SparseArray;
import com.misfit.frameworks.common.log.MFLogger;
import com.misfit.frameworks.network.BuildConfig;
import com.misfit.frameworks.network.request.MFBaseRequest;
import com.misfit.frameworks.network.responses.MFResponse;
import com.misfit.frameworks.network.utils.NetworkUtils;
import com.misfit.frameworks.network.utils.ReturnCodeRangeChecker;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFNetwork {
    @DexIgnore
    public static /* final */ int MAX_REQUEST; // = 10;
    @DexIgnore
    public static /* final */ String TAG; // = "MFNetwork";
    @DexIgnore
    public static MFNetwork instance;
    @DexIgnore
    public static boolean isDebug;
    @DexIgnore
    public String appUserAgent;
    @DexIgnore
    public Context context;
    @DexIgnore
    public int currentTotalRequest; // = 0;
    @DexIgnore
    public String installationId;
    @DexIgnore
    public String locale;
    @DexIgnore
    public NetworkService networkService;
    @DexIgnore
    public OnGlobalServerEvent onGlobalServerEvent;
    @DexIgnore
    public SparseArray pendingRequests;
    @DexIgnore
    public int requestId; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Exception val$exception;
        @DexIgnore
        public /* final */ /* synthetic */ MFResponse val$response;

        @DexIgnore
        public Anon1(MFResponse mFResponse, Exception exc) {
            this.val$response = mFResponse;
            this.val$exception = exc;
        }

        @DexIgnore
        public void run() {
            String access$000 = MFNetwork.TAG;
            MFLogger.d(access$000, "Inside .notifyListeners running with response=" + this.val$response + ", exception=" + this.val$exception + ", of id=" + this.val$response.getRequestId());
            if (this.val$response == null) {
                throw new IllegalArgumentException("Response is null!");
            } else if (this.val$exception != null) {
                synchronized (MFNetwork.this.pendingRequests) {
                    MFServerResultCallback mFServerResultCallback = (MFServerResultCallback) MFNetwork.this.pendingRequests.get(this.val$response.getRequestId());
                    MFNetwork.this.pendingRequests.remove(this.val$response.getRequestId());
                    String access$0002 = MFNetwork.TAG;
                    MFLogger.d(access$0002, "Inside .notifyListeners notify callback=" + mFServerResultCallback + ",id" + this.val$response.getRequestId());
                    if (mFServerResultCallback != null) {
                        mFServerResultCallback.onFail(this.val$response.getInternalErrorCode(), this.val$response);
                    } else {
                        MFLogger.e(MFNetwork.TAG, "Callback is null, do something");
                    }
                }
            } else {
                MFLogger.d(MFNetwork.TAG, "Inside .notifyListeners notify receiver and callback");
                MFNetwork.this.notifyReceiver(this.val$response);
                MFNetwork.this.notifyCallback(this.val$response);
            }
        }
    }

    @DexIgnore
    public interface MFServerResultCallback {
        @DexIgnore
        void onFail(int i, MFResponse mFResponse);

        @DexIgnore
        void onSuccess(MFResponse mFResponse);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class NetworkService {
        @DexIgnore
        public ExecutorService pool;

        @DexIgnore
        public NetworkService() {
            int i = !Build.MANUFACTURER.equals("HUAWEI") ? Math.max(1, Runtime.getRuntime().availableProcessors()) : 1;
            String access$000 = MFNetwork.TAG;
            MFLogger.d(access$000, "Inside " + MFNetwork.TAG + ".NetworkService - num_of_cores=" + i);
            this.pool = Executors.newFixedThreadPool(i);
        }

        @DexIgnore
        public void pollRequest(MFBaseRequest mFBaseRequest) {
            try {
                this.pool.execute(new Worker(MFNetwork.this, mFBaseRequest, null));
            } catch (Exception e) {
                String access$000 = MFNetwork.TAG;
                MFLogger.e(access$000, "Error inside " + MFNetwork.TAG + ".execute - e=" + e);
            }
        }
    }

    @DexIgnore
    public interface OnGlobalServerEvent {
        @DexIgnore
        void onServerMaintenance();

        @DexIgnore
        void onTokenExpired();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Worker implements Runnable {
        @DexIgnore
        public MFBaseRequest request;

        @DexIgnore
        public /* synthetic */ Worker(MFNetwork mFNetwork, MFBaseRequest mFBaseRequest, Anon1 anon1) {
            this(mFBaseRequest);
        }

        @DexIgnore
        public void run() {
            MFResponse execute = this.request.execute();
            String access$000 = MFNetwork.TAG;
            MFLogger.d(access$000, "Execution network is done, response is " + execute.toString() + ", id done:" + execute.getRequestId());
            MFNetwork.this.notifyListeners(execute, this.request.getException());
        }

        @DexIgnore
        public Worker(MFBaseRequest mFBaseRequest) {
            this.request = mFBaseRequest;
        }
    }

    @DexIgnore
    public MFNetwork(Context context2) {
        this.context = context2;
        this.pendingRequests = new SparseArray();
        this.networkService = new NetworkService();
    }

    @DexIgnore
    private void executeRequest(MFBaseRequest mFBaseRequest) {
        this.currentTotalRequest++;
        this.networkService.pollRequest(mFBaseRequest);
    }

    @DexIgnore
    public static synchronized MFNetwork getInstance(Context context2) {
        MFNetwork mFNetwork;
        synchronized (MFNetwork.class) {
            if (instance == null) {
                instance = new MFNetwork(context2);
            }
            mFNetwork = instance;
        }
        return mFNetwork;
    }

    @DexIgnore
    public static boolean isDebug() {
        return isDebug;
    }

    @DexIgnore
    private synchronized void notifyCallback(MFResponse mFResponse) {
        String str = TAG;
        MFLogger.d(str, "Inside .notifyListeners notifyCallback response=" + mFResponse);
        if (mFResponse == null) {
            Log.e(TAG, "Response from request is null");
            return;
        }
        MFServerResultCallback mFServerResultCallback = (MFServerResultCallback) this.pendingRequests.get(mFResponse.getRequestId());
        String str2 = TAG;
        MFLogger.d(str2, "Notify receiver with callback " + mFServerResultCallback + ", id=" + mFResponse.getRequestId() + ", code=" + mFResponse.getHttpReturnCode() + ", callback=" + mFServerResultCallback);
        if (!ReturnCodeRangeChecker.isSuccessReturnCode(mFResponse.getHttpReturnCode())) {
            if (mFResponse.getHttpReturnCode() == 401) {
                int internalErrorCode = mFResponse.getInternalErrorCode();
                if (this.onGlobalServerEvent != null && (internalErrorCode == 401 || internalErrorCode == 4012)) {
                    this.onGlobalServerEvent.onTokenExpired();
                }
            } else if ((mFResponse.getHttpReturnCode() == 504 || mFResponse.getHttpReturnCode() == 503) && this.onGlobalServerEvent != null) {
                this.onGlobalServerEvent.onServerMaintenance();
            }
            if (mFServerResultCallback != null) {
                String str3 = TAG;
                MFLogger.d(str3, "Notify receiver with callback fail of id=" + mFResponse.getRequestId());
                mFServerResultCallback.onFail(mFResponse.getHttpReturnCode(), mFResponse);
            }
        } else if (mFServerResultCallback != null) {
            String str4 = TAG;
            MFLogger.d(str4, "Notify receiver with callback success of id=" + mFResponse.getRequestId());
            mFServerResultCallback.onSuccess(mFResponse);
        }
    }

    @DexIgnore
    private void notifyListeners(MFResponse mFResponse, Exception exc) {
        String str = TAG;
        MFLogger.d(str, "Inside .notifyListeners with response=" + mFResponse + ", exception=" + exc + ", of id=" + mFResponse.getRequestId());
        new Handler(Looper.getMainLooper()).post(new Anon1(mFResponse, exc));
    }

    @DexIgnore
    private void notifyReceiver(MFResponse mFResponse) {
        MFLogger.d(TAG, "Inside .notifyListeners notify receiver");
        Bundle bundle = new Bundle();
        bundle.putSerializable("com.misfit.frameworks.response", mFResponse);
        bundle.putSerializable("com.misfit.frameworks.response", mFResponse.getCommand());
        Intent intent = new Intent();
        intent.setAction("com.misfit.frameworks.response");
        intent.putExtras(bundle);
        this.context.sendBroadcast(intent);
    }

    @DexIgnore
    public synchronized void execute(MFBaseRequest mFBaseRequest, MFServerResultCallback mFServerResultCallback) {
        if (mFServerResultCallback != null) {
            int i = this.requestId + 1;
            this.requestId = i;
            this.pendingRequests.put(i, mFServerResultCallback);
            mFBaseRequest.setRequestId(this.requestId);
            if (!NetworkUtils.isNetworkAvailable(this.context)) {
                MFResponse mFResponse = new MFResponse();
                mFResponse.setRequestId(this.requestId);
                mFServerResultCallback.onFail(601, mFResponse);
            } else {
                String str = TAG;
                MFLogger.d(str, "Execute request, currentRequest: " + this.currentTotalRequest);
                executeRequest(mFBaseRequest);
            }
        } else {
            throw new IllegalArgumentException("Callback must not be null!");
        }
    }

    @DexIgnore
    public String getInstallationId() {
        return this.installationId;
    }

    @DexIgnore
    public String getLocale() {
        return this.locale;
    }

    @DexIgnore
    public int getRequestId() {
        return this.requestId;
    }

    @DexIgnore
    public String getUserAgent() {
        return this.appUserAgent;
    }

    @DexIgnore
    public String getVersion() {
        return BuildConfig.VERSION;
    }

    @DexIgnore
    public void initialize(boolean z, String str) {
        isDebug = z;
        this.appUserAgent = str;
        MFLogger.initialize(this.context);
    }

    @DexIgnore
    public void setInstallationId(String str) {
        this.installationId = str;
    }

    @DexIgnore
    public void setLocale(String str) {
        this.locale = str;
    }

    @DexIgnore
    public void setOnGlobalServerEvent(OnGlobalServerEvent onGlobalServerEvent2) {
        this.onGlobalServerEvent = onGlobalServerEvent2;
    }
}
