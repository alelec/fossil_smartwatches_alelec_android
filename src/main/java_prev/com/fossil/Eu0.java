package com.fossil;

import com.mapped.Wg6;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Eu0 extends K60 {
    @DexIgnore
    public /* final */ Wm0 a;
    @DexIgnore
    public /* final */ Is0 b;
    @DexIgnore
    public /* final */ Sz0 c;

    @DexIgnore
    public Eu0(Wm0 wm0, Is0 is0, Sz0 sz0) {
        this.a = wm0;
        this.b = is0;
        this.c = sz0;
    }

    @DexIgnore
    public /* synthetic */ Eu0(Wm0 wm0, Is0 is0, Sz0 sz0, int i) {
        wm0 = (i & 1) != 0 ? Wm0.a : wm0;
        throw null;
//        Sz0 sz02 = (i & 4) != 0 ? new Sz0(null, null, Ay0.a, null, null, 27) : sz0;
//        this.a = wm0;
//        this.b = is0;
//        this.c = sz02;
    }

    @DexIgnore
    public static /* synthetic */ Eu0 a(Eu0 eu0, Wm0 wm0, Is0 is0, Sz0 sz0, int i) {
        if ((i & 1) != 0) {
            wm0 = eu0.a;
        }
        if ((i & 2) != 0) {
            is0 = eu0.b;
        }
        if ((i & 4) != 0) {
            sz0 = eu0.c;
        }
        return eu0.a(wm0, is0, sz0);
    }

    @DexIgnore
    public final Eu0 a(Wm0 wm0, Is0 is0, Sz0 sz0) {
        return new Eu0(wm0, is0, sz0);
    }

    @DexIgnore
    @Override // com.fossil.K60
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        throw null;
//        try {
//            Yz0.a(Yz0.a(jSONObject, R51.g4, Yz0.a(this.a)), R51.M0, Yz0.a(this.b));
//            if (this.c.c != Ay0.a) {
//                Yz0.a(jSONObject, R51.h4, this.c.a());
//            }
//        } catch (JSONException e) {
//            Wl0.h.a(e);
//        }
//        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Eu0) {
                Eu0 eu0 = (Eu0) obj;
                if (!Wg6.a(this.a, eu0.a) || !Wg6.a(this.b, eu0.b) || !Wg6.a(this.c, eu0.c)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Wm0 wm0 = this.a;
        int hashCode = wm0 != null ? wm0.hashCode() : 0;
        Is0 is0 = this.b;
        int hashCode2 = is0 != null ? is0.hashCode() : 0;
        Sz0 sz0 = this.c;
        if (sz0 != null) {
            i = sz0.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    @Override // com.fossil.K60
    public String toString() {
        StringBuilder b2 = Yh0.b("Result(phaseId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", requestResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }
}
