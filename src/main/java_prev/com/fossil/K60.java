package com.fossil;

import com.mapped.Wg6;
import java.io.Serializable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class K60 implements Serializable {
    @DexIgnore
    public static /* synthetic */ String a(K60 k60, int i, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 1) != 0) {
                i = 2;
            }
            return k60.a(i);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: toJSONString");
    }

    @DexIgnore
    public final String a(int i) {
        if (i == 0) {
            String jSONObject = a().toString();
            Wg6.a((Object) jSONObject, "toJSONObject().toString()");
            return jSONObject;
        }
        throw null;
//        String jSONObject2 = a().toString(i);
//        Wg6.a((Object) jSONObject2, "toJSONObject().toString(indentSpace)");
//        return jSONObject2;
    }

    @DexIgnore
    public abstract JSONObject a();

    @DexIgnore
    public String toString() {
        return a(0);
    }
}
