package com.portfolio.platform;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.FacebookSdk;
import com.facebook.appevents.codeless.CodelessMatcher;
import com.facebook.applinks.FacebookAppLinkResolver;
import com.facebook.places.model.PlaceFields;
import com.facebook.stetho.Stetho;
import com.fossil.Aw6;
import com.fossil.Ay6;
import com.fossil.Be;
import com.fossil.Bj5;
import com.fossil.Dh0;
import com.fossil.Dl7;
import com.fossil.E07;
import com.fossil.E8;
import com.fossil.Ea7;
import com.fossil.Eb5;
import com.fossil.Ef;
import com.fossil.Ff;
import com.fossil.Fv7;
import com.fossil.Gf5;
import com.fossil.Gl4;
import com.fossil.Hl4;
import com.fossil.If5;
import com.fossil.Jf5;
import com.fossil.Jl4;
import com.fossil.Jm5;
import com.fossil.Km5;
import com.fossil.L07;
import com.fossil.Lm5;
import com.fossil.Ng5;
import com.fossil.Ol4;
import com.fossil.Qc5;
import com.fossil.Qe;
import com.fossil.Qe5;
import com.fossil.Qj7;
import com.fossil.Ri5;
import com.fossil.Rl4;
import com.fossil.Rm;
import com.fossil.T97;
import com.fossil.Ti7;
import com.fossil.Uw6;
import com.fossil.Vc5;
import com.fossil.Vh7;
import com.fossil.We7;
import com.fossil.Wj4;
import com.fossil.X24;
import com.fossil.Xd5;
import com.fossil.Xh7;
import com.fossil.Xi5;
import com.fossil.Ya5;
import com.fossil.Ye5;
import com.fossil.Zb7;
import com.fossil.Ze5;
import com.fossil.Zi7;
import com.fossil.Zl;
import com.fossil.Zs1;
import com.fossil.Zz6;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.TimeUtils;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.Hf6;
import com.mapped.Hg6;
import com.mapped.Hh6;
import com.mapped.Iface;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jm4;
import com.mapped.Lf6;
import com.mapped.Mj6;
import com.mapped.Nc6;
import com.mapped.PermissionUtils;
import com.mapped.Qd6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rd6;
import com.mapped.Rh4;
import com.mapped.Rm6;
import com.mapped.ServiceUtils;
import com.mapped.U04;
import com.mapped.W40;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.mapped.Xj6;
import com.mapped.Yj6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.model.workout.WorkoutConfigData;
import com.misfit.frameworks.buttonservice.model.workoutdetection.WorkoutDetectionSetting;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.buddy_challenge.analytic.BCAnalytic;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppDataRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.FitnessHelper;
import com.portfolio.platform.localization.LocalizationManager;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.news.notifications.NotificationReceiver;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.workout.WorkoutTetherGpsManager;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.usecase.GeneratePassphraseUseCase;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.FlutterEngineCache;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.view.FlutterMain;
import java.io.File;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.TimeZone;
import net.sqlcipher.database.SQLiteDatabase;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexAppend;
import lanchon.dexpatcher.annotation.DexReplace;
import retrofit.Endpoints;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioApp extends Ff implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String a0;
    @DexIgnore
    public static boolean b0;
    @DexIgnore
    public static PortfolioApp c0;
    @DexIgnore
    public static boolean d0;
    @DexIgnore
    public static E07 e0;
    @DexIgnore
    public static IButtonConnectivity f0;
    @DexIgnore
    public static /* final */ inner get; // = new inner(null);
    @DexIgnore
    public UserRepository A;
    @DexIgnore
    public WatchFaceRepository B;
    @DexIgnore
    public QuickResponseRepository C;
    @DexIgnore
    public WorkoutSettingRepository D;
    @DexIgnore
    public WatchAppDataRepository E;
    @DexIgnore
    public WorkoutTetherGpsManager F;
    @DexIgnore
    public MutableLiveData<String> G; // = new MutableLiveData<>();
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I; // = true;
    @DexIgnore
    public /* final */ Handler J; // = new Handler();
    @DexIgnore
    public Runnable K;
    @DexIgnore
    public Iface L;
    @DexIgnore
    public Thread.UncaughtExceptionHandler M;
    @DexIgnore
    public boolean N;
    @DexIgnore
    public ServerError O;
    @DexIgnore
    public If5 P;
    @DexIgnore
    public Jf5 Q;
    @DexIgnore
    public NetworkChangedReceiver R;
    @DexIgnore
    public /* final */ Xi5 S; // = new Xi5();
    @DexIgnore
    public Ri5 T;
    @DexIgnore
    public SmsMmsReceiver U;
    @DexIgnore
    public ThemeRepository V;
    @DexIgnore
    public Aw6 W;
    @DexIgnore
    public GeneratePassphraseUseCase X;
    @DexIgnore
    public BCAnalytic Y;
    @DexIgnore
    public /* final */ Il6 Z; // = Zi7.a(Dl7.a(null, 1, null).plus(Qj7.b()));
    @DexIgnore
    public LocalizationManager a;
    @DexIgnore
    public AppPackageInstallReceiver b;
    @DexIgnore
    public An4 c;
    @DexIgnore
    public SummariesRepository d;
    @DexIgnore
    public SleepSummariesRepository e;
    @DexIgnore
    public AlarmHelper f;
    @DexIgnore
    public GuestApiService g;
    @DexIgnore
    public U04 h;
    @DexIgnore
    public ApiServiceV2 i;
    @DexIgnore
    public Uw6 j;
    @DexIgnore
    public Rl4 p;
    @DexIgnore
    public DeleteLogoutUserUseCase q;
    @DexIgnore
    public AnalyticsHelper r;
    @DexIgnore
    public ApplicationEventListener s;
    @DexIgnore
    public DeviceRepository t;
    @DexIgnore
    public FitnessHelper u;
    @DexIgnore
    public ShakeFeedbackService v;
    @DexIgnore
    public DianaPresetRepository w;
    @DexIgnore
    public Zz6 x;
    @DexIgnore
    public WatchLocalizationRepository y;
    @DexIgnore
    public DataValidationManager z;

    @DexIgnore
    public enum ActivityState {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexAdd
    public static Thread inject_key_for;

    @DexAdd
    public PackageManager getPackageManager() {
        if ((inject_key_for != null) && (inject_key_for == Thread.currentThread())) {
            return new PackageManagerWrapper(super.getPackageManager());
        } else {
            return super.getPackageManager();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$configureWorkManager$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class c extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (Il6) obj;
            throw null; // return cVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((c) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                Zl.Ai ai = new Zl.Ai();
                ai.a(this.this$0.o());
                Zl a = ai.a();
                Wg6.a((Object) a, "Configuration.Builder()\n\u2026\n                .build()");
                Rm.a(this.this$0, a);
                PushPendingDataWorker.G.a();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$deleteInstanceId$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class d extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public d(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            d dVar = new d(xe6);
            dVar.p$ = (Il6) obj;
            throw null; // return dVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                try {
                    FirebaseInstanceId.p().b();
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String d = PortfolioApp.get.d();
                    local.d(d, "deleteInstanceId ex-" + e);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {326}, m = "downloadWatchAppData")
    public static final class e extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", f = "PortfolioApp.kt", l = {FailureCode.FAILED_TO_START_STREAMING}, m = "invokeSuspend")
    public static final class f extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(PortfolioApp portfolioApp, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, this.$serial, xe6);
            fVar.p$ = (Il6) obj;
            throw null; // return fVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((f) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object watchLocalizationFromServer;
            IButtonConnectivity b;
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                throw null;
//                WatchLocalizationRepository t = this.this$0.t();
//                this.L$0 = il6;
//                this.label = 1;
//                watchLocalizationFromServer = t.getWatchLocalizationFromServer(true, this);
//                if (watchLocalizationFromServer == a) {
//                    return a;
//                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
                watchLocalizationFromServer = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) watchLocalizationFromServer;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.get.d();
            local.d(d, "path of localization file: " + str);
            if (!(str == null || (b = PortfolioApp.get.b()) == null)) {
                throw null; // b.setLocalizationData(new LocalizationData(str, null, 2, null), this.$serial);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements CoroutineUseCase.Ei<DeleteLogoutUserUseCase.Di, DeleteLogoutUserUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public g(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.Ci ci) {
            Wg6.b(ci, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout unsuccessfully");
            this.a.b((ServerError) null);
            this.a.c(false);
        }

        @SuppressLint("WrongConstant")
        @DexIgnore
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.Di di) {
            Wg6.b(di, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.a, WelcomeActivity.class);
            intent.addFlags(268468224);
            this.a.startActivity(intent);
            this.a.b((ServerError) null);
            this.a.c(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$forceReconnectDevice$2", f = "PortfolioApp.kt", l = {FailureCode.USER_KILL_APP_FROM_TASK_MANAGER}, m = "invokeSuspend")
    public static final class h extends Zb7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PortfolioApp portfolioApp, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, this.$serial, xe6);
            hVar.p$ = (Il6) obj;
            throw null; // return hVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
            throw null; // return ((h) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            Object d;
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.get.d();
                local.d(d2, "Inside " + PortfolioApp.get.d() + ".forceReconnectDevice");
                b = PortfolioApp.get.b();
                if (b != null) {
                    str = this.$serial;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.label = 1;
                    d = portfolioApp.d(this);
                    if (d == a) {
                        return a;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                String str2 = (String) this.L$2;
                b = (IButtonConnectivity) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                try {
                    Nc6.a(obj);
                    str = str2;
                    d = obj;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.get.d();
                    local2.e(d3, "Error inside " + PortfolioApp.get.d() + ".deviceReconnect - e=" + e);
                    return Cd6.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            throw null; // return Hf6.a(b.deviceForceReconnect(str, (UserProfile) d));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {367, 379, 380, 381, 382, 419, 420}, m = "getCurrentUserProfile")
    public static final class i extends Jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public float F$0;
        @DexIgnore
        public float F$1;
        @DexIgnore
        public float F$2;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public int I$2;
        @DexIgnore
        public int I$3;
        @DexIgnore
        public int I$4;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class inner {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.PortfolioApp$Companion", f = "PortfolioApp.kt", l = {2448}, m = "updateButtonService")
        public static final class a extends Jf6 {
            @DexIgnore
            public char C$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public int label;
            @DexIgnore
            public /* synthetic */ Object result;
            @DexIgnore
            public /* final */ /* synthetic */ inner this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(inner inner, Xe6 xe6) {
                super(xe6);
                this.this$0 = inner;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                this.result = obj;
                this.label |= RecyclerView.UNDEFINED_DURATION;
                return this.this$0.a(null, this);
            }
        }

        @DexIgnore
        public inner() {
        }

        @DexIgnore
        public /* synthetic */ inner(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final E07 a() {
            return PortfolioApp.e0;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0070  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
        public final Object a(IButtonConnectivity iButtonConnectivity, Xe6<? super Cd6> xe6) {
            a aVar;
            int i;
            CloudLogConfig cloudLogConfig = null;
            String str;
            String s = null;
            String str2 = null;
            String str3 = null;
            char prefix = 0;
            IButtonConnectivity iButtonConnectivity2 = null;
            String i2;
            if (xe6 instanceof a) {
                a aVar2 = (a) xe6;
                int i3 = aVar2.label;
                if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                    aVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                    aVar = aVar2;
                    Object obj = aVar.result;
                    Object a2 = Ff6.a();
                    i = aVar.label;
                    if (i != 0) {
                        Nc6.a(obj);
                        a(iButtonConnectivity);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String d = d();
                        local.d(d, "updateButtonService " + iButtonConnectivity);
                        try {
                            Access a3 = Ng5.a().a(instance());
                            String h = Ol4.A.h();
                            String e = Ol4.A.e();
                            String str4 = (a3 == null || (i2 = a3.getI()) == null) ? "" : i2;
                            if (a3 == null || (str = a3.getK()) == null) {
                                str = "";
                            }
                            cloudLogConfig = new CloudLogConfig(h, e, str4, str);
                            IButtonConnectivity b = b();
                            if (b != null) {
                                s = Ol4.A.s();
                                if (a3 == null || (str2 = a3.getL()) == null) {
                                    str2 = "";
                                }
                                if (a3 == null || (str3 = a3.getM()) == null) {
                                    str3 = "";
                                }
                                prefix = (Wg6.a((Object)"release", (Object)"release") ? instance().k() : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN).getPrefix();
                                AppHelper b2 = AppHelper.g.b();
                                aVar.L$0 = this;
                                aVar.L$1 = iButtonConnectivity;
                                aVar.L$2 = a3;
                                aVar.L$3 = cloudLogConfig;
                                aVar.L$4 = b;
                                aVar.L$5 = s;
                                aVar.L$6 = str2;
                                aVar.L$7 = str3;
                                aVar.C$0 = (char) prefix;
                                aVar.label = 1;
                                obj = b2.a(aVar);
                                if (obj == a2) {
                                    return a2;
                                }
                                iButtonConnectivity2 = b;
                            } else {
                                Wg6.a();
                                throw null;
                            }
                        } catch (Exception e2) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String d2 = this.d();
                            local2.e(d2, ".updateButtonService(), ex=" + e2);
                            return Cd6.a;
                        }
                    } else if (i == 1) {
                        char c = aVar.C$0;
                        str3 = (String) aVar.L$7;
                        str2 = (String) aVar.L$6;
                        s = (String) aVar.L$5;
                        iButtonConnectivity2 = (IButtonConnectivity) aVar.L$4;
                        CloudLogConfig cloudLogConfig2 = (CloudLogConfig) aVar.L$3;
                        Access access = (Access) aVar.L$2;
                        IButtonConnectivity iButtonConnectivity3 = (IButtonConnectivity) aVar.L$1;
                        inner inner = (inner) aVar.L$0;
                        try {
                            Nc6.a(obj);
                            prefix = c;
                            cloudLogConfig = cloudLogConfig2;
//                            this = inner;
                        } catch (Exception e3) {
//                            this = inner;
                            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                            String d22 = this.d();
                            local22.e(d22, ".updateButtonService(), ex=" + e3);
                            return Cd6.a;
                        }
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    try {
                        iButtonConnectivity2.init(s, str2, str3, prefix, (AppLogInfo) obj, AppHelper.g.b().a(), cloudLogConfig);
                    } catch (RemoteException ex) {
                        ex.printStackTrace();
                    }
                    return Cd6.a;
                }
            }
            aVar = new a(this, xe6);
            Object obj2 = aVar.result;
            Object a22 = Ff6.a();
            i = aVar.label;
            if (i != 0) {
            }
            try {
                iButtonConnectivity2.init(s, str2, str3, prefix, (AppLogInfo) obj2, AppHelper.g.b().a(), cloudLogConfig);
            } catch (Exception e4) {
                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                String d222 = this.d();
                local222.e(d222, ".updateButtonService(), ex=" + e4);
                return Cd6.a;
            }
            return Cd6.a;
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.f0 = iButtonConnectivity;
        }

        @DexIgnore
        public final void a(MFDeviceService.b bVar) {
            PortfolioApp.a(bVar);
        }

        @DexIgnore
        public final void a(Object obj) {
            Wg6.b(obj, Constants.EVENT);
            E07 a2 = a();
            if (a2 != null) {
                a2.a(obj);
            } else {
                Wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final void a(boolean z) {
            PortfolioApp.d(z);
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.f0;
        }

        @DexIgnore
        public final void b(MFDeviceService.b bVar) {
            Wg6.b(bVar, Constants.SERVICE);
            a(bVar);
        }

        @DexIgnore
        public final void b(Object obj) {
            Wg6.b(obj, "o");
            try {
                E07 a2 = a();
                if (a2 != null) {
                    a2.b(obj);
                } else {
                    Wg6.a();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        public final void c(Object obj) {
            Wg6.b(obj, "o");
            try {
                E07 a2 = a();
                if (a2 != null) {
                    a2.c(obj);
                } else {
                    Wg6.a();
                    throw null;
                }
            } catch (Exception e) {
            }
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.a0;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.d0;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.b0;
        }

        @DexIgnore
        public final PortfolioApp instance() {
            PortfolioApp U = PortfolioApp.c0;
            if (U != null) {
                return U;
            }
            Wg6.d("instance");
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {547, 554}, m = "getPassphrase")
    public static final class j extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.e(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {345}, m = "getUserRegisterDate")
    public static final class k extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null; // return this.this$0.g(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {438}, m = "getWorkoutDetectionSetting")
    public static final class l extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$2", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class m extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            m mVar = new m(this.this$0, xe6);
            mVar.p$ = (Il6) obj;
            throw null; // return mVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((m) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.get.d();
                local.d(d, "activeDevice=" + this.this$0.c() + ", was stealed remove it!!!");
                if (DeviceHelper.o.f(this.this$0.c())) {
                    this.this$0.y().deleteWatchFacesWithSerial(this.this$0.c());
                }
                PortfolioApp portfolioApp = this.this$0;
                portfolioApp.r(portfolioApp.c());
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public n(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.C() || !this.a.I) {
                PortfolioApp.get.a(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "still foreground");
                return;
            }
            this.a.b(false);
            PortfolioApp.get.a(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "went background");
            this.a.H();
            If5 a2 = this.a.P;
            if (a2 != null) {
                a2.a("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$onCreate$1", f = "PortfolioApp.kt", l = {596, 597, 598, 604, 619, 630}, m = "invokeSuspend")
    public static final class o extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            o oVar = new o(this.this$0, xe6);
            oVar.p$ = (Il6) obj;
            throw null; // return oVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((o) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0194  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01af  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01c6  */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x01ed  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x01f7  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x0206  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00a3  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00f2  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x0194  */
        /* JADX WARNING: Removed duplicated region for block: B:59:0x01af  */
        /* JADX WARNING: Removed duplicated region for block: B:63:0x01c6  */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x01ed  */
        /* JADX WARNING: Removed duplicated region for block: B:73:0x01f7  */
        /* JADX WARNING: Removed duplicated region for block: B:77:0x0206  */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Il6 il6;
            String str3 = null;
            String str5 = null;
            String str8 = null;
            PortfolioApp portfolioApp;
            ThemeRepository r;
            Il6 il62;
            CloudLogConfig cloudLogConfig;
            FLogger fLogger;
            Object a;
            String str;
            Access access;
            Il6 il63;
            String i;
            String l;
            Object currentUser;
            Il6 il64;
            Access access2;
            String str2;
            PortfolioApp portfolioApp2;
            String g;
            Object a2 = Ff6.a();
            switch (this.label) {
                case 0:
                    Nc6.a(obj);
                    il6 = this.p$;
                    PortfolioApp portfolioApp3 = this.this$0;
                    this.L$0 = il6;
                    this.label = 1;
                    if (portfolioApp3.a(this) == a2) {
                        return a2;
                    }
                    portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.label = 2;
                    if (portfolioApp.i(this) == a2) {
                        return a2;
                    }
                    r = this.this$0.r();
                    this.L$0 = il6;
                    this.label = 3;
                    if (r.initializeLocalTheme(this) == a2) {
                        return a2;
                    }
                    il62 = il6;
                    Access a3 = Ng5.a().a(this.this$0);
                    Zs1 zs1 = Zs1.b;
                    String s = Ol4.A.s();
                    str3 = (a3 != null || (l = a3.getL()) == null) ? "" : l;
                    String str4;
                    if (a3 == null || (str4 = a3.getM()) == null) {
                        str4 = "";
                    }
                    zs1.a(new Dh0(s, str3, str4));
                    String h = Ol4.A.h();
                    String e = Ol4.A.e();
                    str5 = (a3 != null || (i = a3.getI()) == null) ? "" : i;
                    String str6;
                    if (a3 == null || (str6 = a3.getK()) == null) {
                        str6 = "";
                    }
                    cloudLogConfig = new CloudLogConfig(h, e, str5, str6);
                    fLogger = FLogger.INSTANCE;
                    AppHelper b = AppHelper.g.b();
                    this.L$0 = il62;
                    this.L$1 = a3;
                    this.L$2 = cloudLogConfig;
                    this.L$3 = fLogger;
                    this.L$4 = "App";
                    this.label = 4;
                    a = b.a(this);
                    if (a != a2) {
                        return a2;
                    }
                    str = "App";
                    access = a3;
                    il63 = il62;
                    fLogger.init(str, (AppLogInfo) a, AppHelper.g.b().a(), cloudLogConfig, this.this$0, true, "App");
                    FLogger.INSTANCE.getRemote().flush();
                    String str7;
                    if (access == null || (str7 = access.getO()) == null) {
                        str7 = "0000000000000";
                    }
                    FacebookSdk.setApplicationId(str7);
                    if (!FacebookSdk.isInitialized()) {
                        FacebookSdk.sdkInitialize(this.this$0);
                    }
                    if (!Places.isInitialized()) {
                        PortfolioApp portfolioApp4 = this.this$0;
                        if (access == null || (str2 = access.getN()) == null) {
                            str2 = "0000000000000";
                        }
                        Places.initialize(portfolioApp4, str2);
                    }
                    UserRepository s2 = this.this$0.s();
                    this.L$0 = il63;
                    this.L$1 = access;
                    this.L$2 = cloudLogConfig;
                    this.label = 5;
                    currentUser = s2.getCurrentUser(this);
                    if (currentUser != a2) {
                        return a2;
                    }
                    il64 = il63;
                    access2 = access;
                    MFUser mFUser = (MFUser) currentUser;
                    this.this$0.a(mFUser);
                    AppHelper.g.d();
                    ZendeskConfig zendeskConfig = ZendeskConfig.INSTANCE;
                    PortfolioApp portfolioApp5 = this.this$0;
                    String y = Ol4.A.y();
                    str8 = (access2 != null || (g = access2.getG()) == null) ? "" : g;
                    String str9;
                    if (access2 == null || (str9 = access2.getH()) == null) {
                        str9 = "";
                    }
                    zendeskConfig.init(portfolioApp5, y, str8, str9);
                    portfolioApp2 = this.this$0;
                    this.L$0 = il64;
                    this.L$1 = access2;
                    this.L$2 = cloudLogConfig;
                    this.L$3 = mFUser;
                    this.label = 6;
                    if (portfolioApp2.k(this) == a2) {
                        return a2;
                    }
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    String d = PortfolioApp.get.d();
                    remote.i(component, session, "", d, "[App] onAppCreated at " + System.currentTimeMillis());
                    return Cd6.a;
                case 1:
                    il6 = (Il6) this.L$0;
                    Nc6.a(obj);
                    portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.label = 2;
                    if (portfolioApp.i(this) == a2) {
                    }
                    r = this.this$0.r();
                    this.L$0 = il6;
                    this.label = 3;
                    if (r.initializeLocalTheme(this) == a2) {
                    }
                    break;
                case 2:
                    il6 = (Il6) this.L$0;
                    Nc6.a(obj);
                    r = this.this$0.r();
                    this.L$0 = il6;
                    this.label = 3;
                    if (r.initializeLocalTheme(this) == a2) {
                    }
                    break;
                case 3:
                    Nc6.a(obj);
                    il62 = (Il6) this.L$0;
                    Access a32 = Ng5.a().a(this.this$0);
                    Zs1 zs12 = Zs1.b;
                    String s3 = Ol4.A.s();
                    if (a32 != null) {
                        break;
                    }
                    String str42 = "";
                    zs12.a(new Dh0(s3, str3, str42));
                    String h2 = Ol4.A.h();
                    String e2 = Ol4.A.e();
                    if (a32 != null) {
                        break;
                    }
                    String str62 = "";
                    cloudLogConfig = new CloudLogConfig(h2, e2, str5, str62);
                    fLogger = FLogger.INSTANCE;
                    AppHelper b2 = AppHelper.g.b();
                    this.L$0 = il62;
                    this.L$1 = a32;
                    this.L$2 = cloudLogConfig;
                    this.L$3 = fLogger;
                    this.L$4 = "App";
                    this.label = 4;
                    a = b2.a(this);
                    if (a != a2) {
                    }
                    break;
                case 4:
                    str = (String) this.L$4;
                    fLogger = (FLogger) this.L$3;
                    Nc6.a(obj);
                    access = (Access) this.L$1;
                    il63 = (Il6) this.L$0;
                    cloudLogConfig = (CloudLogConfig) this.L$2;
                    a = obj;
                    fLogger.init(str, (AppLogInfo) a, AppHelper.g.b().a(), cloudLogConfig, this.this$0, true, "App");
                    FLogger.INSTANCE.getRemote().flush();
                    String str72 = "0000000000000";
                    FacebookSdk.setApplicationId(str72);
                    if (!FacebookSdk.isInitialized()) {
                    }
                    if (!Places.isInitialized()) {
                    }
                    UserRepository s22 = this.this$0.s();
                    this.L$0 = il63;
                    this.L$1 = access;
                    this.L$2 = cloudLogConfig;
                    this.label = 5;
                    currentUser = s22.getCurrentUser(this);
                    if (currentUser != a2) {
                    }
                    break;
                case 5:
                    access2 = (Access) this.L$1;
                    il64 = (Il6) this.L$0;
                    Nc6.a(obj);
                    currentUser = obj;
                    cloudLogConfig = (CloudLogConfig) this.L$2;
                    MFUser mFUser2 = (MFUser) currentUser;
                    this.this$0.a(mFUser2);
                    AppHelper.g.d();
                    ZendeskConfig zendeskConfig2 = ZendeskConfig.INSTANCE;
                    PortfolioApp portfolioApp52 = this.this$0;
                    String y2 = Ol4.A.y();
                    if (access2 != null) {
                        break;
                    }
                    String str92 = "";
                    zendeskConfig2.init(portfolioApp52, y2, str8, str92);
                    portfolioApp2 = this.this$0;
                    this.L$0 = il64;
                    this.L$1 = access2;
                    this.L$2 = cloudLogConfig;
                    this.L$3 = mFUser2;
                    this.label = 6;
                    if (portfolioApp2.k(this) == a2) {
                    }
                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component2 = FLogger.Component.APP;
                    FLogger.Session session2 = FLogger.Session.OTHER;
                    String d2 = PortfolioApp.get.d();
                    remote2.i(component2, session2, "", d2, "[App] onAppCreated at " + System.currentTimeMillis());
                    return Cd6.a;
                case 6:
                    MFUser mFUser3 = (MFUser) this.L$3;
                    CloudLogConfig cloudLogConfig2 = (CloudLogConfig) this.L$2;
                    Access access3 = (Access) this.L$1;
                    Il6 il65 = (Il6) this.L$0;
                    Nc6.a(obj);
                    IRemoteFLogger remote22 = FLogger.INSTANCE.getRemote();
                    FLogger.Component component22 = FLogger.Component.APP;
                    FLogger.Session session22 = FLogger.Session.OTHER;
                    String d22 = PortfolioApp.get.d();
                    remote22.i(component22, session22, "", d22, "[App] onAppCreated at " + System.currentTimeMillis());
                    return Cd6.a;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class p implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public p(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "uncaughtException - ex=" + th);
            th.printStackTrace();
            Wg6.a((Object) th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        Wg6.a((Object) stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        Wg6.a((Object) className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        Wg6.a((Object) simpleName, "ButtonService::class.java.simpleName");
//                        if (Yj6.a((CharSequence) className, (CharSequence) simpleName, false, 2, (Object) null)) {
//                            FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "uncaughtException - stopLogService");
//                            this.a.b((int) FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
//                            break;
//                        }
                        throw null;
//                        i++;
                    } else {
                        Wg6.a();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            throw null;
//            this.a.v().c(currentTimeMillis);
//            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "Inside .uncaughtException - currentTime = " + currentTimeMillis);
//            Thread.UncaughtExceptionHandler b = this.a.M;
//            if (b != null) {
//                b.uncaughtException(thread, th);
//            } else {
//                Wg6.a();
//                throw null;
//            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$pairDevice$2", f = "PortfolioApp.kt", l = {1795}, m = "invokeSuspend")
    public static final class q extends Zb7 implements Coroutine<Il6, Xe6<? super Object>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $macAddress;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(PortfolioApp portfolioApp, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$serial = str;
            this.$macAddress = str2;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            q qVar = new q(this.this$0, this.$serial, this.$macAddress, xe6);
            qVar.p$ = (Il6) obj;
            throw null;
//            return qVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Object> xe6) {
            throw null; // return ((q) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            String str;
            String str2;
            Object d;
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d2 = PortfolioApp.get.d();
                local.d(d2, "Inside " + PortfolioApp.get.d() + ".pairDevice");
                b = PortfolioApp.get.b();
                if (b != null) {
                    str = this.$serial;
                    str2 = this.$macAddress;
                    PortfolioApp portfolioApp = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = b;
                    this.L$2 = str;
                    this.L$3 = str2;
                    this.label = 1;
//                    d = portfolioApp.d(this);
//                    if (d == a) {
//                        return a;
                    throw null;
//                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                String str3 = (String) this.L$3;
                str = (String) this.L$2;
                b = (IButtonConnectivity) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                try {
                    Nc6.a(obj);
                    d = obj;
                    str2 = str3;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d3 = PortfolioApp.get.d();
                    local2.e(d3, "Error inside " + PortfolioApp.get.d() + ".pairDevice - e=" + e);
                    return Cd6.a;
                }
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            throw null;
//            return Hf6.a(b.pairDevice(str, str2, (UserProfile) d));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {782}, m = "registerContactObserver")
    public static final class r extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.k(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2", f = "PortfolioApp.kt", l = {1128, 1171}, m = "invokeSuspend")
    public static final class s extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1", f = "PortfolioApp.kt", l = {1172}, m = "invokeSuspend")
        public static final class a extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Installation $installation;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ s this$0;

            @DexIgnore
//            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.PortfolioApp$s$a$a")
            @Lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$1$response$1", f = "PortfolioApp.kt", l = {1172}, m = "invokeSuspend")
            /* renamed from: com.portfolio.platform.PortfolioApp$s$a$a  reason: collision with other inner class name */
            public static final class C0043a extends Zb7 implements Hg6<Xe6<? super Fv7<Installation>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0043a(a aVar, Xe6 xe6) {
                    super(1, xe6);
                    this.this$0 = aVar;
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Xe6<Cd6> create(Xe6<?> xe6) {
                    Wg6.b(xe6, "completion");
                    throw null;
//                    return new C0043a(this.this$0, xe6);
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.mapped.Hg6
                public final Object invoke(Xe6<? super Fv7<Installation>> xe6) {
                    throw null;
//                    return ((C0043a) create(xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Ob7
                public final Object invokeSuspend(Object obj) {
                    Object a = Ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        Nc6.a(obj);
                        ApiServiceV2 n = this.this$0.this$0.this$0.n();
                        Installation installation = this.this$0.$installation;
                        this.label = 1;
                        Object insertInstallation = n.insertInstallation(installation, this);
                        return insertInstallation == a ? a : insertInstallation;
                    } else if (i == 1) {
                        Nc6.a(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(s sVar, Installation installation, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = sVar;
                this.$installation = installation;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$installation, xe6);
                aVar.p$ = (Il6) obj;
                throw null;
//                return aVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null; // return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Object a;
                Object a2 = Ff6.a();
                int i = this.label;
                if (i == 0) {
                    Nc6.a(obj);
                    Il6 il6 = this.p$;
                    C0043a aVar = new C0043a(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    a = ResponseKt.a(aVar, this);
                    if (a == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    Nc6.a(obj);
                    a = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Ap4 ap4 = (Ap4) a;
                if (ap4 instanceof Bj5) {
                    Bj5 bj5 = (Bj5) ap4;
                    if (bj5.a() != null) {
                        throw null;
//                        this.this$0.this$0.v().p(((Installation) bj5.a()).getInstallationId());
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public s(PortfolioApp portfolioApp, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            s sVar = new s(this.this$0, xe6);
            sVar.p$ = (Il6) obj;
            throw null;
//            return sVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((s) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
        /* JADX WARNING: Removed duplicated region for block: B:11:0x003c  */
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object currentUser;
            Il6 il6;
            MFUser mFUser;
            Object a2 = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il62 = this.p$;
                throw null;
//                UserRepository s = this.this$0.s();
//                this.L$0 = il62;
//                this.label = 1;
//                currentUser = s.getCurrentUser(this);
//                if (currentUser == a2) {
//                    return a2;
//                }
//                il6 = il62;
//                mFUser = (MFUser) currentUser;
//                if (mFUser != null) {
//                }
            } else if (i == 1) {
                Nc6.a(obj);
                il6 = (Il6) this.L$0;
                currentUser = obj;
                mFUser = (MFUser) currentUser;
                if (mFUser != null) {
                    Installation installation = new Installation();
                    installation.setAppMarketingVersion("4.5.0");
                    installation.setAppBuildNumber(26467);
                    installation.setModel(Build.MODEL);
                    installation.setOsVersion(Build.VERSION.RELEASE);
                    installation.setInstallationId(AppHelper.g.a(mFUser.getUserId()));
                    installation.setAppPermissions(PermissionUtils.a.a());
                    installation.setDeviceToken(this.this$0.v().j());
                    TimeZone timeZone = TimeZone.getDefault();
                    Wg6.a((Object) timeZone, "TimeZone.getDefault()");
                    String id = timeZone.getID();
                    Wg6.a((Object) id, "zone");
//                    if (Yj6.a((CharSequence) id, '/', 0, false, 6, (Object) null) > 0) {
                    if (Yj6.a((CharSequence) id, '/', 0, false) > 0) {
                        installation.setTimeZone(id);
                    }
                    try {
                        String packageName = this.this$0.getPackageName();
                        PackageManager packageManager = this.this$0.getPackageManager();
                        String str = packageManager.getPackageInfo(packageName, 0).versionName;
                        String obj2 = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
                        if (!TextUtils.isEmpty(obj2)) {
                            installation.setAppName(obj2);
                        }
                        if (!TextUtils.isEmpty(str)) {
                            installation.setAppVersion(str);
                        }
                    } catch (Exception e) {
                        FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "Cannot load package info; will not be saved to installation");
                    }
                    installation.setDeviceType("android");
                    this.this$0.a(installation);
                    Ti7 b = Qj7.b();
                    a aVar = new a(this, installation, null);
                    this.L$0 = il6;
                    this.L$1 = mFUser;
                    this.L$2 = installation;
                    this.L$3 = id;
                    this.label = 2;
                    if (Vh7.a(b, aVar, this) == a2) {
                        return a2;
                    }
                }
            } else if (i == 2) {
                String str2 = (String) this.L$3;
                Installation installation2 = (Installation) this.L$2;
                MFUser mFUser2 = (MFUser) this.L$1;
                Il6 il63 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2149}, m = "setImplicitDeviceConfig")
    public static final class t extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public t(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1041}, m = "switchActiveDevice")
    public static final class u extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public u(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.d((String) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class v extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public v(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            v vVar = new v(xe6);
            vVar.p$ = (Il6) obj;
            throw null;
//            return vVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null; // return ((v) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                ActiveDeviceInfo a = AppHelper.g.b().a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.get.d();
                local.d(d, ".updateActiveDeviceInfoLog(), " + new Gson().a(a));
                FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
                try {
                    IButtonConnectivity b = PortfolioApp.get.b();
                    if (b != null) {
                        b.updateActiveDeviceInfoLog(a);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d2 = PortfolioApp.get.d();
                    local2.e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {2396}, m = "updateAppLogInfo")
    public static final class w extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public w(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.m(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {766}, m = "updateCrashlyticsUserInformation")
    public static final class x extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public x(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.n(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1375, 1378}, m = "updatePercentageGoalProgress")
    public static final class y extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public boolean Z$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public y(PortfolioApp portfolioApp, Xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            throw null;
//            return this.this$0.a(false, (Xe6<? super Cd6>) this);
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        Wg6.a((Object) simpleName, "PortfolioApp::class.java.simpleName");
        a0 = simpleName;
    }
    */

    @DexIgnore
    public static final IButtonConnectivity Y() {
        return f0;
    }

    @DexIgnore
    public static final /* synthetic */ void a(MFDeviceService.b bVar) {
    }

    @DexIgnore
    public static final /* synthetic */ void d(boolean z2) {
    }

    @DexIgnore
    public final boolean A() {
        WorkoutTetherGpsManager workoutTetherGpsManager = this.F;
        if (workoutTetherGpsManager != null) {
            return workoutTetherGpsManager.c();
        }
        Wg6.d("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final void B() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(c());
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean C() {
        return this.H;
    }

    @DexIgnore
    public final boolean D() {
        Boolean bool;
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            int[] listActiveCommunicator = iButtonConnectivity != null ? iButtonConnectivity.getListActiveCommunicator() : null;
            List<Integer> e2 = listActiveCommunicator != null ? T97.e(listActiveCommunicator) : null;
            if (e2 != null) {
                bool = Boolean.valueOf(!e2.isEmpty());
            } else {
                bool = null;
            }
            if (bool != null) {
                return bool.booleanValue() && e2.contains(Integer.valueOf(CommunicateMode.OTA.getValue()));
            }
            Wg6.a();
            throw null;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final boolean E() {
        return Wg6.a((Object) getPackageName(), (Object) q());
    }

    @DexIgnore
    public final boolean F() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + "/" + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(a0, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(a0, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        Wg6.a((Object) string, "enabledNotificationListeners");
        throw null; // return Yj6.a(string, str, false, 2, null);
    }

    @DexIgnore
    public final boolean G() {
        return Xj6.b("release", "release", true);
    }

    @DexIgnore
    public final void H() {
        If5 c2 = AnalyticsHelper.f.c("ota_session");
        If5 c3 = AnalyticsHelper.f.c("sync_session");
        If5 c4 = AnalyticsHelper.f.c("setup_device_session");
        if (c2 != null && c2.b()) {
            c2.a(AnalyticsHelper.f.a("ota_session_go_to_background"));
        } else if (c3 != null && c3.b()) {
            c3.a(AnalyticsHelper.f.a("sync_session_go_to_background"));
        } else if (c4 != null && c4.b()) {
            c4.a(AnalyticsHelper.f.a("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void I() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long J() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(c());
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void K() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme(FacebookAppLinkResolver.APP_LINK_TARGET_PACKAGE_KEY);
        registerReceiver(this.b, intentFilter);
    }

    @DexIgnore
    public final void L() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        SmsMmsReceiver smsMmsReceiver = this.U;
        if (smsMmsReceiver != null) {
            registerReceiver(smsMmsReceiver, intentFilter);
        } else {
            Wg6.d("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    public final void M() {
        try {
            Ri5 ri5 = this.T;
            if (ri5 != null) {
                registerReceiver(ri5, new IntentFilter("android.intent.action.PHONE_STATE"));
                L();
                return;
            }
            Wg6.d("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final void N() {
        AlarmHelper alarmHelper = this.f;
        if (alarmHelper != null) {
            alarmHelper.a(this);
            AlarmHelper alarmHelper2 = this.f;
            if (alarmHelper2 != null) {
                alarmHelper2.d(this);
            } else {
                Wg6.d("mAlarmHelper");
                throw null;
            }
        } else {
            Wg6.d("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void O() {
        String c2 = c();
        try {
            FLogger.INSTANCE.getLocal().d(a0, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(c2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "Error inside " + a0 + ".resetDeviceSettingToDefault - e=" + e2);
        }
    }

    @DexIgnore
    public final void P() {
        FLogger.INSTANCE.getLocal().d(a0, "unregisterContactObserver");
        try {
            Uw6 uw6 = this.j;
            if (uw6 != null) {
                uw6.c();
                ContentResolver contentResolver = getContentResolver();
                Uw6 uw62 = this.j;
                if (uw62 != null) {
                    contentResolver.unregisterContentObserver(uw62);
                    An4 an4 = this.c;
                    if (an4 != null) {
                        an4.n(false);
                    } else {
                        Wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    Wg6.d("mContactObserver");
                    throw null;
                }
            } else {
                Wg6.d("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    public final void Q() {
        try {
            Ri5 ri5 = this.T;
            if (ri5 != null) {
                unregisterReceiver(ri5);
                SmsMmsReceiver smsMmsReceiver = this.U;
                if (smsMmsReceiver != null) {
                    unregisterReceiver(smsMmsReceiver);
                } else {
                    Wg6.d("mMessageReceiver");
                    throw null;
                }
            } else {
                Wg6.d("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final Rm6 R() {
        throw null; // return Xh7.b(Zi7.a(Qj7.b()), null, null, new v(null), 3, null);
    }

    @DexIgnore
    public final long a(ReplyMessageMappingGroup replyMessageMappingGroup, String str) {
        Wg6.b(replyMessageMappingGroup, "replyMessageGroup");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setReplyMessageMapping - replyMessageGroup=" + replyMessageMappingGroup);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setReplyMessageMappingSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setPresetApps - watchAppMappingSettings=" + watchAppMappingSettings + ", complicationAppsSettings=" + complicationAppMappingSettings + ", backgroundConfig=" + backgroundConfig);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, int i4, boolean z2) {
        Wg6.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateActivityGoals(str, i2, i3, i4, z2);
                }
                Wg6.a();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", caloriesGoal=" + i3 + ", activeTimeGoal=" + i4 + ", goalRingEnabled=" + z2);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local3.e(str4, "Error Inside " + a0 + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, Location location) {
        Wg6.b(str, "serial");
        Wg6.b(location, PlaceFields.LOCATION);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutGPSDataSession(str, location);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".setWorkoutGPSDataSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, InactiveNudgeData inactiveNudgeData) {
        Wg6.b(str, "serial");
        Wg6.b(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, CustomRequest customRequest) {
        Wg6.b(str, "serial");
        Wg6.b(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        throw null; //
    }

    @DexIgnore
    public final long a(String str, NotificationBaseObj notificationBaseObj) {
        Wg6.b(str, "serial");
        Wg6.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "notifyNotificationControlEvent - notification=" + notificationBaseObj);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.notifyNotificationEvent(notificationBaseObj, str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".notifyNotificationControlEvent(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, WorkoutDetectionSetting workoutDetectionSetting) {
        Wg6.b(str, "serial");
        Wg6.b(workoutDetectionSetting, "workoutDetectionSetting");
        FLogger.INSTANCE.getLocal().d(a0, "setWorkoutDetectionSetting()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setWorkoutDetectionSetting(workoutDetectionSetting, str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "setWorkoutDetectionSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, List<? extends Alarm> list) {
        Wg6.b(str, "serial");
        Wg6.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, (List<Alarm>)list);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(Xe6<? super Cd6> xe6) {
        Object a2 = Vh7.a(Qj7.b(), new c(this, null), xe6);
        return a2 == Ff6.a() ? a2 : Cd6.a;
    }

    @DexIgnore
    public final Object a(String str, Xe6<? super Boolean> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "downloadWatchAppData(), uiVersion = " + str);
        WatchAppDataRepository watchAppDataRepository = this.E;
        if (watchAppDataRepository != null) {
            return watchAppDataRepository.downloadWatchAppData(g(), str, xe6);
        }
        Wg6.d("mWatchAppDataRepository");
        throw null;
    }

    @DexIgnore
    public final Object a(String str, String str2, Xe6<Object> xe6) {
        return Vh7.a(Qj7.b(), new q(this, str, str2, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    public final Object a(boolean z2, Xe6<? super Cd6> xe6) throws RemoteException {
        y yVar;
        int i2;
        IButtonConnectivity iButtonConnectivity = null;
        Object d2 = null;
        String str = null;
        IButtonConnectivity iButtonConnectivity2;
        Object d3;
        String str2;
        boolean z3;
        boolean z4 = true;
        if (xe6 instanceof y) {
            y yVar2 = (y) xe6;
            int i3 = yVar2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                yVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                yVar = yVar2;
                Object obj = yVar.result;
                Object a2 = Ff6.a();
                i2 = yVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    String c2 = c();
                    if (!z2) {
                        iButtonConnectivity2 = f0;
                        if (iButtonConnectivity2 != null) {
                            yVar.L$0 = this;
                            yVar.Z$0 = z2;
                            yVar.L$1 = c2;
                            yVar.L$2 = iButtonConnectivity2;
                            yVar.L$3 = c2;
                            yVar.Z$1 = false;
                            yVar.label = 1;
                            d3 = d(yVar);
                            if (d3 == a2) {
                                return a2;
                            }
                            str2 = c2;
                            z3 = false;
                            try {
                                iButtonConnectivity2.updatePercentageGoalProgress(str2, z3, (UserProfile) d3);
                            } catch (RemoteException ex) {
                                ex.printStackTrace();
                            }
                            return Cd6.a;
                        }
                        Wg6.a();
                        throw null;
                    }
                    iButtonConnectivity = f0;
                    if (iButtonConnectivity != null) {
                        yVar.L$0 = this;
                        yVar.Z$0 = z2;
                        yVar.L$1 = c2;
                        yVar.L$2 = iButtonConnectivity;
                        yVar.L$3 = c2;
                        yVar.Z$1 = true;
                        yVar.label = 2;
                        d2 = d(yVar);
                        if (d2 == a2) {
                            return a2;
                        }
                        str = c2;
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else if (i2 == 1) {
                    z3 = yVar.Z$1;
                    String str3 = (String) yVar.L$1;
                    boolean z5 = yVar.Z$0;
                    PortfolioApp portfolioApp = (PortfolioApp) yVar.L$0;
                    Nc6.a(obj);
                    d3 = obj;
                    str2 = (String) yVar.L$3;
                    iButtonConnectivity2 = (IButtonConnectivity) yVar.L$2;
                    iButtonConnectivity2.updatePercentageGoalProgress(str2, z3, (UserProfile) d3);
                    return Cd6.a;
                } else if (i2 == 2) {
                    z4 = yVar.Z$1;
                    String str4 = (String) yVar.L$3;
                    IButtonConnectivity iButtonConnectivity3 = (IButtonConnectivity) yVar.L$2;
                    String str5 = (String) yVar.L$1;
                    boolean z6 = yVar.Z$0;
                    PortfolioApp portfolioApp2 = (PortfolioApp) yVar.L$0;
                    try {
                        Nc6.a(obj);
                        str = str4;
                        iButtonConnectivity = iButtonConnectivity3;
                        d2 = obj;
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                iButtonConnectivity.updatePercentageGoalProgress(str, z4, (UserProfile) d2);
                return Cd6.a;
            }
        }
        yVar = new y(this, xe6);
        Object obj2 = yVar.result;
        Object a22 = Ff6.a();
        i2 = yVar.label;
        if (i2 != 0) {
        }
        iButtonConnectivity.updatePercentageGoalProgress(str, z4, (UserProfile) d2);
        return Cd6.a;
    }

    @DexIgnore
    public final String a(Uri uri) {
        if (Wg6.a((Object) uri.getScheme(), (Object) "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        Wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a() {
        An4 an4 = this.c;
        if (an4 != null) {
            an4.a(53);
            File cacheDir = getCacheDir();
            Wg6.a((Object) cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                String[] list = file.list();
                for (String str : list) {
                    if (!Wg6.a((Object) str, (Object) "lib")) {
                        a(new File(file, str));
                    }
                }
            }
            @SuppressLint("WrongConstant") PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), 268435456);
            Object systemService = getSystemService(Context.ALARM_SERVICE);
            if (systemService != null) {
                ((AlarmManager) systemService).set(AlarmManager.RTC, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Context.ACTIVITY_SERVICE);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new Rc6("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            Wg6.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final void a(int i2) {
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        String c2 = c();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(c2, min, min2, min3, min4);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "Error inside " + a0 + ".simulateDisconnection - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(Jf5 jf5) {
        If5 if5;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Active view tracer old: " + this.Q + " \n new: " + jf5);
        if (jf5 != null && Wg6.a((Object) jf5.a(), (Object) "view_appearance")) {
            Jf5 jf52 = this.Q;
            if (!(jf52 == null || jf52 == null || jf52.a(jf5) || (if5 = this.P) == null)) {
                Gf5 a2 = AnalyticsHelper.f.a("app_appearance_view_navigate");
                Jf5 jf53 = this.Q;
                if (jf53 != null) {
                    String e2 = jf53.e();
                    if (e2 != null) {
                        a2.a("prev_view", e2);
                        String e3 = jf5.e();
                        if (e3 != null) {
                            a2.a("next_view", e3);
                            if5.a(a2);
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } else {
                        Wg6.a();
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            }
            this.Q = jf5;
        }
    }

    @DexIgnore
    public final void a(Cj4 cj4, boolean z2, int i2) {
        Wg6.b(cj4, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        Qe5.c.b();
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", c());
        Qe.a(this).a(intent);
        String c2 = c();
        if (g(c2)) {
            FLogger.INSTANCE.getLocal().d(a0, "Device is syncing, Skip this sync.");
        } else {
            cj4.b(c2).a(new Km5(i2, c2, z2), (CoroutineUseCase.Ei<? super Lm5, ? super Jm5>) null);
        }
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        Wg6.b(communicateMode, "curCommunicateMode");
        Wg6.b(str, "curSerial");
        Wg6.b(communicateMode2, "newCommunicateMode");
        Wg6.b(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, String str2) {
        Wg6.b(communicateMode, "communicateMode");
        Wg6.b(str, "serial");
        Wg6.b(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(UserDisplayUnit userDisplayUnit, String str) {
        Wg6.b(userDisplayUnit, "userDisplayUnit");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserProfile userProfile) {
        Wg6.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(BackgroundConfig backgroundConfig, String str) {
        Wg6.b(backgroundConfig, "backgroundConfig");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoBackgroundImageConfig - backgroundConfig=" + backgroundConfig);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoBackgroundImageConfig(backgroundConfig, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(DeviceAppResponse deviceAppResponse, String str) {
        Wg6.b(deviceAppResponse, "deviceAppResponse");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        Wg6.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setComplicationApps - complicationAppsSettings=" + complicationAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoComplicationAppSettings(complicationAppMappingSettings, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        Wg6.b(appNotificationFilterSettings, "notificationFilterSettings");
        Wg6.b(str, "serial");
        Iterator<AppNotificationFilter> it = appNotificationFilterSettings.getNotificationFilters().iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "setAutoNotificationFilterSettings " + ((Object) it.next()));
        }
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(WatchAppMappingSettings watchAppMappingSettings, String str) {
        Wg6.b(watchAppMappingSettings, "watchAppMappingSettings");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(MusicResponse musicResponse, String str) {
        Wg6.b(musicResponse, "musicAppResponse");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(WorkoutConfigData workoutConfigData, String str) {
        Wg6.b(workoutConfigData, "workoutConfigData");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setWorkoutConfig - workoutConfigData=" + workoutConfigData);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setWorkoutConfig(workoutConfigData, str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(Installation installation) {
        installation.setLocaleIdentifier(l());
    }

    @DexIgnore
    public final void a(MFUser mFUser) {
        if (mFUser == null) {
            AnalyticsHelper.f.c().a(false);
            return;
        }
        AnalyticsHelper.f.c().b(mFUser.getUserId());
        AnalyticsHelper.f.c().a(mFUser.getDiagnosticEnabled());
    }

    @DexIgnore
    public final void a(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(a0, "forceLogout");
        if (this.H) {
            DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.q;
            if (deleteLogoutUserUseCase != null) {
                deleteLogoutUserUseCase.a(new DeleteLogoutUserUseCase.Bi(2, null), new g(this));
            } else {
                Wg6.d("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.O = serverError;
            this.N = true;
        }
    }

    @DexIgnore
    public final void a(String str) {
        Wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                Cd6 cd6 = Cd6.a;
                return;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            Cd6 cd62 = Cd6.a;
        }
    }

    @DexIgnore
    public final void a(String str, FirmwareData firmwareData, UserProfile userProfile) {
        Wg6.b(str, "serial");
        Wg6.b(firmwareData, "firmwareData");
        Wg6.b(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, DeviceAppResponse deviceAppResponse) {
        Wg6.b(str, "serial");
        Wg6.b(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "Error inside " + a0 + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, VibrationStrengthObj vibrationStrengthObj) {
        Wg6.b(str, "serial");
        Wg6.b(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "Inside " + a0 + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "Error inside " + a0 + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        throw null;
//        String str3;
//        String str4 = "localization_" + str + '.';
//        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
//        int length = listFiles.length;
//        int i2 = 0;
//        while (true) {
//            if (i2 >= length) {
//                str3 = "";
//                break;
//            }
//            File file = listFiles[i2];
//            Wg6.a((Object) file, "file");
//            String path = file.getPath();
//            Wg6.a((Object) path, "file.path");
//            if (Yj6.a((CharSequence) path, (CharSequence) str4, false, 2, (Object) null)) {
//                str3 = file.getPath();
//                Wg6.a((Object) str3, "file.path");
//                break;
//            }
//            i2++;
//        }
//        FLogger.INSTANCE.getLocal().d(a0, "setLocalization - neededLocalizationFilePath: " + str3);
//        if (TextUtils.isEmpty(str3)) {
//            Rm6 unused = Xh7.b(Zi7.a(Qj7.b()), null, null, new f(this, str2, null), 3, null);
//            return;
//        }
//        IButtonConnectivity iButtonConnectivity = f0;
//        if (iButtonConnectivity != null) {
//            iButtonConnectivity.setLocalizationData(new LocalizationData(str3, null, 2, null), str2);
//        }
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        Wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void a(List<? extends Alarm> list) {
        Wg6.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm((List<Alarm>)list);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.e(str, "confirmBcStatusToButtonService - bcStatus: " + z2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmBCStatus(z2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0057, code lost:
        if (com.mapped.Yj6.a((java.lang.CharSequence) r0, (java.lang.CharSequence) "image", false, 2, (java.lang.Object) null) == false) goto L_0x0059;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0067, code lost:
        if (com.mapped.Yj6.a((java.lang.CharSequence) r2, (java.lang.CharSequence) "image", false, 2, (java.lang.Object) null) != false) goto L_0x0069;
     */
    @DexIgnore
    public final boolean a(Intent intent, Uri uri) {
        Wg6.b(uri, "fileUri");
        String a2 = a(uri);
        String type = intent != null ? intent.getType() : "";
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "isImageFile intentMimeType=" + type + ", uriMimeType=" + a2);
        String path = uri.getPath();
        if (path != null) {
            Wg6.a((Object) path, "fileUri.path!!");
            throw null;
            //if (!Yj6.a((CharSequence) path, (CharSequence) "pickerImage", false, 2, (Object) null)) {
//                if (!TextUtils.isEmpty(type)) {
//                    if (type == null) {
//                        Wg6.a();
//                        throw null;
//                    }
//                }
//                if (!TextUtils.isEmpty(a2)) {
//                    if (a2 == null) {
//                        Wg6.a();
//                        throw null;
//                    }
//                }
//                return false;
//            }
//            return true;
        }
        Wg6.a();
        throw null;
    }

    @DexIgnore
    public final boolean a(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = a(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final boolean a(String str, UserProfile userProfile) {
        Wg6.b(str, "serial");
        Wg6.b(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = a0;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = a0;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    @DexIgnore
    public final boolean a(String str, PairingResponse pairingResponse) {
        Wg6.b(str, "serial");
        Wg6.b(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z2, int i2) {
        Wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ff
    public void attachBaseContext(Context context) {
        Wg6.b(context, "base");
        super.attachBaseContext(context);
        throw null;
//        Ef.d(this);
    }

    @DexIgnore
    public final long b(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        Wg6.b(appNotificationFilterSettings, "notificationFilterSettings");
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final Object b(Xe6<? super Cd6> xe6) {
        Object a2 = Vh7.a(Qj7.b(), new d(null), xe6);
        return a2 == Ff6.a() ? a2 : Cd6.a;
    }

    @DexIgnore
    public final Object b(String str, Xe6<Object> xe6) {
        return Vh7.a(Qj7.b(), new h(this, str, null), xe6);
    }

    @DexIgnore
    public final String b() {
        String str;
        String a2;
        String c2 = c();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + c2);
        if (TextUtils.isEmpty(c2)) {
            PortfolioApp portfolioApp = c0;
            if (portfolioApp != null) {
                String a3 = Jm4.a(portfolioApp, 2131887005);
                Wg6.a((Object) a3, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return a3;
            }
            Wg6.d("instance");
            throw null;
        }
        String a4 = Jm4.a(this, 2131887120);
        DeviceRepository deviceRepository = this.t;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(c2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = a0;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(c2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = f0;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(c2) == 2) {
                            PortfolioApp portfolioApp2 = c0;
                            if (portfolioApp2 == null) {
                                Wg6.d("instance");
                                throw null;
                            } else if (portfolioApp2.g(c2)) {
                                PortfolioApp portfolioApp3 = c0;
                                if (portfolioApp3 != null) {
                                    str = Jm4.a(portfolioApp3, 2131886733);
                                } else {
                                    Wg6.d("instance");
                                    throw null;
                                }
                            } else {
                                An4 an4 = this.c;
                                if (an4 != null) {
                                    long e2 = an4.e(c2);
                                    if (e2 == 0) {
                                        a2 = "";
                                    } else if (System.currentTimeMillis() - e2 < 60000) {
                                        We7 we7 = We7.a;
                                        PortfolioApp portfolioApp4 = c0;
                                        if (portfolioApp4 != null) {
                                            String a5 = Jm4.a(portfolioApp4, 2131887136);
                                            Wg6.a((Object) a5, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                                            a2 = String.format(a5, Arrays.copyOf(new Object[]{1}, 1));
                                            Wg6.a((Object) a2, "java.lang.String.format(format, *args)");
                                        } else {
                                            Wg6.d("instance");
                                            throw null;
                                        }
                                    } else {
                                        a2 = TimeUtils.a(e2);
                                    }
                                    We7 we72 = We7.a;
                                    PortfolioApp portfolioApp5 = c0;
                                    if (portfolioApp5 != null) {
                                        String a6 = Jm4.a(portfolioApp5, 2131887126);
                                        Wg6.a((Object) a6, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
                                        str = String.format(a6, Arrays.copyOf(new Object[]{a2}, 1));
                                        Wg6.a((Object) str, "java.lang.String.format(format, *args)");
                                    } else {
                                        Wg6.d("instance");
                                        throw null;
                                    }
                                } else {
                                    Wg6.d("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        } else {
                            str = a4;
                        }
                        return deviceNameBySerial + " : " + str;
                    }
                    Wg6.a();
                    throw null;
                }
                Wg6.a();
                throw null;
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = a0;
                local3.d(str4, "generateDeviceNotificationContent e=" + e3);
                str = a4;
            }
        } else {
            Wg6.d("mDeviceRepository");
            throw null;
        }
        throw null;
        //
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    public final void b(ServerError serverError) {
        this.O = serverError;
    }

    @DexIgnore
    public final void b(String str) {
        Wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final void b(String str, NotificationBaseObj notificationBaseObj) {
        Wg6.b(str, "serial");
        Wg6.b(notificationBaseObj, "notification");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "sendNotificationToDevice packageName " + notificationBaseObj);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    public final void b(String str, List<? extends BLEMapping> list) {
        Wg6.b(str, "serial");
        Wg6.b(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, (List<BLEMapping>)list);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void b(String str, boolean z2) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "notifyWatchAppDataReady(), serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.notifyWatchAppFilesReady(str, z2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "notifyWatchAppDataReady() - e=" + e2);
        }
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        Wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2, int i2) {
        Wg6.b(str, "serial");
        Wg6.b(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final long c(String str, List<? extends MicroAppMapping> list) {
        Wg6.b(str, "serial");
        Wg6.b(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    String[] declarationFiles = microAppMapping.getDeclarationFiles();
                    for (String str2 : declarationFiles) {
                        sb.append("||");
                        sb.append(str2);
                    }
                    FLogger.INSTANCE.getLocal().d(a0, sb.toString());
                    FLogger.INSTANCE.getLocal().d(a0, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping((List<MicroAppMapping>)list));
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final Object c(Xe6<? super Cd6> xe6) {
        e eVar;
        int i2;
        Object downloadWatchAppData;
        if (xe6 instanceof e) {
            e eVar2 = (e) xe6;
            int i3 = eVar2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                eVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                eVar = eVar2;
                Object obj = eVar.result;
                Object a2 = Ff6.a();
                i2 = eVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(a0, "downloadWatchAppData()");
                    String x2 = x();
                    if (x2 != null) {
                        WatchAppDataRepository watchAppDataRepository = this.E;
                        if (watchAppDataRepository != null) {
                            String g2 = g();
                            eVar.L$0 = this;
                            eVar.L$1 = x2;
                            eVar.label = 1;
                            downloadWatchAppData = watchAppDataRepository.downloadWatchAppData(g2, x2, eVar);
                            if (downloadWatchAppData == a2) {
                                return a2;
                            }
                        } else {
                            Wg6.d("mWatchAppDataRepository");
                            throw null;
                        }
                    }
                    return Cd6.a;
                } else if (i2 == 1) {
                    String str = (String) eVar.L$1;
                    PortfolioApp portfolioApp = (PortfolioApp) eVar.L$0;
                    Nc6.a(obj);
                    downloadWatchAppData = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Hf6.a(((Boolean) downloadWatchAppData).booleanValue());
                return Cd6.a;
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = Ff6.a();
        i2 = eVar.label;
        if (i2 != 0) {
        }
        throw null;
        //Hf6.a(((Boolean) downloadWatchAppData).booleanValue());
        //return Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final Object c(String str, Xe6<? super Cd6> xe6) {
        t tVar;
        int i2;
        Object d2;
        UserProfile userProfile;
        if (xe6 instanceof t) {
            t tVar2 = (t) xe6;
            int i3 = tVar2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                tVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                tVar = tVar2;
                Object obj = tVar.result;
                Object a2 = Ff6.a();
                i2 = tVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    tVar.L$0 = this;
                    tVar.L$1 = str;
                    tVar.label = 1;
                    d2 = d(tVar);
                    if (d2 == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    PortfolioApp portfolioApp = (PortfolioApp) tVar.L$0;
                    Nc6.a(obj);
                    d2 = obj;
                    str = (String) tVar.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                userProfile = (UserProfile) d2;
                if (userProfile == null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = a0;
                    local.d(str2, "setImplicitDeviceConfig - currentUserProfile=" + userProfile);
                    try {
                        IButtonConnectivity iButtonConnectivity = f0;
                        if (iButtonConnectivity != null) {
                            iButtonConnectivity.setImplicitDeviceConfig(userProfile, str);
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = a0;
                        local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
                        e2.printStackTrace();
                    }
                } else {
                    FLogger.INSTANCE.getLocal().e(a0, "setImplicitDeviceConfig - currentUserProfile is NULL");
                }
                return Cd6.a;
            }
        }
        tVar = new t(this, xe6);
        Object obj2 = tVar.result;
        Object a22 = Ff6.a();
        i2 = tVar.label;
        if (i2 != 0) {
        }
        throw null;
//        userProfile = (UserProfile) d2;
//        if (userProfile == null) {
//        }
//        return Cd6.a;
    }

    @DexIgnore
    public final String c() {
        An4 an4 = this.c;
        if (an4 != null) {
            String e2 = an4.e();
            return e2 != null ? e2 : "";
        }
        Wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void c(String str, String str2) {
        Wg6.b(str, "serial");
        if (TextUtils.isEmpty(str) || DeviceHelper.o.e(str)) {
            String c2 = c();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, "Inside " + a0 + ".setActiveDeviceSerial - current=" + c2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            An4 an4 = this.c;
            if (an4 != null) {
                an4.o(str);
                if (c2 != str) {
                    throw null;
//                    this.G.a(str);
                }
                R();
                try {
                    IButtonConnectivity iButtonConnectivity = f0;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        FossilNotificationBar.c.a(this);
                        return;
                    }
                    Wg6.a();
                    throw null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                Wg6.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    public final void c(String str, boolean z2) {
        Wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".onUpdateSecretKeyToServerResponse(), isSuccess=" + z2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendPushSecretKeyResponse(str, z2);
                Cd6 cd6 = Cd6.a;
                return;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".onUpdateSecretKeyToServerResponse() - e=" + e2);
            Cd6 cd62 = Cd6.a;
        }
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.N = z2;
    }

    @DexIgnore
    public final boolean c(String str) {
        Wg6.b(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                i(str);
                return forceSwitchDeviceWithoutErase;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final long d(String str, boolean z2) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final MutableLiveData<String> d() {
//        An4 an4 = this.c;
//        if (an4 != null) {
//            String e2 = an4.e();
//            if (!Wg6.a((Object) e2, (Object) this.G.a())) {
//                this.G.a(e2);
//            }
//            return this.G;
//        }
//        Wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x05b4  */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x05da  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x05ec  */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x05f3  */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x05fa  */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0601  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x060f  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x061c A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0243  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0258  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x02a6  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x02ac  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0301  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0305  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x033b  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x036e  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x039d  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x03da  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0414  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0448  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x0458  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0510  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x0561  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x0569  */
    public final Object d(Xe6<? super UserProfile> xe6) {
//        i iVar;
//        Object currentUser;
//        MFUser mFUser;
//        List a2;
//        Object[] array;
//        Object currentActivitySettings;
//        float f2;
//        float f3;
//        MFUser mFUser2;
//        String[] strArr;
//        int i2;
//        String str;
//        int component1;
//        int component2;
//        int component3;
//        SummariesRepository summariesRepository;
//        Object summary;
//        float f4;
//        float f5;
//        int i3;
//        PortfolioApp portfolioApp;
//        SleepSummariesRepository sleepSummariesRepository;
//        Object sleepSummaryFromDb;
//        int i4;
//        String[] strArr2;
//        String str2;
//        PortfolioApp portfolioApp2;
//        ActivitySummary activitySummary;
//        FitnessHelper fitnessHelper;
//        Object a3;
//        String[] strArr3;
//        float f6;
//        PortfolioApp portfolioApp3;
//        String str3;
//        MFSleepDay mFSleepDay;
//        Hh6 hh6;
//        double d2;
//        double d3;
//        An4 an4;
//        UserBiometricData.BiometricWearingPosition biometricWearingPosition;
//        Object f7;
//        String str4;
//        MFSleepDay mFSleepDay2;
//        String[] strArr4;
//        int i5;
//        float f8;
//        float f9;
//        int i6;
//        int i7;
//        int i8;
//        MFUser mFUser3;
//        float f10;
//        Hh6 hh62;
//        PortfolioApp portfolioApp4;
//        Hh6 hh63;
//        UserBiometricData.BiometricGender biometricGender;
//        UserBiometricData.BiometricWearingPosition biometricWearingPosition2;
//        Hh6 hh64;
//        int i9;
//        Object h2;
//        int i10;
//        int i11;
//        float f11;
//        float f12;
//        int i12;
//        UserBiometricData.BiometricGender biometricGender2;
//        UserBiometricData.BiometricWearingPosition biometricWearingPosition3;
//        ReplyMessageMappingGroup replyMessageMappingGroup;
//        if (xe6 instanceof i) {
//            i iVar2 = (i) xe6;
//            int i13 = iVar2.label;
//            if ((Integer.MIN_VALUE & i13) != 0) {
//                iVar2.label = i13 + RecyclerView.UNDEFINED_DURATION;
//                iVar = iVar2;
//                Object obj = iVar.result;
//                Object a4 = Ff6.a();
//                switch (iVar.label) {
//                    case 0:
//                        Nc6.a(obj);
//                        UserRepository userRepository = this.A;
//                        if (userRepository != null) {
//                            iVar.L$0 = this;
//                            iVar.label = 1;
//                            currentUser = userRepository.getCurrentUser(iVar);
//                            if (currentUser == a4) {
//                                return a4;
//                            }
//                            mFUser = (MFUser) currentUser;
//                            if (mFUser != null) {
//                                return null;
//                            }
//                            String birthday = mFUser.getBirthday();
//                            if (Xj6.a((CharSequence) birthday)) {
//                                birthday = "1970-01-01";
//                            }
//                            if (birthday != null) {
//                                List<String> split = new Mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(birthday, 0);
//                                if (!split.isEmpty()) {
//                                    ListIterator<String> listIterator = split.listIterator(split.size());
//                                    while (true) {
//                                        if (listIterator.hasPrevious()) {
//                                            if (!Hf6.a(listIterator.previous().length() == 0).booleanValue()) {
//                                                a2 = Ea7.d(split, listIterator.nextIndex() + 1);
//                                            }
//                                        }
//                                    }
//                                    array = a2.toArray(new String[0]);
//                                    if (array == null) {
//                                        String[] strArr5 = (String[]) array;
//                                        int i14 = Calendar.getInstance().get(1);
//                                        Integer valueOf = Integer.valueOf(strArr5[0]);
//                                        Wg6.a((Object) valueOf, "Integer.valueOf(s[0])");
//                                        int intValue = i14 - valueOf.intValue();
//                                        float e2 = Xd5.e((float) (mFUser.getWeightInGrams() > 0 ? mFUser.getWeightInGrams() : 68039));
//                                        float d4 = Xd5.d((float) (mFUser.getHeightInCentimeters() > 0 ? mFUser.getHeightInCentimeters() : 170));
//                                        SummariesRepository summariesRepository2 = this.d;
//                                        if (summariesRepository2 != null) {
//                                            iVar.L$0 = this;
//                                            iVar.L$1 = mFUser;
//                                            iVar.I$0 = intValue;
//                                            iVar.L$2 = birthday;
//                                            iVar.L$3 = strArr5;
//                                            iVar.F$0 = e2;
//                                            iVar.F$1 = d4;
//                                            iVar.label = 2;
//                                            currentActivitySettings = summariesRepository2.getCurrentActivitySettings(iVar);
//                                            if (currentActivitySettings == a4) {
//                                                return a4;
//                                            }
//                                            f2 = e2;
//                                            f3 = d4;
//                                            mFUser2 = mFUser;
//                                            strArr = strArr5;
//                                            i2 = intValue;
//                                            str = birthday;
//                                            ActivitySettings activitySettings = (ActivitySettings) currentActivitySettings;
//                                            component1 = activitySettings.component1();
//                                            component2 = activitySettings.component2();
//                                            component3 = activitySettings.component3();
//                                            summariesRepository = this.d;
//                                            if (summariesRepository == null) {
//                                                Calendar instance = Calendar.getInstance();
//                                                Wg6.a((Object) instance, "Calendar.getInstance()");
//                                                iVar.L$0 = this;
//                                                iVar.L$1 = mFUser2;
//                                                iVar.I$0 = i2;
//                                                iVar.L$2 = str;
//                                                iVar.L$3 = strArr;
//                                                iVar.F$0 = f2;
//                                                iVar.F$1 = f3;
//                                                iVar.I$1 = component1;
//                                                iVar.I$2 = component2;
//                                                iVar.I$3 = component3;
//                                                iVar.label = 3;
//                                                summary = summariesRepository.getSummary(instance, iVar);
//                                                if (summary == a4) {
//                                                    return a4;
//                                                }
//                                                f4 = f2;
//                                                f5 = f3;
//                                                i3 = i2;
//                                                portfolioApp = this;
//                                                ActivitySummary activitySummary2 = (ActivitySummary) summary;
//                                                sleepSummariesRepository = portfolioApp.e;
//                                                if (sleepSummariesRepository == null) {
//                                                    Calendar instance2 = Calendar.getInstance();
//                                                    Wg6.a((Object) instance2, "Calendar.getInstance()");
//                                                    Date time = instance2.getTime();
//                                                    Wg6.a((Object) time, "Calendar.getInstance().time");
//                                                    iVar.L$0 = portfolioApp;
//                                                    iVar.L$1 = mFUser2;
//                                                    iVar.I$0 = i3;
//                                                    iVar.L$2 = str;
//                                                    iVar.L$3 = strArr;
//                                                    iVar.F$0 = f4;
//                                                    iVar.F$1 = f5;
//                                                    iVar.I$1 = component1;
//                                                    iVar.I$2 = component2;
//                                                    iVar.I$3 = component3;
//                                                    iVar.L$4 = activitySummary2;
//                                                    iVar.label = 4;
//                                                    sleepSummaryFromDb = sleepSummariesRepository.getSleepSummaryFromDb(time, iVar);
//                                                    if (sleepSummaryFromDb == a4) {
//                                                        return a4;
//                                                    }
//                                                    i4 = component2;
//                                                    strArr2 = strArr;
//                                                    str2 = str;
//                                                    portfolioApp2 = portfolioApp;
//                                                    activitySummary = activitySummary2;
//                                                    MFSleepDay mFSleepDay3 = (MFSleepDay) sleepSummaryFromDb;
//                                                    fitnessHelper = portfolioApp2.u;
//                                                    if (fitnessHelper == null) {
//                                                        Date date = new Date();
//                                                        iVar.L$0 = portfolioApp2;
//                                                        iVar.L$1 = mFUser2;
//                                                        iVar.I$0 = i3;
//                                                        iVar.L$2 = str2;
//                                                        iVar.L$3 = strArr2;
//                                                        iVar.F$0 = f4;
//                                                        iVar.F$1 = f5;
//                                                        iVar.I$1 = component1;
//                                                        iVar.I$2 = i4;
//                                                        iVar.I$3 = component3;
//                                                        iVar.L$4 = activitySummary;
//                                                        iVar.L$5 = mFSleepDay3;
//                                                        iVar.label = 5;
//                                                        a3 = fitnessHelper.a(date, true, iVar);
//                                                        if (a3 == a4) {
//                                                            return a4;
//                                                        }
//                                                        strArr3 = strArr2;
//                                                        f6 = f5;
//                                                        portfolioApp3 = portfolioApp2;
//                                                        str3 = str2;
//                                                        mFSleepDay = mFSleepDay3;
//                                                        float floatValue = ((Number) a3).floatValue();
//                                                        hh6 = new Hh6();
//                                                        int i15 = 0;
//                                                        hh6.element = 0;
//                                                        Hh6 hh65 = new Hh6();
//                                                        hh65.element = 0;
//                                                        Hh6 hh66 = new Hh6();
//                                                        hh66.element = 0;
//                                                        Hh6 hh67 = new Hh6();
//                                                        hh67.element = 0;
//                                                        if (activitySummary == null) {
//                                                            i15 = activitySummary.getActiveTime();
//                                                            d3 = activitySummary.getCalories();
//                                                            d2 = ((double) 100) * activitySummary.getDistance();
//                                                        } else {
//                                                            d2 = 0.0d;
//                                                            d3 = 0.0d;
//                                                        }
//                                                        if (mFSleepDay != null) {
//                                                            hh6.element += mFSleepDay.getSleepMinutes();
//                                                            SleepDistribution sleepStateDistInMinute = mFSleepDay.getSleepStateDistInMinute();
//                                                            if (sleepStateDistInMinute != null) {
//                                                                int component12 = sleepStateDistInMinute.component1();
//                                                                int component22 = sleepStateDistInMinute.component2();
//                                                                int component32 = sleepStateDistInMinute.component3();
//                                                                hh65.element = component12 + hh65.element;
//                                                                hh66.element += component22;
//                                                                hh67.element = component32 + hh67.element;
//                                                                Cd6 cd6 = Cd6.a;
//                                                            }
//                                                        }
//                                                        UserBiometricData.BiometricGender biometricGender3 = UserBiometricData.BiometricGender.UNSPECIFIED;
//                                                        if (!Wg6.a((Object) mFUser2.getGender(), (Object) Rh4.MALE.getValue())) {
//                                                            biometricGender3 = UserBiometricData.BiometricGender.MALE;
//                                                        } else if (Wg6.a((Object) mFUser2.getGender(), (Object) Rh4.FEMALE.getValue())) {
//                                                            biometricGender3 = UserBiometricData.BiometricGender.FEMALE;
//                                                        }
//                                                        an4 = portfolioApp3.c;
//                                                        if (an4 == null) {
//                                                            String n2 = an4.n();
//                                                            if (n2 != null) {
//                                                                int hashCode = n2.hashCode();
//                                                                if (hashCode != -1867896629) {
//                                                                    if (hashCode != 647131558) {
//                                                                        if (hashCode == 660843126 && n2.equals("left wrist")) {
//                                                                            biometricWearingPosition = UserBiometricData.BiometricWearingPosition.LEFT_WRIST;
//                                                                            iVar.L$0 = portfolioApp3;
//                                                                            iVar.L$1 = mFUser2;
//                                                                            iVar.I$0 = i3;
//                                                                            iVar.L$2 = str3;
//                                                                            iVar.L$3 = strArr3;
//                                                                            iVar.F$0 = f4;
//                                                                            iVar.F$1 = f6;
//                                                                            iVar.I$1 = component1;
//                                                                            iVar.I$2 = i4;
//                                                                            iVar.I$3 = component3;
//                                                                            iVar.L$4 = activitySummary;
//                                                                            iVar.L$5 = mFSleepDay;
//                                                                            iVar.F$2 = floatValue;
//                                                                            iVar.I$4 = i15;
//                                                                            iVar.D$0 = d3;
//                                                                            iVar.D$1 = d2;
//                                                                            iVar.L$6 = hh6;
//                                                                            iVar.L$7 = hh65;
//                                                                            iVar.L$8 = hh66;
//                                                                            iVar.L$9 = hh67;
//                                                                            iVar.L$10 = biometricGender3;
//                                                                            iVar.L$11 = biometricWearingPosition;
//                                                                            iVar.label = 6;
//                                                                            f7 = portfolioApp3.f(iVar);
//                                                                            if (f7 != a4) {
//                                                                                return a4;
//                                                                            }
//                                                                            str4 = str3;
//                                                                            mFSleepDay2 = mFSleepDay;
//                                                                            strArr4 = strArr3;
//                                                                            i5 = i15;
//                                                                            f8 = f4;
//                                                                            f9 = f6;
//                                                                            i6 = i3;
//                                                                            i7 = component3;
//                                                                            i8 = i4;
//                                                                            mFUser3 = mFUser2;
//                                                                            f10 = floatValue;
//                                                                            hh62 = hh67;
//                                                                            portfolioApp4 = portfolioApp3;
//                                                                            hh63 = hh66;
//                                                                            biometricGender = biometricGender3;
//                                                                            biometricWearingPosition2 = biometricWearingPosition;
//                                                                            hh64 = hh65;
//                                                                            i9 = component1;
//                                                                            ReplyMessageMappingGroup replyMessageMappingGroup2 = (ReplyMessageMappingGroup) f7;
//                                                                            iVar.L$0 = portfolioApp4;
//                                                                            iVar.L$1 = mFUser3;
//                                                                            iVar.I$0 = i6;
//                                                                            iVar.L$2 = str4;
//                                                                            iVar.L$3 = strArr4;
//                                                                            iVar.F$0 = f8;
//                                                                            iVar.F$1 = f9;
//                                                                            iVar.I$1 = i9;
//                                                                            iVar.I$2 = i8;
//                                                                            iVar.I$3 = i7;
//                                                                            iVar.L$4 = activitySummary;
//                                                                            iVar.L$5 = mFSleepDay2;
//                                                                            iVar.F$2 = f10;
//                                                                            iVar.I$4 = i5;
//                                                                            iVar.D$0 = d3;
//                                                                            iVar.D$1 = d2;
//                                                                            iVar.L$6 = hh6;
//                                                                            iVar.L$7 = hh64;
//                                                                            iVar.L$8 = hh63;
//                                                                            iVar.L$9 = hh62;
//                                                                            iVar.L$10 = biometricGender;
//                                                                            iVar.L$11 = biometricWearingPosition2;
//                                                                            iVar.L$12 = replyMessageMappingGroup2;
//                                                                            iVar.label = 7;
//                                                                            h2 = portfolioApp4.h(iVar);
//                                                                            if (h2 != a4) {
//                                                                                return a4;
//                                                                            }
//                                                                            i10 = i7;
//                                                                            i11 = i6;
//                                                                            f11 = f8;
//                                                                            f12 = f9;
//                                                                            i12 = i5;
//                                                                            biometricGender2 = biometricGender;
//                                                                            biometricWearingPosition3 = biometricWearingPosition2;
//                                                                            replyMessageMappingGroup = replyMessageMappingGroup2;
//                                                                            return new UserProfile(new UserBiometricData(i11, biometricGender2, f12, f11, biometricWearingPosition3), (long) i9, (long) f10, i10, i12, (long) i8, (long) d3, (long) d2, Vc5.a(mFUser3), hh6.element, hh64.element, hh63.element, hh62.element, null, false, -1, -1, System.currentTimeMillis(), replyMessageMappingGroup, (WorkoutDetectionSetting) h2);
//                                                                        }
//                                                                    } else if (n2.equals("unspecified wrist")) {
//                                                                        biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED_WRIST;
//                                                                        iVar.L$0 = portfolioApp3;
//                                                                        iVar.L$1 = mFUser2;
//                                                                        iVar.I$0 = i3;
//                                                                        iVar.L$2 = str3;
//                                                                        iVar.L$3 = strArr3;
//                                                                        iVar.F$0 = f4;
//                                                                        iVar.F$1 = f6;
//                                                                        iVar.I$1 = component1;
//                                                                        iVar.I$2 = i4;
//                                                                        iVar.I$3 = component3;
//                                                                        iVar.L$4 = activitySummary;
//                                                                        iVar.L$5 = mFSleepDay;
//                                                                        iVar.F$2 = floatValue;
//                                                                        iVar.I$4 = i15;
//                                                                        iVar.D$0 = d3;
//                                                                        iVar.D$1 = d2;
//                                                                        iVar.L$6 = hh6;
//                                                                        iVar.L$7 = hh65;
//                                                                        iVar.L$8 = hh66;
//                                                                        iVar.L$9 = hh67;
//                                                                        iVar.L$10 = biometricGender3;
//                                                                        iVar.L$11 = biometricWearingPosition;
//                                                                        iVar.label = 6;
//                                                                        f7 = portfolioApp3.f(iVar);
//                                                                        if (f7 != a4) {
//                                                                        }
//                                                                    }
//                                                                } else if (n2.equals("right wrist")) {
//                                                                    biometricWearingPosition = UserBiometricData.BiometricWearingPosition.RIGHT_WRIST;
//                                                                    iVar.L$0 = portfolioApp3;
//                                                                    iVar.L$1 = mFUser2;
//                                                                    iVar.I$0 = i3;
//                                                                    iVar.L$2 = str3;
//                                                                    iVar.L$3 = strArr3;
//                                                                    iVar.F$0 = f4;
//                                                                    iVar.F$1 = f6;
//                                                                    iVar.I$1 = component1;
//                                                                    iVar.I$2 = i4;
//                                                                    iVar.I$3 = component3;
//                                                                    iVar.L$4 = activitySummary;
//                                                                    iVar.L$5 = mFSleepDay;
//                                                                    iVar.F$2 = floatValue;
//                                                                    iVar.I$4 = i15;
//                                                                    iVar.D$0 = d3;
//                                                                    iVar.D$1 = d2;
//                                                                    iVar.L$6 = hh6;
//                                                                    iVar.L$7 = hh65;
//                                                                    iVar.L$8 = hh66;
//                                                                    iVar.L$9 = hh67;
//                                                                    iVar.L$10 = biometricGender3;
//                                                                    iVar.L$11 = biometricWearingPosition;
//                                                                    iVar.label = 6;
//                                                                    f7 = portfolioApp3.f(iVar);
//                                                                    if (f7 != a4) {
//                                                                    }
//                                                                }
//                                                            }
//                                                            biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED;
//                                                            iVar.L$0 = portfolioApp3;
//                                                            iVar.L$1 = mFUser2;
//                                                            iVar.I$0 = i3;
//                                                            iVar.L$2 = str3;
//                                                            iVar.L$3 = strArr3;
//                                                            iVar.F$0 = f4;
//                                                            iVar.F$1 = f6;
//                                                            iVar.I$1 = component1;
//                                                            iVar.I$2 = i4;
//                                                            iVar.I$3 = component3;
//                                                            iVar.L$4 = activitySummary;
//                                                            iVar.L$5 = mFSleepDay;
//                                                            iVar.F$2 = floatValue;
//                                                            iVar.I$4 = i15;
//                                                            iVar.D$0 = d3;
//                                                            iVar.D$1 = d2;
//                                                            iVar.L$6 = hh6;
//                                                            iVar.L$7 = hh65;
//                                                            iVar.L$8 = hh66;
//                                                            iVar.L$9 = hh67;
//                                                            iVar.L$10 = biometricGender3;
//                                                            iVar.L$11 = biometricWearingPosition;
//                                                            iVar.label = 6;
//                                                            f7 = portfolioApp3.f(iVar);
//                                                            if (f7 != a4) {
//                                                            }
//                                                        } else {
//                                                            Wg6.d("sharedPreferencesManager");
//                                                            throw null;
//                                                        }
//                                                    } else {
//                                                        Wg6.d("mFitnessHelper");
//                                                        throw null;
//                                                    }
//                                                } else {
//                                                    Wg6.d("mSleepSummariesRepository");
//                                                    throw null;
//                                                }
//                                            } else {
//                                                Wg6.d("mSummariesRepository");
//                                                throw null;
//                                            }
//                                        } else {
//                                            Wg6.d("mSummariesRepository");
//                                            throw null;
//                                        }
//                                    } else {
//                                        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
//                                    }
//                                }
//                                a2 = Qd6.a();
//                                array = a2.toArray(new String[0]);
//                                if (array == null) {
//                                }
//                            } else {
//                                Wg6.a();
//                                throw null;
//                            }
//                        } else {
//                            Wg6.d("mUserRepository");
//                            throw null;
//                        }
//                    case 1:
//                        Nc6.a(obj);
//                        currentUser = obj;
//                        this = (PortfolioApp) iVar.L$0;
//                        mFUser = (MFUser) currentUser;
//                        if (mFUser != null) {
//                        }
//                        break;
//                    case 2:
//                        float f13 = iVar.F$1;
//                        f2 = iVar.F$0;
//                        int i16 = iVar.I$0;
//                        Nc6.a(obj);
//                        this = (PortfolioApp) iVar.L$0;
//                        f3 = f13;
//                        currentActivitySettings = obj;
//                        mFUser2 = (MFUser) iVar.L$1;
//                        strArr = (String[]) iVar.L$3;
//                        i2 = i16;
//                        str = (String) iVar.L$2;
//                        ActivitySettings activitySettings2 = (ActivitySettings) currentActivitySettings;
//                        component1 = activitySettings2.component1();
//                        component2 = activitySettings2.component2();
//                        component3 = activitySettings2.component3();
//                        summariesRepository = this.d;
//                        if (summariesRepository == null) {
//                        }
//                        break;
//                    case 3:
//                        component3 = iVar.I$3;
//                        component2 = iVar.I$2;
//                        component1 = iVar.I$1;
//                        f5 = iVar.F$1;
//                        f4 = iVar.F$0;
//                        i3 = iVar.I$0;
//                        portfolioApp = (PortfolioApp) iVar.L$0;
//                        Nc6.a(obj);
//                        strArr = (String[]) iVar.L$3;
//                        summary = obj;
//                        str = (String) iVar.L$2;
//                        mFUser2 = (MFUser) iVar.L$1;
//                        ActivitySummary activitySummary22 = (ActivitySummary) summary;
//                        sleepSummariesRepository = portfolioApp.e;
//                        if (sleepSummariesRepository == null) {
//                        }
//                        break;
//                    case 4:
//                        component3 = iVar.I$3;
//                        int i17 = iVar.I$2;
//                        component1 = iVar.I$1;
//                        float f14 = iVar.F$1;
//                        f4 = iVar.F$0;
//                        strArr2 = (String[]) iVar.L$3;
//                        str2 = (String) iVar.L$2;
//                        int i18 = iVar.I$0;
//                        portfolioApp2 = (PortfolioApp) iVar.L$0;
//                        Nc6.a(obj);
//                        i4 = i17;
//                        f5 = f14;
//                        mFUser2 = (MFUser) iVar.L$1;
//                        sleepSummaryFromDb = obj;
//                        activitySummary = (ActivitySummary) iVar.L$4;
//                        i3 = i18;
//                        MFSleepDay mFSleepDay32 = (MFSleepDay) sleepSummaryFromDb;
//                        fitnessHelper = portfolioApp2.u;
//                        if (fitnessHelper == null) {
//                        }
//                        break;
//                    case 5:
//                        component3 = iVar.I$3;
//                        i4 = iVar.I$2;
//                        component1 = iVar.I$1;
//                        float f15 = iVar.F$1;
//                        f4 = iVar.F$0;
//                        str3 = (String) iVar.L$2;
//                        i3 = iVar.I$0;
//                        portfolioApp3 = (PortfolioApp) iVar.L$0;
//                        Nc6.a(obj);
//                        strArr3 = (String[]) iVar.L$3;
//                        f6 = f15;
//                        mFUser2 = (MFUser) iVar.L$1;
//                        mFSleepDay = (MFSleepDay) iVar.L$5;
//                        activitySummary = (ActivitySummary) iVar.L$4;
//                        a3 = obj;
//                        float floatValue2 = ((Number) a3).floatValue();
//                        hh6 = new Hh6();
//                        int i152 = 0;
//                        hh6.element = 0;
//                        Hh6 hh652 = new Hh6();
//                        hh652.element = 0;
//                        Hh6 hh662 = new Hh6();
//                        hh662.element = 0;
//                        Hh6 hh672 = new Hh6();
//                        hh672.element = 0;
//                        if (activitySummary == null) {
//                        }
//                        if (mFSleepDay != null) {
//                        }
//                        UserBiometricData.BiometricGender biometricGender32 = UserBiometricData.BiometricGender.UNSPECIFIED;
//                        if (!Wg6.a((Object) mFUser2.getGender(), (Object) Rh4.MALE.getValue())) {
//                        }
//                        an4 = portfolioApp3.c;
//                        if (an4 == null) {
//                        }
//                        break;
//                    case 6:
//                        d2 = iVar.D$1;
//                        d3 = iVar.D$0;
//                        int i19 = iVar.I$4;
//                        float f16 = iVar.F$2;
//                        mFSleepDay2 = (MFSleepDay) iVar.L$5;
//                        i7 = iVar.I$3;
//                        int i20 = iVar.I$2;
//                        i9 = iVar.I$1;
//                        float f17 = iVar.F$1;
//                        float f18 = iVar.F$0;
//                        int i21 = iVar.I$0;
//                        Nc6.a(obj);
//                        f7 = obj;
//                        str4 = (String) iVar.L$2;
//                        activitySummary = (ActivitySummary) iVar.L$4;
//                        strArr4 = (String[]) iVar.L$3;
//                        i5 = i19;
//                        f8 = f18;
//                        f9 = f17;
//                        i6 = i21;
//                        i8 = i20;
//                        mFUser3 = (MFUser) iVar.L$1;
//                        f10 = f16;
//                        hh62 = (Hh6) iVar.L$9;
//                        portfolioApp4 = (PortfolioApp) iVar.L$0;
//                        hh63 = (Hh6) iVar.L$8;
//                        biometricGender = (UserBiometricData.BiometricGender) iVar.L$10;
//                        biometricWearingPosition2 = (UserBiometricData.BiometricWearingPosition) iVar.L$11;
//                        hh64 = (Hh6) iVar.L$7;
//                        hh6 = (Hh6) iVar.L$6;
//                        ReplyMessageMappingGroup replyMessageMappingGroup22 = (ReplyMessageMappingGroup) f7;
//                        iVar.L$0 = portfolioApp4;
//                        iVar.L$1 = mFUser3;
//                        iVar.I$0 = i6;
//                        iVar.L$2 = str4;
//                        iVar.L$3 = strArr4;
//                        iVar.F$0 = f8;
//                        iVar.F$1 = f9;
//                        iVar.I$1 = i9;
//                        iVar.I$2 = i8;
//                        iVar.I$3 = i7;
//                        iVar.L$4 = activitySummary;
//                        iVar.L$5 = mFSleepDay2;
//                        iVar.F$2 = f10;
//                        iVar.I$4 = i5;
//                        iVar.D$0 = d3;
//                        iVar.D$1 = d2;
//                        iVar.L$6 = hh6;
//                        iVar.L$7 = hh64;
//                        iVar.L$8 = hh63;
//                        iVar.L$9 = hh62;
//                        iVar.L$10 = biometricGender;
//                        iVar.L$11 = biometricWearingPosition2;
//                        iVar.L$12 = replyMessageMappingGroup22;
//                        iVar.label = 7;
//                        h2 = portfolioApp4.h(iVar);
//                        if (h2 != a4) {
//                        }
//                        break;
//                    case 7:
//                        biometricWearingPosition3 = (UserBiometricData.BiometricWearingPosition) iVar.L$11;
//                        biometricGender2 = (UserBiometricData.BiometricGender) iVar.L$10;
//                        Hh6 hh68 = (Hh6) iVar.L$9;
//                        Hh6 hh69 = (Hh6) iVar.L$8;
//                        Hh6 hh610 = (Hh6) iVar.L$7;
//                        Hh6 hh611 = (Hh6) iVar.L$6;
//                        d2 = iVar.D$1;
//                        d3 = iVar.D$0;
//                        i12 = iVar.I$4;
//                        f10 = iVar.F$2;
//                        MFSleepDay mFSleepDay4 = (MFSleepDay) iVar.L$5;
//                        ActivitySummary activitySummary3 = (ActivitySummary) iVar.L$4;
//                        i10 = iVar.I$3;
//                        i8 = iVar.I$2;
//                        i9 = iVar.I$1;
//                        f12 = iVar.F$1;
//                        f11 = iVar.F$0;
//                        String[] strArr6 = (String[]) iVar.L$3;
//                        String str5 = (String) iVar.L$2;
//                        int i22 = iVar.I$0;
//                        MFUser mFUser4 = (MFUser) iVar.L$1;
//                        PortfolioApp portfolioApp5 = (PortfolioApp) iVar.L$0;
//                        Nc6.a(obj);
//                        i11 = i22;
//                        hh62 = hh68;
//                        hh64 = hh610;
//                        hh63 = hh69;
//                        h2 = obj;
//                        replyMessageMappingGroup = (ReplyMessageMappingGroup) iVar.L$12;
//                        mFUser3 = mFUser4;
//                        hh6 = hh611;
//                        return new UserProfile(new UserBiometricData(i11, biometricGender2, f12, f11, biometricWearingPosition3), (long) i9, (long) f10, i10, i12, (long) i8, (long) d3, (long) d2, Vc5.a(mFUser3), hh6.element, hh64.element, hh63.element, hh62.element, null, false, -1, -1, System.currentTimeMillis(), replyMessageMappingGroup, (WorkoutDetectionSetting) h2);
//                    default:
//                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//            }
//        }
//        iVar = new i(this, xe6);
//        Object obj2 = iVar.result;
//        Object a42 = Ff6.a();
//        switch (iVar.label) {
//        }
        throw null;
//
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007f A[SYNTHETIC, Splitter:B:18:0x007f] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    public final Object d(String str, Xe6<? super Boolean> xe6) {
        u uVar;
        int i2;
        boolean z2;
        PortfolioApp portfolioApp;
        UserProfile userProfile;
        boolean z3 = false;
        if (xe6 instanceof u) {
            u uVar2 = (u) xe6;
            int i3 = uVar2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                uVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                uVar = uVar2;
                Object obj = uVar.result;
                Object a2 = Ff6.a();
                i2 = uVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    uVar.L$0 = this;
                    uVar.L$1 = str;
                    uVar.Z$0 = false;
                    uVar.label = 1;
                    obj = d(uVar);
                    if (obj == a2) {
                        return a2;
                    }
                    z2 = false;
                    portfolioApp = this;
                } else if (i2 == 1) {
                    z2 = uVar.Z$0;
                    portfolioApp = (PortfolioApp) uVar.L$0;
                    Nc6.a(obj);
                    str = (String) uVar.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                userProfile = (UserProfile) obj;
                if (userProfile != null) {
                    FLogger.INSTANCE.getLocal().e(a0, "Error inside " + a0 + ".switchActiveDevice - user is null");
                    return Hf6.a(z2);
                }
                try {
                    IButtonConnectivity iButtonConnectivity = f0;
                    if (iButtonConnectivity != null) {
                        boolean switchActiveDevice = iButtonConnectivity.switchActiveDevice(str, userProfile);
                        if (switchActiveDevice) {
                            if (str.length() == 0) {
                                portfolioApp.i(str);
                            }
                        }
                        z3 = switchActiveDevice;
                        return Hf6.a(z3);
                    }
                    Wg6.a();
                    throw null;
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            }
        }
        uVar = new u(this, xe6);
        Object obj2 = uVar.result;
        Object a22 = Ff6.a();
        i2 = uVar.label;
        if (i2 != 0) {
        }
        userProfile = (UserProfile) obj2;
        if (userProfile != null) {
        }
        throw null;
//
    }

    @DexIgnore
    public final String d(String str) {
        Wg6.b(str, "packageName");
//        String str2 = (String) Ea7.f(Yj6.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0, 6, (Object) null));
        String str2 = (String) Ea7.f(Yj6.a((CharSequence) str, new String[]{CodelessMatcher.CURRENT_CLASS_NAME}, false, 0));
        PortfolioApp portfolioApp = c0;
        if (portfolioApp != null) {
            PackageManager packageManager = portfolioApp.getPackageManager();
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
                return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
            } catch (PackageManager.NameNotFoundException e2) {
                return str2;
            }
        } else {
            Wg6.d("instance");
            throw null;
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = a0;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final int e(String str) {
        Wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            return -1;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008f  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    public final Object e(Xe6<? super char[]> xe6) {
        j jVar;
        int i2;
        char[] cArr;
        CoroutineUseCase.Ci ci;
        char[] cArr2 = new char[0];
        GeneratePassphraseUseCase generatePassphraseUseCase;
        Object a2 = null;
        char[] cArr3 = new char[0];
        CoroutineUseCase.Ci ci2;
        if (xe6 instanceof j) {
            j jVar2 = (j) xe6;
            int i3 = jVar2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                jVar2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                jVar = jVar2;
                Object obj = jVar.result;
                Object a3 = Ff6.a();
                i2 = jVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    Aw6 aw6 = this.W;
                    if (aw6 != null) {
                        Aw6.Bi bi = new Aw6.Bi("Passphrase", new Ya5());
                        jVar.L$0 = this;
                        jVar.L$1 = null;
                        jVar.label = 1;
                        obj = Gl4.a(aw6, bi, jVar);
                        if (obj == a3) {
                            return a3;
                        }
                        cArr = null;
                        ci = (CoroutineUseCase.Ci) obj;
                        if (!(ci instanceof Aw6.Di)) {
                        }
                        if (cArr2 != null) {
                        }
                        generatePassphraseUseCase = this.X;
                        if (generatePassphraseUseCase == null) {
                        }
                    } else {
                        Wg6.d("mDecryptValueKeyStoreUseCase");
                        throw null;
                    }
                } else if (i2 == 1) {
                    Nc6.a(obj);
                    throw null;
//                    this = (PortfolioApp) jVar.L$0;
//                    cArr = (char[]) jVar.L$1;
//                    ci = (CoroutineUseCase.Ci) obj;
//                    if (!(ci instanceof Aw6.Di)) {
//                        String a4 = ((Aw6.Di) ci).a();
//                        if (a4 != null) {
//                            cArr2 = a4.toCharArray();
//                            Wg6.a((Object) cArr2, "(this as java.lang.String).toCharArray()");
//                        } else {
//                            throw new Rc6("null cannot be cast to non-null type java.lang.String");
//                        }
//                    } else {
//                        cArr2 = cArr;
//                    }
//                    if (cArr2 != null) {
//                        if (!(cArr2.length == 0)) {
//                            return cArr2;
//                        }
//                    }
//                    generatePassphraseUseCase = this.X;
//                    if (generatePassphraseUseCase == null) {
//                        GeneratePassphraseUseCase.Bi bi2 = new GeneratePassphraseUseCase.Bi();
//                        jVar.L$0 = this;
//                        jVar.L$1 = cArr2;
//                        jVar.L$2 = ci;
//                        jVar.label = 2;
//                        a2 = Gl4.a(generatePassphraseUseCase, bi2, jVar);
//                        if (a2 == a3) {
//                            return a3;
//                        }
//                        cArr3 = cArr2;
//                    } else {
//                        Wg6.d("mGeneratePassphrase");
//                        throw null;
//                    }
                } else if (i2 == 2) {
                    CoroutineUseCase.Ci ci3 = (CoroutineUseCase.Ci) jVar.L$2;
                    PortfolioApp portfolioApp = (PortfolioApp) jVar.L$0;
                    Nc6.a(obj);
                    a2 = obj;
                    cArr3 = (char[]) jVar.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ci2 = (CoroutineUseCase.Ci) a2;
                if (ci2 instanceof GeneratePassphraseUseCase.Ci) {
                    return cArr3;
                }
                String a5 = ((GeneratePassphraseUseCase.Ci) ci2).a();
                if (a5 != null) {
                    char[] charArray = a5.toCharArray();
                    Wg6.a((Object) charArray, "(this as java.lang.String).toCharArray()");
                    return charArray;
                }
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        jVar = new j(this, xe6);
        Object obj2 = jVar.result;
        Object a32 = Ff6.a();
        i2 = jVar.label;
        if (i2 != 0) {
        }
        ci2 = (CoroutineUseCase.Ci) a2;
        if (ci2 instanceof GeneratePassphraseUseCase.Ci) {
        }
        throw null;
//
    }

    @DexAdd
    public String secretKey = "";


    @DexAdd
    String convertKeyForGadgetbridge(String key) {
        // https://github.com/Freeyourgadget/Gadgetbridge/wiki/Fossil-Hybrid-HR#running-the-fossil-app-with-mitmproxy-enabled
        byte[] bytes = Base64.decode(key, 0);

        StringBuilder sb = new StringBuilder(16 * 2);
        sb.append("0x");
        for (int j = 0; j < 16; j++) {
            int v = bytes[j] & 0xFF;
            sb.append(String.format("%02x", v));
        }
        return sb.toString();
    }

    @DexReplace
    public final void e(String key, String str2) {
        Wg6.b(str2, "serial");
        if (!TextUtils.isEmpty(key)) {
            secretKey = convertKeyForGadgetbridge(key);
        }
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + key);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, key);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean e() {
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            return iButtonConnectivity != null && iButtonConnectivity.getGattState(c()) == 2;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.d(str, "exception when get gatt state " + e2);
            return false;
        }
    }

    @DexIgnore
    public final int f(String str) {
        Wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception e2) {
            return 0;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object f(Xe6<? super ReplyMessageMappingGroup> xe6) {
        QuickResponseRepository quickResponseRepository = this.C;
        if (quickResponseRepository != null) {
            List<QuickResponseMessage> allQuickResponse = quickResponseRepository.getAllQuickResponse();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList();
            for (QuickResponseMessage t2 : allQuickResponse) {
                if (!Hf6.a(t2.getResponse().length() == 0).booleanValue()) {
                    arrayList.add(t2);
                }
            }
            ArrayList arrayList2 = new ArrayList(Rd6.a(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            return new ReplyMessageMappingGroup(Ea7.d((Collection) arrayList2), "icMessage.icon");
        }
        Wg6.d("mQuickResponseRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final Object g(Xe6<? super Date> xe6) {
        k kVar;
        int i2;
        Object currentUser = null;
        MFUser mFUser;
        if (xe6 instanceof k) {
            kVar = (k) xe6;
            int i3 = kVar.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                kVar.label = i3 + RecyclerView.UNDEFINED_DURATION;
                Object obj = kVar.result;
                Object a2 = Ff6.a();
                i2 = kVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    UserRepository userRepository = this.A;
                    if (userRepository != null) {
                        kVar.L$0 = this;
                        kVar.label = 1;
                        currentUser = userRepository.getCurrentUser(kVar);
                        if (currentUser == a2) {
                            return a2;
                        }
                    } else {
                        Wg6.d("mUserRepository");
                        throw null;
                    }
                } else if (i2 == 1) {
                    PortfolioApp portfolioApp = (PortfolioApp) kVar.L$0;
                    Nc6.a(obj);
                    currentUser = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                mFUser = (MFUser) currentUser;
                if (mFUser != null) {
                    String createdAt = mFUser.getCreatedAt();
                    if (!TextUtils.isEmpty(createdAt)) {
                        if (createdAt != null) {
                            Date a3 = Ye5.a(TimeUtils.d(createdAt));
                            Wg6.a((Object) a3, "TimeHelper.getStartOfDay(registeredDate)");
                            return a3;
                        }
                        Wg6.a();
                        throw null;
                    }
                }
                FLogger.INSTANCE.getLocal().d(a0, "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)");
                return new Date(1, 1, 1);
            }
        }
        kVar = new k(this, xe6);
        Object obj2 = kVar.result;
        Object a22 = Ff6.a();
        i2 = kVar.label;
        if (i2 != 0) {
        }
        mFUser = (MFUser) currentUser;
        if (mFUser != null) {
        }
        FLogger.INSTANCE.getLocal().d(a0, "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)");
        return new Date(1, 1, 1);
    }


    @DexIgnore
    public final String g() {
//        List a2;
//        boolean z2;
//        boolean z3 = true;
//        if (d0) {
//            List<String> split = new Mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.5.0", 0);
//            if (!split.isEmpty()) {
//                ListIterator<String> listIterator = split.listIterator(split.size());
//                while (true) {
//                    if (!listIterator.hasPrevious()) {
//                        break;
//                    }
//                    if (listIterator.previous().length() == 0) {
//                        z2 = true;
//                        continue;
//                    } else {
//                        z2 = false;
//                        continue;
//                    }
//                    if (!z2) {
//                        a2 = Ea7.d(split, listIterator.nextIndex() + 1);
//                        break;
//                    }
//                }
//            }
//            a2 = Qd6.a();
//            Object[] array = a2.toArray(new String[0]);
//            if (array != null) {
//                String[] strArr = (String[]) array;
//                if (strArr.length != 0) {
//                    z3 = false;
//                }
//                if (!z3) {
//                    return strArr[0];
//                }
//            } else {
//                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
//            }
//        }
        return "4.5.0";
    }

    @DexIgnore
    public final boolean g(String str) {
        Wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            return false;
        }
    }

    @DexIgnore
    public final Iface getIface() {
        Iface iface = this.L;
        if (iface != null) {
            return iface;
        }
        Wg6.d("applicationComponent");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    public final /* synthetic */ Object h(Xe6<? super WorkoutDetectionSetting> xe6) {
        l lVar;
        int i2;
        Object workoutSettingList = null;
        if (xe6 instanceof l) {
            lVar = (l) xe6;
            int i3 = lVar.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                lVar.label = i3 + RecyclerView.UNDEFINED_DURATION;
                Object obj = lVar.result;
                Object a2 = Ff6.a();
                i2 = lVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(a0, "getWorkoutDetectionSetting()");
                    WorkoutSettingRepository workoutSettingRepository = this.D;
                    if (workoutSettingRepository != null) {
                        lVar.L$0 = this;
                        lVar.label = 1;
                        workoutSettingList = workoutSettingRepository.getWorkoutSettingList(lVar);
                        if (workoutSettingList == a2) {
                            return a2;
                        }
                    } else {
                        Wg6.d("mWorkoutSettingRepository");
                        throw null;
                    }
                } else if (i2 == 1) {
                    PortfolioApp portfolioApp = (PortfolioApp) lVar.L$0;
                    Nc6.a(obj);
                    workoutSettingList = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Ay6.a((List) workoutSettingList);
            }
        }
        lVar = new l(this, xe6);
        Object obj2 = lVar.result;
        Object a22 = Ff6.a();
        i2 = lVar.label;
        if (i2 != 0) {
        }
        return Ay6.a((List) workoutSettingList);
    }

    @DexIgnore
    public final String h() {
        String name = Eb5.fromInt(Integer.parseInt(Ol4.A.d())).getName();
        Wg6.a((Object) name, "FossilBrand.fromInt(Inte\u2026onfig.brandId)).getName()");
        return name;
    }

    @DexIgnore
    public final void h(String str) {
        Wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexIgnore
    public final DataValidationManager i() {
        DataValidationManager dataValidationManager = this.z;
        if (dataValidationManager != null) {
            return dataValidationManager;
        }
        Wg6.d("mDataValidationManager");
        throw null;
    }

    @DexIgnore
    public final /* synthetic */ Object i(Xe6<? super Cd6> xe6) {
        FLogger.INSTANCE.getLocal().d(a0, "initCrashlytics");
        X24.a().a("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        X24.a().a("Locale", Locale.getDefault().toString());
        Object n2 = n(xe6);
        return n2 == Ff6.a() ? n2 : Cd6.a;
    }

    @DexIgnore
    public final void i(String str) {
        Wg6.b(str, "newActiveDeviceSerial");
        String c2 = c();
        An4 an4 = this.c;
        if (an4 != null) {
            an4.o(str);
            throw null;
//            this.G.a(str);
//            FossilNotificationBar.c.a(this);
//            ILocalFLogger local = FLogger.INSTANCE.getLocal();
//            String str2 = a0;
//            local.d(str2, "onSwitchActiveDeviceSuccess - current=" + c2 + ", new=" + str);
//            return;
        }
        Wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final long j(String str) {
        Wg6.b(str, "serial");
        return a(str, 1, 1, true);
    }

    @DexIgnore
    public final Object j(Xe6<? super Cd6> xe6) {
        Object a2 = Vh7.a(Qj7.b(), new m(this, null), xe6);
        return a2 == Ff6.a() ? a2 : Cd6.a;
    }

    @DexIgnore
    public final String j() {
        String h2 = h();
        throw null;
//        return Wg6.a(h2, Eb5.CITIZEN.getName()) ? "citizen" : Wg6.a(h2, Eb5.UNIVERSAL.getName()) ? "universal" : Endpoints.DEFAULT_NAME;
    }

    @DexIgnore
    public final long k(String str) {
        Wg6.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.readCurrentWorkoutSession(str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".readCurrentWorkoutSession - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND k() {
        String h2 = h();
        throw null;
//        return Wg6.a(h2, Eb5.CHAPS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CHAPS : Wg6.a(h2, Eb5.DIESEL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.DIESEL : Wg6.a(h2, Eb5.EA.getName()) ? FossilDeviceSerialPatternUtil.BRAND.EA : Wg6.a(h2, Eb5.KATESPADE.getName()) ? FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE : Wg6.a(h2, Eb5.MICHAELKORS.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS : Wg6.a(h2, Eb5.SKAGEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.SKAGEN : Wg6.a(h2, Eb5.AX.getName()) ? FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE : Wg6.a(h2, Eb5.RELIC.getName()) ? FossilDeviceSerialPatternUtil.BRAND.RELIC : Wg6.a(h2, Eb5.MJ.getName()) ? FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS : Wg6.a(h2, Eb5.FOSSIL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.FOSSIL : Wg6.a(h2, Eb5.UNIVERSAL.getName()) ? FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL : Wg6.a(h2, Eb5.CITIZEN.getName()) ? FossilDeviceSerialPatternUtil.BRAND.CITIZEN : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final Object k(Xe6<? super Cd6> xe6) {
//        r rVar;
//        int i2;
//        MFUser mFUser;
//        if (xe6 instanceof r) {
//            rVar = (r) xe6;
//            int i3 = rVar.label;
//            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
//                rVar.label = i3 + RecyclerView.UNDEFINED_DURATION;
//                Object obj = rVar.result;
//                Object a2 = Ff6.a();
//                i2 = rVar.label;
//                if (i2 != 0) {
//                    Nc6.a(obj);
//                    UserRepository userRepository = this.A;
//                    if (userRepository != null) {
//                        rVar.L$0 = this;
//                        rVar.label = 1;
//                        obj = userRepository.getCurrentUser(rVar);
//                        if (obj == a2) {
//                            return a2;
//                        }
//                    } else {
//                        Wg6.d("mUserRepository");
//                        throw null;
//                    }
//                } else if (i2 == 1) {
//                    Nc6.a(obj);
//                    this = (PortfolioApp) rVar.L$0;
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                mFUser = (MFUser) obj;
//                if (mFUser != null) {
//                    return Cd6.a;
//                }
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String str = a0;
//                local.d(str, "registerContactObserver currentUser=" + mFUser);
//                boolean e2 = PermissionUtils.a.e(this);
//                if (e2) {
//                    Uw6 uw6 = this.j;
//                    if (uw6 != null) {
//                        uw6.b();
//                        ContentResolver contentResolver = this.getContentResolver();
//                        Uri uri = ContactsContract.Contacts.CONTENT_URI;
//                        Uw6 uw62 = this.j;
//                        if (uw62 != null) {
//                            contentResolver.registerContentObserver(uri, true, uw62);
//                            FLogger.INSTANCE.getLocal().d(a0, "registerContactObserver success");
//                            An4 an4 = this.c;
//                            if (an4 != null) {
//                                an4.n(true);
//                            } else {
//                                Wg6.d("sharedPreferencesManager");
//                                throw null;
//                            }
//                        } else {
//                            Wg6.d("mContactObserver");
//                            throw null;
//                        }
//                    } else {
//                        Wg6.d("mContactObserver");
//                        throw null;
//                    }
//                } else {
//                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
//                    String str2 = a0;
//                    local2.d(str2, "registerContactObserver fail due to enough Permission =" + e2);
//                }
//                return Cd6.a;
//            }
//        }
//        rVar = new r(this, xe6);
//        Object obj2 = rVar.result;
//        Object a22 = Ff6.a();
//        i2 = rVar.label;
//        if (i2 != 0) {
//        }
//        mFUser = (MFUser) obj2;
//        if (mFUser != null) {
//        }
        throw null;
//
    }


    @DexIgnore
    public final Object l(Xe6<? super Cd6> xe6) {
        Object a2 = Vh7.a(Qj7.b(), new s(this, null), xe6);
        return a2 == Ff6.a() ? a2 : Cd6.a;
    }

    @DexIgnore
    public final String l() {
        Locale locale = Locale.getDefault();
        Wg6.a((Object) locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (Wg6.a((Object) language, (Object) "iw")) {
            language = "he";
        }
        if (Wg6.a((Object) language, (Object) "in")) {
            language = "id";
        }
        if (Wg6.a((Object) language, (Object) "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            We7 we7 = We7.a;
            Locale locale2 = Locale.US;
            Wg6.a((Object) locale2, "Locale.US");
            language = String.format(locale2, "%s-%s", Arrays.copyOf(new Object[]{language, country}, 2));
            Wg6.a((Object) language, "java.lang.String.format(locale, format, *args)");
        }
        Wg6.a((Object) language, "localeString");
        return language;
    }

    @DexIgnore
    public final void l(String str) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0032 A[Catch:{ Exception -> 0x005b }] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    public final Object m(Xe6<? super Cd6> xe6) {
        w wVar;
        int i2;
        Object a2 = null;
        IButtonConnectivity iButtonConnectivity;
        if (xe6 instanceof w) {
            wVar = (w) xe6;
            int i3 = wVar.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                wVar.label = i3 + RecyclerView.UNDEFINED_DURATION;
                Object obj = wVar.result;
                Object a3 = Ff6.a();
                i2 = wVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    AppHelper b2 = AppHelper.g.b();
                    wVar.L$0 = this;
                    wVar.label = 1;
                    a2 = b2.a(wVar);
                    if (a2 == a3) {
                        return a3;
                    }
                } else if (i2 == 1) {
                    PortfolioApp portfolioApp = (PortfolioApp) wVar.L$0;
                    Nc6.a(obj);
                    a2 = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                AppLogInfo appLogInfo = (AppLogInfo) a2;
                FLogger.INSTANCE.updateAppLogInfo(appLogInfo);
                iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    try {
                        iButtonConnectivity.updateAppLogInfo(appLogInfo);
                    } catch (RemoteException ex) {
                        ex.printStackTrace();
                    }
                }
                return Cd6.a;
            }
        }
        wVar = new w(this, xe6);
        Object obj2 = wVar.result;
        Object a32 = Ff6.a();
        i2 = wVar.label;
        if (i2 != 0) {
        }
        AppLogInfo appLogInfo2 = (AppLogInfo) a2;
        FLogger.INSTANCE.updateAppLogInfo(appLogInfo2);
        try {
            iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a0;
            local.e(str, ".updateAppLogInfo(), error=" + e2);
        }
        return Cd6.a;
    }

    @DexIgnore
    public final String m() {
        Resources resources = getResources();
        Wg6.a((Object) resources, "resources");
        String languageTag = E8.a(resources.getConfiguration()).a(0).toLanguageTag();
        Wg6.a((Object) languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final void m(String str) {
        Wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a0;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                Wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final long n(String str) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        if (str == null) {
            FLogger.INSTANCE.getLocal().e(a0, "sendingEncryptedDataSession - encryptedData is null");
            An4 an4 = this.c;
            if (an4 != null) {
                an4.a((Boolean) true);
                return time_stamp_for_non_executable_method;
            }
            Wg6.d("sharedPreferencesManager");
            throw null;
        }
        try {
            byte[] decode = Base64.decode(str, 0);
            An4 an42 = this.c;
            if (an42 != null) {
                Boolean a2 = an42.a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a0;
                local.e(str2, "sendingEncryptedDataSession - encryptedData: " + str + " - latestBCStatus: " + a2);
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    String c2 = c();
                    Wg6.a((Object) a2, "latestBCStatus");
                    return iButtonConnectivity.sendingEncryptedDataSession(decode, c2, a2.booleanValue());
                }
                Wg6.a();
                throw null;
            }
            Wg6.d("sharedPreferencesManager");
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local2.e(str3, ".sendingEncryptedDataSession - e=" + e2);
            e2.printStackTrace();
            An4 an43 = this.c;
            if (an43 != null) {
                an43.a((Boolean) true);
                return time_stamp_for_non_executable_method;
            }
            Wg6.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final ApiServiceV2 n() {
        ApiServiceV2 apiServiceV2 = this.i;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        Wg6.d("mApiService");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    public final /* synthetic */ Object n(Xe6<? super Cd6> xe6) {
        x xVar;
        int i2;
        Object currentUser = null;
        MFUser mFUser;
        if (xe6 instanceof x) {
            xVar = (x) xe6;
            int i3 = xVar.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                xVar.label = i3 + RecyclerView.UNDEFINED_DURATION;
                Object obj = xVar.result;
                Object a2 = Ff6.a();
                i2 = xVar.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    UserRepository userRepository = this.A;
                    if (userRepository != null) {
                        xVar.L$0 = this;
                        xVar.label = 1;
                        currentUser = userRepository.getCurrentUser(xVar);
                        if (currentUser == a2) {
                            return a2;
                        }
                    } else {
                        Wg6.d("mUserRepository");
                        throw null;
                    }
                } else if (i2 == 1) {
                    PortfolioApp portfolioApp = (PortfolioApp) xVar.L$0;
                    Nc6.a(obj);
                    currentUser = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                mFUser = (MFUser) currentUser;
                if (mFUser != null) {
                    return Cd6.a;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a0;
                local.d(str, "updateCrashlyticsUserInformation currentUser=" + mFUser);
                X24.a().a(mFUser.getUserId());
                X24.a().a("UserId", mFUser.getUserId());
                return Cd6.a;
            }
        }
        xVar = new x(this, xe6);
        Object obj2 = xVar.result;
        Object a22 = Ff6.a();
        i2 = xVar.label;
        if (i2 != 0) {
        }
        mFUser = (MFUser) currentUser;
        if (mFUser != null) {
        }
        throw null;
//
    }

    @DexIgnore
    public final Zz6 o() {
        Zz6 zz6 = this.x;
        if (zz6 != null) {
            return zz6;
        }
        Wg6.d("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public final void o(String str) {
        int a2;
        String str2;
        if (str == null) {
            a2 = 1024;
            str2 = "";
        } else {
            a2 = Ze5.a(str);
            str2 = str;
        }
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = a0;
            local.d(str3, "Inside " + a0 + ".setAutoSecondTimezone - offsetMinutes=" + a2 + ", mSecondTimezoneId=" + str2);
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                if (str == null) {
                    str = "";
                }
                iButtonConnectivity.deviceSetAutoSecondTimezone(str);
                return;
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = a0;
            local2.e(str4, "Inside " + a0 + ".setAutoSecondTimezone - ex=" + e2);
        }
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        Wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        Wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        Wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.PAUSE;
        if (d0) {
            ShakeFeedbackService shakeFeedbackService = this.v;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.e();
            } else {
                Wg6.d("mShakeFeedbackService");
                throw null;
            }
        }
        this.I = true;
        Runnable runnable = this.K;
        if (runnable != null) {
            this.J.removeCallbacks(runnable);
        }
        this.J.postDelayed(new n(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        Wg6.b(activity, Constants.ACTIVITY);
        if (d0) {
            ShakeFeedbackService shakeFeedbackService = this.v;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.a(activity);
            } else {
                Wg6.d("mShakeFeedbackService");
                throw null;
            }
        }
        ActivityState activityState = ActivityState.RESUME;
        this.I = false;
        boolean z2 = this.H;
        this.H = true;
        Runnable runnable = this.K;
        if (runnable != null) {
            this.J.removeCallbacks(runnable);
        }
        if (!z2) {
            b0 = true;
            FLogger.INSTANCE.getLocal().d(a0, "from background");
            If5 if5 = this.P;
            if (if5 != null) {
                if5.d();
            }
        } else {
            b0 = false;
            FLogger.INSTANCE.getLocal().d(a0, "still foreground");
        }
        if (this.N) {
            a(this.O);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        Wg6.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        Wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        Wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.STOP;
    }

    @DexAdd
    public class AutoSyncSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        PortfolioApp p;
        AutoSyncSettingsListener(PortfolioApp _p) {
            p = _p;
        }

        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            autoSyncEnabled = AlelecPrefs.getAutoSync(this.p);
            FLogger.INSTANCE.getLocal().v(a0, "autoSyncEnabled: " + autoSyncEnabled);
            // if (AlelecPrefs.getAutoSync(this.p)) {
            //     startAutoSync();
            //     autoSyncEnabled = true
            // } else {
            //     stopAutoSync();
            // }
        }
    }
    @DexAdd
    public boolean autoSyncEnabled = false;
    @DexAdd
    public ScreenReceiver mScreenReceiver = null;
    @DexAdd
    public PendingIntent alarmIntent = null;

    @DexAdd
    public void triggerSync() {
        AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        if (alarmMgr != null) {
            FLogger.INSTANCE.getLocal().v(a0, "startAutoSync");
            Intent intent = new Intent(this, NotificationReceiver.class);
            intent.setAction(".news.notifications.NotificationReceiver");
            intent.putExtra("ACTION_EVENT", 1);
            if (alarmIntent == null) {
                alarmIntent = PendingIntent.getBroadcast(this, Action.DisplayMode.DATE, intent, FLAG_UPDATE_CURRENT);
            }
            alarmMgr.setExact(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + 3 * 1000,
                    alarmIntent);
        }
    }

    @DexAdd
    public class ScreenReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equals(intent.getAction())) {
                // ignore
            } else if (Intent.ACTION_SCREEN_ON.equals(intent.getAction())) {
                FLogger.INSTANCE.getLocal().v(a0, "screen on, autoSyncEnabled: " + autoSyncEnabled);
                autoSyncEnabled = AlelecPrefs.getAutoSync(PortfolioApp.this);
                if (autoSyncEnabled) {
                    triggerSync();
                }
            }
        }

    }

    @DexAdd
    public AutoSyncSettingsListener autoSyncSettingsListener = null;

    @SuppressLint("MissingSuperCall")
    @DexAppend
    public void onCreate() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().build());

        if (autoSyncSettingsListener == null) {
            autoSyncSettingsListener = new AutoSyncSettingsListener(this);
            // if (AlelecPrefs.getAutoSync(this)) {
            //     startAutoSync();
            // }
            autoSyncEnabled = AlelecPrefs.getAutoSync(this);
            AlelecPrefs.registerAutoSync(this, autoSyncSettingsListener);
        }
        if (mScreenReceiver == null) {
            IntentFilter mScreenIntentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            // mScreenIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);
            mScreenReceiver = new ScreenReceiver();
            registerReceiver(mScreenReceiver, mScreenIntentFilter);
        }
    }

    /*
    @DexIgnore
    public void onCreate() {
        super.onCreate();
        c0 = this;
        d0 = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (E()) {
            Wj4 wj4 = new Wj4(this);
            Hl4.Zi L0 = Hl4.L0();
            L0.a(wj4);
            Iface a2 = L0.a();
            Wg6.a((Object) a2, "DaggerApplicationCompone\u2026\n                .build()");
            this.L = a2;
            if (a2 != null) {
                a2.a(this);
                BCAnalytic bCAnalytic = this.Y;
                if (bCAnalytic != null) {
                    bCAnalytic.a(this);
                    System.loadLibrary("FitnessAlgorithm");
                    LifecycleOwner g2 = Be.g();
                    Wg6.a((Object) g2, "ProcessLifecycleOwner.get()");
                    Lifecycle lifecycle = g2.getLifecycle();
                    ApplicationEventListener applicationEventListener = this.s;
                    if (applicationEventListener != null) {
                        lifecycle.a(applicationEventListener);
                        MicroAppEventLogger.initialize(this);
                        if (!G()) {
                            Stetho.initializeWithDefaults(this);
                        }
//                        Rm6 unused = Xh7.b(this.Z, null, null, new o(this, null), 3, null);
                        Rm6 unused = Xh7.b(this.Z, null, null, new o(this, null));
                        FLogger.INSTANCE.getLocal().d(a0, "On app create");
                        e0 = new Qc5(L07.a);
                        MutableLiveData<String> mutableLiveData = this.G;
                        An4 an4 = this.c;
                        if (an4 != null) {
                            mutableLiveData.b(an4.e());
                            registerActivityLifecycleCallbacks(this);
                            String str = "4.5.0";
                            if (d0) {
                                Object[] array = new Mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.5.0", 2).toArray(new String[0]);
                                if (array != null) {
                                    str = ((String[]) array)[0];
                                } else {
                                    throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                            }
                            GuestApiService guestApiService = this.g;
                            if (guestApiService != null) {
                                this.a = new LocalizationManager(this, str, guestApiService);
                                this.b = new AppPackageInstallReceiver();
                                Jl4.a();
                                registerReceiver(this.a, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                                K();
                                LocalizationManager localizationManager = this.a;
                                if (localizationManager != null) {
                                    registerActivityLifecycleCallbacks(localizationManager.a());
                                    LocalizationManager localizationManager2 = this.a;
                                    if (localizationManager2 != null) {
                                        String simpleName = SplashScreenActivity.class.getSimpleName();
                                        Wg6.a((Object) simpleName, "SplashScreenActivity::class.java.simpleName");
                                        localizationManager2.a(simpleName);
                                        LocalizationManager localizationManager3 = this.a;
                                        if (localizationManager3 != null) {
                                            localizationManager3.i();
                                            ResolutionHelper.INSTANCE.initDeviceDensity(this);
                                            try {
                                                if (Build.VERSION.SDK_INT >= 24) {
                                                    NetworkChangedReceiver networkChangedReceiver = new NetworkChangedReceiver();
                                                    this.R = networkChangedReceiver;
                                                    registerReceiver(networkChangedReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                                                }
                                            } catch (Exception e2) {
                                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                String str2 = a0;
                                                local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                                            }
                                            IntentFilter intentFilter = new IntentFilter();
                                            intentFilter.addAction("android.intent.action.TIME_TICK");
                                            intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                                            registerReceiver(this.S, intentFilter);
                                            M();
                                            this.M = Thread.getDefaultUncaughtExceptionHandler();
                                            Thread.setDefaultUncaughtExceptionHandler(new p(this));
                                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                            StrictMode.setVmPolicy(builder.build());
                                            builder.detectFileUriExposure();
                                            AlarmHelper alarmHelper = this.f;
                                            if (alarmHelper != null) {
                                                alarmHelper.c(this);
                                                AlarmHelper alarmHelper2 = this.f;
                                                if (alarmHelper2 != null) {
                                                    alarmHelper2.d(this);
                                                    this.P = AnalyticsHelper.f.b("app_appearance");
                                                    AnalyticsHelper analyticsHelper = this.r;
                                                    if (analyticsHelper != null) {
                                                        analyticsHelper.b("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                                                        if (!TextUtils.isEmpty(c())) {
                                                            FLogger.INSTANCE.getLocal().d(a0, "Service Tracking - startForegroundService in PortfolioApp onCreate");
                                                            throw null;
//                                                            ServiceUtils.Ai.a(ServiceUtils.a, this, MFDeviceService.class, null, 4, null);
//                                                            ServiceUtils.Ai.a(ServiceUtils.a, this, ButtonService.class, null, 4, null);
                                                        }
                                                        FlutterEngine flutterEngine = new FlutterEngine(this);
                                                        flutterEngine.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), Constants.MAP));
                                                        FlutterEngineCache.getInstance().put("map_engine_id", flutterEngine);
                                                        FlutterEngine flutterEngine2 = new FlutterEngine(this);
                                                        flutterEngine2.getDartExecutor().executeDartEntrypoint(new DartExecutor.DartEntrypoint(FlutterMain.findAppBundlePath(), "screenShotMap"));
                                                        FlutterEngineCache.getInstance().put("screenshot_engine_id", flutterEngine2);
                                                        return;
                                                    }
                                                    Wg6.d("mAnalyticsHelper");
                                                    throw null;
                                                }
                                                Wg6.d("mAlarmHelper");
                                                throw null;
                                            }
                                            Wg6.d("mAlarmHelper");
                                            throw null;
                                        }
                                        Wg6.a();
                                        throw null;
                                    }
                                    Wg6.a();
                                    throw null;
                                }
                                Wg6.a();
                                throw null;
                            }
                            Wg6.d("mGuestApiService");
                            throw null;
                        }
                        Wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                    Wg6.d("mAppEventManager");
                    throw null;
                }
                Wg6.d("bcAnalytic");
                throw null;
            }
            Wg6.d("applicationComponent");
            throw null;
        }
    }
    */

    @DexIgnore
    public void onLowMemory() {
        super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Inside " + a0 + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    public void onTerminate() {
        super.onTerminate();
        FLogger.INSTANCE.getLocal().d(a0, "---Inside .onTerminate of Application");
        Q();
        unregisterReceiver(this.a);
        P();
        LocalizationManager localizationManager = this.a;
        unregisterActivityLifecycleCallbacks(localizationManager != null ? localizationManager.a() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(a0, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.R);
        }
    }

    @DexIgnore
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a0;
        local.d(str, "Inside " + a0 + ".onTrimMemory");
        super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    public final LocalizationManager p() {
        return this.a;
    }

    @DexIgnore
    public final void p(String str) {
        MisfitDeviceProfile deviceProfile;
        MisfitDeviceProfile deviceProfile2;
        Wg6.b(str, "serial");
        try {
            String m2 = m();
            IButtonConnectivity iButtonConnectivity = f0;
            String locale = (iButtonConnectivity == null || (deviceProfile2 = iButtonConnectivity.getDeviceProfile(this.G.a())) == null) ? null : deviceProfile2.getLocale();
            An4 an4 = this.c;
            if (an4 != null) {
                String f2 = an4.f(m2);
                IButtonConnectivity iButtonConnectivity2 = f0;
                String localeVersion = (iButtonConnectivity2 == null || (deviceProfile = iButtonConnectivity2.getDeviceProfile(this.G.a())) == null) ? null : deviceProfile.getLocaleVersion();
                An4 an42 = this.c;
                if (an42 != null) {
                    String s2 = an42.s();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = a0;
                    local.d(str2, "phoneLocale " + m2 + " - watchLocale: " + locale + " - standardLocale: " + f2 + "\nwatchLocaleVersion: " + localeVersion + " - standardLocaleVersion: " + s2);
                    if (!Wg6.a((Object) f2, (Object) locale)) {
                        a(m2, str);
                    } else if (!Wg6.a((Object) localeVersion, (Object) s2)) {
                        a(m2, str);
                    } else {
                        FLogger.INSTANCE.getLocal().d(a0, "don't need to set to watch");
                    }
                } else {
                    Wg6.d("sharedPreferencesManager");
                    throw null;
                }
            } else {
                Wg6.d("sharedPreferencesManager");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final String q() {
        try {
            int myPid = Process.myPid();
            Object systemService = getSystemService(Context.ACTIVITY_SERVICE);
            if (systemService != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) systemService).getRunningAppProcesses()) {
                    if (runningAppProcessInfo.pid == myPid) {
                        return runningAppProcessInfo.processName;
                    }
                }
                return null;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.ActivityManager");
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(a0, "exception when get process name");
        }
        throw null;
    }

    @DexIgnore
    public final void q(String str) {
        Wg6.b(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final ThemeRepository r() {
        ThemeRepository themeRepository = this.V;
        if (themeRepository != null) {
            return themeRepository;
        }
        Wg6.d("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final void r(String str) {
        Wg6.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (Xj6.b(str, c(), true)) {
                    An4 an4 = this.c;
                    if (an4 != null) {
                        an4.o("");
                        throw null;
//                        this.G.a("");
                    } else {
                        Wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = f0;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    An4 an42 = this.c;
                    if (an42 != null) {
                        an42.a(str, 0L, false);
                    } else {
                        Wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    Wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = a0;
                local.e(str2, "Inside " + a0 + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final long s(String str) {
        Wg6.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(a0, "verifySecretKey()");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = f0;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.verifySecretKeySession(str);
            }
            Wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = a0;
            local.e(str2, ".verifySecretKey(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final UserRepository s() {
        UserRepository userRepository = this.A;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository t() {
        WatchLocalizationRepository watchLocalizationRepository = this.y;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        Wg6.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutTetherGpsManager u() {
        WorkoutTetherGpsManager workoutTetherGpsManager = this.F;
        if (workoutTetherGpsManager != null) {
            return workoutTetherGpsManager;
        }
        Wg6.d("mWorkoutGpsManager");
        throw null;
    }

    @DexIgnore
    public final An4 v() {
        An4 an4 = this.c;
        if (an4 != null) {
            return an4;
        }
        Wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final String w() {
        An4 an4 = this.c;
        if (an4 != null) {
            String A2 = an4.A();
            return A2 != null ? A2 : "";
        }
        Wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final String x() {
        MisfitDeviceProfile a2;
        W40 uiPackageOSVersion;
        throw null;
//        if (!(!Xj6.a((CharSequence) c())) || (a2 = DeviceHelper.o.e().a(c())) == null || (uiPackageOSVersion = a2.getUiPackageOSVersion()) == null) {
//            return null;
//        }
//        byte major = uiPackageOSVersion.getMajor();
//        byte minor = uiPackageOSVersion.getMinor();
//        if (major == 0 && minor == 0) {
//            return null;
//        }
//        StringBuilder sb = new StringBuilder();
//        sb.append((int) major);
//        sb.append('.');
//        sb.append((int) minor);
//        return sb.toString();
    }

    @DexIgnore
    public final WatchFaceRepository y() {
        WatchFaceRepository watchFaceRepository = this.B;
        if (watchFaceRepository != null) {
            return watchFaceRepository;
        }
        Wg6.d("watchFaceRepository");
        throw null;
    }

    @DexIgnore
    public final boolean z() {
        Object systemService = getSystemService(Context.CONNECTIVITY_SERVICE);
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new Rc6("null cannot be cast to non-null type android.net.ConnectivityManager");
    }
}
