package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bx6;
import com.fossil.Et5;
import com.fossil.Ft5;
import com.fossil.Ho5;
import com.fossil.Jf5;
import com.fossil.Mg;
import com.fossil.Ou5;
import com.fossil.Qb;
import com.fossil.Qu5;
import com.fossil.Qw6;
import com.fossil.Wp5;
import com.mapped.AlertDialogFragment;
import com.mapped.AppWrapper;
import com.mapped.Iface;
import com.mapped.NotificationAppsFragmentBinding;
import com.mapped.PermissionUtils;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.uirenew.home.profile.help.HelpActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.RTLImageView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.ADD)
public final class NotificationAppsFragment extends Ho5 implements Ft5, AlertDialogFragment.Gi {
    @DexIgnore
    public static /* final */ String q;
    @DexIgnore
    public static /* final */ Ai r; // = new Ai(null);
    @DexIgnore
    public Et5 g;
    @DexIgnore
    public Qw6<NotificationAppsFragmentBinding> h;
    @DexIgnore
    public Ou5 i;
    @DexIgnore
    public Wp5 j;
    @DexIgnore
    public HashMap p;

    @DexIgnore
    NotificationAppsFragment() {}

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsFragment.q;
        }

        @DexIgnore
        public final NotificationAppsFragment b() {
            return new NotificationAppsFragment();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Wp5.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public Bi(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        @Override // com.fossil.Wp5.Bi
        public void a(AppWrapper appWrapper, boolean z) {
            Wg6.b(appWrapper, "appWrapper");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsFragment.r.a();
            StringBuilder sb = new StringBuilder();
            sb.append("appName = ");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            sb.append(installedApp != null ? installedApp.getTitle() : null);
            sb.append(", selected = ");
            sb.append(z);
            local.d(a2, sb.toString());
            NotificationAppsFragment.b(this.a).a(appWrapper, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public Ci(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FLogger.INSTANCE.getLocal().d(NotificationAppsFragment.r.a(), "press on close button");
            NotificationAppsFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;

        @DexIgnore
        public Di(NotificationAppsFragment notificationAppsFragment) {
            this.a = notificationAppsFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            NotificationAppsFragment.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding b;

        @DexIgnore
        public Ei(NotificationAppsFragment notificationAppsFragment, NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.a = notificationAppsFragment;
            this.b = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        @SuppressLint("WrongConstant")
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            RTLImageView rTLImageView = this.b.u;
            Wg6.a((Object) rTLImageView, "binding.ivClear");
            rTLImageView.setVisibility(i3 == 0 ? 4 : 0);
            NotificationAppsFragment.a(this.a).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding a;

        @DexIgnore
        public Fi(NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.a = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                NotificationAppsFragmentBinding notificationAppsFragmentBinding = this.a;
                Wg6.a((Object) notificationAppsFragmentBinding, "binding");
                notificationAppsFragmentBinding.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsFragmentBinding a;

        @DexIgnore
        public Gi(NotificationAppsFragmentBinding notificationAppsFragmentBinding) {
            this.a = notificationAppsFragmentBinding;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.r.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationAppsFragment.class.getSimpleName();
        Wg6.a((Object) simpleName, "NotificationAppsFragment::class.java.simpleName");
        q = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ Wp5 a(NotificationAppsFragment notificationAppsFragment) {
        Wp5 wp5 = notificationAppsFragment.j;
        if (wp5 != null) {
            return wp5;
        }
        Wg6.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Et5 b(NotificationAppsFragment notificationAppsFragment) {
        Et5 et5 = notificationAppsFragment.g;
        if (et5 != null) {
            return et5;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5
    public void Z0() {
        HashMap hashMap = this.p;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void a(Et5 et5) {
        Wg6.b(et5, "presenter");
        this.g = et5;
    }

    @DexIgnore
    @Override // com.fossil.Ho5, com.mapped.AlertDialogFragment.Gi
    public void a(String str, int i2, Intent intent) {
        Wg6.b(str, "tag");
        int hashCode = str.hashCode();
        if (hashCode != 927511079) {
            if (hashCode == 1761436236 && str.equals("FAIL_DUE_TO_DEVICE_DISCONNECTED") && i2 == 2131363307) {
                Et5 et5 = this.g;
                if (et5 != null) {
                    et5.i();
                } else {
                    Wg6.d("mPresenter");
                    throw null;
                }
            }
        } else if (!str.equals("UPDATE_FIRMWARE_FAIL_TROUBLESHOOTING")) {
        } else {
            if (i2 == 2131362268) {
                Et5 et52 = this.g;
                if (et52 != null) {
                    et52.h();
                } else {
                    Wg6.d("mPresenter");
                    throw null;
                }
            } else if (i2 != 2131362359) {
                if (i2 == 2131362656) {
                    close();
                }
            } else if (getActivity() != null) {
                HelpActivity.a aVar = HelpActivity.z;
                FragmentActivity requireActivity = requireActivity();
                Wg6.a((Object) requireActivity, "requireActivity()");
                aVar.a(requireActivity);
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Ft5
    public void c() {
        if (isActive()) {
            Bx6 bx6 = Bx6.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Wg6.a((Object) childFragmentManager, "childFragmentManager");
            bx6.n(childFragmentManager);
        }
    }

    @DexIgnore
    @Override // com.fossil.Ft5
    public void close() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment
    public boolean e1() {
        FLogger.INSTANCE.getLocal().d(q, "onActivityBackPressed -");
        Et5 et5 = this.g;
        if (et5 != null) {
            et5.h();
            return true;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ft5
    public void j(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        Qw6<NotificationAppsFragmentBinding> qw6 = this.h;
        if (qw6 != null) {
            NotificationAppsFragmentBinding a = qw6.a();
            if (a != null && (flexibleSwitchCompat = a.y) != null) {
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        Wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.Ft5
    public void k(List<AppWrapper> list) {
        Wg6.b(list, "listAppWrapper");
        Wp5 wp5 = this.j;
        if (wp5 != null) {
            wp5.a(list);
        } else {
            Wg6.d("mNotificationAppsAdapter");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ft5
    public void l() {
        if (isActive()) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.z;
            Context requireContext = requireContext();
            Wg6.a((Object) requireContext, "requireContext()");
            throw null;
//            TroubleshootingActivity.a.a(aVar, requireContext, PortfolioApp.get.instance().c(), false, false, 12, null);
        }
    }

    @DexAdd
    public void loadCsv(View view) {
        try {
            File dir = Environment.getExternalStoragePublicDirectory("Fossil");
            // Ask for storage permissions if not already granted
            PermissionUtils.a.c(this.getActivity(), 255); // function with: "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (dir.exists()) {
                File enabledNotifications = new File(Environment.getExternalStoragePublicDirectory("Fossil"), "enabled_notifications.csv");
                if(enabledNotifications.exists()) {
                    FileInputStream inputStream = new FileInputStream(enabledNotifications);
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString).append("\n");
                    }
                    inputStream.close();
                    String contents = stringBuilder.toString();

                    List<String> enabled = Arrays.asList(contents.trim().split(","));

                    NotificationAppsPresenter pres = (NotificationAppsPresenter) this.g;
                    List<AppWrapper> apps = pres.g;
                    pres.k.b();  // function with "showProgressDialog"
                    for (AppWrapper a : apps) {
                        InstalledApp ia = a.getInstalledApp();
                        boolean z = enabled.contains(ia.identifier);
                        if (ia.isSelected() != z) {
                            pres.a(a, z);  // function with "setAppState: appName = "
                            pres.ListRestored = true;
                        }
                    }
                    onResume(); // This reloads the switches and gets rid of progress dialog
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @DexReplace
    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Wg6.b(layoutInflater, "inflater");
        NotificationAppsFragmentBinding notificationAppsFragmentBinding = (NotificationAppsFragmentBinding) Qb.a(layoutInflater, 2131558587, viewGroup, false, a1());
        notificationAppsFragmentBinding.v.setOnClickListener(new Ci(this));
        notificationAppsFragmentBinding.y.setOnCheckedChangeListener(new Di(this));
        notificationAppsFragmentBinding.r.addTextChangedListener(new Ei(this, notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.r.setOnFocusChangeListener(new Fi(notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.u.setOnClickListener(new Gi(notificationAppsFragmentBinding));
        notificationAppsFragmentBinding.buttonRestore.setOnClickListener(this::loadCsv);
                Wp5 wp5 = new Wp5();
        wp5.a(new Bi(this));
        this.j = wp5;
        Ou5 ou5 = (Ou5) getChildFragmentManager().b(Ou5.v.a());
        this.i = ou5;
        if (ou5 == null) {
            this.i = Ou5.v.b();
        }
        RecyclerView recyclerView = notificationAppsFragmentBinding.x;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        Wp5 wp52 = this.j;
        if (wp52 != null) {
            recyclerView.setAdapter(wp52);
            RecyclerView.j itemAnimator = recyclerView.getItemAnimator();
            if (itemAnimator != null) {
                ((Mg) itemAnimator).setSupportsChangeAnimations(false);
                recyclerView.setHasFixedSize(true);
                Iface iface = PortfolioApp.get.instance().getIface();
                Ou5 ou52 = this.i;
                if (ou52 != null) {
                    iface.a(new Qu5(ou52)).a(this);
                    this.h = new Qw6<>(this, notificationAppsFragmentBinding);
                    V("app_notification_view");
                    Wg6.a((Object) notificationAppsFragmentBinding, "binding");
                    return notificationAppsFragmentBinding.d();
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.settings.NotificationSettingsTypeContract.View");
            }
            throw new Rc6("null cannot be cast to non-null type androidx.recyclerview.widget.DefaultItemAnimator");
        }
        Wg6.d("mNotificationAppsAdapter");
        throw null;
    }

    @DexIgnore
    @Override // com.portfolio.platform.uirenew.BaseFragment, com.fossil.Ho5, androidx.fragment.app.Fragment
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        Z0();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        Et5 et5 = this.g;
        if (et5 != null) {
            et5.g();
            Jf5 c1 = c1();
            if (c1 != null) {
                c1.a("");
            }
            super.onPause();
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        Et5 et5 = this.g;
        if (et5 != null) {
            et5.f();
            Jf5 c1 = c1();
            if (c1 != null) {
                c1.d();
                return;
            }
            return;
        }
        Wg6.d("mPresenter");
        throw null;
    }
}
