package com.portfolio.platform.service.notification;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.graphics.drawable.LayerDrawable;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Fe7;
import com.fossil.Hc7;
import com.fossil.Jx3;
import com.fossil.Mx6;
import com.fossil.Mz3;
import com.fossil.N87;
import com.fossil.Nk5;
import com.fossil.O87;
import com.fossil.Oe7;
import com.fossil.Qj7;
import com.fossil.Xh7;
import com.fossil.Zb7;
import com.fossil.Zi7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.j256.ormlite.field.FieldType;
import com.mapped.An4;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Ff6;
import com.mapped.Gg6;
import com.mapped.Hf6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Mj6;
import com.mapped.Nc6;
import com.mapped.PermissionUtils;
import com.mapped.Qd6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.mapped.Xj6;
import com.mapped.Yj6;
import com.mapped.Zm4;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.AlelecPrefs;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.util.NotificationAppHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import org.joda.time.DateTimeConstants;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaNotificationComponent {
    @DexIgnore
    public static /* final */ HashMap<String, Ci> o; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, Ji> p;
    @DexIgnore
    public static /* final */ Ai q; // = new Ai(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Di b; // = new Di(this.k);
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, Long> c; // = new ConcurrentHashMap<>();
    @DexIgnore
    public /* final */ N87 d; // = O87.a(Oi.INSTANCE);
    @DexIgnore
    public /* final */ N87 e; // = O87.a(Pi.INSTANCE);
    @DexIgnore
    public int f; // = 1380;
    @DexIgnore
    public int g; // = 1140;
    @DexIgnore
    public Hi h;
    @DexIgnore
    public Fi i;
    @DexIgnore
    public /* final */ Queue<NotificationBaseObj> j; // = Mz3.a(Jx3.create(20));
    @DexIgnore
    public /* final */ An4 k;
    @DexIgnore
    public /* final */ DNDSettingsDatabase l;
    @DexIgnore
    public /* final */ QuickResponseRepository m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final HashMap<String, Ci> a() {
            if (DianaNotificationComponent.o.isEmpty()) {
                int i = 1;
                for (DianaNotificationObj.AApplicationName t : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = t.getPackageName();
                    if (!Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        DianaNotificationComponent.o.put(t.getPackageName(), new Ci(i, t, false, false));
                    } else {
                        DianaNotificationComponent.o.put(t.getPackageName(), new Ci(i, t, false, true));
                    }
                    i++;
                }
            }
            return DianaNotificationComponent.o;
        }
    }

    @DexIgnore
    public enum Bi {
        ACTIVE,
        SILENT,
        SKIP
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public Ci(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            Wg6.b(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ List<Ei> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.r();
        @DexIgnore
        public /* final */ An4 c;

        @DexIgnore
        public Di(An4 an4) {
            Wg6.b(an4, "mSharedPref");
            this.c = an4;
        }

        @DexIgnore
        public final int a() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        @DexIgnore
        public final Ei a(Ei ei) {
            Ei t;
            boolean z;
            synchronized (this) {
                Wg6.b(ei, "notificationStatus");
                if (this.a.contains(ei)) {
                    ei = null;
                } else if (Xj6.b(ei.c(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), true)) {
                    Iterator<Ei> it = this.a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        Ei next = it.next();
                        Ei t2 = next;
                        if (!Xj6.b(t2.g(), ei.g(), true) || (t2.i() != ei.i() && ei.i() - t2.i() > 1)) {
                            z = false;
//                            continue;
                        } else {
                            z = true;
//                            continue;
                        }
                        if (z) {
                            t = next;
                            break;
                        }
                    }
                    Ei t3 = t;
                    if (t3 != null) {
                        t3.c(ei.e());
                        t3.a(ei.i());
                        return null;
                    }
                    ei.a(a());
                    this.a.add(ei);
                    a(a() + 1);
                } else {
                    ei.a(a());
                    this.a.add(ei);
                    a(a() + 1);
                }
                return ei;
            }
        }

        @DexIgnore
        public final List<Ei> a(String str, String str2) {
            ArrayList arrayList;
            synchronized (this) {
                Wg6.b(str, "realId");
                Wg6.b(str2, "packageName");
                List<Ei> list = this.a;
                arrayList = new ArrayList();
                for (Ei t : list) {
                    Ei t2 = t;
                    if (Wg6.a((Object)t2.e(), (Object)str) && Wg6.a((Object)t2.c(), (Object)str2)) {
                        arrayList.add(t);
                    }
                }
                this.a.removeAll(arrayList);
            }
            return arrayList;
        }

        @DexIgnore
        public final void a(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.d(i);
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
    public static class Ei {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public long h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public int k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public boolean m;
        @DexAdd
        public boolean dnd = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii {
            @DexIgnore
            public Aii() {
            }

            @DexIgnore
            public /* synthetic */ Aii(Qg6 qg6) {
                this();
            }
        }

        /*
        static {
            new Aii(null);
        }
        */

        @DexIgnore
        public Ei() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
            this.g = "";
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        @DexEdit
        public Ei(StatusBarNotification statusBarNotification, @DexIgnore Void tag) {
            this();
            String obj;
            Bundle bundle;
            DianaNotificationObj.AApplicationName a2;
            Wg6.b(statusBarNotification, "sbn");
            this.a = a((long) statusBarNotification.getId(), statusBarNotification.getTag());
            String packageName = statusBarNotification.getPackageName();
            Wg6.a((Object) packageName, "sbn.packageName");
            this.c = packageName;
            Ci ci = DianaNotificationComponent.q.a().get(this.c);
            String d2 = (ci == null || (a2 = ci.a()) == null || (d2 = a2.getAppName()) == null) ? PortfolioApp.get.instance().d(this.c) : d2;
            CharSequence charSequence = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            String obj2 = (charSequence != null ? charSequence : d2).toString();
            this.d = obj2;
            d(obj2);
            CharSequence charSequence2 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            if (!(charSequence2 == null || Xj6.a(charSequence2))) {
                Notification notification = statusBarNotification.getNotification();
                obj = String.valueOf((notification == null || (bundle = notification.extras) == null) ? null : bundle.getCharSequence("android.bigText"));
            } else {
                String charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.text").toString();
                obj = (charSequence3 == null ? "" : charSequence3).toString();
            }
            e(obj);
            String str = statusBarNotification.getNotification().category;
            this.g = str == null ? "" : str;
            this.i = c(statusBarNotification);
            this.j = b(statusBarNotification);
            this.h = statusBarNotification.getNotification().when;
            this.k = statusBarNotification.getNotification().priority;
            this.l = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            if (a(statusBarNotification)) {
                this.m = true;
                CharSequence charSequence4 = statusBarNotification.getNotification().tickerText;
                if (charSequence4 != null) {
                    int length = charSequence4.length();
                    int i2 = 0;
                    while (true) {
                        if (i2 >= length) {
                            i2 = -1;
                            break;
                        } else if (Wg6.a((Object) String.valueOf(charSequence4.charAt(i2)), (Object) ":")) {
                            break;
                        } else {
                            i2++;
                        }
                    }
                    if (i2 != -1) {
                        String obj3 = charSequence4.subSequence(0, i2).toString();
                        if (obj3 != null) {
                            d(Yj6.d((CharSequence) obj3).toString());
                            String obj4 = charSequence4.subSequence(i2 + 1, charSequence4.length()).toString();
                            if (obj4 != null) {
                                e(Yj6.d((CharSequence) obj4).toString());
                                return;
                            }
                            throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                        }
                        throw new Rc6("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
            }
        }

        @DexAdd
        public class AlelecSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
                Context ctx = PortfolioApp.get.instance().getApplicationContext();
                dndSettingEnabled = AlelecPrefs.getAndroidDND(ctx);
                dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(ctx);
                emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(ctx);
            }
        }

        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public AlelecSettingsListener dndSettingsListener = null;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public Boolean dndSettingEnabled = false;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public static Boolean dndHighSettingEnabled = false;
        @DexAdd
        @SuppressWarnings("WeakerAccess")
        public Boolean emptyNotificationsEnabled = false;

        @DexAdd
        public Ei(StatusBarNotification statusBarNotification) {
            this(statusBarNotification, (Void) null);
            this.dnd = checkDND(statusBarNotification.getNotification());

            // Grab icon if needed

            String packageName = this.c;

            Context context = PortfolioApp.get.instance().getApplicationContext();
            File iconFile = new File(context.getExternalFilesDir("appIcons"), packageName + ".icon");

            synchronized (fileLock) {
                if (!iconFile.exists()) {
                    try {
                        PackageManager manager = context.getPackageManager();
                        Resources resources = manager.getResourcesForApplication(packageName);
                        // Icon small = statusBarNotification.getNotification().getSmallIcon();
                        Drawable drawable;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            try {
                                drawable = statusBarNotification.getNotification().getSmallIcon().loadDrawable(context);
                            } catch (Exception unused) {
                                drawable = statusBarNotification.getNotification().getLargeIcon().loadDrawable(context);
                            }
                        } else {
                            drawable = resources.getDrawable(statusBarNotification.getNotification().icon);
                        }

                        int notifIconWidth = 24;
                        int notifIconHeight = 24;

                        byte[] notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0, false);
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xff000000, false);
                        }
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xffffffff, true);
                        }
                        if (notifIcon != null) {

                            // Convert to RLE with header and footer
                            byte[] rle = rle_encode(notifIcon);
                            notifIcon = new byte[rle.length + 4];
                            notifIcon[0] = (byte) notifIconWidth;
                            notifIcon[1] = (byte) notifIconHeight;
                            notifIcon[notifIcon.length - 1] = (byte) 0xff;
                            notifIcon[notifIcon.length - 2] = (byte) 0xff;
                            System.arraycopy(rle, 0, notifIcon, 2, rle.length);

                            // Write to file for next time

                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(iconFile));
                            bos.write(notifIcon);
                            bos.flush();
                            bos.close();
                        }

                        // Send Filters to watch
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        List<AppWrapper> allApps = (List<AppWrapper>) notificationAppHelper.a(context);
                        List<AppWrapper> apps = new ArrayList<>();
                        for (AppWrapper a : allApps) {
                            InstalledApp installedApp2 = a.getInstalledApp();
                            if (installedApp2 != null) {
                                Boolean isSelected = installedApp2.isSelected();
                                // wd4.a(isSelected, "it.isSelected");
                                if (isSelected) {
                                    apps.add(a);
                                }
                            }
                        }

                        List<AppNotificationFilter> j = new ArrayList<>();
                        // From NotificationAppsPresenter, search for "setRuleToDevice, showProgressDialog", about 9 lines below.
                        //noinspection CollectionAddAllCanBeReplacedWithConstructor
                        j.addAll(Mx6.a(apps, false));
                        List<AppNotificationFilter> phone_sms_filters = notificationAppHelper.a(0, 0);
                        j.addAll(phone_sms_filters);
                        // And from the line below the previous one in NotificationAppsPresenter...
                        PortfolioApp.get.instance().b(new AppNotificationFilterSettings(j, System.currentTimeMillis()), PortfolioApp.get.instance().c());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        @DexAdd
        private boolean checkDND(Notification notification) {
            // Return true if the notification should be hidden for DND
            String category = notification.category;
            NotificationManager notificationManager = (NotificationManager)PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int filter = notificationManager.getCurrentInterruptionFilter();

                FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: filter = " + notificationManager.getCurrentInterruptionFilter());
                FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: category = " + category);
                switch (filter) {
                    case NotificationManager.INTERRUPTION_FILTER_NONE:
                        return true;

                    case NotificationManager.INTERRUPTION_FILTER_ALARMS:
                        if (!TextUtils.equals(Notification.CATEGORY_ALARM, category)) {
                            return true;
                        }
                        break;

                    case NotificationManager.INTERRUPTION_FILTER_PRIORITY:
                        String[] activeCategories = NotificationManager.Policy.priorityCategoriesToString(notificationManager.getNotificationPolicy().priorityCategories).split(",");
                        StringBuilder log = new StringBuilder("checkDND: activeCategories = ");
                        boolean ret = true;
                        for (String active : activeCategories) {
                            log.append(active).append(",");
                            if (TextUtils.equals(active, category)) {
                                ret = false;
                            }
                        }
                        FLogger.INSTANCE.getLocal().d("DianaNotificationComponent",log.toString());

                        if (category == null && notification.priority >= Notification.PRIORITY_HIGH) {
                            FLogger.INSTANCE.getLocal().d("DianaNotificationComponent", "checkDND: priority = " + notification.priority + "dndHighSettingEnabled: " + dndHighSettingEnabled);
                            if (dndHighSettingEnabled) {
                                ret = false;
                            }
                        }

                        return ret;

                    case NotificationManager.INTERRUPTION_FILTER_UNKNOWN:
                    case NotificationManager.INTERRUPTION_FILTER_ALL:
                    default:
                        return false;
                }
            }
            return false;
        }

        @DexAdd
        public byte[] drawableToPixels(Drawable drawable, int width, int height, int background, boolean invert) {

            Bitmap bitmap = null;
            // Draw icon onto 24 x 24 bitmap

            // if (drawable instanceof BitmapDrawable) {
            //     bitmap = ((BitmapDrawable) drawable).getBitmap();
            // } else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (drawable instanceof AdaptiveIconDrawable) {
                    Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                    Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                    Drawable[] drr = new Drawable[2];
                    drr[0] = backgroundDr;
                    drr[1] = foregroundDr;

                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    LayerDrawable layerDrawable = new LayerDrawable(drr);
                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);
                }
            }
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
            }
            if (bitmap != null) {

                // Convert to greyscale
                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bmpGrayscale);

                if ((background & 0xFF000000) != 0) {
                    Paint paint = new Paint();
                    paint.setColor(background);
                    c.drawRect(0F, 0F, (float) width, (float) height, paint);
                }

                Paint paint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                paint.setColorFilter(f);

                c.drawBitmap(bitmap, 0, 0, paint);
                // scaled.recycle();

                // Downsample to 2 bit image
                int[] pixels = new int[width * height];
                bmpGrayscale.getPixels(pixels, 0, width, 0, 0, width, height);
                byte[] b_pixels = new byte[pixels.length];

                int n_light_pix = 0;
                for (int i = 0; i < pixels.length; i++) {
                    int pix = pixels[i];
                    //todo invert
                    b_pixels[i] = (byte) (pix >> 6 & 0x03);
                    if (invert) {
                        b_pixels[i] = (byte)(3 - b_pixels[i]);
                    }
                    if (b_pixels[i] > 0) {
                        n_light_pix++;
                    }
                }
                bitmap.recycle();
                bmpGrayscale.recycle();
                if ((100.0 * n_light_pix / pixels.length) < 5) {
                    // under 5% pixels visible, need to invert the background and try again
                    return null;
                }
                return b_pixels;
            }
            return null;
        }

        @DexAdd
        public static byte[] rle_encode(byte[] chars) {
            try {
                if (chars == null || chars.length == 0) return null;

                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                Writer writer = new OutputStreamWriter(bout);

                // StringBuilder builder = new StringBuilder();
                byte current = chars[0];
                int count = 1;

                for (int i = 1; i < chars.length; i++) {
                    if (current != chars[i]) {
                        if (count > 0) {
                            writer.write(count);
                            writer.write(current);
                        }
                        current = chars[i];
                        count = 1;
                    } else {
                        count++;
                        if (count == 254) {
                            writer.write(count);
                            writer.write(current);
                            count = 0;
                        }
                    }
                }
                if (count > 1) writer.write(count);
                writer.write(current);
                writer.flush();
                return bout.toByteArray();
            } catch (IOException unused) {
                return null;
            }
        }

        @DexAdd
        private static final Object fileLock = new Object();

        @DexIgnore
        public final String a() {
            return this.g;
        }

        @DexIgnore
        public final String a(long j2, String str) {
            return j2 + ':' + str;
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new Mj6("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            boolean z = false;
            int i2 = 0;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void a(long j2) {
            this.h = j2;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.l = z;
        }

        @DexIgnore
        public final boolean a(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!Yj6.a((CharSequence) tag, (CharSequence) "sms", true) && !Yj6.a((CharSequence) tag, (CharSequence) "mms", true) && !Yj6.a((CharSequence) tag, (CharSequence) "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }

        @DexIgnore
        public final void b(String str) {
            Wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.j = z;
        }

        @DexIgnore
        public final boolean b(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final void c(String str) {
            Wg6.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final boolean c(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexIgnore
        public final int d() {
            return this.k;
        }

        @DexIgnore
        public final void d(String str) {
            Wg6.b(str, "value");
            this.e = a(str);
        }

        @DexIgnore
        public final String e() {
            return this.a;
        }

        @DexIgnore
        public final void e(String str) {
            Wg6.b(str, "value");
            this.f = a(str);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof Ei)) {
                return false;
            }
            Ei ei = (Ei) obj;
            return Wg6.a((Object)this.a, (Object)ei.a) && this.h == ei.h;
        }

        @DexIgnore
        public final String f() {
            return this.e;
        }

        @DexIgnore
        public final void f(String str) {
            Wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String g() {
            return this.f;
        }

        @DexIgnore
        public final String h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final long i() {
            return this.h;
        }

        @DexIgnore
        public final boolean j() {
            return this.l;
        }

        @DexIgnore
        public final boolean k() {
            return this.m;
        }

        @DexIgnore
        public final boolean l() {
            return this.j;
        }

        @DexIgnore
        public final boolean m() {
            return this.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Fi extends ContentObserver {
        @DexIgnore
        public Bii a; // = new Bii(this, 0, null, 0, 0, 15, null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public /*static*/ final class Aii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
//                return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    Fi fi = this.this$0;
                    Bii b = fi.a();
                    if (b == null) {
                        b = new Bii(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    fi.a = b;
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class Bii {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public Bii(Fi fi, long j, String str, int i, long j2) {
                Wg6.b(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ Bii(Fi fi, long j, String str, int i, long j2, int i2, Qg6 qg6) {
                this(fi, (i2 & 1) != 0 ? -1 : j, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public /*static*/ final class Cii extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.b(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
//                return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
//                return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Ob7
            public final Object invokeSuspend(Object obj) {
                Ff6.a();
                if (this.label == 0) {
                    Nc6.a(obj);
                    Bii b = this.this$0.a();
                    if (b != null) {
                        Bii a = this.this$0.a;
                        if (a != null) {
                            if (a.a() < b.a()) {
                                if (b.c() == 3) {
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String d = DianaNotificationComponent.this.d();
                                    local.d(d, "processMissedCall - number = " + b.b());
                                    DianaNotificationComponent.this.a(b.b(), new Date(), Gi.MISSED);
                                } else if (b.c() == 5 || b.c() == 6 || b.c() == 1) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String d2 = DianaNotificationComponent.this.d();
                                    local2.d(d2, "processHangUpCall - number = " + b.b());
                                    DianaNotificationComponent.this.a(b.b(), new Date(), Gi.PICKED);
                                }
                            }
                            this.this$0.a = b;
                        } else {
                            Wg6.a();
                            throw null;
                        }
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi() {
            super(null);
            PermissionUtils.Ai ai = PermissionUtils.a;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            if (ai.d(applicationContext)) {
                throw null;
//                Rm6 unused = Xh7.b(Zi7.a(Qj7.a()), null, null, new Aii(this, null), 3, null);
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:14:0x007c, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x007d, code lost:
            com.fossil.Hc7.a(r9, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0080, code lost:
            throw r1;
         */
        @DexIgnore
        @SuppressLint({"MissingPermission"})
        public final Bii a() {
            try {
                Cursor query = PortfolioApp.get.instance().getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "number", "date", "type"}, null, null, "date DESC LIMIT 1");
                if (query != null) {
                    if (query.moveToFirst()) {
                        long j = query.getLong(query.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                        String string = query.getString(query.getColumnIndex("number"));
                        int i = query.getInt(query.getColumnIndex("type"));
                        long j2 = query.getLong(query.getColumnIndex("date"));
                        query.close();
                        Wg6.a((Object) string, "number");
                        Bii bii = new Bii(this, j, string, i, j2);
                        Hc7.a(query, null);
                        return bii;
                    }
                    query.close();
                    Cd6 cd6 = Cd6.a;
                    Hc7.a(query, null);
                }
                return null;
            } catch (Exception e) {
                e.printStackTrace();
                FLogger.INSTANCE.getLocal().e(DianaNotificationComponent.this.d(), e.getMessage());
                return null;
            }
        }

        @DexIgnore
        public final void b() {
            PermissionUtils.Ai ai = PermissionUtils.a;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            if (!ai.d(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(DianaNotificationComponent.this.d(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                throw null;
//                Rm6 unused = Xh7.b(Zi7.a(Qj7.a()), null, null, new Cii(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            b();
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            b();
        }
    }

    @DexIgnore
    public enum Gi {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi extends Ei {
        @DexIgnore
        public String n;
        @DexIgnore
        public boolean o;

        @DexIgnore
        public Hi() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Hi(String str, String str2, String str3, String str4, Date date) {
            this();
            Wg6.b(str, "packageName");
            Wg6.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            Wg6.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            Wg6.b(str4, "message");
            Wg6.b(date, GoalPhase.COLUMN_START_DATE);
            c(a(date.getTime(), null));
            b(str);
            f(str2);
            d(str2);
            e(str4);
            a(date.getTime());
            this.n = str3;
        }

        @DexIgnore
        public final Hi clone() {
            Hi hi = new Hi();
            hi.c(e());
            hi.a(b());
            hi.b(c());
            hi.d(f());
            hi.e(g());
            hi.f(h());
            hi.a(i());
            hi.a(j());
            hi.c(m());
            hi.b(l());
            hi.n = this.n;
            hi.o = this.o;
            return hi;
        }

        @DexIgnore
        public final void d(boolean z) {
            this.o = z;
        }

        @DexIgnore
        public final boolean n() {
            return this.o;
        }

        @DexIgnore
        public final String o() {
            return this.n;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends Ei {
        @DexIgnore
        public String n;

        @DexIgnore
        public Ii() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Ii(String str, String str2, String str3, long j) {
            this();
            Wg6.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            Wg6.b(str2, "message");
            Wg6.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            c(a(j, null));
            b(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            f(str);
            d(str);
            e(str2);
            this.n = str3;
            a(j);
        }

        @DexIgnore
        public final String n() {
            return this.n;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji {
        @DexIgnore
        public String a;

        @DexIgnore
        public Ji() {
            this.a = "";
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public Ji(String str, String str2) {
            this();
            Wg6.b(str, "packageName");
            Wg6.b(str2, "tag");
            this.a = str2;
        }

        @DexIgnore
        public final boolean a(String str) {
            Wg6.b(str, "realId");
            return Yj6.a((CharSequence) str, (CharSequence) this.a, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {455}, m = "checkConditionsToSendToDevice")
    public static final class Ki extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a(null, null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {386, 391, 398}, m = "handleNewLogic")
    public static final class Li extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.a((Ei) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {334}, m = "handleNotificationAdded")
    public static final class Mi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.b((Ei) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$initializeInCalls$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Ni extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(DianaNotificationComponent dianaNotificationComponent, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ni ni = new Ni(this.this$0, xe6);
            ni.p$ = (Il6) obj;
            throw null;
//            return ni;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ni) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:16:0x009b, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x009c, code lost:
            com.fossil.Hc7.a(r1, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x009f, code lost:
            throw r2;
         */
        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                this.this$0.c.clear();
                PermissionUtils.Ai ai = PermissionUtils.a;
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                if (!ai.d(applicationContext)) {
                    return Cd6.a;
                }
                long currentTimeMillis = System.currentTimeMillis();
                try {
                    Cursor query = PortfolioApp.get.instance().getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{"number", "date", "type"}, "date >= ? AND type IN(?,?)", new String[]{String.valueOf(currentTimeMillis - 900000), "1", "3"}, "date DESC");
                    if (query != null) {
                        while (query.moveToNext()) {
                            this.this$0.c.putIfAbsent(query.getString(query.getColumnIndex("number")), Hf6.a(query.getLong(query.getColumnIndex("date"))));
                        }
                        query.close();
                        Cd6 cd6 = Cd6.a;
                        Hc7.a(query, null);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    FLogger.INSTANCE.getLocal().e(this.this$0.d(), e.getMessage());
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

//    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//    public static final class Oi extends Fe7 implements Gg6<AudioManager> {
//        @DexIgnore
//        public static /* final */ Oi INSTANCE; // = new Oi();
//
//        @DexIgnore
//        public Oi() {
//            super(0);
//        }
//
//        @DexIgnore
//        @Override // com.mapped.Gg6
//        public final AudioManager invoke() {
//            Object systemService = PortfolioApp.get.instance().getSystemService("audio");
//            if (systemService != null) {
//                return (AudioManager) systemService;
//            }
//            throw new Rc6("null cannot be cast to non-null type android.media.AudioManager");
//        }
//    }

//    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
//    public static final class Pi extends Fe7 implements Gg6<NotificationManager> {
//        @DexIgnore
//        public static /* final */ Pi INSTANCE; // = new Pi();
//
//        @DexIgnore
//        public Pi() {
//            super(0);
//        }
//
//        @DexIgnore
//        @Override // com.mapped.Gg6
//        public final NotificationManager invoke() {
//            Object systemService = PortfolioApp.get.instance().getSystemService("notification");
//            if (systemService != null) {
//                return (NotificationManager) systemService;
//            }
//            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
//        }
//    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Qi extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Qi(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Qi qi = new Qi(this.this$0, this.$sbn, xe6);
            qi.p$ = (Il6) obj;
            throw null;
//            return qi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Qi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Ff6.a();
            if (this.label == 0) {
                Nc6.a(obj);
                Ei ei = new Ei(this.$sbn);
                Ci ci = DianaNotificationComponent.q.a().get(ei.c());
                if (ci != null) {
                    ei.d(ci.a().getAppName());
                }
                this.this$0.c(new Ei(this.$sbn));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$1", f = "DianaNotificationComponent.kt", l = {193}, m = "invokeSuspend")
    public static final class Ri extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ri(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ri ri = new Ri(this.this$0, this.$sbn, xe6);
            ri.p$ = (Il6) obj;
            throw null;
//            return ri;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ri) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                Ei ei = new Ei(this.$sbn);
                String str = this.$sbn.getNotification().category;
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                this.L$0 = il6;
                this.L$1 = ei;
                this.label = 1;
                if (dianaNotificationComponent.a(ei, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                Ei ei2 = (Ei) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", f = "DianaNotificationComponent.kt", l = {143, 169}, m = "invokeSuspend")
    public static final class Si extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startTime;
        @DexIgnore
        public /* final */ /* synthetic */ Gi $state;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Si(DianaNotificationComponent dianaNotificationComponent, Gi gi, String str, Date date, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$state = gi;
            this.$phoneNumber = str;
            this.$startTime = date;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Si si = new Si(this.this$0, this.$state, this.$phoneNumber, this.$startTime, xe6);
            si.p$ = (Il6) obj;
            throw null;
//            return si;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Si) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = this.this$0.d();
                local.d(d, "process phone call state " + this.$state + " phoneNumber " + this.$phoneNumber);
                int i2 = Nk5.a[this.$state.ordinal()];
                if (i2 == 1) {
                    String a2 = this.this$0.d(this.$phoneNumber);
                    Hi hi = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), a2, this.$phoneNumber, "Incoming Call", this.$startTime);
                    this.this$0.a(this.$startTime.getTime());
                    hi.d(this.this$0.c.containsKey(this.$phoneNumber));
                    this.this$0.c.put(this.$phoneNumber, Hf6.a(this.$startTime.getTime()));
                    if (!this.this$0.a(this.$phoneNumber, true)) {
                        return Cd6.a;
                    }
                    DianaNotificationComponent dianaNotificationComponent = this.this$0;
                    Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent.d(applicationContext);
                    DianaNotificationComponent dianaNotificationComponent2 = this.this$0;
                    Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent2.c(applicationContext2);
                    Hi b = this.this$0.h;
                    if (b != null) {
                        this.this$0.c(b);
                    }
                    this.this$0.h = hi.clone();
                    DianaNotificationComponent dianaNotificationComponent3 = this.this$0;
                    this.L$0 = il6;
                    this.L$1 = a2;
                    this.L$2 = hi;
                    this.label = 1;
                    if (dianaNotificationComponent3.b(hi, this) == a) {
                        return a;
                    }
                } else if (i2 == 2) {
                    Hi b2 = this.this$0.h;
                    if (b2 != null && Wg6.a((Object) b2.o(), (Object) this.$phoneNumber)) {
                        Hi hi2 = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hi2.c(b2.e());
                        hi2.a(b2.b());
                        this.this$0.c(hi2);
                        this.this$0.h = (Hi) null;
                    }
                    DianaNotificationComponent dianaNotificationComponent4 = this.this$0;
                    Context applicationContext3 = PortfolioApp.get.instance().getApplicationContext();
                    Wg6.a((Object) applicationContext3, "PortfolioApp.instance.applicationContext");
                    dianaNotificationComponent4.d(applicationContext3);
                } else if (i2 == 3) {
                    if (!this.this$0.a(this.$phoneNumber, true)) {
                        return Cd6.a;
                    }
                    Hi b3 = this.this$0.h;
                    if (b3 != null && Wg6.a((Object) b3.o(), (Object) this.$phoneNumber)) {
                        Hi hi3 = new Hi(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
                        hi3.a(b3.b());
                        hi3.c(b3.e());
                        this.this$0.c(hi3);
                        this.this$0.h = (Hi) null;
                    }
                    String a3 = this.this$0.d(this.$phoneNumber);
                    DianaNotificationComponent dianaNotificationComponent5 = this.this$0;
                    String packageName = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName();
                    String str = this.$phoneNumber;
                    Calendar instance = Calendar.getInstance();
                    Wg6.a((Object) instance, "Calendar.getInstance()");
                    Date time = instance.getTime();
                    Wg6.a((Object) time, "Calendar.getInstance().time");
                    Hi hi4 = new Hi(packageName, a3, str, "Missed Call", time);
                    this.L$0 = il6;
                    this.L$1 = a3;
                    this.label = 2;
                    if (dianaNotificationComponent5.b(hi4, this) == a) {
                        return a;
                    }
                }
                return Cd6.a;
            } else if (i == 1) {
                Hi hi5 = (Hi) this.L$2;
                String str2 = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
                return Cd6.a;
            } else if (i == 2) {
                String str3 = (String) this.L$1;
                Il6 il63 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DianaNotificationComponent dianaNotificationComponent6 = this.this$0;
            Context applicationContext4 = PortfolioApp.get.instance().getApplicationContext();
            Wg6.a((Object) applicationContext4, "PortfolioApp.instance.applicationContext");
            dianaNotificationComponent6.d(applicationContext4);
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", f = "DianaNotificationComponent.kt", l = {182}, m = "invokeSuspend")
    public static final class Ti extends Zb7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $content;
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ long $receivedTime;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ti(DianaNotificationComponent dianaNotificationComponent, String str, String str2, long j, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$phoneNumber = str;
            this.$content = str2;
            this.$receivedTime = j;
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.b(xe6, "completion");
            Ti ti = new Ti(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, xe6);
            ti.p$ = (Il6) obj;
            throw null;
//            return ti;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
//            return ((Ti) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Ob7
        public final Object invokeSuspend(Object obj) {
            Object a = Ff6.a();
            int i = this.label;
            if (i == 0) {
                Nc6.a(obj);
                Il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(this.this$0.d(), "Process SMS/MMS by old solution");
                if (!this.this$0.a(this.$phoneNumber, false)) {
                    return Cd6.a;
                }
                String a2 = this.this$0.d(this.$phoneNumber);
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                Ii ii = new Ii(a2, this.$content, this.$phoneNumber, this.$receivedTime);
                this.L$0 = il6;
                this.L$1 = a2;
                this.label = 1;
                if (dianaNotificationComponent.b(ii, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                Nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        HashMap<String, Ji> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new Ji("com.facebook.orca", "SMS"));
        p = hashMap;
    }
    */

    @DexIgnore
    public DianaNotificationComponent(An4 an4, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        Wg6.b(an4, "mSharedPreferencesManager");
        Wg6.b(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        Wg6.b(quickResponseRepository, "mQuickResponseRepository");
        Wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = an4;
        this.l = dNDSettingsDatabase;
        this.m = quickResponseRepository;
        this.n = notificationSettingsDatabase;
        String simpleName = DianaNotificationComponent.class.getSimpleName();
        Wg6.a((Object) simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
        q.a();
    }

    @DexIgnore
    public final Rm6 a(StatusBarNotification statusBarNotification) {
        Wg6.b(statusBarNotification, "sbn");
        throw null;
//        return Xh7.b(Zi7.a(Qj7.a()), null, null, new Qi(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final Rm6 a(String str, String str2, long j2) {
        Wg6.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        Wg6.b(str2, "content");
        throw null;
//        return Xh7.b(Zi7.a(Qj7.a()), null, null, new Ti(this, str, str2, j2, null), 3, null);
    }

    @DexIgnore
    public final Rm6 a(String str, Date date, Gi gi) {
        Wg6.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        Wg6.b(date, SampleRaw.COLUMN_START_TIME);
        Wg6.b(gi, "state");
        throw null;
//        return Xh7.b(Zi7.a(Qj7.a()), null, null, new Si(this, gi, str, date, null), 3, null);
    }

    @DexIgnore
    public final Bi a(Ei ei, Ci ci) {
        FLogger.INSTANCE.getLocal().d(this.a, "applyDND()");
        if (Build.VERSION.SDK_INT >= 23) {
            Object systemService = PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
            if (systemService != null) {
                NotificationManager notificationManager = (NotificationManager) systemService;
                int currentInterruptionFilter = notificationManager.getCurrentInterruptionFilter();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.a;
                local.d(str, "currentInterruptionFilter = " + currentInterruptionFilter);
                if (currentInterruptionFilter != 2) {
                    if (currentInterruptionFilter == 3) {
                        return Bi.SILENT;
                    }
                    if (currentInterruptionFilter == 4) {
                        return (g() || !Wg6.a((Object)ei.a(), (Object)Alarm.TABLE_NAME)) ? Bi.SILENT : Bi.ACTIVE;
                    }
                    if (Wg6.a((Object) ci.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a((Object) ci.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                        return j() ? Bi.SILENT : Bi.ACTIVE;
                    }
                    if (Wg6.a((Object) ei.a(), (Object) Alarm.TABLE_NAME)) {
                        return g() ? Bi.SILENT : Bi.ACTIVE;
                    }
                } else if (Wg6.a((Object) ci.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a((Object) ci.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                    return ei instanceof Hi ? ((notificationManager.getNotificationPolicy().priorityCategories & 16) == 0 || !((Hi) ei).n()) ? !a(ei) ? !j() ? Bi.ACTIVE : Bi.SILENT : Bi.SILENT : !j() ? Bi.ACTIVE : Bi.SILENT : a(ei) ? Bi.SILENT : Bi.ACTIVE;
                } else {
                    if (Wg6.a((Object) ci.a().getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName()) || ei.k()) {
                        return !b(ei) ? !i() ? Bi.ACTIVE : Bi.SILENT : Bi.SILENT;
                    }
                    if (Wg6.a((Object) ei.a(), (Object) Alarm.TABLE_NAME)) {
                        return (f() || g()) ? Bi.SILENT : Bi.ACTIVE;
                    }
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
            }
        }
        return i() ? Bi.SILENT : Bi.ACTIVE;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01cf  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final /* synthetic */ Object a(Ei ei, Xe6<? super Cd6> xe6) {
        throw null;
//        Li li;
//        int i2;
//        boolean z;
//        T t;
//        Ei ei2;
//        DianaNotificationComponent dianaNotificationComponent;
//        T t2;
//        DianaNotificationObj.AApplicationName a2;
//        if (xe6 instanceof Li) {
//            Li li2 = (Li) xe6;
//            int i3 = li2.label;
//            if ((Integer.MIN_VALUE & i3) != 0) {
//                li2.label = i3 + RecyclerView.UNDEFINED_DURATION;
//                li = li2;
//                Object obj = li.result;
//                Object a3 = Ff6.a();
//                i2 = li.label;
//                if (i2 != 0) {
//                    Nc6.a(obj);
//                    FLogger.INSTANCE.getLocal().d(this.a, "handleNewLogic " + ei);
//                    Jh6 jh6 = new Jh6();
//                    Oe7 oe7 = new Oe7();
//                    oe7.element = true;
//                    if (Wg6.a((Object) ei.c(), (Object) Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()))) {
//                        Ji ji = p.get(ei.c());
//                        if (ji == null || ji.a(ei.e())) {
//                            jh6.element = (T) q.a().get(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
//                            z = true;
//                            if (jh6.element == null) {
//                                List<AppFilter> allAppFilters = Zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
//                                Wg6.a((Object) allAppFilters, "appNotificationFilters");
//                                Iterator<T> it = allAppFilters.iterator();
//                                while (true) {
//                                    if (!it.hasNext()) {
//                                        t2 = null;
//                                        break;
//                                    }
//                                    T next = it.next();
//                                    T t3 = next;
//                                    Wg6.a((Object) t3, "it");
//                                    if (Hf6.a(Wg6.a((Object) t3.getType(), (Object) ei.c())).booleanValue()) {
//                                        t2 = next;
//                                        break;
//                                    }
//                                }
//                                if (t2 == null) {
//                                    FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
//                                    T t4 = jh6.element;
//                                    a(ei, (t4 == null || (a2 = t4.a()) == null) ? null : a2.getNotificationType());
//                                    return Cd6.a;
//                                }
//                                FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but no supported icon");
//                                oe7.element = false;
//                                jh6.element = (T) new Ci(17, new DianaNotificationObj.AApplicationName(PortfolioApp.get.instance().d(ei.c()), ei.c(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
//                            }
//                            t = jh6.element;
//                            if (t != null) {
//                                if (Wg6.a(t.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL()) || Wg6.a(t.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL())) {
//                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() incoming call or miss called not supported............");
//                                    return Cd6.a;
//                                }
//                                if (z && b(ei, (Ci) jh6.element) && c(ei.f())) {
//                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported - handle for sms - new solution");
//                                    boolean z2 = oe7.element;
//                                    li.L$0 = this;
//                                    li.L$1 = ei;
//                                    li.L$2 = jh6;
//                                    li.L$3 = oe7;
//                                    li.L$4 = t;
//                                    li.label = 1;
//                                    if (a(ei, jh6.element, z2, li) == a3) {
//                                        return a3;
//                                    }
//                                } else if (z || ((!ei.l() && ei.m()) || !b(ei, (Ci) jh6.element) || ei.j())) {
//                                    if (Wg6.a((Object) ei.c(), (Object) "com.google.android.googlequicksearchbox")) {
//                                        boolean z3 = oe7.element;
//                                        li.L$0 = this;
//                                        li.L$1 = ei;
//                                        li.L$2 = jh6;
//                                        li.L$3 = oe7;
//                                        li.L$4 = t;
//                                        li.label = 3;
//                                        if (a(ei, jh6.element, z3, li) == a3) {
//                                            return a3;
//                                        }
//                                    }
//                                    ei2 = ei;
//                                    dianaNotificationComponent = this;
//                                } else {
//                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
//                                    boolean z4 = oe7.element;
//                                    li.L$0 = this;
//                                    li.L$1 = ei;
//                                    li.L$2 = jh6;
//                                    li.L$3 = oe7;
//                                    li.L$4 = t;
//                                    li.label = 2;
//                                    if (a(ei, jh6.element, z4, li) == a3) {
//                                        return a3;
//                                    }
//                                }
//                                ei2 = ei;
//                                dianaNotificationComponent = this;
//                                dianaNotificationComponent.a(ei2, t.a().getNotificationType());
//                            }
//                            return Cd6.a;
//                        }
//                        jh6.element = (T) q.a().get(ei.c());
//                    } else {
//                        jh6.element = (T) q.a().get(ei.c());
//                    }
//                    z = false;
//                    if (jh6.element == null) {
//                    }
//                    t = jh6.element;
//                    if (t != null) {
//                    }
//                    return Cd6.a;
//                } else if (i2 == 1 || i2 == 2) {
//                    t = (Ci) li.L$4;
//                    Oe7 oe72 = (Oe7) li.L$3;
//                    Jh6 jh62 = (Jh6) li.L$2;
//                    ei2 = (Ei) li.L$1;
//                    dianaNotificationComponent = (DianaNotificationComponent) li.L$0;
//                    Nc6.a(obj);
//                    dianaNotificationComponent.a(ei2, t.a().getNotificationType());
//                    return Cd6.a;
//                } else if (i2 == 3) {
//                    t = (Ci) li.L$4;
//                    Oe7 oe73 = (Oe7) li.L$3;
//                    Jh6 jh63 = (Jh6) li.L$2;
//                    ei2 = (Ei) li.L$1;
//                    dianaNotificationComponent = (DianaNotificationComponent) li.L$0;
//                    Nc6.a(obj);
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "processNotificationAdd() app not supported............");
//                dianaNotificationComponent.a(ei2, t.a().getNotificationType());
//                return Cd6.a;
//            }
//        }
//        li = new Li(this, xe6);
//        Object obj2 = li.result;
//        Object a32 = Ff6.a();
//        i2 = li.label;
//        if (i2 != 0) {
//        }
//        FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "processNotificationAdd() app not supported............");
//        dianaNotificationComponent.a(ei2, t.a().getNotificationType());
//        return Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    public final /* synthetic */ Object a(Ei ei, Ci ci, boolean z, Xe6<? super Cd6> xe6) {
        Ki ki;
        int i2;
        if (xe6 instanceof Ki) {
            Ki ki2 = (Ki) xe6;
            int i3 = ki2.label;
            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
                ki2.label = i3 + RecyclerView.UNDEFINED_DURATION;
                ki = ki2;
                Object obj = ki.result;
                Object a2 = Ff6.a();
                i2 = ki.label;
                if (i2 != 0) {
                    Nc6.a(obj);
                    Ei a3 = this.b.a(ei);
                    if (a3 == null) {
                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() - this notification existed.");
                    } else if (h() || (Xj6.a((CharSequence) a3.f()) && Xj6.a((CharSequence) a3.g()))) {
                        return Cd6.a;
                    } else {
                        ki.L$0 = this;
                        ki.L$1 = ei;
                        ki.L$2 = ci;
                        ki.Z$0 = z;
                        ki.L$3 = a3;
                        ki.label = 1;
                        throw null;
//                        if (b(a3, ci, z, ki) == a2) {
//                            return a2;
//                        }
                    }
                } else if (i2 == 1) {
                    Ei ei2 = (Ei) ki.L$3;
                    boolean z2 = ki.Z$0;
                    Ci ci2 = (Ci) ki.L$2;
                    Ei ei3 = (Ei) ki.L$1;
                    DianaNotificationComponent dianaNotificationComponent = (DianaNotificationComponent) ki.L$0;
                    Nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }
        ki = new Ki(this, xe6);
        Object obj2 = ki.result;
        Object a22 = Ff6.a();
        i2 = ki.label;
        if (i2 != 0) {
        }
        return Cd6.a;
    }

    @DexIgnore
    public final String a() {
        return PortfolioApp.get.instance().c();
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x006a, code lost:
        if (r0 != null) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x006c, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006f, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0070, code lost:
        if (r0 != null) goto L_0x006c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0078  */
    public final List<Lc6<String, String>> a(Context context) {
        Cursor cursor;
        Cursor cursor2;
        Throwable th;
        FLogger.INSTANCE.getLocal().d(this.a, "getAllContacts()");
        ArrayList arrayList = new ArrayList();
        try {
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"contact_id", "display_name", "data1"}, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    try {
                        arrayList.add(new Lc6(cursor.getString(cursor.getColumnIndex("display_name")), cursor.getString(cursor.getColumnIndex("data1"))));
                    } catch (Exception e2) {
                        Exception e = e2;
                        try {
                            FLogger.INSTANCE.getLocal().e(this.a, e.getMessage());
                            e.printStackTrace();
                        } catch (Throwable th2) {
                            th = th2;
                            cursor2 = cursor;
                            if (cursor2 != null) {
                            }
                            throw th;
                        }
                    }
                }
            }
        } catch (Exception e3) {
            Exception e = e3;
            cursor = null;
            FLogger.INSTANCE.getLocal().e(this.a, e.getMessage());
            e.printStackTrace();
        } catch (Throwable th3) {
            th = th3;
            cursor2 = null;
            if (cursor2 != null) {
                cursor2.close();
            }
            try {
                throw th;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        throw null;
//
    }

    @DexIgnore
    public final void a(int i2, boolean z) {
        NotificationBaseObj t;
        boolean z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationSentResult, id = " + i2 + ", isSuccess = " + z);
        if (z) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.d(str2, "onNotificationSentResult, to silent notification, id = " + i2);
            Queue<NotificationBaseObj> queue = this.j;
            Wg6.a((Object) queue, "mDeviceNotifications");
            Iterator<NotificationBaseObj> it = queue.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                NotificationBaseObj next = it.next();
                if (next.getUid() == i2) {
                    z2 = true;
//                    continue;
                } else {
                    z2 = false;
//                    continue;
                }
                if (z2) {
                    t = next;
                    break;
                }
            }
            NotificationBaseObj t2 = t;
            if (t2 != null) {
                t2.toExistedNotification();
            }
        }
    }

    @DexIgnore
    public final void a(long j2) {
        Iterator<Map.Entry<String, Long>> it = this.c.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Long> next = it.next();
            Wg6.a((Object) next, "iterator.next()");
            Long value = next.getValue();
            Wg6.a((Object) value, "item.value");
            if (j2 - value.longValue() >= 900000) {
                it.remove();
            }
        }
    }

    @DexIgnore
    public final void a(Ei ei, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + ei.c());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + ei.e());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + ei.f());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + ei.h());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + ei.g());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + ei.m());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + ei.l());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + ei.i());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + ei.d());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + ei.j());
    }

    @TargetApi(Build.VERSION_CODES.M)
    @DexIgnore
    public final boolean a(Ei ei) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND()");
        Object systemService = PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 8) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Don't allow any calls");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityCallSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                return !a(ei, a(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                Wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                return !a(ei, b(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByCallDND() - Allow calls");
                return false;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0012  */
    public final boolean a(Ei ei, List<Lc6<String, String>> list) {
        throw null;
//        String f2 = ei.f();
//        String h2 = ei.h();
//        for (T t : list) {
//            if (Wg6.a((Object) ((String) t.getFirst()), (Object) f2) || Wg6.a((Object) ((String) t.getSecond()), (Object) h2)) {
//                FLogger.INSTANCE.getLocal().d(this.a, "isSenderInContactList() - Found contact");
//                return true;
//            }
//            while (r4.hasNext()) {
//            }
//        }
//        FLogger.INSTANCE.getLocal().d(this.a, "isSenderInContactList() - Not found contact");
//        return false;
    }

    @DexIgnore
    public final boolean a(String str) {
        List<ContactGroup> allContactGroups = Zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            if (contactGroup.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    public final boolean a(String str, boolean z) {
        int i2;
        boolean z2 = false;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.n.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(z);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z2 = a(str);
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isAllowCallOrMessageEvent() - from sender " + str + " is " + z2 + " because type = type = " + i2);
                return z2;
            }
        } else {
            i2 = 0;
        }
        z2 = true;
        FLogger.INSTANCE.getLocal().d(this.a, "isAllowCallOrMessageEvent() - from sender " + str + " is " + z2 + " because type = type = " + i2);
        return z2;
    }

    @DexIgnore
    public final AudioManager b() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final Rm6 b(StatusBarNotification statusBarNotification) {
        Wg6.b(statusBarNotification, "sbn");
        throw null;
//        return Xh7.b(Zi7.a(Qj7.a()), null, null, new Ri(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    public final /* synthetic */ Object b(Ei ei, Xe6<? super Cd6> xe6) {
//        Mi mi;
//        int i2;
//        boolean z;
//        T t;
//        NotificationBaseObj.ANotificationType aNotificationType = null;
//        if (xe6 instanceof Mi) {
//            Mi mi2 = (Mi) xe6;
//            int i3 = mi2.label;
//            if ((i3 & RecyclerView.UNDEFINED_DURATION) != 0) {
//                mi2.label = i3 + RecyclerView.UNDEFINED_DURATION;
//                mi = mi2;
//                Object obj = mi.result;
//                Object a2 = Ff6.a();
//                i2 = mi.label;
//                if (i2 != 0) {
//                    Nc6.a(obj);
//                    FLogger.INSTANCE.getLocal().d(this.a, "handleNotificationAdded " + ei);
//                    Ci ci = q.a().get(ei.c());
//                    List<AppFilter> allAppFilters = Zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
//                    if (ci == null) {
//                        Wg6.a((Object) allAppFilters, "appNotificationFilters");
//                        Iterator<T> it = allAppFilters.iterator();
//                        while (true) {
//                            if (!it.hasNext()) {
//                                t = null;
//                                break;
//                            }
//                            T next = it.next();
//                            T t2 = next;
//                            Wg6.a((Object) t2, "it");
//                            if (Hf6.a(Wg6.a((Object) t2.getType(), (Object) ei.c())).booleanValue()) {
//                                t = next;
//                                break;
//                            }
//                        }
//                        if (t == null) {
//                            FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
//                            a(ei, (NotificationBaseObj.ANotificationType) null);
//                            return Cd6.a;
//                        }
//                        FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but not in list icon supported");
//                        ci = new Ci(17, new DianaNotificationObj.AApplicationName(PortfolioApp.get.instance().d(ei.c()), ei.c(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
//                        z = false;
//                    } else {
//                        z = true;
//                    }
//                    if ((ei.l() || !ei.m()) && b(ei, ci) && !ei.j()) {
//                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
//                        a(ei, ci.a().getNotificationType());
//                        mi.L$0 = this;
//                        mi.L$1 = ei;
//                        mi.L$2 = ci;
//                        mi.L$3 = allAppFilters;
//                        mi.label = 1;
//                        if (a(ei, ci, z, mi) == a2) {
//                            return a2;
//                        }
//                    } else {
//                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
//                        DianaNotificationObj.AApplicationName a3 = ci.a();
//                        if (a3 != null) {
//                            aNotificationType = a3.getNotificationType();
//                        }
//                        a(ei, aNotificationType);
//                    }
//                } else if (i2 == 1) {
//                    List list = (List) mi.L$3;
//                    Ci ci2 = (Ci) mi.L$2;
//                    Ei ei2 = (Ei) mi.L$1;
//                    DianaNotificationComponent dianaNotificationComponent = (DianaNotificationComponent) mi.L$0;
//                    Nc6.a(obj);
//                } else {
//                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
//                }
//                return Cd6.a;
//            }
//        }
//        mi = new Mi(this, xe6);
//        Object obj2 = mi.result;
//        Object a22 = Ff6.a();
//        i2 = mi.label;
//        if (i2 != 0) {
//        }
        return Cd6.a;
    }

    @DexAdd
    public class AlelecSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            Context ctx = PortfolioApp.get.instance().getApplicationContext();
            dndSettingEnabled = AlelecPrefs.getAndroidDND(ctx);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(ctx);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(ctx);
        }
    }

    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public AlelecSettingsListener dndSettingsListener = null;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public boolean dndSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public static boolean dndHighSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public boolean emptyNotificationsEnabled = false;

    @DexAdd
    public final /* synthetic */ Object b(Ei eVar, Ci cVar, boolean z, Xe6<? super Cd6> xe6) {
		// This should wrap the function which contains Flogger ... "sendToDevice() - isSupportedIcon "
        if (dndSettingsListener == null) {
            Context context = PortfolioApp.get.instance().getApplicationContext();
            dndSettingsListener = new AlelecSettingsListener();
            AlelecPrefs.registerAndroidDND(context, dndSettingsListener );
            AlelecPrefs.registerEmptyNotifications(context, dndSettingsListener );
            dndSettingEnabled = AlelecPrefs.getAndroidDND(context);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(context);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(context);
        }
        FLogger.INSTANCE.getLocal().v(this.a, "emptyNotificationsEnabled: " + emptyNotificationsEnabled + ", dndSettingEnabled: " + dndSettingEnabled + ", notif.dnd: " + eVar.dnd);

        if (eVar.dnd && dndSettingEnabled) {
            FLogger.INSTANCE.getLocal().v(this.a, "Skip this notification from " + eVar.b() + " as DND is enabled");
            return Cd6.a;
        }
        if (emptyNotificationsEnabled && TextUtils.isEmpty(eVar.f)) {
            // Allow sending notifications with empty message
            eVar.f = " ";
        }

        return b(eVar, cVar, z, xe6, null);
    }

    @DexEdit
    /* JADX WARNING: Removed duplicated region for block: B:30:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0257  */
    public final /* synthetic */ Object b(Ei ei, Ci ci, boolean z, Xe6<? super Cd6> xe6, @DexIgnore Void tag) {
//        String str;
//        long j2;
//        Bi a2;
//        Boolean bool;
//        FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - isSupportedIcon " + z + " appName " + ci.a().getAppName() + " sender = " + ei.f() + " - text = " + ei.g());
//        if (TextUtils.isEmpty(ei.g())) {
//            FLogger.INSTANCE.getLocal().d(this.a, "Skip this notification from " + ei.c() + " since content is empty");
//            return Cd6.a;
//        }
//        String f2 = z ? ei.f() : ci.a().getAppName();
//        if (z) {
//            str = ei.g();
//        } else {
//            str = ei.f() + ':' + ei.g();
//        }
//        ArrayList arrayList = new ArrayList();
//        if (!DeviceHelper.o.m() || !Wg6.a((Object) ei.c(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName())) {
//            arrayList.add(NotificationBaseObj.ANotificationFlag.IMPORTANT);
//        } else {
//            arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACCEPT_CALL);
//            arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REJECT_CALL);
//        }
//        DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
//        String packageName = ci.a().getPackageName();
//        if (Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) || Wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName())) {
//            String o2 = ei instanceof Hi ? ((Hi) ei).o() : ei instanceof Ii ? ((Ii) ei).n() : "";
//            FLogger.INSTANCE.getLocal().d(this.a, "Receive phone notification, number " + o2 + " isQuickResponseEnable " + this.k.T());
//            Boolean T = this.k.T();
//            Wg6.a((Object) T, "mSharedPreferencesManager.isQuickResponseEnabled");
//            if (T.booleanValue()) {
//                if (o2 != null) {
//                    bool = Hf6.a(o2.length() > 0);
//                } else {
//                    bool = null;
//                }
//                if (bool == null) {
//                    Wg6.a();
//                    throw null;
//                } else if (bool.booleanValue()) {
//                    arrayList.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REPLY_MESSAGE);
//                    long upsertQuickResponseSender = this.m.upsertQuickResponseSender(new QuickResponseSender(o2));
//                    FLogger.INSTANCE.getLocal().d(this.a, "Update sender with number " + o2 + " id " + upsertQuickResponseSender);
//                    j2 = upsertQuickResponseSender;
//                    DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(ei.b(), ci.a().getNotificationType(), ci.a(), ei.h(), f2, (int) j2, str, arrayList, Hf6.a(ei.i()), null, null, 1536, null);
//                    a2 = a(ei, ci);
//                    if (a2 != Bi.SILENT) {
//                        FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - silent notification");
//                        dianaNotificationObj.toSilentNotification();
//                    } else if (a2 == Bi.SKIP) {
//                        FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - skip notification");
//                        return Cd6.a;
//                    }
//                    FLogger.INSTANCE.getLocal().d(this.a, "sendDianaNotification " + dianaNotificationObj);
//                    this.j.offer(dianaNotificationObj);
//                    PortfolioApp.get.instance().b(a(), dianaNotificationObj);
//                    return Cd6.a;
//                }
//            }
//        }
//        j2 = -1;
//        DianaNotificationObj dianaNotificationObj2 = new DianaNotificationObj(ei.b(), ci.a().getNotificationType(), ci.a(), ei.h(), f2, (int) j2, str, arrayList, Hf6.a(ei.i()), null, null, 1536, null);
//        a2 = a(ei, ci);
//        if (a2 != Bi.SILENT) {
//        }
//        FLogger.INSTANCE.getLocal().d(this.a, "sendDianaNotification " + dianaNotificationObj2);
//        this.j.offer(dianaNotificationObj2);
//        PortfolioApp.get.instance().b(a(), dianaNotificationObj2);
        return Cd6.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0072, code lost:
        if (r0 != null) goto L_0x0074;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0074, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0077, code lost:
        return r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0078, code lost:
        if (r0 != null) goto L_0x0074;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    public final List<Lc6<String, String>> b(Context context) {
        Cursor cursor;
        Cursor cursor2;
        Throwable th;
        FLogger.INSTANCE.getLocal().d(this.a, "getStaredContacts()");
        ArrayList arrayList = new ArrayList();
        try {
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, new String[]{"contact_id", "display_name", "data1"}, "starred = ?", new String[]{"1"}, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    try {
                        arrayList.add(new Lc6(cursor.getString(cursor.getColumnIndex("display_name")), cursor.getString(cursor.getColumnIndex("data1"))));
                    } catch (Exception e2) {
                        Exception e = e2;
                        try {
                            FLogger.INSTANCE.getLocal().e(this.a, e.getMessage());
                            e.printStackTrace();
                        } catch (Throwable th2) {
                            th = th2;
                            cursor2 = cursor;
                            if (cursor2 != null) {
                            }
                            throw th;
                        }
                    }
                }
            }
        } catch (Exception e3) {
            Exception e = e3;
            cursor = null;
            FLogger.INSTANCE.getLocal().e(this.a, e.getMessage());
            e.printStackTrace();
        } catch (Throwable th3) {
            th = th3;
            cursor2 = null;
            if (cursor2 != null) {
                cursor2.close();
            }
            try {
                throw th;
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        throw null;
//
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationRemoved, id = " + i2);
        c(i2);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @DexIgnore
    public final boolean b(Ei ei) {
        FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND()");
        Object systemService = PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if ((notificationManager.getNotificationPolicy().priorityCategories & 4) == 0) {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Don't allow any messages");
                return true;
            }
            int i2 = notificationManager.getNotificationPolicy().priorityMessageSenders;
            if (i2 == Math.abs(1)) {
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                Wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                return !a(ei, a(applicationContext));
            } else if (i2 == Math.abs(2)) {
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                Wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                return !a(ei, b(applicationContext2));
            } else if (i2 != Math.abs(0)) {
                return true;
            } else {
                FLogger.INSTANCE.getLocal().d(this.a, "isContactBockedByMessageDND() - Allow messages");
                return false;
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type android.app.NotificationManager");
        }
    }

    @DexIgnore
    public final boolean b(Ei ei, Ci ci) {
        return ci.b() || ei.d() > -2;
    }

    @DexIgnore
    public final boolean b(String str) {
        List<ContactGroup> allContactGroups = Zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            Wg6.a((Object) contactGroup, "contactGroup");
            List<Contact> contacts = contactGroup.getContacts();
            Wg6.a((Object) contacts, "contactGroup.contacts");
            if (!contacts.isEmpty()) {
                Contact contact = contactGroup.getContacts().get(0);
                Wg6.a((Object) contact, "contactGroup.contacts[0]");
                if (Xj6.b(contact.getDisplayName(), str, true)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final NotificationManager c() {
        return (NotificationManager) this.e.getValue();
    }

    @DexIgnore
    public final void c(int i2) {
        FLogger.INSTANCE.getLocal().d(this.a, "removeNotification");
        Queue<NotificationBaseObj> queue = this.j;
        Wg6.a((Object) queue, "mDeviceNotifications");
        synchronized (queue) {
            Iterator<NotificationBaseObj> it = this.j.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getUid() == i2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.a;
                        local.d(str, "notification removed, id = " + i2);
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void c(Context context) {
        if (PermissionUtils.a.d(context)) {
            this.i = new Fi();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            Fi fi = this.i;
            if (fi != null) {
                contentResolver.registerContentObserver(uri, false, fi);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            Wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void c(Ei ei) {
        synchronized (this) {
            Wg6.b(ei, "notification");
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved");
            throw null;
//            for (T t : this.b.a(ei.e(), ei.c())) {
//                ILocalFLogger local = FLogger.INSTANCE.getLocal();
//                String str = this.a;
//                local.d(str, "sendNotificationFromQueue, found notification " + ((Object) t));
//                DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(t.b(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", ei.c(), "", NotificationBaseObj.ANotificationType.REMOVED), t.h(), t.f(), -1, t.g(), Qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(t.i()), null, null, 1536, null);
//                c(t.b());
//                PortfolioApp.get.instance().b(a(), dianaNotificationObj);
//            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0017, code lost:
        if (r0 != 2) goto L_0x0019;
     */
    @DexIgnore
    public final boolean c(String str) {
        int i2;
        boolean z = false;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.n.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z = b(str);
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isAllowMessageFromContactName() - from sender " + str + " is " + z + ", type = type = " + i2);
                return z;
            }
        } else {
            i2 = 0;
        }
        z = true;
        FLogger.INSTANCE.getLocal().d(this.a, "isAllowMessageFromContactName() - from sender " + str + " is " + z + ", type = type = " + i2);
        return z;
    }

    @DexIgnore
    public final String d() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00bf  */
    public final String d(String str) {
        Cursor cursor;
        Cursor cursor2;
        Throwable th;
        if (PermissionUtils.a.e(PortfolioApp.get.instance())) {
            try {
                cursor = PortfolioApp.get.instance().getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name"}, null, null, null);
                if (cursor != null) {
                    try {
                        if (cursor.getCount() > 0) {
                            cursor.moveToFirst();
                            String string = cursor.getString(0);
                            Wg6.a((Object) string, "c.getString(0)");
                            try {
                                FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), number=" + str + ", displayName=" + string);
                                str = string;
                                if (cursor != null) {
                                    cursor.close();
                                }
                            } catch (Exception e2) {
                                Exception e = e2;
                                str = string;
                                try {
                                    FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e);
                                    if (cursor != null) {
                                    }
                                    return str;
                                } catch (Throwable th2) {
                                    th = th2;
                                    cursor2 = cursor;
                                    if (cursor2 != null) {
                                    }
                                    throw th;
                                }
                            }
                        }
                    } catch (Exception e3) {
                        Exception e = e3;
                        FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e);
                        if (cursor != null) {
                        }
                        return str;
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), number=" + str + ", no display name.");
                if (cursor != null) {
                }
            } catch (Exception e4) {
                Exception e = e4;
                cursor = null;
                FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e);
                if (cursor != null) {
                    cursor.close();
                }
                return str;
            } catch (Throwable th3) {
                th = th3;
                cursor2 = null;
                if (cursor2 != null) {
                    cursor2.close();
                }
                try {
                    throw th;
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        } else {
            FLogger.INSTANCE.getLocal().d(this.a, "readDisplayNameFromPhoneNumber(), number=" + str + ", no read contact permission.");
        }
        return str;
    }

    @DexIgnore
    public final void d(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            Fi fi = this.i;
            if (fi != null) {
                context.getContentResolver().unregisterContentObserver(fi);
                this.i = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Rm6 e() {
        throw null;
//        return Xh7.b(Zi7.a(Qj7.b()), null, null, new Ni(this, null), 3, null);
    }

    @DexIgnore
    public final boolean f() {
        throw null;
//        if ((c().getNotificationPolicy().priorityCategories & 32) == 0) {
//            FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Don't allow any alarms");
//            return true;
//        }
//        FLogger.INSTANCE.getLocal().d(this.a, "isAlarmBlockedByDND() - Allow alarms");
//        return false;
    }

    @DexIgnore
    public final boolean g() {
        return b().getStreamVolume(4) == 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0116, code lost:
        if (r4 >= r2) goto L_0x0118;
     */
    @DexIgnore
    public final boolean h() {
        if (this.k.J()) {
            Calendar instance = Calendar.getInstance();
            Wg6.a((Object) instance, "Calendar.getInstance()");
            Date q2 = TimeUtils.q(instance.getTime());
            Wg6.a((Object) q2, "DateHelper.getStartOfDay\u2026endar.getInstance().time)");
            long time = q2.getTime();
            Calendar instance2 = Calendar.getInstance();
            Wg6.a((Object) instance2, "Calendar.getInstance()");
            Date time2 = instance2.getTime();
            Wg6.a((Object) time2, "Calendar.getInstance().time");
            long time3 = (time2.getTime() - time) / ((long) 60000);
            List<DNDScheduledTimeModel> listDNDScheduledTimeModel = this.l.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
            if (!listDNDScheduledTimeModel.isEmpty()) {
                for (DNDScheduledTimeModel dNDScheduledTimeModel : listDNDScheduledTimeModel) {
                    int component2 = dNDScheduledTimeModel.component2();
                    if (dNDScheduledTimeModel.component3() == 0) {
                        this.f = component2;
                    } else {
                        this.g = component2;
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "currentMinutesOfDay = " + a((int) time3) + " -- mDNDStartTime = " + a(this.f) + " -- mDNDEndTime = " + a(this.g));
            int i2 = this.g;
            int i3 = this.f;
            if (i2 > i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime greater than mDNDStartTime");
                long j2 = (long) this.f;
                long j3 = (long) this.g;
                if (j2 <= time3 && j3 >= time3) {
                    FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                    return true;
                }
            } else if (i2 < i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime smaller than mDNDStartTime");
                long j4 = (long) DateTimeConstants.MINUTES_PER_DAY;
                if (((long) this.f) > time3 || j4 < time3) {
                    long j5 = (long) this.g;
                    if (0 <= time3) {
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            } else if (i2 == i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime equals mDNDStartTime");
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is not valid - show notification ");
        return false;
    }

    @DexIgnore
    public final boolean i() {
        return b().getStreamVolume(5) == 0;
    }

    @DexIgnore
    public final boolean j() {
        return b().getStreamVolume(2) == 0;
    }

    @DexIgnore
    public final void k() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onDeviceConnected, notificationList = " + this.j);
        Queue<NotificationBaseObj> queue = this.j;
        Wg6.a((Object) queue, "mDeviceNotifications");
        synchronized (queue) {
            for (NotificationBaseObj notificationBaseObj : this.j) {
                PortfolioApp instance = PortfolioApp.get.instance();
                String a2 = a();
                Wg6.a((Object) notificationBaseObj, "notification");
                instance.b(a2, notificationBaseObj);
            }
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final void l() {
        FLogger.INSTANCE.getLocal().d(this.a, "onServiceConnected");
        e();
    }

    @DexIgnore
    public final void m() {
        this.c.clear();
    }
}
