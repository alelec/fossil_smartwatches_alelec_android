package com.mapped;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexEdit(defaultAction = DexAction.IGNORE)
public abstract class WatchSettingFragmentBinding extends ViewDataBinding {
    @DexIgnore
    public /* final */ ScrollView A;
    @DexIgnore
    public /* final */ View B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ View D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextView H;
    @DexIgnore
    public /* final */ FlexibleTextView I;
    @DexIgnore
    public /* final */ FlexibleTextView J;
    @DexIgnore
    public /* final */ FlexibleTextView K;
    @DexIgnore
    public /* final */ FlexibleTextView L;
    @DexIgnore
    public /* final */ FlexibleTextView M;
    @DexIgnore
    public /* final */ FlexibleTextView N;
    @DexIgnore
    public /* final */ FlexibleTextView O;
    @DexIgnore
    public /* final */ FlexibleTextView P;
    @DexIgnore
    public /* final */ FlexibleTextView Q;
    @DexIgnore
    public /* final */ FlexibleTextView R;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ CardView v;
    @DexIgnore
    public /* final */ FlexibleButton w;
    @DexIgnore
    public /* final */ FlexibleButton x;
    @DexIgnore
    public /* final */ FlexibleButton y;
    @DexIgnore
    public /* final */ ImageView z;

    @DexAdd
    public FlexibleTextView secret_key_value;

    @DexAdd
    public FlexibleSwitchCompat switchAndroidDND;
    @DexAdd
    public FlexibleSwitchCompat switchAndroidDND_HIGH;
    @DexAdd
    public FlexibleSwitchCompat switchEmptyNotifications;
    @DexAdd
    public FlexibleSwitchCompat switchAutoSync;

    @DexAdd
    public WatchSettingFragmentBinding(Object obj, View view, int i, FlexibleButton flexibleButton, RTLImageView rTLImageView, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CardView cardView, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleButton flexibleButton5, ImageView imageView, ScrollView scrollView, View view2, View view3, View view4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14, FlexibleTextView flexibleTextView15, FlexibleSwitchCompat switchAndroidDND, FlexibleSwitchCompat switchAndroidDND_HIGH, FlexibleSwitchCompat switchEmptyNotifications, FlexibleSwitchCompat switchAutoSync) {
        this(obj, view, i, flexibleButton, rTLImageView, flexibleButton2, constraintLayout, constraintLayout2, cardView, flexibleButton3, flexibleButton4, flexibleButton5, imageView, scrollView, view2, view3, view4, flexibleTextView, flexibleTextView2, flexibleTextView3, flexibleTextView4, flexibleTextView5, flexibleTextView6, flexibleTextView7, flexibleTextView8, flexibleTextView9, flexibleTextView10, flexibleTextView11, flexibleTextView12, flexibleTextView13, flexibleTextView14);
        this.secret_key_value = flexibleTextView15;
        this.switchAndroidDND = switchAndroidDND;
        this.switchAndroidDND_HIGH = switchAndroidDND_HIGH;
        this.switchEmptyNotifications = switchEmptyNotifications;
        this.switchAutoSync = switchAutoSync;
    }

    @DexIgnore
    public WatchSettingFragmentBinding(Object obj, View view, int i, FlexibleButton flexibleButton, RTLImageView rTLImageView, FlexibleButton flexibleButton2, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, CardView cardView, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleButton flexibleButton5, ImageView imageView, ScrollView scrollView, View view2, View view3, View view4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, FlexibleTextView flexibleTextView12, FlexibleTextView flexibleTextView13, FlexibleTextView flexibleTextView14) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = rTLImageView;
        this.s = flexibleButton2;
        this.t = constraintLayout;
        this.u = constraintLayout2;
        this.v = cardView;
        this.w = flexibleButton3;
        this.x = flexibleButton4;
        this.y = flexibleButton5;
        this.z = imageView;
        this.A = scrollView;
        this.B = view2;
        this.C = view3;
        this.D = view4;
        this.E = flexibleTextView;
        this.F = flexibleTextView2;
        this.G = flexibleTextView3;
        this.H = flexibleTextView4;
        this.I = flexibleTextView5;
        this.J = flexibleTextView6;
        this.K = flexibleTextView7;
        this.L = flexibleTextView8;
        this.M = flexibleTextView9;
        this.N = flexibleTextView10;
        this.O = flexibleTextView11;
        this.P = flexibleTextView12;
        this.Q = flexibleTextView13;
        this.R = flexibleTextView14;
    }
}
