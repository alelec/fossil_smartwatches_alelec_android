package com.mapped;

import android.widget.RadioButton;
import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.fossil.Pb;
import com.portfolio.platform.view.CustomizeWidget;

import com.fossil.wearables.fossil.R;

import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
public class WatchFacePreviewFragmentMapping extends WatchFacePreviewFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.i C; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray D;
    @DexIgnore
    public long B;

    /*
    static {
        SparseIntArray sparseIntArray = new SparseIntArray();
        D = sparseIntArray;
        sparseIntArray.put(2131363342, 1);
        D.put(2131363220, 2);
        D.put(2131363196, 3);  // tv_accept
        D.put(2131362047, 4);
        D.put(2131362640, 5);
        D.put(2131362021, 6);
        D.put(2131363463, 7);  // wc_top
        D.put(2131363455, 8);  // wc_bottom
        D.put(2131363462, 9);  // wc_start
        D.put(2131363459, 10); // wc_end
    }
    */

    static {
        D.put(R.id.tv_radio_group, 11);
    }

    @DexReplace
    public WatchFacePreviewFragmentMapping(Pb pb, View view) {
        this(pb, view, ViewDataBinding.a(pb, view, 12, C, D));
    }

    @DexReplace
    public WatchFacePreviewFragmentMapping(Pb pb, View view, Object[] objArr) {
        super(
                pb,
                view,
                0,
                (View) objArr[6],
                (ConstraintLayout) objArr[4],
                (ConstraintLayout) objArr[0],
                (ImageView) objArr[5],
                (TextView) objArr[3],
                (TextView) objArr[2],
                (TextView) objArr[1],
                (CustomizeWidget) objArr[8],  //bottom
                (CustomizeWidget) objArr[10], //end
                (CustomizeWidget) objArr[9],  //start
                (CustomizeWidget) objArr[7],  //top
                (RadioGroup) objArr[11]
        );
        this.B = -1;
        ((WatchFacePreviewFragmentBinding) this).s.setTag(null);
        a(view);
        f();
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void a() {
        synchronized (this) {
            this.B = 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public boolean e() {
        synchronized (this) {
            return this.B != 0;
        }
    }

    @DexIgnore
    @Override // androidx.databinding.ViewDataBinding
    public void f() {
        synchronized (this) {
            this.B = 1;
        }
        g();
    }
}
