package com.misfit.frameworks.buttonservice.model.notification;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.Ba0;
import com.fossil.Gc7;
import com.fossil.Hc7;
import com.fossil.blesdk.device.data.notification.NotificationFilter;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.HandMovingConfig;
import com.mapped.NotificationHandMovingConfig;
import com.mapped.NotificationIcon;
import com.mapped.NotificationIconConfig;
import com.mapped.NotificationVibePattern;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE)
public final class AppNotificationFilter implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    public static /* final */ int IS_FIELD_EXIST; // = 1;
    @DexIgnore
    public static /* final */ int IS_FIELD_NOT_EXIST; // = 0;
    @DexIgnore
    public static /* final */ String TAG; // = "AppNotificationFilter";
    @DexIgnore
    public FNotification fNotification;
    @DexIgnore
    public NotificationHandMovingConfig handMovingConfig;
    @DexIgnore
    public Short priority;
    @DexIgnore
    public String sender;
    @DexIgnore
    public NotificationVibePattern vibePattern;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<AppNotificationFilter> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter createFromParcel(Parcel parcel) {
            Wg6.b(parcel, "parcel");
            return new AppNotificationFilter(parcel, null);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public AppNotificationFilter[] newArray(int i) {
            return new AppNotificationFilter[i];
        }
    }

    @DexIgnore
    public AppNotificationFilter(Parcel parcel) {
        FNotification fNotification2 = (FNotification) parcel.readParcelable(FNotification.class.getClassLoader());
        this.fNotification = fNotification2 == null ? new FNotification() : fNotification2;
        if (parcel.readInt() == 1) {
            this.sender = parcel.readString();
        }
        if (parcel.readInt() == 1) {
            this.priority = Short.valueOf((short) parcel.readInt());
        }
        if (parcel.readInt() == 1) {
            this.handMovingConfig = (NotificationHandMovingConfig) parcel.readParcelable(HandMovingConfig.class.getClassLoader());
        }
        if (parcel.readInt() == 1) {
            this.vibePattern = NotificationVibePattern.values()[parcel.readInt()];
        }
    }

    @DexIgnore
    public /* synthetic */ AppNotificationFilter(Parcel parcel, Qg6 qg6) {
        this(parcel);
    }

    @DexIgnore
    public AppNotificationFilter(FNotification fNotification2) {
        Wg6.b(fNotification2, "fNotification");
        this.fNotification = fNotification2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof AppNotificationFilter)) {
            return false;
        }
        AppNotificationFilter appNotificationFilter = (AppNotificationFilter) obj;
        throw null;
//        return Wg6.a(this.fNotification, appNotificationFilter.fNotification) && Wg6.a(this.sender, appNotificationFilter.sender) && Wg6.a(this.priority, appNotificationFilter.priority);
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.handMovingConfig;
    }

    @DexIgnore
    public final String getPackageName() {
        return this.fNotification.getPackageName();
    }

    @DexIgnore
    public final Short getPriority() {
        return this.priority;
    }

    @DexIgnore
    public final String getSender() {
        return this.sender;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.vibePattern;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.handMovingConfig = notificationHandMovingConfig;
    }

    @DexIgnore
    public final void setPriority(Short sh) {
        this.priority = sh;
    }

    @DexIgnore
    public final void setSender(String str) {
        this.sender = str;
    }

    @DexIgnore
    public final void setVibePattern(NotificationVibePattern notificationVibePattern) {
        this.vibePattern = notificationVibePattern;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01b5, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01b6, code lost:
        com.fossil.Hc7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x01b9, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01bc, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x01bd, code lost:
        com.fossil.Hc7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x01c0, code lost:
        throw r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x01c3, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x01c4, code lost:
        com.fossil.Hc7.a(r2, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01c7, code lost:
        throw r3;
     */
    @DexReplace
    public final NotificationFilter toSDKNotificationFilter(Context context) {
        NotificationFilter notificationFilter;
        Wg6.b(context, "context");
        FLogger.INSTANCE.getLocal().d(TAG, "toSDKNotificationFilter() start");
        if (this.handMovingConfig == null) {
            notificationFilter = new NotificationFilter(this.fNotification.getPackageName());
            if (Wg6.a((Object) this.fNotification.getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || Wg6.a((Object) this.fNotification.getPackageName(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName())) {
                try {
                    InputStream open = context.getAssets().open(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getIconFwPath());
                    String iconFwPath = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getIconFwPath();
                    Wg6.a((Object) open, "it");
                    NotificationIcon notificationIcon = new NotificationIcon(iconFwPath, Gc7.a(open));
                    Hc7.a(open, null);
                    InputStream open2 = context.getAssets().open(DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getIconFwPath());
                    String iconFwPath2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getIconFwPath();
                    Wg6.a((Object) open2, "it");
                    NotificationIcon notificationIcon2 = new NotificationIcon(iconFwPath2, Gc7.a(open2));
                    Hc7.a(open2, null);
                    notificationFilter.setIconConfig(new NotificationIconConfig(notificationIcon).setIconForType(Ba0.INCOMING_CALL, notificationIcon).setIconForType(Ba0.MISSED_CALL, notificationIcon2));
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("toSDKNotificationFilter() ");
                    e.printStackTrace();
                    sb.append(Cd6.a);
                    local.e(TAG, sb.toString());
                }
            } else {
                boolean set = false;
                if ((this.fNotification.getIconFwPath().length() > 0) && (! this.fNotification.getIconFwPath().toLowerCase().equals("general_white.bin"))) {
                    try {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.e(TAG, "toSDKNotificationFilter() openIconPath " + this.fNotification.getIconFwPath());
                        InputStream open3 = context.getAssets().open(this.fNotification.getIconFwPath());
                        String iconFwPath3 = this.fNotification.getIconFwPath();
                        Wg6.a((Object) open3, "it");
                        notificationFilter.setIconConfig(new NotificationIconConfig(new NotificationIcon(iconFwPath3, Gc7.a(open3))));
                        Hc7.a(open3, null);
                        set = true;
                    } catch (Exception e2) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("toSDKNotificationFilter() ");
                        e2.printStackTrace();
                        sb2.append(Cd6.a);
                        local3.e(TAG, sb2.toString());
                    }
                }

                if (!set) {
                    File iconFile = new File(context.getExternalFilesDir("appIcons"), this.fNotification.packageName + ".icon");
                    byte[] iconData = null;
                    if (iconFile.exists()) {
                        int size = (int) iconFile.length();
                        if (size > 4) {
                            iconData = new byte[size];
                            try {
                                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(iconFile));
                                buf.read(iconData, 0, iconData.length);
                                buf.close();
                                if (iconData.length > 0) {
                                    notificationFilter.setIconConfig(new NotificationIconConfig(new NotificationIcon(this.fNotification.packageName + ".icon", iconData)));
                                    set = true;
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                if (!set) {
                    String iconFwPath = "general_white.bin";
                    InputStream open3 = null;
                    try {
                        open3 = context.getAssets().open(iconFwPath);
                        Wg6.a((Object) open3, "it");
                        notificationFilter.setIconConfig(new NotificationIconConfig(new NotificationIcon(iconFwPath, Gc7.a(open3))));
                        Hc7.a(open3, null);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (this.vibePattern != null) {
            String packageName = this.fNotification.getPackageName();
            NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
            if (notificationHandMovingConfig != null) {
                NotificationVibePattern notificationVibePattern = this.vibePattern;
                if (notificationVibePattern != null) {
                    notificationFilter = new NotificationFilter(packageName, notificationHandMovingConfig, notificationVibePattern);
                } else {
                    Wg6.a();
                    throw null;
                }
            } else {
                Wg6.a();
                throw null;
            }
        } else {
            String packageName2 = this.fNotification.getPackageName();
            NotificationHandMovingConfig notificationHandMovingConfig2 = this.handMovingConfig;
            if (notificationHandMovingConfig2 != null) {
                notificationFilter = new NotificationFilter(packageName2, notificationHandMovingConfig2, NotificationVibePattern.NO_VIBE);
            } else {
                Wg6.a();
                throw null;
            }
        }
        String str = this.sender;
        if (str != null) {
            notificationFilter.setSender(str);
        }
        Short sh = this.priority;
        if (sh != null) {
            notificationFilter.setPriority(sh.shortValue());
        }
        return notificationFilter;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        Wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.b(parcel, "parcel");
        parcel.writeParcelable(this.fNotification, 0);
        String str = this.sender;
        if (str != null) {
            parcel.writeInt(1);
            parcel.writeString(str);
        } else {
            parcel.writeInt(0);
        }
        Short sh = this.priority;
        if (sh != null) {
            short shortValue = sh.shortValue();
            parcel.writeInt(1);
            parcel.writeInt(shortValue);
        } else {
            parcel.writeInt(0);
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.handMovingConfig;
        if (notificationHandMovingConfig != null) {
            parcel.writeInt(1);
            parcel.writeParcelable(notificationHandMovingConfig, 0);
        } else {
            parcel.writeInt(0);
        }
        NotificationVibePattern notificationVibePattern = this.vibePattern;
        if (notificationVibePattern != null) {
            parcel.writeInt(1);
            parcel.writeInt(notificationVibePattern.ordinal());
            return;
        }
        parcel.writeInt(0);
    }
}
