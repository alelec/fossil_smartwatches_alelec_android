package com.portfolio.platform.retrofit;

import android.text.TextUtils;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Lh5;
import com.fossil.Q88;
import com.fossil.R18;
import com.fossil.T18;
import com.fossil.V18;
import com.fossil.W18;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.ServerErrorException;
import com.portfolio.platform.data.model.room.UserDao;
import com.portfolio.platform.data.model.room.UserDatabase;
import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AuthenticationInterceptor implements Interceptor {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ Ai g; // = new Ai(null);
    @DexIgnore
    public AtomicInteger a; // = new AtomicInteger(0);
    @DexIgnore
    public Auth b;
    @DexIgnore
    public /* final */ PortfolioApp c;
    @DexIgnore
    public /* final */ AuthApiGuestService d;
    @DexIgnore
    public /* final */ An4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return AuthenticationInterceptor.f;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.retrofit.AuthenticationInterceptor$intercept$1", f = "AuthenticationInterceptor.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $currentUser;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(MFUser mFUser, Xe6 xe6) {
            super(2, xe6);
            this.$currentUser = mFUser;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.$currentUser, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            UserDao userDao;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                UserDatabase B = EncryptedDatabaseManager.j.B();
                if (!(B == null || (userDao = B.userDao()) == null)) {
                    userDao.updateUser(this.$currentUser);
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = AuthenticationInterceptor.class.getSimpleName();
        Wg6.b(simpleName, "AuthenticationInterceptor::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public AuthenticationInterceptor(PortfolioApp portfolioApp, AuthApiGuestService authApiGuestService, An4 an4) {
        Wg6.c(portfolioApp, "mApplication");
        Wg6.c(authApiGuestService, "mAuthApiGuestService");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.c = portfolioApp;
        this.d = authApiGuestService;
        this.e = an4;
    }

    @DexIgnore
    public final Auth b(MFUser mFUser, boolean z) {
        Auth auth;
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "getAuth needProcess=" + z + " mCount=" + this.a);
            if (z) {
                Ku3 ku3 = new Ku3();
                ku3.n(Constants.PROFILE_KEY_REFRESH_TOKEN, mFUser.getAuth().getRefreshToken());
                try {
                    Q88<Auth> a2 = this.d.tokenRefresh(ku3).a();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = f;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Refresh Token error=");
                    sb.append(a2 != null ? Integer.valueOf(a2.b()) : null);
                    sb.append(" body=");
                    sb.append(a2 != null ? a2.d() : null);
                    local2.d(str2, sb.toString());
                    this.b = a2.a();
                } catch (Exception e2) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = f;
                    local3.d(str3, "getAuth needProcess=" + z + " mCount=" + this.a + " exception=" + e2.getMessage());
                    e2.printStackTrace();
                    return null;
                }
            }
            auth = this.b;
        }
        return auth;
    }

    @DexIgnore
    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) {
        ServerError serverError;
        MFUser.Auth auth;
        UserDao userDao;
        int i = 0;
        Wg6.c(chain, "chain");
        V18.Ai h = chain.c().h();
        UserDatabase B = EncryptedDatabaseManager.j.B();
        MFUser currentUser = (B == null || (userDao = B.userDao()) == null) ? null : userDao.getCurrentUser();
        String accessToken = (currentUser == null || (auth = currentUser.getAuth()) == null) ? null : auth.getAccessToken();
        if (!TextUtils.isEmpty(accessToken) && currentUser != null) {
            long currentTimeMillis = (System.currentTimeMillis() - this.e.f()) / ((long) 1000);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "loginDurationInSeconds = " + currentTimeMillis + " tokenExpiresIn " + currentUser.getAuth().getAccessTokenExpiresIn());
            if (currentTimeMillis >= ((long) currentUser.getAuth().getAccessTokenExpiresIn())) {
                Auth b2 = b(currentUser, this.a.getAndIncrement() == 0);
                if (this.a.decrementAndGet() == 0) {
                    this.b = null;
                }
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = f;
                local2.d(str2, "intercept decrementAndGet mCount=" + this.a);
                if ((b2 != null ? b2.getAccessToken() : null) != null) {
                    MFUser.Auth auth2 = currentUser.getAuth();
                    String accessToken2 = b2.getAccessToken();
                    if (accessToken2 != null) {
                        auth2.setAccessToken(accessToken2);
                        MFUser.Auth auth3 = currentUser.getAuth();
                        String refreshToken = b2.getRefreshToken();
                        if (refreshToken != null) {
                            auth3.setRefreshToken(refreshToken);
                            MFUser.Auth auth4 = currentUser.getAuth();
                            String w0 = TimeUtils.w0(b2.getAccessTokenExpiresAt());
                            Wg6.b(w0, "DateHelper.toJodaTime(auth.accessTokenExpiresAt)");
                            auth4.setAccessTokenExpiresAt(w0);
                            MFUser.Auth auth5 = currentUser.getAuth();
                            Integer accessTokenExpiresIn = b2.getAccessTokenExpiresIn();
                            if (accessTokenExpiresIn != null) {
                                i = accessTokenExpiresIn.intValue();
                            }
                            auth5.setAccessTokenExpiresIn(i);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = f;
                            local3.d(str3, "accessToken = " + b2.getAccessToken() + ", refreshToken = " + b2.getRefreshToken());
                            Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(currentUser, null), 3, null);
                            this.e.V1(b2.getAccessToken());
                            this.e.H0(System.currentTimeMillis());
                            h.a("Authorization", "Bearer " + b2.getAccessToken());
                            h.b();
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Bearer ");
                    if (accessToken != null) {
                        sb.append(accessToken);
                        h.a("Authorization", sb.toString());
                        h.b();
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("Bearer ");
                if (accessToken != null) {
                    sb2.append(accessToken);
                    h.a("Authorization", sb2.toString());
                    h.b();
                } else {
                    Wg6.i();
                    throw null;
                }
            }
        }
        h.a("Content-Type", Constants.CONTENT_TYPE);
        String g2 = this.e.g();
        if (g2 == null) {
            g2 = "";
        }
        h.a("X-Active-Device", g2);
        h.a("User-Agent", Lh5.b.a());
        h.a("locale", this.c.X());
        try {
            Response d2 = chain.d(h.b());
            Wg6.b(d2, "chain.proceed(requestBuilder.build())");
            return d2;
        } catch (Exception e2) {
            if (e2 instanceof ServerErrorException) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str4 = f;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("exception=");
                ServerErrorException serverErrorException = (ServerErrorException) e2;
                sb3.append(serverErrorException.getServerError());
                local4.d(str4, sb3.toString());
                serverError = serverErrorException.getServerError();
            } else {
                serverError = e2 instanceof UnknownHostException ? new ServerError(601, "") : e2 instanceof SocketTimeoutException ? new ServerError(MFNetworkReturnCode.CLIENT_TIMEOUT, "") : new ServerError(600, "");
            }
            Response.a aVar = new Response.a();
            aVar.p(h.b());
            aVar.n(T18.HTTP_1_1);
            Integer code = serverError.getCode();
            Wg6.b(code, "serverError.code");
            aVar.g(code.intValue());
            String message = serverError.getMessage();
            if (message == null) {
                message = "";
            }
            aVar.k(message);
            aVar.b(W18.create(R18.d(com.zendesk.sdk.network.Constants.APPLICATION_JSON), new Gson().t(serverError)));
            aVar.a("Content-Type", Constants.CONTENT_TYPE);
            aVar.a("User-Agent", Lh5.b.a());
            String g3 = this.e.g();
            if (g3 == null) {
                g3 = "";
            }
            aVar.a("X-Active-Device", g3);
            aVar.a("Locale", this.c.X());
            Response c2 = aVar.c();
            Wg6.b(c2, "Response.Builder()\n     \u2026                 .build()");
            return c2;
        }
    }
}
