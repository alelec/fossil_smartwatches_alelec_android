package com.portfolio.platform.service.musiccontrol;

import android.content.ComponentName;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.os.Build;
import android.view.KeyEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.internal.AnalyticsEvents;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.Nr5;
import com.fossil.Qq7;
import com.fossil.Qr5;
import com.fossil.Rr5;
import com.fossil.Sr5;
import com.fossil.Tr5;
import com.fossil.Ur5;
import com.fossil.Yk7;
import com.fossil.Yn7;
import com.fossil.Zk7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponseFactory;
import com.misfit.frameworks.buttonservice.model.watchapp.response.NotifyMusicEventResponse;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.service.FossilNotificationListenerService;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MusicControlComponent {
    @DexIgnore
    public MediaSessionManager a;
    @DexIgnore
    public Qr5 b;
    @DexIgnore
    public Ur5<Qr5> c; // = new Ur5<>();
    @DexIgnore
    public /* final */ Yk7 d; // = Zk7.a(Ci.INSTANCE);
    @DexIgnore
    public MediaSessionManager.OnActiveSessionsChangedListener e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$enableMusicControlViaNotification$2", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements MediaSessionManager.OnActiveSessionsChangedListener {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Aii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // android.media.session.MediaSessionManager.OnActiveSessionsChangedListener
            public final void onActiveSessionsChanged(List<MediaController> list) {
                FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".enableMusicControlViaNotification Process controllers when active sessions changed");
                this.a.this$0.u(list);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Tr5 {
            @DexIgnore
            public /* final */ /* synthetic */ Ai h;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, String str) {
                super(str);
                this.h = ai;
            }

            @DexIgnore
            @Override // com.fossil.Tr5
            public void j(Rr5 rr5, Rr5 rr52) {
                Wg6.c(rr5, "oldMetadata");
                Wg6.c(rr52, "newMetadata");
                super.j(rr5, rr52);
                FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".OldNotificationMusicController onMetadataChanged");
                this.h.this$0.s(c());
            }

            @DexIgnore
            @Override // com.fossil.Tr5
            public void k(int i, int i2, Qr5 qr5) {
                Wg6.c(qr5, "controller");
                super.k(i, i2, qr5);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", ".OldNotificationMusicController onPlaybackStateChanged oldState " + i + " newState " + i2);
                if (i2 == 3) {
                    this.h.this$0.w(qr5);
                } else if (this.h.this$0.p() == null) {
                    this.h.this$0.w(qr5);
                }
                this.h.this$0.r(i2);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(MusicControlComponent musicControlComponent, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = musicControlComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                boolean c = Jn5.c(Jn5.b, PortfolioApp.get.instance(), Jn5.Ai.SET_WATCH_APP_MUSIC, false, false, true, null, 32, null);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", ".enableMusicControlViaNotification isPermissionGranted " + c + " isRegister " + this.this$0.f);
                if (this.this$0.f || !c) {
                    return Cd6.a;
                }
                if (Build.VERSION.SDK_INT >= 21) {
                    ComponentName componentName = new ComponentName(PortfolioApp.get.instance().getApplicationContext(), FossilNotificationListenerService.class);
                    MusicControlComponent musicControlComponent = this.this$0;
                    Object systemService = PortfolioApp.get.instance().getSystemService("media_session");
                    if (systemService != null) {
                        musicControlComponent.a = (MediaSessionManager) systemService;
                        MediaSessionManager mediaSessionManager = this.this$0.a;
                        if (mediaSessionManager != null) {
                            List<MediaController> activeSessions = mediaSessionManager.getActiveSessions(componentName);
                            Wg6.b(activeSessions, "mediaSessionManager!!.getActiveSessions(component)");
                            FLogger.INSTANCE.getLocal().d("MusicControlComponent", ".enableMusicControlViaNotification Process current active controllers first");
                            this.this$0.u(activeSessions);
                            if (this.this$0.e == null) {
                                FLogger.INSTANCE.getLocal().d("MusicControlComponent", "init activeSessionChangedListener");
                                this.this$0.e = new Aii(this);
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            StringBuilder sb = new StringBuilder();
                            sb.append("add activeSessionChangedListener to mediaSessionManager,current active sessions ");
                            MediaSessionManager mediaSessionManager2 = this.this$0.a;
                            if (mediaSessionManager2 != null) {
                                sb.append(mediaSessionManager2.getActiveSessions(componentName).size());
                                local2.d("MusicControlComponent", sb.toString());
                                MediaSessionManager mediaSessionManager3 = this.this$0.a;
                                if (mediaSessionManager3 != null) {
                                    MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener = this.this$0.e;
                                    if (onActiveSessionsChangedListener != null) {
                                        mediaSessionManager3.addOnActiveSessionsChangedListener(onActiveSessionsChangedListener, componentName);
                                    } else {
                                        Wg6.i();
                                        throw null;
                                    }
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else {
                        throw new Rc6("null cannot be cast to non-null type android.media.session.MediaSessionManager");
                    }
                } else {
                    Bii bii = new Bii(this, "All Apps");
                    if (this.this$0.c.a(bii)) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("MusicControlComponent", ".OldNotificationMusicController is added to list controller, packageName=" + bii.d());
                    }
                }
                this.this$0.f = true;
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent", f = "MusicControlComponent.kt", l = {169}, m = "forcePushEventAndMetadataChanged")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(MusicControlComponent musicControlComponent, Xe6 xe6) {
            super(xe6);
            this.this$0 = musicControlComponent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.n(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Gg6<AudioManager> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(0);
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final AudioManager invoke() {
            Object systemService = PortfolioApp.get.instance().getSystemService("audio");
            if (!(systemService instanceof AudioManager)) {
                systemService = null;
            }
            return (AudioManager) systemService;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Sr5 {
        @DexIgnore
        public /* final */ /* synthetic */ MediaController h;
        @DexIgnore
        public /* final */ /* synthetic */ MusicControlComponent i;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(MediaController mediaController, MediaController mediaController2, String str, MusicControlComponent musicControlComponent) {
            super(mediaController2, str);
            this.h = mediaController;
            this.i = musicControlComponent;
        }

        @DexIgnore
        @Override // com.fossil.Sr5
        public void i(Rr5 rr5, Rr5 rr52) {
            Wg6.c(rr5, "oldMetadata");
            Wg6.c(rr52, "newMetadata");
            super.i(rr5, rr52);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", "onMetadataChanged of controller " + this.h.getPackageName() + " old " + rr5 + " new " + rr52);
            this.i.s(c());
        }

        @DexIgnore
        @Override // com.fossil.Sr5
        public void j(int i2, int i3, Qr5 qr5) {
            Wg6.c(qr5, "controller");
            super.j(i2, i3, qr5);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", "onPlaybackStateChanged of controller " + this.h.getPackageName() + " state " + i3);
            if (this.i.p() == null || i3 == 3) {
                this.i.w(qr5);
            }
            this.i.r(i3);
        }

        @DexIgnore
        @Override // com.fossil.Sr5
        public void k(Qr5 qr5) {
            Wg6.c(qr5, "controller");
            super.k(qr5);
            this.i.c.c(qr5);
            if (Wg6.a(qr5, this.i.p())) {
                this.i.w(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei extends Qq7 implements Hg6<Qr5, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ MediaController $mediaController;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(MediaController mediaController) {
            super(1);
            this.$mediaController = mediaController;
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Boolean invoke(Qr5 qr5) {
            return Boolean.valueOf(invoke(qr5));
        }

        @DexIgnore
        public final boolean invoke(Qr5 qr5) {
            Wg6.c(qr5, "musicController");
            return Wg6.a(qr5.d(), this.$mediaController.getPackageName());
        }
    }

    @DexIgnore
    public final int k() {
        AudioManager q = q();
        if (q != null) {
            return q.getStreamVolume(3);
        }
        return 100;
    }

    @DexIgnore
    public final void l(NotifyMusicEventResponse.MusicMediaAction musicMediaAction) {
        NotifyMusicEventResponse.MusicMediaStatus v;
        Wg6.c(musicMediaAction, "action");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "doMusicAction isMusicActive " + this.g);
        switch (Nr5.a[musicMediaAction.ordinal()]) {
            case 1:
                v = v(126);
                if (v == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    v = v(85);
                    break;
                }
                break;
            case 2:
                v = v(127);
                if (v == NotifyMusicEventResponse.MusicMediaStatus.SUCCESS) {
                    v = v(85);
                    break;
                }
                break;
            case 3:
                v = v(85);
                break;
            case 4:
                v = v(87);
                break;
            case 5:
                v = v(88);
                break;
            case 6:
                v = z();
                break;
            case 7:
                v = y();
                break;
            default:
                v = NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
                break;
        }
        if (this.g) {
            PortfolioApp.get.instance().j1(MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, v), PortfolioApp.get.instance().J());
        }
    }

    @DexIgnore
    public final Object m(Xe6<? super Cd6> xe6) {
        Object g2 = Eu7.g(Bw7.c(), new Ai(this, null), xe6);
        return g2 == Yn7.d() ? g2 : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object n(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 1
            r5 = 0
            boolean r0 = r8 instanceof com.portfolio.platform.service.musiccontrol.MusicControlComponent.Bi
            if (r0 == 0) goto L_0x002a
            r0 = r8
            com.portfolio.platform.service.musiccontrol.MusicControlComponent$Bi r0 = (com.portfolio.platform.service.musiccontrol.MusicControlComponent.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r2 = r0.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0038
            if (r3 != r6) goto L_0x0030
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.service.musiccontrol.MusicControlComponent r0 = (com.portfolio.platform.service.musiccontrol.MusicControlComponent) r0
            com.fossil.El7.b(r2)
        L_0x0027:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0029:
            return r0
        L_0x002a:
            com.portfolio.platform.service.musiccontrol.MusicControlComponent$Bi r0 = new com.portfolio.platform.service.musiccontrol.MusicControlComponent$Bi
            r0.<init>(r7, r8)
            goto L_0x0014
        L_0x0030:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0038:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "forcePushEventAndMetadataChanged activeController "
            r3.append(r4)
            com.fossil.Qr5 r4 = r7.b
            r3.append(r4)
            java.lang.String r4 = " initialize "
            r3.append(r4)
            boolean r4 = r7.f
            r3.append(r4)
            java.lang.String r4 = "MusicControlComponent"
            java.lang.String r3 = r3.toString()
            r2.d(r4, r3)
            r7.g = r6
            com.fossil.Qr5 r2 = r7.b
            if (r2 == 0) goto L_0x00d7
            if (r2 == 0) goto L_0x00d3
            int r0 = r2.e()
            com.fossil.Qr5 r1 = r7.b
            if (r1 == 0) goto L_0x00cf
            com.fossil.Rr5 r1 = r1.c()
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ".forcePushEventAndMetadataChanged active controller="
            r3.append(r4)
            com.fossil.Qr5 r4 = r7.b
            if (r4 == 0) goto L_0x00cb
            java.lang.String r4 = r4.d()
            r3.append(r4)
            java.lang.String r4 = ", state="
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = ", metadata="
            r3.append(r0)
            r3.append(r1)
            java.lang.String r0 = "MusicControlComponent"
            java.lang.String r1 = r3.toString()
            r2.d(r0, r1)
            com.fossil.Qr5 r0 = r7.b
            if (r0 == 0) goto L_0x00c7
            com.fossil.Rr5 r0 = r0.c()
            r7.s(r0)
            com.fossil.Qr5 r0 = r7.b
            if (r0 == 0) goto L_0x00c3
            int r0 = r0.e()
            r7.r(r0)
            goto L_0x0027
        L_0x00c3:
            com.mapped.Wg6.i()
            throw r5
        L_0x00c7:
            com.mapped.Wg6.i()
            throw r5
        L_0x00cb:
            com.mapped.Wg6.i()
            throw r5
        L_0x00cf:
            com.mapped.Wg6.i()
            throw r5
        L_0x00d3:
            com.mapped.Wg6.i()
            throw r5
        L_0x00d7:
            r0.L$0 = r7
            r0.label = r6
            java.lang.Object r0 = r7.m(r0)
            if (r0 != r1) goto L_0x0027
            r0 = r1
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.musiccontrol.MusicControlComponent.n(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final String o(String str) {
        ApplicationInfo applicationInfo;
        String obj;
        PackageManager packageManager = PortfolioApp.get.instance().getPackageManager();
        try {
            applicationInfo = packageManager.getApplicationInfo(str, 0);
        } catch (Exception e2) {
            applicationInfo = null;
        }
        return (applicationInfo == null || (obj = packageManager.getApplicationLabel(applicationInfo).toString()) == null) ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN : obj;
    }

    @DexIgnore
    public final Qr5 p() {
        return this.b;
    }

    @DexIgnore
    public final AudioManager q() {
        return (AudioManager) this.d.getValue();
    }

    @DexIgnore
    public final void r(int i) {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", ".notifyMusicAction(), state=" + i + " isMusicActive " + this.g);
            if (this.g) {
                NotifyMusicEventResponse.MusicMediaAction musicMediaAction = i != 2 ? i != 3 ? null : NotifyMusicEventResponse.MusicMediaAction.PLAY : NotifyMusicEventResponse.MusicMediaAction.PAUSE;
                if (musicMediaAction != null) {
                    PortfolioApp.get.instance().j1(MusicResponseFactory.INSTANCE.createMusicEventResponse(musicMediaAction, NotifyMusicEventResponse.MusicMediaStatus.SUCCESS), PortfolioApp.get.instance().J());
                }
            }
        }
    }

    @DexIgnore
    public final void s(Rr5 rr5) {
        synchronized (this) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("MusicControlComponent", ".notifyMusicTrackInfo(), metadata=" + rr5 + " isMusicActive " + this.g);
            if (this.g) {
                String c2 = rr5.c();
                Qr5 qr5 = this.b;
                if (!(!Wg6.a(c2, qr5 != null ? qr5.d() : null))) {
                    PortfolioApp.get.instance().j1(MusicResponseFactory.INSTANCE.createMusicTrackInfoResponse("", (byte) k(), rr5.d(), rr5.b(), rr5.a()), PortfolioApp.get.instance().J());
                }
            }
        }
    }

    @DexIgnore
    public final void t() {
        MediaSessionManager mediaSessionManager;
        this.f = false;
        MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener = this.e;
        if (onActiveSessionsChangedListener != null && (mediaSessionManager = this.a) != null) {
            mediaSessionManager.removeOnActiveSessionsChangedListener(onActiveSessionsChangedListener);
        }
    }

    @DexIgnore
    public final void u(List<MediaController> list) {
        if (!(list == null || list.isEmpty())) {
            for (T t : list) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("MusicControlComponent", "Process controller " + t.getPackageName());
                if (!(this.c.b(new Ei(t)) != null)) {
                    String packageName = t.getPackageName();
                    Wg6.b(packageName, "mediaController.packageName");
                    Di di = new Di(t, t, o(packageName), this);
                    if (this.b == null) {
                        w(di);
                    }
                    boolean a2 = this.c.a(di);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("MusicControlComponent", ".processActiveMediaControllers() Add controller " + di.b() + " to controller map, currentActiveController " + this.b + ", isSuccess " + a2);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d("MusicControlComponent", ".processActiveMediaControllers() Controller " + t.getPackageName() + " already added");
                }
            }
        }
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus v(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "sendKeyEvent activeMusicController " + this.b);
        Qr5 qr5 = this.b;
        if (qr5 != null) {
            boolean a2 = qr5.a(new KeyEvent(0, i));
            boolean a3 = qr5.a(new KeyEvent(1, i));
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d("MusicControlComponent", "sendKeyEVent actionDown isSuccess " + a2 + " actionUp isSuccess " + a3);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = (!a2 || !a3) ? NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER : NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.NO_MUSIC_PLAYER;
    }

    @DexIgnore
    public final void w(Qr5 qr5) {
        boolean a2 = Wg6.a(this.b, qr5);
        this.b = qr5;
        if ((!a2) && qr5 != null) {
            FLogger.INSTANCE.getLocal().d("MusicControlComponent", "musicController changed notifyMusicTrackInfo");
            s(qr5.c());
        }
    }

    @DexIgnore
    public final void x(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("MusicControlComponent", "Music watch app state change, isActive " + z);
        this.g = z;
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus y() {
        AudioManager q = q();
        if (q != null) {
            q.adjustStreamVolume(3, -1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }

    @DexIgnore
    public final NotifyMusicEventResponse.MusicMediaStatus z() {
        AudioManager q = q();
        if (q != null) {
            q.adjustStreamVolume(3, 1, 1);
            NotifyMusicEventResponse.MusicMediaStatus musicMediaStatus = NotifyMusicEventResponse.MusicMediaStatus.SUCCESS;
            if (musicMediaStatus != null) {
                return musicMediaStatus;
            }
        }
        return NotifyMusicEventResponse.MusicMediaStatus.FAIL_TO_TRIGGER;
    }
}
