package com.portfolio.platform.service.fcm;

import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Ci4;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ux7;
import com.fossil.Ws4;
import com.fossil.Yn7;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.InAppNotificationManager;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.service.buddychallenge.BuddyChallengeManager;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public InAppNotificationManager h;
    @DexIgnore
    public BuddyChallengeManager i;
    @DexIgnore
    public An4 j;
    @DexIgnore
    public FCMRepository k;
    @DexIgnore
    public FlagRepository l;
    @DexIgnore
    public Il6 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$onMessageReceived$1", f = "FossilFirebaseMessagingService.kt", l = {}, m = "invokeSuspend")
    public static final class a extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ci4 $remoteMessage;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends TypeToken<Ws4> {
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(FossilFirebaseMessagingService fossilFirebaseMessagingService, Ci4 ci4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = fossilFirebaseMessagingService;
            this.$remoteMessage = ci4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            a aVar = new a(this.this$0, this.$remoteMessage, xe6);
            aVar.p$ = (Il6) obj;
            throw null;
            //return aVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((a) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Boolean b = this.this$0.A().b();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e("FossilFirebaseMessagingService", "onMessageReceived - isBCOn : " + b);
                Wg6.b(b, "isBcOn");
                if (b.booleanValue()) {
                    try {
                        String obj2 = this.$remoteMessage.c().toString();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.e("FossilFirebaseMessagingService", "dataStr: " + obj2);
                        Ws4 ws4 = (Ws4) new Gson().l(obj2, new a().getType());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        StringBuilder sb = new StringBuilder();
                        sb.append("isActive: ");
                        Il6 il6 = this.this$0.m;
                        sb.append(il6 != null ? Ao7.a(Jv7.g(il6)) : null);
                        sb.append(" - notification: ");
                        sb.append(ws4);
                        local3.e("FossilFirebaseMessagingService", sb.toString());
                        if (ws4 != null) {
                            this.this$0.y().k(ws4);
                            this.this$0.z().a(ws4, this.this$0);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.fcm.FossilFirebaseMessagingService$registerFCM$1", f = "FossilFirebaseMessagingService.kt", l = {131}, m = "invokeSuspend")
    public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $token;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ FossilFirebaseMessagingService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(FossilFirebaseMessagingService fossilFirebaseMessagingService, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = fossilFirebaseMessagingService;
            this.$token = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b bVar = new b(this.this$0, this.$token, xe6);
            bVar.p$ = (Il6) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FCMRepository x = this.this$0.x();
                String str = this.$token;
                this.L$0 = il6;
                this.label = 1;
                if (x.c(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore
    public final An4 A() {
        An4 an4 = this.j;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPrefs");
        throw null;
    }

    @DexIgnore
    public final void B(String str) {
        Il6 il6 = this.m;
        if (il6 != null) {
            Rm6 unused = Gu7.d(il6, Bw7.b(), null, new b(this, str, null), 2, null);
        }
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        this.m = Jv7.h(Jv7.a(Bw7.b()), Ux7.b(null, 1, null));
        PortfolioApp.get.instance().getIface().l0(this);
    }

    @DexIgnore
    @Override // com.fossil.Sh4
    public void onDestroy() {
        super.onDestroy();
        Il6 il6 = this.m;
        if (il6 != null) {
            Jv7.d(il6, null, 1, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void r(Ci4 ci4) {
        Il6 il6;
        Wg6.c(ci4, "remoteMessage");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "onMessageReceived(), from:  " + ci4.f() + " - priority: " + ci4.A() + " - notification: " + ci4.k() + " - messageData: " + ci4.c());
        Map<String, String> c = ci4.c();
        Wg6.b(c, "remoteMessage.data");
        if ((!c.isEmpty()) && (il6 = this.m) != null) {
            Rm6 unused = Gu7.d(il6, Bw7.b(), null, new a(this, ci4, null), 2, null);
        }
    }

    @DexIgnore
    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void t(String str) {
        Wg6.c(str, "refreshedToken");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("FossilFirebaseMessagingService", "Refreshed token: " + str);
        An4 an4 = this.j;
        if (an4 != null) {
            String p = an4.p();
            if (p == null || p.length() == 0) {
                An4 an42 = this.j;
                if (an42 != null) {
                    an42.e1(str);
                    An4 an43 = this.j;
                    if (an43 == null) {
                        Wg6.n("mSharedPrefs");
                        throw null;
                    } else if (an43.Z1()) {
                        B(str);
                    }
                } else {
                    Wg6.n("mSharedPrefs");
                    throw null;
                }
            } else {
                An4 an44 = this.j;
                if (an44 != null) {
                    an44.e1(str);
                    B(str);
                    return;
                }
                Wg6.n("mSharedPrefs");
                throw null;
            }
        } else {
            Wg6.n("mSharedPrefs");
            throw null;
        }
    }

    @DexIgnore
    public final FCMRepository x() {
        FCMRepository fCMRepository = this.k;
        if (fCMRepository != null) {
            return fCMRepository;
        }
        Wg6.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final BuddyChallengeManager y() {
        BuddyChallengeManager buddyChallengeManager = this.i;
        if (buddyChallengeManager != null) {
            return buddyChallengeManager;
        }
        Wg6.n("mBuddyChallengeManager");
        throw null;
    }

    @DexIgnore
    public final InAppNotificationManager z() {
        InAppNotificationManager inAppNotificationManager = this.h;
        if (inAppNotificationManager != null) {
            return inAppNotificationManager;
        }
        Wg6.n("mInAppNotificationManager");
        throw null;
    }
}
