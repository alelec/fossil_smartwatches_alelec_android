package com.portfolio.platform.service;

import android.content.Intent;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleCommandResultManager {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static Ci<CommunicateMode, Ai> b; // = new Ci<>();
    @DexIgnore
    public static /* final */ HashMap<CommunicateMode, Di<Bi>> c; // = new HashMap<>();
    @DexIgnore
    public static /* final */ BleCommandResultManager d; // = new BleCommandResultManager();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Intent a;

        @DexIgnore
        public Ai(CommunicateMode communicateMode, String str, Intent intent) {
            Wg6.c(communicateMode, "mode");
            Wg6.c(intent, "intent");
            this.a = intent;
        }

        @DexIgnore
        public final Intent a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface Bi {
        @DexIgnore
        void a(CommunicateMode communicateMode, Intent intent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T, V> {
        @DexIgnore
        public /* final */ HashMap<T, V> a; // = new HashMap<>();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final V a(T t) {
            return this.a.get(t);
        }

        @DexIgnore
        public final boolean b(T t, V v) {
            boolean z;
            synchronized (this.b) {
                if (Wg6.a(this.a.get(t), v)) {
                    this.a.remove(t);
                    z = true;
                } else {
                    z = false;
                }
            }
            return z;
        }

        @DexIgnore
        public final void c(T t, V v) {
            synchronized (this.b) {
                this.a.put(t, v);
                Cd6 cd6 = Cd6.a;
            }
        }

        @DexIgnore
        public String toString() {
            String hashMap = this.a.toString();
            Wg6.b(hashMap, "mHashMap.toString()");
            return hashMap;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> {
        @DexIgnore
        public /* final */ List<T> a; // = new ArrayList();
        @DexIgnore
        public /* final */ Object b; // = new Object();

        @DexIgnore
        public final void a(T t) {
            synchronized (this.b) {
                this.a.add(t);
            }
        }

        @DexIgnore
        public final List<T> b() {
            ArrayList arrayList;
            synchronized (this.b) {
                arrayList = new ArrayList();
                arrayList.addAll(this.a);
            }
            return arrayList;
        }

        @DexIgnore
        public final void c(T t) {
            synchronized (this.b) {
                this.a.remove(t);
            }
        }

        @DexIgnore
        public final int d() {
            return this.a.size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1", f = "BleCommandResultManager.kt", l = {69}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $communicateModes;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai $bleResult;
            @DexIgnore
            public /* final */ /* synthetic */ CommunicateMode $communicateMode;
            @DexIgnore
            public /* final */ /* synthetic */ Di $observerList;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, CommunicateMode communicateMode, Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.$observerList = di;
                this.$communicateMode = communicateMode;
                this.$bleResult = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$observerList, this.$communicateMode, this.$bleResult, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    for (Bi bi : this.$observerList.b()) {
                        bi.a(this.$communicateMode, this.$bleResult.a());
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(List list, Xe6 xe6) {
            super(2, xe6);
            this.$communicateModes = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.$communicateModes, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
            jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
            	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
            	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
            */
        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0114 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:8:0x0038  */
        @Override // com.fossil.Zn7
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 284
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.BleCommandResultManager.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = BleCommandResultManager.class.getSimpleName();
        Wg6.b(simpleName, "BleCommandResultManager::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final void d(Bi bi, List<? extends CommunicateMode> list) {
        synchronized (this) {
            Wg6.c(bi, "observerReceiver");
            Wg6.c(list, "communicateModes");
            for (T t : list) {
                Di<Bi> di = c.get(t);
                if (di != null) {
                    di.a(bi);
                } else {
                    HashMap<CommunicateMode, Di<Bi>> hashMap = c;
                    Di<Bi> di2 = new Di<>();
                    di2.a(bi);
                    hashMap.put(t, di2);
                }
            }
        }
    }

    @DexIgnore
    public final void e(Bi bi, CommunicateMode... communicateModeArr) {
        Wg6.c(bi, "observerReceiver");
        Wg6.c(communicateModeArr, "communicateModes");
        d(bi, Hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void f(List<? extends CommunicateMode> list) {
        synchronized (this) {
            Wg6.c(list, "communicateModes");
            Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Ei(list, null), 3, null);
        }
    }

    @DexIgnore
    public final void g(CommunicateMode... communicateModeArr) {
        Wg6.c(communicateModeArr, "communicateModes");
        f(Hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }

    @DexIgnore
    public final void h(CommunicateMode communicateMode, Ai ai) {
        synchronized (this) {
            Wg6.c(communicateMode, "mode");
            Wg6.c(ai, "bleResult");
            b.c(communicateMode, ai);
            g(communicateMode);
        }
    }

    @DexIgnore
    public final void i(Bi bi, List<? extends CommunicateMode> list) {
        synchronized (this) {
            Wg6.c(bi, "observerReceiver");
            Wg6.c(list, "communicateModes");
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                Di<Bi> di = c.get(it.next());
                if (di != null) {
                    di.c(bi);
                }
            }
        }
    }

    @DexIgnore
    public final void j(Bi bi, CommunicateMode... communicateModeArr) {
        Wg6.c(bi, "observerReceiver");
        Wg6.c(communicateModeArr, "communicateModes");
        i(bi, Hm7.i((CommunicateMode[]) Arrays.copyOf(communicateModeArr, communicateModeArr.length)));
    }
}
