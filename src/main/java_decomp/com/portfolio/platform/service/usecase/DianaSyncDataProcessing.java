package com.portfolio.platform.service.usecase;

import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gl7;
import com.fossil.Hm7;
import com.fossil.Hr7;
import com.fossil.Im7;
import com.fossil.Jj5;
import com.fossil.Ko7;
import com.fossil.Mn7;
import com.fossil.Pm7;
import com.fossil.Qq7;
import com.fossil.Tk5;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.SyncDataExtensions;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.CalorieWrapper;
import com.portfolio.platform.data.model.fitnessdata.DistanceWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper;
import com.portfolio.platform.data.model.fitnessdata.RestingWrapper;
import com.portfolio.platform.data.model.fitnessdata.StepWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaSyncDataProcessing {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ DianaSyncDataProcessing b; // = new DianaSyncDataProcessing();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ long a;
        @DexIgnore
        public /* final */ List<ActivitySample> b;
        @DexIgnore
        public /* final */ List<ActivitySummary> c;
        @DexIgnore
        public /* final */ List<MFSleepSession> d;
        @DexIgnore
        public /* final */ List<HeartRateSample> e;
        @DexIgnore
        public /* final */ List<DailyHeartRateSummary> f;
        @DexIgnore
        public /* final */ List<Lc6<Long, Long>> g;
        @DexIgnore
        public /* final */ List<WorkoutSession> h;
        @DexIgnore
        public /* final */ List<GFitSample> i;
        @DexIgnore
        public /* final */ List<GFitHeartRate> j;
        @DexIgnore
        public /* final */ List<GFitWorkoutSession> k;

        @DexIgnore
        public Ai(long j2, List<ActivitySample> list, List<ActivitySummary> list2, List<MFSleepSession> list3, List<HeartRateSample> list4, List<DailyHeartRateSummary> list5, List<Lc6<Long, Long>> list6, List<WorkoutSession> list7, List<GFitSample> list8, List<GFitHeartRate> list9, List<GFitWorkoutSession> list10) {
            Wg6.c(list, "sampleRawList");
            Wg6.c(list2, "summaryList");
            Wg6.c(list3, "sleepSessionList");
            Wg6.c(list4, "heartRateDataList");
            Wg6.c(list5, "heartRateSummaryList");
            Wg6.c(list6, "activeTimeList");
            Wg6.c(list7, "workoutSessionList");
            Wg6.c(list8, "gFitSampleList");
            Wg6.c(list9, "gFitHeartRateList");
            Wg6.c(list10, "gFitWorkoutSessionList");
            this.a = j2;
            this.b = list;
            this.c = list2;
            this.d = list3;
            this.e = list4;
            this.f = list5;
            this.g = list6;
            this.h = list7;
            this.i = list8;
            this.j = list9;
            this.k = list10;
        }

        @DexIgnore
        public final List<Lc6<Long, Long>> a() {
            return this.g;
        }

        @DexIgnore
        public final List<GFitHeartRate> b() {
            return this.j;
        }

        @DexIgnore
        public final List<GFitSample> c() {
            return this.i;
        }

        @DexIgnore
        public final List<GFitWorkoutSession> d() {
            return this.k;
        }

        @DexIgnore
        public final List<HeartRateSample> e() {
            return this.e;
        }

        @DexIgnore
        public final List<DailyHeartRateSummary> f() {
            return this.f;
        }

        @DexIgnore
        public final long g() {
            return this.a;
        }

        @DexIgnore
        public final List<ActivitySample> h() {
            return this.b;
        }

        @DexIgnore
        public final List<MFSleepSession> i() {
            return this.d;
        }

        @DexIgnore
        public final List<ActivitySummary> j() {
            return this.c;
        }

        @DexIgnore
        public final List<WorkoutSession> k() {
            return this.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends Qq7 implements Hg6<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ Ci INSTANCE; // = new Ci();

        @DexIgnore
        public Ci() {
            super(1);
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            Wg6.c(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Integer invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Integer.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di extends Qq7 implements Hg6<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ Di INSTANCE; // = new Di();

        @DexIgnore
        public Di() {
            super(1);
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            Wg6.c(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }

        @DexIgnore
        /* Return type fixed from 'java.lang.Object' to match base method */
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public /* bridge */ /* synthetic */ Long invoke(FitnessDataWrapper fitnessDataWrapper) {
            return Long.valueOf(invoke(fitnessDataWrapper));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Comparator<T> {
        @DexIgnore
        @Override // java.util.Comparator
        public final int compare(T t, T t2) {
            return Mn7.c(Long.valueOf(t.getStartLongTime()), Long.valueOf(t2.getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing", f = "DianaSyncDataProcessing.kt", l = {153, 164, 187, 195, 205, 213, 221, 228, 234}, m = "saveSyncResult")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$13;
        @DexIgnore
        public Object L$14;
        @DexIgnore
        public Object L$15;
        @DexIgnore
        public Object L$16;
        @DexIgnore
        public Object L$17;
        @DexIgnore
        public Object L$18;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaSyncDataProcessing this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(DianaSyncDataProcessing dianaSyncDataProcessing, Xe6 xe6) {
            super(xe6);
            this.this$0 = dianaSyncDataProcessing;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, null, null, null, null, null, null, null, null, null, null, null, null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2", f = "DianaSyncDataProcessing.kt", l = {176}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ai $finalResult;
        @DexIgnore
        public /* final */ /* synthetic */ ThirdPartyRepository $thirdPartyRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$2$1", f = "DianaSyncDataProcessing.kt", l = {177, 178}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $gFitActiveTime;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitHeartRate;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitSample;
            @DexIgnore
            public /* final */ /* synthetic */ List $listGFitWorkoutSession;
            @DexIgnore
            public /* final */ /* synthetic */ List $listMFSleepSession;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, List list, Jh6 jh6, List list2, List list3, List list4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$listGFitSample = list;
                this.$gFitActiveTime = jh6;
                this.$listGFitHeartRate = list2;
                this.$listGFitWorkoutSession = list3;
                this.$listMFSleepSession = list4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$listGFitSample, this.$gFitActiveTime, this.$listGFitHeartRate, this.$listGFitWorkoutSession, this.$listMFSleepSession, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Il6 il6;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il62 = this.p$;
                    List<GFitHeartRate> list = this.$listGFitHeartRate;
                    List<GFitWorkoutSession> list2 = this.$listGFitWorkoutSession;
                    List<MFSleepSession> list3 = this.$listMFSleepSession;
                    this.L$0 = il62;
                    this.label = 1;
                    if (this.this$0.$thirdPartyRepository.saveData(this.$listGFitSample, this.$gFitActiveTime.element, list, list2, list3, this) == d) {
                        return d;
                    }
                    il6 = il62;
                } else if (i == 1) {
                    il6 = (Il6) this.L$0;
                    El7.b(obj);
                } else if (i == 2) {
                    Il6 il63 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ThirdPartyRepository thirdPartyRepository = this.this$0.$thirdPartyRepository;
                this.L$0 = il6;
                this.label = 2;
                Object uploadData$default = ThirdPartyRepository.uploadData$default(thirdPartyRepository, null, this, 1, null);
                return uploadData$default == d ? d : uploadData$default;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(Ai ai, ThirdPartyRepository thirdPartyRepository, Xe6 xe6) {
            super(2, xe6);
            this.$finalResult = ai;
            this.$thirdPartyRepository = thirdPartyRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.$finalResult, this.$thirdPartyRepository, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                List<GFitSample> c = this.$finalResult.c();
                Jh6 jh6 = new Jh6();
                jh6.element = null;
                List<Lc6<Long, Long>> a2 = this.$finalResult.a();
                ArrayList arrayList = new ArrayList(Im7.m(a2, 10));
                for (T t : a2) {
                    arrayList.add(Hm7.h((Long) t.getFirst(), (Long) t.getSecond()));
                }
                List h0 = Pm7.h0(Im7.o(arrayList));
                if (!h0.isEmpty()) {
                    jh6.element = (T) new GFitActiveTime(h0);
                }
                List<MFSleepSession> i2 = this.$finalResult.i();
                List<GFitHeartRate> b = this.$finalResult.b();
                List<GFitWorkoutSession> d2 = this.$finalResult.d();
                Dv7 b2 = Bw7.b();
                Aii aii = new Aii(this, c, jh6, b, d2, i2, null);
                this.L$0 = il6;
                this.L$1 = c;
                this.L$2 = jh6;
                this.L$3 = h0;
                this.L$4 = i2;
                this.L$5 = b;
                this.L$6 = d2;
                this.label = 1;
                Object g = Eu7.g(b2, aii, this);
                return g == d ? d : g;
            } else if (i == 1) {
                List list = (List) this.L$6;
                List list2 = (List) this.L$5;
                List list3 = (List) this.L$4;
                List list4 = (List) this.L$3;
                Jh6 jh62 = (Jh6) this.L$2;
                List list5 = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3", f = "DianaSyncDataProcessing.kt", l = {246}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitiesRepository $activityRepository;
        @DexIgnore
        public /* final */ /* synthetic */ List $fitnessDataList;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository $heartRateSampleRepository;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository $heartRateSummaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository $sleepSessionsRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository $sleepSummariesRepository;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository $summaryRepository;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository $workoutSessionRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.service.usecase.DianaSyncDataProcessing$saveSyncResult$3$1", f = "DianaSyncDataProcessing.kt", l = {247, 248, 250, 251, 253, 254, 256, 257}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ActivityStatistic>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $endDate;
            @DexIgnore
            public /* final */ /* synthetic */ Jh6 $startDate;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Hi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Hi hi, Jh6 jh6, Jh6 jh62, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = hi;
                this.$startDate = jh6;
                this.$endDate = jh62;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$startDate, this.$endDate, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0064  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0091  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00c0  */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x00ee  */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x011e  */
            /* JADX WARNING: Removed duplicated region for block: B:32:0x014c  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x017b  */
            /* JADX WARNING: Removed duplicated region for block: B:37:0x017e  */
            /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:8:0x0035  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r12) {
                /*
                // Method dump skipped, instructions count: 410
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.usecase.DianaSyncDataProcessing.Hi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(List list, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, WorkoutSessionRepository workoutSessionRepository, Xe6 xe6) {
            super(2, xe6);
            this.$fitnessDataList = list;
            this.$activityRepository = activitiesRepository;
            this.$summaryRepository = summariesRepository;
            this.$sleepSessionsRepository = sleepSessionsRepository;
            this.$sleepSummariesRepository = sleepSummariesRepository;
            this.$heartRateSampleRepository = heartRateSampleRepository;
            this.$heartRateSummaryRepository = heartRateSummaryRepository;
            this.$workoutSessionRepository = workoutSessionRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.$fitnessDataList, this.$activityRepository, this.$summaryRepository, this.$sleepSessionsRepository, this.$sleepSummariesRepository, this.$heartRateSampleRepository, this.$heartRateSummaryRepository, this.$workoutSessionRepository, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super ActivityStatistic> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Jh6 jh6 = new Jh6();
                jh6.element = (T) ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getStartTimeTZ();
                Jh6 jh62 = new Jh6();
                jh62.element = (T) ((FitnessDataWrapper) this.$fitnessDataList.get(0)).getEndTimeTZ();
                for (FitnessDataWrapper fitnessDataWrapper : this.$fitnessDataList) {
                    if (fitnessDataWrapper.getStartLongTime() < jh6.element.getMillis()) {
                        jh6.element = (T) fitnessDataWrapper.getStartTimeTZ();
                    }
                    if (fitnessDataWrapper.getEndLongTime() > jh62.element.getMillis()) {
                        jh62.element = (T) fitnessDataWrapper.getEndTimeTZ();
                    }
                }
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, jh6, jh62, null);
                this.L$0 = il6;
                this.L$1 = jh6;
                this.L$2 = jh62;
                this.label = 1;
                Object g = Eu7.g(b, aii, this);
                return g == d ? d : g;
            } else if (i == 1) {
                Jh6 jh63 = (Jh6) this.L$2;
                Jh6 jh64 = (Jh6) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String name = DianaSyncDataProcessing.class.getName();
        Wg6.b(name, "DianaSyncDataProcessing::class.java.name");
        a = name;
    }
    */

    @DexIgnore
    public final Ai a(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        Wg6.c(str, "serial");
        Wg6.c(list, "syncData");
        Wg6.c(mFUser, "user");
        Wg6.c(userProfile, "userProfile");
        Wg6.c(portfolioApp, "portfolioApp");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = a;
        local.d(str2, ".buildSyncResult(), get all data files, synctime=" + j + ", data=" + list);
        Ai ai = new Ai(j2, new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList(), new ArrayList());
        if (!list.isEmpty()) {
            return g(str, list, mFUser, userProfile, j, j2, portfolioApp);
        }
        h(str, "Sync data is empty");
        return ai;
    }

    @DexIgnore
    public final DailyHeartRateSummary b(DailyHeartRateSummary dailyHeartRateSummary, HeartRateSample heartRateSample) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "calculateHeartRateSummary - summary=" + dailyHeartRateSummary + ", sample=" + heartRateSample);
        int minuteCount = dailyHeartRateSummary.getMinuteCount() + heartRateSample.getMinuteCount();
        return new DailyHeartRateSummary(((dailyHeartRateSummary.getAverage() * ((float) dailyHeartRateSummary.getMinuteCount())) + (heartRateSample.getAverage() * ((float) heartRateSample.getMinuteCount()))) / ((float) minuteCount), dailyHeartRateSummary.getDate(), dailyHeartRateSummary.getCreatedAt(), System.currentTimeMillis(), Math.min(dailyHeartRateSummary.getMin(), heartRateSample.getMin()), Math.max(dailyHeartRateSummary.getMax(), heartRateSample.getMax()), minuteCount, heartRateSample.getResting() != null ? heartRateSample.getResting() : dailyHeartRateSummary.getResting());
    }

    @DexIgnore
    public final List<GFitHeartRate> c(List<FitnessDataWrapper> list) {
        FLogger.INSTANCE.getLocal().d(a, "getGFitHeartRates");
        ArrayList arrayList = new ArrayList();
        Pm7.b0(list, new Bi());
        for (T t : list) {
            int timezoneOffsetInSecond = t.getTimezoneOffsetInSecond();
            HeartRateWrapper heartRate = t.getHeartRate();
            if (heartRate != null) {
                int component1 = heartRate.component1();
                int i = 0;
                for (T t2 : heartRate.component4()) {
                    if (i >= 0) {
                        short shortValue = t2.shortValue();
                        Calendar calendar = new DateTime(t.getStartLongTime() + (((long) (i * component1)) * 1000), DateTimeZone.forOffsetMillis(timezoneOffsetInSecond * 1000)).toCalendar(Locale.US);
                        calendar.set(13, 0);
                        calendar.set(14, 0);
                        Object clone = calendar.clone();
                        if (clone != null) {
                            Calendar calendar2 = (Calendar) clone;
                            calendar2.add(13, component1);
                            Wg6.b(calendar, "currentStartTime");
                            arrayList.add(new GFitHeartRate((float) shortValue, calendar.getTimeInMillis(), calendar2.getTimeInMillis()));
                            i++;
                        } else {
                            throw new Rc6("null cannot be cast to non-null type java.util.Calendar");
                        }
                    } else {
                        Hm7.l();
                        throw null;
                    }
                }
                continue;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final List<GFitWorkoutSession> d(List<FitnessDataWrapper> list) {
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            for (T t : it.next().getWorkouts()) {
                DateTime component2 = t.component2();
                DateTime component3 = t.component3();
                int component5 = t.component5();
                int component6 = t.component6();
                StepWrapper component11 = t.component11();
                CalorieWrapper component12 = t.component12();
                DistanceWrapper component13 = t.component13();
                HeartRateWrapper component14 = t.component14();
                long millis = component2.getMillis();
                long millis2 = component3.getMillis();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                ArrayList arrayList5 = new ArrayList();
                int size = component11.getValues().size();
                int i = size > 0 ? (int) ((((float) component5) / ((float) size)) * ((float) 1000)) : 0;
                int i2 = 0;
                while (i2 < size) {
                    int i3 = i2 + 1;
                    arrayList2.add(new GFitWOStep(component11.getValues().get(i2).shortValue(), millis + ((long) (i2 * i)), ((long) (i3 * i)) + millis));
                    i2 = i3;
                }
                int size2 = component12.getValues().size();
                int i4 = size2 > 0 ? (int) ((((float) component5) / ((float) size2)) * ((float) 1000)) : 0;
                int i5 = 0;
                while (i5 < size2) {
                    int i6 = i5 + 1;
                    arrayList3.add(new GFitWOCalorie(component12.getValues().get(i5).floatValue(), millis + ((long) (i5 * i4)), ((long) (i6 * i4)) + millis));
                    i5 = i6;
                }
                if (component13 != null) {
                    int size3 = component13.getValues().size();
                    int i7 = size3 > 0 ? (int) ((((float) component5) / ((float) size3)) * ((float) 1000)) : 0;
                    int i8 = 0;
                    while (i8 < size3) {
                        int i9 = i8 + 1;
                        arrayList4.add(new GFitWODistance((float) component13.getValues().get(i8).doubleValue(), millis + ((long) (i8 * i7)), ((long) (i9 * i7)) + millis));
                        i8 = i9;
                    }
                }
                if (component14 != null) {
                    int size4 = component14.getValues().size();
                    int i10 = size4 > 0 ? (int) ((((float) component5) / ((float) size4)) * ((float) 1000)) : 0;
                    int i11 = 0;
                    while (i11 < size4) {
                        int i12 = i11 + 1;
                        arrayList5.add(new GFitWOHeartRate((float) component14.getValues().get(i11).shortValue(), ((long) (i11 * i10)) + millis, ((long) (i12 * i10)) + millis));
                        i11 = i12;
                    }
                }
                if (millis <= 0 || millis2 <= 0) {
                    FLogger.INSTANCE.getLocal().d(a, "getGFitWorkoutSessions gFitWorkoutSession with invalid time");
                } else {
                    arrayList.add(new GFitWorkoutSession(millis, millis2, component6, arrayList2, arrayList3, arrayList4, arrayList5));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Lc6<List<HeartRateSample>, List<DailyHeartRateSummary>> e(List<FitnessDataWrapper> list, String str) {
        Object obj;
        float f;
        ArrayList arrayList;
        DailyHeartRateSummary dailyHeartRateSummary;
        DailyHeartRateSummary dailyHeartRateSummary2;
        HeartRateSample heartRateSample;
        Object obj2;
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        if (list.isEmpty()) {
            return new Lc6<>(arrayList2, arrayList3);
        }
        Pm7.b0(list, Mn7.b(Ci.INSTANCE, Di.INSTANCE));
        Calendar v = TimeUtils.v(list.get(0).getStartLongTime());
        v.set(13, 0);
        v.set(14, 0);
        Wg6.b(v, SampleRaw.COLUMN_START_TIME);
        v.setTimeZone(TimeUtils.t(list.get(0).getTimezoneOffsetInSecond() * 1000));
        long startLongTime = list.get(0).getStartLongTime();
        HeartRateWrapper heartRate = list.get(0).getHeartRate();
        Calendar v2 = TimeUtils.v((((long) (heartRate != null ? heartRate.getResolutionInSecond() : 0)) * 1000) + startLongTime);
        int i = v.get(10);
        int i2 = v.get(12);
        Date time = v.getTime();
        Wg6.b(time, "startTime.time");
        long currentTimeMillis = System.currentTimeMillis();
        long currentTimeMillis2 = System.currentTimeMillis();
        Wg6.b(v2, SampleRaw.COLUMN_END_TIME);
        HeartRateSample heartRateSample2 = new HeartRateSample((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, time, currentTimeMillis, currentTimeMillis2, new DateTime(v2.getTimeInMillis()), new DateTime(v.getTimeInMillis()), list.get(0).getTimezoneOffsetInSecond(), Integer.MAX_VALUE, (int) RecyclerView.UNDEFINED_DURATION, str, 0, (Resting) null, 2048, (Qg6) null);
        DailyHeartRateSummary dailyHeartRateSummary3 = new DailyHeartRateSummary(heartRateSample2.getAverage(), new Date(heartRateSample2.getStartTimeId().getMillis()), System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample2.getMin(), heartRateSample2.getMax(), heartRateSample2.getMinuteCount(), heartRateSample2.getResting());
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        TimeZone timeZone = TimeZone.getDefault();
        Wg6.b(timeZone, "TimeZone.getDefault()");
        int rawOffset = timeZone.getRawOffset() / 1000;
        Jh6 jh6 = new Jh6();
        int i3 = 0;
        int i4 = 0;
        int i5 = i2;
        int i6 = i;
        DailyHeartRateSummary dailyHeartRateSummary4 = dailyHeartRateSummary3;
        HeartRateSample heartRateSample3 = heartRateSample2;
        for (T t : list) {
            if (i4 >= 0) {
                T t2 = t;
                HeartRateWrapper heartRate2 = t2.getHeartRate();
                if (heartRate2 != null) {
                    int component1 = heartRate2.component1();
                    List<Short> component4 = heartRate2.component4();
                    int timezoneOffsetInSecond = t2.getTimezoneOffsetInSecond();
                    long millis = t2.getResting().isEmpty() ^ true ? t2.getResting().get(0).getStartTime().getMillis() : 0;
                    jh6.element = (T) t2.getResting();
                    FLogger.INSTANCE.getLocal().d(a, "Resting: restingStartTimeInSecond=" + millis + ", restingValues=" + Jj5.a(jh6.element));
                    String[] availableIDs = TimeZone.getAvailableIDs(rawOffset * 1000);
                    int i7 = 0;
                    f = f2;
                    dailyHeartRateSummary = dailyHeartRateSummary4;
                    for (T t3 : component4) {
                        if (i7 >= 0) {
                            short shortValue = t3.shortValue();
                            Calendar v3 = TimeUtils.v(t2.getStartLongTime() + (((long) (i7 * component1)) * 1000));
                            v3.set(13, 0);
                            v3.set(14, 0);
                            Wg6.b(v3, "currentStartTime");
                            v3.setTimeZone(TimeZone.getTimeZone(availableIDs[0]));
                            Object clone = v3.clone();
                            if (clone != null) {
                                Calendar calendar = (Calendar) clone;
                                calendar.add(13, component1);
                                Cd6 cd6 = Cd6.a;
                                int i8 = v3.get(10);
                                int i9 = v3.get(12);
                                if (i8 == i6 && i9 / 5 == i5 / 5 && timezoneOffsetInSecond == heartRateSample3.getTimezoneOffsetInSecond()) {
                                    if (shortValue > 0) {
                                        i3++;
                                        heartRateSample3.setMinuteCount(i3);
                                    }
                                    f += (float) shortValue;
                                    if (shortValue > 0) {
                                        if (heartRateSample3.getMin() == 0) {
                                            heartRateSample3.setMin(shortValue);
                                        } else if (shortValue < heartRateSample3.getMin()) {
                                            heartRateSample3.setMin(shortValue);
                                        }
                                    }
                                    if (shortValue > 0 && shortValue > heartRateSample3.getMax()) {
                                        heartRateSample3.setMax(shortValue);
                                    }
                                    heartRateSample3.setEndTime(new DateTime(calendar.getTime()));
                                    if (i3 > 0) {
                                        heartRateSample3.setAverage(f / ((float) i3));
                                    }
                                    for (RestingWrapper restingWrapper : jh6.element) {
                                        if (restingWrapper.getTimezoneOffsetInSecond() == heartRateSample3.getTimezoneOffsetInSecond() && heartRateSample3.getStartTime().getMillis() <= restingWrapper.getStartTime().getMillis() && restingWrapper.getStartTime().getMillis() <= heartRateSample3.getEndTime().getMillis()) {
                                            heartRateSample3.setResting(new Resting(heartRateSample3.getEndTime(), restingWrapper.getValue()));
                                        }
                                    }
                                    heartRateSample = heartRateSample3;
                                } else {
                                    if (heartRateSample3.getAverage() > ((float) 0)) {
                                        DateTime minusMinutes = heartRateSample3.getStartTimeId().minusMinutes(heartRateSample3.getStartTimeId().getMinuteOfHour() % 5);
                                        Wg6.b(minusMinutes, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
                                        heartRateSample3.setStartTimeId(minusMinutes);
                                        arrayList2.add(heartRateSample3);
                                        if (TimeUtils.m0(dailyHeartRateSummary.getDate(), heartRateSample3.getStartTimeId().toLocalDateTime().toDate())) {
                                            dailyHeartRateSummary2 = b.b(dailyHeartRateSummary, heartRateSample3);
                                        } else {
                                            Iterator it = arrayList3.iterator();
                                            while (true) {
                                                if (!it.hasNext()) {
                                                    obj2 = null;
                                                    break;
                                                }
                                                Object next = it.next();
                                                if (TimeUtils.m0(((DailyHeartRateSummary) next).getDate(), heartRateSample3.getDate())) {
                                                    obj2 = next;
                                                    break;
                                                }
                                            }
                                            DailyHeartRateSummary dailyHeartRateSummary5 = (DailyHeartRateSummary) obj2;
                                            if (dailyHeartRateSummary5 != null) {
                                                arrayList3.remove(dailyHeartRateSummary5);
                                                dailyHeartRateSummary2 = b.b(dailyHeartRateSummary5, heartRateSample3);
                                            } else {
                                                arrayList3.add(dailyHeartRateSummary);
                                                float average = heartRateSample3.getAverage();
                                                Date date = heartRateSample3.getStartTimeId().toLocalDateTime().toDate();
                                                Wg6.b(date, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                                                dailyHeartRateSummary2 = new DailyHeartRateSummary(average, date, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample3.getMin(), heartRateSample3.getMax(), heartRateSample3.getMinuteCount(), heartRateSample3.getResting());
                                            }
                                        }
                                    } else {
                                        dailyHeartRateSummary2 = dailyHeartRateSummary;
                                    }
                                    int i10 = v3.get(10);
                                    int i11 = v3.get(12);
                                    f = (float) shortValue;
                                    i3 = shortValue > 0 ? 1 : 0;
                                    Date time2 = v3.getTime();
                                    Wg6.b(time2, "currentStartTime.time");
                                    long currentTimeMillis3 = System.currentTimeMillis();
                                    long currentTimeMillis4 = System.currentTimeMillis();
                                    int i12 = timezoneOffsetInSecond / 3600;
                                    DateTime withZone = new DateTime(calendar.getTimeInMillis()).withZone(DateTimeZone.forOffsetHours(i12));
                                    Wg6.b(withZone, "DateTime(currentEndTime.\u2026neOffsetInSecond / 3600))");
                                    DateTime withZone2 = new DateTime(v3.getTimeInMillis()).withZone(DateTimeZone.forOffsetHours(i12));
                                    Wg6.b(withZone2, "DateTime(currentStartTim\u2026neOffsetInSecond / 3600))");
                                    heartRateSample = new HeartRateSample(f, time2, currentTimeMillis3, currentTimeMillis4, withZone, withZone2, timezoneOffsetInSecond, shortValue, shortValue, str, i3, (Resting) null, 2048, (Qg6) null);
                                    FLogger.INSTANCE.getLocal().d(a, "getHeartRateData startTime=" + v3.getTime() + " endTime=" + calendar.getTime());
                                    i5 = i11;
                                    i6 = i10;
                                    dailyHeartRateSummary = dailyHeartRateSummary2;
                                }
                                Cd6 cd62 = Cd6.a;
                                i7++;
                                heartRateSample3 = heartRateSample;
                            } else {
                                throw new Rc6("null cannot be cast to non-null type java.util.Calendar");
                            }
                        } else {
                            Hm7.l();
                            throw null;
                        }
                    }
                    Cd6 cd63 = Cd6.a;
                    arrayList = arrayList2;
                } else {
                    f = f2;
                    arrayList = arrayList2;
                    dailyHeartRateSummary = dailyHeartRateSummary4;
                }
                i4++;
                f2 = f;
                jh6 = jh6;
                dailyHeartRateSummary4 = dailyHeartRateSummary;
                arrayList2 = arrayList;
                arrayList3 = arrayList3;
            } else {
                Hm7.l();
                throw null;
            }
        }
        float f3 = (float) 0;
        if (heartRateSample3.getAverage() > f3) {
            DateTime minusMinutes2 = heartRateSample3.getStartTimeId().minusMinutes(heartRateSample3.getStartTimeId().getMinuteOfHour() % 5);
            Wg6.b(minusMinutes2, "currentHeartRate.getStar\u2026inusMinutes(minusMinutes)");
            heartRateSample3.setStartTimeId(minusMinutes2);
            arrayList2.add(heartRateSample3);
            if (TimeUtils.m0(dailyHeartRateSummary4.getDate(), heartRateSample3.getStartTimeId().toLocalDateTime().toDate())) {
                arrayList3.add(b(dailyHeartRateSummary4, heartRateSample3));
            } else {
                Iterator it2 = arrayList3.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        obj = null;
                        break;
                    }
                    Object next2 = it2.next();
                    if (TimeUtils.m0(((DailyHeartRateSummary) next2).getDate(), heartRateSample3.getDate())) {
                        obj = next2;
                        break;
                    }
                }
                DailyHeartRateSummary dailyHeartRateSummary6 = (DailyHeartRateSummary) obj;
                if (dailyHeartRateSummary6 != null) {
                    arrayList3.remove(dailyHeartRateSummary6);
                    arrayList3.add(b(dailyHeartRateSummary6, heartRateSample3));
                } else {
                    arrayList3.add(dailyHeartRateSummary4);
                    float average2 = heartRateSample3.getAverage();
                    Date date2 = heartRateSample3.getStartTimeId().toLocalDateTime().toDate();
                    Wg6.b(date2, "currentHeartRate.getStar\u2026oLocalDateTime().toDate()");
                    arrayList3.add(new DailyHeartRateSummary(average2, date2, System.currentTimeMillis(), System.currentTimeMillis(), heartRateSample3.getMin(), heartRateSample3.getMax(), heartRateSample3.getMinuteCount(), heartRateSample3.getResting()));
                }
            }
        } else if (dailyHeartRateSummary4.getAverage() > f3) {
            arrayList3.add(dailyHeartRateSummary4);
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "heartrate " + arrayList2 + " \n summary " + arrayList3);
        return new Lc6<>(arrayList2, arrayList3);
    }

    @DexIgnore
    public final List<WorkoutSession> f(List<FitnessDataWrapper> list, String str, String str2) {
        FLogger.INSTANCE.getLocal().d(a, "getWorkoutData");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        Pm7.b0(list, new Ei());
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            for (T t : it.next().getWorkouts()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = a;
                local.d(str3, "getWorkoutData - value=" + ((Object) t));
                arrayList.add(new WorkoutSession(t, str, str2));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final Ai g(String str, List<FitnessDataWrapper> list, MFUser mFUser, UserProfile userProfile, long j, long j2, PortfolioApp portfolioApp) {
        int i;
        portfolioApp.p(CommunicateMode.SYNC, str, "Calculating sleep and activity...");
        String userId = mFUser.getUserId();
        Gl7<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> d = SyncDataExtensions.d(list, str, userId, 1000 * j);
        List<ActivitySample> first = d.getFirst();
        List<ActivitySummary> second = d.getSecond();
        List<GFitSample> third = d.getThird();
        List<MFSleepSession> e = SyncDataExtensions.e(list, str);
        Lc6<List<HeartRateSample>, List<DailyHeartRateSummary>> e2 = e(list, userId);
        List<HeartRateSample> first2 = e2.getFirst();
        List<DailyHeartRateSummary> second2 = e2.getSecond();
        List<WorkoutSession> f = f(list, str, userId);
        List<GFitHeartRate> c = c(list);
        List<GFitWorkoutSession> d2 = d(list);
        int size = e.size();
        double d3 = 0.0d;
        int i2 = 0;
        int i3 = 0;
        double d4 = 0.0d;
        double d5 = 0.0d;
        double d6 = 0.0d;
        for (T t : first) {
            d4 += t.getCalories();
            d5 += t.getDistance();
            d3 += t.getSteps();
            int activeTime = t.getActiveTime() + i2;
            Boolean p0 = TimeUtils.p0(t.getDate());
            Wg6.b(p0, "DateHelper.isToday(it.date)");
            if (p0.booleanValue()) {
                d6 += t.getSteps();
                i = t.getActiveTime() + i3;
            } else {
                i = i3;
            }
            i3 = i;
            i2 = activeTime;
        }
        Hr7 hr7 = Hr7.a;
        String format = String.format("Done calculating sleep: total %s sleep session(s)", Arrays.copyOf(new Object[]{String.valueOf(size)}, 1));
        Wg6.b(format, "java.lang.String.format(format, *args)");
        h(str, format);
        Hr7 hr72 = Hr7.a;
        String format2 = String.format("After calculation: steps=%s, todayStep=%s, realTimeSteps=%s, lastRealtimeSteps=%s, Calories=%s, DistanceWrapper=%s, ActiveTime=%s, TodayActiveTime=%s", Arrays.copyOf(new Object[]{Double.valueOf(d3), Double.valueOf(d6), Long.valueOf(j2), Long.valueOf(userProfile.getCurrentSteps()), Double.valueOf(d4), Double.valueOf(d5), Integer.valueOf(i2), Integer.valueOf(i3)}, 8));
        Wg6.b(format2, "java.lang.String.format(format, *args)");
        h(str, format2);
        h(str, "HeartRateWrapper data size: " + first2.size());
        FLogger.INSTANCE.getLocal().d(a, "Release=" + Tk5.d());
        if (!Tk5.d()) {
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Sleep sessions details: " + new Gson().t(e));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - SampleRaw list details: " + new Gson().t(first));
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate sample list details: " + first2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Heart Rate summary list details: " + second2);
            FLogger.INSTANCE.getLocal().d(a, "onSyncCompleted - Workout list details: " + new Gson().t(f));
        }
        return new Ai(j2, first, second, e, first2, second2, SyncDataExtensions.b(list), f, third, c, d2);
    }

    @DexIgnore
    public final void h(String str, String str2) {
        PortfolioApp.get.instance().p(CommunicateMode.SYNC, str, str2);
        FLogger.INSTANCE.getLocal().d(a, str2);
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str, a, str2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x07ef  */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x081e  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x08fa  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x08fe  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:140:0x09e9  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x0a12  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0b5f  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x0b7e  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x0bb1  */
    /* JADX WARNING: Removed duplicated region for block: B:200:0x0bc2  */
    /* JADX WARNING: Removed duplicated region for block: B:227:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:229:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:232:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:233:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0202  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x032d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0345  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x03e3  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x05cb  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x05e7  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0674  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x0710  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object i(com.portfolio.platform.service.usecase.DianaSyncDataProcessing.Ai r41, java.lang.String r42, com.portfolio.platform.data.source.SleepSessionsRepository r43, com.portfolio.platform.data.source.SummariesRepository r44, com.portfolio.platform.data.source.SleepSummariesRepository r45, com.portfolio.platform.data.source.HeartRateSampleRepository r46, com.portfolio.platform.data.source.HeartRateSummaryRepository r47, com.portfolio.platform.data.source.WorkoutSessionRepository r48, com.portfolio.platform.data.source.FitnessDataRepository r49, com.portfolio.platform.data.source.ActivitiesRepository r50, com.portfolio.platform.data.source.ThirdPartyRepository r51, com.portfolio.platform.PortfolioApp r52, com.portfolio.platform.helper.FitnessHelper r53, com.mapped.Xe6<? super com.mapped.Cd6> r54) {
        /*
        // Method dump skipped, instructions count: 3100
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.service.usecase.DianaSyncDataProcessing.i(com.portfolio.platform.service.usecase.DianaSyncDataProcessing$Ai, java.lang.String, com.portfolio.platform.data.source.SleepSessionsRepository, com.portfolio.platform.data.source.SummariesRepository, com.portfolio.platform.data.source.SleepSummariesRepository, com.portfolio.platform.data.source.HeartRateSampleRepository, com.portfolio.platform.data.source.HeartRateSummaryRepository, com.portfolio.platform.data.source.WorkoutSessionRepository, com.portfolio.platform.data.source.FitnessDataRepository, com.portfolio.platform.data.source.ActivitiesRepository, com.portfolio.platform.data.source.ThirdPartyRepository, com.portfolio.platform.PortfolioApp, com.portfolio.platform.helper.FitnessHelper, com.mapped.Xe6):java.lang.Object");
    }
}
