package com.portfolio.platform.data;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AccessGroup {
    @DexIgnore
    public /* final */ Access lollipop;
    @DexIgnore
    public /* final */ Access oreo;
    @DexIgnore
    public /* final */ Access pie;

    @DexIgnore
    public AccessGroup() {
        this(null, null, null, 7, null);
    }

    @DexIgnore
    public AccessGroup(Access access, Access access2, Access access3) {
        this.pie = access;
        this.oreo = access2;
        this.lollipop = access3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AccessGroup(Access access, Access access2, Access access3, int i, Qg6 qg6) {
        this((i & 1) != 0 ? null : access, (i & 2) != 0 ? null : access2, (i & 4) != 0 ? null : access3);
    }

    @DexIgnore
    public static /* synthetic */ AccessGroup copy$default(AccessGroup accessGroup, Access access, Access access2, Access access3, int i, Object obj) {
        if ((i & 1) != 0) {
            access = accessGroup.pie;
        }
        if ((i & 2) != 0) {
            access2 = accessGroup.oreo;
        }
        if ((i & 4) != 0) {
            access3 = accessGroup.lollipop;
        }
        return accessGroup.copy(access, access2, access3);
    }

    @DexIgnore
    public final Access component1() {
        return this.pie;
    }

    @DexIgnore
    public final Access component2() {
        return this.oreo;
    }

    @DexIgnore
    public final Access component3() {
        return this.lollipop;
    }

    @DexIgnore
    public final AccessGroup copy(Access access, Access access2, Access access3) {
        return new AccessGroup(access, access2, access3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AccessGroup) {
                AccessGroup accessGroup = (AccessGroup) obj;
                if (!Wg6.a(this.pie, accessGroup.pie) || !Wg6.a(this.oreo, accessGroup.oreo) || !Wg6.a(this.lollipop, accessGroup.lollipop)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Access getLollipop() {
        return this.lollipop;
    }

    @DexIgnore
    public final Access getOreo() {
        return this.oreo;
    }

    @DexIgnore
    public final Access getPie() {
        return this.pie;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Access access = this.pie;
        int hashCode = access != null ? access.hashCode() : 0;
        Access access2 = this.oreo;
        int hashCode2 = access2 != null ? access2.hashCode() : 0;
        Access access3 = this.lollipop;
        if (access3 != null) {
            i = access3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "AccessGroup(pie=" + this.pie + ", oreo=" + this.oreo + ", lollipop=" + this.lollipop + ")";
    }
}
