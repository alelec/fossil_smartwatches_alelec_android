package com.portfolio.platform.data;

import android.util.Base64;
import com.facebook.places.PlaceManager;
import com.fossil.Gi5;
import com.fossil.Im7;
import com.fossil.Ji5;
import com.fossil.Mi5;
import com.fossil.Pm7;
import com.fossil.fitness.GpsDataEncoder;
import com.fossil.fitness.GpsDataPoint;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.diana.workout.WorkoutCadence;
import com.portfolio.platform.data.model.diana.workout.WorkoutCalorie;
import com.portfolio.platform.data.model.diana.workout.WorkoutDistance;
import com.portfolio.platform.data.model.diana.workout.WorkoutGpsPoint;
import com.portfolio.platform.data.model.diana.workout.WorkoutHeartRate;
import com.portfolio.platform.data.model.diana.workout.WorkoutPace;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.diana.workout.WorkoutSpeed;
import com.portfolio.platform.data.model.diana.workout.WorkoutStateChange;
import com.portfolio.platform.data.model.diana.workout.WorkoutStep;
import com.portfolio.platform.data.model.fitnessdata.GpsDataPointWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerWorkoutSession extends ServerError {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    @Vu3("cadence")
    public /* final */ WorkoutCadence cadence;
    @DexIgnore
    @Vu3(MFSleepSession.COLUMN_EDITED_END_TIME)
    public /* final */ String editedEndTime;
    @DexIgnore
    @Vu3("editedMode")
    public /* final */ Gi5 editedMode;
    @DexIgnore
    @Vu3(MFSleepSession.COLUMN_EDITED_START_TIME)
    public /* final */ String editedStartTime;
    @DexIgnore
    @Vu3("editedType")
    public /* final */ Mi5 editedType;
    @DexIgnore
    @Vu3("gpsDataPoints")
    public /* final */ String gpsDataPoints;
    @DexIgnore
    @Vu3("calorie")
    public /* final */ WorkoutCalorie mCalorie;
    @DexIgnore
    @Vu3("createdAt")
    public /* final */ DateTime mCreatedAt;
    @DexIgnore
    @Vu3("date")
    public /* final */ Date mDate;
    @DexIgnore
    @Vu3(MFSleepSession.COLUMN_DEVICE_SERIAL_NUMBER)
    public /* final */ String mDeviceSerialNumber;
    @DexIgnore
    @Vu3("distance")
    public /* final */ WorkoutDistance mDistance;
    @DexIgnore
    @Vu3("duration")
    public /* final */ int mDuration;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @Vu3("heartRate")
    public /* final */ WorkoutHeartRate mHeartRate;
    @DexIgnore
    @Vu3("id")
    public /* final */ String mId;
    @DexIgnore
    @Vu3("source")
    public /* final */ Ji5 mSourceType;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @Vu3("step")
    public /* final */ WorkoutStep mStep;
    @DexIgnore
    @Vu3("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @Vu3("updatedAt")
    public /* final */ DateTime mUpdatedAt;
    @DexIgnore
    @Vu3("type")
    public /* final */ Mi5 mWorkoutType;
    @DexIgnore
    @Vu3("mode")
    public /* final */ Gi5 mode;
    @DexIgnore
    @Vu3("pace")
    public /* final */ WorkoutPace pace;
    @DexIgnore
    @Vu3(PlaceManager.PARAM_SPEED)
    public WorkoutSpeed speed;
    @DexIgnore
    @Vu3("states")
    public List<WorkoutStateChange> states;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerWorkoutSession.class.getSimpleName();
        Wg6.b(simpleName, "ServerWorkoutSession::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, Ji5 ji5, WorkoutStep workoutStep, int i, Mi5 mi5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List<WorkoutStateChange> list, String str5, String str6, Mi5 mi52, Gi5 gi5, String str7, Gi5 gi52, WorkoutPace workoutPace, WorkoutCadence workoutCadence) {
        Wg6.c(str, "mId");
        Wg6.c(date, "mDate");
        Wg6.c(str3, "mStartTime");
        Wg6.c(str4, "mEndTime");
        Wg6.c(dateTime, "mCreatedAt");
        Wg6.c(dateTime2, "mUpdatedAt");
        Wg6.c(list, "states");
        this.mId = str;
        this.mCalorie = workoutCalorie;
        this.mDate = date;
        this.mDeviceSerialNumber = str2;
        this.mDistance = workoutDistance;
        this.mStartTime = str3;
        this.mEndTime = str4;
        this.mHeartRate = workoutHeartRate;
        this.mSourceType = ji5;
        this.mStep = workoutStep;
        this.mTimeZoneOffsetInSecond = i;
        this.mWorkoutType = mi5;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
        this.mDuration = i2;
        this.speed = workoutSpeed;
        this.states = list;
        this.editedStartTime = str5;
        this.editedEndTime = str6;
        this.editedType = mi52;
        this.editedMode = gi5;
        this.gpsDataPoints = str7;
        this.mode = gi52;
        this.pace = workoutPace;
        this.cadence = workoutCadence;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ServerWorkoutSession(String str, WorkoutCalorie workoutCalorie, Date date, String str2, WorkoutDistance workoutDistance, String str3, String str4, WorkoutHeartRate workoutHeartRate, Ji5 ji5, WorkoutStep workoutStep, int i, Mi5 mi5, DateTime dateTime, DateTime dateTime2, int i2, WorkoutSpeed workoutSpeed, List list, String str5, String str6, Mi5 mi52, Gi5 gi5, String str7, Gi5 gi52, WorkoutPace workoutPace, WorkoutCadence workoutCadence, int i3, Qg6 qg6) {
        this(str, workoutCalorie, date, str2, workoutDistance, str3, str4, workoutHeartRate, ji5, workoutStep, i, mi5, dateTime, dateTime2, i2, workoutSpeed, (65536 & i3) != 0 ? new ArrayList() : list, str5, str6, mi52, gi5, str7, gi52, workoutPace, workoutCadence);
    }

    @DexIgnore
    public final WorkoutCadence getCadence() {
        return this.cadence;
    }

    @DexIgnore
    public final String getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public final Gi5 getEditedMode() {
        return this.editedMode;
    }

    @DexIgnore
    public final String getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public final Mi5 getEditedType() {
        return this.editedType;
    }

    @DexIgnore
    public final String getGpsDataPoints() {
        return this.gpsDataPoints;
    }

    @DexIgnore
    public final WorkoutCalorie getMCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final String getMDeviceSerialNumber() {
        return this.mDeviceSerialNumber;
    }

    @DexIgnore
    public final WorkoutDistance getMDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public final int getMDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public final String getMEndTime() {
        return this.mEndTime;
    }

    @DexIgnore
    public final WorkoutHeartRate getMHeartRate() {
        return this.mHeartRate;
    }

    @DexIgnore
    public final String getMId() {
        return this.mId;
    }

    @DexIgnore
    public final Ji5 getMSourceType() {
        return this.mSourceType;
    }

    @DexIgnore
    public final String getMStartTime() {
        return this.mStartTime;
    }

    @DexIgnore
    public final WorkoutStep getMStep() {
        return this.mStep;
    }

    @DexIgnore
    public final int getMTimeZoneOffsetInSecond() {
        return this.mTimeZoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final Mi5 getMWorkoutType() {
        return this.mWorkoutType;
    }

    @DexIgnore
    public final Gi5 getMode() {
        return this.mode;
    }

    @DexIgnore
    public final WorkoutPace getPace() {
        return this.pace;
    }

    @DexIgnore
    public final WorkoutSpeed getSpeed() {
        return this.speed;
    }

    @DexIgnore
    public final List<WorkoutStateChange> getStates() {
        return this.states;
    }

    @DexIgnore
    public final void setSpeed(WorkoutSpeed workoutSpeed) {
        this.speed = workoutSpeed;
    }

    @DexIgnore
    public final void setStates(List<WorkoutStateChange> list) {
        Wg6.c(list, "<set-?>");
        this.states = list;
    }

    @DexIgnore
    public final WorkoutSession toWorkoutSession() {
        WorkoutSession workoutSession;
        Exception e;
        DateTime dateTime;
        DateTime dateTime2;
        List list;
        try {
            DateTimeFormatter withZone = ISODateTimeFormat.dateTime().withZone(DateTimeZone.forOffsetMillis(this.mTimeZoneOffsetInSecond * 1000));
            DateTime parseDateTime = withZone.parseDateTime(this.mStartTime);
            DateTime parseDateTime2 = withZone.parseDateTime(this.mEndTime);
            if (this.editedStartTime == null || (dateTime = withZone.parseDateTime(this.editedStartTime)) == null) {
                dateTime = parseDateTime;
            }
            if (this.editedEndTime == null || (dateTime2 = withZone.parseDateTime(this.editedEndTime)) == null) {
                dateTime2 = parseDateTime2;
            }
            Mi5 mi5 = this.editedType;
            if (mi5 == null) {
                mi5 = this.mWorkoutType;
            }
            Gi5 gi5 = this.editedMode;
            if (gi5 == null) {
                gi5 = this.mode;
            }
            String str = this.gpsDataPoints;
            if (str != null) {
                byte[] decode = Base64.decode(str, 0);
                IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                FLogger.Component component = FLogger.Component.APP;
                FLogger.Session session = FLogger.Session.SYNC;
                StringBuilder sb = new StringBuilder();
                sb.append("[Calculate GPS Points] Encoded GPS string from server ");
                sb.append(decode != null ? Integer.valueOf(decode.length) : null);
                sb.append(' ');
                remote.i(component, session, "", "WorkoutSessionWrapper", sb.toString());
                ArrayList<GpsDataPoint> decode2 = GpsDataEncoder.decode(decode);
                Wg6.b(decode2, "GpsDataEncoder.decode(encodedRawData)");
                ArrayList arrayList = new ArrayList(Im7.m(decode2, 10));
                for (T t : decode2) {
                    Wg6.b(t, "it");
                    arrayList.add(new WorkoutGpsPoint(new GpsDataPointWrapper(t, this.mTimeZoneOffsetInSecond)));
                }
                list = Pm7.j0(arrayList);
            } else {
                list = null;
            }
            IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
            FLogger.Component component2 = FLogger.Component.APP;
            FLogger.Session session2 = FLogger.Session.SYNC;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("[Calculate GPS Points] GPS points size after decrypt from server ");
            sb2.append(list != null ? Integer.valueOf(list.size()) : null);
            sb2.append(' ');
            remote2.i(component2, session2, "", "WorkoutSessionWrapper", sb2.toString());
            String str2 = this.mId;
            Date date = this.mDate;
            Wg6.b(parseDateTime, SampleRaw.COLUMN_START_TIME);
            Wg6.b(parseDateTime2, SampleRaw.COLUMN_END_TIME);
            String str3 = this.mDeviceSerialNumber;
            WorkoutStep workoutStep = this.mStep;
            WorkoutCalorie workoutCalorie = this.mCalorie;
            WorkoutDistance workoutDistance = this.mDistance;
            WorkoutHeartRate workoutHeartRate = this.mHeartRate;
            Ji5 ji5 = this.mSourceType;
            Mi5 mi52 = this.mWorkoutType;
            int i = this.mTimeZoneOffsetInSecond;
            int i2 = this.mDuration;
            long millis = this.mCreatedAt.getMillis();
            long millis2 = this.mUpdatedAt.getMillis();
            String str4 = this.gpsDataPoints;
            Gi5 gi52 = this.mode;
            WorkoutPace workoutPace = this.pace;
            WorkoutCadence workoutCadence = this.cadence;
            Wg6.b(dateTime, MFSleepSession.COLUMN_EDITED_START_TIME);
            Wg6.b(dateTime2, MFSleepSession.COLUMN_EDITED_END_TIME);
            workoutSession = new WorkoutSession(str2, date, parseDateTime, parseDateTime2, str3, workoutStep, workoutCalorie, workoutDistance, workoutHeartRate, ji5, mi52, i, i2, millis, millis2, list, str4, gi52, workoutPace, workoutCadence, dateTime, dateTime2, mi5, gi5);
            try {
                try {
                    workoutSession.setSpeed(this.speed);
                    workoutSession.getStates().addAll(this.states);
                } catch (Exception e2) {
                    e = e2;
                }
            } catch (Exception e3) {
                e = e3;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str5 = TAG;
                StringBuilder sb3 = new StringBuilder();
                sb3.append("toWorkoutSession exception=");
                e.printStackTrace();
                sb3.append(Cd6.a);
                local.d(str5, sb3.toString());
                e.printStackTrace();
                return workoutSession;
            }
        } catch (Exception e4) {
            e = e4;
            workoutSession = null;
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str52 = TAG;
            StringBuilder sb32 = new StringBuilder();
            sb32.append("toWorkoutSession exception=");
            e.printStackTrace();
            sb32.append(Cd6.a);
            local2.d(str52, sb32.toString());
            e.printStackTrace();
            return workoutSession;
        }
        return workoutSession;
    }
}
