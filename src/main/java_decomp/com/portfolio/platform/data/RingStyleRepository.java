package com.portfolio.platform.data;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.remote.RingStyleRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "RingStyleRepository";
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ RingStyleRemoteDataSource mRingStyleRemoveSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public RingStyleRepository(RingStyleRemoteDataSource ringStyleRemoteDataSource, FileRepository fileRepository) {
        Wg6.c(ringStyleRemoteDataSource, "mRingStyleRemoveSource");
        Wg6.c(fileRepository, "mFileRepository");
        this.mRingStyleRemoveSource = ringStyleRemoteDataSource;
        this.mFileRepository = fileRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.RingStyleRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.data.RingStyleRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.RingStyleRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.RingStyleRepository r0 = (com.portfolio.platform.data.RingStyleRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.RingStyleDao r0 = r0.getRingStyleDao()
            r0.clearAllData()
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.RingStyleRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.RingStyleRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.cleanUp(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x015e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x017d  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x019c  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x01cc  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0226 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x02a1  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x02b0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadRingStyleList(java.lang.String r21, boolean r22, com.mapped.Xe6<? super com.mapped.Cd6> r23) {
        /*
        // Method dump skipped, instructions count: 737
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.downloadRingStyleList(java.lang.String, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRingStyles(com.mapped.Xe6<? super java.util.List<com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.RingStyleRepository$getRingStyles$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.RingStyleRepository$getRingStyles$Anon1 r0 = (com.portfolio.platform.data.RingStyleRepository$getRingStyles$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.RingStyleRepository r0 = (com.portfolio.platform.data.RingStyleRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.RingStyleDao r0 = r0.getRingStyleDao()
            java.util.List r0 = r0.getRingStyles()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.RingStyleRepository$getRingStyles$Anon1 r0 = new com.portfolio.platform.data.RingStyleRepository$getRingStyles$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.getRingStyles(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRingStylesAsLiveData(com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle>>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.RingStyleRepository$getRingStylesAsLiveData$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.RingStyleRepository$getRingStylesAsLiveData$Anon1 r0 = (com.portfolio.platform.data.RingStyleRepository$getRingStylesAsLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.RingStyleRepository r0 = (com.portfolio.platform.data.RingStyleRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.RingStyleDao r0 = r0.getRingStyleDao()
            androidx.lifecycle.LiveData r0 = r0.getRingStylesAsLiveData()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.RingStyleRepository$getRingStylesAsLiveData$Anon1 r0 = new com.portfolio.platform.data.RingStyleRepository$getRingStylesAsLiveData$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.getRingStylesAsLiveData(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getRingStylesBySerial(java.lang.String r6, com.mapped.Xe6<? super com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.RingStyleRepository$getRingStylesBySerial$Anon1
            if (r0 == 0) goto L_0x0039
            r0 = r7
            com.portfolio.platform.data.RingStyleRepository$getRingStylesBySerial$Anon1 r0 = (com.portfolio.platform.data.RingStyleRepository$getRingStylesBySerial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0039
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0048
            if (r3 != r4) goto L_0x0040
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.RingStyleRepository r1 = (com.portfolio.platform.data.RingStyleRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.RingStyleDao r0 = r0.getRingStyleDao()
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r0 = r0.getRingStylesBySerial(r6)
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.data.RingStyleRepository$getRingStylesBySerial$Anon1 r0 = new com.portfolio.platform.data.RingStyleRepository$getRingStylesBySerial$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0040:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0048:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.getRingStylesBySerial(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveRingStyle(com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r6, com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.RingStyleRepository$saveRingStyle$Anon1
            if (r0 == 0) goto L_0x003a
            r0 = r7
            com.portfolio.platform.data.RingStyleRepository$saveRingStyle$Anon1 r0 = (com.portfolio.platform.data.RingStyleRepository$saveRingStyle$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x003a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x0049
            if (r3 != r4) goto L_0x0041
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle r0 = (com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.RingStyleRepository r1 = (com.portfolio.platform.data.RingStyleRepository) r1
            com.fossil.El7.b(r2)
            r1 = r2
            r6 = r0
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.RingStyleDao r0 = r0.getRingStyleDao()
            r0.insertRingStyle(r6)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0039:
            return r0
        L_0x003a:
            com.portfolio.platform.data.RingStyleRepository$saveRingStyle$Anon1 r0 = new com.portfolio.platform.data.RingStyleRepository$saveRingStyle$Anon1
            r0.<init>(r5, r7)
            r1 = r0
            goto L_0x0014
        L_0x0041:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0049:
            com.fossil.El7.b(r2)
            com.portfolio.platform.manager.EncryptedDatabaseManager r2 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r1.L$0 = r5
            r1.L$1 = r6
            r1.label = r4
            java.lang.Object r1 = r2.v(r1)
            if (r1 != r0) goto L_0x002d
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.RingStyleRepository.saveRingStyle(com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle, com.mapped.Xe6):java.lang.Object");
    }
}
