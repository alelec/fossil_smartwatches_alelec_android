package com.portfolio.platform.data;

import com.mapped.Vu3;
import com.misfit.frameworks.buttonservice.model.Alarm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParamSkuModel {
    @DexIgnore
    @Vu3("date")
    public DateResponse date;
    @DexIgnore
    @Vu3("deviceLongName")
    public String deviceLongName;
    @DexIgnore
    @Vu3("deviceShortName")
    public String deviceShortName;
    @DexIgnore
    @Vu3("enableAlarmAnimation")
    public boolean enableAlarmAnimation;
    @DexIgnore
    @Vu3("mainHandsFlipped")
    public boolean mainHandsFlipped;
    @DexIgnore
    @Vu3("progress")
    public Progress progress;
    @DexIgnore
    @Vu3("rawBase64")
    public String rawBase64;
    @DexIgnore
    @Vu3("subeye")
    public Subeye subeye;
    @DexIgnore
    @Vu3("version")
    public String version;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DateResponse {
        @DexIgnore
        @Vu3("direction")
        public String direction;
        @DexIgnore
        @Vu3("endAngle")
        public String endAngle;
        @DexIgnore
        @Vu3("hand")
        public String hand;
        @DexIgnore
        @Vu3("startAngle")
        public String startAngle;

        @DexIgnore
        public final String getDirection() {
            return this.direction;
        }

        @DexIgnore
        public final String getEndAngle() {
            return this.endAngle;
        }

        @DexIgnore
        public final String getHand() {
            return this.hand;
        }

        @DexIgnore
        public final String getStartAngle() {
            return this.startAngle;
        }

        @DexIgnore
        public final void setDirection(String str) {
            this.direction = str;
        }

        @DexIgnore
        public final void setEndAngle(String str) {
            this.endAngle = str;
        }

        @DexIgnore
        public final void setHand(String str) {
            this.hand = str;
        }

        @DexIgnore
        public final void setStartAngle(String str) {
            this.startAngle = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Progress {
        @DexIgnore
        @Vu3("direction")
        public String direction;
        @DexIgnore
        @Vu3("endAngle")
        public String endAngle;
        @DexIgnore
        @Vu3("hand")
        public String hand;
        @DexIgnore
        @Vu3("startAngle")
        public String startAngle;

        @DexIgnore
        public final String getDirection() {
            return this.direction;
        }

        @DexIgnore
        public final String getEndAngle() {
            return this.endAngle;
        }

        @DexIgnore
        public final String getHand() {
            return this.hand;
        }

        @DexIgnore
        public final String getStartAngle() {
            return this.startAngle;
        }

        @DexIgnore
        public final void setDirection(String str) {
            this.direction = str;
        }

        @DexIgnore
        public final void setEndAngle(String str) {
            this.endAngle = str;
        }

        @DexIgnore
        public final void setHand(String str) {
            this.hand = str;
        }

        @DexIgnore
        public final void setStartAngle(String str) {
            this.startAngle = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Subeye {
        @DexIgnore
        @Vu3(Alarm.TABLE_NAME)
        public String alarm;
        @DexIgnore
        @Vu3("alert")
        public String alert;
        @DexIgnore
        @Vu3("date")
        public String date;
        @DexIgnore
        @Vu3("time2")
        public String time2;

        @DexIgnore
        public final String getAlarm() {
            return this.alarm;
        }

        @DexIgnore
        public final String getAlert() {
            return this.alert;
        }

        @DexIgnore
        public final String getDate() {
            return this.date;
        }

        @DexIgnore
        public final String getTime2() {
            return this.time2;
        }

        @DexIgnore
        public final void setAlarm(String str) {
            this.alarm = str;
        }

        @DexIgnore
        public final void setAlert(String str) {
            this.alert = str;
        }

        @DexIgnore
        public final void setDate(String str) {
            this.date = str;
        }

        @DexIgnore
        public final void setTime2(String str) {
            this.time2 = str;
        }
    }

    @DexIgnore
    public final DateResponse getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getDeviceLongName() {
        return this.deviceLongName;
    }

    @DexIgnore
    public final String getDeviceShortName() {
        return this.deviceShortName;
    }

    @DexIgnore
    public final boolean getEnableAlarmAnimation() {
        return this.enableAlarmAnimation;
    }

    @DexIgnore
    public final boolean getMainHandsFlipped() {
        return this.mainHandsFlipped;
    }

    @DexIgnore
    public final Progress getProgress() {
        return this.progress;
    }

    @DexIgnore
    public final String getRawBase64() {
        return this.rawBase64;
    }

    @DexIgnore
    public final Subeye getSubeye() {
        return this.subeye;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public final void setDate(DateResponse dateResponse) {
        this.date = dateResponse;
    }

    @DexIgnore
    public final void setDeviceLongName(String str) {
        this.deviceLongName = str;
    }

    @DexIgnore
    public final void setDeviceShortName(String str) {
        this.deviceShortName = str;
    }

    @DexIgnore
    public final void setEnableAlarmAnimation(boolean z) {
        this.enableAlarmAnimation = z;
    }

    @DexIgnore
    public final void setMainHandsFlipped(boolean z) {
        this.mainHandsFlipped = z;
    }

    @DexIgnore
    public final void setProgress(Progress progress2) {
        this.progress = progress2;
    }

    @DexIgnore
    public final void setRawBase64(String str) {
        this.rawBase64 = str;
    }

    @DexIgnore
    public final void setSubeye(Subeye subeye2) {
        this.subeye = subeye2;
    }

    @DexIgnore
    public final void setVersion(String str) {
        this.version = str;
    }
}
