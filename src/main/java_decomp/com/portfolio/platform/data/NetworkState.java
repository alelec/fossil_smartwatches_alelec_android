package com.portfolio.platform.data;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NetworkState {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ NetworkState LOADED; // = new NetworkState(Status.SUCCESS, null, 2, null);
    @DexIgnore
    public static /* final */ NetworkState LOADING; // = new NetworkState(Status.RUNNING, null, 2, null);
    @DexIgnore
    public /* final */ String msg;
    @DexIgnore
    public /* final */ Status status;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final NetworkState error(String str) {
            return new NetworkState(Status.FAILED, str, null);
        }

        @DexIgnore
        public final NetworkState getLOADED() {
            return NetworkState.LOADED;
        }

        @DexIgnore
        public final NetworkState getLOADING() {
            return NetworkState.LOADING;
        }
    }

    @DexIgnore
    public NetworkState(Status status2, String str) {
        this.status = status2;
        this.msg = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ NetworkState(Status status2, String str, int i, Qg6 qg6) {
        this(status2, (i & 2) != 0 ? null : str);
    }

    @DexIgnore
    public /* synthetic */ NetworkState(Status status2, String str, Qg6 qg6) {
        this(status2, str);
    }

    @DexIgnore
    public static /* synthetic */ NetworkState copy$default(NetworkState networkState, Status status2, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            status2 = networkState.status;
        }
        if ((i & 2) != 0) {
            str = networkState.msg;
        }
        return networkState.copy(status2, str);
    }

    @DexIgnore
    public final Status component1() {
        return this.status;
    }

    @DexIgnore
    public final String component2() {
        return this.msg;
    }

    @DexIgnore
    public final NetworkState copy(Status status2, String str) {
        Wg6.c(status2, "status");
        return new NetworkState(status2, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof NetworkState) {
                NetworkState networkState = (NetworkState) obj;
                if (!Wg6.a(this.status, networkState.status) || !Wg6.a(this.msg, networkState.msg)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getMsg() {
        return this.msg;
    }

    @DexIgnore
    public final Status getStatus() {
        return this.status;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Status status2 = this.status;
        int hashCode = status2 != null ? status2.hashCode() : 0;
        String str = this.msg;
        if (str != null) {
            i = str.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "NetworkState(status=" + this.status + ", msg=" + this.msg + ")";
    }
}
