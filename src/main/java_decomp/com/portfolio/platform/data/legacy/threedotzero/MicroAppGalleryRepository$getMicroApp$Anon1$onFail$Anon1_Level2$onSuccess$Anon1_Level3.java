package com.portfolio.platform.data.legacy.threedotzero;

import com.misfit.frameworks.common.log.MFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2$onSuccess$Anon1_Level3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ List $microAppList;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2 this$0;

    @DexIgnore
    public MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2$onSuccess$Anon1_Level3(MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2 microAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2, List list) {
        this.this$0 = microAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2;
        this.$microAppList = list;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter getMicroApp");
        this.this$0.this$0.this$0.mMicroAppSettingLocalDataSource.updateListMicroApp(this.$microAppList);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit getMicroApp");
    }
}
