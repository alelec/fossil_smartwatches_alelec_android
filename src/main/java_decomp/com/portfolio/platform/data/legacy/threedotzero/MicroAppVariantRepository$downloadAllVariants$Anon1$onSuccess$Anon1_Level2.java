package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.Hp4;
import com.misfit.frameworks.common.log.MFLogger;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepository$downloadAllVariants$Anon1$onSuccess$Anon1_Level2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $variantParserList;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppVariantRepository$downloadAllVariants$Anon1 this$0;

    @DexIgnore
    public MicroAppVariantRepository$downloadAllVariants$Anon1$onSuccess$Anon1_Level2(MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon1, ArrayList arrayList) {
        this.this$0 = microAppVariantRepository$downloadAllVariants$Anon1;
        this.$variantParserList = arrayList;
    }

    @DexIgnore
    public final void run() {
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO enter onSuccess downloadAllVariants");
        ArrayList<Hp4> filterVariantList$app_fossilRelease = this.this$0.this$0.filterVariantList$app_fossilRelease(this.$variantParserList);
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon1 = this.this$0;
        microAppVariantRepository$downloadAllVariants$Anon1.this$0.saveMicroAppVariant$app_fossilRelease(microAppVariantRepository$downloadAllVariants$Anon1.$serialNumber, filterVariantList$app_fossilRelease);
        UAppSystemVersionModel uAppSystemVersionModel = this.this$0.this$0.mUAppSystemVersionRepository.getUAppSystemVersionModel(this.this$0.$serialNumber);
        if (uAppSystemVersionModel != null && uAppSystemVersionModel.getMajorVersion() == this.this$0.$major && uAppSystemVersionModel.getMinorVersion() == this.this$0.$minor) {
            uAppSystemVersionModel.setPinType(0);
            this.this$0.this$0.mUAppSystemVersionRepository.addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
        }
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon12 = this.this$0;
        microAppVariantRepository$downloadAllVariants$Anon12.this$0.notifyStatusChanged("DECLARATION_FILES_DOWNLOADED", microAppVariantRepository$downloadAllVariants$Anon12.$serialNumber);
        MicroAppVariantDataSource microAppVariantDataSource = this.this$0.this$0.mMicroAppVariantLocalDataSource;
        MicroAppVariantRepository$downloadAllVariants$Anon1 microAppVariantRepository$downloadAllVariants$Anon13 = this.this$0;
        microAppVariantDataSource.getAllMicroAppVariants(microAppVariantRepository$downloadAllVariants$Anon13.$serialNumber, microAppVariantRepository$downloadAllVariants$Anon13.$major, microAppVariantRepository$downloadAllVariants$Anon13.$minor, microAppVariantRepository$downloadAllVariants$Anon13.$callback);
        MFLogger.d(MicroAppGalleryRepository.Companion.getTAG(), "diskIO exit onSuccess downloadAllVariants");
    }
}
