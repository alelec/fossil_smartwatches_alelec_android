package com.portfolio.platform.data.legacy.threedotzero;

import android.text.TextUtils;
import com.fossil.Lp5;
import com.fossil.Mn5;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MicroAppSettingLocalDataSource implements MicroAppSettingDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppSettingLocalDataSource";

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void addOrUpdateMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "addOrUpdateMicroAppSetting microAppSetting=" + microAppSetting.getSetting() + "microAppId=" + microAppSetting.getMicroAppId());
        Lp5 j = Mn5.p.a().j();
        MicroAppSetting n = j.n(microAppSetting.getMicroAppId());
        if (n != null && n.getFirstUsed() > 0) {
            microAppSetting.setFirstUsed(n.getFirstUsed());
        }
        microAppSetting.setUpdatedAt(System.currentTimeMillis());
        if (j.h(microAppSetting)) {
            String str2 = TAG;
            MFLogger.d(str2, "addOrUpdateMicroAppSetting onSuccess microAppSetting=" + microAppSetting.getSetting());
            microAppSettingCallback.onSuccess(microAppSetting);
            return;
        }
        String str3 = TAG;
        MFLogger.d(str3, "addOrUpdateMicroAppSetting onFail microAppSetting=" + microAppSetting.getSetting());
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void clearData() {
        Mn5.p.a().j().b();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void getMicroAppSetting(String str, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppSetting getMicroAppId=" + str);
        if (TextUtils.isEmpty(str)) {
            String str3 = TAG;
            MFLogger.d(str3, "getMicroAppSetting onFail getMicroAppId=" + str);
            microAppSettingCallback.onFail();
            return;
        }
        MicroAppSetting n = Mn5.p.a().j().n(str);
        if (n != null) {
            String str4 = TAG;
            MFLogger.d(str4, "getMicroAppSetting onSuccess getMicroAppId=" + str);
            microAppSettingCallback.onSuccess(n);
            return;
        }
        String str5 = TAG;
        MFLogger.d(str5, "getMicroAppSetting onFail getMicroAppId=" + str);
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void getMicroAppSettingList(MicroAppSettingDataSource.MicroAppSettingListCallback microAppSettingListCallback) {
        MFLogger.d(TAG, "getMicroAppSettingList");
        List<MicroAppSetting> c = Mn5.p.a().j().c();
        if (c == null || c.isEmpty()) {
            MFLogger.d(TAG, "getMicroAppSettingList onFail");
            microAppSettingListCallback.onFail();
            return;
        }
        String str = TAG;
        MFLogger.d(str, "getMicroAppSettingList onSuccess microAppSettingListSize=" + c.size());
        microAppSettingListCallback.onSuccess(c);
    }

    @DexIgnore
    public List<MicroAppSetting> getPendingMicroAppSettings() {
        return Mn5.p.a().j().getPendingMicroAppSettings();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingDataSource
    public void mergeMicroAppSetting(MicroAppSetting microAppSetting, MicroAppSettingDataSource.MicroAppSettingCallback microAppSettingCallback) {
        String str = TAG;
        MFLogger.d(str, "mergeMicroAppSetting microAppSetting=" + microAppSetting.getSetting());
        Lp5 j = Mn5.p.a().j();
        MicroAppSetting n = j.n(microAppSetting.getMicroAppId());
        if (n != null) {
            if (n.getUpdatedAt() > microAppSetting.getUpdatedAt()) {
                microAppSetting.setCreatedAt(n.getCreatedAt());
                microAppSetting.setSetting(n.getSetting());
                microAppSetting.setLike(n.isLike());
            }
            if (n.getFirstUsed() > 0) {
                microAppSetting.setFirstUsed(n.getFirstUsed());
            }
        }
        microAppSetting.setUpdatedAt(System.currentTimeMillis());
        if (j.h(microAppSetting)) {
            String str2 = TAG;
            MFLogger.d(str2, "mergeMicroAppSetting onSuccess microAppSetting=" + microAppSetting.getSetting());
            microAppSettingCallback.onSuccess(microAppSetting);
            return;
        }
        String str3 = TAG;
        MFLogger.d(str3, "mergeMicroAppSetting onFail microAppSetting=" + microAppSetting.getSetting());
        microAppSettingCallback.onFail();
    }

    @DexIgnore
    public void updateMicroAppSettingPinType(String str, int i) {
        String str2 = TAG;
        MFLogger.d(str2, "updateMicroAppSettingPinType microAppId=" + str + ", pinType=" + i);
        Mn5.p.a().j().l(str, i);
    }
}
