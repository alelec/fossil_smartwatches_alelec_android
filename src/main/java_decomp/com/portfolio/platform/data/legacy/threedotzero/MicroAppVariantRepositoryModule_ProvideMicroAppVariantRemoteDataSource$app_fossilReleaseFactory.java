package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Lk7;
import com.mapped.U04;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory implements Factory<MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ Provider<U04> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppVariantRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        this.module = microAppVariantRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory create(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, Provider<ShortcutApiService> provider, Provider<U04> provider2) {
        return new MicroAppVariantRepositoryModule_ProvideMicroAppVariantRemoteDataSource$app_fossilReleaseFactory(microAppVariantRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease(MicroAppVariantRepositoryModule microAppVariantRepositoryModule, ShortcutApiService shortcutApiService, U04 u04) {
        MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantRemoteDataSource$app_fossilRelease(shortcutApiService, u04);
        Lk7.c(provideMicroAppVariantRemoteDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantRemoteDataSource$app_fossilRelease;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppVariantDataSource get() {
        return provideMicroAppVariantRemoteDataSource$app_fossilRelease(this.module, this.shortcutApiServiceProvider.get(), this.appExecutorsProvider.get());
    }
}
