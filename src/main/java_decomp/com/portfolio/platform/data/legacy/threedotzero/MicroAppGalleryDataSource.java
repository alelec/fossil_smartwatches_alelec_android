package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class MicroAppGalleryDataSource {

    @DexIgnore
    public interface GetMicroAppCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(MicroApp microApp);
    }

    @DexIgnore
    public interface GetMicroAppGalleryCallback {
        @DexIgnore
        Object onFail();  // void declaration

        @DexIgnore
        void onSuccess(List<? extends MicroApp> list);
    }

    @DexIgnore
    public void deleteListMicroApp(String str) {
        Wg6.c(str, "serial");
    }

    @DexIgnore
    public void getMicroApp(String str, String str2, GetMicroAppCallback getMicroAppCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        Wg6.c(str2, "microAppId");
    }

    @DexIgnore
    public abstract void getMicroAppGallery(String str, GetMicroAppGalleryCallback getMicroAppGalleryCallback);

    @DexIgnore
    public void updateListMicroApp(List<? extends MicroApp> list) {
    }

    @DexIgnore
    public void updateMicroApp(MicroApp microApp, GetMicroAppCallback getMicroAppCallback) {
        Wg6.c(microApp, "microApp");
    }
}
