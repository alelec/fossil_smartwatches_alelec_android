package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.U04;
import com.mapped.Wg6;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import com.portfolio.platform.data.source.scope.Local;
import com.portfolio.platform.data.source.scope.Remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule {
    @DexIgnore
    @Local
    public final MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease() {
        return new MicroAppVariantLocalDataSource();
    }

    @DexIgnore
    @Remote
    public final MicroAppVariantDataSource provideMicroAppVariantRemoteDataSource$app_fossilRelease(ShortcutApiService shortcutApiService, U04 u04) {
        Wg6.c(shortcutApiService, "shortcutApiService");
        Wg6.c(u04, "appExecutors");
        return new MicroAppVariantRemoteDataSource(shortcutApiService, u04);
    }
}
