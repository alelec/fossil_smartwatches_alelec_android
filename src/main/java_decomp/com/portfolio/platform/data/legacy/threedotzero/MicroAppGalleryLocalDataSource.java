package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Mn5;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryLocalDataSource extends MicroAppGalleryDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return MicroAppGalleryLocalDataSource.TAG;
        }

        @DexIgnore
        public final void setTAG(String str) {
            Wg6.c(str, "<set-?>");
            MicroAppGalleryLocalDataSource.TAG = str;
        }
    }

    /*
    static {
        String simpleName = MicroAppGalleryLocalDataSource.class.getSimpleName();
        Wg6.b(simpleName, "MicroAppGalleryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void deleteListMicroApp(String str) {
        Wg6.c(str, "serial");
        String str2 = TAG;
        MFLogger.d(str2, "deleteListMicroApp serial=" + str);
        Mn5.p.a().e().deleteMicroAppList(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroApp(String str, String str2, MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        Wg6.c(str2, "microAppId");
        String str3 = TAG;
        MFLogger.d(str3, "getMicroApp deviceSerial=" + str + " microAppId=" + str2);
        MicroApp microApp = Mn5.p.a().e().getMicroApp(str, str2);
        if (microApp != null) {
            MFLogger.d(TAG, "getMicroApp onSuccess");
            if (getMicroAppCallback != null) {
                getMicroAppCallback.onSuccess(microApp);
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroApp onFail");
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void getMicroAppGallery(String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        Wg6.c(str, DeviceLocation.COLUMN_DEVICE_SERIAL);
        String str2 = TAG;
        MFLogger.d(str2, "getMicroAppGallery deviceSerial=" + str);
        List<MicroApp> listMicroApp = Mn5.p.a().e().getListMicroApp(str);
        if (listMicroApp == null || !(!listMicroApp.isEmpty())) {
            MFLogger.d(TAG, "getMicroAppGallery onFail");
            if (getMicroAppGalleryCallback != null) {
                getMicroAppGalleryCallback.onFail();
                return;
            }
            return;
        }
        MFLogger.d(TAG, "getMicroAppGallery onSuccess");
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(listMicroApp);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource
    public void updateListMicroApp(List<? extends MicroApp> list) {
        if (list != null && (!list.isEmpty())) {
            String str = TAG;
            MFLogger.d(str, "updateListMicroApp microAppListSize=" + list.size());
            Mn5.p.a().e().updateListMicroApp(list);
        }
    }
}
