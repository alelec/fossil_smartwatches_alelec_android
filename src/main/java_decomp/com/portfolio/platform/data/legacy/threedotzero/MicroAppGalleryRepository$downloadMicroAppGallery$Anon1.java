package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Rq4;
import com.fossil.Wt7;
import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$downloadMicroAppGallery$Anon1 implements MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryDataSource.GetMicroAppGalleryCallback $callback;
    @DexIgnore
    public /* final */ /* synthetic */ String $deviceSerial;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository this$0;

    @DexIgnore
    public MicroAppGalleryRepository$downloadMicroAppGallery$Anon1(MicroAppGalleryRepository microAppGalleryRepository, String str, MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback) {
        this.this$0 = microAppGalleryRepository;
        this.$deviceSerial = str;
        this.$callback = getMicroAppGalleryCallback;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback
    public void onFail() {
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "downloadMicroAppGallery deviceSerial=" + this.$deviceSerial + " remote onFail");
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback
    public void onSuccess(List<? extends MicroApp> list) {
        Wg6.c(list, "microAppList");
        ArrayList arrayList = new ArrayList();
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "downloadMicroAppGallery deviceSerial=" + this.$deviceSerial + " remote onSuccess");
        for (MicroApp microApp : list) {
            String w = Rq4.G.w();
            String appId = microApp.getAppId();
            Wg6.b(appId, "microApp.appId");
            if (!Wt7.v(w, appId, false, 2, null)) {
                String tag2 = MicroAppGalleryRepository.Companion.getTAG();
                MFLogger.d(tag2, "Add micro app id=" + microApp.getAppId());
                microApp.setPlatform(this.$deviceSerial);
                arrayList.add(microApp);
            } else {
                String tag3 = MicroAppGalleryRepository.Companion.getTAG();
                MFLogger.d(tag3, "Ignore micro app id=" + microApp.getAppId());
            }
        }
        MicroAppGalleryDataSource.GetMicroAppGalleryCallback getMicroAppGalleryCallback = this.$callback;
        if (getMicroAppGalleryCallback != null) {
            getMicroAppGalleryCallback.onSuccess(arrayList);
        }
        this.this$0.mAppExecutors.a().execute(new MicroAppGalleryRepository$downloadMicroAppGallery$Anon1$onSuccess$Anon1_Level2(this, arrayList));
    }
}
