package com.portfolio.platform.data.legacy.threedotzero;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import com.mapped.TimeUtils;
import com.misfit.frameworks.buttonservice.db.DataLogService;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.legacy.threedotzero.MappingSet;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;
import com.portfolio.platform.data.model.SKUModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceProviderImp extends BaseDbProvider implements DeviceProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "deviceModel.db";
    @DexIgnore
    public static /* final */ String LEGACY_DB_NAME; // = "device.db";
    @DexIgnore
    public static /* final */ String TAG; // = "DeviceProviderImp";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            @Override // com.fossil.wearables.fsl.shared.UpgradeCommand
            public void execute(SQLiteDatabase sQLiteDatabase) {
                String str;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = DeviceProviderImp.TAG;
                local.d(str2, "Inside upgrade ver 11 of dbPath=" + DeviceProviderImp.this.getDbPath());
                try {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = DeviceProviderImp.TAG;
                    local2.d(str3, "Inside .doInBackground upgrade device task on thread=" + Thread.currentThread().getName());
                    Cursor query = sQLiteDatabase.query(true, "device", new String[]{"deviceId", LegacyDeviceModel.COLUMN_DEVICE_MODEL, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "deviceBattery", "createdAt", DataLogService.COLUMN_UPDATE_AT, "macAddress"}, null, null, null, null, null, null);
                    ArrayList<DeviceModel> arrayList = new ArrayList();
                    String str4 = DataLogService.COLUMN_UPDATE_AT;
                    if (query != null) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String string = query.getString(query.getColumnIndex("deviceId"));
                            String string2 = query.getString(query.getColumnIndex(LegacyDeviceModel.COLUMN_DEVICE_MODEL));
                            String string3 = query.getString(query.getColumnIndex(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION));
                            int i = query.getInt(query.getColumnIndex("deviceBattery"));
                            String string4 = query.getString(query.getColumnIndex("createdAt"));
                            String string5 = query.getString(query.getColumnIndex(str4));
                            String string6 = query.getString(query.getColumnIndex("macAddress"));
                            DeviceModel deviceModel = new DeviceModel();
                            deviceModel.setDeviceId(string);
                            deviceModel.setSku(string2);
                            deviceModel.setFirmwareRevision(string3);
                            deviceModel.setBatteryLevel(i);
                            deviceModel.setCreatedAt(string4);
                            deviceModel.setUpdateAt(string5);
                            deviceModel.setMacAddress(string6);
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str5 = DeviceProviderImp.TAG;
                            local3.d(str5, "Add device=" + deviceModel);
                            arrayList.add(deviceModel);
                            query.moveToNext();
                        }
                        query.close();
                        str = "createdAt";
                    } else {
                        str4 = DataLogService.COLUMN_UPDATE_AT;
                        str = "createdAt";
                    }
                    sQLiteDatabase.execSQL("CREATE TABLE device_copy (deviceId VARCHAR PRIMARY KEY, sku VARCHAR, firmwareRevision VARCHAR, deviceBattery INTEGER, createdAt VARCHAR, updateAt VARCHAR, macAddress VARCHAR, minor INTEGER, major INTEGER, vibrationStrength INTEGER);");
                    for (DeviceModel deviceModel2 : arrayList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("deviceId", deviceModel2.getDeviceId());
                        contentValues.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, deviceModel2.getSku());
                        contentValues.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, deviceModel2.getFirmwareRevision());
                        contentValues.put("deviceBattery", Integer.valueOf(deviceModel2.getBatteryLevel()));
                        contentValues.put("macAddress", deviceModel2.getMacAddress());
                        contentValues.put(str, deviceModel2.getCreatedAt());
                        contentValues.put(str4, deviceModel2.getUpdateAt());
                        contentValues.put("major", Integer.valueOf(deviceModel2.getMajor()));
                        contentValues.put("minor", Integer.valueOf(deviceModel2.getMinor()));
                        contentValues.put("vibrationStrength", Integer.valueOf(deviceModel2.getVibrationStrength()));
                        sQLiteDatabase.insert("device_copy", null, contentValues);
                    }
                    sQLiteDatabase.execSQL("DROP TABLE device;");
                    sQLiteDatabase.execSQL("ALTER TABLE device_copy RENAME TO device;");
                    FLogger.INSTANCE.getLocal().d(DeviceProviderImp.TAG, "Migration complete");
                } catch (Exception e) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str6 = DeviceProviderImp.TAG;
                    local4.e(str6, "Error inside " + DeviceProviderImp.TAG + ".upgrade - e=" + e);
                }
                sQLiteDatabase.execSQL("CREATE TABLE uappsystemversion (deviceId VARCHAR PRIMARY KEY, majorVersion INTEGER, minorVersion INTEGER, pinType INTEGER);");
                sQLiteDatabase.execSQL("CREATE TABLE microApp (id VARCHAR PRIMARY KEY, appId VARCHAR, name VARCHAR, platform VARCHAR, like INTEGER, appSetting VARCHAR, icon VARCHAR, releaseDate BIGINT, updatedAt BIGINT, createdAt BIGINT, description VARCHAR);");
                sQLiteDatabase.execSQL("CREATE TABLE recommendedPreset (id VARCHAR PRIMARY KEY, objectId VARCHAR, buttons VARCHAR, name VARCHAR, description VARCHAR, createAt BIGINT, updatedAt BIGINT, serialNumber VARCHAR, type VARCHAR);");
                sQLiteDatabase.execSQL("CREATE TABLE savedPreset (id VARCHAR PRIMARY KEY, buttons VARCHAR, name VARCHAR, createAt BIGINT, pinType INTEGER DEFAULT 0, updatedAt BIGINT );");
                sQLiteDatabase.execSQL("CREATE TABLE activePreset (serialNumber VARCHAR PRIMARY KEY, buttons VARCHAR, originalId VARCHAR, createAt BIGINT, pinType INTEGER DEFAULT 0, updatedAt BIGINT);");
                sQLiteDatabase.execSQL("CREATE TABLE microAppVariant (id VARCHAR PRIMARY KEY, appId VARCHAR, description VARCHAR, name VARCHAR, createdAt BIGINT, updatedAt BIGINT, majorNumber INTEGER, minorNumber INTEGER, serialNumber VARCHAR);");
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String str7 = DeviceProviderImp.TAG;
                local5.d(str7, "statements=CREATE TABLE declarationFile (id VARCHAR PRIMARY KEY, fileId VARCHAR, content VARCHAR, variant VARCHAR, description VARCHAR, FOREIGN KEY(variant) REFERENCES microAppVariant (id));");
                sQLiteDatabase.execSQL("CREATE TABLE declarationFile (id VARCHAR PRIMARY KEY, fileId VARCHAR, content VARCHAR, variant VARCHAR, description VARCHAR, FOREIGN KEY(variant) REFERENCES microAppVariant (id));");
                FLogger.INSTANCE.getLocal().d(DeviceProviderImp.TAG, "Migration device complete");
            }
        }

        @DexIgnore
        public Anon1() {
            put(11, new Anon1_Level2());
        }
    }

    @DexIgnore
    public DeviceProviderImp(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<ActivePreset, Integer> getActivePresetSessionDao() throws SQLException {
        return this.databaseHelper.getDao(ActivePreset.class);
    }

    @DexIgnore
    private Dao<DeclarationFile, Integer> getDeclarationFileDao() throws SQLException {
        return this.databaseHelper.getDao(DeclarationFile.class);
    }

    @DexIgnore
    private Dao<DeviceModel, Integer> getDeviceSessionDao() throws SQLException {
        return this.databaseHelper.getDao(DeviceModel.class);
    }

    @DexIgnore
    private Dao<MappingSet, Integer> getMappingSetSessionDao() throws SQLException {
        return this.databaseHelper.getDao(MappingSet.class);
    }

    @DexIgnore
    private MicroApp getMicroAppByAppIdAndSerial(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "getMicroAppByAppIdAndSerial - appId=" + str + " serial=" + str2);
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            QueryBuilder<MicroApp, Integer> queryBuilder = getMicroAppSessionDao().queryBuilder();
            queryBuilder.where().eq("appId", str).and().eq("platform", str2);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local2.e(str4, "getMicroAppByAppIdAndSerial - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    private Dao<MicroApp, Integer> getMicroAppSessionDao() throws SQLException {
        return this.databaseHelper.getDao(MicroApp.class);
    }

    @DexIgnore
    private Dao<RecommendedPreset, Integer> getRecommendedPresetSessionDao() throws SQLException {
        return this.databaseHelper.getDao(RecommendedPreset.class);
    }

    @DexIgnore
    private Dao<SKUModel, Integer> getSKUSessionDao() throws SQLException {
        return this.databaseHelper.getDao(SKUModel.class);
    }

    @DexIgnore
    private Dao<SavedPreset, Integer> getSavedPresetSessionDao() throws SQLException {
        return this.databaseHelper.getDao(SavedPreset.class);
    }

    @DexIgnore
    private Dao<UAppSystemVersionModel, Integer> getUAppSystemVersionDao() throws SQLException {
        return this.databaseHelper.getDao(UAppSystemVersionModel.class);
    }

    @DexIgnore
    private Dao<MicroAppVariant, Integer> getVariantSessionDao() throws SQLException {
        return this.databaseHelper.getDao(MicroAppVariant.class);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateActivePreset(ActivePreset activePreset) {
        if (activePreset != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".addOrUpdateSavedPreset, serialNumber=" + activePreset.getSerialNumber());
            try {
                activePreset.setUpdateAt(System.currentTimeMillis());
                getActivePresetSessionDao().createOrUpdate(activePreset);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".addOrUpdateSavedPreset - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateDeclarationFile(DeclarationFile declarationFile) {
        if (declarationFile != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".addOrUpdateDeclarationFile - fileId=" + declarationFile.getFileId() + ", description=" + declarationFile.getDescription() + ", content=" + declarationFile.getContent());
            try {
                getDeclarationFileDao().createOrUpdate(declarationFile);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".addOrUpdateDeclarationFile - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void addOrUpdateDevice(DeviceModel deviceModel) {
        if (deviceModel != null) {
            try {
                String macAddress = deviceModel.getMacAddress();
                TextUtils.isEmpty(deviceModel.getMacAddress());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.d(str, "---Inside .addOrUpdateDevice deviceId: " + deviceModel.getDeviceId() + ", macAddress=" + deviceModel.getMacAddress() + " fwVersion: " + deviceModel.getFirmwareRevision() + " batLevel: " + deviceModel.getBatteryLevel() + ", major=" + deviceModel.getMajor() + ", minor=" + deviceModel.getMinor());
                DeviceModel deviceById = getDeviceById(deviceModel.getDeviceId());
                if (deviceById != null && TextUtils.isEmpty(deviceModel.getFirmwareRevision())) {
                    deviceModel.setFirmwareRevision(deviceById.getFirmwareRevision());
                }
                if (deviceById == null || deviceModel.getBatteryLevel() > 0) {
                    deviceModel.setCreatedAt(String.valueOf(TimeUtils.w0(new Date())));
                } else {
                    deviceModel.setBatteryLevel(deviceById.getBatteryLevel());
                }
                if (deviceModel.getBatteryLevel() > 100) {
                    deviceModel.setBatteryLevel(100);
                }
                if (!TextUtils.isEmpty(macAddress)) {
                    deviceModel.setMacAddress(macAddress);
                }
                deviceModel.setUpdateAt(String.valueOf(TimeUtils.w0(new Date())));
                getDeviceSessionDao().createOrUpdate(deviceModel);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, "Error inside " + TAG + ".addDevice - e=" + e);
            }
        } else {
            FLogger.INSTANCE.getLocal().e(TAG, "Can't add or update null device");
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateMicroApp(MicroApp microApp) {
        if (microApp != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".addOrUpdateMicroApp - microAppId=" + microApp.getId() + ", serialNumber=" + microApp.getPlatform() + ", appId=" + microApp.getAppId());
            try {
                MicroApp microAppByAppIdAndSerial = getMicroAppByAppIdAndSerial(microApp.getAppId(), microApp.getPlatform());
                if (microAppByAppIdAndSerial != null) {
                    microApp.setId(microAppByAppIdAndSerial.getId());
                }
                getMicroAppSessionDao().createOrUpdate(microApp);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".addOrUpdateSavedPreset - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateMicroAppVariant(MicroAppVariant microAppVariant) {
        boolean z;
        if (microAppVariant != null) {
            FLogger.INSTANCE.getLocal().d(TAG, ".addOrUpdateMicroAppVariant - microAppId=" + microAppVariant.getAppId() + ", major=" + microAppVariant.getMajorNumber() + ", minor=" + microAppVariant.getMinorNumber() + ", variantId=" + microAppVariant.getId());
            try {
                QueryBuilder<MicroAppVariant, Integer> queryBuilder = getVariantSessionDao().queryBuilder();
                queryBuilder.where().eq("appId", microAppVariant.getAppId()).and().eq("name", microAppVariant.getName()).and().eq("serialNumber", microAppVariant.getSerialNumbers()).and().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(microAppVariant.getMajorNumber()));
                MicroAppVariant queryForFirst = queryBuilder.queryForFirst();
                if (queryForFirst != null) {
                    FLogger.INSTANCE.getLocal().d(TAG, "addOrUpdateMicroAppVariant variantInDBId=" + queryForFirst.getId());
                    microAppVariant.setId(queryForFirst.getId());
                    z = true;
                } else {
                    z = false;
                }
                getVariantSessionDao().createOrUpdate(microAppVariant);
                if (z) {
                    deleteDeclarationFileByVariant(queryForFirst);
                }
                return true;
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(TAG, ".addOrUpdateMicroAppVariant - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateRecommendedPreset(RecommendedPreset recommendedPreset) {
        if (recommendedPreset != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".addOrUpdateRecommendedPreset - mappingSetId=" + recommendedPreset.getId() + ", deviceFamily=" + recommendedPreset.getSerialNumber() + ", id=" + recommendedPreset.getId());
            try {
                recommendedPreset.setUpdateAt(System.currentTimeMillis());
                getRecommendedPresetSessionDao().createOrUpdate(recommendedPreset);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".addOrUpdateSavedPreset - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean addOrUpdateSavedPreset(SavedPreset savedPreset) {
        if (savedPreset != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".addOrUpdateSavedPreset, presetId=" + savedPreset.getId());
            try {
                savedPreset.setUpdateAt(System.currentTimeMillis());
                getSavedPresetSessionDao().createOrUpdate(savedPreset);
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".addOrUpdateSavedPreset - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel) {
        if (uAppSystemVersionModel != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, "addOrUpdateUAppSystemVersionModel - serial=" + uAppSystemVersionModel.getDeviceId() + ", uappMajorVersion=" + uAppSystemVersionModel.getMajorVersion() + ", uappMinorVersion=" + uAppSystemVersionModel.getMinorVersion() + ", pinType=" + uAppSystemVersionModel.getPinType());
            try {
                getUAppSystemVersionDao().createOrUpdate(uAppSystemVersionModel);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, "addOrUpdateUAppSystemVersionModel - ex=" + e);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearActivePreset() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearActivePreset");
        try {
            TableUtils.clearTable(getActivePresetSessionDao().getConnectionSource(), ActivePreset.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".clearActivePreset - ex=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearAllDevices() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearAllDevices");
        try {
            TableUtils.clearTable(getDeviceSessionDao().getConnectionSource(), DeviceModel.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "clearAllDevices Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearDeclarationFiles() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearDeclarationFiles");
        try {
            TableUtils.clearTable(getDeclarationFileDao().getConnectionSource(), DeclarationFile.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "clearDeclarationFiles Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean clearListSKUsSupported() {
        FLogger.INSTANCE.getLocal().d(TAG, ".clearListSKUsSupported");
        try {
            getSKUSessionDao().deleteBuilder().delete();
            return true;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".clearListSKUsSupported - e=" + e);
            return false;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearMicroApp() {
        FLogger.INSTANCE.getLocal().e(TAG, "getAllMicroAppSettingList");
        try {
            TableUtils.clearTable(getMicroAppSessionDao().getConnectionSource(), MicroApp.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "getAllMicroAppSettingList Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearRecommendedPreset() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearRecommendedPreset");
        try {
            TableUtils.clearTable(getRecommendedPresetSessionDao().getConnectionSource(), RecommendedPreset.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".clearRecommendedPreset - ex=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearSavedPreset() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearSavedPreset");
        try {
            TableUtils.clearTable(getSavedPresetSessionDao().getConnectionSource(), SavedPreset.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".clearSavedPreset - ex=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void clearVariant() {
        FLogger.INSTANCE.getLocal().e(TAG, "clearVariant");
        try {
            TableUtils.clearTable(getVariantSessionDao().getConnectionSource(), MicroAppVariant.class);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "clearVariant Exception=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean deleteDeclarationFileByVariant(MicroAppVariant microAppVariant) {
        if (microAppVariant != null && !microAppVariant.getId().isEmpty()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.d(str, ".deleteDeclarationFileByVariantId - variantId=" + microAppVariant.getId());
            try {
                DeleteBuilder<DeclarationFile, Integer> deleteBuilder = getDeclarationFileDao().deleteBuilder();
                deleteBuilder.where().eq(DeclarationFile.COLUMN_VARIANT, microAppVariant);
                deleteBuilder.delete();
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.e(str2, ".deleteDeclarationFileByVariant - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean deleteMicroAppList(String str) {
        if (!TextUtils.isEmpty(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.d(str2, ".deleteMicroAppList - serialNumber=" + str);
            try {
                DeleteBuilder<MicroApp, Integer> deleteBuilder = getMicroAppSessionDao().deleteBuilder();
                deleteBuilder.where().eq("platform", str);
                deleteBuilder.delete();
                return true;
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local2.e(str3, ".deleteMicroAppList - e=" + e);
            }
        }
        return false;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void deleteMicroAppVariants(String str, int i, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, ".deleteMicroAppVariants - serialNumber=" + str + " major=" + i + " minor=" + i2);
        try {
            QueryBuilder<MicroAppVariant, Integer> queryBuilder = getVariantSessionDao().queryBuilder();
            queryBuilder.where().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(i)).and().eq("serialNumber", str).and().eq(MicroAppVariant.COLUMN_MINOR_NUMBER, Integer.valueOf(i2));
            for (MicroAppVariant microAppVariant : queryBuilder.query()) {
                deleteDeclarationFileByVariant(microAppVariant);
            }
            DeleteBuilder<MicroAppVariant, Integer> deleteBuilder = getVariantSessionDao().deleteBuilder();
            deleteBuilder.where().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(i)).and().eq("serialNumber", str).and().eq(MicroAppVariant.COLUMN_MINOR_NUMBER, Integer.valueOf(i2));
            deleteBuilder.delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".deleteMicroAppVariants - e=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean deleteRecommendedPreset(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deleteRecommendedPreset - serial=" + str);
        try {
            DeleteBuilder<RecommendedPreset, Integer> deleteBuilder = getRecommendedPresetSessionDao().deleteBuilder();
            deleteBuilder.where().eq("serialNumber", str);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public boolean deleteSavedPreset(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".deleteSavedPresetList - savedPresetId=" + str);
        try {
            DeleteBuilder<SavedPreset, Integer> deleteBuilder = getSavedPresetSessionDao().deleteBuilder();
            deleteBuilder.where().eq("id", str);
            deleteBuilder.delete();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public MappingSet getActiveMappingSetByDeviceFamily(MFDeviceFamily mFDeviceFamily) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, ".getActiveMappingSetByDeviceFamily - MFDeviceFamily=" + mFDeviceFamily);
        try {
            QueryBuilder<MappingSet, Integer> queryBuilder = getMappingSetSessionDao().queryBuilder();
            queryBuilder.where().eq("isActive", Boolean.TRUE).and().eq("deviceFamily", Integer.valueOf(mFDeviceFamily.getValue()));
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, ".getActiveMappingSetByDeviceFamily - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public ActivePreset getActivePreset(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, ".getHybridActivePreset deviceSerial=" + str);
        try {
            QueryBuilder<ActivePreset, Integer> queryBuilder = getActivePresetSessionDao().queryBuilder();
            queryBuilder.where().eq("serialNumber", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getHybridActivePreset - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<ActivePreset> getAllActivePreset() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllActivePreset");
        try {
            return getActivePresetSessionDao().queryForAll();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllActivePreset - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<DeviceModel> getAllDevice() {
        ArrayList arrayList = new ArrayList();
        try {
            return getDeviceSessionDao().query(getDeviceSessionDao().queryBuilder().prepare());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, "Error when get all device " + e);
            return arrayList;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MicroApp> getAllMicroApp() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllMicroApp");
        try {
            return getMicroAppSessionDao().queryForAll();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllMicroApp - e=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MicroAppVariant> getAllMicroAppVariant() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllMicroAppVariant");
        try {
            return getVariantSessionDao().queryForAll();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllMicroAppVariant - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MicroAppVariant> getAllMicroAppVariant(String str, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".getMicroAppVariant - major=" + i + ", serialNumber=" + str);
        try {
            QueryBuilder<MicroAppVariant, Integer> queryBuilder = getVariantSessionDao().queryBuilder();
            queryBuilder.where().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(i)).and().eq("serialNumber", str);
            return queryBuilder.query();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getAllMicroAppVariant - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllMicroAppVariantUniqueBySerial");
        try {
            QueryBuilder<MicroAppVariant, Integer> queryBuilder = getVariantSessionDao().queryBuilder();
            queryBuilder.groupBy("serialNumber");
            return queryBuilder.query();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllMicroAppVariantUniqueBySerial - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<RecommendedPreset> getAllRecommendedPreset() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllRecommendedPreset");
        try {
            return getRecommendedPresetSessionDao().queryForAll();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllRecommendedPreset - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SavedPreset> getAllSavedPreset() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllSavedPreset");
        try {
            return getSavedPresetSessionDao().queryForAll();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllSavedPreset - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SavedPreset> getAllSavedPresets() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllSavedPresets");
        try {
            Dao<SavedPreset, Integer> savedPresetSessionDao = getSavedPresetSessionDao();
            return savedPresetSessionDao.query(savedPresetSessionDao.queryBuilder().prepare());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllSavedPresets - ex=" + e);
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SavedPreset> getAllSavedPresetsExcludeDeleteType() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getAllSavedPresetsExcludeDeleteType");
        try {
            Dao<SavedPreset, Integer> savedPresetSessionDao = getSavedPresetSessionDao();
            QueryBuilder<SavedPreset, Integer> queryBuilder = savedPresetSessionDao.queryBuilder();
            queryBuilder.where().ne("pinType", 3);
            return savedPresetSessionDao.query(queryBuilder.prepare());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getAllSavedPresetsExcludeDeleteType - ex=" + e);
            e.printStackTrace();
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Class<?>[] getDbEntities() {
        return new Class[]{DeviceModel.class, SavedPreset.class, RecommendedPreset.class, SKUModel.class, MicroApp.class, UAppSystemVersionModel.class, MicroAppVariant.class, DeclarationFile.class, ActivePreset.class, MappingSet.class};
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.BaseProvider, com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    @Override // com.fossil.wearables.fsl.shared.BaseDbProvider
    public int getDbVersion() {
        return 11;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public RecommendedPreset getDefaultPreset(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, ".getRecommendPresetList - serial=" + str);
        try {
            QueryBuilder<RecommendedPreset, Integer> queryBuilder = getRecommendedPresetSessionDao().queryBuilder();
            queryBuilder.where().eq("type", SavedPreset.MappingSetType.DEFAULT.getValue()).and().eq("serialNumber", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getRecommendPresetList - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public DeviceModel getDeviceById(String str) {
        if (TextUtils.isEmpty(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, "Error when get device, deviceId " + str);
        }
        try {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, "---Inside .getDeviceById deviceId: " + str);
            QueryBuilder<DeviceModel, Integer> queryBuilder = getDeviceSessionDao().queryBuilder();
            queryBuilder.where().eq("deviceId", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.e(str4, "Error inside " + TAG + ".getDeviceById - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MicroApp> getListMicroApp(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, ".getListMicroApp - serial=" + str);
        try {
            QueryBuilder<MicroApp, Integer> queryBuilder = getMicroAppSessionDao().queryBuilder();
            queryBuilder.where().eq("platform", str);
            return queryBuilder.query();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getListMicroApp - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<RecommendedPreset> getListRecommendedPresetBySerial(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, ".getListFeatureMappingSetByDeviceFamily - serial=" + str);
        try {
            QueryBuilder<RecommendedPreset, Integer> queryBuilder = getRecommendedPresetSessionDao().queryBuilder();
            queryBuilder.where().eq("type", SavedPreset.MappingSetType.RECOMMENDED.getValue()).and().eq("serialNumber", str);
            return queryBuilder.query();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getListFeatureMappingSetByDeviceFamily - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SKUModel> getListSKUsSupported() {
        FLogger.INSTANCE.getLocal().e(TAG, ".getListSKUsSupported");
        try {
            return getSKUSessionDao().queryBuilder().query();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getListSKUsSupported - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<MappingSet> getMappingSetByType(MappingSet.MappingSetType mappingSetType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, ".getMappingSetByType - mappingSetType=" + mappingSetType);
        try {
            QueryBuilder<MappingSet, Integer> queryBuilder = getMappingSetSessionDao().queryBuilder();
            queryBuilder.where().eq("type", Integer.valueOf(mappingSetType.getValue()));
            return queryBuilder.query();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, ".getMappingSetByType - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public MicroApp getMicroApp(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.e(str3, ".getListMicroApp - serial=" + str);
        try {
            QueryBuilder<MicroApp, Integer> queryBuilder = getMicroAppSessionDao().queryBuilder();
            queryBuilder.where().eq("platform", str).and().eq("appId", str2);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local2.e(str4, ".getListMicroApp - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local.d(str4, ".getMicroAppVariant - microAppId=" + str + ", serialNumber=" + str2);
        try {
            QueryBuilder<MicroAppVariant, Integer> queryBuilder = getVariantSessionDao().queryBuilder();
            if (str3 == null || str3.isEmpty()) {
                queryBuilder.where().eq("appId", str).and().eq("serialNumber", str2).and().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(i));
            } else {
                queryBuilder.where().eq("appId", str).and().eq("serialNumber", str2).and().eq(MicroAppVariant.COLUMN_MAJOR_NUMBER, Integer.valueOf(i)).and().eq("name", str3);
            }
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local2.e(str5, ".getMicroAppVariant - e=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<ActivePreset> getPendingActivePresets() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getPendingActivePresets, get all of pending activePreset");
        try {
            QueryBuilder<ActivePreset, Integer> queryBuilder = getActivePresetSessionDao().queryBuilder();
            queryBuilder.where().ne("pinType", 0);
            List<ActivePreset> query = queryBuilder.query();
            return query != null ? query : new ArrayList();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getHybridActivePreset - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SavedPreset> getPendingDeletedUserPresets() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getPendingDeletedHybridUserPresets, get all of pending savedPreset");
        try {
            QueryBuilder<SavedPreset, Integer> queryBuilder = getSavedPresetSessionDao().queryBuilder();
            queryBuilder.where().eq("pinType", 3);
            List<SavedPreset> query = queryBuilder.query();
            return query != null ? query : new ArrayList();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getPendingDeletedHybridUserPresets - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public List<SavedPreset> getPendingUserPresets() {
        FLogger.INSTANCE.getLocal().d(TAG, ".getPendingUserPresets, get all of pending savedPreset");
        try {
            QueryBuilder<SavedPreset, Integer> queryBuilder = getSavedPresetSessionDao().queryBuilder();
            queryBuilder.where().eq("pinType", 1).or().eq("pinType", 2);
            List<SavedPreset> query = queryBuilder.query();
            return query != null ? query : new ArrayList();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".getPendingUserPresets - ex=" + e);
            return new ArrayList();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public SKUModel getSKUModelBySerialNumberPrefix(String str) {
        try {
            String upperCase = str.toUpperCase();
            QueryBuilder<SKUModel, Integer> queryBuilder = getSKUSessionDao().queryBuilder();
            queryBuilder.where().eq("serialNumberPrefix", upperCase);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, ".getSKUModelBySerialNumberPrefix - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public SavedPreset getSavedPresetById(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, ".getSavedPresetById - mappingSetId=" + str);
        try {
            QueryBuilder<SavedPreset, Integer> queryBuilder = getSavedPresetSessionDao().queryBuilder();
            queryBuilder.where().eq("id", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, ".getSavedPresetById - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public UAppSystemVersionModel getUAppSystemVersionModel(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.e(str2, "getUAppSystemVersionModel - serial=" + str);
        try {
            QueryBuilder<UAppSystemVersionModel, Integer> queryBuilder = getUAppSystemVersionDao().queryBuilder();
            queryBuilder.where().eq("deviceId", str);
            return queryBuilder.queryForFirst();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.e(str3, "getUAppSystemVersionModel - ex=" + e);
            return null;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void removeDevice(String str) {
        if (TextUtils.isEmpty(str)) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local.e(str2, "Error when deleteAlarm device, deviceId " + str);
        }
        try {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, "---Inside .removeDevice deviceId: " + str);
            DeleteBuilder<DeviceModel, Integer> deleteBuilder = getDeviceSessionDao().deleteBuilder();
            deleteBuilder.where().eq("deviceId", str);
            deleteBuilder.delete();
        } catch (Exception e) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = TAG;
            local3.e(str4, "Error inside " + TAG + ".removeDevice - e=" + e);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void setListSKUsSupported(List<SKUModel> list) {
        FLogger.INSTANCE.getLocal().d(TAG, ".setListSKUsSupported");
        if (list != null && !list.isEmpty()) {
            try {
                for (SKUModel sKUModel : list) {
                    if (!TextUtils.isEmpty(sKUModel.getSku())) {
                        getSKUSessionDao().createOrUpdate(sKUModel);
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = TAG;
                local.e(str, ".setListSKUsSupported - ex=" + e);
            }
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void updateBatteryLevel(String str, int i) {
        DeviceModel deviceById = getDeviceById(str);
        if (deviceById != null && i > 0) {
            deviceById.setBatteryLevel(i);
            addOrUpdateDevice(deviceById);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.DeviceProvider
    public void updateListMicroApp(List<MicroApp> list) {
        if (!(list == null || list.isEmpty())) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = TAG;
            local.e(str, ".updateListMicroApp - platform=" + list.get(0).getPlatform());
            for (int i = 0; i < list.size(); i++) {
                addOrUpdateMicroApp(list.get(i));
            }
        }
    }
}
