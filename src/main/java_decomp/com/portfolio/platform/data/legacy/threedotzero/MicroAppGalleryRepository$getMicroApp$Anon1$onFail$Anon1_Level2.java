package com.portfolio.platform.data.legacy.threedotzero;

import com.mapped.Wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2 implements MicroAppGalleryDataSource.GetMicroAppGalleryCallback {
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppGalleryRepository$getMicroApp$Anon1 this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2(MicroAppGalleryRepository$getMicroApp$Anon1 microAppGalleryRepository$getMicroApp$Anon1) {
        this.this$0 = microAppGalleryRepository$getMicroApp$Anon1;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback
    public void onFail() {
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onFail");
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback = this.this$0.$callback;
        if (getMicroAppCallback != null) {
            getMicroAppCallback.onFail();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.legacy.threedotzero.MicroAppGalleryDataSource.GetMicroAppGalleryCallback
    public void onSuccess(List<? extends MicroApp> list) {
        MicroAppGalleryDataSource.GetMicroAppCallback getMicroAppCallback;
        Wg6.c(list, "microAppList");
        String tag = MicroAppGalleryRepository.Companion.getTAG();
        MFLogger.d(tag, "getMicroApp microAppId=" + this.this$0.$microAppId + " remote onSuccess");
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ((MicroApp) list.get(i)).setPlatform(this.this$0.$deviceSerial);
            if (Wg6.a(((MicroApp) list.get(i)).getAppId(), this.this$0.$microAppId) && (getMicroAppCallback = this.this$0.$callback) != null) {
                getMicroAppCallback.onSuccess((MicroApp) list.get(i));
            }
        }
        this.this$0.this$0.mAppExecutors.a().execute(new MicroAppGalleryRepository$getMicroApp$Anon1$onFail$Anon1_Level2$onSuccess$Anon1_Level3(this, list));
    }
}
