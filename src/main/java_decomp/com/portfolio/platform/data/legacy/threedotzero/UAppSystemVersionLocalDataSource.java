package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.Mn5;
import com.portfolio.platform.data.source.UAppSystemVersionDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class UAppSystemVersionLocalDataSource implements UAppSystemVersionDataSource {
    @DexIgnore
    @Override // com.portfolio.platform.data.source.UAppSystemVersionDataSource
    public void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel) {
        Mn5.p.a().e().addOrUpdateUAppSystemVersionModel(uAppSystemVersionModel);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UAppSystemVersionDataSource
    public UAppSystemVersionModel getUAppSystemVersionModel(String str) {
        return Mn5.p.a().e().getUAppSystemVersionModel(str);
    }
}
