package com.portfolio.platform.data.model.watchface;

import com.mapped.Pj4;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceUser {
    @DexIgnore
    @Vu3("checksum")
    public String checksum;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3(Firmware.COLUMN_DOWNLOAD_URL)
    public String downloadURL;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("name")
    public String name;
    @DexIgnore
    @Vu3("order")
    public DianaWatchFaceOrder order; // = new DianaWatchFaceOrder("", "", "");
    @DexIgnore
    @Pj4
    public int pinType;
    @DexIgnore
    @Vu3("previewUrl")
    public String previewURL;
    @DexIgnore
    @Vu3("uid")
    public String uid;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt; // = "2016-01-01T01:01:01.001Z";

    @DexIgnore
    public DianaWatchFaceUser(String str, String str2, String str3, String str4, String str5, String str6) {
        Wg6.c(str, "id");
        Wg6.c(str2, "downloadURL");
        Wg6.c(str3, "name");
        Wg6.c(str4, "checksum");
        Wg6.c(str5, "previewURL");
        Wg6.c(str6, "uid");
        this.id = str;
        this.downloadURL = str2;
        this.name = str3;
        this.checksum = str4;
        this.previewURL = str5;
        this.uid = str6;
    }

    @DexIgnore
    public static /* synthetic */ DianaWatchFaceUser copy$default(DianaWatchFaceUser dianaWatchFaceUser, String str, String str2, String str3, String str4, String str5, String str6, int i, Object obj) {
        return dianaWatchFaceUser.copy((i & 1) != 0 ? dianaWatchFaceUser.id : str, (i & 2) != 0 ? dianaWatchFaceUser.downloadURL : str2, (i & 4) != 0 ? dianaWatchFaceUser.name : str3, (i & 8) != 0 ? dianaWatchFaceUser.checksum : str4, (i & 16) != 0 ? dianaWatchFaceUser.previewURL : str5, (i & 32) != 0 ? dianaWatchFaceUser.uid : str6);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.downloadURL;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.checksum;
    }

    @DexIgnore
    public final String component5() {
        return this.previewURL;
    }

    @DexIgnore
    public final String component6() {
        return this.uid;
    }

    @DexIgnore
    public final DianaWatchFaceUser copy(String str, String str2, String str3, String str4, String str5, String str6) {
        Wg6.c(str, "id");
        Wg6.c(str2, "downloadURL");
        Wg6.c(str3, "name");
        Wg6.c(str4, "checksum");
        Wg6.c(str5, "previewURL");
        Wg6.c(str6, "uid");
        return new DianaWatchFaceUser(str, str2, str3, str4, str5, str6);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaWatchFaceUser) {
                DianaWatchFaceUser dianaWatchFaceUser = (DianaWatchFaceUser) obj;
                if (!Wg6.a(this.id, dianaWatchFaceUser.id) || !Wg6.a(this.downloadURL, dianaWatchFaceUser.downloadURL) || !Wg6.a(this.name, dianaWatchFaceUser.name) || !Wg6.a(this.checksum, dianaWatchFaceUser.checksum) || !Wg6.a(this.previewURL, dianaWatchFaceUser.previewURL) || !Wg6.a(this.uid, dianaWatchFaceUser.uid)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDownloadURL() {
        return this.downloadURL;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final DianaWatchFaceOrder getOrder() {
        return this.order;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getPreviewURL() {
        return this.previewURL;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.downloadURL;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.checksum;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.previewURL;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.uid;
        if (str6 != null) {
            i = str6.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final void setChecksum(String str) {
        Wg6.c(str, "<set-?>");
        this.checksum = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDownloadURL(String str) {
        Wg6.c(str, "<set-?>");
        this.downloadURL = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setOrder(DianaWatchFaceOrder dianaWatchFaceOrder) {
        this.order = dianaWatchFaceOrder;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setPreviewURL(String str) {
        Wg6.c(str, "<set-?>");
        this.previewURL = str;
    }

    @DexIgnore
    public final void setUid(String str) {
        Wg6.c(str, "<set-?>");
        this.uid = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaWatchFaceUser(id=" + this.id + ", downloadURL=" + this.downloadURL + ", name=" + this.name + ", checksum=" + this.checksum + ", previewURL=" + this.previewURL + ", uid=" + this.uid + ")";
    }
}
