package com.portfolio.platform.data.model.room;

import com.fossil.Lx0;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1 extends Xh {
    @DexIgnore
    public UserDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(UserDatabase.TAG, "Migration 3 to 4 Start");
        lx0.beginTransaction();
        try {
            lx0.execSQL("CREATE TABLE user_temp (pinType TEXT NOT NULL DEFAULT '0', isOnboardingComplete INTEGER NOT NULL DEFAULT 0, averageSleep INTEGER NOT NULL DEFAULT 0, averageStep INTEGER NOT NULL DEFAULT 0, createdAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', updatedAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z' , uid TEXT NOT NULL DEFAULT '' , authType TEXT NOT NULL DEFAULT 'email' ,birthday TEXT NOT NULL DEFAULT '', brand TEXT NOT NULL DEFAULT '', diagnosticEnabled INTEGER NOT NULL DEFAULT 0,email TEXT NOT NULL DEFAULT '', emailOptIn INTEGER NOT NULL DEFAULT 0 , emailVerified INTEGER NOT NULL DEFAULT 0, firstName TEXT NOT NULL DEFAULT '' , gender TEXT NOT NULL DEFAULT 'M', heightInCentimeters INTEGER NOT NULL DEFAULT 170, integrations TEXT NOT NULL DEFAULT '', lastName TEXT NOT NULL DEFAULT '' , profilePicture TEXT NOT NULL DEFAULT '', registerDate TEXT NOT NULL DEFAULT '2015-11-29', registrationComplete INTEGER NOT NULL DEFAULT 0, useDefaultBiometric INTEGER NOT NULL DEFAULT 0,useDefaultGoals INTEGER NOT NULL DEFAULT 0, username TEXT NOT NULL DEFAULT '' , weightInGrams INTEGER NOT NULL DEFAULT 68030, userAccessToken TEXT NOT NULL DEFAULT '', refreshToken TEXT NOT NULL DEFAULT '', accessTokenExpiresAt TEXT NOT NULL DEFAULT '',accessTokenExpiresIn INTEGER NOT NULL DEFAULT 86400, home TEXT NOT NULL DEFAULT '', work TEXT NOT NULL DEFAULT '' , distanceUnit TEXT NOT NULL DEFAULT 'metric', weightUnit TEXT NOT NULL DEFAULT 'metric', heightUnit TEXT NOT NULL DEFAULT 'metric',activeDeviceId TEXT NOT NULL DEFAULT '', temperatureUnit TEXT NOT NULL DEFAULT 'metric', PRIMARY KEY(uid))");
            lx0.execSQL("INSERT INTO user_temp (registrationComplete,diagnosticEnabled,averageStep,averageSleep,isOnboardingComplete,uid, userAccessToken, refreshToken,accessTokenExpiresAt, createdAt, updatedAt, email, authType,firstname,lastname, weightInGrams,heightInCentimeters, heightUnit, weightUnit, distanceUnit, birthday, gender,emailOptIn) SELECT registrationComplete,diagnosticEnabled,averageStep,averageSleep,isOnboardingComplete,uid, userAccessToken, refreshToken,accessTokenExpiresAt, createdAt,updatedAt, email, authType,firstname,lastname, weightInGrams,heightInCentimeters, heightUnit, weightUnit, distanceUnit, birthday, gender,emailOptIn FROM user");
            lx0.execSQL("UPDATE user_temp SET email = IFNULL((SELECT email FROM user LIMIT 1),''), birthday = IFNULL((SELECT birthday FROM user LIMIT 1),''),profilePicture = IFNULL((SELECT profilePicture FROM user LIMIT 1),'')  ,integrations= IFNULL((SELECT integrations FROM user LIMIT 1),''), activeDeviceId=IFNULL((SELECT activeDeviceId FROM user LIMIT 1),'')");
            lx0.execSQL("DROP TABLE user");
            lx0.execSQL("ALTER TABLE user_temp RENAME TO user");
            FLogger.INSTANCE.getLocal().d(UserDatabase.TAG, "Migration 3 to 4 Exception success");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(UserDatabase.TAG, "Migration 3 to 4 Exception " + e);
            lx0.execSQL("DROP TABLE IF EXISTS user");
            lx0.execSQL("DROP TABLE IF EXISTS user_temp");
            lx0.execSQL("CREATE TABLE user (pinType TEXT NOT NULL DEFAULT '0', isOnboardingComplete INTEGER NOT NULL DEFAULT 0, averageSleep INTEGER NOT NULL DEFAULT 0, averageStep INTEGER NOT NULL DEFAULT 0, createdAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z', updatedAt TEXT NOT NULL DEFAULT '2018-03-20T07:14:33.627Z' , uid TEXT NOT NULL DEFAULT '' , authType TEXT NOT NULL DEFAULT 'email' ,birthday TEXT NOT NULL DEFAULT '', brand TEXT NOT NULL DEFAULT '', diagnosticEnabled INTEGER NOT NULL DEFAULT 0,email TEXT NOT NULL DEFAULT '', emailOptIn INTEGER NOT NULL DEFAULT 0, emailVerified INTEGER NOT NULL DEFAULT 0, firstName TEXT NOT NULL DEFAULT '' , gender TEXT NOT NULL DEFAULT 'M', heightInCentimeters INTEGER NOT NULL DEFAULT 170, integrations TEXT NOT NULL DEFAULT '', lastName TEXT NOT NULL DEFAULT '' , profilePicture TEXT NOT NULL DEFAULT '', registerDate TEXT NOT NULL DEFAULT '2015-11-29', registrationComplete INTEGER NOT NULL DEFAULT 0, useDefaultBiometric INTEGER NOT NULL DEFAULT 0,useDefaultGoals INTEGER NOT NULL DEFAULT 0, username TEXT NOT NULL DEFAULT '' , weightInGrams INTEGER NOT NULL DEFAULT 68030, userAccessToken TEXT NOT NULL DEFAULT '', refreshToken TEXT NOT NULL DEFAULT '', accessTokenExpiresAt TEXT NOT NULL DEFAULT '',accessTokenExpiresIn INTEGER NOT NULL DEFAULT 86400, home TEXT NOT NULL DEFAULT '', work TEXT NOT NULL DEFAULT '' , distanceUnit TEXT NOT NULL DEFAULT 'metric', weightUnit TEXT NOT NULL DEFAULT 'metric', heightUnit TEXT NOT NULL DEFAULT 'metric',activeDeviceId TEXT NOT NULL DEFAULT '', temperatureUnit TEXT NOT NULL DEFAULT 'metric', PRIMARY KEY(uid))");
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
        FLogger.INSTANCE.getLocal().d(UserDatabase.TAG, "Migration 3 to 4 Done");
    }
}
