package com.portfolio.platform.data.model.goaltracking;

import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummary {
    @DexIgnore
    @Vu3("createdAt")
    public long createdAt;
    @DexIgnore
    @Vu3("date")
    public Date date;
    @DexIgnore
    @Vu3("goalTarget")
    public int goalTarget;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int totalTargetOfWeek;
    @DexIgnore
    @Vu3("totalTracked")
    public int totalTracked;
    @DexIgnore
    public int totalValueOfWeek;
    @DexIgnore
    @Vu3("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingSummary(Date date2, int i, int i2, long j, long j2) {
        Wg6.c(date2, "date");
        this.date = date2;
        this.totalTracked = i;
        this.goalTarget = i2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingSummary copy$default(GoalTrackingSummary goalTrackingSummary, Date date2, int i, int i2, long j, long j2, int i3, Object obj) {
        return goalTrackingSummary.copy((i3 & 1) != 0 ? goalTrackingSummary.date : date2, (i3 & 2) != 0 ? goalTrackingSummary.totalTracked : i, (i3 & 4) != 0 ? goalTrackingSummary.goalTarget : i2, (i3 & 8) != 0 ? goalTrackingSummary.createdAt : j, (i3 & 16) != 0 ? goalTrackingSummary.updatedAt : j2);
    }

    @DexIgnore
    public final Date component1() {
        return this.date;
    }

    @DexIgnore
    public final int component2() {
        return this.totalTracked;
    }

    @DexIgnore
    public final int component3() {
        return this.goalTarget;
    }

    @DexIgnore
    public final long component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingSummary copy(Date date2, int i, int i2, long j, long j2) {
        Wg6.c(date2, "date");
        return new GoalTrackingSummary(date2, i, i2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof GoalTrackingSummary)) {
            return false;
        }
        GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) obj;
        return this.totalTracked == goalTrackingSummary.totalTracked && this.goalTarget == goalTrackingSummary.goalTarget && this.totalTargetOfWeek == goalTrackingSummary.totalTargetOfWeek && this.totalValueOfWeek == goalTrackingSummary.totalValueOfWeek;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getGoalTarget() {
        return this.goalTarget;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTotalTargetOfWeek() {
        return this.totalTargetOfWeek;
    }

    @DexIgnore
    public final int getTotalTracked() {
        return this.totalTracked;
    }

    @DexIgnore
    public final int getTotalValueOfWeek() {
        return this.totalValueOfWeek;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        Wg6.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setGoalTarget(int i) {
        this.goalTarget = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setTotalTargetOfWeek(int i) {
        this.totalTargetOfWeek = i;
    }

    @DexIgnore
    public final void setTotalTracked(int i) {
        this.totalTracked = i;
    }

    @DexIgnore
    public final void setTotalValueOfWeek(int i) {
        this.totalValueOfWeek = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingSummary(date=" + this.date + ", totalTracked=" + this.totalTracked + ", goalTarget=" + this.goalTarget + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
