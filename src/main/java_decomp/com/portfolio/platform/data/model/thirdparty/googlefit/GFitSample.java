package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.mapped.C;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSample {
    @DexIgnore
    public float calorie;
    @DexIgnore
    public float distance;
    @DexIgnore
    public long endTime;
    @DexIgnore
    public int id;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public int step;

    @DexIgnore
    public GFitSample(int i, float f, float f2, long j, long j2) {
        this.step = i;
        this.distance = f;
        this.calorie = f2;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitSample copy$default(GFitSample gFitSample, int i, float f, float f2, long j, long j2, int i2, Object obj) {
        return gFitSample.copy((i2 & 1) != 0 ? gFitSample.step : i, (i2 & 2) != 0 ? gFitSample.distance : f, (i2 & 4) != 0 ? gFitSample.calorie : f2, (i2 & 8) != 0 ? gFitSample.startTime : j, (i2 & 16) != 0 ? gFitSample.endTime : j2);
    }

    @DexIgnore
    public final int component1() {
        return this.step;
    }

    @DexIgnore
    public final float component2() {
        return this.distance;
    }

    @DexIgnore
    public final float component3() {
        return this.calorie;
    }

    @DexIgnore
    public final long component4() {
        return this.startTime;
    }

    @DexIgnore
    public final long component5() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitSample copy(int i, float f, float f2, long j, long j2) {
        return new GFitSample(i, f, f2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitSample) {
                GFitSample gFitSample = (GFitSample) obj;
                if (!(this.step == gFitSample.step && Float.compare(this.distance, gFitSample.distance) == 0 && Float.compare(this.calorie, gFitSample.calorie) == 0 && this.startTime == gFitSample.startTime && this.endTime == gFitSample.endTime)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final float getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final float getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((this.step * 31) + Float.floatToIntBits(this.distance)) * 31) + Float.floatToIntBits(this.calorie)) * 31) + C.a(this.startTime)) * 31) + C.a(this.endTime);
    }

    @DexIgnore
    public final void setCalorie(float f) {
        this.calorie = f;
    }

    @DexIgnore
    public final void setDistance(float f) {
        this.distance = f;
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setStep(int i) {
        this.step = i;
    }

    @DexIgnore
    public String toString() {
        return "GFitSample(step=" + this.step + ", distance=" + this.distance + ", calorie=" + this.calorie + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
