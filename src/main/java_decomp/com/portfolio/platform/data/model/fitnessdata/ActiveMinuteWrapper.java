package com.portfolio.platform.data.model.fitnessdata;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveMinuteWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public int total;
    @DexIgnore
    public List<Boolean> values;

    @DexIgnore
    public ActiveMinuteWrapper(int i, List<Boolean> list, int i2) {
        Wg6.c(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = i2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActiveMinuteWrapper(com.fossil.fitness.ActiveMinute r4) {
        /*
            r3 = this;
            java.lang.String r0 = "activeMinute"
            com.mapped.Wg6.c(r4, r0)
            int r0 = r4.getResolutionInSecond()
            java.util.ArrayList r1 = r4.getValues()
            java.lang.String r2 = "activeMinute.values"
            com.mapped.Wg6.b(r1, r2)
            int r2 = r4.getTotal()
            r3.<init>(r0, r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper.<init>(com.fossil.fitness.ActiveMinute):void");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.fitnessdata.ActiveMinuteWrapper */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ActiveMinuteWrapper copy$default(ActiveMinuteWrapper activeMinuteWrapper, int i, List list, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = activeMinuteWrapper.resolutionInSecond;
        }
        if ((i3 & 2) != 0) {
            list = activeMinuteWrapper.values;
        }
        if ((i3 & 4) != 0) {
            i2 = activeMinuteWrapper.total;
        }
        return activeMinuteWrapper.copy(i, list, i2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Boolean> component2() {
        return this.values;
    }

    @DexIgnore
    public final int component3() {
        return this.total;
    }

    @DexIgnore
    public final ActiveMinuteWrapper copy(int i, List<Boolean> list, int i2) {
        Wg6.c(list, "values");
        return new ActiveMinuteWrapper(i, list, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ActiveMinuteWrapper) {
                ActiveMinuteWrapper activeMinuteWrapper = (ActiveMinuteWrapper) obj;
                if (!(this.resolutionInSecond == activeMinuteWrapper.resolutionInSecond && Wg6.a(this.values, activeMinuteWrapper.values) && this.total == activeMinuteWrapper.total)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final int getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Boolean> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = this.resolutionInSecond;
        List<Boolean> list = this.values;
        return (((list != null ? list.hashCode() : 0) + (i * 31)) * 31) + this.total;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(int i) {
        this.total = i;
    }

    @DexIgnore
    public final void setValues(List<Boolean> list) {
        Wg6.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "ActiveMinuteWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }
}
