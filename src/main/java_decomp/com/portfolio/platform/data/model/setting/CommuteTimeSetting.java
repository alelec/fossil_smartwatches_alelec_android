package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @Vu3("commuteAddress")
    public String address;
    @DexIgnore
    @Vu3("commuteAvoidTolls")
    public boolean avoidTolls;
    @DexIgnore
    @Vu3("commuteFormat")
    public String format;
    @DexIgnore
    @Vu3("commuteMovement")
    public String movement;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new CommuteTimeSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeSetting[] newArray(int i) {
            return new CommuteTimeSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeSetting() {
        this(null, null, false, null, 15, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommuteTimeSetting(android.os.Parcel r7) {
        /*
            r6 = this;
            r2 = 0
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r7, r0)
            java.lang.String r0 = r7.readString()
            java.lang.String r4 = ""
            if (r0 == 0) goto L_0x0026
        L_0x000e:
            java.lang.String r1 = r7.readString()
            if (r1 == 0) goto L_0x0029
        L_0x0014:
            byte r3 = r7.readByte()
            byte r5 = (byte) r2
            if (r3 == r5) goto L_0x001c
            r2 = 1
        L_0x001c:
            java.lang.String r3 = r7.readString()
            if (r3 == 0) goto L_0x002c
        L_0x0022:
            r6.<init>(r0, r1, r2, r3)
            return
        L_0x0026:
            java.lang.String r0 = ""
            goto L_0x000e
        L_0x0029:
            java.lang.String r1 = ""
            goto L_0x0014
        L_0x002c:
            r3 = r4
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeSetting.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public CommuteTimeSetting(String str, String str2, boolean z, String str3) {
        Wg6.c(str, "address");
        Wg6.c(str2, "format");
        Wg6.c(str3, "movement");
        this.address = str;
        this.format = str2;
        this.avoidTolls = z;
        this.movement = str3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ CommuteTimeSetting(String str, String str2, boolean z, String str3, int i, Qg6 qg6) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? "travel" : str2, (i & 4) != 0 ? true : z, (i & 8) != 0 ? "car" : str3);
    }

    @DexIgnore
    public static /* synthetic */ CommuteTimeSetting copy$default(CommuteTimeSetting commuteTimeSetting, String str, String str2, boolean z, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = commuteTimeSetting.address;
        }
        if ((i & 2) != 0) {
            str2 = commuteTimeSetting.format;
        }
        if ((i & 4) != 0) {
            z = commuteTimeSetting.avoidTolls;
        }
        if ((i & 8) != 0) {
            str3 = commuteTimeSetting.movement;
        }
        return commuteTimeSetting.copy(str, str2, z, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.address;
    }

    @DexIgnore
    public final String component2() {
        return this.format;
    }

    @DexIgnore
    public final boolean component3() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String component4() {
        return this.movement;
    }

    @DexIgnore
    public final CommuteTimeSetting copy(String str, String str2, boolean z, String str3) {
        Wg6.c(str, "address");
        Wg6.c(str2, "format");
        Wg6.c(str3, "movement");
        return new CommuteTimeSetting(str, str2, z, str3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CommuteTimeSetting) {
                CommuteTimeSetting commuteTimeSetting = (CommuteTimeSetting) obj;
                if (!Wg6.a(this.address, commuteTimeSetting.address) || !Wg6.a(this.format, commuteTimeSetting.format) || this.avoidTolls != commuteTimeSetting.avoidTolls || !Wg6.a(this.movement, commuteTimeSetting.movement)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final boolean getAvoidTolls() {
        return this.avoidTolls;
    }

    @DexIgnore
    public final String getFormat() {
        return this.format;
    }

    @DexIgnore
    public final String getMovement() {
        return this.movement;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.address;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.format;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        boolean z = this.avoidTolls;
        if (z) {
            z = true;
        }
        String str3 = this.movement;
        if (str3 != null) {
            i = str3.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + i;
    }

    @DexIgnore
    public final void setAddress(String str) {
        Wg6.c(str, "<set-?>");
        this.address = str;
    }

    @DexIgnore
    public final void setAvoidTolls(boolean z) {
        this.avoidTolls = z;
    }

    @DexIgnore
    public final void setFormat(String str) {
        Wg6.c(str, "<set-?>");
        this.format = str;
    }

    @DexIgnore
    public final void setMovement(String str) {
        Wg6.c(str, "<set-?>");
        this.movement = str;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeSetting(address=" + this.address + ", format=" + this.format + ", avoidTolls=" + this.avoidTolls + ", movement=" + this.movement + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeString(this.address);
        parcel.writeString(this.format);
        parcel.writeByte(this.avoidTolls ? (byte) 1 : 0);
        parcel.writeString(this.movement);
    }
}
