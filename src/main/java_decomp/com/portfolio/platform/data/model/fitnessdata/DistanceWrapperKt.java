package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.fitness.Distance;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DistanceWrapperKt {
    @DexIgnore
    public static final DistanceWrapper toDistanceWrapper(Distance distance) {
        if (distance == null) {
            return null;
        }
        int resolutionInSecond = distance.getResolutionInSecond();
        ArrayList<Double> values = distance.getValues();
        Wg6.b(values, "this.values");
        return new DistanceWrapper(resolutionInSecond, values, distance.getTotal());
    }
}
