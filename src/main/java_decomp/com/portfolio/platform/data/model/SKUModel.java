package com.portfolio.platform.data.model;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SKUModel {
    @DexIgnore
    public String createdAt;
    @DexIgnore
    public String deviceName;
    @DexIgnore
    public String deviceType;
    @DexIgnore
    public String fastPairId;
    @DexIgnore
    public String gender;
    @DexIgnore
    public String groupName;
    @DexIgnore
    public String serialNumberPrefix;
    @DexIgnore
    public String sku;
    @DexIgnore
    public String updatedAt;

    @DexIgnore
    public SKUModel(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        Wg6.c(str, "serialNumberPrefix");
        this.serialNumberPrefix = str;
        this.sku = str2;
        this.deviceName = str3;
        this.groupName = str4;
        this.gender = str5;
        this.deviceType = str6;
        this.fastPairId = str7;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SKUModel(String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, Qg6 qg6) {
        this(str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3, (i & 8) != 0 ? null : str4, (i & 16) != 0 ? null : str5, (i & 32) != 0 ? null : str6, (i & 64) == 0 ? str7 : null);
    }

    @DexIgnore
    public static /* synthetic */ SKUModel copy$default(SKUModel sKUModel, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i, Object obj) {
        return sKUModel.copy((i & 1) != 0 ? sKUModel.serialNumberPrefix : str, (i & 2) != 0 ? sKUModel.sku : str2, (i & 4) != 0 ? sKUModel.deviceName : str3, (i & 8) != 0 ? sKUModel.groupName : str4, (i & 16) != 0 ? sKUModel.gender : str5, (i & 32) != 0 ? sKUModel.deviceType : str6, (i & 64) != 0 ? sKUModel.fastPairId : str7);
    }

    @DexIgnore
    public final String component1() {
        return this.serialNumberPrefix;
    }

    @DexIgnore
    public final String component2() {
        return this.sku;
    }

    @DexIgnore
    public final String component3() {
        return this.deviceName;
    }

    @DexIgnore
    public final String component4() {
        return this.groupName;
    }

    @DexIgnore
    public final String component5() {
        return this.gender;
    }

    @DexIgnore
    public final String component6() {
        return this.deviceType;
    }

    @DexIgnore
    public final String component7() {
        return this.fastPairId;
    }

    @DexIgnore
    public final SKUModel copy(String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        Wg6.c(str, "serialNumberPrefix");
        return new SKUModel(str, str2, str3, str4, str5, str6, str7);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SKUModel) {
                SKUModel sKUModel = (SKUModel) obj;
                if (!Wg6.a(this.serialNumberPrefix, sKUModel.serialNumberPrefix) || !Wg6.a(this.sku, sKUModel.sku) || !Wg6.a(this.deviceName, sKUModel.deviceName) || !Wg6.a(this.groupName, sKUModel.groupName) || !Wg6.a(this.gender, sKUModel.gender) || !Wg6.a(this.deviceType, sKUModel.deviceType) || !Wg6.a(this.fastPairId, sKUModel.fastPairId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getDeviceName() {
        return this.deviceName;
    }

    @DexIgnore
    public final String getDeviceType() {
        return this.deviceType;
    }

    @DexIgnore
    public final String getFastPairId() {
        return this.fastPairId;
    }

    @DexIgnore
    public final String getGender() {
        return this.gender;
    }

    @DexIgnore
    public final String getGroupName() {
        return this.groupName;
    }

    @DexIgnore
    public final String getSerialNumberPrefix() {
        return this.serialNumberPrefix;
    }

    @DexIgnore
    public final String getSku() {
        return this.sku;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.serialNumberPrefix;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.sku;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.deviceName;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.groupName;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.gender;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.deviceType;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.fastPairId;
        if (str7 != null) {
            i = str7.hashCode();
        }
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + i;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDeviceName(String str) {
        this.deviceName = str;
    }

    @DexIgnore
    public final void setDeviceType(String str) {
        this.deviceType = str;
    }

    @DexIgnore
    public final void setFastPairId(String str) {
        this.fastPairId = str;
    }

    @DexIgnore
    public final void setGender(String str) {
        this.gender = str;
    }

    @DexIgnore
    public final void setGroupName(String str) {
        this.groupName = str;
    }

    @DexIgnore
    public final void setSerialNumberPrefix(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumberPrefix = str;
    }

    @DexIgnore
    public final void setSku(String str) {
        this.sku = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "SKUModel(serialNumberPrefix=" + this.serialNumberPrefix + ", sku=" + this.sku + ", deviceName=" + this.deviceName + ", groupName=" + this.groupName + ", gender=" + this.gender + ", deviceType=" + this.deviceType + ", fastPairId=" + this.fastPairId + ")";
    }
}
