package com.portfolio.platform.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mapped.Qg6;
import com.mapped.Vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = ServerSetting.TABLE_NAME)
public final class ServerSetting {
    @DexIgnore
    public static /* final */ String CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String KEY; // = "key";
    @DexIgnore
    public static /* final */ String OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "SERVER_SETTING";
    @DexIgnore
    public static /* final */ String UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String VALUE; // = "value";
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    @Vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @DatabaseField(columnName = "key", id = true)
    @Vu3("key")
    public String key;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    @Vu3("id")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    @Vu3("updatedAt")
    public String updateAt;
    @DexIgnore
    @DatabaseField(columnName = "value")
    @Vu3("value")
    public String value;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getKey() {
        return this.key;
    }

    @DexIgnore
    public final String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public final String getUpdateAt() {
        return this.updateAt;
    }

    @DexIgnore
    public final String getValue() {
        return this.value;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setKey(String str) {
        this.key = str;
    }

    @DexIgnore
    public final void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public final void setUpdateAt(String str) {
        this.updateAt = str;
    }

    @DexIgnore
    public final void setValue(String str) {
        this.value = str;
    }

    @DexIgnore
    public String toString() {
        return "key: " + this.key + " - value: " + this.value;
    }
}
