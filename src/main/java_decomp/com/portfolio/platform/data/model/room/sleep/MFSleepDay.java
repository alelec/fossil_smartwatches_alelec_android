package com.portfolio.platform.data.model.room.sleep;

import com.fossil.Ll5;
import com.mapped.Wg6;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFSleepDay implements Serializable {
    @DexIgnore
    public Double averageSleepOfWeek; // = Double.valueOf(0.0d);
    @DexIgnore
    public DateTime createdAt;
    @DexIgnore
    public Date date;
    @DexIgnore
    public int goalMinutes;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int sleepMinutes;
    @DexIgnore
    public SleepDistribution sleepStateDistInMinute;
    @DexIgnore
    public int timezoneOffset; // = Ll5.i();
    @DexIgnore
    public DateTime updatedAt;

    @DexIgnore
    public MFSleepDay(Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(date2, "date");
        this.date = date2;
        this.goalMinutes = i;
        this.sleepMinutes = i2;
        this.sleepStateDistInMinute = sleepDistribution;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ MFSleepDay copy$default(MFSleepDay mFSleepDay, Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2, int i3, Object obj) {
        return mFSleepDay.copy((i3 & 1) != 0 ? mFSleepDay.date : date2, (i3 & 2) != 0 ? mFSleepDay.goalMinutes : i, (i3 & 4) != 0 ? mFSleepDay.sleepMinutes : i2, (i3 & 8) != 0 ? mFSleepDay.sleepStateDistInMinute : sleepDistribution, (i3 & 16) != 0 ? mFSleepDay.createdAt : dateTime, (i3 & 32) != 0 ? mFSleepDay.updatedAt : dateTime2);
    }

    @DexIgnore
    public final Date component1() {
        return this.date;
    }

    @DexIgnore
    public final int component2() {
        return this.goalMinutes;
    }

    @DexIgnore
    public final int component3() {
        return this.sleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution component4() {
        return this.sleepStateDistInMinute;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final MFSleepDay copy(Date date2, int i, int i2, SleepDistribution sleepDistribution, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(date2, "date");
        return new MFSleepDay(date2, i, i2, sleepDistribution, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof MFSleepDay) {
                MFSleepDay mFSleepDay = (MFSleepDay) obj;
                if (!Wg6.a(this.date, mFSleepDay.date) || this.goalMinutes != mFSleepDay.goalMinutes || this.sleepMinutes != mFSleepDay.sleepMinutes || !Wg6.a(this.sleepStateDistInMinute, mFSleepDay.sleepStateDistInMinute) || !Wg6.a(this.createdAt, mFSleepDay.createdAt) || !Wg6.a(this.updatedAt, mFSleepDay.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Double getAverageSleepOfWeek() {
        return this.averageSleepOfWeek;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getGoalMinutes() {
        return this.goalMinutes;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getSleepMinutes() {
        return this.sleepMinutes;
    }

    @DexIgnore
    public final SleepDistribution getSleepStateDistInMinute() {
        return this.sleepStateDistInMinute;
    }

    @DexIgnore
    public final int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Date date2 = this.date;
        int hashCode = date2 != null ? date2.hashCode() : 0;
        int i2 = this.goalMinutes;
        int i3 = this.sleepMinutes;
        SleepDistribution sleepDistribution = this.sleepStateDistInMinute;
        int hashCode2 = sleepDistribution != null ? sleepDistribution.hashCode() : 0;
        DateTime dateTime = this.createdAt;
        int hashCode3 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((((hashCode * 31) + i2) * 31) + i3) * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setAverageSleepOfWeek(Double d) {
        this.averageSleepOfWeek = d;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        Wg6.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setGoalMinutes(int i) {
        this.goalMinutes = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSleepMinutes(int i) {
        this.sleepMinutes = i;
    }

    @DexIgnore
    public final void setSleepStateDistInMinute(SleepDistribution sleepDistribution) {
        this.sleepStateDistInMinute = sleepDistribution;
    }

    @DexIgnore
    public final void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepDay(date=" + this.date + ", goalMinutes=" + this.goalMinutes + ", sleepMinutes=" + this.sleepMinutes + ", sleepStateDistInMinute=" + this.sleepStateDistInMinute + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
