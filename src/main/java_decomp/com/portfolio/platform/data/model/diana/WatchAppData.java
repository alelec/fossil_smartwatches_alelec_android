package com.portfolio.platform.data.model.diana;

import com.mapped.Tu3;
import com.mapped.Vu3;
import com.mapped.Wg6;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppData {
    @DexIgnore
    @Tu3
    @Vu3("createdAt")
    public DateTime createdAt;
    @DexIgnore
    @Tu3
    @Vu3("executableBinaryDataUrl")
    public String executableBinaryDataUrl;
    @DexIgnore
    @Tu3
    @Vu3("id")
    public String id;
    @DexIgnore
    @Tu3
    @Vu3("maxAppVersion")
    public String maxAppVersion;
    @DexIgnore
    @Tu3
    @Vu3("maxFirmwareOSVersion")
    public String maxFirmwareOSVersion;
    @DexIgnore
    @Tu3
    @Vu3("minAppVersion")
    public String minAppVersion;
    @DexIgnore
    @Tu3
    @Vu3("minFirmwareOSVersion")
    public String minFirmwareOSVersion;
    @DexIgnore
    @Tu3
    @Vu3("type")
    public String type;
    @DexIgnore
    @Tu3
    @Vu3("updatedAt")
    public DateTime updatedAt;
    @DexIgnore
    @Tu3
    @Vu3("version")
    public String version;

    @DexIgnore
    public WatchAppData(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "type");
        Wg6.c(str3, "maxAppVersion");
        Wg6.c(str4, "maxFirmwareOSVersion");
        Wg6.c(str5, "minAppVersion");
        Wg6.c(str6, "minFirmwareOSVersion");
        Wg6.c(str7, "version");
        Wg6.c(str8, "executableBinaryDataUrl");
        Wg6.c(dateTime, "updatedAt");
        Wg6.c(dateTime2, "createdAt");
        this.id = str;
        this.type = str2;
        this.maxAppVersion = str3;
        this.maxFirmwareOSVersion = str4;
        this.minAppVersion = str5;
        this.minFirmwareOSVersion = str6;
        this.version = str7;
        this.executableBinaryDataUrl = str8;
        this.updatedAt = dateTime;
        this.createdAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ WatchAppData copy$default(WatchAppData watchAppData, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2, int i, Object obj) {
        return watchAppData.copy((i & 1) != 0 ? watchAppData.id : str, (i & 2) != 0 ? watchAppData.type : str2, (i & 4) != 0 ? watchAppData.maxAppVersion : str3, (i & 8) != 0 ? watchAppData.maxFirmwareOSVersion : str4, (i & 16) != 0 ? watchAppData.minAppVersion : str5, (i & 32) != 0 ? watchAppData.minFirmwareOSVersion : str6, (i & 64) != 0 ? watchAppData.version : str7, (i & 128) != 0 ? watchAppData.executableBinaryDataUrl : str8, (i & 256) != 0 ? watchAppData.updatedAt : dateTime, (i & 512) != 0 ? watchAppData.createdAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component10() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component2() {
        return this.type;
    }

    @DexIgnore
    public final String component3() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String component4() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String component5() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String component6() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String component7() {
        return this.version;
    }

    @DexIgnore
    public final String component8() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final DateTime component9() {
        return this.updatedAt;
    }

    @DexIgnore
    public final WatchAppData copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(str, "id");
        Wg6.c(str2, "type");
        Wg6.c(str3, "maxAppVersion");
        Wg6.c(str4, "maxFirmwareOSVersion");
        Wg6.c(str5, "minAppVersion");
        Wg6.c(str6, "minFirmwareOSVersion");
        Wg6.c(str7, "version");
        Wg6.c(str8, "executableBinaryDataUrl");
        Wg6.c(dateTime, "updatedAt");
        Wg6.c(dateTime2, "createdAt");
        return new WatchAppData(str, str2, str3, str4, str5, str6, str7, str8, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchAppData) {
                WatchAppData watchAppData = (WatchAppData) obj;
                if (!Wg6.a(this.id, watchAppData.id) || !Wg6.a(this.type, watchAppData.type) || !Wg6.a(this.maxAppVersion, watchAppData.maxAppVersion) || !Wg6.a(this.maxFirmwareOSVersion, watchAppData.maxFirmwareOSVersion) || !Wg6.a(this.minAppVersion, watchAppData.minAppVersion) || !Wg6.a(this.minFirmwareOSVersion, watchAppData.minFirmwareOSVersion) || !Wg6.a(this.version, watchAppData.version) || !Wg6.a(this.executableBinaryDataUrl, watchAppData.executableBinaryDataUrl) || !Wg6.a(this.updatedAt, watchAppData.updatedAt) || !Wg6.a(this.createdAt, watchAppData.createdAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getExecutableBinaryDataUrl() {
        return this.executableBinaryDataUrl;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getMaxAppVersion() {
        return this.maxAppVersion;
    }

    @DexIgnore
    public final String getMaxFirmwareOSVersion() {
        return this.maxFirmwareOSVersion;
    }

    @DexIgnore
    public final String getMinAppVersion() {
        return this.minAppVersion;
    }

    @DexIgnore
    public final String getMinFirmwareOSVersion() {
        return this.minFirmwareOSVersion;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.type;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.maxAppVersion;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.maxFirmwareOSVersion;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.minAppVersion;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        String str6 = this.minFirmwareOSVersion;
        int hashCode6 = str6 != null ? str6.hashCode() : 0;
        String str7 = this.version;
        int hashCode7 = str7 != null ? str7.hashCode() : 0;
        String str8 = this.executableBinaryDataUrl;
        int hashCode8 = str8 != null ? str8.hashCode() : 0;
        DateTime dateTime = this.updatedAt;
        int hashCode9 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.createdAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + i;
    }

    @DexIgnore
    public final void setCreatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.createdAt = dateTime;
    }

    @DexIgnore
    public final void setExecutableBinaryDataUrl(String str) {
        Wg6.c(str, "<set-?>");
        this.executableBinaryDataUrl = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setMaxAppVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.maxAppVersion = str;
    }

    @DexIgnore
    public final void setMaxFirmwareOSVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.maxFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setMinAppVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.minAppVersion = str;
    }

    @DexIgnore
    public final void setMinFirmwareOSVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.minFirmwareOSVersion = str;
    }

    @DexIgnore
    public final void setType(String str) {
        Wg6.c(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public final void setUpdatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public final void setVersion(String str) {
        Wg6.c(str, "<set-?>");
        this.version = str;
    }

    @DexIgnore
    public String toString() {
        return "WatchAppData(id=" + this.id + ", type=" + this.type + ", maxAppVersion=" + this.maxAppVersion + ", maxFirmwareOSVersion=" + this.maxFirmwareOSVersion + ", minAppVersion=" + this.minAppVersion + ", minFirmwareOSVersion=" + this.minFirmwareOSVersion + ", version=" + this.version + ", executableBinaryDataUrl=" + this.executableBinaryDataUrl + ", updatedAt=" + this.updatedAt + ", createdAt=" + this.createdAt + ")";
    }
}
