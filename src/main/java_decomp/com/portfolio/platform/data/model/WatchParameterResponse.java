package com.portfolio.platform.data.model;

import com.google.gson.Gson;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParameterResponse {
    @DexIgnore
    @Vu3("category")
    public String category;
    @DexIgnore
    @Vu3("data")
    public ResponseData data;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("metadata")
    public MetaData metaData;
    @DexIgnore
    @Vu3("name")
    public String name;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MetaData {
        @DexIgnore
        @Vu3("deviceLongName")
        public String deviceLongName;
        @DexIgnore
        @Vu3("deviceShortName")
        public String deviceShortName;
        @DexIgnore
        @Vu3("mainHandsFlipped")
        public boolean mainHandsFlipped;
        @DexIgnore
        @Vu3("prefixSerialNumbers")
        public ArrayList<String> serialNumbers;
        @DexIgnore
        @Vu3("themeMode")
        public String themeMode;
        @DexIgnore
        @Vu3("version")
        public Version version;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Version {
            @DexIgnore
            @Vu3("major")
            public int versionMajor;
            @DexIgnore
            @Vu3("minor")
            public int versionMinor;

            @DexIgnore
            public final int getVersionMajor() {
                return this.versionMajor;
            }

            @DexIgnore
            public final int getVersionMinor() {
                return this.versionMinor;
            }

            @DexIgnore
            public final void setVersionMajor(int i) {
                this.versionMajor = i;
            }

            @DexIgnore
            public final void setVersionMinor(int i) {
                this.versionMinor = i;
            }
        }

        @DexIgnore
        public final String getDeviceLongName() {
            return this.deviceLongName;
        }

        @DexIgnore
        public final String getDeviceShortName() {
            return this.deviceShortName;
        }

        @DexIgnore
        public final boolean getMainHandsFlipped() {
            return this.mainHandsFlipped;
        }

        @DexIgnore
        public final ArrayList<String> getSerialNumbers() {
            return this.serialNumbers;
        }

        @DexIgnore
        public final String getThemeMode() {
            return this.themeMode;
        }

        @DexIgnore
        public final Version getVersion() {
            Version version2 = this.version;
            if (version2 != null) {
                return version2;
            }
            Wg6.n("version");
            throw null;
        }

        @DexIgnore
        public final void setDeviceLongName(String str) {
            this.deviceLongName = str;
        }

        @DexIgnore
        public final void setDeviceShortName(String str) {
            this.deviceShortName = str;
        }

        @DexIgnore
        public final void setMainHandsFlipped(boolean z) {
            this.mainHandsFlipped = z;
        }

        @DexIgnore
        public final void setSerialNumbers(ArrayList<String> arrayList) {
            this.serialNumbers = arrayList;
        }

        @DexIgnore
        public final void setThemeMode(String str) {
            this.themeMode = str;
        }

        @DexIgnore
        public final void setVersion(Version version2) {
            Wg6.c(version2, "<set-?>");
            this.version = version2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ResponseData {
        @DexIgnore
        @Vu3("content")
        public String content;

        @DexIgnore
        public final String getContent() {
            return this.content;
        }

        @DexIgnore
        public final void setContent(String str) {
            this.content = str;
        }
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final ResponseData getData() {
        return this.data;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final MetaData getMetaData() {
        return this.metaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final void setCategory(String str) {
        this.category = str;
    }

    @DexIgnore
    public final void setData(ResponseData responseData) {
        this.data = responseData;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMetaData(MetaData metaData2) {
        this.metaData = metaData2;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "gson.toJson(this)");
        return t;
    }

    @DexIgnore
    public final WatchParam toWatchParamModel(String str) {
        String str2 = null;
        Wg6.c(str, "serial");
        MetaData metaData2 = this.metaData;
        if (metaData2 != null) {
            int versionMajor = metaData2.getVersion().getVersionMajor();
            MetaData metaData3 = this.metaData;
            if (metaData3 != null) {
                int versionMinor = metaData3.getVersion().getVersionMinor();
                ResponseData responseData = this.data;
                if (responseData != null) {
                    str2 = responseData.getContent();
                }
                return new WatchParam(str, String.valueOf(versionMajor), String.valueOf(versionMinor), str2);
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }
}
