package com.portfolio.platform.data.model.microapp.weather;

import com.mapped.Vu3;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Temperature {
    @DexIgnore
    @Vu3("currently")
    public float currently;
    @DexIgnore
    @Vu3("max")
    public float max;
    @DexIgnore
    @Vu3("min")
    public float min;
    @DexIgnore
    @Vu3(Constants.PROFILE_KEY_UNIT)
    public String unit;

    @DexIgnore
    public float getCurrently() {
        return this.currently;
    }

    @DexIgnore
    public float getMax() {
        return this.max;
    }

    @DexIgnore
    public float getMin() {
        return this.min;
    }

    @DexIgnore
    public String getUnit() {
        return this.unit;
    }
}
