package com.portfolio.platform.data.model;

import com.fossil.Zi4;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.UUID;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "pin_object")
public class PinObject {
    @DexIgnore
    public static /* final */ String COLUMN_CLASS_NAME; // = "className";
    @DexIgnore
    public static /* final */ String COLUMN_JSON_DATA; // = "json";
    @DexIgnore
    public static /* final */ String COLUMN_UUID; // = "uuid";
    @DexIgnore
    public static /* final */ String TAG; // = "PinObject";
    @DexIgnore
    @DatabaseField(columnName = COLUMN_CLASS_NAME)
    public String className;
    @DexIgnore
    @DatabaseField(columnName = "json")
    public String jsonData;
    @DexIgnore
    @DatabaseField(columnName = "uuid", id = true)
    public String uuid;

    @DexIgnore
    public PinObject() {
    }

    @DexIgnore
    public PinObject(String str, Object obj) {
        this.className = str;
        Zi4 zi4 = new Zi4();
        zi4.f(DateTime.class, new GsonConvertDateTime());
        this.jsonData = zi4.d().t(obj);
        this.uuid = UUID.randomUUID().toString();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Init new " + TAG + " - uuid=" + this.uuid + ", className=" + str + ", json=" + this.jsonData);
    }

    @DexIgnore
    public String getClassName() {
        return this.className;
    }

    @DexIgnore
    public String getJsonData() {
        return this.jsonData;
    }

    @DexIgnore
    public String getUuid() {
        return this.uuid;
    }

    @DexIgnore
    public void setClassName(String str) {
        this.className = str;
    }

    @DexIgnore
    public void setJsonData(Object obj) {
        this.jsonData = new Gson().t(obj);
    }

    @DexIgnore
    public void setJsonData(String str) {
        this.jsonData = str;
    }

    @DexIgnore
    public void setUuid(String str) {
        this.uuid = str;
    }
}
