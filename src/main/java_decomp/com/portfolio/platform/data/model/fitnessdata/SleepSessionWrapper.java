package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Wg6;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionWrapper {
    @DexIgnore
    public /* final */ int awakeMinutes;
    @DexIgnore
    public /* final */ int deepSleepMinutes;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ HeartRateWrapper heartRate;
    @DexIgnore
    public /* final */ int lightSleepMinutes;
    @DexIgnore
    public /* final */ int normalizedSleepQuality;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public /* final */ List<SleepStateChangeWrapper> stateChanges;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SleepSessionWrapper(com.fossil.fitness.SleepSession r11) {
        /*
            r10 = this;
            r8 = 1000(0x3e8, double:4.94E-321)
            r6 = 0
            java.lang.String r0 = "sleepSession"
            com.mapped.Wg6.c(r11, r0)
            org.joda.time.DateTime r1 = new org.joda.time.DateTime
            int r0 = r11.getStartTime()
            long r2 = (long) r0
            long r2 = r2 * r8
            int r0 = r11.getTimezoneOffsetInSecond()
            int r0 = r0 * 1000
            org.joda.time.DateTimeZone r0 = org.joda.time.DateTimeZone.forOffsetMillis(r0)
            r1.<init>(r2, r0)
            org.joda.time.DateTime r2 = new org.joda.time.DateTime
            int r0 = r11.getEndTime()
            long r4 = (long) r0
            long r4 = r4 * r8
            int r0 = r11.getTimezoneOffsetInSecond()
            int r0 = r0 * 1000
            org.joda.time.DateTimeZone r0 = org.joda.time.DateTimeZone.forOffsetMillis(r0)
            r2.<init>(r4, r0)
            int r3 = r11.getAwakeMinutes()
            int r4 = r11.getLightSleepMinutes()
            int r5 = r11.getDeepSleepMinutes()
            com.fossil.fitness.HeartRate r0 = r11.getHeartrate()
            if (r0 == 0) goto L_0x0054
            com.fossil.fitness.HeartRate r0 = r11.getHeartrate()
            if (r0 == 0) goto L_0x0089
            java.lang.String r6 = "sleepSession.heartrate!!"
            com.mapped.Wg6.b(r0, r6)
            com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper r6 = new com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper
            r6.<init>(r0)
        L_0x0054:
            int r7 = r11.getQuality()
            int r8 = r11.getTimezoneOffsetInSecond()
            r0 = r10
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            java.util.ArrayList r0 = r11.getStateChanges()
            java.lang.String r1 = "sleepSession.stateChanges"
            com.mapped.Wg6.b(r0, r1)
            java.util.Iterator r1 = r0.iterator()
        L_0x006d:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x008d
            java.lang.Object r0 = r1.next()
            com.fossil.fitness.SleepStateChange r0 = (com.fossil.fitness.SleepStateChange) r0
            java.util.List<com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper> r2 = r10.stateChanges
            java.lang.String r3 = "it"
            com.mapped.Wg6.b(r0, r3)
            com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper r3 = new com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper
            r3.<init>(r0)
            r2.add(r3)
            goto L_0x006d
        L_0x0089:
            com.mapped.Wg6.i()
            throw r6
        L_0x008d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper.<init>(com.fossil.fitness.SleepSession):void");
    }

    @DexIgnore
    public SleepSessionWrapper(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        Wg6.c(dateTime, SampleRaw.COLUMN_START_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.awakeMinutes = i;
        this.lightSleepMinutes = i2;
        this.deepSleepMinutes = i3;
        this.heartRate = heartRateWrapper;
        this.normalizedSleepQuality = i4;
        this.timezoneOffsetInSecond = i5;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionWrapper copy$default(SleepSessionWrapper sleepSessionWrapper, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5, int i6, Object obj) {
        return sleepSessionWrapper.copy((i6 & 1) != 0 ? sleepSessionWrapper.startTime : dateTime, (i6 & 2) != 0 ? sleepSessionWrapper.endTime : dateTime2, (i6 & 4) != 0 ? sleepSessionWrapper.awakeMinutes : i, (i6 & 8) != 0 ? sleepSessionWrapper.lightSleepMinutes : i2, (i6 & 16) != 0 ? sleepSessionWrapper.deepSleepMinutes : i3, (i6 & 32) != 0 ? sleepSessionWrapper.heartRate : heartRateWrapper, (i6 & 64) != 0 ? sleepSessionWrapper.normalizedSleepQuality : i4, (i6 & 128) != 0 ? sleepSessionWrapper.timezoneOffsetInSecond : i5);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int component4() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int component5() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final HeartRateWrapper component6() {
        return this.heartRate;
    }

    @DexIgnore
    public final int component7() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final SleepSessionWrapper copy(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        Wg6.c(dateTime, SampleRaw.COLUMN_START_TIME);
        Wg6.c(dateTime2, SampleRaw.COLUMN_END_TIME);
        return new SleepSessionWrapper(dateTime, dateTime2, i, i2, i3, heartRateWrapper, i4, i5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof SleepSessionWrapper) {
                SleepSessionWrapper sleepSessionWrapper = (SleepSessionWrapper) obj;
                if (!(Wg6.a(this.startTime, sleepSessionWrapper.startTime) && Wg6.a(this.endTime, sleepSessionWrapper.endTime) && this.awakeMinutes == sleepSessionWrapper.awakeMinutes && this.lightSleepMinutes == sleepSessionWrapper.lightSleepMinutes && this.deepSleepMinutes == sleepSessionWrapper.deepSleepMinutes && Wg6.a(this.heartRate, sleepSessionWrapper.heartRate) && this.normalizedSleepQuality == sleepSessionWrapper.normalizedSleepQuality && this.timezoneOffsetInSecond == sleepSessionWrapper.timezoneOffsetInSecond)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getAwakeMinutes() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int getDeepSleepMinutes() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final int getLightSleepMinutes() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<SleepStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        DateTime dateTime = this.startTime;
        int hashCode = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = dateTime2 != null ? dateTime2.hashCode() : 0;
        int i2 = this.awakeMinutes;
        int i3 = this.lightSleepMinutes;
        int i4 = this.deepSleepMinutes;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return (((((((((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + i3) * 31) + i4) * 31) + i) * 31) + this.normalizedSleepQuality) * 31) + this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionWrapper(startTime=" + this.startTime + ", endTime=" + this.endTime + ", awakeMinutes=" + this.awakeMinutes + ", lightSleepMinutes=" + this.lightSleepMinutes + ", deepSleepMinutes=" + this.deepSleepMinutes + ", heartRate=" + this.heartRate + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ")";
    }
}
