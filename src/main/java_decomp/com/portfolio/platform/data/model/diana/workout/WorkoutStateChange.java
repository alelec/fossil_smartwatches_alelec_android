package com.portfolio.platform.data.model.diana.workout;

import com.mapped.Di4;
import com.mapped.Wg6;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChange {
    @DexIgnore
    public /* final */ Di4 state;
    @DexIgnore
    public /* final */ Date time;

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public WorkoutStateChange(int i, com.fossil.fitness.WorkoutStateChange workoutStateChange) {
        this(Di4.Companion.a(workoutStateChange.getState().name()), new Date(((long) (workoutStateChange.getIndexInSecond() + i)) * 1000));
        Wg6.c(workoutStateChange, "workoutStageChange");
    }

    @DexIgnore
    public WorkoutStateChange(Di4 di4, Date date) {
        Wg6.c(di4, "state");
        Wg6.c(date, LogBuilder.KEY_TIME);
        this.state = di4;
        this.time = date;
    }

    @DexIgnore
    private final Di4 component1() {
        return this.state;
    }

    @DexIgnore
    private final Date component2() {
        return this.time;
    }

    @DexIgnore
    public static /* synthetic */ WorkoutStateChange copy$default(WorkoutStateChange workoutStateChange, Di4 di4, Date date, int i, Object obj) {
        if ((i & 1) != 0) {
            di4 = workoutStateChange.state;
        }
        if ((i & 2) != 0) {
            date = workoutStateChange.time;
        }
        return workoutStateChange.copy(di4, date);
    }

    @DexIgnore
    public final WorkoutStateChange copy(Di4 di4, Date date) {
        Wg6.c(di4, "state");
        Wg6.c(date, LogBuilder.KEY_TIME);
        return new WorkoutStateChange(di4, date);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WorkoutStateChange) {
                WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
                if (!Wg6.a(this.state, workoutStateChange.state) || !Wg6.a(this.time, workoutStateChange.time)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Di4 di4 = this.state;
        int hashCode = di4 != null ? di4.hashCode() : 0;
        Date date = this.time;
        if (date != null) {
            i = date.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange(state=" + this.state + ", time=" + this.time + ")";
    }
}
