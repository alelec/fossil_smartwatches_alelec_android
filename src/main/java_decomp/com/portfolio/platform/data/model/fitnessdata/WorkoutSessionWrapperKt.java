package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.Im7;
import com.mapped.Wg6;
import com.portfolio.platform.data.ServerWorkoutSessionWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionWrapperKt {
    @DexIgnore
    public static final List<ServerWorkoutSessionWrapper> toServerWorkoutSessionWrapperList(List<WorkoutSessionWrapper> list) {
        Wg6.c(list, "$this$toServerWorkoutSessionWrapperList");
        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
        for (T t : list) {
            arrayList.add(new ServerWorkoutSessionWrapper(t.getId(), t.getStartTime(), t.getEndTime(), t.getTimezoneOffsetInSecond(), t.getDuration(), t.getType(), t.getMode(), t.getPace(), t.getCadence(), t.getStateChanges(), t.getEncodedGpsData(), t.getStep(), t.getCalorie(), t.getDistance(), t.getHeartRate()));
        }
        return arrayList;
    }
}
