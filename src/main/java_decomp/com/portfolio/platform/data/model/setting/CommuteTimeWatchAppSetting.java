package com.portfolio.platform.data.model.setting;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppSetting implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @Vu3("addresses")
    public List<AddressWrapper> addresses;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<CommuteTimeWatchAppSetting> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeWatchAppSetting createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new CommuteTimeWatchAppSetting(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public CommuteTimeWatchAppSetting[] newArray(int i) {
            return new CommuteTimeWatchAppSetting[i];
        }
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting() {
        this(null, 1, null);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CommuteTimeWatchAppSetting(android.os.Parcel r3) {
        /*
            r2 = this;
            java.lang.String r0 = "parcel"
            com.mapped.Wg6.c(r3, r0)
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            java.lang.Class<com.portfolio.platform.data.model.diana.commutetime.AddressWrapper> r1 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.class
            java.lang.ClassLoader r1 = r1.getClassLoader()
            r3.readList(r0, r1)
            r2.<init>(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.<init>(android.os.Parcel):void");
    }

    @DexIgnore
    public CommuteTimeWatchAppSetting(List<AddressWrapper> list) {
        Wg6.c(list, "addresses");
        this.addresses = list;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CommuteTimeWatchAppSetting(java.util.List r6, int r7, com.mapped.Qg6 r8) {
        /*
            r5 = this;
            r0 = r7 & 1
            if (r0 == 0) goto L_0x002b
            r0 = 2
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper[] r0 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper[r0]
            r1 = 0
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r2 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r3 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.HOME
            java.lang.String r3 = r3.getValue()
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r4 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.HOME
            r2.<init>(r3, r4)
            r0[r1] = r2
            r1 = 1
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper r2 = new com.portfolio.platform.data.model.diana.commutetime.AddressWrapper
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r3 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.WORK
            java.lang.String r3 = r3.getValue()
            com.portfolio.platform.data.model.diana.commutetime.AddressWrapper$AddressType r4 = com.portfolio.platform.data.model.diana.commutetime.AddressWrapper.AddressType.WORK
            r2.<init>(r3, r4)
            r0[r1] = r2
            java.util.List r6 = com.fossil.Hm7.i(r0)
        L_0x002b:
            r5.<init>(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting.<init>(java.util.List, int, com.mapped.Qg6):void");
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CommuteTimeWatchAppSetting copy$default(CommuteTimeWatchAppSetting commuteTimeWatchAppSetting, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            list = commuteTimeWatchAppSetting.addresses;
        }
        return commuteTimeWatchAppSetting.copy(list);
    }

    @DexIgnore
    public final List<AddressWrapper> component1() {
        return this.addresses;
    }

    @DexIgnore
    public final CommuteTimeWatchAppSetting copy(List<AddressWrapper> list) {
        Wg6.c(list, "addresses");
        return new CommuteTimeWatchAppSetting(list);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof CommuteTimeWatchAppSetting) && Wg6.a(this.addresses, ((CommuteTimeWatchAppSetting) obj).addresses));
    }

    @DexIgnore
    public final AddressWrapper getAddressByName(String str) {
        Wg6.c(str, "name");
        for (AddressWrapper addressWrapper : this.addresses) {
            if (Wg6.a(addressWrapper.getName(), str)) {
                return addressWrapper;
            }
        }
        return null;
    }

    @DexIgnore
    public final List<AddressWrapper> getAddresses() {
        return this.addresses;
    }

    @DexIgnore
    public final ArrayList<String> getListAddressNameExceptOf(AddressWrapper addressWrapper) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (AddressWrapper addressWrapper2 : this.addresses) {
            if (!Wg6.a(addressWrapper2.getId(), addressWrapper != null ? addressWrapper.getId() : null)) {
                arrayList.add(addressWrapper2.getName());
            }
        }
        return arrayList;
    }

    @DexIgnore
    public int hashCode() {
        List<AddressWrapper> list = this.addresses;
        if (list != null) {
            return list.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setAddresses(List<AddressWrapper> list) {
        Wg6.c(list, "<set-?>");
        this.addresses = list;
    }

    @DexIgnore
    public String toString() {
        return "CommuteTimeWatchAppSetting(addresses=" + this.addresses + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        Wg6.c(parcel, "parcel");
        parcel.writeList(this.addresses);
    }
}
