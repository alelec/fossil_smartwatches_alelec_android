package com.portfolio.platform.data.model.diana.preset;

import android.graphics.drawable.Drawable;
import com.mapped.Ai4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceWrapper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public Drawable background;
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public Drawable bottomComplication;
    @DexIgnore
    public MetaData bottomMetaData;
    @DexIgnore
    public Drawable combination;
    @DexIgnore
    public String id;
    @DexIgnore
    public boolean isRemoveIconVisible;
    @DexIgnore
    public Drawable leftComplication;
    @DexIgnore
    public MetaData leftMetaData;
    @DexIgnore
    public String name;
    @DexIgnore
    public Drawable rightComplication;
    @DexIgnore
    public MetaData rightMetaData;
    @DexIgnore
    public Drawable topComplication;
    @DexIgnore
    public MetaData topMetaData;
    @DexIgnore
    public Ai4 type;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final WatchFaceWrapper createBackgroundWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig, Ai4 ai4) {
            Wg6.c(str, "id");
            Wg6.c(str2, "name");
            Wg6.c(backgroundConfig, "backgroundConfig");
            Wg6.c(ai4, "type");
            return new WatchFaceWrapper(str, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str2, backgroundConfig, ai4, false, 16384, null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class MetaData {
        @DexIgnore
        public Integer selectedBackgroundColor;
        @DexIgnore
        public Integer selectedForegroundColor;
        @DexIgnore
        public Integer unselectedBackgroundColor;
        @DexIgnore
        public Integer unselectedForegroundColor;

        @DexIgnore
        public MetaData(Integer num, Integer num2, Integer num3, Integer num4) {
            this.selectedForegroundColor = num;
            this.selectedBackgroundColor = num2;
            this.unselectedForegroundColor = num3;
            this.unselectedBackgroundColor = num4;
        }

        @DexIgnore
        public static /* synthetic */ MetaData copy$default(MetaData metaData, Integer num, Integer num2, Integer num3, Integer num4, int i, Object obj) {
            if ((i & 1) != 0) {
                num = metaData.selectedForegroundColor;
            }
            if ((i & 2) != 0) {
                num2 = metaData.selectedBackgroundColor;
            }
            if ((i & 4) != 0) {
                num3 = metaData.unselectedForegroundColor;
            }
            if ((i & 8) != 0) {
                num4 = metaData.unselectedBackgroundColor;
            }
            return metaData.copy(num, num2, num3, num4);
        }

        @DexIgnore
        public final Integer component1() {
            return this.selectedForegroundColor;
        }

        @DexIgnore
        public final Integer component2() {
            return this.selectedBackgroundColor;
        }

        @DexIgnore
        public final Integer component3() {
            return this.unselectedForegroundColor;
        }

        @DexIgnore
        public final Integer component4() {
            return this.unselectedBackgroundColor;
        }

        @DexIgnore
        public final MetaData copy(Integer num, Integer num2, Integer num3, Integer num4) {
            return new MetaData(num, num2, num3, num4);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof MetaData) {
                    MetaData metaData = (MetaData) obj;
                    if (!Wg6.a(this.selectedForegroundColor, metaData.selectedForegroundColor) || !Wg6.a(this.selectedBackgroundColor, metaData.selectedBackgroundColor) || !Wg6.a(this.unselectedForegroundColor, metaData.unselectedForegroundColor) || !Wg6.a(this.unselectedBackgroundColor, metaData.unselectedBackgroundColor)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final Integer getSelectedBackgroundColor() {
            return this.selectedBackgroundColor;
        }

        @DexIgnore
        public final Integer getSelectedForegroundColor() {
            return this.selectedForegroundColor;
        }

        @DexIgnore
        public final Integer getUnselectedBackgroundColor() {
            return this.unselectedBackgroundColor;
        }

        @DexIgnore
        public final Integer getUnselectedForegroundColor() {
            return this.unselectedForegroundColor;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Integer num = this.selectedForegroundColor;
            int hashCode = num != null ? num.hashCode() : 0;
            Integer num2 = this.selectedBackgroundColor;
            int hashCode2 = num2 != null ? num2.hashCode() : 0;
            Integer num3 = this.unselectedForegroundColor;
            int hashCode3 = num3 != null ? num3.hashCode() : 0;
            Integer num4 = this.unselectedBackgroundColor;
            if (num4 != null) {
                i = num4.hashCode();
            }
            return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
        }

        @DexIgnore
        public final void setSelectedBackgroundColor(Integer num) {
            this.selectedBackgroundColor = num;
        }

        @DexIgnore
        public final void setSelectedForegroundColor(Integer num) {
            this.selectedForegroundColor = num;
        }

        @DexIgnore
        public final void setUnselectedBackgroundColor(Integer num) {
            this.unselectedBackgroundColor = num;
        }

        @DexIgnore
        public final void setUnselectedForegroundColor(Integer num) {
            this.unselectedForegroundColor = num;
        }

        @DexIgnore
        public String toString() {
            return "MetaData(selectedForegroundColor=" + this.selectedForegroundColor + ", selectedBackgroundColor=" + this.selectedBackgroundColor + ", unselectedForegroundColor=" + this.unselectedForegroundColor + ", unselectedBackgroundColor=" + this.unselectedBackgroundColor + ")";
        }
    }

    @DexIgnore
    public WatchFaceWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, Ai4 ai4, boolean z) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(backgroundConfig2, "backgroundConfig");
        Wg6.c(ai4, "type");
        this.id = str;
        this.combination = drawable;
        this.background = drawable2;
        this.topComplication = drawable3;
        this.rightComplication = drawable4;
        this.bottomComplication = drawable5;
        this.leftComplication = drawable6;
        this.topMetaData = metaData;
        this.rightMetaData = metaData2;
        this.bottomMetaData = metaData3;
        this.leftMetaData = metaData4;
        this.name = str2;
        this.backgroundConfig = backgroundConfig2;
        this.type = ai4;
        this.isRemoveIconVisible = z;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ WatchFaceWrapper(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, Ai4 ai4, boolean z, int i, Qg6 qg6) {
        this(str, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str2, backgroundConfig2, ai4, (i & 16384) != 0 ? false : z);
    }

    @DexIgnore
    public static /* synthetic */ WatchFaceWrapper copy$default(WatchFaceWrapper watchFaceWrapper, String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, Ai4 ai4, boolean z, int i, Object obj) {
        return watchFaceWrapper.copy((i & 1) != 0 ? watchFaceWrapper.id : str, (i & 2) != 0 ? watchFaceWrapper.combination : drawable, (i & 4) != 0 ? watchFaceWrapper.background : drawable2, (i & 8) != 0 ? watchFaceWrapper.topComplication : drawable3, (i & 16) != 0 ? watchFaceWrapper.rightComplication : drawable4, (i & 32) != 0 ? watchFaceWrapper.bottomComplication : drawable5, (i & 64) != 0 ? watchFaceWrapper.leftComplication : drawable6, (i & 128) != 0 ? watchFaceWrapper.topMetaData : metaData, (i & 256) != 0 ? watchFaceWrapper.rightMetaData : metaData2, (i & 512) != 0 ? watchFaceWrapper.bottomMetaData : metaData3, (i & 1024) != 0 ? watchFaceWrapper.leftMetaData : metaData4, (i & 2048) != 0 ? watchFaceWrapper.name : str2, (i & 4096) != 0 ? watchFaceWrapper.backgroundConfig : backgroundConfig2, (i & 8192) != 0 ? watchFaceWrapper.type : ai4, (i & 16384) != 0 ? watchFaceWrapper.isRemoveIconVisible : z);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final MetaData component10() {
        return this.bottomMetaData;
    }

    @DexIgnore
    public final MetaData component11() {
        return this.leftMetaData;
    }

    @DexIgnore
    public final String component12() {
        return this.name;
    }

    @DexIgnore
    public final BackgroundConfig component13() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final Ai4 component14() {
        return this.type;
    }

    @DexIgnore
    public final boolean component15() {
        return this.isRemoveIconVisible;
    }

    @DexIgnore
    public final Drawable component2() {
        return this.combination;
    }

    @DexIgnore
    public final Drawable component3() {
        return this.background;
    }

    @DexIgnore
    public final Drawable component4() {
        return this.topComplication;
    }

    @DexIgnore
    public final Drawable component5() {
        return this.rightComplication;
    }

    @DexIgnore
    public final Drawable component6() {
        return this.bottomComplication;
    }

    @DexIgnore
    public final Drawable component7() {
        return this.leftComplication;
    }

    @DexIgnore
    public final MetaData component8() {
        return this.topMetaData;
    }

    @DexIgnore
    public final MetaData component9() {
        return this.rightMetaData;
    }

    @DexIgnore
    public final WatchFaceWrapper copy(String str, Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4, Drawable drawable5, Drawable drawable6, MetaData metaData, MetaData metaData2, MetaData metaData3, MetaData metaData4, String str2, BackgroundConfig backgroundConfig2, Ai4 ai4, boolean z) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(backgroundConfig2, "backgroundConfig");
        Wg6.c(ai4, "type");
        return new WatchFaceWrapper(str, drawable, drawable2, drawable3, drawable4, drawable5, drawable6, metaData, metaData2, metaData3, metaData4, str2, backgroundConfig2, ai4, z);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchFaceWrapper) {
                WatchFaceWrapper watchFaceWrapper = (WatchFaceWrapper) obj;
                if (!Wg6.a(this.id, watchFaceWrapper.id) || !Wg6.a(this.combination, watchFaceWrapper.combination) || !Wg6.a(this.background, watchFaceWrapper.background) || !Wg6.a(this.topComplication, watchFaceWrapper.topComplication) || !Wg6.a(this.rightComplication, watchFaceWrapper.rightComplication) || !Wg6.a(this.bottomComplication, watchFaceWrapper.bottomComplication) || !Wg6.a(this.leftComplication, watchFaceWrapper.leftComplication) || !Wg6.a(this.topMetaData, watchFaceWrapper.topMetaData) || !Wg6.a(this.rightMetaData, watchFaceWrapper.rightMetaData) || !Wg6.a(this.bottomMetaData, watchFaceWrapper.bottomMetaData) || !Wg6.a(this.leftMetaData, watchFaceWrapper.leftMetaData) || !Wg6.a(this.name, watchFaceWrapper.name) || !Wg6.a(this.backgroundConfig, watchFaceWrapper.backgroundConfig) || !Wg6.a(this.type, watchFaceWrapper.type) || this.isRemoveIconVisible != watchFaceWrapper.isRemoveIconVisible) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final Drawable getBackground() {
        return this.background;
    }

    @DexIgnore
    public final BackgroundConfig getBackgroundConfig() {
        return this.backgroundConfig;
    }

    @DexIgnore
    public final Drawable getBottomComplication() {
        return this.bottomComplication;
    }

    @DexIgnore
    public final MetaData getBottomMetaData() {
        return this.bottomMetaData;
    }

    @DexIgnore
    public final Drawable getCombination() {
        return this.combination;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final Drawable getLeftComplication() {
        return this.leftComplication;
    }

    @DexIgnore
    public final MetaData getLeftMetaData() {
        return this.leftMetaData;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final Drawable getRightComplication() {
        return this.rightComplication;
    }

    @DexIgnore
    public final MetaData getRightMetaData() {
        return this.rightMetaData;
    }

    @DexIgnore
    public final Drawable getTopComplication() {
        return this.topComplication;
    }

    @DexIgnore
    public final MetaData getTopMetaData() {
        return this.topMetaData;
    }

    @DexIgnore
    public final Ai4 getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        Drawable drawable = this.combination;
        int hashCode2 = drawable != null ? drawable.hashCode() : 0;
        Drawable drawable2 = this.background;
        int hashCode3 = drawable2 != null ? drawable2.hashCode() : 0;
        Drawable drawable3 = this.topComplication;
        int hashCode4 = drawable3 != null ? drawable3.hashCode() : 0;
        Drawable drawable4 = this.rightComplication;
        int hashCode5 = drawable4 != null ? drawable4.hashCode() : 0;
        Drawable drawable5 = this.bottomComplication;
        int hashCode6 = drawable5 != null ? drawable5.hashCode() : 0;
        Drawable drawable6 = this.leftComplication;
        int hashCode7 = drawable6 != null ? drawable6.hashCode() : 0;
        MetaData metaData = this.topMetaData;
        int hashCode8 = metaData != null ? metaData.hashCode() : 0;
        MetaData metaData2 = this.rightMetaData;
        int hashCode9 = metaData2 != null ? metaData2.hashCode() : 0;
        MetaData metaData3 = this.bottomMetaData;
        int hashCode10 = metaData3 != null ? metaData3.hashCode() : 0;
        MetaData metaData4 = this.leftMetaData;
        int hashCode11 = metaData4 != null ? metaData4.hashCode() : 0;
        String str2 = this.name;
        int hashCode12 = str2 != null ? str2.hashCode() : 0;
        BackgroundConfig backgroundConfig2 = this.backgroundConfig;
        int hashCode13 = backgroundConfig2 != null ? backgroundConfig2.hashCode() : 0;
        Ai4 ai4 = this.type;
        if (ai4 != null) {
            i = ai4.hashCode();
        }
        boolean z = this.isRemoveIconVisible;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + hashCode9) * 31) + hashCode10) * 31) + hashCode11) * 31) + hashCode12) * 31) + hashCode13) * 31) + i) * 31) + i2;
    }

    @DexIgnore
    public final boolean isRemoveIconVisible() {
        return this.isRemoveIconVisible;
    }

    @DexIgnore
    public final void setBackground(Drawable drawable) {
        this.background = drawable;
    }

    @DexIgnore
    public final void setBackgroundConfig(BackgroundConfig backgroundConfig2) {
        Wg6.c(backgroundConfig2, "<set-?>");
        this.backgroundConfig = backgroundConfig2;
    }

    @DexIgnore
    public final void setBottomComplication(Drawable drawable) {
        this.bottomComplication = drawable;
    }

    @DexIgnore
    public final void setBottomMetaData(MetaData metaData) {
        this.bottomMetaData = metaData;
    }

    @DexIgnore
    public final void setCombination(Drawable drawable) {
        this.combination = drawable;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setLeftComplication(Drawable drawable) {
        this.leftComplication = drawable;
    }

    @DexIgnore
    public final void setLeftMetaData(MetaData metaData) {
        this.leftMetaData = metaData;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setRemoveIconVisible(boolean z) {
        this.isRemoveIconVisible = z;
    }

    @DexIgnore
    public final void setRightComplication(Drawable drawable) {
        this.rightComplication = drawable;
    }

    @DexIgnore
    public final void setRightMetaData(MetaData metaData) {
        this.rightMetaData = metaData;
    }

    @DexIgnore
    public final void setTopComplication(Drawable drawable) {
        this.topComplication = drawable;
    }

    @DexIgnore
    public final void setTopMetaData(MetaData metaData) {
        this.topMetaData = metaData;
    }

    @DexIgnore
    public final void setType(Ai4 ai4) {
        Wg6.c(ai4, "<set-?>");
        this.type = ai4;
    }

    @DexIgnore
    public String toString() {
        return "WatchFaceWrapper(id=" + this.id + ", combination=" + this.combination + ", background=" + this.background + ", topComplication=" + this.topComplication + ", rightComplication=" + this.rightComplication + ", bottomComplication=" + this.bottomComplication + ", leftComplication=" + this.leftComplication + ", topMetaData=" + this.topMetaData + ", rightMetaData=" + this.rightMetaData + ", bottomMetaData=" + this.bottomMetaData + ", leftMetaData=" + this.leftMetaData + ", name=" + this.name + ", backgroundConfig=" + this.backgroundConfig + ", type=" + this.type + ", isRemoveIconVisible=" + this.isRemoveIconVisible + ")";
    }
}
