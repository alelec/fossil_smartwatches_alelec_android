package com.portfolio.platform.data.model.diana;

import com.mapped.Pj4;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSetting {
    @DexIgnore
    public String appId;
    @DexIgnore
    public String category;
    @DexIgnore
    public String id;
    @DexIgnore
    @Pj4
    @Vu3("pinType")
    public int pinType;
    @DexIgnore
    @Vu3(Constants.USER_SETTING)
    public String setting;

    @DexIgnore
    public DianaAppSetting(String str, String str2, String str3, String str4) {
        Wg6.c(str, "id");
        Wg6.c(str2, "appId");
        Wg6.c(str3, "category");
        Wg6.c(str4, MicroAppSetting.SETTING);
        this.id = str;
        this.appId = str2;
        this.category = str3;
        this.setting = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaAppSetting copy$default(DianaAppSetting dianaAppSetting, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = dianaAppSetting.id;
        }
        if ((i & 2) != 0) {
            str2 = dianaAppSetting.appId;
        }
        if ((i & 4) != 0) {
            str3 = dianaAppSetting.category;
        }
        if ((i & 8) != 0) {
            str4 = dianaAppSetting.setting;
        }
        return dianaAppSetting.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.category;
    }

    @DexIgnore
    public final String component4() {
        return this.setting;
    }

    @DexIgnore
    public final DianaAppSetting copy(String str, String str2, String str3, String str4) {
        Wg6.c(str, "id");
        Wg6.c(str2, "appId");
        Wg6.c(str3, "category");
        Wg6.c(str4, MicroAppSetting.SETTING);
        return new DianaAppSetting(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaAppSetting) {
                DianaAppSetting dianaAppSetting = (DianaAppSetting) obj;
                if (!Wg6.a(this.id, dianaAppSetting.id) || !Wg6.a(this.appId, dianaAppSetting.appId) || !Wg6.a(this.category, dianaAppSetting.category) || !Wg6.a(this.setting, dianaAppSetting.setting)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getCategory() {
        return this.category;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.appId;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.category;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.setting;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        Wg6.c(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCategory(String str) {
        Wg6.c(str, "<set-?>");
        this.category = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSetting(String str) {
        Wg6.c(str, "<set-?>");
        this.setting = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaAppSetting(id=" + this.id + ", appId=" + this.appId + ", category=" + this.category + ", setting=" + this.setting + ")";
    }
}
