package com.portfolio.platform.data.model.goaltracking;

import com.fossil.Pm7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Lc6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataKt {
    @DexIgnore
    public static final Lc6<Date, Date> calculateRangeDownload(List<GoalTrackingData> list, Date date, Date date2) {
        Wg6.c(list, "$this$calculateRangeDownload");
        Wg6.c(date, GoalPhase.COLUMN_START_DATE);
        Wg6.c(date2, GoalPhase.COLUMN_END_DATE);
        if (list.isEmpty()) {
            return new Lc6<>(date, date2);
        }
        if (!TimeUtils.m0(((GoalTrackingData) Pm7.P(list)).getDate(), date2)) {
            return new Lc6<>(((GoalTrackingData) Pm7.P(list)).getDate(), date2);
        }
        if (TimeUtils.m0(((GoalTrackingData) Pm7.F(list)).getDate(), date)) {
            return null;
        }
        return new Lc6<>(date, TimeUtils.P(((GoalTrackingData) Pm7.F(list)).getDate()));
    }
}
