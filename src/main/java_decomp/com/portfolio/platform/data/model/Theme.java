package com.portfolio.platform.data.model;

import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Theme {
    @DexIgnore
    public String id;
    @DexIgnore
    public boolean isCurrentTheme; // = true;
    @DexIgnore
    public String name;
    @DexIgnore
    public ArrayList<Style> styles;
    @DexIgnore
    public String type; // = "fixed";

    @DexIgnore
    public Theme(String str, String str2, ArrayList<Style> arrayList) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(arrayList, "styles");
        this.id = str;
        this.name = str2;
        this.styles = arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.model.Theme */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Theme copy$default(Theme theme, String str, String str2, ArrayList arrayList, int i, Object obj) {
        if ((i & 1) != 0) {
            str = theme.id;
        }
        if ((i & 2) != 0) {
            str2 = theme.name;
        }
        if ((i & 4) != 0) {
            arrayList = theme.styles;
        }
        return theme.copy(str, str2, arrayList);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<Style> component3() {
        return this.styles;
    }

    @DexIgnore
    public final Theme copy(String str, String str2, ArrayList<Style> arrayList) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(arrayList, "styles");
        return new Theme(str, str2, arrayList);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof Theme) {
                Theme theme = (Theme) obj;
                if (!Wg6.a(this.id, theme.id) || !Wg6.a(this.name, theme.name) || !Wg6.a(this.styles, theme.styles)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final ArrayList<Style> getStyles() {
        return this.styles;
    }

    @DexIgnore
    public final String getType() {
        return this.type;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        ArrayList<Style> arrayList = this.styles;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final boolean isCurrentTheme() {
        return this.isCurrentTheme;
    }

    @DexIgnore
    public final void setCurrentTheme(boolean z) {
        this.isCurrentTheme = z;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setStyles(ArrayList<Style> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.styles = arrayList;
    }

    @DexIgnore
    public final void setType(String str) {
        Wg6.c(str, "<set-?>");
        this.type = str;
    }

    @DexIgnore
    public String toString() {
        return "Theme(id=" + this.id + ", name=" + this.name + ", styles=" + this.styles + ")";
    }
}
