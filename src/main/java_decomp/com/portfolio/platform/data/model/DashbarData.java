package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashbarData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR(null);
    @DexIgnore
    @Vu3("endProgress")
    public int endProgress;
    @DexIgnore
    @Vu3("startProgress")
    public int startProgress;
    @DexIgnore
    @Vu3("viewId")
    public int viewId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<DashbarData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(Qg6 qg6) {
            this();
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DashbarData createFromParcel(Parcel parcel) {
            Wg6.c(parcel, "parcel");
            return new DashbarData(parcel);
        }

        @DexIgnore
        @Override // android.os.Parcelable.Creator
        public DashbarData[] newArray(int i) {
            return new DashbarData[i];
        }
    }

    @DexIgnore
    public DashbarData(int i, int i2, int i3) {
        this.viewId = i;
        this.startProgress = i2;
        this.endProgress = i3;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DashbarData(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt(), parcel.readInt());
        Wg6.c(parcel, "parcel");
    }

    @DexIgnore
    public static /* synthetic */ DashbarData copy$default(DashbarData dashbarData, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 1) != 0) {
            i = dashbarData.viewId;
        }
        if ((i4 & 2) != 0) {
            i2 = dashbarData.startProgress;
        }
        if ((i4 & 4) != 0) {
            i3 = dashbarData.endProgress;
        }
        return dashbarData.copy(i, i2, i3);
    }

    @DexIgnore
    public final int component1() {
        return this.viewId;
    }

    @DexIgnore
    public final int component2() {
        return this.startProgress;
    }

    @DexIgnore
    public final int component3() {
        return this.endProgress;
    }

    @DexIgnore
    public final DashbarData copy(int i, int i2, int i3) {
        return new DashbarData(i, i2, i3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DashbarData) {
                DashbarData dashbarData = (DashbarData) obj;
                if (!(this.viewId == dashbarData.viewId && this.startProgress == dashbarData.startProgress && this.endProgress == dashbarData.endProgress)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getEndProgress() {
        return this.endProgress;
    }

    @DexIgnore
    public final int getStartProgress() {
        return this.startProgress;
    }

    @DexIgnore
    public final int getViewId() {
        return this.viewId;
    }

    @DexIgnore
    public int hashCode() {
        return (((this.viewId * 31) + this.startProgress) * 31) + this.endProgress;
    }

    @DexIgnore
    public final void setEndProgress(int i) {
        this.endProgress = i;
    }

    @DexIgnore
    public final void setStartProgress(int i) {
        this.startProgress = i;
    }

    @DexIgnore
    public final void setViewId(int i) {
        this.viewId = i;
    }

    @DexIgnore
    public String toString() {
        return "DashbarData(viewId=" + this.viewId + ", startProgress=" + this.startProgress + ", endProgress=" + this.endProgress + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.viewId);
        }
        if (parcel != null) {
            parcel.writeInt(this.startProgress);
        }
        if (parcel != null) {
            parcel.writeInt(this.endProgress);
        }
    }
}
