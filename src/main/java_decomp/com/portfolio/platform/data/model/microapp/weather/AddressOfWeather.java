package com.portfolio.platform.data.model.microapp.weather;

import com.google.gson.Gson;
import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AddressOfWeather {
    @DexIgnore
    public /* final */ String address;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ double lat;
    @DexIgnore
    public /* final */ double lng;

    @DexIgnore
    public AddressOfWeather() {
        this(0.0d, 0.0d, null, 7, null);
    }

    @DexIgnore
    public AddressOfWeather(double d, double d2, String str) {
        Wg6.c(str, "address");
        this.lat = d;
        this.lng = d2;
        this.address = str;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ AddressOfWeather(double d, double d2, String str, int i, Qg6 qg6) {
        this((i & 1) != 0 ? 0.0d : d, (i & 2) == 0 ? d2 : 0.0d, (i & 4) != 0 ? "" : str);
    }

    @DexIgnore
    public static /* synthetic */ AddressOfWeather copy$default(AddressOfWeather addressOfWeather, double d, double d2, String str, int i, Object obj) {
        return addressOfWeather.copy((i & 1) != 0 ? addressOfWeather.lat : d, (i & 2) != 0 ? addressOfWeather.lng : d2, (i & 4) != 0 ? addressOfWeather.address : str);
    }

    @DexIgnore
    public final double component1() {
        return this.lat;
    }

    @DexIgnore
    public final double component2() {
        return this.lng;
    }

    @DexIgnore
    public final String component3() {
        return this.address;
    }

    @DexIgnore
    public final AddressOfWeather copy(double d, double d2, String str) {
        Wg6.c(str, "address");
        return new AddressOfWeather(d, d2, str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof AddressOfWeather) {
                AddressOfWeather addressOfWeather = (AddressOfWeather) obj;
                if (!(Double.compare(this.lat, addressOfWeather.lat) == 0 && Double.compare(this.lng, addressOfWeather.lng) == 0 && Wg6.a(this.address, addressOfWeather.address))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getAddress() {
        return this.address;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final double getLat() {
        return this.lat;
    }

    @DexIgnore
    public final double getLng() {
        return this.lng;
    }

    @DexIgnore
    public int hashCode() {
        int doubleToLongBits = Double.doubleToLongBits(this.lat);
        int doubleToLongBits2 = Double.doubleToLongBits(this.lng);
        String str = this.address;
        return (str != null ? str.hashCode() : 0) + (((doubleToLongBits * 31) + doubleToLongBits2) * 31);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        String t = new Gson().t(this);
        Wg6.b(t, "Gson().toJson(this)");
        return t;
    }
}
