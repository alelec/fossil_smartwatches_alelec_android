package com.portfolio.platform.data.model.fitnessdata;

import com.mapped.Wg6;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateWrapper {
    @DexIgnore
    public short average;
    @DexIgnore
    public short maximum;
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public List<Integer> sampleIndexInSeconds;
    @DexIgnore
    public List<Short> values;

    @DexIgnore
    public HeartRateWrapper(int i, short s, short s2, List<Short> list, List<Integer> list2) {
        Wg6.c(list, "values");
        Wg6.c(list2, "sampleIndexInSeconds");
        this.resolutionInSecond = i;
        this.average = (short) s;
        this.maximum = (short) s2;
        this.values = list;
        this.sampleIndexInSeconds = list2;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public HeartRateWrapper(com.fossil.fitness.HeartRate r7) {
        /*
            r6 = this;
            java.lang.String r0 = "heartRate"
            com.mapped.Wg6.c(r7, r0)
            int r1 = r7.getResolutionInSecond()
            short r2 = r7.getAverage()
            short r3 = r7.getMaximum()
            java.util.ArrayList r4 = r7.getValues()
            java.lang.String r0 = "heartRate.values"
            com.mapped.Wg6.b(r4, r0)
            java.util.ArrayList r5 = r7.getSampleIndexInSeconds()
            java.lang.String r0 = "heartRate.sampleIndexInSeconds"
            com.mapped.Wg6.b(r5, r0)
            r0 = r6
            r0.<init>(r1, r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.fitnessdata.HeartRateWrapper.<init>(com.fossil.fitness.HeartRate):void");
    }

    @DexIgnore
    public static /* synthetic */ HeartRateWrapper copy$default(HeartRateWrapper heartRateWrapper, int i, short s, short s2, List list, List list2, int i2, Object obj) {
        return heartRateWrapper.copy((i2 & 1) != 0 ? heartRateWrapper.resolutionInSecond : i, (i2 & 2) != 0 ? heartRateWrapper.average : s, (i2 & 4) != 0 ? heartRateWrapper.maximum : s2, (i2 & 8) != 0 ? heartRateWrapper.values : list, (i2 & 16) != 0 ? heartRateWrapper.sampleIndexInSeconds : list2);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final short component2() {
        return this.average;
    }

    @DexIgnore
    public final short component3() {
        return this.maximum;
    }

    @DexIgnore
    public final List<Short> component4() {
        return this.values;
    }

    @DexIgnore
    public final List<Integer> component5() {
        return this.sampleIndexInSeconds;
    }

    @DexIgnore
    public final HeartRateWrapper copy(int i, short s, short s2, List<Short> list, List<Integer> list2) {
        Wg6.c(list, "values");
        Wg6.c(list2, "sampleIndexInSeconds");
        return new HeartRateWrapper(i, s, s2, list, list2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HeartRateWrapper) {
                HeartRateWrapper heartRateWrapper = (HeartRateWrapper) obj;
                if (!(this.resolutionInSecond == heartRateWrapper.resolutionInSecond && this.average == heartRateWrapper.average && this.maximum == heartRateWrapper.maximum && Wg6.a(this.values, heartRateWrapper.values) && Wg6.a(this.sampleIndexInSeconds, heartRateWrapper.sampleIndexInSeconds))) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final short getAverage() {
        return this.average;
    }

    @DexIgnore
    public final short getMaximum() {
        return this.maximum;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Integer> getSampleIndexInSeconds() {
        return this.sampleIndexInSeconds;
    }

    @DexIgnore
    public final List<Short> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        int i2 = this.resolutionInSecond;
        short s = this.average;
        short s2 = this.maximum;
        List<Short> list = this.values;
        int hashCode = list != null ? list.hashCode() : 0;
        List<Integer> list2 = this.sampleIndexInSeconds;
        if (list2 != null) {
            i = list2.hashCode();
        }
        return ((hashCode + (((((i2 * 31) + s) * 31) + s2) * 31)) * 31) + i;
    }

    @DexIgnore
    public final void setAverage(short s) {
        this.average = (short) s;
    }

    @DexIgnore
    public final void setMaximum(short s) {
        this.maximum = (short) s;
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setSampleIndexInSeconds(List<Integer> list) {
        Wg6.c(list, "<set-?>");
        this.sampleIndexInSeconds = list;
    }

    @DexIgnore
    public final void setValues(List<Short> list) {
        Wg6.c(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "HeartRateWrapper(resolutionInSecond=" + this.resolutionInSecond + ", average=" + ((int) this.average) + ", maximum=" + ((int) this.maximum) + ", values=" + this.values + ", sampleIndexInSeconds=" + this.sampleIndexInSeconds + ")";
    }
}
