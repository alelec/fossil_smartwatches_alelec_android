package com.portfolio.platform.data.model.goaltracking.response;

import com.mapped.Vu3;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalDailySummary {
    @DexIgnore
    @Vu3("createdAt")
    public DateTime mCreatedAt;
    @DexIgnore
    @Vu3("date")
    public Date mDate;
    @DexIgnore
    @Vu3("goalTarget")
    public int mGoalTarget;
    @DexIgnore
    @Vu3("totalTracked")
    public int mTotalTracked;
    @DexIgnore
    @Vu3("updatedAt")
    public DateTime mUpdatedAt;

    @DexIgnore
    public GoalDailySummary(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(date, "mDate");
        Wg6.c(dateTime, "mCreatedAt");
        Wg6.c(dateTime2, "mUpdatedAt");
        this.mDate = date;
        this.mTotalTracked = i;
        this.mGoalTarget = i2;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ GoalDailySummary copy$default(GoalDailySummary goalDailySummary, Date date, int i, int i2, DateTime dateTime, DateTime dateTime2, int i3, Object obj) {
        return goalDailySummary.copy((i3 & 1) != 0 ? goalDailySummary.mDate : date, (i3 & 2) != 0 ? goalDailySummary.mTotalTracked : i, (i3 & 4) != 0 ? goalDailySummary.mGoalTarget : i2, (i3 & 8) != 0 ? goalDailySummary.mCreatedAt : dateTime, (i3 & 16) != 0 ? goalDailySummary.mUpdatedAt : dateTime2);
    }

    @DexIgnore
    public final Date component1() {
        return this.mDate;
    }

    @DexIgnore
    public final int component2() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final int component3() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final GoalDailySummary copy(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        Wg6.c(date, "mDate");
        Wg6.c(dateTime, "mCreatedAt");
        Wg6.c(dateTime2, "mUpdatedAt");
        return new GoalDailySummary(date, i, i2, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalDailySummary) {
                GoalDailySummary goalDailySummary = (GoalDailySummary) obj;
                if (!Wg6.a(this.mDate, goalDailySummary.mDate) || this.mTotalTracked != goalDailySummary.mTotalTracked || this.mGoalTarget != goalDailySummary.mGoalTarget || !Wg6.a(this.mCreatedAt, goalDailySummary.mCreatedAt) || !Wg6.a(this.mUpdatedAt, goalDailySummary.mUpdatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final int getMGoalTarget() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final int getMTotalTracked() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        Date date = this.mDate;
        int hashCode = date != null ? date.hashCode() : 0;
        int i2 = this.mTotalTracked;
        int i3 = this.mGoalTarget;
        DateTime dateTime = this.mCreatedAt;
        int hashCode2 = dateTime != null ? dateTime.hashCode() : 0;
        DateTime dateTime2 = this.mUpdatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return (((((((hashCode * 31) + i2) * 31) + i3) * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setMCreatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.mCreatedAt = dateTime;
    }

    @DexIgnore
    public final void setMDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mDate = date;
    }

    @DexIgnore
    public final void setMGoalTarget(int i) {
        this.mGoalTarget = i;
    }

    @DexIgnore
    public final void setMTotalTracked(int i) {
        this.mTotalTracked = i;
    }

    @DexIgnore
    public final void setMUpdatedAt(DateTime dateTime) {
        Wg6.c(dateTime, "<set-?>");
        this.mUpdatedAt = dateTime;
    }

    @DexIgnore
    public final GoalTrackingSummary toGoalTrackingSummary() {
        try {
            return new GoalTrackingSummary(this.mDate, this.mTotalTracked, this.mGoalTarget, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GoalDailySummary", "toGoalTrackingSummary exception=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return "GoalDailySummary(mDate=" + this.mDate + ", mTotalTracked=" + this.mTotalTracked + ", mGoalTarget=" + this.mGoalTarget + ", mCreatedAt=" + this.mCreatedAt + ", mUpdatedAt=" + this.mUpdatedAt + ")";
    }
}
