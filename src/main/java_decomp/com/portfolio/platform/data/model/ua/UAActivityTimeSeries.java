package com.portfolio.platform.data.model.ua;

import com.fossil.An7;
import com.fossil.Jj4;
import com.fossil.Vt7;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mapped.Fu3;
import com.mapped.Ku3;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UAActivityTimeSeries {
    @DexIgnore
    public Map<Long, Double> caloriesMap;
    @DexIgnore
    public Map<Long, Double> distanceMap;
    @DexIgnore
    public Ku3 extraJsonObject;
    @DexIgnore
    public String mExternalId;
    @DexIgnore
    public Map<Long, Integer> stepsMap;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Builder {
        @DexIgnore
        public Map<Long, Double> caloriesMap;
        @DexIgnore
        public Map<Long, Double> distanceMap;
        @DexIgnore
        public String mExternalId;
        @DexIgnore
        public Map<Long, Integer> stepsMap;

        @DexIgnore
        public final void addCalories(long j, double d) {
            if (this.caloriesMap == null) {
                this.caloriesMap = new HashMap();
            }
            Map<Long, Double> map = this.caloriesMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addDistance(long j, double d) {
            if (this.distanceMap == null) {
                this.distanceMap = new HashMap();
            }
            Map<Long, Double> map = this.distanceMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Double.valueOf(d));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
        }

        @DexIgnore
        public final void addSteps(long j, int i) {
            if (this.stepsMap == null) {
                this.stepsMap = new HashMap();
            }
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ((HashMap) map).put(Long.valueOf(j), Integer.valueOf(i));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
        }

        @DexIgnore
        public final UAActivityTimeSeries build() {
            if (this.mExternalId == null) {
                this.mExternalId = UUID.randomUUID().toString();
            }
            return new UAActivityTimeSeries(this);
        }

        @DexIgnore
        public final Map<Long, Double> getCaloriesMap() {
            return this.caloriesMap;
        }

        @DexIgnore
        public final Map<Long, Double> getDistanceMap() {
            return this.distanceMap;
        }

        @DexIgnore
        public final String getMExternalId() {
            return this.mExternalId;
        }

        @DexIgnore
        public final Map<Long, Integer> getStepsMap() {
            return this.stepsMap;
        }

        @DexIgnore
        public final void setCaloriesMap(Map<Long, Double> map) {
            this.caloriesMap = map;
        }

        @DexIgnore
        public final void setDistanceMap(Map<Long, Double> map) {
            this.distanceMap = map;
        }

        @DexIgnore
        public final void setExternalId(String str) {
            Wg6.c(str, Constants.PROFILE_KEY_EXTERNALID);
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setMExternalId(String str) {
            this.mExternalId = str;
        }

        @DexIgnore
        public final void setStepsMap(Map<Long, Integer> map) {
            this.stepsMap = map;
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UAActivityTimeSeries(com.portfolio.platform.data.model.ua.UAActivityTimeSeries.Builder r5) {
        /*
            r4 = this;
            java.lang.String r0 = "builder"
            com.mapped.Wg6.c(r5, r0)
            java.lang.String r0 = r5.getMExternalId()
            if (r0 == 0) goto L_0x001b
            java.util.Map r1 = r5.getStepsMap()
            java.util.Map r2 = r5.getDistanceMap()
            java.util.Map r3 = r5.getCaloriesMap()
            r4.<init>(r0, r1, r2, r3)
            return
        L_0x001b:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.model.ua.UAActivityTimeSeries.<init>(com.portfolio.platform.data.model.ua.UAActivityTimeSeries$Builder):void");
    }

    @DexIgnore
    public UAActivityTimeSeries(String str, Map<Long, Integer> map, Map<Long, Double> map2, Map<Long, Double> map3) {
        Wg6.c(str, "mExternalId");
        this.mExternalId = str;
        this.stepsMap = map;
        this.distanceMap = map2;
        this.caloriesMap = map3;
    }

    @DexIgnore
    public final Ku3 getExtraJsonObject() {
        return this.extraJsonObject;
    }

    @DexIgnore
    public final Ku3 getJsonData() {
        Ku3 ku3 = new Ku3();
        ku3.k("external_id", new Jj4(this.mExternalId));
        Ku3 ku32 = new Ku3();
        if (this.stepsMap != null) {
            Ku3 ku33 = new Ku3();
            ku33.k("interval", new Jj4((Number) 100));
            Gson gson = new Gson();
            Map<Long, Integer> map = this.stepsMap;
            if (map != null) {
                ku33.k("values", (JsonElement) gson.k(Vt7.o(Vt7.o(An7.q((HashMap) map).toString(), "(", "[", false), ")", "]", false), Fu3.class));
                ku32.k("steps", ku33);
            } else {
                throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Int>");
            }
        }
        if (this.distanceMap != null) {
            Ku3 ku34 = new Ku3();
            ku34.k("interval", new Jj4((Number) 100));
            Gson gson2 = new Gson();
            Map<Long, Double> map2 = this.distanceMap;
            if (map2 != null) {
                ku34.k("values", (JsonElement) gson2.k(Vt7.o(Vt7.o(An7.q((HashMap) map2).toString(), "(", "[", false), ")", "]", false), Fu3.class));
                ku32.k("distance", ku34);
            } else {
                throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        if (this.caloriesMap != null) {
            Ku3 ku35 = new Ku3();
            ku35.k("interval", new Jj4((Number) 100));
            Gson gson3 = new Gson();
            Map<Long, Double> map3 = this.caloriesMap;
            if (map3 != null) {
                ku35.k("values", (JsonElement) gson3.k(Vt7.o(Vt7.o(An7.q((HashMap) map3).toString(), "(", "[", false), ")", "]", false), Fu3.class));
                ku32.k("energy_expended", ku35);
            } else {
                throw new Rc6("null cannot be cast to non-null type java.util.HashMap<kotlin.Long, kotlin.Double>");
            }
        }
        ku3.k("time_series", ku32);
        if (this.extraJsonObject != null) {
            Fu3 fu3 = new Fu3();
            fu3.k(this.extraJsonObject);
            Ku3 ku36 = new Ku3();
            ku36.k("data_source", fu3);
            ku3.k("_links", ku36);
        }
        return ku3;
    }

    @DexIgnore
    public final void setExtraJsonObject(Ku3 ku3) {
        this.extraJsonObject = ku3;
    }
}
