package com.portfolio.platform.data.model.diana;

import com.mapped.Wg6;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationLastSetting {
    @DexIgnore
    public String complicationId;
    @DexIgnore
    public String setting;
    @DexIgnore
    public String updatedAt;

    @DexIgnore
    public ComplicationLastSetting(String str, String str2, String str3) {
        Wg6.c(str, "complicationId");
        Wg6.c(str2, "updatedAt");
        Wg6.c(str3, MicroAppSetting.SETTING);
        this.complicationId = str;
        this.updatedAt = str2;
        this.setting = str3;
    }

    @DexIgnore
    public static /* synthetic */ ComplicationLastSetting copy$default(ComplicationLastSetting complicationLastSetting, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = complicationLastSetting.complicationId;
        }
        if ((i & 2) != 0) {
            str2 = complicationLastSetting.updatedAt;
        }
        if ((i & 4) != 0) {
            str3 = complicationLastSetting.setting;
        }
        return complicationLastSetting.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.complicationId;
    }

    @DexIgnore
    public final String component2() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final ComplicationLastSetting copy(String str, String str2, String str3) {
        Wg6.c(str, "complicationId");
        Wg6.c(str2, "updatedAt");
        Wg6.c(str3, MicroAppSetting.SETTING);
        return new ComplicationLastSetting(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ComplicationLastSetting) {
                ComplicationLastSetting complicationLastSetting = (ComplicationLastSetting) obj;
                if (!Wg6.a(this.complicationId, complicationLastSetting.complicationId) || !Wg6.a(this.updatedAt, complicationLastSetting.updatedAt) || !Wg6.a(this.setting, complicationLastSetting.setting)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getComplicationId() {
        return this.complicationId;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.complicationId;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.updatedAt;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.setting;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return (((hashCode * 31) + hashCode2) * 31) + i;
    }

    @DexIgnore
    public final void setComplicationId(String str) {
        Wg6.c(str, "<set-?>");
        this.complicationId = str;
    }

    @DexIgnore
    public final void setSetting(String str) {
        Wg6.c(str, "<set-?>");
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "ComplicationLastSetting(complicationId=" + this.complicationId + ", updatedAt=" + this.updatedAt + ", setting=" + this.setting + ")";
    }
}
