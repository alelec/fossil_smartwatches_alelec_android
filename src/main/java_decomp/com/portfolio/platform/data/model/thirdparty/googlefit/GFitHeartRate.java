package com.portfolio.platform.data.model.thirdparty.googlefit;

import com.mapped.C;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitHeartRate {
    @DexIgnore
    public long endTime;
    @DexIgnore
    public int id;
    @DexIgnore
    public long startTime;
    @DexIgnore
    public float value;

    @DexIgnore
    public GFitHeartRate(float f, long j, long j2) {
        this.value = f;
        this.startTime = j;
        this.endTime = j2;
    }

    @DexIgnore
    public static /* synthetic */ GFitHeartRate copy$default(GFitHeartRate gFitHeartRate, float f, long j, long j2, int i, Object obj) {
        return gFitHeartRate.copy((i & 1) != 0 ? gFitHeartRate.value : f, (i & 2) != 0 ? gFitHeartRate.startTime : j, (i & 4) != 0 ? gFitHeartRate.endTime : j2);
    }

    @DexIgnore
    public final float component1() {
        return this.value;
    }

    @DexIgnore
    public final long component2() {
        return this.startTime;
    }

    @DexIgnore
    public final long component3() {
        return this.endTime;
    }

    @DexIgnore
    public final GFitHeartRate copy(float f, long j, long j2) {
        return new GFitHeartRate(f, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GFitHeartRate) {
                GFitHeartRate gFitHeartRate = (GFitHeartRate) obj;
                if (!(Float.compare(this.value, gFitHeartRate.value) == 0 && this.startTime == gFitHeartRate.startTime && this.endTime == gFitHeartRate.endTime)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final float getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        return (((Float.floatToIntBits(this.value) * 31) + C.a(this.startTime)) * 31) + C.a(this.endTime);
    }

    @DexIgnore
    public final void setEndTime(long j) {
        this.endTime = j;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setStartTime(long j) {
        this.startTime = j;
    }

    @DexIgnore
    public final void setValue(float f) {
        this.value = f;
    }

    @DexIgnore
    public String toString() {
        return "GFitHeartRate(value=" + this.value + ", startTime=" + this.startTime + ", endTime=" + this.endTime + ")";
    }
}
