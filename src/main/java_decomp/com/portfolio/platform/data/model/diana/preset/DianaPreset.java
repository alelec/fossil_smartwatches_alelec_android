package com.portfolio.platform.data.model.diana.preset;

import com.fossil.Um5;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Pj4;
import com.mapped.Qg6;
import com.mapped.TimeUtils;
import com.mapped.Tu3;
import com.mapped.Uu3;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.gson.DianaPresetComplicationSettingSerializer;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPreset {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    @Uu3(DianaPresetComplicationSettingSerializer.class)
    @Vu3("complications")
    public ArrayList<DianaPresetComplicationSetting> complications;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt; // = "";
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("isActive")
    public boolean isActive;
    @DexIgnore
    @Tu3
    @Vu3("name")
    public String name;
    @DexIgnore
    @Pj4
    public int pinType; // = 1;
    @DexIgnore
    @Vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt; // = "";
    @DexIgnore
    @Vu3("watchFaceId")
    public String watchFaceId;
    @DexIgnore
    @Uu3(DianaPresetWatchAppSettingSerializer.class)
    @Tu3
    @Vu3("buttons")
    public ArrayList<DianaPresetWatchAppSetting> watchapps;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final DianaPreset cloneFrom(DianaPreset dianaPreset) {
            Wg6.c(dianaPreset, "preset");
            String w0 = TimeUtils.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            Wg6.b(uuid, "UUID.randomUUID().toString()");
            String serialNumber = dianaPreset.getSerialNumber();
            String c = Um5.c(PortfolioApp.get.instance(), 2131886539);
            Wg6.b(c, "LanguageHelper.getString\u2026wPreset_Title__NewPreset)");
            DianaPreset dianaPreset2 = new DianaPreset(uuid, serialNumber, c, false, CustomizeConfigurationExt.c(dianaPreset.getComplications()), CustomizeConfigurationExt.d(dianaPreset.getWatchapps()), dianaPreset.getWatchFaceId());
            dianaPreset2.setCreatedAt(w0);
            dianaPreset2.setUpdatedAt(w0);
            return dianaPreset2;
        }

        @DexIgnore
        public final DianaPreset cloneFromDefaultPreset(DianaRecommendPreset dianaRecommendPreset) {
            Wg6.c(dianaRecommendPreset, "recommendPreset");
            String w0 = TimeUtils.w0(new Date(System.currentTimeMillis()));
            String uuid = UUID.randomUUID().toString();
            Wg6.b(uuid, "UUID.randomUUID().toString()");
            DianaPreset dianaPreset = new DianaPreset(uuid, dianaRecommendPreset.getSerialNumber(), dianaRecommendPreset.getName(), dianaRecommendPreset.isDefault(), CustomizeConfigurationExt.c(dianaRecommendPreset.getComplications()), CustomizeConfigurationExt.d(dianaRecommendPreset.getWatchapps()), dianaRecommendPreset.getWatchFaceId());
            dianaPreset.setCreatedAt(w0);
            dianaPreset.setUpdatedAt(w0);
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (it.hasNext()) {
                Wg6.b(w0, "timestamp");
                it.next().setLocalUpdateAt(w0);
            }
            Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
            while (it2.hasNext()) {
                Wg6.b(w0, "timestamp");
                it2.next().setLocalUpdateAt(w0);
            }
            return dianaPreset;
        }
    }

    @DexIgnore
    public DianaPreset(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        Wg6.c(str, "id");
        Wg6.c(str2, "serialNumber");
        Wg6.c(str3, "name");
        Wg6.c(arrayList, "complications");
        Wg6.c(arrayList2, "watchapps");
        Wg6.c(str4, "watchFaceId");
        this.id = str;
        this.serialNumber = str2;
        this.name = str3;
        this.isActive = z;
        this.complications = arrayList;
        this.watchapps = arrayList2;
        this.watchFaceId = str4;
    }

    @DexIgnore
    public static /* synthetic */ DianaPreset copy$default(DianaPreset dianaPreset, String str, String str2, String str3, boolean z, ArrayList arrayList, ArrayList arrayList2, String str4, int i, Object obj) {
        return dianaPreset.copy((i & 1) != 0 ? dianaPreset.id : str, (i & 2) != 0 ? dianaPreset.serialNumber : str2, (i & 4) != 0 ? dianaPreset.name : str3, (i & 8) != 0 ? dianaPreset.isActive : z, (i & 16) != 0 ? dianaPreset.complications : arrayList, (i & 32) != 0 ? dianaPreset.watchapps : arrayList2, (i & 64) != 0 ? dianaPreset.watchFaceId : str4);
    }

    @DexIgnore
    public final DianaPreset clone() {
        DianaPreset dianaPreset = new DianaPreset(this.id, this.serialNumber, this.name, this.isActive, CustomizeConfigurationExt.c(this.complications), CustomizeConfigurationExt.d(this.watchapps), this.watchFaceId);
        dianaPreset.updatedAt = this.updatedAt;
        dianaPreset.createdAt = this.createdAt;
        return dianaPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final boolean component4() {
        return this.isActive;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> component5() {
        return this.complications;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component6() {
        return this.watchapps;
    }

    @DexIgnore
    public final String component7() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final DianaPreset copy(String str, String str2, String str3, boolean z, ArrayList<DianaPresetComplicationSetting> arrayList, ArrayList<DianaPresetWatchAppSetting> arrayList2, String str4) {
        Wg6.c(str, "id");
        Wg6.c(str2, "serialNumber");
        Wg6.c(str3, "name");
        Wg6.c(arrayList, "complications");
        Wg6.c(arrayList2, "watchapps");
        Wg6.c(str4, "watchFaceId");
        return new DianaPreset(str, str2, str3, z, arrayList, arrayList2, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaPreset) {
                DianaPreset dianaPreset = (DianaPreset) obj;
                if (!Wg6.a(this.id, dianaPreset.id) || !Wg6.a(this.serialNumber, dianaPreset.serialNumber) || !Wg6.a(this.name, dianaPreset.name) || this.isActive != dianaPreset.isActive || !Wg6.a(this.complications, dianaPreset.complications) || !Wg6.a(this.watchapps, dianaPreset.watchapps) || !Wg6.a(this.watchFaceId, dianaPreset.watchFaceId)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<DianaPresetComplicationSetting> getComplications() {
        return this.complications;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getWatchapps() {
        return this.watchapps;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.serialNumber;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.name;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        boolean z = this.isActive;
        if (z) {
            z = true;
        }
        ArrayList<DianaPresetComplicationSetting> arrayList = this.complications;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        ArrayList<DianaPresetWatchAppSetting> arrayList2 = this.watchapps;
        int hashCode5 = arrayList2 != null ? arrayList2.hashCode() : 0;
        String str4 = this.watchFaceId;
        if (str4 != null) {
            i = str4.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i2) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setComplications(ArrayList<DianaPresetComplicationSetting> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.complications = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        Wg6.c(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public final void setWatchapps(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.watchapps = arrayList;
    }

    @DexIgnore
    public String toString() {
        return "DianaPreset(id=" + this.id + ", serialNumber=" + this.serialNumber + ", name=" + this.name + ", isActive=" + this.isActive + ", complications=" + this.complications + ", watchapps=" + this.watchapps + ", watchFaceId=" + this.watchFaceId + ")";
    }
}
