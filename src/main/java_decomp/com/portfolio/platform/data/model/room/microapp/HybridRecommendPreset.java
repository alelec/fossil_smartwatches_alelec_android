package com.portfolio.platform.data.model.room.microapp;

import com.mapped.CustomizeConfigurationExt;
import com.mapped.Qg6;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridRecommendPreset {
    @DexIgnore
    @Vu3("buttons")
    public ArrayList<HybridPresetAppSetting> buttons;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("isDefault")
    public boolean isDefault;
    @DexIgnore
    @Vu3("name")
    public String name;
    @DexIgnore
    @Vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt;

    @DexIgnore
    public HybridRecommendPreset(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        Wg6.c(str, "id");
        Wg6.c(str3, "serialNumber");
        Wg6.c(arrayList, "buttons");
        Wg6.c(str4, "createdAt");
        Wg6.c(str5, "updatedAt");
        this.id = str;
        this.name = str2;
        this.serialNumber = str3;
        this.buttons = arrayList;
        this.isDefault = z;
        this.createdAt = str4;
        this.updatedAt = str5;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ HybridRecommendPreset(String str, String str2, String str3, ArrayList arrayList, boolean z, String str4, String str5, int i, Qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2, str3, arrayList, z, str4, str5);
    }

    @DexIgnore
    public static /* synthetic */ HybridRecommendPreset copy$default(HybridRecommendPreset hybridRecommendPreset, String str, String str2, String str3, ArrayList arrayList, boolean z, String str4, String str5, int i, Object obj) {
        return hybridRecommendPreset.copy((i & 1) != 0 ? hybridRecommendPreset.id : str, (i & 2) != 0 ? hybridRecommendPreset.name : str2, (i & 4) != 0 ? hybridRecommendPreset.serialNumber : str3, (i & 8) != 0 ? hybridRecommendPreset.buttons : arrayList, (i & 16) != 0 ? hybridRecommendPreset.isDefault : z, (i & 32) != 0 ? hybridRecommendPreset.createdAt : str4, (i & 64) != 0 ? hybridRecommendPreset.updatedAt : str5);
    }

    @DexIgnore
    public final HybridPreset clone() {
        HybridPreset hybridPreset = new HybridPreset(this.id, this.name, this.serialNumber, CustomizeConfigurationExt.b(this.buttons), this.isDefault);
        hybridPreset.setCreatedAt(this.createdAt);
        hybridPreset.setUpdatedAt(this.updatedAt);
        return hybridPreset;
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.name;
    }

    @DexIgnore
    public final String component3() {
        return this.serialNumber;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> component4() {
        return this.buttons;
    }

    @DexIgnore
    public final boolean component5() {
        return this.isDefault;
    }

    @DexIgnore
    public final String component6() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component7() {
        return this.updatedAt;
    }

    @DexIgnore
    public final HybridRecommendPreset copy(String str, String str2, String str3, ArrayList<HybridPresetAppSetting> arrayList, boolean z, String str4, String str5) {
        Wg6.c(str, "id");
        Wg6.c(str3, "serialNumber");
        Wg6.c(arrayList, "buttons");
        Wg6.c(str4, "createdAt");
        Wg6.c(str5, "updatedAt");
        return new HybridRecommendPreset(str, str2, str3, arrayList, z, str4, str5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof HybridRecommendPreset) {
                HybridRecommendPreset hybridRecommendPreset = (HybridRecommendPreset) obj;
                if (!Wg6.a(this.id, hybridRecommendPreset.id) || !Wg6.a(this.name, hybridRecommendPreset.name) || !Wg6.a(this.serialNumber, hybridRecommendPreset.serialNumber) || !Wg6.a(this.buttons, hybridRecommendPreset.buttons) || this.isDefault != hybridRecommendPreset.isDefault || !Wg6.a(this.createdAt, hybridRecommendPreset.createdAt) || !Wg6.a(this.updatedAt, hybridRecommendPreset.updatedAt)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<HybridPresetAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.serialNumber;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        ArrayList<HybridPresetAppSetting> arrayList = this.buttons;
        int hashCode4 = arrayList != null ? arrayList.hashCode() : 0;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        String str4 = this.createdAt;
        int hashCode5 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i2) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setButtons(ArrayList<HybridPresetAppSetting> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "HybridRecommendPreset(id=" + this.id + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", buttons=" + this.buttons + ", isDefault=" + this.isDefault + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
