package com.portfolio.platform.data.model.watchface;

import com.mapped.Tu3;
import com.mapped.Uu3;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.gson.DianaPresetWatchAppSettingSerializer;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRecommendPreset {
    @DexIgnore
    @Uu3(DianaPresetWatchAppSettingSerializer.class)
    @Tu3
    @Vu3("buttons")
    public ArrayList<DianaPresetWatchAppSetting> buttons;
    @DexIgnore
    @Vu3("createdAt")
    public String createdAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    @Vu3("isDefault")
    public boolean isDefault;
    @DexIgnore
    @Vu3("name")
    public String name;
    @DexIgnore
    @Vu3("serialNumber")
    public String serialNumber;
    @DexIgnore
    @Vu3("uid")
    public String uid;
    @DexIgnore
    @Vu3("updatedAt")
    public String updatedAt; // = "2016-01-01T01:01:01.001Z";
    @DexIgnore
    @Vu3("watchFaceId")
    public String watchFaceId;

    @DexIgnore
    public DianaWatchFaceRecommendPreset(String str, boolean z, String str2, String str3, String str4, String str5, ArrayList<DianaPresetWatchAppSetting> arrayList) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(str3, "serialNumber");
        Wg6.c(str4, "uid");
        Wg6.c(str5, "watchFaceId");
        Wg6.c(arrayList, "buttons");
        this.id = str;
        this.isDefault = z;
        this.name = str2;
        this.serialNumber = str3;
        this.uid = str4;
        this.watchFaceId = str5;
        this.buttons = arrayList;
    }

    @DexIgnore
    public static /* synthetic */ DianaWatchFaceRecommendPreset copy$default(DianaWatchFaceRecommendPreset dianaWatchFaceRecommendPreset, String str, boolean z, String str2, String str3, String str4, String str5, ArrayList arrayList, int i, Object obj) {
        return dianaWatchFaceRecommendPreset.copy((i & 1) != 0 ? dianaWatchFaceRecommendPreset.id : str, (i & 2) != 0 ? dianaWatchFaceRecommendPreset.isDefault : z, (i & 4) != 0 ? dianaWatchFaceRecommendPreset.name : str2, (i & 8) != 0 ? dianaWatchFaceRecommendPreset.serialNumber : str3, (i & 16) != 0 ? dianaWatchFaceRecommendPreset.uid : str4, (i & 32) != 0 ? dianaWatchFaceRecommendPreset.watchFaceId : str5, (i & 64) != 0 ? dianaWatchFaceRecommendPreset.buttons : arrayList);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final boolean component2() {
        return this.isDefault;
    }

    @DexIgnore
    public final String component3() {
        return this.name;
    }

    @DexIgnore
    public final String component4() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String component5() {
        return this.uid;
    }

    @DexIgnore
    public final String component6() {
        return this.watchFaceId;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> component7() {
        return this.buttons;
    }

    @DexIgnore
    public final DianaWatchFaceRecommendPreset copy(String str, boolean z, String str2, String str3, String str4, String str5, ArrayList<DianaPresetWatchAppSetting> arrayList) {
        Wg6.c(str, "id");
        Wg6.c(str2, "name");
        Wg6.c(str3, "serialNumber");
        Wg6.c(str4, "uid");
        Wg6.c(str5, "watchFaceId");
        Wg6.c(arrayList, "buttons");
        return new DianaWatchFaceRecommendPreset(str, z, str2, str3, str4, str5, arrayList);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof DianaWatchFaceRecommendPreset) {
                DianaWatchFaceRecommendPreset dianaWatchFaceRecommendPreset = (DianaWatchFaceRecommendPreset) obj;
                if (!Wg6.a(this.id, dianaWatchFaceRecommendPreset.id) || this.isDefault != dianaWatchFaceRecommendPreset.isDefault || !Wg6.a(this.name, dianaWatchFaceRecommendPreset.name) || !Wg6.a(this.serialNumber, dianaWatchFaceRecommendPreset.serialNumber) || !Wg6.a(this.uid, dianaWatchFaceRecommendPreset.uid) || !Wg6.a(this.watchFaceId, dianaWatchFaceRecommendPreset.watchFaceId) || !Wg6.a(this.buttons, dianaWatchFaceRecommendPreset.buttons)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final ArrayList<DianaPresetWatchAppSetting> getButtons() {
        return this.buttons;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getName() {
        return this.name;
    }

    @DexIgnore
    public final String getSerialNumber() {
        return this.serialNumber;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getWatchFaceId() {
        return this.watchFaceId;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        boolean z = this.isDefault;
        if (z) {
            z = true;
        }
        String str2 = this.name;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.serialNumber;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.uid;
        int hashCode4 = str4 != null ? str4.hashCode() : 0;
        String str5 = this.watchFaceId;
        int hashCode5 = str5 != null ? str5.hashCode() : 0;
        ArrayList<DianaPresetWatchAppSetting> arrayList = this.buttons;
        if (arrayList != null) {
            i = arrayList.hashCode();
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return (((((((((((hashCode * 31) + i2) * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + hashCode5) * 31) + i;
    }

    @DexIgnore
    public final boolean isDefault() {
        return this.isDefault;
    }

    @DexIgnore
    public final void setButtons(ArrayList<DianaPresetWatchAppSetting> arrayList) {
        Wg6.c(arrayList, "<set-?>");
        this.buttons = arrayList;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDefault(boolean z) {
        this.isDefault = z;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setName(String str) {
        Wg6.c(str, "<set-?>");
        this.name = str;
    }

    @DexIgnore
    public final void setSerialNumber(String str) {
        Wg6.c(str, "<set-?>");
        this.serialNumber = str;
    }

    @DexIgnore
    public final void setUid(String str) {
        Wg6.c(str, "<set-?>");
        this.uid = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        Wg6.c(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setWatchFaceId(String str) {
        Wg6.c(str, "<set-?>");
        this.watchFaceId = str;
    }

    @DexIgnore
    public String toString() {
        return "DianaWatchFaceRecommendPreset(id=" + this.id + ", isDefault=" + this.isDefault + ", name=" + this.name + ", serialNumber=" + this.serialNumber + ", uid=" + this.uid + ", watchFaceId=" + this.watchFaceId + ", buttons=" + this.buttons + ")";
    }
}
