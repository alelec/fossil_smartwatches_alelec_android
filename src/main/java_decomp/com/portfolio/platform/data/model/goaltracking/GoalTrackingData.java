package com.portfolio.platform.data.model.goaltracking;

import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.mapped.C;
import com.mapped.Vu3;
import com.mapped.Wg6;
import java.io.Serializable;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingData implements Serializable {
    @DexIgnore
    @Vu3("createdAt")
    public long createdAt;
    @DexIgnore
    @Vu3("date")
    public Date date;
    @DexIgnore
    @Vu3("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @Vu3("timezoneOffset")
    public /* final */ int timezoneOffsetInSecond;
    @DexIgnore
    @Vu3(GoalTrackingEvent.COLUMN_TRACKED_AT)
    public /* final */ DateTime trackedAt;
    @DexIgnore
    @Vu3("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingData(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        Wg6.c(str, "id");
        Wg6.c(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        Wg6.c(date2, "date");
        this.id = str;
        this.trackedAt = dateTime;
        this.timezoneOffsetInSecond = i;
        this.date = date2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingData copy$default(GoalTrackingData goalTrackingData, String str, DateTime dateTime, int i, Date date2, long j, long j2, int i2, Object obj) {
        return goalTrackingData.copy((i2 & 1) != 0 ? goalTrackingData.id : str, (i2 & 2) != 0 ? goalTrackingData.trackedAt : dateTime, (i2 & 4) != 0 ? goalTrackingData.timezoneOffsetInSecond : i, (i2 & 8) != 0 ? goalTrackingData.date : date2, (i2 & 16) != 0 ? goalTrackingData.createdAt : j, (i2 & 32) != 0 ? goalTrackingData.updatedAt : j2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.trackedAt;
    }

    @DexIgnore
    public final int component3() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final Date component4() {
        return this.date;
    }

    @DexIgnore
    public final long component5() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component6() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingData copy(String str, DateTime dateTime, int i, Date date2, long j, long j2) {
        Wg6.c(str, "id");
        Wg6.c(dateTime, GoalTrackingEvent.COLUMN_TRACKED_AT);
        Wg6.c(date2, "date");
        return new GoalTrackingData(str, dateTime, i, date2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof GoalTrackingData) {
                GoalTrackingData goalTrackingData = (GoalTrackingData) obj;
                if (!Wg6.a(this.id, goalTrackingData.id) || !Wg6.a(this.trackedAt, goalTrackingData.trackedAt) || this.timezoneOffsetInSecond != goalTrackingData.timezoneOffsetInSecond || !Wg6.a(this.date, goalTrackingData.date) || this.createdAt != goalTrackingData.createdAt || this.updatedAt != goalTrackingData.updatedAt) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final DateTime getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.id;
        int hashCode = str != null ? str.hashCode() : 0;
        DateTime dateTime = this.trackedAt;
        int hashCode2 = dateTime != null ? dateTime.hashCode() : 0;
        int i2 = this.timezoneOffsetInSecond;
        Date date2 = this.date;
        if (date2 != null) {
            i = date2.hashCode();
        }
        return (((((((((hashCode * 31) + hashCode2) * 31) + i2) * 31) + i) * 31) + C.a(this.createdAt)) * 31) + C.a(this.updatedAt);
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        Wg6.c(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setId(String str) {
        Wg6.c(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingData(id=" + this.id + ", trackedAt=" + this.trackedAt + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", date=" + this.date + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
