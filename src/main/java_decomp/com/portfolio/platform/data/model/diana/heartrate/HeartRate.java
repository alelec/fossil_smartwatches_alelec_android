package com.portfolio.platform.data.model.diana.heartrate;

import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.mapped.TimeUtils;
import com.mapped.Vu3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.ServerError;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRate extends ServerError {
    @DexIgnore
    @Vu3(GoalTrackingSummary.COLUMN_AVERAGE)
    public /* final */ float mAverage;
    @DexIgnore
    @Vu3("createdAt")
    public /* final */ Date mCreatedAt;
    @DexIgnore
    @Vu3("date")
    public /* final */ Date mDate;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_END_TIME)
    public /* final */ String mEndTime;
    @DexIgnore
    @Vu3("id")
    public /* final */ String mId;
    @DexIgnore
    @Vu3("max")
    public /* final */ int mMax;
    @DexIgnore
    @Vu3("min")
    public /* final */ int mMin;
    @DexIgnore
    @Vu3("minuteCount")
    public /* final */ int mMinuteCount;
    @DexIgnore
    @Vu3("resting")
    public /* final */ Resting mResting;
    @DexIgnore
    @Vu3(SampleRaw.COLUMN_START_TIME)
    public /* final */ String mStartTime;
    @DexIgnore
    @Vu3("timezoneOffset")
    public /* final */ int mTimeZoneOffsetInSecond;
    @DexIgnore
    @Vu3("updatedAt")
    public /* final */ Date mUpdatedAt;

    @DexIgnore
    public HeartRate(String str, float f, Date date, Date date2, Date date3, String str2, String str3, int i, int i2, int i3, int i4, Resting resting) {
        Wg6.c(str, "mId");
        Wg6.c(date, "mDate");
        Wg6.c(date2, "mCreatedAt");
        Wg6.c(date3, "mUpdatedAt");
        Wg6.c(str2, "mEndTime");
        Wg6.c(str3, "mStartTime");
        this.mId = str;
        this.mAverage = f;
        this.mDate = date;
        this.mCreatedAt = date2;
        this.mUpdatedAt = date3;
        this.mEndTime = str2;
        this.mStartTime = str3;
        this.mTimeZoneOffsetInSecond = i;
        this.mMin = i2;
        this.mMax = i3;
        this.mMinuteCount = i4;
        this.mResting = resting;
    }

    @DexIgnore
    public final HeartRateSample toHeartRateSample() {
        Date date;
        Date date2;
        Date date3;
        Date date4;
        Date time;
        try {
            Calendar instance = Calendar.getInstance();
            Date a2 = TimeUtils.a(this.mTimeZoneOffsetInSecond, this.mStartTime);
            try {
                Wg6.b(instance, "calendar");
                instance.setTime(a2);
                instance.set(13, 0);
                instance.set(14, 0);
                time = instance.getTime();
                try {
                    instance.setTime(TimeUtils.a(this.mTimeZoneOffsetInSecond, this.mEndTime));
                    instance.set(13, 0);
                    instance.set(14, 0);
                    date3 = instance.getTime();
                    date4 = time;
                } catch (ParseException e) {
                    e = e;
                    date2 = time;
                    e.printStackTrace();
                    date3 = date;
                    date4 = date2;
                    String str = this.mId;
                    float f = this.mAverage;
                    Date date5 = this.mDate;
                    long time2 = this.mCreatedAt.getTime();
                    long time3 = this.mUpdatedAt.getTime();
                    DateTime b = TimeUtils.b(date3, this.mTimeZoneOffsetInSecond);
                    Wg6.b(b, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                    DateTime b2 = TimeUtils.b(date4, this.mTimeZoneOffsetInSecond);
                    Wg6.b(b2, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                    return new HeartRateSample(str, f, date5, time2, time3, b, b2, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
                }
            } catch (ParseException e2) {
                e = e2;
                date = null;
                date2 = time;
                e.printStackTrace();
                date3 = date;
                date4 = date2;
                String str2 = this.mId;
                float f2 = this.mAverage;
                Date date52 = this.mDate;
                long time22 = this.mCreatedAt.getTime();
                long time32 = this.mUpdatedAt.getTime();
                DateTime b3 = TimeUtils.b(date3, this.mTimeZoneOffsetInSecond);
                Wg6.b(b3, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                DateTime b22 = TimeUtils.b(date4, this.mTimeZoneOffsetInSecond);
                Wg6.b(b22, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
                return new HeartRateSample(str2, f2, date52, time22, time32, b3, b22, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
            }
        } catch (ParseException e3) {
            e = e3;
            date = null;
            date2 = null;
            e.printStackTrace();
            date3 = date;
            date4 = date2;
            String str22 = this.mId;
            float f22 = this.mAverage;
            Date date522 = this.mDate;
            long time222 = this.mCreatedAt.getTime();
            long time322 = this.mUpdatedAt.getTime();
            DateTime b32 = TimeUtils.b(date3, this.mTimeZoneOffsetInSecond);
            Wg6.b(b32, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime b222 = TimeUtils.b(date4, this.mTimeZoneOffsetInSecond);
            Wg6.b(b222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str22, f22, date522, time222, time322, b32, b222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        }
        try {
            String str222 = this.mId;
            float f222 = this.mAverage;
            Date date5222 = this.mDate;
            long time2222 = this.mCreatedAt.getTime();
            long time3222 = this.mUpdatedAt.getTime();
            DateTime b322 = TimeUtils.b(date3, this.mTimeZoneOffsetInSecond);
            Wg6.b(b322, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            DateTime b2222 = TimeUtils.b(date4, this.mTimeZoneOffsetInSecond);
            Wg6.b(b2222, "DateHelper.createDateTim\u2026 mTimeZoneOffsetInSecond)");
            return new HeartRateSample(str222, f222, date5222, time2222, time3222, b322, b2222, this.mTimeZoneOffsetInSecond, this.mMin, this.mMax, this.mMinuteCount, this.mResting);
        } catch (Exception e4) {
            e4.printStackTrace();
            return null;
        }
    }
}
