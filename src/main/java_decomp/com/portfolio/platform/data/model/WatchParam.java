package com.portfolio.platform.data.model;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchParam {
    @DexIgnore
    public /* final */ String data;
    @DexIgnore
    public /* final */ String prefixSerial;
    @DexIgnore
    public /* final */ String versionMajor;
    @DexIgnore
    public /* final */ String versionMinor;

    @DexIgnore
    public WatchParam(String str, String str2, String str3, String str4) {
        Wg6.c(str, "prefixSerial");
        this.prefixSerial = str;
        this.versionMajor = str2;
        this.versionMinor = str3;
        this.data = str4;
    }

    @DexIgnore
    public static /* synthetic */ WatchParam copy$default(WatchParam watchParam, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = watchParam.prefixSerial;
        }
        if ((i & 2) != 0) {
            str2 = watchParam.versionMajor;
        }
        if ((i & 4) != 0) {
            str3 = watchParam.versionMinor;
        }
        if ((i & 8) != 0) {
            str4 = watchParam.data;
        }
        return watchParam.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.prefixSerial;
    }

    @DexIgnore
    public final String component2() {
        return this.versionMajor;
    }

    @DexIgnore
    public final String component3() {
        return this.versionMinor;
    }

    @DexIgnore
    public final String component4() {
        return this.data;
    }

    @DexIgnore
    public final WatchParam copy(String str, String str2, String str3, String str4) {
        Wg6.c(str, "prefixSerial");
        return new WatchParam(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof WatchParam) {
                WatchParam watchParam = (WatchParam) obj;
                if (!Wg6.a(this.prefixSerial, watchParam.prefixSerial) || !Wg6.a(this.versionMajor, watchParam.versionMajor) || !Wg6.a(this.versionMinor, watchParam.versionMinor) || !Wg6.a(this.data, watchParam.data)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final String getData() {
        return this.data;
    }

    @DexIgnore
    public final String getPrefixSerial() {
        return this.prefixSerial;
    }

    @DexIgnore
    public final String getVersionMajor() {
        return this.versionMajor;
    }

    @DexIgnore
    public final String getVersionMinor() {
        return this.versionMinor;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        String str = this.prefixSerial;
        int hashCode = str != null ? str.hashCode() : 0;
        String str2 = this.versionMajor;
        int hashCode2 = str2 != null ? str2.hashCode() : 0;
        String str3 = this.versionMinor;
        int hashCode3 = str3 != null ? str3.hashCode() : 0;
        String str4 = this.data;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return (((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + i;
    }

    @DexIgnore
    public String toString() {
        return "WatchParam(prefixSerial=" + this.prefixSerial + ", versionMajor=" + this.versionMajor + ", versionMinor=" + this.versionMinor + ", data=" + this.data + ")";
    }
}
