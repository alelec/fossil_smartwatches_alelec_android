package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$2", f = "HeartRateSampleRepository.kt", l = {39}, m = "invokeSuspend")
public final class HeartRateSampleRepository$getHeartRateSamples$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<HeartRateSample>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSampleRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSampleRepository$getHeartRateSamples$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<HeartRateSample>, ApiResponse<HeartRate>> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Hh6 hh6, int i, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$offset = hh6;
                this.$limit = i;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<ApiResponse<HeartRate>>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getHeartRateSamples(k, k2, this.$offset.element, this.$limit, xe6);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0053  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.diana.heartrate.HeartRateSample>>> r8) {
                /*
                    r7 = this;
                    r6 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    if (r0 == 0) goto L_0x0045
                    r0 = r8
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x0045
                    int r1 = r1 + r3
                    r0.label = r1
                L_0x0013:
                    java.lang.Object r1 = r0.result
                    java.lang.Object r2 = com.fossil.Yn7.d()
                    int r3 = r0.label
                    if (r3 == 0) goto L_0x0053
                    if (r3 != r6) goto L_0x004b
                    java.lang.Object r0 = r0.L$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.El7.b(r1)
                    r7 = r0
                L_0x0027:
                    r0 = r1
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
                    com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao r0 = r0.getHeartRateDao()
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r1 = r7.this$0
                    java.util.Date r1 = r1.$startDate
                    java.lang.String r2 = "startDate"
                    com.mapped.Wg6.b(r1, r2)
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r2 = r7.this$0
                    java.util.Date r2 = r2.$endDate
                    java.lang.String r3 = "endDate"
                    com.mapped.Wg6.b(r2, r3)
                    androidx.lifecycle.LiveData r0 = r0.getHeartRateSamples(r1, r2)
                L_0x0044:
                    return r0
                L_0x0045:
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$loadFromDb$Anon1_Level4
                    r0.<init>(r7, r8)
                    goto L_0x0013
                L_0x004b:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0053:
                    com.fossil.El7.b(r1)
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    java.lang.String r3 = com.portfolio.platform.data.source.HeartRateSampleRepository.access$getTAG$cp()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getHeartRateSamples loadFromDb isNotToday day = "
                    r4.append(r5)
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2 r5 = r7.this$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$end
                    r4.append(r5)
                    java.lang.String r4 = r4.toString()
                    r1.d(r3, r4)
                    com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
                    r0.L$0 = r7
                    r0.label = r6
                    java.lang.Object r1 = r1.y(r0)
                    if (r1 != r2) goto L_0x0027
                    r0 = r2
                    goto L_0x0044
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.TAG, "getHeartRateSamples onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<HeartRate> apiResponse, Xe6<? super Boolean> xe6) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = Ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return Ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(HeartRateSampleRepository.TAG, "getHeartRateSamples processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return Ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<HeartRate> apiResponse, Xe6 xe6) {
                return processContinueFetching(apiResponse, (Xe6<? super Boolean>) xe6);
            }

            @DexIgnore
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX WARN: Type inference failed for: r0v27, types: [java.util.List] */
            /* JADX WARNING: Removed duplicated region for block: B:13:0x004d  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* JADX WARNING: Unknown variable types count: 1 */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.diana.heartrate.HeartRate> r8, com.mapped.Xe6<? super com.mapped.Cd6> r9) {
                /*
                    r7 = this;
                    r6 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r9 instanceof com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x003e
                    r0 = r9
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x003e
                    int r1 = r1 + r3
                    r0.label = r1
                    r2 = r0
                L_0x0014:
                    java.lang.Object r3 = r2.result
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r2.label
                    if (r0 == 0) goto L_0x004d
                    if (r0 != r6) goto L_0x0045
                    java.lang.Object r0 = r2.L$2
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r1 = r2.L$1
                    com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
                    java.lang.Object r1 = r2.L$0
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3 r1 = (com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3) r1
                    com.fossil.El7.b(r3)
                    r1 = r3
                    r2 = r0
                L_0x0031:
                    r0 = r1
                    com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
                    com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao r0 = r0.getHeartRateDao()
                    r0.upsertHeartRateSampleList(r2)
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x003d:
                    return r0
                L_0x003e:
                    com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.HeartRateSampleRepository$getHeartRateSamples$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r7, r9)
                    r2 = r0
                    goto L_0x0014
                L_0x0045:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x004d:
                    com.fossil.El7.b(r3)
                    com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r0.getLocal()
                    java.lang.String r3 = com.portfolio.platform.data.source.HeartRateSampleRepository.access$getTAG$cp()
                    java.lang.StringBuilder r5 = new java.lang.StringBuilder
                    r5.<init>()
                    java.lang.String r0 = "getHeartRateSamples saveCallResult onResponse: response = "
                    r5.append(r0)
                    java.util.List r0 = r8.get_items()
                    int r0 = r0.size()
                    r5.append(r0)
                    java.lang.String r0 = " hasNext="
                    r5.append(r0)
                    com.portfolio.platform.data.model.Range r0 = r8.get_range()
                    if (r0 == 0) goto L_0x00af
                    boolean r0 = r0.isHasNext()
                    java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
                L_0x0082:
                    r5.append(r0)
                    java.lang.String r0 = r5.toString()
                    r1.d(r3, r0)
                    java.util.ArrayList r3 = new java.util.ArrayList
                    r3.<init>()
                    java.util.List r0 = r8.get_items()
                    java.util.Iterator r1 = r0.iterator()
                L_0x0099:
                    boolean r0 = r1.hasNext()
                    if (r0 == 0) goto L_0x00b1
                    java.lang.Object r0 = r1.next()
                    com.portfolio.platform.data.model.diana.heartrate.HeartRate r0 = (com.portfolio.platform.data.model.diana.heartrate.HeartRate) r0
                    com.portfolio.platform.data.model.diana.heartrate.HeartRateSample r0 = r0.toHeartRateSample()
                    if (r0 == 0) goto L_0x0099
                    r3.add(r0)
                    goto L_0x0099
                L_0x00af:
                    r0 = 0
                    goto L_0x0082
                L_0x00b1:
                    com.portfolio.platform.manager.EncryptedDatabaseManager r0 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
                    r2.L$0 = r7
                    r2.L$1 = r8
                    r2.L$2 = r3
                    r2.label = r6
                    java.lang.Object r1 = r0.y(r2)
                    if (r1 != r4) goto L_0x00c4
                    r0 = r4
                    goto L_0x003d
                L_0x00c4:
                    r2 = r3
                    goto L_0x0031
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.HeartRateSampleRepository.getHeartRateSamples.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.source.remote.ApiResponse, com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<HeartRate> apiResponse, Xe6 xe6) {
                return saveCallResult(apiResponse, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<HeartRateSample> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<HeartRateSample> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(HeartRateSampleRepository$getHeartRateSamples$Anon2 heartRateSampleRepository$getHeartRateSamples$Anon2, Date date, Date date2) {
            this.this$0 = heartRateSampleRepository$getHeartRateSamples$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
        }

        @DexIgnore
        public final LiveData<H47<List<HeartRateSample>>> apply(List<FitnessDataWrapper> list) {
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            Wg6.b(list, "fitnessDataList");
            Date date = this.$startDate;
            Wg6.b(date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            Wg6.b(date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, hh6, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$getHeartRateSamples$Anon2(HeartRateSampleRepository heartRateSampleRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = heartRateSampleRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        HeartRateSampleRepository$getHeartRateSamples$Anon2 heartRateSampleRepository$getHeartRateSamples$Anon2 = new HeartRateSampleRepository$getHeartRateSamples$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, xe6);
        heartRateSampleRepository$getHeartRateSamples$Anon2.p$ = (Il6) obj;
        throw null;
        //return heartRateSampleRepository$getHeartRateSamples$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<HeartRateSample>>>> xe6) {
        throw null;
        //return ((HeartRateSampleRepository$getHeartRateSamples$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Date V;
        Object g;
        Date date;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            V = TimeUtils.V(this.$start);
            Date E = TimeUtils.E(this.$end);
            Dv7 b = Bw7.b();
            HeartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2 heartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2 = new HeartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.L$1 = V;
            this.L$2 = E;
            this.label = 1;
            g = Eu7.g(b, heartRateSampleRepository$getHeartRateSamples$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
            date = E;
        } else if (i == 1) {
            V = (Date) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            date = (Date) this.L$2;
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDataDao fitnessDataDao = ((FitnessDatabase) g).getFitnessDataDao();
        Wg6.b(V, GoalPhase.COLUMN_START_DATE);
        Wg6.b(date, GoalPhase.COLUMN_END_DATE);
        return Ss0.c(fitnessDataDao.getFitnessDataLiveData(V, date), new Anon1_Level2(this, V, date));
    }
}
