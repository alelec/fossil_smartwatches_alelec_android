package com.portfolio.platform.data.source.remote;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ResultItems {
    @DexIgnore
    public int nCreated;
    @DexIgnore
    public int nDeleted;
    @DexIgnore
    public int nFailed;
    @DexIgnore
    public int nUpdated;

    @DexIgnore
    public ResultItems(int i, int i2, int i3, int i4) {
        this.nCreated = i;
        this.nUpdated = i2;
        this.nDeleted = i3;
        this.nFailed = i4;
    }

    @DexIgnore
    public static /* synthetic */ ResultItems copy$default(ResultItems resultItems, int i, int i2, int i3, int i4, int i5, Object obj) {
        if ((i5 & 1) != 0) {
            i = resultItems.nCreated;
        }
        if ((i5 & 2) != 0) {
            i2 = resultItems.nUpdated;
        }
        if ((i5 & 4) != 0) {
            i3 = resultItems.nDeleted;
        }
        if ((i5 & 8) != 0) {
            i4 = resultItems.nFailed;
        }
        return resultItems.copy(i, i2, i3, i4);
    }

    @DexIgnore
    public final int component1() {
        return this.nCreated;
    }

    @DexIgnore
    public final int component2() {
        return this.nUpdated;
    }

    @DexIgnore
    public final int component3() {
        return this.nDeleted;
    }

    @DexIgnore
    public final int component4() {
        return this.nFailed;
    }

    @DexIgnore
    public final ResultItems copy(int i, int i2, int i3, int i4) {
        return new ResultItems(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof ResultItems) {
                ResultItems resultItems = (ResultItems) obj;
                if (!(this.nCreated == resultItems.nCreated && this.nUpdated == resultItems.nUpdated && this.nDeleted == resultItems.nDeleted && this.nFailed == resultItems.nFailed)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final int getNCreated() {
        return this.nCreated;
    }

    @DexIgnore
    public final int getNDeleted() {
        return this.nDeleted;
    }

    @DexIgnore
    public final int getNFailed() {
        return this.nFailed;
    }

    @DexIgnore
    public final int getNUpdated() {
        return this.nUpdated;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.nCreated * 31) + this.nUpdated) * 31) + this.nDeleted) * 31) + this.nFailed;
    }

    @DexIgnore
    public final void setNCreated(int i) {
        this.nCreated = i;
    }

    @DexIgnore
    public final void setNDeleted(int i) {
        this.nDeleted = i;
    }

    @DexIgnore
    public final void setNFailed(int i) {
        this.nFailed = i;
    }

    @DexIgnore
    public final void setNUpdated(int i) {
        this.nUpdated = i;
    }

    @DexIgnore
    public String toString() {
        return "ResultItems(nCreated=" + this.nCreated + ", nUpdated=" + this.nUpdated + ", nDeleted=" + this.nDeleted + ", nFailed=" + this.nFailed + ")";
    }
}
