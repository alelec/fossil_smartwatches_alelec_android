package com.portfolio.platform.data.source;

import com.fossil.Lx0;
import com.mapped.Wg6;
import com.mapped.Xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1 extends Xh {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_3_TO_4$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        lx0.beginTransaction();
        lx0.execSQL("ALTER TABLE device ADD COLUMN activationDate TEXT");
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
