package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitHeartRateDao_Impl implements GFitHeartRateDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<GFitHeartRate> __deletionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ Hh<GFitHeartRate> __insertionAdapterOfGFitHeartRate;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GFitHeartRate> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitHeartRate gFitHeartRate) {
            mi.bindLong(1, (long) gFitHeartRate.getId());
            mi.bindDouble(2, (double) gFitHeartRate.getValue());
            mi.bindLong(3, gFitHeartRate.getStartTime());
            mi.bindLong(4, gFitHeartRate.getEndTime());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitHeartRate gFitHeartRate) {
            bind(mi, gFitHeartRate);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitHeartRate` (`id`,`value`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<GFitHeartRate> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitHeartRate gFitHeartRate) {
            mi.bindLong(1, (long) gFitHeartRate.getId());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitHeartRate gFitHeartRate) {
            bind(mi, gFitHeartRate);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `gFitHeartRate` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM gFitHeartRate";
        }
    }

    @DexIgnore
    public GFitHeartRateDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGFitHeartRate = new Anon1(oh);
        this.__deletionAdapterOfGFitHeartRate = new Anon2(oh);
        this.__preparedStmtOfClearAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void deleteListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitHeartRate.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public List<GFitHeartRate> getAllGFitHeartRate() {
        Rh f = Rh.f("SELECT * FROM gFitHeartRate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "value");
            int c3 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c4 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitHeartRate gFitHeartRate = new GFitHeartRate(b.getFloat(c2), b.getLong(c3), b.getLong(c4));
                gFitHeartRate.setId(b.getInt(c));
                arrayList.add(gFitHeartRate);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertGFitHeartRate(GFitHeartRate gFitHeartRate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert((Hh<GFitHeartRate>) gFitHeartRate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitHeartRateDao
    public void insertListGFitHeartRate(List<GFitHeartRate> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitHeartRate.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
