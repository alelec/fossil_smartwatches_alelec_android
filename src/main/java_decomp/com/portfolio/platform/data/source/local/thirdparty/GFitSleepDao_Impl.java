package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSleepDao_Impl implements GFitSleepDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<GFitSleep> __deletionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ Hh<GFitSleep> __insertionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GFitSleep> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitSleep gFitSleep) {
            mi.bindLong(1, (long) gFitSleep.getId());
            mi.bindLong(2, (long) gFitSleep.getSleepMins());
            mi.bindLong(3, gFitSleep.getStartTime());
            mi.bindLong(4, gFitSleep.getEndTime());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitSleep gFitSleep) {
            bind(mi, gFitSleep);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSleep` (`id`,`sleepMins`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<GFitSleep> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitSleep gFitSleep) {
            mi.bindLong(1, (long) gFitSleep.getId());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitSleep gFitSleep) {
            bind(mi, gFitSleep);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `gFitSleep` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM GFitSleep";
        }
    }

    @DexIgnore
    public GFitSleepDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGFitSleep = new Anon1(oh);
        this.__deletionAdapterOfGFitSleep = new Anon2(oh);
        this.__preparedStmtOfClearAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void deleteListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSleep.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public List<GFitSleep> getAllGFitSleep() {
        Rh f = Rh.f("SELECT * FROM gFitSleep", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "sleepMins");
            int c3 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c4 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitSleep gFitSleep = new GFitSleep(b.getInt(c2), b.getLong(c3), b.getLong(c4));
                gFitSleep.setId(b.getInt(c));
                arrayList.add(gFitSleep);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertGFitSleep(GFitSleep gFitSleep) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert((Hh<GFitSleep>) gFitSleep);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitSleepDao
    public void insertListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
