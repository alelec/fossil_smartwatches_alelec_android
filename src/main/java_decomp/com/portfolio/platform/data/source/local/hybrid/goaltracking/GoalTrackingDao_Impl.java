package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Bx0;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.fossil.Qz4;
import com.fossil.Sz4;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingEvent;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.mapped.Xe;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDao_Impl extends GoalTrackingDao {
    @DexIgnore
    public /* final */ Qz4 __dateShortStringConverter; // = new Qz4();
    @DexIgnore
    public /* final */ Sz4 __dateTimeISOStringConverter; // = new Sz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<GoalSetting> __insertionAdapterOfGoalSetting;
    @DexIgnore
    public /* final */ Hh<GoalTrackingData> __insertionAdapterOfGoalTrackingData;
    @DexIgnore
    public /* final */ Hh<GoalTrackingSummary> __insertionAdapterOfGoalTrackingSummary;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllGoalTrackingData;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllGoalTrackingSummaries;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteGoalSetting;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveDeletedGoalTrackingData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GoalSetting> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GoalSetting goalSetting) {
            mi.bindLong(1, (long) goalSetting.getId());
            mi.bindLong(2, (long) goalSetting.getCurrentTarget());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GoalSetting goalSetting) {
            bind(mi, goalSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalSetting` (`id`,`currentTarget`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon10(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "date");
                int c3 = Dx0.c(b, "totalTracked");
                int c4 = Dx0.c(b, "goalTarget");
                int c5 = Dx0.c(b, "createdAt");
                int c6 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon11 implements Callable<GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon11(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public GoalTrackingSummary call() throws Exception {
            GoalTrackingSummary goalTrackingSummary = null;
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "date");
                int c3 = Dx0.c(b, "totalTracked");
                int c4 = Dx0.c(b, "goalTarget");
                int c5 = Dx0.c(b, "createdAt");
                int c6 = Dx0.c(b, "updatedAt");
                if (b.moveToFirst()) {
                    goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                }
                return goalTrackingSummary;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon12 implements Callable<List<GoalTrackingSummary>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon12(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingSummary> call() throws Exception {
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "date");
                int c3 = Dx0.c(b, "totalTracked");
                int c4 = Dx0.c(b, "goalTarget");
                int c5 = Dx0.c(b, "createdAt");
                int c6 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                    goalTrackingSummary.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon13 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon13(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "id");
                int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int c4 = Dx0.c(b, "timezoneOffsetInSecond");
                int c5 = Dx0.c(b, "date");
                int c6 = Dx0.c(b, "createdAt");
                int c7 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                    goalTrackingData.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon14 implements Callable<List<GoalTrackingData>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon14(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<GoalTrackingData> call() throws Exception {
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "id");
                int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
                int c4 = Dx0.c(b, "timezoneOffsetInSecond");
                int c5 = Dx0.c(b, "date");
                int c6 = Dx0.c(b, "createdAt");
                int c7 = Dx0.c(b, "updatedAt");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), GoalTrackingDao_Impl.this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                    goalTrackingData.setPinType(b.getInt(c));
                    arrayList.add(goalTrackingData);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<GoalTrackingSummary> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GoalTrackingSummary goalTrackingSummary) {
            mi.bindLong(1, (long) goalTrackingSummary.getPinType());
            String a2 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingSummary.getDate());
            if (a2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, a2);
            }
            mi.bindLong(3, (long) goalTrackingSummary.getTotalTracked());
            mi.bindLong(4, (long) goalTrackingSummary.getGoalTarget());
            mi.bindLong(5, goalTrackingSummary.getCreatedAt());
            mi.bindLong(6, goalTrackingSummary.getUpdatedAt());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GoalTrackingSummary goalTrackingSummary) {
            bind(mi, goalTrackingSummary);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingDay` (`pinType`,`date`,`totalTracked`,`goalTarget`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Hh<GoalTrackingData> {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GoalTrackingData goalTrackingData) {
            mi.bindLong(1, (long) goalTrackingData.getPinType());
            if (goalTrackingData.getId() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, goalTrackingData.getId());
            }
            String a2 = GoalTrackingDao_Impl.this.__dateTimeISOStringConverter.a(goalTrackingData.getTrackedAt());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            mi.bindLong(4, (long) goalTrackingData.getTimezoneOffsetInSecond());
            String a3 = GoalTrackingDao_Impl.this.__dateShortStringConverter.a(goalTrackingData.getDate());
            if (a3 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a3);
            }
            mi.bindLong(6, goalTrackingData.getCreatedAt());
            mi.bindLong(7, goalTrackingData.getUpdatedAt());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GoalTrackingData goalTrackingData) {
            bind(mi, goalTrackingData);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `goalTrackingRaw` (`pinType`,`id`,`trackedAt`,`timezoneOffsetInSecond`,`date`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM goalSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM goalTrackingDay";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Vh {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw WHERE  id == ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends Vh {
        @DexIgnore
        public Anon7(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM goalTrackingRaw";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<Integer> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon8(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Integer call() throws Exception {
            Integer num = null;
            Cursor b = Ex0.b(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                if (b.moveToFirst() && !b.isNull(0)) {
                    num = Integer.valueOf(b.getInt(0));
                }
                return num;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 extends Xe.Bi<Integer, GoalTrackingSummary> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 extends Bx0<GoalTrackingSummary> {
            @DexIgnore
            public Anon1_Level2(Oh oh, Rh rh, boolean z, String... strArr) {
                super(oh, rh, z, strArr);
            }

            @DexIgnore
            @Override // com.fossil.Bx0
            public List<GoalTrackingSummary> convertRows(Cursor cursor) {
                int c = Dx0.c(cursor, "pinType");
                int c2 = Dx0.c(cursor, "date");
                int c3 = Dx0.c(cursor, "totalTracked");
                int c4 = Dx0.c(cursor, "goalTarget");
                int c5 = Dx0.c(cursor, "createdAt");
                int c6 = Dx0.c(cursor, "updatedAt");
                ArrayList arrayList = new ArrayList(cursor.getCount());
                while (cursor.moveToNext()) {
                    GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(GoalTrackingDao_Impl.this.__dateShortStringConverter.b(cursor.getString(c2)), cursor.getInt(c3), cursor.getInt(c4), cursor.getLong(c5), cursor.getLong(c6));
                    goalTrackingSummary.setPinType(cursor.getInt(c));
                    arrayList.add(goalTrackingSummary);
                }
                return arrayList;
            }
        }

        @DexIgnore
        public Anon9(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        /* Return type fixed from 'com.fossil.Bx0<com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary>' to match base method */
        @Override // com.mapped.Xe.Bi
        public Xe<Integer, GoalTrackingSummary> create() {
            return new Anon1_Level2(GoalTrackingDao_Impl.this.__db, this.val$_statement, false, "goalTrackingDay");
        }
    }

    @DexIgnore
    public GoalTrackingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGoalSetting = new Anon1(oh);
        this.__insertionAdapterOfGoalTrackingSummary = new Anon2(oh);
        this.__insertionAdapterOfGoalTrackingData = new Anon3(oh);
        this.__preparedStmtOfDeleteGoalSetting = new Anon4(oh);
        this.__preparedStmtOfDeleteAllGoalTrackingSummaries = new Anon5(oh);
        this.__preparedStmtOfRemoveDeletedGoalTrackingData = new Anon6(oh);
        this.__preparedStmtOfDeleteAllGoalTrackingData = new Anon7(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingData() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllGoalTrackingData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteAllGoalTrackingSummaries() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllGoalTrackingSummaries.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllGoalTrackingSummaries.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void deleteGoalSetting() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteGoalSetting.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteGoalSetting.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataInDate(Date date) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date== ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date == ? AND updatedAt < ? AND trackedAt < ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 4);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, j);
        String a3 = this.__dateTimeISOStringConverter.a(dateTime);
        if (a3 == null) {
            f.bindNull(3);
        } else {
            f.bindString(3, a3);
        }
        f.bindLong(4, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date == ? AND pinType <> 3 ORDER BY trackedAt DESC limit ?", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        f.bindLong(2, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 3 ORDER BY trackedAt ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon13 anon13 = new Anon13(f);
        return invalidationTracker.d(new String[]{"goalTrackingRaw"}, false, anon13);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "totalTracked");
            int c4 = Dx0.c(b, "goalTarget");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingDay WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon12 anon12 = new Anon12(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon12);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getGoalTrackingSummary(Date date) {
        GoalTrackingSummary goalTrackingSummary = null;
        Rh f = Rh.f("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "totalTracked");
            int c4 = Dx0.c(b, "goalTarget");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
            }
            return goalTrackingSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingSummary> getGoalTrackingSummaryList() {
        Rh f = Rh.f("SELECT * FROM goalTrackingDay", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "totalTracked");
            int c4 = Dx0.c(b, "goalTarget");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingSummary goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
                arrayList.add(goalTrackingSummary);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData() {
        Rh f = Rh.f("SELECT * FROM goalTrackingDay", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date) {
        Rh f = Rh.f("SELECT * FROM goalTrackingDay WHERE date == ?", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon11 anon11 = new Anon11(f);
        return invalidationTracker.d(new String[]{"goalTrackingDay"}, false, anon11);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingDao.TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2) {
        GoalTrackingDao.TotalSummary totalSummary = null;
        Rh f = Rh.f("SELECT SUM(totalTracked) as total_values, SUM(goalTarget) as total_targets FROM goalTrackingDay\n        WHERE date >= ? AND date <= ? ORDER BY date ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "total_values");
            int c2 = Dx0.c(b, "total_targets");
            if (b.moveToFirst()) {
                totalSummary = new GoalTrackingDao.TotalSummary();
                totalSummary.setValues(b.getInt(c));
                totalSummary.setTargets(b.getInt(c2));
            }
            return totalSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public Integer getLastGoalSetting() {
        Integer num = null;
        Rh f = Rh.f("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            if (b.moveToFirst() && !b.isNull(0)) {
                num = Integer.valueOf(b.getInt(0));
            }
            return num;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<Integer> getLastGoalSettingLiveData() {
        Rh f = Rh.f("SELECT currentTarget FROM goalSetting LIMIT 1", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"goalSetting"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public GoalTrackingSummary getNearestGoalTrackingFromDate(Date date) {
        GoalTrackingSummary goalTrackingSummary = null;
        Rh f = Rh.f("SELECT * FROM goalTrackingDay WHERE date <= ? ORDER BY date DESC LIMIT 1", 1);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "date");
            int c3 = Dx0.c(b, "totalTracked");
            int c4 = Dx0.c(b, "goalTarget");
            int c5 = Dx0.c(b, "createdAt");
            int c6 = Dx0.c(b, "updatedAt");
            if (b.moveToFirst()) {
                goalTrackingSummary = new GoalTrackingSummary(this.__dateShortStringConverter.b(b.getString(c2)), b.getInt(c3), b.getInt(c4), b.getLong(c5), b.getLong(c6));
                goalTrackingSummary.setPinType(b.getInt(c));
            }
            return goalTrackingSummary;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList() {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, GoalTrackingEvent.COLUMN_TRACKED_AT);
            int c4 = Dx0.c(b, "timezoneOffsetInSecond");
            int c5 = Dx0.c(b, "date");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GoalTrackingData goalTrackingData = new GoalTrackingData(b.getString(c2), this.__dateTimeISOStringConverter.b(b.getString(c3)), b.getInt(c4), this.__dateShortStringConverter.b(b.getString(c5)), b.getLong(c6), b.getLong(c7));
                goalTrackingData.setPinType(b.getInt(c));
                arrayList.add(goalTrackingData);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM goalTrackingRaw WHERE date >= ? AND date <= ? AND pinType <> 0", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon14 anon14 = new Anon14(f);
        return invalidationTracker.d(new String[]{"goalTrackingRaw"}, false, anon14);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public Xe.Bi<Integer, GoalTrackingSummary> getSummariesDataSource() {
        return new Anon9(Rh.f("SELECT * FROM goalTrackingDay ORDER BY date DESC", 0));
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void removeDeletedGoalTrackingData(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveDeletedGoalTrackingData.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeletedGoalTrackingData.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalSettings(GoalSetting goalSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalSetting.insert((Hh<GoalSetting>) goalSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingData(GoalTrackingData goalTrackingData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert((Hh<GoalTrackingData>) goalTrackingData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingDataList(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingSummary.insert((Hh<GoalTrackingSummary>) goalTrackingSummary);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao
    public void upsertListGoalTrackingData(List<GoalTrackingData> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGoalTrackingData.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
