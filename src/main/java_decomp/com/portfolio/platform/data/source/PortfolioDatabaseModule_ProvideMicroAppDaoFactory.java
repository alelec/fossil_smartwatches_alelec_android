package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideMicroAppDaoFactory implements Factory<MicroAppDao> {
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideMicroAppDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideMicroAppDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideMicroAppDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static MicroAppDao provideMicroAppDao(PortfolioDatabaseModule portfolioDatabaseModule, HybridCustomizeDatabase hybridCustomizeDatabase) {
        MicroAppDao provideMicroAppDao = portfolioDatabaseModule.provideMicroAppDao(hybridCustomizeDatabase);
        Lk7.c(provideMicroAppDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppDao get() {
        return provideMicroAppDao(this.module, this.dbProvider.get());
    }
}
