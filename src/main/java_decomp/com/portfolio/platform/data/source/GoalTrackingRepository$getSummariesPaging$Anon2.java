package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Qq7;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.Zt0;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummariesPaging$2", f = "GoalTrackingRepository.kt", l = {286}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummariesPaging$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Listing<GoalTrackingSummary>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ U04 $appExecutors;
    @DexIgnore
    public /* final */ /* synthetic */ Date $createdDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Ai $listener;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public static /* final */ Anon1_Level2 INSTANCE; // = new Anon1_Level2();

        @DexIgnore
        public final LiveData<NetworkState> apply(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
            return goalTrackingSummaryLocalDataSource.getMNetworkState();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((GoalTrackingSummaryLocalDataSource) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            GoalTrackingSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null) {
                e.invalidate();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 extends Qq7 implements Gg6<Cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryDataSourceFactory $sourceFactory;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon3_Level2(GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory) {
            super(0);
            this.$sourceFactory = goalTrackingSummaryDataSourceFactory;
        }

        @DexIgnore
        @Override // com.mapped.Gg6
        public final void invoke() {
            PagingRequestHelper mHelper;
            GoalTrackingSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
            if (e != null && (mHelper = e.getMHelper()) != null) {
                mHelper.g();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummariesPaging$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, U04 u04, PagingRequestHelper.Ai ai, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$createdDate = date;
        this.$appExecutors = u04;
        this.$listener = ai;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getSummariesPaging$Anon2 goalTrackingRepository$getSummariesPaging$Anon2 = new GoalTrackingRepository$getSummariesPaging$Anon2(this.this$0, this.$createdDate, this.$appExecutors, this.$listener, xe6);
        goalTrackingRepository$getSummariesPaging$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getSummariesPaging$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Listing<GoalTrackingSummary>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getSummariesPaging$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Calendar instance;
        Object A;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "getSummariesPaging");
            GoalTrackingSummaryLocalDataSource.Companion companion = GoalTrackingSummaryLocalDataSource.Companion;
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "Calendar.getInstance()");
            Date time = instance2.getTime();
            Wg6.b(time, "Calendar.getInstance().time");
            Date calculateNextKey = companion.calculateNextKey(time, this.$createdDate);
            instance = Calendar.getInstance();
            Wg6.b(instance, "calendar");
            instance.setTime(calculateNextKey);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.L$1 = calculateNextKey;
            this.L$2 = instance;
            this.label = 1;
            A = encryptedDatabaseManager.A(this);
            if (A == d) {
                return d;
            }
        } else if (i == 1) {
            Date date = (Date) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            instance = (Calendar) this.L$2;
            A = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        GoalTrackingRepository goalTrackingRepository = this.this$0;
        Date date2 = this.$createdDate;
        U04 u04 = this.$appExecutors;
        PagingRequestHelper.Ai ai = this.$listener;
        Wg6.b(instance, "calendar");
        GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory = new GoalTrackingSummaryDataSourceFactory(goalTrackingRepository, (GoalTrackingDatabase) A, date2, u04, ai, instance);
        this.this$0.mSourceFactoryList.add(goalTrackingSummaryDataSourceFactory);
        Cf.Fi.Aii aii = new Cf.Fi.Aii();
        aii.c(30);
        aii.b(false);
        aii.d(30);
        aii.e(5);
        Cf.Fi a2 = aii.a();
        Wg6.b(a2, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a3 = new Zt0(goalTrackingSummaryDataSourceFactory, a2).a();
        Wg6.b(a3, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData c = Ss0.c(goalTrackingSummaryDataSourceFactory.getSourceLiveData(), Anon1_Level2.INSTANCE);
        Wg6.b(c, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing(a3, c, new Anon2_Level2(goalTrackingSummaryDataSourceFactory), new Anon3_Level2(goalTrackingSummaryDataSourceFactory));
    }
}
