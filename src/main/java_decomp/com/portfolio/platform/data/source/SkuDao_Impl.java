package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkuDao_Impl implements SkuDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<SKUModel> __insertionAdapterOfSKUModel;
    @DexIgnore
    public /* final */ Hh<WatchParam> __insertionAdapterOfWatchParam;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUpSku;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUpWatchParam;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<SKUModel> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, SKUModel sKUModel) {
            if (sKUModel.getCreatedAt() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, sKUModel.getCreatedAt());
            }
            if (sKUModel.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, sKUModel.getUpdatedAt());
            }
            if (sKUModel.getSerialNumberPrefix() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, sKUModel.getSerialNumberPrefix());
            }
            if (sKUModel.getSku() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, sKUModel.getSku());
            }
            if (sKUModel.getDeviceName() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, sKUModel.getDeviceName());
            }
            if (sKUModel.getGroupName() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, sKUModel.getGroupName());
            }
            if (sKUModel.getGender() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, sKUModel.getGender());
            }
            if (sKUModel.getDeviceType() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, sKUModel.getDeviceType());
            }
            if (sKUModel.getFastPairId() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, sKUModel.getFastPairId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, SKUModel sKUModel) {
            bind(mi, sKUModel);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SKU` (`createdAt`,`updatedAt`,`serialNumberPrefix`,`sku`,`deviceName`,`groupName`,`gender`,`deviceType`,`fastPairId`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<WatchParam> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, WatchParam watchParam) {
            if (watchParam.getPrefixSerial() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, watchParam.getPrefixSerial());
            }
            if (watchParam.getVersionMajor() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, watchParam.getVersionMajor());
            }
            if (watchParam.getVersionMinor() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, watchParam.getVersionMinor());
            }
            if (watchParam.getData() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, watchParam.getData());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, WatchParam watchParam) {
            bind(mi, watchParam);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchParam` (`prefixSerial`,`versionMajor`,`versionMinor`,`data`) VALUES (?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM SKU";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM watchParam";
        }
    }

    @DexIgnore
    public SkuDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfSKUModel = new Anon1(oh);
        this.__insertionAdapterOfWatchParam = new Anon2(oh);
        this.__preparedStmtOfCleanUpSku = new Anon3(oh);
        this.__preparedStmtOfCleanUpWatchParam = new Anon4(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateSkuList(List<SKUModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSKUModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void addOrUpdateWatchParam(WatchParam watchParam) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchParam.insert((Hh<WatchParam>) watchParam);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpSku() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUpSku.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpSku.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public void cleanUpWatchParam() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUpWatchParam.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpWatchParam.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public List<SKUModel> getAllSkus() {
        Rh f = Rh.f("SELECT * FROM SKU", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "serialNumberPrefix");
            int c4 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c5 = Dx0.c(b, "deviceName");
            int c6 = Dx0.c(b, "groupName");
            int c7 = Dx0.c(b, "gender");
            int c8 = Dx0.c(b, "deviceType");
            int c9 = Dx0.c(b, "fastPairId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                SKUModel sKUModel = new SKUModel(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                sKUModel.setCreatedAt(b.getString(c));
                sKUModel.setUpdatedAt(b.getString(c2));
                arrayList.add(sKUModel);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public SKUModel getSkuByDeviceIdPrefix(String str) {
        SKUModel sKUModel = null;
        Rh f = Rh.f("SELECT * FROM SKU WHERE serialNumberPrefix=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "serialNumberPrefix");
            int c4 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c5 = Dx0.c(b, "deviceName");
            int c6 = Dx0.c(b, "groupName");
            int c7 = Dx0.c(b, "gender");
            int c8 = Dx0.c(b, "deviceType");
            int c9 = Dx0.c(b, "fastPairId");
            if (b.moveToFirst()) {
                sKUModel = new SKUModel(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8), b.getString(c9));
                sKUModel.setCreatedAt(b.getString(c));
                sKUModel.setUpdatedAt(b.getString(c2));
            }
            return sKUModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.SkuDao
    public WatchParam getWatchParamById(String str) {
        WatchParam watchParam = null;
        Rh f = Rh.f("SELECT * FROM watchParam WHERE prefixSerial =?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "prefixSerial");
            int c2 = Dx0.c(b, "versionMajor");
            int c3 = Dx0.c(b, "versionMinor");
            int c4 = Dx0.c(b, "data");
            if (b.moveToFirst()) {
                watchParam = new WatchParam(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4));
            }
            return watchParam;
        } finally {
            b.close();
            f.m();
        }
    }
}
