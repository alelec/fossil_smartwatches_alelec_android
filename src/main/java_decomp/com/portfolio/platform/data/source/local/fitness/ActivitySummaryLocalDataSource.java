package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Nw0;
import com.fossil.Pm7;
import com.fossil.wearables.fsl.enums.ActivityIntensity;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.mapped.Af;
import com.mapped.Ji;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.helper.FitnessHelper;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource extends Af<Date, ActivitySummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ FitnessHelper mFitnessHelper;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver;
    @DexIgnore
    public List<Lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ SummariesRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = activitySummaryLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ActivitySummaryLocalDataSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "XXX-invalidate " + set);
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            Wg6.c(date, "date");
            Wg6.c(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar I = TimeUtils.I(instance);
            if (TimeUtils.j0(date2, I.getTime())) {
                I.setTime(date2);
            }
            Date time = I.getTime();
            Wg6.b(time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ActivitySummaryLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = ActivitySummaryLocalDataSource.class.getSimpleName();
        Wg6.b(simpleName, "ActivitySummaryLocalData\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ActivitySummaryLocalDataSource(SummariesRepository summariesRepository, FitnessHelper fitnessHelper, FitnessDataRepository fitnessDataRepository, FitnessDatabase fitnessDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(summariesRepository, "mSummariesRepository");
        Wg6.c(fitnessHelper, "mFitnessHelper");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(fitnessDatabase, "mFitnessDatabase");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "key");
        this.mSummariesRepository = summariesRepository;
        this.mFitnessHelper = fitnessHelper;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = ai;
        this.key = calendar;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, SampleDay.TABLE_NAME, new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar I = TimeUtils.I(instance);
        I.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + I.getTime());
        Date time = I.getTime();
        Wg6.b(time, "calendar.time");
        this.mStartDate = time;
        if (TimeUtils.j0(date, time)) {
            this.mStartDate = date;
        }
    }

    @DexIgnore
    private final void calculateSummaries(List<ActivitySummary> list) {
        int i;
        int i2;
        int i3;
        if (!list.isEmpty()) {
            Calendar instance = Calendar.getInstance();
            Wg6.b(instance, "endCalendar");
            instance.setTime(((ActivitySummary) Pm7.P(list)).getDate());
            if (instance.get(7) != 1) {
                Calendar b0 = TimeUtils.b0(instance.getTime());
                Wg6.b(b0, "DateHelper.getStartOfWeek(endCalendar.time)");
                instance.add(5, -1);
                ActivitySummaryDao activitySummaryDao = this.mFitnessDatabase.activitySummaryDao();
                Date time = b0.getTime();
                Wg6.b(time, "startCalendar.time");
                Date time2 = instance.getTime();
                Wg6.b(time2, "endCalendar.time");
                ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummaryDao.getTotalValuesOfWeek(time, time2);
                i = totalValuesOfWeek.getTotalActiveTimeOfWeek();
                i2 = (int) totalValuesOfWeek.getTotalCaloriesOfWeek();
                i3 = (int) totalValuesOfWeek.getTotalStepsOfWeek();
            } else {
                i = 0;
                i2 = 0;
                i3 = 0;
            }
            Calendar instance2 = Calendar.getInstance();
            Wg6.b(instance2, "calendar");
            instance2.setTime(((ActivitySummary) Pm7.F(list)).getDate());
            Calendar b02 = TimeUtils.b0(instance2.getTime());
            Wg6.b(b02, "DateHelper.getStartOfWeek(calendar.time)");
            b02.add(5, -1);
            int i4 = 0;
            int i5 = 0;
            double d = 0.0d;
            double d2 = 0.0d;
            int i6 = 0;
            for (T t : list) {
                if (i5 >= 0) {
                    T t2 = t;
                    if (TimeUtils.m0(t2.getDate(), b02.getTime())) {
                        list.get(i4).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i6));
                        b02.add(5, -7);
                        d = 0.0d;
                        d2 = 0.0d;
                        i6 = 0;
                        i4 = i5;
                    }
                    d += t2.getSteps();
                    d2 += t2.getCalories();
                    i6 += t2.getActiveTime();
                    if (i5 == list.size() - 1) {
                        d += (double) i3;
                        d2 += (double) i2;
                        i6 += i;
                    }
                    i5++;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
            list.get(i4).setTotalValuesOfWeek(new ActivitySummary.TotalValuesOfWeek(d, d2, i6));
        }
        FLogger.INSTANCE.getLocal().d(TAG, "calculateSummaries summaries.size=" + list.size());
    }

    @DexIgnore
    private final ActivitySummary dummySummary(ActivitySummary activitySummary, Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        int i = instance.get(1);
        int i2 = instance.get(2);
        ActivitySummary activitySummary2 = new ActivitySummary(i, i2 + 1, instance.get(5), activitySummary.getTimezoneName(), activitySummary.getDstOffset(), 0.0d, 0.0d, 0.0d, Hm7.i(0, 0, 0), 0, 0, 0, 0, 7680, null);
        activitySummary2.setCreatedAt(DateTime.now());
        activitySummary2.setUpdatedAt(DateTime.now());
        return activitySummary2;
    }

    @DexIgnore
    private final List<ActivitySummary> getDataInDatabase(Date date, Date date2) {
        ActivitySummary activitySummary;
        ActivitySummary activitySummary2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("getDataInDatabase - startDate=");
        sb.append(date);
        sb.append(", endDate=");
        sb.append(date2);
        sb.append(" DBNAME=");
        Ji openHelper = this.mFitnessDatabase.getOpenHelper();
        Wg6.b(openHelper, "mFitnessDatabase.openHelper");
        sb.append(openHelper.getDatabaseName());
        local.d(str, sb.toString());
        List<ActivitySummary> activitySummariesDesc = this.mFitnessDatabase.activitySummaryDao().getActivitySummariesDesc(TimeUtils.j0(date, date2) ? date2 : date, date2);
        if (!activitySummariesDesc.isEmpty()) {
            Boolean p0 = TimeUtils.p0(((ActivitySummary) Pm7.F(activitySummariesDesc)).getDate());
            Wg6.b(p0, "DateHelper.isToday(summaries.first().getDate())");
            if (p0.booleanValue()) {
                ((ActivitySummary) Pm7.F(activitySummariesDesc)).setSteps(Math.max((double) this.mFitnessHelper.d(new Date()), ((ActivitySummary) Pm7.F(activitySummariesDesc)).getSteps()));
            }
        }
        calculateSummaries(activitySummariesDesc);
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mFitnessDatabase.activitySummaryDao().getLastDate();
        Date date3 = lastDate != null ? lastDate : date;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(activitySummariesDesc);
        ActivitySummary activitySummary3 = this.mFitnessDatabase.activitySummaryDao().getActivitySummary(date2);
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "endCalendar");
        instance.setTime(date2);
        if (activitySummary3 == null) {
            ActivitySummary activitySummary4 = new ActivitySummary(instance.get(1), instance.get(2) + 1, instance.get(5), "", 0, 0.0d, 0.0d, 0.0d, Hm7.i(0, 0, 0), 0, 0, 0, 0, 7680, null);
            activitySummary4.setActiveTimeGoal(30);
            activitySummary4.setStepGoal(VideoUploader.RETRY_DELAY_UNIT_MS);
            activitySummary4.setCaloriesGoal(ActivityIntensity.MAX_STEPS_PER_MINUTE_MODERATE_LEVEL);
            activitySummary = activitySummary4;
        } else {
            activitySummary = activitySummary3;
        }
        if (!TimeUtils.j0(date, date3)) {
            date = date3;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + activitySummariesDesc.size() + ", summaryParent=" + activitySummary + ", lastDate=" + date3 + ", startDateToFill=" + date);
        while (TimeUtils.k0(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    activitySummary2 = null;
                    break;
                }
                Object next = it.next();
                if (TimeUtils.m0(((ActivitySummary) next).getDate(), date2)) {
                    activitySummary2 = next;
                    break;
                }
            }
            ActivitySummary activitySummary5 = activitySummary2;
            if (activitySummary5 == null) {
                arrayList.add(dummySummary(activitySummary, date2));
            } else {
                arrayList.add(activitySummary5);
                arrayList2.remove(activitySummary5);
            }
            date2 = TimeUtils.P(date2);
            Wg6.b(date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            ActivitySummary activitySummary6 = (ActivitySummary) Pm7.F(arrayList);
            Boolean p02 = TimeUtils.p0(activitySummary6.getDate());
            Wg6.b(p02, "DateHelper.isToday(todaySummary.getDate())");
            if (p02.booleanValue()) {
                arrayList.add(0, new ActivitySummary(activitySummary6));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final void loadData(PagingRequestHelper.Di di, Date date, Date date2, PagingRequestHelper.Bi.Aii aii) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadData start=" + date + ", end=" + date2);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new ActivitySummaryLocalDataSource$loadData$Anon1(this, date, date2, di, aii, null), 3, null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadAfter(Af.Fi<Date> fi, Af.Ai<Date, ActivitySummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Object) fi.a));
        if (TimeUtils.j0(fi.a, this.mCreatedDate)) {
            Key key2 = fi.a;
            Wg6.b(key2, "params.key");
            Key key3 = key2;
            Companion companion = Companion;
            Key key4 = fi.a;
            Wg6.b(key4, "params.key");
            Date calculateNextKey = companion.calculateNextKey(key4, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date O = TimeUtils.m0(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : TimeUtils.O(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + O + ", endQueryDate=" + ((Object) key3));
            Wg6.b(O, "startQueryDate");
            ai.a(getDataInDatabase(O, key3), calculateNextKey);
            if (TimeUtils.j0(this.mStartDate, key3)) {
                this.mEndDate = key3;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new Lc6<>(this.mStartDate, this.mEndDate));
                this.mHelper.h(PagingRequestHelper.Di.AFTER, new ActivitySummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadBefore(Af.Fi<Date> fi, Af.Ai<Date, ActivitySummary> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Af
    public void loadInitial(Af.Ei<Date> ei, Af.Ci<Date, ActivitySummary> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date O = TimeUtils.m0(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : TimeUtils.O(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + O + ", endQueryDate=" + date);
        Wg6.b(O, "startQueryDate");
        ci.a(getDataInDatabase(O, date), null, this.key.getTime());
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new ActivitySummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        Wg6.c(date, "<set-?>");
        this.mStartDate = date;
    }
}
