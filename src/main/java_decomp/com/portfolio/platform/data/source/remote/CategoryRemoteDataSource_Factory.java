package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRemoteDataSource_Factory implements Factory<CategoryRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public CategoryRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static CategoryRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new CategoryRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static CategoryRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new CategoryRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public CategoryRemoteDataSource get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
