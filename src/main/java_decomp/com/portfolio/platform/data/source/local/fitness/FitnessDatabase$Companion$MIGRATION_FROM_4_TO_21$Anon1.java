package com.portfolio.platform.data.source.local.fitness;

import com.fossil.Lh5;
import com.fossil.Lx0;
import com.fossil.Nh5;
import com.mapped.Ph4;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1 extends Xh {
    @DexIgnore
    public FitnessDatabase$Companion$MIGRATION_FROM_4_TO_21$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration 4 to 21 start");
        lx0.beginTransaction();
        DateTime dateTime = new DateTime();
        TimeZone timeZone = dateTime.getZone().toTimeZone();
        int year = dateTime.getYear();
        int monthOfYear = dateTime.getMonthOfYear();
        int dayOfMonth = dateTime.getDayOfMonth();
        Wg6.b(timeZone, "timeZone");
        String id = timeZone.getID();
        int dSTSavings = timeZone.inDaylightTime(dateTime.toDate()) ? timeZone.getDSTSavings() : 0;
        FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate with calories goal 140");
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table");
            lx0.execSQL("CREATE TABLE sampleraw_new (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + Ph4.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
            lx0.execSQL("INSERT INTO sampleraw_new (id, startTime, endTime, sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID) SELECT id, substr(startTime, 1, 23), substr(endTime, 1, 23), sourceId, movementTypeValue, steps, calories, distance, sourceTypeValue, pinType, uaPinType, timeZoneID  FROM sampleraw");
            lx0.execSQL("DROP TABLE sampleraw");
            lx0.execSQL("ALTER TABLE sampleraw_new RENAME TO sampleraw");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample raw table success");
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(FitnessDatabase.TAG, "Migrate sample raw table fail " + e);
            lx0.execSQL("DROP TABLE IF EXISTS sampleraw_new");
            lx0.execSQL("DROP TABLE IF EXISTS sampleraw");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sampleraw (id TEXT PRIMARY KEY NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, sourceId TEXT NOT NULL DEFAULT '', movementTypeValue TEXT NOT NULL DEFAULT '" + Nh5.WALKING.getValue() + "', steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, sourceTypeValue TEXT NOT NULL DEFAULT '" + Ph4.Device.getValue() + "', pinType INTEGER NOT NULL DEFAULT 1, uaPinType INTEGER NOT NULL DEFAULT 1, timeZoneID TEXT, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '')");
        }
        try {
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw");
            lx0.execSQL("CREATE TABLE intensities (min INTEGER NOT NULL, max INTEGER NOT NULL, intensity TEXT NOT NULL DEFAULT '')");
            lx0.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (0, 70, 'light')");
            lx0.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (70, 140, 'moderate')");
            lx0.execSQL("INSERT INTO intensities (min, max, intensity) VALUES (140, 2147483647, 'intense')");
            lx0.execSQL("UPDATE sampleraw SET intensityDistInSteps=( SELECT '{\"' || intensity || '\":' || sampleraw.steps || '}' FROM intensities WHERE sampleraw.steps >= intensities.min AND sampleraw.steps < intensities.max)");
            lx0.execSQL("DROP TABLE intensities");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw success");
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(FitnessDatabase.TAG, "Migrate intensity data for sampleraw " + e2);
            lx0.execSQL("DROP TABLE IF EXISTS intensities");
        }
        try {
            lx0.execSQL("CREATE TABLE sampleday_new (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT " + Lh5.b + ".DEFAULT_CAlORIES_GOAL, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
            lx0.execSQL("INSERT INTO sampleday_new (year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType) SELECT year, month, day, timezoneName, dstOffset, steps, calories, distance, createdAt, updatedAt, pinType  FROM sampleday");
            lx0.execSQL("DROP TABLE sampleday");
            lx0.execSQL("ALTER TABLE sampleday_new RENAME TO sampleday");
            lx0.execSQL("INSERT OR IGNORE INTO sampleday (year, month, day, timezoneName, dstOffset, steps, calories, distance, intensities, stepGoal, caloriesGoal, activeTimeGoal, createdAt, updatedAt, activeTime, pinType) VALUES(" + year + ", " + monthOfYear + ", " + dayOfMonth + ", '" + id + "', " + dSTSavings + ", 0.0, 0.0, 0.0, '[0,0,0]', 5000, " + Lh5.b + ".DEFAULT_CAlORIES_GOAL, 30, " + System.currentTimeMillis() + ", " + System.currentTimeMillis() + ", 0, 1)");
            lx0.execSQL("UPDATE sampleday SET stepGoal=(SELECT steps FROM dailygoal) WHERE sampleday.year = year AND sampleday.month = month AND sampleday.day = day");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migrate sample day success");
        } catch (Exception e3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d(FitnessDatabase.TAG, "Migrate sample day fail " + e3);
            lx0.execSQL("DROP TABLE IF EXISTS sampleday_new");
            lx0.execSQL("DROP TABLE IF EXISTS sampleday");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS sampleday (year INTEGER NOT NULL, month INTEGER NOT NULL, day INTEGER NOT NULL, timezoneName TEXT, dstOffset INTEGER, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, intensities TEXT NOT NULL DEFAULT '', stepGoal INTEGER NOT NULL DEFAULT 0, caloriesGoal INTEGER NOT NULL DEFAULT 0, activeTimeGoal INTEGER NOT NULL DEFAULT 0, createdAt INTEGER, updatedAt INTEGER, activeTime INTEGER NOT NULL DEFAULT 0, pinType INTEGER NOT NULL DEFAULT 1, PRIMARY KEY (year, month, day))");
        }
        try {
            lx0.execSQL("CREATE TABLE activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT " + Lh5.b + ".DEFAULT_CAlORIES_GOAL, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
            lx0.execSQL("INSERT INTO activitySettings (currentStepGoal, currentCaloriesGoal) VALUES (IFNULL((SELECT steps FROM dailygoal ORDER BY year DESC, month DESC, day DESC LIMIT 1), 5000), " + Lh5.b + ".DEFAULT_CAlORIES_GOAL)");
            lx0.execSQL("DROP TABLE dailygoal");
            FLogger.INSTANCE.getLocal().d(FitnessDatabase.TAG, "Migration activitySetting success");
        } catch (Exception e4) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d(FitnessDatabase.TAG, "Migration activitySetting fail " + e4);
            lx0.execSQL("DROP TABLE IF EXISTS dailygoal");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS activitySettings (currentStepGoal INTEGER NOT NULL DEFAULT 5000, currentCaloriesGoal INTEGER NOT NULL DEFAULT 140, currentActiveTimeGoal INTEGER NOT NULL DEFAULT 30, id INTEGER PRIMARY KEY ASC NOT NULL)");
        }
        lx0.execSQL("CREATE TABLE IF NOT EXISTS workout_session (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, deviceSerialNumber TEXT, step TEXT, calorie TEXT, distance TEXT, heartRate TEXT, speed TEXT, location TEXT, states TEXT NOT NULL DEFAULT '', sourceType TEXT, workoutType TEXT, timezoneOffset INTEGER NOT NULL, duration INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        lx0.execSQL("CREATE TABLE IF NOT EXISTS fitness_data (startTime TEXT PRIMARY KEY NOT NULL, endTime TEXT NOT NULL, syncTime TEXT NOT NULL, timezoneOffsetInSecond INTEGER NOT NULL, serialNumber TEXT NOT NULL DEFAULT '', step TEXT NOT NULL, activeMinute TEXT NOT NULL, calorie TEXT NOT NULL, distance TEXT NOT NULL, stress TEXT, resting TEXT NOT NULL DEFAULT '', heartRate TEXT, sleeps TEXT NOT NULL DEFAULT '', workouts TEXT NOT NULL DEFAULT '')");
        lx0.execSQL("CREATE TABLE IF NOT EXISTS activityRecommendedGoals (recommendedStepsGoal INTEGER NOT NULL, recommendedCaloriesGoal INTEGER NOT NULL, recommendedActiveTimeGoal INTEGER NOT NULL, id INTEGER PRIMARY KEY ASC NOT NULL)");
        lx0.execSQL("CREATE TABLE IF NOT EXISTS heart_rate_sample (id TEXT PRIMARY KEY NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, average REAL NOT NULL, timezoneOffset INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL)");
        lx0.execSQL("CREATE TABLE IF NOT EXISTS daily_heart_rate_summary (average REAL NOT NULL, date TEXT PRIMARY KEY NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL, min INTEGER NOT NULL DEFAULT 0, max INTEGER NOT NULL DEFAULT 0, minuteCount INTEGER NOT NULL DEFAULT 1, resting TEXT)");
        try {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS activity_statistic (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, activeTimeBestDay TEXT, activeTimeBestStreak TEXT, caloriesBestDay TEXT, caloriesBestStreak TEXT, stepsBestDay TEXT, stepsBestStreak TEXT, totalActiveTime INTEGER NOT NULL, totalCalories REAL NOT NULL, totalDays INTEGER NOT NULL, totalDistance REAL NOT NULL, totalSteps INTEGER NOT NULL, totalIntensityDistInStep TEXT NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS activity_sample (id TEXT PRIMARY KEY NOT NULL, uid TEXT NOT NULL, date TEXT NOT NULL, startTime TEXT NOT NULL, endTime TEXT NOT NULL, steps REAL NOT NULL DEFAULT 0, calories REAL NOT NULL DEFAULT 0, distance REAL NOT NULL DEFAULT 0, activeTime INTEGER NOT NULL DEFAULT 0, intensityDistInSteps TEXT NOT NULL DEFAULT '', timeZoneOffsetInSecond INTEGER NOT NULL, sourceId TEXT NOT NULL DEFAULT '', syncTime INTEGER NOT NULL, createdAt INTEGER NOT NULL, updatedAt INTEGER NOT NULL);");
        } catch (Exception e5) {
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            local5.e(FitnessDatabase.TAG, "MIGRATION_FROM_4_TO_13 - ActivityStatistic -- e=" + e5);
            e5.printStackTrace();
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
