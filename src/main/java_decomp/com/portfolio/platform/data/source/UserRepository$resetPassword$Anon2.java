package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.UserRepository$resetPassword$2", f = "UserRepository.kt", l = {265}, m = "invokeSuspend")
public final class UserRepository$resetPassword$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Integer>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $email;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$resetPassword$Anon2(UserRepository userRepository, String str, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = userRepository;
        this.$email = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        UserRepository$resetPassword$Anon2 userRepository$resetPassword$Anon2 = new UserRepository$resetPassword$Anon2(this.this$0, this.$email, xe6);
        userRepository$resetPassword$Anon2.p$ = (Il6) obj;
        throw null;
        //return userRepository$resetPassword$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Integer>> xe6) {
        throw null;
        //return ((UserRepository$resetPassword$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            FLogger.INSTANCE.getLocal().d(UserRepository.TAG, "resetPassword");
            UserRemoteDataSource userRemoteDataSource = this.this$0.mUserRemoteDataSource;
            String str = this.$email;
            this.L$0 = il6;
            this.label = 1;
            Object resetPassword = userRemoteDataSource.resetPassword(str, this);
            return resetPassword == d ? d : resetPassword;
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
