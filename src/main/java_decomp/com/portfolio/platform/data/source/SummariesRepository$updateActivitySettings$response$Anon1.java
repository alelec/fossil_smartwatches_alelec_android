package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$updateActivitySettings$response$1", f = "SummariesRepository.kt", l = {116}, m = "invokeSuspend")
public final class SummariesRepository$updateActivitySettings$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ActivitySettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ku3 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$updateActivitySettings$response$Anon1(SummariesRepository summariesRepository, Ku3 ku3, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = summariesRepository;
        this.$jsonObject = ku3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new SummariesRepository$updateActivitySettings$response$Anon1(this.this$0, this.$jsonObject, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ActivitySettings>> xe6) {
        throw null;
        //return ((SummariesRepository$updateActivitySettings$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            Ku3 ku3 = this.$jsonObject;
            Wg6.b(ku3, "jsonObject");
            this.label = 1;
            Object updateActivitySetting = apiServiceV2.updateActivitySetting(ku3, this);
            return updateActivitySetting == d ? d : updateActivitySetting;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
