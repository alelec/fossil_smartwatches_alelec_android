package com.portfolio.platform.data.source.local.thirdparty;

import com.fossil.Ex0;
import com.fossil.Hw0;
import com.fossil.Ix0;
import com.fossil.Lx0;
import com.fossil.Nw0;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Ji;
import com.mapped.Oh;
import com.mapped.Qh;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyDatabase_Impl extends ThirdPartyDatabase {
    @DexIgnore
    public volatile GFitActiveTimeDao _gFitActiveTimeDao;
    @DexIgnore
    public volatile GFitHeartRateDao _gFitHeartRateDao;
    @DexIgnore
    public volatile GFitSampleDao _gFitSampleDao;
    @DexIgnore
    public volatile GFitSleepDao _gFitSleepDao;
    @DexIgnore
    public volatile GFitWorkoutSessionDao _gFitWorkoutSessionDao;
    @DexIgnore
    public volatile UASampleDao _uASampleDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Qh.Ai {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void createAllTables(Lx0 lx0) {
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitActiveTime` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `activeTimes` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitHeartRate` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `value` REAL NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitWorkoutSession` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL, `workoutType` INTEGER NOT NULL, `steps` TEXT NOT NULL, `calories` TEXT NOT NULL, `distances` TEXT NOT NULL, `heartRates` TEXT NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `uaSample` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `step` INTEGER NOT NULL, `distance` REAL NOT NULL, `calorie` REAL NOT NULL, `time` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS `gFitSleep` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `sleepMins` INTEGER NOT NULL, `startTime` INTEGER NOT NULL, `endTime` INTEGER NOT NULL)");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            lx0.execSQL("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4a002011cd11b3072c8efe0a76ce12f3')");
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void dropAllTables(Lx0 lx0) {
            lx0.execSQL("DROP TABLE IF EXISTS `gFitSample`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitActiveTime`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitHeartRate`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitWorkoutSession`");
            lx0.execSQL("DROP TABLE IF EXISTS `uaSample`");
            lx0.execSQL("DROP TABLE IF EXISTS `gFitSleep`");
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onDestructiveMigration(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onCreate(Lx0 lx0) {
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onCreate(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onOpen(Lx0 lx0) {
            ThirdPartyDatabase_Impl.this.mDatabase = lx0;
            ThirdPartyDatabase_Impl.this.internalInitInvalidationTracker(lx0);
            if (ThirdPartyDatabase_Impl.this.mCallbacks != null) {
                int size = ThirdPartyDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((Oh.Bi) ThirdPartyDatabase_Impl.this.mCallbacks.get(i)).onOpen(lx0);
                }
            }
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPostMigrate(Lx0 lx0) {
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public void onPreMigrate(Lx0 lx0) {
            Ex0.a(lx0);
        }

        @DexIgnore
        @Override // com.mapped.Qh.Ai
        public Qh.Bi onValidateSchema(Lx0 lx0) {
            HashMap hashMap = new HashMap(6);
            hashMap.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap.put("step", new Ix0.Ai("step", "INTEGER", true, 0, null, 1));
            hashMap.put("distance", new Ix0.Ai("distance", "REAL", true, 0, null, 1));
            hashMap.put("calorie", new Ix0.Ai("calorie", "REAL", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            Ix0 ix0 = new Ix0("gFitSample", hashMap, new HashSet(0), new HashSet(0));
            Ix0 a2 = Ix0.a(lx0, "gFitSample");
            if (!ix0.equals(a2)) {
                return new Qh.Bi(false, "gFitSample(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample).\n Expected:\n" + ix0 + "\n Found:\n" + a2);
            }
            HashMap hashMap2 = new HashMap(2);
            hashMap2.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap2.put("activeTimes", new Ix0.Ai("activeTimes", "TEXT", true, 0, null, 1));
            Ix0 ix02 = new Ix0("gFitActiveTime", hashMap2, new HashSet(0), new HashSet(0));
            Ix0 a3 = Ix0.a(lx0, "gFitActiveTime");
            if (!ix02.equals(a3)) {
                return new Qh.Bi(false, "gFitActiveTime(com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime).\n Expected:\n" + ix02 + "\n Found:\n" + a3);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap3.put("value", new Ix0.Ai("value", "REAL", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap3.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            Ix0 ix03 = new Ix0("gFitHeartRate", hashMap3, new HashSet(0), new HashSet(0));
            Ix0 a4 = Ix0.a(lx0, "gFitHeartRate");
            if (!ix03.equals(a4)) {
                return new Qh.Bi(false, "gFitHeartRate(com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate).\n Expected:\n" + ix03 + "\n Found:\n" + a4);
            }
            HashMap hashMap4 = new HashMap(8);
            hashMap4.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap4.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            hashMap4.put("workoutType", new Ix0.Ai("workoutType", "INTEGER", true, 0, null, 1));
            hashMap4.put("steps", new Ix0.Ai("steps", "TEXT", true, 0, null, 1));
            hashMap4.put("calories", new Ix0.Ai("calories", "TEXT", true, 0, null, 1));
            hashMap4.put("distances", new Ix0.Ai("distances", "TEXT", true, 0, null, 1));
            hashMap4.put("heartRates", new Ix0.Ai("heartRates", "TEXT", true, 0, null, 1));
            Ix0 ix04 = new Ix0("gFitWorkoutSession", hashMap4, new HashSet(0), new HashSet(0));
            Ix0 a5 = Ix0.a(lx0, "gFitWorkoutSession");
            if (!ix04.equals(a5)) {
                return new Qh.Bi(false, "gFitWorkoutSession(com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession).\n Expected:\n" + ix04 + "\n Found:\n" + a5);
            }
            HashMap hashMap5 = new HashMap(5);
            hashMap5.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap5.put("step", new Ix0.Ai("step", "INTEGER", true, 0, null, 1));
            hashMap5.put("distance", new Ix0.Ai("distance", "REAL", true, 0, null, 1));
            hashMap5.put("calorie", new Ix0.Ai("calorie", "REAL", true, 0, null, 1));
            hashMap5.put(LogBuilder.KEY_TIME, new Ix0.Ai(LogBuilder.KEY_TIME, "INTEGER", true, 0, null, 1));
            Ix0 ix05 = new Ix0("uaSample", hashMap5, new HashSet(0), new HashSet(0));
            Ix0 a6 = Ix0.a(lx0, "uaSample");
            if (!ix05.equals(a6)) {
                return new Qh.Bi(false, "uaSample(com.portfolio.platform.data.model.thirdparty.ua.UASample).\n Expected:\n" + ix05 + "\n Found:\n" + a6);
            }
            HashMap hashMap6 = new HashMap(4);
            hashMap6.put("id", new Ix0.Ai("id", "INTEGER", true, 1, null, 1));
            hashMap6.put("sleepMins", new Ix0.Ai("sleepMins", "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_START_TIME, new Ix0.Ai(SampleRaw.COLUMN_START_TIME, "INTEGER", true, 0, null, 1));
            hashMap6.put(SampleRaw.COLUMN_END_TIME, new Ix0.Ai(SampleRaw.COLUMN_END_TIME, "INTEGER", true, 0, null, 1));
            Ix0 ix06 = new Ix0("gFitSleep", hashMap6, new HashSet(0), new HashSet(0));
            Ix0 a7 = Ix0.a(lx0, "gFitSleep");
            if (ix06.equals(a7)) {
                return new Qh.Bi(true, null);
            }
            return new Qh.Bi(false, "gFitSleep(com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep).\n Expected:\n" + ix06 + "\n Found:\n" + a7);
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public void clearAllTables() {
        super.assertNotMainThread();
        Lx0 writableDatabase = super.getOpenHelper().getWritableDatabase();
        try {
            super.beginTransaction();
            writableDatabase.execSQL("DELETE FROM `gFitSample`");
            writableDatabase.execSQL("DELETE FROM `gFitActiveTime`");
            writableDatabase.execSQL("DELETE FROM `gFitHeartRate`");
            writableDatabase.execSQL("DELETE FROM `gFitWorkoutSession`");
            writableDatabase.execSQL("DELETE FROM `uaSample`");
            writableDatabase.execSQL("DELETE FROM `gFitSleep`");
            super.setTransactionSuccessful();
        } finally {
            super.endTransaction();
            writableDatabase.query("PRAGMA wal_checkpoint(FULL)").close();
            if (!writableDatabase.inTransaction()) {
                writableDatabase.execSQL("VACUUM");
            }
        }
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Nw0 createInvalidationTracker() {
        return new Nw0(this, new HashMap(0), new HashMap(0), "gFitSample", "gFitActiveTime", "gFitHeartRate", "gFitWorkoutSession", "uaSample", "gFitSleep");
    }

    @DexIgnore
    @Override // com.mapped.Oh
    public Ji createOpenHelper(Hw0 hw0) {
        Qh qh = new Qh(hw0, new Anon1(2), "4a002011cd11b3072c8efe0a76ce12f3", "fd54d50c044f828885f0e40cfeeed303");
        Ji.Bi.Aii a2 = Ji.Bi.a(hw0.b);
        a2.c(hw0.c);
        a2.b(qh);
        return hw0.a.create(a2.a());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitActiveTimeDao getGFitActiveTimeDao() {
        GFitActiveTimeDao gFitActiveTimeDao;
        if (this._gFitActiveTimeDao != null) {
            return this._gFitActiveTimeDao;
        }
        synchronized (this) {
            if (this._gFitActiveTimeDao == null) {
                this._gFitActiveTimeDao = new GFitActiveTimeDao_Impl(this);
            }
            gFitActiveTimeDao = this._gFitActiveTimeDao;
        }
        return gFitActiveTimeDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitHeartRateDao getGFitHeartRateDao() {
        GFitHeartRateDao gFitHeartRateDao;
        if (this._gFitHeartRateDao != null) {
            return this._gFitHeartRateDao;
        }
        synchronized (this) {
            if (this._gFitHeartRateDao == null) {
                this._gFitHeartRateDao = new GFitHeartRateDao_Impl(this);
            }
            gFitHeartRateDao = this._gFitHeartRateDao;
        }
        return gFitHeartRateDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSampleDao getGFitSampleDao() {
        GFitSampleDao gFitSampleDao;
        if (this._gFitSampleDao != null) {
            return this._gFitSampleDao;
        }
        synchronized (this) {
            if (this._gFitSampleDao == null) {
                this._gFitSampleDao = new GFitSampleDao_Impl(this);
            }
            gFitSampleDao = this._gFitSampleDao;
        }
        return gFitSampleDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitSleepDao getGFitSleepDao() {
        GFitSleepDao gFitSleepDao;
        if (this._gFitSleepDao != null) {
            return this._gFitSleepDao;
        }
        synchronized (this) {
            if (this._gFitSleepDao == null) {
                this._gFitSleepDao = new GFitSleepDao_Impl(this);
            }
            gFitSleepDao = this._gFitSleepDao;
        }
        return gFitSleepDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public GFitWorkoutSessionDao getGFitWorkoutSessionDao() {
        GFitWorkoutSessionDao gFitWorkoutSessionDao;
        if (this._gFitWorkoutSessionDao != null) {
            return this._gFitWorkoutSessionDao;
        }
        synchronized (this) {
            if (this._gFitWorkoutSessionDao == null) {
                this._gFitWorkoutSessionDao = new GFitWorkoutSessionDao_Impl(this);
            }
            gFitWorkoutSessionDao = this._gFitWorkoutSessionDao;
        }
        return gFitWorkoutSessionDao;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase
    public UASampleDao getUASampleDao() {
        UASampleDao uASampleDao;
        if (this._uASampleDao != null) {
            return this._uASampleDao;
        }
        synchronized (this) {
            if (this._uASampleDao == null) {
                this._uASampleDao = new UASampleDao_Impl(this);
            }
            uASampleDao = this._uASampleDao;
        }
        return uASampleDao;
    }
}
