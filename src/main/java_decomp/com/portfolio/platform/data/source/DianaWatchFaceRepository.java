package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ux7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Il6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.remote.DianaWatchFaceRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ int DIANA_WATCH_FACE_USER_LIMITS; // = 20;
    @DexIgnore
    public static /* final */ String TAG; // = "DianaWatchFaceRepository";
    @DexIgnore
    public /* final */ DianaWatchFaceRemoteDataSource mDianaWatchFaceRemoteDataSource;
    @DexIgnore
    public /* final */ FileRepository mFileRepository;
    @DexIgnore
    public /* final */ Il6 mRepositoryScope; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaWatchFaceRepository(FileRepository fileRepository, DianaWatchFaceRemoteDataSource dianaWatchFaceRemoteDataSource) {
        Wg6.c(fileRepository, "mFileRepository");
        Wg6.c(dianaWatchFaceRemoteDataSource, "mDianaWatchFaceRemoteDataSource");
        this.mFileRepository = fileRepository;
        this.mDianaWatchFaceRemoteDataSource = dianaWatchFaceRemoteDataSource;
    }

    @DexIgnore
    public static /* synthetic */ Object asyncDownloadAllDianaWatchFaceUser$default(DianaWatchFaceRepository dianaWatchFaceRepository, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return dianaWatchFaceRepository.asyncDownloadAllDianaWatchFaceUser(i);
    }

    @DexIgnore
    public static /* synthetic */ Object createWatchFace$default(DianaWatchFaceRepository dianaWatchFaceRepository, String str, String str2, String str3, String str4, Xe6 xe6, int i, Object obj) {
        return dianaWatchFaceRepository.createWatchFace(str, str2, str3, (i & 8) != 0 ? "" : str4, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object downloadAllDianaWatchFaceUser$default(DianaWatchFaceRepository dianaWatchFaceRepository, int i, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return dianaWatchFaceRepository.downloadAllDianaWatchFaceUser(i, xe6);
    }

    @DexIgnore
    public final Object asyncDownloadAllDianaWatchFaceUser(int i) {
        return Gu7.d(this.mRepositoryScope, null, null, new DianaWatchFaceRepository$asyncDownloadAllDianaWatchFaceUser$Anon1(this, i, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object cleanUp(com.mapped.Xe6<? super com.mapped.Cd6> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.DianaWatchFaceRepository$cleanUp$Anon1
            if (r0 == 0) goto L_0x0033
            r0 = r6
            com.portfolio.platform.data.source.DianaWatchFaceRepository$cleanUp$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRepository$cleanUp$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0033
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0041
            if (r3 != r4) goto L_0x0039
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao r0 = r0.getDianaWatchFaceUserDao()
            r0.deleteAll()
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0032:
            return r0
        L_0x0033:
            com.portfolio.platform.data.source.DianaWatchFaceRepository$cleanUp$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRepository$cleanUp$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0039:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0041:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository.cleanUp(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x011c  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x018b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0239  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object createWatchFace(java.lang.String r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.watchface.DianaWatchFaceUser>> r23) {
        /*
        // Method dump skipped, instructions count: 653
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository.createWatchFace(java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object deleteDianaWatchFaceUserById(String str, Xe6<? super Ap4<Object>> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$deleteDianaWatchFaceUserById$Anon2(this, str, null), xe6);
    }

    @DexIgnore
    public final Object downloadAllDianaWatchFaceUser(int i, Xe6<Object> xe6) {
        Object g = Eu7.g(Bw7.b(), new DianaWatchFaceRepository$downloadAllDianaWatchFaceUser$Anon2(this, i, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object downloadDianaWatchFaceUser(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$downloadDianaWatchFaceUser$Anon2(this, str, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadDianaWatchFaceUserWithOrderId(java.lang.String r8, com.mapped.Xe6<? super java.lang.Boolean> r9) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon1
            if (r0 == 0) goto L_0x0036
            r0 = r9
            com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0036
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0045
            if (r0 != r6) goto L_0x003d
            java.lang.Object r0 = r2.L$2
            com.fossil.Zq7 r0 = (com.fossil.Zq7) r0
            java.lang.Object r1 = r2.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r2.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRepository r1 = (com.portfolio.platform.data.source.DianaWatchFaceRepository) r1
            com.fossil.El7.b(r3)
        L_0x002f:
            boolean r0 = r0.element
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
        L_0x0035:
            return r0
        L_0x0036:
            com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon1
            r0.<init>(r7, r9)
            r2 = r0
            goto L_0x0014
        L_0x003d:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0045:
            com.fossil.El7.b(r3)
            com.fossil.Zq7 r0 = new com.fossil.Zq7
            r0.<init>()
            r3 = 0
            r0.element = r3
            com.fossil.Dv7 r3 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2 r4 = new com.portfolio.platform.data.source.DianaWatchFaceRepository$downloadDianaWatchFaceUserWithOrderId$Anon2
            r5 = 0
            r4.<init>(r7, r8, r0, r5)
            r2.L$0 = r7
            r2.L$1 = r8
            r2.L$2 = r0
            r2.label = r6
            java.lang.Object r2 = com.fossil.Eu7.g(r3, r4, r2)
            if (r2 != r1) goto L_0x002f
            r0 = r1
            goto L_0x0035
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository.downloadDianaWatchFaceUserWithOrderId(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object downloadWatchFace(String str, String str2, String str3, Xe6<? super Boolean> xe6) {
        return this.mFileRepository.downloadAndSaveWithFileName(str2, str, FileType.MY_FACES, str3, xe6);
    }

    @DexIgnore
    public final Object executePendingRequest(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new DianaWatchFaceRepository$executePendingRequest$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object getAllDianaWatchFaceUser(Xe6<? super List<DianaWatchFaceUser>> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getAllDianaWatchFaceUser$Anon2(null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllDianaWatchFaceUserLiveData(com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.watchface.DianaWatchFaceUser>>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.DianaWatchFaceRepository$getAllDianaWatchFaceUserLiveData$Anon1
            if (r0 == 0) goto L_0x0032
            r0 = r6
            com.portfolio.platform.data.source.DianaWatchFaceRepository$getAllDianaWatchFaceUserLiveData$Anon1 r0 = (com.portfolio.platform.data.source.DianaWatchFaceRepository$getAllDianaWatchFaceUserLiveData$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0032
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0040
            if (r3 != r4) goto L_0x0038
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.DianaWatchFaceRepository r0 = (com.portfolio.platform.data.source.DianaWatchFaceRepository) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase r0 = (com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase) r0
            com.portfolio.platform.data.source.local.diana.DianaWatchFaceUserDao r0 = r0.getDianaWatchFaceUserDao()
            androidx.lifecycle.LiveData r0 = r0.getAllDianaWatchFaceUserLiveData()
        L_0x0031:
            return r0
        L_0x0032:
            com.portfolio.platform.data.source.DianaWatchFaceRepository$getAllDianaWatchFaceUserLiveData$Anon1 r0 = new com.portfolio.platform.data.source.DianaWatchFaceRepository$getAllDianaWatchFaceUserLiveData$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            com.fossil.El7.b(r1)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r0 = r1.v(r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DianaWatchFaceRepository.getAllDianaWatchFaceUserLiveData(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getDianaWatchFaceUser(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getDianaWatchFaceUser$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getDianaWatchFaceUserById(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getDianaWatchFaceUserById$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getDianaWatchFaceUserByOrderId(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getDianaWatchFaceUserByOrderId$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getWatchFaceByOrderId(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getWatchFaceByOrderId$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getWatchFaceByOrderWatchFaceId(String str, Xe6<? super DianaWatchFaceUser> xe6) {
        return Eu7.g(Bw7.b(), new DianaWatchFaceRepository$getWatchFaceByOrderWatchFaceId$Anon2(str, null), xe6);
    }
}
