package com.portfolio.platform.data.source.loader;

import android.content.Context;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSettingRepository;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingListLoader_Factory implements Factory<MicroAppSettingListLoader> {
    @DexIgnore
    public /* final */ Provider<Context> contextProvider;
    @DexIgnore
    public /* final */ Provider<MicroAppSettingRepository> microAppSettingRepositoryProvider;

    @DexIgnore
    public MicroAppSettingListLoader_Factory(Provider<Context> provider, Provider<MicroAppSettingRepository> provider2) {
        this.contextProvider = provider;
        this.microAppSettingRepositoryProvider = provider2;
    }

    @DexIgnore
    public static MicroAppSettingListLoader_Factory create(Provider<Context> provider, Provider<MicroAppSettingRepository> provider2) {
        return new MicroAppSettingListLoader_Factory(provider, provider2);
    }

    @DexIgnore
    public static MicroAppSettingListLoader newInstance(Context context, MicroAppSettingRepository microAppSettingRepository) {
        return new MicroAppSettingListLoader(context, microAppSettingRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public MicroAppSettingListLoader get() {
        return newInstance(this.contextProvider.get(), this.microAppSettingRepositoryProvider.get());
    }
}
