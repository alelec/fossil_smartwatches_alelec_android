package com.portfolio.platform.data.source;

import com.fossil.Qq7;
import com.mapped.Cd6;
import com.mapped.Gg6;
import com.mapped.PagingRequestHelper;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$getSummariesPaging$Anon4 extends Qq7 implements Gg6<Cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryDataSourceFactory $sourceFactory;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSummaryRepository$getSummariesPaging$Anon4(HeartRateSummaryDataSourceFactory heartRateSummaryDataSourceFactory) {
        super(0);
        this.$sourceFactory = heartRateSummaryDataSourceFactory;
    }

    @DexIgnore
    @Override // com.mapped.Gg6
    public final void invoke() {
        PagingRequestHelper mHelper;
        HeartRateSummaryLocalDataSource e = this.$sourceFactory.getSourceLiveData().e();
        if (e != null && (mHelper = e.getMHelper()) != null) {
            mHelper.g();
        }
    }
}
