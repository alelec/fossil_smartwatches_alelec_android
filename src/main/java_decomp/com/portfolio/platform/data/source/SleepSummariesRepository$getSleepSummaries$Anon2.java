package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$2", f = "SleepSummariesRepository.kt", l = {180, 181}, m = "invokeSuspend")
public final class SleepSummariesRepository$getSleepSummaries$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<MFSleepDay>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ String $end;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ String $start;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$getSleepSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<MFSleepDay>, Ku3> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<Ku3>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSleepSummaries(k, k2, 0, 100, xe6);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object loadFromDb(Xe6<? super LiveData<List<MFSleepDay>>> xe6) {
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                String str = this.this$0.$start;
                Wg6.b(str, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
                String str2 = this.this$0.$end;
                Wg6.b(str2, "end");
                return sleepDao.getSleepDaysLiveData(str, str2);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(SleepSummariesRepository.TAG, "getActivityList onFetchFailed");
            }

            @DexIgnore
            public Object saveCallResult(Ku3 ku3, Xe6<? super Cd6> xe6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSummariesRepository.TAG;
                local.d(str, "getSleepSummaries saveCallResult onResponse: response = " + ku3);
                SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0.this$0;
                Object saveSleepSummaries = sleepSummariesRepository$getSleepSummaries$Anon2.this$0.saveSleepSummaries(ku3, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate, this.$downloadingDate, xe6);
                return saveSleepSummaries == Yn7.d() ? saveSleepSummaries : Cd6.a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(Ku3 ku3, Xe6 xe6) {
                return saveCallResult(ku3, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<MFSleepDay> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepDay> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2, SleepDatabase sleepDatabase, String str, String str2) {
            this.this$0 = sleepSummariesRepository$getSleepSummaries$Anon2;
            this.$sleepDatabase = sleepDatabase;
            this.$start = str;
            this.$end = str2;
        }

        @DexIgnore
        public final LiveData<H47<List<MFSleepDay>>> apply(List<FitnessDataWrapper> list) {
            Wg6.b(list, "fitnessDataList");
            SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, sleepSummariesRepository$getSleepSummaries$Anon2.$startDate, sleepSummariesRepository$getSleepSummaries$Anon2.$endDate)).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepSummaries$Anon2(SleepSummariesRepository sleepSummariesRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummariesRepository$getSleepSummaries$Anon2 sleepSummariesRepository$getSleepSummaries$Anon2 = new SleepSummariesRepository$getSleepSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, xe6);
        sleepSummariesRepository$getSleepSummaries$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSummariesRepository$getSleepSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<MFSleepDay>>>> xe6) {
        throw null;
        //return ((SleepSummariesRepository$getSleepSummaries$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00ca  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
            r9 = this;
            r8 = 0
            r7 = 2
            r6 = 1
            java.lang.Object r5 = com.fossil.Yn7.d()
            int r0 = r9.label
            if (r0 == 0) goto L_0x0075
            if (r0 == r6) goto L_0x0046
            if (r0 != r7) goto L_0x003e
            java.lang.Object r0 = r9.L$3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r9.L$2
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r9.L$1
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r3 = r9.L$0
            com.mapped.Il6 r3 = (com.mapped.Il6) r3
            com.fossil.El7.b(r10)
            r3 = r10
            r4 = r1
            r5 = r0
        L_0x0025:
            r0 = r3
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r1 = r5.getFitnessDataDao()
            java.util.Date r3 = r9.$startDate
            java.util.Date r5 = r9.$endDate
            androidx.lifecycle.LiveData r1 = r1.getFitnessDataLiveData(r3, r5)
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$Anon1_Level2 r3 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$Anon1_Level2
            r3.<init>(r9, r0, r2, r4)
            androidx.lifecycle.LiveData r0 = com.fossil.Ss0.c(r1, r3)
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r9.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r9.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r9.L$0
            com.mapped.Il6 r2 = (com.mapped.Il6) r2
            com.fossil.El7.b(r10)
            r4 = r0
            r3 = r10
        L_0x0057:
            r0 = r3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.fossil.Dv7 r3 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2 r6 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$sleepDatabase$Anon1_Level2
            r6.<init>(r8)
            r9.L$0 = r2
            r9.L$1 = r1
            r9.L$2 = r4
            r9.L$3 = r0
            r9.label = r7
            java.lang.Object r3 = com.fossil.Eu7.g(r3, r6, r9)
            if (r3 != r5) goto L_0x00ca
            r0 = r5
            goto L_0x003d
        L_0x0075:
            com.fossil.El7.b(r10)
            com.mapped.Il6 r2 = r9.p$
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.SleepSummariesRepository.access$getTAG$cp()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "getSleepSummaries: startDate = "
            r3.append(r4)
            java.util.Date r4 = r9.$startDate
            r3.append(r4)
            java.lang.String r4 = ", endDate = "
            r3.append(r4)
            java.util.Date r4 = r9.$endDate
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            r0.d(r1, r3)
            java.util.Date r0 = r9.$startDate
            java.lang.String r1 = com.mapped.TimeUtils.k(r0)
            java.util.Date r0 = r9.$endDate
            java.lang.String r0 = com.mapped.TimeUtils.k(r0)
            com.fossil.Dv7 r3 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2$fitnessDatabase$Anon1_Level2
            r4.<init>(r8)
            r9.L$0 = r2
            r9.L$1 = r1
            r9.L$2 = r0
            r9.label = r6
            java.lang.Object r3 = com.fossil.Eu7.g(r3, r4, r9)
            if (r3 != r5) goto L_0x00ce
            r0 = r5
            goto L_0x003d
        L_0x00ca:
            r2 = r1
            r5 = r0
            goto L_0x0025
        L_0x00ce:
            r4 = r0
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSummariesRepository$getSleepSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
