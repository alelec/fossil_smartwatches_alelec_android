package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.B05;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDao_Impl implements HybridPresetDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ B05 __hybridAppSettingTypeConverter; // = new B05();
    @DexIgnore
    public /* final */ Hh<HybridPreset> __insertionAdapterOfHybridPreset;
    @DexIgnore
    public /* final */ Hh<HybridRecommendPreset> __insertionAdapterOfHybridRecommendPreset;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllPresetTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllRecommendPresetTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveAllDeletePinTypePreset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<HybridRecommendPreset> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, HybridRecommendPreset hybridRecommendPreset) {
            if (hybridRecommendPreset.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, hybridRecommendPreset.getId());
            }
            if (hybridRecommendPreset.getName() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, hybridRecommendPreset.getName());
            }
            if (hybridRecommendPreset.getSerialNumber() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, hybridRecommendPreset.getSerialNumber());
            }
            String a2 = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridRecommendPreset.getButtons());
            if (a2 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a2);
            }
            mi.bindLong(5, hybridRecommendPreset.isDefault() ? 1 : 0);
            if (hybridRecommendPreset.getCreatedAt() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, hybridRecommendPreset.getCreatedAt());
            }
            if (hybridRecommendPreset.getUpdatedAt() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, hybridRecommendPreset.getUpdatedAt());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, HybridRecommendPreset hybridRecommendPreset) {
            bind(mi, hybridRecommendPreset);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridRecommendPreset` (`id`,`name`,`serialNumber`,`buttons`,`isDefault`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<HybridPreset> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, HybridPreset hybridPreset) {
            mi.bindLong(1, (long) hybridPreset.getPinType());
            if (hybridPreset.getCreatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, hybridPreset.getCreatedAt());
            }
            if (hybridPreset.getUpdatedAt() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, hybridPreset.getUpdatedAt());
            }
            if (hybridPreset.getId() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, hybridPreset.getId());
            }
            if (hybridPreset.getName() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, hybridPreset.getName());
            }
            if (hybridPreset.getSerialNumber() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, hybridPreset.getSerialNumber());
            }
            String a2 = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridPreset.getButtons());
            if (a2 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a2);
            }
            mi.bindLong(8, hybridPreset.isActive() ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, HybridPreset hybridPreset) {
            bind(mi, hybridPreset);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridPreset` (`pinType`,`createdAt`,`updatedAt`,`id`,`name`,`serialNumber`,`buttons`,`isActive`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM hybridPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM hybridRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Vh {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends Vh {
        @DexIgnore
        public Anon7(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon8(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HybridPreset> call() throws Exception {
            Cursor b = Ex0.b(HybridPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "createdAt");
                int c3 = Dx0.c(b, "updatedAt");
                int c4 = Dx0.c(b, "id");
                int c5 = Dx0.c(b, "name");
                int c6 = Dx0.c(b, "serialNumber");
                int c7 = Dx0.c(b, "buttons");
                int c8 = Dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                    hybridPreset.setPinType(b.getInt(c));
                    hybridPreset.setCreatedAt(b.getString(c2));
                    hybridPreset.setUpdatedAt(b.getString(c3));
                    arrayList.add(hybridPreset);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public HybridPresetDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfHybridRecommendPreset = new Anon1(oh);
        this.__insertionAdapterOfHybridPreset = new Anon2(oh);
        this.__preparedStmtOfClearAllPresetTable = new Anon3(oh);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon4(oh);
        this.__preparedStmtOfClearAllRecommendPresetTable = new Anon5(oh);
        this.__preparedStmtOfDeletePreset = new Anon6(oh);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void clearAllRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getActivePresetBySerial(String str) {
        HybridPreset hybridPreset = null;
        boolean z = true;
        Rh f = Rh.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "createdAt");
            int c3 = Dx0.c(b, "updatedAt");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "name");
            int c6 = Dx0.c(b, "serialNumber");
            int c7 = Dx0.c(b, "buttons");
            int c8 = Dx0.c(b, "isActive");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                ArrayList<HybridPresetAppSetting> b2 = this.__hybridAppSettingTypeConverter.b(b.getString(c7));
                if (b.getInt(c8) == 0) {
                    z = false;
                }
                hybridPreset = new HybridPreset(string, string2, string3, b2, z);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
            }
            return hybridPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPendingPreset(String str) {
        Rh f = Rh.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "createdAt");
            int c3 = Dx0.c(b, "updatedAt");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "name");
            int c6 = Dx0.c(b, "serialNumber");
            int c7 = Dx0.c(b, "buttons");
            int c8 = Dx0.c(b, "isActive");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridPreset> getAllPreset(String str) {
        Rh f = Rh.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "createdAt");
            int c3 = Dx0.c(b, "updatedAt");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "name");
            int c6 = Dx0.c(b, "serialNumber");
            int c7 = Dx0.c(b, "buttons");
            int c8 = Dx0.c(b, "isActive");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(b.getString(c4), b.getString(c5), b.getString(c6), this.__hybridAppSettingTypeConverter.b(b.getString(c7)), b.getInt(c8) != 0);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"hybridPreset"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public HybridPreset getPresetById(String str) {
        HybridPreset hybridPreset = null;
        boolean z = true;
        Rh f = Rh.f("SELECT * FROM hybridPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "createdAt");
            int c3 = Dx0.c(b, "updatedAt");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "name");
            int c6 = Dx0.c(b, "serialNumber");
            int c7 = Dx0.c(b, "buttons");
            int c8 = Dx0.c(b, "isActive");
            if (b.moveToFirst()) {
                String string = b.getString(c4);
                String string2 = b.getString(c5);
                String string3 = b.getString(c6);
                ArrayList<HybridPresetAppSetting> b2 = this.__hybridAppSettingTypeConverter.b(b.getString(c7));
                if (b.getInt(c8) == 0) {
                    z = false;
                }
                hybridPreset = new HybridPreset(string, string2, string3, b2, z);
                hybridPreset.setPinType(b.getInt(c));
                hybridPreset.setCreatedAt(b.getString(c2));
                hybridPreset.setUpdatedAt(b.getString(c3));
            }
            return hybridPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public List<HybridRecommendPreset> getRecommendPresetList(String str) {
        Rh f = Rh.f("SELECT * FROM hybridRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "name");
            int c3 = Dx0.c(b, "serialNumber");
            int c4 = Dx0.c(b, "buttons");
            int c5 = Dx0.c(b, "isDefault");
            int c6 = Dx0.c(b, "createdAt");
            int c7 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new HybridRecommendPreset(b.getString(c), b.getString(c2), b.getString(c3), this.__hybridAppSettingTypeConverter.b(b.getString(c4)), b.getInt(c5) != 0, b.getString(c6), b.getString(c7)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPreset(HybridPreset hybridPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert((Hh<HybridPreset>) hybridPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertPresetList(List<HybridPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.hybrid.microapp.HybridPresetDao
    public void upsertRecommendPresetList(List<HybridRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
