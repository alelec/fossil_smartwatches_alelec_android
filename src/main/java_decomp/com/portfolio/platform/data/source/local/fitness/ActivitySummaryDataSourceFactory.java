package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.helper.FitnessHelper;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDataSourceFactory extends Xe.Bi<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ U04 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ FitnessHelper fitnessHelper;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, FitnessHelper fitnessHelper2, FitnessDataRepository fitnessDataRepository2, FitnessDatabase fitnessDatabase2, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(summariesRepository2, "summariesRepository");
        Wg6.c(fitnessHelper2, "fitnessHelper");
        Wg6.c(fitnessDataRepository2, "fitnessDataRepository");
        Wg6.c(fitnessDatabase2, "fitnessDatabase");
        Wg6.c(date, "createdDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        Wg6.c(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = fitnessHelper2;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = u04;
        this.listener = ai;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Date, ActivitySummary> create() {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.localDataSource = activitySummaryLocalDataSource;
        this.sourceLiveData.l(activitySummaryLocalDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.localDataSource;
        if (activitySummaryLocalDataSource2 != null) {
            return activitySummaryLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
