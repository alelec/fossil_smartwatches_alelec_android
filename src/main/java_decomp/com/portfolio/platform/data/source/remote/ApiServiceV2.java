package com.portfolio.platform.data.source.remote;

import com.fossil.Bt4;
import com.fossil.Ca8;
import com.fossil.Dt4;
import com.fossil.G97;
import com.fossil.I98;
import com.fossil.It4;
import com.fossil.J98;
import com.fossil.Ks4;
import com.fossil.Mo5;
import com.fossil.Ms4;
import com.fossil.Mt4;
import com.fossil.No5;
import com.fossil.P77;
import com.fossil.P98;
import com.fossil.Ps4;
import com.fossil.Q88;
import com.fossil.S98;
import com.fossil.U98;
import com.fossil.W18;
import com.fossil.X98;
import com.fossil.Xs4;
import com.fossil.Y98;
import com.mapped.Ku3;
import com.mapped.Ny6;
import com.mapped.Py6;
import com.mapped.Uy6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.ServerFitnessDataWrapper;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.UserWrapper;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppData;
import com.portfolio.platform.data.model.diana.commutetime.TrafficRequest;
import com.portfolio.platform.data.model.diana.commutetime.TrafficResponse;
import com.portfolio.platform.data.model.diana.heartrate.HeartRate;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.model.room.sleep.SleepRecommendedGoal;
import com.portfolio.platform.data.model.setting.WatchLocalization;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceRing;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.label.Label;
import com.portfolio.platform.data.source.local.workoutsetting.RemoteWorkoutSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ApiServiceV2 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getDeviceAssets$default(ApiServiceV2 apiServiceV2, int i, int i2, String str, String str2, String str3, String str4, String str5, Xe6 xe6, int i3, Object obj) {
            if (obj == null) {
                return apiServiceV2.getDeviceAssets(i, i2, str, str2, str3, str4, (i3 & 64) != 0 ? null : str5, xe6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getDeviceAssets");
        }
    }

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/diana-presets")
    Object batchDeleteDianaPresetList(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/hybrid-presets")
    Object batchDeleteHybridPresetList(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/block-user")
    Object block(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/cancel-current-user-friend-request")
    Object cancelFriendRequest(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/users/me/challenges")
    Object createChallenge(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @J98("users/me/alarms/{id}")
    Object deleteAlarm(@X98("id") String str, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/alarms")
    Object deleteAlarms(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<Ku3>>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "/v2/users/me/diana-photos")
    Object deleteBackgroundPhotos(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @J98("/v2/users/me/challenges/{id}")
    Object deleteChallenge(@X98("id") String str, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "/v2/users/me/diana-watch-face-presets")
    Object deleteDianaPresets(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/goal-events")
    Object deleteGoalTrackingData(@I98 Ku3 ku3, Xe6<? super Q88<GoalEvent>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "/v2/users/me/notifications")
    Object deleteNotification(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @J98("users/me")
    Object deleteUser(Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/diana-faces")
    Object deleteUserDianaWatchFace(@I98 Ku3 ku3, Xe6<? super Q88<Object>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/workout-sessions")
    Object deleteWorkoutSession(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<ServerWorkoutSession>>> xe6);

    @DexIgnore
    @Ny6
    Object downloadFile(@Ca8 String str, Xe6<? super Q88<W18>> xe6);

    @DexIgnore
    @S98("/v2/users/me/challenges/{id}")
    Object editChallenge(@X98("id") String str, @I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("rpc/feature-flag/evaluate-flags")
    Object featureFlag(@P98("X-Active-Device") String str, @P98("User-Agent") String str2, @I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/list-available-challenges")
    Object fetchAvailableChallenges(Xe6<? super Q88<ApiResponse<Ps4>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/diana-photos")
    Object fetchBackgroundPhotos(Xe6<? super Q88<ApiResponse<G97>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/challenges")
    Object fetchChallengesWithStatus(@Y98("status") String[] strArr, @Y98("limit") Integer num, Xe6<? super Q88<ApiResponse<Ps4>>> xe6);

    @DexIgnore
    @Ny6("users/me/heart-rate-daily-summaries")
    Object fetchDailyHeartRateSummaries(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/diana-app-settings")
    Object fetchDianaAppSetting(Xe6<? super Q88<ApiResponse<DianaAppSetting>>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-watch-face-presets")
    Object fetchDianaPreset(@Y98("serialNumber") String str, @Y98("limit") int i, @Y98("offset") int i2, Xe6<? super Q88<ApiResponse<Mo5>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/challenge-histories")
    Object fetchHistoryChallenges(@Y98("limit") int i, @Y98("offset") int i2, Xe6<? super Q88<ApiResponse<Bt4>>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/list-pending-invitations")
    Object fetchPendingChallenges(Xe6<? super Q88<ApiResponse<Ps4>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/sent-friend-requests")
    Object fetchReceivedRequestFriends(@Y98("limit") Integer num, Xe6<? super Q88<ApiResponse<Xs4>>> xe6);

    @DexIgnore
    @Ny6("/v2/diana-recommended-watch-face-presets")
    Object fetchRecommendedDianaPreset(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<No5>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/received-friend-requests")
    Object fetchSentRequestFriends(@Y98("limit") Integer num, Xe6<? super Q88<ApiResponse<Xs4>>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-watch-face-editor-background")
    Object fetchWFBackgroundTemplates(@Y98("sortBy") String str, @Y98("limit") int i, Xe6<? super Q88<ApiResponse<P77>>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-watch-face-editor-sticker")
    Object fetchWFTicker(@Y98("limit") int i, Xe6<? super Q88<ApiResponse<P77>>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/social/search-user-social-profiles")
    Object findFriend(@Y98("keyword") String str, Xe6<? super Q88<ApiResponse<Xs4>>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/diana-watch-face-presets/generate-shareable-link")
    Object generateSharableLink(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/users/me/social-profile")
    Object generateSocialProfile(Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/activities")
    Object getActivities(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<Activity>>> xe6);

    @DexIgnore
    @Ny6("users/me/activity-settings")
    Object getActivitySetting(Xe6<? super Q88<ActivitySettings>> xe6);

    @DexIgnore
    @Ny6("users/me/activity-statistic")
    Object getActivityStatistic(Xe6<? super Q88<ActivityStatistic>> xe6);

    @DexIgnore
    @Ny6("users/me/alarms")
    Object getAlarms(@Y98("limit") int i, Xe6<? super Q88<ApiResponse<Alarm>>> xe6);

    @DexIgnore
    @Ny6("diana-complication-apps")
    Object getAllComplication(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<Complication>>> xe6);

    @DexIgnore
    @Ny6("hybrid-apps")
    Object getAllMicroApp(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<MicroApp>>> xe6);

    @DexIgnore
    @Ny6("hybrid-app-variants")
    Object getAllMicroAppVariant(@Y98("serialNumber") String str, @Y98("majorNumber") String str2, @Y98("minorNumber") String str3, Xe6<? super Q88<ApiResponse<MicroAppVariant>>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-faces")
    Object getAllUserDianaWatchFace(@Y98("limit") int i, @Y98("offset") int i2, Xe6<? super Q88<ApiResponse<DianaWatchFaceUser>>> xe6);

    @DexIgnore
    @Ny6("diana-pusher-apps")
    Object getAllWatchApp(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<WatchApp>>> xe6);

    @DexIgnore
    @Ny6("diana-watch-apps")
    Object getAllWatchAppData(@Y98("appVersion") String str, @Y98("firmwareOSVersion") String str2, Xe6<? super Q88<ApiResponse<WatchAppData>>> xe6);

    @DexIgnore
    @Ny6("app-categories")
    Object getCategories(Xe6<? super Q88<ApiResponse<Category>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/challenges/{id}")
    Object getChallengeById(@X98("id") String str, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/heart-rate-daily-summaries")
    Object getDailyHeartRateSummaries(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<Ku3>>> xe6);

    @DexIgnore
    @Ny6("assets/app-sku-images")
    Object getDeviceAssets(@Y98("size") int i, @Y98("offset") int i2, @Y98("metadata.serialNumber") String str, @Y98("metadata.feature") String str2, @Y98("metadata.resolution") String str3, @Y98("metadata.platform") String str4, @Y98("metadata.fastPairId") String str5, Xe6<? super Q88<ApiResponse<Ku3>>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-complication-ringstyles")
    Object getDianaComplicationRingStyles(@Y98("metadata.isDefault") boolean z, @Y98("metadata.serialNumber") String str, Xe6<? super Q88<ApiResponse<DianaComplicationRingStyle>>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-presets")
    Object getDianaPresetList(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @Ny6("diana-recommended-presets")
    Object getDianaRecommendPresetList(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<DianaRecommendPreset>>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-watch-face-presets/{id}")
    Object getDianaWatchFacePreset(@X98("id") String str, Xe6<? super Q88<ApiResponse<Mo5>>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-watch-face-editor-ring-style")
    Object getDianaWatchFaceRings(@Y98("sortBy") String str, Xe6<? super Q88<ApiResponse<DianaWatchFaceRing>>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/display-players")
    Object getDisplayPlayers(@Y98("challengeIds") List<String> list, Xe6<? super Q88<ApiResponse<Ks4>>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/list-compact-players-current-user-challenges")
    Object getFocusedPlayers(@Y98("challengeIds") List<String> list, @Y98("numberOfTopPlayers") int i, @Y98("numberOfNearPlayers") int i2, Xe6<? super Q88<ApiResponse<Ks4>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/friends")
    Object getFriends(@Y98("status") String str, Xe6<? super Q88<ApiResponse<Xs4>>> xe6);

    @DexIgnore
    @Ny6("users/me/goal-settings")
    Object getGoalSetting(Xe6<? super Q88<GoalSetting>> xe6);

    @DexIgnore
    @Ny6("users/me/goal-events")
    Object getGoalTrackingDataList(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<GoalEvent>>> xe6);

    @DexIgnore
    @Ny6("users/me/goal-daily-summaries")
    Object getGoalTrackingSummaries(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<GoalDailySummary>>> xe6);

    @DexIgnore
    @Ny6("users/me/goal-daily-summaries/{date}")
    Object getGoalTrackingSummary(@X98("date") String str, Xe6<? super Q88<GoalDailySummary>> xe6);

    @DexIgnore
    @Ny6("users/me/heart-rates")
    Object getHeartRateSamples(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<HeartRate>>> xe6);

    @DexIgnore
    @Ny6("users/me/hybrid-presets")
    Object getHybridPresetList(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<HybridPreset>>> xe6);

    @DexIgnore
    @Ny6("hybrid-recommended-presets")
    Object getHybridRecommendPresetList(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<HybridRecommendPreset>>> xe6);

    @DexIgnore
    @Ny6("assets/e-label")
    Object getLabel(@Y98("metadata.serialNumber") String str, Xe6<? super Q88<ApiResponse<Label>>> xe6);

    @DexIgnore
    @Ny6("assets/watch-params")
    Object getLatestWatchParams(@Y98("metadata.serialNumber") String str, @Y98("metadata.version.major") int i, @Y98("sortBy") String str2, @Y98("offset") int i2, @Y98("limit") int i3, Xe6<? super Q88<ApiResponse<WatchParameterResponse>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/social-profile")
    Object getMySocialProfile(Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/challenges/{id}/pending-players")
    Object getPendingPlayers(@X98("id") String str, @Y98("limit") Integer num, Xe6<? super Q88<ApiResponse<Xs4>>> xe6);

    @DexIgnore
    @Ny6("rpc/activity/get-recommended-goals")
    Object getRecommendedGoalsRaw(@Y98("age") int i, @Y98("weightInGrams") int i2, @Y98("heightInCentimeters") int i3, @Y98("gender") String str, Xe6<? super Q88<ActivityRecommendedGoals>> xe6);

    @DexIgnore
    @Ny6("rpc/sleep/get-recommended-goals")
    Object getRecommendedSleepGoalRaw(@Y98("age") int i, @Y98("weightInGrams") int i2, @Y98("heightInCentimeters") int i3, @Y98("gender") String str, Xe6<? super Q88<SleepRecommendedGoal>> xe6);

    @DexIgnore
    @Ny6("server-settings")
    Object getServerSettingList(@Y98("limit") int i, @Y98("offset") int i2, Xe6<? super Q88<ServerSettingList>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/diana-watch-face-presets/get-sharing-face")
    Object getSharingFace(@Y98("id") String str, @Y98("token") String str2, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/sleep-sessions")
    Object getSleepSessions(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/sleep-settings")
    Object getSleepSetting(Xe6<? super Q88<MFSleepSettings>> xe6);

    @DexIgnore
    @Ny6("users/me/sleep-statistic")
    Object getSleepStatistic(Xe6<? super Q88<SleepStatistic>> xe6);

    @DexIgnore
    @Ny6("users/me/sleep-daily-summaries")
    Object getSleepSummaries(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/activity-daily-summaries")
    Object getSummaries(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/get-current-user-sync-status-data")
    Object getSyncStatusData(Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("rpc/commute-time/calc-traffic-status")
    Object getTrafficStatus(@I98 TrafficRequest trafficRequest, Xe6<? super Q88<TrafficResponse>> xe6);

    @DexIgnore
    @Ny6("users/me/profile")
    Object getUser(Xe6<? super Q88<UserWrapper>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-faces/{id}")
    Object getUserDianaWatchFace(@X98("id") String str, Xe6<? super Q88<DianaWatchFaceUser>> xe6);

    @DexIgnore
    @Ny6("users/me/diana-faces")
    Object getUserDianaWatchFaceWithOrderId(@Y98("orderId") String str, Xe6<? super Q88<ApiResponse<DianaWatchFaceUser>>> xe6);

    @DexIgnore
    @Ny6("users/me/settings")
    Object getUserSettings(Xe6<? super Q88<UserSettings>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-watch-face-editor-ring-style")
    Object getWFRings(@Y98("sortBy") String str, @Y98("limit") int i, Xe6<? super Q88<ApiResponse<P77>>> xe6);

    @DexIgnore
    @Ny6("/v2/diana-watch-faces")
    Object getWatchFaces(@Y98("serialNumber") String str, Xe6<? super Q88<ApiResponse<WatchFace>>> xe6);

    @DexIgnore
    @Ny6("/v2/assets/diana-watch-localizations")
    Object getWatchLocalizationData(@Y98("metadata.locale") String str, Xe6<? super Q88<ApiResponse<WatchLocalization>>> xe6);

    @DexIgnore
    @Ny6("weather-info")
    Object getWeather(@Y98("lat") String str, @Y98("lng") String str2, @Y98("temperatureUnit") String str3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/workout-sessions")
    Object getWorkoutSessions(@Y98("startDate") String str, @Y98("endDate") String str2, @Y98("offset") int i, @Y98("limit") int i2, Xe6<? super Q88<ApiResponse<ServerWorkoutSession>>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/workout-settings")
    Object getWorkoutSettingList(Xe6<? super Q88<RemoteWorkoutSetting>> xe6);

    @DexIgnore
    @Uy6("users/me/activities")
    Object insertActivities(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<Activity>>> xe6);

    @DexIgnore
    @Uy6("users/me/fitness-files")
    Object insertFitnessDataFiles(@I98 ApiResponse<ServerFitnessDataWrapper> apiResponse, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("users/me/goal-events")
    Object insertGoalTrackingDataList(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @U98("users/me/installations")
    Object insertInstallation(@I98 Installation installation, Xe6<? super Q88<Installation>> xe6);

    @DexIgnore
    @Uy6("users/me/sleep-sessions")
    Object insertSleepSessions(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/challenges/{id}/players")
    Object invitedPlayers(@X98("id") String str, @Y98("status") String[] strArr, Xe6<? super Q88<ApiResponse<Ms4>>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/join-challenge")
    Object joinChallenge(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/leave-challenge")
    Object leaveChallenge(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/users/me/notifications")
    Object notifications(@Y98("limit") Integer num, Xe6<? super Q88<ApiResponse<Dt4>>> xe6);

    @DexIgnore
    @S98("/v2/users/me/diana-app-settings")
    Object patchDianaAppSetting(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<DianaAppSetting>>> xe6);

    @DexIgnore
    @Uy6("users/me/notification-clients")
    Object pushFCMToken(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/player/push-current-user-step-data")
    Object pushStepData(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("/v2/rpc/challenges/list-pending-or-available-challenges")
    Object recommendedChallenges(Xe6<? super Q88<ApiResponse<Mt4>>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/rematch-challenge")
    Object rematchChallenge(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Py6(hasBody = true, method = "DELETE", path = "users/me/notification-clients")
    Object removeFCMToken(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @U98("users/me/diana-presets")
    Object replaceDianaPresetList(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @U98("users/me/hybrid-presets")
    Object replaceHybridPresetList(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<HybridPreset>>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/update-current-user-friend-request")
    Object respondFriendRequest(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/respond-invitation")
    Object respondInvitation(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/invite")
    Object sendInvitation(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/add-current-user-friend")
    Object sendRequest(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @S98("users/me/sleep-settings")
    Object setSleepSetting(@I98 Ku3 ku3, Xe6<? super Q88<MFSleepSettings>> xe6);

    @DexIgnore
    @U98("/v2/users/me/social-profile")
    Object socialId(@I98 Ku3 ku3, Xe6<? super Q88<It4>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/challenges/start-challenge")
    Object startChallenge(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/unfriend-current-user")
    Object unFriend(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("/v2/rpc/social/friend/unblock-user")
    Object unblock(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @S98("users/me/activity-settings")
    Object updateActivitySetting(@I98 Ku3 ku3, Xe6<? super Q88<ActivitySettings>> xe6);

    @DexIgnore
    @S98("users/me/profile")
    Object updateUser(@I98 Ku3 ku3, Xe6<? super Q88<UserWrapper>> xe6);

    @DexIgnore
    @S98("users/me/settings")
    Object updateUserSetting(@I98 Ku3 ku3, Xe6<? super Q88<UserSettings>> xe6);

    @DexIgnore
    @S98("users/me/workout-sessions")
    Object updateWorkoutSessions(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<ServerWorkoutSession>>> xe6);

    @DexIgnore
    @S98("users/me/alarms")
    Object upsertAlarms(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<Alarm>>> xe6);

    @DexIgnore
    @S98("/v2/users/me/diana-photos")
    Object upsertBackgroundPhotos(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<G97>>> xe6);

    @DexIgnore
    @S98("users/me/diana-presets")
    Object upsertDianaPresetList(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<DianaPreset>>> xe6);

    @DexIgnore
    @S98("users/me/goal-settings")
    Object upsertGoalSetting(@I98 Ku3 ku3, Xe6<? super Q88<GoalSetting>> xe6);

    @DexIgnore
    @S98("users/me/hybrid-presets")
    Object upsertHybridPresetList(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<HybridPreset>>> xe6);

    @DexIgnore
    @U98("/v2/users/me/workout-settings")
    Object upsertWorkoutSetting(@I98 Ku3 ku3, Xe6<? super Q88<RemoteWorkoutSetting>> xe6);
}
