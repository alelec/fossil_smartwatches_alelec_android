package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RingStyleRemoteDataSource_Factory implements Factory<RingStyleRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mServiceProvider;

    @DexIgnore
    public RingStyleRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mServiceProvider = provider;
    }

    @DexIgnore
    public static RingStyleRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new RingStyleRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static RingStyleRemoteDataSource newInstance(ApiServiceV2 apiServiceV2) {
        return new RingStyleRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public RingStyleRemoteDataSource get() {
        return newInstance(this.mServiceProvider.get());
    }
}
