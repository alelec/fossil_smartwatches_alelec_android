package com.portfolio.platform.data.source;

import com.fossil.Dl7;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.mapped.Cd6;
import com.mapped.Hh6;
import com.mapped.Lc3;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2 implements Lc3 {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeDeviceSerial$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Hh6 $countSizeOfList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ List $gFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ GoogleSignInAccount $googleSignInAccount$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ int $sizeOfGFitWorkoutSessionList$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ThirdPartyRepository this$0;

    @DexIgnore
    public ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(GoogleSignInAccount googleSignInAccount, Hh6 hh6, int i, Lk6 lk6, ThirdPartyRepository thirdPartyRepository, List list, String str) {
        this.$googleSignInAccount$inlined = googleSignInAccount;
        this.$countSizeOfList$inlined = hh6;
        this.$sizeOfGFitWorkoutSessionList$inlined = i;
        this.$continuation$inlined = lk6;
        this.this$0 = thirdPartyRepository;
        this.$gFitWorkoutSessionList$inlined = list;
        this.$activeDeviceSerial$inlined = str;
    }

    @DexIgnore
    @Override // com.mapped.Lc3
    public final void onFailure(Exception exc) {
        Wg6.c(exc, "it");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("There was a problem inserting the session: ");
        exc.printStackTrace();
        sb.append(Cd6.a);
        local.e(ThirdPartyRepository.TAG, sb.toString());
        Hh6 hh6 = this.$countSizeOfList$inlined;
        int i = hh6.element + 1;
        hh6.element = i;
        if (i >= this.$sizeOfGFitWorkoutSessionList$inlined && this.$continuation$inlined.isActive()) {
            FLogger.INSTANCE.getLocal().d(ThirdPartyRepository.TAG, "End saveGFitWorkoutSessionToGoogleFit");
            Lk6 lk6 = this.$continuation$inlined;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(null));
        }
    }
}
