package com.portfolio.platform.data.source;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.Device;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDao_Impl implements DeviceDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<Device> __insertionAdapterOfDevice;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveDeviceByDeviceId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Device> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Device device) {
            mi.bindLong(1, (long) device.getMajor());
            mi.bindLong(2, (long) device.getMinor());
            if (device.getCreatedAt() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, device.getCreatedAt());
            }
            if (device.getUpdatedAt() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, device.getUpdatedAt());
            }
            if (device.getOwner() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, device.getOwner());
            }
            if (device.getProductDisplayName() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, device.getProductDisplayName());
            }
            if (device.getManufacturer() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, device.getManufacturer());
            }
            if (device.getSoftwareRevision() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, device.getSoftwareRevision());
            }
            if (device.getHardwareRevision() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, device.getHardwareRevision());
            }
            if (device.getActivationDate() == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, device.getActivationDate());
            }
            if (device.getDeviceId() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, device.getDeviceId());
            }
            if (device.getMacAddress() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, device.getMacAddress());
            }
            if (device.getSku() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, device.getSku());
            }
            if (device.getFirmwareRevision() == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, device.getFirmwareRevision());
            }
            mi.bindLong(15, (long) device.getBatteryLevel());
            if (device.getVibrationStrength() == null) {
                mi.bindNull(16);
            } else {
                mi.bindLong(16, (long) device.getVibrationStrength().intValue());
            }
            mi.bindLong(17, device.isActive() ? 1 : 0);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Device device) {
            bind(mi, device);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `device` (`major`,`minor`,`createdAt`,`updatedAt`,`owner`,`productDisplayName`,`manufacturer`,`softwareRevision`,`hardwareRevision`,`activationDate`,`deviceId`,`macAddress`,`sku`,`firmwareRevision`,`batteryLevel`,`vibrationStrength`,`isActive`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM device WHERE deviceId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM device";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<Device>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<Device> call() throws Exception {
            Cursor b = Ex0.b(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "major");
                int c2 = Dx0.c(b, "minor");
                int c3 = Dx0.c(b, "createdAt");
                int c4 = Dx0.c(b, "updatedAt");
                int c5 = Dx0.c(b, "owner");
                int c6 = Dx0.c(b, "productDisplayName");
                int c7 = Dx0.c(b, "manufacturer");
                int c8 = Dx0.c(b, "softwareRevision");
                int c9 = Dx0.c(b, "hardwareRevision");
                int c10 = Dx0.c(b, "activationDate");
                int c11 = Dx0.c(b, "deviceId");
                int c12 = Dx0.c(b, "macAddress");
                int c13 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int c14 = Dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int c15 = Dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = Dx0.c(b, "vibrationStrength");
                int c17 = Dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    Device device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                    arrayList.add(device);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 implements Callable<Device> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon5(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public Device call() throws Exception {
            Device device;
            Cursor b = Ex0.b(DeviceDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "major");
                int c2 = Dx0.c(b, "minor");
                int c3 = Dx0.c(b, "createdAt");
                int c4 = Dx0.c(b, "updatedAt");
                int c5 = Dx0.c(b, "owner");
                int c6 = Dx0.c(b, "productDisplayName");
                int c7 = Dx0.c(b, "manufacturer");
                int c8 = Dx0.c(b, "softwareRevision");
                int c9 = Dx0.c(b, "hardwareRevision");
                int c10 = Dx0.c(b, "activationDate");
                int c11 = Dx0.c(b, "deviceId");
                int c12 = Dx0.c(b, "macAddress");
                int c13 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
                int c14 = Dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
                int c15 = Dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = Dx0.c(b, "vibrationStrength");
                int c17 = Dx0.c(b, "isActive");
                if (b.moveToFirst()) {
                    device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                } else {
                    device = null;
                }
                return device;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DeviceDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDevice = new Anon1(oh);
        this.__preparedStmtOfRemoveDeviceByDeviceId = new Anon2(oh);
        this.__preparedStmtOfCleanUp = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addAllDevice(List<Device> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void addOrUpdateDevice(Device device) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDevice.insert((Hh<Device>) device);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public List<Device> getAllDevice() {
        Rh f = Rh.f("SELECT * FROM device", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "major");
            int c2 = Dx0.c(b, "minor");
            int c3 = Dx0.c(b, "createdAt");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "owner");
            int c6 = Dx0.c(b, "productDisplayName");
            int c7 = Dx0.c(b, "manufacturer");
            int c8 = Dx0.c(b, "softwareRevision");
            int c9 = Dx0.c(b, "hardwareRevision");
            int c10 = Dx0.c(b, "activationDate");
            int c11 = Dx0.c(b, "deviceId");
            int c12 = Dx0.c(b, "macAddress");
            int c13 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c14 = Dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            try {
                int c15 = Dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = Dx0.c(b, "vibrationStrength");
                int c17 = Dx0.c(b, "isActive");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    Device device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                    arrayList.add(device);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<List<Device>> getAllDeviceAsLiveData() {
        Rh f = Rh.f("SELECT * FROM device", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"device"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public Device getDeviceByDeviceId(String str) {
        Throwable th;
        Device device;
        Rh f = Rh.f("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "major");
            int c2 = Dx0.c(b, "minor");
            int c3 = Dx0.c(b, "createdAt");
            int c4 = Dx0.c(b, "updatedAt");
            int c5 = Dx0.c(b, "owner");
            int c6 = Dx0.c(b, "productDisplayName");
            int c7 = Dx0.c(b, "manufacturer");
            int c8 = Dx0.c(b, "softwareRevision");
            int c9 = Dx0.c(b, "hardwareRevision");
            int c10 = Dx0.c(b, "activationDate");
            int c11 = Dx0.c(b, "deviceId");
            int c12 = Dx0.c(b, "macAddress");
            int c13 = Dx0.c(b, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int c14 = Dx0.c(b, LegacyDeviceModel.COLUMN_FIRMWARE_VERSION);
            try {
                int c15 = Dx0.c(b, LegacyDeviceModel.COLUMN_BATTERY_LEVEL);
                int c16 = Dx0.c(b, "vibrationStrength");
                int c17 = Dx0.c(b, "isActive");
                if (b.moveToFirst()) {
                    device = new Device(b.getString(c11), b.getString(c12), b.getString(c13), b.getString(c14), b.getInt(c15), b.isNull(c16) ? null : Integer.valueOf(b.getInt(c16)), b.getInt(c17) != 0);
                    device.setMajor(b.getInt(c));
                    device.setMinor(b.getInt(c2));
                    device.setCreatedAt(b.getString(c3));
                    device.setUpdatedAt(b.getString(c4));
                    device.setOwner(b.getString(c5));
                    device.setProductDisplayName(b.getString(c6));
                    device.setManufacturer(b.getString(c7));
                    device.setSoftwareRevision(b.getString(c8));
                    device.setHardwareRevision(b.getString(c9));
                    device.setActivationDate(b.getString(c10));
                } else {
                    device = null;
                }
                b.close();
                f.m();
                return device;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM device WHERE deviceId=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon5 anon5 = new Anon5(f);
        return invalidationTracker.d(new String[]{"device"}, false, anon5);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.DeviceDao
    public void removeDeviceByDeviceId(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveDeviceByDeviceId.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveDeviceByDeviceId.release(acquire);
        }
    }
}
