package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$findNextActiveAlarm$2", f = "AlarmsRepository.kt", l = {34}, m = "invokeSuspend")
public final class AlarmsRepository$findNextActiveAlarm$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Alarm>, Object> {
    @DexIgnore
    public int I$0;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    public AlarmsRepository$findNextActiveAlarm$Anon2(Xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        AlarmsRepository$findNextActiveAlarm$Anon2 alarmsRepository$findNextActiveAlarm$Anon2 = new AlarmsRepository$findNextActiveAlarm$Anon2(xe6);
        alarmsRepository$findNextActiveAlarm$Anon2.p$ = (Il6) obj;
        throw null;
        //return alarmsRepository$findNextActiveAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Alarm> xe6) {
        throw null;
        //return ((AlarmsRepository$findNextActiveAlarm$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        int i;
        Object u;
        Object d = Yn7.d();
        int i2 = this.label;
        if (i2 == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            Calendar instance = Calendar.getInstance();
            i = (instance.get(11) * 60) + instance.get(12);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.L$1 = instance;
            this.I$0 = i;
            this.label = 1;
            u = encryptedDatabaseManager.u(this);
            if (u == d) {
                return d;
            }
        } else if (i2 == 1) {
            i = this.I$0;
            Calendar calendar = (Calendar) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            u = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((AlarmDatabase) u).alarmDao().getInComingActiveAlarm(i);
    }
}
