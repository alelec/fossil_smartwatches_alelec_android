package com.portfolio.platform.data.source.remote;

import com.fossil.I98;
import com.fossil.Q88;
import com.mapped.Ku3;
import com.mapped.Uy6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AuthApiGuestService {
    @DexIgnore
    @Uy6("rpc/auth/check-account-existence-by-email")
    Object checkAuthenticationEmailExisting(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/check-account-existence-by-social")
    Object checkAuthenticationSocialExisting(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/login")
    Object loginWithEmail(@I98 Ku3 ku3, Xe6<? super Q88<Auth>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/login-with")
    Object loginWithSocial(@I98 Ku3 ku3, Xe6<? super Q88<Auth>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/password/request-reset")
    Object passwordRequestReset(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/register")
    Object registerEmail(@I98 SignUpEmailAuth signUpEmailAuth, Xe6<? super Q88<Auth>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/register-with")
    Object registerSocial(@I98 SignUpSocialAuth signUpSocialAuth, Xe6<? super Q88<Auth>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/request-email-otp")
    Object requestEmailOtp(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Uy6("rpc/auth/token/exchange-legacy")
    Call<Auth> tokenExchangeLegacy(@I98 Ku3 ku3);

    @DexIgnore
    @Uy6("rpc/auth/token/refresh")
    Call<Auth> tokenRefresh(@I98 Ku3 ku3);

    @DexIgnore
    @Uy6("rpc/auth/verify-email-otp")
    Object verifyEmailOtp(@I98 Ku3 ku3, Xe6<? super Q88<Void>> xe6);
}
