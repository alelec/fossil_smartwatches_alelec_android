package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nz4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.misfit.frameworks.buttonservice.model.Alarm;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDao_Impl implements AlarmDao {
    @DexIgnore
    public /* final */ Nz4 __alarmConverter; // = new Nz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<Alarm> __insertionAdapterOfAlarm;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveAlarm;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<Alarm> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, Alarm alarm) {
            if (alarm.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, alarm.getId());
            }
            if (alarm.getUri() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, alarm.getUri());
            }
            if (alarm.getTitle() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, alarm.getTitle());
            }
            if (alarm.getMessage() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, alarm.getMessage());
            }
            mi.bindLong(5, (long) alarm.getHour());
            mi.bindLong(6, (long) alarm.getMinute());
            String a2 = AlarmDao_Impl.this.__alarmConverter.a(alarm.getDays());
            if (a2 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a2);
            }
            mi.bindLong(8, alarm.isActive() ? 1 : 0);
            mi.bindLong(9, alarm.isRepeated() ? 1 : 0);
            if (alarm.getCreatedAt() == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, alarm.getCreatedAt());
            }
            if (alarm.getUpdatedAt() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, alarm.getUpdatedAt());
            }
            mi.bindLong(12, (long) alarm.getPinType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, Alarm alarm) {
            bind(mi, alarm);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `alarm` (`id`,`uri`,`title`,`message`,`hour`,`minute`,`days`,`isActive`,`isRepeated`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM alarm WHERE uri =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM alarm";
        }
    }

    @DexIgnore
    public AlarmDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfAlarm = new Anon1(oh);
        this.__preparedStmtOfRemoveAlarm = new Anon2(oh);
        this.__preparedStmtOfCleanUp = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getActiveAlarms() {
        Rh f = Rh.f("SELECT *FROM alarm WHERE isActive = 1 and pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getAlarmWithUri(String str) {
        Rh f = Rh.f("SELECT * FROM alarm WHERE uri =?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Alarm alarm = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            if (b.moveToFirst()) {
                alarm = new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12));
            }
            return alarm;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarms() {
        Rh f = Rh.f("SELECT * FROM alarm", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getAlarmsIgnoreDeleted() {
        Rh f = Rh.f("SELECT * FROM alarm WHERE pinType <> 3 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Alarm getInComingActiveAlarm(int i) {
        Rh f = Rh.f("SELECT * FROM alarm WHERE isActive = 1 and minute + hour * 60 > ? ORDER BY hour, minute ASC LIMIT 1", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Alarm alarm = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            if (b.moveToFirst()) {
                alarm = new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12));
            }
            return alarm;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getInComingActiveAlarms() {
        Rh f = Rh.f("SELECT * FROM alarm WHERE isActive = 1 and pinType <> 3 ORDER BY hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public List<Alarm> getPendingAlarms() {
        Rh f = Rh.f("SELECT*FROM alarm where pinType <> 0 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "uri");
            int c3 = Dx0.c(b, "title");
            int c4 = Dx0.c(b, "message");
            int c5 = Dx0.c(b, AppFilter.COLUMN_HOUR);
            int c6 = Dx0.c(b, MFSleepGoal.COLUMN_MINUTE);
            int c7 = Dx0.c(b, Alarm.COLUMN_DAYS);
            int c8 = Dx0.c(b, "isActive");
            int c9 = Dx0.c(b, "isRepeated");
            int c10 = Dx0.c(b, "createdAt");
            int c11 = Dx0.c(b, "updatedAt");
            int c12 = Dx0.c(b, "pinType");
            try {
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new Alarm(b.getString(c), b.getString(c2), b.getString(c3), b.getString(c4), b.getInt(c5), b.getInt(c6), this.__alarmConverter.b(b.getString(c7)), b.getInt(c8) != 0, b.getInt(c9) != 0, b.getString(c10), b.getString(c11), b.getInt(c12)));
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public long insertAlarm(Alarm alarm) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfAlarm.insertAndReturnId(alarm);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public Long[] insertAlarms(List<Alarm> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfAlarm.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.alarm.AlarmDao
    public int removeAlarm(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveAlarm.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            int executeUpdateDelete = acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
            return executeUpdateDelete;
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAlarm.release(acquire);
        }
    }
}
