package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.UserRepository$verifyEmailOtp$2", f = "UserRepository.kt", l = {281}, m = "invokeSuspend")
public final class UserRepository$verifyEmailOtp$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Void>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $emailAddress;
    @DexIgnore
    public /* final */ /* synthetic */ String $otpCode;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRepository$verifyEmailOtp$Anon2(UserRepository userRepository, String str, String str2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = userRepository;
        this.$emailAddress = str;
        this.$otpCode = str2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        UserRepository$verifyEmailOtp$Anon2 userRepository$verifyEmailOtp$Anon2 = new UserRepository$verifyEmailOtp$Anon2(this.this$0, this.$emailAddress, this.$otpCode, xe6);
        userRepository$verifyEmailOtp$Anon2.p$ = (Il6) obj;
        throw null;
        //return userRepository$verifyEmailOtp$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Void>> xe6) {
        throw null;
        //return ((UserRepository$verifyEmailOtp$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            UserRemoteDataSource userRemoteDataSource = this.this$0.mUserRemoteDataSource;
            String str = this.$emailAddress;
            String str2 = this.$otpCode;
            this.L$0 = il6;
            this.label = 1;
            Object verifyEmailOtp = userRemoteDataSource.verifyEmailOtp(str, str2, this);
            return verifyEmailOtp == d ? d : verifyEmailOtp;
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
