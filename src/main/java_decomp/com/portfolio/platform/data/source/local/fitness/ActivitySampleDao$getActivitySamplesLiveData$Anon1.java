package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.Im7;
import com.fossil.Ss0;
import com.mapped.V3;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySampleDao$getActivitySamplesLiveData$Anon1<I, O> implements V3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySampleDao this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, Y> {
        @DexIgnore
        public /* final */ /* synthetic */ List $activitySamples;

        @DexIgnore
        public Anon1_Level2(List list) {
            this.$activitySamples = list;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }

        @DexIgnore
        public final List<ActivitySample> apply(List<SampleRaw> list) {
            List list2 = this.$activitySamples;
            Wg6.b(list2, "activitySamples");
            if ((!list2.isEmpty()) || list.isEmpty()) {
                return this.$activitySamples;
            }
            this.$activitySamples.clear();
            List list3 = this.$activitySamples;
            Wg6.b(list, "samplesRaw");
            ArrayList arrayList = new ArrayList(Im7.m(list, 10));
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                arrayList.add(it.next().toActivitySample());
            }
            list3.addAll(arrayList);
            return this.$activitySamples;
        }
    }

    @DexIgnore
    public ActivitySampleDao$getActivitySamplesLiveData$Anon1(ActivitySampleDao activitySampleDao, Date date, Date date2) {
        this.this$0 = activitySampleDao;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    public final LiveData<List<ActivitySample>> apply(List<ActivitySample> list) {
        return Ss0.b(this.this$0.getActivitySamplesLiveDataV1(this.$startDate, this.$endDate), new Anon1_Level2(list));
    }

    @DexIgnore
    @Override // com.mapped.V3
    public /* bridge */ /* synthetic */ Object apply(Object obj) {
        return apply((List) obj);
    }
}
