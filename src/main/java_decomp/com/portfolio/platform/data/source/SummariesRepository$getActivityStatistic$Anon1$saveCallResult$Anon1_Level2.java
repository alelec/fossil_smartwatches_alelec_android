package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Cd6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getActivityStatistic$1", f = "SummariesRepository.kt", l = {MFNetworkReturnCode.CONTENT_TYPE_ERROR}, m = "saveCallResult")
public final class SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getActivityStatistic$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2(SummariesRepository$getActivityStatistic$Anon1 summariesRepository$getActivityStatistic$Anon1, Xe6 xe6) {
        super(xe6);
        this.this$0 = summariesRepository$getActivityStatistic$Anon1;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.saveCallResult((ActivityStatistic) null, (Xe6<? super Cd6>) this);
    }
}
