package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory implements Factory<NotificationsDataSource> {
    @DexIgnore
    public /* final */ NotificationsRepositoryModule module;

    @DexIgnore
    public NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory(NotificationsRepositoryModule notificationsRepositoryModule) {
        this.module = notificationsRepositoryModule;
    }

    @DexIgnore
    public static NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory create(NotificationsRepositoryModule notificationsRepositoryModule) {
        return new NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory(notificationsRepositoryModule);
    }

    @DexIgnore
    public static NotificationsDataSource provideRemoteNotificationsDataSource(NotificationsRepositoryModule notificationsRepositoryModule) {
        NotificationsDataSource provideRemoteNotificationsDataSource = notificationsRepositoryModule.provideRemoteNotificationsDataSource();
        Lk7.c(provideRemoteNotificationsDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideRemoteNotificationsDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationsDataSource get() {
        return provideRemoteNotificationsDataSource(this.module);
    }
}
