package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$getSleepStatistic$2", f = "SleepSummariesRepository.kt", l = {360}, m = "loadFromDb")
public final class SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository$getSleepStatistic$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatistic$Anon2$loadFromDb$Anon1_Level2(SleepSummariesRepository$getSleepStatistic$Anon2 sleepSummariesRepository$getSleepStatistic$Anon2, Xe6 xe6) {
        super(xe6);
        this.this$0 = sleepSummariesRepository$getSleepStatistic$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.loadFromDb(this);
    }
}
