package com.portfolio.platform.data.source.remote;

import com.mapped.Fu3;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import java.util.Calendar;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRemoteDataSource.class.getSimpleName();
        Wg6.b(simpleName, "AlarmsRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final Fu3 intArray2JsonArray(int[] iArr) {
        Fu3 fu3 = new Fu3();
        if (iArr == null) {
            iArr = new int[0];
        }
        Calendar instance = Calendar.getInstance();
        for (int i : iArr) {
            instance.set(7, i);
            String displayName = instance.getDisplayName(7, 2, Locale.US);
            Wg6.b(displayName, "calendar.getDisplayName(\u2026Calendar.LONG, Locale.US)");
            if (displayName != null) {
                String substring = displayName.substring(0, 3);
                Wg6.b(substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
                if (substring != null) {
                    String lowerCase = substring.toLowerCase();
                    Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                    fu3.l(lowerCase);
                } else {
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                throw new Rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return fu3;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteAlarm(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1
            if (r0 == 0) goto L_0x0057
            r0 = r10
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r5) goto L_0x005e
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "deleteAlarm onResponse: response = "
            r3.append(r5)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x007c
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x0056:
            return r0
        L_0x0057:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarm$repoResponse$Anon1
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0056
        L_0x007c:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0099
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0056
        L_0x0099:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.deleteAlarm(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteAlarms(java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1
            if (r0 == 0) goto L_0x005f
            r0 = r10
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005f
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x006e
            if (r0 != r7) goto L_0x0066
            java.lang.Object r0 = r1.L$3
            com.mapped.Fu3 r0 = (com.mapped.Fu3) r0
            java.lang.Object r0 = r1.L$2
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0035:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "deleteAlarms onResponse: response = "
            r3.append(r5)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x00af
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r4, r1, r2, r4)
        L_0x005e:
            return r0
        L_0x005f:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0066:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006e:
            com.fossil.El7.b(r2)
            com.mapped.Ku3 r2 = new com.mapped.Ku3
            r2.<init>()
            com.mapped.Fu3 r5 = new com.mapped.Fu3
            r5.<init>()
            java.util.Iterator r6 = r9.iterator()
        L_0x007f:
            boolean r0 = r6.hasNext()
            if (r0 == 0) goto L_0x0093
            java.lang.Object r0 = r6.next()
            com.portfolio.platform.data.source.local.alarm.Alarm r0 = (com.portfolio.platform.data.source.local.alarm.Alarm) r0
            java.lang.String r0 = r0.getId()
            r5.l(r0)
            goto L_0x007f
        L_0x0093:
            java.lang.String r0 = "_ids"
            r2.k(r0, r5)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$deleteAlarms$repoResponse$Anon1
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r2
            r1.L$3 = r5
            r1.label = r7
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x0035
            r0 = r3
            goto L_0x005e
        L_0x00af:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00cc
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x005e
        L_0x00cc:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.deleteAlarms(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0085  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAlarms(com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1
            if (r0 == 0) goto L_0x0063
            r0 = r9
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1 r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0063
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0071
            if (r3 != r5) goto L_0x0069
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.TAG
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = "getAlarms onResponse: response = "
            r3.append(r5)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            r1.d(r2, r3)
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0085
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            if (r1 == 0) goto L_0x0058
            java.util.List r4 = r1.get_items()
        L_0x0058:
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            boolean r0 = r0.b()
            r1.<init>(r4, r0)
            r0 = r1
        L_0x0062:
            return r0
        L_0x0063:
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1 r0 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$Anon1
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0069:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0071:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1 r1 = new com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource$getAlarms$repoResponse$Anon1
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0062
        L_0x0085:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00a2
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0062
        L_0x00a2:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.getAlarms(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertAlarms(java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm> r11, com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.source.local.alarm.Alarm>>> r12) {
        /*
        // Method dump skipped, instructions count: 351
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource.upsertAlarms(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
