package com.portfolio.platform.data.source;

import com.fossil.Mn7;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1<T> implements Comparator<T> {
    @DexIgnore
    public /* final */ /* synthetic */ List $ids$inlined;

    @DexIgnore
    public ComplicationRepository$getComplicationByIds$$inlined$sortedBy$Anon1(List list) {
        this.$ids$inlined = list;
    }

    @DexIgnore
    @Override // java.util.Comparator
    public final int compare(T t, T t2) {
        return Mn7.c(Integer.valueOf(this.$ids$inlined.indexOf(t.getComplicationId())), Integer.valueOf(this.$ids$inlined.indexOf(t2.getComplicationId())));
    }
}
