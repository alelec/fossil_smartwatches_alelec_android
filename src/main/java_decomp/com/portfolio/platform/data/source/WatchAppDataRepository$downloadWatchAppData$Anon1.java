package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WatchAppDataRepository", f = "WatchAppDataRepository.kt", l = {16, 17, 33}, m = "downloadWatchAppData")
public final class WatchAppDataRepository$downloadWatchAppData$Anon1 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$10;
    @DexIgnore
    public Object L$11;
    @DexIgnore
    public Object L$12;
    @DexIgnore
    public Object L$13;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public Object L$8;
    @DexIgnore
    public Object L$9;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppDataRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchAppDataRepository$downloadWatchAppData$Anon1(WatchAppDataRepository watchAppDataRepository, Xe6 xe6) {
        super(xe6);
        this.this$0 = watchAppDataRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.downloadWatchAppData(null, null, this);
    }
}
