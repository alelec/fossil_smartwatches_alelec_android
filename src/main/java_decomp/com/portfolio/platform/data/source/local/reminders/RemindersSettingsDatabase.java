package com.portfolio.platform.data.source.local.reminders;

import com.mapped.Oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class RemindersSettingsDatabase extends Oh {
    @DexIgnore
    public abstract InactivityNudgeTimeDao getInactivityNudgeTimeDao();

    @DexIgnore
    public abstract RemindTimeDao getRemindTimeDao();
}
