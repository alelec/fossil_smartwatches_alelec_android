package com.portfolio.platform.data.source;

import com.mapped.Af6;
import com.mapped.Ve6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import kotlinx.coroutines.CoroutineExceptionHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1 extends Ve6 implements CoroutineExceptionHandler {
    @DexIgnore
    public ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(Af6.Ci ci) {
        super(ci);
    }

    @DexIgnore
    @Override // kotlinx.coroutines.CoroutineExceptionHandler
    public void handleException(Af6 af6, Throwable th) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(ThirdPartyRepository.TAG, "exception when push third party database " + th);
    }
}
