package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.remote.UserRemoteDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ An4 mSharedPreferencesManager;
    @DexIgnore
    public /* final */ UserRemoteDataSource mUserRemoteDataSource;
    @DexIgnore
    public /* final */ UserSettingDao mUserSettingDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRepository.class.getSimpleName();
        Wg6.b(simpleName, "UserRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRepository(UserRemoteDataSource userRemoteDataSource, UserSettingDao userSettingDao, An4 an4) {
        Wg6.c(userRemoteDataSource, "mUserRemoteDataSource");
        Wg6.c(userSettingDao, "mUserSettingDao");
        Wg6.c(an4, "mSharedPreferencesManager");
        this.mUserRemoteDataSource = userRemoteDataSource;
        this.mUserSettingDao = userSettingDao;
        this.mSharedPreferencesManager = an4;
    }

    @DexIgnore
    public final Object checkAuthenticationEmailExisting(String str, Xe6<? super Ap4<Boolean>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$checkAuthenticationEmailExisting$Anon2(this, str, null), xe6);
    }

    @DexIgnore
    public final Object checkAuthenticationSocialExisting(String str, String str2, Xe6<? super Ap4<Boolean>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$checkAuthenticationSocialExisting$Anon2(this, str, str2, null), xe6);
    }

    @DexIgnore
    public final Object clearAllUser(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new UserRepository$clearAllUser$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void clearUserSettings() {
        this.mUserSettingDao.cleanUp();
    }

    @DexIgnore
    public final Object deleteUser(Xe6<? super Integer> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$deleteUser$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final Object getCurrentUser(Xe6<? super MFUser> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$getCurrentUser$Anon2(null), xe6);
    }

    @DexIgnore
    public final UserSettings getUserSetting() {
        return this.mUserSettingDao.getCurrentUserSetting();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getUserSettingFromServer(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.UserSettings>> r6) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r6 instanceof com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            if (r0 == 0) goto L_0x0043
            r0 = r6
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0043
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0051
            if (r3 != r4) goto L_0x0049
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.El7.b(r1)
            r5 = r0
        L_0x0027:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0062
            r1 = r0
            com.fossil.Kq5 r1 = (com.fossil.Kq5) r1
            java.lang.Object r2 = r1.a()
            if (r2 == 0) goto L_0x0042
            com.portfolio.platform.data.source.UserSettingDao r2 = r5.mUserSettingDao
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            r2.addOrUpdateUserSetting(r1)
        L_0x0042:
            return r0
        L_0x0043:
            com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$getUserSettingFromServer$Anon1
            r0.<init>(r5, r6)
            goto L_0x0013
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r1 = r5.mUserRemoteDataSource
            r0.L$0 = r5
            r0.label = r4
            java.lang.Object r1 = r1.getUserSettingFromServer(r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0042
        L_0x0062:
            boolean r1 = r0 instanceof com.fossil.Hq5
            goto L_0x0042
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.getUserSettingFromServer(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object insertUser(MFUser mFUser, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new UserRepository$insertUser$Anon2(mFUser, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void insertUserSetting(UserSettings userSettings) {
        Wg6.c(userSettings, "userSettings");
        this.mUserSettingDao.addOrUpdateUserSetting(userSettings);
    }

    @DexIgnore
    public final Object loadUserInfo(Xe6<? super Ap4<? extends MFUser>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$loadUserInfo$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final Object loginEmail(String str, String str2, Xe6<? super Ap4<? extends MFUser.Auth>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$loginEmail$Anon2(this, str, str2, null), xe6);
    }

    @DexIgnore
    public final Object loginWithSocial(String str, String str2, String str3, Xe6<? super Ap4<? extends MFUser.Auth>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$loginWithSocial$Anon2(this, str, str2, str3, null), xe6);
    }

    @DexIgnore
    public final Object logoutUser(Xe6<? super Integer> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$logoutUser$Anon2(this, null), xe6);
    }

    @DexIgnore
    public final Object pushPendingUser(MFUser mFUser, Xe6<? super Ap4<MFUser>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$pushPendingUser$Anon2(this, mFUser, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object pushPendingUserSetting(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            if (r0 == 0) goto L_0x002e
            r0 = r9
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = (com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002e
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x003d
            if (r3 != r7) goto L_0x0035
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.UserSettings r0 = (com.portfolio.platform.data.model.UserSettings) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.UserRepository r0 = (com.portfolio.platform.data.source.UserRepository) r0
            com.fossil.El7.b(r2)
        L_0x002b:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x002d:
            return r0
        L_0x002e:
            com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1 r0 = new com.portfolio.platform.data.source.UserRepository$pushPendingUserSetting$Anon1
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0014
        L_0x0035:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003d:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.UserSettingDao r2 = r8.mUserSettingDao
            com.portfolio.platform.data.model.UserSettings r2 = r2.getPendingUserSetting()
            if (r2 == 0) goto L_0x002b
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.UserRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "pushPendingUserSetting(), userSetting="
            r5.append(r6)
            r5.append(r2)
            java.lang.String r5 = r5.toString()
            r3.d(r4, r5)
            r1.L$0 = r8
            r1.L$1 = r2
            r1.label = r7
            java.lang.Object r1 = r8.sendUserSettingToServer(r2, r1)
            if (r1 != r0) goto L_0x002b
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.UserRepository.pushPendingUserSetting(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object requestEmailOtp(String str, Xe6<? super Ap4<Void>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$requestEmailOtp$Anon2(this, str, null), xe6);
    }

    @DexIgnore
    public final Object resetPassword(String str, Xe6<? super Ap4<Integer>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$resetPassword$Anon2(this, str, null), xe6);
    }

    @DexIgnore
    public final Object sendUserSettingToServer(UserSettings userSettings, Xe6<? super Ap4<UserSettings>> xe6) {
        return this.mUserRemoteDataSource.sendUserSettingToServer(userSettings, xe6);
    }

    @DexIgnore
    public final Object signUpEmail(SignUpEmailAuth signUpEmailAuth, Xe6<? super Ap4<? extends MFUser.Auth>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$signUpEmail$Anon2(this, signUpEmailAuth, null), xe6);
    }

    @DexIgnore
    public final Object signUpSocial(SignUpSocialAuth signUpSocialAuth, Xe6<? super Ap4<? extends MFUser.Auth>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$signUpSocial$Anon2(this, signUpSocialAuth, null), xe6);
    }

    @DexIgnore
    public final Object updateUser(MFUser mFUser, boolean z, Xe6<? super Ap4<? extends MFUser>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$updateUser$Anon2(this, z, mFUser, null), xe6);
    }

    @DexIgnore
    public final Object verifyEmailOtp(String str, String str2, Xe6<? super Ap4<Void>> xe6) {
        return Eu7.g(Bw7.b(), new UserRepository$verifyEmailOtp$Anon2(this, str, str2, null), xe6);
    }
}
