package com.portfolio.platform.data.source.remote;

import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public CategoryRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getAllCategory(com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.Category>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1
            if (r0 == 0) goto L_0x0052
            r0 = r9
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1 r0 = (com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0052
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0060
            if (r3 != r5) goto L_0x0058
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.CategoryRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0074
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r0 = (com.portfolio.platform.data.source.remote.ApiResponse) r0
            if (r0 == 0) goto L_0x004a
            java.util.List r0 = r0.get_items()
            if (r0 == 0) goto L_0x004a
            boolean r0 = r1.addAll(r0)
            com.fossil.Ao7.a(r0)
        L_0x004a:
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r2 = 0
            r3 = 2
            r0.<init>(r1, r2, r3, r4)
        L_0x0051:
            return r0
        L_0x0052:
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1 r0 = new com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$Anon1
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0058:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0060:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$response$Anon1 r1 = new com.portfolio.platform.data.source.remote.CategoryRemoteDataSource$getAllCategory$response$Anon1
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x0051
        L_0x0074:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x0091
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0051
        L_0x0091:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.CategoryRemoteDataSource.getAllCategory(com.mapped.Xe6):java.lang.Object");
    }
}
