package com.portfolio.platform.data.source.remote;

import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FailureResponse {
    @DexIgnore
    public List<ErrorItems> _items;
    @DexIgnore
    public ResultItems _result;

    @DexIgnore
    public FailureResponse(List<ErrorItems> list, ResultItems resultItems) {
        Wg6.c(list, CloudLogWriter.ITEMS_PARAM);
        Wg6.c(resultItems, "_result");
        this._items = list;
        this._result = resultItems;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: com.portfolio.platform.data.source.remote.FailureResponse */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ FailureResponse copy$default(FailureResponse failureResponse, List list, ResultItems resultItems, int i, Object obj) {
        if ((i & 1) != 0) {
            list = failureResponse._items;
        }
        if ((i & 2) != 0) {
            resultItems = failureResponse._result;
        }
        return failureResponse.copy(list, resultItems);
    }

    @DexIgnore
    public final List<ErrorItems> component1() {
        return this._items;
    }

    @DexIgnore
    public final ResultItems component2() {
        return this._result;
    }

    @DexIgnore
    public final FailureResponse copy(List<ErrorItems> list, ResultItems resultItems) {
        Wg6.c(list, CloudLogWriter.ITEMS_PARAM);
        Wg6.c(resultItems, "_result");
        return new FailureResponse(list, resultItems);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof FailureResponse) {
                FailureResponse failureResponse = (FailureResponse) obj;
                if (!Wg6.a(this._items, failureResponse._items) || !Wg6.a(this._result, failureResponse._result)) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final List<ErrorItems> get_items() {
        return this._items;
    }

    @DexIgnore
    public final ResultItems get_result() {
        return this._result;
    }

    @DexIgnore
    public int hashCode() {
        int i = 0;
        List<ErrorItems> list = this._items;
        int hashCode = list != null ? list.hashCode() : 0;
        ResultItems resultItems = this._result;
        if (resultItems != null) {
            i = resultItems.hashCode();
        }
        return (hashCode * 31) + i;
    }

    @DexIgnore
    public final void set_items(List<ErrorItems> list) {
        Wg6.c(list, "<set-?>");
        this._items = list;
    }

    @DexIgnore
    public final void set_result(ResultItems resultItems) {
        Wg6.c(resultItems, "<set-?>");
        this._result = resultItems;
    }

    @DexIgnore
    public String toString() {
        return "FailureResponse(_items=" + this._items + ", _result=" + this._result + ")";
    }
}
