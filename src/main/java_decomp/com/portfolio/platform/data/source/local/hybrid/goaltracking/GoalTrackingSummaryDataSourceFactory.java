package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryDataSourceFactory extends Xe.Bi<Date, GoalTrackingSummary> {
    @DexIgnore
    public GoalTrackingSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai mListener;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<GoalTrackingSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public GoalTrackingSummaryDataSourceFactory(GoalTrackingRepository goalTrackingRepository, GoalTrackingDatabase goalTrackingDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(goalTrackingDatabase, "mGoalTrackingDatabase");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(u04, "mAppExecutors");
        Wg6.c(ai, "mListener");
        Wg6.c(calendar, "mStartCalendar");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = u04;
        this.mListener = ai;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Date, GoalTrackingSummary> create() {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = new GoalTrackingSummaryLocalDataSource(this.mGoalTrackingRepository, this.mCreatedDate, this.mGoalTrackingDatabase, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localDataSource = goalTrackingSummaryLocalDataSource;
        this.sourceLiveData.l(goalTrackingSummaryLocalDataSource);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.localDataSource;
        if (goalTrackingSummaryLocalDataSource2 != null) {
            return goalTrackingSummaryLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final GoalTrackingSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<GoalTrackingSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.localDataSource = goalTrackingSummaryLocalDataSource;
    }
}
