package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2", f = "SummariesRepository.kt", l = {231}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<ActivitySummary>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<ActivitySummary>, Ku3> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<Ku3>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSummaries(k, k2, 0, 100, xe6);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:16:0x0078  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object loadFromDb(com.mapped.Xe6<? super androidx.lifecycle.LiveData<java.util.List<com.portfolio.platform.data.model.room.fitness.ActivitySummary>>> r9) {
                /*
                // Method dump skipped, instructions count: 269
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.loadFromDb(com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - onFetchFailed");
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:19:0x0083  */
            /* JADX WARNING: Removed duplicated region for block: B:22:0x00bd  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.mapped.Ku3 r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
                /*
                // Method dump skipped, instructions count: 378
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository.getSummaries.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.mapped.Ku3, com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(Ku3 ku3, Xe6 xe6) {
                return saveCallResult(ku3, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<ActivitySummary> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<ActivitySummary> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2, FitnessDatabase fitnessDatabase) {
            this.this$0 = summariesRepository$getSummaries$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
        }

        @DexIgnore
        public final LiveData<H47<List<ActivitySummary>>> apply(List<FitnessDataWrapper> list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate + " fitnessDataList=" + list.size());
            Wg6.b(list, "fitnessDataList");
            SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = this.this$0;
            return new Anon1_Level3(this, FitnessDataWrapperKt.calculateRangeDownload(list, summariesRepository$getSummaries$Anon2.$startDate, summariesRepository$getSummaries$Anon2.$endDate)).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$getSummaries$Anon2 summariesRepository$getSummaries$Anon2 = new SummariesRepository$getSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, this.$shouldFetch, xe6);
        summariesRepository$getSummaries$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$getSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<ActivitySummary>>>> xe6) {
        throw null;
        //return ((SummariesRepository$getSummaries$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object g;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate);
            Dv7 b = Bw7.b();
            SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2 = new SummariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2(null);
            this.L$0 = il6;
            this.label = 1;
            g = Eu7.g(b, summariesRepository$getSummaries$Anon2$fitnessDatabase$Anon1_Level2, this);
            if (g == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            g = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) g;
        return Ss0.c(fitnessDatabase.getFitnessDataDao().getFitnessDataLiveData(this.$startDate, this.$endDate), new Anon1_Level2(this, fitnessDatabase));
    }
}
