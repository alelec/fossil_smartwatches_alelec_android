package com.portfolio.platform.data.source.local.fitness;

import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$0;
        PagingRequestHelper.Di di = PagingRequestHelper.Di.INITIAL;
        Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        Wg6.b(aii, "helperCallback");
        activitySummaryLocalDataSource2.loadData(di, mStartDate, mEndDate, aii);
    }
}
