package com.portfolio.platform.data.source.local.hybrid.microapp;

import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface MicroAppDao {
    @DexIgnore
    Object clearAllDeclarationFileTable();  // void declaration

    @DexIgnore
    Object clearAllMicroApp();  // void declaration

    @DexIgnore
    Object clearAllMicroAppGalleryTable();  // void declaration

    @DexIgnore
    Object clearAllMicroAppSettingTable();  // void declaration

    @DexIgnore
    Object clearAllMicroAppVariantTable();  // void declaration

    @DexIgnore
    void deleteMicroAppsBySerial(String str);

    @DexIgnore
    List<DeclarationFile> getAllDeclarationFile();

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariant(String str, int i);

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial();

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariants();

    @DexIgnore
    List<MicroApp> getAllMicroApps();

    @DexIgnore
    List<DeclarationFile> getDeclarationFiles(String str, String str2, String str3);

    @DexIgnore
    List<MicroApp> getListMicroApp(String str);

    @DexIgnore
    List<MicroAppSetting> getListMicroAppSetting();

    @DexIgnore
    MicroApp getMicroApp(String str, String str2);

    @DexIgnore
    MicroApp getMicroAppById(String str, String str2);

    @DexIgnore
    List<MicroApp> getMicroAppByIds(List<String> list, String str);

    @DexIgnore
    MicroAppSetting getMicroAppSetting(String str);

    @DexIgnore
    MicroAppVariant getMicroAppVariant(String str, String str2, int i);

    @DexIgnore
    MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3);

    @DexIgnore
    List<MicroAppSetting> getPendingMicroAppSettings();

    @DexIgnore
    List<MicroApp> queryMicroAppByName(String str, String str2);

    @DexIgnore
    void upsertDeclarationFileList(List<DeclarationFile> list);

    @DexIgnore
    void upsertListMicroApp(List<MicroApp> list);

    @DexIgnore
    void upsertMicroApp(MicroApp microApp);

    @DexIgnore
    void upsertMicroAppSetting(MicroAppSetting microAppSetting);

    @DexIgnore
    void upsertMicroAppSettingList(List<MicroAppSetting> list);

    @DexIgnore
    void upsertMicroAppVariant(MicroAppVariant microAppVariant);

    @DexIgnore
    void upsertMicroAppVariantList(List<MicroAppVariant> list);
}
