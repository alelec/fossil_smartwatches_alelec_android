package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon2;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$getSummaries$2$1$1$saveCallResult$fitnessDatas$1", f = "SummariesRepository.kt", l = {}, m = "invokeSuspend")
public final class SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 extends Ko7 implements Coroutine<Il6, Xe6<? super List<FitnessDataWrapper>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(SummariesRepository$getSummaries$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = anon1_Level3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4 = new SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4(this.this$0, xe6);
        summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super List<FitnessDataWrapper>> xe6) {
        throw null;
        //return ((SummariesRepository$getSummaries$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$fitnessDatas$Anon1_Level4) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Date date;
        Date date2;
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            FitnessDataDao fitnessDataDao = this.this$0.this$0.$fitnessDatabase.getFitnessDataDao();
            Lc6 lc6 = this.this$0.$downloadingDate;
            Date date3 = (lc6 == null || (date2 = (Date) lc6.getFirst()) == null) ? this.this$0.this$0.this$0.$startDate : date2;
            Lc6 lc62 = this.this$0.$downloadingDate;
            if (lc62 == null || (date = (Date) lc62.getSecond()) == null) {
                date = this.this$0.this$0.this$0.$endDate;
            }
            return fitnessDataDao.getFitnessData(date3, date);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
