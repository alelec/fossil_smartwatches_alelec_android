package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class GoalTrackingDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "GoalTrackingDao";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TotalSummary {
        @DexIgnore
        public int targets;
        @DexIgnore
        public int values;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public TotalSummary() {
        }

        @DexIgnore
        public final int getTargets() {
            return this.targets;
        }

        @DexIgnore
        public final int getValues() {
            return this.values;
        }

        @DexIgnore
        public final void setTargets(int i) {
            this.targets = i;
        }

        @DexIgnore
        public final void setValues(int i) {
            this.values = i;
        }
    }

    @DexIgnore
    private final GoalTrackingSummary calculateSample(GoalTrackingSummary goalTrackingSummary, GoalTrackingSummary goalTrackingSummary2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "calculateSample - currentSummary=" + goalTrackingSummary + ", newSummary=" + goalTrackingSummary2);
        int totalTracked = goalTrackingSummary2.getTotalTracked() + goalTrackingSummary.getTotalTracked();
        goalTrackingSummary.setUpdatedAt(new Date().getTime());
        if (totalTracked <= 0) {
            totalTracked = 0;
        }
        goalTrackingSummary.setTotalTracked(totalTracked);
        return goalTrackingSummary;
    }

    @DexIgnore
    private final GoalTrackingSummary createSummary(GoalTrackingData goalTrackingData, boolean z) {
        return new GoalTrackingSummary(goalTrackingData.getDate(), z ? 1 : -1, getNearestGoalTrackingTargetFromDate(goalTrackingData.getDate()), new Date().getTime(), new Date().getTime());
    }

    @DexIgnore
    private final void updateGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "updateGoalTrackingSummary - GoalTrackingSummary=" + goalTrackingSummary);
        GoalTrackingSummary goalTrackingSummary2 = getGoalTrackingSummary(goalTrackingSummary.getDate());
        if (goalTrackingSummary2 != null) {
            goalTrackingSummary = calculateSample(goalTrackingSummary2, goalTrackingSummary);
        } else {
            goalTrackingSummary.setGoalTarget(getNearestGoalTrackingTargetFromDate(goalTrackingSummary.getDate()));
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d(TAG, "updateGoalTrackingSummary - new GoalTrackingSummary=" + goalTrackingSummary);
        upsertGoalTrackingSummary(goalTrackingSummary);
    }

    @DexIgnore
    public final void addGoalTrackingRawDataList(List<GoalTrackingData> list) {
        Wg6.c(list, "goalTrackingDataList");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "addGoalTrackingRawData goalTrackingDataList size=" + list.size());
        for (GoalTrackingData goalTrackingData : list) {
            goalTrackingData.setPinType(1);
            updateGoalTrackingSummary(createSummary(goalTrackingData, true));
        }
        upsertGoalTrackingDataList(list);
    }

    @DexIgnore
    public abstract void deleteAllGoalTrackingData();

    @DexIgnore
    public abstract void deleteAllGoalTrackingSummaries();

    @DexIgnore
    public abstract void deleteGoalSetting();

    @DexIgnore
    public final void deleteGoalTrackingRawData(GoalTrackingData goalTrackingData) {
        Wg6.c(goalTrackingData, "goalTrackingData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "deleteGoalTrackingRawData goalTrackingData=" + goalTrackingData);
        goalTrackingData.setPinType(3);
        goalTrackingData.setUpdatedAt(new Date().getTime());
        upsertGoalTrackingData(goalTrackingData);
        updateGoalTrackingSummary(createSummary(goalTrackingData, false));
    }

    @DexIgnore
    public abstract List<GoalTrackingData> getGoalTrackingDataInDate(Date date);

    @DexIgnore
    public abstract List<GoalTrackingData> getGoalTrackingDataList(Date date, Date date2);

    @DexIgnore
    public abstract List<GoalTrackingData> getGoalTrackingDataListAfterInDate(Date date, DateTime dateTime, long j, int i);

    @DexIgnore
    public abstract List<GoalTrackingData> getGoalTrackingDataListInitInDate(Date date, int i);

    @DexIgnore
    public abstract LiveData<List<GoalTrackingData>> getGoalTrackingDataListLiveData(Date date, Date date2);

    @DexIgnore
    public abstract List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<GoalTrackingSummary>> getGoalTrackingSummariesLiveData(Date date, Date date2);

    @DexIgnore
    public abstract GoalTrackingSummary getGoalTrackingSummary(Date date);

    @DexIgnore
    public abstract List<GoalTrackingSummary> getGoalTrackingSummaryList();

    @DexIgnore
    public abstract LiveData<List<GoalTrackingSummary>> getGoalTrackingSummaryListLiveData();

    @DexIgnore
    public abstract LiveData<GoalTrackingSummary> getGoalTrackingSummaryLiveData(Date date);

    @DexIgnore
    public abstract TotalSummary getGoalTrackingValueAndTarget(Date date, Date date2);

    @DexIgnore
    public abstract Integer getLastGoalSetting();

    @DexIgnore
    public abstract LiveData<Integer> getLastGoalSettingLiveData();

    @DexIgnore
    public abstract GoalTrackingSummary getNearestGoalTrackingFromDate(Date date);

    @DexIgnore
    public final int getNearestGoalTrackingTargetFromDate(Date date) {
        Wg6.c(date, "date");
        GoalTrackingSummary nearestGoalTrackingFromDate = getNearestGoalTrackingFromDate(date);
        Integer valueOf = nearestGoalTrackingFromDate != null ? Integer.valueOf(nearestGoalTrackingFromDate.getGoalTarget()) : getLastGoalSetting();
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return 8;
    }

    @DexIgnore
    public abstract List<GoalTrackingData> getPendingGoalTrackingDataList();

    @DexIgnore
    public abstract List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2);

    @DexIgnore
    public abstract Xe.Bi<Integer, GoalTrackingSummary> getSummariesDataSource();

    @DexIgnore
    public abstract void removeDeletedGoalTrackingData(String str);

    @DexIgnore
    public abstract void upsertGoalSettings(GoalSetting goalSetting);

    @DexIgnore
    public abstract void upsertGoalTrackingData(GoalTrackingData goalTrackingData);

    @DexIgnore
    public abstract void upsertGoalTrackingDataList(List<GoalTrackingData> list);

    @DexIgnore
    public abstract void upsertGoalTrackingSummaries(List<GoalTrackingSummary> list);

    @DexIgnore
    public abstract void upsertGoalTrackingSummary(GoalTrackingSummary goalTrackingSummary);

    @DexIgnore
    public abstract void upsertListGoalTrackingData(List<GoalTrackingData> list);
}
