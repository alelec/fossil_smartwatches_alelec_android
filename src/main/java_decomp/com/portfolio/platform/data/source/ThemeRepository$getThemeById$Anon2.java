package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.Theme;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ThemeRepository$getThemeById$2", f = "ThemeRepository.kt", l = {}, m = "invokeSuspend")
public final class ThemeRepository$getThemeById$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Theme>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $id;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemeRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ThemeRepository$getThemeById$Anon2(ThemeRepository themeRepository, String str, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = themeRepository;
        this.$id = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ThemeRepository$getThemeById$Anon2 themeRepository$getThemeById$Anon2 = new ThemeRepository$getThemeById$Anon2(this.this$0, this.$id, xe6);
        themeRepository$getThemeById$Anon2.p$ = (Il6) obj;
        throw null;
        //return themeRepository$getThemeById$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Theme> xe6) {
        throw null;
        //return ((ThemeRepository$getThemeById$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            return this.this$0.mThemeDao.getThemeById(this.$id);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
