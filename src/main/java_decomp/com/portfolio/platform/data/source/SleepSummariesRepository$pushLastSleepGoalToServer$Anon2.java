package com.portfolio.platform.data.source;

import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hg6;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.response.ResponseKt;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
public final class SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<MFSleepSettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $sleepGoal;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$pushLastSleepGoalToServer$2$1", f = "SleepSummariesRepository.kt", l = {117}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Hg6<Xe6<? super Q88<MFSleepSettings>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ku3 $jsonObject;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2, Ku3 ku3, Xe6 xe6) {
            super(1, xe6);
            this.this$0 = sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
            this.$jsonObject = ku3;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            return new Anon1_Level2(this.this$0, this.$jsonObject, xe6);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Hg6
        public final Object invoke(Xe6<? super Q88<MFSleepSettings>> xe6) {
            throw null;
            //return ((Anon1_Level2) create(xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiService;
                Ku3 ku3 = this.$jsonObject;
                this.label = 1;
                Object sleepSetting = apiServiceV2.setSleepSetting(ku3, this);
                return sleepSetting == d ? d : sleepSetting;
            } else if (i == 1) {
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(SleepSummariesRepository sleepSummariesRepository, int i, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummariesRepository;
        this.$sleepGoal = i;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummariesRepository$pushLastSleepGoalToServer$Anon2 sleepSummariesRepository$pushLastSleepGoalToServer$Anon2 = new SleepSummariesRepository$pushLastSleepGoalToServer$Anon2(this.this$0, this.$sleepGoal, xe6);
        sleepSummariesRepository$pushLastSleepGoalToServer$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSummariesRepository$pushLastSleepGoalToServer$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<MFSleepSettings>> xe6) {
        throw null;
        //return ((SleepSummariesRepository$pushLastSleepGoalToServer$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = SleepSummariesRepository.TAG;
            local.d(str, "pushLastSleepGoalToServer sleepGoal=" + this.$sleepGoal);
            Ku3 ku3 = new Ku3();
            try {
                ku3.m("currentGoalMinutes", Ao7.e(this.$sleepGoal));
                TimeZone timeZone = TimeZone.getDefault();
                Wg6.b(timeZone, "TimeZone.getDefault()");
                ku3.m("timezoneOffset", Ao7.e(timeZone.getRawOffset() / 1000));
            } catch (Exception e) {
            }
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, ku3, null);
            this.L$0 = il6;
            this.L$1 = ku3;
            this.label = 1;
            Object d2 = ResponseKt.d(anon1_Level2, this);
            return d2 == d ? d : d2;
        } else if (i == 1) {
            Ku3 ku32 = (Ku3) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
