package com.portfolio.platform.data.source.local.diana.heartrate;

import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

    @DexIgnore
    public HeartRateSummaryLocalDataSource$loadInitial$Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.this$0 = heartRateSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.this$0;
        heartRateSummaryLocalDataSource.calculateStartDate(heartRateSummaryLocalDataSource.mCreatedDate);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource2 = this.this$0;
        Date mStartDate = heartRateSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        Wg6.b(aii, "helperCallback");
        Rm6 unused = heartRateSummaryLocalDataSource2.loadData(mStartDate, mEndDate, aii);
    }
}
