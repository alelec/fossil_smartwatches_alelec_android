package com.portfolio.platform.data.source.local;

import com.mapped.Oh;
import com.mapped.Qg6;
import com.mapped.Xh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class FileDatabase extends Oh {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ Xh MIGRATION_FROM_1_TO_2; // = new FileDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(1, 2);
    @DexIgnore
    public static /* final */ String TAG; // = "FileDatabase";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final Xh getMIGRATION_FROM_1_TO_2() {
            return FileDatabase.MIGRATION_FROM_1_TO_2;
        }
    }

    @DexIgnore
    public abstract FileDao fileDao();
}
