package com.portfolio.platform.data.source.local.thirdparty;

import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GFitActiveTimeDao {
    @DexIgnore
    Object clearAll();  // void declaration

    @DexIgnore
    void deleteGFitActiveTime(GFitActiveTime gFitActiveTime);

    @DexIgnore
    List<GFitActiveTime> getAllGFitActiveTime();

    @DexIgnore
    void insertGFitActiveTime(GFitActiveTime gFitActiveTime);

    @DexIgnore
    void insertListGFitActiveTime(List<GFitActiveTime> list);
}
