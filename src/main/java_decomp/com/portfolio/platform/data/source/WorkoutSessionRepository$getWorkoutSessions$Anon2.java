package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Ss0;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ServerWorkoutSession;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WorkoutSessionRepository$getWorkoutSessions$2", f = "WorkoutSessionRepository.kt", l = {53}, m = "invokeSuspend")
public final class WorkoutSessionRepository$getWorkoutSessions$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<WorkoutSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ FitnessDatabase $fitnessDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionRepository$getWorkoutSessions$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<WorkoutSession>, ApiResponse<ServerWorkoutSession>> {
            @DexIgnore
            public /* final */ /* synthetic */ List $fitnessDataList;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Hh6 hh6, int i, List list) {
                this.this$0 = anon1_Level2;
                this.$offset = hh6;
                this.$limit = i;
                this.$fitnessDataList = list;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<ApiResponse<ServerWorkoutSession>>> xe6) {
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                String k = TimeUtils.k(this.this$0.$startDate);
                Wg6.b(k, "DateHelper.formatShortDate(startDate)");
                String k2 = TimeUtils.k(this.this$0.$endDate);
                Wg6.b(k2, "DateHelper.formatShortDate(endDate)");
                return apiServiceV2.getWorkoutSessions(k, k2, this.$offset.element, this.$limit, xe6);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object loadFromDb(Xe6<? super LiveData<List<WorkoutSession>>> xe6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                local.d(tAG$app_fossilRelease, "getWorkoutSessions - loadFromDb -- end=" + this.this$0.this$0.$end + ", startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
                WorkoutDao workoutDao = this.this$0.$fitnessDatabase.getWorkoutDao();
                Date date = this.this$0.$startDate;
                Wg6.b(date, GoalPhase.COLUMN_START_DATE);
                Date date2 = this.this$0.$endDate;
                Wg6.b(date2, GoalPhase.COLUMN_END_DATE);
                return workoutDao.getWorkoutSessionsDesc(date, date2);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, Xe6<? super Boolean> xe6) {
                Boolean a2;
                Range range = apiResponse.get_range();
                if (range == null || (a2 = Ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return Ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - processContinueFetching -- hasNext=TRUE");
                this.$offset.element += this.$limit;
                return Ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object processContinueFetching(ApiResponse<ServerWorkoutSession> apiResponse, Xe6 xe6) {
                return processContinueFetching(apiResponse, (Xe6<? super Boolean>) xe6);
            }

            @DexIgnore
            public Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, Xe6<? super Cd6> xe6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease = WorkoutSessionRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("getWorkoutSessions - saveCallResult -- item.size=");
                sb.append(apiResponse.get_items().size());
                sb.append(", hasNext=");
                Range range = apiResponse.get_range();
                sb.append(range != null ? Ao7.a(range.isHasNext()) : null);
                local.d(tAG$app_fossilRelease, sb.toString());
                ArrayList arrayList = new ArrayList();
                Iterator<T> it = apiResponse.get_items().iterator();
                while (it.hasNext()) {
                    WorkoutSession workoutSession = it.next().toWorkoutSession();
                    if (workoutSession != null) {
                        arrayList.add(workoutSession);
                    }
                }
                if (!apiResponse.get_items().isEmpty()) {
                    this.this$0.$fitnessDatabase.getWorkoutDao().upsertListWorkoutSession(arrayList);
                }
                FLogger.INSTANCE.getLocal().d(WorkoutSessionRepository.Companion.getTAG$app_fossilRelease(), "getWorkoutSessions - saveCallResult -- DONE!!!");
                return Cd6.a;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(ApiResponse<ServerWorkoutSession> apiResponse, Xe6 xe6) {
                return saveCallResult(apiResponse, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<WorkoutSession> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<WorkoutSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$fitnessDataList.isEmpty();
            }
        }

        @DexIgnore
        public Anon1_Level2(WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2, FitnessDatabase fitnessDatabase, Date date, Date date2) {
            this.this$0 = workoutSessionRepository$getWorkoutSessions$Anon2;
            this.$fitnessDatabase = fitnessDatabase;
            this.$startDate = date;
            this.$endDate = date2;
        }

        @DexIgnore
        public final LiveData<H47<List<WorkoutSession>>> apply(List<FitnessDataWrapper> list) {
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            return new Anon1_Level3(this, hh6, 100, list).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WorkoutSessionRepository$getWorkoutSessions$Anon2(WorkoutSessionRepository workoutSessionRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = workoutSessionRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        WorkoutSessionRepository$getWorkoutSessions$Anon2 workoutSessionRepository$getWorkoutSessions$Anon2 = new WorkoutSessionRepository$getWorkoutSessions$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, xe6);
        workoutSessionRepository$getWorkoutSessions$Anon2.p$ = (Il6) obj;
        throw null;
        //return workoutSessionRepository$getWorkoutSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<WorkoutSession>>>> xe6) {
        throw null;
        //return ((WorkoutSessionRepository$getWorkoutSessions$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Date V;
        Object y;
        Date date;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            V = TimeUtils.V(this.$start);
            Date E = TimeUtils.E(this.$end);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.L$1 = V;
            this.L$2 = E;
            this.label = 1;
            y = encryptedDatabaseManager.y(this);
            if (y == d) {
                return d;
            }
            date = E;
        } else if (i == 1) {
            V = (Date) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            date = (Date) this.L$2;
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        FitnessDatabase fitnessDatabase = (FitnessDatabase) y;
        FitnessDataDao fitnessDataDao = fitnessDatabase.getFitnessDataDao();
        Wg6.b(V, GoalPhase.COLUMN_START_DATE);
        Wg6.b(date, GoalPhase.COLUMN_END_DATE);
        return Ss0.c(fitnessDataDao.getFitnessDataLiveData(V, date), new Anon1_Level2(this, fitnessDatabase, V, date));
    }
}
