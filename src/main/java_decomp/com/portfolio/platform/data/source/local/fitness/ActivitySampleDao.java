package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.LiveData;
import com.fossil.Ss0;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ActivitySampleDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ActivitySampleDao.class.getSimpleName();
        Wg6.b(simpleName, "ActivitySampleDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateSample(ActivitySample activitySample, ActivitySample activitySample2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateSample - currentSample=" + activitySample + ", newSample=" + activitySample2);
        double steps = activitySample2.getSteps();
        double steps2 = activitySample.getSteps();
        double calories = activitySample2.getCalories();
        double calories2 = activitySample.getCalories();
        double distance = activitySample2.getDistance();
        double distance2 = activitySample.getDistance();
        int activeTime = activitySample2.getActiveTime();
        int activeTime2 = activitySample.getActiveTime();
        activitySample2.setSteps(steps + steps2);
        activitySample2.setCalories(calories + calories2);
        activitySample2.setDistance(distance + distance2);
        activitySample2.setActiveTime(activeTime + activeTime2);
        activitySample2.setCreatedAt(activitySample.getCreatedAt());
        activitySample2.getIntensityDistInSteps().updateActivityIntensities(activitySample.getIntensityDistInSteps());
        if (activitySample2.getSteps() != activitySample.getSteps()) {
            activitySample2.setUpdatedAt(new Date().getTime());
        }
    }

    @DexIgnore
    public abstract void deleteAllActivitySamples();

    @DexIgnore
    public abstract ActivitySample getActivitySample(String str);

    @DexIgnore
    public final LiveData<List<ActivitySample>> getActivitySamplesLiveData(Date date, Date date2) {
        Wg6.c(date, GoalPhase.COLUMN_START_DATE);
        Wg6.c(date2, GoalPhase.COLUMN_END_DATE);
        LiveData<List<ActivitySample>> c = Ss0.c(getActivitySamplesLiveDataV2(date, date2), new ActivitySampleDao$getActivitySamplesLiveData$Anon1(this, date, date2));
        Wg6.b(c, "Transformations.switchMa\u2026}\n            }\n        }");
        return c;
    }

    @DexIgnore
    public abstract LiveData<List<SampleRaw>> getActivitySamplesLiveDataV1(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<ActivitySample>> getActivitySamplesLiveDataV2(Date date, Date date2);

    @DexIgnore
    public final void insertActivitySamples(List<ActivitySample> list) {
        Wg6.c(list, "activitySamples");
        for (T t : list) {
            ActivitySample activitySample = getActivitySample(t.getId());
            if (activitySample != null) {
                calculateSample(activitySample, t);
            } else {
                t.setCreatedAt(new Date().getTime());
                t.setUpdatedAt(new Date().getTime());
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "XXX- upsertActivitySamples " + list);
        upsertListActivitySample(list);
    }

    @DexIgnore
    public abstract void upsertListActivitySample(List<ActivitySample> list);
}
