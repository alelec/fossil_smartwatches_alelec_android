package com.portfolio.platform.data.source;

import com.fossil.Lx0;
import com.mapped.Wg6;
import com.mapped.Xh;
import com.misfit.frameworks.buttonservice.log.FLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1 extends Xh {
    @DexIgnore
    public DeviceDatabase$Companion$MIGRATION_FROM_1_TO_2$Anon1(int i, int i2) {
        super(i, i2);
    }

    @DexIgnore
    @Override // com.mapped.Xh
    public void migrate(Lx0 lx0) {
        Wg6.c(lx0, "database");
        lx0.beginTransaction();
        try {
            lx0.execSQL("CREATE TABLE `watchParam` (`prefixSerial` TEXT, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXTPRIMARY KEY(`prefixSerial`))");
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(DeviceDatabase.TAG, "migrate DeviceDatabase from 1->2 failed");
            lx0.execSQL("DROP TABLE IF EXISTS watchParam");
            lx0.execSQL("CREATE TABLE IF NOT EXISTS watchParam (prefixSerial TEXT PRIMARY KEY NOT NULL, versionMajor TEXT, versionMinor TEXT, data TEXT)");
        }
        lx0.setTransactionSuccessful();
        lx0.endTransaction();
    }
}
