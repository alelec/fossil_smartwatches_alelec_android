package com.portfolio.platform.data.source.remote;

import com.fossil.I98;
import com.fossil.Mo5;
import com.fossil.Q88;
import com.fossil.S98;
import com.mapped.Ku3;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SecureApiService {
    @DexIgnore
    @S98("users/me/diana-faces")
    Object createWatchFace(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<DianaWatchFaceUser>>> xe6);

    @DexIgnore
    @S98("users/me/diana-watch-face-presets")
    Object upsertPresets(@I98 Ku3 ku3, Xe6<? super Q88<ApiResponse<Mo5>>> xe6);
}
