package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaAppSettingDao_Impl implements DianaAppSettingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DianaAppSetting> __insertionAdapterOfDianaAppSetting;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteDianaAppSetting;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaAppSetting> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaAppSetting dianaAppSetting) {
            mi.bindLong(1, (long) dianaAppSetting.getPinType());
            if (dianaAppSetting.getId() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaAppSetting.getId());
            }
            if (dianaAppSetting.getAppId() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dianaAppSetting.getAppId());
            }
            if (dianaAppSetting.getCategory() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, dianaAppSetting.getCategory());
            }
            if (dianaAppSetting.getSetting() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, dianaAppSetting.getSetting());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaAppSetting dianaAppSetting) {
            bind(mi, dianaAppSetting);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `DianaAppSetting` (`pinType`,`id`,`appId`,`category`,`setting`) VALUES (?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM DianaAppSetting WHERE category=? AND appId=? ";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM DianaAppSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<DianaAppSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaAppSetting> call() throws Exception {
            Cursor b = Ex0.b(DianaAppSettingDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "pinType");
                int c2 = Dx0.c(b, "id");
                int c3 = Dx0.c(b, "appId");
                int c4 = Dx0.c(b, "category");
                int c5 = Dx0.c(b, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                    dianaAppSetting.setPinType(b.getInt(c));
                    arrayList.add(dianaAppSetting);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaAppSettingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaAppSetting = new Anon1(oh);
        this.__preparedStmtOfDeleteDianaAppSetting = new Anon2(oh);
        this.__preparedStmtOfCleanUp = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void deleteDianaAppSetting(String str, String str2) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteDianaAppSetting.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        if (str2 == null) {
            acquire.bindNull(2);
        } else {
            acquire.bindString(2, str2);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteDianaAppSetting.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getAllDianaAppSetting() {
        Rh f = Rh.f("SELECT * FROM DianaAppSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, "appId");
            int c4 = Dx0.c(b, "category");
            int c5 = Dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getAllDianaAppSetting(String str) {
        Rh f = Rh.f("SELECT * FROM DianaAppSetting WHERE category=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, "appId");
            int c4 = Dx0.c(b, "category");
            int c5 = Dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public LiveData<List<DianaAppSetting>> getAllDianaAppSettingAsLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM DianaAppSetting WHERE category=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"DianaAppSetting"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public DianaAppSetting getDianaAppSetting(String str, String str2) {
        DianaAppSetting dianaAppSetting = null;
        Rh f = Rh.f("SELECT * FROM DianaAppSetting WHERE category=? AND appId=? ", 2);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        if (str2 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, "appId");
            int c4 = Dx0.c(b, "category");
            int c5 = Dx0.c(b, MicroAppSetting.SETTING);
            if (b.moveToFirst()) {
                dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
            }
            return dianaAppSetting;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public List<DianaAppSetting> getPendingDianaAppSetting() {
        Rh f = Rh.f("SELECT * FROM DianaAppSetting WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "pinType");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, "appId");
            int c4 = Dx0.c(b, "category");
            int c5 = Dx0.c(b, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaAppSetting dianaAppSetting = new DianaAppSetting(b.getString(c2), b.getString(c3), b.getString(c4), b.getString(c5));
                dianaAppSetting.setPinType(b.getInt(c));
                arrayList.add(dianaAppSetting);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public Long[] insert(List<DianaAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfDianaAppSetting.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void upsertDianaAppSetting(DianaAppSetting dianaAppSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaAppSetting.insert((Hh<DianaAppSetting>) dianaAppSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaAppSettingDao
    public void upsertDianaAppSettingList(List<DianaAppSetting> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaAppSetting.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
