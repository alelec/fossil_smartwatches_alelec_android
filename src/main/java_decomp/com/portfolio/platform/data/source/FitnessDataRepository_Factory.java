package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FitnessDataRepository_Factory implements Factory<FitnessDataRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;

    @DexIgnore
    public FitnessDataRepository_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceV2Provider = provider;
    }

    @DexIgnore
    public static FitnessDataRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new FitnessDataRepository_Factory(provider);
    }

    @DexIgnore
    public static FitnessDataRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new FitnessDataRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public FitnessDataRepository get() {
        return newInstance(this.mApiServiceV2Provider.get());
    }
}
