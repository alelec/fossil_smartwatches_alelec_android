package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.F05;
import com.fossil.G05;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetDao_Impl implements DianaPresetDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<DianaPreset> __insertionAdapterOfDianaPreset;
    @DexIgnore
    public /* final */ Hh<DianaRecommendPreset> __insertionAdapterOfDianaRecommendPreset;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearDianaPresetTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearDianaRecommendPresetTable;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveAllDeletePinTypePreset;
    @DexIgnore
    public /* final */ F05 __presetComplicationSettingTypeConverter; // = new F05();
    @DexIgnore
    public /* final */ G05 __presetWatchAppSettingTypeConverter; // = new G05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaRecommendPreset> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaRecommendPreset dianaRecommendPreset) {
            if (dianaRecommendPreset.getSerialNumber() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaRecommendPreset.getSerialNumber());
            }
            if (dianaRecommendPreset.getId() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaRecommendPreset.getId());
            }
            if (dianaRecommendPreset.getName() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dianaRecommendPreset.getName());
            }
            mi.bindLong(4, dianaRecommendPreset.isDefault() ? 1 : 0);
            String a2 = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaRecommendPreset.getComplications());
            if (a2 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a2);
            }
            String a3 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaRecommendPreset.getWatchapps());
            if (a3 == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, a3);
            }
            if (dianaRecommendPreset.getWatchFaceId() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, dianaRecommendPreset.getWatchFaceId());
            }
            if (dianaRecommendPreset.getCreatedAt() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, dianaRecommendPreset.getCreatedAt());
            }
            if (dianaRecommendPreset.getUpdatedAt() == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, dianaRecommendPreset.getUpdatedAt());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaRecommendPreset dianaRecommendPreset) {
            bind(mi, dianaRecommendPreset);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaRecommendPreset` (`serialNumber`,`id`,`name`,`isDefault`,`complications`,`watchapps`,`watchFaceId`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon10 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon10(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            boolean z = false;
            Cursor b = Ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "id");
                int c5 = Dx0.c(b, "serialNumber");
                int c6 = Dx0.c(b, "name");
                int c7 = Dx0.c(b, "isActive");
                int c8 = Dx0.c(b, "complications");
                int c9 = Dx0.c(b, "watchapps");
                int c10 = Dx0.c(b, "watchFaceId");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    if (b.getInt(c7) != 0) {
                        z = true;
                    }
                    dianaPreset = new DianaPreset(string, string2, string3, z, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                }
                return dianaPreset;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Hh<DianaPreset> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaPreset dianaPreset) {
            if (dianaPreset.getCreatedAt() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaPreset.getCreatedAt());
            }
            if (dianaPreset.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaPreset.getUpdatedAt());
            }
            mi.bindLong(3, (long) dianaPreset.getPinType());
            if (dianaPreset.getId() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, dianaPreset.getId());
            }
            if (dianaPreset.getSerialNumber() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, dianaPreset.getSerialNumber());
            }
            if (dianaPreset.getName() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, dianaPreset.getName());
            }
            mi.bindLong(7, dianaPreset.isActive() ? 1 : 0);
            String a2 = DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.a(dianaPreset.getComplications());
            if (a2 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, a2);
            }
            String a3 = DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.a(dianaPreset.getWatchapps());
            if (a3 == null) {
                mi.bindNull(9);
            } else {
                mi.bindString(9, a3);
            }
            if (dianaPreset.getWatchFaceId() == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, dianaPreset.getWatchFaceId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaPreset dianaPreset) {
            bind(mi, dianaPreset);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaPreset` (`createdAt`,`updatedAt`,`pinType`,`id`,`serialNumber`,`name`,`isActive`,`complications`,`watchapps`,`watchFaceId`) VALUES (?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends Vh {
        @DexIgnore
        public Anon4(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends Vh {
        @DexIgnore
        public Anon5(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends Vh {
        @DexIgnore
        public Anon6(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends Vh {
        @DexIgnore
        public Anon7(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM dianaPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon8(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public DianaPreset call() throws Exception {
            DianaPreset dianaPreset = null;
            boolean z = false;
            Cursor b = Ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "id");
                int c5 = Dx0.c(b, "serialNumber");
                int c6 = Dx0.c(b, "name");
                int c7 = Dx0.c(b, "isActive");
                int c8 = Dx0.c(b, "complications");
                int c9 = Dx0.c(b, "watchapps");
                int c10 = Dx0.c(b, "watchFaceId");
                if (b.moveToFirst()) {
                    String string = b.getString(c4);
                    String string2 = b.getString(c5);
                    String string3 = b.getString(c6);
                    if (b.getInt(c7) != 0) {
                        z = true;
                    }
                    dianaPreset = new DianaPreset(string, string2, string3, z, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                }
                return dianaPreset;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon9 implements Callable<List<DianaPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon9(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<DianaPreset> call() throws Exception {
            Cursor b = Ex0.b(DianaPresetDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "createdAt");
                int c2 = Dx0.c(b, "updatedAt");
                int c3 = Dx0.c(b, "pinType");
                int c4 = Dx0.c(b, "id");
                int c5 = Dx0.c(b, "serialNumber");
                int c6 = Dx0.c(b, "name");
                int c7 = Dx0.c(b, "isActive");
                int c8 = Dx0.c(b, "complications");
                int c9 = Dx0.c(b, "watchapps");
                int c10 = Dx0.c(b, "watchFaceId");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, DianaPresetDao_Impl.this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), DianaPresetDao_Impl.this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                    dianaPreset.setCreatedAt(b.getString(c));
                    dianaPreset.setUpdatedAt(b.getString(c2));
                    dianaPreset.setPinType(b.getInt(c3));
                    arrayList.add(dianaPreset);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public DianaPresetDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaRecommendPreset = new Anon1(oh);
        this.__insertionAdapterOfDianaPreset = new Anon2(oh);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon3(oh);
        this.__preparedStmtOfClearDianaPresetTable = new Anon4(oh);
        this.__preparedStmtOfClearDianaRecommendPresetTable = new Anon5(oh);
        this.__preparedStmtOfDeletePreset = new Anon6(oh);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearDianaPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void clearDianaRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearDianaRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearDianaRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.bindNull(1);
        } else {
            acquire.bindString(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getActivePresetBySerial(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            if (b.moveToFirst()) {
                dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
            }
            return dianaPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<DianaPreset> getActivePresetBySerialLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon8 anon8 = new Anon8(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon8);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPendingPreset(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPreset(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<List<DianaPreset>> getAllPresetAsLiveData(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon9 anon9 = new Anon9(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon9);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getAllPresets() {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE pinType != 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaRecommendPreset> getDianaRecommendPresetList(String str) {
        Rh f = Rh.f("SELECT * FROM dianaRecommendPreset WHERE serialNumber=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "serialNumber");
            int c2 = Dx0.c(b, "id");
            int c3 = Dx0.c(b, "name");
            int c4 = Dx0.c(b, "isDefault");
            int c5 = Dx0.c(b, "complications");
            int c6 = Dx0.c(b, "watchapps");
            int c7 = Dx0.c(b, "watchFaceId");
            int c8 = Dx0.c(b, "createdAt");
            int c9 = Dx0.c(b, "updatedAt");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new DianaRecommendPreset(b.getString(c), b.getString(c2), b.getString(c3), b.getInt(c4) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c5)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c6)), b.getString(c7), b.getString(c8), b.getString(c9)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public DianaPreset getPresetById(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        DianaPreset dianaPreset = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            if (b.moveToFirst()) {
                dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
            }
            return dianaPreset;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public LiveData<DianaPreset> getPresetByIdLive(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE id=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon10 anon10 = new Anon10(f);
        return invalidationTracker.d(new String[]{"dianaPreset"}, false, anon10);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public List<DianaPreset> getPresetListByWatchFaceId(String str) {
        Rh f = Rh.f("SELECT * FROM dianaPreset WHERE watchFaceId=? AND pinType != 3", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "pinType");
            int c4 = Dx0.c(b, "id");
            int c5 = Dx0.c(b, "serialNumber");
            int c6 = Dx0.c(b, "name");
            int c7 = Dx0.c(b, "isActive");
            int c8 = Dx0.c(b, "complications");
            int c9 = Dx0.c(b, "watchapps");
            int c10 = Dx0.c(b, "watchFaceId");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaPreset dianaPreset = new DianaPreset(b.getString(c4), b.getString(c5), b.getString(c6), b.getInt(c7) != 0, this.__presetComplicationSettingTypeConverter.b(b.getString(c8)), this.__presetWatchAppSettingTypeConverter.b(b.getString(c9)), b.getString(c10));
                dianaPreset.setCreatedAt(b.getString(c));
                dianaPreset.setUpdatedAt(b.getString(c2));
                dianaPreset.setPinType(b.getInt(c3));
                arrayList.add(dianaPreset);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertDianaRecommendPresetList(List<DianaRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPreset(DianaPreset dianaPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert((Hh<DianaPreset>) dianaPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaPresetDao
    public void upsertPresetList(List<DianaPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
