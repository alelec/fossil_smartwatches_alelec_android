package com.portfolio.platform.data.source;

import com.fossil.Dl7;
import com.mapped.Lk6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.manager.FileDownloadManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$downloadAndSaveWithFileName$$inlined$suspendCancellableCoroutine$lambda$Anon1 implements FileDownloadManager.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ String $checksum$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ String $fileName$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ String $remoteUrl$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileType $type$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FileRepository this$0;

    @DexIgnore
    public FileRepository$downloadAndSaveWithFileName$$inlined$suspendCancellableCoroutine$lambda$Anon1(Lk6 lk6, FileRepository fileRepository, String str, String str2, String str3, FileType fileType) {
        this.$continuation = lk6;
        this.this$0 = fileRepository;
        this.$remoteUrl$inlined = str;
        this.$fileName$inlined = str2;
        this.$checksum$inlined = str3;
        this.$type$inlined = fileType;
    }

    @DexIgnore
    @Override // com.portfolio.platform.manager.FileDownloadManager.Bi
    public void onComplete(boolean z, LocalFile localFile) {
        Wg6.c(localFile, "file");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = FileRepository.Companion.getTAG();
        local.d(tag, "downloadFromURL with remoteUrl " + this.$remoteUrl$inlined + " isSuccess " + z);
        if (z) {
            this.this$0.mFileDao.upsertLocalFile(localFile);
        }
        if (this.$continuation.isActive()) {
            Lk6 lk6 = this.$continuation;
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(Boolean.valueOf(z)));
        }
    }
}
