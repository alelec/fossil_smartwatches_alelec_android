package com.portfolio.platform.data.source.local.sleep;

import androidx.lifecycle.MutableLiveData;
import com.mapped.PagingRequestHelper;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.portfolio.platform.data.SleepSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummaryDataSourceFactory extends Xe.Bi<Date, SleepSummary> {
    @DexIgnore
    public SleepSummaryLocalDataSource localSource;
    @DexIgnore
    public /* final */ U04 mAppExecutors;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai mListener;
    @DexIgnore
    public /* final */ SleepDatabase mSleepDatabase;
    @DexIgnore
    public /* final */ SleepSessionsRepository mSleepSessionsRepository;
    @DexIgnore
    public /* final */ SleepSummariesRepository mSleepSummariesRepository;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<SleepSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();

    @DexIgnore
    public SleepSummaryDataSourceFactory(SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, FitnessDataRepository fitnessDataRepository, SleepDatabase sleepDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai, Calendar calendar) {
        Wg6.c(sleepSummariesRepository, "mSleepSummariesRepository");
        Wg6.c(sleepSessionsRepository, "mSleepSessionsRepository");
        Wg6.c(fitnessDataRepository, "mFitnessDataRepository");
        Wg6.c(sleepDatabase, "mSleepDatabase");
        Wg6.c(date, "mCreatedDate");
        Wg6.c(u04, "mAppExecutors");
        Wg6.c(ai, "mListener");
        Wg6.c(calendar, "mStartCalendar");
        this.mSleepSummariesRepository = sleepSummariesRepository;
        this.mSleepSessionsRepository = sleepSessionsRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mSleepDatabase = sleepDatabase;
        this.mCreatedDate = date;
        this.mAppExecutors = u04;
        this.mListener = ai;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    @Override // com.mapped.Xe.Bi
    public Xe<Date, SleepSummary> create() {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource = new SleepSummaryLocalDataSource(this.mSleepSummariesRepository, this.mSleepSessionsRepository, this.mFitnessDataRepository, this.mSleepDatabase, this.mCreatedDate, this.mAppExecutors, this.mListener, this.mStartCalendar);
        this.localSource = sleepSummaryLocalDataSource;
        this.sourceLiveData.l(sleepSummaryLocalDataSource);
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource2 = this.localSource;
        if (sleepSummaryLocalDataSource2 != null) {
            return sleepSummaryLocalDataSource2;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final SleepSummaryLocalDataSource getLocalSource() {
        return this.localSource;
    }

    @DexIgnore
    public final MutableLiveData<SleepSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalSource(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        this.localSource = sleepSummaryLocalDataSource;
    }
}
