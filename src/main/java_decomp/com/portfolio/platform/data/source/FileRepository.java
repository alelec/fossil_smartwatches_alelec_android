package com.portfolio.platform.data.source;

import android.graphics.Bitmap;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.B68;
import com.fossil.Cp7;
import com.fossil.Dl7;
import com.fossil.Go7;
import com.fossil.I37;
import com.fossil.Lu7;
import com.fossil.Xn7;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.FileHelper;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.data.source.local.FileDao;
import com.portfolio.platform.manager.FileDownloadManager;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ PortfolioApp mApp;
    @DexIgnore
    public /* final */ FileDao mFileDao;
    @DexIgnore
    public /* final */ FileDownloadManager mFileDownloadManager;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return FileRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = FileRepository.class.getSimpleName();
        Wg6.b(simpleName, "FileRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public FileRepository(FileDao fileDao, FileDownloadManager fileDownloadManager, PortfolioApp portfolioApp) {
        Wg6.c(fileDao, "mFileDao");
        Wg6.c(fileDownloadManager, "mFileDownloadManager");
        Wg6.c(portfolioApp, "mApp");
        this.mFileDao = fileDao;
        this.mFileDownloadManager = fileDownloadManager;
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public static /* synthetic */ void asyncDownloadFromUrl$default(FileRepository fileRepository, String str, FileType fileType, String str2, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        fileRepository.asyncDownloadFromUrl(str, fileType, str2);
    }

    @DexIgnore
    private final void deleteFileRecursively(String str) {
        File file = new File(str);
        if (file.exists()) {
            Cp7.e(file);
        }
    }

    @DexIgnore
    private final void deletedFile(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "deletedFile() - filePath = " + str);
        File file = new File(str);
        if (file.exists()) {
            file.delete();
        }
    }

    @DexIgnore
    public static /* synthetic */ Object downloadAndSaveWithFileName$default(FileRepository fileRepository, String str, String str2, FileType fileType, String str3, Xe6 xe6, int i, Object obj) {
        return fileRepository.downloadAndSaveWithFileName(str, str2, fileType, (i & 8) != 0 ? null : str3, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object downloadAndSaveWithRemoteUrl$default(FileRepository fileRepository, String str, FileType fileType, String str2, Xe6 xe6, int i, Object obj) {
        if ((i & 4) != 0) {
            str2 = null;
        }
        return fileRepository.downloadAndSaveWithRemoteUrl(str, fileType, str2, xe6);
    }

    @DexIgnore
    private final void upsertLocalFile(LocalFile localFile) {
        this.mFileDao.upsertLocalFile(localFile);
    }

    @DexIgnore
    public static /* synthetic */ void writeFileWithDir$default(FileRepository fileRepository, String str, FileType fileType, String str2, byte[] bArr, String str3, int i, Object obj) {
        fileRepository.writeFileWithDir(str, fileType, str2, bArr, (i & 16) != 0 ? null : str3);
    }

    @DexIgnore
    public final void asyncDownloadFromUrl(String str, FileType fileType, String str2) {
        Wg6.c(fileType, "type");
        if (!TextUtils.isEmpty(str)) {
            LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
            if (localFileByRemoteUrl == null) {
                String b = Ym5.b(str);
                Wg6.b(b, "fileName");
                if (str != null) {
                    localFileByRemoteUrl = new LocalFile(b, "", str, str2);
                    localFileByRemoteUrl.setType(fileType);
                    this.mFileDao.upsertLocalFile(localFileByRemoteUrl);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else if (localFileByRemoteUrl.getPinType() == 0) {
                return;
            }
            this.mFileDownloadManager.i(new FileDownloadManager.Ai(localFileByRemoteUrl, new FileRepository$asyncDownloadFromUrl$Anon1(this, str)));
        }
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().e(TAG, "cleanUp()");
        String downloadedPath = FileUtils.getDownloadedPath(this.mApp.getApplicationContext());
        Wg6.b(downloadedPath, "directory");
        deleteFileRecursively(downloadedPath);
        this.mFileDao.clearLocalFileTable();
    }

    @DexIgnore
    public final void delete(LocalFile localFile) {
        Wg6.c(localFile, "localFile");
        this.mFileDao.deleteLocalFile(localFile);
    }

    @DexIgnore
    public final void deleteBackgroundFiles() {
        File[] listFiles;
        FLogger.INSTANCE.getLocal().d(TAG, "deleteBackgroundFiles()");
        File file = new File(FileUtils.getDirectory(this.mApp.getApplicationContext(), FileType.WATCH_FACE));
        if (file.exists() && (listFiles = file.listFiles(FileRepository$deleteBackgroundFiles$Anon1.INSTANCE)) != null) {
            for (File file2 : listFiles) {
                Wg6.b(file2, "it");
                String path = file2.getPath();
                Wg6.b(path, "it.path");
                deletedFile(path);
                FileDao fileDao = this.mFileDao;
                String path2 = file2.getPath();
                Wg6.b(path2, "it.path");
                fileDao.deleteLocalFileByUri(path2);
            }
        }
    }

    @DexIgnore
    public final void deleteFileByName(String str, FileType fileType) {
        Wg6.c(str, "fileName");
        Wg6.c(fileType, "fileType");
        FLogger.INSTANCE.getLocal().e(TAG, "deleteFile() - fileName: " + str);
        String str2 = FileUtils.getDirectory(this.mApp.getApplicationContext(), fileType) + File.separator;
        if (new File(str2).exists()) {
            String str3 = str2 + str;
            deletedFile(str3);
            this.mFileDao.deleteLocalFileByUri(str3);
        }
    }

    @DexIgnore
    public final void deletedFilesByType(FileType fileType) {
        Wg6.c(fileType, "fileType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "deletedFiles() - fileType: " + fileType);
        String directory = FileUtils.getDirectory(this.mApp.getApplicationContext(), fileType);
        Wg6.b(directory, "directory");
        deleteFileRecursively(directory);
        this.mFileDao.deleteLocalFileByType(fileType.getMValue());
    }

    @DexIgnore
    public final Object downloadAndSaveWithFileName(String str, String str2, FileType fileType, String str3, Xe6<? super Boolean> xe6) {
        String str4;
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        if (TextUtils.isEmpty(str)) {
            Dl7.Ai ai = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(Ao7.a(false)));
        } else {
            LocalFile fileByFileName = this.mFileDao.getFileByFileName(str2);
            if (fileByFileName == null || (str4 = fileByFileName.getLocalUri()) == null) {
                str4 = "";
            }
            boolean exists = new File(str4).exists();
            if (fileByFileName == null || !exists) {
                LocalFile localFile = new LocalFile(str2, "", str, str3);
                localFile.setType(fileType);
                this.mFileDao.upsertLocalFile(localFile);
                this.mFileDownloadManager.i(new FileDownloadManager.Ai(localFile, new FileRepository$downloadAndSaveWithFileName$$inlined$suspendCancellableCoroutine$lambda$Anon1(lu7, this, str, str2, str3, fileType)));
            } else {
                Dl7.Ai ai2 = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(Ao7.a(true)));
            }
        }
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final Object downloadAndSaveWithRemoteUrl(String str, FileType fileType, String str2, Xe6<? super Boolean> xe6) {
        String str3;
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        if (TextUtils.isEmpty(str)) {
            Dl7.Ai ai = Dl7.Companion;
            lu7.resumeWith(Dl7.constructor-impl(Ao7.a(false)));
        } else {
            LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
            if (localFileByRemoteUrl == null || (str3 = localFileByRemoteUrl.getLocalUri()) == null) {
                str3 = "";
            }
            boolean exists = new File(str3).exists();
            if (localFileByRemoteUrl == null || !exists) {
                String b = Ym5.b(str);
                Wg6.b(b, "fileName");
                if (str != null) {
                    LocalFile localFile = new LocalFile(b, "", str, str2);
                    localFile.setType(fileType);
                    this.mFileDao.upsertLocalFile(localFile);
                    this.mFileDownloadManager.i(new FileDownloadManager.Ai(localFile, new FileRepository$downloadAndSaveWithRemoteUrl$$inlined$suspendCancellableCoroutine$lambda$Anon1(lu7, this, str, str2, fileType)));
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Dl7.Ai ai2 = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(Ao7.a(true)));
            }
        }
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final void downloadPendingFile() {
        List<LocalFile> listPendingFile = this.mFileDao.getListPendingFile();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "downloadPendingFile with size " + listPendingFile.size());
        for (T t : listPendingFile) {
            if (!(t.getRemoteUrl().length() == 0)) {
                this.mFileDownloadManager.i(new FileDownloadManager.Ai(t, new FileRepository$downloadPendingFile$$inlined$forEach$lambda$Anon1(t, this)));
            } else {
                return;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        com.fossil.So7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getBinaryStringByFileName(java.lang.String r5, com.mapped.Xe6<? super java.lang.String> r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.File r1 = r4.getFileByFileName(r5)
            if (r1 == 0) goto L_0x0024
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r1)
            r1 = r0
        L_0x000d:
            byte[] r0 = com.fossil.B68.g(r1)     // Catch:{ all -> 0x0026 }
            r3 = 0
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r3)     // Catch:{ all -> 0x0026 }
            java.lang.String r3 = "Base64.encodeToString(IO\u2026rray(it), Base64.DEFAULT)"
            com.mapped.Wg6.b(r0, r3)     // Catch:{ all -> 0x0026 }
            com.fossil.So7.a(r1, r2)
            java.lang.String r1 = "file?.inputStream().use \u2026Base64.DEFAULT)\n        }"
            com.mapped.Wg6.b(r0, r1)
            return r0
        L_0x0024:
            r1 = r2
            goto L_0x000d
        L_0x0026:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0028 }
        L_0x0028:
            r2 = move-exception
            com.fossil.So7.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FileRepository.getBinaryStringByFileName(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0028, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0029, code lost:
        com.fossil.So7.a(r1, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
        throw r2;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getBinaryStringByRemoteUrl(java.lang.String r5, com.mapped.Xe6<? super java.lang.String> r6) {
        /*
            r4 = this;
            r2 = 0
            java.io.File r1 = r4.getFileByRemoteUrl(r5)
            if (r1 == 0) goto L_0x0024
            java.io.FileInputStream r0 = new java.io.FileInputStream
            r0.<init>(r1)
            r1 = r0
        L_0x000d:
            byte[] r0 = com.fossil.B68.g(r1)     // Catch:{ all -> 0x0026 }
            r3 = 0
            java.lang.String r0 = android.util.Base64.encodeToString(r0, r3)     // Catch:{ all -> 0x0026 }
            java.lang.String r3 = "Base64.encodeToString(IO\u2026rray(it), Base64.DEFAULT)"
            com.mapped.Wg6.b(r0, r3)     // Catch:{ all -> 0x0026 }
            com.fossil.So7.a(r1, r2)
            java.lang.String r1 = "file?.inputStream().use \u2026Base64.DEFAULT)\n        }"
            com.mapped.Wg6.b(r0, r1)
            return r0
        L_0x0024:
            r1 = r2
            goto L_0x000d
        L_0x0026:
            r0 = move-exception
            throw r0     // Catch:{ all -> 0x0028 }
        L_0x0028:
            r2 = move-exception
            com.fossil.So7.a(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.FileRepository.getBinaryStringByRemoteUrl(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final File getFileByFileName(String str) {
        Wg6.c(str, "fileName");
        LocalFile fileByFileName = this.mFileDao.getFileByFileName(str);
        String localUri = fileByFileName != null ? fileByFileName.getLocalUri() : null;
        boolean a2 = FileHelper.a.a(localUri);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getFileLocalUrl with fileName " + str + " isFileExist " + a2);
        if (TextUtils.isEmpty(localUri) || !a2) {
            return null;
        }
        if (localUri != null) {
            return new File(localUri);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final File getFileByName(String str, FileType fileType) {
        Wg6.c(str, "fileName");
        Wg6.c(fileType, "fileType");
        FLogger.INSTANCE.getLocal().e(TAG, "getFileByName() - fileName: " + str);
        String str2 = FileUtils.getDirectory(this.mApp.getApplicationContext(), fileType) + File.separator;
        if (!new File(str2).exists()) {
            return null;
        }
        return new File(str2 + str);
    }

    @DexIgnore
    public final File getFileByRemoteUrl(String str) {
        LocalFile localFileByRemoteUrl = this.mFileDao.getLocalFileByRemoteUrl(str);
        String localUri = localFileByRemoteUrl != null ? localFileByRemoteUrl.getLocalUri() : null;
        boolean a2 = FileHelper.a.a(localUri);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "getFileLocalUrl with remoteUrl " + str + " isFileExist " + a2);
        if (TextUtils.isEmpty(localUri) || !a2) {
            return null;
        }
        if (localUri != null) {
            return new File(localUri);
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final LocalFile getLocalFileByFileName(String str) {
        Wg6.c(str, "fileName");
        return this.mFileDao.getFileByFileName(str);
    }

    @DexIgnore
    public final LiveData<List<LocalFile>> getPendingFilesAsLiveData() {
        return this.mFileDao.getListPendingFileAsLiveData();
    }

    @DexIgnore
    public final void insertLocalFile(LocalFile localFile) {
        Wg6.c(localFile, "localFile");
        this.mFileDao.upsertLocalFile(localFile);
    }

    @DexIgnore
    public final String saveLocalDataFile(String str, String str2, String str3) {
        Wg6.c(str2, "fileName");
        Wg6.c(str3, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local.d(str4, "Start saving " + str2 + " to " + str3);
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (str != null) {
            Bitmap e = I37.e(str);
            FileHelper fileHelper = FileHelper.a;
            Wg6.b(e, "bitmap");
            fileHelper.f(e, str3);
            LocalFile localFile = new LocalFile(str2, str3, "", "");
            localFile.setPinType(0);
            localFile.setType(FileType.WORKOUT_SCREEN_SHOT);
            this.mFileDao.upsertLocalFile(localFile);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str5 = TAG;
            local2.d(str5, "Done saving " + str2 + " to " + str3 + ", localUri " + localFile.getLocalUri());
            return localFile.getLocalUri();
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final void writeFileWithDir(String str, FileType fileType, String str2, byte[] bArr, String str3) {
        FileOutputStream fileOutputStream;
        Throwable th;
        Wg6.c(str, "remoteUrl");
        Wg6.c(fileType, "type");
        Wg6.c(str2, "checksum");
        Wg6.c(bArr, "data");
        FLogger.INSTANCE.getLocal().e(TAG, "writeFileWithDir - remoteUrl: " + str + " - id: " + str3);
        String b = str3 != null ? str3 : Ym5.b(str);
        String directory = FileUtils.getDirectory(PortfolioApp.get.instance().getApplicationContext(), fileType);
        String str4 = directory + File.separator + b;
        Wg6.b(b, "fileName");
        if (str3 == null) {
            str3 = str;
        }
        LocalFile localFile = new LocalFile(b, str4, str3, str2);
        localFile.setType(fileType);
        byte[] bArr2 = new byte[bArr.length];
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            File file = new File(directory);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(str4);
            if (!file2.exists()) {
                file2.createNewFile();
            }
            fileOutputStream = new FileOutputStream(str4, false);
            while (true) {
                try {
                    int read = byteArrayInputStream.read(bArr2);
                    if (read == -1) {
                        fileOutputStream.flush();
                        localFile.setPinType(0);
                        this.mFileDao.upsertLocalFile(localFile);
                        B68.b(byteArrayInputStream);
                        B68.c(fileOutputStream);
                        return;
                    }
                    fileOutputStream.write(bArr2, 0, read);
                } catch (Exception e) {
                    e = e;
                    try {
                        localFile.setPinType(1);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("exception ");
                        e.printStackTrace();
                        sb.append(Cd6.a);
                        local.d(str5, sb.toString());
                        e.printStackTrace();
                        this.mFileDao.upsertLocalFile(localFile);
                        B68.b(byteArrayInputStream);
                        B68.c(fileOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        this.mFileDao.upsertLocalFile(localFile);
                        B68.b(byteArrayInputStream);
                        B68.c(fileOutputStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    this.mFileDao.upsertLocalFile(localFile);
                    B68.b(byteArrayInputStream);
                    B68.c(fileOutputStream);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            fileOutputStream = null;
            localFile.setPinType(1);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str52 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("exception ");
            e.printStackTrace();
            sb2.append(Cd6.a);
            local2.d(str52, sb2.toString());
            e.printStackTrace();
            this.mFileDao.upsertLocalFile(localFile);
            B68.b(byteArrayInputStream);
            B68.c(fileOutputStream);
        } catch (Throwable th4) {
            th = th4;
            fileOutputStream = null;
            this.mFileDao.upsertLocalFile(localFile);
            B68.b(byteArrayInputStream);
            B68.c(fileOutputStream);
            throw th;
        }
    }
}
