package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$deleteGoalTracking$2", f = "GoalTrackingRepository.kt", l = {434, 435}, m = "invokeSuspend")
public final class GoalTrackingRepository$deleteGoalTracking$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingData $goalTrackingData;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$deleteGoalTracking$Anon2(GoalTrackingRepository goalTrackingRepository, GoalTrackingData goalTrackingData, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$goalTrackingData = goalTrackingData;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$deleteGoalTracking$Anon2 goalTrackingRepository$deleteGoalTracking$Anon2 = new GoalTrackingRepository$deleteGoalTracking$Anon2(this.this$0, this.$goalTrackingData, xe6);
        goalTrackingRepository$deleteGoalTracking$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$deleteGoalTracking$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingRepository$deleteGoalTracking$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0041  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x0043
            if (r0 == r6) goto L_0x0020
            if (r0 != r7) goto L_0x0018
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
        L_0x0015:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0017:
            return r0
        L_0x0018:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0020:
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0029:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r1 = r8.$goalTrackingData
            r0.deleteGoalTrackingRawData(r1)
            com.portfolio.platform.data.source.GoalTrackingRepository r0 = r8.this$0
            r8.L$0 = r2
            r8.label = r7
            java.lang.Object r0 = r0.pushPendingGoalTrackingDataList(r8)
            if (r0 != r3) goto L_0x0015
            r0 = r3
            goto L_0x0017
        L_0x0043:
            com.fossil.El7.b(r9)
            com.mapped.Il6 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "deleteGoalTracking "
            r4.append(r5)
            com.portfolio.platform.data.model.goaltracking.GoalTrackingData r5 = r8.$goalTrackingData
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = r1.A(r8)
            if (r1 != r3) goto L_0x0078
            r0 = r3
            goto L_0x0017
        L_0x0078:
            r2 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$deleteGoalTracking$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
