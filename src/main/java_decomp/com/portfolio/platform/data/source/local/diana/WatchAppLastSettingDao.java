package com.portfolio.platform.data.source.local.diana;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WatchAppLastSettingDao {
    @DexIgnore
    Object cleanUp();  // void declaration

    @DexIgnore
    void deleteWatchAppLastSettingById(String str);

    @DexIgnore
    List<WatchAppLastSetting> getAllWatchAppLastSetting();

    @DexIgnore
    LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData();

    @DexIgnore
    WatchAppLastSetting getWatchAppLastSetting(String str);

    @DexIgnore
    void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting);
}
