package com.portfolio.platform.data.source.remote;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.remote.HybridPresetRemoteDataSource$downloadHybridPresetList$response$1", f = "HybridPresetRemoteDataSource.kt", l = {77}, m = "invokeSuspend")
public final class HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<HybridPreset>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ HybridPresetRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1(HybridPresetRemoteDataSource hybridPresetRemoteDataSource, String str, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = hybridPresetRemoteDataSource;
        this.$serial = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1(this.this$0, this.$serial, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<HybridPreset>>> xe6) {
        throw null;
        //return ((HybridPresetRemoteDataSource$downloadHybridPresetList$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiServiceV2;
            String str = this.$serial;
            this.label = 1;
            Object hybridPresetList = apiServiceV2.getHybridPresetList(str, this);
            return hybridPresetList == d ? d : hybridPresetList;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
