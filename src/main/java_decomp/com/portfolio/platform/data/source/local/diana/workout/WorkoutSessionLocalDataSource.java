package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.facebook.internal.NativeProtocol;
import com.fossil.Bw7;
import com.fossil.Gl5;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Nw0;
import com.mapped.PagingRequestHelper;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Ye;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource extends Ye<Long, WorkoutSession> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ PagingRequestHelper.Ai listener;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public PagingRequestHelper mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState;
    @DexIgnore
    public /* final */ Nw0.Ci mObserver; // = new Anon1(this, "workout_session", new String[0]);
    @DexIgnore
    public int mOffset;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends Nw0.Ci {
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = workoutSessionLocalDataSource;
        }

        @DexIgnore
        @Override // com.fossil.Nw0.Ci
        public void onInvalidated(Set<String> set) {
            Wg6.c(set, "tables");
            FLogger.INSTANCE.getLocal().d(WorkoutSessionLocalDataSource.Companion.getTAG(), "XXX- invalidate workout session table");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return WorkoutSessionLocalDataSource.TAG;
        }
    }

    /*
    static {
        String simpleName = WorkoutSessionLocalDataSource.class.getSimpleName();
        Wg6.b(simpleName, "WorkoutSessionLocalDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public WorkoutSessionLocalDataSource(WorkoutSessionRepository workoutSessionRepository2, FitnessDatabase fitnessDatabase, Date date, U04 u04, PagingRequestHelper.Ai ai) {
        Wg6.c(workoutSessionRepository2, "workoutSessionRepository");
        Wg6.c(fitnessDatabase, "mFitnessDatabase");
        Wg6.c(date, "currentDate");
        Wg6.c(u04, "appExecutors");
        Wg6.c(ai, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.mFitnessDatabase = fitnessDatabase;
        this.currentDate = date;
        this.listener = ai;
        PagingRequestHelper pagingRequestHelper = new PagingRequestHelper(u04.a());
        this.mHelper = pagingRequestHelper;
        this.mNetworkState = Gl5.b(pagingRequestHelper);
        this.mHelper.a(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<WorkoutSession> getDataInDatabase(int i) {
        return this.mFitnessDatabase.getWorkoutDao().getWorkoutSessionsInDateDesc(this.currentDate, i);
    }

    @DexIgnore
    private final Rm6 loadData(PagingRequestHelper.Bi.Aii aii, int i) {
        return Gu7.d(Jv7.a(Bw7.b()), null, null, new WorkoutSessionLocalDataSource$loadData$Anon1(this, i, aii, null), 3, null);
    }

    @DexIgnore
    public static /* synthetic */ Rm6 loadData$default(WorkoutSessionLocalDataSource workoutSessionLocalDataSource, PagingRequestHelper.Bi.Aii aii, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        return workoutSessionLocalDataSource.loadData(aii, i);
    }

    @DexIgnore
    public Long getKey(WorkoutSession workoutSession) {
        Wg6.c(workoutSession, "item");
        return Long.valueOf(workoutSession.getCreatedAt());
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Ye
    public /* bridge */ /* synthetic */ Long getKey(WorkoutSession workoutSession) {
        return getKey(workoutSession);
    }

    @DexIgnore
    public final PagingRequestHelper getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    @Override // com.mapped.Xe
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().h();
        return super.isInvalid();
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadAfter(Ye.Fi<Long> fi, Ye.Ai<WorkoutSession> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - params = " + ((Object) fi.a) + " & currentDate = " + this.currentDate);
        WorkoutDao workoutDao = this.mFitnessDatabase.getWorkoutDao();
        Date date = this.currentDate;
        Key key = fi.a;
        Wg6.b(key, "params.key");
        ai.a(workoutDao.getWorkoutSessionsInDateAfterDesc(date, key.longValue(), fi.b));
        this.mHelper.h(PagingRequestHelper.Di.AFTER, new WorkoutSessionLocalDataSource$loadAfter$Anon1(this));
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadBefore(Ye.Fi<Long> fi, Ye.Ai<WorkoutSession> ai) {
        Wg6.c(fi, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ai, Constants.CALLBACK);
    }

    @DexIgnore
    @Override // com.mapped.Ye
    public void loadInitial(Ye.Ei<Long> ei, Ye.Ci<WorkoutSession> ci) {
        Wg6.c(ei, NativeProtocol.WEB_DIALOG_PARAMS);
        Wg6.c(ci, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - currentDate = " + this.currentDate);
        ci.a(getDataInDatabase(ei.b));
        this.mHelper.h(PagingRequestHelper.Di.INITIAL, new WorkoutSessionLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.f(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().j(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(PagingRequestHelper pagingRequestHelper) {
        Wg6.c(pagingRequestHelper, "<set-?>");
        this.mHelper = pagingRequestHelper;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        Wg6.c(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }
}
