package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.portfolio.platform.data.model.watchface.DianaWatchFaceTemplate;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaWatchFaceTemplateDao_Impl implements DianaWatchFaceTemplateDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<DianaWatchFaceTemplate> __deletionAdapterOfDianaWatchFaceTemplate;
    @DexIgnore
    public /* final */ Hh<DianaWatchFaceTemplate> __insertionAdapterOfDianaWatchFaceTemplate;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<DianaWatchFaceTemplate> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            if (dianaWatchFaceTemplate.getCreatedAt() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaWatchFaceTemplate.getCreatedAt());
            }
            if (dianaWatchFaceTemplate.getUpdatedAt() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, dianaWatchFaceTemplate.getUpdatedAt());
            }
            if (dianaWatchFaceTemplate.getId() == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, dianaWatchFaceTemplate.getId());
            }
            if (dianaWatchFaceTemplate.getDownloadURL() == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, dianaWatchFaceTemplate.getDownloadURL());
            }
            if (dianaWatchFaceTemplate.getChecksum() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, dianaWatchFaceTemplate.getChecksum());
            }
            if (dianaWatchFaceTemplate.getFirmwareOSVersion() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, dianaWatchFaceTemplate.getFirmwareOSVersion());
            }
            if (dianaWatchFaceTemplate.getPackageVersion() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, dianaWatchFaceTemplate.getPackageVersion());
            }
            if (dianaWatchFaceTemplate.getThemeClass() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, dianaWatchFaceTemplate.getThemeClass());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            bind(mi, dianaWatchFaceTemplate);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `dianaWatchfaceTemplate` (`createdAt`,`updatedAt`,`id`,`downloadURL`,`checksum`,`firmwareOSVersion`,`packageVersion`,`themeClass`) VALUES (?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<DianaWatchFaceTemplate> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            if (dianaWatchFaceTemplate.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, dianaWatchFaceTemplate.getId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, DianaWatchFaceTemplate dianaWatchFaceTemplate) {
            bind(mi, dianaWatchFaceTemplate);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `dianaWatchfaceTemplate` WHERE `id` = ?";
        }
    }

    @DexIgnore
    public DianaWatchFaceTemplateDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfDianaWatchFaceTemplate = new Anon1(oh);
        this.__deletionAdapterOfDianaWatchFaceTemplate = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public void deleteDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfDianaWatchFaceTemplate.handle(dianaWatchFaceTemplate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public List<DianaWatchFaceTemplate> getAllDianaWatchFaceTemplate() {
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceTemplate", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "id");
            int c4 = Dx0.c(b, "downloadURL");
            int c5 = Dx0.c(b, "checksum");
            int c6 = Dx0.c(b, "firmwareOSVersion");
            int c7 = Dx0.c(b, "packageVersion");
            int c8 = Dx0.c(b, "themeClass");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                DianaWatchFaceTemplate dianaWatchFaceTemplate = new DianaWatchFaceTemplate(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8));
                dianaWatchFaceTemplate.setCreatedAt(b.getString(c));
                dianaWatchFaceTemplate.setUpdatedAt(b.getString(c2));
                arrayList.add(dianaWatchFaceTemplate);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public DianaWatchFaceTemplate getDianaWatchFaceTemplateById(String str) {
        DianaWatchFaceTemplate dianaWatchFaceTemplate = null;
        Rh f = Rh.f("SELECT * FROM dianaWatchfaceTemplate WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "createdAt");
            int c2 = Dx0.c(b, "updatedAt");
            int c3 = Dx0.c(b, "id");
            int c4 = Dx0.c(b, "downloadURL");
            int c5 = Dx0.c(b, "checksum");
            int c6 = Dx0.c(b, "firmwareOSVersion");
            int c7 = Dx0.c(b, "packageVersion");
            int c8 = Dx0.c(b, "themeClass");
            if (b.moveToFirst()) {
                dianaWatchFaceTemplate = new DianaWatchFaceTemplate(b.getString(c3), b.getString(c4), b.getString(c5), b.getString(c6), b.getString(c7), b.getString(c8));
                dianaWatchFaceTemplate.setCreatedAt(b.getString(c));
                dianaWatchFaceTemplate.setUpdatedAt(b.getString(c2));
            }
            return dianaWatchFaceTemplate;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.DianaWatchFaceTemplateDao
    public void upsertDianaWatchFaceTemplate(DianaWatchFaceTemplate dianaWatchFaceTemplate) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfDianaWatchFaceTemplate.insert((Hh<DianaWatchFaceTemplate>) dianaWatchFaceTemplate);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
