package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummaryLocalDataSource$loadInitial$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore
    public GoalTrackingSummaryLocalDataSource$loadInitial$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource) {
        this.this$0 = goalTrackingSummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource = this.this$0;
        goalTrackingSummaryLocalDataSource.calculateStartDate(goalTrackingSummaryLocalDataSource.mCreatedDate);
        GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource2 = this.this$0;
        PagingRequestHelper.Di di = PagingRequestHelper.Di.INITIAL;
        Date mStartDate = goalTrackingSummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        Wg6.b(aii, "helperCallback");
        Rm6 unused = goalTrackingSummaryLocalDataSource2.loadData(di, mStartDate, mEndDate, aii);
    }
}
