package com.portfolio.platform.data.source.local.diana.workout;

import com.mapped.PagingRequestHelper;
import com.mapped.Rm6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ WorkoutSessionLocalDataSource this$0;

    @DexIgnore
    public WorkoutSessionLocalDataSource$loadAfter$Anon1(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.this$0 = workoutSessionLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.this$0;
        Wg6.b(aii, "helperCallback");
        Rm6 unused = workoutSessionLocalDataSource.loadData(aii, this.this$0.mOffset);
    }
}
