package com.portfolio.platform.data.source.local.sleep;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1$sleepSessionsDeferred$1", f = "SleepSummaryLocalDataSource.kt", l = {176}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Lc6 $downloadingRange;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource$loadData$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1, Lc6 lc6, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummaryLocalDataSource$loadData$Anon1;
        this.$downloadingRange = lc6;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2 = new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(this.this$0, this.$downloadingRange, xe6);
        sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2.p$ = (Il6) obj;
        throw null;
        //return sleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
        throw null;
        //return ((SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            this.L$0 = this.p$;
            this.label = 1;
            Object fetchSleepSessions$default = SleepSessionsRepository.fetchSleepSessions$default(this.this$0.this$0.mSleepSessionsRepository, (Date) this.$downloadingRange.getFirst(), (Date) this.$downloadingRange.getSecond(), 0, 0, this, 12, null);
            return fetchSleepSessions$default == d ? d : fetchSleepSessions$default;
        } else if (i == 1) {
            Il6 il6 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
