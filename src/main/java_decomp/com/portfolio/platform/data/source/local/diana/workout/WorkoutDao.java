package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface WorkoutDao {
    @DexIgnore
    Object deleteAllWorkoutSession();  // void declaration

    @DexIgnore
    void deleteWorkoutSessionById(String str);

    @DexIgnore
    WorkoutSession getWorkoutSessionById(String str);

    @DexIgnore
    LiveData<List<WorkoutSession>> getWorkoutSessions(Date date, Date date2);

    @DexIgnore
    LiveData<List<WorkoutSession>> getWorkoutSessionsDesc(Date date, Date date2);

    @DexIgnore
    List<WorkoutSession> getWorkoutSessionsInDate(Date date);

    @DexIgnore
    List<WorkoutSession> getWorkoutSessionsInDateAfterDesc(Date date, long j, int i);

    @DexIgnore
    List<WorkoutSession> getWorkoutSessionsInDateDesc(Date date, int i);

    @DexIgnore
    List<WorkoutSession> getWorkoutSessionsRaw(Date date, Date date2);

    @DexIgnore
    void insertWorkoutSession(WorkoutSession workoutSession);

    @DexIgnore
    void upsertListWorkoutSession(List<WorkoutSession> list);

    @DexIgnore
    void upsertWorkoutSession(WorkoutSession workoutSession);
}
