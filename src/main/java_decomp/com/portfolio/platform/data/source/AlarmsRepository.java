package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmsRemoteDataSource mAlarmRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRepository.class.getSimpleName();
        Wg6.b(simpleName, "AlarmsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRepository(AlarmsRemoteDataSource alarmsRemoteDataSource) {
        Wg6.c(alarmsRemoteDataSource, "mAlarmRemoteDataSource");
        this.mAlarmRemoteDataSource = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new AlarmsRepository$cleanUp$Anon2(null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object deleteAlarm(Alarm alarm, Xe6<? super Ap4<? extends Void>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$deleteAlarm$Anon2(this, alarm, null), xe6);
    }

    @DexIgnore
    public final Object downloadAlarms(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new AlarmsRepository$downloadAlarms$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object executePendingRequest(Xe6<? super Boolean> xe6) {
        Object g;
        synchronized (this) {
            g = Eu7.g(Bw7.b(), new AlarmsRepository$executePendingRequest$Anon2(this, null), xe6);
        }
        return g;
    }

    @DexIgnore
    public final Object findIncomingActiveAlarm(Xe6<? super Alarm> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$findIncomingActiveAlarm$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object findNextActiveAlarm(Xe6<? super Alarm> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$findNextActiveAlarm$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object getActiveAlarms(Xe6<? super List<Alarm>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$getActiveAlarms$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object getAlarmById(String str, Xe6<? super Alarm> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$getAlarmById$Anon2(str, null), xe6);
    }

    @DexIgnore
    public final Object getAllAlarmIgnoreDeletePinType(Xe6<? super List<Alarm>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$getAllAlarmIgnoreDeletePinType$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object getInComingActiveAlarms(Xe6<? super List<Alarm>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$getInComingActiveAlarms$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object insertAlarm(Alarm alarm, Xe6<? super Ap4<Alarm>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$insertAlarm$Anon2(this, alarm, null), xe6);
    }

    @DexIgnore
    public final Object updateAlarm(Alarm alarm, Xe6<? super Ap4<Alarm>> xe6) {
        return Eu7.g(Bw7.b(), new AlarmsRepository$updateAlarm$Anon2(this, alarm, null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm r12, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.local.alarm.Alarm>> r13) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.AlarmsRepository.upsertAlarm(com.portfolio.platform.data.source.local.alarm.Alarm, com.mapped.Xe6):java.lang.Object");
    }
}
