package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.HeartRateSampleRepository$insertFromDevice$2", f = "HeartRateSampleRepository.kt", l = {135}, m = "invokeSuspend")
public final class HeartRateSampleRepository$insertFromDevice$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $heartRateSamples;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public HeartRateSampleRepository$insertFromDevice$Anon2(List list, Xe6 xe6) {
        super(2, xe6);
        this.$heartRateSamples = list;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        HeartRateSampleRepository$insertFromDevice$Anon2 heartRateSampleRepository$insertFromDevice$Anon2 = new HeartRateSampleRepository$insertFromDevice$Anon2(this.$heartRateSamples, xe6);
        heartRateSampleRepository$insertFromDevice$Anon2.p$ = (Il6) obj;
        throw null;
        //return heartRateSampleRepository$insertFromDevice$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((HeartRateSampleRepository$insertFromDevice$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object y;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = HeartRateSampleRepository.TAG;
            local.d(str, "insertFromDevice: heartRateSamples = " + this.$heartRateSamples);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.label = 1;
            y = encryptedDatabaseManager.y(this);
            if (y == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            y = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((FitnessDatabase) y).getHeartRateDao().insertHeartRateSamples(this.$heartRateSamples);
        return Cd6.a;
    }
}
