package com.portfolio.platform.data.source.remote;

import com.fossil.I98;
import com.fossil.J98;
import com.fossil.Q88;
import com.fossil.S98;
import com.fossil.U98;
import com.fossil.X98;
import com.mapped.Ku3;
import com.mapped.Ny6;
import com.mapped.Uy6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.Device;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface SecureApiService2Dot1 {
    @DexIgnore
    @J98("users/me/devices/{deviceId}")
    Object deleteDevice(@X98("deviceId") String str, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Uy6("rpc/device/generate-pairing-key")
    Object generatePairingKey(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/devices/{deviceId}")
    Object getDevice(@X98("deviceId") String str, Xe6<? super Q88<Device>> xe6);

    @DexIgnore
    @Ny6("users/me/devices/{id}/secret-key")
    Object getDeviceSecretKey(@X98("id") String str, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/device-secret-keys")
    Object getDeviceSecretKeys(Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @Ny6("users/me/devices")
    Object getDevices(Xe6<? super Q88<ApiResponse<Device>>> xe6);

    @DexIgnore
    @Ny6("users/me/devices/latest-active")
    Object getLastActiveDevice(Xe6<? super Q88<Device>> xe6);

    @DexIgnore
    @Ny6("users/me/device-secret-keys/{id}")
    Object getSecretKey(@X98("id") String str, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @U98("users/me/devices")
    Object linkDevice(@I98 Device device, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @Uy6("rpc/device/swap-pairing-keys")
    Object swapPairingKey(@I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @S98("users/me/devices/{deviceId}")
    Object updateDevice(@X98("deviceId") String str, @I98 Device device, Xe6<? super Q88<Void>> xe6);

    @DexIgnore
    @S98("users/me/devices/{id}/secret-key")
    Object updateDeviceSecretKey(@X98("id") String str, @I98 Ku3 ku3, Xe6<? super Q88<Ku3>> xe6);

    @DexIgnore
    @S98("users/me/device-secret-keys/{id}")
    Object updateSecretKey(@X98("id") String str, Xe6<? super Q88<Ku3>> xe6);
}
