package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Kc6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.fitness.ActivityRecommendedGoals;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$downloadRecommendedGoals$2", f = "SummariesRepository.kt", l = {388}, m = "invokeSuspend")
public final class SummariesRepository$downloadRecommendedGoals$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ActivityRecommendedGoals>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $age;
    @DexIgnore
    public /* final */ /* synthetic */ String $gender;
    @DexIgnore
    public /* final */ /* synthetic */ int $heightInCentimeters;
    @DexIgnore
    public /* final */ /* synthetic */ int $weightInGrams;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$downloadRecommendedGoals$Anon2(SummariesRepository summariesRepository, int i, int i2, int i3, String str, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
        this.$age = i;
        this.$weightInGrams = i2;
        this.$heightInCentimeters = i3;
        this.$gender = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$downloadRecommendedGoals$Anon2 summariesRepository$downloadRecommendedGoals$Anon2 = new SummariesRepository$downloadRecommendedGoals$Anon2(this.this$0, this.$age, this.$weightInGrams, this.$heightInCentimeters, this.$gender, xe6);
        summariesRepository$downloadRecommendedGoals$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$downloadRecommendedGoals$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<ActivityRecommendedGoals>> xe6) {
        throw null;
        //return ((SummariesRepository$downloadRecommendedGoals$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Object d2 = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2 = new SummariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = il6;
            this.label = 1;
            d = ResponseKt.d(summariesRepository$downloadRecommendedGoals$Anon2$response$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            d = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Ap4 ap4 = (Ap4) d;
        if (ap4 instanceof Kq5) {
            Object a2 = ((Kq5) ap4).a();
            if (a2 != null) {
                return new Kq5((ActivityRecommendedGoals) a2, false, 2, null);
            }
            Wg6.i();
            throw null;
        } else if (ap4 instanceof Hq5) {
            Hq5 hq5 = (Hq5) ap4;
            return new Hq5(hq5.a(), hq5.c(), null, null, null, 16, null);
        } else {
            throw new Kc6();
        }
    }
}
