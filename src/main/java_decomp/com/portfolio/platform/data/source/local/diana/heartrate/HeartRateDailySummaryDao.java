package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.facebook.places.internal.LocationScannerImpl;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class HeartRateDailySummaryDao {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateDailySummaryDao.class.getSimpleName();
        Wg6.b(simpleName, "HeartRateDailySummaryDao::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    private final void calculateDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary, DailyHeartRateSummary dailyHeartRateSummary2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateDailyHeartRateSummary - currentSummary=" + dailyHeartRateSummary + ", newSummary=" + dailyHeartRateSummary2);
        if (dailyHeartRateSummary.getAverage() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            int minuteCount = dailyHeartRateSummary.getMinuteCount() + dailyHeartRateSummary2.getMinuteCount();
            dailyHeartRateSummary2.setAverage(((dailyHeartRateSummary2.getAverage() * ((float) dailyHeartRateSummary2.getMinuteCount())) + (dailyHeartRateSummary.getAverage() * ((float) dailyHeartRateSummary.getMinuteCount()))) / ((float) minuteCount));
            dailyHeartRateSummary2.setMinuteCount(minuteCount);
        }
        int min = Math.min(dailyHeartRateSummary2.getMin(), dailyHeartRateSummary.getMin());
        int max = Math.max(dailyHeartRateSummary2.getMax(), dailyHeartRateSummary.getMax());
        dailyHeartRateSummary2.setDate(dailyHeartRateSummary.getDate());
        dailyHeartRateSummary2.setCreatedAt(dailyHeartRateSummary.getCreatedAt());
        dailyHeartRateSummary2.setUpdatedAt(System.currentTimeMillis());
        dailyHeartRateSummary2.setMin(min);
        dailyHeartRateSummary2.setMax(max);
        Resting resting = dailyHeartRateSummary2.getResting();
        if (resting == null) {
            resting = dailyHeartRateSummary.getResting();
        }
        dailyHeartRateSummary2.setResting(resting);
    }

    @DexIgnore
    public abstract void deleteAllHeartRateSummaries();

    @DexIgnore
    public abstract List<DailyHeartRateSummary> getDailyHeartRateSummariesDesc(Date date, Date date2);

    @DexIgnore
    public abstract LiveData<List<DailyHeartRateSummary>> getDailyHeartRateSummariesLiveData(Date date, Date date2);

    @DexIgnore
    public abstract DailyHeartRateSummary getDailyHeartRateSummary(Date date);

    @DexIgnore
    public abstract Date getLastDate();

    @DexIgnore
    public abstract Xe.Bi<Integer, DailyHeartRateSummary> getSummariesDataSource();

    @DexIgnore
    public abstract void insertDailyHeartRateSummary(DailyHeartRateSummary dailyHeartRateSummary);

    @DexIgnore
    public final void insertHeartRateSummaries(List<DailyHeartRateSummary> list) {
        Wg6.c(list, "heartRateSummaries");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.e(str, "insertHeartRateSummaries - summarySize=" + list.size());
        for (T t : list) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.e(str2, "addHeartRateSummary - summary=" + ((Object) t));
            DailyHeartRateSummary dailyHeartRateSummary = getDailyHeartRateSummary(t.getDate());
            if (dailyHeartRateSummary != null) {
                calculateDailyHeartRateSummary(dailyHeartRateSummary, t);
            }
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local3.d(str3, "addHeartRateSummary - after calculate to summary=" + ((Object) t));
        }
        insertListDailyHeartRateSummary(list);
    }

    @DexIgnore
    public abstract void insertListDailyHeartRateSummary(List<DailyHeartRateSummary> list);
}
