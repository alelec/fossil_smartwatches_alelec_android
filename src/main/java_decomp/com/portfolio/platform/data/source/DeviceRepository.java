package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.Vt7;
import com.mapped.Ap4;
import com.mapped.Ku3;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.source.remote.DeviceRemoteDataSource;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ DeviceDao mDeviceDao;
    @DexIgnore
    public /* final */ DeviceRemoteDataSource mDeviceRemoteDataSource;
    @DexIgnore
    public /* final */ SkuDao mSkuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String getTAG() {
            return DeviceRepository.TAG;
        }
    }

    /*
    static {
        String simpleName = DeviceRepository.class.getSimpleName();
        Wg6.b(simpleName, "DeviceRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRepository(DeviceDao deviceDao, SkuDao skuDao, DeviceRemoteDataSource deviceRemoteDataSource) {
        Wg6.c(deviceDao, "mDeviceDao");
        Wg6.c(skuDao, "mSkuDao");
        Wg6.c(deviceRemoteDataSource, "mDeviceRemoteDataSource");
        this.mDeviceDao = deviceDao;
        this.mSkuDao = skuDao;
        this.mDeviceRemoteDataSource = deviceRemoteDataSource;
    }

    @DexIgnore
    public static /* synthetic */ Object downloadSupportedSku$default(DeviceRepository deviceRepository, int i, Xe6 xe6, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = 0;
        }
        return deviceRepository.downloadSupportedSku(i, xe6);
    }

    @DexIgnore
    private final boolean isHasDevice(List<Device> list, String str) {
        if (list != null) {
            if ((list instanceof Collection) && list.isEmpty()) {
                return false;
            }
            Iterator<T> it = list.iterator();
            while (it.hasNext()) {
                if (Vt7.j(it.next().getDeviceId(), str, true)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mDeviceDao.cleanUp();
        this.mSkuDao.cleanUpSku();
        this.mSkuDao.cleanUpWatchParam();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadDeviceList(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.source.remote.ApiResponse<com.portfolio.platform.data.model.Device>>> r8) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r8 instanceof com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1
            if (r0 == 0) goto L_0x002f
            r0 = r8
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x002f
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0015:
            java.lang.Object r3 = r2.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x006f
            if (r0 == r5) goto L_0x003e
            if (r0 != r6) goto L_0x0036
            java.lang.Object r0 = r2.L$1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            java.lang.Object r1 = r2.L$0
            com.portfolio.platform.data.source.DeviceRepository r1 = (com.portfolio.platform.data.source.DeviceRepository) r1
            com.fossil.El7.b(r3)
        L_0x002e:
            return r0
        L_0x002f:
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon1
            r0.<init>(r7, r8)
            r2 = r0
            goto L_0x0015
        L_0x0036:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003e:
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.El7.b(r3)
            r1 = r3
            r7 = r0
        L_0x0047:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x002e
            r1 = r0
            com.fossil.Kq5 r1 = (com.fossil.Kq5) r1
            boolean r1 = r1.b()
            if (r1 != 0) goto L_0x002e
            com.fossil.Dv7 r1 = com.fossil.Bw7.a()
            com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon2 r3 = new com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$Anon2
            r5 = 0
            r3.<init>(r7, r0, r5)
            r2.L$0 = r7
            r2.L$1 = r0
            r2.label = r6
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r3, r2)
            if (r1 != r4) goto L_0x002e
            r0 = r4
            goto L_0x002e
        L_0x006f:
            com.fossil.El7.b(r3)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = r7.mDeviceRemoteDataSource
            r2.L$0 = r7
            r2.label = r5
            java.lang.Object r1 = r0.getAllDevice(r2)
            if (r1 != r4) goto L_0x0047
            r0 = r4
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.downloadDeviceList(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadSupportedSku(int r11, com.mapped.Xe6<? super com.mapped.Cd6> r12) {
        /*
            r10 = this;
            r9 = 2
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 0
            r8 = 1
            boolean r0 = r12 instanceof com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1
            if (r0 == 0) goto L_0x0034
            r0 = r12
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0034
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0016:
            java.lang.Object r1 = r3.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x00b8
            if (r0 == r8) goto L_0x0043
            if (r0 != r9) goto L_0x003b
            java.lang.Object r0 = r3.L$1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            int r0 = r3.I$0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.El7.b(r1)
        L_0x0031:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0033:
            return r0
        L_0x0034:
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon1
            r0.<init>(r10, r12)
            r3 = r0
            goto L_0x0016
        L_0x003b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0043:
            int r11 = r3.I$0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.data.source.DeviceRepository r0 = (com.portfolio.platform.data.source.DeviceRepository) r0
            com.fossil.El7.b(r1)
            r10 = r0
        L_0x004d:
            r0 = r1
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "downloadSupportedSku() - offset = "
            r6.append(r7)
            r6.append(r11)
            java.lang.String r6 = r6.toString()
            r1.d(r2, r6)
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0031
            r1 = r0
            com.fossil.Kq5 r1 = (com.fossil.Kq5) r1
            boolean r2 = r1.b()
            if (r2 != 0) goto L_0x0031
            com.portfolio.platform.data.source.SkuDao r6 = r10.mSkuDao
            java.lang.Object r2 = r1.a()
            com.portfolio.platform.data.source.remote.ApiResponse r2 = (com.portfolio.platform.data.source.remote.ApiResponse) r2
            if (r2 == 0) goto L_0x00cc
            java.util.List r2 = r2.get_items()
        L_0x0087:
            if (r2 == 0) goto L_0x00ce
            r6.addOrUpdateSkuList(r2)
            java.lang.Object r1 = r1.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            com.portfolio.platform.data.model.Range r1 = r1.get_range()
            if (r1 == 0) goto L_0x0031
            boolean r1 = r1.isHasNext()
            if (r1 != r8) goto L_0x0031
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2 r2 = new com.portfolio.platform.data.source.DeviceRepository$downloadSupportedSku$Anon2
            r2.<init>(r10, r11, r5)
            r3.L$0 = r10
            r3.I$0 = r11
            r3.L$1 = r0
            r3.label = r9
            java.lang.Object r0 = com.fossil.Eu7.g(r1, r2, r3)
            if (r0 != r4) goto L_0x0031
            r0 = r4
            goto L_0x0033
        L_0x00b8:
            com.fossil.El7.b(r1)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r0 = r10.mDeviceRemoteDataSource
            r3.L$0 = r10
            r3.I$0 = r11
            r3.label = r8
            java.lang.Object r1 = r0.getSupportedSku(r11, r3)
            if (r1 != r4) goto L_0x004d
            r0 = r4
            goto L_0x0033
        L_0x00cc:
            r2 = r5
            goto L_0x0087
        L_0x00ce:
            com.mapped.Wg6.i()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku(int, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00ca  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object forceLinkDevice(com.portfolio.platform.data.model.Device r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.forceLinkDevice(com.portfolio.platform.data.model.Device, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object generatePairingKey(String str, Xe6<? super Ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.generatePairingKey(str, xe6);
    }

    @DexIgnore
    public final List<Device> getAllDevice() {
        return this.mDeviceDao.getAllDevice();
    }

    @DexIgnore
    public final LiveData<List<Device>> getAllDeviceAsLiveData() {
        return this.mDeviceDao.getAllDeviceAsLiveData();
    }

    @DexIgnore
    public final Device getDeviceBySerial(String str) {
        Wg6.c(str, "serial");
        return this.mDeviceDao.getDeviceByDeviceId(str);
    }

    @DexIgnore
    public final LiveData<Device> getDeviceBySerialAsLiveData(String str) {
        Wg6.c(str, "serial");
        return this.mDeviceDao.getDeviceBySerialAsLiveData(str);
    }

    @DexIgnore
    public final String getDeviceNameBySerial(String str) {
        Wg6.c(str, "serial");
        SKUModel skuByDeviceIdPrefix = this.mSkuDao.getSkuByDeviceIdPrefix(DeviceHelper.o.m(str));
        if (skuByDeviceIdPrefix == null || TextUtils.isEmpty(skuByDeviceIdPrefix.getDeviceName())) {
            return (TextUtils.isEmpty(str) || str.length() < 6) ? "UNKNOWN" : DeviceIdentityUtils.isDianaDevice(str) ? "Hybrid HR" : "Hybrid Smartwatch";
        }
        String deviceName = skuByDeviceIdPrefix.getDeviceName();
        if (deviceName != null) {
            return deviceName;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final Object getDeviceSecretKey(String str, Xe6<? super Ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.getDeviceSecretKey(str, xe6);
    }

    @DexIgnore
    public final Object getLatestWatchParamFromServer(String str, int i, Xe6<? super WatchParameterResponse> xe6) {
        return this.mDeviceRemoteDataSource.getLatestWatchParamFromServer(str, i, xe6);
    }

    @DexIgnore
    public final SKUModel getSkuModelBySerialPrefix(String str) {
        Wg6.c(str, "prefix");
        return this.mSkuDao.getSkuByDeviceIdPrefix(str);
    }

    @DexIgnore
    public final List<SKUModel> getSupportedSku() {
        return this.mSkuDao.getAllSkus();
    }

    @DexIgnore
    public final WatchParam getWatchParamBySerialId(String str) {
        Wg6.c(str, "serial");
        return this.mSkuDao.getWatchParamById(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008b, code lost:
        if (r2 == r0) goto L_0x004c;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object removeDevice(com.portfolio.platform.data.model.Device r8, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r9) {
        /*
            r7 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1
            if (r0 == 0) goto L_0x004d
            r0 = r9
            com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004d
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0014:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r3 = r1.label
            if (r3 == 0) goto L_0x005c
            if (r3 != r6) goto L_0x0054
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DeviceRepository r1 = (com.portfolio.platform.data.source.DeviceRepository) r1
            com.fossil.El7.b(r2)
            r7 = r1
            r8 = r0
        L_0x002d:
            r0 = r2
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 != 0) goto L_0x0043
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x004c
            r1 = r0
            com.fossil.Hq5 r1 = (com.fossil.Hq5) r1
            int r1 = r1.a()
            r2 = 404(0x194, float:5.66E-43)
            if (r1 != r2) goto L_0x004c
        L_0x0043:
            com.portfolio.platform.data.source.DeviceDao r1 = r7.mDeviceDao
            java.lang.String r2 = r8.getDeviceId()
            r1.removeDeviceByDeviceId(r2)
        L_0x004c:
            return r0
        L_0x004d:
            com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$removeDevice$Anon1
            r0.<init>(r7, r9)
            r1 = r0
            goto L_0x0014
        L_0x0054:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x005c:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Inside .removeDevice deviceId="
            r4.append(r5)
            java.lang.String r5 = r8.getDeviceId()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.d(r3, r4)
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r2 = r7.mDeviceRemoteDataSource
            r1.L$0 = r7
            r1.L$1 = r8
            r1.label = r6
            java.lang.Object r2 = r2.removeDevice(r8, r1)
            if (r2 != r0) goto L_0x002d
            goto L_0x004c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.removeDevice(com.portfolio.platform.data.model.Device, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object removeStealDevice(java.util.List<com.portfolio.platform.data.model.Device> r13, com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
            r12 = this;
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r11 = 1
            boolean r0 = r14 instanceof com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1
            if (r0 == 0) goto L_0x00a4
            r0 = r14
            com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x00a4
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0014:
            java.lang.Object r5 = r4.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x00b4
            if (r0 != r11) goto L_0x00ac
            java.lang.Object r0 = r4.L$4
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$3
            java.util.Iterator r0 = (java.util.Iterator) r0
            java.lang.Object r1 = r4.L$2
            java.util.List r1 = (java.util.List) r1
            java.lang.Object r2 = r4.L$1
            java.util.List r2 = (java.util.List) r2
            java.lang.Object r3 = r4.L$0
            com.portfolio.platform.data.source.DeviceRepository r3 = (com.portfolio.platform.data.source.DeviceRepository) r3
            com.fossil.El7.b(r5)
            r5 = r0
        L_0x0038:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00c6
            java.lang.Object r0 = r5.next()
            com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
            java.lang.String r0 = r0.component1()
            boolean r7 = r3.isHasDevice(r2, r0)
            if (r7 != 0) goto L_0x0038
            com.misfit.frameworks.buttonservice.log.FLogger r7 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r7 = r7.getLocal()
            java.lang.String r8 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Inside .removeStealDevice device="
            r9.append(r10)
            r9.append(r0)
            java.lang.String r10 = " was stealed, remove it on local"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            r7.d(r8, r9)
            com.portfolio.platform.data.source.DeviceDao r7 = r3.mDeviceDao
            r7.removeDeviceByDeviceId(r0)
            boolean r7 = android.text.TextUtils.isEmpty(r0)
            if (r7 != 0) goto L_0x0038
            com.portfolio.platform.PortfolioApp$inner r7 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r7 = r7.instance()
            java.lang.String r7 = r7.J()
            boolean r7 = com.fossil.Vt7.j(r0, r7, r11)
            if (r7 == 0) goto L_0x0038
            com.portfolio.platform.PortfolioApp$inner r7 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r7 = r7.instance()
            r4.L$0 = r3
            r4.L$1 = r2
            r4.L$2 = r1
            r4.L$3 = r5
            r4.L$4 = r0
            r4.label = r11
            java.lang.Object r0 = r7.G0(r4)
            if (r0 != r6) goto L_0x0038
            r0 = r6
        L_0x00a3:
            return r0
        L_0x00a4:
            com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$removeStealDevice$Anon1
            r0.<init>(r12, r14)
            r4 = r0
            goto L_0x0014
        L_0x00ac:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x00b4:
            com.fossil.El7.b(r5)
            com.portfolio.platform.data.source.DeviceDao r0 = r12.mDeviceDao
            java.util.List r1 = r0.getAllDevice()
            java.util.Iterator r0 = r1.iterator()
            r3 = r12
            r2 = r13
            r5 = r0
            goto L_0x0038
        L_0x00c6:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.removeStealDevice(java.util.List, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void saveWatchParamModel(WatchParam watchParam) {
        Wg6.c(watchParam, "watchParam");
        this.mSkuDao.addOrUpdateWatchParam(watchParam);
    }

    @DexIgnore
    public final Object swapPairingKey(String str, String str2, Xe6<? super Ap4<String>> xe6) {
        return this.mDeviceRemoteDataSource.swapPairingKey(str, str2, xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateDevice(com.portfolio.platform.data.model.Device r9, boolean r10, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r11) {
        /*
            r8 = this;
            r7 = 1
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r3 = 0
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1
            if (r0 == 0) goto L_0x0044
            r0 = r11
            com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1 r0 = (com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x0044
            int r1 = r1 + r4
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r4 = r1.label
            if (r4 == 0) goto L_0x0053
            if (r4 != r7) goto L_0x004b
            boolean r0 = r1.Z$0
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.model.Device r0 = (com.portfolio.platform.data.model.Device) r0
            java.lang.Object r1 = r1.L$0
            com.portfolio.platform.data.source.DeviceRepository r1 = (com.portfolio.platform.data.source.DeviceRepository) r1
            com.fossil.El7.b(r2)
            r9 = r0
        L_0x002f:
            r0 = r2
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            com.portfolio.platform.data.source.DeviceDao r2 = r1.mDeviceDao
            java.lang.String r3 = r9.getDeviceId()
            com.portfolio.platform.data.model.Device r2 = r2.getDeviceByDeviceId(r3)
            if (r2 == 0) goto L_0x00b8
            com.portfolio.platform.data.source.DeviceDao r1 = r1.mDeviceDao
            r1.addOrUpdateDevice(r2)
        L_0x0043:
            return r0
        L_0x0044:
            com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1 r0 = new com.portfolio.platform.data.source.DeviceRepository$updateDevice$Anon1
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x004b:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0053:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "updateDevice "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r6 = " isRemote "
            r5.append(r6)
            r5.append(r10)
            java.lang.String r5 = r5.toString()
            r2.d(r4, r5)
            if (r10 == 0) goto L_0x00c7
            java.lang.String r2 = r9.getDeviceId()
            boolean r2 = android.text.TextUtils.isEmpty(r2)
            if (r2 == 0) goto L_0x00a5
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.String r2 = "Inside .updateDevice can't update device without serial number"
            r0.d(r1, r2)
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            r1 = 600(0x258, float:8.41E-43)
            com.portfolio.platform.data.model.ServerError r2 = new com.portfolio.platform.data.model.ServerError
            r2.<init>()
            r6 = 24
            r4 = r3
            r5 = r3
            r7 = r3
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0043
        L_0x00a5:
            com.portfolio.platform.data.source.remote.DeviceRemoteDataSource r2 = r8.mDeviceRemoteDataSource
            r1.L$0 = r8
            r1.L$1 = r9
            r1.Z$0 = r10
            r1.label = r7
            java.lang.Object r2 = r2.updateDevice(r9, r1)
            if (r2 == r0) goto L_0x0043
            r1 = r8
            goto L_0x002f
        L_0x00b8:
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.DeviceRepository.TAG
            java.lang.String r3 = "device is removed, no need to update to DB"
            r1.d(r2, r3)
            goto L_0x0043
        L_0x00c7:
            java.lang.String r0 = r9.getDeviceId()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00d6
            com.portfolio.platform.data.source.DeviceDao r0 = r8.mDeviceDao
            r0.addOrUpdateDevice(r9)
        L_0x00d6:
            com.fossil.Kq5 r0 = new com.fossil.Kq5
            r1 = 0
            r2 = 2
            r0.<init>(r3, r1, r2, r3)
            goto L_0x0043
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.DeviceRepository.updateDevice(com.portfolio.platform.data.model.Device, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object updateDeviceSecretKey(String str, String str2, Xe6<? super Ap4<Ku3>> xe6) {
        return this.mDeviceRemoteDataSource.updateDeviceSecretKey(str, str2, xe6);
    }
}
