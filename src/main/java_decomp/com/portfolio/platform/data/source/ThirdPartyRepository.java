package com.portfolio.platform.data.source;

import com.facebook.internal.FacebookRequestErrorClassification;
import com.fossil.Bs7;
import com.fossil.Bw7;
import com.fossil.Dl7;
import com.fossil.Eu7;
import com.fossil.Go7;
import com.fossil.Gu7;
import com.fossil.H42;
import com.fossil.Hm7;
import com.fossil.Jv7;
import com.fossil.Lu7;
import com.fossil.Nt3;
import com.fossil.Oh2;
import com.fossil.Pm7;
import com.fossil.Rh2;
import com.fossil.Uh2;
import com.fossil.Ur7;
import com.fossil.Ux7;
import com.fossil.Wh2;
import com.fossil.Wi2;
import com.fossil.Xn7;
import com.fossil.Yn7;
import com.fossil.Zh2;
import com.fossil.fitness.WorkoutType;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.mapped.Cd6;
import com.mapped.G72;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Kk4;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.CoroutineExceptionHandler;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public /* final */ CoroutineExceptionHandler mExceptionHandling; // = new ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(CoroutineExceptionHandler.q);
    @DexIgnore
    public Kk4 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ Il6 mPushDataSupervisorJob; // = Jv7.a(Ux7.b(null, 1, null).plus(Bw7.b()).plus(this.mExceptionHandling));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        Object onComplete();  // void declaration
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0;

        /*
        static {
            int[] iArr = new int[WorkoutType.values().length];
            $EnumSwitchMapping$0 = iArr;
            iArr[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(Kk4 kk4, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        Wg6.c(kk4, "mGoogleFitHelper");
        Wg6.c(activitiesRepository, "mActivitiesRepository");
        Wg6.c(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = kk4;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession mFSleepSession : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(mFSleepSession.getRealSleepMinutes(), ((long) mFSleepSession.getRealStartTime()) * j, ((long) mFSleepSession.getRealEndTime()) * j));
        }
        return arrayList;
    }

    @DexIgnore
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.k);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        List a0 = Pm7.a0(gFitActiveTime.getActiveTimes());
        Ur7 l = Bs7.l(Bs7.m(0, a0.size()), 2);
        int a2 = l.a();
        int b = l.b();
        int c = l.c();
        if (c < 0 ? a2 >= b : a2 <= b) {
            while (true) {
                DataPoint h = f.h();
                h.o0(((Number) a0.get(a2)).longValue(), ((Number) a0.get(a2 + 1)).longValue(), TimeUnit.MILLISECONDS);
                h.L(Wh2.e).A("unknown");
                f.c(h);
                if (a2 == b) {
                    break;
                }
                a2 += c;
            }
        }
        Wg6.b(f, "dataSetForActiveMinutes");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForCalories samples " + list);
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.m);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        for (GFitSample gFitSample : list) {
            float component3 = gFitSample.component3();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            try {
                DataPoint h = f.h();
                h.o0(component4, component5, TimeUnit.MILLISECONDS);
                h.L(Wh2.J).D(component3);
                f.c(h);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(TAG, "add calories " + component3 + " as data point " + h);
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.e(TAG, "createDataSetForCalories() fall into error=" + e.getMessage());
            }
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        local4.d(TAG, "createDataSetForCalories " + f);
        Wg6.b(f, "dataSetForCalories");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.A);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        for (GFitSample gFitSample : list) {
            float component2 = gFitSample.component2();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint h = f.h();
            h.o0(component4, component5, TimeUnit.MILLISECONDS);
            h.L(Wh2.x).D(component2);
            f.c(h);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForDistances " + f);
        Wg6.b(f, "dataSetForDistances");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.x);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        for (GFitHeartRate gFitHeartRate : list) {
            float component1 = gFitHeartRate.component1();
            long component2 = gFitHeartRate.component2();
            long component3 = gFitHeartRate.component3();
            DataPoint h = f.h();
            h.o0(component2, component3, TimeUnit.MILLISECONDS);
            h.L(Wh2.s).D(component1);
            f.c(h);
        }
        Wg6.b(f, "dataSet");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.f);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        for (GFitSample gFitSample : list) {
            int component1 = gFitSample.component1();
            long component4 = gFitSample.component4();
            long component5 = gFitSample.component5();
            DataPoint h = f.h();
            h.o0(component4, component5, TimeUnit.MILLISECONDS);
            h.L(Wh2.h).F(component1);
            f.c(h);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForSteps " + f);
        Wg6.b(f, "dataSetForSteps");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOCalories steps " + list + " startTime " + j + " endTime " + j2);
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.m);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        try {
            for (GFitWOCalorie gFitWOCalorie : list) {
                if (gFitWOCalorie.getStartTime() >= j && gFitWOCalorie.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOCalorie.getStartTime(), gFitWOCalorie.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(Wh2.J).D(gFitWOCalorie.getCalorie());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout calories data point " + e);
        }
        Wg6.b(f, "dataSetForCalories");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWODistances listDistances " + list + " startTime " + j + " endTime " + j2);
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.A);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        for (GFitWODistance gFitWODistance : list) {
            if (gFitWODistance.getStartTime() >= j && gFitWODistance.getEndTime() <= j2) {
                DataPoint h = f.h();
                h.o0(gFitWODistance.getStartTime(), gFitWODistance.getEndTime(), TimeUnit.MILLISECONDS);
                h.L(Wh2.x).D(gFitWODistance.getDistance());
                f.c(h);
            }
        }
        Wg6.b(f, "dataSetForDistances");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOHeartRates listHeartRates " + list + " startTime " + j + " endTime " + j2);
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.x);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        try {
            for (GFitWOHeartRate gFitWOHeartRate : list) {
                if (gFitWOHeartRate.getStartTime() >= j && gFitWOHeartRate.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOHeartRate.getStartTime(), gFitWOHeartRate.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(Wh2.s).D(gFitWOHeartRate.getHeartRate());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout heartrate data point " + e);
        }
        Wg6.b(f, "dataSet");
        return f;
    }

    @DexIgnore
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createDataSetForWOSteps steps " + list + " startTime " + j + " endTime " + j2);
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.f);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(new G72(this.mPortfolioApp.getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet f = DataSet.f(ai.a());
        try {
            for (GFitWOStep gFitWOStep : list) {
                if (gFitWOStep.getStartTime() >= j && gFitWOStep.getEndTime() <= j2) {
                    DataPoint h = f.h();
                    h.o0(gFitWOStep.getStartTime(), gFitWOStep.getEndTime(), TimeUnit.MILLISECONDS);
                    h.L(Wh2.h).F(gFitWOStep.getStep());
                    f.c(h);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            local2.d(TAG, "exception when add workout steps data point " + e);
        }
        Wg6.b(f, "dataSetForSteps");
        return f;
    }

    @DexIgnore
    private final Wi2 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(TAG, "createWorkoutSession dataSetForSteps " + createDataSetForWOSteps + " dataSetForCalories " + createDataSetForWOCalories + " dataSetForDistances " + createDataSetForWODistances + " dataSetForHeartRates " + createDataSetForWOHeartRates);
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        Zh2.Ai ai = new Zh2.Ai();
        ai.f(workoutType.name());
        ai.b(getFitnessActivitiesType(workoutType));
        ai.g(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        ai.d(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        Zh2 a2 = ai.a();
        Wi2.Ai ai2 = new Wi2.Ai();
        ai2.c(a2);
        if (!createDataSetForWOSteps.isEmpty()) {
            ai2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            ai2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            ai2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            ai2.a(createDataSetForWOHeartRates);
        }
        Wi2 b = ai2.b();
        Wg6.b(b, "sessionBuilder.build()");
        return b;
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "walking.treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return FacebookRequestErrorClassification.KEY_OTHER;
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        return i < WorkoutType.values().length ? WorkoutType.values()[i] : WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ Object saveData$default(ThirdPartyRepository thirdPartyRepository, List list, GFitActiveTime gFitActiveTime, List list2, List list3, List list4, Xe6 xe6, int i, Object obj) {
        return thirdPartyRepository.saveData((i & 1) != 0 ? Hm7.e() : list, (i & 2) != 0 ? null : gFitActiveTime, (i & 4) != 0 ? Hm7.e() : list2, (i & 8) != 0 ? Hm7.e() : list3, (i & 16) != 0 ? Hm7.e() : list4, xe6);
    }

    @DexIgnore
    public static /* synthetic */ Object uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, Xe6 xe6, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback, xe6);
    }

    @DexIgnore
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, G72 g72) {
        Wg6.c(gFitSleep, "gFitSleep");
        Wg6.c(g72, "device");
        Uh2.Ai ai = new Uh2.Ai();
        ai.b(this.mPortfolioApp);
        ai.d(DataType.k);
        ai.f(this.mPortfolioApp.getString(2131887274));
        ai.g(0);
        ai.e(g72);
        DataSet f = DataSet.f(ai.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint h = f.h();
        h.o0(startTime, endTime, TimeUnit.MILLISECONDS);
        h.L(Wh2.e).F(gFitSleep.getSleepMins());
        f.c(h);
        Wg6.b(f, "dataSetForSleep");
        return f;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final Object pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, Xe6<? super Rm6> xe6) {
        return Eu7.g(Bw7.b(), new ThirdPartyRepository$pushPendingData$Anon2(this, pushPendingThirdPartyDataCallback, null), xe6);
    }

    @DexIgnore
    public final Object saveData(List<GFitSample> list, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list2, List<GFitWorkoutSession> list3, List<MFSleepSession> list4, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new ThirdPartyRepository$saveData$Anon2(this, list, gFitActiveTime, list2, list3, list4, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, Xe6<Object> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitActiveTimeToGoogleFit");
        GoogleSignInAccount b = H42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitActiveTimeToGoogleFit");
            if (lu7.isActive()) {
                Dl7.Ai ai = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(null));
            }
        } else {
            Rh2 a2 = Oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitActiveTime to Google Fit");
            int size = list.size();
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            for (T t : list) {
                DataSet createDataSetForActiveMinutes = createDataSetForActiveMinutes(t, str);
                List<DataPoint> k = createDataSetForActiveMinutes.k();
                Wg6.b(k, "dataSet.dataPoints");
                Iterator<T> it = k.iterator();
                while (it.hasNext()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "saveGFitActiveTimeToGoogleFit send data " + ((Object) it.next()));
                }
                Nt3<Void> s = a2.s(createDataSetForActiveMinutes);
                s.f(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(t, a2, hh6, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, hh6, size, lu7, this, list, str));
            }
        }
        Object t2 = lu7.t();
        if (t2 == Yn7.d()) {
            Go7.c(xe6);
        }
        return t2;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, Xe6<Object> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitHeartRateToGoogleFit");
        GoogleSignInAccount b = H42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitHeartRateToGoogleFit");
            if (lu7.isActive()) {
                Dl7.Ai ai = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(null));
            }
        } else {
            Rh2 a2 = Oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitHeartRate to Google Fit");
            List<List> A = Pm7.A(list, 500);
            int size = A.size();
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            for (List list2 : A) {
                DataSet createDataSetForHeartRates = createDataSetForHeartRates(list2, str);
                List<DataPoint> k = createDataSetForHeartRates.k();
                Wg6.b(k, "dataSet.dataPoints");
                Iterator<T> it = k.iterator();
                while (it.hasNext()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "saveGFitHeartRateToGoogleFit send data " + ((Object) it.next()));
                }
                Nt3<Void> s = a2.s(createDataSetForHeartRates);
                s.f(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(list2, a2, hh6, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a2, hh6, size, lu7, this, list, str));
            }
        }
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, Xe6<Object> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        GoogleSignInAccount b = H42.b(getMPortfolioApp());
        if (b == null) {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
            if (lu7.isActive()) {
                Dl7.Ai ai = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(null));
            }
        } else {
            Rh2 a2 = Oh2.a(getMPortfolioApp(), b);
            FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
            List<List> A = Pm7.A(list, 500);
            int size = A.size();
            Hh6 hh6 = new Hh6();
            int i = 0;
            hh6.element = 0;
            for (List list2 : A) {
                ArrayList arrayList = new ArrayList();
                DataSet createDataSetForSteps = createDataSetForSteps(list2, str);
                DataSet createDataSetForDistances = createDataSetForDistances(list2, str);
                DataSet createDataSetForCalories = createDataSetForCalories(list2, str);
                arrayList.add(createDataSetForSteps);
                arrayList.add(createDataSetForDistances);
                arrayList.add(createDataSetForCalories);
                int size2 = arrayList.size();
                Hh6 hh62 = new Hh6();
                hh62.element = i;
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    DataSet dataSet = (DataSet) it.next();
                    Wg6.b(dataSet, "dataSet");
                    List<DataPoint> k = dataSet.k();
                    Wg6.b(k, "dataSet.dataPoints");
                    if (!k.isEmpty()) {
                        List<DataPoint> k2 = dataSet.k();
                        Wg6.b(k2, "dataSet.dataPoints");
                        Iterator<T> it2 = k2.iterator();
                        while (it2.hasNext()) {
                            FLogger.INSTANCE.getLocal().d(TAG, "saveGFitSampleToGoogleFit send data " + ((Object) it2.next()));
                        }
                        Nt3<Void> s = a2.s(dataSet);
                        s.f(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(hh62, size2, list2, a2, hh6, lu7, size, this, list, str));
                        s.d(new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(size2, a2, hh6, lu7, size, this, list, str));
                        Wg6.b(s, "historyClient.insertData\u2026                        }");
                    } else {
                        hh6.element++;
                    }
                    i = 0;
                }
            }
        }
        Object t = lu7.t();
        if (t == Yn7.d()) {
            Go7.c(xe6);
        }
        return t;
    }

    @DexIgnore
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, Xe6<Object> xe6) {
        Lu7 lu7 = new Lu7(Xn7.c(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount b = H42.b(getMPortfolioApp());
        if (b != null) {
            int size = list.size();
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            G72 g72 = new G72(getMPortfolioApp().getString(2131887274), DeviceIdentityUtils.getNameBySerial(str), str, 3);
            ArrayList arrayList = new ArrayList();
            for (T t : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(t, g72);
                Zh2.Ai ai = new Zh2.Ai();
                ai.e(str + new DateTime());
                ai.f(getMPortfolioApp().getString(2131887274));
                ai.c("User Sleep");
                ai.g(t.getStartTime(), TimeUnit.MILLISECONDS);
                ai.d(t.getEndTime(), TimeUnit.MILLISECONDS);
                ai.b("sleep");
                Zh2 a2 = ai.a();
                Wi2.Ai ai2 = new Wi2.Ai();
                ai2.c(a2);
                ai2.a(createDataSetForSleepData);
                Nt3<Void> s = Oh2.b(getMPortfolioApp(), b).s(ai2.b());
                s.f(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(t, g72, b, hh6, arrayList, size, lu7, this, list, str));
                s.d(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(g72, b, hh6, arrayList, size, lu7, this, list, str));
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (lu7.isActive()) {
                Dl7.Ai ai3 = Dl7.Companion;
                lu7.resumeWith(Dl7.constructor-impl(null));
            }
        }
        Object t2 = lu7.t();
        if (t2 == Yn7.d()) {
            Go7.c(xe6);
        }
        return t2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0192  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object saveGFitWorkoutSessionToGoogleFit(java.util.List<com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession> r14, java.lang.String r15, com.mapped.Xe6<java.lang.Object> r16) {
        /*
        // Method dump skipped, instructions count: 449
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ThirdPartyRepository.saveGFitWorkoutSessionToGoogleFit(java.util.List, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        Wg6.c(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        Wg6.c(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final Object uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, Xe6<? super Rm6> xe6) {
        return Gu7.d(this.mPushDataSupervisorJob, null, null, new ThirdPartyRepository$uploadData$Anon2(this, pushPendingThirdPartyDataCallback, null), 3, null);
    }
}
