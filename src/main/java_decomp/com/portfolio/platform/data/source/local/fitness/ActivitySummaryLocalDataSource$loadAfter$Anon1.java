package com.portfolio.platform.data.source.local.fitness;

import com.fossil.Pm7;
import com.mapped.Lc6;
import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadAfter$Anon1 implements PagingRequestHelper.Bi {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadAfter$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    @Override // com.mapped.PagingRequestHelper.Bi
    public final void run(PagingRequestHelper.Bi.Aii aii) {
        Lc6 lc6 = (Lc6) Pm7.F(this.this$0.mRequestAfterQueue);
        Wg6.b(aii, "helperCallback");
        this.this$0.loadData(PagingRequestHelper.Di.AFTER, (Date) lc6.getFirst(), (Date) lc6.getSecond(), aii);
    }
}
