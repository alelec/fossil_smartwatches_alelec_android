package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory implements Factory<ServerSettingDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiServiceProvider;
    @DexIgnore
    public /* final */ RepositoriesModule module;

    @DexIgnore
    public RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        this.module = repositoriesModule;
        this.apiServiceProvider = provider;
    }

    @DexIgnore
    public static RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory create(RepositoriesModule repositoriesModule, Provider<ApiServiceV2> provider) {
        return new RepositoriesModule_ProvideServerSettingRemoteDataSourceFactory(repositoriesModule, provider);
    }

    @DexIgnore
    public static ServerSettingDataSource provideServerSettingRemoteDataSource(RepositoriesModule repositoriesModule, ApiServiceV2 apiServiceV2) {
        ServerSettingDataSource provideServerSettingRemoteDataSource = repositoriesModule.provideServerSettingRemoteDataSource(apiServiceV2);
        Lk7.c(provideServerSettingRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideServerSettingRemoteDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public ServerSettingDataSource get() {
        return provideServerSettingRemoteDataSource(this.module, this.apiServiceProvider.get());
    }
}
