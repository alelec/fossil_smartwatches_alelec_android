package com.portfolio.platform.data.source;

import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.ServerSettingList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ServerSettingRepository$getServerSettingList$1$onSuccess$1", f = "ServerSettingRepository.kt", l = {}, m = "invokeSuspend")
public final class ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingList $serverSettingList;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRepository$getServerSettingList$Anon1 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(ServerSettingRepository$getServerSettingList$Anon1 serverSettingRepository$getServerSettingList$Anon1, ServerSettingList serverSettingList, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = serverSettingRepository$getServerSettingList$Anon1;
        this.$serverSettingList = serverSettingList;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2 = new ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2(this.this$0, this.$serverSettingList, xe6);
        serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2.p$ = (Il6) obj;
        throw null;
        //return serverSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((ServerSettingRepository$getServerSettingList$Anon1$onSuccess$Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getServerSettingList - onSuccess - serverSettingList = [ " + this.$serverSettingList + " ]");
            ServerSettingList serverSettingList = this.$serverSettingList;
            if (serverSettingList != null) {
                List<ServerSetting> serverSettings = serverSettingList.getServerSettings();
                Boolean a2 = serverSettings != null ? Ao7.a(!serverSettings.isEmpty()) : null;
                if (a2 == null) {
                    Wg6.i();
                    throw null;
                } else if (a2.booleanValue()) {
                    this.this$0.this$0.mServerSettingLocalDataSource.addOrUpdateServerSettingList(this.$serverSettingList.getServerSettings());
                }
            }
            return Cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
