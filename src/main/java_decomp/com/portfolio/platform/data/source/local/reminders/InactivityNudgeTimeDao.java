package com.portfolio.platform.data.source.local.reminders;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface InactivityNudgeTimeDao {
    @DexIgnore
    Object delete();  // void declaration

    @DexIgnore
    InactivityNudgeTimeModel getInactivityNudgeTimeModelWithFieldNudgeTimeType(int i);

    @DexIgnore
    LiveData<InactivityNudgeTimeModel> getInactivityNudgeTimeWithFieldNudgeTimeType(int i);

    @DexIgnore
    LiveData<List<InactivityNudgeTimeModel>> getListInactivityNudgeTime();

    @DexIgnore
    List<InactivityNudgeTimeModel> getListInactivityNudgeTimeModel();

    @DexIgnore
    void upsertInactivityNudgeTime(InactivityNudgeTimeModel inactivityNudgeTimeModel);

    @DexIgnore
    void upsertListInactivityNudgeTime(List<InactivityNudgeTimeModel> list);
}
