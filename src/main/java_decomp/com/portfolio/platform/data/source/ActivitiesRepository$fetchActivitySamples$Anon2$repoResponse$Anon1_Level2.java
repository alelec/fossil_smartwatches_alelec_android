package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Activity;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository$fetchActivitySamples$2$repoResponse$1", f = "ActivitiesRepository.kt", l = {146}, m = "invokeSuspend")
public final class ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2 extends Ko7 implements Hg6<Xe6<? super Q88<ApiResponse<Activity>>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository$fetchActivitySamples$Anon2 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2(ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = activitiesRepository$fetchActivitySamples$Anon2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2(this.this$0, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<ApiResponse<Activity>>> xe6) {
        throw null;
        //return ((ActivitiesRepository$fetchActivitySamples$Anon2$repoResponse$Anon1_Level2) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.this$0.mApiService;
            String k = TimeUtils.k(this.this$0.$start);
            Wg6.b(k, "DateHelper.formatShortDate(start)");
            String k2 = TimeUtils.k(this.this$0.$end);
            Wg6.b(k2, "DateHelper.formatShortDate(end)");
            ActivitiesRepository$fetchActivitySamples$Anon2 activitiesRepository$fetchActivitySamples$Anon2 = this.this$0;
            int i2 = activitiesRepository$fetchActivitySamples$Anon2.$offset;
            int i3 = activitiesRepository$fetchActivitySamples$Anon2.$limit;
            this.label = 1;
            Object activities = apiServiceV2.getActivities(k, k2, i2, i3, this);
            return activities == d ? d : activities;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
