package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.DeviceRepository$downloadDeviceList$2", f = "DeviceRepository.kt", l = {106}, m = "invokeSuspend")
public final class DeviceRepository$downloadDeviceList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ap4 $response;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceRepository$downloadDeviceList$Anon2(DeviceRepository deviceRepository, Ap4 ap4, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = deviceRepository;
        this.$response = ap4;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        DeviceRepository$downloadDeviceList$Anon2 deviceRepository$downloadDeviceList$Anon2 = new DeviceRepository$downloadDeviceList$Anon2(this.this$0, this.$response, xe6);
        deviceRepository$downloadDeviceList$Anon2.p$ = (Il6) obj;
        throw null;
        //return deviceRepository$downloadDeviceList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((DeviceRepository$downloadDeviceList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        List<Device> list;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            ApiResponse apiResponse = (ApiResponse) ((Kq5) this.$response).a();
            list = apiResponse != null ? apiResponse.get_items() : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = DeviceRepository.Companion.getTAG();
            local.d(tag, "downloadDeviceList() - isFromCache = false " + list);
            DeviceRepository deviceRepository = this.this$0;
            this.L$0 = il6;
            this.L$1 = list;
            this.label = 1;
            if (deviceRepository.removeStealDevice(list, this) == d) {
                return d;
            }
        } else if (i == 1) {
            list = (List) this.L$1;
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (list != null) {
            for (Device device : list) {
                if (DeviceHelper.o.v(device.getDeviceId())) {
                    Device deviceByDeviceId = this.this$0.mDeviceDao.getDeviceByDeviceId(device.getDeviceId());
                    if (deviceByDeviceId != null) {
                        if (device.getBatteryLevel() <= 0) {
                            device.setBatteryLevel(deviceByDeviceId.getBatteryLevel());
                        }
                        device.setVibrationStrength(deviceByDeviceId.getVibrationStrength());
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tag2 = DeviceRepository.Companion.getTAG();
                    local2.d(tag2, "update device: " + device);
                    this.this$0.mDeviceDao.addOrUpdateDevice(device);
                } else {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String tag3 = DeviceRepository.Companion.getTAG();
                    local3.d(tag3, "Ignoring legacy device=" + device.getDeviceId());
                }
            }
            return Cd6.a;
        }
        Wg6.i();
        throw null;
    }
}
