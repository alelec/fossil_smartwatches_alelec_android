package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.fossil.Lr4;
import com.portfolio.platform.app_setting.AppSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesFlagDaoFactory implements Factory<Lr4> {
    @DexIgnore
    public /* final */ Provider<AppSettingsDatabase> appSettingsDatabaseProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesFlagDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AppSettingsDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.appSettingsDatabaseProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesFlagDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<AppSettingsDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesFlagDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Lr4 providesFlagDao(PortfolioDatabaseModule portfolioDatabaseModule, AppSettingsDatabase appSettingsDatabase) {
        Lr4 providesFlagDao = portfolioDatabaseModule.providesFlagDao(appSettingsDatabase);
        Lk7.c(providesFlagDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesFlagDao;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Lr4 get() {
        return providesFlagDao(this.module, this.appSettingsDatabaseProvider.get());
    }
}
