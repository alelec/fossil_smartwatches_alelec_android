package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.QuickResponseMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseMessageDao_Impl extends QuickResponseMessageDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<QuickResponseMessage> __insertionAdapterOfQuickResponseMessage;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveAll;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<QuickResponseMessage> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, QuickResponseMessage quickResponseMessage) {
            mi.bindLong(1, (long) quickResponseMessage.getId());
            if (quickResponseMessage.getResponse() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, quickResponseMessage.getResponse());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, QuickResponseMessage quickResponseMessage) {
            bind(mi, quickResponseMessage);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseMessage` (`id`,`response`) VALUES (nullif(?, 0),?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM quickResponseMessage WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM quickResponseMessage";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon4(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<QuickResponseMessage> call() throws Exception {
            Cursor b = Ex0.b(QuickResponseMessageDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "response");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(b.getString(c2));
                    quickResponseMessage.setId(b.getInt(c));
                    arrayList.add(quickResponseMessage);
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public QuickResponseMessageDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfQuickResponseMessage = new Anon1(oh);
        this.__preparedStmtOfRemoveById = new Anon2(oh);
        this.__preparedStmtOfRemoveAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public List<QuickResponseMessage> getAllRawResponse() {
        Rh f = Rh.f("SELECT * FROM quickResponseMessage", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "response");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                QuickResponseMessage quickResponseMessage = new QuickResponseMessage(b.getString(c2));
                quickResponseMessage.setId(b.getInt(c));
                arrayList.add(quickResponseMessage);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public LiveData<List<QuickResponseMessage>> getAllResponse() {
        Rh f = Rh.f("SELECT * FROM quickResponseMessage", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon4 anon4 = new Anon4(f);
        return invalidationTracker.d(new String[]{"quickResponseMessage"}, false, anon4);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponse(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert((Hh<QuickResponseMessage>) quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void insertResponses(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.bindLong(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert((Hh<QuickResponseMessage>) quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao
    public void update(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
