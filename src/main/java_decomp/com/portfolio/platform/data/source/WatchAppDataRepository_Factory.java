package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.WatchAppDataRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppDataRepository_Factory implements Factory<WatchAppDataRepository> {
    @DexIgnore
    public /* final */ Provider<FileRepository> mFileRepositoryProvider;
    @DexIgnore
    public /* final */ Provider<WatchAppDataRemoteDataSource> mRemoteSourceProvider;

    @DexIgnore
    public WatchAppDataRepository_Factory(Provider<WatchAppDataRemoteDataSource> provider, Provider<FileRepository> provider2) {
        this.mRemoteSourceProvider = provider;
        this.mFileRepositoryProvider = provider2;
    }

    @DexIgnore
    public static WatchAppDataRepository_Factory create(Provider<WatchAppDataRemoteDataSource> provider, Provider<FileRepository> provider2) {
        return new WatchAppDataRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static WatchAppDataRepository newInstance(WatchAppDataRemoteDataSource watchAppDataRemoteDataSource, FileRepository fileRepository) {
        return new WatchAppDataRepository(watchAppDataRemoteDataSource, fileRepository);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WatchAppDataRepository get() {
        return newInstance(this.mRemoteSourceProvider.get(), this.mFileRepositoryProvider.get());
    }
}
