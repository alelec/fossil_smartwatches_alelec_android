package com.portfolio.platform.data.source.remote;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;
    @DexIgnore
    public /* final */ AuthApiGuestService mAuthApiGuestService;
    @DexIgnore
    public /* final */ AuthApiUserService mAuthApiUserService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = UserRemoteDataSource.class.getSimpleName();
        Wg6.b(simpleName, "UserRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public UserRemoteDataSource(ApiServiceV2 apiServiceV2, AuthApiGuestService authApiGuestService, AuthApiUserService authApiUserService) {
        Wg6.c(apiServiceV2, "mApiService");
        Wg6.c(authApiGuestService, "mAuthApiGuestService");
        Wg6.c(authApiUserService, "mAuthApiUserService");
        this.mApiService = apiServiceV2;
        this.mAuthApiGuestService = authApiGuestService;
        this.mAuthApiUserService = authApiUserService;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00f0  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAuthenticationEmailExisting(java.lang.String r13, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Boolean>> r14) {
        /*
        // Method dump skipped, instructions count: 328
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.checkAuthenticationEmailExisting(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00df  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object checkAuthenticationSocialExisting(java.lang.String r13, java.lang.String r14, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Boolean>> r15) {
        /*
        // Method dump skipped, instructions count: 311
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.checkAuthenticationSocialExisting(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object deleteUser(com.mapped.Xe6<? super java.lang.Integer> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1
            if (r0 == 0) goto L_0x0034
            r0 = r7
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0034
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0042
            if (r3 != r5) goto L_0x003a
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0064
            r0 = 200(0xc8, float:2.8E-43)
        L_0x002f:
            java.lang.Integer r0 = com.fossil.Ao7.e(r0)
        L_0x0033:
            return r0
        L_0x0034:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$Anon1
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x003a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0042:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r4 = "deleteUser"
            r1.d(r3, r4)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$response$Anon1 r1 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$deleteUser$response$Anon1
            r3 = 0
            r1.<init>(r6, r3)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0033
        L_0x0064:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x006f
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r0 = r0.a()
            goto L_0x002f
        L_0x006f:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.deleteUser(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002e  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getUserSettingFromServer(com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.UserSettings>> r9) {
        /*
            r8 = this;
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1
            if (r0 == 0) goto L_0x005f
            r0 = r9
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005f
            int r1 = r1 + r3
            r0.label = r1
        L_0x0014:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x006d
            if (r3 != r6) goto L_0x0065
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0028:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x008e
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r1 = "getUserSettingFromServer() success, userSetting="
            r5.append(r1)
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.model.UserSettings r1 = (com.portfolio.platform.data.model.UserSettings) r1
            r5.append(r1)
            java.lang.String r1 = r5.toString()
            r2.d(r3, r1)
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x005e:
            return r0
        L_0x005f:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$Anon1
            r0.<init>(r8, r9)
            goto L_0x0014
        L_0x0065:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x006d:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "getUserSettingFromServer()"
            r1.d(r3, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$response$Anon1 r1 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$getUserSettingFromServer$response$Anon1
            r1.<init>(r8, r4)
            r0.L$0 = r8
            r0.label = r6
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0028
            r0 = r2
            goto L_0x005e
        L_0x008e:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00dd
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r3 = "getUserSettingFromServer() failed, errorCode="
            r5.append(r3)
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            int r0 = r3.a()
            r5.append(r0)
            java.lang.String r0 = ", message="
            r5.append(r0)
            com.portfolio.platform.data.model.ServerError r0 = r3.c()
            if (r0 == 0) goto L_0x00e3
            java.lang.String r0 = r0.getMessage()
        L_0x00bd:
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r1.d(r2, r0)
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x005e
        L_0x00dd:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        L_0x00e3:
            r0 = r4
            goto L_0x00bd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.getUserSettingFromServer(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loadUserInfo(com.portfolio.platform.data.model.MFUser r10, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.MFUser>> r11) {
        /*
        // Method dump skipped, instructions count: 562
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loadUserInfo(com.portfolio.platform.data.model.MFUser, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loginEmail(java.lang.String r9, java.lang.String r10, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.Auth>> r11) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r11 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1
            if (r0 == 0) goto L_0x004a
            r0 = r11
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0059
            if (r0 != r7) goto L_0x0051
            java.lang.Object r0 = r1.L$3
            com.mapped.Ku3 r0 = (com.mapped.Ku3) r0
            java.lang.Object r0 = r1.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x0035:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x009c
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x0049:
            return r0
        L_0x004a:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$Anon1
            r0.<init>(r8, r11)
            r1 = r0
            goto L_0x0015
        L_0x0051:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0059:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r5 = "loginEmail"
            r0.d(r2, r5)
            com.mapped.Ku3 r0 = new com.mapped.Ku3
            r0.<init>()
            java.lang.String r2 = "email"
            r0.n(r2, r9)
            java.lang.String r2 = "password"
            r0.n(r2, r10)
            java.lang.String r2 = "clientId"
            com.portfolio.platform.helper.AppHelper$Ai r5 = com.portfolio.platform.helper.AppHelper.g
            java.lang.String r6 = ""
            java.lang.String r5 = r5.a(r6)
            r0.n(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$response$Anon1 r2 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$loginEmail$response$Anon1
            r2.<init>(r8, r0, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.L$2 = r10
            r1.L$3 = r0
            r1.label = r7
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r2, r1)
            if (r0 != r3) goto L_0x0035
            r0 = r3
            goto L_0x0049
        L_0x009c:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00b9
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0049
        L_0x00b9:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loginEmail(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object loginWithSocial(java.lang.String r9, java.lang.String r10, java.lang.String r11, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.Auth>> r12) {
        /*
        // Method dump skipped, instructions count: 229
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.loginWithSocial(java.lang.String, java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002d  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object logoutUser(com.mapped.Xe6<? super java.lang.Integer> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1
            if (r0 == 0) goto L_0x0034
            r0 = r7
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0034
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0042
            if (r3 != r5) goto L_0x003a
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r1)
            r0 = r1
        L_0x0027:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0064
            r0 = 200(0xc8, float:2.8E-43)
        L_0x002f:
            java.lang.Integer r0 = com.fossil.Ao7.e(r0)
        L_0x0033:
            return r0
        L_0x0034:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$Anon1
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x003a:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0042:
            com.fossil.El7.b(r1)
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.String r3 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.String r4 = "logoutUser"
            r1.d(r3, r4)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$response$Anon1 r1 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$logoutUser$response$Anon1
            r3 = 0
            r1.<init>(r6, r3)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r1, r0)
            if (r0 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0033
        L_0x0064:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x006f
            com.fossil.Hq5 r0 = (com.fossil.Hq5) r0
            int r0 = r0.a()
            goto L_0x002f
        L_0x006f:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.logoutUser(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0099  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object requestEmailOtp(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r10) {
        /*
        // Method dump skipped, instructions count: 241
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.requestEmailOtp(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object resetPassword(java.lang.String r9, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Integer>> r10) {
        /*
        // Method dump skipped, instructions count: 232
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.resetPassword(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0062  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object sendUserSettingToServer(com.portfolio.platform.data.model.UserSettings r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.UserSettings>> r10) {
        /*
        // Method dump skipped, instructions count: 265
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.sendUserSettingToServer(com.portfolio.platform.data.model.UserSettings, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object signUpEmail(com.portfolio.platform.data.SignUpEmailAuth r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.Auth>> r10) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1
            if (r0 == 0) goto L_0x0042
            r0 = r10
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0051
            if (r0 != r7) goto L_0x0049
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.SignUpEmailAuth r0 = (com.portfolio.platform.data.SignUpEmailAuth) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0083
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "signUpEmail auth "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r0.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpEmail$response$Anon1
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r7
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0041
        L_0x0083:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00a0
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0041
        L_0x00a0:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.signUpEmail(com.portfolio.platform.data.SignUpEmailAuth, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object signUpSocial(com.portfolio.platform.data.SignUpSocialAuth r9, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.Auth>> r10) {
        /*
            r8 = this;
            r7 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1
            if (r0 == 0) goto L_0x0042
            r0 = r10
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1 r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0042
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0051
            if (r0 != r7) goto L_0x0049
            java.lang.Object r0 = r1.L$1
            com.portfolio.platform.data.SignUpSocialAuth r0 = (com.portfolio.platform.data.SignUpSocialAuth) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.UserRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.UserRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0083
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r0 = r0.a()
            r2 = 0
            r3 = 2
            r1.<init>(r0, r2, r3, r4)
            r0 = r1
        L_0x0041:
            return r0
        L_0x0042:
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$Anon1
            r0.<init>(r8, r10)
            r1 = r0
            goto L_0x0015
        L_0x0049:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0051:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r2 = com.portfolio.platform.data.source.remote.UserRemoteDataSource.TAG
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "signUpSocial auth "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r5 = r5.toString()
            r0.d(r2, r5)
            com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.UserRemoteDataSource$signUpSocial$response$Anon1
            r0.<init>(r8, r9, r4)
            r1.L$0 = r8
            r1.L$1 = r9
            r1.label = r7
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0041
        L_0x0083:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00a0
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0041
        L_0x00a0:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.signUpSocial(com.portfolio.platform.data.SignUpSocialAuth, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:107:0x02cd  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006a  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object updateUser(com.portfolio.platform.data.model.MFUser r13, com.mapped.Xe6<? super com.mapped.Ap4<com.portfolio.platform.data.model.MFUser>> r14) {
        /*
        // Method dump skipped, instructions count: 808
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.updateUser(com.portfolio.platform.data.model.MFUser, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object verifyEmailOtp(java.lang.String r9, java.lang.String r10, com.mapped.Xe6<? super com.mapped.Ap4<java.lang.Void>> r11) {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.UserRemoteDataSource.verifyEmailOtp(java.lang.String, java.lang.String, com.mapped.Xe6):java.lang.Object");
    }
}
