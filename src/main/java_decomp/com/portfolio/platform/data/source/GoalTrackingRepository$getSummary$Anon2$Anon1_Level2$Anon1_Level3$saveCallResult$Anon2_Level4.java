package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2$1$1$saveCallResult$2", f = "GoalTrackingRepository.kt", l = {}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalDailySummary $item;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(GoalTrackingRepository$getSummary$Anon2.Anon1_Level2.Anon1_Level3 anon1_Level3, GoalDailySummary goalDailySummary, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = anon1_Level3;
        this.$item = goalDailySummary;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 = new GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4(this.this$0, this.$item, xe6);
        goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Yn7.d();
        if (this.label == 0) {
            El7.b(obj);
            GoalTrackingDao goalTrackingDao = this.this$0.this$0.$goalTrackingDb.getGoalTrackingDao();
            GoalTrackingSummary goalTrackingSummary = this.$item.toGoalTrackingSummary();
            if (goalTrackingSummary != null) {
                goalTrackingDao.upsertGoalTrackingSummary(goalTrackingSummary);
                return Cd6.a;
            }
            Wg6.i();
            throw null;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
