package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository_Factory implements Factory<WorkoutSessionRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;

    @DexIgnore
    public WorkoutSessionRepository_Factory(Provider<ApiServiceV2> provider) {
        this.mApiServiceProvider = provider;
    }

    @DexIgnore
    public static WorkoutSessionRepository_Factory create(Provider<ApiServiceV2> provider) {
        return new WorkoutSessionRepository_Factory(provider);
    }

    @DexIgnore
    public static WorkoutSessionRepository newInstance(ApiServiceV2 apiServiceV2) {
        return new WorkoutSessionRepository(apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WorkoutSessionRepository get() {
        return newInstance(this.mApiServiceProvider.get());
    }
}
