package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.DianaAppSettingRepository$getAllDianaAppSettings$2", f = "DianaAppSettingRepository.kt", l = {113}, m = "invokeSuspend")
public final class DianaAppSettingRepository$getAllDianaAppSettings$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends DianaAppSetting>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $category;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaAppSettingRepository$getAllDianaAppSettings$Anon2(String str, Xe6 xe6) {
        super(2, xe6);
        this.$category = str;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        DianaAppSettingRepository$getAllDianaAppSettings$Anon2 dianaAppSettingRepository$getAllDianaAppSettings$Anon2 = new DianaAppSettingRepository$getAllDianaAppSettings$Anon2(this.$category, xe6);
        dianaAppSettingRepository$getAllDianaAppSettings$Anon2.p$ = (Il6) obj;
        throw null;
        //return dianaAppSettingRepository$getAllDianaAppSettings$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super List<? extends DianaAppSetting>> xe6) {
        throw null;
        //return ((DianaAppSettingRepository$getAllDianaAppSettings$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object v;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il6;
            this.label = 1;
            v = encryptedDatabaseManager.v(this);
            if (v == d) {
                return d;
            }
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            v = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return ((DianaCustomizeDatabase) v).getDianaAppSettingDao().getAllDianaAppSetting(this.$category);
    }
}
