package com.portfolio.platform.data.source.local.reminders;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.RemindTimeModel;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeDao_Impl implements RemindTimeDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<RemindTimeModel> __insertionAdapterOfRemindTimeModel;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<RemindTimeModel> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, RemindTimeModel remindTimeModel) {
            if (remindTimeModel.getRemindTimeName() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, remindTimeModel.getRemindTimeName());
            }
            mi.bindLong(2, (long) remindTimeModel.getMinutes());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, RemindTimeModel remindTimeModel) {
            bind(mi, remindTimeModel);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `remindTimeModel` (`remindTimeName`,`minutes`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM remindTimeModel";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public RemindTimeModel call() throws Exception {
            RemindTimeModel remindTimeModel = null;
            Cursor b = Ex0.b(RemindTimeDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "remindTimeName");
                int c2 = Dx0.c(b, "minutes");
                if (b.moveToFirst()) {
                    remindTimeModel = new RemindTimeModel(b.getString(c), b.getInt(c2));
                }
                return remindTimeModel;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public RemindTimeDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfRemindTimeModel = new Anon1(oh);
        this.__preparedStmtOfDelete = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public LiveData<RemindTimeModel> getRemindTime() {
        Rh f = Rh.f("SELECT * FROM remindTimeModel", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"remindTimeModel"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public RemindTimeModel getRemindTimeModel() {
        RemindTimeModel remindTimeModel = null;
        Rh f = Rh.f("SELECT * FROM remindTimeModel", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "remindTimeName");
            int c2 = Dx0.c(b, "minutes");
            if (b.moveToFirst()) {
                remindTimeModel = new RemindTimeModel(b.getString(c), b.getInt(c2));
            }
            return remindTimeModel;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.reminders.RemindTimeDao
    public void upsertRemindTimeModel(RemindTimeModel remindTimeModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfRemindTimeModel.insert((Hh<RemindTimeModel>) remindTimeModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
