package com.portfolio.platform.data.source;

import com.fossil.Bw7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.mapped.Jh6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4 implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX WARN: Incorrect args count in method signature: ()V */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4(GoalTrackingRepository goalTrackingRepository) {
        this.this$0 = goalTrackingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onFail(int i) {
        FLogger.INSTANCE.getLocal().d(GoalTrackingRepository.Companion.getTAG(), "pushPendingGoalTrackingDataList onFetchFailed");
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.GoalTrackingRepository.PushPendingGoalTrackingDataListCallback
    public void onSuccess(List<GoalTrackingData> list) {
        Wg6.c(list, "goalTrackingList");
        Jh6 jh6 = new Jh6();
        jh6.element = (T) list.get(0).getDate();
        Jh6 jh62 = new Jh6();
        jh62.element = (T) list.get(0).getDate();
        for (GoalTrackingData goalTrackingData : list) {
            if (goalTrackingData.getDate().getTime() < jh6.element.getTime()) {
                jh6.element = (T) goalTrackingData.getDate();
            }
            if (goalTrackingData.getDate().getTime() > jh62.element.getTime()) {
                jh62.element = (T) goalTrackingData.getDate();
            }
        }
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon4$onSuccess$Anon1_Level2(this, jh6, jh62, null), 3, null);
    }
}
