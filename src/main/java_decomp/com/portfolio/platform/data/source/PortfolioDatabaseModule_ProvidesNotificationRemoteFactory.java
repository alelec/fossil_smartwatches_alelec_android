package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.buddy_challenge.domain.NotificationRemoteDataSource;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesNotificationRemoteFactory implements Factory<NotificationRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesNotificationRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesNotificationRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesNotificationRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static NotificationRemoteDataSource providesNotificationRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        NotificationRemoteDataSource providesNotificationRemote = portfolioDatabaseModule.providesNotificationRemote(apiServiceV2);
        Lk7.c(providesNotificationRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesNotificationRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public NotificationRemoteDataSource get() {
        return providesNotificationRemote(this.module, this.apiProvider.get());
    }
}
