package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.watchface.data.source.WFAssetRemote;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory implements Factory<WFAssetRemote> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> apiProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        this.module = portfolioDatabaseModule;
        this.apiProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<ApiServiceV2> provider) {
        return new PortfolioDatabaseModule_ProvidesWFTemplateRemoteFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WFAssetRemote providesWFTemplateRemote(PortfolioDatabaseModule portfolioDatabaseModule, ApiServiceV2 apiServiceV2) {
        WFAssetRemote providesWFTemplateRemote = portfolioDatabaseModule.providesWFTemplateRemote(apiServiceV2);
        Lk7.c(providesWFTemplateRemote, "Cannot return null from a non-@Nullable @Provides method");
        return providesWFTemplateRemote;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public WFAssetRemote get() {
        return providesWFTemplateRemote(this.module, this.apiProvider.get());
    }
}
