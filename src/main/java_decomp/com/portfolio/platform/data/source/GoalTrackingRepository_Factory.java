package com.portfolio.platform.data.source;

import com.mapped.An4;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository_Factory implements Factory<GoalTrackingRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceV2Provider;
    @DexIgnore
    public /* final */ Provider<An4> mSharedPreferencesManagerProvider;
    @DexIgnore
    public /* final */ Provider<UserRepository> mUserRepositoryProvider;

    @DexIgnore
    public GoalTrackingRepository_Factory(Provider<UserRepository> provider, Provider<An4> provider2, Provider<ApiServiceV2> provider3) {
        this.mUserRepositoryProvider = provider;
        this.mSharedPreferencesManagerProvider = provider2;
        this.mApiServiceV2Provider = provider3;
    }

    @DexIgnore
    public static GoalTrackingRepository_Factory create(Provider<UserRepository> provider, Provider<An4> provider2, Provider<ApiServiceV2> provider3) {
        return new GoalTrackingRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static GoalTrackingRepository newInstance(UserRepository userRepository, An4 an4, ApiServiceV2 apiServiceV2) {
        return new GoalTrackingRepository(userRepository, an4, apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public GoalTrackingRepository get() {
        return newInstance(this.mUserRepositoryProvider.get(), this.mSharedPreferencesManagerProvider.get(), this.mApiServiceV2Provider.get());
    }
}
