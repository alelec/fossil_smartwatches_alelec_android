package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Bw7;
import com.fossil.Eu7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.data.source.remote.WorkoutSettingRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSettingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "WorkoutSettingRepository";
    @DexIgnore
    public /* final */ WorkoutSettingRemoteDataSource mWorkoutSettingRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public WorkoutSettingRepository(WorkoutSettingRemoteDataSource workoutSettingRemoteDataSource) {
        Wg6.c(workoutSettingRemoteDataSource, "mWorkoutSettingRemoteDataSource");
        this.mWorkoutSettingRemoteDataSource = workoutSettingRemoteDataSource;
    }

    @DexIgnore
    public final Object cleanUp(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new WorkoutSettingRepository$cleanUp$Anon2(null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final Object downloadWorkoutSettings(Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new WorkoutSettingRepository$downloadWorkoutSettings$Anon2(this, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0023  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00bc  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object executePendingRequest(com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.executePendingRequest(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object getWorkoutSettingList(Xe6<? super List<WorkoutSetting>> xe6) {
        return Eu7.g(Bw7.b(), new WorkoutSettingRepository$getWorkoutSettingList$Anon2(null), xe6);
    }

    @DexIgnore
    public final Object getWorkoutSettingListAsLiveData(Xe6<? super LiveData<List<WorkoutSetting>>> xe6) {
        return Eu7.g(Bw7.c(), new WorkoutSettingRepository$getWorkoutSettingListAsLiveData$Anon2(null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00c0  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00e2  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object upsertWorkoutSettingList(java.util.List<com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting> r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
        /*
        // Method dump skipped, instructions count: 324
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.WorkoutSettingRepository.upsertWorkoutSettingList(java.util.List, com.mapped.Xe6):java.lang.Object");
    }
}
