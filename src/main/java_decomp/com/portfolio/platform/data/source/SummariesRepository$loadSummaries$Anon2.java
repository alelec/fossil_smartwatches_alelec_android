package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.google.gson.reflect.TypeToken;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SummariesRepository$loadSummaries$2", f = "SummariesRepository.kt", l = {Action.Presenter.PREVIOUS, Action.Presenter.BLACKOUT}, m = "invokeSuspend")
public final class SummariesRepository$loadSummaries$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Ku3>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends TypeToken<ApiResponse<FitnessDayData>> {
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SummariesRepository$loadSummaries$Anon2(SummariesRepository summariesRepository, Date date, Date date2, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SummariesRepository$loadSummaries$Anon2 summariesRepository$loadSummaries$Anon2 = new SummariesRepository$loadSummaries$Anon2(this.this$0, this.$startDate, this.$endDate, xe6);
        summariesRepository$loadSummaries$Anon2.p$ = (Il6) obj;
        throw null;
        //return summariesRepository$loadSummaries$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Ku3>> xe6) {
        throw null;
        //return ((SummariesRepository$loadSummaries$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011b  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r10) {
        /*
        // Method dump skipped, instructions count: 476
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SummariesRepository$loadSummaries$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
