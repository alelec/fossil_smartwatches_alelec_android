package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.ActivitiesRepository$insert$2", f = "ActivitiesRepository.kt", l = {278, 297}, m = "invokeSuspend")
public final class ActivitiesRepository$insert$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<List<ActivitySample>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $activityList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitiesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitiesRepository$insert$Anon2(ActivitiesRepository activitiesRepository, List list, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = activitiesRepository;
        this.$activityList = list;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        ActivitiesRepository$insert$Anon2 activitiesRepository$insert$Anon2 = new ActivitiesRepository$insert$Anon2(this.this$0, this.$activityList, xe6);
        activitiesRepository$insert$Anon2.p$ = (Il6) obj;
        throw null;
        //return activitiesRepository$insert$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<List<ActivitySample>>> xe6) {
        throw null;
        //return ((ActivitiesRepository$insert$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0092  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01cc  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r15) {
        /*
        // Method dump skipped, instructions count: 484
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.ActivitiesRepository$insert$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
