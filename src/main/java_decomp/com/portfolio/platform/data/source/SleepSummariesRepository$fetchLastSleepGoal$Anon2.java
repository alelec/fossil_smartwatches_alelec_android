package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Kq5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.response.ResponseKt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$fetchLastSleepGoal$2", f = "SleepSummariesRepository.kt", l = {122, 126}, m = "invokeSuspend")
public final class SleepSummariesRepository$fetchLastSleepGoal$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<MFSleepSettings>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$fetchLastSleepGoal$Anon2(SleepSummariesRepository sleepSummariesRepository, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSummariesRepository$fetchLastSleepGoal$Anon2 sleepSummariesRepository$fetchLastSleepGoal$Anon2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2(this.this$0, xe6);
        sleepSummariesRepository$fetchLastSleepGoal$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSummariesRepository$fetchLastSleepGoal$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<MFSleepSettings>> xe6) {
        throw null;
        //return ((SleepSummariesRepository$fetchLastSleepGoal$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d;
        Il6 il6;
        String userMessage;
        String message;
        Object d2 = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il62 = this.p$;
            FLogger.INSTANCE.getLocal().d(SleepSummariesRepository.TAG, "fetchLastSleepGoal");
            SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2 = new SleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2(this, null);
            this.L$0 = il62;
            this.label = 1;
            d = ResponseKt.d(sleepSummariesRepository$fetchLastSleepGoal$Anon2$response$Anon1_Level2, this);
            if (d == d2) {
                return d2;
            }
            il6 = il62;
        } else if (i == 1) {
            El7.b(obj);
            il6 = (Il6) this.L$0;
            d = obj;
        } else if (i == 2) {
            Ap4 ap4 = (Ap4) this.L$1;
            Il6 il63 = (Il6) this.L$0;
            El7.b(obj);
            return ap4;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        Ap4 ap42 = (Ap4) d;
        if (ap42 instanceof Kq5) {
            Kq5 kq5 = (Kq5) ap42;
            if (kq5.a() == null || kq5.b()) {
                return ap42;
            }
            SleepSummariesRepository sleepSummariesRepository = this.this$0;
            int sleepGoal = ((MFSleepSettings) kq5.a()).getSleepGoal();
            this.L$0 = il6;
            this.L$1 = ap42;
            this.label = 2;
            return sleepSummariesRepository.saveSleepSettingToDB(sleepGoal, this) == d2 ? d2 : ap42;
        } else if (!(ap42 instanceof Hq5)) {
            return ap42;
        } else {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = SleepSummariesRepository.TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("fetchActivitySettings Failure code=");
            Hq5 hq5 = (Hq5) ap42;
            sb.append(hq5.a());
            sb.append(" message=");
            ServerError c = hq5.c();
            if (c == null || (message = c.getMessage()) == null) {
                ServerError c2 = hq5.c();
                userMessage = c2 != null ? c2.getUserMessage() : null;
            } else {
                userMessage = message;
            }
            if (userMessage == null) {
                userMessage = "";
            }
            sb.append(userMessage);
            local.d(str, sb.toString());
            return ap42;
        }
    }
}
