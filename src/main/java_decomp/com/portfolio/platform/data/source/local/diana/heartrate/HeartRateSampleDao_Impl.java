package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.H05;
import com.fossil.Nw0;
import com.fossil.Qz4;
import com.fossil.Sz4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleDao_Impl extends HeartRateSampleDao {
    @DexIgnore
    public /* final */ Qz4 __dateShortStringConverter; // = new Qz4();
    @DexIgnore
    public /* final */ Sz4 __dateTimeISOStringConverter; // = new Sz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<HeartRateSample> __insertionAdapterOfHeartRateSample;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllHeartRateSamples;
    @DexIgnore
    public /* final */ H05 __restingConverter; // = new H05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<HeartRateSample> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, HeartRateSample heartRateSample) {
            if (heartRateSample.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, heartRateSample.getId());
            }
            mi.bindDouble(2, (double) heartRateSample.getAverage());
            String a2 = HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(heartRateSample.getDate());
            if (a2 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a2);
            }
            mi.bindLong(4, heartRateSample.getCreatedAt());
            mi.bindLong(5, heartRateSample.getUpdatedAt());
            String a3 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getEndTime());
            if (a3 == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, a3);
            }
            String a4 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getStartTime());
            if (a4 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, a4);
            }
            mi.bindLong(8, (long) heartRateSample.getTimezoneOffsetInSecond());
            mi.bindLong(9, (long) heartRateSample.getMin());
            mi.bindLong(10, (long) heartRateSample.getMax());
            mi.bindLong(11, (long) heartRateSample.getMinuteCount());
            String b = HeartRateSampleDao_Impl.this.__restingConverter.b(heartRateSample.getResting());
            if (b == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, HeartRateSample heartRateSample) {
            bind(mi, heartRateSample);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `heart_rate_sample` (`id`,`average`,`date`,`createdAt`,`updatedAt`,`endTime`,`startTime`,`timezoneOffset`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM heart_rate_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<HeartRateSample>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<HeartRateSample> call() throws Exception {
            Cursor b = Ex0.b(HeartRateSampleDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
                int c3 = Dx0.c(b, "date");
                int c4 = Dx0.c(b, "createdAt");
                int c5 = Dx0.c(b, "updatedAt");
                int c6 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
                int c7 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
                int c8 = Dx0.c(b, "timezoneOffset");
                int c9 = Dx0.c(b, "min");
                int c10 = Dx0.c(b, "max");
                int c11 = Dx0.c(b, "minuteCount");
                int c12 = Dx0.c(b, "resting");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new HeartRateSample(b.getString(c), b.getFloat(c2), HeartRateSampleDao_Impl.this.__dateShortStringConverter.b(b.getString(c3)), b.getLong(c4), b.getLong(c5), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c6)), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), HeartRateSampleDao_Impl.this.__restingConverter.a(b.getString(c12))));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public HeartRateSampleDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfHeartRateSample = new Anon1(oh);
        this.__preparedStmtOfDeleteAllHeartRateSamples = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void deleteAllHeartRateSamples() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllHeartRateSamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public HeartRateSample getHeartRateSample(String str) {
        Rh f = Rh.f("SELECT * FROM heart_rate_sample WHERE id = ?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        HeartRateSample heartRateSample = null;
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, GoalTrackingSummary.COLUMN_AVERAGE);
            int c3 = Dx0.c(b, "date");
            int c4 = Dx0.c(b, "createdAt");
            int c5 = Dx0.c(b, "updatedAt");
            int c6 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c7 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c8 = Dx0.c(b, "timezoneOffset");
            int c9 = Dx0.c(b, "min");
            int c10 = Dx0.c(b, "max");
            int c11 = Dx0.c(b, "minuteCount");
            int c12 = Dx0.c(b, "resting");
            if (b.moveToFirst()) {
                heartRateSample = new HeartRateSample(b.getString(c), b.getFloat(c2), this.__dateShortStringConverter.b(b.getString(c3)), b.getLong(c4), b.getLong(c5), this.__dateTimeISOStringConverter.b(b.getString(c6)), this.__dateTimeISOStringConverter.b(b.getString(c7)), b.getInt(c8), b.getInt(c9), b.getInt(c10), b.getInt(c11), this.__restingConverter.a(b.getString(c12)));
            }
            return heartRateSample;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM heart_rate_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a2 = this.__dateShortStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateShortStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"heart_rate_sample"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void insertHeartRateSample(HeartRateSample heartRateSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert((Hh<HeartRateSample>) heartRateSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao
    public void upsertHeartRateSampleList(List<HeartRateSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
