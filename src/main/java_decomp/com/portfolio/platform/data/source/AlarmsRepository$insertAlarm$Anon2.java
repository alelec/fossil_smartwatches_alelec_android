package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.local.alarm.AlarmDatabase;
import com.portfolio.platform.manager.EncryptedDatabaseManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.AlarmsRepository$insertAlarm$2", f = "AlarmsRepository.kt", l = {124, 126}, m = "invokeSuspend")
public final class AlarmsRepository$insertAlarm$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<Alarm>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Alarm $alarm;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ AlarmsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AlarmsRepository$insertAlarm$Anon2(AlarmsRepository alarmsRepository, Alarm alarm, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = alarmsRepository;
        this.$alarm = alarm;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        AlarmsRepository$insertAlarm$Anon2 alarmsRepository$insertAlarm$Anon2 = new AlarmsRepository$insertAlarm$Anon2(this.this$0, this.$alarm, xe6);
        alarmsRepository$insertAlarm$Anon2.p$ = (Il6) obj;
        throw null;
        //return alarmsRepository$insertAlarm$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Ap4<Alarm>> xe6) {
        throw null;
        //return ((AlarmsRepository$insertAlarm$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object u;
        Il6 il6;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il62 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = AlarmsRepository.TAG;
            local.d(str, "insertAlarm - alarmId=" + this.$alarm.getId());
            this.$alarm.setPinType(1);
            EncryptedDatabaseManager encryptedDatabaseManager = EncryptedDatabaseManager.j;
            this.L$0 = il62;
            this.label = 1;
            u = encryptedDatabaseManager.u(this);
            if (u == d) {
                return d;
            }
            il6 = il62;
        } else if (i == 1) {
            El7.b(obj);
            il6 = (Il6) this.L$0;
            u = obj;
        } else if (i == 2) {
            Il6 il63 = (Il6) this.L$0;
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ((AlarmDatabase) u).alarmDao().insertAlarm(this.$alarm);
        AlarmsRepository alarmsRepository = this.this$0;
        Alarm alarm = this.$alarm;
        this.L$0 = il6;
        this.label = 2;
        Object upsertAlarm = alarmsRepository.upsertAlarm(alarm, this);
        return upsertAlarm == d ? d : upsertAlarm;
    }
}
