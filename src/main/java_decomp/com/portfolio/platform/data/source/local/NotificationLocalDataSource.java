package com.portfolio.platform.data.source.local;

import android.util.SparseArray;
import com.fossil.Fp5;
import com.fossil.Mn5;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.appfilter.AppFilterProvider;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProvider;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.NotificationsDataSource;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationLocalDataSource implements NotificationsDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "NotificationLocalDataSource";

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllNotificationSetting() {
        Mn5.p.a().d().removeAllContactGroups();
        Mn5.p.a().c().removeAllAppFilters();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void clearAllPhoneFavoritesContacts() {
        Mn5.p.a().k().i();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return Mn5.p.a().c().getAllAppFiltersWithHour(i, i2);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return Mn5.p.a().c().getAllAppFilterVibration(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<AppFilter> getAllAppFilters(int i) {
        return Mn5.p.a().c().getAllAppFilters(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getAllContactGroups(int i) {
        return Mn5.p.a().d().getAllContactGroups(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        SparseArray<List<BaseFeatureModel>> sparseArray = new SparseArray<>();
        ArrayList<BaseFeatureModel> arrayList = new ArrayList();
        List<ContactGroup> allContactGroups = Mn5.p.a().d().getAllContactGroups(i);
        List<AppFilter> allAppFilters = Mn5.p.a().c().getAllAppFilters(i);
        if (allContactGroups != null && !allContactGroups.isEmpty()) {
            arrayList.addAll(allContactGroups);
        }
        if (allAppFilters != null && !allAppFilters.isEmpty()) {
            arrayList.addAll(allAppFilters);
        }
        for (BaseFeatureModel baseFeatureModel : arrayList) {
            List<BaseFeatureModel> list = sparseArray.get(baseFeatureModel.getHour());
            if (list == null || list.isEmpty()) {
                list = new ArrayList<>();
            }
            list.add(baseFeatureModel);
            sparseArray.put(baseFeatureModel.getHour(), list);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Inside .getAllNotificationsByHour inLocal, result=" + sparseArray);
        return sparseArray;
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public AppFilter getAppFilterByType(String str, int i) {
        return Mn5.p.a().c().getAppFilterMatchingType(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public Contact getContactById(int i) {
        return ((Fp5) Mn5.p.a().d()).E(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getContactGroupId(List<Integer> list) {
        return ((Fp5) Mn5.p.a().d()).F(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return Mn5.p.a().d().getContactGroupsMatchingEmail(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return Mn5.p.a().d().getContactGroupsMatchingIncomingCall(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return Mn5.p.a().d().getContactGroupsMatchingSms(str, i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public List<Integer> getLocalContactId() {
        return ((Fp5) Mn5.p.a().d()).G();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAllAppFilters() {
        Mn5.p.a().c().removeAllAppFilters();
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeAppFilter(AppFilter appFilter) {
        Mn5.p.a().c().removeAppFilter(appFilter);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContact(Contact contact) {
        Mn5.p.a().d().removeContact(contact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroup(ContactGroup contactGroup) {
        Mn5.p.a().d().removeContactGroup(contactGroup);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeContactGroupList(List<ContactGroup> list) {
        ContactProvider d = Mn5.p.a().d();
        for (ContactGroup contactGroup : list) {
            d.removeContactGroup(contactGroup);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListAppFilter(List<AppFilter> list) {
        AppFilterProvider c = Mn5.p.a().c();
        for (AppFilter appFilter : list) {
            c.removeAppFilter(appFilter);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeListContact(List<Contact> list) {
        ContactProvider d = Mn5.p.a().d();
        for (Contact contact : list) {
            d.removeContact(contact);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContact(List<Integer> list) {
        ((Fp5) Mn5.p.a().d()).H(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeLocalRedundantContactGroup(List<Integer> list) {
        ((Fp5) Mn5.p.a().d()).K(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        Mn5.p.a().k().removePhoneFavoritesContact(phoneFavoritesContact.getPhoneNumber());
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneFavoritesContact(String str) {
        Mn5.p.a().k().removePhoneFavoritesContact(str);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumber(List<Integer> list) {
        ((Fp5) Mn5.p.a().d()).I(list);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removePhoneNumberByContactGroupId(int i) {
        ((Fp5) Mn5.p.a().d()).J(i);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void removeRedundantContact() {
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveAppFilter(AppFilter appFilter) {
        Mn5.p.a().c().saveAppFilter(appFilter);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContact(Contact contact) {
        Mn5.p.a().d().saveContact(contact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroup(ContactGroup contactGroup) {
        Mn5.p.a().d().saveContactGroup(contactGroup);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveContactGroupList(List<ContactGroup> list) {
        ContactProvider d = Mn5.p.a().d();
        for (ContactGroup contactGroup : list) {
            d.saveContactGroup(contactGroup);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveEmailAddress(EmailAddress emailAddress) {
        Mn5.p.a().d().saveEmailAddress(emailAddress);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListAppFilters(List<AppFilter> list) {
        AppFilterProvider c = Mn5.p.a().c();
        for (AppFilter appFilter : list) {
            c.saveAppFilter(appFilter);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListContact(List<Contact> list) {
        ContactProvider d = Mn5.p.a().d();
        for (Contact contact : list) {
            d.saveContact(contact);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void saveListPhoneNumber(List<PhoneNumber> list) {
        ContactProvider d = Mn5.p.a().d();
        for (PhoneNumber phoneNumber : list) {
            d.savePhoneNumber(phoneNumber);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        Mn5.p.a().k().g(phoneFavoritesContact);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.NotificationsDataSource
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        Mn5.p.a().d().savePhoneNumber(phoneNumber);
    }
}
