package com.portfolio.platform.data.source;

import com.fossil.Lk7;
import com.fossil.Qs4;
import com.fossil.Rt4;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory implements Factory<Rt4> {
    @DexIgnore
    public /* final */ Provider<Qs4> daoProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Qs4> provider) {
        this.module = portfolioDatabaseModule;
        this.daoProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<Qs4> provider) {
        return new PortfolioDatabaseModule_ProvidesChallengeLocalDataSourceFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static Rt4 providesChallengeLocalDataSource(PortfolioDatabaseModule portfolioDatabaseModule, Qs4 qs4) {
        Rt4 providesChallengeLocalDataSource = portfolioDatabaseModule.providesChallengeLocalDataSource(qs4);
        Lk7.c(providesChallengeLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return providesChallengeLocalDataSource;
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public Rt4 get() {
        return providesChallengeLocalDataSource(this.module, this.daoProvider.get());
    }
}
