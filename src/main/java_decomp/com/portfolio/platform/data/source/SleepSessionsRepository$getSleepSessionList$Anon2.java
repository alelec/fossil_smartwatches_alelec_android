package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.Ao7;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Zi4;
import com.fossil.wearables.fsl.goaltracking.GoalPhase;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Hh6;
import com.mapped.Il6;
import com.mapped.Ku3;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.local.sleep.SleepDao;
import com.portfolio.platform.data.source.local.sleep.SleepDatabase;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConverterShortDate;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;
import net.sqlcipher.database.SQLiteDatabase;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$2", f = "SleepSessionsRepository.kt", l = {68, 69}, m = "invokeSuspend")
public final class SleepSessionsRepository$getSleepSessionList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends List<MFSleepSession>>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $end;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $start;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepDatabase $sleepDatabase;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startDate;
        @DexIgnore
        public /* final */ /* synthetic */ SleepSessionsRepository$getSleepSessionList$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<List<MFSleepSession>, Ku3> {
            @DexIgnore
            public /* final */ /* synthetic */ Lc6 $downloadingDate;
            @DexIgnore
            public /* final */ /* synthetic */ int $limit;
            @DexIgnore
            public /* final */ /* synthetic */ Hh6 $offset;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, Hh6 hh6, int i, Lc6 lc6) {
                this.this$0 = anon1_Level2;
                this.$offset = hh6;
                this.$limit = i;
                this.$downloadingDate = lc6;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<Ku3>> xe6) {
                Date date;
                Date date2;
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiService;
                Lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                String k = TimeUtils.k(date);
                Wg6.b(k, "DateHelper.formatShortDa\u2026            ?: startDate)");
                Lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                String k2 = TimeUtils.k(date2);
                Wg6.b(k2, "DateHelper.formatShortDa\u2026              ?: endDate)");
                return apiServiceV2.getSleepSessions(k, k2, this.$offset.element, this.$limit, xe6);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object loadFromDb(Xe6<? super LiveData<List<MFSleepSession>>> xe6) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = SleepSessionsRepository.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("getSleepSessionList: startMilli = ");
                Date date = this.this$0.$startDate;
                Wg6.b(date, GoalPhase.COLUMN_START_DATE);
                sb.append(date.getTime());
                sb.append(", endMilli = ");
                Date date2 = this.this$0.$endDate;
                Wg6.b(date2, GoalPhase.COLUMN_END_DATE);
                sb.append(date2.getTime());
                local.d(str, sb.toString());
                SleepDao sleepDao = this.this$0.$sleepDatabase.sleepDao();
                Date date3 = this.this$0.$startDate;
                Wg6.b(date3, GoalPhase.COLUMN_START_DATE);
                long time = date3.getTime();
                Date date4 = this.this$0.$endDate;
                Wg6.b(date4, GoalPhase.COLUMN_END_DATE);
                return sleepDao.getSleepSessionsLiveData(time, date4.getTime());
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList onFetchFailed");
            }

            @DexIgnore
            public Object processContinueFetching(Ku3 ku3, Xe6<? super Boolean> xe6) {
                Boolean a2;
                Zi4 zi4 = new Zi4();
                zi4.f(DateTime.class, new GsonConvertDateTime());
                zi4.f(Date.class, new GsonConverterShortDate());
                Range range = ((ApiResponse) zi4.d().l(ku3.toString(), new SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2$Anon1_Level3$processContinueFetching$sleepSessionList$Anon1_Level4().getType())).get_range();
                if (range == null || (a2 = Ao7.a(range.isHasNext())) == null || !a2.booleanValue()) {
                    return Ao7.a(false);
                }
                FLogger.INSTANCE.getLocal().d(SleepSessionsRepository.TAG, "getSleepSessionList getActivityList processContinueFetching hasNext");
                this.$offset.element += this.$limit;
                return Ao7.a(true);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object processContinueFetching(Ku3 ku3, Xe6 xe6) {
                return processContinueFetching(ku3, (Xe6<? super Boolean>) xe6);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0041  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.mapped.Ku3 r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
                /*
                // Method dump skipped, instructions count: 245
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository.getSleepSessionList.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.mapped.Ku3, com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(Ku3 ku3, Xe6 xe6) {
                return saveCallResult(ku3, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(List<MFSleepSession> list) {
                return shouldFetch(list);
            }

            @DexIgnore
            public boolean shouldFetch(List<MFSleepSession> list) {
                return this.this$0.this$0.$shouldFetch && this.$downloadingDate != null;
            }
        }

        @DexIgnore
        public Anon1_Level2(SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2, Date date, Date date2, SleepDatabase sleepDatabase) {
            this.this$0 = sleepSessionsRepository$getSleepSessionList$Anon2;
            this.$startDate = date;
            this.$endDate = date2;
            this.$sleepDatabase = sleepDatabase;
        }

        @DexIgnore
        public final LiveData<H47<List<MFSleepSession>>> apply(List<FitnessDataWrapper> list) {
            Hh6 hh6 = new Hh6();
            hh6.element = 0;
            Wg6.b(list, "fitnessDataList");
            Date date = this.$startDate;
            Wg6.b(date, GoalPhase.COLUMN_START_DATE);
            Date date2 = this.$endDate;
            Wg6.b(date2, GoalPhase.COLUMN_END_DATE);
            return new Anon1_Level3(this, hh6, SQLiteDatabase.LOCK_ACQUIRED_WARNING_TIME_IN_MS, FitnessDataWrapperKt.calculateRangeDownload(list, date, date2)).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$getSleepSessionList$Anon2(SleepSessionsRepository sleepSessionsRepository, Date date, Date date2, boolean z, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSessionsRepository;
        this.$start = date;
        this.$end = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSessionsRepository$getSleepSessionList$Anon2 sleepSessionsRepository$getSleepSessionList$Anon2 = new SleepSessionsRepository$getSleepSessionList$Anon2(this.this$0, this.$start, this.$end, this.$shouldFetch, xe6);
        sleepSessionsRepository$getSleepSessionList$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSessionsRepository$getSleepSessionList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends List<MFSleepSession>>>> xe6) {
        throw null;
        //return ((SleepSessionsRepository$getSleepSessionList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00c6  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
            r11 = this;
            r10 = 0
            r9 = 2
            r8 = 1
            java.lang.Object r5 = com.fossil.Yn7.d()
            int r0 = r11.label
            if (r0 == 0) goto L_0x0075
            if (r0 == r8) goto L_0x0046
            if (r0 != r9) goto L_0x003e
            java.lang.Object r0 = r11.L$3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            java.lang.Object r1 = r11.L$2
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r11.L$1
            java.util.Date r2 = (java.util.Date) r2
            java.lang.Object r3 = r11.L$0
            com.mapped.Il6 r3 = (com.mapped.Il6) r3
            com.fossil.El7.b(r12)
            r3 = r12
            r4 = r1
            r5 = r0
        L_0x0025:
            r0 = r3
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.FitnessDataDao r1 = r5.getFitnessDataDao()
            java.util.Date r3 = r11.$start
            java.util.Date r5 = r11.$end
            androidx.lifecycle.LiveData r1 = r1.getFitnessDataLiveData(r3, r5)
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2 r3 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$Anon1_Level2
            r3.<init>(r11, r2, r4, r0)
            androidx.lifecycle.LiveData r0 = com.fossil.Ss0.c(r1, r3)
        L_0x003d:
            return r0
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r11.L$2
            java.util.Date r0 = (java.util.Date) r0
            java.lang.Object r1 = r11.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r11.L$0
            com.mapped.Il6 r2 = (com.mapped.Il6) r2
            com.fossil.El7.b(r12)
            r4 = r0
            r3 = r12
        L_0x0057:
            r0 = r3
            com.portfolio.platform.data.source.local.fitness.FitnessDatabase r0 = (com.portfolio.platform.data.source.local.fitness.FitnessDatabase) r0
            com.fossil.Dv7 r3 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2 r6 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$sleepDatabase$Anon1_Level2
            r6.<init>(r10)
            r11.L$0 = r2
            r11.L$1 = r1
            r11.L$2 = r4
            r11.L$3 = r0
            r11.label = r9
            java.lang.Object r3 = com.fossil.Eu7.g(r3, r6, r11)
            if (r3 != r5) goto L_0x00c6
            r0 = r5
            goto L_0x003d
        L_0x0075:
            com.fossil.El7.b(r12)
            com.mapped.Il6 r2 = r11.p$
            java.util.Date r0 = r11.$start
            java.util.Date r1 = com.mapped.TimeUtils.V(r0)
            java.util.Date r0 = r11.$end
            java.util.Date r0 = com.mapped.TimeUtils.E(r0)
            com.misfit.frameworks.buttonservice.log.FLogger r3 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r3 = r3.getLocal()
            java.lang.String r4 = com.portfolio.platform.data.source.SleepSessionsRepository.access$getTAG$cp()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "getSleepSessionList: start = "
            r6.append(r7)
            r6.append(r1)
            java.lang.String r7 = ", end = "
            r6.append(r7)
            r6.append(r0)
            java.lang.String r6 = r6.toString()
            r3.d(r4, r6)
            com.fossil.Dv7 r3 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2 r4 = new com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2$fitnessDatabase$Anon1_Level2
            r4.<init>(r10)
            r11.L$0 = r2
            r11.L$1 = r1
            r11.L$2 = r0
            r11.label = r8
            java.lang.Object r3 = com.fossil.Eu7.g(r3, r4, r11)
            if (r3 != r5) goto L_0x00ca
            r0 = r5
            goto L_0x003d
        L_0x00c6:
            r2 = r1
            r5 = r0
            goto L_0x0025
        L_0x00ca:
            r4 = r0
            goto L_0x0057
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$getSleepSessionList$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
