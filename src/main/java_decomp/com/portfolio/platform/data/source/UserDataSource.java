package com.portfolio.platform.data.source;

import com.fossil.Ao7;
import com.fossil.Kq5;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Xe6;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class UserDataSource {
    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationEmailExisting$suspendImpl(UserDataSource userDataSource, String str, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object checkAuthenticationSocialExisting$suspendImpl(UserDataSource userDataSource, String str, String str2, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSetting$suspendImpl(UserDataSource userDataSource, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object getUserSettingFromServer$suspendImpl(UserDataSource userDataSource, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loadUserInfo$suspendImpl(UserDataSource userDataSource, MFUser mFUser, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginEmail$suspendImpl(UserDataSource userDataSource, String str, String str2, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object loginWithSocial$suspendImpl(UserDataSource userDataSource, String str, String str2, String str3, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object requestEmailOtp$suspendImpl(UserDataSource userDataSource, String str, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object resetPassword$suspendImpl(UserDataSource userDataSource, String str, Xe6 xe6) {
        return new Kq5(Ao7.e(200), false, 2, null);
    }

    @DexIgnore
    public static /* synthetic */ Object sendUserSettingToServer$suspendImpl(UserDataSource userDataSource, UserSettings userSettings, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpEmail$suspendImpl(UserDataSource userDataSource, SignUpEmailAuth signUpEmailAuth, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object signUpSocial$suspendImpl(UserDataSource userDataSource, SignUpSocialAuth signUpSocialAuth, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public static /* synthetic */ Object verifyEmailOtp$suspendImpl(UserDataSource userDataSource, String str, String str2, Xe6 xe6) {
        return null;
    }

    @DexIgnore
    public Object checkAuthenticationEmailExisting(String str, Xe6<? super Ap4<Boolean>> xe6) {
        return checkAuthenticationEmailExisting$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object checkAuthenticationSocialExisting(String str, String str2, Xe6<? super Ap4<Boolean>> xe6) {
        return checkAuthenticationSocialExisting$suspendImpl(this, str, str2, xe6);
    }

    @DexIgnore
    public void clearAllUser() {
    }

    @DexIgnore
    public Object deleteUser(MFUser mFUser, Xe6<? super Integer> xe6) {
        return Ao7.e(0);
    }

    @DexIgnore
    public MFUser getCurrentUser() {
        return null;
    }

    @DexIgnore
    public Object getUserSetting(Xe6<? super UserSettings> xe6) {
        return getUserSetting$suspendImpl(this, xe6);
    }

    @DexIgnore
    public Object getUserSettingFromServer(Xe6<? super Ap4<UserSettings>> xe6) {
        return getUserSettingFromServer$suspendImpl(this, xe6);
    }

    @DexIgnore
    public abstract void insertUser(MFUser mFUser);

    @DexIgnore
    public Object insertUserSetting(UserSettings userSettings, Xe6<? super Cd6> xe6) {
        return Cd6.a;
    }

    @DexIgnore
    public Object loadUserInfo(MFUser mFUser, Xe6<? super Ap4<MFUser>> xe6) {
        return loadUserInfo$suspendImpl(this, mFUser, xe6);
    }

    @DexIgnore
    public Object loginEmail(String str, String str2, Xe6<? super Ap4<Auth>> xe6) {
        return loginEmail$suspendImpl(this, str, str2, xe6);
    }

    @DexIgnore
    public Object loginWithSocial(String str, String str2, String str3, Xe6<? super Ap4<Auth>> xe6) {
        return loginWithSocial$suspendImpl(this, str, str2, str3, xe6);
    }

    @DexIgnore
    public Object logoutUser(Xe6<? super Integer> xe6) {
        return Ao7.e(0);
    }

    @DexIgnore
    public Object requestEmailOtp(String str, Xe6<? super Ap4<Void>> xe6) {
        return requestEmailOtp$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object resetPassword(String str, Xe6<? super Ap4<Integer>> xe6) {
        return resetPassword$suspendImpl(this, str, xe6);
    }

    @DexIgnore
    public Object sendUserSettingToServer(UserSettings userSettings, Xe6<? super Ap4<UserSettings>> xe6) {
        return sendUserSettingToServer$suspendImpl(this, userSettings, xe6);
    }

    @DexIgnore
    public Object signUpEmail(SignUpEmailAuth signUpEmailAuth, Xe6<? super Ap4<Auth>> xe6) {
        return signUpEmail$suspendImpl(this, signUpEmailAuth, xe6);
    }

    @DexIgnore
    public Object signUpSocial(SignUpSocialAuth signUpSocialAuth, Xe6<? super Ap4<Auth>> xe6) {
        return signUpSocial$suspendImpl(this, signUpSocialAuth, xe6);
    }

    @DexIgnore
    public abstract Object updateUser(MFUser mFUser, boolean z, Xe6<? super Ap4<MFUser>> xe6);

    @DexIgnore
    public Object verifyEmailOtp(String str, String str2, Xe6<? super Ap4<Void>> xe6) {
        return verifyEmailOtp$suspendImpl(this, str, str2, xe6);
    }
}
