package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.SleepSessionsRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSessionsRepository$pushPendingSleepSessions$2", f = "SleepSessionsRepository.kt", l = {240, 241, 244}, m = "invokeSuspend")
public final class SleepSessionsRepository$pushPendingSleepSessions$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository.PushPendingSleepSessionsCallback $pushPendingSleepSessionsCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSessionsRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSessionsRepository$pushPendingSleepSessions$Anon2(SleepSessionsRepository sleepSessionsRepository, SleepSessionsRepository.PushPendingSleepSessionsCallback pushPendingSleepSessionsCallback, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSessionsRepository;
        this.$pushPendingSleepSessionsCallback = pushPendingSleepSessionsCallback;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        SleepSessionsRepository$pushPendingSleepSessions$Anon2 sleepSessionsRepository$pushPendingSleepSessions$Anon2 = new SleepSessionsRepository$pushPendingSleepSessions$Anon2(this.this$0, this.$pushPendingSleepSessionsCallback, xe6);
        sleepSessionsRepository$pushPendingSleepSessions$Anon2.p$ = (Il6) obj;
        throw null;
        //return sleepSessionsRepository$pushPendingSleepSessions$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((SleepSessionsRepository$pushPendingSleepSessions$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00a1  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00ae  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 3
            r4 = 2
            r2 = 1
            r5 = 0
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x008e
            if (r0 == r2) goto L_0x0066
            if (r0 == r4) goto L_0x002c
            if (r0 != r7) goto L_0x0024
            java.lang.Object r0 = r8.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r8.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
        L_0x0021:
            com.mapped.Cd6 r5 = com.mapped.Cd6.a
        L_0x0023:
            return r5
        L_0x0024:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x002c:
            java.lang.Object r0 = r8.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r8.L$0
            com.mapped.Il6 r1 = (com.mapped.Il6) r1
            com.fossil.El7.b(r9)
            r2 = r9
            r4 = r0
        L_0x0039:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x00a4
            java.lang.String r0 = r0.getUserId()
        L_0x0042:
            boolean r2 = r4.isEmpty()
            r2 = r2 ^ 1
            if (r2 == 0) goto L_0x00aa
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x00aa
            com.portfolio.platform.data.source.SleepSessionsRepository r2 = r8.this$0
            if (r0 == 0) goto L_0x00a6
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r3 = r8.$pushPendingSleepSessionsCallback
            r8.L$0 = r1
            r8.L$1 = r4
            r8.L$2 = r0
            r8.label = r7
            java.lang.Object r0 = r2.saveSleepSessionsToServer(r0, r4, r3, r8)
            if (r0 != r6) goto L_0x0021
            r5 = r6
            goto L_0x0023
        L_0x0066:
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
            r3 = r0
            r1 = r9
        L_0x006f:
            r0 = r1
            com.portfolio.platform.data.source.local.sleep.SleepDatabase r0 = (com.portfolio.platform.data.source.local.sleep.SleepDatabase) r0
            com.portfolio.platform.data.source.local.sleep.SleepDao r0 = r0.sleepDao()
            java.util.List r0 = r0.getPendingSleepSessions()
            com.portfolio.platform.data.source.SleepSessionsRepository r1 = r8.this$0
            com.portfolio.platform.data.source.UserRepository r1 = com.portfolio.platform.data.source.SleepSessionsRepository.access$getMUserRepository$p(r1)
            r8.L$0 = r3
            r8.L$1 = r0
            r8.label = r4
            java.lang.Object r2 = r1.getCurrentUser(r8)
            if (r2 != r6) goto L_0x00a1
            r5 = r6
            goto L_0x0023
        L_0x008e:
            com.fossil.El7.b(r9)
            com.mapped.Il6 r0 = r8.p$
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r8.L$0 = r0
            r8.label = r2
            java.lang.Object r1 = r1.D(r8)
            if (r1 != r6) goto L_0x00b7
            r5 = r6
            goto L_0x0023
        L_0x00a1:
            r1 = r3
            r4 = r0
            goto L_0x0039
        L_0x00a4:
            r0 = r5
            goto L_0x0042
        L_0x00a6:
            com.mapped.Wg6.i()
            throw r5
        L_0x00aa:
            com.portfolio.platform.data.source.SleepSessionsRepository$PushPendingSleepSessionsCallback r0 = r8.$pushPendingSleepSessionsCallback
            if (r0 == 0) goto L_0x0023
            r1 = 404(0x194, float:5.66E-43)
            r0.onFail(r1)
            com.mapped.Cd6 r5 = com.mapped.Cd6.a
            goto L_0x0023
        L_0x00b7:
            r3 = r0
            goto L_0x006f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.SleepSessionsRepository$pushPendingSleepSessions$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
