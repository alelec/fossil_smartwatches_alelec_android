package com.portfolio.platform.data.source;

import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.data.source.local.CategoryDao;
import com.portfolio.platform.data.source.remote.CategoryRemoteDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "CategoryRepository";
    @DexIgnore
    public /* final */ CategoryDao mCategoryDao;
    @DexIgnore
    public /* final */ CategoryRemoteDataSource mCategoryRemoteDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public CategoryRepository(CategoryDao categoryDao, CategoryRemoteDataSource categoryRemoteDataSource) {
        Wg6.c(categoryDao, "mCategoryDao");
        Wg6.c(categoryRemoteDataSource, "mCategoryRemoteDataSource");
        this.mCategoryDao = categoryDao;
        this.mCategoryRemoteDataSource = categoryRemoteDataSource;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object downloadCategories(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
        // Method dump skipped, instructions count: 238
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.CategoryRepository.downloadCategories(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final List<Category> getAllCategories() {
        return this.mCategoryDao.getAllCategory();
    }
}
