package com.portfolio.platform.data.source.remote;

import com.mapped.Qg6;
import com.mapped.Wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaRingRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion(null);
    @DexIgnore
    public static /* final */ String TAG; // = "DianaRingRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(Qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DianaRingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        Wg6.c(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object getDianaWatchFaceRings(com.mapped.Xe6<? super com.mapped.Ap4<java.util.List<com.portfolio.platform.data.model.watchface.DianaWatchFaceRing>>> r9) {
        /*
            r8 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            r4 = 0
            boolean r0 = r9 instanceof com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$Anon1
            if (r0 == 0) goto L_0x005a
            r0 = r9
            com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$Anon1 r0 = (com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$Anon1) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x005a
            int r1 = r1 + r3
            r0.label = r1
            r1 = r0
        L_0x0015:
            java.lang.Object r2 = r1.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r1.label
            if (r0 == 0) goto L_0x0069
            if (r0 != r5) goto L_0x0061
            java.lang.Object r0 = r1.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r1.L$0
            com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource r0 = (com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource) r0
            com.fossil.El7.b(r2)
            r0 = r2
        L_0x002d:
            com.mapped.Ap4 r0 = (com.mapped.Ap4) r0
            boolean r1 = r0 instanceof com.fossil.Kq5
            if (r1 == 0) goto L_0x0083
            java.util.ArrayList r2 = new java.util.ArrayList
            r2.<init>()
            com.fossil.Kq5 r0 = (com.fossil.Kq5) r0
            java.lang.Object r1 = r0.a()
            com.portfolio.platform.data.source.remote.ApiResponse r1 = (com.portfolio.platform.data.source.remote.ApiResponse) r1
            if (r1 == 0) goto L_0x004f
            java.util.List r1 = r1.get_items()
            if (r1 == 0) goto L_0x004f
            boolean r1 = r2.addAll(r1)
            com.fossil.Ao7.a(r1)
        L_0x004f:
            com.fossil.Kq5 r1 = new com.fossil.Kq5
            boolean r0 = r0.b()
            r1.<init>(r2, r0)
            r0 = r1
        L_0x0059:
            return r0
        L_0x005a:
            com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$Anon1 r0 = new com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$Anon1
            r0.<init>(r8, r9)
            r1 = r0
            goto L_0x0015
        L_0x0061:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0069:
            com.fossil.El7.b(r2)
            com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$response$Anon1 r0 = new com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource$getDianaWatchFaceRings$response$Anon1
            java.lang.String r2 = "-metadata.priority"
            r0.<init>(r8, r2, r4)
            r1.L$0 = r8
            java.lang.String r2 = "-metadata.priority"
            r1.L$1 = r2
            r1.label = r5
            java.lang.Object r0 = com.portfolio.platform.response.ResponseKt.d(r0, r1)
            if (r0 != r3) goto L_0x002d
            r0 = r3
            goto L_0x0059
        L_0x0083:
            boolean r1 = r0 instanceof com.fossil.Hq5
            if (r1 == 0) goto L_0x00a0
            r3 = r0
            com.fossil.Hq5 r3 = (com.fossil.Hq5) r3
            com.fossil.Hq5 r0 = new com.fossil.Hq5
            int r1 = r3.a()
            com.portfolio.platform.data.model.ServerError r2 = r3.c()
            java.lang.Throwable r3 = r3.d()
            r6 = 24
            r5 = r4
            r7 = r4
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            goto L_0x0059
        L_0x00a0:
            com.mapped.Kc6 r0 = new com.mapped.Kc6
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.remote.DianaRingRemoteDataSource.getDianaWatchFaceRings(com.mapped.Xe6):java.lang.Object");
    }
}
