package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import com.fossil.El7;
import com.fossil.Hq5;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.ServerError;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1", f = "GoalTrackingSummaryLocalDataSource.kt", l = {144, 148, 152, 157}, m = "invokeSuspend")
public final class GoalTrackingSummaryLocalDataSource$loadData$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Bi.Aii $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ PagingRequestHelper.Di $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$1", f = "GoalTrackingSummaryLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.$helperCallback.b();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$1$2", f = "GoalTrackingSummaryLocalDataSource.kt", l = {}, m = "invokeSuspend")
    public static final class Anon2_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ap4 $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingSummaryLocalDataSource$loadData$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon2_Level2(GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1, Ap4 ap4, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingSummaryLocalDataSource$loadData$Anon1;
            this.$data = ap4;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon2_Level2 anon2_Level2 = new Anon2_Level2(this.this$0, this.$data, xe6);
            anon2_Level2.p$ = (Il6) obj;
            throw null;
            //return anon2_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Anon2_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                if (((Hq5) this.$data).d() != null) {
                    this.this$0.$helperCallback.a(((Hq5) this.$data).d());
                } else if (((Hq5) this.$data).c() != null) {
                    ServerError c = ((Hq5) this.$data).c();
                    PagingRequestHelper.Bi.Aii aii = this.this$0.$helperCallback;
                    String userMessage = c.getUserMessage();
                    if (userMessage == null) {
                        userMessage = c.getMessage();
                    }
                    if (userMessage == null) {
                        userMessage = "";
                    }
                    aii.a(new Throwable(userMessage));
                }
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingSummaryLocalDataSource$loadData$Anon1(GoalTrackingSummaryLocalDataSource goalTrackingSummaryLocalDataSource, Date date, Date date2, PagingRequestHelper.Di di, PagingRequestHelper.Bi.Aii aii, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = di;
        this.$helperCallback = aii;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingSummaryLocalDataSource$loadData$Anon1 goalTrackingSummaryLocalDataSource$loadData$Anon1 = new GoalTrackingSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, xe6);
        goalTrackingSummaryLocalDataSource$loadData$Anon1.p$ = (Il6) obj;
        throw null;
        //return goalTrackingSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingSummaryLocalDataSource$loadData$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0125  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r12) {
        /*
        // Method dump skipped, instructions count: 303
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource$loadData$Anon1.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
