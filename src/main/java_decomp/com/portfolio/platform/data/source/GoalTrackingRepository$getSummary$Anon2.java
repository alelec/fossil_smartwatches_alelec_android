package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.H47;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.util.NetworkBoundResource;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$2", f = "GoalTrackingRepository.kt", l = {175, 177}, m = "invokeSuspend")
public final class GoalTrackingRepository$getSummary$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super LiveData<H47<? extends GoalTrackingSummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $date;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDatabase $goalTrackingDb;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingRepository$getSummary$Anon2 this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Anon1_Level3 extends NetworkBoundResource<GoalTrackingSummary, GoalDailySummary> {
            @DexIgnore
            public /* final */ /* synthetic */ List $pendingList;
            @DexIgnore
            public /* final */ /* synthetic */ Anon1_Level2 this$0;

            @DexIgnore
            public Anon1_Level3(Anon1_Level2 anon1_Level2, List list) {
                this.this$0 = anon1_Level2;
                this.$pendingList = list;
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object createCall(Xe6<? super Q88<GoalDailySummary>> xe6) {
                ApiServiceV2 apiServiceV2 = this.this$0.this$0.this$0.mApiServiceV2;
                String k = TimeUtils.k(this.this$0.this$0.$date);
                Wg6.b(k, "DateHelper.formatShortDate(date)");
                return apiServiceV2.getGoalTrackingSummary(k, xe6);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public Object loadFromDb(Xe6<? super LiveData<GoalTrackingSummary>> xe6) {
                return this.this$0.$goalTrackingDb.getGoalTrackingDao().getGoalTrackingSummaryLiveData(this.this$0.this$0.$date);
            }

            @DexIgnore
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public void onFetchFailed(Throwable th) {
                FLogger.INSTANCE.getLocal().e(GoalTrackingRepository.Companion.getTAG(), "getSummary onFetchFailed");
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:15:0x003d  */
            /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.Object saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r7, com.mapped.Xe6<? super com.mapped.Cd6> r8) {
                /*
                    r6 = this;
                    r4 = 1
                    r3 = -2147483648(0xffffffff80000000, float:-0.0)
                    boolean r0 = r8 instanceof com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    if (r0 == 0) goto L_0x002e
                    r0 = r8
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4) r0
                    int r1 = r0.label
                    r2 = r1 & r3
                    if (r2 == 0) goto L_0x002e
                    int r1 = r1 + r3
                    r0.label = r1
                    r1 = r0
                L_0x0014:
                    java.lang.Object r2 = r1.result
                    java.lang.Object r0 = com.fossil.Yn7.d()
                    int r3 = r1.label
                    if (r3 == 0) goto L_0x003d
                    if (r3 != r4) goto L_0x0035
                    java.lang.Object r0 = r1.L$1
                    com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary r0 = (com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary) r0
                    java.lang.Object r0 = r1.L$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3 r0 = (com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3) r0
                    com.fossil.El7.b(r2)     // Catch:{ Exception -> 0x00bb }
                L_0x002b:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x002d:
                    return r0
                L_0x002e:
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4 r0 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon1_Level4
                    r0.<init>(r6, r8)
                    r1 = r0
                    goto L_0x0014
                L_0x0035:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x003d:
                    com.fossil.El7.b(r2)
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r3 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r3 = r3.getTAG()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getSummary date="
                    r4.append(r5)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r5 = r6.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r5 = r5.this$0
                    java.util.Date r5 = r5.$date
                    r4.append(r5)
                    java.lang.String r5 = " saveCallResult onResponse: response = "
                    r4.append(r5)
                    r4.append(r7)
                    java.lang.String r4 = r4.toString()
                    r2.d(r3, r4)
                    com.fossil.Dv7 r2 = com.fossil.Bw7.b()     // Catch:{ Exception -> 0x0086 }
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4 r3 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2$Anon1_Level3$saveCallResult$Anon2_Level4     // Catch:{ Exception -> 0x0086 }
                    r4 = 0
                    r3.<init>(r6, r7, r4)     // Catch:{ Exception -> 0x0086 }
                    r1.L$0 = r6     // Catch:{ Exception -> 0x0086 }
                    r1.L$1 = r7     // Catch:{ Exception -> 0x0086 }
                    r4 = 1
                    r1.label = r4     // Catch:{ Exception -> 0x0086 }
                    java.lang.Object r1 = com.fossil.Eu7.g(r2, r3, r1)     // Catch:{ Exception -> 0x0086 }
                    if (r1 != r0) goto L_0x002b
                    goto L_0x002d
                L_0x0086:
                    r1 = move-exception
                    r0 = r6
                L_0x0088:
                    com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                    com.portfolio.platform.data.source.GoalTrackingRepository$Companion r3 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
                    java.lang.String r3 = r3.getTAG()
                    java.lang.StringBuilder r4 = new java.lang.StringBuilder
                    r4.<init>()
                    java.lang.String r5 = "getSummary date="
                    r4.append(r5)
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r0 = r0.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2 r0 = r0.this$0
                    java.util.Date r0 = r0.$date
                    r4.append(r0)
                    java.lang.String r0 = " exception="
                    r4.append(r0)
                    r4.append(r1)
                    java.lang.String r0 = r4.toString()
                    r2.e(r3, r0)
                    r1.printStackTrace()
                    goto L_0x002b
                L_0x00bb:
                    r1 = move-exception
                    goto L_0x0088
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository.getSummary.Anon2.Anon1_Level2.Anon1_Level3.saveCallResult(com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary, com.mapped.Xe6):java.lang.Object");
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, com.mapped.Xe6] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ Object saveCallResult(GoalDailySummary goalDailySummary, Xe6 xe6) {
                return saveCallResult(goalDailySummary, (Xe6<? super Cd6>) xe6);
            }

            @DexIgnore
            public boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
                return this.$pendingList.isEmpty();
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.portfolio.platform.util.NetworkBoundResource
            public /* bridge */ /* synthetic */ boolean shouldFetch(GoalTrackingSummary goalTrackingSummary) {
                return shouldFetch(goalTrackingSummary);
            }
        }

        @DexIgnore
        public Anon1_Level2(GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2, GoalTrackingDatabase goalTrackingDatabase) {
            this.this$0 = goalTrackingRepository$getSummary$Anon2;
            this.$goalTrackingDb = goalTrackingDatabase;
        }

        @DexIgnore
        public final LiveData<H47<GoalTrackingSummary>> apply(List<GoalTrackingData> list) {
            return new Anon1_Level3(this, list).asLiveData();
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return apply((List) obj);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$getSummary$Anon2(GoalTrackingRepository goalTrackingRepository, Date date, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$date = date;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$getSummary$Anon2 goalTrackingRepository$getSummary$Anon2 = new GoalTrackingRepository$getSummary$Anon2(this.this$0, this.$date, xe6);
        goalTrackingRepository$getSummary$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$getSummary$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super LiveData<H47<? extends GoalTrackingSummary>>> xe6) {
        throw null;
        //return ((GoalTrackingRepository$getSummary$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x008b  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r9) {
        /*
            r8 = this;
            r7 = 2
            r6 = 1
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r8.label
            if (r0 == 0) goto L_0x004e
            if (r0 == r6) goto L_0x0030
            if (r0 != r7) goto L_0x0028
            java.lang.Object r0 = r8.L$1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            java.lang.Object r1 = r8.L$0
            com.mapped.Il6 r1 = (com.mapped.Il6) r1
            com.fossil.El7.b(r9)
            r1 = r9
            r2 = r0
        L_0x001b:
            r0 = r1
            androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2 r1 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$Anon1_Level2
            r1.<init>(r8, r2)
            androidx.lifecycle.LiveData r0 = com.fossil.Ss0.c(r0, r1)
        L_0x0027:
            return r0
        L_0x0028:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0030:
            java.lang.Object r0 = r8.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r9)
            r2 = r0
            r1 = r9
        L_0x0039:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = r8.this$0
            java.util.Date r4 = r8.$date
            r8.L$0 = r2
            r8.L$1 = r0
            r8.label = r7
            java.lang.Object r1 = r1.getPendingGoalTrackingDataListLiveData(r4, r4, r8)
            if (r1 != r3) goto L_0x008b
            r0 = r3
            goto L_0x0027
        L_0x004e:
            com.fossil.El7.b(r9)
            com.mapped.Il6 r0 = r8.p$
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            com.portfolio.platform.data.source.GoalTrackingRepository$Companion r2 = com.portfolio.platform.data.source.GoalTrackingRepository.Companion
            java.lang.String r2 = r2.getTAG()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "getSummary date="
            r4.append(r5)
            java.util.Date r5 = r8.$date
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r1.d(r2, r4)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2 r2 = new com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2$goalTrackingDb$Anon1_Level2
            r4 = 0
            r2.<init>(r4)
            r8.L$0 = r0
            r8.label = r6
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
            if (r1 != r3) goto L_0x008d
            r0 = r3
            goto L_0x0027
        L_0x008b:
            r2 = r0
            goto L_0x001b
        L_0x008d:
            r2 = r0
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$getSummary$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
