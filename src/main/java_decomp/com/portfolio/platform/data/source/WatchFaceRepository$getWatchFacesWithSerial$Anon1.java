package com.portfolio.platform.data.source;

import androidx.recyclerview.widget.RecyclerView;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.WatchFaceRepository", f = "WatchFaceRepository.kt", l = {71}, m = "getWatchFacesWithSerial")
public final class WatchFaceRepository$getWatchFacesWithSerial$Anon1 extends Jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ WatchFaceRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WatchFaceRepository$getWatchFacesWithSerial$Anon1(WatchFaceRepository watchFaceRepository, Xe6 xe6) {
        super(xe6);
        this.this$0 = watchFaceRepository;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= RecyclerView.UNDEFINED_DURATION;
        return this.this$0.getWatchFacesWithSerial(null, this);
    }
}
