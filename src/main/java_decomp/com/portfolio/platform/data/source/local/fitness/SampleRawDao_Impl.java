package com.portfolio.platform.data.source.local.fitness;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Lz4;
import com.fossil.Pz4;
import com.fossil.wearables.fsl.fitness.SampleDay;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SampleRawDao_Impl extends SampleRawDao {
    @DexIgnore
    public /* final */ Lz4 __activityIntensitiesConverter; // = new Lz4();
    @DexIgnore
    public /* final */ Pz4 __dateLongStringConverter; // = new Pz4();
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<SampleRaw> __insertionAdapterOfSampleRaw;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfDeleteAllActivitySamples;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<SampleRaw> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, SampleRaw sampleRaw) {
            if (sampleRaw.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, sampleRaw.getId());
            }
            mi.bindLong(2, (long) sampleRaw.getPinType());
            mi.bindLong(3, (long) sampleRaw.getUaPinType());
            String a2 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getStartTime());
            if (a2 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a2);
            }
            String a3 = SampleRawDao_Impl.this.__dateLongStringConverter.a(sampleRaw.getEndTime());
            if (a3 == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, a3);
            }
            if (sampleRaw.getSourceId() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, sampleRaw.getSourceId());
            }
            if (sampleRaw.getSourceTypeValue() == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, sampleRaw.getSourceTypeValue());
            }
            if (sampleRaw.getMovementTypeValue() == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, sampleRaw.getMovementTypeValue());
            }
            mi.bindDouble(9, sampleRaw.getSteps());
            mi.bindDouble(10, sampleRaw.getCalories());
            mi.bindDouble(11, sampleRaw.getDistance());
            mi.bindLong(12, (long) sampleRaw.getActiveTime());
            String a4 = SampleRawDao_Impl.this.__activityIntensitiesConverter.a(sampleRaw.getIntensityDistInSteps());
            if (a4 == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, a4);
            }
            if (sampleRaw.getTimeZoneID() == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, sampleRaw.getTimeZoneID());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, SampleRaw sampleRaw) {
            bind(mi, sampleRaw);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `sampleraw` (`id`,`pinType`,`uaPinType`,`startTime`,`endTime`,`sourceId`,`sourceTypeValue`,`movementTypeValue`,`steps`,`calories`,`distance`,`activeTime`,`intensityDistInSteps`,`timeZoneID`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM sampleraw";
        }
    }

    @DexIgnore
    public SampleRawDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfSampleRaw = new Anon1(oh);
        this.__preparedStmtOfDeleteAllActivitySamples = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void deleteAllActivitySamples() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfDeleteAllActivitySamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllActivitySamples.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getListActivitySampleByUaType(int i) {
        Rh f = Rh.f("SELECT * FROM sampleraw WHERE uaPinType = ?", 1);
        f.bindLong(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "pinType");
            int c3 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = Dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples() {
        Rh f = Rh.f("SELECT * FROM sampleraw WHERE pinType <> 0", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "pinType");
            int c3 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = Dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public List<SampleRaw> getPendingActivitySamples(Date date, Date date2) {
        Rh f = Rh.f("SELECT * FROM sampleraw WHERE startTime >= ? AND startTime < ? AND pinType <> 0", 2);
        String a2 = this.__dateLongStringConverter.a(date);
        if (a2 == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, a2);
        }
        String a3 = this.__dateLongStringConverter.a(date2);
        if (a3 == null) {
            f.bindNull(2);
        } else {
            f.bindString(2, a3);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "pinType");
            int c3 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_UA_PIN_TYPE);
            int c4 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_START_TIME);
            int c5 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_END_TIME);
            int c6 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_ID);
            int c7 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_SOURCE_TYPE_VALUE);
            int c8 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_MOVEMENT_TYPE_VALUE);
            int c9 = Dx0.c(b, "steps");
            int c10 = Dx0.c(b, "calories");
            int c11 = Dx0.c(b, "distance");
            int c12 = Dx0.c(b, SampleDay.COLUMN_ACTIVE_TIME);
            int c13 = Dx0.c(b, "intensityDistInSteps");
            try {
                int c14 = Dx0.c(b, com.fossil.wearables.fsl.fitness.SampleRaw.COLUMN_TIMEZONE_ID);
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    SampleRaw sampleRaw = new SampleRaw(this.__dateLongStringConverter.b(b.getString(c4)), this.__dateLongStringConverter.b(b.getString(c5)), b.getString(c6), b.getString(c7), b.getString(c8), b.getDouble(c9), b.getDouble(c10), b.getDouble(c11), b.getInt(c12), this.__activityIntensitiesConverter.b(b.getString(c13)), b.getString(c14));
                    sampleRaw.setId(b.getString(c));
                    sampleRaw.setPinType(b.getInt(c2));
                    sampleRaw.setUaPinType(b.getInt(c3));
                    arrayList.add(sampleRaw);
                }
                b.close();
                f.m();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.fitness.SampleRawDao
    public void upsertListActivitySample(List<SampleRaw> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSampleRaw.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
