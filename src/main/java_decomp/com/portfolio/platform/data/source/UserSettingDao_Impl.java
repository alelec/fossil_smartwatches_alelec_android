package com.portfolio.platform.data.source;

import android.database.Cursor;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.O05;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.UserSettings;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettingDao_Impl implements UserSettingDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<UserSettings> __insertionAdapterOfUserSettings;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ O05 __userSettingConverter; // = new O05();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<UserSettings> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, UserSettings userSettings) {
            mi.bindLong(1, userSettings.isShowGoalRing() ? 1 : 0);
            String a2 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedLocationDataSharing());
            if (a2 == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, a2);
            }
            String a3 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedPrivacies());
            if (a3 == null) {
                mi.bindNull(3);
            } else {
                mi.bindString(3, a3);
            }
            String a4 = UserSettingDao_Impl.this.__userSettingConverter.a(userSettings.getAcceptedTermsOfService());
            if (a4 == null) {
                mi.bindNull(4);
            } else {
                mi.bindString(4, a4);
            }
            if (userSettings.getUid() == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, userSettings.getUid());
            }
            if (userSettings.getId() == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, userSettings.getId());
            }
            mi.bindLong(7, userSettings.isLatestLocationDataSharingAccepted() ? 1 : 0);
            mi.bindLong(8, userSettings.isLatestPrivacyAccepted() ? 1 : 0);
            mi.bindLong(9, userSettings.isLatestTermsOfServiceAccepted() ? 1 : 0);
            if (userSettings.getLatestLocationDataSharingVersion() == null) {
                mi.bindNull(10);
            } else {
                mi.bindString(10, userSettings.getLatestLocationDataSharingVersion());
            }
            if (userSettings.getLatestPrivacyVersion() == null) {
                mi.bindNull(11);
            } else {
                mi.bindString(11, userSettings.getLatestPrivacyVersion());
            }
            if (userSettings.getLatestTermsOfServiceVersion() == null) {
                mi.bindNull(12);
            } else {
                mi.bindString(12, userSettings.getLatestTermsOfServiceVersion());
            }
            if (userSettings.getStartDayOfWeek() == null) {
                mi.bindNull(13);
            } else {
                mi.bindString(13, userSettings.getStartDayOfWeek());
            }
            if (userSettings.getCreatedAt() == null) {
                mi.bindNull(14);
            } else {
                mi.bindString(14, userSettings.getCreatedAt());
            }
            if (userSettings.getUpdatedAt() == null) {
                mi.bindNull(15);
            } else {
                mi.bindString(15, userSettings.getUpdatedAt());
            }
            mi.bindLong(16, (long) userSettings.getPinType());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, UserSettings userSettings) {
            bind(mi, userSettings);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `userSettings` (`isShowGoalRing`,`acceptedLocationDataSharing`,`acceptedPrivacies`,`acceptedTermsOfService`,`uid`,`id`,`isLatestLocationDataSharingAccepted`,`isLatestPrivacyAccepted`,`isLatestTermsOfServiceAccepted`,`latestLocationDataSharingVersion`,`latestPrivacyVersion`,`latestTermsOfServiceVersion`,`startDayOfWeek`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM userSettings";
        }
    }

    @DexIgnore
    public UserSettingDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfUserSettings = new Anon1(oh);
        this.__preparedStmtOfCleanUp = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void addOrUpdateUserSetting(UserSettings userSettings) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfUserSettings.insert((Hh<UserSettings>) userSettings);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getCurrentUserSetting() {
        Throwable th;
        UserSettings userSettings;
        Rh f = Rh.f("SELECT * FROM userSettings LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "isShowGoalRing");
            int c2 = Dx0.c(b, "acceptedLocationDataSharing");
            int c3 = Dx0.c(b, "acceptedPrivacies");
            int c4 = Dx0.c(b, "acceptedTermsOfService");
            int c5 = Dx0.c(b, "uid");
            int c6 = Dx0.c(b, "id");
            int c7 = Dx0.c(b, "isLatestLocationDataSharingAccepted");
            int c8 = Dx0.c(b, "isLatestPrivacyAccepted");
            int c9 = Dx0.c(b, "isLatestTermsOfServiceAccepted");
            int c10 = Dx0.c(b, "latestLocationDataSharingVersion");
            int c11 = Dx0.c(b, "latestPrivacyVersion");
            int c12 = Dx0.c(b, "latestTermsOfServiceVersion");
            int c13 = Dx0.c(b, "startDayOfWeek");
            try {
                int c14 = Dx0.c(b, "createdAt");
                int c15 = Dx0.c(b, "updatedAt");
                int c16 = Dx0.c(b, "pinType");
                if (b.moveToFirst()) {
                    userSettings = new UserSettings();
                    userSettings.setShowGoalRing(b.getInt(c) != 0);
                    userSettings.setAcceptedLocationDataSharing(this.__userSettingConverter.b(b.getString(c2)));
                    userSettings.setAcceptedPrivacies(this.__userSettingConverter.b(b.getString(c3)));
                    userSettings.setAcceptedTermsOfService(this.__userSettingConverter.b(b.getString(c4)));
                    userSettings.setUid(b.getString(c5));
                    userSettings.setId(b.getString(c6));
                    userSettings.setLatestLocationDataSharingAccepted(b.getInt(c7) != 0);
                    userSettings.setLatestPrivacyAccepted(b.getInt(c8) != 0);
                    userSettings.setLatestTermsOfServiceAccepted(b.getInt(c9) != 0);
                    userSettings.setLatestLocationDataSharingVersion(b.getString(c10));
                    userSettings.setLatestPrivacyVersion(b.getString(c11));
                    userSettings.setLatestTermsOfServiceVersion(b.getString(c12));
                    userSettings.setStartDayOfWeek(b.getString(c13));
                    userSettings.setCreatedAt(b.getString(c14));
                    userSettings.setUpdatedAt(b.getString(c15));
                    userSettings.setPinType(b.getInt(c16));
                } else {
                    userSettings = null;
                }
                b.close();
                f.m();
                return userSettings;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.UserSettingDao
    public UserSettings getPendingUserSetting() {
        Throwable th;
        UserSettings userSettings;
        Rh f = Rh.f("SELECT * FROM userSettings WHERE pinType = 1 LIMIT 1", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "isShowGoalRing");
            int c2 = Dx0.c(b, "acceptedLocationDataSharing");
            int c3 = Dx0.c(b, "acceptedPrivacies");
            int c4 = Dx0.c(b, "acceptedTermsOfService");
            int c5 = Dx0.c(b, "uid");
            int c6 = Dx0.c(b, "id");
            int c7 = Dx0.c(b, "isLatestLocationDataSharingAccepted");
            int c8 = Dx0.c(b, "isLatestPrivacyAccepted");
            int c9 = Dx0.c(b, "isLatestTermsOfServiceAccepted");
            int c10 = Dx0.c(b, "latestLocationDataSharingVersion");
            int c11 = Dx0.c(b, "latestPrivacyVersion");
            int c12 = Dx0.c(b, "latestTermsOfServiceVersion");
            int c13 = Dx0.c(b, "startDayOfWeek");
            try {
                int c14 = Dx0.c(b, "createdAt");
                int c15 = Dx0.c(b, "updatedAt");
                int c16 = Dx0.c(b, "pinType");
                if (b.moveToFirst()) {
                    userSettings = new UserSettings();
                    userSettings.setShowGoalRing(b.getInt(c) != 0);
                    userSettings.setAcceptedLocationDataSharing(this.__userSettingConverter.b(b.getString(c2)));
                    userSettings.setAcceptedPrivacies(this.__userSettingConverter.b(b.getString(c3)));
                    userSettings.setAcceptedTermsOfService(this.__userSettingConverter.b(b.getString(c4)));
                    userSettings.setUid(b.getString(c5));
                    userSettings.setId(b.getString(c6));
                    userSettings.setLatestLocationDataSharingAccepted(b.getInt(c7) != 0);
                    userSettings.setLatestPrivacyAccepted(b.getInt(c8) != 0);
                    userSettings.setLatestTermsOfServiceAccepted(b.getInt(c9) != 0);
                    userSettings.setLatestLocationDataSharingVersion(b.getString(c10));
                    userSettings.setLatestPrivacyVersion(b.getString(c11));
                    userSettings.setLatestTermsOfServiceVersion(b.getString(c12));
                    userSettings.setStartDayOfWeek(b.getString(c13));
                    userSettings.setCreatedAt(b.getString(c14));
                    userSettings.setUpdatedAt(b.getString(c15));
                    userSettings.setPinType(b.getInt(c16));
                } else {
                    userSettings = null;
                }
                b.close();
                f.m();
                return userSettings;
            } catch (Throwable th2) {
                th = th2;
                b.close();
                f.m();
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            b.close();
            f.m();
            throw th;
        }
    }
}
