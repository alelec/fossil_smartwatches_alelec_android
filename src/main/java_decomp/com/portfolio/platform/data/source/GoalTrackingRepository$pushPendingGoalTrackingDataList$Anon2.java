package com.portfolio.platform.data.source;

import com.fossil.Ko7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.source.GoalTrackingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$2", f = "GoalTrackingRepository.kt", l = {624, 627}, m = "invokeSuspend")
public final class GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository.PushPendingGoalTrackingDataListCallback $pushPendingGoalTrackingDataListCallback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(GoalTrackingRepository goalTrackingRepository, GoalTrackingRepository.PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, Xe6 xe6) {
        super(2, xe6);
        this.this$0 = goalTrackingRepository;
        this.$pushPendingGoalTrackingDataListCallback = pushPendingGoalTrackingDataListCallback;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2 = new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2(this.this$0, this.$pushPendingGoalTrackingDataListCallback, xe6);
        goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2.p$ = (Il6) obj;
        throw null;
        //return goalTrackingRepository$pushPendingGoalTrackingDataList$Anon2;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0063  */
    @Override // com.fossil.Zn7
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object invokeSuspend(java.lang.Object r7) {
        /*
            r6 = this;
            r5 = 2
            r2 = 1
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r0 = r6.label
            if (r0 == 0) goto L_0x0050
            if (r0 == r2) goto L_0x0024
            if (r0 != r5) goto L_0x001c
            java.lang.Object r0 = r6.L$1
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r6.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r7)
        L_0x0019:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x001b:
            return r0
        L_0x001c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0024:
            java.lang.Object r0 = r6.L$0
            com.mapped.Il6 r0 = (com.mapped.Il6) r0
            com.fossil.El7.b(r7)
            r2 = r0
            r1 = r7
        L_0x002d:
            r0 = r1
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase r0 = (com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase) r0
            com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao r0 = r0.getGoalTrackingDao()
            java.util.List r0 = r0.getPendingGoalTrackingDataList()
            int r1 = r0.size()
            if (r1 <= 0) goto L_0x0063
            com.portfolio.platform.data.source.GoalTrackingRepository r1 = r6.this$0
            com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback r4 = r6.$pushPendingGoalTrackingDataListCallback
            r6.L$0 = r2
            r6.L$1 = r0
            r6.label = r5
            java.lang.Object r0 = r1.saveGoalTrackingDataListToServer(r0, r4, r6)
            if (r0 != r3) goto L_0x0019
            r0 = r3
            goto L_0x001b
        L_0x0050:
            com.fossil.El7.b(r7)
            com.mapped.Il6 r0 = r6.p$
            com.portfolio.platform.manager.EncryptedDatabaseManager r1 = com.portfolio.platform.manager.EncryptedDatabaseManager.j
            r6.L$0 = r0
            r6.label = r2
            java.lang.Object r1 = r1.A(r6)
            if (r1 != r3) goto L_0x0071
            r0 = r3
            goto L_0x001b
        L_0x0063:
            com.portfolio.platform.data.source.GoalTrackingRepository$PushPendingGoalTrackingDataListCallback r0 = r6.$pushPendingGoalTrackingDataListCallback
            if (r0 == 0) goto L_0x006f
            r1 = 404(0x194, float:5.66E-43)
            r0.onFail(r1)
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
            goto L_0x001b
        L_0x006f:
            r0 = 0
            goto L_0x001b
        L_0x0071:
            r2 = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.data.source.GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon2.invokeSuspend(java.lang.Object):java.lang.Object");
    }
}
