package com.portfolio.platform.data.source;

import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Q88;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Hg6;
import com.mapped.Ku3;
import com.mapped.Lf6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.room.sleep.MFSleepSettings;
import com.portfolio.platform.data.source.remote.ApiServiceV2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@Lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository$updateLastSleepGoal$response$1", f = "SleepSummariesRepository.kt", l = {92}, m = "invokeSuspend")
public final class SleepSummariesRepository$updateLastSleepGoal$response$Anon1 extends Ko7 implements Hg6<Xe6<? super Q88<MFSleepSettings>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Ku3 $jsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$updateLastSleepGoal$response$Anon1(SleepSummariesRepository sleepSummariesRepository, Ku3 ku3, Xe6 xe6) {
        super(1, xe6);
        this.this$0 = sleepSummariesRepository;
        this.$jsonObject = ku3;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        return new SleepSummariesRepository$updateLastSleepGoal$response$Anon1(this.this$0, this.$jsonObject, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public final Object invoke(Xe6<? super Q88<MFSleepSettings>> xe6) {
        throw null;
        //return ((SleepSummariesRepository$updateLastSleepGoal$response$Anon1) create(xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            ApiServiceV2 apiServiceV2 = this.this$0.mApiService;
            Ku3 ku3 = this.$jsonObject;
            this.label = 1;
            Object sleepSetting = apiServiceV2.setSleepSetting(ku3, this);
            return sleepSetting == d ? d : sleepSetting;
        } else if (i == 1) {
            El7.b(obj);
            return obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }
}
