package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionsRepository_Factory implements Factory<SleepSessionsRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<UserRepository> mUserRepositoryProvider;

    @DexIgnore
    public SleepSessionsRepository_Factory(Provider<UserRepository> provider, Provider<ApiServiceV2> provider2) {
        this.mUserRepositoryProvider = provider;
        this.mApiServiceProvider = provider2;
    }

    @DexIgnore
    public static SleepSessionsRepository_Factory create(Provider<UserRepository> provider, Provider<ApiServiceV2> provider2) {
        return new SleepSessionsRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static SleepSessionsRepository newInstance(UserRepository userRepository, ApiServiceV2 apiServiceV2) {
        return new SleepSessionsRepository(userRepository, apiServiceV2);
    }

    @DexIgnore
    @Override // javax.inject.Provider
    public SleepSessionsRepository get() {
        return newInstance(this.mUserRepositoryProvider.get(), this.mApiServiceProvider.get());
    }
}
