package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import androidx.lifecycle.LiveData;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Nw0;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.CustomizeRealData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeRealDataDao_Impl implements CustomizeRealDataDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Hh<CustomizeRealData> __insertionAdapterOfCustomizeRealData;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfCleanUp;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<CustomizeRealData> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, CustomizeRealData customizeRealData) {
            if (customizeRealData.getId() == null) {
                mi.bindNull(1);
            } else {
                mi.bindString(1, customizeRealData.getId());
            }
            if (customizeRealData.getValue() == null) {
                mi.bindNull(2);
            } else {
                mi.bindString(2, customizeRealData.getValue());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, CustomizeRealData customizeRealData) {
            bind(mi, customizeRealData);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `customizeRealData` (`id`,`value`) VALUES (?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Vh {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM customizeRealData";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<CustomizeRealData>> {
        @DexIgnore
        public /* final */ /* synthetic */ Rh val$_statement;

        @DexIgnore
        public Anon3(Rh rh) {
            this.val$_statement = rh;
        }

        @DexIgnore
        @Override // java.util.concurrent.Callable
        public List<CustomizeRealData> call() throws Exception {
            Cursor b = Ex0.b(CustomizeRealDataDao_Impl.this.__db, this.val$_statement, false, null);
            try {
                int c = Dx0.c(b, "id");
                int c2 = Dx0.c(b, "value");
                ArrayList arrayList = new ArrayList(b.getCount());
                while (b.moveToNext()) {
                    arrayList.add(new CustomizeRealData(b.getString(c), b.getString(c2)));
                }
                return arrayList;
            } finally {
                b.close();
            }
        }

        @DexIgnore
        @Override // java.lang.Object
        public void finalize() {
            this.val$_statement.m();
        }
    }

    @DexIgnore
    public CustomizeRealDataDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfCustomizeRealData = new Anon1(oh);
        this.__preparedStmtOfCleanUp = new Anon2(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public LiveData<List<CustomizeRealData>> getAllRealDataAsLiveData() {
        Rh f = Rh.f("SELECT * FROM customizeRealData", 0);
        Nw0 invalidationTracker = this.__db.getInvalidationTracker();
        Anon3 anon3 = new Anon3(f);
        return invalidationTracker.d(new String[]{"customizeRealData"}, false, anon3);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public List<CustomizeRealData> getAllRealDataRaw() {
        Rh f = Rh.f("SELECT * FROM customizeRealData", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "value");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                arrayList.add(new CustomizeRealData(b.getString(c), b.getString(c2)));
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public CustomizeRealData getRealData(String str) {
        CustomizeRealData customizeRealData = null;
        Rh f = Rh.f("SELECT * FROM customizeRealData WHERE id=?", 1);
        if (str == null) {
            f.bindNull(1);
        } else {
            f.bindString(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, "value");
            if (b.moveToFirst()) {
                customizeRealData = new CustomizeRealData(b.getString(c), b.getString(c2));
            }
            return customizeRealData;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.CustomizeRealDataDao
    public void upsertRealData(CustomizeRealData customizeRealData) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCustomizeRealData.insert((Hh<CustomizeRealData>) customizeRealData);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
