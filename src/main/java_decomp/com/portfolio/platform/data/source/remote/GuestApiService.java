package com.portfolio.platform.data.source.remote;

import com.fossil.Q88;
import com.fossil.Y98;
import com.mapped.Ku3;
import com.mapped.Ny6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.Firmware;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GuestApiService {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class DefaultImpls {
        @DexIgnore
        public static /* synthetic */ Object getFirmwares$default(GuestApiService guestApiService, String str, String str2, String str3, boolean z, Xe6 xe6, int i, Object obj) {
            if (obj == null) {
                return guestApiService.getFirmwares(str, str2, (i & 4) != 0 ? "android" : str3, (i & 8) != 0 ? true : z, xe6);
            }
            throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getFirmwares");
        }
    }

    @DexIgnore
    @Ny6("firmwares")
    Object getFirmwares(@Y98("appVersion") String str, @Y98("deviceModel") String str2, @Y98("os") String str3, @Y98("includesInactive") boolean z, Xe6<? super Q88<ApiResponse<Firmware>>> xe6);

    @DexIgnore
    @Ny6("assets/app-localizations")
    Object getLocalizations(@Y98("metadata.appVersion") String str, @Y98("metadata.platform") String str2, Xe6<? super Q88<ApiResponse<Ku3>>> xe6);
}
