package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import com.fossil.A05;
import com.fossil.Dx0;
import com.fossil.Ex0;
import com.fossil.Xz4;
import com.fossil.Yz4;
import com.fossil.Zz4;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import com.mapped.Gh;
import com.mapped.Hh;
import com.mapped.Mi;
import com.mapped.Oh;
import com.mapped.Rh;
import com.mapped.Vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitWorkoutSessionDao_Impl implements GFitWorkoutSessionDao {
    @DexIgnore
    public /* final */ Oh __db;
    @DexIgnore
    public /* final */ Gh<GFitWorkoutSession> __deletionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ Xz4 __gFitWOCaloriesConverter; // = new Xz4();
    @DexIgnore
    public /* final */ Yz4 __gFitWODistancesConverter; // = new Yz4();
    @DexIgnore
    public /* final */ Zz4 __gFitWOHeartRatesConverter; // = new Zz4();
    @DexIgnore
    public /* final */ A05 __gFitWOStepsConverter; // = new A05();
    @DexIgnore
    public /* final */ Hh<GFitWorkoutSession> __insertionAdapterOfGFitWorkoutSession;
    @DexIgnore
    public /* final */ Vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends Hh<GFitWorkoutSession> {
        @DexIgnore
        public Anon1(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitWorkoutSession gFitWorkoutSession) {
            mi.bindLong(1, (long) gFitWorkoutSession.getId());
            mi.bindLong(2, gFitWorkoutSession.getStartTime());
            mi.bindLong(3, gFitWorkoutSession.getEndTime());
            mi.bindLong(4, (long) gFitWorkoutSession.getWorkoutType());
            String b = GFitWorkoutSessionDao_Impl.this.__gFitWOStepsConverter.b(gFitWorkoutSession.getSteps());
            if (b == null) {
                mi.bindNull(5);
            } else {
                mi.bindString(5, b);
            }
            String b2 = GFitWorkoutSessionDao_Impl.this.__gFitWOCaloriesConverter.b(gFitWorkoutSession.getCalories());
            if (b2 == null) {
                mi.bindNull(6);
            } else {
                mi.bindString(6, b2);
            }
            String b3 = GFitWorkoutSessionDao_Impl.this.__gFitWODistancesConverter.b(gFitWorkoutSession.getDistances());
            if (b3 == null) {
                mi.bindNull(7);
            } else {
                mi.bindString(7, b3);
            }
            String b4 = GFitWorkoutSessionDao_Impl.this.__gFitWOHeartRatesConverter.b(gFitWorkoutSession.getHeartRates());
            if (b4 == null) {
                mi.bindNull(8);
            } else {
                mi.bindString(8, b4);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Hh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitWorkoutSession gFitWorkoutSession) {
            bind(mi, gFitWorkoutSession);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitWorkoutSession` (`id`,`startTime`,`endTime`,`workoutType`,`steps`,`calories`,`distances`,`heartRates`) VALUES (nullif(?, 0),?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends Gh<GFitWorkoutSession> {
        @DexIgnore
        public Anon2(Oh oh) {
            super(oh);
        }

        @DexIgnore
        public void bind(Mi mi, GFitWorkoutSession gFitWorkoutSession) {
            mi.bindLong(1, (long) gFitWorkoutSession.getId());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.mapped.Mi, java.lang.Object] */
        @Override // com.mapped.Gh
        public /* bridge */ /* synthetic */ void bind(Mi mi, GFitWorkoutSession gFitWorkoutSession) {
            bind(mi, gFitWorkoutSession);
        }

        @DexIgnore
        @Override // com.mapped.Vh, com.mapped.Gh
        public String createQuery() {
            return "DELETE FROM `gFitWorkoutSession` WHERE `id` = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends Vh {
        @DexIgnore
        public Anon3(Oh oh) {
            super(oh);
        }

        @DexIgnore
        @Override // com.mapped.Vh
        public String createQuery() {
            return "DELETE FROM gFitWorkoutSession";
        }
    }

    @DexIgnore
    public GFitWorkoutSessionDao_Impl(Oh oh) {
        this.__db = oh;
        this.__insertionAdapterOfGFitWorkoutSession = new Anon1(oh);
        this.__deletionAdapterOfGFitWorkoutSession = new Anon2(oh);
        this.__preparedStmtOfClearAll = new Anon3(oh);
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        Mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.executeUpdateDelete();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void deleteGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitWorkoutSession.handle(gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public List<GFitWorkoutSession> getAllGFitWorkoutSession() {
        Rh f = Rh.f("SELECT * FROM gFitWorkoutSession", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor b = Ex0.b(this.__db, f, false, null);
        try {
            int c = Dx0.c(b, "id");
            int c2 = Dx0.c(b, SampleRaw.COLUMN_START_TIME);
            int c3 = Dx0.c(b, SampleRaw.COLUMN_END_TIME);
            int c4 = Dx0.c(b, "workoutType");
            int c5 = Dx0.c(b, "steps");
            int c6 = Dx0.c(b, "calories");
            int c7 = Dx0.c(b, "distances");
            int c8 = Dx0.c(b, "heartRates");
            ArrayList arrayList = new ArrayList(b.getCount());
            while (b.moveToNext()) {
                GFitWorkoutSession gFitWorkoutSession = new GFitWorkoutSession(b.getLong(c2), b.getLong(c3), b.getInt(c4), this.__gFitWOStepsConverter.a(b.getString(c5)), this.__gFitWOCaloriesConverter.a(b.getString(c6)), this.__gFitWODistancesConverter.a(b.getString(c7)), this.__gFitWOHeartRatesConverter.a(b.getString(c8)));
                gFitWorkoutSession.setId(b.getInt(c));
                arrayList.add(gFitWorkoutSession);
            }
            return arrayList;
        } finally {
            b.close();
            f.m();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertGFitWorkoutSession(GFitWorkoutSession gFitWorkoutSession) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert((Hh<GFitWorkoutSession>) gFitWorkoutSession);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    @Override // com.portfolio.platform.data.source.local.thirdparty.GFitWorkoutSessionDao
    public void insertListGFitWorkoutSession(List<GFitWorkoutSession> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitWorkoutSession.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
