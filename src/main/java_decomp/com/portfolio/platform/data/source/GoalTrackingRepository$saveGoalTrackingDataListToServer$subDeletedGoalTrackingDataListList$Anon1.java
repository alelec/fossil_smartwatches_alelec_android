package com.portfolio.platform.data.source;

import com.fossil.Qq7;
import com.mapped.Hg6;
import com.mapped.Wg6;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1 extends Qq7 implements Hg6<GoalTrackingData, Boolean> {
    @DexIgnore
    public static /* final */ GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1 INSTANCE; // = new GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1();

    @DexIgnore
    public GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1() {
        super(1);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
    @Override // com.mapped.Hg6
    public /* bridge */ /* synthetic */ Boolean invoke(GoalTrackingData goalTrackingData) {
        return Boolean.valueOf(invoke(goalTrackingData));
    }

    @DexIgnore
    public final boolean invoke(GoalTrackingData goalTrackingData) {
        Wg6.c(goalTrackingData, "it");
        return goalTrackingData.getPinType() == 3;
    }
}
