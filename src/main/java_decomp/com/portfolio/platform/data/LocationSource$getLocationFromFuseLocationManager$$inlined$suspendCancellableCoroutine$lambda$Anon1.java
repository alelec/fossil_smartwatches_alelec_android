package com.portfolio.platform.data;

import android.content.Context;
import android.location.Location;
import com.fossil.By7;
import com.fossil.Dl7;
import com.fossil.El7;
import com.fossil.Ko7;
import com.fossil.Nt3;
import com.fossil.Yn7;
import com.google.android.gms.location.LocationRequest;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lc3;
import com.mapped.Lk6;
import com.mapped.Mc3;
import com.mapped.Wg6;
import com.mapped.Wv2;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Context $context$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ Lk6 $continuation;
    @DexIgnore
    public /* final */ /* synthetic */ Wv2 $fusedLocationClient;
    @DexIgnore
    public /* final */ /* synthetic */ Jh6 $location;
    @DexIgnore
    public /* final */ /* synthetic */ LocationRequest $locationRequest;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public Il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ LocationSource this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends Ko7 implements Coroutine<Il6, Xe6<? super Location>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (Il6) obj;
            throw null;
            //return anon1_Level2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Location> xe6) {
            throw null;
            //return ((Anon1_Level2) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = this.this$0;
                LocationSource locationSource = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.this$0;
                Wv2 wv2 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.$fusedLocationClient;
                Wg6.b(wv2, "fusedLocationClient");
                LocationRequest locationRequest = this.this$0.$locationRequest;
                this.L$0 = il6;
                this.label = 1;
                Object requestLocationUpdates = locationSource.requestLocationUpdates(wv2, locationRequest, this);
                return requestLocationUpdates == d ? d : requestLocationUpdates;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon2_Level2<TResult> implements Mc3<Location> {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon2_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        public final void onSuccess(Location location) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation success " + location);
            Lk6 lk6 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(location, LocationSource.ErrorState.SUCCESS, false, 4, null);
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(result));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Mc3
        public /* bridge */ /* synthetic */ void onSuccess(Location location) {
            onSuccess(location);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon3_Level2 implements Lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 this$0;

        @DexIgnore
        public Anon3_Level2(LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) {
            this.this$0 = locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            Wg6.c(exc, "it");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager retrieveLastLocation fail " + exc);
            Lk6 lk6 = this.this$0.$continuation;
            LocationSource.Result result = new LocationSource.Result(null, LocationSource.ErrorState.UNKNOWN, false, 4, null);
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(result));
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(Jh6 jh6, Wv2 wv2, LocationRequest locationRequest, Lk6 lk6, Xe6 xe6, LocationSource locationSource, Context context) {
        super(2, xe6);
        this.$location = jh6;
        this.$fusedLocationClient = wv2;
        this.$locationRequest = locationRequest;
        this.$continuation = lk6;
        this.this$0 = locationSource;
        this.$context$inlined = context;
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
        Wg6.c(xe6, "completion");
        LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1 = new LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1(this.$location, this.$fusedLocationClient, this.$locationRequest, this.$continuation, xe6, this.this$0, this.$context$inlined);
        locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1.p$ = (Il6) obj;
        throw null;
        //return locationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
    @Override // com.mapped.Coroutine
    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
        throw null;
        //return ((LocationSource$getLocationFromFuseLocationManager$$inlined$suspendCancellableCoroutine$lambda$Anon1) create(il6, xe6)).invokeSuspend(Cd6.a);
    }

    @DexIgnore
    @Override // com.fossil.Zn7
    public final Object invokeSuspend(Object obj) {
        Object c;
        Jh6 jh6;
        Object d = Yn7.d();
        int i = this.label;
        if (i == 0) {
            El7.b(obj);
            Il6 il6 = this.p$;
            Jh6 jh62 = this.$location;
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, null);
            this.L$0 = il6;
            this.L$1 = jh62;
            this.label = 1;
            c = By7.c(5000, anon1_Level2, this);
            if (c == d) {
                return d;
            }
            jh6 = jh62;
        } else if (i == 1) {
            Il6 il62 = (Il6) this.L$0;
            El7.b(obj);
            jh6 = (Jh6) this.L$1;
            c = obj;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        jh6.element = (T) ((Location) c);
        if (this.$location.element == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = LocationSource.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getLocationFromFuseLocationManager timeout, try to get lastLocation=" + ((Object) this.$location.element));
            Wv2 wv2 = this.$fusedLocationClient;
            Wg6.b(wv2, "fusedLocationClient");
            Nt3<Location> s = wv2.s();
            s.f(new Anon2_Level2(this));
            s.d(new Anon3_Level2(this));
            Wg6.b(s, "fusedLocationClient.last\u2026                        }");
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease2 = LocationSource.Companion.getTAG$app_fossilRelease();
            local2.d(tAG$app_fossilRelease2, "getLocationFromFuseLocationManager success lastLocation=" + ((Object) this.$location.element));
            Lk6 lk6 = this.$continuation;
            LocationSource.Result result = new LocationSource.Result(this.$location.element, LocationSource.ErrorState.SUCCESS, false, 4, null);
            Dl7.Ai ai = Dl7.Companion;
            lk6.resumeWith(Dl7.constructor-impl(result));
        }
        return Cd6.a;
    }
}
