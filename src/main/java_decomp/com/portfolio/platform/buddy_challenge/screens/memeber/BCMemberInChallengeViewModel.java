package com.portfolio.platform.buddy_challenge.screens.memeber;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.At4;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Hm7;
import com.fossil.Jl5;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ms4;
import com.fossil.Py4;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Xs4;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.buddy_challenge.domain.FriendRepository;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCMemberInChallengeViewModel extends Ts0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Tt4 e;
    @DexIgnore
    public /* final */ FriendRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel", f = "BCMemberInChallengeViewModel.kt", l = {82, 82}, m = "loadInvitedPlayers")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCMemberInChallengeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCMemberInChallengeViewModel bCMemberInChallengeViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCMemberInChallengeViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.i(null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Rl6<? extends Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCMemberInChallengeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$joined$1", f = "BCMemberInChallengeViewModel.kt", l = {91}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Tt4 tt4 = this.this$0.this$0.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = il6;
                            this.label = 1;
                            Object t = tt4.t(str, new String[]{"waiting", "running", "completed", "left_after_start"}, this);
                            return t == d ? d : t;
                        }
                        Wg6.i();
                        throw null;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadInvitedPlayers$2$1$pending$1", f = "BCMemberInChallengeViewModel.kt", l = {86}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Ms4>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Ms4>>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object t;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Bi bi = this.this$0.this$0;
                        if (!bi.$isWaiting) {
                            return new Kz4(Hm7.e(), null, 2, null);
                        }
                        Tt4 tt4 = bi.this$0.e;
                        String str = this.this$0.this$0.$challengeId;
                        if (str != null) {
                            this.L$0 = il6;
                            this.label = 1;
                            t = tt4.t(str, new String[]{"invited", "invitation_expired"}, this);
                            if (t == d) {
                                return d;
                            }
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        t = obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return (Kz4) t;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    return Hl7.a(Gu7.b(il6, null, null, new Biii(this, null), 3, null), Gu7.b(il6, null, null, new Aiii(this, null), 3, null));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCMemberInChallengeViewModel bCMemberInChallengeViewModel, boolean z, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCMemberInChallengeViewModel;
            this.$isWaiting = z;
            this.$challengeId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$isWaiting, this.$challengeId, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Rl6<? extends Lc6<? extends Rl6<? extends Kz4<List<? extends Ms4>>>, ? extends Rl6<? extends Kz4<List<? extends Ms4>>>>>> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Gu7.b(this.p$, Bw7.b(), null, new Aii(this, null), 2, null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1", f = "BCMemberInChallengeViewModel.kt", l = {38, 39, 40, 42, 45}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $challengeId;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isWaiting;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCMemberInChallengeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $any;
            @DexIgnore
            public /* final */ /* synthetic */ List $friends;
            @DexIgnore
            public /* final */ /* synthetic */ List $joined;
            @DexIgnore
            public /* final */ /* synthetic */ List $pending;
            @DexIgnore
            public /* final */ /* synthetic */ Kz4 $resultJoined;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, List list2, List list3, List list4, Kz4 kz4, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$joined = list;
                this.$friends = list2;
                this.$pending = list3;
                this.$any = list4;
                this.$resultJoined = kz4;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$joined, this.$friends, this.$pending, this.$any, this.$resultJoined, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List list = this.$joined;
                    if (list != null) {
                        Lc6<List<At4>, List<At4>> j = Py4.j(this.$friends, list);
                        List<At4> first = j.getFirst();
                        List<At4> second = j.getSecond();
                        List list2 = this.$pending;
                        List<At4> h = list2 != null ? Py4.h(list2) : null;
                        if (!first.isEmpty()) {
                            this.$any.add(Jl5.b.p(first.size()));
                            this.$any.addAll(first);
                        }
                        if (!second.isEmpty()) {
                            this.$any.add(Jl5.b.q(second.size()));
                            this.$any.addAll(second);
                        }
                        if (h != null && (!h.isEmpty())) {
                            this.$any.add(Jl5.b.r(h.size()));
                            this.$any.addAll(h);
                        }
                        this.this$0.this$0.c.l(this.$any);
                    } else if (this.this$0.this$0.c.e() == null) {
                        this.this$0.this$0.d.l(Hl7.a(Ao7.a(true), this.$resultJoined.a()));
                    } else {
                        this.this$0.this$0.d.l(Hl7.a(Ao7.a(false), this.$resultJoined.a()));
                    }
                    this.this$0.this$0.b.l(Ao7.a(false));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$loadMembers$1$friends$1", f = "BCMemberInChallengeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Xs4>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Xs4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.f.l();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCMemberInChallengeViewModel bCMemberInChallengeViewModel, String str, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCMemberInChallengeViewModel;
            this.$challengeId = str;
            this.$isWaiting = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$challengeId, this.$isWaiting, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r0v21, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r1v13, types: [java.util.List] */
        /* JADX WARN: Type inference failed for: r2v16, types: [java.util.List] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0090  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00ce  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00fd  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0125  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x015a  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0161  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0167  */
        /* JADX WARNING: Unknown variable types count: 3 */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r15) {
            /*
            // Method dump skipped, instructions count: 366
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public BCMemberInChallengeViewModel(Tt4 tt4, FriendRepository friendRepository) {
        Wg6.c(tt4, "challengeRepository");
        Wg6.c(friendRepository, "friendRepository");
        this.e = tt4;
        this.f = friendRepository;
        String simpleName = BCMemberInChallengeViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCMemberInChallengeViewM\u2026el::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> f() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Boolean> g() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> h() {
        LiveData<List<Object>> a2 = Ss0.a(this.c);
        Wg6.b(a2, "Transformations.distinctUntilChanged(this)");
        return a2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object i(java.lang.String r8, boolean r9, com.mapped.Xe6<? super com.mapped.Lc6<? extends com.mapped.Rl6<com.fossil.Kz4<java.util.List<com.fossil.Ms4>>>, ? extends com.mapped.Rl6<com.fossil.Kz4<java.util.List<com.fossil.Ms4>>>>> r10) {
        /*
            r7 = this;
            r6 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel.Ai
            if (r0 == 0) goto L_0x0031
            r0 = r10
            com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$Ai r0 = (com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0031
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0015:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x0061
            if (r0 == r5) goto L_0x0040
            if (r0 != r6) goto L_0x0038
            boolean r0 = r3.Z$0
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r3.L$0
            com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel) r0
            com.fossil.El7.b(r2)
        L_0x0030:
            return r2
        L_0x0031:
            com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$Ai r0 = new com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$Ai
            r0.<init>(r7, r10)
            r3 = r0
            goto L_0x0015
        L_0x0038:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0040:
            boolean r9 = r3.Z$0
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel r1 = (com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel) r1
            com.fossil.El7.b(r2)
            r8 = r0
        L_0x004e:
            r0 = r2
            com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
            r3.L$0 = r1
            r3.L$1 = r8
            r3.Z$0 = r9
            r3.label = r6
            java.lang.Object r2 = r0.l(r3)
            if (r2 != r4) goto L_0x0030
            r2 = r4
            goto L_0x0030
        L_0x0061:
            com.fossil.El7.b(r2)
            com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$Bi r0 = new com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel$Bi
            r1 = 0
            r0.<init>(r7, r9, r8, r1)
            r3.L$0 = r7
            r3.L$1 = r8
            r3.Z$0 = r9
            r3.label = r5
            java.lang.Object r2 = com.fossil.Ux7.c(r0, r3)
            if (r2 != r4) goto L_0x007a
            r2 = r4
            goto L_0x0030
        L_0x007a:
            r1 = r7
            goto L_0x004e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.memeber.BCMemberInChallengeViewModel.i(java.lang.String, boolean, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void j(String str, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.e(str2, "loadMembers - challengeId: " + str + " - isWaiting: " + z);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, str, z, null), 3, null);
    }
}
