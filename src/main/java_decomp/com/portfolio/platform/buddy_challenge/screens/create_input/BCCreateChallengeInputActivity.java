package com.portfolio.platform.buddy_challenge.screens.create_input;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.fossil.Ku4;
import com.fossil.Ts4;
import com.fossil.Vs4;
import com.mapped.Cd6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateChallengeInputActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, Ts4 ts4) {
            Wg6.c(fragment, "fragment");
            Wg6.c(ts4, "draft");
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_draft_extra", ts4);
            fragment.startActivityForResult(intent, 14);
        }

        @DexIgnore
        public final void b(Fragment fragment, Vs4 vs4) {
            Wg6.c(fragment, "fragment");
            Wg6.c(vs4, MessengerShareContentUtility.ATTACHMENT_TEMPLATE_TYPE);
            Intent intent = new Intent(fragment.getContext(), BCCreateChallengeInputActivity.class);
            intent.putExtra("challenge_template_extra", vs4);
            fragment.startActivityForResult(intent, 11);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Ku4) getSupportFragmentManager().Y(2131362158)) == null) {
            Ku4 b = Ku4.t.b((Vs4) getIntent().getParcelableExtra("challenge_template_extra"), (Ts4) getIntent().getParcelableExtra("challenge_draft_extra"));
            if (b != null) {
                k(b, Ku4.t.a(), 2131362158);
                Cd6 cd6 = Cd6.a;
                return;
            }
            Wg6.i();
            throw null;
        }
    }
}
