package com.portfolio.platform.buddy_challenge.screens.create_profile;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.It4;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateSocialProfileViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Pattern j; // = Pattern.compile("^[a-z0-9.]+$");
    @DexIgnore
    public MutableLiveData<Bi> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Ai> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> d; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ProfileRepository f;
    @DexIgnore
    public /* final */ FCMRepository g;
    @DexIgnore
    public /* final */ An4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;

        @DexIgnore
        public Ai(int i, String str) {
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (this.a != ai.a || !Wg6.a(this.b, ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.a;
            String str = this.b;
            return (str != null ? str.hashCode() : 0) + (i * 31);
        }

        @DexIgnore
        public String toString() {
            return "Error(errorCode=" + this.a + ", errorMessage=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Bi(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!(this.a == bi.a && this.b == bi.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (i2 * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "IdValidation(isCombineMet=" + this.a + ", isLengthMet=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1", f = "BCCreateSocialProfileViewModel.kt", l = {57, 65}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $socialId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCCreateSocialProfileViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$1", f = "BCCreateSocialProfileViewModel.kt", l = {66, 71}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0072  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 2
                    r6 = 1
                    java.lang.Object r3 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x0074
                    if (r0 == r6) goto L_0x0024
                    if (r0 != r7) goto L_0x001c
                    java.lang.Object r0 = r8.L$1
                    com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                L_0x0019:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001b:
                    return r0
                L_0x001c:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0024:
                    java.lang.Object r0 = r8.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r9)
                    r2 = r0
                    r1 = r9
                L_0x002d:
                    r0 = r1
                    com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                    com.portfolio.platform.data.model.ServerError r1 = r0.a()
                    if (r1 == 0) goto L_0x0019
                    com.portfolio.platform.data.model.ServerError r1 = r0.a()
                    java.lang.Integer r1 = r1.getCode()
                    if (r1 == 0) goto L_0x0019
                    int r1 = r1.intValue()
                    if (r1 != 0) goto L_0x0019
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                    java.lang.String r4 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.e()
                    java.lang.String r5 = "reset device Id"
                    r1.e(r4, r5)
                    com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci r1 = r8.this$0
                    com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r1.this$0
                    com.mapped.An4 r1 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.c(r1)
                    r1.Y1(r6)
                    com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                    com.portfolio.platform.PortfolioApp r1 = r1.instance()
                    r8.L$0 = r2
                    r8.L$1 = r0
                    r8.label = r7
                    java.lang.Object r0 = r1.A(r8)
                    if (r0 != r3) goto L_0x0019
                    r0 = r3
                    goto L_0x001b
                L_0x0074:
                    com.fossil.El7.b(r9)
                    com.mapped.Il6 r0 = r8.p$
                    com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci r1 = r8.this$0
                    com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r1.this$0
                    com.portfolio.platform.buddy_challenge.domain.FCMRepository r1 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.b(r1)
                    r8.L$0 = r0
                    r8.label = r6
                    java.lang.Object r1 = r1.d(r8)
                    if (r1 != r3) goto L_0x008d
                    r0 = r3
                    goto L_0x001b
                L_0x008d:
                    r2 = r0
                    goto L_0x002d
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$createSocialProfile$1$result$1", f = "BCCreateSocialProfileViewModel.kt", l = {58}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<It4>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Kz4<It4>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ProfileRepository profileRepository = this.this$0.this$0.f;
                    String str = this.this$0.$socialId;
                    this.L$0 = il6;
                    this.label = 1;
                    Object a2 = profileRepository.a(str, this);
                    return a2 == d ? d : a2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCCreateSocialProfileViewModel;
            this.$socialId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$socialId, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0090  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 0
                r6 = 2
                r5 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0069
                if (r0 == r5) goto L_0x0025
                if (r0 != r6) goto L_0x001d
                java.lang.Object r0 = r8.L$1
                com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x001a:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0025:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
                r2 = r0
                r1 = r9
            L_0x002e:
                r0 = r1
                com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.f(r1)
                r4 = 0
                java.lang.Boolean r4 = com.fossil.Ao7.a(r4)
                r1.l(r4)
                java.lang.Object r1 = r0.c()
                if (r1 == 0) goto L_0x0090
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.g(r1)
                java.lang.Boolean r4 = com.fossil.Ao7.a(r5)
                r1.l(r4)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci$Aii r4 = new com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci$Aii
                r4.<init>(r8, r7)
                r8.L$0 = r2
                r8.L$1 = r0
                r8.label = r6
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r4, r8)
                if (r0 != r3) goto L_0x001a
                r0 = r3
                goto L_0x001c
            L_0x0069:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r8.this$0
                androidx.lifecycle.MutableLiveData r1 = com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.f(r1)
                java.lang.Boolean r2 = com.fossil.Ao7.a(r5)
                r1.l(r2)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci$Bii r2 = new com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel$Ci$Bii
                r2.<init>(r8, r7)
                r8.L$0 = r0
                r8.label = r5
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                if (r1 != r3) goto L_0x00b6
                r0 = r3
                goto L_0x001c
            L_0x0090:
                com.portfolio.platform.data.model.ServerError r1 = r0.a()
                if (r1 == 0) goto L_0x001a
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel r1 = r8.this$0
                com.portfolio.platform.data.model.ServerError r2 = r0.a()
                java.lang.Integer r2 = r2.getCode()
                java.lang.String r3 = "result.error.code"
                com.mapped.Wg6.b(r2, r3)
                int r2 = r2.intValue()
                com.portfolio.platform.data.model.ServerError r0 = r0.a()
                java.lang.String r0 = r0.getMessage()
                com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.a(r1, r2, r0)
                goto L_0x001a
            L_0x00b6:
                r2 = r0
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.create_profile.BCCreateSocialProfileViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String name = BCCreateSocialProfileViewModel.class.getName();
        Wg6.b(name, "BCCreateSocialProfileViewModel::class.java.name");
        i = name;
    }
    */

    @DexIgnore
    public BCCreateSocialProfileViewModel(ProfileRepository profileRepository, FCMRepository fCMRepository, An4 an4) {
        Wg6.c(profileRepository, "socialProfileRepository");
        Wg6.c(fCMRepository, "fcmRepository");
        Wg6.c(an4, "sharePrefs");
        this.f = profileRepository;
        this.g = fCMRepository;
        this.h = an4;
    }

    @DexIgnore
    public static /* synthetic */ void k(BCCreateSocialProfileViewModel bCCreateSocialProfileViewModel, boolean z, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            z = false;
        }
        if ((i2 & 2) != 0) {
            z2 = false;
        }
        bCCreateSocialProfileViewModel.j(z, z2);
    }

    @DexIgnore
    public final void h(String str) {
        Wg6.c(str, "socialId");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, str, null), 3, null);
    }

    @DexIgnore
    public final void i(int i2, String str) {
        this.b.l(new Ai(i2, str));
    }

    @DexIgnore
    public final void j(boolean z, boolean z2) {
        this.a.l(new Bi(z, z2));
    }

    @DexIgnore
    public final LiveData<Boolean> l() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Ai> m() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<Boolean> n() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Boolean> o() {
        return this.e;
    }

    @DexIgnore
    public final LiveData<Bi> p() {
        return this.a;
    }

    @DexIgnore
    public final void q(String str) {
        boolean z = true;
        Wg6.c(str, "socialId");
        if (str.length() > 0) {
            int length = str.length();
            boolean z2 = 6 <= length && 24 >= length;
            boolean matches = j.matcher(str).matches();
            j(matches, z2);
            MutableLiveData<Boolean> mutableLiveData = this.d;
            if (!matches || !z2) {
                z = false;
            }
            mutableLiveData.l(Boolean.valueOf(z));
            return;
        }
        k(this, false, false, 3, null);
        this.d.l(Boolean.FALSE);
    }
}
