package com.portfolio.platform.buddy_challenge.screens.create_intro;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Pu4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCCreateChallengeIntroActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            Wg6.c(fragment, "fragment");
            fragment.startActivityForResult(new Intent(fragment.getContext(), BCCreateChallengeIntroActivity.class), 11);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Pu4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(Pu4.m.b(), Pu4.m.a(), 2131362158);
        }
    }
}
