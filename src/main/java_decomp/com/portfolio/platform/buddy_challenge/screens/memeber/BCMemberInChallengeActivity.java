package com.portfolio.platform.buddy_challenge.screens.memeber;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Sv4;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCMemberInChallengeActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a A; // = new a(null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, boolean z) {
            Wg6.c(fragment, "fragment");
            Wg6.c(str, "challengeId");
            Intent intent = new Intent(fragment.getContext(), BCMemberInChallengeActivity.class);
            intent.putExtra("challenge_id_extra", str);
            intent.putExtra("challenge_status_extra", z);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558428);
        if (((Sv4) getSupportFragmentManager().Y(2131362158)) == null) {
            k(Sv4.t.b(getIntent().getStringExtra("challenge_id_extra"), getIntent().getBooleanExtra("challenge_status_extra", true)), Sv4.t.a(), 2131362158);
        }
    }
}
