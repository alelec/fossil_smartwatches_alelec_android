package com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.Cz4;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Hl7;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Mt4;
import com.fossil.Py4;
import com.fossil.Ts0;
import com.fossil.Tt4;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.ServerError;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCRecommendationViewModel extends Ts0 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> b; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Mt4, Integer>> d; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<Object>> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Lc6<Boolean, ServerError>> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Integer> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Tt4 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$initLoadRecommendedChallenge$1", f = "BCRecommendationViewModel.kt", l = {51}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCRecommendationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(BCRecommendationViewModel bCRecommendationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCRecommendationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.b.l(Ao7.a(true));
                BCRecommendationViewModel bCRecommendationViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCRecommendationViewModel.j(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel", f = "BCRecommendationViewModel.kt", l = {69, 86}, m = "loadRecommended")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCRecommendationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCRecommendationViewModel bCRecommendationViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCRecommendationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.j(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$data$1", f = "BCRecommendationViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Object>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $recommended;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(List list, Xe6 xe6) {
            super(2, xe6);
            this.$recommended = list;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.$recommended, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super List<? extends Object>> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Py4.k(this.$recommended);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$loadRecommended$result$1", f = "BCRecommendationViewModel.kt", l = {69}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<List<? extends Mt4>>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCRecommendationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCRecommendationViewModel bCRecommendationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCRecommendationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<List<? extends Mt4>>> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Tt4 tt4 = this.this$0.h;
                this.L$0 = il6;
                this.label = 1;
                Object u = tt4.u(this);
                return u == d ? d : u;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel$refresh$1", f = "BCRecommendationViewModel.kt", l = {58}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCRecommendationViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCRecommendationViewModel bCRecommendationViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCRecommendationViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                BCRecommendationViewModel bCRecommendationViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCRecommendationViewModel.j(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.b.l(Ao7.a(false));
            return Cd6.a;
        }
    }

    @DexIgnore
    public BCRecommendationViewModel(Tt4 tt4) {
        Wg6.c(tt4, "challengeRepository");
        this.h = tt4;
        String simpleName = BCRecommendationViewModel.class.getSimpleName();
        Wg6.b(simpleName, "BCRecommendationViewModel::class.java.simpleName");
        this.a = simpleName;
    }

    @DexIgnore
    public final LiveData<Boolean> c() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Lc6<Boolean, ServerError>> d() {
        return this.f;
    }

    @DexIgnore
    public final LiveData<Boolean> e() {
        return this.b;
    }

    @DexIgnore
    public final LiveData<List<Object>> f() {
        return Cz4.a(this.e);
    }

    @DexIgnore
    public final LiveData<Lc6<Mt4, Integer>> g() {
        return this.d;
    }

    @DexIgnore
    public final LiveData<Integer> h() {
        return this.g;
    }

    @DexIgnore
    public final void i() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006f  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00b8  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00fe  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object j(com.mapped.Xe6<? super com.mapped.Cd6> r9) {
        /*
        // Method dump skipped, instructions count: 283
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.tab.challenge.recommendation.BCRecommendationViewModel.j(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void k() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }

    @DexIgnore
    public final void l(Mt4 mt4, int i) {
        Wg6.c(mt4, "recommended");
        this.d.l(Hl7.a(mt4, Integer.valueOf(i)));
    }

    @DexIgnore
    public final void m(int i) {
        this.g.l(Integer.valueOf(i));
    }
}
