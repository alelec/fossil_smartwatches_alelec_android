package com.portfolio.platform.buddy_challenge.screens.main;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.It4;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BCMainViewModel extends Ts0 {
    @DexIgnore
    public MutableLiveData<Boolean> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Ai> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Integer> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ProfileRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;

        @DexIgnore
        public Ai(boolean z, boolean z2) {
            this.a = z;
            this.b = z2;
        }

        @DexIgnore
        public final boolean a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!(this.a == ai.a && this.b == ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 1;
            boolean z = this.a;
            if (z) {
                z = true;
            }
            boolean z2 = this.b;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            return (i2 * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "Tab(shouldGoToCreatedPage=" + this.a + ", shouldGoToTabs=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel", f = "BCMainViewModel.kt", l = {54}, m = "checkOnServer")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ BCMainViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(BCMainViewModel bCMainViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = bCMainViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.c(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkOnServer$result$1", f = "BCMainViewModel.kt", l = {54}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<It4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCMainViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(BCMainViewModel bCMainViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCMainViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<It4>> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ProfileRepository profileRepository = this.this$0.d;
                this.L$0 = il6;
                this.label = 1;
                Object c = profileRepository.c(this);
                return c == d ? d : c;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1", f = "BCMainViewModel.kt", l = {36, 41}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCMainViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$checkSocialProfile$1$socialProfile$1", f = "BCMainViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super It4>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super It4> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.d.d();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(BCMainViewModel bCMainViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCMainViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0033  */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0054  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x003a
                if (r0 == r4) goto L_0x0025
                if (r0 != r5) goto L_0x001d
                java.lang.Object r0 = r7.L$1
                com.fossil.It4 r0 = (com.fossil.It4) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x001a:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001c:
                return r0
            L_0x001d:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0025:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x002e:
                r0 = r1
                com.fossil.It4 r0 = (com.fossil.It4) r0
                if (r0 == 0) goto L_0x0054
                com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel r0 = r7.this$0
                r1 = 0
                com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel.f(r0, r4, r1, r5, r6)
                goto L_0x001a
            L_0x003a:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Di$Aii r2 = new com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Di$Aii
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0064
                r0 = r3
                goto L_0x001c
            L_0x0054:
                com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel r1 = r7.this$0
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = r1.c(r7)
                if (r0 != r3) goto L_0x001a
                r0 = r3
                goto L_0x001c
            L_0x0064:
                r2 = r0
                goto L_0x002e
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$retry$1", f = "BCMainViewModel.kt", l = {49}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ BCMainViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(BCMainViewModel bCMainViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = bCMainViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                this.this$0.a.l(Ao7.a(true));
                BCMainViewModel bCMainViewModel = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (bCMainViewModel.c(this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        Wg6.b(BCMainViewModel.class.getName(), "BCMainViewModel::class.java.name");
    }
    */

    @DexIgnore
    public BCMainViewModel(ProfileRepository profileRepository) {
        Wg6.c(profileRepository, "socialProfileRepository");
        this.d = profileRepository;
    }

    @DexIgnore
    public static /* synthetic */ void f(BCMainViewModel bCMainViewModel, boolean z, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        if ((i & 2) != 0) {
            z2 = false;
        }
        bCMainViewModel.e(z, z2);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0032  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x005f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object c(com.mapped.Xe6<? super com.mapped.Cd6> r8) {
        /*
            r7 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r6 = 0
            r2 = 0
            r5 = 1
            boolean r0 = r8 instanceof com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel.Bi
            if (r0 == 0) goto L_0x0039
            r0 = r8
            com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Bi r0 = (com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel.Bi) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0039
            int r1 = r1 + r4
            r0.label = r1
        L_0x0015:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0047
            if (r4 != r5) goto L_0x003f
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel r0 = (com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel) r0
            com.fossil.El7.b(r1)
            r7 = r0
        L_0x0029:
            r0 = r1
            com.fossil.Kz4 r0 = (com.fossil.Kz4) r0
            java.lang.Object r1 = r0.c()
            if (r1 == 0) goto L_0x005f
            r0 = 2
            f(r7, r5, r6, r0, r2)
        L_0x0036:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0038:
            return r0
        L_0x0039:
            com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Bi r0 = new com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Bi
            r0.<init>(r7, r8)
            goto L_0x0015
        L_0x003f:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0047:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Ci r4 = new com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel$Ci
            r4.<init>(r7, r2)
            r0.L$0 = r7
            r0.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0029
            r0 = r3
            goto L_0x0038
        L_0x005f:
            com.portfolio.platform.data.model.ServerError r1 = r0.a()
            if (r1 == 0) goto L_0x0084
            java.lang.Integer r1 = r1.getCode()
        L_0x0069:
            if (r1 != 0) goto L_0x0086
        L_0x006b:
            androidx.lifecycle.MutableLiveData<java.lang.Boolean> r1 = r7.a
            java.lang.Boolean r3 = com.fossil.Ao7.a(r6)
            r1.l(r3)
            androidx.lifecycle.MutableLiveData<java.lang.Integer> r1 = r7.c
            com.portfolio.platform.data.model.ServerError r0 = r0.a()
            if (r0 == 0) goto L_0x0092
            java.lang.Integer r0 = r0.getCode()
        L_0x0080:
            r1.l(r0)
            goto L_0x0036
        L_0x0084:
            r1 = r2
            goto L_0x0069
        L_0x0086:
            r3 = 404(0x194, float:5.66E-43)
            int r1 = r1.intValue()
            if (r3 != r1) goto L_0x006b
            f(r7, r6, r5, r5, r2)
            goto L_0x0036
        L_0x0092:
            r0 = r2
            goto L_0x0080
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.buddy_challenge.screens.main.BCMainViewModel.c(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void d() {
        this.a.l(Boolean.TRUE);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    public final void e(boolean z, boolean z2) {
        this.b.l(new Ai(z2, z));
    }

    @DexIgnore
    public final LiveData<Boolean> g() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Integer> h() {
        return this.c;
    }

    @DexIgnore
    public final LiveData<Ai> i() {
        return this.b;
    }

    @DexIgnore
    public final void j() {
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, null), 3, null);
    }
}
