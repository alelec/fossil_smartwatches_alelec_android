package com.portfolio.platform.gson;

import com.fossil.Jj5;
import com.google.gson.JsonElement;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDeserializer implements Hu3<HybridPreset> {
    @DexIgnore
    public HybridPreset a(JsonElement jsonElement, Type type, Gu3 gu3) {
        String str;
        String str2;
        String str3;
        String w0;
        String str4;
        if (jsonElement != null) {
            Ku3 d = jsonElement.d();
            JsonElement p = d.p("name");
            Wg6.b(p, "jsonObject.get(Constants.JSON_KEY_NAME)");
            String f = p.f();
            JsonElement p2 = d.p("id");
            Wg6.b(p2, "jsonObject.get(\"id\")");
            String f2 = p2.f();
            if (d.s("serialNumber")) {
                JsonElement p3 = d.p("serialNumber");
                Wg6.b(p3, "jsonObject.get(Constants.JSON_KEY_SERIAL_NUMBER)");
                str = p3.f();
            } else {
                str = "";
            }
            JsonElement p4 = d.p("isActive");
            Wg6.b(p4, "jsonObject.get(Constants\u2026SON_KEY_IS_PRESET_ACTIVE)");
            boolean a2 = p4.a();
            JsonElement p5 = d.p("updatedAt");
            Wg6.b(p5, "jsonObject.get(Constants.JSON_KEY_UPDATED_AT)");
            String f3 = p5.f();
            JsonElement p6 = d.p("createdAt");
            Wg6.b(p6, "jsonObject.get(Constants.JSON_KEY_CREATED_AT)");
            String f4 = p6.f();
            ArrayList arrayList = new ArrayList();
            if (gu3 != null) {
                Iterator<JsonElement> it = d.q("buttons").iterator();
                while (it.hasNext()) {
                    JsonElement next = it.next();
                    Wg6.b(next, "item");
                    Ku3 d2 = next.d();
                    if (d2.s("buttonPosition")) {
                        JsonElement p7 = d2.p("buttonPosition");
                        Wg6.b(p7, "itemJsonObject.get(Constants.JSON_KEY_BUTTON_POS)");
                        str2 = p7.f();
                    } else {
                        str2 = "";
                    }
                    if (d2.s("appId")) {
                        JsonElement p8 = d2.p("appId");
                        Wg6.b(p8, "itemJsonObject.get(Constants.JSON_KEY_APP_ID)");
                        str3 = p8.f();
                    } else {
                        str3 = "";
                    }
                    if (d2.s("localUpdatedAt")) {
                        JsonElement p9 = d2.p("localUpdatedAt");
                        Wg6.b(p9, "itemJsonObject.get(Const\u2026SON_KEY_LOCAL_UPDATED_AT)");
                        w0 = p9.f();
                    } else {
                        Calendar instance = Calendar.getInstance();
                        Wg6.b(instance, "Calendar.getInstance()");
                        w0 = TimeUtils.w0(instance.getTime());
                    }
                    if (d2.s(Constants.USER_SETTING)) {
                        try {
                            JsonElement p10 = d2.p(Constants.USER_SETTING);
                            Wg6.b(p10, "itemJsonObject.get(Constants.JSON_KEY_SETTINGS)");
                            str4 = Jj5.a(p10.d());
                        } catch (Exception e) {
                            FLogger.INSTANCE.getLocal().d("HybridPresetDeserializer", "Exception when parse json string");
                        }
                        Wg6.b(str2, "position");
                        Wg6.b(str3, "appId");
                        Wg6.b(w0, "localUpdatedAt");
                        arrayList.add(new HybridPresetAppSetting(str2, str3, w0, str4));
                    }
                    str4 = "";
                    Wg6.b(str2, "position");
                    Wg6.b(str3, "appId");
                    Wg6.b(w0, "localUpdatedAt");
                    arrayList.add(new HybridPresetAppSetting(str2, str3, w0, str4));
                }
            }
            Wg6.b(f2, "id");
            Wg6.b(str, "serialNumber");
            HybridPreset hybridPreset = new HybridPreset(f2, f, str, arrayList, a2);
            hybridPreset.setCreatedAt(f4);
            hybridPreset.setUpdatedAt(f3);
            return hybridPreset;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ HybridPreset deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
