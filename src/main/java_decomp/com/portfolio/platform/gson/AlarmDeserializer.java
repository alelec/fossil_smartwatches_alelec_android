package com.portfolio.platform.gson;

import com.fossil.Hm7;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.sleep.MFSleepGoal;
import com.google.gson.JsonElement;
import com.mapped.Fu3;
import com.mapped.Gu3;
import com.mapped.Hu3;
import com.mapped.Ku3;
import com.mapped.Wg6;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDeserializer implements Hu3<Alarm> {
    @DexIgnore
    public Alarm a(JsonElement jsonElement, Type type, Gu3 gu3) {
        String str;
        String str2;
        String str3;
        String str4;
        int[] iArr;
        Ku3 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        JsonElement p = d.p("id");
        String f = p != null ? p.f() : null;
        if (f == null) {
            return null;
        }
        JsonElement p2 = d.p("title");
        if (p2 == null || (str = p2.f()) == null) {
            str = "";
        }
        JsonElement p3 = d.p("message");
        if (p3 == null || (str2 = p3.f()) == null) {
            str2 = "";
        }
        JsonElement p4 = d.p(MFSleepGoal.COLUMN_MINUTE);
        int i = 0;
        int b = p4 != null ? p4.b() : 0;
        JsonElement p5 = d.p(AppFilter.COLUMN_HOUR);
        int b2 = p5 != null ? p5.b() : 0;
        JsonElement p6 = d.p("isActive");
        boolean a2 = p6 != null ? p6.a() : false;
        JsonElement p7 = d.p("isRepeated");
        boolean a3 = p7 != null ? p7.a() : false;
        JsonElement p8 = d.p("createdAt");
        if (p8 == null || (str3 = p8.f()) == null) {
            str3 = "";
        }
        JsonElement p9 = d.p("updatedAt");
        if (p9 == null || (str4 = p9.f()) == null) {
            str4 = "";
        }
        Fu3 q = d.q(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
        if (q != null) {
            iArr = new int[q.size()];
            for (Object obj : q) {
                if (i >= 0) {
                    JsonElement jsonElement2 = (JsonElement) obj;
                    if (jsonElement2 != null) {
                        AlarmHelper.Ai ai = AlarmHelper.d;
                        String f2 = jsonElement2.f();
                        Wg6.b(f2, "it.asString");
                        iArr[i] = ai.b(f2);
                    }
                    i++;
                } else {
                    Hm7.l();
                    throw null;
                }
            }
        } else {
            iArr = null;
        }
        return new Alarm(f, f, str, str2, b2, b, iArr, a2, a3, str3, str4, 0);
    }

    @DexIgnore
    /* Return type fixed from 'java.lang.Object' to match base method */
    @Override // com.mapped.Hu3
    public /* bridge */ /* synthetic */ Alarm deserialize(JsonElement jsonElement, Type type, Gu3 gu3) {
        return a(jsonElement, type, gu3);
    }
}
