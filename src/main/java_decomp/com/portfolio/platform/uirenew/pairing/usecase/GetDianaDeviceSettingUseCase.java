package com.portfolio.platform.uirenew.pairing.usecase;

import android.text.TextUtils;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.D26;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Mo5;
import com.fossil.Oo5;
import com.fossil.Ry6;
import com.fossil.Sy6;
import com.fossil.Ty6;
import com.fossil.Uy6;
import com.fossil.V36;
import com.google.gson.Gson;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.preset.data.source.DianaRecommendedPresetRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetDianaDeviceSettingUseCase extends CoroutineUseCase<Ty6, Uy6, Sy6> {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public String d;
    @DexIgnore
    public Ry6 e; // = Ry6.LOGIN;
    @DexIgnore
    public /* final */ WatchAppRepository f;
    @DexIgnore
    public /* final */ ComplicationRepository g;
    @DexIgnore
    public /* final */ DianaAppSettingRepository h;
    @DexIgnore
    public /* final */ CategoryRepository i;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository j;
    @DexIgnore
    public /* final */ RingStyleRepository k;
    @DexIgnore
    public /* final */ V36 l;
    @DexIgnore
    public /* final */ D26 m;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase n;
    @DexIgnore
    public /* final */ An4 o;
    @DexIgnore
    public /* final */ WatchLocalizationRepository p;
    @DexIgnore
    public /* final */ AlarmsRepository q;
    @DexIgnore
    public /* final */ WFAssetRepository r;
    @DexIgnore
    public /* final */ FileRepository s;
    @DexIgnore
    public /* final */ DianaPresetRepository t;
    @DexIgnore
    public /* final */ DianaRecommendedPresetRepository u;
    @DexIgnore
    public /* final */ UserRepository v;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase", f = "GetDianaDeviceSettingUseCase.kt", l = {125, 130, 160, 166}, m = "initialisePresets")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetDianaDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = getDianaDeviceSettingUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.G(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase", f = "GetDianaDeviceSettingUseCase.kt", l = {175, 177, 186}, m = "signInUpPresets")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetDianaDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, Xe6 xe6) {
            super(xe6);
            this.this$0 = getDianaDeviceSettingUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.J(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase$start$1", f = "GetDianaDeviceSettingUseCase.kt", l = {82, 83, 84, 85, 86, 87, 88, 89, 90, 94, 98, 99, 103, 104, 113}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GetDianaDeviceSettingUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = getDianaDeviceSettingUseCase;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x02a7  */
        /* JADX WARNING: Removed duplicated region for block: B:102:0x02ab  */
        /* JADX WARNING: Removed duplicated region for block: B:104:0x02af  */
        /* JADX WARNING: Removed duplicated region for block: B:108:0x02b9  */
        /* JADX WARNING: Removed duplicated region for block: B:110:0x02bf  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x007e  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00cc  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00f7  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0127  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0142  */
        /* JADX WARNING: Removed duplicated region for block: B:50:0x0187  */
        /* JADX WARNING: Removed duplicated region for block: B:54:0x01a3  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x01be  */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x01d6  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x01fc  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0214  */
        /* JADX WARNING: Removed duplicated region for block: B:78:0x0236  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0037  */
        /* JADX WARNING: Removed duplicated region for block: B:84:0x025b  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x0276  */
        /* JADX WARNING: Removed duplicated region for block: B:93:0x028e  */
        /* JADX WARNING: Removed duplicated region for block: B:98:0x02a3  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 742
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = GetDianaDeviceSettingUseCase.class.getSimpleName();
        Wg6.b(simpleName, "GetDianaDeviceSettingUse\u2026se::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public GetDianaDeviceSettingUseCase(WatchAppRepository watchAppRepository, ComplicationRepository complicationRepository, DianaAppSettingRepository dianaAppSettingRepository, CategoryRepository categoryRepository, DianaWatchFaceRepository dianaWatchFaceRepository, RingStyleRepository ringStyleRepository, V36 v36, D26 d26, NotificationSettingsDatabase notificationSettingsDatabase, An4 an4, WatchLocalizationRepository watchLocalizationRepository, AlarmsRepository alarmsRepository, WFAssetRepository wFAssetRepository, FileRepository fileRepository, DianaPresetRepository dianaPresetRepository, DianaRecommendedPresetRepository dianaRecommendedPresetRepository, UserRepository userRepository, WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        Wg6.c(categoryRepository, "mCategoryRepository");
        Wg6.c(dianaWatchFaceRepository, "mDianaWatchFaceRepository");
        Wg6.c(ringStyleRepository, "mRingStyleRepository");
        Wg6.c(v36, "mGetApp");
        Wg6.c(d26, "mGetAllContactGroups");
        Wg6.c(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        Wg6.c(an4, "mSharePref");
        Wg6.c(watchLocalizationRepository, "mWatchLocalizationRepository");
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(dianaPresetRepository, "dianaPresetRepository");
        Wg6.c(dianaRecommendedPresetRepository, "dianaRecommendedPresetRepository");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(wFBackgroundPhotoRepository, "photoRepository");
        this.f = watchAppRepository;
        this.g = complicationRepository;
        this.h = dianaAppSettingRepository;
        this.i = categoryRepository;
        this.j = dianaWatchFaceRepository;
        this.k = ringStyleRepository;
        this.l = v36;
        this.m = d26;
        this.n = notificationSettingsDatabase;
        this.o = an4;
        this.p = watchLocalizationRepository;
        this.q = alarmsRepository;
        this.r = wFAssetRepository;
        this.s = fileRepository;
        this.t = dianaPresetRepository;
        this.u = dianaRecommendedPresetRepository;
        this.v = userRepository;
        this.w = wFBackgroundPhotoRepository;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02e7  */
    /* JADX WARNING: Removed duplicated region for block: B:101:0x02ea  */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x02e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x010a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0113  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x013c  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01d8  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01fb  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01ff  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0228  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0024  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x024f  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x025f  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0290  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object G(com.mapped.Xe6<? super com.mapped.Cd6> r19) {
        /*
        // Method dump skipped, instructions count: 749
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.G(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public Object H(Ty6 ty6, Xe6<? super Cd6> xe6) {
        if (ty6 == null) {
            i(new Sy6(600, ""));
            return Cd6.a;
        }
        this.d = ty6.a();
        this.e = ty6.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "download device setting of " + this.d);
        if (!Hx5.b(PortfolioApp.get.instance())) {
            i(new Sy6(601, ""));
            return Cd6.a;
        }
        if (!TextUtils.isEmpty(this.d)) {
            DeviceHelper.Ai ai = DeviceHelper.o;
            String str2 = this.d;
            if (str2 == null) {
                Wg6.i();
                throw null;
            } else if (ai.v(str2)) {
                K();
                return Cd6.a;
            }
        }
        i(new Sy6(600, ""));
        return Cd6.a;
    }

    @DexIgnore
    public final void I(Mo5 mo5) {
        String str;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        StringBuilder sb = new StringBuilder();
        sb.append("setUpPresetDataForWatch - id: ");
        sb.append(mo5 != null ? mo5.e() : null);
        sb.append(" - url: ");
        sb.append(mo5 != null ? mo5.d() : null);
        local.e(str2, sb.toString());
        if (mo5 != null) {
            List<Oo5> a2 = mo5.a();
            if (a2 != null) {
                WatchAppMappingSettings l2 = CustomizeConfigurationExt.l(a2, new Gson(), new ArrayList());
                PortfolioApp instance = PortfolioApp.get.instance();
                String str3 = this.d;
                if (str3 != null) {
                    instance.v1(l2, str3);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            PortfolioApp instance2 = PortfolioApp.get.instance();
            File fileByFileName = this.s.getFileByFileName(mo5.e());
            if (fileByFileName == null || (str = fileByFileName.getAbsolutePath()) == null) {
                str = "";
            }
            String str4 = this.d;
            if (str4 != null) {
                PortfolioApp.W1(instance2, str, null, str4, 2, null);
            } else {
                Wg6.i();
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00a9  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object J(com.mapped.Xe6<? super com.mapped.Cd6> r14) {
        /*
        // Method dump skipped, instructions count: 285
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase.J(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Rm6 K() {
        return Gu7.d(g(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return x;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ty6 ty6, Xe6 xe6) {
        return H(ty6, xe6);
    }
}
