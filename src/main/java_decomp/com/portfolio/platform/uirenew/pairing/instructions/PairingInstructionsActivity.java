package com.portfolio.platform.uirenew.pairing.instructions;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.fossil.By6;
import com.fossil.Sx6;
import com.fossil.Zx6;
import com.mapped.Qg6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PairingInstructionsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public By6 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Context context, boolean z, boolean z2, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            if ((i & 4) != 0) {
                z2 = false;
            }
            aVar.a(context, z, z2);
        }

        @DexIgnore
        public final void a(Context context, boolean z, boolean z2) {
            Wg6.c(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("", "start isOnboarding=" + z + ", isClearTop=" + z2);
            Intent intent = new Intent(context, PairingInstructionsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putBoolean("IS_ONBOARDING_FLOW", z);
            intent.putExtras(bundle);
            if (!z2) {
                intent.setFlags(536870912);
            } else {
                intent.setFlags(67108864);
            }
            context.startActivity(intent);
        }
    }

    /*
    static {
        Wg6.b(PairingInstructionsActivity.class.getSimpleName(), "PairingInstructionsActivity::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        boolean z = false;
        super.onCreate(bundle);
        setContentView(2131558439);
        Sx6 sx6 = (Sx6) getSupportFragmentManager().Y(2131362158);
        Intent intent = getIntent();
        if (intent != null) {
            z = intent.getBooleanExtra("IS_ONBOARDING_FLOW", false);
        }
        if (sx6 == null) {
            sx6 = Sx6.l.a(z);
            i(sx6, 2131362158);
        }
        PortfolioApp.get.instance().getIface().E1(new Zx6(sx6)).a(this);
    }
}
