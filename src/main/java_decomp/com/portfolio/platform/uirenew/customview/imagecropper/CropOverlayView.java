package com.portfolio.platform.uirenew.customview.imagecropper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Ry5;
import com.fossil.Ty5;
import com.fossil.Uy5;
import com.fossil.Vy5;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CropOverlayView extends View {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public float D;
    @DexIgnore
    public CropImageView.d E;
    @DexIgnore
    public CropImageView.c F;
    @DexIgnore
    public /* final */ Rect G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public Integer I;
    @DexIgnore
    public ScaleGestureDetector b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ Uy5 d;
    @DexIgnore
    public b e;
    @DexIgnore
    public /* final */ RectF f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Path k;
    @DexIgnore
    public /* final */ float[] l;
    @DexIgnore
    public /* final */ RectF m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public Vy5 z;

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @TargetApi(11)
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            RectF h = CropOverlayView.this.d.h();
            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();
            float currentSpanY = scaleGestureDetector.getCurrentSpanY() / 2.0f;
            float currentSpanX = scaleGestureDetector.getCurrentSpanX() / 2.0f;
            float f = focusY - currentSpanY;
            float f2 = focusX - currentSpanX;
            float f3 = focusX + currentSpanX;
            float f4 = focusY + currentSpanY;
            if (f2 >= f3 || f > f4 || f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f3 > CropOverlayView.this.d.c() || f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f4 > CropOverlayView.this.d.b()) {
                return true;
            }
            h.set(f2, f, f3, f4);
            CropOverlayView.this.d.t(h);
            CropOverlayView.this.invalidate();
            return true;
        }
    }

    @DexIgnore
    public CropOverlayView(Context context) {
        this(context, null);
    }

    @DexIgnore
    public CropOverlayView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.d = new Uy5();
        this.f = new RectF();
        this.k = new Path();
        this.l = new float[8];
        this.m = new RectF();
        this.D = ((float) this.B) / ((float) this.C);
        this.G = new Rect();
    }

    @DexIgnore
    public static Paint j(int i2) {
        Paint paint = new Paint();
        paint.setColor(i2);
        return paint;
    }

    @DexIgnore
    public static Paint k(float f2, int i2) {
        if (f2 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            return null;
        }
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        return paint;
    }

    @DexIgnore
    public final boolean b(RectF rectF) {
        float z2 = Ry5.z(this.l);
        float B2 = Ry5.B(this.l);
        float A2 = Ry5.A(this.l);
        float u2 = Ry5.u(this.l);
        if (!n()) {
            this.m.set(z2, B2, A2, u2);
            return false;
        }
        float[] fArr = this.l;
        float f2 = fArr[0];
        float f3 = fArr[1];
        float f4 = fArr[4];
        float f5 = fArr[5];
        float f6 = fArr[6];
        float f7 = fArr[7];
        if (fArr[7] < fArr[1]) {
            if (fArr[1] < fArr[3]) {
                f2 = fArr[6];
                f3 = fArr[7];
                f4 = fArr[2];
                f5 = fArr[3];
                f6 = fArr[4];
                f7 = fArr[5];
            } else {
                f2 = fArr[4];
                f3 = fArr[5];
                f4 = fArr[0];
                f5 = fArr[1];
                f6 = fArr[2];
                f7 = fArr[3];
            }
        } else if (fArr[1] > fArr[3]) {
            f2 = fArr[2];
            f3 = fArr[3];
            f4 = fArr[6];
            f5 = fArr[7];
            f6 = fArr[0];
            f7 = fArr[1];
        }
        float f8 = (f7 - f3) / (f6 - f2);
        float f9 = -1.0f / f8;
        float f10 = f3 - (f8 * f2);
        float f11 = f3 - (f2 * f9);
        float f12 = f5 - (f8 * f4);
        float f13 = f5 - (f4 * f9);
        float centerY = rectF.centerY();
        float f14 = rectF.top;
        float centerX = rectF.centerX();
        float f15 = rectF.left;
        float f16 = (centerY - f14) / (centerX - f15);
        float f17 = -f16;
        float f18 = rectF.top;
        float f19 = f18 - (f15 * f16);
        float f20 = rectF.right;
        float f21 = f18 - (f17 * f20);
        float f22 = f8 - f16;
        float f23 = (f19 - f10) / f22;
        if (f23 >= f20) {
            f23 = z2;
        }
        float max = Math.max(z2, f23);
        float f24 = (f19 - f11) / (f9 - f16);
        if (f24 >= rectF.right) {
            f24 = max;
        }
        float max2 = Math.max(max, f24);
        float f25 = f9 - f17;
        float f26 = (f21 - f13) / f25;
        if (f26 >= rectF.right) {
            f26 = max2;
        }
        float max3 = Math.max(max2, f26);
        float f27 = (f21 - f11) / f25;
        if (f27 <= rectF.left) {
            f27 = A2;
        }
        float min = Math.min(A2, f27);
        float f28 = (f21 - f12) / (f8 - f17);
        if (f28 <= rectF.left) {
            f28 = min;
        }
        float min2 = Math.min(min, f28);
        float f29 = (f19 - f12) / f22;
        if (f29 <= rectF.left) {
            f29 = min2;
        }
        float min3 = Math.min(min2, f29);
        float max4 = Math.max(B2, Math.max((f8 * max3) + f10, (f9 * min3) + f11));
        float min4 = Math.min(u2, Math.min(f13 + (f9 * max3), (f8 * min3) + f12));
        RectF rectF2 = this.m;
        rectF2.left = max3;
        rectF2.top = max4;
        rectF2.right = min3;
        rectF2.bottom = min4;
        return true;
    }

    @DexIgnore
    public final void c(boolean z2) {
        try {
            if (this.e != null) {
                this.e.a(z2);
            }
        } catch (Exception e2) {
            Log.e("AIC", "Exception in crop window changed", e2);
        }
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        RectF h2 = this.d.h();
        float max = Math.max(Ry5.z(this.l), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float max2 = Math.max(Ry5.B(this.l), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float min = Math.min(Ry5.A(this.l), (float) getWidth());
        float min2 = Math.min(Ry5.u(this.l), (float) getHeight());
        if (this.F != CropImageView.c.RECTANGLE) {
            this.k.reset();
            if (Build.VERSION.SDK_INT > 17 || this.F != CropImageView.c.OVAL) {
                this.f.set(h2.left, h2.top, h2.right, h2.bottom);
            } else {
                this.f.set(h2.left + 2.0f, h2.top + 2.0f, h2.right - 2.0f, h2.bottom - 2.0f);
            }
            this.k.addOval(this.f, Path.Direction.CW);
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.k);
            } else {
                canvas.clipPath(this.k, Region.Op.XOR);
            }
            canvas.drawRect(max, max2, min, min2, this.j);
            canvas.restore();
        } else if (!n() || Build.VERSION.SDK_INT <= 17) {
            canvas.drawRect(max, max2, min, h2.top, this.j);
            canvas.drawRect(max, h2.bottom, min, min2, this.j);
            canvas.drawRect(max, h2.top, h2.left, h2.bottom, this.j);
            canvas.drawRect(h2.right, h2.top, min, h2.bottom, this.j);
        } else {
            this.k.reset();
            Path path = this.k;
            float[] fArr = this.l;
            path.moveTo(fArr[0], fArr[1]);
            Path path2 = this.k;
            float[] fArr2 = this.l;
            path2.lineTo(fArr2[2], fArr2[3]);
            Path path3 = this.k;
            float[] fArr3 = this.l;
            path3.lineTo(fArr3[4], fArr3[5]);
            Path path4 = this.k;
            float[] fArr4 = this.l;
            path4.lineTo(fArr4[6], fArr4[7]);
            this.k.close();
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.k);
            } else {
                canvas.clipPath(this.k, Region.Op.INTERSECT);
            }
            canvas.clipRect(h2, Region.Op.XOR);
            canvas.drawRect(max, max2, min, min2, this.j);
            canvas.restore();
        }
    }

    @DexIgnore
    public final void e(Canvas canvas) {
        Paint paint = this.g;
        if (paint != null) {
            float strokeWidth = paint.getStrokeWidth();
            RectF h2 = this.d.h();
            float f2 = strokeWidth / 2.0f;
            h2.inset(f2, f2);
            if (this.F == CropImageView.c.RECTANGLE) {
                canvas.drawRect(h2, this.g);
            } else {
                canvas.drawOval(h2, this.g);
            }
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (this.h != null) {
            Paint paint = this.g;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : 0.0f;
            float strokeWidth2 = this.h.getStrokeWidth();
            float f3 = strokeWidth2 / 2.0f;
            if (this.F == CropImageView.c.RECTANGLE) {
                f2 = this.u;
            }
            float f4 = f2 + f3;
            RectF h2 = this.d.h();
            h2.inset(f4, f4);
            float f5 = (strokeWidth2 - strokeWidth) / 2.0f;
            float f6 = f3 + f5;
            float f7 = h2.left;
            float f8 = h2.top;
            canvas.drawLine(f7 - f5, f8 - f6, f7 - f5, f8 + this.v, this.h);
            float f9 = h2.left;
            float f10 = h2.top;
            canvas.drawLine(f9 - f6, f10 - f5, this.v + f9, f10 - f5, this.h);
            float f11 = h2.right;
            float f12 = h2.top;
            canvas.drawLine(f11 + f5, f12 - f6, f11 + f5, f12 + this.v, this.h);
            float f13 = h2.right;
            float f14 = h2.top;
            canvas.drawLine(f13 + f6, f14 - f5, f13 - this.v, f14 - f5, this.h);
            float f15 = h2.left;
            float f16 = h2.bottom;
            canvas.drawLine(f15 - f5, f16 + f6, f15 - f5, f16 - this.v, this.h);
            float f17 = h2.left;
            float f18 = h2.bottom;
            canvas.drawLine(f17 - f6, f18 + f5, this.v + f17, f18 + f5, this.h);
            float f19 = h2.right;
            float f20 = h2.bottom;
            canvas.drawLine(f19 + f5, f20 + f6, f19 + f5, f20 - this.v, this.h);
            float f21 = h2.right;
            float f22 = h2.bottom;
            canvas.drawLine(f21 + f6, f22 + f5, f21 - this.v, f22 + f5, this.h);
        }
    }

    @DexIgnore
    public final void g(Canvas canvas) {
        if (this.i != null) {
            Paint paint = this.g;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            RectF h2 = this.d.h();
            h2.inset(strokeWidth, strokeWidth);
            float width = h2.width() / 3.0f;
            float height = h2.height() / 3.0f;
            if (this.F == CropImageView.c.OVAL) {
                float width2 = (h2.width() / 2.0f) - strokeWidth;
                float height2 = (h2.height() / 2.0f) - strokeWidth;
                float f2 = h2.left + width;
                float f3 = h2.right - width;
                float sin = (float) (Math.sin(Math.acos((double) ((width2 - width) / width2))) * ((double) height2));
                canvas.drawLine(f2, (h2.top + height2) - sin, f2, (h2.bottom - height2) + sin, this.i);
                canvas.drawLine(f3, (h2.top + height2) - sin, f3, (h2.bottom - height2) + sin, this.i);
                float f4 = h2.top + height;
                float f5 = h2.bottom - height;
                float cos = (float) (((double) width2) * Math.cos(Math.asin((double) ((height2 - height) / height2))));
                canvas.drawLine((h2.left + width2) - cos, f4, (h2.right - width2) + cos, f4, this.i);
                canvas.drawLine((h2.left + width2) - cos, f5, (h2.right - width2) + cos, f5, this.i);
                return;
            }
            float f6 = h2.left + width;
            float f7 = h2.right - width;
            canvas.drawLine(f6, h2.top, f6, h2.bottom, this.i);
            canvas.drawLine(f7, h2.top, f7, h2.bottom, this.i);
            float f8 = h2.top + height;
            float f9 = h2.bottom - height;
            canvas.drawLine(h2.left, f8, h2.right, f8, this.i);
            canvas.drawLine(h2.left, f9, h2.right, f9, this.i);
        }
    }

    @DexIgnore
    public int getAspectRatioX() {
        return this.B;
    }

    @DexIgnore
    public int getAspectRatioY() {
        return this.C;
    }

    @DexIgnore
    public CropImageView.c getCropShape() {
        return this.F;
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        return this.d.h();
    }

    @DexIgnore
    public CropImageView.d getGuidelines() {
        return this.E;
    }

    @DexIgnore
    public Rect getInitialCropWindowRect() {
        return this.G;
    }

    @DexIgnore
    public final void h(RectF rectF) {
        if (rectF.width() < this.d.e()) {
            float e2 = (this.d.e() - rectF.width()) / 2.0f;
            rectF.left -= e2;
            rectF.right = e2 + rectF.right;
        }
        if (rectF.height() < this.d.d()) {
            float d2 = (this.d.d() - rectF.height()) / 2.0f;
            rectF.top -= d2;
            rectF.bottom = d2 + rectF.bottom;
        }
        if (rectF.width() > this.d.c()) {
            float width = (rectF.width() - this.d.c()) / 2.0f;
            rectF.left += width;
            rectF.right -= width;
        }
        if (rectF.height() > this.d.b()) {
            float height = (rectF.height() - this.d.b()) / 2.0f;
            rectF.top += height;
            rectF.bottom -= height;
        }
        b(rectF);
        if (this.m.width() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.m.height() > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float max = Math.max(this.m.left, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float max2 = Math.max(this.m.top, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            float min = Math.min(this.m.right, (float) getWidth());
            float min2 = Math.min(this.m.bottom, (float) getHeight());
            if (rectF.left < max) {
                rectF.left = max;
            }
            if (rectF.top < max2) {
                rectF.top = max2;
            }
            if (rectF.right > min) {
                rectF.right = min;
            }
            if (rectF.bottom > min2) {
                rectF.bottom = min2;
            }
        }
        if (this.A && ((double) Math.abs(rectF.width() - (rectF.height() * this.D))) > 0.1d) {
            if (rectF.width() > rectF.height() * this.D) {
                float abs = Math.abs((rectF.height() * this.D) - rectF.width()) / 2.0f;
                rectF.left += abs;
                rectF.right -= abs;
                return;
            }
            float abs2 = Math.abs((rectF.width() / this.D) - rectF.height()) / 2.0f;
            rectF.top += abs2;
            rectF.bottom -= abs2;
        }
    }

    @DexIgnore
    public void i() {
        RectF cropWindowRect = getCropWindowRect();
        h(cropWindowRect);
        this.d.t(cropWindowRect);
    }

    @DexIgnore
    public final void l() {
        float max = Math.max(Ry5.z(this.l), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float max2 = Math.max(Ry5.B(this.l), (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        float min = Math.min(Ry5.A(this.l), (float) getWidth());
        float min2 = Math.min(Ry5.u(this.l), (float) getHeight());
        if (min > max && min2 > max2) {
            RectF rectF = new RectF();
            this.H = true;
            float f2 = this.w;
            float f3 = min - max;
            float f4 = f2 * f3;
            float f5 = min2 - max2;
            float f6 = f2 * f5;
            if (this.G.width() > 0 && this.G.height() > 0) {
                rectF.left = (((float) this.G.left) / this.d.k()) + max;
                rectF.top = (((float) this.G.top) / this.d.j()) + max2;
                rectF.right = rectF.left + (((float) this.G.width()) / this.d.k());
                rectF.bottom = rectF.top + (((float) this.G.height()) / this.d.j());
                rectF.left = Math.max(max, rectF.left);
                rectF.top = Math.max(max2, rectF.top);
                rectF.right = Math.min(min, rectF.right);
                rectF.bottom = Math.min(min2, rectF.bottom);
            } else if (!this.A || min <= max || min2 <= max2) {
                rectF.left = max + f4;
                rectF.top = max2 + f6;
                rectF.right = min - f4;
                rectF.bottom = min2 - f6;
            } else if (f3 / f5 > this.D) {
                rectF.top = max2 + f6;
                rectF.bottom = min2 - f6;
                float width = ((float) getWidth()) / 2.0f;
                this.D = ((float) this.B) / ((float) this.C);
                float max3 = Math.max(this.d.e(), rectF.height() * this.D) / 2.0f;
                rectF.left = width - max3;
                rectF.right = width + max3;
            } else {
                rectF.left = max + f4;
                rectF.right = min - f4;
                float height = ((float) getHeight()) / 2.0f;
                float max4 = Math.max(this.d.d(), rectF.width() / this.D) / 2.0f;
                rectF.top = height - max4;
                rectF.bottom = height + max4;
            }
            h(rectF);
            this.d.t(rectF);
        }
    }

    @DexIgnore
    public boolean m() {
        return this.A;
    }

    @DexIgnore
    public final boolean n() {
        float[] fArr = this.l;
        return (fArr[0] == fArr[6] || fArr[1] == fArr[7]) ? false : true;
    }

    @DexIgnore
    public final void o(float f2, float f3) {
        Vy5 f4 = this.d.f(f2, f3, this.x, this.F);
        this.z = f4;
        if (f4 != null) {
            invalidate();
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        d(canvas);
        if (this.d.u()) {
            CropImageView.d dVar = this.E;
            if (dVar == CropImageView.d.ON) {
                g(canvas);
            } else if (dVar == CropImageView.d.ON_TOUCH && this.z != null) {
                g(canvas);
            }
        }
        e(canvas);
        f(canvas);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        if (this.c) {
            this.b.onTouchEvent(motionEvent);
        }
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    p(motionEvent.getX(), motionEvent.getY());
                    getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                } else if (action != 3) {
                    return false;
                }
            }
            getParent().requestDisallowInterceptTouchEvent(false);
            q();
            return true;
        }
        o(motionEvent.getX(), motionEvent.getY());
        return true;
    }

    @DexIgnore
    public final void p(float f2, float f3) {
        if (this.z != null) {
            float f4 = this.y;
            RectF h2 = this.d.h();
            if (b(h2)) {
                f4 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            }
            this.z.m(h2, f2, f3, this.m, this.s, this.t, f4, this.A, this.D);
            this.d.t(h2);
            c(true);
            invalidate();
        }
    }

    @DexIgnore
    public final void q() {
        if (this.z != null) {
            this.z = null;
            c(false);
            invalidate();
        }
    }

    @DexIgnore
    public void r() {
        if (this.H) {
            setCropWindowRect(Ry5.b);
            l();
            invalidate();
        }
    }

    @DexIgnore
    public void s(float[] fArr, int i2, int i3) {
        if (fArr == null || !Arrays.equals(this.l, fArr)) {
            if (fArr == null) {
                Arrays.fill(this.l, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                System.arraycopy(fArr, 0, this.l, 0, fArr.length);
            }
            this.s = i2;
            this.t = i3;
            RectF h2 = this.d.h();
            if (h2.width() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || h2.height() == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                l();
            }
        }
    }

    @DexIgnore
    public void setAspectRatioX(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.B != i2) {
            this.B = i2;
            this.D = ((float) i2) / ((float) this.C);
            if (this.H) {
                l();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setAspectRatioY(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.C != i2) {
            this.C = i2;
            this.D = ((float) this.B) / ((float) i2);
            if (this.H) {
                l();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setCropShape(CropImageView.c cVar) {
        if (this.F != cVar) {
            this.F = cVar;
            if (Build.VERSION.SDK_INT <= 17) {
                if (cVar == CropImageView.c.OVAL) {
                    Integer valueOf = Integer.valueOf(getLayerType());
                    this.I = valueOf;
                    if (valueOf.intValue() != 1) {
                        setLayerType(1, null);
                    } else {
                        this.I = null;
                    }
                } else {
                    Integer num = this.I;
                    if (num != null) {
                        setLayerType(num.intValue(), null);
                        this.I = null;
                    }
                }
            }
            invalidate();
        }
    }

    @DexIgnore
    public void setCropWindowChangeListener(b bVar) {
        this.e = bVar;
    }

    @DexIgnore
    public void setCropWindowRect(RectF rectF) {
        this.d.t(rectF);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        if (this.A != z2) {
            this.A = z2;
            if (this.H) {
                l();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setGuidelines(CropImageView.d dVar) {
        if (this.E != dVar) {
            this.E = dVar;
            if (this.H) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setInitialAttributeValues(Ty5 ty5) {
        this.d.q(ty5);
        setCropShape(ty5.b);
        this.y = ty5.c;
        setGuidelines(ty5.e);
        setFixedAspectRatio(ty5.m);
        setAspectRatioX(ty5.s);
        setAspectRatioY(ty5.t);
        w(ty5.j);
        this.x = ty5.d;
        this.w = ty5.l;
        this.g = k(ty5.u, ty5.v);
        this.u = ty5.x;
        this.v = ty5.y;
        this.h = k(ty5.w, ty5.z);
        this.i = k(ty5.A, ty5.B);
        this.j = j(ty5.C);
    }

    @DexIgnore
    public void setInitialCropWindowRect(Rect rect) {
        Rect rect2 = this.G;
        if (rect == null) {
            rect = Ry5.a;
        }
        rect2.set(rect);
        if (this.H) {
            l();
            invalidate();
            c(false);
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        this.y = f2;
    }

    @DexIgnore
    public void t(float f2, float f3, float f4, float f5) {
        this.d.p(f2, f3, f4, f5);
    }

    @DexIgnore
    public void u(int i2, int i3) {
        this.d.r(i2, i3);
    }

    @DexIgnore
    public void v(int i2, int i3) {
        this.d.s(i2, i3);
    }

    @DexIgnore
    public boolean w(boolean z2) {
        if (this.c == z2) {
            return false;
        }
        this.c = z2;
        if (z2 && this.b == null) {
            this.b = new ScaleGestureDetector(getContext(), new c());
        }
        return true;
    }
}
