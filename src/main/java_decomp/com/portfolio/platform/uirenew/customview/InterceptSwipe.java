package com.portfolio.platform.uirenew.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.ListView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Co0;
import com.fossil.Do0;
import com.fossil.Go0;
import com.fossil.Ho0;
import com.fossil.Ip0;
import com.fossil.Iy5;
import com.fossil.Mo0;
import com.fossil.Wx0;
import com.mapped.W6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class InterceptSwipe extends ViewGroup implements Go0, Co0 {
    @DexIgnore
    public static /* final */ String W; // = InterceptSwipe.class.getSimpleName();
    @DexIgnore
    public static /* final */ int[] a0; // = {16842766};
    @DexIgnore
    public /* final */ DecelerateInterpolator A;
    @DexIgnore
    public Iy5 B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public float E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public int H;
    @DexIgnore
    public Wx0 I;
    @DexIgnore
    public Animation J;
    @DexIgnore
    public Animation K;
    @DexIgnore
    public Animation L;
    @DexIgnore
    public Animation M;
    @DexIgnore
    public Animation N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public int P;
    @DexIgnore
    public boolean Q;
    @DexIgnore
    public i R;
    @DexIgnore
    public Animation.AnimationListener S;
    @DexIgnore
    public Float T;
    @DexIgnore
    public /* final */ Animation U;
    @DexIgnore
    public /* final */ Animation V;
    @DexIgnore
    public String b;
    @DexIgnore
    public View c;
    @DexIgnore
    public j d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public int f;
    @DexIgnore
    public float g;
    @DexIgnore
    public float h;
    @DexIgnore
    public /* final */ Ho0 i;
    @DexIgnore
    public /* final */ Do0 j;
    @DexIgnore
    public /* final */ int[] k;
    @DexIgnore
    public /* final */ int[] l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public boolean w;
    @DexIgnore
    public int x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Animation.AnimationListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            j jVar;
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            if (interceptSwipe.e) {
                interceptSwipe.I.setAlpha(255);
                InterceptSwipe.this.I.start();
                InterceptSwipe interceptSwipe2 = InterceptSwipe.this;
                if (interceptSwipe2.O && (jVar = interceptSwipe2.d) != null) {
                    jVar.a();
                }
                InterceptSwipe interceptSwipe3 = InterceptSwipe.this;
                interceptSwipe3.t = interceptSwipe3.B.getTop();
                return;
            }
            interceptSwipe.r();
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends Animation {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.setAnimationProgress(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends Animation {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.setAnimationProgress(1.0f - f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends Animation {
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public d(int i, int i2) {
            this.b = i;
            this.c = i2;
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            Wx0 wx0 = InterceptSwipe.this.I;
            int i = this.b;
            wx0.setAlpha((int) ((((float) (this.c - i)) * f) + ((float) i)));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Animation.AnimationListener {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationEnd(Animation animation) {
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            if (!interceptSwipe.y) {
                interceptSwipe.z(null);
            }
        }

        @DexIgnore
        public void onAnimationRepeat(Animation animation) {
        }

        @DexIgnore
        public void onAnimationStart(Animation animation) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends Animation {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            int abs = !interceptSwipe.Q ? interceptSwipe.G - Math.abs(interceptSwipe.F) : interceptSwipe.G;
            InterceptSwipe interceptSwipe2 = InterceptSwipe.this;
            int i = interceptSwipe2.D;
            InterceptSwipe.this.setTargetOffsetTopAndBottom((((int) (((float) (abs - i)) * f)) + i) - interceptSwipe2.B.getTop());
            InterceptSwipe.this.I.e(1.0f - f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends Animation {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe.this.p(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends Animation {
        @DexIgnore
        public h() {
        }

        @DexIgnore
        public void applyTransformation(float f, Transformation transformation) {
            InterceptSwipe interceptSwipe = InterceptSwipe.this;
            float f2 = interceptSwipe.E;
            interceptSwipe.setAnimationProgress(f2 + ((-f2) * f));
            InterceptSwipe.this.p(f);
        }
    }

    @DexIgnore
    public interface i {
        @DexIgnore
        boolean a(InterceptSwipe interceptSwipe, View view);
    }

    @DexIgnore
    public interface j {
        @DexIgnore
        Object a();  // void declaration
    }

    @DexIgnore
    public InterceptSwipe(Context context) {
        this(context, null);
    }

    @DexIgnore
    public InterceptSwipe(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.b = InterceptSwipe.class.getSimpleName();
        this.e = false;
        this.g = -1.0f;
        this.k = new int[2];
        this.l = new int[2];
        this.x = -1;
        this.C = -1;
        this.S = new a();
        this.T = Float.valueOf((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.U = new f();
        this.V = new g();
        this.f = ViewConfiguration.get(context).getScaledTouchSlop();
        this.s = getResources().getInteger(17694721);
        setWillNotDraw(false);
        this.A = new DecelerateInterpolator(2.0f);
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        this.P = (int) (displayMetrics.density * 40.0f);
        d();
        setChildrenDrawingOrderEnabled(true);
        int i2 = (int) (displayMetrics.density * 64.0f);
        this.G = i2;
        this.g = (float) i2;
        this.i = new Ho0(this);
        this.j = new Do0(this);
        setNestedScrollingEnabled(true);
        int i3 = -this.P;
        this.t = i3;
        this.F = i3;
        p(1.0f);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, a0);
        setEnabled(obtainStyledAttributes.getBoolean(0, true));
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    private void setColorViewAlpha(int i2) {
        this.B.getBackground().setAlpha(i2);
        this.I.setAlpha(i2);
    }

    @DexIgnore
    public final void A(int i2, Animation.AnimationListener animationListener) {
        this.D = i2;
        this.E = this.B.getScaleX();
        h hVar = new h();
        this.N = hVar;
        hVar.setDuration(150);
        if (animationListener != null) {
            this.B.b(animationListener);
        }
        this.B.clearAnimation();
        this.B.startAnimation(this.N);
    }

    @DexIgnore
    public final void B(Animation.AnimationListener animationListener) {
        this.B.setVisibility(0);
        this.I.setAlpha(255);
        b bVar = new b();
        this.J = bVar;
        bVar.setDuration((long) this.s);
        if (animationListener != null) {
            this.B.b(animationListener);
        }
        this.B.clearAnimation();
        this.B.startAnimation(this.J);
    }

    @DexIgnore
    public final void a(int i2, Animation.AnimationListener animationListener) {
        this.D = i2;
        this.U.reset();
        this.U.setDuration(200);
        this.U.setInterpolator(this.A);
        if (animationListener != null) {
            this.B.b(animationListener);
        }
        this.B.clearAnimation();
        this.B.startAnimation(this.U);
    }

    @DexIgnore
    public final void b(int i2, Animation.AnimationListener animationListener) {
        if (this.y) {
            A(i2, animationListener);
            return;
        }
        this.D = i2;
        this.V.reset();
        this.V.setDuration(200);
        this.V.setInterpolator(this.A);
        if (animationListener != null) {
            this.B.b(animationListener);
        }
        this.B.clearAnimation();
        this.B.startAnimation(this.V);
    }

    @DexIgnore
    public boolean c() {
        i iVar = this.R;
        if (iVar != null) {
            return iVar.a(this, this.c);
        }
        View view = this.c;
        return view instanceof ListView ? Ip0.a((ListView) view, -1) : view.canScrollVertically(-1);
    }

    @DexIgnore
    public final void d() {
        this.B = new Iy5(getContext(), -328966);
        Wx0 wx0 = new Wx0(getContext());
        this.I = wx0;
        wx0.l(1);
        this.B.setImageDrawable(this.I);
        this.B.setVisibility(8);
        addView(this.B);
    }

    @DexIgnore
    public boolean dispatchNestedFling(float f2, float f3, boolean z2) {
        return this.j.a(f2, f3, z2);
    }

    @DexIgnore
    public boolean dispatchNestedPreFling(float f2, float f3) {
        return this.j.b(f2, f3);
    }

    @DexIgnore
    public boolean dispatchNestedPreScroll(int i2, int i3, int[] iArr, int[] iArr2) {
        return this.j.c(i2, i3, iArr, iArr2);
    }

    @DexIgnore
    public boolean dispatchNestedScroll(int i2, int i3, int i4, int i5, int[] iArr) {
        return this.j.f(i2, i3, i4, i5, iArr);
    }

    @DexIgnore
    public final void e() {
        if (this.c == null) {
            for (int i2 = 0; i2 < getChildCount(); i2++) {
                View childAt = getChildAt(i2);
                if (!childAt.equals(this.B)) {
                    this.c = childAt;
                    return;
                }
            }
        }
    }

    @DexIgnore
    public final void f(float f2) {
        if (f2 > this.g) {
            u(true, true);
            return;
        }
        this.e = false;
        this.I.j(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        e eVar = null;
        if (!this.y) {
            eVar = new e();
        }
        b(this.t, eVar);
        this.I.d(false);
    }

    @DexIgnore
    public final void g(MotionEvent motionEvent, float f2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.b;
        local.e(str, "handleInterceptTouchEventWithParent event: " + motionEvent.getAction() + " overScroll: " + f2);
        int action = motionEvent.getAction();
        if (action == 0) {
            this.T = Float.valueOf(motionEvent.getX());
            getParent().requestDisallowInterceptTouchEvent(true);
        } else if (action == 2) {
            float abs = Math.abs(motionEvent.getX() - this.T.floatValue());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.b;
            local2.e(str2, "dx: " + abs + " - touchSlope: " + this.f);
            if (abs > ((float) this.f)) {
                if (f2 != -1.0f) {
                    f(f2);
                }
                getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
    }

    @DexIgnore
    public int getChildDrawingOrder(int i2, int i3) {
        int i4 = this.C;
        return i4 < 0 ? i3 : i3 == i2 + -1 ? i4 : i3 >= i4 ? i3 + 1 : i3;
    }

    @DexIgnore
    public int getNestedScrollAxes() {
        return this.i.a();
    }

    @DexIgnore
    public int getProgressCircleDiameter() {
        return this.P;
    }

    @DexIgnore
    public int getProgressViewEndOffset() {
        return this.G;
    }

    @DexIgnore
    public int getProgressViewStartOffset() {
        return this.F;
    }

    @DexIgnore
    public final boolean h(Animation animation) {
        return animation != null && animation.hasStarted() && !animation.hasEnded();
    }

    @DexIgnore
    public boolean hasNestedScrollingParent() {
        return this.j.k();
    }

    @DexIgnore
    public final void i(float f2) {
        this.I.d(true);
        float min = Math.min(1.0f, Math.abs(f2 / this.g));
        float max = (((float) Math.max(((double) min) - 0.4d, 0.0d)) * 5.0f) / 3.0f;
        float abs = Math.abs(f2);
        float f3 = this.g;
        int i2 = this.H;
        if (i2 <= 0) {
            i2 = this.Q ? this.G - this.F : this.G;
        }
        float f4 = (float) i2;
        double max2 = (double) (Math.max((float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Math.min(abs - f3, f4 * 2.0f) / f4) / 4.0f);
        float pow = ((float) (max2 - Math.pow(max2, 2.0d))) * 2.0f;
        int i3 = this.F;
        int i4 = (int) ((f4 * pow * 2.0f) + (min * f4));
        if (this.B.getVisibility() != 0) {
            this.B.setVisibility(0);
        }
        if (!this.y) {
            this.B.setScaleX(1.0f);
            this.B.setScaleY(1.0f);
        }
        if (this.y) {
            setAnimationProgress(Math.min(1.0f, f2 / this.g));
        }
        if (f2 < this.g) {
            if (this.I.getAlpha() > 76 && !h(this.L)) {
                y();
            }
        } else if (this.I.getAlpha() < 255 && !h(this.M)) {
            x();
        }
        this.I.j(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, Math.min(0.8f, max * 0.8f));
        this.I.e(Math.min(1.0f, max));
        this.I.g((((max * 0.4f) - 0.25f) + (pow * 2.0f)) * 0.5f);
        setTargetOffsetTopAndBottom((i4 + i3) - this.t);
    }

    @DexIgnore
    @Override // com.fossil.Co0
    public boolean isNestedScrollingEnabled() {
        return this.j.m();
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        r();
    }

    @DexIgnore
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        g(motionEvent, -1.0f);
        e();
        int actionMasked = motionEvent.getActionMasked();
        if (this.z && actionMasked == 0) {
            this.z = false;
        }
        if (!isEnabled() || this.z || c() || this.e || this.m) {
            return false;
        }
        if (actionMasked != 0) {
            if (actionMasked != 1) {
                if (actionMasked == 2) {
                    int i2 = this.x;
                    if (i2 == -1) {
                        Log.e(W, "Got ACTION_MOVE event but don't have an active pointer id.");
                        return false;
                    }
                    int findPointerIndex = motionEvent.findPointerIndex(i2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = this.b;
                    local.e(str, "onInterceptTouchEvent - ActionMove - mActivePointerId " + this.x);
                    if (findPointerIndex < 0) {
                        return false;
                    }
                    w(motionEvent.getY(findPointerIndex));
                } else if (actionMasked != 3) {
                    if (actionMasked == 6) {
                        q(motionEvent);
                    }
                }
            }
            this.w = false;
            this.x = -1;
        } else {
            setTargetOffsetTopAndBottom(this.F - this.B.getTop());
            int pointerId = motionEvent.getPointerId(0);
            this.x = pointerId;
            this.w = false;
            int findPointerIndex2 = motionEvent.findPointerIndex(pointerId);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.b;
            local2.e(str2, "onInterceptTouchEvent - ActionDown - pointerIndex " + findPointerIndex2);
            if (findPointerIndex2 < 0) {
                return false;
            }
            this.v = motionEvent.getY(findPointerIndex2);
        }
        return this.w;
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        if (getChildCount() != 0) {
            if (this.c == null) {
                e();
            }
            View view = this.c;
            if (view != null) {
                int paddingLeft = getPaddingLeft();
                int paddingTop = getPaddingTop();
                view.layout(paddingLeft, paddingTop, ((measuredWidth - getPaddingLeft()) - getPaddingRight()) + paddingLeft, ((measuredHeight - getPaddingTop()) - getPaddingBottom()) + paddingTop);
                int measuredWidth2 = this.B.getMeasuredWidth();
                int measuredHeight2 = this.B.getMeasuredHeight();
                Iy5 iy5 = this.B;
                int i6 = measuredWidth / 2;
                int i7 = measuredWidth2 / 2;
                int i8 = this.t;
                iy5.layout(i6 - i7, i8, i6 + i7, measuredHeight2 + i8);
            }
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        if (this.c == null) {
            e();
        }
        View view = this.c;
        if (view != null) {
            view.measure(View.MeasureSpec.makeMeasureSpec((getMeasuredWidth() - getPaddingLeft()) - getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec((getMeasuredHeight() - getPaddingTop()) - getPaddingBottom(), 1073741824));
            this.B.measure(View.MeasureSpec.makeMeasureSpec(this.P, 1073741824), View.MeasureSpec.makeMeasureSpec(this.P, 1073741824));
            this.C = -1;
            for (int i4 = 0; i4 < getChildCount(); i4++) {
                if (getChildAt(i4) == this.B) {
                    this.C = i4;
                    return;
                }
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public boolean onNestedFling(View view, float f2, float f3, boolean z2) {
        return dispatchNestedFling(f2, f3, z2);
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public boolean onNestedPreFling(View view, float f2, float f3) {
        return dispatchNestedPreFling(f2, f3);
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public void onNestedPreScroll(View view, int i2, int i3, int[] iArr) {
        if (i3 > 0) {
            float f2 = this.h;
            if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                float f3 = (float) i3;
                if (f3 > f2) {
                    iArr[1] = i3 - ((int) f2);
                    this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
                } else {
                    this.h = f2 - f3;
                    iArr[1] = i3;
                }
                i(this.h);
            }
        }
        if (this.Q && i3 > 0 && this.h == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && Math.abs(i3 - iArr[1]) > 0) {
            this.B.setVisibility(8);
        }
        int[] iArr2 = this.k;
        if (dispatchNestedPreScroll(i2 - iArr[0], i3 - iArr[1], iArr2, null)) {
            iArr[0] = iArr[0] + iArr2[0];
            iArr[1] = iArr2[1] + iArr[1];
        }
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public void onNestedScroll(View view, int i2, int i3, int i4, int i5) {
        dispatchNestedScroll(i2, i3, i4, i5, this.l);
        int i6 = this.l[1] + i5;
        if (i6 < 0 && !c()) {
            float abs = ((float) Math.abs(i6)) + this.h;
            this.h = abs;
            i(abs);
        }
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public void onNestedScrollAccepted(View view, View view2, int i2) {
        this.i.b(view, view2, i2);
        startNestedScroll(i2 & 2);
        this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.m = true;
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public boolean onStartNestedScroll(View view, View view2, int i2) {
        return isEnabled() && !this.z && !this.e && (i2 & 2) != 0;
    }

    @DexIgnore
    @Override // com.fossil.Go0
    public void onStopNestedScroll(View view) {
        this.i.d(view);
        this.m = false;
        float f2 = this.h;
        if (f2 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            f(f2);
            this.h = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        }
        stopNestedScroll();
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (this.z && actionMasked == 0) {
            this.z = false;
        }
        if (!isEnabled() || this.z || c() || this.e || this.m) {
            return false;
        }
        if (actionMasked == 0) {
            FLogger.INSTANCE.getLocal().e(this.b, "onTouch - ActionDown");
            this.x = motionEvent.getPointerId(0);
            this.w = false;
        } else if (actionMasked == 1) {
            int findPointerIndex = motionEvent.findPointerIndex(this.x);
            if (findPointerIndex < 0) {
                Log.e(W, "Got ACTION_UP event but don't have an active pointer id.");
                return false;
            }
            if (this.w) {
                float y2 = motionEvent.getY(findPointerIndex);
                float f2 = this.u;
                this.w = false;
                f((y2 - f2) * 0.5f);
            }
            this.x = -1;
            return false;
        } else if (actionMasked == 2) {
            FLogger.INSTANCE.getLocal().e(this.b, "onTouch - ActionMove");
            int findPointerIndex2 = motionEvent.findPointerIndex(this.x);
            if (findPointerIndex2 < 0) {
                Log.e(W, "Got ACTION_MOVE event but have an invalid active pointer id.");
                return false;
            }
            float y3 = motionEvent.getY(findPointerIndex2);
            w(y3);
            if (this.w) {
                float f3 = (y3 - this.u) * 0.5f;
                if (f3 <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                    return false;
                }
                i(f3);
                g(motionEvent, f3);
            } else {
                g(motionEvent, -1.0f);
            }
        } else if (actionMasked == 3) {
            return false;
        } else {
            if (actionMasked == 5) {
                int actionIndex = motionEvent.getActionIndex();
                if (actionIndex < 0) {
                    Log.e(W, "Got ACTION_POINTER_DOWN event but have an invalid action index.");
                    return false;
                }
                this.x = motionEvent.getPointerId(actionIndex);
            } else if (actionMasked == 6) {
                q(motionEvent);
            }
        }
        return true;
    }

    @DexIgnore
    public void p(float f2) {
        int i2 = this.D;
        setTargetOffsetTopAndBottom((i2 + ((int) (((float) (this.F - i2)) * f2))) - this.B.getTop());
    }

    @DexIgnore
    public final void q(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        if (motionEvent.getPointerId(actionIndex) == this.x) {
            this.x = motionEvent.getPointerId(actionIndex == 0 ? 1 : 0);
        }
    }

    @DexIgnore
    public void r() {
        this.B.clearAnimation();
        this.I.stop();
        this.B.setVisibility(8);
        setColorViewAlpha(255);
        if (this.y) {
            setAnimationProgress(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        } else {
            setTargetOffsetTopAndBottom(this.F - this.t);
        }
        this.t = this.B.getTop();
    }

    @DexIgnore
    public void requestDisallowInterceptTouchEvent(boolean z2) {
        if (Build.VERSION.SDK_INT >= 21 || !(this.c instanceof AbsListView)) {
            View view = this.c;
            if (view == null || Mo0.R(view)) {
                super.requestDisallowInterceptTouchEvent(z2);
            }
        }
    }

    @DexIgnore
    public void s(boolean z2, int i2) {
        this.G = i2;
        this.y = z2;
        this.B.invalidate();
    }

    @DexIgnore
    public void setAnimationProgress(float f2) {
        this.B.setScaleX(f2);
        this.B.setScaleY(f2);
    }

    @DexIgnore
    @Deprecated
    public void setColorScheme(int... iArr) {
        setColorSchemeResources(iArr);
    }

    @DexIgnore
    public void setColorSchemeColors(int... iArr) {
        e();
        this.I.f(iArr);
    }

    @DexIgnore
    public void setColorSchemeResources(int... iArr) {
        Context context = getContext();
        int[] iArr2 = new int[iArr.length];
        for (int i2 = 0; i2 < iArr.length; i2++) {
            iArr2[i2] = W6.d(context, iArr[i2]);
        }
        setColorSchemeColors(iArr2);
    }

    @DexIgnore
    public void setDistanceToTriggerSync(int i2) {
        this.g = (float) i2;
    }

    @DexIgnore
    public void setEnabled(boolean z2) {
        super.setEnabled(z2);
        if (!z2) {
            r();
        }
    }

    @DexIgnore
    @Override // com.fossil.Co0
    public void setNestedScrollingEnabled(boolean z2) {
        this.j.n(z2);
    }

    @DexIgnore
    public void setOnChildScrollUpCallback(i iVar) {
        this.R = iVar;
    }

    @DexIgnore
    public void setOnRefreshListener(j jVar) {
        this.d = jVar;
    }

    @DexIgnore
    @Deprecated
    public void setProgressBackgroundColor(int i2) {
        setProgressBackgroundColorSchemeResource(i2);
    }

    @DexIgnore
    public void setProgressBackgroundColorSchemeColor(int i2) {
        this.B.setBackgroundColor(i2);
    }

    @DexIgnore
    public void setProgressBackgroundColorSchemeResource(int i2) {
        setProgressBackgroundColorSchemeColor(W6.d(getContext(), i2));
    }

    @DexIgnore
    public void setRefreshing(boolean z2) {
        if (!z2 || this.e == z2) {
            u(z2, false);
            return;
        }
        this.e = z2;
        setTargetOffsetTopAndBottom((!this.Q ? this.G + this.F : this.G) - this.t);
        this.O = false;
        B(this.S);
    }

    @DexIgnore
    public void setSize(int i2) {
        if (i2 == 0 || i2 == 1) {
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            if (i2 == 0) {
                this.P = (int) (displayMetrics.density * 56.0f);
            } else {
                this.P = (int) (displayMetrics.density * 40.0f);
            }
            this.B.setImageDrawable(null);
            this.I.l(i2);
            this.B.setImageDrawable(this.I);
        }
    }

    @DexIgnore
    public void setSlingshotDistance(int i2) {
        this.H = i2;
    }

    @DexIgnore
    public void setTargetOffsetTopAndBottom(int i2) {
        this.B.bringToFront();
        Mo0.W(this.B, i2);
        this.t = this.B.getTop();
    }

    @DexIgnore
    public boolean startNestedScroll(int i2) {
        return this.j.p(i2);
    }

    @DexIgnore
    @Override // com.fossil.Co0
    public void stopNestedScroll() {
        this.j.r();
    }

    @DexIgnore
    public void t(boolean z2, int i2, int i3) {
        this.y = z2;
        this.F = i2;
        this.G = i3;
        this.Q = true;
        r();
        this.e = false;
    }

    @DexIgnore
    public final void u(boolean z2, boolean z3) {
        if (this.e != z2) {
            this.O = z3;
            e();
            this.e = z2;
            if (z2) {
                a(this.t, this.S);
            } else {
                z(this.S);
            }
        }
    }

    @DexIgnore
    public final Animation v(int i2, int i3) {
        d dVar = new d(i2, i3);
        dVar.setDuration(300);
        this.B.b(null);
        this.B.clearAnimation();
        this.B.startAnimation(dVar);
        return dVar;
    }

    @DexIgnore
    public final void w(float f2) {
        float f3 = this.v;
        int i2 = this.f;
        if (f2 - f3 > ((float) i2) && !this.w) {
            this.u = f3 + ((float) i2);
            this.w = true;
            this.I.setAlpha(76);
        }
    }

    @DexIgnore
    public final void x() {
        this.M = v(this.I.getAlpha(), 255);
    }

    @DexIgnore
    public final void y() {
        this.L = v(this.I.getAlpha(), 76);
    }

    @DexIgnore
    public void z(Animation.AnimationListener animationListener) {
        c cVar = new c();
        this.K = cVar;
        cVar.setDuration(150);
        this.B.b(animationListener);
        this.B.clearAnimation();
        this.B.startAnimation(this.K);
    }
}
