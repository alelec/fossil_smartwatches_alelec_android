package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Xh5;
import com.fossil.Yi6;
import com.fossil.Yn7;
import com.fossil.Zi6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.HeartRateOverviewMonthFragment;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewMonthPresenter extends Yi6 {
    @DexIgnore
    public /* final */ MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public List<DailyHeartRateSummary> h; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<H47<List<DailyHeartRateSummary>>> i;
    @DexIgnore
    public /* final */ Zi6 j;
    @DexIgnore
    public /* final */ UserRepository k;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$1$currentUser$1", f = "HeartRateOverviewMonthPresenter.kt", l = {103}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.k;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) g;
            if (mFUser != null) {
                this.this$0.g = TimeUtils.q0(mFUser.getCreatedAt());
                Zi6 zi6 = this.this$0.j;
                Date date = this.this$0.f;
                if (date != null) {
                    Date date2 = this.this$0.g;
                    if (date2 == null) {
                        date2 = new Date();
                    }
                    zi6.g(date, date2);
                    this.this$0.e.l(this.this$0.f);
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$mHeartRateSummaries$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {44, 44}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<DailyHeartRateSummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<DailyHeartRateSummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x004e  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 289
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            this.a = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<DailyHeartRateSummary>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {69}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public /* final */ /* synthetic */ H47 $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$start$1$1$1", f = "HeartRateOverviewMonthPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Calendar $calendar;
                @DexIgnore
                public /* final */ /* synthetic */ TreeMap $map;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Calendar calendar, TreeMap treeMap, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$calendar = calendar;
                    this.$map = treeMap;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$calendar, this.$map, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Integer e;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        for (DailyHeartRateSummary dailyHeartRateSummary : (List) this.this$0.$it.c()) {
                            Calendar calendar = this.$calendar;
                            Wg6.b(calendar, "calendar");
                            calendar.setTime(dailyHeartRateSummary.getDate());
                            this.$calendar.set(14, 0);
                            TreeMap treeMap = this.$map;
                            Calendar calendar2 = this.$calendar;
                            Wg6.b(calendar2, "calendar");
                            Long f = Ao7.f(calendar2.getTimeInMillis());
                            Resting resting = dailyHeartRateSummary.getResting();
                            treeMap.put(f, Ao7.e((resting == null || (e = Ao7.e(resting.getValue())) == null) ? 0 : e.intValue()));
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, H47 h47, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
                this.$it = h47;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, this.$it, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                TreeMap<Long, Integer> treeMap;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    this.this$0.a.h = this.$data;
                    treeMap = new TreeMap<>();
                    Calendar instance = Calendar.getInstance();
                    Dv7 h = this.this$0.a.h();
                    Aiii aiii = new Aiii(this, instance, treeMap, null);
                    this.L$0 = il6;
                    this.L$1 = treeMap;
                    this.L$2 = instance;
                    this.label = 1;
                    if (Eu7.g(h, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Calendar calendar = (Calendar) this.L$2;
                    treeMap = (TreeMap) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.j.e(treeMap);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            this.a = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<DailyHeartRateSummary>> h47) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(h47 != null ? h47.d() : null);
            sb.append(" -- data.size=");
            sb.append((h47 == null || (list = (List) h47.c()) == null) ? null : Integer.valueOf(list.size()));
            local.d("HeartRateOverviewMonthPresenter", sb.toString());
            if ((h47 != null ? h47.d() : null) != Xh5.DATABASE_LOADING) {
                List list2 = h47 != null ? (List) h47.c() : null;
                if (list2 != null && (!Wg6.a(this.a.h, list2))) {
                    Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list2, h47, null), 3, null);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<DailyHeartRateSummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public HeartRateOverviewMonthPresenter(Zi6 zi6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        Wg6.c(zi6, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.j = zi6;
        this.k = userRepository;
        this.l = heartRateSummaryRepository;
        LiveData<H47<List<DailyHeartRateSummary>>> c = Ss0.c(this.e, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026        }\n        }\n    }");
        this.i = c;
    }

    @DexIgnore
    public void A() {
        Date date = this.f;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            this.f = new Date();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
            Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
            return;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.d("HeartRateOverviewMonthPresenter", "loadData - mDateLiveData=" + this.f);
    }

    @DexIgnore
    public void B() {
        this.j.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        A();
        LiveData<H47<List<DailyHeartRateSummary>>> liveData = this.i;
        Zi6 zi6 = this.j;
        if (zi6 != null) {
            liveData.h((HeartRateOverviewMonthFragment) zi6, new Ci(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        MFLogger.d("HeartRateOverviewMonthPresenter", "stop");
        try {
            LiveData<H47<List<DailyHeartRateSummary>>> liveData = this.i;
            Zi6 zi6 = this.j;
            if (zi6 != null) {
                liveData.n((HeartRateOverviewMonthFragment) zi6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
        } catch (Exception e2) {
            MFLogger.d("HeartRateOverviewMonthPresenter", "stop ex " + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Yi6
    public void n(Date date) {
        Wg6.c(date, "date");
        if (this.e.e() == null || !TimeUtils.m0(this.e.e(), date)) {
            this.e.l(date);
        }
    }
}
