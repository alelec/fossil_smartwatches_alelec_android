package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.graphics.Color;
import androidx.lifecycle.MutableLiveData;
import com.fossil.Bw7;
import com.fossil.Fr6;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ts0;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.ThemeRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeActiveCaloriesChartViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String d;
    @DexIgnore
    public static /* final */ String e; // = "#bdbdbd";
    @DexIgnore
    public MutableLiveData<Ai> a; // = new MutableLiveData<>();
    @DexIgnore
    public Ai b; // = new Ai(null, null, 3, null);
    @DexIgnore
    public /* final */ ThemeRepository c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Integer a;
        @DexIgnore
        public Integer b;

        @DexIgnore
        public Ai() {
            this(null, null, 3, null);
        }

        @DexIgnore
        public Ai(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(Integer num, Integer num2, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : num, (i & 2) != 0 ? null : num2);
        }

        @DexIgnore
        public final Integer a() {
            return this.b;
        }

        @DexIgnore
        public final Integer b() {
            return this.a;
        }

        @DexIgnore
        public final void c(Integer num, Integer num2) {
            this.a = num;
            this.b = num2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel$saveColor$1", f = "CustomizeActiveCaloriesChartViewModel.kt", l = {45, 46, 57}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $colorChart;
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeActiveCaloriesChartViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = customizeActiveCaloriesChartViewModel;
            this.$id = str;
            this.$colorChart = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$id, this.$colorChart, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0044  */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ad  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x00d5  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0114  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0117  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
            // Method dump skipped, instructions count: 283
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = CustomizeActiveCaloriesChartViewModel.class.getSimpleName();
        Wg6.b(simpleName, "CustomizeActiveCaloriesC\u2026el::class.java.simpleName");
        d = simpleName;
    }
    */

    @DexIgnore
    public CustomizeActiveCaloriesChartViewModel(ThemeRepository themeRepository) {
        Wg6.c(themeRepository, "mThemesRepository");
        this.c = themeRepository;
    }

    @DexIgnore
    public final void d() {
        this.a.l(this.b);
    }

    @DexIgnore
    public final MutableLiveData<Ai> e() {
        return this.a;
    }

    @DexIgnore
    public final void f(String str, String str2) {
        Wg6.c(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = d;
        local.d(str3, "saveColor colorChart=" + str2);
        Rm6 unused = Gu7.d(Jv7.a(Bw7.b()), null, null, new Bi(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final void g() {
        String a2 = Fr6.m.a();
        int parseColor = a2 != null ? Color.parseColor(a2) : Color.parseColor(e);
        this.b.c(Integer.valueOf(parseColor), Integer.valueOf(parseColor));
        d();
    }

    @DexIgnore
    public final void h(int i, int i2) {
        if (i == 503) {
            this.b.c(Integer.valueOf(i2), Integer.valueOf(i2));
            d();
        }
    }
}
