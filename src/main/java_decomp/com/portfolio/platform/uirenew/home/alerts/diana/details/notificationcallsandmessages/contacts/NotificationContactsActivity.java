package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.K16;
import com.fossil.L16;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationContactsPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment) {
            Wg6.c(fragment, "fragment");
            Intent intent = new Intent(fragment.getContext(), NotificationContactsActivity.class);
            intent.setFlags(536870912);
            fragment.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
        super.onActivityResult(i, i2, intent);
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        K16 k16 = (K16) getSupportFragmentManager().Y(2131362158);
        if (k16 == null) {
            k16 = K16.l.b();
            k(k16, K16.l.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (k16 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            Wg6.b(supportLoaderManager, "supportLoaderManager");
            iface.m1(new L16(k16, supportLoaderManager)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsContract.View");
    }
}
