package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.At0;
import com.fossil.B78;
import com.fossil.El7;
import com.fossil.G46;
import com.fossil.Gu7;
import com.fossil.H46;
import com.fossil.H68;
import com.fossil.I46;
import com.fossil.J06;
import com.fossil.K66;
import com.fossil.Ko7;
import com.fossil.L56;
import com.fossil.Mm7;
import com.fossil.Pm7;
import com.fossil.Tq4;
import com.fossil.Uh5;
import com.fossil.Uq4;
import com.fossil.Y56;
import com.fossil.Yn7;
import com.fossil.Zs0;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.AppWrapper;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.SetNotificationFiltersUserCase;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.service.BleCommandResultManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationContactsAndAppsAssignedPresenter extends G46 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String v;
    @DexIgnore
    public static /* final */ Ai w; // = new Ai(null);
    @DexIgnore
    public /* final */ List<Object> f; // = new ArrayList();
    @DexIgnore
    public List<Object> g; // = new ArrayList();
    @DexIgnore
    public boolean h; // = true;
    @DexIgnore
    public /* final */ List<J06> i; // = new ArrayList();
    @DexIgnore
    public /* final */ List<AppWrapper> j; // = new ArrayList();
    @DexIgnore
    public /* final */ List<J06> k; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> l; // = new ArrayList();
    @DexIgnore
    public /* final */ List<Integer> m; // = new ArrayList();
    @DexIgnore
    public /* final */ LoaderManager n;
    @DexIgnore
    public /* final */ H46 o;
    @DexIgnore
    public /* final */ int p;
    @DexIgnore
    public /* final */ Uq4 q;
    @DexIgnore
    public /* final */ Y56 r;
    @DexIgnore
    public /* final */ L56 s;
    @DexIgnore
    public /* final */ K66 t;
    @DexIgnore
    public /* final */ SetNotificationFiltersUserCase u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationContactsAndAppsAssignedPresenter.v;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Tq4.Di<L56.Ai, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            this.a = notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            Wg6.c(ai, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onError");
            this.a.o.k();
        }

        @DexIgnore
        public void c(L56.Ai ai) {
            Wg6.c(ai, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onSuccess");
            ArrayList arrayList = new ArrayList();
            for (AppWrapper appWrapper : ai.a()) {
                InstalledApp installedApp = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected == null) {
                    Wg6.i();
                    throw null;
                } else if (isSelected.booleanValue() && appWrapper.getCurrentHandGroup() == this.a.p) {
                    arrayList.add(appWrapper);
                }
            }
            this.a.I().addAll(arrayList);
            this.a.H().addAll(arrayList);
            List<Object> J = this.a.J();
            Object[] array = arrayList.toArray(new AppWrapper[0]);
            if (array != null) {
                Serializable a2 = H68.a((Serializable) array);
                Wg6.b(a2, "SerializationUtils.clone\u2026sSelected.toTypedArray())");
                Mm7.t(J, (Object[]) a2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = NotificationContactsAndAppsAssignedPresenter.w.a();
                local.d(a3, "mContactAndAppDataFirstLoad.size=" + this.a.J().size());
                this.a.n.d(1, new Bundle(), this.a);
                this.a.o.F0(this.a.I());
                this.a.o.k();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(L56.Ai ai) {
            c(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Di<Y56.Ci, Y56.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {126}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Y56.Ci $responseValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter$loadContactData$1$onSuccess$1$populateContact$1", f = "NotificationContactsAndAppsAssignedPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $contactWrapperList;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, List list, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                    this.$contactWrapperList = list;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$contactWrapperList, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Contact contact;
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        for (ContactGroup contactGroup : this.this$0.$responseValue.a()) {
                            for (Contact contact2 : contactGroup.getContacts()) {
                                if (contactGroup.getHour() == this.this$0.this$0.a.p) {
                                    J06 j06 = new J06(contact2, "");
                                    j06.setAdded(true);
                                    Wg6.b(contact2, "contact");
                                    ContactGroup contactGroup2 = contact2.getContactGroup();
                                    Wg6.b(contactGroup2, "contact.contactGroup");
                                    j06.setCurrentHandGroup(contactGroup2.getHour());
                                    Contact contact3 = j06.getContact();
                                    if (contact3 != null) {
                                        contact3.setDbRowId(contact2.getDbRowId());
                                        contact3.setUseSms(contact2.isUseSms());
                                        contact3.setUseCall(contact2.isUseCall());
                                    }
                                    List<PhoneNumber> phoneNumbers = contact2.getPhoneNumbers();
                                    Wg6.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact2.getPhoneNumbers().get(0);
                                        Wg6.b(phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            j06.setHasPhoneNumber(true);
                                            j06.setPhoneNumber(number);
                                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                            String a2 = NotificationContactsAndAppsAssignedPresenter.w.a();
                                            local.d(a2, "mGetAllHybridContactGroups filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String a3 = NotificationContactsAndAppsAssignedPresenter.w.a();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("mGetAllHybridContactGroups filter selected contact, hand=");
                                    ContactGroup contactGroup3 = contact2.getContactGroup();
                                    Wg6.b(contactGroup3, "contact.contactGroup");
                                    sb.append(contactGroup3.getHour());
                                    sb.append(" ,rowId=");
                                    sb.append(contact2.getDbRowId());
                                    sb.append(" ,isUseText=");
                                    sb.append(contact2.isUseSms());
                                    sb.append(" ,isUseCall=");
                                    sb.append(contact2.isUseCall());
                                    local2.d(a3, sb.toString());
                                    this.$contactWrapperList.add(j06);
                                    Contact contact4 = j06.getContact();
                                    if ((contact4 != null && contact4.getContactId() == -100) || ((contact = j06.getContact()) != null && contact.getContactId() == -200)) {
                                        this.this$0.this$0.a.L().add(j06);
                                    }
                                }
                            }
                        }
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Y56.Ci ci2, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$responseValue = ci2;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$responseValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                List list;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetAllHybridContactGroups onSuccess");
                    ArrayList arrayList = new ArrayList();
                    Rl6 rl6 = Gu7.b(il6, this.this$0.a.h(), null, new Aiii(this, arrayList, null), 2, null);
                    this.L$0 = il6;
                    this.L$1 = arrayList;
                    this.L$2 = rl6;
                    this.label = 1;
                    if (rl6.l(this) == d) {
                        return d;
                    }
                    list = arrayList;
                } else if (i == 1) {
                    Rl6 rl62 = (Rl6) this.L$2;
                    list = (List) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.I().addAll(list);
                this.this$0.a.K().addAll(list);
                List<Object> J = this.this$0.a.J();
                Object[] array = list.toArray(new J06[0]);
                if (array != null) {
                    Serializable a2 = H68.a((Serializable) array);
                    Wg6.b(a2, "SerializationUtils.clone\u2026apperList.toTypedArray())");
                    Mm7.t(J, (Object[]) a2);
                    this.this$0.a.N();
                    return Cd6.a;
                }
                throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter) {
            this.a = notificationContactsAndAppsAssignedPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Y56.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Y56.Ai ai) {
            Wg6.c(ai, "errorValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetAllHybridContactGroups onError");
            this.a.o.k();
        }

        @DexIgnore
        public void c(Y56.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, ci, null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(Y56.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements Tq4.Di<L56.Ai, Tq4.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;

        @DexIgnore
        public Di(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, ArrayList arrayList, List list) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = arrayList;
            this.c = list;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(Tq4.Ai ai) {
            Wg6.c(ai, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetApps onError");
            this.a.o.k();
        }

        @DexIgnore
        public void c(L56.Ai ai) {
            Wg6.c(ai, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "mGetHybridApp onSuccess");
            ArrayList arrayList = new ArrayList();
            for (String str : this.b) {
                Iterator<T> it = ai.a().iterator();
                while (true) {
                    if (it.hasNext()) {
                        T next = it.next();
                        if (Wg6.a(String.valueOf(next.getUri()), str)) {
                            InstalledApp installedApp = next.getInstalledApp();
                            if (installedApp != null) {
                                installedApp.setSelected(true);
                            }
                            next.setCurrentHandGroup(this.a.p);
                            arrayList.add(next);
                            List list = this.c;
                            Uri uri = next.getUri();
                            if (uri != null) {
                                list.add(uri);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    }
                }
            }
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "queryAppsString: appsSelected=" + arrayList);
            this.a.I().addAll(arrayList);
            NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter = this.a;
            notificationContactsAndAppsAssignedPresenter.w(notificationContactsAndAppsAssignedPresenter.M());
            this.a.o.B3(this.a.q());
            int size = this.a.H().size();
            for (int i = 0; i < size; i++) {
                if (!Pm7.B(this.c, this.a.H().get(i).getUri())) {
                    AppWrapper appWrapper = this.a.H().get(i);
                    InstalledApp installedApp2 = appWrapper.getInstalledApp();
                    if (installedApp2 != null) {
                        installedApp2.setSelected(false);
                        this.a.I().add(appWrapper);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
            }
            this.a.o.F0(this.a.I());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(L56.Ai ai) {
            c(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Ei<SetNotificationFiltersUserCase.Ci, SetNotificationFiltersUserCase.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ List c;
        @DexIgnore
        public /* final */ /* synthetic */ boolean d;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements Tq4.Di<K66.Bi, Tq4.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ei a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Aii(Ei ei) {
                this.a = ei;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void a(Tq4.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(Tq4.Ai ai) {
                Wg6.c(ai, "errorResponse");
                FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), ".Inside mSaveAllNotification onError");
                this.a.a.o.k();
            }

            @DexIgnore
            public void c(K66.Bi bi) {
                Wg6.c(bi, "successResponse");
                FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), ".Inside mSaveAllNotification onSuccess");
                this.a.a.o.k();
                Ei ei = this.a;
                if (ei.d) {
                    ei.a.o.close();
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void onSuccess(K66.Bi bi) {
                c(bi);
            }
        }

        @DexIgnore
        public Ei(NotificationContactsAndAppsAssignedPresenter notificationContactsAndAppsAssignedPresenter, List list, List list2, boolean z) {
            this.a = notificationContactsAndAppsAssignedPresenter;
            this.b = list;
            this.c = list2;
            this.d = z;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(SetNotificationFiltersUserCase.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(SetNotificationFiltersUserCase.Bi bi) {
            Wg6.c(bi, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationContactsAndAppsAssignedPresenter.w.a();
            local.d(a2, "saveNotification fail!! errorValue=" + bi.a());
            this.a.o.k();
            if (bi.c() != 1101) {
                this.a.o.u();
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "Bluetooth is disabled");
            List<Uh5> convertBLEPermissionErrorCode = Uh5.convertBLEPermissionErrorCode(bi.b());
            Wg6.b(convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            H46 h46 = this.a.o;
            Object[] array = convertBLEPermissionErrorCode.toArray(new Uh5[0]);
            if (array != null) {
                Uh5[] uh5Arr = (Uh5[]) array;
                h46.M((Uh5[]) Arrays.copyOf(uh5Arr, uh5Arr.length));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public void c(SetNotificationFiltersUserCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), "saveNotification success!!");
            this.a.q.a(this.a.t, new K66.Ai(this.b, this.c), new Aii(this));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(SetNotificationFiltersUserCase.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = NotificationContactsAndAppsAssignedPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationContactsAndA\u2026er::class.java.simpleName");
        v = simpleName;
    }
    */

    @DexIgnore
    public NotificationContactsAndAppsAssignedPresenter(LoaderManager loaderManager, H46 h46, int i2, Uq4 uq4, Y56 y56, L56 l56, K66 k66, SetNotificationFiltersUserCase setNotificationFiltersUserCase) {
        Wg6.c(loaderManager, "mLoaderManager");
        Wg6.c(h46, "mView");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(y56, "mGetAllHybridContactGroups");
        Wg6.c(l56, "mGetHybridApp");
        Wg6.c(k66, "mSaveAllHybridNotification");
        Wg6.c(setNotificationFiltersUserCase, "mSetNotificationFiltersUserCase");
        this.n = loaderManager;
        this.o = h46;
        this.p = i2;
        this.q = uq4;
        this.r = y56;
        this.s = l56;
        this.t = k66;
        this.u = setNotificationFiltersUserCase;
    }

    @DexIgnore
    public final List<AppWrapper> H() {
        return this.j;
    }

    @DexIgnore
    public final List<Object> I() {
        return this.f;
    }

    @DexIgnore
    public final List<Object> J() {
        return this.g;
    }

    @DexIgnore
    public final List<J06> K() {
        return this.i;
    }

    @DexIgnore
    public final List<J06> L() {
        return this.k;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:129:0x0065 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x012e A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean M() {
        /*
        // Method dump skipped, instructions count: 366
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter.M():boolean");
    }

    @DexIgnore
    public final void N() {
        this.q.a(this.s, null, new Bi(this));
    }

    @DexIgnore
    public final void O() {
        this.o.m();
        this.q.a(this.r, null, new Ci(this));
    }

    @DexIgnore
    public void P(At0<Cursor> at0, Cursor cursor) {
        Wg6.c(at0, "loader");
        if (cursor != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = v;
            local.d(str, "onLoadFinished, loader id = " + at0.getId() + "cursor is closed = " + cursor.isClosed());
            if (cursor.isClosed()) {
                this.n.f(at0.getId(), new Bundle(), this);
                return;
            }
            if (cursor.moveToFirst()) {
                do {
                    String string = cursor.getString(cursor.getColumnIndex("display_name"));
                    String string2 = cursor.getString(cursor.getColumnIndex("photo_thumb_uri"));
                    int i2 = cursor.getInt(cursor.getColumnIndex("contact_id"));
                    this.l.add(Integer.valueOf(i2));
                    T(i2, string, string2);
                } while (cursor.moveToNext());
            }
            Q();
            this.o.F0(this.f);
            cursor.close();
        }
    }

    @DexIgnore
    public final void Q() {
        if (!this.m.isEmpty()) {
            Collection a2 = B78.a(this.m, this.l);
            Wg6.b(a2, "CollectionUtils.subtract\u2026actIds, mPhoneContactIds)");
            List h0 = Pm7.h0(a2);
            if (!h0.isEmpty()) {
                List<J06> arrayList = new ArrayList<>();
                for (int size = this.f.size() - 1; size >= 0; size--) {
                    Object obj = this.f.get(size);
                    if (obj instanceof J06) {
                        J06 j06 = (J06) obj;
                        Contact contact = j06.getContact();
                        Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
                        int size2 = h0.size();
                        int i2 = 0;
                        while (true) {
                            if (i2 >= size2) {
                                break;
                            } else if (Wg6.a((Integer) h0.get(i2), valueOf)) {
                                j06.setAdded(false);
                                arrayList.add(obj);
                                this.f.remove(size);
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.o.m();
                    R(arrayList, new ArrayList<>(), false);
                }
            }
        }
    }

    @DexIgnore
    public final void R(List<J06> list, List<AppWrapper> list2, boolean z) {
        this.u.e(new SetNotificationFiltersUserCase.Ai(list, list2, this.p), new Ei(this, list, list2, z));
    }

    @DexIgnore
    public void S() {
        this.o.M5(this);
    }

    @DexIgnore
    public final void T(int i2, String str, String str2) {
        J06 j06;
        Contact contact;
        for (Object obj : this.f) {
            if ((obj instanceof J06) && (contact = (j06 = (J06) obj).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = j06.getContact();
                if (contact2 != null) {
                    contact2.setFirstName(str);
                }
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setPhotoThumbUri(str2);
                    return;
                }
                return;
            }
        }
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.At0, java.lang.Object] */
    @Override // androidx.loader.app.LoaderManager.a
    public /* bridge */ /* synthetic */ void a(At0<Cursor> at0, Cursor cursor) {
        P(at0, cursor);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public At0<Cursor> d(int i2, Bundle bundle) {
        boolean z;
        this.m.clear();
        this.l.clear();
        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder("has_phone_number != 0 AND mimetype =? ");
        ArrayList arrayList = new ArrayList();
        arrayList.add("vnd.android.cursor.item/phone_v2");
        if (!this.f.isEmpty()) {
            z = false;
            for (T t2 : this.f) {
                if (t2 instanceof J06) {
                    T t3 = t2;
                    if (t3.getContact() == null) {
                        continue;
                    } else {
                        Contact contact = t3.getContact();
                        if (contact == null) {
                            Wg6.i();
                            throw null;
                        } else if (contact.getContactId() >= 0) {
                            if (!z) {
                                sb2.append("AND contact_id IN (");
                                z = true;
                            }
                            sb2.append("?, ");
                            Contact contact2 = t3.getContact();
                            Integer valueOf = contact2 != null ? Integer.valueOf(contact2.getContactId()) : null;
                            if (valueOf != null) {
                                this.m.add(valueOf);
                            }
                            arrayList.add(String.valueOf(valueOf));
                        }
                    }
                }
            }
        } else {
            z = false;
        }
        if (z) {
            sb.append((CharSequence) new StringBuilder(sb2.substring(0, sb2.length() - 2)));
            sb.append(")");
        } else {
            sb.append((CharSequence) sb2);
        }
        FLogger.INSTANCE.getLocal().d(v, ".Inside onCreateLoader, selectionQuery = " + ((Object) sb));
        PortfolioApp instance = PortfolioApp.get.instance();
        Uri uri = ContactsContract.Data.CONTENT_URI;
        String sb3 = sb.toString();
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            return new Zs0(instance, uri, new String[]{"contact_id", "display_name", "photo_thumb_uri"}, sb3, (String[]) array, null);
        }
        throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(At0<Cursor> at0) {
        Wg6.c(at0, "loader");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(v, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        this.u.q();
        BleCommandResultManager.d.g(CommunicateMode.SET_AUTO_NOTIFICATION_FILTERS);
        H46 h46 = this.o;
        if (h46 == null) {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedFragment");
        } else if (((I46) h46).getContext() != null) {
            this.o.w5(this.p);
            if (!this.f.isEmpty() || !this.h) {
                Wg6.b(this.n.d(1, new Bundle(), this), "mLoaderManager.initLoade\u2026AndAppsAssignedPresenter)");
                return;
            }
            this.h = false;
            O();
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(v, "stop");
        this.u.q();
        this.n.a(1);
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void n(ArrayList<J06> arrayList) {
        Wg6.c(arrayList, "contactWrappersSelected");
        FLogger.INSTANCE.getLocal().d(v, "addContactWrapperList: contactWrappersSelected = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof J06) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact = t2.getContact();
            if (contact != null) {
                arrayList2.add(Integer.valueOf(contact.getContactId()));
            } else {
                Wg6.i();
                throw null;
            }
        }
        w(M());
        this.o.B3(q());
        int size2 = this.i.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact2 = this.i.get(i2).getContact();
            if (!Pm7.B(arrayList2, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                J06 j06 = this.i.get(i2);
                j06.setAdded(false);
                this.f.add(j06);
            }
        }
        this.o.F0(this.f);
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void o(ArrayList<J06> arrayList) {
        J06 j06;
        Contact contact;
        Contact contact2;
        Wg6.c(arrayList, "contactWrappersSelected");
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof J06) && (((contact = (j06 = (J06) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = j06.getContact()) != null && contact2.getContactId() == -200))) {
                    this.f.remove(size);
                }
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (T t2 : arrayList) {
            this.f.add(t2);
            Contact contact3 = t2.getContact();
            if (contact3 != null) {
                arrayList2.add(Integer.valueOf(contact3.getContactId()));
            } else {
                Wg6.i();
                throw null;
            }
        }
        w(M());
        this.o.B3(q());
        int size2 = this.k.size();
        for (int i2 = 0; i2 < size2; i2++) {
            Contact contact4 = this.k.get(i2).getContact();
            if (!Pm7.B(arrayList2, contact4 != null ? Integer.valueOf(contact4.getContactId()) : null)) {
                J06 j062 = this.k.get(i2);
                j062.setAdded(false);
                this.f.add(j062);
            }
        }
        this.o.F0(this.f);
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void p() {
        w(M());
        this.o.B3(q());
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void r() {
        ArrayList<String> arrayList = new ArrayList<>();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if (obj instanceof AppWrapper) {
                    AppWrapper appWrapper = (AppWrapper) obj;
                    InstalledApp installedApp = appWrapper.getInstalledApp();
                    Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                    if (isSelected == null) {
                        Wg6.i();
                        throw null;
                    } else if (isSelected.booleanValue()) {
                        Uri uri = appWrapper.getUri();
                        if (uri != null) {
                            arrayList.add(uri.toString());
                        }
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.N4(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.H46 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.G46
    public void s() {
        J06 j06;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof J06) && ((contact = (j06 = (J06) obj).getContact()) == null || contact.getContactId() != -100 || (contact2 = j06.getContact()) == null || contact2.getContactId() != -200)) {
                    if (j06.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.m6(this.p, arrayList);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for r0v3, resolved type: com.fossil.H46 */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // com.fossil.G46
    public void t() {
        J06 j06;
        Contact contact;
        Contact contact2;
        ArrayList arrayList = new ArrayList();
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                Object obj = this.f.get(size);
                if ((obj instanceof J06) && (((contact = (j06 = (J06) obj).getContact()) != null && contact.getContactId() == -100) || ((contact2 = j06.getContact()) != null && contact2.getContactId() == -200))) {
                    if (j06.isAdded()) {
                        arrayList.add(obj);
                    } else {
                        this.f.remove(size);
                    }
                }
            }
        }
        this.o.X1(this.p, arrayList);
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void u(ArrayList<String> arrayList) {
        Wg6.c(arrayList, "stringAppsSelected");
        FLogger.INSTANCE.getLocal().d(v, "queryAppsString = " + arrayList);
        if (!this.f.isEmpty()) {
            for (int size = this.f.size() - 1; size >= 0; size--) {
                if (this.f.get(size) instanceof AppWrapper) {
                    this.f.remove(size);
                }
            }
        }
        this.q.a(this.s, null, new Di(this, arrayList, new ArrayList()));
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void v() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "mContactAndAppData.size=" + this.f.size() + " mContactAndAppDataFirstLoad.size=" + this.g.size());
        if (q()) {
            this.o.m();
            List<J06> arrayList = new ArrayList<>();
            List<AppWrapper> arrayList2 = new ArrayList<>();
            for (J06 j06 : this.f) {
                if (j06 instanceof J06) {
                    arrayList.add(j06);
                } else if (j06 != null) {
                    arrayList2.add((AppWrapper) j06);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                }
            }
            R(arrayList, arrayList2, true);
        }
    }

    @DexIgnore
    @Override // com.fossil.G46
    public void x(int i2, boolean z, boolean z2) {
        J06 j06;
        Contact contact;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "updateContactWrapper: contactId=" + i2 + ", useCall=" + z + ", useText=" + z2);
        Iterator<Object> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            if ((next instanceof J06) && (contact = (j06 = (J06) next).getContact()) != null && contact.getContactId() == i2) {
                Contact contact2 = j06.getContact();
                if (contact2 != null) {
                    contact2.setUseCall(z);
                }
                Contact contact3 = j06.getContact();
                if (contact3 != null) {
                    contact3.setUseSms(z2);
                }
            }
        }
        w(M());
        this.o.B3(q());
        this.o.F0(this.f);
    }
}
