package com.portfolio.platform.uirenew.home.customize.diana.complications.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.C96;
import com.fossil.D96;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ComplicationSearchPresenter extends C96 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h; // = "empty";
    @DexIgnore
    public String i;
    @DexIgnore
    public /* final */ ArrayList<Complication> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ ArrayList<Complication> k; // = new ArrayList<>();
    @DexIgnore
    public /* final */ D96 l;
    @DexIgnore
    public /* final */ ComplicationRepository m;
    @DexIgnore
    public /* final */ An4 n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public /* final */ /* synthetic */ Jh6 $results;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationSearchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$search$1$1", f = "ComplicationSearchPresenter.kt", l = {75}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Complication>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Complication>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object queryComplicationByName;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    ComplicationRepository complicationRepository = this.this$0.this$0.m;
                    String str = this.this$0.$query;
                    this.L$0 = il6;
                    this.label = 1;
                    queryComplicationByName = complicationRepository.queryComplicationByName(str, this);
                    if (queryComplicationByName == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    queryComplicationByName = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Pm7.j0((Collection) queryComplicationByName);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(ComplicationSearchPresenter complicationSearchPresenter, String str, Jh6 jh6, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationSearchPresenter;
            this.$query = str;
            this.$results = jh6;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$query, this.$results, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Jh6 jh6;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$query.length() > 0) {
                    Jh6 jh62 = this.$results;
                    Dv7 i2 = this.this$0.i();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = jh62;
                    this.label = 1;
                    g = Eu7.g(i2, aii, this);
                    if (g == d) {
                        return d;
                    }
                    jh6 = jh62;
                }
                this.this$0.y().B(this.this$0.C(this.$results.element));
                this.this$0.i = this.$query;
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                jh6 = (Jh6) this.L$1;
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            jh6.element = (T) ((List) g);
            this.this$0.y().B(this.this$0.C(this.$results.element));
            this.this$0.i = this.$query;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1", f = "ComplicationSearchPresenter.kt", l = {41}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ComplicationSearchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$start$1$1", f = "ComplicationSearchPresenter.kt", l = {42, 43}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x006f  */
            /* JADX WARNING: Removed duplicated region for block: B:15:0x0094  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r6 = 2
                    r3 = 1
                    java.lang.Object r4 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0071
                    if (r0 == r3) goto L_0x002f
                    if (r0 != r6) goto L_0x0027
                    java.lang.Object r0 = r7.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r7.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r8)
                    r1 = r8
                    r2 = r0
                L_0x001b:
                    r0 = r1
                    java.util.Collection r0 = (java.util.Collection) r0
                    boolean r0 = r2.addAll(r0)
                    java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
                L_0x0026:
                    return r0
                L_0x0027:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x002f:
                    java.lang.Object r0 = r7.L$1
                    java.util.ArrayList r0 = (java.util.ArrayList) r0
                    java.lang.Object r1 = r7.L$0
                    com.mapped.Il6 r1 = (com.mapped.Il6) r1
                    com.fossil.El7.b(r8)
                    r3 = r0
                    r2 = r8
                L_0x003c:
                    r0 = r2
                    java.util.Collection r0 = (java.util.Collection) r0
                    r3.addAll(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$Bi r0 = r7.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.t(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$Bi r2 = r7.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.s(r2)
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$Bi r3 = r7.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter r3 = r3.this$0
                    com.mapped.An4 r3 = com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.u(r3)
                    java.util.List r3 = r3.l()
                    java.lang.String r5 = "sharedPreferencesManager\u2026licationSearchedIdsRecent"
                    com.mapped.Wg6.b(r3, r5)
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r6
                    java.lang.Object r1 = r2.getComplicationByIds(r3, r7)
                    if (r1 != r4) goto L_0x0094
                    r0 = r4
                    goto L_0x0026
                L_0x0071:
                    com.fossil.El7.b(r8)
                    com.mapped.Il6 r1 = r7.p$
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$Bi r0 = r7.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter r0 = r0.this$0
                    java.util.ArrayList r0 = com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.r(r0)
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter$Bi r2 = r7.this$0
                    com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.ComplicationRepository r2 = com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.s(r2)
                    r7.L$0 = r1
                    r7.L$1 = r0
                    r7.label = r3
                    java.lang.Object r2 = r2.getAllComplicationRaw(r7)
                    if (r2 != r4) goto L_0x0096
                    r0 = r4
                    goto L_0x0026
                L_0x0094:
                    r2 = r0
                    goto L_0x001b
                L_0x0096:
                    r3 = r0
                    goto L_0x003c
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.complications.search.ComplicationSearchPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ComplicationSearchPresenter complicationSearchPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = complicationSearchPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.z();
            return Cd6.a;
        }
    }

    @DexIgnore
    public ComplicationSearchPresenter(D96 d96, ComplicationRepository complicationRepository, An4 an4) {
        Wg6.c(d96, "mView");
        Wg6.c(complicationRepository, "mComplicationRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        this.l = d96;
        this.m = complicationRepository;
        this.n = an4;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("bottom", this.f);
            bundle.putString(ViewHierarchy.DIMENSION_LEFT_KEY, this.g);
            bundle.putString("right", this.h);
        }
        return bundle;
    }

    @DexIgnore
    public void B() {
        this.l.M5(this);
    }

    @DexIgnore
    public final List<Lc6<Complication, String>> C(List<Complication> list) {
        ArrayList arrayList = new ArrayList();
        for (Complication complication : list) {
            String complicationId = complication.getComplicationId();
            if (Wg6.a(complicationId, this.e)) {
                arrayList.add(new Lc6(complication, ViewHierarchy.DIMENSION_TOP_KEY));
            } else if (Wg6.a(complicationId, this.f)) {
                arrayList.add(new Lc6(complication, "bottom"));
            } else if (Wg6.a(complicationId, this.h)) {
                arrayList.add(new Lc6(complication, "right"));
            } else if (Wg6.a(complicationId, this.g)) {
                arrayList.add(new Lc6(complication, ViewHierarchy.DIMENSION_LEFT_KEY));
            } else {
                arrayList.add(new Lc6(complication, ""));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final void D(String str, String str2, String str3, String str4) {
        Wg6.c(str, "topComplication");
        Wg6.c(str2, "bottomComplication");
        Wg6.c(str3, "rightComplication");
        Wg6.c(str4, "leftComplication");
        this.e = str;
        this.f = str2;
        this.h = str3;
        this.g = str4;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.C96
    public void n() {
        this.i = "";
        this.l.D();
        z();
    }

    @DexIgnore
    @Override // com.fossil.C96
    public void o(Complication complication) {
        Wg6.c(complication, "selectedComplication");
        List<String> l2 = this.n.l();
        Wg6.b(l2, "sharedPreferencesManager\u2026licationSearchedIdsRecent");
        if (!l2.contains(complication.getComplicationId())) {
            l2.add(0, complication.getComplicationId());
            if (l2.size() > 5) {
                l2 = l2.subList(0, 5);
            }
            this.n.R0(l2);
        }
        this.l.b3(complication);
    }

    @DexIgnore
    @Override // com.fossil.C96
    public void p(String str) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Jh6 jh6 = new Jh6();
        jh6.element = (T) new ArrayList();
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, str, jh6, null), 3, null);
    }

    @DexIgnore
    public final D96 y() {
        return this.l;
    }

    @DexIgnore
    public final void z() {
        if (this.k.isEmpty()) {
            this.l.B(C(Pm7.j0(this.j)));
        } else {
            this.l.K(C(Pm7.j0(this.k)));
        }
        if (!TextUtils.isEmpty(this.i)) {
            D96 d96 = this.l;
            String str = this.i;
            if (str != null) {
                d96.z(str);
                String str2 = this.i;
                if (str2 != null) {
                    p(str2);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }
}
