package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.text.SpannableString;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Ll5;
import com.fossil.Ls0;
import com.fossil.X26;
import com.fossil.Y26;
import com.fossil.Yn7;
import com.fossil.Z26;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationWatchRemindersPresenter extends X26 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public /* final */ LiveData<List<InactivityNudgeTimeModel>> m;
    @DexIgnore
    public /* final */ LiveData<RemindTimeModel> n; // = this.q.getRemindTimeDao().getRemindTime();
    @DexIgnore
    public /* final */ Y26 o;
    @DexIgnore
    public /* final */ An4 p;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1", f = "NotificationWatchRemindersPresenter.kt", l = {151, 160}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends InactivityNudgeTimeModel>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends InactivityNudgeTimeModel>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.q.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$remindMinutes$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super RemindTimeModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super RemindTimeModel> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.q.getRemindTimeDao().getRemindTimeModel();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x00de  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0151  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0154  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 352
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {60}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $lInActivityNudgeTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$1$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.a.q.getInactivityNudgeTimeDao().upsertListInactivityNudgeTime(this.this$0.$lInActivityNudgeTimeModel);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$lInActivityNudgeTimeModel = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$lInActivityNudgeTimeModel, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 i2 = this.this$0.a.i();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(i2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Bi(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(List<InactivityNudgeTimeModel> list) {
            if (list == null || list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                InactivityNudgeTimeModel inactivityNudgeTimeModel = new InactivityNudgeTimeModel("Start", 660, 0);
                InactivityNudgeTimeModel inactivityNudgeTimeModel2 = new InactivityNudgeTimeModel("End", 1260, 1);
                arrayList.add(inactivityNudgeTimeModel);
                arrayList.add(inactivityNudgeTimeModel2);
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, arrayList, null), 3, null);
                return;
            }
            for (T t : list) {
                if (t.getNudgeTimeType() == 0) {
                    Y26 y26 = this.a.o;
                    SpannableString h = Ll5.h(t.getMinutes());
                    Wg6.b(h, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    y26.t5(h);
                } else {
                    Y26 y262 = this.a.o;
                    SpannableString h2 = Ll5.h(t.getMinutes());
                    Wg6.b(h2, "TimeUtils.getTimeSpannab\u2026tyNudgeTimeModel.minutes)");
                    y262.Y5(h2);
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends InactivityNudgeTimeModel> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1", f = "NotificationWatchRemindersPresenter.kt", l = {79}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ RemindTimeModel $tempRemindTimeModel;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$start$2$1$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.a.q.getRemindTimeDao().upsertRemindTimeModel(this.this$0.$tempRemindTimeModel);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, RemindTimeModel remindTimeModel, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$tempRemindTimeModel = remindTimeModel;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$tempRemindTimeModel, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 i2 = this.this$0.a.i();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(i2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(RemindTimeModel remindTimeModel) {
            if (remindTimeModel == null) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, new RemindTimeModel("RemindTime", 20), null), 3, null);
                return;
            }
            Y26 y26 = this.a.o;
            String g = Ll5.g(remindTimeModel.getMinutes());
            Wg6.b(g, "TimeUtils.getRemindTimeS\u2026(remindTimeModel.minutes)");
            y26.b6(g);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(RemindTimeModel remindTimeModel) {
            a(remindTimeModel);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di<T> implements Ls0<List<? extends InactivityNudgeTimeModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public Di(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(List<InactivityNudgeTimeModel> list) {
            Y26 unused = this.a.o;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(List<? extends InactivityNudgeTimeModel> list) {
            a(list);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei<T> implements Ls0<RemindTimeModel> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationWatchRemindersPresenter a;

        @DexIgnore
        public Ei(NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
            this.a = notificationWatchRemindersPresenter;
        }

        @DexIgnore
        public final void a(RemindTimeModel remindTimeModel) {
            Y26 unused = this.a.o;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(RemindTimeModel remindTimeModel) {
            a(remindTimeModel);
        }
    }

    /*
    static {
        String simpleName = NotificationWatchRemindersPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationWatchReminde\u2026er::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public NotificationWatchRemindersPresenter(Y26 y26, An4 an4, RemindersSettingsDatabase remindersSettingsDatabase) {
        Wg6.c(y26, "mView");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.o = y26;
        this.p = an4;
        this.q = remindersSettingsDatabase;
        this.m = remindersSettingsDatabase.getInactivityNudgeTimeDao().getListInactivityNudgeTime();
        boolean u0 = this.p.u0();
        this.e = u0;
        this.i = u0;
    }

    @DexIgnore
    public final boolean B() {
        return (this.i == this.e && this.j == this.f && this.k == this.g && this.l == this.h) ? false : true;
    }

    @DexIgnore
    public void C() {
        this.o.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(r, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        LiveData<List<InactivityNudgeTimeModel>> liveData = this.m;
        Y26 y26 = this.o;
        if (y26 != null) {
            liveData.h((Z26) y26, new Bi(this));
            this.n.h((LifecycleOwner) this.o, new Ci(this));
            boolean v0 = this.p.v0();
            this.f = v0;
            this.j = v0;
            boolean w0 = this.p.w0();
            this.g = w0;
            this.k = w0;
            boolean t0 = this.p.t0();
            this.h = t0;
            this.l = t0;
            this.o.w4(this.i);
            this.o.S1(this.j);
            this.o.n4(this.k);
            this.o.s1(this.l);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        this.m.m(new Di(this));
        this.n.m(new Ei(this));
    }

    @DexIgnore
    @Override // com.fossil.X26
    public void n() {
        this.l = !this.l;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.X26
    public void o() {
        this.i = !this.i;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
        this.o.w4(this.i);
    }

    @DexIgnore
    @Override // com.fossil.X26
    public void p() {
        this.j = !this.j;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.X26
    public void q() {
        this.k = !this.k;
        if (B()) {
            this.o.Y(true);
        } else {
            this.o.Y(false);
        }
    }

    @DexIgnore
    @Override // com.fossil.X26
    public void r() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }
}
