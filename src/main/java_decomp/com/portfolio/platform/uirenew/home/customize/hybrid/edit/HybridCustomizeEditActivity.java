package com.portfolio.platform.uirenew.home.customize.hybrid.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.fossil.Ks5;
import com.fossil.Ln0;
import com.fossil.Po4;
import com.fossil.Rk0;
import com.fossil.Sk0;
import com.mapped.HybridCustomizeEditPresenter;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeEditActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a(null);
    @DexIgnore
    public HybridCustomizeEditPresenter A;
    @DexIgnore
    public Po4 B;
    @DexIgnore
    public HybridCustomizeViewModel C;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            Wg6.c(context, "context");
            Wg6.c(str, "presetId");
            Wg6.c(str2, "microAppPos");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HybridCustomizeEditActivity", "start - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(context, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(FragmentActivity fragmentActivity, String str, ArrayList<Ln0<View, String>> arrayList, List<? extends Ln0<CustomizeWidget, String>> list, String str2) {
            Wg6.c(fragmentActivity, "context");
            Wg6.c(str, "presetId");
            Wg6.c(arrayList, "views");
            Wg6.c(list, "customizeWidgetViews");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DianaCustomizeEditActivity", "startForResultAnimation() - presetId=" + str + ", microAppPos=" + str2);
            Intent intent = new Intent(fragmentActivity, HybridCustomizeEditActivity.class);
            intent.putExtra("KEY_PRESET_ID", str);
            intent.putExtra("KEY_PRESET_WATCH_APP_POS_SELECTED", str2);
            Iterator<? extends Ln0<CustomizeWidget, String>> it = list.iterator();
            while (it.hasNext()) {
                Ln0 ln0 = (Ln0) it.next();
                arrayList.add(new Ln0<>(ln0.a, ln0.b));
                Bundle bundle = new Bundle();
                Ks5.Ai ai = Ks5.c;
                F f = ln0.a;
                if (f != null) {
                    Wg6.b(f, "wcPair.first!!");
                    ai.a(f, bundle);
                    F f2 = ln0.a;
                    if (f2 != null) {
                        Wg6.b(f2, "wcPair.first!!");
                        intent.putExtra(f2.getTransitionName(), bundle);
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new Ln0[0]);
            if (array != null) {
                Ln0[] ln0Arr = (Ln0[]) array;
                Sk0 a2 = Sk0.a(fragmentActivity, (Ln0[]) Arrays.copyOf(ln0Arr, ln0Arr.length));
                Wg6.b(a2, "ActivityOptionsCompat.ma\u2026context, *viewsTypeArray)");
                Rk0.y(fragmentActivity, intent, 100, a2.b());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        Fragment Y = getSupportFragmentManager().Y(2131362158);
        if (Y != null) {
            Y.onActivityResult(i, i2, intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0108  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x013b  */
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
        // Method dump skipped, instructions count: 329
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity.onCreate(android.os.Bundle):void");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        HybridCustomizeEditPresenter hybridCustomizeEditPresenter = this.A;
        if (hybridCustomizeEditPresenter != null) {
            hybridCustomizeEditPresenter.A(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }
}
