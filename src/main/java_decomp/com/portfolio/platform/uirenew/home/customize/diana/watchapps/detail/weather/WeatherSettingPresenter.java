package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.text.TextUtils;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Jx7;
import com.fossil.Ko7;
import com.fossil.Nt3;
import com.fossil.P47;
import com.fossil.Pm7;
import com.fossil.Ra6;
import com.fossil.Sa6;
import com.fossil.Yn7;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc3;
import com.mapped.Lf6;
import com.mapped.Mc3;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingPresenter extends Ra6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public WeatherWatchAppSetting f;
    @DexIgnore
    public List<WeatherLocationWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ Sa6 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai<TResult> implements Mc3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ LatLng $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.a.h.h2(this.this$0.this$0.a.g);
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(LatLng latLng, Xe6 xe6, Ai ai) {
                super(2, xe6);
                this.$it = latLng;
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.$it, xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    String c = P47.c(this.this$0.b);
                    List list = this.this$0.a.g;
                    String str = this.this$0.c;
                    LatLng latLng = this.$it;
                    double d2 = latLng.b;
                    double d3 = latLng.c;
                    Wg6.b(c, "name");
                    list.add(new WeatherLocationWrapper(str, d2, d3, c, this.this$0.b, false, true, 32, null));
                    Jx7 c2 = Bw7.c();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = c;
                    this.label = 1;
                    if (Eu7.g(c2, aiii, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    String str2 = (String) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ai(WeatherSettingPresenter weatherSettingPresenter, String str, String str2) {
            this.a = weatherSettingPresenter;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        public final void a(FetchPlaceResponse fetchPlaceResponse) {
            this.a.h.a();
            Wg6.b(fetchPlaceResponse, "response");
            Place place = fetchPlaceResponse.getPlace();
            Wg6.b(place, "response.place");
            LatLng latLng = place.getLatLng();
            if (latLng != null) {
                Rm6 unused = Gu7.d(Jv7.a(Bw7.a()), null, null, new Aii(latLng, null, this), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.mapped.Mc3
        public /* bridge */ /* synthetic */ void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            a(fetchPlaceResponse);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements Lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter a;

        @DexIgnore
        public Bi(WeatherSettingPresenter weatherSettingPresenter) {
            this.a = weatherSettingPresenter;
        }

        @DexIgnore
        @Override // com.mapped.Lc3
        public final void onFailure(Exception exc) {
            Wg6.c(exc, "exception");
            this.a.h.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = WeatherSettingPresenter.i;
            local.e(str, "FetchPlaceRequest - exception=" + exc);
            this.a.h.E4();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1", f = "WeatherSettingPresenter.kt", l = {129}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends WeatherLocationWrapper>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Ci ci) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends WeatherLocationWrapper>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    List list = this.this$0.this$0.g;
                    ArrayList arrayList = new ArrayList();
                    for (Object obj2 : list) {
                        if (Ao7.a(((WeatherLocationWrapper) obj2).isEnableLocation()).booleanValue()) {
                            arrayList.add(obj2);
                        }
                    }
                    return arrayList;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WeatherSettingPresenter weatherSettingPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherSettingPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            WeatherWatchAppSetting weatherWatchAppSetting;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WeatherWatchAppSetting weatherWatchAppSetting2 = this.this$0.f;
                if (weatherWatchAppSetting2 != null) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(null, this);
                    this.L$0 = il6;
                    this.L$1 = weatherWatchAppSetting2;
                    this.label = 1;
                    g = Eu7.g(h, aii, this);
                    if (g == d) {
                        return d;
                    }
                    weatherWatchAppSetting = weatherWatchAppSetting2;
                }
                this.this$0.h.a();
                this.this$0.h.x0(true);
                return Cd6.a;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
                weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            weatherWatchAppSetting.setLocations(Pm7.j0((List) g));
            this.this$0.h.a();
            this.this$0.h.x0(true);
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = WeatherSettingPresenter.class.getSimpleName();
        Wg6.b(simpleName, "WeatherSettingPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public WeatherSettingPresenter(Sa6 sa6, GoogleApiService googleApiService) {
        Wg6.c(sa6, "mView");
        Wg6.c(googleApiService, "mGoogleApiService");
        this.h = sa6;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.e = Places.createClient(PortfolioApp.get.instance());
        this.h.h2(this.g);
        this.h.H(this.e);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.Ra6
    public void n(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        Wg6.c(str, "address");
        Wg6.c(str2, "placeId");
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (arrayList.size() > 1) {
            this.h.G1();
            return;
        }
        this.h.b();
        if (this.e != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Place.Field.ADDRESS);
            arrayList2.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str2, arrayList2);
            Wg6.b(builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.e;
            if (placesClient != null) {
                Nt3<FetchPlaceResponse> fetchPlace = placesClient.fetchPlace(builder.build());
                Wg6.b(fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.f(new Ai(this, str, str2));
                fetchPlace.d(new Bi(this));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Ra6
    public WeatherWatchAppSetting o() {
        return this.f;
    }

    @DexIgnore
    @Override // com.fossil.Ra6
    public void p() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "saveWeatherWatchAppSetting - mLocationWrappers=" + this.g);
        this.h.b();
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Ra6
    public void q(int i2, boolean z) {
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isEnableLocation()) {
                arrayList.add(t);
            }
        }
        if (z && arrayList.size() > 1) {
            this.h.G1();
        } else if (i2 < this.g.size()) {
            this.g.get(i2).setEnableLocation(z);
            this.h.h2(this.g);
        }
    }

    @DexIgnore
    public void w(String str) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        Wg6.c(str, MicroAppSetting.SETTING);
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) new Gson().k(str, WeatherWatchAppSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse weather setting " + e2);
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        this.f = weatherWatchAppSetting;
        if (weatherWatchAppSetting == null) {
            this.f = new WeatherWatchAppSetting();
        }
        WeatherWatchAppSetting weatherWatchAppSetting2 = this.f;
        if (weatherWatchAppSetting2 != null) {
            List<WeatherLocationWrapper> locations = weatherWatchAppSetting2.getLocations();
            ArrayList arrayList = new ArrayList();
            for (T t : locations) {
                T t2 = t;
                if (!TextUtils.isEmpty(t2 != null ? t2.getId() : null)) {
                    arrayList.add(t);
                }
            }
            if (arrayList.isEmpty()) {
                this.f = new WeatherWatchAppSetting();
            }
            this.g.clear();
            WeatherWatchAppSetting weatherWatchAppSetting3 = this.f;
            if (weatherWatchAppSetting3 != null) {
                for (WeatherLocationWrapper weatherLocationWrapper : weatherWatchAppSetting3.getLocations()) {
                    if (weatherLocationWrapper != null) {
                        this.g.add(weatherLocationWrapper);
                    }
                }
                return;
            }
            Wg6.i();
            throw null;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public void x() {
        this.h.M5(this);
    }
}
