package com.portfolio.platform.uirenew.home.profile.edit;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.lifecycle.MutableLiveData;
import com.facebook.internal.Utility;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Fj1;
import com.fossil.Gu7;
import com.fossil.Hk5;
import com.fossil.I37;
import com.fossil.Ko7;
import com.fossil.Qh5;
import com.fossil.Ry5;
import com.fossil.Tj5;
import com.fossil.Ts0;
import com.fossil.Um5;
import com.fossil.Us0;
import com.fossil.Vk5;
import com.fossil.Wc1;
import com.fossil.Wt7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.information.domain.usecase.UpdateUser;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.chrono.BasicChronology;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileEditViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Bi l; // = new Bi(null);
    @DexIgnore
    public MFUser a;
    @DexIgnore
    public MFUser b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public String d;
    @DexIgnore
    public volatile boolean e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g; // = true;
    @DexIgnore
    public MutableLiveData<Ci> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ UpdateUser i;
    @DexIgnore
    public /* final */ GetUser j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public String a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;

        @DexIgnore
        public Ai() {
            this(null, null, null, 7, null);
        }

        @DexIgnore
        public Ai(String str, String str2, String str3) {
            this.a = str;
            this.b = str2;
            this.c = str3;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Ai(String str, String str2, String str3, int i, Qg6 qg6) {
            this((i & 1) != 0 ? null : str, (i & 2) != 0 ? null : str2, (i & 4) != 0 ? null : str3);
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b) || !Wg6.a(this.c, ai.c)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            String str = this.a;
            int hashCode = str != null ? str.hashCode() : 0;
            String str2 = this.b;
            int hashCode2 = str2 != null ? str2.hashCode() : 0;
            String str3 = this.c;
            if (str3 != null) {
                i = str3.hashCode();
            }
            return (((hashCode * 31) + hashCode2) * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "CapturedData(userFirstName=" + this.a + ", userLastName=" + this.b + ", bitmapData=" + this.c + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public Bi() {
        }

        @DexIgnore
        public /* synthetic */ Bi(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return ProfileEditViewModel.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public MFUser a;
        @DexIgnore
        public Uri b;
        @DexIgnore
        public Boolean c;
        @DexIgnore
        public Lc6<Integer, String> d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public MFUser f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public Bundle h;
        @DexIgnore
        public String i;
        @DexIgnore
        public String j;
        @DexIgnore
        public Ai k;

        @DexIgnore
        public Ci(MFUser mFUser, Uri uri, Boolean bool, Lc6<Integer, String> lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, Ai ai) {
            this.a = mFUser;
            this.b = uri;
            this.c = bool;
            this.d = lc6;
            this.e = z;
            this.f = mFUser2;
            this.g = z2;
            this.h = bundle;
            this.i = str;
            this.j = str2;
            this.k = ai;
        }

        @DexIgnore
        public final String a() {
            return this.i;
        }

        @DexIgnore
        public final String b() {
            return this.j;
        }

        @DexIgnore
        public final Ai c() {
            return this.k;
        }

        @DexIgnore
        public final Bundle d() {
            return this.h;
        }

        @DexIgnore
        public final Uri e() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ci) {
                    Ci ci = (Ci) obj;
                    if (!Wg6.a(this.a, ci.a) || !Wg6.a(this.b, ci.b) || !Wg6.a(this.c, ci.c) || !Wg6.a(this.d, ci.d) || this.e != ci.e || !Wg6.a(this.f, ci.f) || this.g != ci.g || !Wg6.a(this.h, ci.h) || !Wg6.a(this.i, ci.i) || !Wg6.a(this.j, ci.j) || !Wg6.a(this.k, ci.k)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.e;
        }

        @DexIgnore
        public final Lc6<Integer, String> h() {
            return this.d;
        }

        @DexIgnore
        public int hashCode() {
            int i2 = 1;
            int i3 = 0;
            MFUser mFUser = this.a;
            int hashCode = mFUser != null ? mFUser.hashCode() : 0;
            Uri uri = this.b;
            int hashCode2 = uri != null ? uri.hashCode() : 0;
            Boolean bool = this.c;
            int hashCode3 = bool != null ? bool.hashCode() : 0;
            Lc6<Integer, String> lc6 = this.d;
            int hashCode4 = lc6 != null ? lc6.hashCode() : 0;
            boolean z = this.e;
            if (z) {
                z = true;
            }
            MFUser mFUser2 = this.f;
            int hashCode5 = mFUser2 != null ? mFUser2.hashCode() : 0;
            boolean z2 = this.g;
            if (!z2) {
                i2 = z2 ? 1 : 0;
            }
            Bundle bundle = this.h;
            int hashCode6 = bundle != null ? bundle.hashCode() : 0;
            String str = this.i;
            int hashCode7 = str != null ? str.hashCode() : 0;
            String str2 = this.j;
            int hashCode8 = str2 != null ? str2.hashCode() : 0;
            Ai ai = this.k;
            if (ai != null) {
                i3 = ai.hashCode();
            }
            int i4 = z ? 1 : 0;
            int i5 = z ? 1 : 0;
            int i6 = z ? 1 : 0;
            return (((((((((((((((((((hashCode * 31) + hashCode2) * 31) + hashCode3) * 31) + hashCode4) * 31) + i4) * 31) + hashCode5) * 31) + i2) * 31) + hashCode6) * 31) + hashCode7) * 31) + hashCode8) * 31) + i3;
        }

        @DexIgnore
        public final MFUser i() {
            return this.f;
        }

        @DexIgnore
        public final MFUser j() {
            return this.a;
        }

        @DexIgnore
        public final Boolean k() {
            return this.c;
        }

        @DexIgnore
        public String toString() {
            return "UIModelWrapper(user=" + this.a + ", imageUri=" + this.b + ", isUserChanged=" + this.c + ", showServerError=" + this.d + ", showProcessImageError=" + this.e + ", showSuccess=" + this.f + ", showLoading=" + this.g + ", dobBundle=" + this.h + ", birthday=" + this.i + ", birthdayErrorMsg=" + this.j + ", capturedData=" + this.k + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ei<GetUser.Ai, CoroutineUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CoroutineUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(CoroutineUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onError");
            ProfileEditViewModel.h(this.a, null, null, null, null, false, null, false, null, null, null, null, 1983, null);
            this.a.f = true;
        }

        @DexIgnore
        public void c(GetUser.Ai ai) {
            Wg6.c(ai, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside mGetUser onSuccess");
            ProfileEditViewModel.h(this.a, null, null, null, null, false, null, false, null, null, null, null, 1983, null);
            if (ai.a() != null) {
                this.a.b = ai.a();
                this.a.a = MFUser.copy$default(ai.a(), null, null, null, null, null, false, null, false, false, null, null, 0, null, null, null, null, false, null, false, false, false, null, 0, 0, null, 0, 67108863, null);
                ProfileEditViewModel profileEditViewModel = this.a;
                ProfileEditViewModel.h(profileEditViewModel, profileEditViewModel.b, null, null, null, false, null, false, null, null, null, null, 2046, null);
            } else {
                FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), "loadUserFirstTime user is null");
            }
            this.a.f = true;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetUser.Ai ai) {
            c(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel$onProfilePictureChanged$1", f = "ProfileEditViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Intent $data;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(ProfileEditViewModel profileEditViewModel, Intent intent, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = profileEditViewModel;
            this.$data = intent;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$data, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Bitmap bitmap;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Uri g = Vk5.g(this.$data, PortfolioApp.get.instance());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = ProfileEditViewModel.l.a();
                StringBuilder sb = new StringBuilder();
                sb.append("Inside .onActivityResult imageUri=");
                if (g != null) {
                    sb.append(g);
                    local.d(a2, sb.toString());
                    if (!PortfolioApp.get.instance().w0(this.$data, g)) {
                        return Cd6.a;
                    }
                    ProfileEditViewModel.h(this.this$0, null, g, null, null, false, null, false, null, null, null, null, 2045, null);
                    try {
                        if (!TextUtils.equals(g.getLastPathSegment(), "pickerImage.jpg")) {
                            bitmap = (Bitmap) Tj5.a(PortfolioApp.get.instance()).D().d1(g).T0(((Fj1) ((Fj1) new Fj1().l(Wc1.a)).m0(true)).o0(new Hk5())).R0(200, 200).get();
                        } else {
                            Bitmap q = Ry5.q(g.getPath(), 256, true);
                            this.this$0.c = true;
                            String f = I37.f(q);
                            MFUser mFUser = this.this$0.b;
                            String firstName = mFUser != null ? mFUser.getFirstName() : null;
                            MFUser mFUser2 = this.this$0.b;
                            ProfileEditViewModel.h(this.this$0, null, null, null, null, false, null, false, null, null, null, new Ai(firstName, mFUser2 != null ? mFUser2.getLastName() : null, f), BasicChronology.CACHE_MASK, null);
                            bitmap = q;
                        }
                        MFUser mFUser3 = this.this$0.b;
                        if (mFUser3 != null) {
                            if (bitmap != null) {
                                String f2 = I37.f(bitmap);
                                Wg6.b(f2, "BitmapUtils.encodeToBase64(bitmap!!)");
                                mFUser3.setProfilePicture(f2);
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                        this.this$0.c = true;
                        ProfileEditViewModel.h(this.this$0, null, null, Ao7.a(this.this$0.l()), null, false, null, false, null, null, null, null, 2043, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                        ProfileEditViewModel.h(this.this$0, null, null, null, null, true, null, false, null, null, null, null, 2031, null);
                    }
                    return Cd6.a;
                }
                Wg6.i();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CoroutineUseCase.Ei<UpdateUser.Ci, UpdateUser.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ ProfileEditViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(ProfileEditViewModel profileEditViewModel) {
            this.a = profileEditViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateUser.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(UpdateUser.Bi bi) {
            Wg6.c(bi, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = ProfileEditViewModel.l.a();
            local.d(a2, ".Inside updateUser onError, errorCode=" + bi.a());
            ProfileEditViewModel.h(this.a, null, null, null, new Lc6(Integer.valueOf(bi.a()), ""), false, null, false, null, null, null, null, 1975, null);
        }

        @DexIgnore
        public void c(UpdateUser.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(ProfileEditViewModel.l.a(), ".Inside updateUser onSuccess");
            ProfileEditViewModel.h(this.a, null, null, null, null, false, ci.a(), false, null, null, null, null, 1951, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateUser.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = ProfileEditViewModel.class.getSimpleName();
        Wg6.b(simpleName, "ProfileEditViewModel::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public ProfileEditViewModel(UpdateUser updateUser, GetUser getUser) {
        Wg6.c(updateUser, "mUpdateUser");
        Wg6.c(getUser, "mGetUser");
        this.i = updateUser;
        this.j = getUser;
    }

    @DexIgnore
    public static /* synthetic */ void h(ProfileEditViewModel profileEditViewModel, MFUser mFUser, Uri uri, Boolean bool, Lc6 lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, Ai ai, int i2, Object obj) {
        profileEditViewModel.g((i2 & 1) != 0 ? null : mFUser, (i2 & 2) != 0 ? null : uri, (i2 & 4) != 0 ? null : bool, (i2 & 8) != 0 ? null : lc6, (i2 & 16) != 0 ? false : z, (i2 & 32) != 0 ? null : mFUser2, (i2 & 64) != 0 ? false : z2, (i2 & 128) != 0 ? null : bundle, (i2 & 256) != 0 ? null : str, (i2 & 512) != 0 ? null : str2, (i2 & 1024) != 0 ? null : ai);
    }

    @DexIgnore
    public final void g(MFUser mFUser, Uri uri, Boolean bool, Lc6<Integer, String> lc6, boolean z, MFUser mFUser2, boolean z2, Bundle bundle, String str, String str2, Ai ai) {
        this.h.l(new Ci(mFUser, uri, bool, lc6, z, mFUser2, z2, bundle, str, str2, ai));
    }

    @DexIgnore
    public final MutableLiveData<Ci> i() {
        return this.h;
    }

    @DexIgnore
    public final boolean j(Date date) {
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        Years yearsBetween = Years.yearsBetween(LocalDate.fromCalendarFields(instance), LocalDate.now());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        StringBuilder sb = new StringBuilder();
        sb.append("age=");
        Wg6.b(yearsBetween, "age");
        sb.append(yearsBetween.getYears());
        local.d(str, sb.toString());
        return yearsBetween.getYears() >= 16;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0038, code lost:
        if (r0 != r4.getHeightInCentimeters()) goto L_0x003a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0074, code lost:
        if (com.fossil.Lr7.b(r4 * r5) != com.fossil.Lr7.b((((float) r5.getWeightInGrams()) / 1000.0f) * r5)) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00e7, code lost:
        if ((r4 + r6) == (r0.intValue() + r7)) goto L_0x00e9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00e9, code lost:
        r0 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x011c, code lost:
        if (com.fossil.Lr7.b(r4 * r5) == com.fossil.Lr7.b(r1 * r5)) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x011e, code lost:
        r1 = false;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean k() {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.k():boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x008a, code lost:
        if ((!com.mapped.Wg6.a(r0, r3.getBirthday())) != false) goto L_0x008c;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean l() {
        /*
            r4 = this;
            r2 = 0
            r1 = 0
            com.portfolio.platform.data.model.MFUser r0 = r4.a
            if (r0 == 0) goto L_0x000a
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 != 0) goto L_0x000c
        L_0x000a:
            r0 = r2
        L_0x000b:
            return r0
        L_0x000c:
            if (r0 == 0) goto L_0x00c3
            java.lang.String r0 = r0.getFirstName()
            if (r0 == 0) goto L_0x009b
            if (r0 == 0) goto L_0x0093
            java.lang.CharSequence r0 = com.fossil.Wt7.u0(r0)
            java.lang.String r0 = r0.toString()
        L_0x001e:
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00bf
            java.lang.String r3 = r3.getFirstName()
            boolean r0 = com.mapped.Wg6.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.a
            if (r0 == 0) goto L_0x00bb
            java.lang.String r0 = r0.getLastName()
            if (r0 == 0) goto L_0x00a5
            if (r0 == 0) goto L_0x009d
            java.lang.CharSequence r0 = com.fossil.Wt7.u0(r0)
            java.lang.String r0 = r0.toString()
        L_0x0042:
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00b7
            java.lang.String r3 = r3.getLastName()
            boolean r0 = com.mapped.Wg6.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            boolean r0 = r4.k()
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.a
            if (r0 == 0) goto L_0x00b3
            java.lang.String r0 = r0.getGender()
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00af
            java.lang.String r3 = r3.getGender()
            boolean r0 = com.mapped.Wg6.a(r0, r3)
            r0 = r0 ^ 1
            if (r0 != 0) goto L_0x008c
            boolean r0 = r4.c
            if (r0 != 0) goto L_0x008c
            com.portfolio.platform.data.model.MFUser r0 = r4.a
            if (r0 == 0) goto L_0x00ab
            java.lang.String r0 = r0.getBirthday()
            com.portfolio.platform.data.model.MFUser r3 = r4.b
            if (r3 == 0) goto L_0x00a7
            java.lang.String r1 = r3.getBirthday()
            boolean r0 = com.mapped.Wg6.a(r0, r1)
            r0 = r0 ^ 1
            if (r0 == 0) goto L_0x00c7
        L_0x008c:
            boolean r0 = r4.g
            if (r0 == 0) goto L_0x00c7
            r0 = 1
            goto L_0x000b
        L_0x0093:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x009b:
            r0 = r1
            goto L_0x001e
        L_0x009d:
            com.mapped.Rc6 r0 = new com.mapped.Rc6
            java.lang.String r1 = "null cannot be cast to non-null type kotlin.CharSequence"
            r0.<init>(r1)
            throw r0
        L_0x00a5:
            r0 = r1
            goto L_0x0042
        L_0x00a7:
            com.mapped.Wg6.i()
            throw r1
        L_0x00ab:
            com.mapped.Wg6.i()
            throw r1
        L_0x00af:
            com.mapped.Wg6.i()
            throw r1
        L_0x00b3:
            com.mapped.Wg6.i()
            throw r1
        L_0x00b7:
            com.mapped.Wg6.i()
            throw r1
        L_0x00bb:
            com.mapped.Wg6.i()
            throw r1
        L_0x00bf:
            com.mapped.Wg6.i()
            throw r1
        L_0x00c3:
            com.mapped.Wg6.i()
            throw r1
        L_0x00c7:
            r0 = r2
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.edit.ProfileEditViewModel.l():boolean");
    }

    @DexIgnore
    public final void m() {
        if (!this.f) {
            h(this, null, null, null, null, false, null, true, null, null, null, null, 1983, null);
            this.j.e(null, new Di(this));
        }
    }

    @DexIgnore
    public final void n(Date date) {
        Wg6.c(date, "birthDay");
        FLogger.INSTANCE.getLocal().d(k, "onBirthDayChanged");
        this.d = TimeUtils.k(date);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = k;
        local.d(str, "mBirthday=" + this.d);
        MFUser mFUser = this.b;
        if (mFUser != null) {
            String str2 = this.d;
            if (str2 != null) {
                mFUser.setBirthday(str2);
            } else {
                Wg6.i();
                throw null;
            }
        }
        if (!j(date)) {
            this.g = false;
            h(this, null, null, Boolean.FALSE, null, false, null, false, null, TimeUtils.i(date), Um5.c(PortfolioApp.get.instance(), 2131886965), null, 1275, null);
            return;
        }
        this.g = true;
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, TimeUtils.i(date), null, null, 1787, null);
    }

    @DexIgnore
    public final void o() {
        List Y;
        String birthday;
        List Y2;
        Bundle bundle = new Bundle();
        String str = this.d;
        if (str == null || (Y2 = Wt7.Y(str, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, null)) == null) {
            MFUser mFUser = this.b;
            Y = (mFUser == null || (birthday = mFUser.getBirthday()) == null) ? null : Wt7.Y(birthday, new String[]{ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR}, false, 0, 6, null);
        } else {
            Y = Y2;
        }
        FLogger.INSTANCE.getLocal().d(k, String.valueOf(Y));
        if (Y != null && Y.size() == 3) {
            bundle.putInt("DAY", Integer.parseInt((String) Y.get(2)));
            bundle.putInt("MONTH", Integer.parseInt((String) Y.get(1)));
            bundle.putInt("YEAR", Integer.parseInt((String) Y.get(0)));
        }
        h(this, null, null, null, null, false, null, false, bundle, null, null, null, 1919, null);
    }

    @DexIgnore
    public final void p(String str) {
        Wg6.c(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setFirstName(str);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void q(Qh5 qh5) {
        Wg6.c(qh5, "gender");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setGender(qh5.toString());
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void r(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void s(String str) {
        Wg6.c(str, "name");
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setLastName(str);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void t(Intent intent) {
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ei(this, intent, null), 2, null);
    }

    @DexIgnore
    public final void u(int i2) {
        this.e = true;
        MFUser mFUser = this.b;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
        h(this, null, null, Boolean.valueOf(l()), null, false, null, false, null, null, null, null, 2043, null);
    }

    @DexIgnore
    public final void v() {
        if (this.b != null) {
            h(this, null, null, null, null, false, null, true, null, null, null, null, 1983, null);
            MFUser mFUser = this.b;
            if (mFUser != null) {
                if (!TextUtils.isEmpty(mFUser.getProfilePicture())) {
                    MFUser mFUser2 = this.b;
                    if (mFUser2 != null) {
                        String profilePicture = mFUser2.getProfilePicture();
                        Boolean valueOf = profilePicture != null ? Boolean.valueOf(Wt7.v(profilePicture, Utility.URL_SCHEME, false, 2, null)) : null;
                        if (valueOf == null) {
                            Wg6.i();
                            throw null;
                        } else if (valueOf.booleanValue()) {
                            MFUser mFUser3 = this.b;
                            if (mFUser3 != null) {
                                mFUser3.setProfilePicture("");
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    } else {
                        Wg6.i();
                        throw null;
                    }
                }
                MFUser mFUser4 = this.b;
                if (mFUser4 != null) {
                    if (mFUser4.getUseDefaultBiometric() && this.e) {
                        MFUser mFUser5 = this.b;
                        if (mFUser5 != null) {
                            mFUser5.setUseDefaultBiometric(false);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    UpdateUser updateUser = this.i;
                    MFUser mFUser6 = this.b;
                    if (mFUser6 != null) {
                        updateUser.e(new UpdateUser.Ai(mFUser6), new Fi(this));
                    } else {
                        Wg6.i();
                        throw null;
                    }
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }
}
