package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import androidx.loader.app.LoaderManager;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.At0;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.J06;
import com.fossil.Ko7;
import com.fossil.O56;
import com.fossil.P56;
import com.fossil.Tq4;
import com.fossil.Uq4;
import com.fossil.W37;
import com.fossil.Y56;
import com.fossil.Yn7;
import com.fossil.Zs0;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rl6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactPresenter extends O56 implements LoaderManager.a<Cursor> {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static /* final */ Ai m; // = new Ai(null);
    @DexIgnore
    public /* final */ List<J06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ P56 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<J06> h;
    @DexIgnore
    public /* final */ LoaderManager i;
    @DexIgnore
    public /* final */ Uq4 j;
    @DexIgnore
    public /* final */ Y56 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridContactPresenter.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1", f = "NotificationHybridContactPresenter.kt", l = {47}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridContactPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$1", f = "NotificationHybridContactPresenter.kt", l = {48}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements Tq4.Di<Y56.Ci, Y56.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Bi a;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1", f = "NotificationHybridContactPresenter.kt", l = {99}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ Y56.Ci $successResponse;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter$start$1$2$onSuccess$1$populateContacts$1", f = "NotificationHybridContactPresenter.kt", l = {}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Aiii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Aiii aiii, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aiii;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Yn7.d();
                        if (this.label == 0) {
                            El7.b(obj);
                            for (T t : this.this$0.$successResponse.a()) {
                                for (Contact contact : t.getContacts()) {
                                    J06 j06 = new J06(contact, null, 2, null);
                                    j06.setAdded(true);
                                    Contact contact2 = j06.getContact();
                                    if (contact2 != null) {
                                        Wg6.b(contact, "contact");
                                        contact2.setDbRowId(contact.getDbRowId());
                                        contact2.setUseSms(contact.isUseSms());
                                        contact2.setUseCall(contact.isUseCall());
                                    }
                                    j06.setCurrentHandGroup(t.getHour());
                                    Wg6.b(contact, "contact");
                                    List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                                    Wg6.b(phoneNumbers, "contact.phoneNumbers");
                                    if (!phoneNumbers.isEmpty()) {
                                        PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                        Wg6.b(phoneNumber, "contact.phoneNumbers[0]");
                                        String number = phoneNumber.getNumber();
                                        if (!TextUtils.isEmpty(number)) {
                                            j06.setHasPhoneNumber(true);
                                            j06.setPhoneNumber(number);
                                            FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), " filter selected contact, phoneNumber=" + number);
                                        }
                                    }
                                    Iterator it = this.this$0.this$0.a.this$0.h.iterator();
                                    int i = 0;
                                    while (true) {
                                        if (!it.hasNext()) {
                                            i = -1;
                                            break;
                                        }
                                        Contact contact3 = ((J06) it.next()).getContact();
                                        if (Ao7.a(contact3 != null && contact3.getContactId() == contact.getContactId()).booleanValue()) {
                                            break;
                                        }
                                        i++;
                                    }
                                    if (i != -1) {
                                        j06.setCurrentHandGroup(this.this$0.this$0.a.this$0.g);
                                        Wg6.b(this.this$0.this$0.a.this$0.h.remove(i), "mContactWrappersSelected\u2026moveAt(indexContactFound)");
                                    } else if (j06.getCurrentHandGroup() == this.this$0.this$0.a.this$0.g) {
                                    }
                                    FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                                    this.this$0.this$0.a.this$0.z().add(j06);
                                }
                            }
                            return Cd6.a;
                        }
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Y56.Ci ci, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                    this.$successResponse = ci;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, this.$successResponse, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Rl6 rl6 = Gu7.b(il6, this.this$0.a.this$0.h(), null, new Aiiii(this, null), 2, null);
                        this.L$0 = il6;
                        this.L$1 = rl6;
                        this.label = 1;
                        if (rl6.l(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Rl6 rl62 = (Rl6) this.L$1;
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (!this.this$0.a.this$0.h.isEmpty()) {
                        for (J06 j06 : this.this$0.a.this$0.h) {
                            this.this$0.a.this$0.z().add(j06);
                        }
                    }
                    this.this$0.a.this$0.f.n1(this.this$0.a.this$0.z(), W37.a.a(), this.this$0.a.this$0.g);
                    this.this$0.a.this$0.i.d(0, new Bundle(), this.this$0.a.this$0);
                    return Cd6.a;
                }
            }

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Bi bi) {
                this.a = bi;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void a(Y56.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(Y56.Ai ai) {
                Wg6.c(ai, "errorResponse");
                FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            public void c(Y56.Ci ci) {
                Wg6.c(ci, "successResponse");
                FLogger.INSTANCE.getLocal().d(NotificationHybridContactPresenter.m.a(), "GetAllContactGroup onSuccess");
                Rm6 unused = Gu7.d(this.a.this$0.k(), null, null, new Aiii(this, ci, null), 3, null);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void onSuccess(Y56.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(NotificationHybridContactPresenter notificationHybridContactPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridContactPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().k0().s0()) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(h, aii, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.z().isEmpty()) {
                this.this$0.j.a(this.this$0.k, null, new Bii(this));
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationHybridContactPresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridContac\u2026er::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridContactPresenter(P56 p56, int i2, ArrayList<J06> arrayList, LoaderManager loaderManager, Uq4 uq4, Y56 y56) {
        Wg6.c(p56, "mView");
        Wg6.c(arrayList, "mContactWrappersSelected");
        Wg6.c(loaderManager, "mLoaderManager");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(y56, "mGetAllHybridContactGroups");
        this.f = p56;
        this.g = i2;
        this.h = arrayList;
        this.i = loaderManager;
        this.j = uq4;
        this.k = y56;
    }

    @DexIgnore
    public void A(At0<Cursor> at0, Cursor cursor) {
        Wg6.c(at0, "loader");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onLoadFinished cursor=" + cursor);
        this.f.T(cursor);
    }

    @DexIgnore
    public void B() {
        this.f.M5(this);
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.fossil.At0, java.lang.Object] */
    @Override // androidx.loader.app.LoaderManager.a
    public /* bridge */ /* synthetic */ void a(At0<Cursor> at0, Cursor cursor) {
        A(at0, cursor);
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public At0<Cursor> d(int i2, Bundle bundle) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, ".Inside onCreateLoader, selection = has_phone_number!=0 AND mimetype=?");
        return new Zs0(PortfolioApp.get.instance(), ContactsContract.Data.CONTENT_URI, new String[]{"contact_id", "display_name", "data1", "has_phone_number", "starred", "photo_thumb_uri", "sort_key", "display_name"}, "has_phone_number!=0 AND mimetype=?", new String[]{"vnd.android.cursor.item/phone_v2"}, "display_name COLLATE LOCALIZED ASC");
    }

    @DexIgnore
    @Override // androidx.loader.app.LoaderManager.a
    public void g(At0<Cursor> at0) {
        Wg6.c(at0, "loader");
        FLogger.INSTANCE.getLocal().d(l, ".Inside onLoaderReset");
        this.f.V();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(l, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(l, "stop");
    }

    @DexIgnore
    @Override // com.fossil.O56
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.O56
    public void o() {
        ArrayList<J06> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.I(arrayList);
    }

    @DexIgnore
    @Override // com.fossil.O56
    public void p(J06 j06) {
        T t;
        Wg6.c(j06, "contactWrapper");
        FLogger.INSTANCE.getLocal().d(l, "reassignContact: contactWrapper=" + j06);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            Contact contact = next.getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = j06.getContact();
            if (Wg6.a(valueOf, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                t = next;
                break;
            }
        }
        T t2 = t;
        if (t2 != null) {
            t2.setAdded(true);
            t2.setCurrentHandGroup(this.g);
            this.f.A3();
        }
    }

    @DexIgnore
    public final List<J06> z() {
        return this.e;
    }
}
