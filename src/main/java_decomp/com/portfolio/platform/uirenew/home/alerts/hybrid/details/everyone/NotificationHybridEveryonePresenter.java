package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import android.text.TextUtils;
import com.facebook.share.internal.VideoUploader;
import com.fossil.B66;
import com.fossil.C66;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.J06;
import com.fossil.Ko7;
import com.fossil.Tq4;
import com.fossil.Uq4;
import com.fossil.Y56;
import com.fossil.Yn7;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.AppType;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridEveryonePresenter extends B66 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ Ai l; // = new Ai(null);
    @DexIgnore
    public /* final */ List<J06> e; // = new ArrayList();
    @DexIgnore
    public /* final */ C66 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<J06> h;
    @DexIgnore
    public /* final */ Uq4 i;
    @DexIgnore
    public /* final */ Y56 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridEveryonePresenter.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1", f = "NotificationHybridEveryonePresenter.kt", l = {135}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryonePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$registerContactObserver$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {136}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridEveryonePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(h, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1", f = "NotificationHybridEveryonePresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridEveryonePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryonePresenter$start$1$1", f = "NotificationHybridEveryonePresenter.kt", l = {41}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    PortfolioApp instance = PortfolioApp.get.instance();
                    this.L$0 = il6;
                    this.label = 1;
                    if (instance.V0(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii implements Tq4.Di<Y56.Ci, Y56.Ai> {
            @DexIgnore
            public /* final */ /* synthetic */ Ci a;

            @DexIgnore
            /* JADX WARN: Incorrect args count in method signature: ()V */
            public Bii(Ci ci) {
                this.a = ci;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void a(Y56.Ai ai) {
                b(ai);
            }

            @DexIgnore
            public void b(Y56.Ai ai) {
                Wg6.c(ai, "errorResponse");
                FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), "GetAllContactGroup onError");
            }

            @DexIgnore
            public void c(Y56.Ci ci) {
                Wg6.c(ci, "successResponse");
                FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), "GetAllContactGroup onSuccess");
                for (T t : ci.a()) {
                    for (Contact contact : t.getContacts()) {
                        Wg6.b(contact, "contact");
                        if (contact.getContactId() == -100 || contact.getContactId() == -200) {
                            J06 j06 = new J06(contact, null, 2, null);
                            j06.setAdded(true);
                            Contact contact2 = j06.getContact();
                            if (contact2 != null) {
                                contact2.setDbRowId(contact.getDbRowId());
                                contact2.setUseSms(contact.isUseSms());
                                contact2.setUseCall(contact.isUseCall());
                            }
                            j06.setCurrentHandGroup(t.getHour());
                            List<PhoneNumber> phoneNumbers = contact.getPhoneNumbers();
                            Wg6.b(phoneNumbers, "contact.phoneNumbers");
                            if (!phoneNumbers.isEmpty()) {
                                PhoneNumber phoneNumber = contact.getPhoneNumbers().get(0);
                                Wg6.b(phoneNumber, "contact.phoneNumbers[0]");
                                String number = phoneNumber.getNumber();
                                if (!TextUtils.isEmpty(number)) {
                                    j06.setHasPhoneNumber(true);
                                    j06.setPhoneNumber(number);
                                    FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), " filter selected contact, phoneNumber=" + number);
                                }
                            }
                            Iterator it = this.a.this$0.h.iterator();
                            int i = 0;
                            while (true) {
                                if (!it.hasNext()) {
                                    i = -1;
                                    break;
                                }
                                Contact contact3 = ((J06) it.next()).getContact();
                                if (contact3 != null && contact3.getContactId() == contact.getContactId()) {
                                    break;
                                }
                                i++;
                            }
                            if (i != -1) {
                                j06.setCurrentHandGroup(this.a.this$0.g);
                                this.a.this$0.h.remove(i);
                            }
                            FLogger.INSTANCE.getLocal().d(NotificationHybridEveryonePresenter.l.a(), ".Inside loadContactData filter selected contact, rowId = " + contact.getDbRowId() + ", isUseText = " + contact.isUseSms() + ", isUseCall = " + contact.isUseCall());
                            this.a.this$0.B().add(j06);
                        }
                    }
                }
                if (!this.a.this$0.h.isEmpty()) {
                    for (J06 j062 : this.a.this$0.h) {
                        this.a.this$0.B().add(j062);
                    }
                }
                this.a.this$0.f.V3(this.a.this$0.B(), this.a.this$0.g);
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Tq4.Di
            public /* bridge */ /* synthetic */ void onSuccess(Y56.Ci ci) {
                c(ci);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(NotificationHybridEveryonePresenter notificationHybridEveryonePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationHybridEveryonePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (!PortfolioApp.get.instance().k0().s0()) {
                    Dv7 h = this.this$0.h();
                    Aii aii = new Aii(null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (Eu7.g(h, aii, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.B().isEmpty()) {
                this.this$0.i.a(this.this$0.j, null, new Bii(this));
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationHybridEveryonePresenter.class.getSimpleName();
        Wg6.b(simpleName, "NotificationHybridEveryo\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public NotificationHybridEveryonePresenter(C66 c66, int i2, ArrayList<J06> arrayList, Uq4 uq4, Y56 y56) {
        Wg6.c(c66, "mView");
        Wg6.c(arrayList, "mContactWrappersSelected");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(y56, "mGetAllHybridContactGroups");
        this.f = c66;
        this.g = i2;
        this.h = arrayList;
        this.i = uq4;
        this.j = y56;
    }

    @DexIgnore
    public final J06 A(int i2, String str) {
        List<PhoneNumber> phoneNumbers;
        List<PhoneNumber> phoneNumbers2;
        Contact contact = new Contact();
        contact.setContactId(i2);
        contact.setFirstName(str);
        J06 j06 = new J06(contact, null, 2, null);
        j06.setHasPhoneNumber(true);
        j06.setAdded(true);
        j06.setCurrentHandGroup(this.g);
        if (i2 == -100) {
            Contact contact2 = j06.getContact();
            if (contact2 != null) {
                contact2.setUseCall(true);
            }
            Contact contact3 = j06.getContact();
            if (contact3 != null) {
                contact3.setUseSms(false);
            }
            PhoneNumber phoneNumber = new PhoneNumber();
            phoneNumber.setContact(j06.getContact());
            phoneNumber.setNumber("-1234");
            Contact contact4 = j06.getContact();
            if (!(contact4 == null || (phoneNumbers2 = contact4.getPhoneNumbers()) == null)) {
                phoneNumbers2.add(phoneNumber);
            }
        } else {
            Contact contact5 = j06.getContact();
            if (contact5 != null) {
                contact5.setUseCall(false);
            }
            Contact contact6 = j06.getContact();
            if (contact6 != null) {
                contact6.setUseSms(true);
            }
            PhoneNumber phoneNumber2 = new PhoneNumber();
            phoneNumber2.setContact(j06.getContact());
            phoneNumber2.setNumber("-5678");
            Contact contact7 = j06.getContact();
            if (!(contact7 == null || (phoneNumbers = contact7.getPhoneNumbers()) == null)) {
                phoneNumbers.add(phoneNumber2);
            }
        }
        return j06;
    }

    @DexIgnore
    public final List<J06> B() {
        return this.e;
    }

    @DexIgnore
    public void C() {
        this.f.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(k, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    @Override // com.fossil.B66
    public int n() {
        return this.g;
    }

    @DexIgnore
    @Override // com.fossil.B66
    public void o() {
        boolean z;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -100) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.N0(this.e.get(size));
                    z = true;
                } else {
                    this.e.remove(size);
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_CALLS.packageName;
                Wg6.b(str, "AppType.ALL_CALLS.packageName");
                this.e.add(A(-100, str));
            }
        } else {
            String str2 = AppType.ALL_CALLS.packageName;
            Wg6.b(str2, "AppType.ALL_CALLS.packageName");
            this.e.add(A(-100, str2));
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.B66
    public void p() {
        boolean z;
        if (!this.e.isEmpty()) {
            int size = this.e.size() - 1;
            while (true) {
                if (size < 0) {
                    z = false;
                    break;
                }
                Contact contact = this.e.get(size).getContact();
                if (contact == null || contact.getContactId() != -200) {
                    size--;
                } else if (this.e.get(size).getCurrentHandGroup() != this.g) {
                    this.f.N0(this.e.get(size));
                    z = true;
                } else {
                    this.e.remove(size);
                    z = true;
                }
            }
            if (!z) {
                String str = AppType.ALL_SMS.packageName;
                Wg6.b(str, "AppType.ALL_SMS.packageName");
                this.e.add(A(-200, str));
            }
        } else {
            String str2 = AppType.ALL_SMS.packageName;
            Wg6.b(str2, "AppType.ALL_SMS.packageName");
            this.e.add(A(-200, str2));
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.B66
    public void q() {
        ArrayList<J06> arrayList = new ArrayList<>();
        for (T t : this.e) {
            if (t.isAdded() && t.getCurrentHandGroup() == this.g) {
                arrayList.add(t);
            }
        }
        this.f.I(arrayList);
    }

    @DexIgnore
    @Override // com.fossil.B66
    public void r(J06 j06) {
        Wg6.c(j06, "contactWrapper");
        for (T t : this.e) {
            Contact contact = t.getContact();
            Integer valueOf = contact != null ? Integer.valueOf(contact.getContactId()) : null;
            Contact contact2 = j06.getContact();
            if (Wg6.a(valueOf, contact2 != null ? Integer.valueOf(contact2.getContactId()) : null)) {
                t.setCurrentHandGroup(this.g);
                t.setAdded(true);
            }
        }
        this.f.V3(this.e, this.g);
    }

    @DexIgnore
    @Override // com.fossil.B66
    public void s() {
        if (!PortfolioApp.get.instance().k0().s0()) {
            Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
        }
    }
}
