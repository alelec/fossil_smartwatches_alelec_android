package com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.loader.app.LoaderManager;
import com.fossil.J06;
import com.fossil.Q56;
import com.fossil.R56;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridContactActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationHybridContactPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<J06> arrayList) {
            Wg6.c(fragment, "fragment");
            Wg6.c(arrayList, "contactWrappersSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridContactActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putExtra("LIST_CONTACTS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 5678);
        }
    }

    /*
    static {
        Wg6.b(NotificationHybridContactActivity.class.getSimpleName(), "NotificationHybridContac\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Q56 q56 = (Q56) getSupportFragmentManager().Y(2131362158);
        if (q56 == null) {
            q56 = Q56.l.b();
            k(q56, Q56.l.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (q56 != null) {
            LoaderManager supportLoaderManager = getSupportLoaderManager();
            Wg6.b(supportLoaderManager, "supportLoaderManager");
            iface.n0(new R56(q56, getIntent().getIntExtra("HAND_NUMBER", 0), (ArrayList) getIntent().getSerializableExtra("LIST_CONTACTS_SELECTED"), supportLoaderManager)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactContract.View");
    }
}
