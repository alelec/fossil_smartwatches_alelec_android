package com.portfolio.platform.uirenew.home.details.workout;

import android.content.Context;
import android.content.Intent;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ux7;
import com.fossil.V37;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.dart.DartExecutor;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutDetailActivity extends FlutterActivity {
    @DexIgnore
    public static /* final */ a f; // = new a(null);
    @DexIgnore
    public WorkoutSessionRepository b;
    @DexIgnore
    public MethodChannel c;
    @DexIgnore
    public boolean d; // = true;
    @DexIgnore
    public /* final */ Il6 e; // = Jv7.a(Bw7.c().plus(Ux7.b(null, 1, null)));

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            Wg6.c(context, "context");
            Wg6.c(str, "workoutSessionId");
            Wg6.c(str2, "unitType");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutDetailActivity", "start with workout id " + str + " unitType " + str2);
            Intent intent = new Intent(context, WorkoutDetailActivity.class);
            intent.putExtra("EXTRA_WORKOUT_ID", str);
            intent.putExtra("EXTRA_UNIT_TYPE", str2);
            context.startActivity(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1", f = "WorkoutDetailActivity.kt", l = {94, 98}, m = "invokeSuspend")
    public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WorkoutDetailActivity this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements MethodChannel.Result {
            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void error(String str, String str2, Object obj) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WorkoutDetailActivity", "Unable to load data: " + str2);
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void notImplemented() {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "Unable to load data: notImplemented");
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.Result
            public void success(Object obj) {
                FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "success to load data");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b extends Ko7 implements Coroutine<Il6, Xe6<? super String>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $unitType$inlined;
            @DexIgnore
            public /* final */ /* synthetic */ WorkoutSession $workoutSession$inlined;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public b(Xe6 xe6, b bVar, WorkoutSession workoutSession, String str) {
                super(2, xe6);
                this.this$0 = bVar;
                this.$workoutSession$inlined = workoutSession;
                this.$unitType$inlined = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                b bVar = new b(xe6, this.this$0, this.$workoutSession$inlined, this.$unitType$inlined);
                bVar.p$ = (Il6) obj;
                throw null;
                //return bVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super String> xe6) {
                throw null;
                //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    WorkoutDetailActivity workoutDetailActivity = this.this$0.this$0;
                    WorkoutSession workoutSession = this.$workoutSession$inlined;
                    String str = this.$unitType$inlined;
                    Wg6.b(str, "unitType");
                    if (str != null) {
                        String lowerCase = str.toLowerCase();
                        Wg6.b(lowerCase, "(this as java.lang.String).toLowerCase()");
                        return workoutDetailActivity.f(workoutSession, lowerCase);
                    }
                    throw new Rc6("null cannot be cast to non-null type java.lang.String");
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements MethodChannel.MethodCallHandler {
            @DexIgnore
            public /* final */ /* synthetic */ b b;

            @DexIgnore
            public c(String str, b bVar, WorkoutSession workoutSession, String str2) {
                this.b = bVar;
            }

            @DexIgnore
            @Override // io.flutter.plugin.common.MethodChannel.MethodCallHandler
            public final void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                Wg6.c(methodCall, "call");
                Wg6.c(result, "<anonymous parameter 1>");
                if (Wg6.a(methodCall.method, "dismissMap")) {
                    this.b.this$0.d = true;
                    FLogger.INSTANCE.getLocal().d("WorkoutDetailActivity", "MethodCallHandler dismiss");
                    this.b.this$0.finish();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity$loadData$1$workoutSession$1", f = "WorkoutDetailActivity.kt", l = {94}, m = "invokeSuspend")
        public static final class d extends Ko7 implements Coroutine<Il6, Xe6<? super WorkoutSession>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ String $workoutId;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ b this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public d(b bVar, String str, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bVar;
                this.$workoutId = str;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                d dVar = new d(this.this$0, this.$workoutId, xe6);
                dVar.p$ = (Il6) obj;
                throw null;
                //return dVar;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super WorkoutSession> xe6) {
                throw null;
                //return ((d) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WorkoutSessionRepository d2 = this.this$0.this$0.d();
                    String str = this.$workoutId;
                    this.L$0 = il6;
                    this.label = 1;
                    Object workoutSessionById = d2.getWorkoutSessionById(str, this);
                    return workoutSessionById == d ? d : workoutSessionById;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WorkoutDetailActivity workoutDetailActivity, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = workoutDetailActivity;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (Il6) obj;
            throw null;
            //return bVar;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((b) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x0096  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 296
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.b.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public WorkoutDetailActivity() {
        PortfolioApp.get.instance().getIface().V1(this);
    }

    @DexIgnore
    public static final /* synthetic */ MethodChannel a(WorkoutDetailActivity workoutDetailActivity) {
        MethodChannel methodChannel = workoutDetailActivity.c;
        if (methodChannel != null) {
            return methodChannel;
        }
        Wg6.n("mChannel");
        throw null;
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineConfigurator, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        Wg6.c(flutterEngine, "flutterEngine");
        DartExecutor dartExecutor = flutterEngine.getDartExecutor();
        Wg6.b(dartExecutor, "flutterEngine.dartExecutor");
        this.c = new MethodChannel(dartExecutor.getBinaryMessenger(), "detailTracking/map", JSONMethodCodec.INSTANCE);
    }

    @DexIgnore
    public final WorkoutSessionRepository d() {
        WorkoutSessionRepository workoutSessionRepository = this.b;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        Wg6.n("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final Rm6 e() {
        return Gu7.d(this.e, null, null, new b(this, null), 3, null);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r4 != null) goto L_0x001f;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String f(com.portfolio.platform.data.model.diana.workout.WorkoutSession r23, java.lang.String r24) {
        /*
        // Method dump skipped, instructions count: 331
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.details.workout.WorkoutDetailActivity.f(com.portfolio.platform.data.model.diana.workout.WorkoutSession, java.lang.String):java.lang.String");
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public void onFlutterUiDisplayed() {
        super.onFlutterUiDisplayed();
        if (this.d) {
            this.d = false;
            e();
        }
    }

    @DexIgnore
    @Override // io.flutter.embedding.android.FlutterEngineProvider, io.flutter.embedding.android.FlutterActivityAndFragmentDelegate.Host, io.flutter.embedding.android.FlutterActivity
    public FlutterEngine provideFlutterEngine(Context context) {
        Wg6.c(context, "context");
        return V37.c.a();
    }
}
