package com.portfolio.platform.uirenew.home;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Bv5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.N47;
import com.fossil.Tq4;
import com.fossil.Uq4;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.fossil.Yy5;
import com.fossil.Zy5;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.OtaEvent;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.data.InAppNotification;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.ServerSettingRepository;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.util.DeviceUtils;
import com.portfolio.platform.watchface.edit.model.StorageHelper;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomePresenter extends Yy5 {
    @DexIgnore
    public static /* final */ String x;
    @DexIgnore
    public static /* final */ Ai y; // = new Ai(null);
    @DexIgnore
    public int e;
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public LiveData<List<InAppNotification>> h;
    @DexIgnore
    public /* final */ Fi i; // = new Fi(this);
    @DexIgnore
    public /* final */ Ii j; // = new Ii(this);
    @DexIgnore
    public /* final */ Hi k; // = new Hi(this);
    @DexIgnore
    public /* final */ Zy5 l;
    @DexIgnore
    public /* final */ An4 m;
    @DexIgnore
    public /* final */ DeviceRepository n;
    @DexIgnore
    public /* final */ PortfolioApp o;
    @DexIgnore
    public /* final */ UpdateFirmwareUsecase p;
    @DexIgnore
    public /* final */ Uq4 q;
    @DexIgnore
    public /* final */ Cj4 r;
    @DexIgnore
    public /* final */ Bv5 s;
    @DexIgnore
    public /* final */ N47 t;
    @DexIgnore
    public /* final */ ServerSettingRepository u;
    @DexIgnore
    public /* final */ GetZendeskInformation v;
    @DexIgnore
    public /* final */ FlagRepository w;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HomePresenter.x;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1", f = "HomePresenter.kt", l = {282}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeDeviceSerial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkFirmware$1$isLatestFw$1", f = "HomePresenter.kt", l = {282}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    DeviceUtils a2 = DeviceUtils.h.a();
                    String str = this.this$0.$activeDeviceSerial;
                    this.L$0 = il6;
                    this.label = 1;
                    Object j = a2.j(str, null, this);
                    return j == d ? d : j;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(HomePresenter homePresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$activeDeviceSerial = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, this.$activeDeviceSerial, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() enter checkFirmware");
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            boolean booleanValue = ((Boolean) g).booleanValue();
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str = this.$activeDeviceSerial;
            String a2 = HomePresenter.y.a();
            remote.i(component, session, str, a2, "[Sync OK] Is device has latest FW " + booleanValue);
            if (booleanValue) {
                FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() fw is latest, skip OTA after sync success");
            } else {
                this.this$0.S();
            }
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "checkFirmware() exit checkFirmware");
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements Tq4.Di<Bv5.Ai.Cii, Bv5.Ai.Aii> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ci(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void a(Bv5.Ai.Aii aii) {
            b(aii);
        }

        @DexIgnore
        public void b(Bv5.Ai.Aii aii) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "GetServerSettingUseCase onError");
            this.a.M(false);
        }

        @DexIgnore
        public void c(Bv5.Ai.Cii cii) {
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "GetServerSettingUseCase onSuccess");
            this.a.M(true);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Tq4.Di
        public /* bridge */ /* synthetic */ void onSuccess(Bv5.Ai.Cii cii) {
            c(cii);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1", f = "HomePresenter.kt", l = {332}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $hasServerSettings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1", f = "HomePresenter.kt", l = {352, 353}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkIfHappyUser$1$1$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        this.this$0.this$0.this$0.l.O3();
                        return Cd6.a;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:14:0x007a  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r14) {
                /*
                // Method dump skipped, instructions count: 429
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.HomePresenter.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HomePresenter homePresenter, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$hasServerSettings = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$hasServerSettings, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(i2, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1", f = "HomePresenter.kt", l = {253}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $activeSerial;
        @DexIgnore
        public /* final */ /* synthetic */ String $lastFwTemp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$checkLastOTASuccess$1$activeDevice$1", f = "HomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Device>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Device> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.n.getDeviceBySerial(this.this$0.$activeSerial);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(HomePresenter homePresenter, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$activeSerial = str;
            this.$lastFwTemp = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$activeSerial, this.$lastFwTemp, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            String str = null;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Device device = (Device) g;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.y.a();
            StringBuilder sb = new StringBuilder();
            sb.append("Inside .checkLastOTASuccess, getDeviceBySerial success currentDeviceFw ");
            if (device != null) {
                str = device.getFirmwareRevision();
            }
            sb.append(str);
            local.d(a2, sb.toString());
            if (device != null && !Vt7.j(this.$lastFwTemp, device.getFirmwareRevision(), true)) {
                FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "Handle OTA complete on check last OTA success");
                this.this$0.m.B0(this.$activeSerial);
                this.this$0.m.X0(this.this$0.o.J(), -1);
                this.this$0.l.A(true);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            String stringExtra = intent.getStringExtra("SERIAL");
            if (!TextUtils.isEmpty(stringExtra) && Vt7.j(stringExtra, this.a.o.J(), true)) {
                this.a.l.C1(intent);
                int intExtra = intent.getIntExtra("sync_result", 3);
                int intExtra2 = intent.getIntExtra("LAST_ERROR_CODE", -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra("LIST_ERROR_CODE");
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>();
                }
                int intExtra3 = intent.getIntExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomePresenter.y.a();
                local.d(a2, "Inside .syncReceiver syncResult=" + intExtra + ", serial=" + stringExtra + ", errorCode=" + intExtra2 + " permissionErrors " + integerArrayListExtra + " originalSyncMode " + intExtra3);
                if (intExtra == 1) {
                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                    FLogger.Component component = FLogger.Component.APP;
                    FLogger.Session session = FLogger.Session.OTHER;
                    Wg6.b(stringExtra, "serial");
                    remote.i(component, session, stringExtra, HomePresenter.y.a(), "[Sync OK] Check FW to OTA");
                    this.a.K();
                    this.a.L();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.HomePresenter$onActiveSerialChange$1", f = "HomePresenter.kt", l = {174}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $appMode;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(HomePresenter homePresenter, String str, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = homePresenter;
            this.$serial = str;
            this.$appMode = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$serial, this.$appMode, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x00ab  */
        /* JADX WARNING: Removed duplicated region for block: B:7:0x0057  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r6) {
            /*
                r5 = this;
                r3 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r5.label
                if (r0 == 0) goto L_0x0073
                if (r0 != r3) goto L_0x006b
                java.lang.Object r0 = r5.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r6)
                r0 = r6
            L_0x0013:
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                boolean r0 = r0.booleanValue()
                com.portfolio.platform.uirenew.home.HomePresenter r1 = r5.this$0
                com.mapped.An4 r1 = com.portfolio.platform.uirenew.home.HomePresenter.D(r1)
                java.lang.Boolean r2 = com.fossil.Ao7.a(r0)
                r1.O0(r2)
            L_0x0026:
                com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
                com.portfolio.platform.uirenew.home.HomePresenter$Ai r2 = com.portfolio.platform.uirenew.home.HomePresenter.y
                java.lang.String r2 = r2.a()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "isBCOn: "
                r3.append(r4)
                r3.append(r0)
                java.lang.String r3 = r3.toString()
                r1.e(r2, r3)
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                r1.w(r0)
                java.lang.String r1 = r5.$serial
                boolean r1 = android.text.TextUtils.isEmpty(r1)
                if (r1 != 0) goto L_0x00ab
                com.portfolio.platform.uirenew.home.HomePresenter r1 = r5.this$0
                com.fossil.Zy5 r1 = com.portfolio.platform.uirenew.home.HomePresenter.F(r1)
                java.lang.String r2 = r5.$serial
                com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil$DEVICE r2 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r2)
                int r3 = r5.$appMode
                r1.L0(r2, r3, r0)
            L_0x0068:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x006a:
                return r0
            L_0x006b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0073:
                com.fossil.El7.b(r6)
                com.mapped.Il6 r0 = r5.p$
                com.portfolio.platform.uirenew.home.HomePresenter r2 = r5.this$0
                boolean r2 = com.portfolio.platform.uirenew.home.HomePresenter.x(r2)
                if (r2 == 0) goto L_0x0094
                com.portfolio.platform.uirenew.home.HomePresenter r0 = r5.this$0
                com.mapped.An4 r0 = com.portfolio.platform.uirenew.home.HomePresenter.D(r0)
                java.lang.Boolean r0 = r0.b()
                java.lang.String r1 = "mSharedPreferencesManager.bcStatus()"
                com.mapped.Wg6.b(r0, r1)
                boolean r0 = r0.booleanValue()
                goto L_0x0026
            L_0x0094:
                com.portfolio.platform.uirenew.home.HomePresenter r2 = r5.this$0
                com.portfolio.platform.uirenew.home.HomePresenter.I(r2, r3)
                com.portfolio.platform.uirenew.home.HomePresenter r2 = r5.this$0
                com.portfolio.platform.app_setting.flag.data.FlagRepository r2 = com.portfolio.platform.uirenew.home.HomePresenter.w(r2)
                r5.L$0 = r0
                r5.label = r3
                java.lang.Object r0 = r2.g(r5)
                if (r0 != r1) goto L_0x0013
                r0 = r1
                goto L_0x006a
            L_0x00ab:
                com.portfolio.platform.uirenew.home.HomePresenter r1 = r5.this$0
                com.fossil.Zy5 r1 = com.portfolio.platform.uirenew.home.HomePresenter.F(r1)
                r2 = 0
                int r3 = r5.$appMode
                r1.L0(r2, r3, r0)
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.HomePresenter.Gi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements BleCommandResultManager.Bi {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hi(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            this.a.Q(false);
            boolean booleanExtra = intent.getBooleanExtra("OTA_RESULT", false);
            boolean v0 = this.a.o.v0();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.y.a();
            local.d(a2, "ota complete isSuccess" + booleanExtra + " isDeviceStillOta " + v0);
            if (!v0 || !booleanExtra) {
                this.a.l.A(booleanExtra);
            }
            if (booleanExtra) {
                this.a.m.X0(this.a.o.J(), -1);
                PortfolioApp.get.instance().S1(this.a.r, false, 13);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ii(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            Wg6.c(context, "context");
            Wg6.c(intent, "intent");
            OtaEvent otaEvent = (OtaEvent) intent.getParcelableExtra(Constants.OTA_PROCESS);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HomePresenter.y.a();
            local.d(a2, "---Inside .otaProgressReceiver ota progress " + otaEvent.getProcess() + ", serial=" + otaEvent.getSerial());
            if (!TextUtils.isEmpty(otaEvent.getSerial())) {
                if (!this.a.O()) {
                    this.a.Q(true);
                }
                if (Vt7.j(otaEvent.getSerial(), this.a.o.J(), true)) {
                    this.a.l.o0((int) otaEvent.getProcess());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements CoroutineUseCase.Ei<GetZendeskInformation.Ci, GetZendeskInformation.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ GetZendeskInformation.Ci $responseValue;

            @DexIgnore
            public Aii(GetZendeskInformation.Ci ci) {
                this.$responseValue = ci;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HomePresenter.y.a();
                local.d(a2, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ji(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetZendeskInformation.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(GetZendeskInformation.Ai ai) {
            Wg6.c(ai, "errorValue");
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "startCollectingUserFeedback onError");
        }

        @DexIgnore
        public void c(GetZendeskInformation.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(HomePresenter.y.a(), "startCollectingUserFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(ci.f()).withEmailIdentifier(ci.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(ci.b());
            this.a.l.z3(new Aii(ci));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetZendeskInformation.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements CoroutineUseCase.Ei<UpdateFirmwareUsecase.Di, UpdateFirmwareUsecase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ HomePresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ki(HomePresenter homePresenter) {
            this.a = homePresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(UpdateFirmwareUsecase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(UpdateFirmwareUsecase.Ci ci) {
            Wg6.c(ci, "errorValue");
        }

        @DexIgnore
        public void c(UpdateFirmwareUsecase.Di di) {
            Wg6.c(di, "responseValue");
            this.a.Q(true);
            this.a.l.T1();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(UpdateFirmwareUsecase.Di di) {
            c(di);
        }
    }

    /*
    static {
        String simpleName = HomePresenter.class.getSimpleName();
        Wg6.b(simpleName, "HomePresenter::class.java.simpleName");
        x = simpleName;
    }
    */

    @DexIgnore
    public HomePresenter(Zy5 zy5, An4 an4, DeviceRepository deviceRepository, PortfolioApp portfolioApp, UpdateFirmwareUsecase updateFirmwareUsecase, Uq4 uq4, Cj4 cj4, Bv5 bv5, N47 n47, ServerSettingRepository serverSettingRepository, GetZendeskInformation getZendeskInformation, FlagRepository flagRepository) {
        Wg6.c(zy5, "mView");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(portfolioApp, "mApp");
        Wg6.c(updateFirmwareUsecase, "mUpdateFwUseCase");
        Wg6.c(uq4, "mUseCaseHandler");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(bv5, "mGetServerSettingUseCase");
        Wg6.c(n47, "mUserUtils");
        Wg6.c(serverSettingRepository, "mServerSettingRepository");
        Wg6.c(getZendeskInformation, "mGetZendeskInformation");
        Wg6.c(flagRepository, "flagRepository");
        this.l = zy5;
        this.m = an4;
        this.n = deviceRepository;
        this.o = portfolioApp;
        this.p = updateFirmwareUsecase;
        this.q = uq4;
        this.r = cj4;
        this.s = bv5;
        this.t = n47;
        this.u = serverSettingRepository;
        this.v = getZendeskInformation;
        this.w = flagRepository;
    }

    @DexIgnore
    public final void K() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "checkFirmware(), isSkipOTA=" + this.m.y0());
        if (!PortfolioApp.get.e() || !this.m.y0()) {
            String J = this.o.J();
            boolean v0 = this.o.v0();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "checkFirmware() isDeviceOtaing=" + v0);
            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
            FLogger.Component component = FLogger.Component.APP;
            FLogger.Session session = FLogger.Session.OTHER;
            String str3 = x;
            remote.i(component, session, J, str3, "[Sync OK] Is device OTAing " + v0);
            if (!v0) {
                Rm6 unused = Gu7.d(k(), null, null, new Bi(this, J, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final void L() {
        this.q.a(this.s, new Bv5.Ai.Bii(), new Ci(this));
    }

    @DexIgnore
    public final void M(boolean z) {
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, z, null), 3, null);
    }

    @DexIgnore
    public final void N() {
        String J = this.o.J();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "Inside .checkLastOTASuccess, activeSerial=" + J);
        if (!TextUtils.isEmpty(J)) {
            String Q = this.m.Q(J);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = x;
            local2.d(str2, "Inside .checkLastOTASuccess, lastTempFw=" + Q);
            if (!TextUtils.isEmpty(Q)) {
                Rm6 unused = Gu7.d(k(), null, null, new Ei(this, J, Q, null), 3, null);
            }
        }
    }

    @DexIgnore
    public final boolean O() {
        return this.f;
    }

    @DexIgnore
    public void P(int i2, int i3, Intent intent) {
        this.l.onActivityResult(i2, i3, intent);
    }

    @DexIgnore
    public final void Q(boolean z) {
        this.f = z;
    }

    @DexIgnore
    public void R() {
        this.l.M5(this);
    }

    @DexIgnore
    public final void S() {
        FLogger.INSTANCE.getLocal().d(x, "Inside .updateFirmware");
        String J = this.o.J();
        FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, J, x, "[Sync OK] Start update FW");
        if (!TextUtils.isEmpty(J)) {
            this.p.e(new UpdateFirmwareUsecase.Bi(J, false, 2, null), new Ki(this));
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(x, VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        PortfolioApp portfolioApp = this.o;
        Ii ii = this.j;
        portfolioApp.registerReceiver(ii, new IntentFilter(this.o.getPackageName() + ButtonService.Companion.getACTION_OTA_PROGRESS()));
        BleCommandResultManager.d.e(this.k, CommunicateMode.OTA);
        BleCommandResultManager.d.e(this.i, CommunicateMode.SYNC);
        BleCommandResultManager.d.g(CommunicateMode.SYNC, CommunicateMode.OTA);
        N();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(x, "stop");
        try {
            BleCommandResultManager.d.j(this.k, CommunicateMode.OTA);
            BleCommandResultManager.d.j(this.i, CommunicateMode.SYNC);
            this.o.unregisterReceiver(this.j);
            LiveData<List<InAppNotification>> liveData = this.h;
            if (liveData != null) {
                Zy5 zy5 = this.l;
                if (zy5 != null) {
                    liveData.n((HomeFragment) zy5);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.HomeFragment");
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = x;
            local.d(str, "Exception when stop presenter=" + e2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void n() {
        StorageHelper.a.a();
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void o(InAppNotification inAppNotification) {
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public int p() {
        return this.e;
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public boolean q() {
        return this.f || this.o.v0();
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void r(String str) {
        Wg6.c(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = x;
        local.d(str2, "on active serial change serial " + str);
        Rm6 unused = Gu7.d(k(), null, null, new Gi(this, str, TextUtils.isEmpty(this.o.k0().R()) ? 0 : (TextUtils.isEmpty(str) || !DeviceHelper.o.v(str)) ? 1 : 2, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void s() {
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void t(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = x;
        local.d(str, "setTab " + i2);
        this.e = i2;
    }

    @DexIgnore
    @Override // com.fossil.Yy5
    public void u(String str) {
        Wg6.c(str, "subject");
        this.v.e(new GetZendeskInformation.Bi(str), new Ji(this));
    }
}
