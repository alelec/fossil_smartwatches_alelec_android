package com.portfolio.platform.uirenew.home.profile.workout;

import android.content.Intent;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.R47;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.workoutsetting.WorkoutSetting;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetWorkoutSettingUseCase extends CoroutineUseCase<Ai, Ci, Bi> {
    @DexIgnore
    public /* final */ Di d; // = new Di();
    @DexIgnore
    public List<WorkoutSetting> e;
    @DexIgnore
    public /* final */ WorkoutSettingRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ List<WorkoutSetting> a;

        @DexIgnore
        public Ai(List<WorkoutSetting> list) {
            Wg6.c(list, "workoutSettings");
            this.a = list;
        }

        @DexIgnore
        public final List<WorkoutSetting> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;

        @DexIgnore
        public Bi(int i, int i2, ArrayList<Integer> arrayList) {
            Wg6.c(arrayList, "errorCodes");
            this.a = i2;
            this.b = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Di {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Di implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1", f = "SetWorkoutSettingUseCase.kt", l = {35, 37}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$SetWorkoutSettingReceiver$receive$1$2", f = "SetWorkoutSettingUseCase.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Rm6>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Rm6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        return SetWorkoutSettingUseCase.this.j(new Ci());
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:11:0x003f  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 2
                    r4 = 1
                    java.lang.Object r1 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    if (r0 == 0) goto L_0x0041
                    if (r0 == r4) goto L_0x0020
                    if (r0 != r5) goto L_0x0018
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0015:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x0017:
                    return r0
                L_0x0018:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0020:
                    java.lang.Object r0 = r6.L$1
                    java.util.List r0 = (java.util.List) r0
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x002b:
                    com.fossil.Jx7 r2 = com.fossil.Bw7.c()
                    com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$Di$Aii$Aiii r3 = new com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$Di$Aii$Aiii
                    r4 = 0
                    r3.<init>(r6, r4)
                    r6.L$0 = r0
                    r6.label = r5
                    java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r6)
                    if (r0 != r1) goto L_0x0015
                    r0 = r1
                    goto L_0x0017
                L_0x0041:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r0 = r6.p$
                    com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase r2 = com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase.this
                    java.util.List r2 = com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase.n(r2)
                    if (r2 == 0) goto L_0x002b
                    com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase$Di r3 = r6.this$0
                    com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase r3 = com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase.this
                    com.portfolio.platform.data.source.WorkoutSettingRepository r3 = com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase.o(r3)
                    r6.L$0 = r0
                    r6.L$1 = r2
                    r6.label = r4
                    java.lang.Object r2 = r3.upsertWorkoutSettingList(r2, r6)
                    if (r2 != r1) goto L_0x002b
                    r0 = r1
                    goto L_0x0017
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.profile.workout.SetWorkoutSettingUseCase.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Di() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive()");
            if (communicateMode != CommunicateMode.SET_WORKOUT_DETECTION) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "onReceive() - success");
                Rm6 unused = Gu7.d(SetWorkoutSettingUseCase.this.g(), null, null, new Aii(this, null), 3, null);
                return;
            }
            int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetWorkoutSettingUseCase", "onReceive() - failed, errorCode = " + intExtra);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra);
            }
            SetWorkoutSettingUseCase.this.i(new Bi(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
        }
    }

    @DexIgnore
    public SetWorkoutSettingUseCase(WorkoutSettingRepository workoutSettingRepository) {
        Wg6.c(workoutSettingRepository, "mWorkoutSettingRepository");
        this.f = workoutSettingRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return "SetWorkoutSettingUseCase";
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Ai ai, Xe6 xe6) {
        return q(ai, xe6);
    }

    @DexIgnore
    public final void p() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "registerBroadcastReceiver()");
        BleCommandResultManager.d.e(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
        BleCommandResultManager.d.g(CommunicateMode.SET_WORKOUT_DETECTION);
    }

    @DexIgnore
    public Object q(Ai ai, Xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "executeUseCase()");
        if (ai != null) {
            this.e = ai.a();
            Ao7.f(PortfolioApp.get.instance().O1(PortfolioApp.get.instance().J(), R47.a(ai.a())));
        }
        return new Object();
    }

    @DexIgnore
    public final void r() {
        FLogger.INSTANCE.getLocal().d("SetWorkoutSettingUseCase", "unregisterBroadcastReceiver()");
        BleCommandResultManager.d.j(this.d, CommunicateMode.SET_WORKOUT_DETECTION);
    }
}
