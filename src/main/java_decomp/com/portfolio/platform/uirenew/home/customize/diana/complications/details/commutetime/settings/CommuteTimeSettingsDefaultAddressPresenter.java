package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Um5;
import com.fossil.V76;
import com.fossil.W76;
import com.fossil.Yn7;
import com.fossil.Zq7;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressPresenter extends V76 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ String h; // = Um5.c(PortfolioApp.get.instance(), 2131886354);
    @DexIgnore
    public /* final */ String i; // = Um5.c(PortfolioApp.get.instance(), 2131886355);
    @DexIgnore
    public /* final */ ArrayList<String> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ W76 k;
    @DexIgnore
    public /* final */ An4 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Zq7 $isAddressSaved$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Zq7 $isChanged$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.x().I0(this.this$0.this$0.j);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = y.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(MFUser mFUser, Xe6 xe6, Ai ai) {
                super(2, xe6);
                this.$user = mFUser;
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.$user, xe6, this.this$0);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFUser>> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    MFUser mFUser = this.$user;
                    this.L$0 = il6;
                    this.label = 1;
                    Object updateUser = y.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Dii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFUser>>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $user;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Dii(MFUser mFUser, Xe6 xe6, Ai ai) {
                super(2, xe6);
                this.$user = mFUser;
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Dii dii = new Dii(this.$user, xe6, this.this$0);
                dii.p$ = (Il6) obj;
                throw null;
                //return dii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFUser>> xe6) {
                throw null;
                //return ((Dii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository y = this.this$0.this$0.y();
                    MFUser mFUser = this.$user;
                    this.L$0 = il6;
                    this.label = 1;
                    Object updateUser = y.updateUser(mFUser, false, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(String str, Xe6 xe6, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, String str2, Zq7 zq7, Zq7 zq72) {
            super(2, xe6);
            this.$it = str;
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
            this.$address$inlined = str2;
            this.$isChanged$inlined = zq7;
            this.$isAddressSaved$inlined = zq72;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$it, xe6, this.this$0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:116:0x02ff  */
        /* JADX WARNING: Removed duplicated region for block: B:117:0x030a  */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0081  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00af  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0101  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0150  */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x023b  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
            // Method dump skipped, instructions count: 781
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1$recentSearchedAddress$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<String>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<String>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.x().h();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 i2 = this.this$0.i();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(i2, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Wg6.b(g, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.j.clear();
            this.this$0.j.addAll((List) g);
            this.this$0.e = Places.createClient(PortfolioApp.get.instance());
            this.this$0.z().H(this.this$0.e);
            if (Wg6.a(this.this$0.g, "Home")) {
                W76 z = this.this$0.z();
                String str = this.this$0.h;
                Wg6.b(str, "mHomeTitle");
                z.setTitle(str);
            } else {
                W76 z2 = this.this$0.z();
                String str2 = this.this$0.i;
                Wg6.b(str2, "mWorkTitle");
                z2.setTitle(str2);
            }
            this.this$0.z().V1(this.this$0.f);
            this.this$0.z().h0(this.this$0.j);
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsDefaultAddressPresenter.class.getSimpleName();
        Wg6.b(simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    public CommuteTimeSettingsDefaultAddressPresenter(W76 w76, An4 an4, UserRepository userRepository) {
        Wg6.c(w76, "mView");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(userRepository, "mUserRepository");
        this.k = w76;
        this.l = an4;
        this.m = userRepository;
    }

    @DexIgnore
    public void A(String str, String str2) {
        Wg6.c(str, "addressType");
        Wg6.c(str2, "defaultPlace");
        this.f = str2;
        this.g = str;
    }

    @DexIgnore
    public void B() {
        this.k.M5(this);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.e = null;
    }

    @DexIgnore
    @Override // com.fossil.V76
    public void n(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "onUserExit with address " + str);
        Zq7 zq7 = new Zq7();
        zq7.element = false;
        Zq7 zq72 = new Zq7();
        zq72.element = false;
        if (str == null || Gu7.d(k(), null, null, new Ai(str, null, this, str, zq7, zq72), 3, null) == null) {
            this.k.S3(null);
            Cd6 cd6 = Cd6.a;
        }
    }

    @DexIgnore
    public final An4 x() {
        return this.l;
    }

    @DexIgnore
    public final UserRepository y() {
        return this.m;
    }

    @DexIgnore
    public final W76 z() {
        return this.k;
    }
}
