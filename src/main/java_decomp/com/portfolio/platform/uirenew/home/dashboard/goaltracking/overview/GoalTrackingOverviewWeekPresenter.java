package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ai6;
import com.fossil.Bi6;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hm7;
import com.fossil.Hs0;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Ss0;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.GoalTrackingOverviewWeekFragment;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewWeekPresenter extends Ai6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public /* final */ LiveData<H47<List<GoalTrackingSummary>>> g;
    @DexIgnore
    public BarChart.c h;
    @DexIgnore
    public /* final */ Bi6 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ An4 k;
    @DexIgnore
    public /* final */ GoalTrackingRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter", f = "GoalTrackingOverviewWeekPresenter.kt", l = {148}, m = "calculateStartAndEndDate")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$mGoalTrackingSummaries$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {35, 39, 37}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<GoalTrackingSummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$mGoalTrackingSummaries$1$1$startAndEnd$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {35}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Date, ? extends Date>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Aii aii = this.this$0;
                        GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter = aii.this$0.a;
                        Date date = aii.$it;
                        Wg6.b(date, "it");
                        this.L$0 = il6;
                        this.label = 1;
                        Object w = goalTrackingOverviewWeekPresenter.w(date, this);
                        return w == d ? d : w;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<GoalTrackingSummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r6 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ab
                    if (r0 == r4) goto L_0x0049
                    if (r0 == r5) goto L_0x0027
                    if (r0 != r7) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x001c:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001e:
                    return r0
                L_0x001f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0027:
                    java.lang.Object r0 = r8.L$2
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$1
                    com.mapped.Lc6 r1 = (com.mapped.Lc6) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.Hs0 r2 = (com.fossil.Hs0) r2
                    com.fossil.El7.b(r9)
                    r5 = r0
                    r3 = r9
                L_0x0038:
                    r0 = r3
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r7
                    java.lang.Object r0 = r5.a(r0, r8)
                    if (r0 != r6) goto L_0x001c
                    r0 = r6
                    goto L_0x001e
                L_0x0049:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                    r4 = r0
                    r1 = r9
                L_0x0052:
                    r0 = r1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r1 = "mGoalTrackingSummaries onDateChanged - startDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = ", endDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getSecond()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = "GoalTrackingOverviewWeekPresenter"
                    java.lang.String r3 = r3.toString()
                    r2.d(r1, r3)
                    com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter r1 = r1.a
                    com.portfolio.platform.data.source.GoalTrackingRepository r3 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.q(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    java.lang.Object r2 = r0.getSecond()
                    java.util.Date r2 = (java.util.Date) r2
                    r8.L$0 = r4
                    r8.L$1 = r0
                    r8.L$2 = r4
                    r8.label = r5
                    r5 = 0
                    java.lang.Object r3 = r3.getSummaries(r1, r2, r5, r8)
                    if (r3 != r6) goto L_0x00cb
                    r0 = r6
                    goto L_0x001e
                L_0x00ab:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter r1 = r1.a
                    com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.n(r1)
                    com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Bi$Aii$Aiii r2 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Bi$Aii$Aiii
                    r3 = 0
                    r2.<init>(r8, r3)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                    if (r1 != r6) goto L_0x00d0
                    r0 = r6
                    goto L_0x001e
                L_0x00cb:
                    r2 = r4
                    r1 = r0
                    r5 = r4
                    goto L_0x0038
                L_0x00d0:
                    r4 = r0
                    goto L_0x0052
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            this.a = goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<GoalTrackingSummary>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<GoalTrackingSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {65}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1$1$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super BarChart.c>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super BarChart.c> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter = this.this$0.this$0.a;
                        Date date = goalTrackingOverviewWeekPresenter.e;
                        if (date != null) {
                            return goalTrackingOverviewWeekPresenter.z(date, this.this$0.$data);
                        }
                        Wg6.i();
                        throw null;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter2 = this.this$0.a;
                    Dv7 h = goalTrackingOverviewWeekPresenter2.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.L$1 = goalTrackingOverviewWeekPresenter2;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                    goalTrackingOverviewWeekPresenter = goalTrackingOverviewWeekPresenter2;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                    goalTrackingOverviewWeekPresenter = (GoalTrackingOverviewWeekPresenter) this.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                goalTrackingOverviewWeekPresenter.h = (BarChart.c) g;
                Bi6 bi6 = this.this$0.a.i;
                BarChart.c cVar = this.this$0.a.h;
                if (cVar == null) {
                    cVar = new BarChart.c(0, 0, null, 7, null);
                }
                bi6.p(cVar);
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
            this.a = goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<GoalTrackingSummary>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mGoalTrackingSummaries -- GoalTrackingSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list, null), 3, null);
                this.a.i.x(!this.a.k.g0());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<GoalTrackingSummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public GoalTrackingOverviewWeekPresenter(Bi6 bi6, UserRepository userRepository, An4 an4, GoalTrackingRepository goalTrackingRepository) {
        Wg6.c(bi6, "mView");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(an4, "mSharedPreferencesManager");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        this.i = bi6;
        this.j = userRepository;
        this.k = an4;
        this.l = goalTrackingRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<H47<List<GoalTrackingSummary>>> c = Ss0.c(mutableLiveData, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = c;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        x();
        Date date = this.e;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewWeekPresenter", "start with date " + this.e);
        LiveData<H47<List<GoalTrackingSummary>>> liveData = this.g;
        Bi6 bi6 = this.i;
        if (bi6 != null) {
            liveData.h((GoalTrackingOverviewWeekFragment) bi6, new Ci(this));
            this.i.x(!this.k.g0());
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "stop");
        try {
            LiveData<H47<List<GoalTrackingSummary>>> liveData = this.g;
            Bi6 bi6 = this.i;
            if (bi6 != null) {
                liveData.n((GoalTrackingOverviewWeekFragment) bi6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(java.util.Date r6, com.mapped.Xe6<? super com.mapped.Lc6<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.Ai
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Ai r0 = (com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.mapped.Jh6 r0 = (com.mapped.Jh6) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter r2 = (com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.mapped.TimeUtils.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.mapped.TimeUtils.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Ai r0 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$Ai
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r3)
            com.mapped.Jh6 r0 = new com.mapped.Jh6
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.mapped.TimeUtils.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r1 = r6
            r4 = r0
            goto L_0x0031
        L_0x008a:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter.w(java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public void x() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void y() {
        this.i.M5(this);
    }

    @DexIgnore
    public final BarChart.c z(Date date, List<GoalTrackingSummary> list) {
        T t;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, null, 7, null);
        Calendar instance = Calendar.getInstance(Locale.US);
        Wg6.b(instance, "calendar");
        instance.setTime(date);
        instance.add(5, -6);
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        while (i4 <= 6) {
            Date time = instance.getTime();
            Wg6.b(time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (TimeUtils.m0(next.getDate(), instance.getTime())) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    i3 = t2.getGoalTarget();
                    int totalTracked = t2.getTotalTracked();
                    i2 = Math.max(Math.max(i3, totalTracked), i2);
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, totalTracked, null, 23, null))), time2, i4 == 6));
                } else {
                    cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
                }
            } else {
                cVar.b().add(new BarChart.a(i3, Hm7.c(Hm7.c(new BarChart.b(0, null, 0, 0, null, 23, null))), time2, i4 == 6));
            }
            instance.add(5, 1);
            i3 = i3;
            i2 = i2;
            i4++;
        }
        if (i2 > 0) {
            i3 = i2;
        }
        cVar.f(i3);
        return cVar;
    }
}
