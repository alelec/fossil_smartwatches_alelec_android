package com.portfolio.platform.uirenew.home.dashboard.goaltracking;

import androidx.lifecycle.LiveData;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ch6;
import com.fossil.Dh6;
import com.fossil.Eh6;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cf;
import com.mapped.Coroutine;
import com.mapped.Gg6;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.PagingRequestHelper;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.U04;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashboardGoalTrackingPresenter extends Ch6 {
    @DexIgnore
    public Date e; // = new Date();
    @DexIgnore
    public Listing<GoalTrackingSummary> f;
    @DexIgnore
    public /* final */ Dh6 g;
    @DexIgnore
    public /* final */ GoalTrackingRepository h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ U04 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1", f = "DashboardGoalTrackingPresenter.kt", l = {55, 62}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DashboardGoalTrackingPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii implements PagingRequestHelper.Ai {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Aii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            @Override // com.mapped.PagingRequestHelper.Ai
            public final void e(PagingRequestHelper.Gi gi) {
                Wg6.c(gi, "report");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("DashboardGoalTrackingPresenter", "onStatusChange status=" + gi);
                if (gi.b()) {
                    this.a.this$0.w().d();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Bii<T> implements Ls0<Cf<GoalTrackingSummary>> {
            @DexIgnore
            public /* final */ /* synthetic */ Ai a;

            @DexIgnore
            public Bii(Ai ai) {
                this.a = ai;
            }

            @DexIgnore
            public final void a(Cf<GoalTrackingSummary> cf) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummariesPaging observer size=");
                sb.append(cf != null ? Integer.valueOf(cf.size()) : null);
                local.d("DashboardGoalTrackingPresenter", sb.toString());
                if (cf != null) {
                    this.a.this$0.w().q(cf);
                }
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // com.fossil.Ls0
            public /* bridge */ /* synthetic */ void onChanged(Cf<GoalTrackingSummary> cf) {
                a(cf);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$initDataSource$1$user$1", f = "DashboardGoalTrackingPresenter.kt", l = {55}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.i;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(DashboardGoalTrackingPresenter dashboardGoalTrackingPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = dashboardGoalTrackingPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0066  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r8 = 2
                r5 = 1
                java.lang.Object r4 = com.fossil.Yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0098
                if (r0 == r5) goto L_0x0058
                if (r0 != r8) goto L_0x0050
                java.lang.Object r0 = r9.L$4
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r0 = (com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter) r0
                java.lang.Object r1 = r9.L$3
                java.util.Date r1 = (java.util.Date) r1
                java.lang.Object r1 = r9.L$2
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r9.L$1
                com.portfolio.platform.data.model.MFUser r1 = (com.portfolio.platform.data.model.MFUser) r1
                java.lang.Object r1 = r9.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0027:
                r0 = r1
                com.portfolio.platform.data.Listing r0 = (com.portfolio.platform.data.Listing) r0
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.v(r2, r0)
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r0 = r9.this$0
                com.fossil.Dh6 r0 = r0.w()
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r1 = r9.this$0
                com.portfolio.platform.data.Listing r1 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.t(r1)
                if (r1 == 0) goto L_0x004d
                androidx.lifecycle.LiveData r1 = r1.getPagedList()
                if (r1 == 0) goto L_0x004d
                if (r0 == 0) goto L_0x00b8
                com.fossil.Eh6 r0 = (com.fossil.Eh6) r0
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Bii r2 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Bii
                r2.<init>(r9)
                r1.h(r0, r2)
            L_0x004d:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x004f:
                return r0
            L_0x0050:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0058:
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
                r2 = r0
                r1 = r10
            L_0x0061:
                r0 = r1
                com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
                if (r0 == 0) goto L_0x004d
                java.lang.String r1 = r0.getCreatedAt()
                java.util.Date r1 = com.mapped.TimeUtils.q0(r1)
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r3 = r9.this$0
                com.portfolio.platform.data.source.GoalTrackingRepository r5 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.s(r3)
                java.lang.String r6 = "createdDate"
                com.mapped.Wg6.b(r1, r6)
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r6 = r9.this$0
                com.mapped.U04 r6 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.r(r6)
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Aii r7 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Aii
                r7.<init>(r9)
                r9.L$0 = r2
                r9.L$1 = r0
                r9.L$2 = r0
                r9.L$3 = r1
                r9.L$4 = r3
                r9.label = r8
                java.lang.Object r1 = r5.getSummariesPaging(r1, r6, r7, r9)
                if (r1 != r4) goto L_0x00b5
                r0 = r4
                goto L_0x004f
            L_0x0098:
                com.fossil.El7.b(r10)
                com.mapped.Il6 r0 = r9.p$
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter r1 = r9.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.q(r1)
                com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Cii r2 = new com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter$Ai$Cii
                r3 = 0
                r2.<init>(r9, r3)
                r9.L$0 = r0
                r9.label = r5
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r9)
                if (r1 != r4) goto L_0x00c0
                r0 = r4
                goto L_0x004f
            L_0x00b5:
                r2 = r3
                goto L_0x0027
            L_0x00b8:
                com.mapped.Rc6 r0 = new com.mapped.Rc6
                java.lang.String r1 = "null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment"
                r0.<init>(r1)
                throw r0
            L_0x00c0:
                r2 = r0
                goto L_0x0061
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public DashboardGoalTrackingPresenter(Dh6 dh6, GoalTrackingRepository goalTrackingRepository, UserRepository userRepository, U04 u04) {
        Wg6.c(dh6, "mView");
        Wg6.c(goalTrackingRepository, "mGoalTrackingRepository");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(u04, "mAppExecutors");
        this.g = dh6;
        this.h = goalTrackingRepository;
        this.i = userRepository;
        this.j = u04;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        if (!TimeUtils.p0(this.e).booleanValue()) {
            this.e = new Date();
            Listing<GoalTrackingSummary> listing = this.f;
            if (listing != null) {
                listing.getRefresh();
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "stop");
    }

    @DexIgnore
    @Override // com.fossil.Ch6
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Ch6
    public void o() {
        LiveData<Cf<GoalTrackingSummary>> pagedList;
        try {
            Dh6 dh6 = this.g;
            Listing<GoalTrackingSummary> listing = this.f;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                if (dh6 != null) {
                    pagedList.n((Eh6) dh6);
                } else {
                    throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingFragment");
                }
            }
            this.h.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(Cd6.a);
            local.e("DashboardGoalTrackingPresenter", sb.toString());
        }
    }

    @DexIgnore
    @Override // com.fossil.Ch6
    public void p() {
        Gg6<Cd6> retry;
        FLogger.INSTANCE.getLocal().d("DashboardGoalTrackingPresenter", "retry all failed request");
        Listing<GoalTrackingSummary> listing = this.f;
        if (listing != null && (retry = listing.getRetry()) != null) {
            retry.invoke();
        }
    }

    @DexIgnore
    public final Dh6 w() {
        return this.g;
    }

    @DexIgnore
    public void x() {
        this.g.M5(this);
    }
}
