package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.os.Bundle;
import android.text.TextUtils;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Ab6;
import com.fossil.Bb6;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Pm7;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.zendesk.sdk.network.impl.ZendeskBlipsProvider;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSearchPresenter extends Ab6 {
    @DexIgnore
    public String e; // = "empty";
    @DexIgnore
    public String f; // = "empty";
    @DexIgnore
    public String g; // = "empty";
    @DexIgnore
    public String h;
    @DexIgnore
    public ArrayList<WatchApp> i; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<WatchApp> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ Bb6 k;
    @DexIgnore
    public /* final */ WatchAppRepository l;
    @DexIgnore
    public /* final */ An4 m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $query;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$search$1$1", f = "WatchAppSearchPresenter.kt", l = {73}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<WatchApp>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object queryWatchAppByName;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    String str = this.this$0.$query;
                    this.L$0 = il6;
                    this.label = 1;
                    queryWatchAppByName = watchAppRepository.queryWatchAppByName(str, this);
                    if (queryWatchAppByName == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    queryWatchAppByName = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Pm7.j0((Collection) queryWatchAppByName);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WatchAppSearchPresenter watchAppSearchPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppSearchPresenter;
            this.$query = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, this.$query, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            List arrayList;
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                arrayList = new ArrayList();
                if (this.$query.length() > 0) {
                    Dv7 i2 = this.this$0.i();
                    Aii aii = new Aii(this, null);
                    this.L$0 = il6;
                    this.L$1 = arrayList;
                    this.label = 1;
                    g = Eu7.g(i2, aii, this);
                    if (g == d) {
                        return d;
                    }
                }
                this.this$0.k.B(this.this$0.C(arrayList));
                this.this$0.h = this.$query;
                return Cd6.a;
            } else if (i == 1) {
                List list = (List) this.L$1;
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            arrayList = (List) g;
            this.this$0.k.B(this.this$0.C(arrayList));
            this.this$0.h = this.$query;
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1", f = "WatchAppSearchPresenter.kt", l = {40, 43}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchAppSearchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allSearchedWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {43}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends WatchApp>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    List<String> T = this.this$0.this$0.m.T();
                    Wg6.b(T, "sharedPreferencesManager.watchAppSearchedIdsRecent");
                    this.L$0 = il6;
                    this.label = 1;
                    Object watchAppByIds = watchAppRepository.getWatchAppByIds(T, this);
                    return watchAppByIds == d ? d : watchAppByIds;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {40}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends WatchApp>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends WatchApp>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    WatchAppRepository watchAppRepository = this.this$0.this$0.l;
                    this.L$0 = il6;
                    this.label = 1;
                    Object allWatchAppRaw = watchAppRepository.getAllWatchAppRaw(this);
                    return allWatchAppRaw == d ? d : allWatchAppRaw;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(WatchAppSearchPresenter watchAppSearchPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchAppSearchPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0074  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r3 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0076
                if (r0 == r4) goto L_0x003f
                if (r0 != r5) goto L_0x0037
                java.lang.Object r0 = r7.L$1
                java.util.List r0 = (java.util.List) r0
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x001b:
                java.util.List r0 = (java.util.List) r0
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.r(r1)
                r1.clear()
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.r(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r0 = r7.this$0
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.w(r0)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0036:
                return r0
            L_0x0037:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x003f:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r2 = r0
                r1 = r8
            L_0x0048:
                r0 = r1
                java.util.List r0 = (java.util.List) r0
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.s(r1)
                r1.clear()
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                java.util.ArrayList r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.s(r1)
                r1.addAll(r0)
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.q(r1)
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$Bi$Aii r4 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$Bi$Aii
                r4.<init>(r7, r6)
                r7.L$0 = r2
                r7.L$1 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r1, r4, r7)
                if (r0 != r3) goto L_0x001b
                r0 = r3
                goto L_0x0036
            L_0x0076:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter r1 = r7.this$0
                com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.q(r1)
                com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$Bi$Bii r2 = new com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$Bi$Bii
                r2.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r7)
                if (r1 != r3) goto L_0x0092
                r0 = r3
                goto L_0x0036
            L_0x0092:
                r2 = r0
                goto L_0x0048
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public WatchAppSearchPresenter(Bb6 bb6, WatchAppRepository watchAppRepository, An4 an4) {
        Wg6.c(bb6, "mView");
        Wg6.c(watchAppRepository, "mWatchAppRepository");
        Wg6.c(an4, "sharedPreferencesManager");
        this.k = bb6;
        this.l = watchAppRepository;
        this.m = an4;
    }

    @DexIgnore
    public Bundle A(Bundle bundle) {
        if (bundle != null) {
            bundle.putString(ViewHierarchy.DIMENSION_TOP_KEY, this.e);
            bundle.putString("middle", this.f);
            bundle.putString("bottom", this.g);
        }
        return bundle;
    }

    @DexIgnore
    public void B() {
        this.k.M5(this);
    }

    @DexIgnore
    public final List<Lc6<WatchApp, String>> C(List<WatchApp> list) {
        ArrayList arrayList = new ArrayList();
        for (WatchApp watchApp : list) {
            if (!Wg6.a(watchApp.getWatchappId(), "empty")) {
                String watchappId = watchApp.getWatchappId();
                if (Wg6.a(watchappId, this.e)) {
                    arrayList.add(new Lc6(watchApp, ViewHierarchy.DIMENSION_TOP_KEY));
                } else if (Wg6.a(watchappId, this.f)) {
                    arrayList.add(new Lc6(watchApp, "middle"));
                } else if (Wg6.a(watchappId, this.g)) {
                    arrayList.add(new Lc6(watchApp, "bottom"));
                } else {
                    arrayList.add(new Lc6(watchApp, ""));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void D(String str, String str2, String str3) {
        Wg6.c(str, "watchAppTop");
        Wg6.c(str2, "watchAppMiddle");
        Wg6.c(str3, "watchAppBottom");
        this.e = str;
        this.g = str3;
        this.f = str2;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Ab6
    public void n() {
        this.h = "";
        this.k.D();
        z();
    }

    @DexIgnore
    @Override // com.fossil.Ab6
    public void o(WatchApp watchApp) {
        Wg6.c(watchApp, "selectedWatchApp");
        List<String> T = this.m.T();
        Wg6.b(T, "sharedPreferencesManager.watchAppSearchedIdsRecent");
        if (!T.contains(watchApp.getWatchappId())) {
            T.add(0, watchApp.getWatchappId());
            if (T.size() > 5) {
                T = T.subList(0, 5);
            }
            this.m.W1(T);
        }
        this.k.D3(watchApp);
    }

    @DexIgnore
    @Override // com.fossil.Ab6
    public void p(String str) {
        Wg6.c(str, ZendeskBlipsProvider.BLIP_QUERY_FIELD_NAME);
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, str, null), 3, null);
    }

    @DexIgnore
    public final void z() {
        if (this.j.isEmpty()) {
            this.k.B(C(Pm7.j0(this.i)));
        } else {
            this.k.K(C(Pm7.j0(this.j)));
        }
        if (!TextUtils.isEmpty(this.h)) {
            Bb6 bb6 = this.k;
            String str = this.h;
            if (str != null) {
                bb6.z(str);
                String str2 = this.h;
                if (str2 != null) {
                    p(str2);
                } else {
                    Wg6.i();
                    throw null;
                }
            } else {
                Wg6.i();
                throw null;
            }
        }
    }
}
