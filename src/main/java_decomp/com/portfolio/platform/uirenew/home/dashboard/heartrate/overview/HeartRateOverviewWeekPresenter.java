package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.share.internal.VideoUploader;
import com.fossil.Ao7;
import com.fossil.Dv7;
import com.fossil.Ej6;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Fj6;
import com.fossil.Gj6;
import com.fossil.Gu7;
import com.fossil.H47;
import com.fossil.Hs0;
import com.fossil.Im7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Or0;
import com.fossil.Pm7;
import com.fossil.Ss0;
import com.fossil.Um5;
import com.fossil.Xh5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.TimeUtils;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewWeekPresenter extends Ej6 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public /* final */ MutableLiveData<Date> f;
    @DexIgnore
    public /* final */ LiveData<H47<List<DailyHeartRateSummary>>> g;
    @DexIgnore
    public List<String> h; // = new ArrayList();
    @DexIgnore
    public /* final */ Fj6 i;
    @DexIgnore
    public /* final */ UserRepository j;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter", f = "HeartRateOverviewWeekPresenter.kt", l = {111}, m = "calculateStartAndEndDate")
    public static final class Ai extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$mHeartRateSummaries$1$1", f = "HeartRateOverviewWeekPresenter.kt", l = {38, 42, 40}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Hs0<H47<? extends List<DailyHeartRateSummary>>>, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Date $it;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public int label;
            @DexIgnore
            public Hs0 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$mHeartRateSummaries$1$1$startAndEnd$1", f = "HeartRateOverviewWeekPresenter.kt", l = {38}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Lc6<? extends Date, ? extends Date>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Lc6<? extends Date, ? extends Date>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        Aii aii = this.this$0;
                        HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter = aii.this$0.a;
                        Date date = aii.$it;
                        Wg6.b(date, "it");
                        this.L$0 = il6;
                        this.label = 1;
                        Object s = heartRateOverviewWeekPresenter.s(date, this);
                        return s == d ? d : s;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Date date, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$it = date;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$it, xe6);
                aii.p$ = (Hs0) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Hs0<H47<? extends List<DailyHeartRateSummary>>> hs0, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(hs0, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
            /* JADX WARNING: Removed duplicated region for block: B:16:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:20:0x00cb  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r9) {
                /*
                    r8 = this;
                    r7 = 3
                    r5 = 2
                    r4 = 1
                    java.lang.Object r6 = com.fossil.Yn7.d()
                    int r0 = r8.label
                    if (r0 == 0) goto L_0x00ab
                    if (r0 == r4) goto L_0x0049
                    if (r0 == r5) goto L_0x0027
                    if (r0 != r7) goto L_0x001f
                    java.lang.Object r0 = r8.L$1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                L_0x001c:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001e:
                    return r0
                L_0x001f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0027:
                    java.lang.Object r0 = r8.L$2
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    java.lang.Object r1 = r8.L$1
                    com.mapped.Lc6 r1 = (com.mapped.Lc6) r1
                    java.lang.Object r2 = r8.L$0
                    com.fossil.Hs0 r2 = (com.fossil.Hs0) r2
                    com.fossil.El7.b(r9)
                    r5 = r0
                    r3 = r9
                L_0x0038:
                    r0 = r3
                    androidx.lifecycle.LiveData r0 = (androidx.lifecycle.LiveData) r0
                    r8.L$0 = r2
                    r8.L$1 = r1
                    r8.label = r7
                    java.lang.Object r0 = r5.a(r0, r8)
                    if (r0 != r6) goto L_0x001c
                    r0 = r6
                    goto L_0x001e
                L_0x0049:
                    java.lang.Object r0 = r8.L$0
                    com.fossil.Hs0 r0 = (com.fossil.Hs0) r0
                    com.fossil.El7.b(r9)
                    r4 = r0
                    r1 = r9
                L_0x0052:
                    r0 = r1
                    com.mapped.Lc6 r0 = (com.mapped.Lc6) r0
                    com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                    com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r1.getLocal()
                    java.lang.StringBuilder r3 = new java.lang.StringBuilder
                    r3.<init>()
                    java.lang.String r1 = "mHeartRateSummaries onDateChanged - startDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = ", endDate="
                    r3.append(r1)
                    java.lang.Object r1 = r0.getSecond()
                    java.util.Date r1 = (java.util.Date) r1
                    r3.append(r1)
                    java.lang.String r1 = "HeartRateOverviewWeekPresenter"
                    java.lang.String r3 = r3.toString()
                    r2.d(r1, r3)
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter r1 = r1.a
                    com.portfolio.platform.data.source.HeartRateSummaryRepository r3 = com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.o(r1)
                    java.lang.Object r1 = r0.getFirst()
                    java.util.Date r1 = (java.util.Date) r1
                    java.lang.Object r2 = r0.getSecond()
                    java.util.Date r2 = (java.util.Date) r2
                    r8.L$0 = r4
                    r8.L$1 = r0
                    r8.L$2 = r4
                    r8.label = r5
                    r5 = 0
                    java.lang.Object r3 = r3.getHeartRateSummaries(r1, r2, r5, r8)
                    if (r3 != r6) goto L_0x00cb
                    r0 = r6
                    goto L_0x001e
                L_0x00ab:
                    com.fossil.El7.b(r9)
                    com.fossil.Hs0 r0 = r8.p$
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Bi r1 = r8.this$0
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter r1 = r1.a
                    com.fossil.Dv7 r1 = com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.n(r1)
                    com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Bi$Aii$Aiii r2 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Bi$Aii$Aiii
                    r3 = 0
                    r2.<init>(r8, r3)
                    r8.L$0 = r0
                    r8.label = r4
                    java.lang.Object r1 = com.fossil.Eu7.g(r1, r2, r8)
                    if (r1 != r6) goto L_0x00d0
                    r0 = r6
                    goto L_0x001e
                L_0x00cb:
                    r2 = r4
                    r1 = r0
                    r5 = r4
                    goto L_0x0038
                L_0x00d0:
                    r4 = r0
                    goto L_0x0052
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.Bi.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        public Bi(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            this.a = heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final LiveData<H47<List<DailyHeartRateSummary>>> a(Date date) {
            return Or0.c(null, 0, new Aii(this, date, null), 3, null);
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((Date) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci<T> implements Ls0<H47<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewWeekPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$1", f = "HeartRateOverviewWeekPresenter.kt", l = {66}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ List $data;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$start$1$1$listRestingDataPoint$1", f = "HeartRateOverviewWeekPresenter.kt", l = {}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Integer>>, Object> {
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Aii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Aii aii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super List<? extends Integer>> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Yn7.d();
                    if (this.label == 0) {
                        El7.b(obj);
                        List<T> list = this.this$0.$data;
                        if (list == null) {
                            return null;
                        }
                        ArrayList arrayList = new ArrayList(Im7.m(list, 10));
                        for (T t : list) {
                            Resting resting = t.getResting();
                            arrayList.add(resting != null ? Ao7.e(resting.getValue()) : null);
                        }
                        return arrayList;
                    }
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, List list, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
                this.$data = list;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$data, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Dv7 h = this.this$0.a.h();
                    Aiii aiii = new Aiii(this, null);
                    this.L$0 = il6;
                    this.label = 1;
                    g = Eu7.g(h, aiii, this);
                    if (g == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List list = (List) g;
                if (list != null) {
                    this.this$0.a.i.q5(Pm7.j0(list), this.this$0.a.h);
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        public Ci(HeartRateOverviewWeekPresenter heartRateOverviewWeekPresenter) {
            this.a = heartRateOverviewWeekPresenter;
        }

        @DexIgnore
        public final void a(H47<? extends List<DailyHeartRateSummary>> h47) {
            Xh5 a2 = h47.a();
            List list = (List) h47.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSummaries -- heartRateSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            local.d("HeartRateOverviewWeekPresenter", sb.toString());
            if (a2 != Xh5.DATABASE_LOADING) {
                Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, list, null), 3, null);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(H47<? extends List<DailyHeartRateSummary>> h47) {
            a(h47);
        }
    }

    @DexIgnore
    public HeartRateOverviewWeekPresenter(Fj6 fj6, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        Wg6.c(fj6, "mView");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.i = fj6;
        this.j = userRepository;
        this.k = heartRateSummaryRepository;
        MutableLiveData<Date> mutableLiveData = new MutableLiveData<>();
        this.f = mutableLiveData;
        LiveData<H47<List<DailyHeartRateSummary>>> c = Ss0.c(mutableLiveData, new Bi(this));
        Wg6.b(c, "Transformations.switchMa\u2026, false))\n        }\n    }");
        this.g = c;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewWeekPresenter", VideoUploader.PARAM_VALUE_UPLOAD_START_PHASE);
        Date date = this.e;
        if (date == null || !TimeUtils.p0(date).booleanValue()) {
            Date date2 = new Date();
            this.e = date2;
            this.f.l(date2);
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HeartRateOverviewWeekPresenter", "start with date " + this.e);
        t();
        LiveData<H47<List<DailyHeartRateSummary>>> liveData = this.g;
        Fj6 fj6 = this.i;
        if (fj6 != null) {
            liveData.h((Gj6) fj6, new Ci(this));
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        MFLogger.d("HeartRateOverviewWeekPresenter", "stop");
        try {
            LiveData<H47<List<DailyHeartRateSummary>>> liveData = this.g;
            Fj6 fj6 = this.i;
            if (fj6 != null) {
                liveData.n((Gj6) fj6);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekFragment");
        } catch (Exception e2) {
            MFLogger.d("HeartRateOverviewWeekPresenter", "stop exception " + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object s(java.util.Date r6, com.mapped.Xe6<? super com.mapped.Lc6<? extends java.util.Date, ? extends java.util.Date>> r7) {
        /*
            r5 = this;
            r4 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.Ai
            if (r0 == 0) goto L_0x0057
            r0 = r7
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Ai r0 = (com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.Ai) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0057
            int r1 = r1 + r3
            r0.label = r1
            r2 = r0
        L_0x0014:
            java.lang.Object r3 = r2.result
            java.lang.Object r1 = com.fossil.Yn7.d()
            int r0 = r2.label
            if (r0 == 0) goto L_0x0066
            if (r0 != r4) goto L_0x005e
            java.lang.Object r0 = r2.L$2
            com.mapped.Jh6 r0 = (com.mapped.Jh6) r0
            java.lang.Object r1 = r2.L$1
            java.util.Date r1 = (java.util.Date) r1
            java.lang.Object r2 = r2.L$0
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter r2 = (com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter) r2
            com.fossil.El7.b(r3)
            r2 = r3
            r4 = r0
        L_0x0031:
            r0 = r2
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCreatedAt()
            if (r0 == 0) goto L_0x008a
            java.util.Date r2 = com.mapped.TimeUtils.q0(r0)
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            boolean r0 = com.mapped.TimeUtils.j0(r0, r2)
            if (r0 != 0) goto L_0x004c
            r4.element = r2
        L_0x004c:
            com.mapped.Lc6 r2 = new com.mapped.Lc6
            T r0 = r4.element
            java.util.Date r0 = (java.util.Date) r0
            r2.<init>(r0, r1)
            r0 = r2
        L_0x0056:
            return r0
        L_0x0057:
            com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Ai r0 = new com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter$Ai
            r0.<init>(r5, r7)
            r2 = r0
            goto L_0x0014
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r3)
            com.mapped.Jh6 r0 = new com.mapped.Jh6
            r0.<init>()
            r3 = 6
            java.util.Date r3 = com.mapped.TimeUtils.Q(r6, r3)
            r0.element = r3
            com.portfolio.platform.data.source.UserRepository r3 = r5.j
            r2.L$0 = r5
            r2.L$1 = r6
            r2.L$2 = r0
            r2.label = r4
            java.lang.Object r2 = r3.getCurrentUser(r2)
            if (r2 != r1) goto L_0x0087
            r0 = r1
            goto L_0x0056
        L_0x0087:
            r1 = r6
            r4 = r0
            goto L_0x0031
        L_0x008a:
            com.mapped.Wg6.i()
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewWeekPresenter.s(java.util.Date, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void t() {
        this.h.clear();
        Calendar instance = Calendar.getInstance();
        Wg6.b(instance, "calendar");
        instance.setTime(this.e);
        instance.add(5, -6);
        for (int i2 = 1; i2 <= 7; i2++) {
            Boolean p0 = TimeUtils.p0(instance.getTime());
            Wg6.b(p0, "DateHelper.isToday(calendar.time)");
            if (p0.booleanValue()) {
                List<String> list = this.h;
                String c = Um5.c(PortfolioApp.get.instance(), 2131886648);
                Wg6.b(c, "LanguageHelper.getString\u2026_Sleep7days_Label__Today)");
                list.add(c);
            } else {
                switch (instance.get(7)) {
                    case 1:
                        List<String> list2 = this.h;
                        String c2 = Um5.c(PortfolioApp.get.instance(), 2131886770);
                        Wg6.b(c2, "LanguageHelper.getString\u2026Main_Steps7days_Label__S)");
                        list2.add(c2);
                        continue;
                    case 2:
                        List<String> list3 = this.h;
                        String c3 = Um5.c(PortfolioApp.get.instance(), 2131886769);
                        Wg6.b(c3, "LanguageHelper.getString\u2026Main_Steps7days_Label__M)");
                        list3.add(c3);
                        continue;
                    case 3:
                        List<String> list4 = this.h;
                        String c4 = Um5.c(PortfolioApp.get.instance(), 2131886772);
                        Wg6.b(c4, "LanguageHelper.getString\u2026Main_Steps7days_Label__T)");
                        list4.add(c4);
                        continue;
                    case 4:
                        List<String> list5 = this.h;
                        String c5 = Um5.c(PortfolioApp.get.instance(), 2131886774);
                        Wg6.b(c5, "LanguageHelper.getString\u2026Main_Steps7days_Label__W)");
                        list5.add(c5);
                        continue;
                    case 5:
                        List<String> list6 = this.h;
                        String c6 = Um5.c(PortfolioApp.get.instance(), 2131886773);
                        Wg6.b(c6, "LanguageHelper.getString\u2026in_Steps7days_Label__T_1)");
                        list6.add(c6);
                        continue;
                    case 6:
                        List<String> list7 = this.h;
                        String c7 = Um5.c(PortfolioApp.get.instance(), 2131886768);
                        Wg6.b(c7, "LanguageHelper.getString\u2026Main_Steps7days_Label__F)");
                        list7.add(c7);
                        continue;
                    case 7:
                        List<String> list8 = this.h;
                        String c8 = Um5.c(PortfolioApp.get.instance(), 2131886771);
                        Wg6.b(c8, "LanguageHelper.getString\u2026in_Steps7days_Label__S_1)");
                        list8.add(c8);
                        continue;
                }
            }
            instance.add(5, 1);
        }
    }

    @DexIgnore
    public void u() {
        this.i.M5(this);
    }
}
