package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.E56;
import com.fossil.F56;
import com.fossil.I56;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridAppActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public I56 A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<String> arrayList) {
            Wg6.c(fragment, "fragment");
            Wg6.c(arrayList, "stringAppsSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridAppActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putStringArrayListExtra("LIST_APPS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 4567);
        }
    }

    /*
    static {
        Wg6.b(NotificationHybridAppActivity.class.getSimpleName(), "NotificationHybridAppAct\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        E56 e56 = (E56) getSupportFragmentManager().Y(2131362158);
        if (e56 == null) {
            e56 = E56.k.b();
            k(e56, E56.k.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (e56 != null) {
            iface.j(new F56(e56, getIntent().getIntExtra("HAND_NUMBER", 0), getIntent().getStringArrayListExtra("LIST_APPS_SELECTED"))).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.app.NotificationHybridAppContract.View");
    }
}
