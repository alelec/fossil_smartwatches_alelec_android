package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.lifecycle.MutableLiveData;
import com.facebook.share.internal.ShareConstants;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.N66;
import com.fossil.Ry5;
import com.fossil.Ts0;
import com.fossil.Us0;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.fossil.imagefilters.EInkImageFilter;
import com.fossil.imagefilters.FilterResult;
import com.fossil.imagefilters.FilterType;
import com.fossil.imagefilters.Format;
import com.fossil.imagefilters.OutputSettings;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.FileHelper;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.watchface.edit.photobackground.data.source.WFBackgroundPhotoRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoViewModel extends Ts0 {
    @DexIgnore
    public MutableLiveData<Di> a; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Bi> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Ai> c; // = new MutableLiveData<>();
    @DexIgnore
    public ArrayList<N66> d;
    @DexIgnore
    public /* final */ WFBackgroundPhotoRepository e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ FilterResult a;

        @DexIgnore
        public Ai(FilterResult filterResult) {
            Wg6.c(filterResult, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.a = filterResult;
        }

        @DexIgnore
        public final FilterResult a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Ai) && Wg6.a(this.a, ((Ai) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            FilterResult filterResult = this.a;
            if (filterResult != null) {
                return filterResult.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "ApplyFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ List<FilterResult> a;

        @DexIgnore
        public Bi(List<FilterResult> list) {
            Wg6.c(list, ShareConstants.WEB_DIALOG_PARAM_FILTERS);
            this.a = list;
        }

        @DexIgnore
        public final List<FilterResult> a() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            return this == obj || ((obj instanceof Bi) && Wg6.a(this.a, ((Bi) obj).a));
        }

        @DexIgnore
        public int hashCode() {
            List<FilterResult> list = this.a;
            if (list != null) {
                return list.hashCode();
            }
            return 0;
        }

        @DexIgnore
        public String toString() {
            return "BuildFiltersResult(filters=" + this.a + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci {
        @DexIgnore
        public /* final */ Bitmap a;
        @DexIgnore
        public /* final */ float[] b;
        @DexIgnore
        public /* final */ int c;
        @DexIgnore
        public /* final */ boolean d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ int f;
        @DexIgnore
        public /* final */ boolean g;
        @DexIgnore
        public /* final */ boolean h;

        @DexIgnore
        public Ci(Bitmap bitmap, float[] fArr, int i, boolean z, int i2, int i3, boolean z2, boolean z3) {
            Wg6.c(bitmap, "bitmap");
            Wg6.c(fArr, "points");
            this.a = bitmap;
            this.b = fArr;
            this.c = i;
            this.d = z;
            this.e = i2;
            this.f = i3;
            this.g = z2;
            this.h = z3;
        }

        @DexIgnore
        public final int a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.f;
        }

        @DexIgnore
        public final Bitmap c() {
            return this.a;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final boolean e() {
            return this.d;
        }

        @DexIgnore
        public final boolean f() {
            return this.g;
        }

        @DexIgnore
        public final boolean g() {
            return this.h;
        }

        @DexIgnore
        public final float[] h() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di {
        @DexIgnore
        public /* final */ boolean a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Di() {
            this(false, null, 3, null);
        }

        @DexIgnore
        public Di(boolean z, String str) {
            Wg6.c(str, "data");
            this.a = z;
            this.b = str;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ Di(boolean z, String str, int i, Qg6 qg6) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? "" : str);
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final boolean b() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Di) {
                    Di di = (Di) obj;
                    if (this.a != di.a || !Wg6.a(this.b, di.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            boolean z = this.a;
            if (z) {
                z = true;
            }
            String str = this.b;
            int hashCode = str != null ? str.hashCode() : 0;
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (i * 31) + hashCode;
        }

        @DexIgnore
        public String toString() {
            return "CropImageResult(isSuccess=" + this.a + ", data=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1", f = "EditPhotoViewModel.kt", l = {83}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filter;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$applyFilter$1$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    EInkImageFilter create = EInkImageFilter.create();
                    Wg6.b(create, "EInkImageFilter.create()");
                    OutputSettings outputSettings = new OutputSettings(this.this$0.$bitmap.getWidth(), this.this$0.$bitmap.getHeight(), Format.RAW, false, false);
                    Ei ei = this.this$0;
                    FilterResult apply = create.apply(ei.$bitmap, ei.$filter, false, false, outputSettings);
                    Wg6.b(apply, "algorithm.apply(bitmap, \u2026, false, false, settings)");
                    this.this$0.this$0.e().l(new Ai(apply));
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(EditPhotoViewModel editPhotoViewModel, Bitmap bitmap, FilterType filterType, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$bitmap = bitmap;
            this.$filter = filterType;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, this.$bitmap, this.$filter, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1", f = "EditPhotoViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Ci $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList $filterList;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$buildFilterList$1$filters$1", f = "EditPhotoViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super ArrayList<FilterResult>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Fi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Fi fi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = fi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super ArrayList<FilterResult>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    Ci ci = this.this$0.$cropImageInfo;
                    Bitmap a2 = Ry5.a(Ry5.h(ci.c(), ci.h(), ci.d(), ci.e(), ci.a(), ci.b(), ci.f(), ci.g()).a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    Wg6.b(create, "EInkImageFilter.create()");
                    Wg6.b(a2, "bitmap");
                    return create.applyFilters(a2, this.this$0.$filterList, false, false, new OutputSettings(a2.getWidth(), a2.getHeight(), Format.RAW, false, false));
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(EditPhotoViewModel editPhotoViewModel, Ci ci, ArrayList arrayList, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$cropImageInfo = ci;
            this.$filterList = arrayList;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$cropImageInfo, this.$filterList, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Wg6.b(g, "withContext(IO) {\n      \u2026          }\n            }");
            this.this$0.f().l(new Bi((ArrayList) g));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1", f = "EditPhotoViewModel.kt", l = {43}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Context $context;
        @DexIgnore
        public /* final */ /* synthetic */ Ci $cropImageInfo;
        @DexIgnore
        public /* final */ /* synthetic */ FilterType $filterType;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ EditPhotoViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel$cropImage$1$1", f = "EditPhotoViewModel.kt", l = {56}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    Ci ci = this.this$0.$cropImageInfo;
                    Ry5.Ai h = Ry5.h(ci.c(), ci.h(), ci.d(), ci.e(), ci.a(), ci.b(), ci.f(), ci.g());
                    Bitmap a2 = Ry5.a(h.a, 480, 480);
                    EInkImageFilter create = EInkImageFilter.create();
                    Wg6.b(create, "EInkImageFilter.create()");
                    FilterResult apply = create.apply(a2, this.this$0.$filterType, false, false, OutputSettings.BACKGROUND);
                    Wg6.b(apply, "algorithm.apply(rawBitma\u2026utputSettings.BACKGROUND)");
                    Bitmap s = Ry5.s(apply.getPreview(), false);
                    String e = Ym5.e(24);
                    String str2 = FileUtils.getDirectory(this.this$0.$context, FileType.WATCH_FACE) + File.separator;
                    FileHelper fileHelper = FileHelper.a;
                    Wg6.b(e, "imageName");
                    Wg6.b(s, "circleBitmap");
                    fileHelper.e(str2, e, s);
                    WFBackgroundPhotoRepository wFBackgroundPhotoRepository = this.this$0.this$0.e;
                    String str3 = str2 + e;
                    this.L$0 = il6;
                    this.L$1 = ci;
                    this.L$2 = h;
                    this.L$3 = a2;
                    this.L$4 = create;
                    this.L$5 = apply;
                    this.L$6 = s;
                    this.L$7 = e;
                    this.L$8 = str2;
                    this.label = 1;
                    if (wFBackgroundPhotoRepository.f(e, str3, this) == d) {
                        return d;
                    }
                    str = e;
                } else if (i == 1) {
                    String str4 = (String) this.L$8;
                    str = (String) this.L$7;
                    Bitmap bitmap = (Bitmap) this.L$6;
                    FilterResult filterResult = (FilterResult) this.L$5;
                    EInkImageFilter eInkImageFilter = (EInkImageFilter) this.L$4;
                    Bitmap bitmap2 = (Bitmap) this.L$3;
                    Ry5.Ai ai = (Ry5.Ai) this.L$2;
                    Ci ci2 = (Ci) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                MutableLiveData<Di> h2 = this.this$0.this$0.h();
                Wg6.b(str, "imageName");
                h2.l(new Di(true, str));
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(EditPhotoViewModel editPhotoViewModel, Ci ci, FilterType filterType, Context context, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = editPhotoViewModel;
            this.$cropImageInfo = ci;
            this.$filterType = filterType;
            this.$context = context;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$cropImageInfo, this.$filterType, this.$context, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        System.loadLibrary("EInkImageFilter");
    }
    */

    @DexIgnore
    public EditPhotoViewModel(WFBackgroundPhotoRepository wFBackgroundPhotoRepository) {
        Wg6.c(wFBackgroundPhotoRepository, "wfBackgroundPhotoRepository");
        this.e = wFBackgroundPhotoRepository;
    }

    @DexIgnore
    public final void b(Bitmap bitmap, FilterType filterType) {
        Wg6.c(bitmap, "bitmap");
        Wg6.c(filterType, "filter");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "applyFilter()");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ei(this, bitmap, filterType, null), 3, null);
    }

    @DexIgnore
    public final void c(Ci ci, ArrayList<FilterType> arrayList) {
        Wg6.c(ci, "cropImageInfo");
        Wg6.c(arrayList, "filterList");
        FLogger.INSTANCE.getLocal().d("EditPhotoViewModel", "buildFilterList()");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Fi(this, ci, arrayList, null), 3, null);
    }

    @DexIgnore
    public final void d(Ci ci, FilterType filterType) {
        Wg6.c(ci, "cropImageInfo");
        Wg6.c(filterType, "filterType");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("EditPhotoViewModel", "cropImage(), filterType = " + filterType);
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Gi(this, ci, filterType, PortfolioApp.get.instance().getApplicationContext(), null), 3, null);
    }

    @DexIgnore
    public final MutableLiveData<Ai> e() {
        return this.c;
    }

    @DexIgnore
    public final MutableLiveData<Bi> f() {
        return this.b;
    }

    @DexIgnore
    public final ArrayList<N66> g() {
        return this.d;
    }

    @DexIgnore
    public final MutableLiveData<Di> h() {
        return this.a;
    }

    @DexIgnore
    public final void i(ArrayList<N66> arrayList) {
        this.d = arrayList;
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        Jv7.d(Us0.a(this), null, 1, null);
        super.onCleared();
    }
}
