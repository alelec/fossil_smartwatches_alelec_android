package com.portfolio.platform.uirenew.home.customize.hybrid;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Bw7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.Ls0;
import com.fossil.Ss0;
import com.fossil.Ts0;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.CustomizeConfigurationExt;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.V3;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppLastSettingRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridCustomizeViewModel extends Ts0 {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ Ai q; // = new Ai(null);
    @DexIgnore
    public HybridPreset a;
    @DexIgnore
    public MutableLiveData<HybridPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ ArrayList<MicroApp> d; // = new ArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> e; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> f; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<MicroApp> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Gson h; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> i;
    @DexIgnore
    public /* final */ LiveData<MicroApp> j;
    @DexIgnore
    public /* final */ LiveData<Boolean> k;
    @DexIgnore
    public /* final */ Ls0<HybridPreset> l;
    @DexIgnore
    public /* final */ HybridPresetRepository m;
    @DexIgnore
    public /* final */ MicroAppLastSettingRepository n;
    @DexIgnore
    public /* final */ MicroAppRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return HybridCustomizeViewModel.p;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi<T> implements Ls0<HybridPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public Bi(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for r0v8, resolved type: androidx.lifecycle.MutableLiveData */
        /* JADX WARN: Multi-variable type inference failed */
        public final void a(HybridPreset hybridPreset) {
            T t;
            boolean z;
            FLogger.INSTANCE.getLocal().d(HybridCustomizeViewModel.q.a(), "current preset change=" + hybridPreset);
            if (hybridPreset != null) {
                String str = (String) this.a.e.e();
                String str2 = (String) this.a.f.e();
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    T t2 = next;
                    if (!Wg6.a(t2.getPosition(), str) || Vt7.j(t2.getAppId(), str2, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        t = next;
                        break;
                    }
                }
                T t3 = t;
                if (t3 != null) {
                    FLogger.INSTANCE.getLocal().d(HybridCustomizeViewModel.q.a(), "Update new microapp id=" + t3.getAppId() + " at position=" + str);
                    this.a.f.l(t3.getAppId());
                }
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.fossil.Ls0
        public /* bridge */ /* synthetic */ void onChanged(HybridPreset hybridPreset) {
            a(hybridPreset);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1", f = "HybridCustomizeViewModel.kt", l = {102, 107}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$init$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.b.i(this.this$0.this$0.l);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(HybridCustomizeViewModel hybridCustomizeViewModel, String str, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$presetId, this.$microAppPos, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 2
                r6 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0048
                if (r0 == r6) goto L_0x0020
                if (r0 != r7) goto L_0x0018
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0015:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0017:
                return r0
            L_0x0018:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0020:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0027:
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.h(r2)
                java.lang.String r3 = r8.$microAppPos
                r2.l(r3)
                com.fossil.Jx7 r2 = com.fossil.Bw7.c()
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ci$Aii r3 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ci$Aii
                r4 = 0
                r3.<init>(r8, r4)
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r8)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0017
            L_0x0048:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ai r3 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.q
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "init presetId="
                r4.append(r5)
                java.lang.String r5 = r8.$presetId
                r4.append(r5)
                java.lang.String r5 = " originalPreset="
                r4.append(r5)
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r5 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.e(r5)
                r4.append(r5)
                java.lang.String r5 = " microAppPos="
                r4.append(r5)
                java.lang.String r5 = r8.$microAppPos
                r4.append(r5)
                r5 = 32
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r3, r4)
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.e(r2)
                if (r2 != 0) goto L_0x0027
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                java.lang.String r3 = r8.$presetId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.x(r3, r8)
                if (r2 != r1) goto L_0x0027
                r0 = r1
                goto L_0x0017
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1", f = "HybridCustomizeViewModel.kt", l = {118, 125}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $microAppPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ HybridPreset $savedOriginalPreset;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initFromSaveInstanceState$1$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    this.this$0.this$0.b.i(this.this$0.this$0.l);
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(HybridCustomizeViewModel hybridCustomizeViewModel, String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
            this.$savedOriginalPreset = hybridPreset;
            this.$savedCurrentPreset = hybridPreset2;
            this.$microAppPos = str2;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$microAppPos, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0055  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r9) {
            /*
                r8 = this;
                r7 = 2
                r6 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r8.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r6) goto L_0x002f
                if (r0 != r7) goto L_0x0027
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0015:
                com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r8.$savedCurrentPreset
                if (r0 == 0) goto L_0x0024
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r0 = r8.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.a(r0)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r1 = r8.$savedCurrentPreset
                r0.l(r1)
            L_0x0024:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0026:
                return r0
            L_0x0027:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x002f:
                java.lang.Object r0 = r8.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r9)
            L_0x0036:
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                androidx.lifecycle.MutableLiveData r2 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.h(r2)
                java.lang.String r3 = r8.$microAppPos
                r2.l(r3)
                com.fossil.Jx7 r2 = com.fossil.Bw7.c()
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Di$Aii r3 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Di$Aii
                r4 = 0
                r3.<init>(r8, r4)
                r8.L$0 = r0
                r8.label = r7
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r8)
                if (r0 != r1) goto L_0x0015
                r0 = r1
                goto L_0x0026
            L_0x0057:
                com.fossil.El7.b(r9)
                com.mapped.Il6 r0 = r8.p$
                com.misfit.frameworks.buttonservice.log.FLogger r2 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
                com.misfit.frameworks.buttonservice.log.ILocalFLogger r2 = r2.getLocal()
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ai r3 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.q
                java.lang.String r3 = r3.a()
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "initFromSaveInstanceState presetId "
                r4.append(r5)
                java.lang.String r5 = r8.$presetId
                r4.append(r5)
                java.lang.String r5 = " savedOriginalPreset="
                r4.append(r5)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = r8.$savedOriginalPreset
                r4.append(r5)
                r5 = 44
                r4.append(r5)
                java.lang.String r5 = " savedCurrentPreset="
                r4.append(r5)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r5 = r8.$savedCurrentPreset
                r4.append(r5)
                java.lang.String r5 = " microAppPos="
                r4.append(r5)
                java.lang.String r5 = r8.$microAppPos
                r4.append(r5)
                java.lang.String r5 = " currentPreset"
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                r2.d(r3, r4)
                com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r8.$savedOriginalPreset
                if (r2 != 0) goto L_0x00bb
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                java.lang.String r3 = r8.$presetId
                r8.L$0 = r0
                r8.label = r6
                java.lang.Object r2 = r2.x(r3, r8)
                if (r2 != r1) goto L_0x0036
                r0 = r1
                goto L_0x0026
            L_0x00bb:
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.j(r2)
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r2 = r8.this$0
                com.portfolio.platform.data.model.room.microapp.HybridPreset r3 = r8.$savedOriginalPreset
                com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.l(r2, r3)
                goto L_0x0036
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel", f = "HybridCustomizeViewModel.kt", l = {263, 264}, m = "initializePreset")
    public static final class Ei extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(HybridCustomizeViewModel hybridCustomizeViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = hybridCustomizeViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.x(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$2", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(HybridCustomizeViewModel hybridCustomizeViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                this.this$0.w();
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$initializePreset$preset$1", f = "HybridCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Gi extends Ko7 implements Coroutine<Il6, Xe6<? super HybridPreset>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(HybridCustomizeViewModel hybridCustomizeViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = hybridCustomizeViewModel;
            this.$presetId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Gi gi = new Gi(this.this$0, this.$presetId, xe6);
            gi.p$ = (Il6) obj;
            throw null;
            //return gi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super HybridPreset> xe6) {
            throw null;
            //return ((Gi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return this.this$0.m.getPresetById(this.$presetId);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public Hi(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final MutableLiveData<MicroApp> a(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.q.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            HybridCustomizeViewModel hybridCustomizeViewModel = this.a;
            Wg6.b(str, "id");
            this.a.g.l(hybridCustomizeViewModel.n(str));
            return this.a.g;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public Ii(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final MutableLiveData<String> a(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = HybridCustomizeViewModel.q.a();
            local.d(a2, "transformMicroAppPosToId pos=" + str);
            HybridPreset hybridPreset = (HybridPreset) this.a.b.e();
            if (hybridPreset != null) {
                Iterator<T> it = hybridPreset.getButtons().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getPosition(), str)) {
                        t = next;
                        break;
                    }
                }
                T t2 = t;
                if (t2 != null) {
                    this.a.f.o(t2.getAppId());
                }
            }
            return this.a.f;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((String) obj);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji<I, O> implements V3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HybridCustomizeViewModel a;

        @DexIgnore
        public Ji(HybridCustomizeViewModel hybridCustomizeViewModel) {
            this.a = hybridCustomizeViewModel;
        }

        @DexIgnore
        public final MutableLiveData<Boolean> a(HybridPreset hybridPreset) {
            ArrayList<HybridPresetAppSetting> buttons = hybridPreset.getButtons();
            Gson gson = this.a.h;
            HybridPreset hybridPreset2 = this.a.a;
            if (hybridPreset2 != null) {
                boolean h = CustomizeConfigurationExt.h(buttons, gson, hybridPreset2.getButtons());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = HybridCustomizeViewModel.q.a();
                local.d(a2, "isTheSameHybridAppSetting " + h);
                if (h) {
                    this.a.c.l(Boolean.FALSE);
                } else {
                    this.a.c.l(Boolean.TRUE);
                }
                return this.a.c;
            }
            Wg6.i();
            throw null;
        }

        @DexIgnore
        @Override // com.mapped.V3
        public /* bridge */ /* synthetic */ Object apply(Object obj) {
            return a((HybridPreset) obj);
        }
    }

    /*
    static {
        String simpleName = HybridCustomizeViewModel.class.getSimpleName();
        Wg6.b(simpleName, "HybridCustomizeViewModel::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public HybridCustomizeViewModel(HybridPresetRepository hybridPresetRepository, MicroAppLastSettingRepository microAppLastSettingRepository, MicroAppRepository microAppRepository) {
        Wg6.c(hybridPresetRepository, "mHybridPresetRepository");
        Wg6.c(microAppLastSettingRepository, "mMicroAppLastSettingRepository");
        Wg6.c(microAppRepository, "mMicroAppRepository");
        this.m = hybridPresetRepository;
        this.n = microAppLastSettingRepository;
        this.o = microAppRepository;
        LiveData<String> c2 = Ss0.c(this.e, new Ii(this));
        Wg6.b(c2, "Transformations.switchMa\u2026dMicroAppIdLiveData\n    }");
        this.i = c2;
        LiveData<MicroApp> c3 = Ss0.c(c2, new Hi(this));
        Wg6.b(c3, "Transformations.switchMa\u2026tedMicroAppLiveData\n    }");
        this.j = c3;
        LiveData<Boolean> c4 = Ss0.c(this.b, new Ji(this));
        Wg6.b(c4, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.k = c4;
        this.l = new Bi(this);
    }

    @DexIgnore
    public final void A(String str) {
        Wg6.c(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = p;
        local.d(str2, "setSelectedMicroApp watchAppPos=" + str);
        this.e.l(str);
    }

    @DexIgnore
    public final void B(HybridPreset hybridPreset) {
        Wg6.c(hybridPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "savePreset newPreset=" + hybridPreset);
        this.b.l(hybridPreset.clone());
    }

    @DexIgnore
    public final List<MicroApp> m(String str) {
        Wg6.c(str, "category");
        ArrayList<MicroApp> arrayList = this.d;
        ArrayList arrayList2 = new ArrayList();
        for (T t : arrayList) {
            if (t.getCategories().contains(str)) {
                arrayList2.add(t);
            }
        }
        return arrayList2;
    }

    @DexIgnore
    public final MicroApp n(String str) {
        T t;
        Wg6.c(str, "microAppId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            T next = it.next();
            if (Wg6.a(str, next.getId())) {
                t = next;
                break;
            }
        }
        return t;
    }

    @DexIgnore
    public final MutableLiveData<HybridPreset> o() {
        return this.b;
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = p;
        local.d(str, "onCleared originalPreset=" + this.a + " currentPreset=" + this.b.e());
        this.b.m(this.l);
        super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> p() {
        return this.k;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v23, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting */
    /* JADX DEBUG: Multi-variable search result rejected for r0v27, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0045, code lost:
        if (r1 != null) goto L_0x0047;
     */
    @DexIgnore
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Parcelable q(java.lang.String r8) {
        /*
        // Method dump skipped, instructions count: 336
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.q(java.lang.String):android.os.Parcelable");
    }

    @DexIgnore
    public final HybridPreset r() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<MicroApp> s() {
        LiveData<MicroApp> liveData = this.j;
        if (liveData != null) {
            return liveData;
        }
        Wg6.i();
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> t() {
        return this.e;
    }

    @DexIgnore
    public final Rm6 u(String str, String str2) {
        Wg6.c(str, "presetId");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Ci(this, str, str2, null), 3, null);
    }

    @DexIgnore
    public final Rm6 v(String str, HybridPreset hybridPreset, HybridPreset hybridPreset2, String str2) {
        Wg6.c(str, "presetId");
        return Gu7.d(Jv7.a(Bw7.a()), null, null, new Di(this, str, hybridPreset, hybridPreset2, str2, null), 3, null);
    }

    @DexIgnore
    public final void w() {
        this.d.clear();
        this.d.addAll(this.o.getAllMicroApp(PortfolioApp.get.instance().J()));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object x(java.lang.String r10, com.mapped.Xe6<? super com.mapped.Cd6> r11) {
        /*
            r9 = this;
            r8 = 0
            r7 = 2
            r6 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r11 instanceof com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.Ei
            if (r0 == 0) goto L_0x0047
            r0 = r11
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ei r0 = (com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.Ei) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0047
            int r1 = r1 + r3
            r0.label = r1
            r3 = r0
        L_0x0016:
            java.lang.Object r2 = r3.result
            java.lang.Object r4 = com.fossil.Yn7.d()
            int r0 = r3.label
            if (r0 == 0) goto L_0x007e
            if (r0 == r6) goto L_0x0056
            if (r0 != r7) goto L_0x004e
            java.lang.Object r0 = r3.L$2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            java.lang.Object r1 = r3.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r1 = (com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel) r1
            com.fossil.El7.b(r2)
        L_0x0033:
            if (r0 == 0) goto L_0x0044
            com.portfolio.platform.data.model.room.microapp.HybridPreset r2 = r0.clone()
            r1.a = r2
            androidx.lifecycle.MutableLiveData<com.portfolio.platform.data.model.room.microapp.HybridPreset> r1 = r1.b
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = r0.clone()
            r1.l(r0)
        L_0x0044:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0046:
            return r0
        L_0x0047:
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ei r0 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Ei
            r0.<init>(r9, r11)
            r3 = r0
            goto L_0x0016
        L_0x004e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0056:
            java.lang.Object r0 = r3.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r1 = r3.L$0
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel r1 = (com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel) r1
            com.fossil.El7.b(r2)
            r10 = r0
        L_0x0062:
            r0 = r2
            com.portfolio.platform.data.model.room.microapp.HybridPreset r0 = (com.portfolio.platform.data.model.room.microapp.HybridPreset) r0
            com.fossil.Dv7 r2 = com.fossil.Bw7.a()
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Fi r5 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Fi
            r5.<init>(r1, r8)
            r3.L$0 = r1
            r3.L$1 = r10
            r3.L$2 = r0
            r3.label = r7
            java.lang.Object r2 = com.fossil.Eu7.g(r2, r5, r3)
            if (r2 != r4) goto L_0x0033
            r0 = r4
            goto L_0x0046
        L_0x007e:
            com.fossil.El7.b(r2)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.String r1 = com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.p
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r5 = "initializePreset presetId="
            r2.append(r5)
            r2.append(r10)
            java.lang.String r2 = r2.toString()
            r0.d(r1, r2)
            com.fossil.Dv7 r0 = com.fossil.Bw7.b()
            com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Gi r1 = new com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel$Gi
            r1.<init>(r9, r10, r8)
            r3.L$0 = r9
            r3.L$1 = r10
            r3.label = r6
            java.lang.Object r2 = com.fossil.Eu7.g(r0, r1, r3)
            if (r2 != r4) goto L_0x00b4
            r0 = r4
            goto L_0x0046
        L_0x00b4:
            r1 = r9
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.home.customize.hybrid.HybridCustomizeViewModel.x(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final boolean y(String str) {
        Wg6.c(str, "microAppId");
        HybridPreset e2 = this.b.e();
        if (e2 != null) {
            Iterator<HybridPresetAppSetting> it = e2.getButtons().iterator();
            while (it.hasNext()) {
                if (Wg6.a(it.next().getAppId(), str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean z() {
        Boolean e2 = this.c.e();
        if (e2 != null) {
            return e2.booleanValue();
        }
        return false;
    }
}
