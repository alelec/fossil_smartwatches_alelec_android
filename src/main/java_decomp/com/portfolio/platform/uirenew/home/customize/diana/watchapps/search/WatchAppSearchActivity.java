package com.portfolio.platform.uirenew.home.customize.diana.watchapps.search;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import androidx.fragment.app.Fragment;
import com.facebook.appevents.codeless.internal.ViewHierarchy;
import com.fossil.Cb6;
import com.fossil.Db6;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSearchActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public WatchAppSearchPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, String str, String str2, String str3) {
            Wg6.c(fragment, "fragment");
            Wg6.c(str, "watchAppTop");
            Wg6.c(str2, "watchAppMiddle");
            Wg6.c(str3, "watchAppBottom");
            Intent intent = new Intent(fragment.getContext(), WatchAppSearchActivity.class);
            intent.putExtra(ViewHierarchy.DIMENSION_TOP_KEY, str);
            intent.putExtra("middle", str2);
            intent.putExtra("bottom", str3);
            fragment.startActivityForResult(intent, 101, ActivityOptions.makeSceneTransitionAnimation(fragment.getActivity(), new Pair[0]).toBundle());
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Cb6 cb6 = (Cb6) getSupportFragmentManager().Y(2131362158);
        if (cb6 == null) {
            cb6 = Cb6.l.b();
            k(cb6, Cb6.l.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (cb6 != null) {
            iface.f1(new Db6(cb6)).a(this);
            Intent intent = getIntent();
            Wg6.b(intent, "intent");
            Bundle extras = intent.getExtras();
            if (extras != null) {
                WatchAppSearchPresenter watchAppSearchPresenter = this.A;
                if (watchAppSearchPresenter != null) {
                    String string = extras.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string == null) {
                        string = "empty";
                    }
                    String string2 = extras.getString("middle");
                    if (string2 == null) {
                        string2 = "empty";
                    }
                    String string3 = extras.getString("bottom");
                    if (string3 == null) {
                        string3 = "empty";
                    }
                    watchAppSearchPresenter.D(string, string2, string3);
                } else {
                    Wg6.n("mPresenter");
                    throw null;
                }
            }
            if (bundle != null) {
                WatchAppSearchPresenter watchAppSearchPresenter2 = this.A;
                if (watchAppSearchPresenter2 != null) {
                    String string4 = bundle.getString(ViewHierarchy.DIMENSION_TOP_KEY);
                    if (string4 == null) {
                        string4 = "empty";
                    }
                    String string5 = bundle.getString("middle");
                    if (string5 == null) {
                        string5 = "empty";
                    }
                    String string6 = bundle.getString("bottom");
                    if (string6 == null) {
                        string6 = "empty";
                    }
                    watchAppSearchPresenter2.D(string4, string5, string6);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchContract.View");
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        WatchAppSearchPresenter watchAppSearchPresenter = this.A;
        if (watchAppSearchPresenter != null) {
            watchAppSearchPresenter.A(bundle);
            if (bundle != null) {
                super.onSaveInstanceState(bundle);
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            Wg6.n("mPresenter");
            throw null;
        }
    }
}
