package com.portfolio.platform.uirenew.home.customize.diana.theme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.mapped.EditPhotoFragment;
import com.mapped.Rc6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EditPhotoActivity extends BaseActivity {
    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        Intent intent;
        super.onCreate(bundle);
        setContentView(2131558439);
        if (((EditPhotoFragment) getSupportFragmentManager().Y(2131362158)) == null && (intent = getIntent()) != null) {
            Uri uri = null;
            if (intent.hasExtra("IMAGE_URI_EXTRA")) {
                uri = (Uri) intent.getParcelableExtra("IMAGE_URI_EXTRA");
            }
            if (uri != null) {
                EditPhotoFragment a2 = EditPhotoFragment.u.a(uri);
                if (a2 != null) {
                    k(a2, "EditPhotoFragment", 2131362158);
                    return;
                }
                throw new Rc6("null cannot be cast to non-null type androidx.fragment.app.Fragment");
            }
            FLogger.INSTANCE.getLocal().d(r(), "Can not start EditPhotoFragment with invalid params");
        }
    }
}
