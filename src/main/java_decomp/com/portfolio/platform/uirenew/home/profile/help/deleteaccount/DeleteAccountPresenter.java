package com.portfolio.platform.uirenew.home.profile.help.deleteaccount;

import androidx.fragment.app.FragmentActivity;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Kp6;
import com.fossil.Xp6;
import com.fossil.Yn7;
import com.fossil.Yp6;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.profile.usecase.GetZendeskInformation;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAccountPresenter extends Xp6 {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ Ai k; // = new Ai(null);
    @DexIgnore
    public /* final */ Yp6 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ AnalyticsHelper g;
    @DexIgnore
    public /* final */ GetZendeskInformation h;
    @DexIgnore
    public /* final */ DeleteLogoutUserUseCase i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return DeleteAccountPresenter.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<DeleteLogoutUserUseCase.Di, DeleteLogoutUserUseCase.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DeleteLogoutUserUseCase.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(DeleteLogoutUserUseCase.Ci ci) {
            Wg6.c(ci, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "deleteUser - onError");
            this.a.e.k();
            this.a.e.o(ci.a(), "");
        }

        @DexIgnore
        public void c(DeleteLogoutUserUseCase.Di di) {
            Wg6.c(di, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "deleteUser - onSuccess");
            this.a.e.k();
            if (((Kp6) this.a.e).isActive()) {
                this.a.e.a6();
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DeleteLogoutUserUseCase.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1", f = "DeleteAccountPresenter.kt", l = {108}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendFeedbackSuccessfullyEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super SKUModel> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.get.instance().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(DeleteAccountPresenter deleteAccountPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteAccountPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                this.this$0.g.l("feedback_submit", hashMap);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1", f = "DeleteAccountPresenter.kt", l = {93}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "DeleteAccountPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super SKUModel>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super SKUModel> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.get.instance().J());
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(DeleteAccountPresenter deleteAccountPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = deleteAccountPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) g;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.g.l("feedback_open", hashMap);
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Ei<GetZendeskInformation.Ci, GetZendeskInformation.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAccountPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends BaseZendeskFeedbackConfiguration {
            @DexIgnore
            public /* final */ /* synthetic */ GetZendeskInformation.Ci $responseValue;

            @DexIgnore
            public Aii(GetZendeskInformation.Ci ci) {
                this.$responseValue = ci;
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public String getAdditionalInfo() {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DeleteAccountPresenter.k.a();
                local.d(a2, "Inside. getAdditionalInfo: \n" + this.$responseValue.a());
                return this.$responseValue.a();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration
            public String getRequestSubject() {
                FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "getRequestSubject");
                return this.$responseValue.d();
            }

            @DexIgnore
            @Override // com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration, com.zendesk.sdk.feedback.BaseZendeskFeedbackConfiguration
            public List<String> getTags() {
                FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "getTags");
                return this.$responseValue.e();
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ei(DeleteAccountPresenter deleteAccountPresenter) {
            this.a = deleteAccountPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetZendeskInformation.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(GetZendeskInformation.Ai ai) {
            Wg6.c(ai, "errorValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "sendFeedback onError");
        }

        @DexIgnore
        public void c(GetZendeskInformation.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(DeleteAccountPresenter.k.a(), "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(ci.f()).withEmailIdentifier(ci.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(ci.b());
            this.a.e.j0(new Aii(ci));
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetZendeskInformation.Ci ci) {
            c(ci);
        }
    }

    /*
    static {
        String simpleName = DeleteAccountPresenter.class.getSimpleName();
        Wg6.b(simpleName, "DeleteAccountPresenter::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public DeleteAccountPresenter(Yp6 yp6, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, UserRepository userRepository, GetZendeskInformation getZendeskInformation, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        Wg6.c(yp6, "mView");
        Wg6.c(deviceRepository, "mDeviceRepository");
        Wg6.c(analyticsHelper, "mAnalyticsHelper");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(getZendeskInformation, "mGetZendeskInformation");
        Wg6.c(deleteLogoutUserUseCase, "mDeleteLogoutUserUseCase");
        this.e = yp6;
        this.f = deviceRepository;
        this.g = analyticsHelper;
        this.h = getZendeskInformation;
        this.i = deleteLogoutUserUseCase;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        FLogger.INSTANCE.getLocal().d(j, "presenter start");
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        FLogger.INSTANCE.getLocal().d(j, "presenter stop");
    }

    @DexIgnore
    @Override // com.fossil.Xp6
    public void n() {
        FLogger.INSTANCE.getLocal().d(j, "deleteUser");
        this.e.m();
        DeleteLogoutUserUseCase deleteLogoutUserUseCase = this.i;
        Yp6 yp6 = this.e;
        if (yp6 != null) {
            FragmentActivity activity = ((Kp6) yp6).getActivity();
            if (activity != null) {
                deleteLogoutUserUseCase.e(new DeleteLogoutUserUseCase.Bi(0, new WeakReference(activity)), new Bi(this));
                return;
            }
            throw new Rc6("null cannot be cast to non-null type android.app.Activity");
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.profile.help.DeleteAccountFragment");
    }

    @DexIgnore
    @Override // com.fossil.Xp6
    public void o() {
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Xp6
    public void p() {
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Xp6
    public void q(String str) {
        Wg6.c(str, "subject");
        this.h.e(new GetZendeskInformation.Bi(str), new Ei(this));
    }

    @DexIgnore
    public void v() {
        this.e.M5(this);
    }
}
