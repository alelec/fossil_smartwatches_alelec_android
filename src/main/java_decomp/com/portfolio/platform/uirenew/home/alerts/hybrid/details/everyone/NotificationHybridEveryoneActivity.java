package com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone;

import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.D66;
import com.fossil.E66;
import com.fossil.J06;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridEveryoneActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public NotificationHybridEveryonePresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, ArrayList<J06> arrayList) {
            Wg6.c(fragment, "fragment");
            Wg6.c(arrayList, "contactWrappersSelected");
            Intent intent = new Intent(fragment.getContext(), NotificationHybridEveryoneActivity.class);
            intent.putExtra("HAND_NUMBER", i);
            intent.putExtra("LIST_CONTACTS_SELECTED", arrayList);
            intent.setFlags(603979776);
            fragment.startActivityForResult(intent, 6789);
        }
    }

    /*
    static {
        Wg6.b(NotificationHybridEveryoneActivity.class.getSimpleName(), "NotificationHybridEveryo\u2026ty::class.java.simpleName");
    }
    */

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        D66 d66 = (D66) getSupportFragmentManager().Y(2131362158);
        if (d66 == null) {
            d66 = D66.k.b();
            k(d66, D66.k.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (d66 != null) {
            iface.q(new E66(d66, getIntent().getIntExtra("HAND_NUMBER", 0), (ArrayList) getIntent().getSerializableExtra("LIST_CONTACTS_SELECTED"))).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.hybrid.details.everyone.NotificationHybridEveryoneContract.View");
    }
}
