package com.portfolio.platform.uirenew.welcome;

import android.text.Html;
import android.text.Spanned;
import com.fossil.Ao7;
import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.F27;
import com.fossil.G27;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.M47;
import com.fossil.Um5;
import com.fossil.Vt7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.SoLibraryLoader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WelcomePresenter extends F27 {
    @DexIgnore
    public /* final */ G27 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1", f = "WelcomePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WelcomePresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.welcome.WelcomePresenter$navigateToAccountCreation$1$isAA$1", f = "WelcomePresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return Ao7.a(SoLibraryLoader.f().c(PortfolioApp.get.instance()) != null);
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WelcomePresenter welcomePresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = welcomePresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.this$0, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(b, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (((Boolean) g).booleanValue()) {
                this.this$0.p().V5();
            } else {
                this.this$0.p().d2();
            }
            return Cd6.a;
        }
    }

    /*
    static {
        Wg6.b(WelcomePresenter.class.getSimpleName(), "WelcomePresenter::class.java.simpleName");
    }
    */

    @DexIgnore
    public WelcomePresenter(G27 g27) {
        Wg6.c(g27, "mView");
        this.e = g27;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        G27 g27 = this.e;
        Spanned fromHtml = Html.fromHtml(q());
        Wg6.b(fromHtml, "Html.fromHtml(getTermsOf\u2026AndPrivacyPolicyString())");
        g27.w0(fromHtml);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.F27
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.F27
    public void o() {
        this.e.y5();
    }

    @DexIgnore
    public final G27 p() {
        return this.e;
    }

    @DexIgnore
    public final String q() {
        String a2 = M47.a(M47.Ci.TERMS, null);
        String a3 = M47.a(M47.Ci.PRIVACY, null);
        String str = Um5.c(PortfolioApp.get.instance(), 2131886977).toString();
        Wg6.b(a2, "termOfUseUrl");
        String q = Vt7.q(str, "term_of_use_url", a2, false, 4, null);
        Wg6.b(a3, "privacyPolicyUrl");
        return Vt7.q(q, "privacy_policy", a3, false, 4, null);
    }

    @DexIgnore
    public void r() {
        this.e.M5(this);
    }
}
