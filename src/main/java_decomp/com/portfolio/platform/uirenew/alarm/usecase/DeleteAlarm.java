package com.portfolio.platform.uirenew.alarm.usecase;

import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Dj5;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeleteAlarm extends CoroutineUseCase<Di, Ei, Ci> {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ Ai i; // = new Ai(null);
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Di e;
    @DexIgnore
    public /* final */ Bi f; // = new Bi();
    @DexIgnore
    public /* final */ AlarmsRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return DeleteAlarm.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class Bi implements BleCommandResultManager.Bi {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$DeleteAlarmsBroadcastReceiver$receive$1", f = "DeleteAlarm.kt", l = {39}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Alarm $requestAlarm;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Alarm alarm, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
                this.$requestAlarm = alarm;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$requestAlarm, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    AlarmsRepository alarmsRepository = DeleteAlarm.this.g;
                    Alarm alarm = this.$requestAlarm;
                    this.L$0 = il6;
                    this.label = 1;
                    if (alarmsRepository.deleteAlarm(alarm, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi() {
        }

        @DexIgnore
        @Override // com.portfolio.platform.service.BleCommandResultManager.Bi
        public void a(CommunicateMode communicateMode, Intent intent) {
            Wg6.c(communicateMode, "communicateMode");
            Wg6.c(intent, "intent");
            if (communicateMode == CommunicateMode.SET_LIST_ALARM && DeleteAlarm.this.p()) {
                DeleteAlarm.this.u(false);
                FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive");
                Alarm a2 = DeleteAlarm.this.q().a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(DeleteAlarm.i.a(), "onReceive success");
                    Rm6 unused = Gu7.d(DeleteAlarm.this.g(), null, null, new Aii(this, a2, null), 3, null);
                    DeleteAlarm.this.j(new Ei(a2));
                    return;
                }
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = DeleteAlarm.i.a();
                local.d(a3, "onReceive error - errorCode=" + intExtra);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                DeleteAlarm.this.i(new Ci(a2, intExtra, integerArrayListExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ai {
        @DexIgnore
        public /* final */ Alarm a;
        @DexIgnore
        public /* final */ int b;
        @DexIgnore
        public /* final */ ArrayList<Integer> c;

        @DexIgnore
        public Ci(Alarm alarm, int i, ArrayList<Integer> arrayList) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            Wg6.c(arrayList, "errorCodes");
            this.a = alarm;
            this.b = i;
            this.c = arrayList;
        }

        @DexIgnore
        public final ArrayList<Integer> a() {
            return this.c;
        }

        @DexIgnore
        public final int b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Bi {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ List<Alarm> b;
        @DexIgnore
        public /* final */ Alarm c;

        @DexIgnore
        public Di(String str, List<Alarm> list, Alarm alarm) {
            Wg6.c(str, "deviceId");
            Wg6.c(list, "alarms");
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = str;
            this.b = list;
            this.c = alarm;
        }

        @DexIgnore
        public final Alarm a() {
            return this.c;
        }

        @DexIgnore
        public final List<Alarm> b() {
            return this.b;
        }

        @DexIgnore
        public final String c() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements CoroutineUseCase.Di {
        @DexIgnore
        public /* final */ Alarm a;

        @DexIgnore
        public Ei(Alarm alarm) {
            Wg6.c(alarm, com.misfit.frameworks.buttonservice.model.Alarm.TABLE_NAME);
            this.a = alarm;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm", f = "DeleteAlarm.kt", l = {68, 87}, m = "run")
    public static final class Fi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DeleteAlarm this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(DeleteAlarm deleteAlarm, Xe6 xe6) {
            super(xe6);
            this.this$0 = deleteAlarm;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.s(null, this);
        }
    }

    /*
    static {
        String simpleName = DeleteAlarm.class.getSimpleName();
        Wg6.b(simpleName, "DeleteAlarm::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public DeleteAlarm(AlarmsRepository alarmsRepository) {
        Wg6.c(alarmsRepository, "mAlarmsRepository");
        this.g = alarmsRepository;
    }

    @DexIgnore
    @Override // com.portfolio.platform.CoroutineUseCase
    public String h() {
        return h;
    }

    @DexIgnore
    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [com.portfolio.platform.CoroutineUseCase$Bi, com.mapped.Xe6] */
    @Override // com.portfolio.platform.CoroutineUseCase
    public /* bridge */ /* synthetic */ Object k(Di di, Xe6 xe6) {
        return s(di, xe6);
    }

    @DexIgnore
    public final boolean p() {
        return this.d;
    }

    @DexIgnore
    public final Di q() {
        Di di = this.e;
        if (di != null) {
            return di;
        }
        Wg6.n("mRequestValues");
        throw null;
    }

    @DexIgnore
    public final void r() {
        BleCommandResultManager.d.e(this.f, CommunicateMode.SET_LIST_ALARM);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00b3  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d6  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0106  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object s(com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm.Di r14, com.mapped.Xe6<java.lang.Object> r15) {
        /*
        // Method dump skipped, instructions count: 310
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm.s(com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm$Di, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void t(List<Alarm> list, String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "setAlarms - alarms=" + list);
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            if (t.isActive()) {
                arrayList.add(t);
            }
        }
        PortfolioApp.get.instance().o1(str, Dj5.a(arrayList));
    }

    @DexIgnore
    public final void u(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void v() {
        BleCommandResultManager.d.j(this.f, CommunicateMode.SET_LIST_ALARM);
    }
}
