package com.portfolio.platform.uirenew.splash;

import android.os.Handler;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.Ao7;
import com.fossil.El7;
import com.fossil.Gu7;
import com.fossil.I07;
import com.fossil.J07;
import com.fossil.Ko7;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.migration.MigrationHelper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SplashPresenter extends I07 {
    @DexIgnore
    public static /* final */ String m;
    @DexIgnore
    public static /* final */ Ai n; // = new Ai(null);
    @DexIgnore
    public /* final */ Handler e; // = new Handler();
    @DexIgnore
    public /* final */ Runnable f; // = new Ei(this);
    @DexIgnore
    public boolean g;
    @DexIgnore
    public /* final */ J07 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ MigrationHelper j;
    @DexIgnore
    public /* final */ Cj4 k;
    @DexIgnore
    public /* final */ ThemeRepository l;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SplashPresenter.m;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter", f = "SplashPresenter.kt", l = {84}, m = "checkAuthToken")
    public static final class Bi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(SplashPresenter splashPresenter, Xe6 xe6) {
            super(xe6);
            this.this$0 = splashPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.w(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkAuthToken$isAA$1", f = "SplashPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Boolean>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore
        public Ci(Xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Boolean> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                return Ao7.a(SoLibraryLoader.f().c(PortfolioApp.get.instance()) != null);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1", f = "SplashPresenter.kt", l = {68, 69, 74, 77}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$checkToGoToNextStep$1$mCurrentUser$1", f = "SplashPresenter.kt", l = {69}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.i;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(SplashPresenter splashPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = splashPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:13:0x004b  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x006a  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x010f  */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0130  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r13) {
            /*
            // Method dump skipped, instructions count: 330
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.splash.SplashPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ei implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter b;

        @DexIgnore
        public Ei(SplashPresenter splashPresenter) {
            this.b = splashPresenter;
        }

        @DexIgnore
        public final void run() {
            FLogger.INSTANCE.getLocal().d(SplashPresenter.n.a(), "runnable");
            this.b.x();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.splash.SplashPresenter$start$1", f = "SplashPresenter.kt", l = {51}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $currentAppVersion;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SplashPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(SplashPresenter splashPresenter, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = splashPresenter;
            this.$currentAppVersion = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$currentAppVersion, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                MigrationHelper migrationHelper = this.this$0.j;
                String str = this.$currentAppVersion;
                this.L$0 = il6;
                this.label = 1;
                if (migrationHelper.f(str, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    /*
    static {
        String simpleName = SplashPresenter.class.getSimpleName();
        Wg6.b(simpleName, "SplashPresenter::class.java.simpleName");
        m = simpleName;
    }
    */

    @DexIgnore
    public SplashPresenter(J07 j07, UserRepository userRepository, MigrationHelper migrationHelper, Cj4 cj4, ThemeRepository themeRepository) {
        Wg6.c(j07, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(migrationHelper, "mMigrationHelper");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(themeRepository, "mThemeRepository");
        this.h = j07;
        this.i = userRepository;
        this.j = migrationHelper;
        this.k = cj4;
        this.l = themeRepository;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        String P = PortfolioApp.get.instance().P();
        boolean d = this.j.d(P);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = m;
        local.d(str, "currentAppVersion " + P + " complete " + d);
        if (!d) {
            this.g = true;
            Rm6 unused = Gu7.d(k(), null, null, new Fi(this, P, null), 3, null);
        }
        this.e.postDelayed(this.f, 1000);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
        this.e.removeCallbacksAndMessages(null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object w(com.mapped.Xe6<? super com.mapped.Cd6> r7) {
        /*
            r6 = this;
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r7 instanceof com.portfolio.platform.uirenew.splash.SplashPresenter.Bi
            if (r0 == 0) goto L_0x004a
            r0 = r7
            com.portfolio.platform.uirenew.splash.SplashPresenter$Bi r0 = (com.portfolio.platform.uirenew.splash.SplashPresenter.Bi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x004a
            int r1 = r1 + r3
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r2 = com.fossil.Yn7.d()
            int r3 = r0.label
            if (r3 == 0) goto L_0x0058
            if (r3 != r5) goto L_0x0050
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.uirenew.splash.SplashPresenter r0 = (com.portfolio.platform.uirenew.splash.SplashPresenter) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0071
            boolean r0 = r6.g
            if (r0 == 0) goto L_0x0042
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            com.mapped.Cj4 r1 = r6.k
            r2 = 0
            r3 = 13
            r0.S1(r1, r2, r3)
        L_0x0042:
            com.fossil.J07 r0 = r6.h
            r0.l()
        L_0x0047:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x0049:
            return r0
        L_0x004a:
            com.portfolio.platform.uirenew.splash.SplashPresenter$Bi r0 = new com.portfolio.platform.uirenew.splash.SplashPresenter$Bi
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x0050:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0058:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.uirenew.splash.SplashPresenter$Ci r3 = new com.portfolio.platform.uirenew.splash.SplashPresenter$Ci
            r4 = 0
            r3.<init>(r4)
            r0.L$0 = r6
            r0.label = r5
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r3, r0)
            if (r1 != r2) goto L_0x0027
            r0 = r2
            goto L_0x0049
        L_0x0071:
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.IRemoteFLogger r0 = r0.getRemote()
            com.misfit.frameworks.buttonservice.log.FLogger$Component r1 = com.misfit.frameworks.buttonservice.log.FLogger.Component.APP
            com.misfit.frameworks.buttonservice.log.FLogger$Session r2 = com.misfit.frameworks.buttonservice.log.FLogger.Session.OTHER
            com.portfolio.platform.PortfolioApp$inner r3 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r3 = r3.instance()
            java.lang.String r3 = r3.J()
            com.portfolio.platform.ApplicationEventListener$a r4 = com.portfolio.platform.ApplicationEventListener.J
            java.lang.String r4 = r4.a()
            java.lang.String r5 = "[App Authentication] Unauthorized"
            r0.i(r1, r2, r3, r4, r5)
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            com.portfolio.platform.data.model.ServerError r1 = new com.portfolio.platform.data.model.ServerError
            r1.<init>()
            r0.F(r1)
            goto L_0x0047
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.splash.SplashPresenter.w(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final void x() {
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    public void y() {
        this.h.M5(this);
    }
}
