package com.portfolio.platform.uirenew.onboarding.profilesetup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import com.fossil.Ax6;
import com.fossil.Xv5;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ProfileSetupActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a B; // = new a(null);
    @DexIgnore
    public ProfileSetupPresenter A;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context, SignUpEmailAuth signUpEmailAuth) {
            Wg6.c(context, "context");
            Wg6.c(signUpEmailAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "start with auth=" + signUpEmailAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("EMAIL_AUTH", signUpEmailAuth);
            context.startActivity(intent);
        }

        @DexIgnore
        public final void b(Context context, SignUpSocialAuth signUpSocialAuth) {
            Wg6.c(context, "context");
            Wg6.c(signUpSocialAuth, "auth");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("ProfileSetupActivity", "startSetUpSocial with " + signUpSocialAuth);
            Intent intent = new Intent(context, ProfileSetupActivity.class);
            intent.putExtra("SOCIAL_AUTH", signUpSocialAuth);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, com.portfolio.platform.ui.BaseActivity
    public void onBackPressed() {
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Xv5 xv5 = (Xv5) getSupportFragmentManager().Y(2131362158);
        if (xv5 == null) {
            xv5 = Xv5.t.a();
            k(xv5, "ProfileSetupFragment", 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (xv5 != null) {
            iface.F0(new Ax6(xv5)).a(this);
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent socialAuth");
                    ProfileSetupPresenter profileSetupPresenter = this.A;
                    if (profileSetupPresenter != null) {
                        Parcelable parcelableExtra = intent.getParcelableExtra("SOCIAL_AUTH");
                        Wg6.b(parcelableExtra, "it.getParcelableExtra(co\u2026ms.Constants.SOCIAL_AUTH)");
                        profileSetupPresenter.j0((SignUpSocialAuth) parcelableExtra);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from intent emailAuth");
                    ProfileSetupPresenter profileSetupPresenter2 = this.A;
                    if (profileSetupPresenter2 != null) {
                        Parcelable parcelableExtra2 = intent.getParcelableExtra("EMAIL_AUTH");
                        Wg6.b(parcelableExtra2, "it.getParcelableExtra(co\u2026ums.Constants.EMAIL_AUTH)");
                        profileSetupPresenter2.h0((SignUpEmailAuth) parcelableExtra2);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
                if (intent.hasExtra("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean booleanExtra = intent.getBooleanExtra("IS_UPDATE_PROFILE_PROCESS", false);
                    ProfileSetupPresenter profileSetupPresenter3 = this.A;
                    if (profileSetupPresenter3 != null) {
                        profileSetupPresenter3.i0(booleanExtra);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            }
            if (bundle != null) {
                if (bundle.containsKey("SOCIAL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from savedInstanceState socialAuth");
                    SignUpSocialAuth signUpSocialAuth = (SignUpSocialAuth) bundle.getParcelable("SOCIAL_AUTH");
                    if (signUpSocialAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter4 = this.A;
                        if (profileSetupPresenter4 != null) {
                            profileSetupPresenter4.j0(signUpSocialAuth);
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("EMAIL_AUTH")) {
                    FLogger.INSTANCE.getLocal().d(r(), "Retrieve from savedInstanceState emailAuth");
                    SignUpEmailAuth signUpEmailAuth = (SignUpEmailAuth) bundle.getParcelable("EMAIL_AUTH");
                    if (signUpEmailAuth != null) {
                        ProfileSetupPresenter profileSetupPresenter5 = this.A;
                        if (profileSetupPresenter5 != null) {
                            profileSetupPresenter5.h0(signUpEmailAuth);
                        } else {
                            Wg6.n("mPresenter");
                            throw null;
                        }
                    }
                }
                if (bundle.containsKey("IS_UPDATE_PROFILE_PROCESS")) {
                    boolean z = bundle.getBoolean("IS_UPDATE_PROFILE_PROCESS");
                    ProfileSetupPresenter profileSetupPresenter6 = this.A;
                    if (profileSetupPresenter6 != null) {
                        profileSetupPresenter6.i0(z);
                    } else {
                        Wg6.n("mPresenter");
                        throw null;
                    }
                }
            }
        } else {
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupContract.View");
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity
    public void onSaveInstanceState(Bundle bundle) {
        Wg6.c(bundle, "outState");
        ProfileSetupPresenter profileSetupPresenter = this.A;
        if (profileSetupPresenter != null) {
            SignUpEmailAuth R = profileSetupPresenter.R();
            if (R != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String r = r();
                local.d(r, "onSaveInstanceState put emailAuth " + R);
                bundle.putParcelable("EMAIL_AUTH", R);
            }
            ProfileSetupPresenter profileSetupPresenter2 = this.A;
            if (profileSetupPresenter2 != null) {
                SignUpSocialAuth Y = profileSetupPresenter2.Y();
                if (Y != null) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String r2 = r();
                    local2.d(r2, "onSaveInstanceState put socialAuth " + Y);
                    bundle.putParcelable("SOCIAL_AUTH", Y);
                }
                ProfileSetupPresenter profileSetupPresenter3 = this.A;
                if (profileSetupPresenter3 != null) {
                    bundle.putBoolean("IS_UPDATE_PROFILE_PROCESS", profileSetupPresenter3.f0());
                    super.onSaveInstanceState(bundle);
                    return;
                }
                Wg6.n("mPresenter");
                throw null;
            }
            Wg6.n("mPresenter");
            throw null;
        }
        Wg6.n("mPresenter");
        throw null;
    }
}
