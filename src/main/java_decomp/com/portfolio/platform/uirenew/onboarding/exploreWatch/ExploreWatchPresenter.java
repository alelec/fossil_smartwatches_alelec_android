package com.portfolio.platform.uirenew.onboarding.exploreWatch;

import com.fossil.Bw7;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Ko7;
import com.fossil.Pv6;
import com.fossil.Qv6;
import com.fossil.Um5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ExploreWatchPresenter extends Pv6 {
    @DexIgnore
    public /* final */ Qv6 e;
    @DexIgnore
    public /* final */ Cj4 f;
    @DexIgnore
    public /* final */ DeviceRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp $portfolioApp;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                String str;
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    String J = this.this$0.$portfolioApp.J();
                    String deviceNameBySerial = this.this$0.this$0.g.getDeviceNameBySerial(J);
                    MisfitDeviceProfile m = DeviceHelper.o.j().m(J);
                    if (m == null || (str = m.getFirmwareVersion()) == null) {
                        str = "";
                    }
                    AnalyticsHelper.f.g().h(DeviceHelper.o.m(J), deviceNameBySerial, str);
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, J, "ExploreWatchPresenter", "[Sync Start] AUTO SYNC after pair new device");
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(PortfolioApp portfolioApp, Xe6 xe6, ExploreWatchPresenter exploreWatchPresenter) {
            super(2, xe6);
            this.$portfolioApp = portfolioApp;
            this.this$0 = exploreWatchPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$portfolioApp, xe6, this.this$0);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.$portfolioApp.S1(this.this$0.f, false, 13);
            this.this$0.u().j3();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$1", f = "ExploreWatchPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ExploreWatchPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.exploreWatch.ExploreWatchPresenter$start$1$data$1", f = "ExploreWatchPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super List<? extends Explore>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<? extends Explore>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    return this.this$0.this$0.t();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(ExploreWatchPresenter exploreWatchPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = exploreWatchPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object g;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                g = Eu7.g(h, aii, this);
                if (g == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                g = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.u().z5((List) g);
            this.this$0.u().f();
            return Cd6.a;
        }
    }

    @DexIgnore
    public ExploreWatchPresenter(Qv6 qv6, Cj4 cj4, DeviceRepository deviceRepository) {
        Wg6.c(qv6, "mView");
        Wg6.c(cj4, "mDeviceSettingFactory");
        Wg6.c(deviceRepository, "mDeviceRepository");
        this.e = qv6;
        this.f = cj4;
        this.g = deviceRepository;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Pv6
    public void n() {
        Rm6 unused = Gu7.d(k(), null, null, new Ai(PortfolioApp.get.instance(), null, this), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Pv6
    public void o(boolean z) {
    }

    @DexIgnore
    public final List<Explore> t() {
        ArrayList arrayList = new ArrayList();
        Explore explore = new Explore();
        explore.setTitle(Um5.c(PortfolioApp.get.instance(), 2131887042));
        explore.setDescription(Um5.c(PortfolioApp.get.instance(), 2131887039));
        explore.setExploreType(Explore.ExploreType.WRIST_FLICK);
        Explore explore2 = new Explore();
        explore2.setTitle(Um5.c(PortfolioApp.get.instance(), 2131887040));
        explore2.setDescription(Um5.c(PortfolioApp.get.instance(), 2131887037));
        explore2.setExploreType(Explore.ExploreType.DOUBLE_TAP);
        Explore explore3 = new Explore();
        explore3.setTitle(Um5.c(PortfolioApp.get.instance(), 2131887043));
        explore3.setDescription(Um5.c(PortfolioApp.get.instance(), 2131887038));
        explore3.setExploreType(Explore.ExploreType.PRESS_AND_HOLD);
        arrayList.add(explore);
        arrayList.add(explore2);
        arrayList.add(explore3);
        return arrayList;
    }

    @DexIgnore
    public final Qv6 u() {
        return this.e;
    }

    @DexIgnore
    public void v() {
        this.e.M5(this);
    }
}
