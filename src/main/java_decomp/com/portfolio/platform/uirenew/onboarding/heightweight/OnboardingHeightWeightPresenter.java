package com.portfolio.platform.uirenew.onboarding.heightweight;

import com.fossil.Ai5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Gu7;
import com.fossil.Hw6;
import com.fossil.Iw6;
import com.fossil.Ko7;
import com.fossil.Qh5;
import com.fossil.Yn7;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.usecase.GetRecommendedGoalUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OnboardingHeightWeightPresenter extends Hw6 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ Ai j; // = new Ai(null);
    @DexIgnore
    public MFUser e;
    @DexIgnore
    public /* final */ Iw6 f;
    @DexIgnore
    public /* final */ UserRepository g;
    @DexIgnore
    public /* final */ GetRecommendedGoalUseCase h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return OnboardingHeightWeightPresenter.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<GetRecommendedGoalUseCase.Ci, GetRecommendedGoalUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ MFUser a;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public Bi(MFUser mFUser, OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, boolean z) {
            this.a = mFUser;
            this.b = onboardingHeightWeightPresenter;
            this.c = z;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(GetRecommendedGoalUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(GetRecommendedGoalUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            this.b.y(this.a, this.c);
        }

        @DexIgnore
        public void c(GetRecommendedGoalUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.b.y(this.a, this.c);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetRecommendedGoalUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isDefaultValuesUsed;
        @DexIgnore
        public /* final */ /* synthetic */ MFUser $user;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$onSetUpHeightWeightComplete$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {124}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<? extends MFUser>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Ap4<? extends MFUser>> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    MFUser mFUser = this.this$0.$user;
                    this.L$0 = il6;
                    this.label = 1;
                    Object updateUser = userRepository.updateUser(mFUser, true, this);
                    return updateUser == d ? d : updateUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, MFUser mFUser, boolean z, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = onboardingHeightWeightPresenter;
            this.$user = mFUser;
            this.$isDefaultValuesUsed = z;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, this.$user, this.$isDefaultValuesUsed, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                if (this.$user.getUseDefaultBiometric()) {
                    this.$user.setUseDefaultBiometric(this.$isDefaultValuesUsed);
                }
                Dv7 h = this.this$0.h();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(h, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.f.h();
            this.this$0.f.u2();
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ OnboardingHeightWeightPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter$start$1$1", f = "OnboardingHeightWeightPresenter.kt", l = {32}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository userRepository = this.this$0.this$0.g;
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = userRepository.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(OnboardingHeightWeightPresenter onboardingHeightWeightPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = onboardingHeightWeightPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:7:0x004d  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
            // Method dump skipped, instructions count: 322
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.onboarding.heightweight.OnboardingHeightWeightPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    /*
    static {
        String simpleName = OnboardingHeightWeightPresenter.class.getSimpleName();
        Wg6.b(simpleName, "OnboardingHeightWeightPr\u2026er::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public OnboardingHeightWeightPresenter(Iw6 iw6, UserRepository userRepository, GetRecommendedGoalUseCase getRecommendedGoalUseCase) {
        Wg6.c(iw6, "mView");
        Wg6.c(userRepository, "mUserRepository");
        Wg6.c(getRecommendedGoalUseCase, "mGetRecommendedGoalUseCase");
        this.f = iw6;
        this.g = userRepository;
        this.h = getRecommendedGoalUseCase;
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Hw6
    public void n(boolean z) {
        String str = null;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = i;
        local.d(str2, "completeOnboarding currentUser=" + this.e);
        this.f.i();
        MFUser mFUser = this.e;
        if (mFUser != null) {
            String birthday = mFUser.getBirthday();
            if (birthday != null) {
                int age = mFUser.getAge(birthday);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                StringBuilder sb = new StringBuilder();
                sb.append("completeOnboarding update heightUnit=");
                MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
                sb.append(unitGroup != null ? unitGroup.getHeight() : null);
                sb.append(", weightUnit=");
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    str = unitGroup2.getWeight();
                }
                sb.append(str);
                sb.append(',');
                sb.append(" height=");
                sb.append(mFUser.getHeightInCentimeters());
                sb.append(", weight=");
                sb.append(mFUser.getWeightInGrams());
                local2.d(str3, sb.toString());
                this.h.e(new GetRecommendedGoalUseCase.Bi(age, mFUser.getHeightInCentimeters(), mFUser.getWeightInGrams(), Qh5.Companion.a(mFUser.getGender())), new Bi(mFUser, this, z));
                return;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Hw6
    public void o(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onHeightChanged heightInCentimeters=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setHeightInCentimeters(i2);
        }
    }

    @DexIgnore
    @Override // com.fossil.Hw6
    public void p(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitHeightChanged unit=" + ai5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setHeight(value);
                Iw6 iw6 = this.f;
                int heightInCentimeters = mFUser.getHeightInCentimeters();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String height = unitGroup2.getHeight();
                    if (height != null) {
                        Ai5 fromString = Ai5.fromString(height);
                        Wg6.b(fromString, "Unit.fromString(it.unitGroup!!.height!!)");
                        iw6.R0(heightInCentimeters, fromString);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Hw6
    public void q(Ai5 ai5) {
        Wg6.c(ai5, Constants.PROFILE_KEY_UNIT);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onUnitWeightChanged unit=" + ai5);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            MFUser.UnitGroup unitGroup = mFUser.getUnitGroup();
            if (unitGroup != null) {
                String value = ai5.getValue();
                Wg6.b(value, "unit.value");
                unitGroup.setWeight(value);
                Iw6 iw6 = this.f;
                int weightInGrams = mFUser.getWeightInGrams();
                MFUser.UnitGroup unitGroup2 = mFUser.getUnitGroup();
                if (unitGroup2 != null) {
                    String weight = unitGroup2.getWeight();
                    if (weight != null) {
                        Ai5 fromString = Ai5.fromString(weight);
                        Wg6.b(fromString, "Unit.fromString(it.unitGroup!!.weight!!)");
                        iw6.H1(weightInGrams, fromString);
                        return;
                    }
                    Wg6.i();
                    throw null;
                }
                Wg6.i();
                throw null;
            }
            Wg6.i();
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Hw6
    public void r(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onWeightChanged weightInGram=" + i2);
        MFUser mFUser = this.e;
        if (mFUser != null) {
            mFUser.setWeightInGrams(i2);
        }
    }

    @DexIgnore
    public final void y(MFUser mFUser, boolean z) {
        Wg6.c(mFUser, "user");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "onSetUpHeightWeightComplete register date: " + mFUser.getRegisterDate());
        Rm6 unused = Gu7.d(k(), null, null, new Ci(this, mFUser, z, null), 3, null);
    }

    @DexIgnore
    public void z() {
        this.f.M5(this);
    }
}
