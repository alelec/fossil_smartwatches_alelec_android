package com.portfolio.platform.uirenew.login;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.B47;
import com.fossil.Bw7;
import com.fossil.Dv5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ev5;
import com.fossil.Gu7;
import com.fossil.Gv5;
import com.fossil.Hv5;
import com.fossil.It4;
import com.fossil.Ko7;
import com.fossil.Kz4;
import com.fossil.Sy6;
import com.fossil.Um5;
import com.fossil.Uq4;
import com.fossil.Uu6;
import com.fossil.Uy6;
import com.fossil.Vt7;
import com.fossil.Vu6;
import com.fossil.Xn5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.buddy_challenge.domain.ProfileRepository;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginEmailUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.signup.SignUpPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginPresenter extends Uu6 {
    @DexIgnore
    public static /* final */ String Q;
    @DexIgnore
    public static /* final */ Ai R; // = new Ai(null);
    @DexIgnore
    public CheckAuthenticationSocialExisting A;
    @DexIgnore
    public AnalyticsHelper B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries F;
    @DexIgnore
    public FetchGoalTrackingData G;
    @DexIgnore
    public GetSecretKeyUseCase H;
    @DexIgnore
    public WatchLocalizationRepository I;
    @DexIgnore
    public AlarmsRepository J;
    @DexIgnore
    public ProfileRepository K;
    @DexIgnore
    public FCMRepository L;
    @DexIgnore
    public FlagRepository M;
    @DexIgnore
    public WorkoutSettingRepository N;
    @DexIgnore
    public /* final */ Vu6 O;
    @DexIgnore
    public /* final */ BaseActivity P;
    @DexIgnore
    public String e;
    @DexIgnore
    public String f;
    @DexIgnore
    public LoginEmailUseCase g;
    @DexIgnore
    public LoginSocialUseCase h;
    @DexIgnore
    public DownloadUserInfoUseCase i;
    @DexIgnore
    public ReconnectDeviceUseCase j;
    @DexIgnore
    public Cj4 k;
    @DexIgnore
    public UserRepository l;
    @DexIgnore
    public DeviceRepository m;
    @DexIgnore
    public An4 n;
    @DexIgnore
    public FetchActivities o;
    @DexIgnore
    public FetchSummaries p;
    @DexIgnore
    public Uq4 q;
    @DexIgnore
    public FetchSleepSessions r;
    @DexIgnore
    public FetchSleepSummaries s;
    @DexIgnore
    public FetchHeartRateSamples t;
    @DexIgnore
    public FetchDailyHeartRateSummaries u;
    @DexIgnore
    public Dv5 v;
    @DexIgnore
    public Xn5 w;
    @DexIgnore
    public Ev5 x;
    @DexIgnore
    public Hv5 y;
    @DexIgnore
    public Gv5 z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return LoginPresenter.Q;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$1", f = "LoginPresenter.kt", l = {572, 573, 577, 582, 585}, m = "invokeSuspend")
    public static final class Bi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$checkOnboarding$1$currentUser$1", f = "LoginPresenter.kt", l = {585}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Bi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Bi bi, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = bi;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository S = this.this$0.this$0.S();
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = S.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Bi(LoginPresenter loginPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = loginPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Bi bi = new Bi(this.this$0, xe6);
            bi.p$ = (Il6) obj;
            throw null;
            //return bi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Bi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x012f  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x017f  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0182  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0186  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 393
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.login.LoginPresenter.Bi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci implements CoroutineUseCase.Ei<CheckAuthenticationSocialExisting.Ci, CheckAuthenticationSocialExisting.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public Ci(LoginPresenter loginPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = loginPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CheckAuthenticationSocialExisting.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(CheckAuthenticationSocialExisting.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.O.h();
            this.a.Z(bi.a(), "");
        }

        @DexIgnore
        public void c(CheckAuthenticationSocialExisting.Ci ci) {
            Wg6.c(ci, "responseValue");
            boolean a2 = ci.a();
            if (a2) {
                this.a.Y(this.b);
            } else if (!a2) {
                this.a.O.e5(this.b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(CheckAuthenticationSocialExisting.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1", f = "LoginPresenter.kt", l = {507, 517}, m = "invokeSuspend")
    public static final class Di extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$1", f = "LoginPresenter.kt", l = {508, 509, 510, 511, 512, 513}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0054  */
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0038  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                    r6 = this;
                    r5 = 1
                    r4 = 0
                    java.lang.Object r1 = com.fossil.Yn7.d()
                    int r0 = r6.label
                    switch(r0) {
                        case 0: goto L_0x00ab;
                        case 1: goto L_0x008e;
                        case 2: goto L_0x0072;
                        case 3: goto L_0x0056;
                        case 4: goto L_0x003a;
                        case 5: goto L_0x001d;
                        case 6: goto L_0x0013;
                        default: goto L_0x000b;
                    }
                L_0x000b:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0013:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x001a:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x001c:
                    return r0
                L_0x001d:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0024:
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.DeviceRepository r2 = r2.L()
                    r6.L$0 = r0
                    r0 = 6
                    r6.label = r0
                    r0 = 0
                    java.lang.Object r0 = com.portfolio.platform.data.source.DeviceRepository.downloadSupportedSku$default(r2, r4, r6, r5, r0)
                    if (r0 != r1) goto L_0x001a
                    r0 = r1
                    goto L_0x001c
                L_0x003a:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0041:
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.WorkoutSettingRepository r2 = r2.U()
                    r6.L$0 = r0
                    r3 = 5
                    r6.label = r3
                    java.lang.Object r2 = r2.downloadWorkoutSettings(r6)
                    if (r2 != r1) goto L_0x0024
                    r0 = r1
                    goto L_0x001c
                L_0x0056:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x005d:
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.GoalTrackingRepository r2 = r2.O()
                    r6.L$0 = r0
                    r3 = 4
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchGoalSetting(r6)
                    if (r2 != r1) goto L_0x0041
                    r0 = r1
                    goto L_0x001c
                L_0x0072:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0079:
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.SleepSummariesRepository r2 = r2.Q()
                    r6.L$0 = r0
                    r3 = 3
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchLastSleepGoal(r6)
                    if (r2 != r1) goto L_0x005d
                    r0 = r1
                    goto L_0x001c
                L_0x008e:
                    java.lang.Object r0 = r6.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r7)
                L_0x0095:
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.SummariesRepository r2 = r2.R()
                    r6.L$0 = r0
                    r3 = 2
                    r6.label = r3
                    java.lang.Object r2 = r2.fetchActivitySettings(r6)
                    if (r2 != r1) goto L_0x0079
                    r0 = r1
                    goto L_0x001c
                L_0x00ab:
                    com.fossil.El7.b(r7)
                    com.mapped.Il6 r0 = r6.p$
                    com.portfolio.platform.uirenew.login.LoginPresenter$Di r2 = r6.this$0
                    com.portfolio.platform.uirenew.login.LoginPresenter r2 = r2.this$0
                    com.portfolio.platform.data.source.WatchLocalizationRepository r2 = r2.T()
                    r6.L$0 = r0
                    r6.label = r5
                    java.lang.Object r2 = r2.getWatchLocalizationFromServer(r4, r6)
                    if (r2 != r1) goto L_0x0095
                    r0 = r1
                    goto L_0x001c
                    switch-data {0->0x00ab, 1->0x008e, 2->0x0072, 3->0x0056, 4->0x003a, 5->0x001d, 6->0x0013, }
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.login.LoginPresenter.Di.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadOptionalsResources$1$alarms$1", f = "LoginPresenter.kt", l = {517}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Di this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Di di, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = di;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    AlarmsRepository J = this.this$0.this$0.J();
                    this.L$0 = il6;
                    this.label = 1;
                    Object activeAlarms = J.getActiveAlarms(this);
                    return activeAlarms == d ? d : activeAlarms;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(LoginPresenter loginPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = loginPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Di di = new Di(this.this$0, xe6);
            di.p$ = (Il6) obj;
            throw null;
            //return di;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Di) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0054  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x0017:
                java.util.List r0 = (java.util.List) r0
                if (r0 != 0) goto L_0x0020
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
            L_0x0020:
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                java.util.List r0 = com.fossil.Dj5.a(r0)
                r1.q1(r0)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x003f:
                com.portfolio.platform.uirenew.login.LoginPresenter r2 = r7.this$0
                com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.login.LoginPresenter.z(r2)
                com.portfolio.platform.uirenew.login.LoginPresenter$Di$Bii r3 = new com.portfolio.platform.uirenew.login.LoginPresenter$Di$Bii
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r7)
                if (r0 != r1) goto L_0x0017
                r0 = r1
                goto L_0x002f
            L_0x0056:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.login.LoginPresenter r2 = r7.this$0
                com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.login.LoginPresenter.z(r2)
                com.portfolio.platform.uirenew.login.LoginPresenter$Di$Aii r3 = new com.portfolio.platform.uirenew.login.LoginPresenter$Di$Aii
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = com.fossil.Eu7.g(r2, r3, r7)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.login.LoginPresenter.Di.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$downloadSocialProfile$2", f = "LoginPresenter.kt", l = {610}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Kz4<It4>>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(LoginPresenter loginPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = loginPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Kz4<It4>> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                ProfileRepository V = this.this$0.V();
                this.L$0 = il6;
                this.label = 1;
                Object c = V.c(this);
                return c == d ? d : c;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CoroutineUseCase.Ei<LoginSocialUseCase.Ci, LoginSocialUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(LoginSocialUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(LoginSocialUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            this.a.O.h();
            this.a.W(ai.a(), "");
        }

        @DexIgnore
        public void c(LoginSocialUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            PortfolioApp.get.instance().getIface().S0(this.a);
            this.a.a0();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(LoginSocialUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements CoroutineUseCase.Ei<LoginEmailUseCase.Ci, LoginEmailUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(LoginEmailUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(LoginEmailUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginEmail failed with error=" + ai.a());
            this.a.O.h();
            this.a.Z(ai.a(), ai.b());
        }

        @DexIgnore
        public void c(LoginEmailUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            FLogger.INSTANCE.getLocal().d(LoginPresenter.R.a(), "Inside .loginEmail success ");
            PortfolioApp.get.instance().getIface().S0(this.a);
            this.a.a0();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(LoginEmailUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements CoroutineUseCase.Ei<Dv5.Di, Dv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Hi(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Dv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Dv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + ci.a());
            this.a.O.h();
            if (2 != ci.a()) {
                this.a.Z(ci.a(), "");
            }
        }

        @DexIgnore
        public void c(Dv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginFacebook success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Dv5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements CoroutineUseCase.Ei<Ev5.Di, Ev5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ii(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Ev5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Ev5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + ci.a());
            this.a.O.h();
            if (2 != ci.a()) {
                this.a.Z(ci.a(), "");
            }
        }

        @DexIgnore
        public void c(Ev5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginGoogle success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Ev5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements CoroutineUseCase.Ei<Gv5.Di, Gv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ji(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Gv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Gv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.e(a2, "Inside .loginWechat failed with error=" + ci.a());
            this.a.O.h();
            this.a.Z(ci.a(), "");
        }

        @DexIgnore
        public void c(Gv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginWechat success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Gv5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements CoroutineUseCase.Ei<Hv5.Di, Hv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ki(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Hv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Hv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + ci.a());
            this.a.O.h();
            this.a.Z(ci.a(), "");
        }

        @DexIgnore
        public void c(Hv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "Inside .loginWeibo success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Hv5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements CoroutineUseCase.Ei<DownloadUserInfoUseCase.Ci, DownloadUserInfoUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ LoginPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onError$1", f = "LoginPresenter.kt", l = {495}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DownloadUserInfoUseCase.Ai $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Li this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Li li, DownloadUserInfoUseCase.Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = li;
                this.$errorValue = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$errorValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository S = this.this$0.a.S();
                    this.L$0 = il6;
                    this.label = 1;
                    if (S.clearAllUser(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.O.h();
                this.this$0.a.W(this.$errorValue.a(), this.$errorValue.b());
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1", f = "LoginPresenter.kt", l = {407, 413, 419, 422, MFNetworkReturnCode.RATE_LIMIT_EXEEDED, 444, 452, 484}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $currentUser;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Li this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$1", f = "LoginPresenter.kt", l = {407}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        UserRepository S = this.this$0.this$0.a.S();
                        this.L$0 = il6;
                        this.label = 1;
                        if (S.clearAllUser(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$2", f = "LoginPresenter.kt", l = {HelpSearchRecyclerViewAdapter.TYPE_PADDING, 424}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<UserSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<UserSettings>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        il6 = this.p$;
                        PortfolioApp instance = PortfolioApp.get.instance();
                        this.L$0 = il6;
                        this.label = 1;
                        if (instance.g2(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        El7.b(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    UserRepository S = this.this$0.this$0.a.S();
                    this.L$0 = il6;
                    this.label = 2;
                    Object userSettingFromServer = S.getUserSettingFromServer(this);
                    return userSettingFromServer == d ? d : userSettingFromServer;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Ciii implements CoroutineUseCase.Ei<Uy6, Sy6> {
                @DexIgnore
                public /* final */ /* synthetic */ Bii a;
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 b;
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 c;
                @DexIgnore
                public /* final */ /* synthetic */ List d;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$4$onError$1", f = "LoginPresenter.kt", l = {472}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Sy6 $errorValue;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Ciii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Ciii ciii, Sy6 sy6, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = ciii;
                        this.$errorValue = sy6;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, this.$errorValue, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a2 = LoginPresenter.R.a();
                            local.d(a2, "onLoginSuccess download device setting fail " + this.$errorValue.a());
                            Dv7 i2 = this.this$0.a.this$0.a.i();
                            LoginPresenter$l$b$c$a$a loginPresenter$l$b$c$a$a = new LoginPresenter$l$b$c$a$a(this, null);
                            this.L$0 = il6;
                            this.label = 1;
                            if (Eu7.g(i2, loginPresenter$l$b$c$a$a, this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        this.this$0.a.this$0.a.O.h();
                        this.this$0.a.this$0.a.W(this.$errorValue.a(), this.$errorValue.b());
                        return Cd6.a;
                    }
                }

                @DexIgnore
                public Ciii(Bii bii, Jh6 jh6, Jh6 jh62, List list) {
                    this.a = bii;
                    this.b = jh6;
                    this.c = jh62;
                    this.d = list;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void a(Sy6 sy6) {
                    b(sy6);
                }

                @DexIgnore
                public void b(Sy6 sy6) {
                    Wg6.c(sy6, "errorValue");
                    Rm6 unused = Gu7.d(this.a.this$0.a.k(), null, null, new Aiiii(this, sy6, null), 3, null);
                }

                @DexIgnore
                public void c(Uy6 uy6) {
                    Wg6.c(uy6, "responseValue");
                    FLogger.INSTANCE.getLocal().d(LoginPresenter.R.a(), "onLoginSuccess download device setting success");
                    PortfolioApp.get.instance().n1(this.b.element, this.c.element);
                    for (Lc6 lc6 : this.d) {
                        PortfolioApp.get.instance().F1((String) lc6.getFirst(), (String) lc6.getSecond());
                    }
                    this.a.this$0.a.b0(this.b.element);
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, this.b.element, LoginPresenter.R.a(), "[Sync Start] AUTO SYNC after login");
                    PortfolioApp.get.instance().S1(this.a.this$0.a.M(), false, 13);
                    this.a.this$0.a.D();
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void onSuccess(Uy6 uy6) {
                    c(uy6);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.login.LoginPresenter$onLoginSuccess$1$onSuccess$1$response$1", f = "LoginPresenter.kt", l = {MFNetworkReturnCode.RATE_LIMIT_EXEEDED}, m = "invokeSuspend")
            public static final class Diii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<Device>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Diii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Diii diii = new Diii(this.this$0, xe6);
                    diii.p$ = (Il6) obj;
                    throw null;
                    //return diii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<Device>>> xe6) {
                    throw null;
                    //return ((Diii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceRepository L = this.this$0.this$0.a.L();
                        this.L$0 = il6;
                        this.label = 1;
                        Object downloadDeviceList = L.downloadDeviceList(this);
                        return downloadDeviceList == d ? d : downloadDeviceList;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Li li, MFUser mFUser, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = li;
                this.$currentUser = mFUser;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$currentUser, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0126  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x01a9  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01cc  */
            /* JADX WARNING: Removed duplicated region for block: B:44:0x0222  */
            /* JADX WARNING: Removed duplicated region for block: B:59:0x02af  */
            /* JADX WARNING: Removed duplicated region for block: B:67:0x031a  */
            /* JADX WARNING: Removed duplicated region for block: B:75:0x0369  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 898
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.login.LoginPresenter.Li.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Li(LoginPresenter loginPresenter) {
            this.a = loginPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DownloadUserInfoUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(DownloadUserInfoUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = LoginPresenter.R.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + ai.a());
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, ai, null), 3, null);
        }

        @DexIgnore
        public void c(DownloadUserInfoUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Bii(this, ci.a(), null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DownloadUserInfoUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements CoroutineUseCase.Ei<ReconnectDeviceUseCase.Ei, ReconnectDeviceUseCase.Di> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(ReconnectDeviceUseCase.Di di) {
            b(di);
        }

        @DexIgnore
        public void b(ReconnectDeviceUseCase.Di di) {
            Wg6.c(di, "errorValue");
        }

        @DexIgnore
        public void c(ReconnectDeviceUseCase.Ei ei) {
            Wg6.c(ei, "responseValue");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(ReconnectDeviceUseCase.Ei ei) {
            c(ei);
        }
    }

    /*
    static {
        String simpleName = LoginPresenter.class.getSimpleName();
        Wg6.b(simpleName, "LoginPresenter::class.java.simpleName");
        Q = simpleName;
    }
    */

    @DexIgnore
    public LoginPresenter(Vu6 vu6, BaseActivity baseActivity) {
        Wg6.c(vu6, "mView");
        Wg6.c(baseActivity, "mContext");
        this.O = vu6;
        this.P = baseActivity;
    }

    @DexIgnore
    public final void D() {
        Rm6 unused = Gu7.d(k(), null, null, new Bi(this, null), 3, null);
    }

    @DexIgnore
    public final void E(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.U.a();
        local.d(a2, "checkSocialAccountIsExisted " + signUpSocialAuth);
        CheckAuthenticationSocialExisting checkAuthenticationSocialExisting = this.A;
        if (checkAuthenticationSocialExisting != null) {
            checkAuthenticationSocialExisting.e(new CheckAuthenticationSocialExisting.Ai(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new Ci(this, signUpSocialAuth));
        } else {
            Wg6.n("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(Q, "downloadOptionalsResources");
        Date date = new Date();
        Rm6 unused = Gu7.d(k(), null, null, new Di(this, null), 3, null);
        FetchSummaries fetchSummaries = this.p;
        if (fetchSummaries != null) {
            fetchSummaries.e(new FetchSummaries.Ai(date), null);
            FetchActivities fetchActivities = this.o;
            if (fetchActivities != null) {
                fetchActivities.e(new FetchActivities.Ai(date), null);
                FetchSleepSessions fetchSleepSessions = this.r;
                if (fetchSleepSessions != null) {
                    fetchSleepSessions.e(new FetchSleepSessions.Ai(date), null);
                    FetchSleepSummaries fetchSleepSummaries = this.s;
                    if (fetchSleepSummaries != null) {
                        fetchSleepSummaries.e(new FetchSleepSummaries.Ai(date), null);
                        FetchHeartRateSamples fetchHeartRateSamples = this.t;
                        if (fetchHeartRateSamples != null) {
                            fetchHeartRateSamples.e(new FetchHeartRateSamples.Ai(date), null);
                            FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = this.u;
                            if (fetchDailyHeartRateSummaries != null) {
                                fetchDailyHeartRateSummaries.e(new FetchDailyHeartRateSummaries.Ai(date), null);
                                FetchGoalTrackingData fetchGoalTrackingData = this.G;
                                if (fetchGoalTrackingData != null) {
                                    fetchGoalTrackingData.e(new FetchGoalTrackingData.Ai(date), null);
                                    FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries = this.F;
                                    if (fetchDailyGoalTrackingSummaries != null) {
                                        fetchDailyGoalTrackingSummaries.e(new FetchDailyGoalTrackingSummaries.Ai(date), null);
                                    } else {
                                        Wg6.n("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            Wg6.n("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        Wg6.n("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    Wg6.n("mFetchSleepSessions");
                    throw null;
                }
            } else {
                Wg6.n("mFetchActivities");
                throw null;
            }
        } else {
            Wg6.n("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final /* synthetic */ Object G(Xe6<? super Kz4<It4>> xe6) {
        return Eu7.g(Bw7.b(), new Ei(this, null), xe6);
    }

    @DexIgnore
    public final FCMRepository H() {
        FCMRepository fCMRepository = this.L;
        if (fCMRepository != null) {
            return fCMRepository;
        }
        Wg6.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final FlagRepository I() {
        FlagRepository flagRepository = this.M;
        if (flagRepository != null) {
            return flagRepository;
        }
        Wg6.n("flagRepository");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository J() {
        AlarmsRepository alarmsRepository = this.J;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        Wg6.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper K() {
        AnalyticsHelper analyticsHelper = this.B;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        Wg6.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository L() {
        DeviceRepository deviceRepository = this.m;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final Cj4 M() {
        Cj4 cj4 = this.k;
        if (cj4 != null) {
            return cj4;
        }
        Wg6.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase N() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.H;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        Wg6.n("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository O() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        Wg6.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final An4 P() {
        An4 an4 = this.n;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository Q() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        Wg6.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository R() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        Wg6.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository S() {
        UserRepository userRepository = this.l;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository T() {
        WatchLocalizationRepository watchLocalizationRepository = this.I;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        Wg6.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingRepository U() {
        WorkoutSettingRepository workoutSettingRepository = this.N;
        if (workoutSettingRepository != null) {
            return workoutSettingRepository;
        }
        Wg6.n("mWorkoutSettingRepository");
        throw null;
    }

    @DexIgnore
    public final ProfileRepository V() {
        ProfileRepository profileRepository = this.K;
        if (profileRepository != null) {
            return profileRepository;
        }
        Wg6.n("socialProfileRepository");
        throw null;
    }

    @DexIgnore
    public final void W(int i2, String str) {
        Wg6.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = SignUpPresenter.U.a();
        local.d(a2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.O.C(i2, str);
        } else if (!Hx5.b(PortfolioApp.get.instance())) {
            this.O.C(601, "");
        } else {
            this.O.C(i2, "");
        }
    }

    @DexIgnore
    public final boolean X() {
        String str = this.e;
        if (str == null || str.length() == 0) {
            this.O.P1(false, "");
        } else if (!B47.a(this.e)) {
            Vu6 vu6 = this.O;
            String c = Um5.c(PortfolioApp.get.instance(), 2131887009);
            Wg6.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            vu6.P1(true, c);
        } else {
            this.O.P1(false, "");
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void Y(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        LoginSocialUseCase loginSocialUseCase = this.h;
        if (loginSocialUseCase != null) {
            loginSocialUseCase.e(new LoginSocialUseCase.Bi(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new Fi(this));
        } else {
            Wg6.n("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void Z(int i2, String str) {
        Wg6.c(str, "errorMessage");
        if ((400 <= i2 && 407 >= i2) || (407 <= i2 && 499 >= i2)) {
            this.O.W1();
        } else if (i2 != 408) {
            this.O.C(i2, str);
        } else if (!Hx5.b(PortfolioApp.get.instance())) {
            this.O.C(601, "");
        } else {
            this.O.C(i2, "");
        }
    }

    @DexIgnore
    public final void a0() {
        FLogger.INSTANCE.getLocal().d(Q, "onLoginSuccess download user info");
        DownloadUserInfoUseCase downloadUserInfoUseCase = this.i;
        if (downloadUserInfoUseCase != null) {
            downloadUserInfoUseCase.e(new DownloadUserInfoUseCase.Bi(), new Li(this));
        } else {
            Wg6.n("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void b0(String str) {
        Wg6.c(str, "activeSerial");
        ReconnectDeviceUseCase reconnectDeviceUseCase = this.j;
        if (reconnectDeviceUseCase != null) {
            reconnectDeviceUseCase.e(new ReconnectDeviceUseCase.Ci(str), new Mi());
        } else {
            Wg6.n("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public void c0() {
        this.O.M5(this);
    }

    @DexIgnore
    public final void d0() {
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            Wg6.b(locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                Wg6.b(locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append(LocaleConverter.LOCALE_DELIMITER);
                Locale locale4 = Locale.getDefault();
                Wg6.b(locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = Q;
                local.d(str, "language: " + sb2);
                if (Vt7.j(sb2, "zh_CN", true) || Vt7.j(sb2, "zh_SG", true) || Vt7.j(sb2, "zh_TW", true)) {
                    this.O.O4(true);
                    return;
                } else {
                    this.O.O4(false);
                    return;
                }
            }
        }
        this.O.O4(false);
    }

    @DexIgnore
    public final void e0() {
        if (!X() || TextUtils.isEmpty(this.f)) {
            this.O.O0();
        } else {
            this.O.c3();
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        e0();
        d0();
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void n() {
        if (PortfolioApp.get.instance().p0()) {
            this.O.R();
        } else {
            W(601, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void o() {
        Vu6 vu6 = this.O;
        String str = this.e;
        if (str == null) {
            str = "";
        }
        vu6.H5(str);
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void p(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        this.O.i();
        E(signUpSocialAuth);
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void q(String str, String str2) {
        Wg6.c(str, Constants.EMAIL);
        Wg6.c(str2, "password");
        if (X()) {
            this.O.i();
            LoginEmailUseCase loginEmailUseCase = this.g;
            if (loginEmailUseCase != null) {
                loginEmailUseCase.e(new LoginEmailUseCase.Bi(str, str2), new Gi(this));
            } else {
                Wg6.n("mLoginEmailUseCase");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void r() {
        this.O.i();
        Dv5 dv5 = this.v;
        if (dv5 != null) {
            dv5.e(new Dv5.Bi(new WeakReference(this.P)), new Hi(this));
        } else {
            Wg6.n("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void s() {
        this.O.i();
        Ev5 ev5 = this.x;
        if (ev5 != null) {
            ev5.e(new Ev5.Bi(new WeakReference(this.P)), new Ii(this));
        } else {
            Wg6.n("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void t() {
        if (!AppHelper.g.j(this.P, "com.tencent.mm")) {
            AppHelper.g.k(this.P, "com.tencent.mm");
            return;
        }
        this.O.i();
        Gv5 gv5 = this.z;
        if (gv5 != null) {
            gv5.e(new Gv5.Bi(new WeakReference(this.P)), new Ji(this));
        } else {
            Wg6.n("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void u() {
        this.O.i();
        Hv5 hv5 = this.y;
        if (hv5 != null) {
            hv5.e(new Hv5.Bi(new WeakReference(this.P)), new Ki(this));
        } else {
            Wg6.n("mLoginWeiboUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void v(boolean z2) {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void w(String str) {
        Wg6.c(str, Constants.EMAIL);
        this.e = str;
    }

    @DexIgnore
    @Override // com.fossil.Uu6
    public void x(String str) {
        Wg6.c(str, "password");
        this.f = str;
        e0();
    }
}
