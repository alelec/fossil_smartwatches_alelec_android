package com.portfolio.platform.uirenew.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.Un5;
import com.fossil.Vn5;
import com.fossil.Wu6;
import com.fossil.Xn5;
import com.fossil.Xu6;
import com.mapped.Iface;
import com.mapped.Qg6;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.login.MFLoginWechatManager;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LoginActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a F; // = new a(null);
    @DexIgnore
    public Un5 A;
    @DexIgnore
    public Vn5 B;
    @DexIgnore
    public Xn5 C;
    @DexIgnore
    public MFLoginWechatManager D;
    @DexIgnore
    public LoginPresenter E;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void a(Context context) {
            Wg6.c(context, "context");
            context.startActivity(new Intent(context, LoginActivity.class));
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (intent != null) {
            Un5 un5 = this.A;
            if (un5 != null) {
                un5.d(i, i2, intent);
                Vn5 vn5 = this.B;
                if (vn5 != null) {
                    vn5.h(i, i2, intent);
                    Xn5 xn5 = this.C;
                    if (xn5 != null) {
                        xn5.b(i, i2, intent);
                        Fragment Y = getSupportFragmentManager().Y(2131362158);
                        if (Y != null) {
                            Y.onActivityResult(i, i2, intent);
                            return;
                        }
                        return;
                    }
                    Wg6.n("mLoginWeiboManager");
                    throw null;
                }
                Wg6.n("mLoginGoogleManager");
                throw null;
            }
            Wg6.n("mLoginFacebookManager");
            throw null;
        }
    }

    @DexIgnore
    @Override // androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, com.portfolio.platform.ui.BaseActivity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Wu6 wu6 = (Wu6) getSupportFragmentManager().Y(2131362158);
        if (wu6 == null) {
            wu6 = Wu6.k.b();
            k(wu6, Wu6.k.a(), 2131362158);
        }
        Iface iface = PortfolioApp.get.instance().getIface();
        if (wu6 != null) {
            iface.N0(new Xu6(this, wu6)).a(this);
            return;
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.login.LoginContract.View");
    }

    @DexIgnore
    @Override // androidx.fragment.app.FragmentActivity
    public void onNewIntent(Intent intent) {
        Wg6.c(intent, "intent");
        super.onNewIntent(intent);
        MFLoginWechatManager mFLoginWechatManager = this.D;
        if (mFLoginWechatManager != null) {
            Intent intent2 = getIntent();
            Wg6.b(intent2, "getIntent()");
            mFLoginWechatManager.g(intent2);
            return;
        }
        Wg6.n("mLoginWechatManager");
        throw null;
    }
}
