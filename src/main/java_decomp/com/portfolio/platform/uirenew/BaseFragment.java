package com.portfolio.platform.uirenew;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.Sr4;
import com.fossil.Ul5;
import com.fossil.V78;
import com.fossil.Vl5;
import com.fossil.Xq0;
import com.fossil.Zp0;
import com.mapped.Rc6;
import com.mapped.Wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseFragment extends Fragment implements V78.Ai, View.OnKeyListener, Ul5.Ai {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public AnalyticsHelper b;
    @DexIgnore
    public /* final */ Zp0 c; // = new Sr4(this);
    @DexIgnore
    public Vl5 d;
    @DexIgnore
    public HashMap e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public /* final */ /* synthetic */ Window a;

        @DexIgnore
        public Ai(Window window) {
            this.a = window;
        }

        @DexIgnore
        public final void onAnimationUpdate(ValueAnimator valueAnimator) {
            Window window = this.a;
            Wg6.b(window, "window");
            Wg6.b(valueAnimator, "animation");
            Object animatedValue = valueAnimator.getAnimatedValue();
            if (animatedValue != null) {
                window.setStatusBarColor(((Integer) animatedValue).intValue());
                return;
            }
            throw new Rc6("null cannot be cast to non-null type kotlin.Int");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi extends AnimatorListenerAdapter {
        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animation");
            super.onAnimationEnd(animator);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ci extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ View a;

        @DexIgnore
        public Ci(View view) {
            this.a = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            Wg6.c(animator, "animation");
            this.a.setVisibility(4);
            super.onAnimationEnd(animator);
        }
    }

    /*
    static {
        String simpleName = BaseFragment.class.getSimpleName();
        Wg6.b(simpleName, "BaseFragment::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public final Zp0 A6() {
        return this.c;
    }

    @DexIgnore
    public final AnalyticsHelper B6() {
        AnalyticsHelper analyticsHelper = this.b;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        Wg6.n("mAnalyticHelper");
        throw null;
    }

    @DexIgnore
    @Override // com.fossil.V78.Ai
    public void C4(int i, List<String> list) {
        Wg6.c(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "onPermissionsGranted: perm = " + it.next());
        }
    }

    @DexIgnore
    public final Vl5 C6() {
        return this.d;
    }

    @DexIgnore
    public String D6() {
        return f;
    }

    @DexIgnore
    public final void E6(String str) {
        Wg6.c(str, "view");
        Vl5 e2 = AnalyticsHelper.f.e();
        e2.l(str);
        this.d = e2;
        if (e2 != null) {
            e2.h(this);
        }
    }

    @DexIgnore
    public boolean F6() {
        if (!isActive() || getParentFragment() == null || !(getParentFragment() instanceof BaseFragment)) {
            return false;
        }
        Fragment parentFragment = getParentFragment();
        if (parentFragment != null) {
            return ((BaseFragment) parentFragment).F6();
        }
        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.BaseFragment");
    }

    @DexIgnore
    public final void G6(Fragment fragment, String str, int i) {
        Wg6.c(fragment, "fragment");
        Wg6.c(str, "tag");
        FragmentManager childFragmentManager = getChildFragmentManager();
        Wg6.b(childFragmentManager, "childFragmentManager");
        Xq0 j = childFragmentManager.j();
        Wg6.b(j, "fragmentManager.beginTransaction()");
        Fragment Y = childFragmentManager.Y(i);
        if (Y != null) {
            j.q(Y);
        }
        j.f(null);
        j.s(i, fragment, str);
        j.i();
    }

    @DexIgnore
    public final void H6(String str) {
        Wg6.c(str, "title");
        FLogger.INSTANCE.getLocal().d(f, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).G(str);
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void I6(String str) {
        Wg6.c(str, "content");
        LayoutInflater layoutInflater = getLayoutInflater();
        Wg6.b(layoutInflater, "layoutInflater");
        FragmentActivity activity = getActivity();
        View inflate = layoutInflater.inflate(2131558834, activity != null ? (ViewGroup) activity.findViewById(2131362178) : null);
        View findViewById = inflate.findViewById(2131362386);
        Wg6.b(findViewById, "view.findViewById<Flexib\u2026xtView>(R.id.ftv_content)");
        ((FlexibleTextView) findViewById).setText(str);
        Toast toast = new Toast(getContext());
        toast.setGravity(80, 0, 200);
        toast.setDuration(1);
        toast.setView(inflate);
        toast.show();
    }

    @DexIgnore
    public final void J6(Intent intent, String str) {
        Wg6.c(intent, "browserIntent");
        Wg6.c(str, "tag");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e2) {
            FLogger.INSTANCE.getLocal().e(str, "Exception when start url with no browser app");
        }
    }

    @DexIgnore
    @Override // com.fossil.Ul5.Ai
    public void T4() {
        FLogger.INSTANCE.getLocal().d(f, "Tracer ended");
    }

    @DexIgnore
    public final void a() {
        FLogger.INSTANCE.getLocal().d(f, "hideProgressDialog");
        if (getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).t();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    public final void b() {
        FLogger.INSTANCE.getLocal().d(f, "showProgressDialog");
        if (isActive() && getActivity() != null) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                ((BaseActivity) activity).F();
                return;
            }
            throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }

    @DexIgnore
    @Override // com.fossil.Ul5.Ai
    public void f2() {
        FLogger.INSTANCE.getLocal().d(f, "Tracer started");
        PortfolioApp.get.instance().f2(this.d);
    }

    @DexIgnore
    public final boolean isActive() {
        return isAdded();
    }

    @DexIgnore
    @Override // com.fossil.V78.Ai
    public void k1(int i, List<String> list) {
        Wg6.c(list, "perms");
        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = f;
            local.d(str, "onPermissionsDenied: perm = " + it.next());
        }
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = AnalyticsHelper.f.g();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        Vl5 vl5 = this.d;
        if (vl5 != null) {
            vl5.g();
        }
        super.onDestroyView();
        v6();
    }

    @DexIgnore
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        Wg6.c(view, "view");
        Wg6.c(keyEvent, "keyEvent");
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(f, "onKey KEYCODE_BACK");
        return F6();
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment, com.fossil.Rk0.Bi
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Wg6.c(strArr, "permissions");
        Wg6.c(iArr, "grantResults");
        super.onRequestPermissionsResult(i, strArr, iArr);
        V78.c(i, strArr, iArr, this);
    }

    @DexIgnore
    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Wg6.c(view, "view");
        super.onViewCreated(view, bundle);
        ViewGroup viewGroup = (ViewGroup) view.findViewById(2131363010);
        if (viewGroup != null) {
            String d2 = ThemeManager.l.a().d(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(d2)) {
                viewGroup.setBackgroundColor(Color.parseColor(d2));
            }
        }
        view.setOnKeyListener(this);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
    }

    @DexIgnore
    public void v6() {
        HashMap hashMap = this.e;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void w6(String str, Map<String, String> map) {
        Wg6.c(str, Constants.EVENT);
        Wg6.c(map, "values");
        AnalyticsHelper analyticsHelper = this.b;
        if (analyticsHelper != null) {
            analyticsHelper.l(str, map);
        } else {
            Wg6.n("mAnalyticHelper");
            throw null;
        }
    }

    @DexIgnore
    public void x6(String str, String str2, long j) {
        Wg6.c(str, "startColor");
        Wg6.c(str2, "endColor");
        if (Build.VERSION.SDK_INT >= 23) {
            FragmentActivity requireActivity = requireActivity();
            Wg6.b(requireActivity, "requireActivity()");
            Window window = requireActivity.getWindow();
            window.clearFlags(67108864);
            window.addFlags(RecyclerView.UNDEFINED_DURATION);
            int parseColor = Color.parseColor(str);
            int parseColor2 = Color.parseColor(str2);
            ValueAnimator ofObject = ValueAnimator.ofObject(new ArgbEvaluator(), Integer.valueOf(parseColor), Integer.valueOf(parseColor2));
            ofObject.addUpdateListener(new Ai(window));
            ofObject.setDuration(j).start();
        }
    }

    @DexIgnore
    public final void y6(View view, long j, int[] iArr) {
        Wg6.c(view, "v");
        Wg6.c(iArr, "target");
        view.setVisibility(0);
        view.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setX((float) iArr[0]);
        view.setY((float) iArr[1]);
        view.animate().setDuration(j).translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new Bi()).setInterpolator(new OvershootInterpolator(0.9f)).alpha(1.0f).start();
    }

    @DexIgnore
    public final void z6(View view, long j, int[] iArr) {
        Wg6.c(view, "v");
        Wg6.c(iArr, "target");
        view.setVisibility(0);
        view.setAlpha(1.0f);
        view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        view.animate().setDuration(j).y((float) iArr[1]).x((float) iArr[0]).setInterpolator(new AccelerateDecelerateInterpolator()).setListener(new Ci(view)).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).start();
    }
}
