package com.portfolio.platform.uirenew.signup;

import android.text.TextUtils;
import com.baseflow.geolocator.utils.LocaleConverter;
import com.fossil.B47;
import com.fossil.Dv5;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Ev5;
import com.fossil.Gu7;
import com.fossil.Gv5;
import com.fossil.Hv5;
import com.fossil.Ko7;
import com.fossil.Oz6;
import com.fossil.Pz6;
import com.fossil.Sy6;
import com.fossil.Um5;
import com.fossil.Uq4;
import com.fossil.Uy6;
import com.fossil.Vt7;
import com.fossil.Xn5;
import com.fossil.Yn7;
import com.mapped.An4;
import com.mapped.Ap4;
import com.mapped.Cd6;
import com.mapped.Cj4;
import com.mapped.Coroutine;
import com.mapped.Hx5;
import com.mapped.Il6;
import com.mapped.Jh6;
import com.mapped.Lc6;
import com.mapped.Lf6;
import com.mapped.Qg6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.app_setting.flag.data.FlagRepository;
import com.portfolio.platform.buddy_challenge.domain.FCMRepository;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.UserSettings;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.WorkoutSettingRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.AppHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.device.domain.usecase.ReconnectDeviceUseCase;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchDailyGoalTrackingSummaries;
import com.portfolio.platform.ui.goaltracking.domain.usecase.FetchGoalTrackingData;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries;
import com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples;
import com.portfolio.platform.ui.stats.activity.day.domain.usecase.FetchActivities;
import com.portfolio.platform.ui.stats.activity.month.domain.usecase.FetchSummaries;
import com.portfolio.platform.ui.stats.sleep.day.domain.usecase.FetchSleepSessions;
import com.portfolio.platform.ui.stats.sleep.month.domain.usecase.FetchSleepSummaries;
import com.portfolio.platform.ui.user.information.domain.usecase.GetUser;
import com.portfolio.platform.ui.user.usecase.DownloadUserInfoUseCase;
import com.portfolio.platform.ui.user.usecase.LoginSocialUseCase;
import com.portfolio.platform.uirenew.login.LoginPresenter;
import com.portfolio.platform.usecase.CheckAuthenticationEmailExisting;
import com.portfolio.platform.usecase.CheckAuthenticationSocialExisting;
import com.portfolio.platform.usecase.GetSecretKeyUseCase;
import com.portfolio.platform.usecase.RequestEmailOtp;
import com.zendesk.sdk.support.help.HelpSearchRecyclerViewAdapter;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpPresenter extends Oz6 {
    @DexIgnore
    public static /* final */ String S;
    @DexIgnore
    public static /* final */ Pattern T;
    @DexIgnore
    public static /* final */ Ai U; // = new Ai(null);
    @DexIgnore
    public AnalyticsHelper A;
    @DexIgnore
    public GetUser B;
    @DexIgnore
    public SummariesRepository C;
    @DexIgnore
    public SleepSummariesRepository D;
    @DexIgnore
    public GoalTrackingRepository E;
    @DexIgnore
    public FetchDailyGoalTrackingSummaries F;
    @DexIgnore
    public FetchGoalTrackingData G;
    @DexIgnore
    public RequestEmailOtp H;
    @DexIgnore
    public GetSecretKeyUseCase I;
    @DexIgnore
    public WatchLocalizationRepository J;
    @DexIgnore
    public An4 K;
    @DexIgnore
    public FCMRepository L;
    @DexIgnore
    public FlagRepository M;
    @DexIgnore
    public WorkoutSettingRepository N;
    @DexIgnore
    public String O;
    @DexIgnore
    public String P;
    @DexIgnore
    public /* final */ Pz6 Q;
    @DexIgnore
    public /* final */ BaseActivity R;
    @DexIgnore
    public Dv5 e;
    @DexIgnore
    public Xn5 f;
    @DexIgnore
    public Ev5 g;
    @DexIgnore
    public Hv5 h;
    @DexIgnore
    public Gv5 i;
    @DexIgnore
    public LoginSocialUseCase j;
    @DexIgnore
    public UserRepository k;
    @DexIgnore
    public DeviceRepository l;
    @DexIgnore
    public Uq4 m;
    @DexIgnore
    public FetchSleepSessions n;
    @DexIgnore
    public FetchSleepSummaries o;
    @DexIgnore
    public FetchActivities p;
    @DexIgnore
    public FetchSummaries q;
    @DexIgnore
    public FetchHeartRateSamples r;
    @DexIgnore
    public FetchDailyHeartRateSummaries s;
    @DexIgnore
    public AlarmsRepository t;
    @DexIgnore
    public ReconnectDeviceUseCase u;
    @DexIgnore
    public Cj4 v;
    @DexIgnore
    public DownloadUserInfoUseCase w;
    @DexIgnore
    public An4 x;
    @DexIgnore
    public CheckAuthenticationEmailExisting y;
    @DexIgnore
    public CheckAuthenticationSocialExisting z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public Ai() {
        }

        @DexIgnore
        public /* synthetic */ Ai(Qg6 qg6) {
            this();
        }

        @DexIgnore
        public final String a() {
            return SignUpPresenter.S;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi implements CoroutineUseCase.Ei<GetUser.Ai, CoroutineUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Bi(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CoroutineUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(CoroutineUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            FLogger.INSTANCE.getLocal().d(SignUpPresenter.U.a(), "Get current user failed");
        }

        @DexIgnore
        public void c(GetUser.Ai ai) {
            Wg6.c(ai, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Get current user success: " + ai.a());
            MFUser a3 = ai.a();
            if (a3 != null) {
                this.a.J().w(a3.getUserId());
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(GetUser.Ai ai) {
            c(ai);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1", f = "SignUpPresenter.kt", l = {574, 575, 579, 584, 587}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$checkOnboardingProgress$1$currentUser$1", f = "SignUpPresenter.kt", l = {587}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super MFUser>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super MFUser> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository S = this.this$0.this$0.S();
                    this.L$0 = il6;
                    this.label = 1;
                    Object currentUser = S.getCurrentUser(this);
                    return currentUser == d ? d : currentUser;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(SignUpPresenter signUpPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = signUpPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x012f  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0149  */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x017f  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0182  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0186  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 393
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.signup.SignUpPresenter.Ci.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Di implements CoroutineUseCase.Ei<CheckAuthenticationSocialExisting.Ci, CheckAuthenticationSocialExisting.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpSocialAuth b;

        @DexIgnore
        public Di(SignUpPresenter signUpPresenter, SignUpSocialAuth signUpSocialAuth) {
            this.a = signUpPresenter;
            this.b = signUpSocialAuth;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CheckAuthenticationSocialExisting.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(CheckAuthenticationSocialExisting.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.Q.h();
            this.a.V(bi.a(), "");
        }

        @DexIgnore
        public void c(CheckAuthenticationSocialExisting.Ci ci) {
            Wg6.c(ci, "responseValue");
            boolean a2 = ci.a();
            if (a2) {
                this.a.Y(this.b);
            } else if (!a2) {
                this.a.Q.g4(this.b);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(CheckAuthenticationSocialExisting.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1", f = "SignUpPresenter.kt", l = {526, 537}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$1", f = "SignUpPresenter.kt", l = {527, 528, 529, 530, HelpSearchRecyclerViewAdapter.TYPE_ARTICLE, 532, 533}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:13:0x0054  */
            /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
            /* JADX WARNING: Removed duplicated region for block: B:21:0x008c  */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x00a8  */
            /* JADX WARNING: Removed duplicated region for block: B:29:0x00c5  */
            /* JADX WARNING: Removed duplicated region for block: B:9:0x0038  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r7) {
                /*
                // Method dump skipped, instructions count: 246
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.signup.SignUpPresenter.Ei.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$downloadOptionalsResources$1$alarms$1", f = "SignUpPresenter.kt", l = {537}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super List<Alarm>>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ei this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ei ei, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ei;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super List<Alarm>> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    AlarmsRepository I = this.this$0.this$0.I();
                    this.L$0 = il6;
                    this.label = 1;
                    Object activeAlarms = I.getActiveAlarms(this);
                    return activeAlarms == d ? d : activeAlarms;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    return obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(SignUpPresenter signUpPresenter, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = signUpPresenter;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x0054  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r6 = 0
                r5 = 2
                r4 = 1
                java.lang.Object r1 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x0056
                if (r0 == r4) goto L_0x0038
                if (r0 != r5) goto L_0x0030
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
                r0 = r8
            L_0x0017:
                java.util.List r0 = (java.util.List) r0
                if (r0 != 0) goto L_0x0020
                java.util.ArrayList r0 = new java.util.ArrayList
                r0.<init>()
            L_0x0020:
                com.portfolio.platform.PortfolioApp$inner r1 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r1 = r1.instance()
                java.util.List r0 = com.fossil.Dj5.a(r0)
                r1.q1(r0)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x002f:
                return r0
            L_0x0030:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0038:
                java.lang.Object r0 = r7.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r8)
            L_0x003f:
                com.portfolio.platform.uirenew.signup.SignUpPresenter r2 = r7.this$0
                com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.signup.SignUpPresenter.x(r2)
                com.portfolio.platform.uirenew.signup.SignUpPresenter$Ei$Bii r3 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$Ei$Bii
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r5
                java.lang.Object r0 = com.fossil.Eu7.g(r2, r3, r7)
                if (r0 != r1) goto L_0x0017
                r0 = r1
                goto L_0x002f
            L_0x0056:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r0 = r7.p$
                com.portfolio.platform.uirenew.signup.SignUpPresenter r2 = r7.this$0
                com.fossil.Dv7 r2 = com.portfolio.platform.uirenew.signup.SignUpPresenter.y(r2)
                com.portfolio.platform.uirenew.signup.SignUpPresenter$Ei$Aii r3 = new com.portfolio.platform.uirenew.signup.SignUpPresenter$Ei$Aii
                r3.<init>(r7, r6)
                r7.L$0 = r0
                r7.label = r4
                java.lang.Object r2 = com.fossil.Eu7.g(r2, r3, r7)
                if (r2 != r1) goto L_0x003f
                r0 = r1
                goto L_0x002f
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.signup.SignUpPresenter.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Fi implements CoroutineUseCase.Ei<LoginSocialUseCase.Ci, LoginSocialUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Fi(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(LoginSocialUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(LoginSocialUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            this.a.Q.h();
            this.a.V(ai.a(), "");
        }

        @DexIgnore
        public void c(LoginSocialUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            PortfolioApp.get.instance().getIface().z0(this.a);
            this.a.Z();
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(LoginSocialUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Gi implements CoroutineUseCase.Ei<DownloadUserInfoUseCase.Ci, DownloadUserInfoUseCase.Ai> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onError$1", f = "SignUpPresenter.kt", l = {514}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ DownloadUserInfoUseCase.Ai $errorValue;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Gi gi, DownloadUserInfoUseCase.Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$errorValue = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, this.$errorValue, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    UserRepository S = this.this$0.a.S();
                    this.L$0 = il6;
                    this.label = 1;
                    if (S.clearAllUser(this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                this.this$0.a.Q.h();
                this.this$0.a.V(this.$errorValue.a(), this.$errorValue.b());
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1", f = "SignUpPresenter.kt", l = {427, 436, HelpSearchRecyclerViewAdapter.TYPE_NO_RESULTS, 448, 464, 472, 503}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ MFUser $currentUser;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Gi this$0;

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$1", f = "SignUpPresenter.kt", l = {427}, m = "invokeSuspend")
            public static final class Aiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Aiii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Aiii aiii = new Aiii(this.this$0, xe6);
                    aiii.p$ = (Il6) obj;
                    throw null;
                    //return aiii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                    throw null;
                    //return ((Aiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        UserRepository S = this.this$0.this$0.a.S();
                        this.L$0 = il6;
                        this.label = 1;
                        if (S.clearAllUser(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return Cd6.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$2", f = "SignUpPresenter.kt", l = {442, 443}, m = "invokeSuspend")
            public static final class Biii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<UserSettings>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Biii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Biii biii = new Biii(this.this$0, xe6);
                    biii.p$ = (Il6) obj;
                    throw null;
                    //return biii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<UserSettings>> xe6) {
                    throw null;
                    //return ((Biii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Il6 il6;
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        il6 = this.p$;
                        PortfolioApp instance = PortfolioApp.get.instance();
                        this.L$0 = il6;
                        this.label = 1;
                        if (instance.g2(this) == d) {
                            return d;
                        }
                    } else if (i == 1) {
                        il6 = (Il6) this.L$0;
                        El7.b(obj);
                    } else if (i == 2) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    UserRepository S = this.this$0.this$0.a.S();
                    this.L$0 = il6;
                    this.label = 2;
                    Object userSettingFromServer = S.getUserSettingFromServer(this);
                    return userSettingFromServer == d ? d : userSettingFromServer;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            public static final class Ciii implements CoroutineUseCase.Ei<Uy6, Sy6> {
                @DexIgnore
                public /* final */ /* synthetic */ Bii a;
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 b;
                @DexIgnore
                public /* final */ /* synthetic */ Jh6 c;
                @DexIgnore
                public /* final */ /* synthetic */ List d;

                @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
                @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$4$onError$1", f = "SignUpPresenter.kt", l = {491}, m = "invokeSuspend")
                public static final class Aiiii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Sy6 $errorValue;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public Il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ Ciii this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public Aiiii(Ciii ciii, Sy6 sy6, Xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = ciii;
                        this.$errorValue = sy6;
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                        Wg6.c(xe6, "completion");
                        Aiiii aiiii = new Aiiii(this.this$0, this.$errorValue, xe6);
                        aiiii.p$ = (Il6) obj;
                        throw null;
                        //return aiiii;
                    }

                    @DexIgnore
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                    @Override // com.mapped.Coroutine
                    public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                        throw null;
                        //return ((Aiiii) create(il6, xe6)).invokeSuspend(Cd6.a);
                    }

                    @DexIgnore
                    @Override // com.fossil.Zn7
                    public final Object invokeSuspend(Object obj) {
                        Object d = Yn7.d();
                        int i = this.label;
                        if (i == 0) {
                            El7.b(obj);
                            Il6 il6 = this.p$;
                            Dv7 h = this.this$0.a.this$0.a.h();
                            SignUpPresenter$g$b$c$a$a signUpPresenter$g$b$c$a$a = new SignUpPresenter$g$b$c$a$a(this, null);
                            this.L$0 = il6;
                            this.label = 1;
                            if (Eu7.g(h, signUpPresenter$g$b$c$a$a, this) == d) {
                                return d;
                            }
                        } else if (i == 1) {
                            Il6 il62 = (Il6) this.L$0;
                            El7.b(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        this.this$0.a.this$0.a.Q.h();
                        this.this$0.a.this$0.a.V(this.$errorValue.a(), this.$errorValue.b());
                        return Cd6.a;
                    }
                }

                @DexIgnore
                public Ciii(Bii bii, Jh6 jh6, Jh6 jh62, List list) {
                    this.a = bii;
                    this.b = jh6;
                    this.c = jh62;
                    this.d = list;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void a(Sy6 sy6) {
                    b(sy6);
                }

                @DexIgnore
                public void b(Sy6 sy6) {
                    Wg6.c(sy6, "errorValue");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = SignUpPresenter.U.a();
                    local.d(a2, "onLoginSuccess download device setting fail " + sy6.a());
                    Rm6 unused = Gu7.d(this.a.this$0.a.k(), null, null, new Aiiii(this, sy6, null), 3, null);
                }

                @DexIgnore
                public void c(Uy6 uy6) {
                    Wg6.c(uy6, "responseValue");
                    PortfolioApp.get.instance().n1(this.b.element, this.c.element);
                    FLogger.INSTANCE.getLocal().d(SignUpPresenter.U.a(), "onLoginSuccess download device setting success");
                    for (Lc6 lc6 : this.d) {
                        PortfolioApp.get.instance().F1((String) lc6.getFirst(), (String) lc6.getSecond());
                    }
                    this.a.this$0.a.a0(this.b.element);
                    PortfolioApp.get.instance().S1(this.a.this$0.a.L(), false, 13);
                    this.a.this$0.a.D();
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // com.portfolio.platform.CoroutineUseCase.Ei
                public /* bridge */ /* synthetic */ void onSuccess(Uy6 uy6) {
                    c(uy6);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @Lf6(c = "com.portfolio.platform.uirenew.signup.SignUpPresenter$onLoginSocialSuccess$1$onSuccess$1$response$1", f = "SignUpPresenter.kt", l = {448}, m = "invokeSuspend")
            public static final class Diii extends Ko7 implements Coroutine<Il6, Xe6<? super Ap4<ApiResponse<Device>>>, Object> {
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public Il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ Bii this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public Diii(Bii bii, Xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = bii;
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                    Wg6.c(xe6, "completion");
                    Diii diii = new Diii(this.this$0, xe6);
                    diii.p$ = (Il6) obj;
                    throw null;
                    //return diii;
                }

                @DexIgnore
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
                @Override // com.mapped.Coroutine
                public final Object invoke(Il6 il6, Xe6<? super Ap4<ApiResponse<Device>>> xe6) {
                    throw null;
                    //return ((Diii) create(il6, xe6)).invokeSuspend(Cd6.a);
                }

                @DexIgnore
                @Override // com.fossil.Zn7
                public final Object invokeSuspend(Object obj) {
                    Object d = Yn7.d();
                    int i = this.label;
                    if (i == 0) {
                        El7.b(obj);
                        Il6 il6 = this.p$;
                        DeviceRepository K = this.this$0.this$0.a.K();
                        this.L$0 = il6;
                        this.label = 1;
                        Object downloadDeviceList = K.downloadDeviceList(this);
                        return downloadDeviceList == d ? d : downloadDeviceList;
                    } else if (i == 1) {
                        Il6 il62 = (Il6) this.L$0;
                        El7.b(obj);
                        return obj;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Gi gi, MFUser mFUser, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = gi;
                this.$currentUser = mFUser;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, this.$currentUser, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:17:0x00c8  */
            /* JADX WARNING: Removed duplicated region for block: B:27:0x0126  */
            /* JADX WARNING: Removed duplicated region for block: B:36:0x01a9  */
            /* JADX WARNING: Removed duplicated region for block: B:40:0x01d3  */
            /* JADX WARNING: Removed duplicated region for block: B:55:0x0296  */
            /* JADX WARNING: Removed duplicated region for block: B:63:0x0301  */
            /* JADX WARNING: Removed duplicated region for block: B:71:0x034f  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r15) {
                /*
                // Method dump skipped, instructions count: 870
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.uirenew.signup.SignUpPresenter.Gi.Bii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Gi(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(DownloadUserInfoUseCase.Ai ai) {
            b(ai);
        }

        @DexIgnore
        public void b(DownloadUserInfoUseCase.Ai ai) {
            Wg6.c(ai, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "onLoginSuccess download userInfo failed " + ai.a());
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Aii(this, ai, null), 3, null);
        }

        @DexIgnore
        public void c(DownloadUserInfoUseCase.Ci ci) {
            Wg6.c(ci, "responseValue");
            Rm6 unused = Gu7.d(this.a.k(), null, null, new Bii(this, ci.a(), null), 3, null);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(DownloadUserInfoUseCase.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Hi implements CoroutineUseCase.Ei<ReconnectDeviceUseCase.Ei, ReconnectDeviceUseCase.Di> {
        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(ReconnectDeviceUseCase.Di di) {
            b(di);
        }

        @DexIgnore
        public void b(ReconnectDeviceUseCase.Di di) {
            Wg6.c(di, "errorValue");
        }

        @DexIgnore
        public void c(ReconnectDeviceUseCase.Ei ei) {
            Wg6.c(ei, "responseValue");
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(ReconnectDeviceUseCase.Ei ei) {
            c(ei);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ii implements CoroutineUseCase.Ei<RequestEmailOtp.Ci, RequestEmailOtp.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailAuth b;

        @DexIgnore
        public Ii(SignUpPresenter signUpPresenter, SignUpEmailAuth signUpEmailAuth) {
            this.a = signUpPresenter;
            this.b = signUpEmailAuth;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(RequestEmailOtp.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(RequestEmailOtp.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.Q.h();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "requestOtpCode errorCode=" + bi.a() + " message=" + bi.b());
            this.a.Q.l3(bi.a(), bi.b());
        }

        @DexIgnore
        public void c(RequestEmailOtp.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.Q.h();
            this.a.Q.y3(this.b);
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(RequestEmailOtp.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ji implements CoroutineUseCase.Ei<CheckAuthenticationEmailExisting.Ci, CheckAuthenticationEmailExisting.Bi> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public Ji(SignUpPresenter signUpPresenter, String str, String str2) {
            this.a = signUpPresenter;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(CheckAuthenticationEmailExisting.Bi bi) {
            b(bi);
        }

        @DexIgnore
        public void b(CheckAuthenticationEmailExisting.Bi bi) {
            Wg6.c(bi, "errorValue");
            this.a.Q.h();
            this.a.V(bi.a(), "");
        }

        @DexIgnore
        public void c(CheckAuthenticationEmailExisting.Ci ci) {
            Wg6.c(ci, "responseValue");
            this.a.F();
            boolean a2 = ci.a();
            if (a2) {
                this.a.Q.h();
                Pz6 pz6 = this.a.Q;
                String c2 = Um5.c(PortfolioApp.get.instance(), 2131887007);
                Wg6.b(c2, "LanguageHelper.getString\u2026_ThisEmailIsAlreadyInUse)");
                pz6.g6(c2);
                this.a.C();
            } else if (!a2) {
                SignUpEmailAuth signUpEmailAuth = new SignUpEmailAuth();
                signUpEmailAuth.setEmail(this.b);
                signUpEmailAuth.setPassword(this.c);
                this.a.b0(signUpEmailAuth);
            }
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(CheckAuthenticationEmailExisting.Ci ci) {
            c(ci);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ki implements CoroutineUseCase.Ei<Dv5.Di, Dv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ki(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Dv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Dv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginFacebook failed with error=" + ci.a());
            this.a.Q.h();
            if (2 != ci.a()) {
                this.a.V(ci.a(), "");
            }
        }

        @DexIgnore
        public void c(Dv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginFacebook success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Dv5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Li implements CoroutineUseCase.Ei<Ev5.Di, Ev5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Li(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Ev5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Ev5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginGoogle failed with error=" + ci.a());
            this.a.Q.h();
            if (2 != ci.a()) {
                this.a.V(ci.a(), "");
            }
        }

        @DexIgnore
        public void c(Ev5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginGoogle success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Ev5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Mi implements CoroutineUseCase.Ei<Gv5.Di, Gv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Mi(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Gv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Gv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginWechat failed with error=" + ci.a());
            this.a.Q.h();
            this.a.V(ci.a(), "");
        }

        @DexIgnore
        public void c(Gv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginWechat success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Gv5.Di di) {
            c(di);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ni implements CoroutineUseCase.Ei<Hv5.Di, Hv5.Ci> {
        @DexIgnore
        public /* final */ /* synthetic */ SignUpPresenter a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Ni(SignUpPresenter signUpPresenter) {
            this.a = signUpPresenter;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void a(Hv5.Ci ci) {
            b(ci);
        }

        @DexIgnore
        public void b(Hv5.Ci ci) {
            Wg6.c(ci, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginWeibo failed with error=" + ci.a());
            this.a.Q.h();
            this.a.V(ci.a(), "");
        }

        @DexIgnore
        public void c(Hv5.Di di) {
            Wg6.c(di, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = SignUpPresenter.U.a();
            local.d(a2, "Inside .loginWeibo success with result=" + di.a());
            this.a.E(di.a());
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
        @Override // com.portfolio.platform.CoroutineUseCase.Ei
        public /* bridge */ /* synthetic */ void onSuccess(Hv5.Di di) {
            c(di);
        }
    }

    /*
    static {
        String simpleName = SignUpPresenter.class.getSimpleName();
        Wg6.b(simpleName, "SignUpPresenter::class.java.simpleName");
        S = simpleName;
        Pattern compile = Pattern.compile("((?=.*\\d)(?=.*[a-zA-Z]).+)");
        if (compile != null) {
            T = compile;
        } else {
            Wg6.i();
            throw null;
        }
    }
    */

    @DexIgnore
    public SignUpPresenter(Pz6 pz6, BaseActivity baseActivity) {
        Wg6.c(pz6, "mView");
        Wg6.c(baseActivity, "mContext");
        this.Q = pz6;
        this.R = baseActivity;
    }

    @DexIgnore
    public final void C() {
        GetUser getUser = this.B;
        if (getUser != null) {
            getUser.e(null, new Bi(this));
        } else {
            Wg6.n("mGetUser");
            throw null;
        }
    }

    @DexIgnore
    public final Rm6 D() {
        return Gu7.d(k(), null, null, new Ci(this, null), 3, null);
    }

    @DexIgnore
    public final void E(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = S;
        local.d(str, "checkSocialAccountIsExisted " + signUpSocialAuth);
        CheckAuthenticationSocialExisting checkAuthenticationSocialExisting = this.z;
        if (checkAuthenticationSocialExisting != null) {
            checkAuthenticationSocialExisting.e(new CheckAuthenticationSocialExisting.Ai(signUpSocialAuth.getService(), signUpSocialAuth.getToken()), new Di(this, signUpSocialAuth));
        } else {
            Wg6.n("mCheckAuthenticationSocialExisting");
            throw null;
        }
    }

    @DexIgnore
    public final void F() {
        FLogger.INSTANCE.getLocal().d(LoginPresenter.R.a(), "downloadOptionalsResources");
        Date date = new Date();
        Rm6 unused = Gu7.d(k(), null, null, new Ei(this, null), 3, null);
        FetchSummaries fetchSummaries = this.q;
        if (fetchSummaries != null) {
            fetchSummaries.e(new FetchSummaries.Ai(date), null);
            FetchActivities fetchActivities = this.p;
            if (fetchActivities != null) {
                fetchActivities.e(new FetchActivities.Ai(date), null);
                FetchSleepSessions fetchSleepSessions = this.n;
                if (fetchSleepSessions != null) {
                    fetchSleepSessions.e(new FetchSleepSessions.Ai(date), null);
                    FetchSleepSummaries fetchSleepSummaries = this.o;
                    if (fetchSleepSummaries != null) {
                        fetchSleepSummaries.e(new FetchSleepSummaries.Ai(date), null);
                        FetchHeartRateSamples fetchHeartRateSamples = this.r;
                        if (fetchHeartRateSamples != null) {
                            fetchHeartRateSamples.e(new FetchHeartRateSamples.Ai(date), null);
                            FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = this.s;
                            if (fetchDailyHeartRateSummaries != null) {
                                fetchDailyHeartRateSummaries.e(new FetchDailyHeartRateSummaries.Ai(date), null);
                                FetchGoalTrackingData fetchGoalTrackingData = this.G;
                                if (fetchGoalTrackingData != null) {
                                    fetchGoalTrackingData.e(new FetchGoalTrackingData.Ai(date), null);
                                    FetchDailyGoalTrackingSummaries fetchDailyGoalTrackingSummaries = this.F;
                                    if (fetchDailyGoalTrackingSummaries != null) {
                                        fetchDailyGoalTrackingSummaries.e(new FetchDailyGoalTrackingSummaries.Ai(date), null);
                                    } else {
                                        Wg6.n("mFetchDailyGoalTrackingSummaries");
                                        throw null;
                                    }
                                } else {
                                    Wg6.n("mFetchGoalTrackingData");
                                    throw null;
                                }
                            } else {
                                Wg6.n("mFetchDailyHeartRateSummaries");
                                throw null;
                            }
                        } else {
                            Wg6.n("mFetchHeartRateSamples");
                            throw null;
                        }
                    } else {
                        Wg6.n("mFetchSleepSummaries");
                        throw null;
                    }
                } else {
                    Wg6.n("mFetchSleepSessions");
                    throw null;
                }
            } else {
                Wg6.n("mFetchActivities");
                throw null;
            }
        } else {
            Wg6.n("mFetchSummaries");
            throw null;
        }
    }

    @DexIgnore
    public final FCMRepository G() {
        FCMRepository fCMRepository = this.L;
        if (fCMRepository != null) {
            return fCMRepository;
        }
        Wg6.n("fcmRepository");
        throw null;
    }

    @DexIgnore
    public final FlagRepository H() {
        FlagRepository flagRepository = this.M;
        if (flagRepository != null) {
            return flagRepository;
        }
        Wg6.n("flagRepository");
        throw null;
    }

    @DexIgnore
    public final AlarmsRepository I() {
        AlarmsRepository alarmsRepository = this.t;
        if (alarmsRepository != null) {
            return alarmsRepository;
        }
        Wg6.n("mAlarmsRepository");
        throw null;
    }

    @DexIgnore
    public final AnalyticsHelper J() {
        AnalyticsHelper analyticsHelper = this.A;
        if (analyticsHelper != null) {
            return analyticsHelper;
        }
        Wg6.n("mAnalyticsHelper");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository K() {
        DeviceRepository deviceRepository = this.l;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        Wg6.n("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final Cj4 L() {
        Cj4 cj4 = this.v;
        if (cj4 != null) {
            return cj4;
        }
        Wg6.n("mDeviceSettingFactory");
        throw null;
    }

    @DexIgnore
    public final GetSecretKeyUseCase M() {
        GetSecretKeyUseCase getSecretKeyUseCase = this.I;
        if (getSecretKeyUseCase != null) {
            return getSecretKeyUseCase;
        }
        Wg6.n("mGetSecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository N() {
        GoalTrackingRepository goalTrackingRepository = this.E;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        Wg6.n("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final An4 O() {
        An4 an4 = this.K;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final An4 P() {
        An4 an4 = this.x;
        if (an4 != null) {
            return an4;
        }
        Wg6.n("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository Q() {
        SleepSummariesRepository sleepSummariesRepository = this.D;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        Wg6.n("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final SummariesRepository R() {
        SummariesRepository summariesRepository = this.C;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        Wg6.n("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository S() {
        UserRepository userRepository = this.k;
        if (userRepository != null) {
            return userRepository;
        }
        Wg6.n("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository T() {
        WatchLocalizationRepository watchLocalizationRepository = this.J;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        Wg6.n("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final WorkoutSettingRepository U() {
        WorkoutSettingRepository workoutSettingRepository = this.N;
        if (workoutSettingRepository != null) {
            return workoutSettingRepository;
        }
        Wg6.n("mWorkoutSettingRepository");
        throw null;
    }

    @DexIgnore
    public final void V(int i2, String str) {
        Wg6.c(str, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = S;
        local.d(str2, "handleError errorCode=" + i2 + " message=" + str);
        if (i2 != 408) {
            this.Q.C(i2, str);
        } else if (!Hx5.b(PortfolioApp.get.instance())) {
            this.Q.C(601, "");
        } else {
            this.Q.C(i2, "");
        }
    }

    @DexIgnore
    public final boolean W() {
        String str = this.O;
        if (str == null || str.length() == 0) {
            this.Q.n0(false, false, "");
        } else if (!B47.a(this.O)) {
            Pz6 pz6 = this.Q;
            String c = Um5.c(PortfolioApp.get.instance(), 2131887009);
            Wg6.b(c, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            pz6.n0(false, true, c);
        } else {
            this.Q.n0(true, false, "");
            return true;
        }
        return false;
    }

    @DexIgnore
    public final boolean X() {
        if (!TextUtils.isEmpty(this.P)) {
            String str = this.P;
            if (str != null) {
                boolean z2 = str.length() >= 7;
                boolean matches = T.matcher(this.P).matches();
                this.Q.U3(z2, matches);
                if (z2 && matches) {
                    return true;
                }
            } else {
                Wg6.i();
                throw null;
            }
        } else {
            this.Q.U3(false, false);
        }
        return false;
    }

    @DexIgnore
    public final void Y(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        LoginSocialUseCase loginSocialUseCase = this.j;
        if (loginSocialUseCase != null) {
            loginSocialUseCase.e(new LoginSocialUseCase.Bi(signUpSocialAuth.getService(), signUpSocialAuth.getToken(), signUpSocialAuth.getClientId()), new Fi(this));
        } else {
            Wg6.n("mLoginSocialUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void Z() {
        FLogger.INSTANCE.getLocal().d(S, "onLoginSuccess download user info");
        DownloadUserInfoUseCase downloadUserInfoUseCase = this.w;
        if (downloadUserInfoUseCase != null) {
            downloadUserInfoUseCase.e(new DownloadUserInfoUseCase.Bi(), new Gi(this));
        } else {
            Wg6.n("mDownloadUserInfoUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void a0(String str) {
        Wg6.c(str, "activeSerial");
        ReconnectDeviceUseCase reconnectDeviceUseCase = this.u;
        if (reconnectDeviceUseCase != null) {
            reconnectDeviceUseCase.e(new ReconnectDeviceUseCase.Ci(str), new Hi());
        } else {
            Wg6.n("mReconnectDeviceUseCase");
            throw null;
        }
    }

    @DexIgnore
    public final void b0(SignUpEmailAuth signUpEmailAuth) {
        Wg6.c(signUpEmailAuth, "emailAuth");
        RequestEmailOtp requestEmailOtp = this.H;
        if (requestEmailOtp != null) {
            requestEmailOtp.e(new RequestEmailOtp.Ai(signUpEmailAuth.getEmail()), new Ii(this, signUpEmailAuth));
        } else {
            Wg6.n("mRequestEmailOtp");
            throw null;
        }
    }

    @DexIgnore
    public void c0() {
        this.Q.M5(this);
    }

    @DexIgnore
    public final void d0() {
        Locale locale = Locale.getDefault();
        Wg6.b(locale, "Locale.getDefault()");
        if (!TextUtils.isEmpty(locale.getLanguage())) {
            Locale locale2 = Locale.getDefault();
            Wg6.b(locale2, "Locale.getDefault()");
            if (!TextUtils.isEmpty(locale2.getCountry())) {
                StringBuilder sb = new StringBuilder();
                Locale locale3 = Locale.getDefault();
                Wg6.b(locale3, "Locale.getDefault()");
                sb.append(locale3.getLanguage());
                sb.append(LocaleConverter.LOCALE_DELIMITER);
                Locale locale4 = Locale.getDefault();
                Wg6.b(locale4, "Locale.getDefault()");
                sb.append(locale4.getCountry());
                String sb2 = sb.toString();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = S;
                local.d(str, "language: " + sb2);
                if (Vt7.j(sb2, "zh_CN", true) || Vt7.j(sb2, "zh_SG", true) || Vt7.j(sb2, "zh_TW", true)) {
                    this.Q.d3(true);
                    return;
                } else {
                    this.Q.d3(false);
                    return;
                }
            }
        }
        this.Q.d3(false);
    }

    @DexIgnore
    public final void e0() {
        boolean z2 = TextUtils.isEmpty(this.O) || TextUtils.isEmpty(this.P);
        boolean W = W();
        boolean X = X();
        if (z2 || !X || !W) {
            this.Q.B4();
        } else {
            this.Q.x2();
        }
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void l() {
        this.Q.f();
        e0();
        d0();
    }

    @DexIgnore
    @Override // com.fossil.Fq4
    public void m() {
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void n() {
        if (PortfolioApp.get.instance().p0()) {
            this.Q.R();
        } else {
            V(601, "");
        }
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void o(boolean z2) {
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void p(String str) {
        Wg6.c(str, Constants.EMAIL);
        this.O = str;
        W();
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void q(String str) {
        Wg6.c(str, "password");
        this.P = str;
        e0();
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void r(SignUpSocialAuth signUpSocialAuth) {
        Wg6.c(signUpSocialAuth, "auth");
        this.Q.i();
        E(signUpSocialAuth);
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void s(String str, String str2) {
        Wg6.c(str, Constants.EMAIL);
        Wg6.c(str2, "password");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = S;
        local.d(str3, "signupEmail " + str + ' ' + str2);
        if (W()) {
            this.Q.i();
            CheckAuthenticationEmailExisting checkAuthenticationEmailExisting = this.y;
            if (checkAuthenticationEmailExisting != null) {
                checkAuthenticationEmailExisting.e(new CheckAuthenticationEmailExisting.Ai(str), new Ji(this, str, str2));
            } else {
                Wg6.n("mCheckAuthenticationEmailExisting");
                throw null;
            }
        }
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void t() {
        this.Q.i();
        Dv5 dv5 = this.e;
        if (dv5 != null) {
            dv5.e(new Dv5.Bi(new WeakReference(this.R)), new Ki(this));
        } else {
            Wg6.n("mLoginFacebookUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void u() {
        this.Q.i();
        Ev5 ev5 = this.g;
        if (ev5 != null) {
            ev5.e(new Ev5.Bi(new WeakReference(this.R)), new Li(this));
        } else {
            Wg6.n("mLoginGoogleUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void v() {
        if (!AppHelper.g.j(this.R, "com.tencent.mm")) {
            AppHelper.g.k(this.R, "com.tencent.mm");
            return;
        }
        this.Q.i();
        Gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.e(new Gv5.Bi(new WeakReference(this.R)), new Mi(this));
        } else {
            Wg6.n("mLoginWechatUseCase");
            throw null;
        }
    }

    @DexIgnore
    @Override // com.fossil.Oz6
    public void w() {
        this.Q.i();
        Hv5 hv5 = this.h;
        if (hv5 != null) {
            hv5.e(new Hv5.Bi(new WeakReference(this.R)), new Ni(this));
        } else {
            Wg6.n("mLoginWeiboUseCase");
            throw null;
        }
    }
}
