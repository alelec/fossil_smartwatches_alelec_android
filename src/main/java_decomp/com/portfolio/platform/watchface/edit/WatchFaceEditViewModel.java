package com.portfolio.platform.watchface.edit;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.A97;
import com.fossil.Ab7;
import com.fossil.Ao7;
import com.fossil.B87;
import com.fossil.Bb7;
import com.fossil.Bw7;
import com.fossil.Cb7;
import com.fossil.Ct0;
import com.fossil.Cy1;
import com.fossil.Dv7;
import com.fossil.El7;
import com.fossil.Eu7;
import com.fossil.Fb7;
import com.fossil.Gb7;
import com.fossil.Gl7;
import com.fossil.Gu7;
import com.fossil.Hb7;
import com.fossil.Hl5;
import com.fossil.Hq4;
import com.fossil.I77;
import com.fossil.Ib7;
import com.fossil.Ik5;
import com.fossil.Im7;
import com.fossil.J37;
import com.fossil.Jn5;
import com.fossil.Ko7;
import com.fossil.L77;
import com.fossil.Lb7;
import com.fossil.Mo5;
import com.fossil.O87;
import com.fossil.Ob7;
import com.fossil.Pb7;
import com.fossil.S87;
import com.fossil.U08;
import com.fossil.Ub7;
import com.fossil.Um5;
import com.fossil.Us0;
import com.fossil.Vt7;
import com.fossil.W08;
import com.fossil.W67;
import com.fossil.Yn7;
import com.fossil.Za7;
import com.google.gson.Gson;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Jf6;
import com.mapped.Lf6;
import com.mapped.Rc6;
import com.mapped.Rm6;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.misfit.frameworks.buttonservice.utils.ConversionUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.DianaAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.source.DianaAppSettingRepository;
import com.portfolio.platform.data.source.DianaWatchFaceRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.manager.CustomizeRealDataManager;
import com.portfolio.platform.preset.data.source.DianaPresetRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchFaceEditViewModel extends Hq4 {
    @DexIgnore
    public /* final */ FileRepository A;
    @DexIgnore
    public /* final */ WFAssetRepository B;
    @DexIgnore
    public /* final */ DianaAppSettingRepository C;
    @DexIgnore
    public /* final */ DianaWatchFaceRepository D;
    @DexIgnore
    public /* final */ MutableLiveData<Boolean> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Gl7<Boolean, Boolean, Integer>> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Ub7> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Object> k; // = new MutableLiveData<>();
    @DexIgnore
    public Mo5 l;
    @DexIgnore
    public Ai m;
    @DexIgnore
    public Ob7 n; // = new Ob7(null, null, 2, null);
    @DexIgnore
    public Ob7 o; // = new Ob7(null, null, 2, null);
    @DexIgnore
    public /* final */ List<DianaAppSetting> p; // = new ArrayList();
    @DexIgnore
    public /* final */ MutableLiveData<Bi> q; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<List<S87>> r; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ Set<String> s; // = new LinkedHashSet();
    @DexIgnore
    public /* final */ Map<Integer, Object> t; // = new LinkedHashMap();
    @DexIgnore
    public /* final */ U08 u; // = W08.b(false, 1, null);
    @DexIgnore
    public /* final */ Qi v; // = new Qi(this);
    @DexIgnore
    public /* final */ CustomizeRealDataManager w;
    @DexIgnore
    public /* final */ UserRepository x;
    @DexIgnore
    public /* final */ CustomizeRealDataRepository y;
    @DexIgnore
    public /* final */ DianaPresetRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Ai {
        @DexIgnore
        public /* final */ L77 a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public Ai(L77 l77, String str) {
            Wg6.c(l77, "type");
            Wg6.c(str, "name");
            this.a = l77;
            this.b = str;
        }

        @DexIgnore
        public final String a() {
            return this.b;
        }

        @DexIgnore
        public final L77 b() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Ai) {
                    Ai ai = (Ai) obj;
                    if (!Wg6.a(this.a, ai.a) || !Wg6.a(this.b, ai.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            L77 l77 = this.a;
            int hashCode = l77 != null ? l77.hashCode() : 0;
            String str = this.b;
            if (str != null) {
                i = str.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "BackgroundData(type=" + this.a + ", name=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Bi {
        @DexIgnore
        public /* final */ Ab7 a;
        @DexIgnore
        public /* final */ Bb7 b;

        @DexIgnore
        public Bi(Ab7 ab7, Bb7 bb7) {
            Wg6.c(ab7, Constants.EVENT);
            Wg6.c(bb7, "complication");
            this.a = ab7;
            this.b = bb7;
        }

        @DexIgnore
        public final Bb7 a() {
            return this.b;
        }

        @DexIgnore
        public final Ab7 b() {
            return this.a;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof Bi) {
                    Bi bi = (Bi) obj;
                    if (!Wg6.a(this.a, bi.a) || !Wg6.a(this.b, bi.b)) {
                        return false;
                    }
                }
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = 0;
            Ab7 ab7 = this.a;
            int hashCode = ab7 != null ? ab7.hashCode() : 0;
            Bb7 bb7 = this.b;
            if (bb7 != null) {
                i = bb7.hashCode();
            }
            return (hashCode * 31) + i;
        }

        @DexIgnore
        public String toString() {
            return "ComplicationWrapperEvent(event=" + this.a + ", complication=" + this.b + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$applyTheme$2", f = "WatchFaceEditViewModel.kt", l = {241}, m = "invokeSuspend")
    public static final class Ci extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$applyTheme$2$1", f = "WatchFaceEditViewModel.kt", l = {242, 245}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ci this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Ci ci, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ci;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(this.this$0, xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: Removed duplicated region for block: B:12:0x0049  */
            /* JADX WARNING: Removed duplicated region for block: B:18:0x0086  */
            @Override // com.fossil.Zn7
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final java.lang.Object invokeSuspend(java.lang.Object r8) {
                /*
                    r7 = this;
                    r3 = 0
                    r6 = 2
                    r4 = 1
                    r1 = 0
                    java.lang.Object r5 = com.fossil.Yn7.d()
                    int r0 = r7.label
                    if (r0 == 0) goto L_0x0071
                    if (r0 == r4) goto L_0x0037
                    if (r0 != r6) goto L_0x002f
                    java.lang.Object r0 = r7.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r8)
                L_0x0017:
                    com.fossil.B77 r0 = com.fossil.B77.a
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r1 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r1 = r1.this$0
                    com.fossil.Ob7 r1 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.p(r1)
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r2 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r2 = r2.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ai r2 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.q(r2)
                    r0.g(r1, r2)
                L_0x002c:
                    com.mapped.Cd6 r0 = com.mapped.Cd6.a
                L_0x002e:
                    return r0
                L_0x002f:
                    java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                    java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                    r0.<init>(r1)
                    throw r0
                L_0x0037:
                    java.lang.Object r0 = r7.L$0
                    com.mapped.Il6 r0 = (com.mapped.Il6) r0
                    com.fossil.El7.b(r8)
                    r4 = r0
                    r2 = r8
                L_0x0040:
                    r0 = r2
                    java.lang.Boolean r0 = (java.lang.Boolean) r0
                    boolean r0 = r0.booleanValue()
                    if (r0 == 0) goto L_0x0086
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r0 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r0.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.I(r0)
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r0 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r0.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.n(r0)
                    com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
                    com.portfolio.platform.PortfolioApp r0 = r0.instance()
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r1 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r1 = r1.this$0
                    com.fossil.Ob7 r1 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.p(r1)
                    r7.L$0 = r4
                    r7.label = r6
                    java.lang.Object r0 = r0.q(r1, r7)
                    if (r0 != r5) goto L_0x0017
                    r0 = r5
                    goto L_0x002e
                L_0x0071:
                    com.fossil.El7.b(r8)
                    com.mapped.Il6 r0 = r7.p$
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r2 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r2 = r2.this$0
                    r7.L$0 = r0
                    r7.label = r4
                    java.lang.Object r2 = r2.Q(r7)
                    if (r2 != r5) goto L_0x009b
                    r0 = r5
                    goto L_0x002e
                L_0x0086:
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r0 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r0.this$0
                    r4 = 6
                    r2 = r1
                    r5 = r3
                    com.fossil.Hq4.d(r0, r1, r2, r3, r4, r5)
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ci r0 = r7.this$0
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r0.this$0
                    r1 = -1
                    java.lang.String r2 = "Fail to download data required for this theme"
                    com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.o(r0, r1, r2)
                    goto L_0x002c
                L_0x009b:
                    r4 = r0
                    goto L_0x0040
                */
                throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ci.Aii.invokeSuspend(java.lang.Object):java.lang.Object");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ci(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ci ci = new Ci(this.this$0, xe6);
            ci.p$ = (Il6) obj;
            throw null;
            //return ci;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ci) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                Hq4.d(this.this$0, true, false, null, 6, null);
                Dv7 b = Bw7.b();
                Aii aii = new Aii(this, null);
                this.L$0 = il6;
                this.label = 1;
                if (Eu7.g(b, aii, this) == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {530}, m = "checkAndDownloadThemeData")
    public static final class Di extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Di(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.Q(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$generateThemeBinary$1", f = "WatchFaceEditViewModel.kt", l = {217, 220, 223}, m = "invokeSuspend")
    public static final class Ei extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ei(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ei ei = new Ei(this.this$0, xe6);
            ei.p$ = (Il6) obj;
            throw null;
            //return ei;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ei) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x0076  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00d7  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x00ef  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r14) {
            /*
            // Method dump skipped, instructions count: 257
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ei.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$getComplicationData$1", f = "WatchFaceEditViewModel.kt", l = {453}, m = "invokeSuspend")
    public static final class Fi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationId;
        @DexIgnore
        public /* final */ /* synthetic */ Za7 $dimension;
        @DexIgnore
        public /* final */ /* synthetic */ Ab7 $event;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Fi(WatchFaceEditViewModel watchFaceEditViewModel, String str, Za7 za7, Ab7 ab7, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
            this.$complicationId = str;
            this.$dimension = za7;
            this.$event = ab7;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Fi fi = new Fi(this.this$0, this.$complicationId, this.$dimension, this.$event, xe6);
            fi.p$ = (Il6) obj;
            throw null;
            //return fi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Fi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object X;
            Object obj2;
            String str = null;
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                WatchFaceEditViewModel watchFaceEditViewModel = this.this$0;
                String str2 = this.$complicationId;
                this.L$0 = il6;
                this.label = 1;
                X = watchFaceEditViewModel.X(str2, this);
                if (X == d) {
                    return d;
                }
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                X = obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str3 = (String) X;
            if (Wg6.a(this.$complicationId, "second-timezone")) {
                Iterator it = this.this$0.p.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        obj2 = null;
                        break;
                    }
                    Object next = it.next();
                    if (Ao7.a(Wg6.a(((DianaAppSetting) next).getAppId(), this.$complicationId)).booleanValue()) {
                        obj2 = next;
                        break;
                    }
                }
                DianaAppSetting dianaAppSetting = (DianaAppSetting) obj2;
                if (dianaAppSetting != null) {
                    str = dianaAppSetting.getSetting();
                }
            }
            this.this$0.q.l(new Bi(this.$event, new Bb7(this.$complicationId, str3, str, this.$dimension)));
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {479, 480}, m = "getComplicationSettingData")
    public static final class Gi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Gi(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.X(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$handleResultFromThemeAction$1", f = "WatchFaceEditViewModel.kt", l = {272, 290}, m = "invokeSuspend")
    public static final class Hi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $action;
        @DexIgnore
        public /* final */ /* synthetic */ int $errorCode;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isSucceeded;
        @DexIgnore
        public /* final */ /* synthetic */ byte[] $themeBinary;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Hi(WatchFaceEditViewModel watchFaceEditViewModel, String str, boolean z, byte[] bArr, int i, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
            this.$action = str;
            this.$isSucceeded = z;
            this.$themeBinary = bArr;
            this.$errorCode = i;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Hi hi = new Hi(this.this$0, this.$action, this.$isSucceeded, this.$themeBinary, this.$errorCode, xe6);
            hi.p$ = (Il6) obj;
            throw null;
            //return hi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Hi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                String str = this.$action;
                int hashCode = str.hashCode();
                if (hashCode != -1937539297) {
                    if (hashCode != -442528637) {
                        if (hashCode == 1718094493 && str.equals("apply_theme_action")) {
                            if (this.$isSucceeded) {
                                byte[] bArr = this.$themeBinary;
                                if (bArr != null) {
                                    WatchFaceEditViewModel watchFaceEditViewModel = this.this$0;
                                    this.L$0 = il6;
                                    this.label = 1;
                                    if (watchFaceEditViewModel.o0(bArr, this) == d) {
                                        return d;
                                    }
                                } else {
                                    Hq4.d(this.this$0, false, false, null, 6, null);
                                    this.this$0.a(-1, "apply them succeed but no data received");
                                }
                            } else {
                                this.this$0.i.l(new Gl7(Ao7.a(false), Ao7.a(false), Ao7.e(this.$errorCode)));
                                Hq4.d(this.this$0, false, false, null, 6, null);
                            }
                        }
                    } else if (str.equals("preview_theme_action")) {
                        this.this$0.i.l(new Gl7(Ao7.a(this.$isSucceeded), Ao7.a(false), Ao7.e(this.$errorCode)));
                        Hq4.d(this.this$0, false, false, null, 6, null);
                    }
                } else if (str.equals("parse_theme_data_to_bin_action")) {
                    this.this$0.i.l(new Gl7(Ao7.a(this.$isSucceeded), Ao7.a(false), Ao7.e(this.$errorCode)));
                    WatchFaceEditViewModel watchFaceEditViewModel2 = this.this$0;
                    byte[] bArr2 = this.$themeBinary;
                    this.L$0 = il6;
                    this.label = 2;
                    if (watchFaceEditViewModel2.g0(bArr2, this) == d) {
                        return d;
                    }
                }
            } else if (i == 1 || i == 2) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return Cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {261}, m = "handleThemeBinaryOfInactivePreset")
    public static final class Ii extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ii(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.g0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {465}, m = "isReachedMaxPreset")
    public static final class Ji extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ji(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.h0(this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$isReachedMaxPreset$totalPreset$1", f = "WatchFaceEditViewModel.kt", l = {465}, m = "invokeSuspend")
    public static final class Ki extends Ko7 implements Coroutine<Il6, Xe6<? super Integer>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ki(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ki ki = new Ki(this.this$0, xe6);
            ki.p$ = (Il6) obj;
            throw null;
            //return ki;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Integer> xe6) {
            throw null;
            //return ((Ki) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            Object d = Yn7.d();
            int i = this.label;
            if (i == 0) {
                El7.b(obj);
                Il6 il6 = this.p$;
                DianaPresetRepository dianaPresetRepository = this.this$0.z;
                String J = PortfolioApp.get.instance().J();
                this.L$0 = il6;
                this.label = 1;
                Object i2 = dianaPresetRepository.i(J, this);
                return i2 == d ? d : i2;
            } else if (i == 1) {
                Il6 il62 = (Il6) this.L$0;
                El7.b(obj);
                return obj;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$loadWatchFace$2", f = "WatchFaceEditViewModel.kt", l = {389, MFNetworkReturnCode.WRONG_PASSWORD, 414, MFNetworkReturnCode.CONTENT_TYPE_ERROR, 416, 434}, m = "invokeSuspend")
    public static final class Li extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$12;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Li(WatchFaceEditViewModel watchFaceEditViewModel, String str, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
            this.$presetId = str;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Li li = new Li(this.this$0, this.$presetId, xe6);
            li.p$ = (Il6) obj;
            throw null;
            //return li;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Li) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:18:0x0113  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x0133  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x01b2  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x0240  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x02ca  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x02f1  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0387  */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0457  */
        /* JADX WARNING: Removed duplicated region for block: B:65:0x0465  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x047b  */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0484  */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0477 A[SYNTHETIC] */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r25) {
            /*
            // Method dump skipped, instructions count: 1196
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Li.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$onBackgroundChanged$1", f = "WatchFaceEditViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class Mi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Fb7 $newBackground;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Mi(WatchFaceEditViewModel watchFaceEditViewModel, Fb7 fb7, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
            this.$newBackground = fb7;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Mi mi = new Mi(this.this$0, this.$newBackground, xe6);
            mi.p$ = (Il6) obj;
            throw null;
            //return mi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Mi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            File file;
            Yn7.d();
            if (this.label == 0) {
                El7.b(obj);
                Fb7 fb7 = this.$newBackground;
                if (fb7 != null) {
                    this.this$0.m = new Ai(fb7.a(), this.$newBackground.d());
                    if (this.$newBackground.c() == null) {
                        file = this.this$0.A.getFileByRemoteUrl(this.$newBackground.g());
                    } else {
                        String c = this.$newBackground.c();
                        if (c != null) {
                            file = new File(c);
                        } else {
                            Wg6.i();
                            throw null;
                        }
                    }
                    byte[] b = file != null ? J37.b(file) : null;
                    if (b == null) {
                        String g = this.$newBackground.g();
                        if (!(g == null || g.length() == 0)) {
                            Set set = this.this$0.s;
                            String g2 = this.$newBackground.g();
                            if (g2 != null) {
                                set.add(g2);
                                FileRepository fileRepository = this.this$0.A;
                                String g3 = this.$newBackground.g();
                                if (g3 != null) {
                                    FileRepository.asyncDownloadFromUrl$default(fileRepository, g3, FileType.WATCH_FACE, null, 4, null);
                                } else {
                                    Wg6.i();
                                    throw null;
                                }
                            } else {
                                Wg6.i();
                                throw null;
                            }
                        }
                    }
                    this.this$0.n.d(new Hb7(b, this.$newBackground.b()));
                } else {
                    this.this$0.n.d(null);
                }
                this.this$0.h.l(Ao7.a(!Wg6.a(this.this$0.n, this.this$0.o)));
                return Cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$onMovingObjectChanged$1", f = "WatchFaceEditViewModel.kt", l = {689, 486}, m = "invokeSuspend")
    public static final class Ni extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ S87 $elementConfig;
        @DexIgnore
        public /* final */ /* synthetic */ W67.Bi $type;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ni this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Aii(Xe6 xe6, Ni ni) {
                super(2, xe6);
                this.this$0 = ni;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6, this.this$0);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    List<Gb7> a2 = this.this$0.this$0.n.a();
                    int i2 = B87.a[this.this$0.$type.ordinal()];
                    if (i2 == 1) {
                        Ni ni = this.this$0;
                        a2.add(ni.this$0.p0(ni.$elementConfig));
                    } else if (i2 == 2) {
                        Ni ni2 = this.this$0;
                        a2.add(ni2.this$0.p0(ni2.$elementConfig));
                        Ni ni3 = this.this$0;
                        ni3.this$0.n0(ni3.$elementConfig.c(), a2);
                    } else if (i2 == 3) {
                        Ni ni4 = this.this$0;
                        ni4.this$0.n0(ni4.$elementConfig.a(), a2);
                    }
                    this.this$0.this$0.n.c(a2);
                    this.this$0.this$0.h.l(Ao7.a(!Wg6.a(this.this$0.this$0.n, this.this$0.this$0.o)));
                    Ni ni5 = this.this$0;
                    WatchFaceEditViewModel watchFaceEditViewModel = ni5.this$0;
                    S87 s87 = ni5.$elementConfig;
                    this.L$0 = il6;
                    this.L$1 = a2;
                    this.label = 1;
                    if (watchFaceEditViewModel.R(s87, this) == d) {
                        return d;
                    }
                } else if (i == 1) {
                    List list = (List) this.L$1;
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ni(WatchFaceEditViewModel watchFaceEditViewModel, W67.Bi bi, S87 s87, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
            this.$type = bi;
            this.$elementConfig = s87;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ni ni = new Ni(this.this$0, this.$type, this.$elementConfig, xe6);
            ni.p$ = (Il6) obj;
            throw null;
            //return ni;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ni) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:15:0x004c  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r8) {
            /*
                r7 = this;
                r1 = 2
                r3 = 1
                r6 = 0
                java.lang.Object r2 = com.fossil.Yn7.d()
                int r0 = r7.label
                if (r0 == 0) goto L_0x004e
                if (r0 == r3) goto L_0x002a
                if (r0 != r1) goto L_0x0022
                java.lang.Object r0 = r7.L$1
                com.fossil.U08 r0 = (com.fossil.U08) r0
                java.lang.Object r1 = r7.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r8)     // Catch:{ all -> 0x006c }
            L_0x001a:
                com.mapped.Cd6 r1 = com.mapped.Cd6.a     // Catch:{ all -> 0x006c }
                r0.b(r6)
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0021:
                return r0
            L_0x0022:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x002a:
                java.lang.Object r0 = r7.L$1
                com.fossil.U08 r0 = (com.fossil.U08) r0
                java.lang.Object r1 = r7.L$0
                com.mapped.Il6 r1 = (com.mapped.Il6) r1
                com.fossil.El7.b(r8)
            L_0x0035:
                com.fossil.Dv7 r3 = com.fossil.Bw7.a()     // Catch:{ all -> 0x0067 }
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ni$Aii r4 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ni$Aii     // Catch:{ all -> 0x0067 }
                r5 = 0
                r4.<init>(r5, r7)     // Catch:{ all -> 0x0067 }
                r7.L$0 = r1     // Catch:{ all -> 0x0067 }
                r7.L$1 = r0     // Catch:{ all -> 0x0067 }
                r1 = 2
                r7.label = r1     // Catch:{ all -> 0x0067 }
                java.lang.Object r1 = com.fossil.Eu7.g(r3, r4, r7)     // Catch:{ all -> 0x0067 }
                if (r1 != r2) goto L_0x001a
                r0 = r2
                goto L_0x0021
            L_0x004e:
                com.fossil.El7.b(r8)
                com.mapped.Il6 r1 = r7.p$
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r7.this$0
                com.fossil.U08 r0 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.v(r0)
                r7.L$0 = r1
                r7.L$1 = r0
                r7.label = r3
                java.lang.Object r3 = r0.a(r6, r7)
                if (r3 != r2) goto L_0x0035
                r0 = r2
                goto L_0x0021
            L_0x0067:
                r1 = move-exception
            L_0x0068:
                r0.b(r6)
                throw r1
            L_0x006c:
                r1 = move-exception
                goto L_0x0068
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ni.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$previewTheme$1", f = "WatchFaceEditViewModel.kt", l = {200, 203}, m = "invokeSuspend")
    public static final class Oi extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Oi(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(2, xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Oi oi = new Oi(this.this$0, xe6);
            oi.p$ = (Il6) obj;
            throw null;
            //return oi;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Oi) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0070  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r10) {
            /*
                r9 = this;
                r4 = 6
                r8 = 2
                r1 = 1
                r3 = 0
                r2 = 0
                java.lang.Object r7 = com.fossil.Yn7.d()
                int r0 = r9.label
                if (r0 == 0) goto L_0x0057
                if (r0 == r1) goto L_0x0023
                if (r0 != r8) goto L_0x001b
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
            L_0x0018:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x001a:
                return r0
            L_0x001b:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0023:
                java.lang.Object r0 = r9.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r10)
                r5 = r0
                r1 = r10
            L_0x002c:
                r0 = r1
                java.lang.Boolean r0 = (java.lang.Boolean) r0
                boolean r0 = r0.booleanValue()
                if (r0 == 0) goto L_0x0070
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.I(r0)
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.n(r0)
                com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
                com.portfolio.platform.PortfolioApp r0 = r0.instance()
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r1 = r9.this$0
                com.fossil.Ob7 r1 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.p(r1)
                r9.L$0 = r5
                r9.label = r8
                java.lang.Object r0 = r0.Q0(r1, r9)
                if (r0 != r7) goto L_0x0018
                r0 = r7
                goto L_0x001a
            L_0x0057:
                com.fossil.El7.b(r10)
                com.mapped.Il6 r6 = r9.p$
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                r5 = r3
                com.fossil.Hq4.d(r0, r1, r2, r3, r4, r5)
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                r9.L$0 = r6
                r9.label = r1
                java.lang.Object r1 = r0.Q(r9)
                if (r1 != r7) goto L_0x009d
                r0 = r7
                goto L_0x001a
            L_0x0070:
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                r1 = r2
                r5 = r3
                com.fossil.Hq4.d(r0, r1, r2, r3, r4, r5)
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                r1 = -1
                java.lang.String r3 = "Fail to download data required for this theme"
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.o(r0, r1, r3)
                com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = r9.this$0
                androidx.lifecycle.MutableLiveData r0 = com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.B(r0)
                com.fossil.Gl7 r1 = new com.fossil.Gl7
                java.lang.Boolean r3 = com.fossil.Ao7.a(r2)
                java.lang.Boolean r2 = com.fossil.Ao7.a(r2)
                r4 = 600(0x258, float:8.41E-43)
                java.lang.Integer r4 = com.fossil.Ao7.e(r4)
                r1.<init>(r3, r2, r4)
                r0.l(r1)
                goto L_0x0018
            L_0x009d:
                r5 = r6
                goto L_0x002c
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Oi.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.WatchFaceEditViewModel", f = "WatchFaceEditViewModel.kt", l = {298, Action.Presenter.PREVIOUS, 311, 324, 339, 346}, m = "savePresetWithThemeBinary")
    public static final class Pi extends Jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Pi(WatchFaceEditViewModel watchFaceEditViewModel, Xe6 xe6) {
            super(xe6);
            this.this$0 = watchFaceEditViewModel;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= RecyclerView.UNDEFINED_DURATION;
            return this.this$0.o0(null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Qi extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ WatchFaceEditViewModel a;

        @DexIgnore
        /* JADX WARN: Incorrect args count in method signature: ()V */
        public Qi(WatchFaceEditViewModel watchFaceEditViewModel) {
            this.a = watchFaceEditViewModel;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WatchFaceEditViewModel", "wfBleConnection onReceive " + intent);
            if (intent != null) {
                String action = intent.getAction();
                if (action == null) {
                    action = "";
                }
                Wg6.b(action, "it.action ?: \"\"");
                this.a.f0(action, intent.getBooleanExtra("theme_result_extra", true), intent.getByteArrayExtra(ButtonService.THEME_BINARY_EXTRA), intent.getIntExtra("error_code_extra", -1));
            }
        }
    }

    @DexIgnore
    public WatchFaceEditViewModel(CustomizeRealDataManager customizeRealDataManager, UserRepository userRepository, CustomizeRealDataRepository customizeRealDataRepository, DianaPresetRepository dianaPresetRepository, FileRepository fileRepository, WFAssetRepository wFAssetRepository, DianaAppSettingRepository dianaAppSettingRepository, DianaWatchFaceRepository dianaWatchFaceRepository) {
        Wg6.c(customizeRealDataManager, "customizeRealDataManager");
        Wg6.c(userRepository, "userRepository");
        Wg6.c(customizeRealDataRepository, "customizeRealDataRepository");
        Wg6.c(dianaPresetRepository, "presetRepository");
        Wg6.c(fileRepository, "fileRepository");
        Wg6.c(wFAssetRepository, "wfAssetRepository");
        Wg6.c(dianaAppSettingRepository, "mDianaAppSettingRepository");
        Wg6.c(dianaWatchFaceRepository, "mWatchFaceRepository");
        this.w = customizeRealDataManager;
        this.x = userRepository;
        this.y = customizeRealDataRepository;
        this.z = dianaPresetRepository;
        this.A = fileRepository;
        this.B = wFAssetRepository;
        this.C = dianaAppSettingRepository;
        this.D = dianaWatchFaceRepository;
        I77.c.c();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("preview_theme_action");
        intentFilter.addAction("apply_theme_action");
        intentFilter.addAction("parse_theme_data_to_bin_action");
        Ct0.b(PortfolioApp.get.instance()).c(this.v, intentFilter);
    }

    @DexIgnore
    public final void P() {
        byte[] h2;
        byte[] a2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("applyTheme - backgroud size: ");
        Hb7 b = this.n.b();
        sb.append((b == null || (a2 = b.a()) == null) ? null : Integer.valueOf(a2.length));
        sb.append(' ');
        sb.append("- complication counted: ");
        sb.append(this.n.a().size());
        sb.append(" - each size : ");
        List<Gb7> a3 = this.n.a();
        ArrayList arrayList = new ArrayList(Im7.m(a3, 10));
        for (T t2 : a3) {
            if (!(t2 instanceof Ib7)) {
                t2 = null;
            }
            T t3 = t2;
            arrayList.add((t3 == null || (h2 = t3.h()) == null) ? null : Integer.valueOf(h2.length));
        }
        sb.append(arrayList);
        local.d("1MBDebugTag", sb.toString());
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ci(this, null), 3, null);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:57)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0092 A[SYNTHETIC] */
    public final /* synthetic */ java.lang.Object Q(com.mapped.Xe6<? super java.lang.Boolean> r11) {
        /*
            r10 = this;
            r3 = 0
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 1
            boolean r0 = r11 instanceof com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Di
            if (r0 == 0) goto L_0x006f
            r0 = r11
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Di r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Di) r0
            int r1 = r0.label
            r2 = r1 & r4
            if (r2 == 0) goto L_0x006f
            int r1 = r1 + r4
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r2 = r4.result
            java.lang.Object r9 = com.fossil.Yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x007e
            if (r0 != r7) goto L_0x0076
            java.lang.Object r0 = r4.L$3
            java.io.File r0 = (java.io.File) r0
            java.lang.Object r0 = r4.L$2
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$1
            java.util.Iterator r0 = (java.util.Iterator) r0
            java.lang.Object r1 = r4.L$0
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r1 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel) r1
            com.fossil.El7.b(r2)
            r5 = r0
            r10 = r1
        L_0x0036:
            r0 = r2
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x008a
            r0 = r7
            r8 = r5
        L_0x0041:
            boolean r1 = r8.hasNext()
            if (r1 == 0) goto L_0x0092
            java.lang.Object r1 = r8.next()
            java.lang.String r1 = (java.lang.String) r1
            com.portfolio.platform.data.source.FileRepository r2 = r10.A
            java.io.File r5 = r2.getFileByRemoteUrl(r1)
            if (r5 != 0) goto L_0x008e
            if (r0 == 0) goto L_0x0099
            com.portfolio.platform.data.source.FileRepository r0 = r10.A
            com.misfit.frameworks.buttonservice.model.FileType r2 = com.misfit.frameworks.buttonservice.model.FileType.WATCH_FACE
            r4.L$0 = r10
            r4.L$1 = r8
            r4.L$2 = r1
            r4.L$3 = r5
            r4.label = r7
            r5 = 4
            r6 = r3
            java.lang.Object r2 = com.portfolio.platform.data.source.FileRepository.downloadAndSaveWithRemoteUrl$default(r0, r1, r2, r3, r4, r5, r6)
            if (r2 != r9) goto L_0x0097
            r0 = r9
        L_0x006e:
            return r0
        L_0x006f:
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Di r0 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Di
            r0.<init>(r10, r11)
            r4 = r0
            goto L_0x0015
        L_0x0076:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x007e:
            com.fossil.El7.b(r2)
            java.util.Set<java.lang.String> r0 = r10.s
            java.util.Iterator r1 = r0.iterator()
            r0 = r7
            r8 = r1
            goto L_0x0041
        L_0x008a:
            r1 = r5
        L_0x008b:
            r0 = 0
            r8 = r1
            goto L_0x0041
        L_0x008e:
            r8.remove()
            goto L_0x0041
        L_0x0092:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
            goto L_0x006e
        L_0x0097:
            r5 = r8
            goto L_0x0036
        L_0x0099:
            r1 = r8
            goto L_0x008b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Q(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final /* synthetic */ Object R(S87 s87, Xe6<? super Cd6> xe6) {
        if (s87 instanceof S87.Ai) {
            S87.Ai ai = (S87.Ai) s87;
            if (ai.g() == null) {
                Cb7 k2 = ai.k();
                if ((k2 != null ? k2.c() : null) != null) {
                    if (!Vt7.l(ai.k().c())) {
                        FileRepository.asyncDownloadFromUrl$default(this.A, ai.k().c(), FileType.WATCH_FACE, null, 4, null);
                        this.s.add(ai.k().c());
                        this.t.put(Ao7.e(s87.a()), ai.k().c());
                    } else {
                        Bitmap c = A97.b.c(ai.k().b());
                        if (c != null) {
                            this.t.put(Ao7.e(s87.a()), c);
                        }
                    }
                }
            }
        } else if (s87 instanceof S87.Bi) {
            S87.Bi bi = (S87.Bi) s87;
            if (bi.i() == null && (!bi.m().isEmpty())) {
                if (bi.h() == O87.BLACK) {
                    FileRepository.asyncDownloadFromUrl$default(this.A, bi.m().get(0).c(), FileType.WATCH_FACE, null, 4, null);
                    this.s.add(bi.m().get(0).c());
                    this.t.put(Ao7.e(s87.a()), bi.m().get(0).c());
                } else {
                    FileRepository.asyncDownloadFromUrl$default(this.A, bi.m().get(1).c(), FileType.WATCH_FACE, null, 4, null);
                    this.s.add(bi.m().get(1).c());
                    this.t.put(Ao7.e(s87.a()), bi.m().get(1).c());
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.e("WatchFaceEditViewModel", "needDownloadingUrl: " + this.s);
        return Cd6.a;
    }

    @DexIgnore
    public final void S() {
        T t2;
        for (T t3 : this.n.a()) {
            if (t3.getType() == Lb7.SECOND_TIME) {
                Iterator<T> it = this.p.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t2 = null;
                        break;
                    }
                    T next = it.next();
                    if (Wg6.a(next.getAppId(), "second-timezone")) {
                        t2 = next;
                        break;
                    }
                }
                T t4 = t2;
                String setting = t4 != null ? t4.getSetting() : null;
                FLogger.INSTANCE.getLocal().d("WatchFaceEditViewModel", "checkingSecondTimeZoneComplication, setting = " + setting);
                if (!(setting == null || Vt7.l(setting))) {
                    SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) new Gson().k(setting, SecondTimezoneSetting.class);
                    if (t3 != null) {
                        T t5 = t3;
                        t5.j(Integer.valueOf(ConversionUtils.INSTANCE.getTimezoneRawOffsetById(secondTimezoneSetting.getTimeZoneId())));
                        t5.i(secondTimezoneSetting.getCityCode());
                    } else {
                        throw new Rc6("null cannot be cast to non-null type com.portfolio.platform.watchface.model.theme.communication.WFComplicationData");
                    }
                } else {
                    continue;
                }
            }
        }
    }

    @DexIgnore
    public final void T() {
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Ei(this, null), 2, null);
    }

    @DexIgnore
    public final LiveData<Gl7<Boolean, Boolean, Integer>> U() {
        return this.i;
    }

    @DexIgnore
    public final void V(Ab7 ab7, String str, Za7 za7) {
        Wg6.c(ab7, Constants.EVENT);
        Wg6.c(str, "complicationId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "getComplicationData, complicationId = " + str);
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Fi(this, str, za7, ab7, null), 2, null);
    }

    @DexIgnore
    public final LiveData<Bi> W() {
        return this.q;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x006d  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object X(java.lang.String r9, com.mapped.Xe6<? super java.lang.String> r10) {
        /*
            r8 = this;
            r7 = 2
            r5 = 1
            r3 = -2147483648(0xffffffff80000000, float:-0.0)
            boolean r0 = r10 instanceof com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Gi
            if (r0 == 0) goto L_0x0037
            r0 = r10
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Gi r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Gi) r0
            int r1 = r0.label
            r2 = r1 & r3
            if (r2 == 0) goto L_0x0037
            int r1 = r1 + r3
            r0.label = r1
            r4 = r0
        L_0x0015:
            java.lang.Object r3 = r4.result
            java.lang.Object r6 = com.fossil.Yn7.d()
            int r0 = r4.label
            if (r0 == 0) goto L_0x006d
            if (r0 == r5) goto L_0x0046
            if (r0 != r7) goto L_0x003e
            java.lang.Object r0 = r4.L$3
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r0 = r4.L$1
            java.lang.String r0 = (java.lang.String) r0
            java.lang.Object r0 = r4.L$0
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel) r0
            com.fossil.El7.b(r3)
        L_0x0036:
            return r3
        L_0x0037:
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Gi r0 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Gi
            r0.<init>(r8, r10)
            r4 = r0
            goto L_0x0015
        L_0x003e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0046:
            java.lang.Object r0 = r4.L$2
            java.util.List r0 = (java.util.List) r0
            java.lang.Object r1 = r4.L$1
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r2 = r4.L$0
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r2 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel) r2
            com.fossil.El7.b(r3)
            r5 = r0
        L_0x0056:
            r0 = r3
            com.portfolio.platform.data.model.MFUser r0 = (com.portfolio.platform.data.model.MFUser) r0
            com.portfolio.platform.manager.CustomizeRealDataManager r3 = r2.w
            r4.L$0 = r2
            r4.L$1 = r1
            r4.L$2 = r5
            r4.L$3 = r0
            r4.label = r7
            java.lang.Object r3 = r3.d(r0, r5, r1, r4)
            if (r3 != r6) goto L_0x0036
            r3 = r6
            goto L_0x0036
        L_0x006d:
            com.fossil.El7.b(r3)
            com.misfit.frameworks.buttonservice.log.FLogger r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r0 = r0.getLocal()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "complicationId = "
            r1.append(r2)
            r1.append(r9)
            java.lang.String r2 = "WatchFaceEditViewModel"
            java.lang.String r1 = r1.toString()
            r0.d(r2, r1)
            com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository r0 = r8.y
            java.util.List r0 = r0.getAllRealDataRaw()
            com.portfolio.platform.data.source.UserRepository r1 = r8.x
            r4.L$0 = r8
            r4.L$1 = r9
            r4.L$2 = r0
            r4.label = r5
            java.lang.Object r3 = r1.getCurrentUser(r4)
            if (r3 != r6) goto L_0x00a4
            r3 = r6
            goto L_0x0036
        L_0x00a4:
            r1 = r9
            r5 = r0
            r2 = r8
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.X(java.lang.String, com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final LiveData<List<S87>> Y() {
        return this.r;
    }

    @DexIgnore
    public final List<Jn5.Ai> Z(List<? extends S87> list) {
        Wg6.c(list, "elementConfigs");
        ArrayList arrayList = new ArrayList();
        for (T t2 : list) {
            if (t2 instanceof S87.Ai) {
                arrayList.add(t2);
            }
        }
        ArrayList<S87.Ai> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (Ik5.d.c(((S87.Ai) obj).i())) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (S87.Ai ai : arrayList2) {
            Jn5.Ai b = Hl5.a.b(ai.i());
            if (b != null) {
                arrayList3.add(b);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "missing features " + arrayList3);
        return arrayList3;
    }

    @DexIgnore
    public final Mo5 a0() {
        return this.l;
    }

    @DexIgnore
    public final LiveData<Object> b0() {
        return this.k;
    }

    @DexIgnore
    public final List<String> c0(List<? extends S87> list) {
        T t2;
        Wg6.c(list, "elementConfigs");
        ArrayList arrayList = new ArrayList();
        for (T t3 : list) {
            if (t3 instanceof S87.Ai) {
                arrayList.add(t3);
            }
        }
        ArrayList<S87.Ai> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (Ik5.d.d(((S87.Ai) obj).i())) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList();
        for (S87.Ai ai : arrayList2) {
            Iterator<T> it = this.p.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t2 = null;
                    break;
                }
                T next = it.next();
                if (Wg6.a(next.getAppId(), ai.i())) {
                    t2 = next;
                    break;
                }
            }
            if (t2 == null) {
                String i2 = ai.i();
                int hashCode = i2.hashCode();
                if (hashCode != -829740640) {
                    if (hashCode == 134170930 && i2.equals("second-timezone")) {
                        arrayList3.add("Please choose a city for second timezone complication");
                    }
                } else if (i2.equals("commute-time")) {
                    String c = Um5.c(PortfolioApp.get.instance(), 2131886368);
                    Wg6.b(c, "LanguageHelper.getString\u2026ayCommuteTimePleaseEnter)");
                    arrayList3.add(c);
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "missing setting " + arrayList3);
        return arrayList3;
    }

    @DexIgnore
    public final LiveData<Ub7> d0() {
        return this.j;
    }

    @DexIgnore
    public final LiveData<Boolean> e0() {
        return this.h;
    }

    @DexIgnore
    public final void f0(String str, boolean z2, byte[] bArr, int i2) {
        Wg6.c(str, "action");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "handleResultFromThemeAction action " + str + " isSucceed " + z2 + " errorCode " + i2);
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Hi(this, str, z2, bArr, i2, null), 2, null);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:13:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0020  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object g0(byte[] r9, com.mapped.Xe6<? super com.mapped.Cd6> r10) {
        /*
            r8 = this;
            r3 = 0
            r7 = 1
            r5 = -2147483648(0xffffffff80000000, float:-0.0)
            r1 = 0
            boolean r0 = r10 instanceof com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ii
            if (r0 == 0) goto L_0x0030
            r0 = r10
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ii r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ii) r0
            int r2 = r0.label
            r4 = r2 & r5
            if (r4 == 0) goto L_0x0030
            int r2 = r2 + r5
            r0.label = r2
            r2 = r0
        L_0x0016:
            java.lang.Object r4 = r2.result
            java.lang.Object r0 = com.fossil.Yn7.d()
            int r5 = r2.label
            if (r5 == 0) goto L_0x003f
            if (r5 != r7) goto L_0x0037
            java.lang.Object r0 = r2.L$1
            byte[] r0 = (byte[]) r0
            java.lang.Object r0 = r2.L$0
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel) r0
            com.fossil.El7.b(r4)
        L_0x002d:
            com.mapped.Cd6 r0 = com.mapped.Cd6.a
        L_0x002f:
            return r0
        L_0x0030:
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ii r0 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ii
            r0.<init>(r8, r10)
            r2 = r0
            goto L_0x0016
        L_0x0037:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x003f:
            com.fossil.El7.b(r4)
            com.misfit.frameworks.buttonservice.log.FLogger r4 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r4 = r4.getLocal()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "handleResultFromThemeAction data "
            r5.append(r6)
            r5.append(r9)
            java.lang.String r6 = "WatchFaceEditViewModel"
            java.lang.String r5 = r5.toString()
            r4.d(r6, r5)
            if (r9 != 0) goto L_0x0080
            r4 = 6
            r0 = r8
            r2 = r1
            r5 = r3
            com.fossil.Hq4.d(r0, r1, r2, r3, r4, r5)
            androidx.lifecycle.MutableLiveData<com.fossil.Gl7<java.lang.Boolean, java.lang.Boolean, java.lang.Integer>> r0 = r8.i
            com.fossil.Gl7 r2 = new com.fossil.Gl7
            java.lang.Boolean r3 = com.fossil.Ao7.a(r1)
            java.lang.Boolean r1 = com.fossil.Ao7.a(r1)
            r4 = 600(0x258, float:8.41E-43)
            java.lang.Integer r4 = com.fossil.Ao7.e(r4)
            r2.<init>(r3, r1, r4)
            r0.l(r2)
            goto L_0x002d
        L_0x0080:
            r2.L$0 = r8
            r2.L$1 = r9
            r2.label = r7
            java.lang.Object r1 = r8.o0(r9, r2)
            if (r1 != r0) goto L_0x002d
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.g0(byte[], com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.Object h0(com.mapped.Xe6<? super java.lang.Boolean> r7) {
        /*
            r6 = this;
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r2 = 1
            boolean r0 = r7 instanceof com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ji
            if (r0 == 0) goto L_0x0058
            r0 = r7
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ji r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.Ji) r0
            int r1 = r0.label
            r3 = r1 & r4
            if (r3 == 0) goto L_0x0058
            int r1 = r1 + r4
            r0.label = r1
        L_0x0013:
            java.lang.Object r1 = r0.result
            java.lang.Object r3 = com.fossil.Yn7.d()
            int r4 = r0.label
            if (r4 == 0) goto L_0x0066
            if (r4 != r2) goto L_0x005e
            java.lang.Object r0 = r0.L$0
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel r0 = (com.portfolio.platform.watchface.edit.WatchFaceEditViewModel) r0
            com.fossil.El7.b(r1)
            r6 = r0
        L_0x0027:
            r0 = r1
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            com.misfit.frameworks.buttonservice.log.FLogger r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE
            com.misfit.frameworks.buttonservice.log.ILocalFLogger r1 = r1.getLocal()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "isReachedMaxPreset total "
            r3.append(r4)
            r3.append(r0)
            java.lang.String r4 = "WatchFaceEditViewModel"
            java.lang.String r3 = r3.toString()
            r1.d(r4, r3)
            r1 = 20
            if (r0 < r1) goto L_0x0052
            com.fossil.Mo5 r0 = r6.l
            if (r0 == 0) goto L_0x007f
        L_0x0052:
            r0 = 0
        L_0x0053:
            java.lang.Boolean r0 = com.fossil.Ao7.a(r0)
        L_0x0057:
            return r0
        L_0x0058:
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ji r0 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ji
            r0.<init>(r6, r7)
            goto L_0x0013
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
            r0.<init>(r1)
            throw r0
        L_0x0066:
            com.fossil.El7.b(r1)
            com.fossil.Dv7 r1 = com.fossil.Bw7.b()
            com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ki r4 = new com.portfolio.platform.watchface.edit.WatchFaceEditViewModel$Ki
            r5 = 0
            r4.<init>(r6, r5)
            r0.L$0 = r6
            r0.label = r2
            java.lang.Object r1 = com.fossil.Eu7.g(r1, r4, r0)
            if (r1 != r3) goto L_0x0027
            r0 = r3
            goto L_0x0057
        L_0x007f:
            com.portfolio.platform.PortfolioApp$inner r0 = com.portfolio.platform.PortfolioApp.get
            com.portfolio.platform.PortfolioApp r0 = r0.instance()
            r1 = 2131886573(0x7f1201ed, float:1.9407729E38)
            java.lang.String r0 = com.fossil.Um5.c(r0, r1)
            java.lang.String r1 = "LanguageHelper.getString\u2026t__MaximumPresetsCreated)"
            com.mapped.Wg6.b(r0, r1)
            r1 = -1
            r6.a(r1, r0)
            r0 = r2
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.h0(com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    public final Object i0(String str, Xe6<? super Cd6> xe6) {
        Object g = Eu7.g(Bw7.b(), new Li(this, str, null), xe6);
        return g == Yn7.d() ? g : Cd6.a;
    }

    @DexIgnore
    public final void j0(Fb7 fb7) {
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Mi(this, fb7, null), 2, null);
    }

    @DexIgnore
    public final void k0(S87 s87, W67.Bi bi) {
        Wg6.c(s87, "elementConfig");
        Wg6.c(bi, "type");
        Rm6 unused = Gu7.d(Us0.a(this), null, null, new Ni(this, bi, s87, null), 3, null);
    }

    @DexIgnore
    public final void l0() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "previewTheme - data: " + this.n);
        Rm6 unused = Gu7.d(Us0.a(this), Bw7.b(), null, new Oi(this, null), 2, null);
    }

    @DexIgnore
    public final void m0() {
        int i2;
        byte[] bArr;
        byte[] bArr2;
        for (Map.Entry<Integer, Object> entry : this.t.entrySet()) {
            int intValue = entry.getKey().intValue();
            Object value = entry.getValue();
            Iterator<Gb7> it = this.n.a().iterator();
            int i3 = 0;
            while (true) {
                if (!it.hasNext()) {
                    i2 = -1;
                    break;
                }
                if (it.next().a() == intValue) {
                    i2 = i3;
                    break;
                }
                i3++;
            }
            if (i2 != -1) {
                Gb7 gb7 = this.n.a().get(i2);
                this.n.a().remove(i2);
                if (gb7 instanceof Pb7) {
                    if (value instanceof String) {
                        Pb7 pb7 = (Pb7) gb7;
                        File fileByRemoteUrl = this.A.getFileByRemoteUrl((String) value);
                        if (fileByRemoteUrl == null || (bArr2 = J37.b(fileByRemoteUrl)) == null) {
                            bArr2 = new byte[0];
                        }
                        pb7.f(bArr2);
                    }
                } else if (gb7 instanceof Ib7) {
                    if (value instanceof String) {
                        Ib7 ib7 = (Ib7) gb7;
                        File fileByRemoteUrl2 = this.A.getFileByRemoteUrl((String) value);
                        if (fileByRemoteUrl2 == null || (bArr = J37.b(fileByRemoteUrl2)) == null) {
                            bArr = new byte[0];
                        }
                        ib7.k(bArr);
                    } else if (value instanceof Bitmap) {
                        ((Ib7) gb7).k(Cy1.b((Bitmap) value, null, 1, null));
                    }
                }
                this.n.a().add(i2, gb7);
            }
        }
        this.t.clear();
    }

    @DexIgnore
    public final void n0(int i2, List<Gb7> list) {
        T t2;
        boolean z2;
        this.t.remove(Integer.valueOf(i2));
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            T next = it.next();
            if (next.a() == i2) {
                z2 = true;
                continue;
            } else {
                z2 = false;
                continue;
            }
            if (z2) {
                t2 = next;
                break;
            }
        }
        T t3 = t2;
        if (t3 != null) {
            list.remove(t3);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0034  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0500  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0505  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00d1  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x01db  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0201  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x022c  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0261  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x027f  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0325  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0379  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x037d  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x03a4  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x03c2  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x0485  */
    /* JADX WARNING: Removed duplicated region for block: B:96:0x048a  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0499  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object o0(byte[] r25, com.mapped.Xe6<? super com.mapped.Cd6> r26) {
        /*
        // Method dump skipped, instructions count: 1322
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.o0(byte[], com.mapped.Xe6):java.lang.Object");
    }

    @DexIgnore
    @Override // com.fossil.Ts0
    public void onCleared() {
        I77.c.f();
        Ct0.b(PortfolioApp.get.instance()).e(this.v);
        super.onCleared();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0135  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fossil.Gb7 p0(com.fossil.S87 r12) {
        /*
        // Method dump skipped, instructions count: 322
        */
        throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.WatchFaceEditViewModel.p0(com.fossil.S87):com.fossil.Gb7");
    }

    @DexIgnore
    public final void q0(List<DianaAppSetting> list) {
        Wg6.c(list, "updatedSettings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WatchFaceEditViewModel", "updateComplicationSettings " + list);
        this.p.clear();
        this.p.addAll(list);
    }
}
