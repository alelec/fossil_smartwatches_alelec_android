package com.portfolio.platform.watchface.edit.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.A97;
import com.fossil.El7;
import com.fossil.Jv7;
import com.fossil.Ko7;
import com.fossil.P77;
import com.fossil.Ym5;
import com.fossil.Yn7;
import com.mapped.Cd6;
import com.mapped.Coroutine;
import com.mapped.Il6;
import com.mapped.Lf6;
import com.mapped.WatchFaceFontStorage;
import com.mapped.WatchFaceStickerStorage;
import com.mapped.Wg6;
import com.mapped.Xe6;
import com.misfit.frameworks.buttonservice.model.FileType;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.watchface.data.source.WFAssetRepository;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class StorageHelper {
    @DexIgnore
    public static /* final */ StorageHelper a; // = new StorageHelper();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @Lf6(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2", f = "StorageHelper.kt", l = {51, 52, 53}, m = "invokeSuspend")
    public static final class Ai extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ FileRepository $fileRepository;
        @DexIgnore
        public /* final */ /* synthetic */ WFAssetRepository $wfAssetRepository;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public Il6 p$;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initFontTask$1", f = "StorageHelper.kt", l = {}, m = "invokeSuspend")
        public static final class Aii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;

            @DexIgnore
            public Aii(Xe6 xe6) {
                super(2, xe6);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Aii aii = new Aii(xe6);
                aii.p$ = (Il6) obj;
                throw null;
                //return aii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Aii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Yn7.d();
                if (this.label == 0) {
                    El7.b(obj);
                    if (!WatchFaceFontStorage.d.e()) {
                        WatchFaceFontStorage.d.g();
                    }
                    return Cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initRingTask$1", f = "StorageHelper.kt", l = {40}, m = "invokeSuspend")
        public static final class Bii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Bii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Bii bii = new Bii(this.this$0, xe6);
                bii.p$ = (Il6) obj;
                throw null;
                //return bii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Bii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object f;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    if (!A97.b.d()) {
                        WFAssetRepository wFAssetRepository = this.this$0.$wfAssetRepository;
                        this.L$0 = il6;
                        this.label = 1;
                        f = wFAssetRepository.f(this);
                        if (f == d) {
                            return d;
                        }
                    }
                    return Cd6.a;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    f = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                for (P77 p77 : (List) f) {
                    File fileByRemoteUrl = this.this$0.$fileRepository.getFileByRemoteUrl(p77.c().a());
                    Bitmap decodeFile = BitmapFactory.decodeFile(fileByRemoteUrl != null ? fileByRemoteUrl.getAbsolutePath() : null);
                    if (decodeFile != null) {
                        A97.b.a(p77.d(), decodeFile);
                    }
                }
                return Cd6.a;
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @Lf6(c = "com.portfolio.platform.watchface.edit.model.StorageHelper$initAll$2$initStickerTask$1", f = "StorageHelper.kt", l = {24}, m = "invokeSuspend")
        public static final class Cii extends Ko7 implements Coroutine<Il6, Xe6<? super Cd6>, Object> {
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public int label;
            @DexIgnore
            public Il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ Ai this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Cii(Ai ai, Xe6 xe6) {
                super(2, xe6);
                this.this$0 = ai;
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
                Wg6.c(xe6, "completion");
                Cii cii = new Cii(this.this$0, xe6);
                cii.p$ = (Il6) obj;
                throw null;
                //return cii;
            }

            @DexIgnore
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
            @Override // com.mapped.Coroutine
            public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
                throw null;
                //return ((Cii) create(il6, xe6)).invokeSuspend(Cd6.a);
            }

            @DexIgnore
            @Override // com.fossil.Zn7
            public final Object invokeSuspend(Object obj) {
                Object g;
                Object d = Yn7.d();
                int i = this.label;
                if (i == 0) {
                    El7.b(obj);
                    Il6 il6 = this.p$;
                    if (!WatchFaceStickerStorage.c.d()) {
                        WFAssetRepository wFAssetRepository = this.this$0.$wfAssetRepository;
                        this.L$0 = il6;
                        this.label = 1;
                        g = wFAssetRepository.g(this);
                        if (g == d) {
                            return d;
                        }
                    }
                    return Cd6.a;
                } else if (i == 1) {
                    Il6 il62 = (Il6) this.L$0;
                    El7.b(obj);
                    g = obj;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                List<P77> list = (List) g;
                for (P77 p77 : list) {
                    String b = Ym5.b(p77.c().a());
                    FileRepository fileRepository = this.this$0.$fileRepository;
                    Wg6.b(b, "fileName");
                    File fileByName = fileRepository.getFileByName(b, FileType.WATCH_FACE);
                    Bitmap decodeFile = BitmapFactory.decodeFile(fileByName != null ? fileByName.getAbsolutePath() : null);
                    if (decodeFile != null) {
                        WatchFaceStickerStorage.c.a(p77.f(), decodeFile);
                    }
                }
                WatchFaceStickerStorage.c.g(list);
                return Cd6.a;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Ai(WFAssetRepository wFAssetRepository, FileRepository fileRepository, Xe6 xe6) {
            super(2, xe6);
            this.$wfAssetRepository = wFAssetRepository;
            this.$fileRepository = fileRepository;
        }

        @DexIgnore
        @Override // com.fossil.Zn7
        public final Xe6<Cd6> create(Object obj, Xe6<?> xe6) {
            Wg6.c(xe6, "completion");
            Ai ai = new Ai(this.$wfAssetRepository, this.$fileRepository, xe6);
            ai.p$ = (Il6) obj;
            throw null;
            //return ai;
        }

        @DexIgnore
        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object, java.lang.Object] */
        @Override // com.mapped.Coroutine
        public final Object invoke(Il6 il6, Xe6<? super Cd6> xe6) {
            throw null;
            //return ((Ai) create(il6, xe6)).invokeSuspend(Cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:12:0x0053  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0079  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x00bd  */
        @Override // com.fossil.Zn7
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.Object invokeSuspend(java.lang.Object r12) {
            /*
                r11 = this;
                r10 = 3
                r9 = 1
                r4 = 2
                r2 = 0
                java.lang.Object r8 = com.fossil.Yn7.d()
                int r0 = r11.label
                if (r0 == 0) goto L_0x007b
                if (r0 == r9) goto L_0x0055
                if (r0 == r4) goto L_0x0030
                if (r0 != r10) goto L_0x0028
                java.lang.Object r0 = r11.L$3
                com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
                java.lang.Object r0 = r11.L$2
                com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
                java.lang.Object r0 = r11.L$1
                com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
                java.lang.Object r0 = r11.L$0
                com.mapped.Il6 r0 = (com.mapped.Il6) r0
                com.fossil.El7.b(r12)
            L_0x0025:
                com.mapped.Cd6 r0 = com.mapped.Cd6.a
            L_0x0027:
                return r0
            L_0x0028:
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.String r1 = "call to 'resume' before 'invoke' with coroutine"
                r0.<init>(r1)
                throw r0
            L_0x0030:
                java.lang.Object r0 = r11.L$3
                com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
                java.lang.Object r1 = r11.L$2
                com.mapped.Rl6 r1 = (com.mapped.Rl6) r1
                java.lang.Object r2 = r11.L$1
                com.mapped.Rl6 r2 = (com.mapped.Rl6) r2
                java.lang.Object r3 = r11.L$0
                com.mapped.Il6 r3 = (com.mapped.Il6) r3
                com.fossil.El7.b(r12)
            L_0x0043:
                r11.L$0 = r3
                r11.L$1 = r2
                r11.L$2 = r1
                r11.L$3 = r0
                r11.label = r10
                java.lang.Object r0 = r0.l(r11)
                if (r0 != r8) goto L_0x0025
                r0 = r8
                goto L_0x0027
            L_0x0055:
                java.lang.Object r0 = r11.L$3
                com.mapped.Rl6 r0 = (com.mapped.Rl6) r0
                java.lang.Object r1 = r11.L$2
                com.mapped.Rl6 r1 = (com.mapped.Rl6) r1
                java.lang.Object r2 = r11.L$1
                com.mapped.Rl6 r2 = (com.mapped.Rl6) r2
                java.lang.Object r3 = r11.L$0
                com.mapped.Il6 r3 = (com.mapped.Il6) r3
                com.fossil.El7.b(r12)
                r5 = r0
            L_0x0069:
                r11.L$0 = r3
                r11.L$1 = r2
                r11.L$2 = r1
                r11.L$3 = r5
                r11.label = r4
                java.lang.Object r0 = r1.l(r11)
                if (r0 != r8) goto L_0x00bd
                r0 = r8
                goto L_0x0027
            L_0x007b:
                com.fossil.El7.b(r12)
                com.mapped.Il6 r0 = r11.p$
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Aii r3 = new com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Aii
                r3.<init>(r2)
                r5 = r2
                com.mapped.Rl6 r6 = com.fossil.Eu7.b(r0, r1, r2, r3, r4, r5)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Cii r3 = new com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Cii
                r3.<init>(r11, r2)
                r5 = r2
                com.mapped.Rl6 r7 = com.fossil.Eu7.b(r0, r1, r2, r3, r4, r5)
                com.fossil.Dv7 r1 = com.fossil.Bw7.b()
                com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Bii r3 = new com.portfolio.platform.watchface.edit.model.StorageHelper$Ai$Bii
                r3.<init>(r11, r2)
                r5 = r2
                com.mapped.Rl6 r5 = com.fossil.Eu7.b(r0, r1, r2, r3, r4, r5)
                r11.L$0 = r0
                r11.L$1 = r6
                r11.L$2 = r7
                r11.L$3 = r5
                r11.label = r9
                java.lang.Object r1 = r6.l(r11)
                if (r1 != r8) goto L_0x00bf
                r0 = r8
                goto L_0x0027
            L_0x00bd:
                r0 = r5
                goto L_0x0043
            L_0x00bf:
                r2 = r6
                r1 = r7
                r3 = r0
                goto L_0x0069
            */
            throw new UnsupportedOperationException("Method not decompiled: com.portfolio.platform.watchface.edit.model.StorageHelper.Ai.invokeSuspend(java.lang.Object):java.lang.Object");
        }
    }

    @DexIgnore
    public final void a() {
        WatchFaceFontStorage.d.b();
        WatchFaceStickerStorage.c.b();
        A97.b.b();
    }

    @DexIgnore
    public final Object b(WFAssetRepository wFAssetRepository, FileRepository fileRepository, Xe6<? super Cd6> xe6) {
        Object e = Jv7.e(new Ai(wFAssetRepository, fileRepository, null), xe6);
        return e == Yn7.d() ? e : Cd6.a;
    }
}
